-- Stored procedure definition script USP_OpportunityMaster_ValidateContainerQty for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_ValidateContainerQty(v_numDomainID NUMERIC(18,0),
 v_numOppId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitAddedEnoughContainer  BOOLEAN DEFAULT 1;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numTempContainer  NUMERIC(18,0);
   v_numRequiredQty  DOUBLE PRECISION;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numContainerItem NUMERIC(18,0),
      fltRequiredQty DOUBLE PRECISION
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numItemCode NUMERIC(18,0),
      numContainer NUMERIC(18,0),
      numItemQty DOUBLE PRECISION,
      numUnitHour DOUBLE PRECISION,
      bitContainer BOOLEAN
   );
   INSERT INTO
   tt_TEMPITEMS(numItemCode, numContainer, numItemQty, numUnitHour, bitContainer)
   SELECT
   OI.numItemCode,
		coalesce(I.numContainer,0),
		coalesce(I.numNoItemIntoContainer,0),
		OI.numUnitHour,
		(CASE WHEN coalesce(I.bitContainer,false) = true THEN true ELSE false END)
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   AND coalesce(I.charItemType,'') = 'P'
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numOppId = v_numOppId
   ORDER BY
   coalesce(I.bitContainer,false);

   IF(SELECT COUNT(*) FROM tt_TEMPITEMS WHERE bitContainer = false AND numContainer = 0) > 0 then
	
      v_bitAddedEnoughContainer := false;
   ELSE
      INSERT INTO
      tt_TEMP(numContainerItem, fltRequiredQty)
      SELECT
      T1.numContainer
			,SUM(T1.numNoItemIntoContainer)
      FROM(SELECT
         I.numContainer,
				CEIL(SUM(OI.numUnitHour)/CAST(I.numNoItemIntoContainer AS DOUBLE PRECISION)) AS numNoItemIntoContainer
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         INNER JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         AND coalesce(I.charItemType,0) = 'P'
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.numOppId = v_numOppId
         AND numContainer > 0
         GROUP BY
         I.numContainer,I.numNoItemIntoContainer) AS T1
      GROUP BY
      T1.numContainer;
      select   COUNT(*) INTO v_COUNT FROM tt_TEMP;
      WHILE v_i <= v_COUNT LOOP
         select   numContainerItem, fltRequiredQty INTO v_numTempContainer,v_numRequiredQty FROM
         tt_TEMP WHERE
         ID = v_i;
         IF coalesce((SELECT SUM(numUnitHour) FROM OpportunityItems WHERE numOppId = v_numOppId AND numItemCode = v_numTempContainer),0) < v_numRequiredQty then
			
            v_bitAddedEnoughContainer := false;
            EXIT;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;

   open SWV_RefCur for SELECT v_bitAddedEnoughContainer;
END; $$;











