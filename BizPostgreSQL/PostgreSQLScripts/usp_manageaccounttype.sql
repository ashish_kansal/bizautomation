-- Stored procedure definition script USP_ManageAccountType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAccountType(INOUT v_numAccountTypeID NUMERIC(9,0) DEFAULT 0 ,
v_vcAccountCode VARCHAR(50) DEFAULT NULL,
v_vcAccountType VARCHAR(100) DEFAULT NULL,
v_numParentID NUMERIC(9,0) DEFAULT null,
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_tintMode SMALLINT DEFAULT NULL,
v_bitActive BOOLEAN DEFAULT true)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_newAccountCode  VARCHAR(50);
BEGIN
   IF v_tintMode = 1 then

      v_newAccountCode := GetAccountTypeCode(v_numDomainID,v_numParentID,0::SMALLINT);
      INSERT INTO AccountTypeDetail(vcAccountCode,
		vcAccountType,
		numParentID,
		numDomainID,
		dtCreateDate,
		dtModifiedDate,
		bitSystem,
		bitActive) VALUES(v_newAccountCode,
		v_vcAccountType,
		v_numParentID,
		v_numDomainID,
		TIMEZONE('UTC',now()),
		TIMEZONE('UTC',now()),
		false,
		coalesce(v_bitActive, true));
	
      v_numAccountTypeID := CURRVAL('AccountTypeDetail_seq');
   end if;

   IF v_tintMode = 2 then

      IF(SELECT  LENGTH(vcAccountCode) FROM AccountTypeDetail WHERE numAccountTypeID = v_numAccountTypeID AND numDomainID = v_numDomainID) > 4 then
	
--		SELECT @newAccountCode= dbo.GetAccountTypeCode(@numDomainID,@numParentID);
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=@newAccountCode,
--			vcAccountType=@vcAccountType,
--			numParentID=@numParentID
--		WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID
--		-- Update Child if any 
		
		
--Commented by chintan, reason:any account faling under given account type is using prefix of account type code,when changing account type code,then child account codes become invalid.
--		EXEC USP_UpdateAccountTypeCode @numDomainID,@numAccountTypeID,@numParentID
		
         UPDATE AccountTypeDetail SET vcAccountType  = v_vcAccountType,bitActive = coalesce(v_bitActive,true) WHERE numAccountTypeID = v_numAccountTypeID AND numDomainID = v_numDomainID;
      end if;
   end if;

   IF v_tintMode = 3 then

      IF EXISTS(SELECT * FROM AccountTypeDetail WHERE bitSystem = true AND numDomainID = v_numDomainID AND numAccountTypeID = v_numAccountTypeID) then
		
         RAISE EXCEPTION 'SYSTEM';
         RETURN;
      end if;
      IF EXISTS(SELECT * FROM AccountTypeDetail WHERE (vcAccountCode ilike '01020102%' OR vcAccountCode ilike '01010105') AND numDomainID = v_numDomainID AND numAccountTypeID = v_numAccountTypeID) then
		
         RAISE EXCEPTION 'AR/AP';
         RETURN;
      end if;
      IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numParntAcntTypeID = v_numAccountTypeID) then
		
         RAISE EXCEPTION 'CHILD_ACCOUNT';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM AccountTypeDetail WHERE numParentID = v_numAccountTypeID) > 0 then
		
         RAISE EXCEPTION 'CHILD';
         RETURN;
      ELSE
		--only allow deltion of where account code lenth is > 4
         DELETE FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND  numAccountTypeID = v_numAccountTypeID AND LENGTH(vcAccountCode) > 4;
      end if;
   end if;
END; $$;



