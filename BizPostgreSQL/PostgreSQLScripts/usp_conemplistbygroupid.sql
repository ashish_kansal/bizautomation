-- Stored procedure definition script USP_ConEmpListByGroupId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConEmpListByGroupId(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numGroupId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT A.numContactId,CONCAT(A.vcFirstName,' ',A.vcLastname) as vcUserName
   from UserMaster UM
   join AdditionalContactsInformation A
   on UM.numUserDetailId = A.numContactId
   where UM.numDomainID = v_numDomainID and UM.numDomainID = A.numDomainID and UM.intAssociate = 1  AND UM.numGroupID = v_numGroupId;
END; $$;













