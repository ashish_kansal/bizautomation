-- Stored procedure definition script USP_StagePercentageDetailsTaskTimeLog_Save for PostgreSQL
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTaskTimeLog_Save(v_ID NUMERIC(18,0)
	,v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_tintAction SMALLINT
	,v_dtActionTime TIMESTAMP
	,v_numProcessedQty DOUBLE PRECISION
	,v_numReasonForPause NUMERIC(18,0)
	,v_vcNotes VARCHAR(1000)
	,v_bitManualEntry BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProjectId  NUMERIC DEFAULT 0;
   v_dtmActualStartDate  TIMESTAMP;
   v_dtmActualFinishDate  TIMESTAMP;
   v_intTotalProgress  INTEGER DEFAULT 0;
   v_intTotalTaskCount  INTEGER DEFAULT 0;
   v_intTotalTaskClosed  INTEGER DEFAULT 0;
   v_numContractsLogId  NUMERIC(18,0);
   v_numContractID  NUMERIC(18,0);
   v_numUsedTime  INTEGER;
   v_balanceContractTime  INTEGER;
   v_numTaskAssignee  NUMERIC(18,0);
BEGIN
   IF NOT EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND tintAction = 4) then
	
      IF v_tintAction = 1 AND EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND tintAction = 1 AND ID <> coalesce(v_ID,0)) then
		
         RAISE EXCEPTION 'TASK_IS_ALREADY_STARTED';
      ELSE
         IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID = v_ID) then
			
            UPDATE
            StagePercentageDetailsTaskTimeLog
            SET
            tintAction = v_tintAction
			,dtActionTime = '1900-01-01':: date + make_interval(mins => DATEDIFF('minute','1900-01-01'::TIMESTAMP,v_dtActionTime)),
            numProcessedQty = v_numProcessedQty,vcNotes = v_vcNotes,bitManualEntry = v_bitManualEntry,
            numModifiedBy = v_numUserCntID,numReasonForPause = v_numReasonForPause,
            dtModifiedDate = TIMEZONE('UTC',now())
            WHERE
            ID = v_ID;
         ELSE
            IF EXISTS(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID) then
               select   numAssignTo INTO v_numTaskAssignee FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
               INSERT INTO StagePercentageDetailsTaskTimeLog(numDomainID
						,numUserCntID
						,numTaskId
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry)
					VALUES(v_numDomainID
						,v_numTaskAssignee
						,v_numTaskID
						,v_tintAction
						,'1900-01-01':: date + make_interval(mins => DATEDIFF('minute','1900-01-01'::TIMESTAMP,v_dtActionTime))
						,v_numProcessedQty
						,v_numReasonForPause
						,v_vcNotes
						,v_bitManualEntry);
            ELSE
               RAISE EXCEPTION 'TASK_DOES_NOT_EXISTS';
            end if;
         end if;
      end if;
   ELSE
      RAISE EXCEPTION 'TASK_IS_MARKED_AS_FINISHED';
   end if;
   v_numProjectId := coalesce((SELECT  numProjectId FROM StagePercentageDetailsTask where numTaskId = v_numTaskID LIMIT 1),0);
   IF(v_numProjectId > 0) then
      SELECT MIN(dtActionTime) INTO v_dtmActualStartDate FROM StagePercentageDetailsTaskTimeLog where numDomainID = v_numDomainID AND numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numProjectId = v_numProjectId);
      SELECT COUNT(*) INTO v_intTotalTaskCount FROM StagePercentageDetailsTask WHERE numProjectId = v_numProjectId AND numDomainID = v_numDomainID;
      SELECT COUNT(distinct numTaskId) INTO v_intTotalTaskClosed FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId = v_numProjectId AND numDomainID = v_numDomainID) AND tintAction = 4;
      IF(v_intTotalTaskCount = v_intTotalTaskClosed) then
		
         SELECT MAX(dtActionTime) INTO v_dtmActualFinishDate FROM StagePercentageDetailsTaskTimeLog where numDomainID = v_numDomainID AND numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numProjectId = v_numProjectId);
         UPDATE ProjectsMaster SET dtCompletionDate = v_dtmActualFinishDate WHERE numProId = v_numProjectId;
      end if;
      IF(v_intTotalTaskCount > 0 AND v_intTotalTaskClosed > 0) then
		
         v_intTotalProgress := ROUND(CAST((CAST((v_intTotalTaskClosed::bigint*100) AS decimal)/v_intTotalTaskCount::bigint) AS NUMERIC),0);
      end if;
      IF((SELECT COUNT(*) FROM ProjectProgress WHERE coalesce(numProId,0) = v_numProjectId AND numDomainID = v_numDomainID) > 0) then
		
         UPDATE ProjectProgress SET intTotalProgress = v_intTotalProgress WHERE coalesce(numProId,0) = v_numProjectId AND numDomainID = v_numDomainID;
      ELSE
         INSERT INTO ProjectProgress(numProId,
				numOppId,
				numDomainID,
				intTotalProgress,
				numWorkOrderId)VALUES(v_numProjectId,
				NULL,
				v_numDomainID,
				v_intTotalProgress,
				NULL);
      end if;
		

		-- REMOVE EXISTING CONTRACT LOG ENTRY
      IF EXISTS(SELECT
      CL.numContractId
      FROM
      ContractsLog CL
      WHERE
      CL.numDivisionID = v_numDomainID
      AND CL.numReferenceId = v_numTaskID
      AND vcDetails = '1') then
		
         select   numContractsLogId, CL.numContractId, numUsedTime INTO v_numContractsLogId,v_numContractID,v_numUsedTime FROM
         ContractsLog CL
         INNER JOIN
         Contracts C
         ON
         CL.numContractId = C.numContractId WHERE
         C.numDomainId = v_numDomainID
         AND CL.numReferenceId = v_numTaskID
         AND vcDetails = '1';
         UPDATE Contracts SET timeUsed = coalesce(timeUsed,0) -coalesce(v_numUsedTime,0),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContractID;
         DELETE FROM ContractsLog WHERE numContractsLogId = v_numContractsLogId;
      end if;
      IF EXISTS(SELECT
      C.numContractId
      FROM
      ProjectsMaster PM
      INNER JOIN
      Contracts C
      ON
      PM.numDivisionId = C.numDivisonId
      WHERE
      PM.numdomainId = v_numDomainID
      AND PM.numProId = v_numProjectId
      AND C.numDomainId = v_numDomainID
      AND C.numDivisonId = PM.numDivisionId
      AND C.intType = 1) then
		
         IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND tintAction = 4) then
			
            select   C.numContractId INTO v_numContractID FROM
            ProjectsMaster PM
            INNER JOIN
            Contracts C
            ON
            PM.numDivisionId = C.numDivisonId WHERE
            PM.numdomainId = v_numDomainID
            AND PM.numProId = v_numProjectId
            AND C.numDomainId = v_numDomainID
            AND C.numDivisonId = PM.numDivisionId
            AND C.intType = 1   ORDER BY
            C.numContractId DESC LIMIT 1;

				-- GET UPDATED TIME
            v_numUsedTime := CAST(GetTimeSpendOnTaskByProject(v_numDomainID,v_numTaskID,2::SMALLINT) AS INTEGER);

				-- CREATE NEW ENTRY FOR
            UPDATE Contracts SET timeUsed = timeUsed+(coalesce(v_numUsedTime,0)*60),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContractID;
            SELECT coalesce((coalesce(timeLeft,0) -coalesce(timeUsed,0)),0) INTO v_balanceContractTime FROM Contracts WHERE numContractId = v_numContractID;
            INSERT INTO ContractsLog(intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType)
            SELECT
            1,Contracts.numDivisonId,v_numTaskID,TIMEZONE('UTC',now()),0,1,(coalesce(v_numUsedTime,0)*60),v_balanceContractTime,numContractId,1
            FROM
            Contracts
            WHERE
            numContractId = v_numContractID;
         end if;
      end if;
   end if;

   open SWV_RefCur for SELECT
   cast(coalesce(numWorkOrderId,0) as VARCHAR(255)) AS numWorkOrderId
   FROM
   StagePercentageDetailsTask
   WHERE
   numDomainID = v_numDomainID
   AND numTaskId = v_numTaskID;
END; $$;












