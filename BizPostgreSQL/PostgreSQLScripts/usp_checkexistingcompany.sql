-- Stored procedure definition script usp_CheckExistingCompany for PostgreSQL
CREATE OR REPLACE FUNCTION usp_CheckExistingCompany(v_numCompanyID NUMERIC,
 v_vcCompanyName VARCHAR(200),
 v_numDivisionID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(CI.vcCompanyName as VARCHAR(255))
   FROM
   CompanyInfo CI
   INNER JOIN
   DivisionMaster DM
   ON
   CI.numCompanyId = DM.numCompanyID
   Where
   DM.numDivisionID <> v_numDivisionID
   AND LOWER(CI.vcCompanyName) = LOWER(v_vcCompanyName)
   AND CI.numCompanyId <> v_numCompanyID
   AND DM.bitPublicFlag = false
   AND DM.tintCRMType in(1,2);
END; $$;












