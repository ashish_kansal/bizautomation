-- Stored procedure definition script USP_GetAllReferringPage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAllReferringPage(v_ReferringPage VARCHAR(100),                    
v_searchTerm VARCHAR(100),                    
v_From TIMESTAMP,                    
v_To TIMESTAMP,                    
v_CurrentPage INTEGER,                            
v_PageSize INTEGER,                            
INOUT v_TotRecs INTEGER ,                            
v_columnName VARCHAR(50),                            
v_columnSortOrder VARCHAR(10),            
v_strCrawler VARCHAR(5) DEFAULT '',          
v_MinimumPages VARCHAR(5) DEFAULT NULL,          
v_numDomainID  NUMERIC(9,0) DEFAULT NULL,  
v_ClientOffsetTime INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                            
                          
--Create a Temporary table to hold data                            
                           
                   
                    
                         
   AS $$
   DECLARE
   v_strSql  VARCHAR(5000);
BEGIN
   v_strSql := 'select DTL.dtTime        
from TrackingVisitorsHDR THDR         
Join TrackingVisitorsDTL DTL on THDR.numTrackingID = DTL.numTracVisitorsHDRID        
        
where THDR.numDomainID = cast(NULLIF(''' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ''','''') as NUMERIC(18,0)) and 
THDR.dtCreated between ''' || CAST(coalesce(v_From,'') || CAST(v_ClientOffsetTime || 'minute' as interval) AS VARCHAR(50)) || ''':: BYTEA and 
''' || CAST(coalesce(v_To,'') || CAST(v_ClientOffsetTime || 'minute' as interval) AS VARCHAR(50)) || ''':: BYTEA and THDR.numVistedPages >= cast(NULLIF(''' || coalesce(v_MinimumPages,'') || ''','''') as NUMERIC(18,0))';        
          
                
                
   if (v_searchTerm <> '' and v_searchTerm is not null) then

      v_strSql := coalesce(v_strSql,'') || ' and THDR.vcSearchTerm ilike ''%' || coalesce(v_searchTerm,'') || '%''';
   end if;             
   if v_strCrawler <> '' then  
      v_strSql := coalesce(v_strSql,'') || ' and THDR.vcCrawler = ''' || coalesce(v_strCrawler,'') || '''';
   end if;               
   if (v_ReferringPage <> '' and v_ReferringPage is not null) then

      v_strSql := coalesce(v_strSql,'') || ' and THDR.vcOrginalRef ilike ''%' || coalesce(v_ReferringPage,'') || '%''';
   end if;                             
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                          
   RAISE NOTICE '%',v_strSql;                      
             
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


