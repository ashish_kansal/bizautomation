CREATE OR REPLACE FUNCTION USP_GetCaseReportForAgents(v_dtFromDate TIMESTAMP,                            
        v_dtToDate TIMESTAMP,                            
        v_numUserCntID NUMERIC DEFAULT 0,        
        v_intStatus INTEGER DEFAULT NULL,     
        v_tintRights SMALLINT DEFAULT NULL,    
        v_numDomainID BIGINT DEFAULT NULL,    
        v_tintType BIGINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights <> 3 then

      If v_intStatus = 0 then

         open SWV_RefCur for
         SELECT
         CMP.vcCompanyName || ', ' || Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyId,fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,
   Cs.vcCaseNumber,
  Cs.bintCreatedDate as bintCreatedDate,
   Cs.textSubject,
   Cs.intTargetResolveDate,
   coalesce(lst.vcData,'-') as Status
         FROM
         Cases  Cs
         JOIN DivisionMaster Div
         ON Cs.numDivisionID = Div.numDivisionID
         JOIN CompanyInfo CMP
         ON Div.numCompanyID = CMP.numCompanyId
         left join Listdetails lst
         on lst.numListItemID = Cs.numStatus
         where Cs.numAssignedTo = v_numUserCntID and Cs.bintCreatedDate between   v_dtFromDate and v_dtToDate;
      end if;
      If v_intStatus <> 0 then

         open SWV_RefCur for
         SELECT
         CMP.vcCompanyName || ', ' || Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyId,fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,
   Cs.vcCaseNumber,
  Cs.bintCreatedDate as bintCreatedDate,
   Cs.textSubject,
   Cs.intTargetResolveDate,
   coalesce(lst.vcData,'-') as Status
         FROM
         Cases  Cs
         JOIN DivisionMaster Div
         ON Cs.numDivisionID = Div.numDivisionID
         JOIN CompanyInfo CMP
         ON Div.numCompanyID = CMP.numCompanyId
         left join Listdetails lst
         on lst.numListItemID = Cs.numStatus
         where Cs.numAssignedTo = v_numUserCntID And Cs.numStatus = v_intStatus
         And Cs.bintCreatedDate between   v_dtFromDate and v_dtToDate;
      end if;
   end if;      
    
    
   If v_tintRights = 3 then

      If v_intStatus = 0 then

         open SWV_RefCur for
         SELECT
         CMP.vcCompanyName || ', ' || Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyId,fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,
   Cs.vcCaseNumber,
  Cs.bintCreatedDate as bintCreatedDate,
   Cs.textSubject,
   Cs.intTargetResolveDate,
   coalesce(lst.vcData,'-') as Status
         FROM
         Cases  Cs
         JOIN DivisionMaster Div
         ON Cs.numDivisionID = Div.numDivisionID
         JOIN CompanyInfo CMP
         ON Div.numCompanyID = CMP.numCompanyId
         Join AdditionalContactsInformation ADC on
         Cs.numAssignedTo = ADC.numContactId
         left join Listdetails lst
         on lst.numListItemID = Cs.numStatus
         where Cs.numAssignedTo = v_numUserCntID and Cs.bintCreatedDate between   v_dtFromDate and v_dtToDate
         and Div.numTerID   in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType);
      end if;
      If v_intStatus <> 0 then

         open SWV_RefCur for
         SELECT
         CMP.vcCompanyName || ', ' || Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyId,fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,
   Cs.vcCaseNumber,
  Cs.bintCreatedDate as bintCreatedDate,
   Cs.textSubject,
   Cs.intTargetResolveDate,
   coalesce(lst.vcData,'-') as Status
         FROM
         Cases  Cs
         JOIN DivisionMaster Div
         ON Cs.numDivisionID = Div.numDivisionID
         JOIN CompanyInfo CMP
         ON Div.numCompanyID = CMP.numCompanyId
         Join AdditionalContactsInformation ADC on
         Cs.numAssignedTo = ADC.numContactId
         left join Listdetails lst
         on lst.numListItemID = Cs.numStatus
         where  Cs.numAssignedTo = v_numUserCntID and Cs.numStatus = v_intStatus
         And Cs.bintCreatedDate between   v_dtFromDate and v_dtToDate
         and Div.numTerID   in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType);
      end if;
   end if;
   RETURN;
END; $$;


