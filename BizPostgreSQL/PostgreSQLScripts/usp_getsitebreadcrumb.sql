-- Stored procedure definition script USP_GetSiteBreadCrumb for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSiteBreadCrumb(v_numSiteID NUMERIC(18,0), 
	v_numDomainID NUMERIC(18,0),
	v_numBreadCrumbID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	
   open SWV_RefCur for SELECT cast(SBC.numBreadCrumbID as VARCHAR(255))
		  ,cast(SBC.numPageID as VARCHAR(255))
		  ,cast(SP.vcPageName as VARCHAR(255))
		  ,cast(SP.vcPageURL as VARCHAR(255))
		  ,cast(SBC.numSiteID as VARCHAR(255))
		  ,cast(SBC.tintLevel as VARCHAR(255))
		  ,cast(SBC.vcDisplayName as VARCHAR(255))
   FROM SiteBreadCrumb SBC
   INNER JOIN  SitePages SP
   ON SBC.numPageID = SP.numPageID
   WHERE SBC.numSiteID = v_numSiteID
   AND SBC.numDomainID = v_numDomainID
   AND(numBreadCrumbID = v_numBreadCrumbID
   OR v_numBreadCrumbID = 0)
   ORDER BY tintLevel;
END; $$;













