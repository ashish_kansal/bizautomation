-- Stored procedure definition script USP_DycFieldMasterSynonym_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DycFieldMasterSynonym_Save(v_numDomainID NUMERIC(18,0)
	,v_numFieldID NUMERIC(18,0)
	,v_vcSynonym VARCHAR(200)
	,v_bitCustomField BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO DycFieldMasterSynonym(numDomainID
		,numFieldID
		,bitCustomField
		,vcSynonym
		,bitDefault)
	VALUES(v_numDomainID
		,v_numFieldID
		,v_bitCustomField
		,v_vcSynonym
		,false);
RETURN;
END; $$;


