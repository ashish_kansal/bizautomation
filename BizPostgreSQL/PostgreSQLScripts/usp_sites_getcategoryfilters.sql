DROP FUNCTION IF EXISTS USP_Sites_GetCategoryFilters;

CREATE OR REPLACE FUNCTION USP_Sites_GetCategoryFilters(v_numDomainID NUMERIC(18,0)
	,v_numCategoryID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		CFW_Fld_Master.Fld_id AS "Fld_id"
		,CFW_Fld_Master.FLd_label AS "Fld_label"
		,CFW_Fld_Master.numlistid AS "numlistid"
   FROM
   Category
   INNER JOIN
   CategoryMatrixGroup
   ON
   Category.numCategoryID = CategoryMatrixGroup.numCategoryID
   INNER JOIN
   ItemGroups
   ON
   CategoryMatrixGroup.numItemGroup = ItemGroups.numItemGroupID
   INNER JOIN
   ItemGroupsDTL
   ON
   ItemGroups.numItemGroupID = ItemGroupsDTL.numItemGroupID
   AND ItemGroupsDTL.tintType = 2
   INNER JOIN
   CFW_Fld_Master
   ON
   ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
   WHERE
   Category.numDomainID = v_numDomainID
   AND Category.numCategoryID = v_numCategoryID
   GROUP BY
   CFW_Fld_Master.Fld_id,CFW_Fld_Master.FLd_label,CFW_Fld_Master.numlistid;
END; $$;












