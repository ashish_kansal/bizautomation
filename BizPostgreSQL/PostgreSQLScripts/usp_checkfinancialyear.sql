-- Stored procedure definition script USP_CheckFinancialyear for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckFinancialyear(v_numDomainID NUMERIC(9,0),
	v_dtDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COUNT(*) FROM FinancialYear WHERE numDomainId = v_numDomainID AND bitCloseStatus = false AND bitCurrentYear = true AND (CAST(dtPeriodFrom AS DATE) <= CAST(v_dtDate AS DATE) AND CAST(dtPeriodTo AS DATE) >= CAST(v_dtDate AS DATE));
END; $$;	












