-- Stored procedure definition script USP_GetProjectSummary for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProjectSummary(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		P.vcProjectID AS "vcProjectID"
		,C.vcCompanyName AS "vcCompanyName"
		,DT.vcDomainName AS "vcByCompanyName"
		,ST.vcTaskName AS "vcTaskName"
		,
CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP)
   END AS "dtStartDate"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN FormatedDateFromDate((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID)
   ELSE ''
   END AS "dtFinishDate",
ltrim(SUBSTR(TO_CHAR(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP)
   END,'Mon dd yyyy hh:miAM'),length(TO_CHAR(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP)
   END,'Mon dd yyyy hh:miAM')) -7+1)) AS "StartTiime"
			,ltrim(SUBSTR(TO_CHAR(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP)
   END,'Mon dd yyyy hh:miAM'),length(TO_CHAR(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP)
   END,'Mon dd yyyy hh:miAM')) -7+1)) AS "FinishTime",
			(EXTRACT(DAY FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))*60*24+EXTRACT(HOUR FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))*60+EXTRACT(MINUTE FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))) As "TaskDurationComplete",
				ADC.vcFirstName || ' ' || ADC.vcLastname AS "AssignedTo",
				(((ST.numHours*60)+ST.numMinutes) -((EXTRACT(DAY FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))*60*24+EXTRACT(HOUR FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))*60+EXTRACT(MINUTE FROM(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END) -(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)
   THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   ELSE CAST(null as TIMESTAMP) END))))) AS "BalanceTime"
				,GetTotalProgress(v_numDomainID,v_numProId,2::SMALLINT,3::SMALLINT,'',ST.numStageDetailsId) AS "numTotalProgress",
				P.txtComments AS "txtComments"
   FROM StagePercentageDetailsTask AS ST
   LEFT JOIN
   ProjectsMaster AS P
   ON  ST.numProjectId = P.numProId
   LEFT JOIN DivisionMaster AS D
   ON P.numDivisionId = D.numDivisionID
   LEFT JOIN CompanyInfo AS C
   ON D.numCompanyID = C.numCompanyId
   LEFT JOIN Domain DT ON DT.numDomainId = P.numdomainId
   LEFT JOIN AdditionalContactsInformation AS ADC ON ST.numAssignTo = ADC.numContactId
   WHERE
   ST.numProjectId = v_numProId AND ST.numDomainID = v_numDomainID AND(SELECT coalesce((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = ST.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),0)) = 4;
END; $$; 












