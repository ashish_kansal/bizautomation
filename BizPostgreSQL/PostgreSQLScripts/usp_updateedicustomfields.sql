-- Stored procedure definition script USP_UpdateEDICustomFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Author :  Priya Sharma
		Date : 19 June 2018
 ******/
CREATE OR REPLACE FUNCTION USP_UpdateEDICustomFields(v_Grp_id NUMERIC(5,0)
	,v_RecID NUMERIC(18,0)
	,v_Fld_id NUMERIC(9,0)
	,v_Fld_Value TEXT
	,v_FldDTLID NUMERIC(9,0)
	,v_numModifiedBy NUMERIC(9,0)
	,v_bintModifiedBy TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql  
  
	-- @numDomainID NUMERIC(18,0)
   AS $$
BEGIN


  /****** Insert Update Opp/Order  ******/
   IF v_FldDTLID > 0 AND v_Grp_id = 19 then
	
      UPDATE CFW_Fld_Values_Opp_EDI
      SET
      Fld_ID = v_Fld_id,Fld_Value = v_Fld_Value,RecId = v_RecID,numModifiedBy = v_numModifiedBy,
      bintModifiedDate = v_bintModifiedBy
      WHERE FldDTLID = v_FldDTLID AND RecId = v_RecID;
   ELSEIF v_FldDTLID <= 0 AND v_Grp_id = 19
   then
	 
      INSERT INTO CFW_Fld_Values_Opp_EDI(Fld_ID
			,Fld_Value
			,RecId
			,numModifiedBy
			,bintModifiedDate)
		VALUES(v_Fld_id
			,v_Fld_Value
			,v_RecID
			,v_numModifiedBy
			,v_bintModifiedBy);
   end if;	
	

	  /****** Insert Update Item  ******/

   IF v_FldDTLID > 0 AND v_Grp_id = 20 then
	
      UPDATE CFW_FLD_Values_Item_EDI
      SET
      Fld_ID = v_Fld_id,Fld_Value = v_Fld_Value,RecId = v_RecID
      WHERE FldDTLID = v_FldDTLID AND RecId = v_RecID;
   ELSEIF v_FldDTLID <= 0 AND v_Grp_id = 20
   then
	
      INSERT INTO CFW_FLD_Values_Item_EDI(Fld_ID
			,Fld_Value
			,RecId)
		VALUES(v_Fld_id
			,v_Fld_Value
			,v_RecID);
   end if;

  /****** Insert Update Opp/Order Item  ******/
   IF v_FldDTLID > 0 AND v_Grp_id = 21 then
	
      UPDATE CFW_Fld_Values_OppItems_EDI
      SET
      Fld_ID = v_Fld_id,Fld_Value = v_Fld_Value,RecId = v_RecID
      WHERE FldDTLID = v_FldDTLID AND RecId = v_RecID;
   ELSEIF v_FldDTLID <= 0 AND v_Grp_id = 21
   then
	
      INSERT INTO CFW_Fld_Values_OppItems_EDI(Fld_ID
			,Fld_Value
			,RecId)
		VALUES(v_Fld_id
			,v_Fld_Value
			,v_RecID);
   end if;
   RETURN;

  /****** Insert Update Opp/Order Item  ******/


END; $$;



