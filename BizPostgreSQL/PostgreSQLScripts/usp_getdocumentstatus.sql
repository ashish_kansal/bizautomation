-- Stored procedure definition script usp_GetDocumentStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDocumentStatus(v_numDomain NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPLIST CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPLIST
   (
      vcData VARCHAR(250),
      numListItemID NUMERIC(18,0),
      intSortId NUMERIC(1,0)
   );

   insert into tt_TEMPLIST
   select vcData,numListItemID,-2 from Listdetails where numListID = 29 and  (constFlag = true or  numDomainid = v_numDomain)
   union
   select vcData,numListItemID,-2 from Listdetails where numListID = 28 and  (constFlag = true or  numDomainid = v_numDomain)
   union
   select vcData,numListItemID,-3 from Listdetails where numListID = 27 and  (constFlag = true or numDomainid = v_numDomain); 

   open SWV_RefCur for
   select * from tt_TEMPLIST order by intSortId,numListItemID,vcData;

   open SWV_RefCur2 for
   select  vcData,numListItemID from Listdetails where numListID = 11 and numDomainid = v_numDomain
   order by vcData;

   RETURN;
END; $$;



