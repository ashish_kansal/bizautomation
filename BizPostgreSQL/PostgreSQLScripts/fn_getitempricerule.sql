-- Function definition script fn_GetItemPriceRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemPriceRule(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintRuleFor SMALLINT DEFAULT 0,
    v_numItemCode NUMERIC(9,0) DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0)
RETURNS table
(
   numPricRuleID NUMERIC(9,0)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC(9,0);                  
   v_numProfile  NUMERIC(9,0);                  

/*Profile and relationship id */
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETITEMPRICERULE_PRICEBOOKRULES CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETITEMPRICERULE_PRICEBOOKRULES
   (
      numPricRuleID NUMERIC(9,0)
   );
   select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile from DivisionMaster D
   join CompanyInfo C on C.numCompanyId = D.numCompanyID where numDivisionID = v_numDivisionID;       

   INSERT INTO tt_FN_GETITEMPRICERULE_PRICEBOOKRULES
   SELECT P.numPricRuleID
   FROM    Item I
   LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
   LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
   LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
   LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2
   AND PP.Step3Value = P.tintStep3
   WHERE   numItemCode = v_numItemCode AND tintRuleFor = v_tintRuleFor AND I.numDomainID = v_numDomainID
   AND
(
((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
   OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
   OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

   OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
   OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
   OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

   OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
   OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
   OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
);

RETURN QUERY (SELECT * FROM tt_FN_GETITEMPRICERULE_PRICEBOOKRULES);
END; $$;

