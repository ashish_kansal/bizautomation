-- Stored procedure definition script USP_DeleteTaxItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteTaxItem(v_numTaxItemID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numSalesBizDocFormFieldID  NUMERIC(9,0);
   v_numPurBizDocFormFieldID  NUMERIC(9,0);
   v_numBizDocSummaryFormFieldID  NUMERIC(9,0);
   v_numChartActID  NUMERIC(9,0);
BEGIN
   select   numSalesBizDocFormFieldID, numPurBizDocFormFieldID, numBizDocSummaryFormFieldID, numChartofAcntID INTO v_numSalesBizDocFormFieldID,v_numPurBizDocFormFieldID,v_numBizDocSummaryFormFieldID,
   v_numChartActID from TaxItems where numTaxItemID = v_numTaxItemID;

   delete from DycFormConfigurationDetails where numFieldID = v_numSalesBizDocFormFieldID and numDomainID = v_numDomainID;

   delete from DycFormField_Mapping where numFieldID = v_numSalesBizDocFormFieldID and numDomainID = v_numDomainID;

   delete from DycFieldMaster where numFieldID = v_numSalesBizDocFormFieldID and numDomainID = v_numDomainID;

--Commented By chintan Now deleting Account is not required
--exec USP_DeleteChartCategory @numChartActID,@numDomainId

   delete from TaxDetails where numTaxItemID = v_numTaxItemID;
   delete from TaxItems Where numTaxItemID = v_numTaxItemID;

/****** Object:  StoredProcedure [dbo].[USP_DocumentWorkFlow]    Script Date: 07/26/2008 16:15:46 ******/
   RETURN;
END; $$;


