-- FUNCTION: public.usp_getestimateshippingecommerce(numeric, refcursor)

-- DROP FUNCTION public.usp_getestimateshippingecommerce(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getestimateshippingecommerce(
	v_numdomainid numeric,
	INOUT swv_refcur refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF v_numDomainID IN(187,214) then
	
      open SWV_RefCur for
      SELECT * FROM(SELECT
         intNsoftEnum AS numServiceTypeID
				,0 AS numRuleID
				,numShippingCompanyID
				,vcServiceName
				,monRate
				,intNsoftEnum
				,intFrom
				,intTo
				,fltMarkup
				,bitMarkUpType
				,bitEnabled
				,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
         FROM
         ShippingServiceTypes  AS S
         LEFT JOIN
         Listdetails AS L
         ON
         S.numShippingCompanyID = L.numListItemID
         LEFT JOIN ShippingRules AS SR ON S.numRuleID = SR.numRuleID
         WHERE
         S.numDomainID = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         UNION
         SELECT
         -1 AS numServiceTypeID
				,0 AS numRuleID
				,92 AS numShippingCompanyID
				,'Will call' AS vcServiceName
				,0 AS monRate
				,0 AS intNsoftEnum
				,0 AS intFrom
				,0 AS intTo
				,0 AS fltMarkup
				,false AS bitMarkUpType
				,true AS bitEnabled
				,'' AS vcItemClassification
		,false AS bitItemClassification
		,'0' AS numWareHouseID
		,0 AS numSiteID
		,0 AS numItemClassificationRuleID) TEMP
      ORDER BY
      numShippingCompanyID,intNsoftEnum;
   ELSE
      open SWV_RefCur for
      SELECT
      numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
			,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
      FROM
      ShippingServiceTypes  AS S
      LEFT JOIN
      Listdetails AS L
      ON
      S.numShippingCompanyID = L.numListItemID
      LEFT JOIN ShippingRules AS SR ON S.numRuleID = SR.numRuleID
      WHERE
      S.numDomainID = v_numDomainID
      AND coalesce(bitEnabled,false) = true;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getestimateshippingecommerce(numeric, refcursor)
    OWNER TO postgres;
