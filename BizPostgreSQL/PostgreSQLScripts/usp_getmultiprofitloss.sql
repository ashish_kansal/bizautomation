-- Stored procedure definition script USP_GetMultiProfitLoss for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMultiProfitLoss(v_numParentDomainID NUMERIC(9,0),                        
v_dtFromDate TIMESTAMP,                      
v_dtToDate TIMESTAMP ,
v_numSubscriberID INTEGER,
INOUT SWV_RefCur refcursor default null,
v_RollUp BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFinYear  INTEGER;
   v_dtFinYearFrom  TIMESTAMP;
BEGIN
   drop table IF EXISTS tt_TLGROUP CASCADE;
   CREATE TEMPORARY TABLE tt_TLGROUP 
   (
      numDomainId NUMERIC(9,0),
      vcDomainCode VARCHAR(50),
      vcDomainName VARCHAR(150), 
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );


   INSERT INTO  tt_TLGROUP
   SELECT AD.numDomainID,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeID,CAST(AD.vcAccountType AS VARCHAR(250)),numParentID,CAST('' AS VARCHAR(250)),AD.vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO,Chart_Of_Accounts COA WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND FinancialYear.numDomainId = AD.numDomainID)
      AND CAO.numDomainID = AD.numDomainID AND
      CAO.numAccountId = COA.numAccountId  AND
      COA.vcAccountCode ilike AD.vcAccountCode || '%'),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = AD.numDomainID AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),
   0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM AccountTypeDetail AD,
	(select * from Domain dn where dn.numSubscriberID = v_numSubscriberID) dn,
	(select * from Domain DNN where DNN.numSubscriberID = v_numSubscriberID) DNN
   WHERE AD.numDomainID = DNN.numDomainId AND
   DNN.numDomainId = dn.numDomainId AND
   dn.numDomainId = v_numParentDomainID AND
	(AD.vcAccountCode ilike '0103%' OR AD.vcAccountCode ilike '0104%')
   UNION
   SELECT AD.numDomainID,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeID,CAST(AD.vcAccountType AS VARCHAR(250)),numParentID,'',AD.vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO,Chart_Of_Accounts COA WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND FinancialYear.numDomainId = AD.numDomainID)
      AND CAO.numDomainID = AD.numDomainID AND
      CAO.numAccountId = COA.numAccountId  AND
      COA.vcAccountCode ilike AD.vcAccountCode || '%'),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = AD.numDomainID AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),
   0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM AccountTypeDetail AD,
	(select * from Domain dn where dn.numSubscriberID = v_numSubscriberID) dn,
	(select * from Domain DNN where DNN.numSubscriberID = v_numSubscriberID) DNN
   WHERE AD.numDomainID = DNN.numDomainId AND
   DNN.vcDomainCode ilike dn.vcDomainCode || '%'  AND
   dn.numParentDomainID = v_numParentDomainID   AND
	(AD.vcAccountCode ilike '0103%' OR AD.vcAccountCode ilike '0104%');

   if v_RollUp = false then
	
      open SWV_RefCur for
      select vcDomainName as Company, sum(coalesce(Opening,0))+SUM(coalesce(Debit,0)) -SUM(coalesce(Credit,0)) as Amount from tt_TLGROUP
      WHERE vcAccountCode in('0103','0104')
      group by vcDomainName;
   ELSEIF v_RollUp = true
   then
	
      open SWV_RefCur for
      select 'Profit & Loss Roll-Up' as Company, sum(coalesce(Opening,0))+SUM(coalesce(Debit,0)) -SUM(coalesce(Credit,0)) as Amount from tt_TLGROUP
      WHERE vcAccountCode in('0103','0104');
   end if;
   RETURN;
END; $$;
-- Created by Sojan        
-- [USP_GetChartAcntDetails] @numParentDomainID=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            


