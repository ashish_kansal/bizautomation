-- Stored procedure definition script USP_Item_GetVendors for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetVendors(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		Vendor.numVendorID
		,coalesce(CompanyInfo.vcCompanyName,'') AS vcCompanyName
		,coalesce(Vendor.monCost,0) as monVendorCost
		,coalesce(Vendor.intMinQty),0) as monVendorCost
		,CASE WHEN NULLIF(LastOrder.vcPOppName,'') IS NULL THEN '-' ELSE CONCAT('<a href=''#'' onclick=''openVendorPriceHistory(',Vendor.numVendorID,');''>',
      monUnitCOst,'</a> (',numQty,') ',dtOrderedDate,
      ' ',vcpOppName) END AS vcLastOrder
   FROM
   Vendor
   INNER JOIN
   Item
   ON
   Vendor.numItemCode = Item.numItemCode
   INNER JOIN
   DivisionMaster
   ON
   Vendor.numVendorID = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN LATERAL(SELECT 
      OM.vcpOppName
			,fn_UOMConversion(OI.numUOMId,v_numItemCode,v_numItemCode,Item.numBaseUnit) AS numQty
			,FormatedDateFromDate(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS dtOrderedDate
			,CAST(coalesce(OI.monPrice,0) AS DECIMAL(20,5)) AS monUnitCOst
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numDivisionId = Vendor.numVendorID
      AND OI.numItemCode = v_numItemCode
      AND tintopptype = 2
      AND tintoppstatus = 1
      AND coalesce(bitStockTransfer,false) = false
      ORDER BY
      OM.numOppId DESC LIMIT 1) AS LastOrder on TRUE
   WHERE
   Vendor.numDomainID = v_numDomainID
   AND Vendor.numItemCode = v_numItemCode;
END; $$;












