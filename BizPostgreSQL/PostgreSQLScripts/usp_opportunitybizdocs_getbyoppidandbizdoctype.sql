-- Stored procedure definition script USP_OpportunityBizDocs_GetByOppIDAndBizDocType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetByOppIDAndBizDocType(v_numDomainID NUMERIC(18,0),
 v_numOppId NUMERIC(18,0),
 v_numBizDocId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingReportItems WHERE numShippingReportId IN(SELECT
   numShippingReportID
   FROM
   ShippingReport
   WHERE
   numOppBizDocId IN(SELECT DISTINCT
      numOppBizDocsId
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityMaster
      ON
      OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
      LEFT JOIN
      ShippingReport
      ON
      OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
      LEFT JOIN
      ShippingBox
      ON
      ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND OpportunityBizDocs.numoppid = v_numOppId
      AND numBizDocId = v_numBizDocId
      AND (ShippingBox.numBoxID IS NULL OR coalesce(ShippingBox.vcShippingLabelImage,'') = '')));

   DELETE FROM ShippingBox WHERE numShippingReportID IN(SELECT
   numShippingReportID
   FROM
   ShippingReport
   WHERE
   numOppBizDocId IN(SELECT DISTINCT
      numOppBizDocsId
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityMaster
      ON
      OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
      LEFT JOIN
      ShippingReport
      ON
      OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
      LEFT JOIN
      ShippingBox
      ON
      ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND OpportunityBizDocs.numoppid = v_numOppId
      AND numBizDocId = v_numBizDocId
      AND (ShippingBox.numBoxID IS NULL OR coalesce(ShippingBox.vcShippingLabelImage,'') = '')));

   DELETE FROM ShippingReport WHERE numOppBizDocId IN(SELECT DISTINCT
   numOppBizDocsId
   FROM
   OpportunityBizDocs
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   LEFT JOIN
   ShippingReport
   ON
   OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
   LEFT JOIN
   ShippingBox
   ON
   ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityBizDocs.numoppid = v_numOppId
   AND numBizDocId = v_numBizDocId
   AND (ShippingBox.numBoxID IS NULL OR coalesce(ShippingBox.vcShippingLabelImage,'') = ''));



   open SWV_RefCur for SELECT DISTINCT
   numOppBizDocsId
   FROM
   OpportunityBizDocs
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   LEFT JOIN
   ShippingReport
   ON
   OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
   LEFT JOIN
   ShippingBox
   ON
   ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityBizDocs.numoppid = v_numOppId
   AND numBizDocId = v_numBizDocId
   AND (ShippingBox.numBoxID IS NULL OR coalesce(ShippingBox.vcShippingLabelImage,'') = '');
   RETURN;
END; $$;













