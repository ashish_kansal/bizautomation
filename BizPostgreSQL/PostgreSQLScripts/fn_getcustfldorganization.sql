CREATE OR REPLACE FUNCTION fn_GetCustFldOrganization(v_numFldId NUMERIC(18,0),v_pageId SMALLINT,v_numRecordId NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(300);  
   v_vcFldValue  TEXT;
   v_fld_type  VARCHAR(20);
BEGIN
   select fld_type INTO v_fld_type FROM CFW_Fld_Master WHERE Fld_id = v_numFldId;

   select Fld_Value INTO v_vcFldValue FROM CFW_FLD_Values WHERE Fld_ID = v_numFldId AND RecId = v_numRecordId;  

   IF v_fld_type = 'SelectBox' AND SWF_IsNumeric(v_vcFldValue) = true then
      SELECT vcData INTO v_vcValue FROM Listdetails WHERE numListItemID = CAST(v_vcFldValue AS NUMERIC);
   ELSEIF v_fld_type = 'CheckBoxList' then
      v_vcValue := COALESCE((SELECT STRING_AGG(vcData,'') FROM Listdetails WHERE numListItemID IN (SELECT coalesce(Id,0) FROM SplitIDs(v_vcFldValue,','))),'');
   ELSEIF v_fld_type = 'CheckBox' then
      v_vcValue :=(CASE WHEN cast(NULLIF(coalesce(v_vcFldValue,cast(0 as TEXT)),'') as INTEGER) = 1 THEN 'Yes' ELSE 'No' END);
   ELSE
      v_vcValue := v_vcFldValue;
   end if;

   IF v_vcValue IS NULL then
      v_vcValue := '';
   end if;  

   RETURN v_vcValue;
END; $$;

