-- Stored procedure definition script USP_Item_UpdateItemVendorCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_UpdateItemVendorCost(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_vcSelectedRecords TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numVendorID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      monUnitCost DECIMAL(20,5)
   );

   INSERT INTO tt_TEMP(numVendorID
		,numItemCode
		,monUnitCost)
   SELECT
   coalesce(VendorID,0)
		,coalesce(ItemCode,0)
		,coalesce(UnitCost,0)
   FROM
   XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcSelectedRecords AS XML)
				COLUMNS
					id FOR ORDINALITY,
					VendorID NUMERIC(18,0) PATH 'VendorID',
					ItemCode NUMERIC(18,0) PATH 'ItemCode',
					UnitCost DECIMAL(20,5) PATH 'UnitCost'
			) AS X ;

 
   UPDATE
   Vendor V
   SET
   monCost = T.monUnitCost
   FROM
   tt_TEMP T WHERE(V.numDomainID = v_numDomainID
   AND T.numVendorID = V.numVendorID
   AND T.numItemCode = V.numItemCode);
   RETURN;
END; $$;


