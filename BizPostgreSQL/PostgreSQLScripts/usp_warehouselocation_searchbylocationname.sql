-- Stored procedure definition script USP_WarehouseLocation_SearchByLocationName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WarehouseLocation_SearchByLocationName(v_numDomainID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_vcSearchText VARCHAR(100)
	,v_intOffset INTEGER
	,v_intPageSize INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numWLocationID as "numWLocationID"
		,vcLocation as "vcLocation"
		,numWLocationID  AS "id"
		,coalesce(vcLocation,'') as "text"
		,COUNT(*) OVER() AS "TotalRecords"
   FROM
   WarehouseLocation
   WHERE
   numDomainID = v_numDomainID
   AND numWarehouseID = v_numWarehouseID
   AND (vcLocation ilike '%' || coalesce(v_vcSearchText,'') || '%' OR LENGTH(coalesce(v_vcSearchText,'')) = 0)
   ORDER BY
   vcLocation;
END; $$;












