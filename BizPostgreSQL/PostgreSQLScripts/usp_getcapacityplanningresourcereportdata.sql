-- Stored procedure definition script USP_GetCapacityPlanningResourceReportData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCapacityPlanningResourceReportData(v_numDomainID NUMERIC(18,0)
	,v_vcTeams VARCHAR(200)
	,v_vcEmployees VARCHAR(200)
	,v_tintProcessType SMALLINT
	,v_numProcessID NUMERIC(18,0)
	,v_vcMilestone VARCHAR(300)
	,v_numStageDetailsId NUMERIC(18,0)
	,v_vcTaskIDs VARCHAR(200)
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_tintView SMALLINT --1:Day, 2:Week, 3:Month
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTempTaskID  NUMERIC(18,0);
   v_numTempParentTaskID  NUMERIC(18,0);
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numWorkOrderID  NUMERIC(18,0);
   v_numProjectID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;

   v_numWorkScheduleID  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_dtStartDate  TIMESTAMP;
BEGIN
   open SWV_RefCur for
   SELECT
   numListItemID AS "numListItemID"
		,coalesce(vcData,'-') AS "vcData"
   FROM
   Listdetails
   WHERE
   numDomainid = v_numDomainID
   AND numListID = 35
   AND (coalesce(v_vcTeams,'') = '' OR numListItemID IN(SELECT Id FROM SplitIDs(v_vcTeams,','))); 

   open SWV_RefCur2 for
   SELECT
   ACI.numContactId AS "numContactId"
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastname) AS "vcContactName"
		,coalesce(ACI.numTeam,0) AS "numTeam"
   FROM
   UserMaster UM
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   UM.numUserDetailId = ACI.numContactId
   WHERE
   UM.numDomainID = v_numDomainID
   AND ACI.numDomainID = v_numDomainID
   AND coalesce(UM.bitactivateflag,false) = true
   AND (coalesce(v_vcTeams,'') = '' OR numTeam IN(SELECT Id FROM SplitIDs(v_vcTeams,',')))
   AND (coalesce(v_vcEmployees,'') = '' OR numContactId IN(SELECT Id FROM SplitIDs(v_vcEmployees,',')));  

   DROP TABLE IF EXISTS tt_EMPLOYEETIME CASCADE;
   CREATE TEMPORARY TABLE tt_EMPLOYEETIME
   (
      numUserCntID NUMERIC(18,0),
      numRecordID NUMERIC(18,0),
      tintRecordType SMALLINT,
      vcSource VARCHAR(200),
      dtDate DATE,
      vcDate VARCHAR(50),
      numType SMALLINT,
      vcType VARCHAR(500),
      vcHours VARCHAR(20),
      numMinutes INTEGER,
      vcDetails TEXT
   );

	-- Time & Expense
   INSERT INTO tt_EMPLOYEETIME(numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails)
   SELECT
   timeandexpense.numUserCntID
		,timeandexpense.numCategoryHDRID
		,1
		,CAST('Time & Expense' AS VARCHAR(200)) AS vcSource
		,dtFromDate
		,FormatedDateFromDate(dtFromDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval),v_numDomainID) AS vcDate
		,timeandexpense.numtype
		,CAST((CASE
   WHEN timeandexpense.numtype = 4 THEN 'Absence (Un-Paid)'
   WHEN timeandexpense.numtype = 3 THEN 'Absence (Paid)'
   WHEN timeandexpense.numtype = 2 Then 'Non-Billable Time'
   WHEN timeandexpense.numtype = 1 Then 'Billable'
   ELSE ''
   END) AS VARCHAR(500)) AS vcType
		,CAST(SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS VARCHAR(20)) AS vcHours
		,(EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) AS numMinutes
		,coalesce(timeandexpense.txtDesc,'') AS vcDetails
   FROM
   timeandexpense
   LEFT JOIN
   OpportunityMaster
   ON
   timeandexpense.numOppId = OpportunityMaster.numOppId
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND coalesce(numCategory,0) = 1
   AND numType NOT IN(4,7) -- DO NOT INCLUDE UNPAID LEAVE
   AND dtFromDate BETWEEN v_dtFromDate AND v_dtToDate
   ORDER BY
   dtFromDate;

	-- Action Item
   INSERT INTO tt_EMPLOYEETIME(numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails)
   SELECT
   Communication.numAssign
		,Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,
   '&lngType=0">',GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),
      DM.numDivisionID,'">',coalesce(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)) AS vcSource
		,dtStartTime
		,CAST(FormatedDateFromDate(Communication.dtStartTime+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval),v_numDomainID) AS VARCHAR(50)) AS vcDate
		,0
		,CAST(GetListIemName(bitTask) AS VARCHAR(500)) AS vcType
		,CAST(SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM Communication.dtEndTime -Communication.dtStartTime)*60*24+EXTRACT(HOUR FROM Communication.dtEndTime -Communication.dtStartTime)*60+EXTRACT(MINUTE FROM Communication.dtEndTime -Communication.dtStartTime)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS VARCHAR(20)) AS vcHours
		,(EXTRACT(DAY FROM Communication.dtEndTime -Communication.dtStartTime)*60*24+EXTRACT(HOUR FROM Communication.dtEndTime -Communication.dtStartTime)*60+EXTRACT(MINUTE FROM Communication.dtEndTime -Communication.dtStartTime)) AS numMinutes
		,coalesce(Communication.textDetails,'') AS vcDetails
   FROM
   Communication
   LEFT JOIN
   DivisionMaster DM
   ON
   Communication.numDivisionID = DM.numDivisionID
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   Communication.numDomainID = v_numDomainID
   AND Communication.dtStartTime BETWEEN v_dtFromDate AND v_dtToDate
   AND coalesce(bitClosedFlag,false) = true
   ORDER BY
   dtStartTime;

	-- Calendar
   INSERT INTO tt_EMPLOYEETIME(numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails)
   SELECT
   Resource.numUserCntId
		,Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,
   '&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,CAST(FormatedDateFromDate(Activity.StartDateTimeUtc+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval),v_numDomainID) AS VARCHAR(50)) AS vcDate
		,0
		,CAST('Calendar' AS VARCHAR(500)) AS vcType
		,CAST(SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60*24+EXTRACT(HOUR FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60+EXTRACT(MINUTE FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS VARCHAR(20)) AS vcHours
		,(EXTRACT(DAY FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60*24+EXTRACT(HOUR FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60+EXTRACT(MINUTE FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)) AS numMinutes
		,coalesce(Activity.ActivityDescription,'') AS vcDetails
   FROM
   Resource
   INNER JOIN
   ActivityResource
   ON
   Resource.ResourceID = ActivityResource.ResourceID
   INNER JOIN
   Activity
   ON
   ActivityResource.ActivityID = Activity.ActivityID
   WHERE
   Activity.StartDateTimeUtc BETWEEN v_dtFromDate AND v_dtToDate
   ORDER BY
   Activity.StartDateTimeUtc;


   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numWorkOrderID NUMERIC(18,0),
      numProjectID NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );

   DROP TABLE IF EXISTS tt_TEMPTIMELOG CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTIMELOG
   (
      numWrokOrder NUMERIC(18,0),
      numProjectID NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      numReferenceTaskId NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numAssignTo NUMERIC(18,0),
      numProductiveMinutes NUMERIC(18,0),
      dtDate TIMESTAMP,
      numTotalMinutes NUMERIC(18,0)
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numReferenceTaskId NUMERIC(18,0),
      numWorkOrderID NUMERIC(18,0),
      numProjectID NUMERIC(18,0),
      numAssignTo NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      intTaskType INTEGER,
      dtStartDate TIMESTAMP,
      dtFinishDate TIMESTAMP,
      numQtyItemsReq DOUBLE PRECISION,
      numProcessedQty DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPTASKS(numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numProjectID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty)
   SELECT
   SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,ProjectsMaster.numProId
		,SPDT.numAssignTo
		,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(coalesce(numQtyItemsReq,0) -coalesce((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE
   WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId) > 0
   THEN(SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId)
   ELSE coalesce(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
   END)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyItemsReq ELSE 1 END)
		,coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)
   FROM
   StagePercentageDetailsTask SPDT
   LEFT JOIN
   WorkOrder
   ON
   SPDT.numWorkOrderId = WorkOrder.numWOId
   LEFT JOIN
   ProjectsMaster
   ON
   SPDT.numProjectId = ProjectsMaster.numProId
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4) = 0 -- Task in not finished
   AND (coalesce(numWorkOrderId,0) > 0 OR coalesce(numProId,0) > 0);
		--TODO: Uncomment if there is performance issue on production
		--AND ((ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate AND (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0) 
		--		OR (ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate)
		--		OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)

   INSERT INTO tt_TEMPTASKASSIGNEE(numWorkOrderID
		,numProjectID
		,numAssignedTo)
   SELECT
   numWorkOrderID
		,numProjectID
		,numAssignTo
   FROM
   tt_TEMPTASKS
   GROUP BY
   numWorkOrderID,numProjectID,numAssignTo;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

   WHILE v_i <= v_iCount LOOP
      select   numTaskID, numReferenceTaskId, numAssignTo, numTaskTimeInMinutes, intTaskType, dtStartDate, numWorkOrderID, numProjectID, numQtyItemsReq INTO v_numTempTaskID,v_numTempParentTaskID,v_numTaskAssignee,v_numTotalTaskInMinutes,
      v_intTaskType,v_dtStartDate,v_numWorkOrderID,v_numProjectID,
      v_numQtyItemsReq FROM
      tt_TEMPTASKS WHERE
      ID = v_i;

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
      select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), tmStartOfDay, CONCAT(',',vcWorkDays,','), CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval) INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_tmStartOfDay,v_vcWorkDays,
      v_dtStartDate FROM
      WorkSchedule WS
      INNER JOIN
      UserMaster
      ON
      WS.numUserCntID = UserMaster.numUserDetailId WHERE
      WS.numUserCntID = v_numTaskAssignee;
      IF v_intTaskType = 1 AND EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE coalesce(numWorkOrderID,0) = coalesce(v_numWorkOrderID,0) AND coalesce(numProjectID,0) = coalesce(v_numProjectID,0) AND numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE coalesce(numWorkOrderID,0) = coalesce(v_numWorkOrderID,0) AND coalesce(numProjectID,0) = coalesce(v_numProjectID,0) AND numAssignedTo = v_numTaskAssignee;
      ELSEIF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE coalesce(numWorkOrderID,0) = coalesce(v_numWorkOrderID,0) AND coalesce(numProjectID,0) = coalesce(v_numProjectID,0) AND numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
      then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE coalesce(numWorkOrderID,0) = coalesce(v_numWorkOrderID,0) AND coalesce(numProjectID,0) = coalesce(v_numProjectID,0) AND numAssignedTo = v_numTaskAssignee;
      end if;
      UPDATE tt_TEMPTASKS SET dtStartDate = v_dtStartDate WHERE ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtStartDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
					-- CHECK TIME LEFT FOR DAY BASED
               v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));
               IF v_numTimeLeftForDay > 0 then
					
                  INSERT INTO tt_TEMPTIMELOG(numWrokOrder
							,numProjectID
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes)
						VALUES(v_numWorkOrderID
							,v_numProjectID
							,v_numTempTaskID
							,v_numTempParentTaskID
							,v_numQtyItemsReq
							,v_numTaskAssignee
							,v_numProductiveTimeInMinutes
							,v_dtStartDate
							,(CASE WHEN v_numTimeLeftForDay > v_numTotalTaskInMinutes THEN v_numTotalTaskInMinutes ELSE v_numTimeLeftForDay END));
						
                  IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
						
                     v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                  ELSE
                     v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
                  end if;
                  v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
               ELSE
                  v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
               end if;
            ELSE
               v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
            end if;
         END LOOP;
      end if;
      UPDATE tt_TEMPTASKS SET dtFinishDate = v_dtStartDate WHERE ID = v_i;
      UPDATE tt_TEMPTASKASSIGNEE SET dtLastTaskCompletionTime = v_dtStartDate WHERE coalesce(numWorkOrderID,0) = coalesce(v_numWorkOrderID,0) AND coalesce(numProjectID,0) = coalesce(v_numProjectID,0) AND numAssignedTo = v_numTaskAssignee;
      v_i := v_i::bigint+1;
   END LOOP;

   IF v_tintView = 1 then
	
      INSERT INTO tt_EMPLOYEETIME(numUserCntID
			,numRecordID
			,tintRecordType
			,vcSource
			,dtDate
			,vcDate
			,numType
			,vcType
			,vcHours
			,numMinutes
			,vcDetails)
      SELECT
      TTL.numAssignTo
			,(CASE WHEN coalesce(TTL.numWrokOrder,0) > 0 THEN TTL.numWrokOrder ELSE TTL.numProjectID END)
			,(CASE WHEN coalesce(TTL.numWrokOrder,0) > 0 THEN 4 ELSE 5 END)
			,(CASE
      WHEN coalesce(TTL.numWrokOrder,0) > 0
      THEN CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,
         '">',coalesce(WO.vcWorkOrderName,'-'),'</a>')
      ELSE CONCAT('<a target="_blank" href="../projects/frmProjects.aspx?ProId=',PM.numProId,
         '">',coalesce(PM.vcProjectName,'-'),'</a>')
      END)
			,TTL.dtDate
			,CAST(FormatedDateFromDate(TTL.dtDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval),v_numDomainID) AS VARCHAR(50))
			,0
			,CAST('Work Order' AS VARCHAR(500))
			,CONCAT(TO_CHAR(coalesce(TTL.numTotalMinutes,0)/60,'00'),':',TO_CHAR(MOD(coalesce(TTL.numTotalMinutes,0),60),'00'))
			,coalesce(TTL.numTotalMinutes,0)
			,coalesce(SPDT.vcTaskName,'') AS vcDetails
      FROM
      tt_TEMPTIMELOG TTL
      INNER JOIN
      StagePercentageDetailsTask SPDT
      ON
      TTL.numTaskID = SPDT.numTaskId
      LEFT JOIN
      WorkOrder WO
      ON
      TTL.numWrokOrder = WO.numWOId
      LEFT JOIN
      ProjectsMaster PM
      ON
      TTL.numProjectID = PM.numProId
      WHERE
      dtDate BETWEEN v_dtFromDate AND v_dtToDate;
      open SWV_RefCur3 for
      SELECT numUserCntID AS "numUserCntID",
      numRecordID AS "numUserCntID",
      tintRecordType AS "tintRecordType",
      vcSource AS "vcSource",
      dtDate AS "dtDate",
      vcDate AS "vcDate",
      numType AS "numType",
      vcType AS "vcType",
      vcHours AS "vcHours",
      numMinutes AS "numMinutes",
      vcDetails AS "vcDetails" FROM tt_EMPLOYEETIME ORDER BY dtDate;
   ELSEIF v_tintView = 2 OR v_tintView = 3
   then
      DROP TABLE IF EXISTS tt_TEMPRESULT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPRESULT
      (
         dtDate DATE,
         numUserCntID NUMERIC(18,0),
         numTotalMinutes NUMERIC(18,0),
         vcTotalHours VARCHAR(20),
         numProductiveMinutes NUMERIC(18,0),
         vcMaxHours VARCHAR(20),
         numTotalBuildTasks NUMERIC(18,0),
         numTotalProjectTasks NUMERIC(18,0),
         numCapacityLoad NUMERIC(18,0)
      );
      WHILE v_dtFromDate <= v_dtToDate LOOP
         INSERT INTO tt_TEMPRESULT(dtDate
				,numUserCntID
				,numTotalMinutes
				,numProductiveMinutes
				,vcMaxHours
				,numTotalBuildTasks
				,numTotalProjectTasks)
         SELECT
         v_dtFromDate
				,TTL.numAssignTo
				,coalesce((SELECT SUM(numMinutes) FROM tt_EMPLOYEETIME WHERE numUserCntID = TTL.numAssignTo AND CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)),
         0)+coalesce((SELECT SUM(numTotalMinutes) FROM tt_TEMPTIMELOG TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND CAST(TTLInner.dtDate AS DATE)  = CAST(v_dtFromDate AS DATE)),0)
				,coalesce(numProductiveMinutes,0)
				,CONCAT(TO_CHAR(coalesce(numProductiveMinutes,0)/60,'00'),':',TO_CHAR(MOD(coalesce(numProductiveMinutes,0),60),'00'))
				,coalesce((SELECT SUM(T.numQtyItemsReq) FROM(SELECT numQtyItemsReq FROM tt_TEMPTIMELOG TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND coalesce(TTLInner.numWrokOrder,0) > 0 AND CAST(TTLInner.dtDate AS DATE)  = CAST(v_dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)
				,coalesce((SELECT SUM(T.numQtyItemsReq) FROM(SELECT numQtyItemsReq FROM tt_TEMPTIMELOG TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND coalesce(TTLInner.numProjectID,0) > 0 AND CAST(TTLInner.dtDate AS DATE)  = CAST(v_dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)
         FROM
         tt_TEMPTIMELOG TTL
         WHERE
         CAST(dtDate AS DATE)  = CAST(v_dtFromDate AS DATE)
         GROUP BY
         numAssignTo,numProductiveMinutes;
         v_dtFromDate := v_dtFromDate+INTERVAL '1 day';
      END LOOP;
      UPDATE
      tt_TEMPRESULT
      SET
      vcTotalHours = CONCAT(TO_CHAR(coalesce(numTotalMinutes,0)/60,'00'),':',TO_CHAR(MOD(coalesce(numTotalMinutes,0),60),'00')),numCapacityLoad =(CASE WHEN coalesce(numProductiveMinutes,0) > 0 THEN(numTotalMinutes*100)/numProductiveMinutes ELSE 0 END);
      open SWV_RefCur3 for
      SELECT dtDate AS "dtDate",
         numUserCntID AS "numUserCntID",
         numTotalMinutes AS "numTotalMinutes",
         vcTotalHours AS "vcTotalHours",
         numProductiveMinutes AS "numProductiveMinutes",
         vcMaxHours AS "vcMaxHours",
         numTotalBuildTasks AS "numTotalBuildTasks",
         numTotalProjectTasks AS "numTotalProjectTasks",
         numCapacityLoad AS "numCapacityLoad" FROM tt_TEMPRESULT;
   end if;
   RETURN;
END; $$;


