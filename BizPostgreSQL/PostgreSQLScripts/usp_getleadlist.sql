-- Stored procedure definition script USP_GetLeadList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetLeadList(v_numGrpID NUMERIC,                                                                
 v_numUserCntID NUMERIC,                                                                
 v_tintUserRightType SMALLINT,                                                                
 v_CustName VARCHAR(100) DEFAULT '',                                                                
 v_FirstName VARCHAR(100) DEFAULT '',                                                                
 v_LastName VARCHAR(100) DEFAULT '',                                                              
 v_SortChar CHAR(1) DEFAULT '0' ,                                                              
 v_numFollowUpStatus NUMERIC(9,0) DEFAULT 0 ,                                                              
 v_CurrentPage INTEGER DEFAULT NULL,                                                              
 v_PageSize INTEGER DEFAULT NULL,                                                              
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                              
 v_columnName VARCHAR(50) DEFAULT '',                                                              
 v_columnSortOrder VARCHAR(10) DEFAULT '',                                      
 v_ListType SMALLINT DEFAULT 0,                                      
 v_TeamType SMALLINT DEFAULT 0,                                       
 v_TeamMemID NUMERIC(9,0) DEFAULT 0,                                  
 v_numDomainID NUMERIC(9,0) DEFAULT 0    ,    
v_ClientTimeZoneOffset INTEGER DEFAULT NULL  ,
v_bitPartner BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(7100);                                                               
   v_firstRec  INTEGER;                                                              
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE  
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numDivisionID NUMERIC(9,0)  
   );                                                                      
                                                              
                                                
   v_strSql := 'SELECT                                                                                   
   DM.numDivisionID                                                                          
  FROM                                                                  
   CompanyInfo cmp join DivisionMaster DM on cmp.numCompanyId = DM.numCompanyID                                   
 left join ListDetails lst on lst.numListItemID = DM.numFollowUpStatus                                   
 join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID      
 left join AdditionalContactsInformation ADC1 on ADC1.numContactId = DM.numAssignedTo    
 left join AdditionalContactsInformation ADC2 on ADC2.numContactId = DM.numRecOwner                                                                         
  WHERE                                                                 
   DM.tintCRMType = 0  and coalesce(ADC.bitPrimaryContact,false) = true                                                                                        
 AND DM.numGrpId =' || SUBSTR(CAST(v_numGrpID AS VARCHAR(15)),1,15) || '                                   
 AND DM.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ''; 

   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' and (DM.numAssignedTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ||
      ' or DM.numCreatedBy =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
   end if;
                                                                      
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
   end if;                              
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''';
   end if;                              
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and cmp.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                             
                                                                     
   if v_numFollowUpStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || '  AND DM.numFollowUpStatus =' || SUBSTR(CAST(v_numFollowUpStatus AS VARCHAR(15)),1,15);
   end if;                                                               
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And cmp.vcCompanyName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;              
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in(select numTerritoryID from  UserTerritory where numUserCntID = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID = 0 or DM.numAssignedTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') 
 
';
   end if;    
      
                                             
   if v_ListType = 0 then

      if v_numGrpID = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
      end if;
      if v_numGrpID = 5 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
      end if;
   ELSEIF v_ListType = 1
   then

      if v_TeamMemID = 0 then
 
         if v_numGrpID <> 2 then 
            v_strSql := coalesce(v_strSql,'') || '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                        
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation              
  
    
          
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ')))';
         end if;
      else
         if v_numGrpID <> 2 then 
            v_strSql := coalesce(v_strSql,'') || '  AND ( DM.numRecOwner=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15) || '  or  DM.numAssignedTo=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15) || ') ';
         end if;
      end if;
   end if;                                      
                                      
   v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                        
                      
                      
   RAISE NOTICE '%',v_strSql;                                              
   EXECUTE 'insert into tt_TEMPTABLE(numDivisionID)                                              
' || v_strSql;                                              
                                                             
                                                                         
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                             
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                              
                                                             
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                              
                                              
                                              
                                              
                                              
                                              
   open SWV_RefCur for
   SELECT
   DM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as bintCreatedDate,
   cmp.numCompanyId,
   cmp.vcCompanyName,
   DM.vcDivisionName,
   DM.numDivisionID,
   ADC.numContactId,
   fn_GetListItemName(cmp.numNoOfEmployeesId) as Employees,
   ADC.vcFirstName || ' ' || ADC.vcLastname as Name,
   case when ADC.numPhone <> '' then ADC.numPhone || case when ADC.numPhoneExtension <> '' then ' - ' || ADC.numPhoneExtension else '' end  else '' end as numPhone,
   ADC.vcEmail,
   DM.numRecOwner,
   DM.numGrpId,
   DM.numTerID,
   lst.vcData,
   ADC1.vcFirstName || ' ' || ADC1.vcLastname as vcUserName,
   fn_GetContactName(numAssignedTo) || '/' || fn_GetContactName(numAssignedBy) AS AssignedToBy
   FROM
   CompanyInfo cmp
   join DivisionMaster DM
   on cmp.numCompanyId = DM.numCompanyID
   left join Listdetails lst
   on lst.numListItemID = DM.numFollowUpStatus
   join AdditionalContactsInformation ADC
   on ADC.numDivisionId = DM.numDivisionID
   left join AdditionalContactsInformation ADC1 on ADC1.numContactId = DM.numRecOwner
   join tt_TEMPTABLE T
   on T.numDivisionID = DM.numDivisionID
   WHERE
   DM.tintCRMType = 0 and coalesce(ADC.bitPrimaryContact,false) = true
   and ID > v_firstRec and ID < v_lastRec order by ID;                                          
                                                    
                          
                                              
                                              
                                              
   RETURN;
END; $$;


