-- Stored procedure definition script USP_BizAPI_ValidateSalesOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizAPI_ValidateSalesOrder(v_numDomainID NUMERIC(18,0),
	v_numCustomerID NUMERIC(18,0) DEFAULT NULL,
	v_numContactID NUMERIC(18,0) DEFAULT NULL,
	v_vcItems TEXT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_ErrorCode  INTEGER;
   v_str  VARCHAR(20);
   v_index  INTEGER;
	
   v_TotalItem  INTEGER;
   v_ItemFound  INTEGER;
BEGIN

	
   DROP TABLE IF EXISTS tt_ITEM CASCADE;
   CREATE TEMPORARY TABLE tt_ITEM
   (
      ID BIGINT
   );
   v_ErrorCode := 0;
   v_TotalItem := 0;
   v_ItemFound := 0;
	
   IF v_numCustomerID IS NOT NULL then
	
      IF NOT EXISTS(SELECT * FROM DivisionMaster WHERE numDivisionID = v_numCustomerID AND numDomainID = v_numDomainID) then
         v_ErrorCode := 60000;
      end if;
   end if;
	
   IF v_numContactID IS NOT NULL AND v_ErrorCode = 0 then
	
      IF NOT EXISTS(SELECT * FROM AdditionalContactsInformation WHERE numContactId =  v_numContactID) then
         v_ErrorCode := 60001;
      end if;
   end if;
	
   IF v_vcItems IS NOT NULL AND v_ErrorCode = 0 then
	
      v_index := POSITION(',' IN v_vcItems);
      WHILE v_index > 0 LOOP
         v_str := SUBSTR(v_vcItems,1,v_index -1);
         v_vcItems := SUBSTR(v_vcItems,v_index+1,LENGTH(v_vcItems) -v_index);
         INSERT INTO tt_ITEM(ID) values(CAST(v_str AS numeric));
			  
         v_index := POSITION(',' IN v_vcItems);
      END LOOP;
      v_str := v_vcItems;
      INSERT INTO tt_ITEM(ID) values(CAST(v_str AS numeric));
		
      select   COUNT(*) INTO v_TotalItem FROM tt_ITEM;
      select   COUNT(*) INTO v_ItemFound FROM Item WHERE numDomainID = v_numDomainID AND numItemCode IN(SELECT ID FROM tt_ITEM);
      IF v_TotalItem <> v_ItemFound then
         v_ErrorCode := 60002;
      end if;
   end if;
	
   open SWV_RefCur for SELECT v_ErrorCode;
END; $$;













