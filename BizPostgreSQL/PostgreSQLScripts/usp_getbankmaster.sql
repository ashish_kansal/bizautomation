-- Stored procedure definition script USP_GetBankMaster for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBankMaster(v_numBankMasterID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT
   cast(numBankMasterID as VARCHAR(255)),
	cast(tintBankType as VARCHAR(255)),
	cast(vcFIId as VARCHAR(255)),
	cast(vcFIName as VARCHAR(255)),
	cast(vcFIOrganization as VARCHAR(255)),
	cast(vcFIOFXServerUrl as VARCHAR(255)),
	cast(vcBankID as VARCHAR(255)),
	cast(vcOFXAccessKey as VARCHAR(255)),
	cast(dtCreatedDate as VARCHAR(255)),
	cast(dtModifiedDate as VARCHAR(255)),
	cast(vcBankPhone as VARCHAR(255)),
	cast(vcBankWebsite as VARCHAR(255)),
	cast(vcUserIdLabel as VARCHAR(255))
   FROM
   BankMaster
   WHERE
	(numBankMasterID = v_numBankMasterID
   or v_numBankMasterID = 0);
END; $$;














