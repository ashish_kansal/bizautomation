-- Stored procedure definition script usp_SaveTopicMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveTopicMaster(INOUT v_numTopicId NUMERIC(18,0) DEFAULT 0   , 
v_intRecordType INTEGER DEFAULT 0 , 
v_numRecordId NUMERIC(18,0) DEFAULT 0, 
v_vcTopicTitle TEXT DEFAULT '', 
v_numCreatedBy NUMERIC(18,0) DEFAULT 0, 
v_numUpdatedBy NUMERIC(18,0) DEFAULT 0, 
v_bitIsInternal BOOLEAN DEFAULT false, 
v_numDomainId NUMERIC(18,0) DEFAULT 0,
INOUT v_status VARCHAR(100)  DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
                                                        
   IF v_numTopicId = 0 then
		
      INSERT INTO TOPICMASTER(intRecordType, numRecordId, vcTopicTitle, numCreateBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, bitIsInternal, numDomainID)
			VALUES(v_intRecordType, v_numRecordId, v_vcTopicTitle, v_numCreatedBy, LOCALTIMESTAMP , v_numUpdatedBy, LOCALTIMESTAMP , v_bitIsInternal, v_numDomainId);
			

      v_numTopicId := CURRVAL('TopicMaster_seq');
      v_status := '1';
   ELSE
      UPDATE
      TOPICMASTER
      SET
      intRecordType = v_intRecordType,numRecordId = v_numRecordId,vcTopicTitle = v_vcTopicTitle,
      numUpdatedBy = v_numUpdatedBy,dtmUpdatedOn = LOCALTIMESTAMP,
      bitIsInternal = v_bitIsInternal,numDomainID = v_numDomainId
      WHERE
      numTopicId = v_numTopicId
      AND numDomainID = v_numDomainId;
      v_status := '3';
   end if;
   RETURN;
END; $$;


