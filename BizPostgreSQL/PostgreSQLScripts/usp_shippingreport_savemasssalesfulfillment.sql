DROP FUNCTION IF EXISTS USP_ShippingReport_SaveMassSalesFulfillment;

CREATE OR REPLACE FUNCTION USP_ShippingReport_SaveMassSalesFulfillment(v_numDomainId NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
    ,v_numOppBizDocId NUMERIC(18,0)
	,v_numShippingCompany NUMERIC(18,0)
	,v_numShippingService NUMERIC(18,0)
	,v_vcBoxes TEXT
	,v_vcBoxItems TEXT
	,v_vcShippingDetail TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingReportID  NUMERIC(18,0);
   v_numOrderShippingCompany  NUMERIC(18,0);
   v_numOrderShippingService  NUMERIC(18,0);
   v_numDivisionShippingCompany  NUMERIC(18,0);
   v_numDivisionShippingService  NUMERIC(18,0);

   v_vcFromName  VARCHAR(1000);
   v_vcFromCompany  VARCHAR(1000);
   v_vcFromPhone  VARCHAR(100);
   v_vcFromAddressLine1  VARCHAR(50);
   v_vcFromAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcFromCity  VARCHAR(50);
   v_vcFromState  VARCHAR(50);
   v_vcFromZip  VARCHAR(50);
   v_vcFromCountry  VARCHAR(50);
   v_bitFromResidential  BOOLEAN DEFAULT 0;

   v_vcToName  VARCHAR(1000);
   v_vcToCompany  VARCHAR(1000);
   v_vcToPhone  VARCHAR(100);
   v_vcToAddressLine1  VARCHAR(50);
   v_vcToAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcToCity  VARCHAR(50);
   v_vcToState  VARCHAR(50);
   v_vcToZip  VARCHAR(50);
   v_vcToCountry  VARCHAR(50);
   v_bitToResidential  BOOLEAN DEFAULT 0;

   v_IsCOD  BOOLEAN;
   v_IsDryIce  BOOLEAN;
   v_IsHoldSaturday  BOOLEAN;
   v_IsHomeDelivery  BOOLEAN;
   v_IsInsideDelevery  BOOLEAN;
   v_IsInsidePickup  BOOLEAN;
   v_IsReturnShipment  BOOLEAN;
   v_IsSaturdayDelivery  BOOLEAN;
   v_IsSaturdayPickup  BOOLEAN;
   v_IsAdditionalHandling  BOOLEAN;
   v_IsLargePackage  BOOLEAN;
   v_numCODAmount  NUMERIC(18,2);
   v_vcCODType  VARCHAR(50);
   v_vcDeliveryConfirmation  VARCHAR(1000);
   v_vcDescription  TEXT;
   v_tintSignatureType  SMALLINT;
   v_tintPayorType  SMALLINT DEFAULT 0;
   v_vcPayorAccountNo  VARCHAR(20) DEFAULT '';
   v_numPayorCountry  NUMERIC(18,0);
   v_vcPayorZip  VARCHAR(50) DEFAULT '';
   v_numTotalCustomsValue  NUMERIC(18,2);
   v_numTotalInsuredValue  NUMERIC(18,2);

   v_hDocBoxItem  INTEGER;
   v_iBox  INTEGER DEFAULT 1;
   v_iBoxCount  INTEGER; 
   v_numUserBoxID  NUMERIC(18,0);
   v_numTempBoxID  NUMERIC(18,0);
   v_bitUserProvidedBoxWeight  BOOLEAN;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_hDocShippingDetail  INTEGER;

	-- MAKE ENTRY IN ShippingReport TABLE
   v_bitUseBizdocAmount  BOOLEAN;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF coalesce(v_numShippingCompany,0) = 0 OR coalesce(v_numShippingService,0) = 0 then
	
         select   coalesce(intUsedShippingCompany,0), coalesce(numShippingService,0), coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0) INTO v_numOrderShippingCompany,v_numOrderShippingService,v_numDivisionShippingCompany,
         v_numDivisionShippingService FROM
         OpportunityMaster
         INNER JOIN
         DivisionMaster
         ON
         OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE
         OpportunityMaster.numDomainId = v_numDomainId
         AND DivisionMaster.numDomainID = v_numDomainId
         AND OpportunityMaster.numOppId = v_numOppID;

	
		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
         IF coalesce(v_numOrderShippingCompany,0) > 0 then
		
            IF coalesce(v_numOrderShippingService,0) = 0 then
			
               v_numOrderShippingService := CASE v_numOrderShippingCompany
               WHEN 91 THEN 15 --FEDEX
               WHEN 88 THEN 43 --UPS
               WHEN 90 THEN 70 --USPS
               END;
            end if;
            v_numShippingCompany := v_numOrderShippingCompany;
            v_numShippingService := v_numOrderShippingService;
         ELSEIF coalesce(v_numDivisionShippingCompany,0) > 0
         then -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		
            IF coalesce(v_numDivisionShippingService,0) = 0 then
			
               v_numDivisionShippingService := CASE v_numDivisionShippingCompany
               WHEN 91 THEN 15 --FEDEX
               WHEN 88 THEN 43 --UPS
               WHEN 90 THEN 70 --USPS
               END;
            end if;
            v_numShippingCompany := v_numDivisionShippingCompany;
            v_numShippingService := v_numDivisionShippingService;
         ELSE
            select   coalesce(numShipCompany,0) INTO v_numShippingCompany FROM Domain WHERE numDomainId = v_numDomainId;
            IF v_numShippingCompany <> 91 OR v_numShippingCompany <> 88 OR v_numShippingCompany <> 90 then
			
               v_numShippingCompany := 91;
            end if;
            v_numShippingService := CASE v_numShippingCompany
            WHEN 91 THEN 15 --FEDEX
            WHEN 88 THEN 43 --UPS
            WHEN 90 THEN 70 --USPS
            END;
         end if;
      end if;
      IF LENGTH(coalesce(v_vcShippingDetail,'')) > 0 then
         select   FromContact, FromCompany, FromPhone, FromAddress1, FromAddress2, FromCity, SUBSTR(CAST(FromState AS VARCHAR(18)),1,18), FromZip, SUBSTR(CAST(FromCountry AS VARCHAR(18)),1,18), IsFromResidential, ToContact, ToCompany, ToPhone, ToAddress1, ToAddress2, ToCity, SUBSTR(CAST(ToState AS VARCHAR(18)),1,18), ToZip, SUBSTR(CAST(ToCountry AS VARCHAR(18)),1,18), IsToResidential, PayerType, AccountNo, Country, ZipCode, IsCOD, IsHomeDelivery, IsInsideDelivery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, TotalInsuredValue, TotalCustomsValue, CODType, CODAmount, Description, CAST(coalesce(SignatureType,'0') AS SMALLINT) INTO v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromAddressLine2,
         v_vcFromCity,v_vcFromState,v_vcFromZip,v_vcFromCountry,
         v_bitFromResidential,v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,
         v_vcToAddressLine2,v_vcToCity,v_vcToState,v_vcToZip,v_vcToCountry,
         v_bitToResidential,v_tintPayorType,v_vcPayorAccountNo,v_numPayorCountry,
         v_vcPayorZip,v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,
         v_IsSaturdayDelivery,v_IsSaturdayPickup,v_IsAdditionalHandling,
         v_IsLargePackage,v_numTotalInsuredValue,v_numTotalCustomsValue,v_vcCODType,
         v_numCODAmount,v_vcDescription,v_tintSignatureType 
		 FROM
		 XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcShippingDetail AS XML)
			COLUMNS
				FromContact VARCHAR(1000) PATH 'FromContact'
				,FromPhone VARCHAR(100) PATH 'FromPhone'
				,FromCompany VARCHAR(1000) PATH 'FromCompany'
				,FromAddress1 VARCHAR(50) PATH 'FromAddress1'
				,FromAddress2 VARCHAR(50) PATH 'FromAddress2'
				,FromCountry NUMERIC(18,0) PATH 'FromCountry'
				,FromState NUMERIC(18,0) PATH 'FromState'
				,FromCity VARCHAR(50) PATH 'FromCity'
				,FromZip VARCHAR(50) PATH 'FromZip'
				,IsFromResidential BOOLEAN PATH 'IsFromResidential'
				,ToContact VARCHAR(1000) PATH 'ToContact'
				,ToPhone VARCHAR(100) PATH 'ToPhone'
				,ToCompany VARCHAR(1000) PATH 'ToCompany'
				,ToAddress1 VARCHAR(50) PATH 'ToAddress1'
				,ToAddress2 VARCHAR(50) PATH 'ToAddress2'
				,ToCountry NUMERIC(18,0) PATH 'ToCountry'
				,ToState NUMERIC(18,0) PATH 'ToState'
				,ToCity VARCHAR(50) PATH 'ToCity'
				,ToZip VARCHAR(50) PATH 'ToZip'
				,IsToResidential BOOLEAN PATH 'IsToResidential'
				,PayerType SMALLINT PATH 'PayerType'
				,ReferenceNo VARCHAR(200) PATH 'ReferenceNo'
				,AccountNo VARCHAR(20) PATH 'AccountNo'
				,ZipCode VARCHAR(50) PATH 'ZipCode'
				,Country NUMERIC(18,0) PATH 'Country'
				,IsAdditionalHandling BOOLEAN PATH 'IsAdditionalHandling'
				,IsCOD BOOLEAN PATH 'IsCOD'
				,IsHomeDelivery BOOLEAN PATH 'IsHomeDelivery'
				,IsInsideDelivery BOOLEAN PATH 'IsInsideDelivery'
				,IsInsidePickup BOOLEAN PATH 'IsInsidePickup'
				,IsLargePackage BOOLEAN PATH 'IsLargePackage'
				,IsSaturdayDelivery BOOLEAN PATH 'IsSaturdayDelivery'
				,IsSaturdayPickup BOOLEAN PATH 'IsSaturdayPickup'
				,SignatureType SMALLINT PATH 'SignatureType'
				,Description TEXT PATH 'Description'
				,TotalInsuredValue NUMERIC(18,2) PATH 'TotalInsuredValue'
				,TotalCustomsValue NUMERIC(18,2) PATH 'TotalCustomsValue'
				,CODType VARCHAR(50) PATH 'CODType'
				,CODAmount NUMERIC(18,2) PATH 'CODAmount'
		) AS X;
      ELSE
		-- GET FROM ADDRESS
		
         select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, SUBSTR(CAST(vcState AS VARCHAR(18)),1,18), vcZipCode, SUBSTR(CAST(vcCountry AS VARCHAR(18)),1,18), bitResidential INTO v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromCity,
         v_vcFromState,v_vcFromZip,v_vcFromCountry,v_bitFromResidential FROM
         fn_GetShippingReportAddress(1,v_numDomainId,v_numOppID);

		-- GET TO ADDRESS
         select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, SUBSTR(CAST(vcState AS VARCHAR(18)),1,18), vcZipCode, SUBSTR(CAST(vcCountry AS VARCHAR(18)),1,18), bitResidential INTO v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,v_vcToCity,v_vcToState,
         v_vcToZip,v_vcToCountry,v_bitToResidential FROM
         fn_GetShippingReportAddress(2,v_numDomainId,v_numOppID);
         select   IsCOD, IsHomeDelivery, IsInsideDelevery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, vcCODType, vcDeliveryConfirmation, vcDescription, CAST(coalesce(vcSignatureType,'0') AS SMALLINT) INTO v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsSaturdayDelivery,
         v_IsSaturdayPickup,v_IsAdditionalHandling,v_IsLargePackage,
         v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,v_tintSignatureType FROM
         OpportunityMaster
         INNER JOIN
         DivisionMaster
         ON
         OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
         LEFT JOIN
         DivisionMasterShippingConfiguration
         ON
         DivisionMasterShippingConfiguration.numDomainID = v_numDomainId
         AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID WHERE
         OpportunityMaster.numDomainId = v_numDomainId
         AND DivisionMaster.numDomainID = v_numDomainId
         AND OpportunityMaster.numOppId = v_numOppID;
         select   bitUseBizdocAmount, coalesce(numTotalInsuredValue,0), coalesce(numTotalCustomsValue,0) INTO v_bitUseBizdocAmount,v_numTotalCustomsValue,v_numTotalInsuredValue FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_bitUseBizdocAmount = true then
		
            select   coalesce(monDealAmount,0), coalesce(monDealAmount,0), coalesce(monDealAmount,0) INTO v_numTotalCustomsValue,v_numTotalInsuredValue,v_numCODAmount FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocId;
         ELSE
            select   coalesce(monDealAmount,0) INTO v_numCODAmount FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocId;
         end if;
      end if;

	-- MAKE ENTRY IN ShippingReport TABLE
      INSERT INTO ShippingReport(numDomainID,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
		,tintPayorType,vcPayorAccountNo,numPayorCountry,vcPayorZip)
	VALUES(v_numDomainId,v_numUserCntID,TIMEZONE('UTC',now()),v_numOppID,v_numOppBizDocId,v_numShippingCompany,CAST(v_numShippingService AS VARCHAR(30))
		,v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromAddressLine2,v_vcFromCity,v_vcFromState,v_vcFromZip,v_vcFromCountry,v_bitFromResidential
		,v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,v_vcToAddressLine2,v_vcToCity,v_vcToState,v_vcToZip,v_vcToCountry,v_bitToResidential
		,v_IsCOD,v_IsDryIce,v_IsHoldSaturday,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsReturnShipment,v_IsSaturdayDelivery,v_IsSaturdayPickup
		,v_numCODAmount,v_vcCODType,v_numTotalInsuredValue,v_IsAdditionalHandling,v_IsLargePackage,v_vcDeliveryConfirmation,v_vcDescription,v_numTotalCustomsValue,v_tintSignatureType
		,v_tintPayorType,v_vcPayorAccountNo,v_numPayorCountry,v_vcPayorZip);
	
      v_numShippingReportID := CURRVAL('ShippingReport_seq');
      BEGIN
         CREATE TEMP SEQUENCE tt_TableShippingBox_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TABLESHIPPINGBOX CASCADE;
      CREATE TEMPORARY TABLE tt_TABLESHIPPINGBOX
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numBoxID INTEGER,
         vcBoxName VARCHAR(300),
         fltHeight DOUBLE PRECISION,
         fltWidth DOUBLE PRECISION,
         fltLength DOUBLE PRECISION,
         fltWeight DOUBLE PRECISION,
         bitUserProvidedBoxWeight BOOLEAN,
         numPackageType NUMERIC(18,0)
      );
  
      INSERT INTO tt_TABLESHIPPINGBOX(numBoxID
		,vcBoxName
		,fltHeight
		,fltWidth
		,fltLength
		,fltWeight
		,bitUserProvidedBoxWeight
		,numPackageType)
      SELECT
      ID
		,coalesce(Name,'')
		,coalesce(Height,0)
		,coalesce(Width,0)
		,coalesce(Length,0)
		,coalesce(Weight,0)
		,coalesce(IsUserProvidedBoxWeight,false)
		,PackageType
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcBoxes AS XML)
			COLUMNS
				numID FOR ORDINALITY,
				ID NUMERIC(18,0) PATH 'ID',
				Name VARCHAR(100) PATH 'Name',
				Height DOUBLE PRECISION PATH 'Height',
				Width DOUBLE PRECISION PATH 'Width',
				Length DOUBLE PRECISION PATH 'Length',
				Weight DOUBLE PRECISION PATH 'Weight',
				IsUserProvidedBoxWeight BOOLEAN PATH 'IsUserProvidedBoxWeight',
				PackageType NUMERIC(18,0) PATH 'PackageType'
		) AS X;

      DROP TABLE IF EXISTS tt_TABLESHIPPINGBOXITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TABLESHIPPINGBOXITEMS
      (
         numBoxID NUMERIC(18,0),
         numOppItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION
      );

      INSERT INTO tt_TABLESHIPPINGBOXITEMS(numBoxID
		,numOppItemID
		,numUnitHour)
      SELECT
      BoxID
		,OppItemID
		,coalesce(Quantity,0)
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcBoxItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				BoxID NUMERIC(18,0) PATH 'BoxID',
				OppItemID NUMERIC(18,0) PATH 'OppItemID',
				Quantity DOUBLE PRECISION PATH 'Quantity'
		) AS X;

      select   COUNT(*) INTO v_iBoxCount FROM tt_TABLESHIPPINGBOX;
      WHILE v_iBox <= v_iBoxCount LOOP
         select   numBoxID, coalesce(bitUserProvidedBoxWeight,false) INTO v_numUserBoxID,v_bitUserProvidedBoxWeight FROM
         tt_TABLESHIPPINGBOX WHERE
         ID = v_iBox;
         INSERT INTO ShippingBox(vcBoxName
			,numShippingReportID
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numPackageTypeID
			,numServiceTypeID
			,fltDimensionalWeight
			,numShipCompany)
         SELECT
         coalesce(vcBoxName,'')
			,v_numShippingReportID
			,(CASE WHEN coalesce(fltWeight,0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(fltHeight,0) = 0 THEN 1 ELSE fltHeight END)*(CASE WHEN coalesce(fltWidth,0) = 0 THEN 1 ELSE fltWidth END)*(CASE WHEN coalesce(fltLength,0) = 0 THEN 1 ELSE fltLength END) AS DOUBLE PRECISION)/166) ELSE fltWeight END)
			,(CASE WHEN coalesce(fltHeight,0) = 0 THEN 1 ELSE fltHeight END)
			,(CASE WHEN coalesce(fltWidth,0) = 0 THEN 1 ELSE fltWidth END)
			,(CASE WHEN coalesce(fltLength,0) = 0 THEN 1 ELSE fltLength END)
			,TIMEZONE('UTC',now())
			,v_numUserCntID
			,(CASE
         WHEN coalesce(numPackageType,0) > 0 AND EXISTS(SELECT
            ID
            FROM
            ShippingPackageType
            WHERE
            numShippingCompanyID = coalesce(v_numShippingCompany,0)
            AND numNsoftwarePackageTypeID = numPackageType
            AND 1 =(CASE
            WHEN (numShippingCompanyID = 88 OR numShippingCompanyID = 91) THEN 1
            ELSE(CASE WHEN coalesce(bitEndicia,false) = (CASE WHEN coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = v_numDomainId AND numListItemID = 90 AND intShipFieldID = 21),'0')='1' THEN true ELSE false END) THEN 1 ELSE 0 END)
            END))
         THEN(SELECT 
               ID
               FROM
               ShippingPackageType
               WHERE
               numShippingCompanyID = coalesce(v_numShippingCompany,0)
               AND numNsoftwarePackageTypeID = numPackageType
               AND 1 =(CASE
               WHEN (numShippingCompanyID = 88 OR numShippingCompanyID = 91) THEN 1
               ELSE(CASE WHEN coalesce(bitEndicia,false) = (CASE WHEN coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = v_numDomainId AND numListItemID = 90 AND intShipFieldID = 21),'0') = '1' THEN true ELSE FALSE END) THEN 1 ELSE 0 END)
               END) LIMIT 1)
         ELSE(CASE
            WHEN v_numShippingCompany = 88 THEN 19
            WHEN v_numShippingCompany = 90 THEN (CASE WHEN coalesce((SELECT  vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = v_numDomainId AND intShipFieldID = 21 AND numListItemID = 90 LIMIT 1),'0') = '1' THEN 69 ELSE 50 END)
            WHEN v_numShippingCompany = 91 THEN 7
            END)
         END)
			,v_numShippingService
			,(CASE WHEN coalesce(fltWeight,0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(fltHeight,0) = 0 THEN 1 ELSE fltHeight END)*(CASE WHEN coalesce(fltWidth,0) = 0 THEN 1 ELSE fltWidth END)*(CASE WHEN coalesce(fltLength,0) = 0 THEN 1 ELSE fltLength END) AS DOUBLE PRECISION)/166) ELSE fltWeight END)
			,v_numShippingCompany
         FROM
         tt_TABLESHIPPINGBOX
         WHERE
         ID = v_iBox;
         v_numTempBoxID := CURRVAL('ShippingBox_seq');

		-- MAKE ENTRY IN ShippingReportItems Table
         INSERT INTO ShippingReportItems(numShippingReportId
			,numItemCode
			,tintServiceType
			,monShippingRate
			,fltTotalWeight
			,intNoOfBox
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numBoxID
			,numOppBizDocItemID
			,intBoxQty)
         SELECT
         v_numShippingReportID
			,OpportunityBizDocItems.numItemCode
			,v_numShippingService
			,0
			,(CASE
         WHEN coalesce(bitKitParent,false) = true
         AND(CASE
         WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
         THEN 1
         ELSE 0
         END) = 0
         THEN
            GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
         ELSE
            coalesce(fltWeight,0)
         END)
			,1
			,coalesce(I.fltHeight,0)
			,coalesce(I.fltWidth,0)
			,coalesce(I.fltLength,0)
			,TIMEZONE('UTC',now())
			,v_numUserCntID
			,v_numTempBoxID
			,OpportunityBizDocItems.numOppBizDocItemID
			,TSBI.numUnitHour
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         INNER JOIN
         OpportunityItems OI
         ON
         OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         WareHouseItems WI
         ON
         OpportunityBizDocItems.numWarehouseItmsID = WI.numWareHouseItemID
         INNER JOIN
         Item I
         ON
         OpportunityBizDocItems.numItemCode = I.numItemCode
         INNER JOIN
         tt_TABLESHIPPINGBOXITEMS TSBI
         ON
         OpportunityBizDocItems.numOppItemID = TSBI.numOppItemID
         AND OI.numoppitemtCode = TSBI.numOppItemID
         WHERE
         OpportunityBizDocs.numOppBizDocsId = v_numOppBizDocId
         AND TSBI.numBoxID = v_numUserBoxID;
         IF coalesce(v_bitUserProvidedBoxWeight,false) <> true then
		
            UPDATE
            ShippingBox
            SET
            fltTotalWeight = coalesce(fltTotalWeight,0)+coalesce((SELECT SUM((coalesce(fltTotalWeight,0)*coalesce(intBoxQty,0))) FROM ShippingReportItems WHERE numShippingReportId = v_numShippingReportID AND numBoxID = v_numTempBoxID),0)
            WHERE
            numBoxID = v_numTempBoxID;
         end if;
         v_iBox := v_iBox::bigint+1;
      END LOOP;
      open SWV_RefCur for SELECT v_numShippingReportID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;











