-- Stored procedure definition script USP_BizDOcAlertManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDOcAlertManage(v_numBizDocID NUMERIC,  
v_bitCreated BOOLEAN,  
v_bitModified BOOLEAN,  
v_numEmailTemplate NUMERIC,  
v_bitOppOwner BOOLEAN,  
v_bitCCManager BOOLEAN,  
v_bitPriConatct BOOLEAN,
v_bitApproved BOOLEAN,
v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into BizDocAlerts(numBizDocID,
  bitCreated,
  bitModified,
  bitApproved,
  numEmailTemplate,
  bitOppOwner,
  bitCCManager,
  bitPriConatct,
  numDomainID)
  values(v_numBizDocID,
  v_bitCreated,
  v_bitModified,
  v_bitApproved,
  v_numEmailTemplate,
  v_bitOppOwner,
  v_bitCCManager,
  v_bitPriConatct,
  v_numDomainID);
RETURN;
END; $$;


