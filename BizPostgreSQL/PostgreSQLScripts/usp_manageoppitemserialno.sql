-- Stored procedure definition script USP_ManageOppItemSerialNo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageOppItemSerialNo(v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
   v_numTempLotQty  NUMERIC(18,0);
   v_numTempWareHouseItmsDTLID  NUMERIC(18,0);
   v_numTempFromWarehouseItemID  NUMERIC(18,0);
   v_numTempToWarehouseItemID  NUMERIC(18,0);
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;

   v_numOppID  NUMERIC(18,0);
   v_numOppItemID  NUMERIC(18,0);
   v_minROWNUMBER  INTEGER;
   v_maxROWNUMBER  INTEGER;
   v_bitStockTransfer  BOOLEAN;
   v_numWareHouseItmsDTLID  NUMERIC(9,0);
   v_numWareHouseItemID  NUMERIC(9,0);
   v_numTempItemCode  NUMERIC(18,0);
   v_vcTempSerialNo  VARCHAR(100);
BEGIN    
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT
      X.*
		,ROW_NUMBER() OVER(order by X.numWareHouseItemID) AS ROWNUMBER
	
      FROM(SELECT
      vcSerialNo
			,numWareHouseItemID
			,numOppId
			,numOppItemId
			,numQty
      FROM
	  XMLTABLE
		(
			'NewDataSet/SerializedItems'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcSerialNo VARCHAR(50) PATH 'vcSerialNo',
				numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
				numOppId NUMERIC(9,0) PATH 'numOppId',
				numOppItemId NUMERIC(9,0) PATH 'numOppItemId',
				numQty NUMERIC(9,0) PATH 'numQty'
		)) AS X;

   select   MIN(ROWNUMBER), MAX(ROWNUMBER) INTO v_minROWNUMBER,v_maxROWNUMBER FROM tt_TEMPTABLE;

   WHILE  v_minROWNUMBER <= v_maxROWNUMBER LOOP
      select   numWareHouseItemID, numOppID, numOppItemId, numQty INTO v_numWareHouseItemID,v_numOppID,v_numOppItemID,v_numTempLotQty FROM tt_TEMPTABLE X WHERE X.ROWNUMBER = v_minROWNUMBER;
      select   X.vcSerialNo INTO v_vcTempSerialNo FROM tt_TEMPTABLE X WHERE X.ROWNUMBER = v_minROWNUMBER;
      select   coalesce(bitSerialized,false), numItemCode, coalesce(bitLotNo,false) INTO v_bitSerial,v_numTempItemCode,v_bitLot FROM Item WHERE numItemCode IN(SELECT numItemID FROM WareHouseItems WHERE numWareHouseItemID = v_numWareHouseItemID);
      select   coalesce(bitStockTransfer,false) INTO v_bitStockTransfer FROM OpportunityMaster WHERE numOppId = v_numOppID;
      IF v_bitStockTransfer = true then
		
         IF(SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID IN(SELECT coalesce(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = v_numTempItemCode) AND LOWER(vcSerialNo) = LOWER(v_vcTempSerialNo)) = 0 then
			
            RAISE EXCEPTION 'INVALID_SERIAL_NO';
            RETURN;
         end if;
         select   numWarehouseItmsID, numToWarehouseItemID INTO v_numTempFromWarehouseItemID,v_numTempToWarehouseItemID FROM OpportunityItems WHERE numOppId = v_numOppID AND numoppitemtCode = v_numOppItemID;
         select   numWareHouseItmsDTLID INTO v_numTempWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempFromWarehouseItemID AND vcSerialNo = v_vcTempSerialNo    LIMIT 1;
         IF v_bitLot = true then
			
            IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempFromWarehouseItemID AND numQty >= v_numTempLotQty AND vcSerialNo = v_vcTempSerialNo) then
				
               UPDATE
               WareHouseItmsDTL
               SET
               numQty = numQty -v_numTempLotQty
               WHERE
               numWareHouseItemID = v_numTempFromWarehouseItemID
               AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
               INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
						vcSerialNo,
						numQty,
						dExpirationDate,
						bitAddedFromPO)
               SELECT
               v_numTempToWarehouseItemID,
						vcSerialNo,
						v_numTempLotQty,
						dExpirationDate,
						bitAddedFromPO
               FROM
               WareHouseItmsDTL
               WHERE
               numWareHouseItemID = v_numTempFromWarehouseItemID AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
            ELSE
               RAISE EXCEPTION 'INSUFFICIENT LOT QUANTITY';
               RETURN;
            end if;
         ELSE
            IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempFromWarehouseItemID AND vcSerialNo = v_vcTempSerialNo AND numQty = 1) then
				
               RAISE NOTICE '%',v_numTempFromWarehouseItemID;
               RAISE NOTICE '%',v_numTempToWarehouseItemID;
               RAISE NOTICE '%',v_numTempWareHouseItmsDTLID;
               UPDATE
               WareHouseItmsDTL
               SET
               numWareHouseItemID = v_numTempToWarehouseItemID
               WHERE
               numWareHouseItemID = v_numTempFromWarehouseItemID
               AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
            ELSE
               RAISE EXCEPTION 'INSUFFICIENT SERIAL QUANTITY';
               RETURN;
            end if;
         end if;
      ELSE
         IF v_bitSerial = true AND(SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID IN(SELECT coalesce(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = v_numTempItemCode) AND LOWER(vcSerialNo) = LOWER(v_vcTempSerialNo)) > 0 then
			
            RAISE EXCEPTION 'DUPLICATE_SERIAL_NO';
            RETURN;
         end if;
         INSERT INTO WareHouseItmsDTL(numWareHouseItemID
				,vcSerialNo
				,numQty
				,bitAddedFromPO)
         SELECT
         X.numWareHouseItemID
				,X.vcSerialNo
				,X.numQty
				,true
         FROM
         tt_TEMPTABLE X
         WHERE
         X.ROWNUMBER = v_minROWNUMBER;
         v_numWareHouseItmsDTLID := CURRVAL('WareHouseItmsDTL_seq');
         INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID
				,numOppID
				,numOppItemID
				,numWarehouseItmsID
				,numQty)
         SELECT
         v_numWareHouseItmsDTLID
				,X.numOppID
				,X.numOppItemId
				,X.numWareHouseItemID
				,X.numQty
         FROM
         tt_TEMPTABLE X
         WHERE
         X.ROWNUMBER = v_minROWNUMBER;
      end if;
      select   MIN(ROWNUMBER) INTO v_minROWNUMBER FROM  tt_TEMPTABLE WHERE  ROWNUMBER > v_minROWNUMBER;
   END LOOP;	

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
END; $$;
                      

