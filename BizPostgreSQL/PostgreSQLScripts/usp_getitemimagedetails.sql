-- Stored procedure definition script USP_GetItemImageDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemImageDetails(v_numItemImageId		BIGINT,
	v_numDomainID			NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT coalesce(II.numItemImageId,0) AS numItemImageId,
			coalesce(I.numItemCode,0) AS numItemCode,
			coalesce(II.vcPathForImage,'') AS vcPathForImage,
			coalesce(II.vcPathForTImage,'') AS vcPathForTImage,
			coalesce(II.bitDefault,false) AS bitDefault,
			coalesce(II.intDisplayOrder,0) AS intDisplayOrder,
			coalesce(II.numDomainId,0) AS numDomainId,
			coalesce(II.bitIsImage,false) AS bitIsImage
   FROM  ItemImages II
   JOIN Item I ON II.numItemCode = I.numItemCode AND II.numDomainId =  I.numDomainID
   WHERE II.numItemImageId = v_numItemImageId
   AND I.numDomainID = v_numDomainID;
END; $$;












