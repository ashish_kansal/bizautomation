-- Function definition script fn_GetVendorTransitCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetVendorTransitCount(v_numVendorID NUMERIC,
	v_numAddressID NUMERIC,
	v_numDomainID NUMERIC)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_VendorTransitCount  INTEGER;
BEGIN
   select   COUNT(*) INTO v_VendorTransitCount FROM
   OpportunityMaster Opp
   INNER JOIN
   OpportunityItems OppI
   ON
   Opp.numOppId = OppI.numOppId WHERE
   Opp.numDomainId = v_numDomainID
   AND Opp.numDivisionId = v_numVendorID
   AND Opp.tintopptype = 2
   AND Opp.tintoppstatus = 1
   AND Opp.tintshipped = 0
   AND OppI.numVendorWareHouse = v_numAddressID
   AND coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0) > 0;

   RETURN v_VendorTransitCount;
END; $$;

