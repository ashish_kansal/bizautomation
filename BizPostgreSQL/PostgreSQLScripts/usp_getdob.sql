-- Stored procedure definition script usp_getDOB for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDOB(v_numContactid NUMERIC(9,0) DEFAULT 0 
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT Count(*) FROM AdditionalContactsInformation WHERE numContactId = v_numContactid AND bintDOB <> '1900-01-01 00:00:00.000' AND NOT bintDOB IS NULL;
END; $$;












