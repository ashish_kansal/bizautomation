-- Stored procedure definition script USP_GetPayrollDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPayrollDetail(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numPayrollHeaderID NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtStartDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP;
   v_decTotalHrsWorked  DECIMAL(10,2);
   v_decActualTotalHrsWorked  DECIMAL(10,2);       
   v_decTotalOverTimeHrsWorked  DECIMAL(10,2);
   v_decActualTotalOverTimeHrsWorked  DECIMAL(10,2);    
   v_decTotalHrs  DECIMAL(10,2);
   v_decActualTotalHrs  DECIMAL(10,2);  
   v_decTotalPaidLeaveHrs  DECIMAL(10,2);
   v_decActualTotalPaidLeaveHrs  DECIMAL(10,2);     
   v_bitOverTime  BOOLEAN;
   v_bitOverTimeHrsDailyOrWeekly  BOOLEAN;
   v_decOverTime  DECIMAL;          
   v_monExpense  DECIMAL(20,5);
   v_monReimburse  DECIMAL(20,5);
   v_monCommPaidInvoice  DECIMAL(20,5);
   v_monCommUNPaidInvoice  DECIMAL(20,5); 
   v_monActualExpense  DECIMAL(20,5);
   v_monActualReimburse  DECIMAL(20,5);
   v_monActualCommPaidInvoice  DECIMAL(20,5);
   v_monActualCommUNPaidInvoice  DECIMAL(20,5); 
    
   v_minID  NUMERIC(9,0);
   v_maxID  NUMERIC(9,0);
BEGIN
   open SWV_RefCur for
   SELECT  numPayrollHeaderID,
		numPayrolllReferenceNo,
		numDomainID,
		dtFromDate,
		dtToDate,
		dtPaydate,
		numCreatedBy,
		dtCreatedDate,
		numModifiedBy,
		dtModifiedDate,
		coalesce(numPayrollStatus,0) AS numPayrollStatus
   FROM  PayrollHeader WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numDomainID = v_numDomainId;

   select   dtFromDate, dtToDate INTO v_dtStartDate,v_dtEndDate FROM PayrollHeader WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numDomainID = v_numDomainId;

   DROP TABLE IF EXISTS tt_TEMPHRS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPHRS 
   (
      numPayrollDetailID NUMERIC,
      numCheckStatus NUMERIC,
      numUserId NUMERIC,
      vcUserName VARCHAR(100),
      numUserCntID NUMERIC,
      vcEmployeeId VARCHAR(50),
      monHourlyRate DECIMAL(20,5),
      bitOverTime BOOLEAN,
      bitOverTimeHrsDailyOrWeekly BOOLEAN,
      numLimDailHrs DOUBLE PRECISION,
      monOverTimeRate DECIMAL(20,5),
      decRegularHrs DECIMAL(10,2),
      decActualRegularHrs DECIMAL(10,2),
      decOvertimeHrs DECIMAL(10,2),
      decActualOvertimeHrs DECIMAL(10,2),
      decPaidLeaveHrs DECIMAL(10,2),
      decActualPaidLeaveHrs DECIMAL(10,2),
      decTotalHrs DECIMAL(10,2),
      decActualTotalHrs DECIMAL(10,2),
      monExpense DECIMAL(20,5),
      monActualExpense DECIMAL(20,5),
      monReimburse DECIMAL(20,5),
      monActualReimburse DECIMAL(20,5),
      monCommPaidInvoice DECIMAL(20,5),
      monActualCommPaidInvoice DECIMAL(20,5),
      monCommUNPaidInvoice DECIMAL(20,5),
      monActualCommUNPaidInvoice DECIMAL(20,5),
      monDeductions DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPHRS(numPayrollDetailID,numCheckStatus,numUserId,vcUserName,numUserCntID,vcEmployeeId,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,monDeductions)
   SELECT coalesce(PD.numPayrollDetailID,0) AS numPayrollDetailID,coalesce(PD.numCheckStatus,0) AS numCheckStatus,UM.numUserId,CAST(coalesce(ADC.vcFirstName || ' ' || ADC.vcLastname,'-') AS VARCHAR(100)) AS vcUserName,
           ADC.numContactId AS numUserCntID,coalesce(UM.vcEmployeeId,'') AS vcEmployeeId,
 CASE WHEN coalesce(PD.numPayrollDetailID,0) > 0 THEN coalesce(PD.monHourlyRate,0) ELSE coalesce(UM.monHourlyRate,0) END AS monHourlyRate,
 CASE WHEN coalesce(PD.numPayrollDetailID,0) > 0 THEN coalesce(PD.bitOverTime,false) ELSE coalesce(UM.bitOverTime,false) END AS bitOverTime,
 CASE WHEN coalesce(PD.numPayrollDetailID,0) > 0 THEN coalesce(PD.bitOverTimeHrsDailyOrWeekly,false) ELSE coalesce(UM.bitOverTimeHrsDailyOrWeekly,false) END AS bitOverTimeHrsDailyOrWeekly,
 CASE WHEN coalesce(PD.numPayrollDetailID,0) > 0 THEN coalesce(PD.numLimDailHrs,0) ELSE coalesce(UM.numLimDailHrs,0) END AS numLimDailHrs,
 CASE WHEN coalesce(PD.numPayrollDetailID,0) > 0 THEN coalesce(PD.monOverTimeRate,0) ELSE coalesce(UM.monOverTimeRate,0) END AS monOverTimeRate,coalesce(PD.monDeductions,0) AS monDeductions
   FROM  UserMaster UM JOIN AdditionalContactsInformation ADC ON ADC.numContactId = UM.numUserDetailId
   LEFT JOIN PayrollDetail PD ON PD.numUserCntID = ADC.numContactId AND PD.numPayrollHeaderID = v_numPayrollHeaderID
   WHERE UM.numDomainID = v_numDomainId; 

	
   DROP TABLE IF EXISTS tt_TEMPPAYROLLTRACKING CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPAYROLLTRACKING AS
      SELECT * FROM PayrollTracking WHERE numDomainID = v_numDomainId;

   select   max(numUserCntID), min(numUserCntID) INTO v_maxID,v_minID FROM tt_TEMPHRS;

   WHILE v_minID <= v_maxID LOOP
      v_decTotalHrsWorked := 0;
      v_decTotalOverTimeHrsWorked := 0;
      v_decTotalHrs := 0;
      v_decTotalPaidLeaveHrs := 0;
      v_monExpense := 0;
      v_monReimburse := 0;
      v_monCommPaidInvoice := 0;
      v_monCommUNPaidInvoice := 0;
      v_decActualTotalHrsWorked := 0;
      v_decActualTotalOverTimeHrsWorked := 0;
      v_decActualTotalHrs := 0;
      v_decActualTotalPaidLeaveHrs := 0;
      v_monActualExpense := 0;
      v_monActualReimburse := 0;
      v_monActualCommPaidInvoice := 0;
      v_monActualCommUNPaidInvoice := 0;
      select   bitOverTime, bitOverTimeHrsDailyOrWeekly, numLimDailHrs INTO v_bitOverTime,v_bitOverTimeHrsDailyOrWeekly,v_decOverTime FROM tt_TEMPHRS WHERE numUserCntID = v_minID;
  
  
  --Regular Hrs
      select   sum(x.Hrs) INTO v_decTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
         from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
         And
         numUserCntID = v_minID  And numDomainID = v_numDomainId
         AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID != v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0)) x;           
  
  --Actual Regular Hrs
      select   sum(x.Hrs) INTO v_decActualTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
         from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
         And
         numUserCntID = v_minID  And numDomainID = v_numDomainId
         AND numCategoryHDRID IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID = v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0)) x;           

 --OrverTime Hrs         
      if v_bitOverTime = true  AND v_bitOverTimeHrsDailyOrWeekly = true AND v_decTotalHrsWorked > v_decOverTime then
 
         v_decTotalOverTimeHrsWorked := v_decTotalHrsWorked -v_decOverTime;
      end if;
      if v_bitOverTime = true  AND v_bitOverTimeHrsDailyOrWeekly = true AND v_decActualTotalHrsWorked > v_decOverTime then
 
         v_decActualTotalOverTimeHrsWorked := v_decActualTotalHrsWorked -v_decOverTime;
      end if;
    
 --Paid Leave Hrs
      select   coalesce(SUM(CASE WHEN dtFromDate = dtToDate THEN 24 -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END)
      ELSE(DATE_PART('day',dtToDate:: timestamp -dtFromDate:: timestamp)+1)*24
         -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END)
         -(CASE WHEN bittofullday = false THEN 12 ELSE 0 END) END),0) INTO v_decTotalPaidLeaveHrs FROM   timeandexpense WHERE  numtype = 3 AND numCategory = 3 AND numusercntid = v_minID AND numDomainID = v_numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And @dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID != v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);
 
 --Actual Paid Leave Hrs
      select   coalesce(SUM(CASE WHEN dtFromDate = dtToDate THEN 24 -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END)
      ELSE(DATE_PART('day',dtToDate:: timestamp -dtFromDate:: timestamp)+1)*24
         -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END)
         -(CASE WHEN bittofullday = false THEN 12 ELSE 0 END) END),0) INTO v_decActualTotalPaidLeaveHrs FROM   timeandexpense WHERE  numtype = 3 AND numCategory = 3 AND numusercntid = v_minID AND numDomainID = v_numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
      AND numCategoryHDRID IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID = v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);

 --Expenses
      select   coalesce((Sum(Cast(monAmount as DOUBLE PRECISION))),0) INTO v_monExpense from timeandexpense Where numCategory = 2 And numtype in(1,2)
      And numUserCntID = v_minID And numDomainID = v_numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID != v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);    

 --Actual Expenses
      select   coalesce((Sum(Cast(monAmount as DOUBLE PRECISION))),0) INTO v_monActualExpense from timeandexpense Where numCategory = 2 And numtype in(1,2)
      And numUserCntID = v_minID And numDomainID = v_numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
      AND numCategoryHDRID IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID = v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);    


 --Reimbursable Expenses
      select   coalesce((SUM(CAST(monAmount AS DOUBLE PRECISION))),0) INTO v_monReimburse FROM   timeandexpense WHERE  bitReimburse = true AND numCategory = 2
      AND numusercntid = v_minID AND numDomainID = v_numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID != v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);

 --Actual Reimbursable Expenses
      select   coalesce((SUM(CAST(monAmount AS DOUBLE PRECISION))),0) INTO v_monActualReimburse FROM   timeandexpense WHERE  bitReimburse = true AND numCategory = 2
      AND numusercntid = v_minID AND numDomainID = v_numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
      AND numCategoryHDRID IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE numPayrollHeaderID = v_numPayrollHeaderID AND coalesce(numCategoryHDRID,0) > 0);

--LOGIC OF COMMISSION TYPE 3 (PROEJCT GROSS PROFIT) IS REMOVED BECAUSE THIS OPTION IS REMOVED FROM GLOBAL SETTINGS 

	--Commission Paid Invoice 
      select   coalesce(sum(BC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) INTO v_monCommPaidInvoice FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      LEFT JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID AND PT.numPayrollHeaderID != v_numPayrollHeaderID where Opp.numDomainId = v_numDomainId and
      BC.numUserCntID = v_minID AND BC.bitCommisionPaid = false
      AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount;
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))


--Actual Commission Paid Invoice 
      select   coalesce(sum(PT.monCommissionAmt),0) INTO v_monActualCommPaidInvoice FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID AND PT.numPayrollHeaderID = v_numPayrollHeaderID where Opp.numDomainId = v_numDomainId and
      BC.numUserCntID = v_minID AND BC.bitCommisionPaid = false
      AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount;
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
   

--Commission UnPaid Invoice 
      select   coalesce(sum(BC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) INTO v_monCommUNPaidInvoice FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      LEFT JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID AND PT.numPayrollHeaderID != v_numPayrollHeaderID where Opp.numDomainId = v_numDomainId and
      BC.numUserCntID = v_minID AND BC.bitCommisionPaid = false
      AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount;
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate)) 
    
	   
--Actual Commission UnPaid Invoice 
      select   coalesce(sum(PT.monCommissionAmt),0) INTO v_monActualCommUNPaidInvoice FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID AND PT.numPayrollHeaderID = v_numPayrollHeaderID where Opp.numDomainId = v_numDomainId and
      BC.numUserCntID = v_minID AND BC.bitCommisionPaid = false
      AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount;
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))                                   

                                           
      UPDATE tt_TEMPHRS SET    decRegularHrs = v_decTotalHrsWorked -v_decTotalOverTimeHrsWorked,decOvertimeHrs = v_decTotalOverTimeHrsWorked,
      decTotalHrs = v_decTotalHrsWorked,decPaidLeaveHrs = v_decTotalPaidLeaveHrs,
      monExpense = v_monExpense,monReimburse = v_monReimburse,
      monCommPaidInvoice = v_monCommPaidInvoice,monCommUNPaidInvoice = v_monCommUNPaidInvoice,
      decActualRegularHrs = coalesce(v_decActualTotalHrsWorked,0) -coalesce(v_decActualTotalOverTimeHrsWorked,0),decActualOvertimeHrs = coalesce(v_decActualTotalOverTimeHrsWorked,0),
      decActualTotalHrs = coalesce(v_decActualTotalHrsWorked,0),
      decActualPaidLeaveHrs = coalesce(v_decActualTotalPaidLeaveHrs,0),
      monActualExpense = coalesce(v_monActualExpense,0),
      monActualReimburse = coalesce(v_monActualReimburse,0),
      monActualCommPaidInvoice = coalesce(v_monActualCommPaidInvoice,0),
      monActualCommUNPaidInvoice = coalesce(v_monActualCommUNPaidInvoice,0)
      WHERE numUserCntID = v_minID;
      select   min(numUserCntID) INTO v_minID FROM tt_TEMPHRS WHERE numUserCntID > v_minID;
   END LOOP;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPHRS;
   DROP TABLE IF EXISTS tt_TEMPPAYROLLTRACKING CASCADE;


   open SWV_RefCur3 for
   SELECT MAX(numPayrolllReferenceNo)+1 AS numMaxPayrolllReferenceNo FROM PayrollHeader WHERE numDomainID = v_numDomainId;
   RETURN;
END; $$;


