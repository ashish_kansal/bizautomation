-- Stored procedure definition script USP_GetAcntTypeListDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAcntTypeListDetails(v_numListItemId NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(vcData as VARCHAR(255)) from Listdetails Where numListItemID = v_numListItemId; --And numDomainID=@numDomainID    
END; $$;












