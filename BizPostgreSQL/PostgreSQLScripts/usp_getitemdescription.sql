CREATE OR REPLACE FUNCTION usp_GetItemDescription(   
--
v_numItemcode NUMERIC,
	v_numFlg NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemDesc  VARCHAR(250);
   v_charItemType  CHAR(1);
BEGIN
   if v_numFlg = 0 then
		
      select   txtItemDesc INTO v_vcItemDesc from Item where numItemCode = v_numItemcode;
      IF v_vcItemDesc is not null then
         open SWV_RefCur for
         SELECT v_vcItemDesc;
      end if;
   ELSE
      select   charItemType INTO v_charItemType from Item where numItemCode = v_numItemcode;
      IF v_charItemType is not null then
         open SWV_RefCur for
         SELECT v_charItemType;
      end if;
   end if;
   RETURN;
END; $$;


