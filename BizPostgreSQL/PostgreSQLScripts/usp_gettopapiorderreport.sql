CREATE OR REPLACE FUNCTION USP_GetTopAPIOrderReport(v_numDomainID NUMERIC(9,0),
          v_numWebApiId NUMERIC(9,0),
		v_Mode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_Mode = 0 then

      open SWV_RefCur for
      SELECT  coalesce(vcWebApiOrderReportId,'') as ReportId ,coalesce(tintReportTypeId,0) as tintReportTypeId FROM WebApiOrderReports
      WHERE numDomainID = v_numDomainID AND WebApiId = v_numWebApiId AND tintStatus = 0 LIMIT 1;
   end if;

   IF v_Mode = 1 then

      open SWV_RefCur for
      SELECT  IAO.numImportApiOrderReqId as RequestId,coalesce(IAO.tintRequestType,0) AS tintRequestType,coalesce(IAO.vcApiOrderId,'') AS vcApiOrderId,
coalesce(IAO.dtFromDate,null) as dtFromDate,coalesce(IAO.dtToDate,null) AS dtToDate,coalesce(IAO.dtCreatedDate,null) AS dtCreatedDate,IAO.numOppId,OM.vcMarketplaceOrderReportId,IAO.numOppId
      FROM ImportApiOrder IAO JOIN OpportunityMaster OM on IAO.numOppId = OM.numOppId
      WHERE IAO.numDomainID = v_numDomainID AND IAO.numWebApiId = v_numWebApiId AND IAO.bitIsActive = true
      ORDER BY IAO.numImportApiOrderReqId DESC LIMIT 1;
   end if;
   RETURN;
END; $$; 


