-- Stored procedure definition script USP_BankReconcileMatchRule_GetAll for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BankReconcileMatchRule_GetAll(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(ID as VARCHAR(255))
		,cast(vcName as VARCHAR(255))
		,(CASE WHEN bitMatchAllConditions = true THEN 'YES' ELSE 'NO' END) AS vcMatchAll
		,COALESCE((SELECT string_agg(vcAccountName,', ') FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId IN(SELECT ID FROM SplitIDs(BRMR.vcBankAccounts,','))),'') as vcBankAccounts
		,COALESCE((SELECT string_agg(CONCAT('When <b>',(CASE tintColumn WHEN 1 THEN 'Payee ' WHEN 2 THEN 'Description ' WHEN 3 THEN 'Reference ' END),'</b>',(CASE tintConditionOperator WHEN 1 THEN 'contains ' WHEN 2 THEN 'equals ' WHEN 3 THEN 'starts with ' WHEN 4 THEN 'ends with ' END),
      '<b>',vcTextToMatch,'</b> than select organization <b>',
      (SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDomainID = v_numDomainID AND DivisionMaster.numDivisionID = BRMRC.numDivisionID),'</b>'),'<br/>') FROM BankReconcileMatchRuleCondition BRMRC WHERE numRuleID = BRMR.ID),'') AS vcConditions
		,cast(tintOrder as VARCHAR(255))
   FROM
   BankReconcileMatchRule BRMR
   WHERE
   numDomainID = v_numDomainID
   ORDER BY
   tintOrder;
END; $$;












