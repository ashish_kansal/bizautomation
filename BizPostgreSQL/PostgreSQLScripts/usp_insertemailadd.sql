-- Stored procedure definition script USP_InsertEmailAdd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertEmailAdd(v_vcEmailAdd VARCHAR(2000) DEFAULT '',
v_Type SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_startPos  INTEGER;
   v_EndPos  INTEGER;
   v_EndPos1  INTEGER;
   v_Email  VARCHAR(100);
   v_Name  VARCHAR(100);
BEGIN
   if v_vcEmailAdd <> '' then

      while POSITION(',' IN v_vcEmailAdd) > 0 LOOP
         v_Email := SUBSTR(v_vcEmailAdd,1,POSITION(',' IN v_vcEmailAdd) -1);
         v_startPos := POSITION('<' IN v_Email);
         v_EndPos := POSITION('>' IN v_Email);
         if v_startPos > 0 then
		
			--print substring(@Email,@startPos+1,@EndPos-@startPos-1)
            v_startPos := POSITION('"' IN v_Email);
            v_EndPos1 := POSITION('"' IN SUBSTR(v_Email,v_startPos+1,LENGTH(v_Email)));
         end if;	
		--else  print @Email
		
         v_vcEmailAdd := SUBSTR(v_vcEmailAdd,POSITION(',' IN v_vcEmailAdd)+1,LENGTH(v_vcEmailAdd));
      END LOOP; 
	--print @vcEmailAdd
      v_startPos := POSITION('<' IN v_vcEmailAdd);
      v_EndPos := POSITION('>' IN v_vcEmailAdd);
      if v_startPos > 0 then
		
		--print substring(@vcEmailAdd,@startPos+1,@EndPos-@startPos-1)
         v_startPos := POSITION('"' IN v_vcEmailAdd);
         v_EndPos1 := POSITION('"' IN SUBSTR(v_vcEmailAdd,v_startPos+1,LENGTH(v_vcEmailAdd)));
      end if;
	--else print @vcEmailAdd
      v_vcEmailAdd := SUBSTR(v_vcEmailAdd,POSITION(',' IN v_vcEmailAdd)+1,LENGTH(v_vcEmailAdd));
   end if;
   RETURN;
END; $$;


