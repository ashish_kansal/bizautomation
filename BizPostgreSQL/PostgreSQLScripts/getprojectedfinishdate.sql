-- Function definition script GetProjectedFinishDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectedFinishDate(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numBusinessProcessID NUMERIC(18,0)
	,v_dtProjectedStartDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER)
RETURNS TABLE     
(    
   dtProjectedDate TIMESTAMP,
   monMFGOverhead DECIMAL(20,5),
   monLaborCost DECIMAL(20,5)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtProjectedFinishDate  TIMESTAMP DEFAULT v_dtProjectedStartDate;
   v_monLaborCost  DECIMAL(20,5);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numWorkScheduleID  NUMERIC(18,0);
   v_numTempUserCntID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
BEGIN
   DROP TABLE IF EXISTS tt_GETPROJECTEDFINISHDATE_RESULT;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETPROJECTEDFINISHDATE_RESULT
   (
      dtProjectedDate TIMESTAMP,
      monMFGOverhead DECIMAL(20,5),
      monLaborCost DECIMAL(20,5)
   );
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      numTaskID NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      numUserCntID NUMERIC(18,0),
      intTaskType INTEGER --1:Parallel, 2:Sequential
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TaskAssignee_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TASKASSIGNEE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWorkScheduleID NUMERIC(18,0),
      numUserCntID NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      numProductiveTimeInMinutes NUMERIC(18,0),
      monHourlyRate DECIMAL(20,5),
      tmStartOfDay TIME,
      vcWorkDays VARCHAR(20),
      dtFinishDate TIMESTAMP
   );
   INSERT INTO tt_TEMPTASKS(numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numUserCntID)
   SELECT
   SPDT.numTaskId
		,(coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0)
		,coalesce(SPDT.intTaskType,1)
		,SPDT.numAssignTo
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   SPD.numStageDetailsId = SPDT.numStageDetailsId
   WHERE
   SPD.numdomainid = v_numDomainID
   AND SPD.slp_id = v_numBusinessProcessID
   AND coalesce(SPDT.numWorkOrderId,0) = 0;


   INSERT INTO tt_TASKASSIGNEE(numWorkScheduleID
		,numUserCntID
		,monHourlyRate
		,numProductiveTimeInMinutes
		,tmStartOfDay
		,vcWorkDays)
   SELECT
   WS.ID
		,WS.numUserCntID
		,coalesce(UserMaster.monHourlyRate,0)
		,(coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0)
		,tmStartOfDay
		,coalesce(WS.vcWorkDays,'')
   FROM
   WorkSchedule WS
   INNER JOIN
   UserMaster
   ON
   WS.numUserCntID = UserMaster.numUserDetailId
   WHERE
   WS.numUserCntID IN(SELECT numUserCntID FROM tt_TEMPTASKS);

	UPDATE
		tt_TASKASSIGNEE TA
	SET
		numTaskTimeInMinutes = COALESCE((SELECT
											MAX(TT.numTaskTimeInMinutes) AS numTaskTimeInMinutes
										FROM
											tt_TEMPTASKS TT
										WHERE
											TT.numUserCntID = TA.numUserCntID
											AND intTaskType = 1),0);
		

	UPDATE
		tt_TASKASSIGNEE TA
	SET
		numTaskTimeInMinutes = coalesce(TA.numTaskTimeInMinutes,0)+coalesce((SELECT
			SUm(TT.numTaskTimeInMinutes) AS numTaskTimeInMinutes
		FROM
			tt_TEMPTASKS TT
		WHERE
			TT.numUserCntID = TA.numUserCntID
			AND intTaskType = 2),0);


   select   COUNT(*) INTO v_iCount FROM tt_TASKASSIGNEE;

   WHILE v_i <= v_iCount LOOP
      select   numWorkScheduleID, numTaskTimeInMinutes, numProductiveTimeInMinutes, tmStartOfDay, vcWorkDays, CAST(TO_CHAR(v_dtProjectedStartDate,'yyyymmdd') AS TIMESTAMP)+tmStartOfDay INTO v_numWorkScheduleID,v_numTotalTaskInMinutes,v_numProductiveTimeInMinutes,
      v_tmStartOfDay,v_vcWorkDays,v_dtStartDate FROM
      tt_TASKASSIGNEE WHERE
      ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF EXTRACT(DOW FROM v_dtStartDate)+1 IN(SELECT Id FROM SplitIDs(v_vcWorkDays,',')) AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
					-- IF START DATE IS LESS THEN TODAY DATE
               IF CAST(v_dtStartDate AS DATE) >= CAST(TIMEZONE('UTC',now()) AS DATE) then
					
                  IF CAST(v_dtStartDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE) then
						
							-- CHECK TIME LEFT FOR DAY BASED
                     v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(TIMEZONE('UTC',now()),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -TIMEZONE('UTC',now()))*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(TIMEZONE('UTC',now()),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -TIMEZONE('UTC',now()))*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(TIMEZONE('UTC',now()),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -TIMEZONE('UTC',now())));
                     IF v_numTimeLeftForDay > 0 then
							
                        v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
                     end if;
                  ELSE
                     v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
                  end if;
                  IF v_numTimeLeftForDay > 0 then
						
                     IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
							
                        v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                     ELSE
                        v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
                     end if;
                     v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
                  ELSE
                     v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
                  end if;
               ELSE
                  v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
               end if;
            ELSE
               v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
            end if;
         END LOOP;
      end if;
      UPDATE tt_TASKASSIGNEE SET dtFinishDate = v_dtStartDate WHERE ID = v_i;
      v_i := v_i::bigint+1;
   END LOOP;

	
   IF(SELECT COUNT(*) FROM tt_TASKASSIGNEE) > 0 then
	
      select   MAX(dtFinishDate) INTO v_dtProjectedFinishDate FROM tt_TASKASSIGNEE;
   end if;

   v_monLaborCost := coalesce((SELECT(numTaskTimeInMinutes*(monHourlyRate/60)) FROM tt_TASKASSIGNEE),0);
	
   INSERT INTO tt_GETPROJECTEDFINISHDATE_RESULT(dtProjectedDate,
		monMFGOverhead,
		monLaborCost)
	VALUES(v_dtProjectedFinishDate,
		0,
		v_monLaborCost);

	RETURN QUERY (SELECT * FROM tt_GETPROJECTEDFINISHDATE_RESULT);
END; $$;

