-- Stored procedure definition script USP_GetAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAccounts(v_numAccountID NUMERIC(9,0),
      v_numDomainID NUMERIC(9,0),
      v_numParntAcntTypeId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAccountCode  VARCHAR(50);
BEGIN
   select   vcAccountCode INTO v_vcAccountCode FROM    AccountTypeDetail WHERE   numAccountTypeID = v_numParntAcntTypeId;
				
   IF LENGTH(v_vcAccountCode) > 0 then
            
      IF (v_numAccountID = 0) then
                    
         open SWV_RefCur for
         SELECT DISTINCT Chart_Of_Accounts.numAccountId,
                                Chart_Of_Accounts.vcAccountCode,
                                Chart_Of_Accounts.vcAccountName,
                                Chart_Of_Accounts.vcAccountDescription,
                                Chart_Of_Accounts.numAcntTypeId,
                                Chart_Of_Accounts.numParentAccId
         FROM    Chart_Of_Accounts
         LEFT JOIN Chart_Of_Accounts C ON C.numParentAccId = Chart_Of_Accounts.numAccountId
         WHERE   Chart_Of_Accounts.numDomainId = v_numDomainID
         AND Chart_Of_Accounts.vcAccountCode ilike coalesce(v_vcAccountCode,'')
         || '%'
         AND (Chart_Of_Accounts.numAccountId <> v_numAccountID
         OR coalesce(v_numAccountID,0) = 0);
      ELSE
         open SWV_RefCur for
         SELECT  numAccountId,
                                vcAccountCode,
                                vcAccountName,
                                vcAccountDescription,
                                numAcntTypeId
         FROM    Chart_Of_Accounts
         WHERE   numDomainId = v_numDomainID
         AND vcAccountCode ilike coalesce(v_vcAccountCode,'') || '%'
         AND numAccountId <> v_numAccountID;
      end if;
   end if;
   RETURN;
END; $$;    

