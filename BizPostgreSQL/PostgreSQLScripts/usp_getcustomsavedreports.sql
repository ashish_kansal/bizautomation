-- Stored procedure definition script usp_GetCustomSavedReports for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCustomSavedReports(v_numCustomReportID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from CustomReport where numCustomReportId = v_numCustomReportID;
END; $$;












