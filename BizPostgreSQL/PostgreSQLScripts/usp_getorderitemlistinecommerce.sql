-- Stored procedure definition script USP_GetOrderItemListInEcommerce for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOrderItemListInEcommerce(v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,
 v_CurrentPage INTEGER DEFAULT NULL,
 v_PageSize INTEGER DEFAULT NULL,
 v_UserId NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER DEFAULT 0;
   v_lastRec  INTEGER DEFAULT 0;
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   open SWV_RefCur for
   SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY numOppId) AS Rownumber,* FROM(select
         OI.numOppId,
		OI.numItemCode,
		OI.vcItemName,
		O.vcpOppName,
		FormatedDateFromDate(O.bintCreatedDate,O.numDomainId) AS bintCreatedDate,
		OI.numUnitHour,
		(OI.monPrice*O.fltExchangeRate) AS monPrice,
		(OI.monTotAmtBefDiscount*O.fltExchangeRate) AS monTotAmount,
		O.tintoppstatus,
		O."vcCustomerPO#"
         from
         OpportunityItems AS OI
         LEFT JOIN
         OpportunityMaster AS O ON O.numOppId = OI.numOppId
         WHERE
         O.numDivisionId = v_numDivisionID AND O.tintopptype = 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
         AND O.numContactId = v_UserId) AS K) AS T
   WHERE T.Rownumber > v_firstRec AND T.Rownumber < v_lastRec;

   open SWV_RefCur2 for
   select
   COUNT(OI.numItemCode)
   from
   OpportunityItems AS OI
   LEFT JOIN
   OpportunityMaster AS O ON O.numOppId = OI.numOppId
   WHERE
   O.numDivisionId = v_numDivisionID AND O.tintopptype = 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
   AND O.numContactId = v_UserId;






/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
   RETURN;
END; $$;


