-- Stored procedure definition script USP_ChartCategoryBitFixed for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ChartCategoryBitFixed(v_numAccountId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select bitFixed from Chart_Of_Accounts Where numAccountId = v_numAccountId;
END; $$;












