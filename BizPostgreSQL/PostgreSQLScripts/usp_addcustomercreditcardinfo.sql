-- Stored procedure definition script USP_AddCustomerCreditCardInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddCustomerCreditCardInfo(v_numContactId   NUMERIC(18,0),
               v_vcCardHolder   VARCHAR(500),
               v_vcCreditCardNo VARCHAR(500),
               v_vcCVV2         VARCHAR(200),
               v_numCardTypeID   NUMERIC(9,0),
               v_tintValidMonth SMALLINT,
               v_intValidYear   INTEGER,
               v_numUserCntId   NUMERIC(18,0),
               v_bitIsDefault BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitIsDefault = true then
    
      UPDATE CustomerCreditCardInfo SET bitIsDefault = false
      WHERE numContactId = v_numContactId and CustomerCreditCardInfo.bitIsDefault = true;
   end if;

   IF EXISTS(SELECT * FROM   CustomerCreditCardInfo WHERE  numContactId = v_numContactId
   AND vcCreditCardNo = v_vcCreditCardNo) then
      
      UPDATE CustomerCreditCardInfo
      SET
      vcCardHolder = v_vcCardHolder,vcCreditCardNo = v_vcCreditCardNo,vcCVV2 = v_vcCVV2,
      numCardTypeID = v_numCardTypeID,tintValidMonth = v_tintValidMonth,
      intValidYear = v_intValidYear,numModifiedby = v_numUserCntId,dtModified = TIMEZONE('UTC',now()),
      bitIsDefault = v_bitIsDefault
      WHERE numContactId = v_numContactId
      AND vcCreditCardNo  = v_vcCreditCardNo;
   ELSE
      INSERT INTO CustomerCreditCardInfo(numContactId,
                    vcCardHolder,
                    vcCreditCardNo,
                    vcCVV2,
                    numCardTypeID,
                    tintValidMonth,
                    intValidYear,
                    numCreatedby,
                    dtCreated,
                    numModifiedby,
                    dtModified,
                    bitIsDefault)
        VALUES(v_numContactId,
                    v_vcCardHolder,
                    v_vcCreditCardNo,
                    v_vcCVV2,
                    v_numCardTypeID,
                    v_tintValidMonth,
                    v_intValidYear,
                    v_numUserCntId,
                    TIMEZONE('UTC',now()),
                    v_numUserCntId,
                    TIMEZONE('UTC',now()),
                    v_bitIsDefault);
   end if;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/



