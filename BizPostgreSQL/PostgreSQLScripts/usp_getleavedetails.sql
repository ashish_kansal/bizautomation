-- Stored procedure definition script USP_GetLeaveDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetLeaveDetails(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_FromDate TIMESTAMP DEFAULT NULL,
v_ToDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select DATE_PART('day',dtToDate+INTERVAL '1 day' -dtFromDate:: timestamp) -(case when bitFromFullDay = true then 0 else .5 end) -(case when bitToFullDay = true then 0 else .5 end) as NoOfdays,dtFromDate,dtToDate,
	numType,bitApproved,bitFromFullDay,bitToFullDay,
	txtDesc,A.vcFirstName || ' ' || A.vcLastname as Name,
	case when NULLIF(MGR.vcFirstName,'') is null then '-' else  MGR.vcFirstName || ' ' || MGR.vcLastname end as MGRName,coalesce(U1.numUserId,0) as MGRUserID,numCategoryHDRID
   from timeandexpense T
   join UserMaster U
   on U.numUserDetailId = T.numUserCntID
   join AdditionalContactsInformation A
   on A.numContactId = U.numUserDetailId
   left join AdditionalContactsInformation MGR
   on MGR.numContactId = A.numManagerID
   left join UserMaster U1
   on MGR.numContactId = U1.numUserDetailId
   where numCategory = 3  and (dtFromDate between v_FromDate and v_ToDate or dtToDate between v_FromDate and v_ToDate)
   and T.numUserCntID = v_numUserCntID;
END; $$;
