DROP FUNCTION IF EXISTS USP_ConEmpList;

CREATE OR REPLACE FUNCTION USP_ConEmpList(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_bitPartner BOOLEAN DEFAULT false,    
v_numContactID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitPartner = false then
	
      open SWV_RefCur for
      SELECT
      A.numContactId AS "numContactId"
			,A.vcFirstName || ' ' || A.vcLastname as "vcUserName"
			,coalesce(AGM.tintGroupType,0) AS "tintGroupType"
      FROM
      UserMaster UM
      INNER JOIN
      AuthenticationGroupMaster AGM
      ON
      UM.numGroupID = AGM.numGroupID
      INNER JOIN
      AdditionalContactsInformation A
      ON
      UM.numUserDetailId = A.numContactId
      WHERE
      UM.numDomainID = v_numDomainID
      AND UM.numDomainID = A.numDomainID
      AND UM.intAssociate = 1
	  ORDER BY
		CONCAT(A.vcFirstName,' ',A.vcLastname) ASC;
   end if;    
   IF v_bitPartner = true then
	
      open SWV_RefCur for
      SELECT
      A.numContactId AS "numContactId"
			,A.vcFirstName || ' ' || A.vcLastname as "vcUserName"
			,coalesce(AGM.tintGroupType,0) AS "tintGroupType"
      FROM
      UserMaster UM
      INNER JOIN
      AuthenticationGroupMaster AGM
      ON
      UM.numGroupID = AGM.numGroupID
      INNER JOIN
      AdditionalContactsInformation A
      ON
      UM.numUserDetailId = A.numContactId
      WHERE
      UM.numDomainID = v_numDomainID
	  ORDER BY
		CONCAT(A.vcFirstName,' ',A.vcLastname) ASC;
   end if;
   RETURN;
END; $$;

