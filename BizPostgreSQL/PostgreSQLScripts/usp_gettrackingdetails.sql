-- Stored procedure definition script usp_GetTrackingDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetTrackingDetails(v_numDomainId NUMERIC(9,0), 
 v_numWebApiId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT AOID.WebApiOrderItemId ,AOID.numDomainID,AOID.numWebApiId,AOID.numOppId,OBI.numItemCode,OBI.numOppBizDocItemID,
		AOID.numOppItemId,vcapiOppId,vcApiOppItemId,OB.numOppBizDocsId,OB.vcTrackingNo ,OB.monShipCost,OB.vcShippingMethod,LD.vcData AS ShippingCarrier,
		(SELECT  vcAPIItemID FROM ItemAPI
      WHERE numItemID = OBI.numItemCode
      AND WebApiId = AOID.numWebApiId
      AND 1 =(CASE coalesce(vcSKU,'') WHEN(SELECT vcSKU FROM Item WHERE numItemCode = numItemID AND numDomainID = v_numDomainId) THEN 1
      WHEN(SELECT vcWHSKU FROM WareHouseItems WI WHERE OBI.numWarehouseItmsID  = WI.numWareHouseItemID) THEN 1
      ELSE 0
      END) LIMIT 1) AS vcAPIItemId,
		(SELECT COUNT(*) FROM OpportunityBizDocItems WHERE numOppBizDocID = OB.numOppBizDocsId AND tintTrackingStatus = 1) AS numTrackingNoUpdated,OBI.numOppBizDocID
   FROM WebApiOppItemDetails AOID
   INNER JOIN OpportunityBizDocs OB ON AOID.numOppId  = OB.numoppid AND OB.bitAuthoritativeBizDocs = 1
   INNER JOIN OpportunityBizDocItems OBI  ON OBI.numOppBizDocID = OB.numOppBizDocsId
   INNER JOIN Listdetails LD ON LD.numListItemID = OB.numShipVia
   WHERE  AOID.numWebApiId = v_numWebApiId AND AOID.numDomainID = v_numDomainId
   AND LENGTH(AOID.vcAPIOppId) > 2 AND LENGTH(OB.vcTrackingNo) > 1
   AND (OBI.tintTrackingStatus = 0 or OBI.tintTrackingStatus IS NULL);
END; $$;












