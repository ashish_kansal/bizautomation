-- Stored procedure definition script USP_GetTimeAndExpenseDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeAndExpenseDetail(v_tintMode SMALLINT DEFAULT 0,
    v_numProId NUMERIC(9,0) DEFAULT NULL,
    v_numStageID NUMERIC(9,0) DEFAULT NULL,
    v_numDivisionID NUMERIC(9,0) DEFAULT NULL,
    v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      open SWV_RefCur for SELECT  TE.numCategoryHDRID,
                    CAST('Billable Time' AS CHAR(13)) AS TimeType,
                    cast(I.vcItemName as VARCHAR(255)),
                    OM.vcpOppName,
                    CASE numType
      WHEN 1 THEN coalesce(OPP.numUnitHour,0)
      ELSE coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
      END AS numUnitHour,
                    coalesce(OPP.monPrice,0) AS monPrice,
                    FormatedDateFromDate(TE.dtTCreatedOn,v_numDomainID) AS dtDateEntered,
                    fn_GetContactName(TE.numUserCntID) AS ContactName,TE.numOppId
      FROM    timeandexpense TE
      INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
      INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
      AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectID = TE.numProId
      INNER JOIN Item I ON OPP.numItemCode = I.numItemCode
      WHERE   numProId = v_numProId
      AND TE.numStageId = v_numStageID
      AND TE.numDivisionID = v_numDivisionID
--                    AND TE.numUserCntID = @numUserCntID
      AND TE.numDomainID = v_numDomainID
      AND numType = 1 --Billable
      UNION
      SELECT  TE.numCategoryHDRID ,
                    'Non-Billable Time' AS TimeType,
                    coalesce(SUBSTR(CAST(txtDesc AS VARCHAR(25)),1,25) || '..','') AS vcItemName,
                    ''  AS vcBizDocID,
                    coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
      AS numUnitHour,
                    0 AS monPrice,
                    FormatedDateFromDate(TE.dtTCreatedOn,v_numDomainID) AS dtDateEntered,
                    fn_GetContactName(TE.numUserCntID) AS ContactName,0 as numOppId
      FROM    timeandexpense TE
      WHERE   numProId = v_numProId
      AND TE.numStageId = v_numStageID
      AND TE.numDivisionID = v_numDivisionID
--                    AND TE.numUserCntID = @numUserCntID
      AND TE.numDomainID = v_numDomainID
      AND numtype = 2;
   end if;
END; $$;	













