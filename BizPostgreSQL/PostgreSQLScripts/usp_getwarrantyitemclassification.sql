-- Stored procedure definition script USP_GetWarrantyItemClassification for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWarrantyItemClassification(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   C.vcItemClassification as "vcItemClassification"
   FROM
   Contracts AS C
   LEFT JOIN
   DivisionMaster AS D
   ON
   C.numDivisonId = D.numDivisionID
   LEFT JOIN
   CompanyInfo AS CI
   ON
   D.numCompanyID = CI.numCompanyId
   WHERE
   C.numDomainId = v_numDomainID AND
   C.intType = 2;
END; $$;













