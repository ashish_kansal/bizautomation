-- Stored procedure definition script USP_MANAGE_IMPORT_FILE_FIELD_MAPPING for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MANAGE_IMPORT_FILE_FIELD_MAPPING(v_numImportFileID			BIGINT,
  v_numFormFieldID			NUMERIC,
  v_intMapColumnNo			INTEGER,
  v_bitCustomField			BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO Import_File_Field_Mapping(intImportFileID
					,numFormFieldID
					,intMapColumnNo
					,bitCustomField)
			VALUES(v_numImportFileID
					,v_numFormFieldID
					,v_intMapColumnNo
					,v_bitCustomField);
RETURN;
END; $$;


