-- Stored procedure definition script USP_GetNewsList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetNewsList(v_numNewsID NUMERIC(9,0) DEFAULT 0,    
v_bitActive BOOLEAN DEFAULT false,    
v_vcHeading VARCHAR(200) DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numNewsID = 0 then

      open SWV_RefCur for
      select numNewsID,vcNewsHeading,vcDate,
vcDesc,vcImagePath,vcURL,vcCreatedDate,
vcModifieddate,case when tintType = 1 then 'News Coverage' when tintType = 2 then 'Press Releases' when tintType = 3 then 'White papers' end as type,
case when bitActive = true then 'Active' when bitActive = false then 'Inactive' end as Active
      from news where bitActive = v_bitActive and vcNewsHeading ilike '%' || coalesce(v_vcHeading,'') || '%' and tintType = 1
      order by numNewsID desc;

      open SWV_RefCur2 for
      select numNewsID,vcNewsHeading,vcDate,
vcDesc,vcImagePath,vcURL,vcCreatedDate,
vcModifieddate,case when tintType = 1 then 'News Coverage' when tintType = 2 then 'Press Releases' when tintType = 3 then 'White papers' end as type,
case when bitActive = true then 'Active' when bitActive = false then 'Inactive' end as Active
      from news where bitActive = v_bitActive and vcNewsHeading ilike '%' || coalesce(v_vcHeading,'') || '%'  and tintType = 2
      order by numNewsID desc;

      open SWV_RefCur3 for
      select numNewsID,vcNewsHeading,vcDate,
vcDesc,vcImagePath,vcURL,vcCreatedDate,
vcModifieddate,case when tintType = 1 then 'News Coverage' when tintType = 2 then 'Press Releases' when tintType = 3 then 'White papers' end as type,
case when bitActive = true then 'Active' when bitActive = false then 'Inactive' end as Active
      from news where bitActive = v_bitActive and vcNewsHeading ilike '%' || coalesce(v_vcHeading,'') || '%' and tintType = 3
      order by numNewsID desc;
   end if;    

   if v_numNewsID > 0 then

      open SWV_RefCur for
      select numNewsID,vcNewsHeading,vcDate,
vcDesc,vcImagePath,vcURL,vcCreatedDate,
vcModifieddate,bitActive,tintType
      from news where numNewsID = v_numNewsID;
   end if;
   RETURN;
END; $$;


