-- Stored procedure definition script USP_CompanyList2 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CompanyList2(v_numRelationType NUMERIC(9,0),                                       
v_numUserCntID NUMERIC(9,0),                                          
v_tintUserRightType SMALLINT,                                              
v_SortChar CHAR(1) DEFAULT '0',                                         
v_FirstName VARCHAR(100) DEFAULT '',                                          
v_LastName VARCHAR(100) DEFAULT '',                                          
v_CustName VARCHAR(100) DEFAULT '',                                        
v_CurrentPage INTEGER DEFAULT NULL,                                        
v_PageSize INTEGER DEFAULT NULL,                                        
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                        
v_columnName VARCHAR(50) DEFAULT NULL,                                        
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                
v_tintSortOrder NUMERIC DEFAULT 0,                                 
v_numProfile NUMERIC(9,0) DEFAULT 0,                        
v_numDomainID NUMERIC(9,0) DEFAULT 0,                
v_tintCRMType SMALLINT DEFAULT NULL,                
v_bitPartner BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_join  VARCHAR(400);  
   v_firstRec  INTEGER;                                                    
   v_lastRec  INTEGER;                                                    
   v_column  VARCHAR(50);      
   v_lookbckTable  VARCHAR(50);      
   v_strSql  VARCHAR(5000);                                      
   v_tintOrder  SMALLINT;                                            
   v_vcFormFieldName  VARCHAR(50);                                            
   v_vcListItemType  VARCHAR(1);                                       
   v_vcListItemType1  VARCHAR(1);                                           
   v_vcAssociatedControlType  VARCHAR(10);                                            
   v_numListID  NUMERIC(9,0);                                            
   v_vcDbColumnName  VARCHAR(20);                
   v_WhereCondition  VARCHAR(2000);                 
   v_Table  VARCHAR(2000);          
   v_bitCustom  BOOLEAN;  
   v_numFormFieldId  NUMERIC;  
            
   v_Nocolumns  SMALLINT;     
   v_DefaultNocolumns  SMALLINT;          
   v_strColumns  VARCHAR(2000);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_join := ''; 
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                    
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                              
   v_column := v_columnName;      
   v_lookbckTable := '';  
      
   if v_column ilike 'Cust%' then

      v_join := ' left Join CFW_FLD_Values CFW on CFW.RecId=Dm.numDivisionId  and CFW.fld_id= ' || replace(v_column,'Cust','') || ' ';
      v_column := 'CFW.Fld_Value';
   end if;                         
   if v_columnName ilike 'DCust%' then

      v_join := ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=Dm.numDivisionId   and CFW.fld_id= ' || replace(v_column,'DCust','');
      v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  ';
      v_column := 'LstCF.vcData';
   end if;                                       
   if v_column not ilike 'Cust%' or  v_column not ilike 'Cust%' then

      select   vcLookBackTableName INTO v_lookbckTable from DynamicFormFieldMaster D where D.numFormId = 11 and vcDbColumnName = v_columnName;
      if v_lookbckTable <> '' then
	 
         if v_lookbckTable = 'Contacts' then
            v_column := 'ADC.' || coalesce(v_column,'');
         end if;
         if v_lookbckTable = 'Division' then
            v_column := 'DM.' || coalesce(v_column,'');
         end if;
      end if;
   end if;                                       
                                        
   v_strSql := 'with tblCompany as (SELECT   ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_column,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,                                           
     DM.numDivisionID                              
    FROM  CompanyInfo CMP                                        
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID';                                
                                
   if v_tintSortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactid=DM.numDivisionID ';
   end if;                                
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0               
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;              
   v_strSql := coalesce(v_strSql,'') || ' join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                        
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                         
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                         
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1                                         
    AND (DM.bitPublicFlag=0 OR DM.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')                                              
    AND DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                                        
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                         
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                                        
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                   
   if v_numRelationType <> cast(NULLIF('0','') as NUMERIC(9,0)) then 
      v_strSql := coalesce(v_strSql,'') || ' AND CMP.numCompanyType = ' || SUBSTR(CAST(v_numRelationType AS VARCHAR(15)),1,15) || '';
   end if;                                        
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CMP.vcCompanyName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                         
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND ((DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( CA.bitShareportal=1 and CA.bitDeleted=0))' else ')' end;
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;         
   if v_tintSortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and cType=''O''';
   end if;                                
   if v_numProfile > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  vcProfile=' || SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
   end if;                                
   if v_tintCRMType > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  DM.tintCRMType=' || SUBSTR(CAST(v_tintCRMType AS VARCHAR(1)),1,1);
   end if;                                  
                                      
                                      
                                        
   v_strSql := coalesce(v_strSql,'') || ')';          
                                                                     
   v_tintOrder := 0;                                            
   v_WhereCondition := '';           
             
   v_Nocolumns := 0;          
   select   coalesce(count(*),0) INTO v_Nocolumns from InitialListColumnConf A where A.numFormId = 11 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID  and numtype = v_numRelationType;              
   v_DefaultNocolumns :=  v_Nocolumns;           
   v_strColumns := '';          
   while v_DefaultNocolumns > 0 LOOP
      v_strColumns := coalesce(v_strColumns,'') || ',null';
      v_DefaultNocolumns := v_DefaultNocolumns -1;
   END LOOP;      
   v_strSql := coalesce(v_strSql,'') || ' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber';           
   if v_Nocolumns > 0 then

      select   tintOrder+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, a.bitCustom, a.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from InitialListColumnConf a
      join DynamicFormFieldMaster D
      on D.numFormFieldId = a.numFormFieldId where a.numFormId = 11 and numUserCntID = v_numUserCntID and a.numDomainID = v_numDomainID  and numtype = v_numRelationType   order by tintOrder asc LIMIT 1;
   Else
      select   coalesce(Count(*),0) INTO v_DefaultNocolumns from  DynamicFormFieldMaster D where D.numFormId = 11   and bitDefault = 1;
      while v_DefaultNocolumns > 0 LOOP
         v_strColumns := coalesce(v_strColumns,'') || ',null';
         v_DefaultNocolumns := v_DefaultNocolumns -1;
      END LOOP;
      select   "Order"+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, 0, numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from  DynamicFormFieldMaster D where D.numFormId = 11   and bitDefault = 1   order by "Order" asc LIMIT 1;
   end if;    
   v_DefaultNocolumns :=  v_Nocolumns;                                          
   while v_tintOrder > 0 LOOP
      if v_bitCustom = false then
	
         if v_vcAssociatedControlType = 'SelectBox' then
				
            if v_vcListItemType = 'L' then
		  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
		  
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
		  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
               if v_Table = 'Contacts' then
                  v_Prefix := 'ADC.';
               end if;
               if v_Table = 'Division' then
                  v_Prefix := 'DM.';
               end if;
               v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            end if;
         else 
            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'year(getutcdate())-year(bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
         end if;
      end if;
      if v_bitCustom = true then

         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_Id AS VARCHAR(10)),1,10) INTO v_vcFormFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master join CFW_Fld_Dtl
         on Fld_id = numFieldId
         left join CFw_Grp_Master
         on subgrp = CFw_Grp_Master.Grp_id where CFW_Fld_Master.Grp_id = 1 and numRelation = v_numRelationType and CFW_Fld_Master.numDomainID = v_numDomainID  and CFW_Fld_Master.Fld_id = v_numFormFieldId;
         RAISE NOTICE '%',v_vcAssociatedControlType;
         if v_vcAssociatedControlType <> 'SelectBox' then
			
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Dm.numDivisionId   ';
         end if;
         if v_vcAssociatedControlType = 'SelectBox' then
			
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' 
					on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Dm.numDivisionId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         end if;
      end if;
      if v_DefaultNocolumns > 0 then
 
         select   tintOrder+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, a.bitCustom, a.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
         v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from InitialListColumnConf a
         join DynamicFormFieldMaster D
         on D.numFormFieldId = a.numFormFieldId where a.numFormId = 11 and numUserCntID = v_numUserCntID and a.numDomainID = v_numDomainID  and numtype = v_numRelationType and
         tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      else
         select   "Order"+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, 0, numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
         v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from DynamicFormFieldMaster D where D.numFormId = 11 and bitDefault = 1  and
         "Order" > v_tintOrder -1   order by "Order" asc LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      end if;
   END LOOP;                 
              
                        
   v_strSql := coalesce(v_strSql,'') || ' From CompanyInfo CMP                                                    
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                          
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
   ' || coalesce(v_WhereCondition,'') ||
   ' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                              
  WHERE RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) ||
   ' union select count(*),null,null,null,null,null ' || coalesce(v_strColumns,'') || ' from tblCompany order by RowNumber';                                     
                                         
   RAISE NOTICE '%',v_strSql;                  
          
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;                                                  
                                                     
                              
                              
                              
--SELECT  CMP.vcCompanyName + ' - <I>' + DM.vcDivisionName + '</I>' as CompanyName,                                           
--     DM.numTerID,                                          
--     ADC.vcFirstName  + ' ' +ADC.vcLastName as PrimaryContact,                                          
--     case when ADC.numPhone<>'' then + ADC.numPhone +case when ADC.numPhoneExtension<>'' then ' - ' + ADC.numPhoneExtension else '' end  else '' end as [Phone],                                       
--     ADC.vcEmail As vcEmail,                                          
--     CMP.numCompanyID AS numCompanyID,                                          
--     DM.numDivisionID As numDivisionID,                                
--     ADC.numContactID AS numContactID,                                           
--     CMP.numCompanyRating AS numCompanyRating,                                           
--     LD.vcData as vcRating,                                            
--     DM.numStatusID AS numStatusID,                                           
--     LD1.vcData as Follow,                                          
--     DM.numCreatedby AS numCreatedby, DM.numRecOwner , DM.tintCRMType,                                     
--     case when DM.tintCRMType=1 then 'Prospects' when DM.tintCRMType=2 then 'Accounts' end as AccountType                                      
--    FROM  CompanyInfo CMP                                        
--    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                              
--    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                        
--    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                         
--    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                         
--    join #tempTable T on T.numDivisionID=DM.numDivisionID                                           
--    WHERE ADC.numContacttype=70 and ID > @firstRec and ID < @lastRec order by ID                       
--drop table #tempTable


