CREATE OR REPLACE FUNCTION USP_ManageShippingForECom(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numRelationShipID NUMERIC DEFAULT NULL,
v_numProfileID NUMERIC DEFAULT NULL,
v_strItems TEXT DEFAULT NULL,
v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 1 then --insert
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then

         DELETE FROM ShippingProviderForEComerce WHERE numDomainID = v_numDomainID AND numRelationshipID = v_numRelationShipID AND numProfileID = v_numProfileID;
         INSERT INTO ShippingProviderForEComerce(numRelationshipID,
					numProfileID,
					numShippingCompanyID,
					numDomainID)
         SELECT
         X.numRelationshipID,
						 X.numProfileID,
						 X.numShippingCompanyID,
						 v_numDomainID
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numRelationshipID NUMERIC(9,0) PATH 'numRelationshipID',
					numProfileID NUMERIC PATH 'numProfileID',
					numShippingCompanyID NUMERIC PATH 'numShippingCompanyID'
			) AS X;

      end if;
      open SWV_RefCur for
      SELECT 0;
   end if;
   IF v_tintMode = 0 then --select
		
      open SWV_RefCur for
      SELECT   numRelationshipID,
					numProfileID,
					numShippingCompanyID
      FROM     ShippingProviderForEComerce
      WHERE    numDomainID = v_numDomainID
      AND  numRelationshipID = v_numRelationShipID
      AND  numProfileID = v_numProfileID;
   end if;
   RETURN;
END; $$;



