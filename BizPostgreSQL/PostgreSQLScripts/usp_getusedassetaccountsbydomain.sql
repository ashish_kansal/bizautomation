-- Stored procedure definition script USP_GetUsedAssetAccountsByDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUsedAssetAccountsByDomain(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   open SWV_RefCur for SELECT
   DISTINCT numAssetChartAcntId as "numAssetChartAcntId"
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND coalesce(numAssetChartAcntId,0) > 0;
END; $$;












