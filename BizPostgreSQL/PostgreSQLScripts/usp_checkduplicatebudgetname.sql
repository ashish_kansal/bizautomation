-- Stored procedure definition script USP_CheckDuplicateBudgetName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckDuplicateBudgetName(v_numBudgetId NUMERIC(9,0) DEFAULT 0,        
v_vcBudgetName VARCHAR(100) DEFAULT '',  
v_numDivisionId NUMERIC(9,0) DEFAULT 0,
v_numDepartmentId NUMERIC(9,0) DEFAULT 0,
v_numCostCenterId NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select Count(*) From OperationBudgetMaster Where numDivisionId = v_numDivisionId And numDepartmentId = v_numDepartmentId
   And numCostCenterId = v_numCostCenterId
   And numBudgetId <> v_numBudgetId And numDomainId = v_numDomainId;
END; $$;












