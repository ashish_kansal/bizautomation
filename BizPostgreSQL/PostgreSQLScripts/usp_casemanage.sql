-- Stored procedure definition script USP_CaseManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CaseManage(INOUT v_numCaseId NUMERIC(9,0) DEFAULT 0 ,                      
v_numDivisionID NUMERIC(9,0) DEFAULT null,                      
v_numContactId NUMERIC(9,0) DEFAULT null,                      
v_vcCaseNumber VARCHAR(50) DEFAULT '',                      
v_textSubject VARCHAR(2000) DEFAULT '',                      
v_textDesc TEXT DEFAULT '',                      
v_numUserCntID NUMERIC(9,0) DEFAULT null,                                  
v_numDomainID NUMERIC(9,0) DEFAULT null,                      
v_tintSupportKeyType SMALLINT DEFAULT NULL,                      
v_intTargetResolveDate TIMESTAMP DEFAULT NULL,                      
v_numStatus NUMERIC(9,0) DEFAULT null,                      
v_numPriority NUMERIC(9,0) DEFAULT null,                      
v_textInternalComments TEXT DEFAULT '',                      
v_numReason NUMERIC(9,0) DEFAULT null,                      
v_numOrigin  NUMERIC(9,0) DEFAULT null,                      
v_numType NUMERIC(9,0) DEFAULT null,                      
v_numAssignedTo NUMERIC(9,0) DEFAULT 0,              
v_numContractId NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tempAssignedTo  NUMERIC(9,0);
   v_tintPageID  SMALLINT;         
        
        
          
 ---Updating if Case is assigned to someone              
   v_update  BOOLEAN;
   v_Usedincidents  NUMERIC(9,0);
   v_Availableincidents  NUMERIC(9,0);
BEGIN
   if v_intTargetResolveDate = '1753-01-01 12:00:00 AM' then 
      v_intTargetResolveDate := null;
   end if;                        
   if v_numCaseId = 0 then
 --  tinyint
	               
      insert into Cases(numDivisionID,
 numContactId,
 vcCaseNumber,
 textSubject,
 textDesc,
 numCreatedby,
 bintCreatedDate,
 numModifiedBy,
 bintModifiedDate,
 numDomainID,
 tintSupportKeyType,
 intTargetResolveDate,
 numRecOwner ,
 numContractID,
 numStatus,numPriority,numOrigin,numType,numReason,textInternalComments)
 values(v_numDivisionID,
 v_numContactId,
 v_vcCaseNumber,
 v_textSubject,
 v_textDesc,
 v_numUserCntID,
 TIMEZONE('UTC',now()),
 v_numUserCntID,
 TIMEZONE('UTC',now()),
 v_numDomainID,
 v_tintSupportKeyType,
 v_intTargetResolveDate,
 v_numUserCntID ,
 v_numContractId,
 v_numStatus,v_numPriority,v_numOrigin,v_numType,v_numReason,v_textInternalComments) RETURNING numCaseId INTO v_numCaseId;
      
      IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId = v_numDivisionID AND numDomainId = v_numDomainID AND intType = 3 AND(coalesce(numIncidents,0) -coalesce(numIncidentsUsed,0)) > 0) then

         UPDATE
         Contracts
         SET
         numIncidentsUsed = coalesce(numIncidentsUsed,0)+1,numModifiedBy = v_numUserCntID,
         dtmModifiedOn = TIMEZONE('UTC',now())
         WHERE
         numDomainId = v_numDomainID AND numDivisonId = v_numDivisionID  AND intType = 3;
         INSERT INTO ContractsLog(intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId)
         SELECT
         3,v_numDivisionID,v_numCaseId,TIMEZONE('UTC',now()),v_numUserCntID,v_vcCaseNumber,1,(coalesce(numIncidents,0) -coalesce(numIncidentsUsed,0)),numContractId
         FROM Contracts WHERE numDivisonId = v_numDivisionID AND numDomainId = v_numDomainID AND intType = 3;
      end if;
      PERFORM usp_CaseDefaultAssociateContacts(v_numCaseId := v_numCaseId,v_numDomainID := v_numDomainID);
  --Map Custom Field	
   --  numeric(18, 0)
	 --  numeric(18, 0)
	 --  numeric(18, 0)
      v_tintPageID := 3;
      PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numCaseId,v_numParentRecId := v_numDivisionID,
      v_tintPageID := v_tintPageID::SMALLINT);
   else                      
		-- Checking For Incidents
		
      v_update := false;
      if v_numContractId <> 0 then
		
         IF(SELECT bitIncidents FROM ContractManagement WHERE numdomainId = v_numDomainID  and numContractId = v_numContractId) = true then
            v_Usedincidents := 0;
            select   v_Usedincidents+count(*) INTO v_Usedincidents from Cases where numContractID = v_numContractId and numDomainID = v_numDomainID and numCaseId <> v_numCaseId;
            select   v_Usedincidents+count(*) INTO v_Usedincidents from ProjectsMaster where numcontractId = v_numContractId and numdomainId = v_numDomainID;
            select   coalesce(numincidents,0) INTO v_Availableincidents from ContractManagement where numdomainId = v_numDomainID  and numContractId = v_numContractId;
            RAISE NOTICE '@Usedincidents-----';
            RAISE NOTICE '%',v_Usedincidents;
            RAISE NOTICE '%',v_Availableincidents;
            if v_Usedincidents >= v_Availableincidents then
               v_update := true;
            end if;
         end if;
      end if;
      if v_update = false then
	
         update Cases
         set
         intTargetResolveDate = v_intTargetResolveDate,textSubject = v_textSubject,
         numStatus = v_numStatus,numPriority = v_numPriority,textDesc = v_textDesc,textInternalComments = v_textInternalComments,
         numReason = v_numReason,numOrigin = v_numOrigin,
         numType = v_numType,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
         numContractID = v_numContractId,numContactId = v_numContactId
         where numCaseId = v_numCaseId;
         v_numCaseId := v_numCaseId;
      else
         v_numCaseId := 0;
      end if;
   end if;         
        
        
          
 ---Updating if Case is assigned to someone              
   v_tempAssignedTo := null;             
   select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo from Cases where  numCaseId = v_numCaseId;              
          
   if (v_tempAssignedTo <> v_numAssignedTo and  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
  
      update Cases set numAssignedTo = v_numAssignedTo,numAssignedBy = v_numUserCntID where  numCaseId = v_numCaseId;
   ELSEIF (v_numAssignedTo = 0)
   then
  
      update Cases set numAssignedTo = 0,numAssignedBy = 0 where  numCaseId = v_numCaseId;
   end if;
   RETURN;
END; $$;


