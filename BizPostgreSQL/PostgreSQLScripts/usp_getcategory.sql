DROP FUNCTION IF EXISTS USP_GetCategory;

CREATE OR REPLACE FUNCTION USP_GetCategory(v_numCatergoryId NUMERIC(9,0),                        
v_byteMode SMALLINT ,                  
v_str VARCHAR(1000),                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numSiteID NUMERIC(9,0) DEFAULT 0 ,
v_numCurrentPage NUMERIC(9,0) DEFAULT 0,
v_PageSize INTEGER DEFAULT 20,
INOUT v_numTotalPage NUMERIC(9,0)  DEFAULT NULL,    
v_numItemCode NUMERIC(9,0) DEFAULT NULL,
v_SortChar CHAR(1) DEFAULT '0',
v_numCategoryProfileID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strsql  VARCHAR(8000);
   v_strRowCount  VARCHAR(8000);
   v_bParentCatrgory  BOOLEAN;
   v_tintDisplayCategory  Boolean;
   v_bSubCatrgory  BOOLEAN;
BEGIN
                     
--selecting all categories                     
   BEGIN
      CREATE TEMP SEQUENCE tt_TempCategory_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPCATEGORY CASCADE;
   create TEMPORARY TABLE tt_TEMPCATEGORY
   (
      PKId NUMERIC(9,0) DEFAULT NEXTVAL('tt_TempCategory_seq'),
      numItemCode NUMERIC(9,0),
      vcItemName VARCHAR(250)
   );

                  
                        
--selecting all catories where level is 1                       
                      
--selecting subCategories                      
   if v_byteMode = 0 then

      open SWV_RefCur for
      with recursive CategoryList
      As(SELECT C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink = false then 'No' when C1.bitLink = true then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LENGTH(coalesce(C1.vcDescription,'')) > 0 THEN
         CASE WHEN LENGTH(C1.vcDescription) > 40 THEN SUBSTR(C1.vcDescription,0,40) || ' [...]'
         ELSE C1.vcDescription || ' [...]'	END
      ELSE
         coalesce(C1.vcDescription,'')
      END
      AS vcShortDescription,CAST(1 AS INTEGER) AS Level,
	CAST(REPEAT('.',1) || C1.vcCategoryName as VARCHAR(1000))  as vcNewCategoryName,
	CAST(C1.numCategoryID AS TEXT) AS "Order"
      FROM Category C1 WHERE C1.numDepCategory = 0 AND  C1.numDomainID = v_numDomainID AND C1.numCategoryProfileID = v_numCategoryProfileID
      UNION ALL
      SELECT  C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink = false then 'No' when C1.bitLink = true then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LENGTH(coalesce(C1.vcDescription,'')) > 0 THEN
         CASE WHEN LENGTH(C1.vcDescription) > 40 THEN SUBSTR(C1.vcDescription,0,40) || ' [...]'
         ELSE C1.vcDescription || ' [...]'	END
      ELSE
         coalesce(C1.vcDescription,'')
      END
      AS vcShortDescription,CL.Level+1 AS Level,
	CAST(REPEAT('.',Level*3) || C1.vcCategoryName as VARCHAR(1000))  as vcNewCategoryName,
	"Order" || '.' || CAST(C1.numCategoryID AS TEXT) AS "Order"
      FROM Category C1
      INNER JOIN CategoryList CL
      ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID = v_numDomainID  AND C1.numCategoryProfileID = v_numCategoryProfileID)
      
		SELECT 
			numCategoryID AS "numCategoryID"
			,vcCategoryName AS "vcCategoryName"
			,vcCategoryNameURL AS "vcCategoryNameURL"
			,numDepCategory AS "numDepCategory"
			,tintLevel AS "tintLevel"
			,Link as "Link"
			,vcLink as "vcLink"
			,DepCategory as "DepCategory"
			,intDisplayOrder AS "intDisplayOrder"
			,vcDescription AS "vcDescription"
			,vcShortDescription AS "vcShortDescription"
			,Level AS "Level"
			,vcNewCategoryName AS "vcNewCategoryName"
			,"Order" AS "Order"
			,(SELECT COUNT(*) FROM SiteCategories SC WHERE SC.numSiteID = v_numSiteID AND SC.numCategoryID = C1.numCategoryID) AS "ShowInECommerce"
			,coalesce((SELECT COUNT(*) FROM ItemCategory WHERE numCategoryID = C1.numCategoryID),0) AS "ItemCount" 
		FROM 
			CategoryList C1 
		ORDER BY 
			"Order";                                                
--deleting a category                      
   ELSEIF v_byteMode = 1
   then

      delete from SiteCategories where numCategoryID = v_numCatergoryId;
      delete from ItemCategory where numCategoryID = v_numCatergoryId;
      delete from Category where numCategoryID = v_numCatergoryId;
      open SWV_RefCur for
      select 1;
   ELSEIF v_byteMode = 2
   then

		open SWV_RefCur for
		select 
			numCategoryID AS "numCategoryID"
			,vcCategoryName AS "vcCategoryName"
			,vcCategoryNameURL AS "vcCategoryNameURL"
			,intDisplayOrder AS "intDisplayOrder"
			,vcDescription AS "vcDescription" 
		from Category
      where (tintLevel = 1   or (tintLevel <> 1 and numDepCategory = v_numCatergoryId))  and numCategoryID != v_numCatergoryId
      and numDomainID = v_numDomainID
      ORDER BY vcCategoryName;
   ELSEIF v_byteMode = 3
   then

		open SWV_RefCur for
		select 
			numCategoryID AS "numCategoryID"
			,vcCategoryName AS "vcCategoryName"
			,vcCategoryNameURL AS "vcCategoryNameURL"
			,intDisplayOrder AS "intDisplayOrder"
			,vcDescription AS "vcDescription"  
		from Category
      where numDepCategory = v_numCatergoryId
      ORDER BY vcCategoryName;
   end if;                      
                      
--selecting Items                      
 


--selecting Items not present in Category                   
   


--selecting Items From Item Groups                  


-- Selecting Categories Site Wise and Sorted into Sort Order 
   if v_byteMode = 4 then

      open SWV_RefCur for
      select numItemCode AS "numItemCode",vcItemName AS "vcItemName"  from Item
      where numDomainID = v_numDomainID;                      
                      
--selecting Item belongs to a category                      
   ELSEIF v_byteMode = 5
   then

      insert into tt_TEMPCATEGORY(numItemCode, vcItemName)
      select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item
      join ItemCategory
      on numItemCode = numItemID
      where numCategoryID = v_numCatergoryId
      and numDomainID = v_numDomainID
      ORDER BY vcItemName;
      select   COUNT(*) INTO v_numTotalPage from tt_TEMPCATEGORY;
      if v_numTotalPage < v_PageSize then
		
         v_numTotalPage := 1;
      else
         v_numTotalPage := v_numTotalPage/v_PageSize::bigint;
      end if;
      open SWV_RefCur for
      select numItemCode AS "numItemCode",vcItemName AS "vcItemName" from tt_TEMPCATEGORY  where PKId between(v_numCurrentPage*v_PageSize::bigint) -(v_PageSize::bigint -1) and  v_numCurrentPage*v_PageSize::bigint;
      open SWV_RefCur2 for
      select v_numTotalPage;
   ELSEIF v_byteMode = 6
   then

      open SWV_RefCur for
      select 
	  C1.numCategoryID AS "numCategoryID"
	  ,C1.vcCategoryName AS "vcCategoryName"
	  ,((select Count(*) from Category C2 where C2.numDepCategory = C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID = C1.numCategoryID)) as "Count"
	  ,CAST(bitLink AS INTEGER) as "Type"
	  ,vcLink AS "vcLink"
	  ,C1.intDisplayOrder AS "intDisplayOrder",
	C1.vcDescription AS "vcDescription"
      from Category C1
      where tintLevel = 1
      and numDomainID = v_numDomainID
      AND numCategoryProfileID = v_numCategoryProfileID;
   ELSEIF v_byteMode = 7
   then

		open SWV_RefCur for
		select 
			C1.numCategoryID AS "numCategoryID"
			,C1.vcCategoryName AS "vcCategoryName"
			,((select Count(*) from Category C2 where C2.numDepCategory = C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID = C1.numCategoryID)) as "Count"
			,CAST(bitLink AS INTEGER) as "Type"
			,vcLink AS "vcLink"
			,C1.intDisplayOrder AS "intDisplayOrder"
			,C1.vcDescription AS "vcDescription"
			,numDepCategory as "Category"
      from Category C1
      where C1.numDepCategory = v_numCatergoryId and numDomainID = v_numDomainID
      union
      select numItemCode,vcItemName,CAST(0 AS INTEGER) ,CAST(2 AS INTEGER),'',0,'',0  from Item
      join ItemCategory
      on numItemCode = numItemID
      where numCategoryID = v_numCatergoryId  and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 8
   then
                    
     --kishan               
      v_strsql := 'select numItemCode AS "numItemCode",vcItemName AS "vcItemName",txtItemDesc AS "txtItemDesc",0,
	(
	SELECT II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = true AND II.bitIsImage = true 
    AND II.numDomainID =' || SUBSTR(cast(v_numDomainID as VARCHAR(50)),1,50) || ' LIMIT 1)  AS 
	"vcPathForTImage" ,
	1 as "Type",monListPrice as "price",0 as "numQtyOnHand"  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in(' || coalesce(v_str,'') || ')';
      OPEN SWV_RefCur FOR EXECUTE v_strsql;
   ELSEIF v_byteMode = 9
   then

      open SWV_RefCur for
      select distinct(numItemCode) AS "numItemCode",vcItemName AS "vcItemName",txtItemDesc AS "txtItemDesc",0 , (SELECT  vcPathForImage FROM ItemImages WHERE numDomainId = v_numDomainID AND bitDefault = true AND numItemCode = Item.numItemCode LIMIT 1) AS "vcPathForImage",1 as "Type",monListPrice as "price",0 as "numQtyOnHand" from Item
      join ItemCategory
      on numItemCode = numItemID
      where (vcItemName ilike '%' || coalesce(v_str,'') || '%' or txtItemDesc ilike '%' || coalesce(v_str,'') || '%') and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 10
   then

      open SWV_RefCur for
      select ROW_NUMBER() OVER(ORDER BY Category Asc) AS "RowNumber"
	  ,numCategoryID AS "numCategoryID"
	  ,vcCategoryName AS "vcCategoryName"
	  ,Type AS "Type"
	  ,vcLink AS "vcLink"
	  ,"Category" AS "Category"
	  ,intDisplayOrder AS "intDisplayOrder"
	  ,vcDescription AS "vcDescription"
	  
	  from(select   C1.numCategoryID,C1.vcCategoryName,CAST(bitLink AS INTEGER) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription
         from Category C1
         where  numDomainID = v_numDomainID) X;
   ELSEIF v_byteMode = 11
   then

      insert into tt_TEMPCATEGORY(numItemCode, vcItemName)
      SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM Item  I LEFT OUTER JOIN ItemCategory IC ON IC.numItemID = I.numItemCode
      WHERE numDomainID = v_numDomainID AND coalesce(IC.numCategoryID,0) <> v_numCatergoryId
      ORDER BY vcItemName;
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



      select   COUNT(*) INTO v_numTotalPage from tt_TEMPCATEGORY;
      v_numTotalPage := v_numTotalPage/v_PageSize::bigint;
      open SWV_RefCur for
      select numItemCode AS "numItemCode",vcItemName AS "vcItemName" from tt_TEMPCATEGORY  where PKId between(v_numCurrentPage*v_PageSize::bigint) -(v_PageSize::bigint -1) and  v_numCurrentPage*v_PageSize::bigint;
      open SWV_RefCur2 for
      select v_numTotalPage;
   ELSEIF v_byteMode = 12
   then

      insert into tt_TEMPCATEGORY(numItemCode, vcItemName)
      select numItemCode,vcItemName  from Item
      where numItemGroup = v_numCatergoryId
      ORDER BY vcItemName;
      select   COUNT(*) INTO v_numTotalPage from tt_TEMPCATEGORY;
      v_numTotalPage := v_numTotalPage/v_PageSize::bigint;
      open SWV_RefCur for
      select numItemCode AS "numItemCode", vcItemName AS "vcItemName" from tt_TEMPCATEGORY  where PKId between(v_numCurrentPage*v_PageSize::bigint) -(v_PageSize::bigint -1) and  v_numCurrentPage*v_PageSize::bigint;
      open SWV_RefCur2 for
      select v_numTotalPage;
   ELSEIF v_byteMode = 13
   then
      select   CPS.numCategoryProfileID, S.numDomainID INTO v_numCategoryProfileID,v_numDomainID FROM
      Sites S
      JOIN
      CategoryProfileSites CPS
      ON
      S.numSiteID = CPS.numSiteID
      JOIN
      CategoryProfile CP
      ON
      CPS.numCategoryProfileID = CP.ID WHERE
      S.numSiteID = v_numSiteID;
      select   vcAttributeValue INTO v_bParentCatrgory FROM PageElementDetail WHERE numSiteID = v_numSiteID AND numAttributeID = 39;
      v_bParentCatrgory := coalesce(v_bParentCatrgory,false);
      select   coalesce(bitDisplayCategory,false) INTO v_tintDisplayCategory FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
      IF coalesce(v_tintDisplayCategory,false) = false then
	
         open SWV_RefCur for
         SELECT C.numCategoryID AS "numCategoryID",
			   C.vcCategoryName AS "vcCategoryName",
			   C.tintLevel AS "tintLevel",
			   CAST(C.bitLink AS INTEGER) as "TYPE",
			   C.vcLink AS "vcLink",
			   numDepCategory as "Category",C.intDisplayOrder AS "intDisplayOrder",C.vcDescription AS "vcDescription"
         FROM   SiteCategories SC
         INNER JOIN Category C ON SC.numCategoryID = C.numCategoryID
         WHERE  SC.numSiteID = v_numSiteID
         AND SC.numCategoryID = C.numCategoryID
         AND 1 =(CASE WHEN v_bParentCatrgory = true THEN tintLevel ELSE 1 END)
         ORDER BY tintLevel,coalesce(C.intDisplayOrder,0),numDepCategory;
      ELSE
         open SWV_RefCur for
         SELECT C.numCategoryID AS "numCategoryID",
			   C.vcCategoryName AS "vcCategoryName",
			   C.tintLevel AS "tintLevel",
			   CAST(C.bitLink AS INTEGER) as "TYPE",
			   C.vcLink AS "vcLink",
			   numDepCategory as "Category",C.intDisplayOrder AS "intDisplayOrder",C.vcDescription AS "vcDescription"
         FROM   SiteCategories SC
         INNER JOIN Category C ON SC.numCategoryID = C.numCategoryID
         LEFT JOIN ItemCategory IC ON IC.numCategoryID = C.numCategoryID
         LEFT JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID AND IC.numItemID = WI.numItemID
         WHERE  SC.numSiteID = v_numSiteID
         AND SC.numCategoryID = C.numCategoryID
         AND 1 =(CASE WHEN v_bParentCatrgory = true THEN tintLevel ELSE 1 END)
         AND ((coalesce(numOnHand,0) > 0 OR coalesce(numAllocation,0) > 0) OR tintLevel = 1)
         GROUP BY
         C.numCategoryID,C.vcCategoryName,C.tintLevel,C.bitLink,C.vcLink,numDepCategory,
         C.intDisplayOrder,C.vcDescription
         ORDER BY
         tintLevel,coalesce(C.intDisplayOrder,0),"Category";
      end if;
   ELSEIF v_byteMode = 14
   then

      open SWV_RefCur for
      SELECT C.numCategoryID AS "numCategoryID",
		   C.vcCategoryName AS "vcCategoryName",
		   C.tintLevel AS "tintLevel",
		   CAST(C.bitLink AS INTEGER) as "TYPE",
		   C.vcLink AS "vcLink",
		   numDepCategory as "Category",
		   C.intDisplayOrder AS "intDisplayOrder",
		   C.vcPathForCategoryImage AS "vcPathForCategoryImage",
		   C.vcDescription AS "vcDescription"
      FROM   SiteCategories SC
      INNER JOIN Category C
      ON SC.numCategoryID = C.numCategoryID
      WHERE  SC.numSiteID = v_numSiteID
      AND SC.numCategoryID = C.numCategoryID
      AND coalesce(numDepCategory,0) = 0
      ORDER BY coalesce(C.intDisplayOrder,0),numDepCategory;
   ELSEIF v_byteMode = 15
   then
      select   vcAttributeValue INTO v_bSubCatrgory FROM PageElementDetail WHERE numSiteID = v_numSiteID AND numAttributeID = 40;
      v_bSubCatrgory := coalesce(v_bSubCatrgory,false);
      open SWV_RefCur for
      SELECT C.numCategoryID AS "numCategoryID",
		   C.vcCategoryName AS "vcCategoryName",
		   C.tintLevel AS "tintLevel",
		   CAST(C.bitLink AS INTEGER) as "TYPE",
		   C.vcLink AS "vcLink",
		   numDepCategory as "Category"
		   ,C.intDisplayOrder AS "intDisplayOrder"
		   ,C.vcPathForCategoryImage AS "vcPathForCategoryImage"
		   ,C.vcDescription AS "vcDescription"
      FROM   SiteCategories SC
      INNER JOIN Category C
      ON SC.numCategoryID = C.numCategoryID
      WHERE  SC.numSiteID = v_numSiteID
      AND SC.numCategoryID = C.numCategoryID AND numDepCategory = v_numCatergoryId
      AND 1 =(CASE WHEN v_bSubCatrgory = true THEN 1 ELSE 0 END)
      ORDER BY coalesce(C.intDisplayOrder,0),numDepCategory;
   ELSEIF v_byteMode = 16
   then

      open SWV_RefCur for
      SELECT vcPathForCategoryImage AS "vcPathForCategoryImage" FROM Category WHERE numCategoryID = v_numCatergoryId AND numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 17
   then

		open SWV_RefCur for
		SELECT 
			numCategoryID AS "numCategoryID"
			,vcCategoryName AS "vcCategoryName"
			,vcCategoryNameURL AS "vcCategoryNameURL"
			,vcDescription AS "vcDescription"
			,intDisplayOrder AS "intDisplayOrder"
			,vcPathForCategoryImage AS "vcPathForCategoryImage"
			,numDepCategory AS "numDepCategory"
			,coalesce(vcMetaTitle,'') AS "vcMetaTitle"
			,coalesce(vcMetaKeywords,'') AS "vcMetaKeywords"
			,coalesce(vcMetaDescription,'') AS "vcMetaDescription" FROM Category  WHERE numCategoryID = v_numCatergoryId AND numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 18
   then

	
		-- USED FOR HIERARCHICAL CATEGORY LIST
      open SWV_RefCur for
      SELECT * FROM(SELECT
         ROW_NUMBER() OVER(ORDER BY C1.vcCategoryName) AS "RowNum",
			C1.numCategoryID AS "numCategoryID",
			C1.vcCategoryName AS "vcCategoryName",
			C1.vcCategoryNameURL AS "vcCategoryNameURL",
			NULLIF(numDepCategory,0) AS "numDepCategory",
			coalesce(numDepCategory,0) AS "numDepCategory1",
			0 AS "tintLevel",
			coalesce((SELECT COUNT(*) FROM ItemCategory WHERE numCategoryID = C1.numCategoryID),0) AS "ItemCount",
			coalesce((SELECT COUNT(*) FROM ItemCategory WHERE numCategoryID IN(SELECT numCategoryID FROM Category WHERE numDepCategory = C1.numCategoryID)),
         0) AS "ItemSubcategoryCount"
			,coalesce(bitVisible,true) AS "bitVisible"
			,coalesce((SELECT
            string_agg(IG.vcItemGroup,', ' order by IG.vcItemGroup)
            FROM
            CategoryMatrixGroup CMG
            INNER JOIN
            ItemGroups IG
            ON
            CMG.numItemGroup = IG.numItemGroupID
            WHERE
            CMG.numDomainID = v_numDomainID
            AND CMG.numCategoryID = C1.numCategoryID
            ),'') AS "vcMatrixGroups"
         FROM
         Category C1
         WHERE
         numDomainID = v_numDomainID AND numCategoryProfileID = v_numCategoryProfileID) TEMp
      ORDER BY
      TEMp."vcCategoryName";
   ELSEIF v_byteMode = 19
   then

      select   CPS.numCategoryProfileID INTO v_numCategoryProfileID FROM
      Sites S
      JOIN
      CategoryProfileSites CPS
      ON
      S.numSiteID = CPS.numSiteID
      JOIN
      CategoryProfile CP
      ON
      CPS.numCategoryProfileID = CP.ID WHERE
      S.numSiteID = v_numSiteID;
      BEGIN
         open SWV_RefCur for
         with recursive CategoryList
         As(SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink = false then 'No' when C1.bitLink = true then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LENGTH(coalesce(C1.vcDescription,'')) > 0 THEN
            CASE WHEN LENGTH(C1.vcDescription) > 40 THEN SUBSTR(C1.vcDescription,0,40) || ' [...]'
            ELSE C1.vcDescription || ' [...]'	END
         ELSE
            coalesce(C1.vcDescription,'')
         END
         AS vcShortDescription,CAST(1 AS INTEGER) AS Level,
	CAST(REPEAT('.',1) || C1.vcCategoryName as VARCHAR(1000))  as vcNewCategoryName,
	CAST(C1.numCategoryID AS TEXT) AS "Order"
         FROM Category C1 WHERE C1.numDepCategory = 0 AND  C1.numDomainID = v_numDomainID AND C1.numCategoryProfileID = v_numCategoryProfileID
         UNION ALL
         SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink = false then 'No' when C1.bitLink = true then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LENGTH(coalesce(C1.vcDescription,'')) > 0 THEN
            CASE WHEN LENGTH(C1.vcDescription) > 40 THEN SUBSTR(C1.vcDescription,0,40) || ' [...]'
            ELSE C1.vcDescription || ' [...]'	END
         ELSE
            coalesce(C1.vcDescription,'')
         END
         AS vcShortDescription,CL.Level+1 AS Level,
	CAST(REPEAT('.',Level*3) || C1.vcCategoryName as VARCHAR(1000))  as vcNewCategoryName,
	"Order" || '.' || CAST(C1.numCategoryID AS TEXT) AS "Order"
         FROM Category C1
         INNER JOIN CategoryList CL
         ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID = v_numDomainID AND C1.numCategoryProfileID = v_numCategoryProfileID)
         
		 SELECT 
			numCategoryID AS "numCategoryID"
			,vcCategoryName AS "vcCategoryName"
			,(CASE WHEN  numDepCategory = 0 THEN NULL ELSE numDepCategory END) AS "numDepCategory"
			,tintLevel AS "tintLevel"
			,Link AS "Link"
			,vcLink AS "vcLink"
			,DepCategory AS "DepCategory"
			,(SELECT COUNT(*) FROM SiteCategories SC WHERE SC.numSiteID = v_numSiteID AND SC.numCategoryID = C1.numCategoryID) AS "ShowInECommerce"
			,coalesce((SELECT COUNT(*) FROM ItemCategory WHERE numCategoryID = C1.numCategoryID),0) AS "ItemCount"
			FROM CategoryList C1 ORDER BY "Order";
      END;
      DROP TABLE IF EXISTS tt_TEMPCATEGORY CASCADE;
   end if;
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
   RETURN;
END; $$;


