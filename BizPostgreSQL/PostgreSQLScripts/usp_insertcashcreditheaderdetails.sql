-- Stored procedure definition script USP_InsertCashCreditHeaderDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertCashCreditHeaderDetails(v_numCashCreditId NUMERIC(9,0) DEFAULT 0,                  
v_datEntry_Date TIMESTAMP DEFAULT NULL,                                
v_monAmount DECIMAL(20,5) DEFAULT NULL,                        
v_vcMemo VARCHAR(8000) DEFAULT NULL,                  
v_numPurchaseId NUMERIC(9,0) DEFAULT 0,                
v_numAcntTypeId NUMERIC(9,0) DEFAULT 0,           
v_vcReferenceNo VARCHAR(8000) DEFAULT NULL,          
v_bitMoneyOut BOOLEAN DEFAULT NULL,          
v_bitChargeBack BOOLEAN DEFAULT NULL,          
v_numDomainId NUMERIC(9,0) DEFAULT 0,        
v_numRecurringId NUMERIC(9,0) DEFAULT 0,      
v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numRecurringId = 0 then 
      v_numRecurringId := null;
   end if;                             
   If v_numCashCreditId = 0 then
 
      Insert into CashCreditCardDetails(datEntry_Date,monAmount,vcMemo,numPurchaseId,numAcntTypeId,vcReference,bitMoneyOut,bitChargeBack,numDomainID,numRecurringId,numCreatedBy,dtCreatedDate) Values(v_datEntry_Date,v_monAmount,v_vcMemo,v_numPurchaseId
  ,v_numAcntTypeId,v_vcReferenceNo,v_bitMoneyOut,v_bitChargeBack,v_numDomainId,v_numRecurringId,v_numUserCntID,TIMEZONE('UTC',now()));
  
      v_numCashCreditId := CURRVAL('CashCreditCardDetails_seq');
      open SWV_RefCur for Select v_numCashCreditId;
   end if;                    
               
   If v_numCashCreditId <> 0 then
 
      Update CashCreditCardDetails Set datEntry_Date = v_datEntry_Date,monAmount = v_monAmount,vcMemo = v_vcMemo,numPurchaseId = v_numPurchaseId,
      numAcntTypeId = v_numAcntTypeId,vcReference = v_vcReferenceNo,
      bitMoneyOut = v_bitMoneyOut,bitChargeBack = v_bitChargeBack,
      numDomainID = v_numDomainId,numRecurringId = v_numRecurringId,numModifiedBy = v_numUserCntID,
      dtModifiedDate = TIMEZONE('UTC',now())  Where numCashCreditId = v_numCashCreditId;
   end if;
   RETURN;
END; $$;












