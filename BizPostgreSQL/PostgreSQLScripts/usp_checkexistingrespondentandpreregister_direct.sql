-- Stored procedure definition script usp_CheckExistingRespondentAndPreRegister_Direct for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CheckExistingRespondentAndPreRegister_Direct(v_numSurID NUMERIC,  
 v_numContactId NUMERIC,
 v_numDomainID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql                
--This will insert response for the surveys.                
   AS $$
   DECLARE
   v_numDivisionId  NUMERIC;          
   v_numRespondantID  NUMERIC;          
   v_vcEmail  VARCHAR(50);
   SWV_RowCount INTEGER;
BEGIN
   select   numDivisionId, vcEmail INTO v_numDivisionId,v_vcEmail FROM AdditionalContactsInformation WHERE numContactId = v_numContactId and numDomainID = v_numDomainID    LIMIT 1;     
        
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   IF SWV_RowCount > 0 then
   
      INSERT INTO SurveyRespondentsMaster(numSurID, numSurRating, dtDateofResponse, bitRegisteredRespondant, numRegisteredRespondentContactId, numDomainId)
 VALUES(v_numSurID, 0, TIMEZONE('UTC',now()), true, v_numContactId, v_numDomainID);
 
      v_numRespondantID := CURRVAL('SurveyRespondentsMaster_seq');
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
      SELECT v_numRespondantID, v_numSurID, 'numCustomerId','Customer Id', 0, numCompanyID,
 0, 1,'EditBox'  FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
 VALUES(v_numRespondantID, v_numSurID, 'vcEmail','Email Address', 0, v_vcEmail,
 0, 2,'EditBox');
 
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
      SELECT v_numRespondantID, v_numSurID, 'vcFirstName','First Name', 0, vcFirstName,
 1, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
      SELECT v_numRespondantID, v_numSurID, 'vcLastName','Last Name', 0, vcLastname,
 1, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
      SELECT v_numRespondantID, v_numSurID, 'numPhone','Phone', 0,numPhone ,
 2, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)
      SELECT v_numRespondantID, v_numSurID, 'numPhoneExtension','Extension', 0, numPhoneExtension,
 2, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      open SWV_RefCur for
      SELECT v_numRespondantID;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


