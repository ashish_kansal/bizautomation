-- Stored procedure definition script USP_ManageExtranetAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageExtranetAccounts(v_numExtranetID NUMERIC(9,0) DEFAULT null,      
v_numGroupID NUMERIC(9,0) DEFAULT null,      
v_strEmail TEXT DEFAULT '',  
--@numPartnerGroup as numeric(9)=0,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   update  ExtarnetAccounts set
   numGroupID = v_numGroupID
--numPartnerGroupID=@numPartnerGroup      
   where numExtranetID = v_numExtranetID;      

	insert into ExtranetAccountsDtl(numExtranetID,numContactID,tintAccessAllowed,vcPassword,numDomainID)
	select 
		v_numExtranetID,
		X.ContactID
		,X.Portal
		,X.Password
		,v_numDomainID  
	from
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strEmail AS XML)
		COLUMNS
			id FOR ORDINALITY,
			ContactID NUMERIC(9,0) PATH 'ContactID',
			Portal SMALLINT PATH 'Portal',
			Password VARCHAR(100) PATH 'Password',
			ExtranetDTLID NUMERIC(9,0) PATH 'ExtranetDTLID'
	) AS X
	WHERE
		COALESCE(ExtranetDTLID,0) = 0;
           
	Update 
		ExtranetAccountsDtl
	SET 
		tintAccessAllowed = X.Portal,              
		vcPassword = X.Password,
		numDomainID = v_numDomainID
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strEmail AS XML)
		COLUMNS
			id FOR ORDINALITY,
			ContactID NUMERIC(9,0) PATH 'ContactID',
			Portal SMALLINT PATH 'Portal',
			Password VARCHAR(100) PATH 'Password',
			ExtranetDTLID NUMERIC(9,0) PATH 'ExtranetDTLID'
	) AS X
	WHERE
		COALESCE(ExtranetDTLID,0) > 0;
      
   RETURN;
END; $$;


