DROP FUNCTION IF EXISTS USP_GetDomainDetailsForPriceMargin;

CREATE OR REPLACE FUNCTION USP_GetDomainDetailsForPriceMargin(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numListItemID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listIds  TEXT;
BEGIN
   v_listIds := COALESCE((select string_agg(numUserID::VARCHAR,',') FROM UnitPriceApprover WHERE numDomainID = v_numDomainID),'');

   IF v_numListItemID > 0 then

      open SWV_RefCur for
      SELECT
      coalesce(AP.numAbovePercent,0) AS numAbovePercent,
		coalesce(AP.numBelowPercent,0) AS numBelowPercent,
		coalesce(AP.numBelowPriceField,0) AS numBelowPriceField,
		coalesce(AP.bitCostApproval,false) AS bitCostApproval,
		coalesce(AP.bitListPriceApproval,false) AS bitListPriceApproval,
		coalesce(AP.bitMarginPriceViolated,false) AS bitMarginPriceViolated,
		coalesce(numListItemID,0) AS numListItemID,
		v_listIds AS vcUnitPriceApprover
      FROM Domain D
      LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainId
      WHERE (D.numDomainId = v_numDomainID OR v_numDomainID = -1)
      AND numListItemID = v_numListItemID;
   ELSE
      open SWV_RefCur for
      SELECT
      coalesce(AP.numAbovePercent,0) AS numAbovePercent,
		coalesce(AP.numBelowPercent,0) AS numBelowPercent,
		coalesce(AP.numBelowPriceField,0) AS numBelowPriceField,
		coalesce(AP.bitCostApproval,false) AS bitCostApproval,
		coalesce(AP.bitListPriceApproval,false) AS bitListPriceApproval,
		coalesce(AP.bitMarginPriceViolated,false) AS bitMarginPriceViolated,
		coalesce(numListItemID,0) AS numListItemID,
		v_listIds AS vcUnitPriceApprover
      FROM Domain D
      LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainId
      WHERE (D.numDomainId = v_numDomainID OR v_numDomainID = -1);
   end if;

/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormList]    Script Date: 07/26/2008 16:17:24 ******/
   RETURN;
END; $$;


