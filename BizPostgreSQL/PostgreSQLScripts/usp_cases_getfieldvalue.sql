-- Stored procedure definition script USP_Cases_GetFieldValue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Cases_GetFieldValue(v_numDomainID NUMERIC(18,0),    
	v_numCaseId NUMERIC(18,0),
	v_vcFieldName VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'numDivisionID' then
	
      open SWV_RefCur for SELECT numDivisionID  FROM Cases WHERE numDomainID = v_numDomainID AND numCaseId = v_numCaseId;
   ELSE
      RAISE EXCEPTION 'FIELD_NOT_FOUND';
   end if;
END; $$;













