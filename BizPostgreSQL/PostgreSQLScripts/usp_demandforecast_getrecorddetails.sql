-- Stored procedure definition script USP_DemandForecast_GetRecordDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DemandForecast_GetRecordDetails(v_numDomainID NUMERIC(18,0)
	,v_tintType SMALLINT
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_numForecastDays INTEGER
	,v_numAnalysisDays INTEGER
	,v_bitBasedOnLastYear BOOLEAN
	,v_numOpportunityPercentComplete NUMERIC(18,0)
	,v_CurrentPage INTEGER
    ,v_PageSize INTEGER
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtFromDate  TIMESTAMP;
   v_dtToDate  TIMESTAMP;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numOppID NUMERIC(18,0),
      numWOID NUMERIC(18,0),
      dtCreatedDate TIMESTAMP,
      vcOppName VARCHAR(500),
      numOrderedQty DOUBLE PRECISION,
      numQtyToShip DOUBLE PRECISION,
      numOpportunityForecastQty DOUBLE PRECISION,
      dtReleaseDate VARCHAR(50)
   );
   IF v_tintType = 1 then -- Release Dates
	
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OpportunityMaster.numOppId
			,0
			,OpportunityMaster.bintCreatedDate
			,CAST(OpportunityMaster.vcpOppName AS VARCHAR(500))
			,OpportunityItems.numUnitHour
			,OpportunityItems.numUnitHour -coalesce(numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityMaster
      INNER JOIN
      OpportunityItems
      ON
      OpportunityMaster.numOppId = OpportunityItems.numOppId
      INNER JOIN
      Item
      ON
      OpportunityItems.numItemCode = Item.numItemCode
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND OpportunityMaster.tintopptype = 1
      AND OpportunityMaster.tintoppstatus = 1
      AND coalesce(OpportunityMaster.tintshipped,0) = 0
      AND(coalesce(OpportunityItems.numUnitHour,0)  -coalesce(OpportunityItems.numQtyShipped,0)) > 0
      AND coalesce(OpportunityItems.bitDropShip,false) = false
      AND coalesce(Item.IsArchieve,false) = false
      AND coalesce(Item.bitAssembly,false) = false
      AND coalesce(Item.bitKitParent,false) = false
      AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
      AND OpportunityItems.numItemCode = v_numItemCode
      AND OpportunityItems.numWarehouseItmsID = v_numWarehouseItemID
      AND coalesce(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval);

		-- USED IN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq -coalesce(OKI.numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      Item
      ON
      OKI.numChildItemID = Item.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(OM.tintshipped,0) = 0
      AND coalesce(OI.bitDropShip,false) = false
      AND coalesce(OI.bitWorkOrder,false) = false
      AND coalesce(Item.IsArchieve,false) = false
      AND coalesce(Item.bitAssembly,false) = false
      AND coalesce(Item.bitKitParent,false) = false
      AND(coalesce(OKI.numQtyItemsReq,0) -coalesce(OKI.numQtyShipped,0)) > 0
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND OKI.numChildItemID = v_numItemCode
      AND OKI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval);

		-- USED IN KIT WITHIN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq -coalesce(OKCI.numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OI.numoppitemtCode = OKCI.numOppItemID
      INNER JOIN
      Item
      ON
      OKCI.numItemID = Item.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(OM.tintshipped,0) = 0
      AND coalesce(OI.bitDropShip,false) = false
      AND coalesce(OI.bitWorkOrder,false) = false
      AND coalesce(Item.IsArchieve,false) = false
      AND coalesce(Item.bitAssembly,false) = false
      AND coalesce(Item.bitKitParent,false) = false
      AND(coalesce(OKCI.numQtyItemsReq,0) -coalesce(OKCI.numQtyShipped,0)) > 0
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND OKCI.numItemID = v_numItemCode
      AND OKCI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval);
		
		-- USED IN WORK ORDER
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcpOppName ELSE 'Work Order' END,' (Assembly Memeber)')
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,0
			,CAST(CASE WHEN OM.numOppId IS NOT NULL THEN FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) ELSE FormatedDateFromDate(WO.bintCompliationDate,v_numDomainID) END AS VARCHAR(50))
      FROM
      WorkOrderDetails WOD
      INNER JOIN
      Item
      ON
      WOD.numChildItemID = Item.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      WOD.numWarehouseItemID = WI.numWareHouseItemID
      INNER JOIN
      WorkOrder WO
      ON
      WOD.numWOId = WO.numWOId
      LEFT JOIN
      OpportunityItems OI
      ON
      WO.numOppItemID = OI.numoppitemtCode
      LEFT JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      WO.numDomainId = v_numDomainID
      AND WO.numWOStatus <> 23184 -- NOT COMPLETED
      AND coalesce(Item.IsArchieve,0) = 0
      AND coalesce(Item.bitAssembly,0) = 0
      AND coalesce(Item.bitKitParent,0) = 0
      AND coalesce(WOD.numQtyItemsReq,0) > 0
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
      AND 1 =(CASE WHEN OM.numOppId IS NOT NULL THEN(CASE WHEN OM.tintoppstatus = 1 THEN 1 ELSE 0 END) ELSE 1 END)
      AND WOD.numChildItemID = v_numItemCode
      AND WOD.numWarehouseItemID = v_numWarehouseItemID
      AND 1 =(CASE
      WHEN OI.numoppitemtCode IS NOT NULL
      THEN(CASE
         WHEN coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
         THEN 1
         ELSE 0
         END)
      ELSE(CASE
         WHEN WO.bintCompliationDate <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
         THEN 1
         ELSE 0
         END)
      END);
   ELSEIF v_tintType = 2
   then -- Historical Sales
      IF coalesce(v_bitBasedOnLastYear,false) = true then --last week from last year
		
         v_dtToDate := TIMEZONE('UTC',now())+INTERVAL '-1 day'+INTERVAL '-1 year';
         v_dtFromDate := TIMEZONE('UTC',now())+CAST(-(v_numAnalysisDays) || 'day' as interval)+INTERVAL '-1 year';
      ELSE
         v_dtToDate := TIMEZONE('UTC',now())+INTERVAL '-1 day';
         v_dtFromDate := TIMEZONE('UTC',now())+CAST(-(v_numAnalysisDays) || 'day' as interval);
      end if;
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CAST(OM.vcpOppName AS VARCHAR(500))
			,OI.numUnitHour
			,OI.numUnitHour -coalesce(numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND numItemCode = v_numItemCode
      AND numWarehouseItmsID = v_numWarehouseItemID
      AND coalesce(bitDropship,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE);

		-- USED IN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq -coalesce(OKI.numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      OpportunityItems OI
      ON
      OKI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND OKI.numChildItemID = v_numItemCode
      AND OKI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE);

		-- USED IN KIT WITHIN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq -coalesce(OKCI.numQtyShipped,0)
			,0
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      OpportunityItems OI
      ON
      OKCI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND OKCI.numItemID = v_numItemCode
      AND OKCI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE);

		-- USED IN WORK ORDER
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcpOppName ELSE 'Work Order' END,' (Assembly Memeber)')
			,WOD.numQtyItemsReq
			,(CASE WHEN WO.numWOStatus = 23184 THEN 0 ELSE WOD.numQtyItemsReq END)
			,0
			,CAST(CASE WHEN OM.numOppId IS NOT NULL THEN FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) ELSE FormatedDateFromDate(WO.bintCompliationDate,v_numDomainID) END AS VARCHAR(50))
      FROM
      WorkOrderDetails WOD
      INNER JOIN
      WorkOrder WO
      ON
      WOD.numWOId = WO.numWOId
      LEFT JOIN
      OpportunityItems OI
      ON
      WO.numOppItemID = OI.numoppitemtCode
      LEFT JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      WO.numDomainId = v_numDomainID
      AND WOD.numChildItemID = v_numItemCode
      AND WOD.numWarehouseItemID = v_numWarehouseItemID
      AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE);
   ELSEIF v_tintType = 3
   then -- Sales Opportunity
	
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CAST(OM.vcpOppName AS VARCHAR(500))
			,OI.numUnitHour
			,OI.numUnitHour
			,OI.numUnitHour*(PP.intTotalProgress/100)
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND numItemCode = v_numItemCode
      AND numWarehouseItmsID = v_numWarehouseItemID
      AND coalesce(bitDropship,false) = false
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete;

		-- USED IN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq*(PP.intTotalProgress/100)
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      OpportunityItems OI
      ON
      OKI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND OKI.numChildItemID = v_numItemCode
      AND OKI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete;

		-- USED IN KIT WITHIN KIT
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcpOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq*(PP.intTotalProgress/100)
			,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      OpportunityItems OI
      ON
      OKCI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND OKCI.numItemID = v_numItemCode
      AND OKCI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete;

		-- USED IN WORK ORDER
      INSERT INTO tt_TEMP(numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate)
      SELECT
      OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcpOppName ELSE 'Work Order' END,' (Assembly Memeber)')
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq*(PP.intTotalProgress/100)
			,CAST(CASE WHEN OM.numOppId IS NOT NULL THEN FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) ELSE FormatedDateFromDate(WO.bintCompliationDate,v_numDomainID) END AS VARCHAR(50))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      WorkOrder WO
      ON
      OI.numoppitemtCode = WO.numOppItemID
      INNER JOIN
      WorkOrderDetails WOD
      ON
      WO.numWOId = WOD.numWOId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND WO.numDomainId = v_numDomainID
      AND WO.numWOStatus <> 23184 -- NOT COMPLETED
      AND WOD.numChildItemID = v_numItemCode
      AND WOD.numWarehouseItemID = v_numWarehouseItemID
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete;
   end if;

   open SWV_RefCur for SELECT * FROM tt_TEMP ORDER BY dtCreatedDate;
END; $$;












