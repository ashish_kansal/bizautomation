-- Function definition script fn_getOppPreviousStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getOppPreviousStage(v_numOppId NUMERIC(9,0),v_numCurrentStageId NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppStageId  NUMERIC(9,0);
   v_numPrevOppStageId  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numPrevOppStageId := 0;
   v_numOppStageId := 0;

   select   numOppStageId INTO v_numOppStageId from OpportunityStageDetails where numoppid = v_numOppId and numStagePercentage not in(0,100)   order by numOppStageId,numStagePercentage LIMIT 1;

   while v_numOppStageId > 0 LOOP
      if v_numOppStageId <> v_numCurrentStageId then
         v_numPrevOppStageId := v_numOppStageId;
      end if;
      if v_numOppStageId = v_numCurrentStageId then
         v_numOppStageId := 0;
      end if;
      if v_numOppStageId <> 0 then
		
         select   numOppStageId INTO v_numOppStageId from OpportunityStageDetails where numoppid = v_numOppId and numStagePercentage not in(0,100) and  numOppStageId > v_numOppStageId   order by numOppStageId,numStagePercentage LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numOppStageId := 0;
         end if;
      end if;
   END LOOP;

   return  v_numPrevOppStageId;
END; $$;

