-- Function definition script SplitIDs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SplitIDs(v_IdList VARCHAR(8000),
	v_Delimiter CHAR(1) DEFAULT '/')
RETURNS table
(
   Id INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Id  VARCHAR(10);
   v_Pos  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_SPLITIDS_PARSEDLIST CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_SPLITIDS_PARSEDLIST
   (
      Id INTEGER
   );
   v_IdList := LTRIM(RTRIM(v_IdList)) || coalesce(v_Delimiter,'');
   v_Pos := POSITION(v_Delimiter IN v_IdList);

   IF REPLACE(v_IdList,v_Delimiter,'') <> '' then
	
      WHILE v_Pos > 0 LOOP
         IF SWF_IsNumeric(LTRIM(RTRIM(SUBSTR(v_IdList,1,v_Pos -1)))) = true then
			
            v_Id := LTRIM(RTRIM(SUBSTR(v_IdList,1,v_Pos -1)));
            IF v_Id <> '' then
				
               INSERT INTO tt_SPLITIDS_PARSEDLIST(Id)
					VALUES(case when v_Id = '' then 0 else v_Id:: INTEGER end);
            end if;
         end if;
         v_IdList := SUBSTR(v_IdList,length(v_IdList) -(LENGTH(v_IdList) -v_Pos)+1);
         v_Pos := POSITION(v_Delimiter IN v_IdList);
      END LOOP;
   end if;	
   RETURN QUERY (SELECT * FROM tt_SPLITIDS_PARSEDLIST);
END; $$;

