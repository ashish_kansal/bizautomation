-- Function definition script CountChildren for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CountChildren(v_id INTEGER, v_cChildren INTEGER)
RETURNS BIGINT LANGUAGE plpgsql
   AS $$
BEGIN


--IF EXISTS (SELECT     
--    Sites.SiteCatID
--FROM         
--    dbo.Categories 
--INNER JOIN
--    dbo.Sites 
--ON 
--    dbo.Categories.CategoryID = dbo.Sites.SiteCatID
--WHERE 
--    dbo.Categories.ParentID = @id OR dbo.Sites.SiteCatID = @id)
--BEGIN 
--   SET @cChildren = @cChildren + (
--     SELECT 
--        Count(SiteCatID) 
--        FROM 
--            Sites 
--        WHERE 
--            SiteCatID = @id AND SiteActive = 1)
--  SELECT 
--              @cChildren = dbo.CountChildren(CategoryID, @cChildren) 
--            FROM 
--              Categories 
--            WHERE 
--              ParentID = @id 
--END 
   RETURN v_cChildren;
END; $$;

