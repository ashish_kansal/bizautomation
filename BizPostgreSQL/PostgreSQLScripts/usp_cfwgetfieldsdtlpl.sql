-- Stored procedure definition script USP_cfwGetFieldsDTLPL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_cfwGetFieldsDTLPL(v_PageId SMALLINT,                              
v_numRelation NUMERIC(9,0),                              
v_numRecordId NUMERIC(9,0),                  
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;                              
--if @PageId= 1                              
--begin                              
--select fld_id,fld_type,fld_label,numlistid,tintFldReq,numOrder,            
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL  from CFW_Fld_Master join CFW_Fld_Dtl                              
--on Fld_id=numFieldId                              
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0                       
-- union        
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID        
--order by subgrp,numOrder                             
--end                              
--                              
--if @PageId= 4                              
--begin                        
--declare @ContactId as numeric(9)                              
--select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
--where AddC.numContactID=@ContactId                              
--                              
--select fld_id,fld_type,fld_label,numlistid,tintFldReq,numOrder,              
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL from CFW_Fld_Master join CFW_Fld_Dtl                              
--on Fld_id=numFieldId                              
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0                           
----select @numRelation=numContactType from AdditionalContactsInformation AddC                              
----join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
----join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
----where AddC.numContactID=@numRecordId                              
--                              
--union        
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                            
--   order by subgrp,numOrder                              
--                              
--end                              
--                              
--if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8       or @PageId= 11                
--begin                       
--select fld_id,fld_type,fld_label,numlistid,              
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL from CFW_Fld_Master                               
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0        
--union      
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId       and numDomainID=@numDomainID                   
--end


