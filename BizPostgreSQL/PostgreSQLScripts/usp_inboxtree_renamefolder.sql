-- Stored procedure definition script USP_InboxTree_RenameFolder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InboxTree_RenameFolder(v_numDomainID NUMERIC(18,0),              
v_numUserCntID NUMERIC(18,0),
v_numNodeID NUMERIC(18,0),
v_vcName VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE InboxTreeSort SET vcNodeName = v_vcName WHERE numNodeID = v_numNodeID AND numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID;
   RETURN;
END; $$;


