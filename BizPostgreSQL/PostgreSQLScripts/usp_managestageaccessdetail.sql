-- Stored procedure definition script usp_ManageStageAccessDetail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ManageStageAccessDetail(v_numProjectID NUMERIC DEFAULT 0,
    v_numOppID NUMERIC DEFAULT 0,
	v_numStageDetailsId NUMERIC DEFAULT 0,
    v_numContactId NUMERIC DEFAULT 0,
	v_tintMode SMALLINT DEFAULT -1,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintMode = 0 then

      if EXISTS(select 1 from StageAccessDetail where numContactId = v_numContactId and
      numProjectID = v_numProjectID and numOppID = v_numOppID) then
	
         Update StageAccessDetail set numStageDetailsId = v_numStageDetailsId;
      Else
         insert into StageAccessDetail(numProjectID,numOppID,numStageDetailsId
      ,numContactId) values(v_numProjectID,v_numOppID,v_numStageDetailsId,v_numContactId);
      end if;
   Else
      open SWV_RefCur for select cast(numStageDetailsId as VARCHAR(255)) from StageAccessDetail where numContactId = v_numContactId and
      numProjectID = v_numProjectID and numOppID = v_numOppID;
   end if;
END; $$;













