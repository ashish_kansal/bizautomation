-- Stored procedure definition script USP_DeleteTask for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteTask(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numTaskId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskId AND tintAction = 4) then
	
      RAISE EXCEPTION 'TASK_IS_MARKED_AS_FINISHED';
   ELSEIF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskId)
   then
	
      RAISE EXCEPTION 'TASK_IS_ALREADY_STARTED';
   ELSEIF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND tintAction = 4 AND numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID = v_numDomainID AND SPDT.numParentTaskId = v_numTaskId))
   then
	
      RAISE EXCEPTION 'CHILD_TASK_IS_MARKED_AS_FINISHED';
   ELSEIF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID = v_numDomainID AND SPDT.numParentTaskId = v_numTaskId))
   then
	
      RAISE EXCEPTION 'CHILD_TASK_IS_ALREADY_STARTED';
   ELSEIF coalesce((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskId),0) > 0 AND NOT EXISTS(SELECT * FROM StagePercentageDetailsTask WHERE numStageDetailsId = coalesce((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskId),0) AND numTaskId <> v_numTaskId)
   then
	
      RAISE EXCEPTION 'STAGE_MUST_HAVE_AT_LEAST_ONE_TASK';
      RETURN;
   end if;

   DELETE FROM
   StagePercentageDetailsTask
   WHERE
   numDomainID = v_numDomainID AND numParentTaskId = v_numTaskId;

   DELETE FROM
   StagePercentageDetailsTask
   WHERE
   numDomainID = v_numDomainID AND numTaskId = v_numTaskId;
END; $$; 


