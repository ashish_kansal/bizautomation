-- Stored procedure definition script USP_CheckInventoryForLandedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckInventoryForLandedCost(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM OpportunityMaster WHERE numOppId = v_numOppID) > 0 then
	
      IF(SELECT
      COUNT(*)
      FROM
      OpportunityItems
      INNER JOIN
      Item
      ON
      OpportunityItems.numItemCode = Item.numItemCode
      LEFT JOIN LATERAL(SELECT
         coalesce((SUM(numOnHand)+SUM(numAllocation)),0) AS Qty
         FROM
         WareHouseItems
         WHERE
         numItemID = Item.numItemCode) AS TotalOnHand on TRUE
      WHERE
      numOppId = v_numOppID
      AND coalesce(numWarehouseItmsID,0) > 0
      AND TotalOnHand.Qty < OpportunityItems.numUnitHour) > 0 then
		
         open SWV_RefCur for
         SELECT 0;
      ELSE
         open SWV_RefCur for
         SELECT 1;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;

