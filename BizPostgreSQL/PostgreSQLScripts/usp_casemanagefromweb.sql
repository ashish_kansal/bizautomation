-- Stored procedure definition script USP_CaseManageFromWeb for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CaseManageFromWeb(INOUT v_numCaseId NUMERIC(9,0) DEFAULT 0 ,                      
v_numDivisionID NUMERIC(9,0) DEFAULT null,                      
v_numContactId NUMERIC(9,0) DEFAULT null,                      
v_vcCaseNumber VARCHAR(50) DEFAULT '',                      
v_textSubject VARCHAR(2000) DEFAULT '',                      
v_textDesc TEXT DEFAULT '',                      
v_numUserCntID NUMERIC(9,0) DEFAULT null,                                  
v_numDomainID NUMERIC(9,0) DEFAULT null,                      
v_tintSupportKeyType SMALLINT DEFAULT NULL,                      
v_intTargetResolveDate TIMESTAMP DEFAULT NULL,                      
v_numStatus NUMERIC(9,0) DEFAULT null,                      
v_numPriority NUMERIC(9,0) DEFAULT null,                      
v_textInternalComments TEXT DEFAULT '',                      
v_numReason NUMERIC(9,0) DEFAULT null,                      
v_numOrigin  NUMERIC(9,0) DEFAULT null,                      
v_numType NUMERIC(9,0) DEFAULT null,                      
v_numAssignedTo NUMERIC(9,0) DEFAULT 0,              
v_numContractId NUMERIC(9,0) DEFAULT 0,
v_vcEmail VARCHAR(50) DEFAULT null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPageID  SMALLINT;
   v_tempAssignedTo  NUMERIC(18,0) DEFAULT NULL;
BEGIN
   IF v_intTargetResolveDate = '1753-01-01 12:00:00 AM' then
	
      v_intTargetResolveDate := NULL;
   end if;

   IF v_numCaseId = 0 then
	 --  tinyint               
      IF EXISTS(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND vcEmail = v_vcEmail) then
		
         select   numContactId, numDivisionId INTO v_numContactId,v_numDivisionID FROM
         AdditionalContactsInformation WHERE
         numDomainID = v_numDomainID
         AND vcEmail = v_vcEmail    LIMIT 1;
      end if;
      INSERT INTO Cases(numDivisionID,
			numContactId,
			vcCaseNumber,
			textSubject,
			textDesc,
			numCreatedby,
			bintCreatedDate,
			numModifiedBy,
			bintModifiedDate,
			numDomainID,
			tintSupportKeyType,
			intTargetResolveDate,
			numRecOwner ,
			numContractID,
			numStatus,numPriority,numOrigin,numType,numReason,textInternalComments)
		 VALUES(v_numDivisionID,
			 v_numContactId,
			 v_vcCaseNumber,
			 v_textSubject,
			 v_textDesc,
			 v_numUserCntID,
			 TIMEZONE('UTC',now()),
			 v_numUserCntID,
			 TIMEZONE('UTC',now()),
			 v_numDomainID,
			 v_tintSupportKeyType,
			 v_intTargetResolveDate,
			 -1 ,
			 v_numContractId,
			 v_numStatus,v_numPriority,v_numOrigin,v_numType,v_numReason,v_textInternalComments);
		
      v_numCaseId := CURRVAL('Cases_seq');               
              
      
		--Map Custom Field	
		 --  numeric(18, 0)
		 --  numeric(18, 0)
		 --  numeric(18, 0)
      v_tintPageID := 3;
      PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numCaseId,v_numParentRecId := v_numDivisionID,
      v_tintPageID := v_tintPageID::SMALLINT);
   ELSE
      select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo FROM
      Cases WHERE
      numCaseId = v_numCaseId;
      IF (v_tempAssignedTo <> v_numAssignedTo and  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
		
         UPDATE
         Cases
         SET
         numAssignedTo = v_numAssignedTo,numAssignedBy = v_numUserCntID
         WHERE
         numCaseId = v_numCaseId;
      ELSEIF (v_numAssignedTo = 0)
      then
		
         UPDATE
         Cases
         SET
         numAssignedTo = 0,numAssignedBy = 0
         WHERE
         numCaseId = v_numCaseId;
      end if;
   end if;
   RETURN;
END; $$;


