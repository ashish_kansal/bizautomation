-- Stored procedure definition script USP_CategoryMatrixGroup_GetByCategoryID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CategoryMatrixGroup_GetByCategoryID(v_numDomainID NUMERIC(18,0)
	,v_numCategoryID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM CategoryMatrixGroup WHERE numDomainID = v_numDomainID AND numCategoryID = v_numCategoryID;
END; $$;













