CREATE OR REPLACE FUNCTION USP_SaveContractsContacts
(
	v_numContactID NUMERIC(18,0),
	v_numContractID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	DELETE FROM ContractsContact WHERE numContactId = v_numContactID AND numContractID = v_numContractID AND numDomainId = v_numDomainID;

	INSERT INTO ContractsContact(numContactId,numContractID,numDomainId) VALUES (v_numContactID,v_numContractID,v_numDomainID);
RETURN;
END; $$;


