-- Stored procedure definition script USP_GetPriceNamesTable for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPriceNamesTable(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		pricingNamesID,
        numDomainID,
        tintPriceLevel,
 		coalesce(vcPriceLevelName,'') AS vcPriceLevelName
	FROM
		PricingNamesTable
	WHERE
		numDomainID = v_numDomainID;
END; $$;
--USP_GetPriceTable 360












