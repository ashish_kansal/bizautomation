-- Stored procedure definition script USP_GetGLTransactionDetailById for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetGLTransactionDetailById(v_numDomainID INTEGER,
	  v_numTransactionID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT General_Journal_Details.numDomainId AS "numDomainId"
   , Chart_Of_Accounts.numAccountId AS "numAccountId"
   , General_Journal_Header.numJOurnal_Id as "JournalId",
FormatedDateFromDate(General_Journal_Header.datEntry_Date,General_Journal_Details.numDomainId) AS "Date",
General_Journal_Header.varDescription AS "varDescription",
CompanyInfo.vcCompanyName AS "CompanyName",
General_Journal_Details.numTransactionId AS "numTransactionId",
coalesce(General_Journal_Header.numOppBizDocsId,0) AS "numOppBizDocsId",
coalesce(General_Journal_Header.numCheckHeaderID,0) AS "CheckId",
coalesce(General_Journal_Header.numCashCreditCardId,0) AS "CashCreditCardId",
coalesce(General_Journal_Header.numOppId,0) AS "numOppId",
coalesce(General_Journal_Header.numDepositId,0) AS "numDepositId",
coalesce(General_Journal_Header.numCategoryHDRID,0) AS "numCategoryHDRID",
coalesce(DivisionMaster.numDivisionID,0) AS "numDivisionID",
coalesce(General_Journal_Header.numBillID,0) AS "numBillID",
coalesce(General_Journal_Header.numBillPaymentID,0) AS "numBillPaymentID",
coalesce(General_Journal_Details.numClassID,0) AS "numClassID",
coalesce(General_Journal_Details.numProjectID,0) AS "numProjectID",
coalesce(General_Journal_Header.numReturnID,0) AS "numReturnID",
coalesce(General_Journal_Details.numCurrencyID,0) AS "numCurrencyID",
coalesce(coalesce(VIEW_BIZPAYMENT.Narration,General_Journal_Header.varDescription) || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(SUBSTR(CAST(CH.numCheckNo AS VARCHAR(50)),1,50),'No-Cheq')
   ELSE  VIEW_JOURNALCHEQ.Narration
   END),
   '') as "Narration",
General_Journal_Details.vcReference AS "TranRef",
General_Journal_Details.varDescription AS "TranDesc",
coalesce(General_Journal_Details.numDebitAmt,0) AS "numDebitAmt",
coalesce(General_Journal_Details.numCreditAmt,0) AS "numCreditAmt",
Chart_Of_Accounts.vcAccountName AS "vcAccountName",
CAST('' AS CHAR(1)) as "balance",
CASE
   WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 'Checks'
   WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
   WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
   WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
   OpportunityMaster.tintopptype = 1 THEN 'BizDocs Invoice'
   WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
   OpportunityMaster.tintopptype = 2 THEN 'BizDocs Purchase'
   WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
   WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN 'Bill'
   WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 'Pay Bill'
   WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN
      CASE
      WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
      WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
      WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
      WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
      WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund'
      END
   WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
   WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 'Journal'
   END AS "TransactionType",
cast(coalesce(General_Journal_Details.bitCleared,false) as BOOLEAN) AS "bitCleared",
cast(coalesce(General_Journal_Details.bitReconcile,false) as BOOLEAN) AS "bitReconcile",
Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS "numLandedCostOppId"
   FROM General_Journal_Header INNER JOIN
   General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId INNER JOIN
   Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
   timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID
   LEFT OUTER JOIN DivisionMaster ON General_Journal_Details.numCustomerId = DivisionMaster.numDivisionID and
   General_Journal_Details.numDomainId = DivisionMaster.numDomainID
   LEFT OUTER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT OUTER JOIN OpportunityMaster ON General_Journal_Header.numOppId = OpportunityMaster.numOppId LEFT OUTER JOIN
   OpportunityBizDocs ON General_Journal_Header.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
   VIEW_JOURNALCHEQ ON (General_Journal_Header.numCheckHeaderID = VIEW_JOURNALCHEQ.numCheckHeaderID
   or (General_Journal_Header.numBillPaymentID = VIEW_JOURNALCHEQ.numReferenceID and VIEW_JOURNALCHEQ.tintReferenceType = 8)) LEFT OUTER JOIN
   VIEW_BIZPAYMENT ON General_Journal_Header.numBizDocsPaymentDetId = VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
   DepositMaster DM ON DM.numDepositId = General_Journal_Header.numDepositId  LEFT OUTER JOIN
   ReturnHeader RH ON RH.numReturnHeaderID = General_Journal_Header.numReturnID LEFT OUTER JOIN
   CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID AND CH.tintReferenceType = 10
   LEFT OUTER JOIN BillHeader BH  ON coalesce(General_Journal_Header.numBillID,0) = BH.numBillID
   WHERE General_Journal_Header.numDomainId = v_numDomainID
   AND numTransactionId = v_numTransactionID;
   RETURN;
END; $$;



