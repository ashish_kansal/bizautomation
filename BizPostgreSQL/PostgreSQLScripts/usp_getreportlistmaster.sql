DROP FUNCTION IF EXISTS USP_GetReportListMaster;
CREATE OR REPLACE FUNCTION USP_GetReportListMaster(v_numDomainId NUMERIC(18,0),  
v_numUserCntID NUMERIC(18,0),
v_numReportModuleID NUMERIC(18,0),   
v_CurrentPage INTEGER,                                                        
v_PageSize INTEGER,                                                        
INOUT v_TotRecs INTEGER ,     
v_SortChar CHAR(1) DEFAULT '0' ,                                                       
v_columnName VARCHAR(50) DEFAULT NULL,                                                        
v_columnSortOrder VARCHAR(50) DEFAULT NULL  ,
v_SearchStr VARCHAR(50) DEFAULT NULL,
v_tintReportType INTEGER DEFAULT NULL,
v_tintPerspective INTEGER DEFAULT NULL,
v_vcTimeline VARCHAR(100) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numGroupID  NUMERIC(18,0);
   v_strSql  VARCHAR(8000);                                                  
    
   v_firstRec  INTEGER;                                                        
   v_lastRec  INTEGER;
BEGIN


   select   coalesce(numGroupID,0) INTO v_numGroupID FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
     
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numReportId NUMERIC(18,0)
   );     
   v_strSql := CONCAT('Select ReportListMaster.numReportID from ReportListMaster INNER JOIN ReportDashboardAllowedReports ON ReportListMaster.numReportID = ReportDashboardAllowedReports.numReportID  WHERE ReportListMaster.numdomainid=',v_numDomainId,' AND ReportDashboardAllowedReports.numDomainID=',v_numDomainId,
   ' AND ReportDashboardAllowedReports.numGrpID=',v_numGroupID);
   IF v_numReportModuleID <> 0 then
	
      v_strSql := coalesce(v_strSql,'') || ' And numReportModuleID = ' || SUBSTR(CAST(v_numReportModuleID AS VARCHAR(15)),1,15) || '';
   end if;

   IF v_SearchStr <> '' then
	
      v_strSql := coalesce(v_strSql,'') || ' AND LOWER(vcReportName) iLike ''%' || LOWER(v_SearchStr) || '%''';
   end if;

   IF v_tintReportType <> -1 then
	
      v_strSql := coalesce(v_strSql,'') || CONCAT(' AND tintReportType = ',v_tintReportType);
   end if;

   IF v_tintPerspective <> -1 then
	
      v_strSql := coalesce(v_strSql,'') || CONCAT(' AND tintRecordFilter = ',v_tintPerspective);
   end if;

   IF LENGTH(coalesce(v_vcTimeline,'')) > 0 AND coalesce(v_vcTimeline,'') <> '-1' then
	
      v_strSql := coalesce(v_strSql,'') || CONCAT(' AND vcDateFieldValue = ''',v_vcTimeline,'''');
   end if;
    
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY ReportListMaster.' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');    
    
   RAISE NOTICE '%',v_strSql;
   EXECUTE 'INSERT INTO tt_TEMPTABLE(numReportID) ' || v_strSql;    
    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                        
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                         

   select   COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLE;	
	
   open SWV_RefCur for
   SELECT
   RLM.numReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,RMM.vcModuleName
		,RMGM.vcGroupName
		,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
		,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
		,(CASE
   WHEN coalesce(RLM.numDateFieldID,0) > 0 AND LENGTH(coalesce(RLM.vcDateFieldValue,'')) > 0
   THEN
      CONCAT((CASE WHEN bitDateFieldColumnCustom = true THEN(SELECT FLd_label FROM CFW_Fld_Master WHERE Fld_id = numDateFieldID) ELSE(SELECT vcFieldName FROM DycFieldMaster WHERE numFieldID = numDateFieldID) END),' (',(CASE RLM.vcDateFieldValue
      WHEN 'AllTime' THEN 'All Time'
      WHEN 'Custom' THEN 'Custom'
      WHEN 'CurYear' THEN 'Current CY'
      WHEN 'PreYear' THEN 'Previous CY'
      WHEN 'Pre2Year' THEN 'Previous 2 CY'
      WHEN 'Ago2Year' THEN '2 CY Ago'
      WHEN 'NextYear' THEN 'Next CY'
      WHEN 'CurPreYear' THEN 'Current and Previous CY'
      WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
      WHEN 'CurNextYear' THEN 'Current and Next CY'
      WHEN 'CuQur' THEN 'Current CQ'
      WHEN 'CurNextQur' THEN 'Current and Next CQ'
      WHEN 'CurPreQur' THEN 'Current and Previous CQ'
      WHEN 'NextQur' THEN 'Next CQ'
      WHEN 'PreQur' THEN 'Previous CQ'
      WHEN 'LastMonth' THEN 'Last Month'
      WHEN 'ThisMonth' THEN 'This Month'
      WHEN 'NextMonth' THEN 'Next Month'
      WHEN 'CurPreMonth' THEN 'Current and Previous Month'
      WHEN 'CurNextMonth' THEN 'Current and Next Month'
      WHEN 'LastWeek' THEN 'Last Week'
      WHEN 'ThisWeek' THEN 'This Week'
      WHEN 'NextWeek' THEN 'Next Week'
      WHEN 'Yesterday' THEN 'Yesterday'
      WHEN 'Today' THEN 'Today'
      WHEN 'Tomorrow' THEN 'Tomorrow'
      WHEN 'Last7Day' THEN 'Last 7 Days'
      WHEN 'Last30Day' THEN 'Last 30 Days'
      WHEN 'Last60Day' THEN 'Last 60 Days'
      WHEN 'Last90Day' THEN 'Last 90 Days'
      WHEN 'Last120Day' THEN 'Last 120 Days'
      WHEN 'Next7Day' THEN 'Next 7 Days'
      WHEN 'Next30Day' THEN 'Next 30 Days'
      WHEN 'Next60Day' THEN 'Next 60 Days'
      WHEN 'Next90Day' THEN 'Next 90 Days'
      WHEN 'Next120Day' THEN 'Next 120 Days'
      ELSE '-'
      END),
      ')')
   ELSE
      ''
   END) AS vcTimeline,
		(CASE WHEN(SELECT COUNT(*) FROM ReportFilterList WHERE numReportID = RLM.numReportID) > 0 THEN COALESCE((SELECT DISTINCT string_agg(vcFilterValue,', ') FROM ReportFilterList WHERE numReportID = RLM.numReportID AND numFieldID = 199),'') ELSE '' END) AS vcLocationSource
   FROM
   ReportListMaster RLM
   JOIN
   tt_TEMPTABLE T
   ON
   T.numReportId = RLM.numReportID
   JOIN
   ReportModuleMaster RMM
   ON
   RLM.numReportModuleID = RMM.numReportModuleID
   JOIN
   ReportModuleGroupMaster RMGM
   ON
   RLM.numReportModuleGroupID = RMGM.numReportModuleGroupID
   WHERE
   ID > v_firstRec
   AND ID < v_lastRec
   ORDER BY
   ID;
   
   RETURN;
END; $$;


