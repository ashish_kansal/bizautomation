CREATE OR REPLACE FUNCTION usp_DeleteMilestoneStage(v_numDomainid NUMERIC DEFAULT 0,
    v_numProjectID NUMERIC DEFAULT 0 ,
    v_numStageDetailsID NUMERIC DEFAULT 0,
   v_tintModeType SMALLINT DEFAULT -1)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM    ProjectsOpportunities
   WHERE   numDomainId = v_numDomainid
   AND v_numProjectID =(CASE v_tintModeType
   WHEN 0 THEN numOppId
   WHEN 1 THEN numProId
   END) AND numStageID = v_numStageDetailsID) then
    
      RAISE EXCEPTION 'DEPENDANT';
      RETURN;
   end if;	
    
   DELETE FROM Comments WHERE numStageID = v_numStageDetailsID;

   delete from StageDependency where numStageDetailId = v_numStageDetailsID or
   numDependantOnID = v_numStageDetailsID;


   DELETE FROM StageAccessDetail WHERE numStageDetailsId = v_numStageDetailsID and
   v_numProjectID =(case v_tintModeType when 0 then numOppID
   when 1 then numProjectID end);

   DELETE FROM DocumentWorkflow WHERE numDocID IN(SELECT numGenericDocID FROM GenericDocuments WHERE vcDocumentSection = 'PS' AND
   numRecID IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId = v_numStageDetailsID and
      v_numProjectID =(case v_tintModeType when 0 then numOppid
      when 1 then numProjectid end) AND numdomainid = v_numDomainid) AND numDomainId = v_numDomainid); 

   DELETE FROM GenericDocuments WHERE vcDocumentSection = 'PS' AND
   numRecID IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId = v_numStageDetailsID and
   v_numProjectID =(case v_tintModeType when 0 then numOppid
   when 1 then numProjectid end) AND numdomainid = v_numDomainid) AND numDomainId = v_numDomainid;
	
   delete from StagePercentageDetails where
   v_numProjectID =(case v_tintModeType when 0 then numOppid
   when 1 then numProjectid end)
   and numStageDetailsId = v_numStageDetailsID
   and numdomainid = v_numDomainid;
END; $$; 



