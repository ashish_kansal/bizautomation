CREATE OR REPLACE FUNCTION usp_vendorleadtimes_getbyid
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numdivisionid NUMERIC(18,0)
	,p_numvendorleadtimesid NUMERIC(18,0)
	,INOUT p_swvrefcur REFCURSOR
)
LANGUAGE plpgsql
   AS $$
BEGIN
	OPEN p_swvrefcur FOR
	SELECT 
		vcdata
	FROM   
		VendorLeadTimes VLT
	INNER JOIN 
		DivisionMaster DM 
	ON 
		VLT.numdivisionid = DM.numdivisionid
	WHERE
		VLT.numdivisionid = p_numdivisionid
		AND VLT.numvendorleadtimesid = p_numvendorleadtimesid
		AND DM.numdomainid = p_numdomainid;
END; $$;