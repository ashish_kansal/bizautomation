-- Function definition script ufn_GetDaysInYear for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION ufn_GetDaysInYear(v_pDate    TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_IsLeapYear SMALLINT;
BEGIN
   v_IsLeapYear := 0;
   IF (MOD(CAST(EXTRACT(YEAR FROM v_pDate) AS INTEGER),4) = 0 AND MOD(CAST(EXTRACT(YEAR FROM v_pDate) AS INTEGER),100) != 0) OR
   MOD(CAST(EXTRACT(YEAR FROM v_pDate) AS INTEGER),400) = 0 then
      v_IsLeapYear := 1;
   end if;

   RETURN 365 + v_IsLeapYear;
END; $$;

