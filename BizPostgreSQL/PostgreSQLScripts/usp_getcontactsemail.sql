CREATE OR REPLACE FUNCTION USP_GetContactsEmail(v_numContactId NUMERIC(9,0),
v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      select vcEmail from AdditionalContactsInformation where numContactId = v_numContactId;
   ELSEIF v_tintMode = 1
   then --select primary contact's email id from secondary contact id 

      open SWV_RefCur for
      SELECT coalesce(vcEmail,'') AS vcEmail FROM AdditionalContactsInformation WHERE coalesce(bitPrimaryContact,false) = true AND numDivisionId IN(select numDivisionId from AdditionalContactsInformation where numContactId = v_numContactId);
   end if;
   RETURN;
END; $$;
	



