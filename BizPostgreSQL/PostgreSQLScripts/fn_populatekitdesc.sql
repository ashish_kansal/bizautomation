CREATE OR REPLACE FUNCTION fn_PopulateKitDesc(v_numOppId NUMERIC,v_numOppItemID NUMERIC)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Desc  TEXT;
BEGIN
   v_Desc := COALESCE((SELECT string_agg(coalesce(I.vcItemName,'') || CASE 
																		WHEN charitemtype = 'P' 
																		THEN '(' || CAST(CAST((OKI.numQtyItemsReq/OKI.numQtyItemsReq_Orig)*(OKI.numQtyItemsReq_Orig/fn_UOMConversion(OKI.numUOMId,0::NUMERIC,I.numDomainID,I.numBaseUnit)) AS DECIMAL(10,2)) AS VARCHAR(30)) || ' ' || coalesce(UOM.vcUnitName,'Units') || ')' 
																		ELSE '' 
																	END,', ' ORDER BY numOppChildItemID ASC)
   FROM  OpportunityKitItems OKI JOIN Item I ON OKI.numChildItemID = I.numItemCode JOIN UOM ON UOM.numUOMId = OKI.numUOMId
   WHERE OKI.numOppId = v_numOppId AND OKI.numOppItemID = v_numOppItemID),'');
	
   return COALESCE(v_Desc,'');
END; $$;

