-- Stored procedure definition script Usp_DelScheduled for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_DelScheduled(v_numScheduleId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM CustRptSchContacts where numScheduleId = v_numScheduleId;
   delete FROM CustRptScheduler where numScheduleid = v_numScheduleId;
   RETURN;
END; $$;


