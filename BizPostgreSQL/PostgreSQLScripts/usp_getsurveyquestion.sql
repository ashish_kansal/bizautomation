-- Stored procedure definition script usp_GetSurveyQuestion for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyQuestion(v_numDomainId NUMERIC(9,0),
 v_SurveyId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all survey questions              
   AS $$
BEGIN
   open SWV_RefCur for SELECT CAST(numQID AS VARCHAR(10)) || '~' || CAST(tIntAnsType AS VARCHAR(10)) AS "numQID", vcQuestion as "vcQuestion"
   FROM SurveyQuestionMaster
   WHERE numSurID = v_SurveyId;
END; $$;












