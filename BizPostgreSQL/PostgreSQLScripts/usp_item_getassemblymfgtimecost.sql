-- Stored procedure definition script USP_Item_GetAssemblyMfgTimeCost for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetAssemblyMfgTimeCost(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_numQtyToBuild NUMERIC(18,0)
	,v_numBuildProcessID NUMERIC(18,0)
	,v_dtProjectedStartDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtProjectedFinishDate  TIMESTAMP DEFAULT v_dtProjectedStartDate;
   v_monLaborCost  DECIMAL(20,5) DEFAULT 0;
   v_numOverheadServiceItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(numOverheadServiceItemID,0) INTO v_numOverheadServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   IF coalesce(v_numBuildProcessID,0) = 0 then
	
      select   coalesce(numBusinessProcessId,0) INTO v_numBuildProcessID FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
   end if;

   IF coalesce(v_numQtyToBuild,0) = 0 then
	
      v_numQtyToBuild := 1;
   end if;

   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numAssignedTo NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );
	
   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_GetAssemblyMfgTimeCost CASCADE;
   CREATE TEMPORARY TABLE tt_GetAssemblyMfgTimeCost
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      numTaskAssignee NUMERIC(18,0),
      monHourlyRate DECIMAL(20,5),
      intTaskType INTEGER, --1:Parallel, 2:Sequential
      dtPlannedStartDate TIMESTAMP,
      dtFinishDate TIMESTAMP
   );

   INSERT INTO tt_GetAssemblyMfgTimeCost
   (numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
		,monHourlyRate)
   SELECT
   SPDT.numTaskId
		,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))
		,coalesce(SPDT.intTaskType,1)
		,SPDT.numAssignTo
		,coalesce(UM.monHourlyRate,0)
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   SPD.numStageDetailsId = SPDT.numStageDetailsId
   INNER JOIN
   UserMaster UM
   ON
   SPDT.numAssignTo = UM.numUserDetailId
   WHERE
   SPD.numdomainid = v_numDomainID
   AND SPD.slp_id = v_numBuildProcessID
   AND coalesce(SPDT.numWorkOrderId,0) = 0;

	
   v_dtProjectedFinishDate := coalesce(GetProjectedFinish(v_numDomainID,1::SMALLINT,v_numItemCode,v_numQtyToBuild,v_numBuildProcessID,v_dtProjectedStartDate,
   v_ClientTimeZoneOffset,v_numWarehouseID),v_dtProjectedStartDate);

   v_monLaborCost := coalesce((SELECT SUM(t.numTaskTimeInMinutes*(t.monHourlyRate/60)) FROM tt_GetAssemblyMfgTimeCost AS t),0);
	
   open SWV_RefCur for SELECT
   v_dtProjectedFinishDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtProjectedFinishDate
		,cast(coalesce((SELECT SUM(coalesce(ID.numQtyItemsReq,0)*coalesce(IChild.monAverageCost,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID = IChild.numItemCode WHERE ID.numItemKitID = v_numItemCode AND ID.numChildItemID <> v_numOverheadServiceItemID),
   0) as VARCHAR(255)) AS monMaterialCost
		,cast(coalesce((SELECT SUM(coalesce(ID.numQtyItemsReq,0)*coalesce(IChild.monListPrice,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID = IChild.numItemCode WHERE ID.numItemKitID = v_numItemCode AND ID.numChildItemID = v_numOverheadServiceItemID),0) as VARCHAR(255)) AS monMFGOverhead
		,v_monLaborCost AS monLaborCost;
END; $$;












