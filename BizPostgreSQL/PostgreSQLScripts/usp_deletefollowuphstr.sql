-- Stored procedure definition script USP_DeleteFollowUpHstr for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteFollowUpHstr(v_numFollowUpStatusID NUMERIC(9,0),
v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from FollowUpHistory where numFollowUpStatusID = v_numFollowUpStatusID  and numDomainID = v_numDomainId;
   RETURN;
END; $$;


