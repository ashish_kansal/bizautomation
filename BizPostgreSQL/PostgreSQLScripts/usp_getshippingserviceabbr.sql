-- Stored procedure definition script USP_GetShippingServiceAbbr for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingServiceAbbr(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numShipViaID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(SS.numShippingServiceID as VARCHAR(255))
		,cast(SS.numShipViaID as VARCHAR(255))
		,cast(SS.vcShipmentService as VARCHAR(255))
		,cast(coalesce(SSA.vcAbbreviation,'') as VARCHAR(255)) AS vcAbbreviation
   FROM
   ShippingServiceAbbreviations  SSA
   LEFT JOIN
   ShippingService SS
   ON
   SSA.numShippingServiceID = SS.numShippingServiceID
   WHERE
   SSA.numDomainID = v_numDomainID
   AND SS.numShipViaID = v_numShipViaID
   ORDER BY
   SS.numShippingServiceID;
END; $$;












