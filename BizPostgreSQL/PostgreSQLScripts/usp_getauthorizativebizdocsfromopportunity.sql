-- Stored procedure definition script USP_GetAuthorizativeBizDocsFromOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAuthorizativeBizDocsFromOpportunity(v_numAuthorizativeSalesId NUMERIC(9,0) DEFAULT 0,    
v_numAuthorizativePurchaseId NUMERIC(9,0) DEFAULT 0,     
v_numDomainId NUMERIC(9,0) DEFAULT 0,    
INOUT v_numCountSalesBizDocs NUMERIC(9,0) DEFAULT 0 ,    
INOUT v_numCountPurchaseBizDocs NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   Count(*) INTO v_numCountSalesBizDocs From OpportunityMaster OM
   Inner Join OpportunityBizDocs OppBizDocs On OM.numOppId = OppBizDocs.numoppid Where OppBizDocs.numBizDocId = v_numAuthorizativeSalesId And OM.numDomainId = v_numDomainId And  coalesce(OppBizDocs.bitAuthoritativeBizDocs,0) = 1;    
    
   select   Count(*) INTO v_numCountPurchaseBizDocs From OpportunityMaster OM
   Inner Join OpportunityBizDocs OppBizDocs On OM.numOppId = OppBizDocs.numoppid Where OppBizDocs.numBizDocId = v_numAuthorizativePurchaseId And OM.numDomainId = v_numDomainId And  coalesce(OppBizDocs.bitAuthoritativeBizDocs,0) = 1;
   RETURN;
END; $$;


