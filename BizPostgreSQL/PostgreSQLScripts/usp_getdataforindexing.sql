DROP FUNCTION IF EXISTS USP_GetDataForIndexing;

CREATE OR REPLACE FUNCTION USP_GetDataForIndexing( --Used for rebuilding all index from scratch
v_tintMode SMALLINT DEFAULT 0,
    v_numDomainID NUMERIC DEFAULT NULL,
    v_numSiteID NUMERIC DEFAULT NULL,
    v_bitSchemaOnly BOOLEAN DEFAULT false,
    v_tintAddUpdateMode SMALLINT DEFAULT NULL, -- 1 = add , 2= update ,3=Deleted items
    v_bitDeleteAll BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(4000);
   v_Fields  VARCHAR(1000);
   v_numFldID  VARCHAR(15);
   v_vcFldname  VARCHAR(200);
   v_vcDBFldname  VARCHAR(200);
   v_vcLookBackTableName  VARCHAR(200);
   v_intRowNum  INTEGER;
   v_strSQLUpdate  VARCHAR(4000);
   v_Custom  BOOLEAN;
   SWV_RowCount INTEGER;
BEGIN
   IF v_tintMode = 0 then -- items 
        

      DROP TABLE IF EXISTS tt_TEMPCUSTOMFIELD CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCUSTOMFIELD AS
         SELECT  ROW_NUMBER()
         OVER(ORDER BY vcFormFieldName) AS tintOrder,* FROM(SELECT   distinct numFieldID as numFormFieldId,
										vcDbColumnName,
										vcFieldName as vcFormFieldName,
										0 AS Custom,
										vcLookBackTableName
         FROM      View_DynamicColumns
         WHERE     numFormId = 30
         AND numDomainID = v_numDomainID
--										AND DFC.tintPageType = 1 --Commented so can agreegate all fields of simple and advance search
         AND coalesce(numRelCntType,0) = 0
         AND coalesce(bitCustom,false) = false
         AND coalesce(bitSettingField,false) = true
         UNION
         SELECT    numFieldId as numFormFieldId,
										vcFieldName,
										vcFieldName,
										1 AS Custom,
										'' AS vcLookBackTableName
         FROM      View_DynamicCustomColumns
         WHERE     Grp_id = 5
         AND numFormId = 30
         AND numDomainID = v_numDomainID
         AND coalesce(numRelCntType,0) = 0) X;
      IF v_bitSchemaOnly = true then
       
         open SWV_RefCur for
         SELECT * FROM tt_TEMPCUSTOMFIELD;
         RETURN;
      end if;
      IF v_bitDeleteAll = true then
	   
         DELETE FROM LuceneItemsIndex WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteID;
      end if;
       
       --Create Fields query 
      v_Fields := '';
      select(tintOrder+1), numFormFieldId, vcFormFieldName, vcDbColumnName, vcLookBackTableName INTO v_intRowNum,v_numFldID,v_vcFldname,v_vcDBFldname,v_vcLookBackTableName FROM tt_TEMPCUSTOMFIELD WHERE Custom = 0   ORDER BY tintOrder LIMIT 1;
      while v_intRowNum > 0 LOOP
--					PRINT @numFldID
         IF(v_vcDBFldname = 'numItemCode') then
            v_vcDBFldname := 'I.numItemCode';
         end if;
         v_Fields := coalesce(v_Fields,'') || ','  || coalesce(v_vcDBFldname,'') || ' as "'  || coalesce(v_vcFldname,'') || '"';
         select(tintOrder+1), numFormFieldId, vcFormFieldName, vcDbColumnName, vcLookBackTableName INTO v_intRowNum,v_numFldID,v_vcFldname,v_vcDBFldname,v_vcLookBackTableName FROM tt_TEMPCUSTOMFIELD WHERE tintOrder >= v_intRowNum AND Custom = 0   ORDER BY tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_intRowNum := 0;
         end if;
      END LOOP;
       
--       PRINT @Fields
       
       
      v_strSQL :=  'DROP TABLE IF EXISTS tt_DATAFORINDEXING CASCADE;
      CREATE TEMPORARY TABLE tt_DATAFORINDEXING AS  SELECT I.numItemCode AS numItemCode
			' || coalesce(v_Fields,'') || '            
            FROM    Item I 
            LEFT JOIN ItemExtendedDetails IED
            ON I.numItemCode = IED.numItemCode
            WHERE   I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || '
                    AND I.numItemCode IN (
                    SELECT  numItemID
                    FROM    ItemCategory
                    WHERE COALESCE(I.IsArchieve,false) = false AND  numCategoryID IN ( SELECT   numCategoryID
                                               FROM     SiteCategories
                                               WHERE    numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || ' )) ';
      IF v_tintAddUpdateMode = 1 then -- Add mode
         v_strSQL := coalesce(v_strSQL,'') || ' and I.numItemCode Not in (SELECT numItemCode FROM LuceneItemsIndex WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) ||  ' and numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || ') ';
      end if;
      IF v_tintAddUpdateMode = 2 then -- Update mode
         v_strSQL := coalesce(v_strSQL,'') || ' and bintModifiedDate BETWEEN timezone(''utc'', now()) + make_interval(hours => -1) AND timezone(''utc'', now())
			AND I.numItemCode in(SELECT I1.numItemCode FROM Item I1 INNER JOIN ItemCategory IC ON I1.numItemCode = IC.numItemID
			INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID
			WHERE SC.numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || ' AND I1.numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')';
      end if;
      IF v_tintAddUpdateMode = 3 then -- Delete mode
		
         v_strSQL := 'SELECT numItemCode FROM LuceneItemsIndex WHERE bitItemDeleted = true and numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) ||  ' and numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10);
         OPEN SWV_RefCur FOR EXECUTE v_strSQL;
         RETURN;
      end if;

	  v_strSQL := CONCAT(v_strSQL,(CASE WHEN v_tintAddUpdateMode = 1 THEN 'LIMIT 1000;' ELSE ';' END));

	    RAISE NOTICE '%',v_strSQL;
	  EXECUTE coalesce(v_strSQL,'');

      v_strSQLUpdate := '';
			--Add  custom Fields and data to table
      select(tintOrder+1), numFormFieldId, vcFormFieldName INTO v_intRowNum,v_numFldID,v_vcFldname FROM tt_TEMPCUSTOMFIELD WHERE Custom = 1   ORDER BY tintOrder LIMIT 1;
      while v_intRowNum > 0 LOOP
--				 PRINT @numFldID
				EXECUTE ' alter table tt_DATAFORINDEXING add "' || coalesce(v_vcFldname,'') || '_CustomField" VARCHAR(500); ';
        
				EXECUTE ' update tt_DATAFORINDEXING set "' || coalesce(v_vcFldname,'') || '_CustomField" = GetCustFldValueItem(' || coalesce(v_numFldID,'') || ',numItemCode) where numItemCode > 0;';
         
		 select(tintOrder+1), numFormFieldId, vcFormFieldName INTO v_intRowNum,v_numFldID,v_vcFldname FROM tt_TEMPCUSTOMFIELD WHERE tintOrder > v_intRowNum AND Custom = 1   ORDER BY tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_intRowNum := 0;
         end if;
      END LOOP;

	  IF v_bitSchemaOnly = false then
				 
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ' INSERT INTO LuceneItemsIndex
				 	 SELECT numItemCode,true,false,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || '  FROM tt_DATAFORINDEXING where numItemCode not in(select numItemCode from LuceneItemsIndex where numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || ' );';
      end if;

	

      RAISE NOTICE '%',v_strSQLUpdate;
	  EXECUTE v_strSQLUpdate;
      OPEN SWV_RefCur FOR EXECUTE ' SELECT * FROM tt_DATAFORINDEXING; ';
   end if;       
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/



