-- Stored procedure definition script Resource_RemByName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Resource_RemByName(v_ResourceName	VARCHAR(64)	-- name of the resource to delete
)
RETURNS VOID LANGUAGE plpgsql
	-- Start a transaction to commit both deletions on the Resource
	-- and ActivityResource tables simultaneously.
   AS $$
   DECLARE
   v_ResourceID  INTEGER;
BEGIN
   -- BEGIN TRANSACTION
select   Resource.ResourceID INTO v_ResourceID FROM
   Resource WHERE
   Resource.ResourceName = v_ResourceName;

   IF (v_ResourceID IS NOT NULL) then
	
      UPDATE
      ActivityResource
      SET
      ResourceID =(-999)
      WHERE
      ActivityResource.ResourceID = v_ResourceID;
      DELETE FROM ResourcePreference
      WHERE  ResourcePreference.ResourceID = v_ResourceID;
      DELETE FROM Resource
      WHERE  Resource.ResourceID = v_ResourceID;
      -- COMMIT
ELSE
      NULL;
      -- ROLLBACK
end if;
   RETURN;
END; $$;


