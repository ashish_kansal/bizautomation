-- Stored procedure definition script usp_GetAuthorizationGroupsForSelectedUser for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAuthorizationGroupsForSelectedUser(v_numUserID NUMERIC(9,0)   
--    
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numGroupID as VARCHAR(255)) FROM UserMaster
   WHERE numUserId = v_numUserID
   ORDER BY numGroupID;
END; $$;












