-- Stored procedure definition script USP_SalesFulfillmentQueue_UpdateStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SalesFulfillmentQueue_UpdateStatus(v_numDomainID NUMERIC(18,0),  
	v_numSFQID NUMERIC(18,0),
	v_vcMessage TEXT,
	v_tintRule SMALLINT,
	v_bitSuccess BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numUserCntID  NUMERIC(18,0);
   v_numOppID  NUMERIC(18,0);
   v_numRule2SuccessOrderStatus  NUMERIC(18,0);
   v_numRule2FailOrderStatus  NUMERIC(18,0);
   v_numRule3FailOrderStatus  NUMERIC(18,0);
   v_numRule4FailOrderStatus  NUMERIC(18,0);
   v_numRule5OrderStatus  NUMERIC(18,0);
   v_numRule6OrderStatus  NUMERIC(18,0);
   v_bitActive  BOOLEAN;
   v_bitRule5IsActive  BOOLEAN;
   v_bitRule6IsActive  BOOLEAN;
   v_numTempOppID  NUMERIC(18,0);
BEGIN
   v_numTempOppID := v_numSFQID;

   select   bitActive, numRule2SuccessOrderStatus, numRule2FailOrderStatus, numRule3FailOrderStatus, numRule4FailOrderStatus, bitRule5IsActive, numRule5OrderStatus, bitRule6IsActive, numRule6OrderStatus INTO v_bitActive,v_numRule2SuccessOrderStatus,v_numRule2FailOrderStatus,v_numRule3FailOrderStatus,
   v_numRule4FailOrderStatus,v_bitRule5IsActive,v_numRule5OrderStatus,
   v_bitRule6IsActive,v_numRule6OrderStatus FROM
   SalesFulfillmentConfiguration WHERE
   numDomainID = v_numDomainID;

   select   numOppID, numUserCntID INTO v_numOppID,v_numUserCntID FROM
   SalesFulfillmentQueue WHERE
   numSFQID = v_numSFQID
   AND v_tintRule NOT IN(5,6);

	-- CHANGE ORDER STATUS BASED ON SALES FULFILLMENT CONFIGURATION
   IF v_tintRule = 2 then
	
      IF v_bitSuccess = true then
		
         UPDATE OpportunityMaster SET numStatus = v_numRule2SuccessOrderStatus WHERE numOppId = v_numOppID;
         PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numRule2SuccessOrderStatus);
      ELSE
         UPDATE OpportunityMaster SET numStatus = v_numRule2FailOrderStatus WHERE numOppId = v_numOppID;
         PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numRule2FailOrderStatus);
      end if;
   ELSEIF v_tintRule = 3 AND coalesce(v_bitSuccess,false) = false
   then
	
      UPDATE OpportunityMaster SET numStatus = v_numRule3FailOrderStatus WHERE numOppId = v_numOppID;
      PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numRule3FailOrderStatus);
   ELSEIF v_tintRule = 4 AND coalesce(v_bitSuccess,false) = false
   then
	
      UPDATE OpportunityMaster SET numStatus = v_numRule4FailOrderStatus WHERE numOppId = v_numOppID;
      PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numRule4FailOrderStatus);
   ELSEIF v_tintRule = 5 AND coalesce(v_bitRule5IsActive,false) = true AND coalesce(v_bitActive,false) = true
   then
	
      UPDATE OpportunityMaster SET numStatus = v_numRule5OrderStatus WHERE numOppId = v_numSFQID;
      IF(SELECT
      COUNT(*)
      FROM
      SalesFulfillmentConfiguration
      WHERE
      numDomainID = v_numDomainID
      AND coalesce(bitActive,false) = true
      ANd coalesce(bitRule5IsActive,false) = true) > 0
      AND NOT EXISTS(SELECT * FROM SalesFulfillmentQueue WHERE numOppID = v_numOppID AND numOrderStatus = -1) then
		
         IF NOT EXISTS(SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID = v_numSFQID AND vcMessage = v_vcMessage) then
			
            INSERT INTO SalesFulfillmentQueue(numDomainID
					,numUserCntID
					,numOppID
					,numOrderStatus
					,dtDate
					,bitExecuted
					,intNoOfTimesTried)
				VALUES(v_numDomainID
					,0
					,v_numSFQID
					,-1
					,TIMEZONE('UTC',now())
					,1
					,1) RETURNING numSFQID INTO v_numSFQID;
         end if;
      end if;
   ELSEIF v_tintRule = 6 AND coalesce(v_bitRule6IsActive,false) = true AND coalesce(v_bitActive,false) = true
   then
	
      UPDATE OpportunityMaster SET numStatus = v_numRule6OrderStatus WHERE numOppId = v_numSFQID;
      IF(SELECT
      COUNT(*)
      FROM
      SalesFulfillmentConfiguration
      WHERE
      numDomainID = v_numDomainID
      AND coalesce(bitActive,false) = true
      ANd coalesce(bitRule6IsActive,false) = true) > 0
      AND NOT EXISTS(SELECT * FROM SalesFulfillmentQueue WHERE numOppID = v_numOppID AND numOrderStatus = -2) then
		
         INSERT INTO SalesFulfillmentQueue(numDomainID
				,numUserCntID
				,numOppID
				,numOrderStatus
				,dtDate
				,bitExecuted
				,intNoOfTimesTried)
			VALUES(v_numDomainID
				,0
				,v_numSFQID
				,-2
				,TIMEZONE('UTC',now())
				,1
				,1) RETURNING numSFQID INTO v_numSFQID;
      end if;
   end if;

	-- MARK RULE AS EXECUTED
   UPDATE
   SalesFulfillmentQueue
   SET
   bitExecuted = true
   WHERE
   numSFQID = v_numSFQID
   AND v_tintRule NOT IN(5,6);

	-- MAKE ENTRY IN LOG TABLE
   IF v_tintRule = 5 then
	
      IF NOT EXISTS(SELECT numSFLID FROM SalesFulfillmentLog WHERE numSFQID = v_numSFQID AND vcMessage = v_vcMessage) then
		
         INSERT INTO SalesFulfillmentLog(numDomainID,
				numSFQID,
				numOppID,
				vcMessage,
				dtDate,
				bitSuccess)
         SELECT
         numDomainID,
				numSFQID,
				numOppID,
				v_vcMessage,
				TIMEZONE('UTC',now()),
				v_bitSuccess
         FROM
         SalesFulfillmentQueue
         WHERE
         numSFQID = v_numSFQID;
      end if;
   ELSEIF v_tintRule = 6
   then
	
      IF NOT EXISTS(SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID = v_numTempOppID AND vcMessage = v_vcMessage) then
		
         INSERT INTO SalesFulfillmentLog(numDomainID,
				numSFQID,
				numOppID,
				vcMessage,
				dtDate,
				bitSuccess)
         SELECT
         numDomainID,
				numSFQID,
				numOppID,
				v_vcMessage,
				TIMEZONE('UTC',now()),
				v_bitSuccess
         FROM
         SalesFulfillmentQueue
         WHERE
         numSFQID = v_numSFQID;
      end if;
   ELSE
      INSERT INTO SalesFulfillmentLog(numDomainID,
			numSFQID,
			numOppID,
			vcMessage,
			dtDate,
			bitSuccess)
      SELECT
      numDomainID,
			numSFQID,
			numOppID,
			v_vcMessage,
			TIMEZONE('UTC',now()),
			v_bitSuccess
      FROM
      SalesFulfillmentQueue
      WHERE
      numSFQID = v_numSFQID;
   end if;
   RETURN;
END; $$;



