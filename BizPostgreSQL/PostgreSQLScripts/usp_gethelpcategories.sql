-- Stored procedure definition script USP_GetHelpCategories for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetHelpCategories(v_numHelpCategoryID NUMERIC,
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      open SWV_RefCur for
      SELECT  numHelpCategoryID,
                    vcCatecoryName,
                    bitLinkPage,
                    numPageID,
                    numParentCatID,
                    tintLevel
      FROM    HelpCategories
      WHERE   (numHelpCategoryID = v_numHelpCategoryID	OR v_numHelpCategoryID = 0);
   ELSEIF v_tintMode = 1
   then --select parent category dropdown
            
      open SWV_RefCur for
      SELECT  numHelpCategoryID,
                        vcCatecoryName
      FROM    HelpCategories
      WHERE   numHelpCategoryID <> v_numHelpCategoryID;
   end if;

/****** Object:  StoredProcedure [dbo].[usp_GetImapEnabledUsers]    Script Date: 06/04/2009 15:11:43 ******/
   RETURN;
END; $$;


