CREATE OR REPLACE FUNCTION UpdateCurrentBalanceAfterUpdate_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
		v_numChartAccountId  NUMERIC(18,0);
		v_DebitAmount  DECIMAL(20,5);
		v_CreditAmount  DECIMAL(20,5);
		v_OnumChartAccountId  NUMERIC(18,0);
		v_ODebitAmount  DECIMAL(20,5);
		v_OCreditAmount  DECIMAL(20,5);
		v_numDomainId  NUMERIC(18,0);
   
	BizDoc_CursorD CURSOR FOR SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from old_table;
	BizDoc_Cursor CURSOR FOR SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from new_table;
BEGIN
	v_DebitAmount := 0;
	v_CreditAmount := 0;
	v_ODebitAmount := 0;
	v_OCreditAmount := 0;

	OPEN BizDoc_CursorD;

	FETCH NEXT FROM BizDoc_CursorD into v_OnumChartAccountId,v_ODebitAmount,v_OCreditAmount,v_numDomainId;

	WHILE FOUND LOOP
		UPDATE 
			Chart_Of_Accounts 
		SET 
			numOpeningBal = coalesce(numOpeningBal,0) - v_ODebitAmount + v_OCreditAmount
		WHERE 
			numAccountId = v_OnumChartAccountId
			AND numDomainId = v_numDomainId;

		FETCH NEXT FROM BizDoc_CursorD INTO v_OnumChartAccountId,v_ODebitAmount,v_OCreditAmount,v_numDomainId;
	END LOOP;

	CLOSE BizDoc_CursorD;
      
	OPEN BizDoc_Cursor;
    
	FETCH NEXT FROM BizDoc_Cursor INTO v_numChartAccountId,v_DebitAmount,v_CreditAmount,v_numDomainId;
    
	WHILE FOUND LOOP
		UPDATE 
			Chart_Of_Accounts 
		SET 
			numOpeningBal = coalesce(numOpeningBal,0) + v_DebitAmount - v_CreditAmount
		WHERE 
			numAccountId = v_numChartAccountId
			AND numDomainId = v_numDomainId;

        FETCH NEXT FROM BizDoc_Cursor INTO v_numChartAccountId,v_DebitAmount,v_CreditAmount,v_numDomainId;
    END LOOP;
      
	CLOSE BizDoc_Cursor;

	RETURN NULL;
END; $$;
CREATE TRIGGER UpdateCurrentBalanceAfterUpdate AFTER UPDATE ON  General_Journal_Details REFERENCING NEW TABLE AS new_table OLD TABLE AS old_table FOR EACH ROW EXECUTE PROCEDURE UpdateCurrentBalanceAfterUpdate_TrFunc();



