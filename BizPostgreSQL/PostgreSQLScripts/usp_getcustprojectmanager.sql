-- Stored procedure definition script USP_GetCustProjectManager for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCustProjectManager(v_ProID NUMERIC ,
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select ACI.vcEmail from ProjectsMaster PM
   INNER JOIN AdditionalContactsInformation ACI
   ON PM.numCustPrjMgr = ACI.numContactId
   WHERE PM.numProId = v_ProID  AND PM.numdomainId = v_numDomainId;
END; $$;

 













