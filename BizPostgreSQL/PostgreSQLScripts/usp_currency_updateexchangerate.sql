-- Stored procedure definition script USP_Currency_UpdateExchangeRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Currency_UpdateExchangeRate(v_vcToCurrency VARCHAR(10),
	v_decRate DOUBLE PRECISION,
	v_vcDomains TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Currency SET fltExchangeRate = v_decRate WHERE numDomainID IN (SELECT Id FROM SplitIDs(v_vcDomains)) AND chrCurrency = coalesce(v_vcToCurrency,'') AND bitEnabled = true;

   RETURN;
END; $$;

