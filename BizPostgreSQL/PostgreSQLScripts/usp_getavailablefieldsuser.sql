-- FUNCTION: public.usp_getavailablefieldsuser(numeric, numeric, numeric, numeric, refcursor, refcursor)

-- DROP FUNCTION public.usp_getavailablefieldsuser(numeric, numeric, numeric, numeric, refcursor, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getavailablefieldsuser(
	v_numgrouptype numeric,
	v_numdomainid numeric,
	v_numcontactid numeric,
	v_numtabid numeric,
	INOUT swv_refcur refcursor,
	INOUT swv_refcur2 refcursor)
    RETURNS record
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF(SELECT COUNT(*)
   FROM   ShortCutGrpConf
   WHERE  numDomainId = v_numDomainId
   AND numGroupId = v_numGroupType) = 0 then
        
      INSERT  INTO ShortCutGrpConf(numGroupId,
                      numLinkId,
                      numOrder,
                      bitType,
                      numDomainId,
                      numTabId,
                      bitInitialPage,
                      tintLinkType)
      SELECT  v_numGroupType,
                            x.id,
                            x."order",
                            x.bittype,
                            v_numDomainId,
                            x.numTabId,
                            X.bitInitialPage,
                            0
      FROM((SELECT * FROM    ShortCutBar
            WHERE   bitdefault = true
            AND numContactId = 0)) x;
   end if;

        
   IF(SELECT COUNT(*)
   FROM   ShortCutGrpConf
   WHERE  numDomainId = v_numDomainId
   AND numGroupId = v_numGroupType
   AND numTabId = v_numTabId) > 0 then
        
      IF(SELECT COUNT(*)
      FROM   ShortCutUsrCnf
      WHERE  numDomainID = v_numDomainId
      AND numGroupID = v_numGroupType
      AND numContactId = v_numContactId
      AND numTabId = v_numTabId) > 0 then
                
         IF v_numTabId = 1 then
					
						
						 --ORDER BY  sconf.numOrder
	                      
            open SWV_RefCur for
            SELECT    CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
									Hdr.vcLinkName,
									sconf.numOrder AS "Order"
            FROM      ShortCutGrpConf sconf
            JOIN ShortCutBar Hdr ON Hdr.ID = sconf.numLinkId
            WHERE     Hdr.numTabId = v_numTabId
            AND sconf.numTabId = Hdr.numTabId
            AND sconf.numDomainId = v_numDomainId
            AND sconf.numGroupId = v_numGroupType
            AND numContactId = 0
            AND coalesce(sconf.tintLinkType,0) = 0
            AND sconf.numLinkId NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId
               AND coalesce(tintLinkType,0) = 0)
            UNION
            SELECT    CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id,
									LD.vcData AS vclinkname,
									sconf.numOrder AS "Order"
            FROM      ShortCutGrpConf sconf
            JOIN Listdetails LD ON LD.numListItemID = sconf.numLinkId
            WHERE     LD.numListID = 5
            AND LD.numListItemID <> 46
            AND (LD.numDomainid = v_numDomainId
            OR constflag = true)
            AND sconf.numTabId = v_numTabId
            AND sconf.numDomainId = v_numDomainId
            AND sconf.numGroupId = v_numGroupType
            AND coalesce(sconf.tintLinkType,0) = 5
            AND v_numTabId = 7
            AND sconf.numLinkId NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId
               AND coalesce(tintLinkType,0) = 5)
            UNION
            SELECT    CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
									Hdr.vcLinkName,
									"order"
            FROM      ShortCutBar Hdr
            WHERE     Hdr.numTabId = v_numTabId
            AND Hdr.numDomainID = v_numDomainId
            AND Hdr.numGroupID = v_numGroupType
            AND numContactId = v_numContactId
            AND ID NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId)
            UNION ALL
            SELECT    CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
									LD.vcData AS vclinkname,
									sintOrder AS "Order"
            FROM      Listdetails LD
            WHERE     numListID = 27
            AND LD.numDomainid = v_numDomainId
            AND coalesce(constFlag,false) = false
            AND LD.numListItemID NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId)
            UNION ALL
            SELECT    CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
									LD.vcData AS vclinkname,
									sintOrder AS order
            FROM      Listdetails LD
            WHERE     numListID = 27
            AND LD.numDomainid = v_numDomainId
            AND coalesce(constFlag,false) = true
            AND LD.numListItemID NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId);
            open SWV_RefCur2 for
            SELECT  CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
								Hdr.vcLinkName,
								coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
								Sconf.numOrder,
								coalesce(Sconf.bitFavourite,false) AS bitFavourite
            FROM    ShortCutUsrCnf Sconf
            JOIN ShortCutBar Hdr ON Hdr.ID = Sconf.numLinkid
            WHERE   Hdr.numTabId = v_numTabId
            AND Sconf.numTabId = Hdr.numTabId
            AND Sconf.numDomainID = v_numDomainId
            AND Sconf.numGroupID = v_numGroupType
            AND Sconf.numContactId = v_numContactId
            AND coalesce(Sconf.tintLinkType,0) = 0
            UNION
            SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id,
								LD.vcData AS vclinkname,
								coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
								Sconf.numOrder,
								coalesce(Sconf.bitFavourite,false) AS bitFavourite
            FROM    ShortCutUsrCnf Sconf
            JOIN Listdetails LD ON LD.numListItemID = Sconf.numLinkid
            WHERE   LD.numListID = 5
            AND LD.numListItemID <> 46
            AND (LD.numDomainid = v_numDomainId
            OR constflag = true)
            AND Sconf.numTabId = v_numTabId
            AND Sconf.numDomainID = v_numDomainId
            AND Sconf.numGroupID = v_numGroupType
            AND Sconf.numContactId = v_numContactId
            AND coalesce(Sconf.tintLinkType,0) = 5
            AND v_numTabId = 7
						--ORDER BY sconf.numOrder
            UNION ALL
            SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
								LD.vcData AS vclinkname,
								false AS bitInitialPage,
								sintOrder AS "order",
								false AS bitFavourite
            FROM    Listdetails LD
            JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
            WHERE   numListID = 27
            AND LD.numDomainid = v_numDomainId
            AND Sconf.numContactId = v_numContactId
            AND coalesce(constFlag,false) = false
            UNION ALL
            SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
								LD.vcData AS vclinkname,
								false AS bitInitialPage,
								sintOrder AS "order",
								false AS bitFavourite
            FROM    Listdetails LD
            JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
            WHERE   numListID = 27
            AND LD.numDomainid = v_numDomainId
            AND coalesce(constFlag,false) = true
            ORDER BY 4;
         ELSE
            open SWV_RefCur for
            SELECT    CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
										Hdr.vcLinkName,
										sconf.numOrder AS "Order"
            FROM      ShortCutGrpConf sconf
            JOIN ShortCutBar Hdr ON Hdr.ID = sconf.numLinkId
            WHERE     Hdr.numTabId = v_numTabId
            AND sconf.numTabId = Hdr.numTabId
            AND sconf.numDomainId = v_numDomainId
            AND sconf.numGroupId = v_numGroupType
            AND numContactId = 0
            AND coalesce(sconf.tintLinkType,0) = 0
            AND sconf.numLinkId NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId
               AND coalesce(tintLinkType,0) = 0)
            UNION
            SELECT    CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id,
										LD.vcData AS vclinkname,
										sconf.numOrder AS "Order"
            FROM      ShortCutGrpConf sconf
            JOIN Listdetails LD ON LD.numListItemID = sconf.numLinkId
            WHERE     LD.numListID = 5
            AND LD.numListItemID <> 46
            AND (LD.numDomainid = v_numDomainId
            OR constflag = 1)
            AND sconf.numTabId = v_numTabId
            AND sconf.numDomainId = v_numDomainId
            AND sconf.numGroupId = v_numGroupType
            AND coalesce(sconf.tintLinkType,0) = 5
            AND v_numTabId = 7
            AND sconf.numLinkId NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId
               AND coalesce(tintLinkType,0) = 5)
            UNION
            SELECT    CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
										Hdr.vcLinkName,
										Hdr.Order 
            FROM      ShortCutBar Hdr
            WHERE     Hdr.numTabId = v_numTabId
            AND Hdr.numDomainID = v_numDomainId
            AND Hdr.numGroupID = v_numGroupType
            AND numContactId = v_numContactId
            AND ID NOT IN(SELECT  numLinkid
               FROM    ShortCutUsrCnf
               WHERE   numDomainID = v_numDomainId
               AND numGroupID = v_numGroupType
               AND numContactId = v_numContactId
               AND numTabId = v_numTabId) ORDER BY Hdr.Order ;
            open SWV_RefCur2 for
            SELECT  CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
									Hdr.vcLinkName,
									coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
									Sconf.numOrder,
									coalesce(Sconf.bitFavourite,false) AS bitFavourite
            FROM    ShortCutUsrCnf Sconf
            JOIN ShortCutBar Hdr ON Hdr.ID = Sconf.numLinkid
            WHERE   Hdr.numTabId = v_numTabId
            AND Sconf.numTabId = Hdr.numTabId
            AND Sconf.numDomainID = v_numDomainId
            AND Sconf.numGroupID = v_numGroupType
            AND Sconf.numContactId = v_numContactId
            AND coalesce(Sconf.tintLinkType,0) = 0
            UNION
            SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id,
									LD.vcData AS vclinkname,
									coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
									Sconf.numOrder,
									coalesce(Sconf.bitFavourite,false) AS bitFavourite
            FROM    ShortCutUsrCnf Sconf
            JOIN Listdetails LD ON LD.numListItemID = Sconf.numLinkid
            WHERE   LD.numListID = 5
            AND LD.numListItemID <> 46
            AND (LD.numDomainid = v_numDomainId
            OR constflag = 1)
            AND Sconf.numTabId = v_numTabId
            AND Sconf.numDomainID = v_numDomainId
            AND Sconf.numGroupID = v_numGroupType
            AND Sconf.numContactId = v_numContactId
            AND coalesce(Sconf.tintLinkType,0) = 5
            AND v_numTabId = 7
            ORDER BY 4;
         end if;
      ELSE
         open SWV_RefCur for
         SELECT  CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
                            Hdr.vcLinkName
         FROM    ShortCutGrpConf Sconf
         JOIN ShortCutBar Hdr ON Hdr.ID = Sconf.numLinkId
         AND Sconf.numTabId = Hdr.numTabId
         WHERE   Sconf.numGroupId = 0
         AND Hdr.numTabId = v_numTabId
         AND coalesce(Sconf.tintLinkType,0) = 0
         UNION ALL
         SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
                            LD.vcData AS vclinkname
         FROM    Listdetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,0) = 0
         UNION ALL
         SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
                            LD.vcData AS vclinkname
         FROM    Listdetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,0) = 1;
         open SWV_RefCur2 for
         SELECT    CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id,
                                Hdr.vcLinkName,
                                Sconf.numOrder AS "order",
                                coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
                                false AS bitFavourite
         FROM      ShortCutGrpConf Sconf
         JOIN ShortCutBar Hdr ON Hdr.ID = Sconf.numLinkId
         WHERE     Hdr.numTabId = v_numTabId
         AND Sconf.numTabId = Hdr.numTabId
         AND Sconf.numGroupId = v_numGroupType
         AND Sconf.numDomainId = v_numDomainId
         AND numContactId = 0
         AND coalesce(Sconf.tintLinkType,0) = 0
         UNION
         SELECT    CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id,
                                LD.vcData AS vclinkname,
                                Sconf.numOrder AS "order",
                                coalesce(Sconf.bitInitialPage,false) AS bitInitialPage,
                                false AS bitFavourite
         FROM      ShortCutGrpConf Sconf
         JOIN Listdetails LD ON LD.numListItemID = Sconf.numLinkId
         WHERE     LD.numListID = 5
         AND LD.numListItemID <> 46
         AND (LD.numDomainid = v_numDomainId
         OR constflag = 1)
         AND Sconf.numTabId = v_numTabId
         AND Sconf.numDomainId = v_numDomainId
         AND Sconf.numGroupId = v_numGroupType
         AND coalesce(Sconf.tintLinkType,0) = 5
         AND v_numTabId = 7
         UNION
         SELECT    CAST(ID AS VARCHAR(10)) || '~0' AS id,
                                vcLinkName,
                                ShortCutBar.Order,
								false AS bitInitialPage,
                                false AS bitFavourite
         FROM      ShortCutBar
         WHERE     numTabId = v_numTabId
         AND bitdefault = false
         AND numGroupID = v_numGroupType
         AND numDomainID = v_numDomainId
         AND numContactId = v_numContactId ORDER BY ShortCutBar.Order;
      end if;
   ELSE
      IF v_numTabId = 1 then
			
         open SWV_RefCur for
         SELECT  CAST(ID AS VARCHAR(10)) || '~0' AS id,
                    vcLinkName
         FROM    ShortCutBar
         WHERE   numTabId = v_numTabId
         AND bitdefault = false
         AND numContactId = 0;
         open SWV_RefCur2 for
         SELECT  CAST(ID AS VARCHAR(10)) || '~0' AS id,
                    vcLinkName,
                    coalesce(bitInitialPage,false) AS bitInitialPage,
                    false AS bitFavourite
         FROM    ShortCutBar
         WHERE   numTabId = v_numTabId
         AND bitdefault = true
         AND numContactId = 0
         UNION
         SELECT  CAST(numListItemID AS VARCHAR(10)) || '~5' AS id,
                    vcData AS vclinkname,
                    false AS bitInitialPage,
                    false AS bitFavourite
         FROM    Listdetails
         WHERE   numListID = 5
         AND numListItemID <> 46
         AND (numDomainid = v_numDomainId
         OR constFlag = 1)
         AND v_numTabId = 7
         UNION ALL
         SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
                    LD.vcData AS vclinkname,
                    false AS bitInitialPage,
                    false AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,0) = 0
         UNION ALL
         SELECT  CAST(LD.numListItemID AS VARCHAR(10)) || '~0' AS id,
                    LD.vcData AS vclinkname,
                    false AS bitInitialPage,
                    false AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,0) = 1;
      ELSE
         open SWV_RefCur for
         SELECT  CAST(ID AS VARCHAR(10)) || '~0' AS id,
						vcLinkName
         FROM    ShortCutBar
         WHERE   numTabId = v_numTabId
         AND bitdefault = false
         AND numContactId = 0;
         open SWV_RefCur2 for
         SELECT  CAST(ID AS VARCHAR(10)) || '~0' AS id,
						vcLinkName,
						coalesce(bitInitialPage,false) AS bitInitialPage,
						false AS bitFavourite
         FROM    ShortCutBar
         WHERE   numTabId = v_numTabId
         AND bitdefault = true
         AND numContactId = 0
         UNION
         SELECT  CAST(numListItemID AS VARCHAR(10)) || '~5' AS id,
						vcData AS vclinkname,
						false AS bitInitialPage,
						false AS bitFavourite
         FROM    Listdetails
         WHERE   numListID = 5
         AND numListItemID <> 46
         AND (numDomainid = v_numDomainId
         OR constFlag = 1)
         AND v_numTabId = 7;
      end if;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getavailablefieldsuser(numeric, numeric, numeric, numeric, refcursor, refcursor)
    OWNER TO postgres;
