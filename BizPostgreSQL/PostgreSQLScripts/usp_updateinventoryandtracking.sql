-- Stored procedure definition script USP_UpdateInventoryAndTracking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateInventoryAndTracking(v_numOnHand DOUBLE PRECISION,
v_numOnAllocation DOUBLE PRECISION,
v_numOnBackOrder DOUBLE PRECISION,
v_numOnOrder DOUBLE PRECISION,
v_numWarehouseItemID NUMERIC(18,0),
v_numReferenceID NUMERIC(18,0),
v_tintRefType SMALLINT,
v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_Description VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRAN

	--UPDATE WAREHOUSE INVENTORY
	UPDATE
      WareHouseItems
      SET
      numOnHand = v_numOnHand,numAllocation = v_numOnAllocation,numBackOrder = v_numOnBackOrder,
      numonOrder = v_numOnOrder,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID; 
			
	--UPDATE WAREHOUSE TRACKING
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numReferenceID,
      v_tintRefType := v_tintRefType::SMALLINT,v_vcDescription := v_Description,
      v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
      SWV_RefCur := null);
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


