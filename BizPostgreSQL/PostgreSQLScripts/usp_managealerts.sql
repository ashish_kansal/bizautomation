-- Stored procedure definition script USP_ManageAlerts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAlerts(v_numAlertID NUMERIC(9,0) DEFAULT 0,          
v_numAlertDTLid NUMERIC(9,0) DEFAULT 0,          
v_numEmailTemplate NUMERIC(9,0) DEFAULT 0,          
v_tintCCManager SMALLINT DEFAULT NULL,          
v_numDaysAfterDue NUMERIC(9,0) DEFAULT 0,          
v_monAmount DECIMAL(20,5) DEFAULT NULL,          
v_numBizDocs NUMERIC(9,0) DEFAULT 0,          
v_tintAlertOn SMALLINT DEFAULT NULL,      
v_numDepartMentID NUMERIC(9,0) DEFAULT 0,      
v_numNoOfItems NUMERIC(9,0) DEFAULT 0,    
v_numAge NUMERIC(9,0) DEFAULT 0  ,
v_numDomainId NUMERIC(9,0)     DEFAULT 0,
v_numEmailCampaignId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numAlertDTLid <> 0 then

      if ((select count(*) from AlertDomainDtl where numAlertDTLid = v_numAlertDTLid and numDomainID = v_numDomainId) > 0) then
		
         update AlertDomainDtl set
         numEmailTemplate = v_numEmailTemplate,numDaysAfterDue = v_numDaysAfterDue,
         monAmount = v_monAmount,numBizDocs = v_numBizDocs,tintAlertOn = v_tintAlertOn,
         numDepartment = v_numDepartMentID,numNoOfItems = v_numNoOfItems,numAge = v_numAge,
         numEmailCampaignId = v_numEmailCampaignId
         where numAlertDTLid = v_numAlertDTLid and numDomainID = v_numDomainId;
      else
         insert into AlertDomainDtl(numEmailTemplate,
				 numDaysAfterDue,
				 monAmount,
				 numBizDocs,
				 tintAlertOn,
				 numDepartment,
				 numNoOfItems,
				 numAge,
				 numAlertDTLid,
				 numDomainID,
				 numEmailCampaignId)
				 values(v_numEmailTemplate,
				v_numDaysAfterDue,
				v_monAmount,
				v_numBizDocs,
				v_tintAlertOn,
				v_numDepartMentID,
				v_numNoOfItems,
				v_numAge,
				v_numAlertDTLid,
				v_numDomainId,
				v_numEmailCampaignId);
      end if;
   end if;  
  
  
   if v_numAlertDTLid = 0 then

      insert into AlertDTL(numAlertID) values(v_numAlertID);
 
      v_numAlertDTLid := CURRVAL('AlertDTL_seq');
      insert into AlertDomainDtl(numEmailTemplate,
				 numDaysAfterDue,
				 monAmount,
				 numBizDocs,
				 tintAlertOn,
				 numDepartment,
				 numNoOfItems,
				 numAge,
				 numAlertDTLid,
				 numDomainID)
				 values(v_numEmailTemplate,
				v_numDaysAfterDue,
				v_monAmount,
				v_numBizDocs,
				v_tintAlertOn,
				v_numDepartMentID,
				v_numNoOfItems,
				v_numAge,
				v_numAlertDTLid,
				v_numDomainId);
   end if;
   RETURN;
END; $$;


