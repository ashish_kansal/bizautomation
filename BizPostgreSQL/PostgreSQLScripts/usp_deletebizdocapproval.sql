CREATE OR REPLACE FUNCTION USP_DeleteBizDocApproval(v_numDomainID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID in (select numBizDocAppID from BizDocApprovalRule where numDomainID = v_numDomainID);

   DELETE FROM BizDocStatusApprove WHERE numDomainID = v_numDomainID;
   DELETE FROM BizDocApprovalRule WHERE numDomainID = v_numDomainID;
   open SWV_RefCur for SELECT 1;

   RETURN;
END; $$;












