-- Stored procedure definition script Usp_cflDeleteField for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_cflDeleteField(v_fld_id NUMERIC(9,0) DEFAULT NULL,
v_numDomainID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDomainID > 0 then

      IF(EXISTS(SELECT 1 FROM eCommerceDTL WHERE numDomainID = v_numDomainID AND bitElasticSearch = true)) then
	
         IF (EXISTS(SELECT 1 FROM CFW_Fld_Master where Fld_id = v_fld_id AND Grp_id IN(5,9))) then
		
            INSERT INTO ElasticSearchBizCart(numDomainID,vcValue,vcAction,vcModule)
            SELECT v_numDomainID,
			CASE
            WHEN Grp_id = 5 THEN REPLACE(FLd_label,' ','') || '_C'
            WHEN Grp_id = 9 THEN REPLACE(FLd_label,' ','') || '_' || CAST(Fld_id AS VARCHAR(30))
            END
			,'Delete'
			,'CustomField'
            FROM CFW_Fld_Master where Fld_id = v_fld_id AND Grp_id IN(5,9);
         end if;
      end if;
      DELETE FROM DycFormConfigurationDetails WHERE bitCustom = true AND numFieldID = v_fld_id AND numDomainID = v_numDomainID; -- Bug id 278 permenant fix 

      delete from Listdetails where numListID =(select numlistid from  CFW_Fld_Master where Fld_id = v_fld_id);
      delete from listmaster where numListID =(select numlistid from  CFW_Fld_Master where Fld_id = v_fld_id);
      delete from CFW_Fld_Values_Product where Fld_ID = v_fld_id;
      delete from CFW_FLD_Values where Fld_ID = v_fld_id;
      delete from CFW_FLD_Values_Case where Fld_ID = v_fld_id;
      delete from CFW_FLD_Values_Cont where fld_id = v_fld_id;
      delete from CFW_FLD_Values_Item where Fld_ID = v_fld_id;
      delete from CFW_Fld_Values_Opp where Fld_ID = v_fld_id;
      DELETE FROM CFW_Fld_Values_OppItems where fld_id = v_fld_id;
      DELETE FROM ItemAttributes WHERE Fld_ID = v_fld_id;
      DELETE FROM CFW_Validation WHERE numFieldId = v_fld_id;
      delete from CFW_Fld_Dtl where numFieldId = v_fld_id;
      delete from CFW_Fld_Master where Fld_id = v_fld_id;
   end if;
   RETURN;
END; $$;       


