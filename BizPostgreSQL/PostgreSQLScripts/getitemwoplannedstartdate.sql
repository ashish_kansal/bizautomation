-- Function definition script GetItemWOPlannedStartDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemWOPlannedStartDate(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_numQtyToBuild DOUBLE PRECISION
	,v_dtPlannedStartDate TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtVendorShipDate  TIMESTAMP;
BEGIN
   SELECT
   MAX(dtVendorShipDate) INTO v_dtVendorShipDate FROM(SELECT
      Item.numItemCode
									,(CASE
      WHEN coalesce(Item.bitAssembly,false) = true
      THEN GetItemWOPlannedStartDate(v_numDomainID,Item.numItemCode,v_numWarehouseID,(CASE
         WHEN coalesce((SELECT  coalesce(numOnHand,0) -coalesce(numBackOrder,0) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID AND coalesce(numWLocationID,0) = 0 ORDER BY numWareHouseItemID LIMIT 1),0) > 0
         THEN CAST((v_numQtyToBuild*coalesce(ItemDetails.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ItemDetails.numUOMID,0),Item.numItemCode,v_numDomainID,coalesce(Item.numBaseUnit,0)),1)) AS DOUBLE PRECISION) -coalesce((SELECT  coalesce(numOnHand,0) -coalesce(numBackOrder,0) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID AND coalesce(numWLocationID,0) = 0 ORDER BY numWareHouseItemID LIMIT 1),0)
         ELSE CAST((v_numQtyToBuild*coalesce(ItemDetails.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ItemDetails.numUOMID,0),Item.numItemCode,v_numDomainID,coalesce(Item.numBaseUnit,0)),1)) AS DOUBLE PRECISION)
         END),
         v_dtPlannedStartDate)
      ELSE v_dtPlannedStartDate+CAST(coalesce(GetVendorPreferredMethodLeadTime(Item.numVendorID,v_numWarehouseID),
         0) || 'day' as interval)
      END) AS dtVendorShipDate
      FROM
      ItemDetails
      INNER JOIN
      Item
      ON
      ItemDetails.numChildItemID = Item.numItemCode
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      WHERE
      numItemKitID = v_numItemCode
      AND Item.charItemType = 'P'
      AND 1 =(CASE
      WHEN Item.bitAssembly = true
      THEN(CASE WHEN coalesce((SELECT  coalesce(numOnHand,0) -coalesce(numBackOrder,0) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID AND coalesce(numWLocationID,0) = 0 ORDER BY numWareHouseItemID LIMIT 1),0) < CAST((v_numQtyToBuild*coalesce(ItemDetails.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ItemDetails.numUOMID,0),Item.numItemCode,v_numDomainID,coalesce(Item.numBaseUnit,0)),1)) AS DOUBLE PRECISION) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN coalesce((SELECT SUM(numOnHand) -SUM(numBackOrder) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID),0) < CAST((v_numQtyToBuild*coalesce(ItemDetails.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ItemDetails.numUOMID,0),Item.numItemCode,v_numDomainID,coalesce(Item.numBaseUnit,0)),1)) AS DOUBLE PRECISION) THEN 1 ELSE 0 END)
      END)) TEMP;

   RETURN(CASE WHEN v_dtVendorShipDate IS NOT NULL THEN v_dtVendorShipDate ELSE v_dtPlannedStartDate END);
END; $$;

