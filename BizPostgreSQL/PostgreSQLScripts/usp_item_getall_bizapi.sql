DROP FUNCTION IF EXISTS USP_Item_GetAll_BIZAPI;

CREATE OR REPLACE FUNCTION USP_Item_GetAll_BIZAPI(v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numPageIndex INTEGER DEFAULT 0,
    v_numPageSize INTEGER DEFAULT 0,
    INOUT v_TotalRecords INTEGER  DEFAULT NULL,
	v_dtCreatedAfter TIMESTAMP DEFAULT NULL,
	v_dtModifiedAfter TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

   DROp TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numItemCode NUMERIC(18,0)
   );

   INSERT INTO tt_TEMP(numItemCode)
   SELECT DISTINCT
   numItemCode
   FROM
   Item
   LEFT JOIN
   WareHouseItems
   ON
   Item.numItemCode = numItemID
   WHERE
   Item.numDomainID = v_numDomainID
   AND (v_dtCreatedAfter IS NULL OR Item.bintCreatedDate > v_dtCreatedAfter)
   AND (v_dtModifiedAfter IS NULL OR Item.bintModifiedDate > v_dtModifiedAfter OR dtModified+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'hour' as interval) > v_dtModifiedAfter);

   v_TotalRecords := coalesce((SELECT COUNT(*) FROM tt_TEMP),0);

   DROp TABLE IF EXISTS tt_TEMPResult CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPResult ON COMMIT DROP AS
      SELECT
      ROW_NUMBER() OVER(ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT  vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND coalesce(bitDefault,false) = true LIMIT 1) AS vcPathForImage,
		(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND coalesce(bitDefault,false) = true LIMIT 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE
      WHEN Item.charItemType = 'P' THEN 'Inventory Item'
      WHEN Item.charItemType = 'N' THEN 'Non Inventory Item'
      WHEN Item.charItemType = 'S' THEN 'Service'
      WHEN Item.charItemType = 'A' THEN 'Accessory'
      END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN coalesce(Item.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,
		WI.numOnOrder,
		WI.numReorder,
		WI.numAllocation,
		WI.numBackOrder,
		bintCreatedDate,
		CASE WHEN WI.dtModified+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'hour' as interval) > bintModifiedDate THEN WI.dtModified+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'hour' as interval) ELSE bintModifiedDate END AS bintModifiedDate,
		COALESCE((SELECT DISTINCT string_agg(CAST(numCategoryID AS VARCHAR),',') FROM ItemCategory WHERE numItemID = Item.numItemCode),'') AS vcCategories
	
      FROM
      Item 
      INNER JOIN
      tt_TEMP T1
      ON
      Item.numItemCode = T1.numItemCode
      LEFT JOIN LATERAL(SELECT
      SUM(numOnHand) AS numOnHand,
			SUM(numonOrder) AS numOnOrder,
			SUM(numReorder) AS numReorder,
			SUM(numAllocation) AS numAllocation,
			SUM(numBackOrder) AS numBackOrder,
			MAX(dtModified) AS dtModified
      FROM
      WareHouseItems
      WHERE
      WareHouseItems.numItemID = Item.numItemCode) WI on TRUE
      ORDER BY
      Item.numItemCode
	  OFFSET (CASE WHEN COALESCE(v_numPageIndex,0) > 0 THEN (v_numPageIndex - 1) ELSE 0 END) * v_numPageSize FETCH NEXT (CASE WHEN COALESCE(v_numPageSize,0) > 0 THEN v_numPageSize ELSE 999999999 END) ROWS ONLY;
   
   open SWV_RefCur for
   SELECT * FROM tt_TEMPResult;

	--GET CHILD ITEMS
   open SWV_RefCur2 for
   SELECT
   ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMID) AS vcUnitName
   FROM
   tt_TEMPResult T1
   INNER JOIN
   ItemDetails 
   ON
   T1.numItemCode = ItemDetails.numItemKitID;

	--GET CUSTOM FIELDS
   open SWV_RefCur3 for
   SELECT
   I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS Type,
		TEMPCustFields.Fld_Value AS ValueId,
		CASE
   WHEN TEMPCustFields.Fld_type = 'TextBox' or TEMPCustFields.Fld_type = 'TextArea' THEN(CASE WHEN TEMPCustFields.Fld_Value = '0' OR TEMPCustFields.Fld_Value = '' THEN '-' ELSE TEMPCustFields.Fld_Value END)
   WHEN TEMPCustFields.Fld_type = 'SelectBox' THEN(CASE WHEN TEMPCustFields.Fld_Value = '' THEN '-' ELSE GetListIemName(CAST(TEMPCustFields.Fld_Value AS NUMERIC)) END)
   WHEN TEMPCustFields.Fld_type = 'CheckBox' THEN(CASE WHEN TEMPCustFields.Fld_Value = '0' THEN 'No' ELSE 'Yes' END)
   ELSE
      ''
   END AS Value
   FROM
   tt_TEMPResult I
   LEFT JOIN LATERAL(SELECT
      CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.FLd_label,
			CFW_Fld_Master.fld_type,
			CFW_FLD_Values_Item.Fld_Value
      FROM
      CFW_Fld_Master 
      LEFT JOIN
      CFW_FLD_Values_Item 
      ON
      CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
      AND I.numItemCode = CFW_FLD_Values_Item.RecId
      WHERE
      numDomainID = v_numDomainID
      AND Grp_id = 5) TEMPCustFields on TRUE;

   RETURN;
END; $$;


