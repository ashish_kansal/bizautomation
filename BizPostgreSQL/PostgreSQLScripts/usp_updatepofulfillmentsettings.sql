-- Stored procedure definition script USP_UpdatePOFulfillmentSettings for PostgreSQL
CREATE OR REPLACE FUNCTION USP_UpdatePOFulfillmentSettings(v_numDomainID NUMERIC(18,0) DEFAULT 0, 	
	v_numPartialReceiveOrderStatus NUMERIC(18,0) DEFAULT NULL, 
	v_numFullReceiveOrderStatus NUMERIC(18,0) DEFAULT NULL,
	v_bitPartialQtyOrderStatus BOOLEAN DEFAULT NULL,
	v_bitAllQtyOrderStatus BOOLEAN DEFAULT NULL,
	--@bitBizDoc AS BIT,
	v_bitClosePO BOOLEAN DEFAULT NULL,
	v_numUserId NUMERIC(18,0) DEFAULT 0,
	v_numBizDocID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN  
	--IF @numPOFulfillmentID > 0
	IF EXISTS(SELECT numPOFulfillmentID FROM PurchaseOrderFulfillmentSettings WHERE numDomainID = v_numDomainID) then
	
         UPDATE
         PurchaseOrderFulfillmentSettings
         SET
         numPartialReceiveOrderStatus = v_numPartialReceiveOrderStatus,numFullReceiveOrderStatus = v_numFullReceiveOrderStatus,
         bitPartialQtyOrderStatus = v_bitPartialQtyOrderStatus,
         bitAllQtyOrderStatus = v_bitAllQtyOrderStatus,
			--bitBizDoc = @bitBizDoc,
         numBizDocID = v_numBizDocID,bitClosePO = v_bitClosePO,numModifiedBy = v_numUserId,
         bintModifiedDate = LOCALTIMESTAMP
         WHERE
         numDomainID = v_numDomainID;
      ELSE
         INSERT INTO PurchaseOrderFulfillmentSettings(numDomainID
			,numPartialReceiveOrderStatus
			,numFullReceiveOrderStatus
			,bitPartialQtyOrderStatus
			,bitAllQtyOrderStatus
			--,bitBizDoc
			,numBizDocID
			,bitClosePO
			,numCreatedBY
			,bintCreatedDate)
		VALUES(v_numDomainID
			,v_numPartialReceiveOrderStatus
			,v_numFullReceiveOrderStatus
			,v_bitPartialQtyOrderStatus
			,v_bitAllQtyOrderStatus
			--,@bitBizDoc
			,v_numBizDocID
			,v_bitClosePO
			,v_numUserId
			,LOCALTIMESTAMP);
      end if;
   RETURN;
END; $$;












