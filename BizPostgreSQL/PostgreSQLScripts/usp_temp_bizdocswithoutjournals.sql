-- Stored procedure definition script USP_Temp_BizDocsWithoutJournals for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Temp_BizDocsWithoutJournals(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT GHD.numJournalId,GH.numJOurnal_Id,GH.numOppBizDocsId,GH.numOppId,GH.numDomainId,
	(SELECT numShipVia FROM OpportunityBizDocs WHERE numOppBizDocsId = GH.numOppBizDocsId) AS numShipVia,
	(SELECT numDivisionId FROM OpportunityMaster WHERE numOppId = GH.numOppId) AS numDivisionID
	--(SELECT vcDomainName FROM dbo.Domain WHERE numDomainId=GH.numDomainId) domainName 
--	into TempJournalData
   FROM
   General_Journal_Header GH LEFT JOIN General_Journal_Details GHD
   ON GH.numJOurnal_Id = GHD.numJournalId
   WHERE
   GH.datCreatedDate > '2011-07-15 00:00:00.000'
   AND
   GHD.numJournalId IS NULL
   AND
   numOppBizDocsId IN(SELECT numOppBizDocsId FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OM.numOppId = OBD.numoppid WHERE bitAuthoritativeBizDocs = true AND OM.tintopptype = 1)
   ORDER BY GH.numDomainId;
	--SELECT * FROM dbo.OpportunityBizDocs WHERE bitAuthoritativeBizDocs=1	
END; $$;











