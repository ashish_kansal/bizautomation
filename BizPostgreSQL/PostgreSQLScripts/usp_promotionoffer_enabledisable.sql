-- Stored procedure definition script USP_PromotionOffer_EnableDisable for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromotionOffer_EnableDisable(v_numDomainID NUMERIC(18,0),
	v_numProId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numProId FROM PromotionOffer WHERE numDomainId = v_numDomainID AND numProId = v_numProId AND coalesce(bitEnabled,false) = false AND coalesce(bitError,false) = true) then
	
      RAISE EXCEPTION 'PROMOTION_CONFIGURATION_INVALID';
      RETURN;
   ELSE
      UPDATE
      PromotionOffer
      SET
      bitEnabled =(CASE WHEN coalesce(bitEnabled,false) = true THEN false ELSE true END)
      WHERE
      numDomainId = v_numDomainID
      AND numProId = v_numProId;
   end if;
END; $$;



