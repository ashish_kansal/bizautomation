-- Stored procedure definition script USP_OppManage_API for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppManage_API(INOUT v_numOppID NUMERIC DEFAULT 0 ,                                                                          
  v_numContactId NUMERIC(9,0) DEFAULT null,                                                                          
  v_numDivisionId NUMERIC(9,0) DEFAULT null,                                                                          
  v_tintSource NUMERIC(9,0) DEFAULT null,                                                                          
  INOUT v_vcPOppName VARCHAR(100) DEFAULT '' ,                                                                          
  v_Comments VARCHAR(1000) DEFAULT '',                                                                          
  v_bitPublicFlag BOOLEAN DEFAULT false,                                                                          
  v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                 
  v_monPAmount DECIMAL(20,5) DEFAULT 0,                                                 
  v_numAssignedTo NUMERIC(9,0) DEFAULT 0,                                                                                                        
  v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                                                                   
  v_strItems TEXT DEFAULT null,                                                                          
  v_strMilestone TEXT DEFAULT null,                                                                          
  v_dtEstimatedCloseDate TIMESTAMP DEFAULT NULL,                                                                          
  v_CampaignID NUMERIC(9,0) DEFAULT null,                                                                          
  v_lngPConclAnalysis NUMERIC(9,0) DEFAULT null,                                                                         
  v_tintOppType SMALLINT DEFAULT NULL,                                                                                                                                             
  v_tintActive SMALLINT DEFAULT NULL,                                                              
  v_numSalesOrPurType NUMERIC(9,0) DEFAULT NULL,              
  v_numRecurringId NUMERIC(9,0) DEFAULT NULL,
  v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
  v_vcSource VARCHAR(50) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltExchangeRate  DOUBLE PRECISION;                                 
   v_hDocItem  INTEGER;                                                                                                                            
   v_TotalAmount  DECIMAL(20,5);  
   v_WebApiId  INTEGER;
   v_intOppTcode  NUMERIC(9,0);
   v_tintOppStatus  SMALLINT;
   v_tempAssignedTo  NUMERIC(9,0);
BEGIN
   if v_numOppID = 0 then
      select   max(numOppId) INTO v_intOppTcode from OpportunityMaster;
      v_intOppTcode := coalesce(v_intOppTcode,0)+1;
      v_vcPOppName := coalesce(v_vcPOppName,'') || '-' || CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS VARCHAR(4))  || '-A' || SUBSTR(CAST(v_intOppTcode AS VARCHAR(10)),1,10);
      if v_numCurrencyID = 0 then
  
         v_fltExchangeRate := 1;
         select   CASE bitMultiCurrency WHEN true THEN numCurrencyID ELSE 0 END INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
      else 
         v_fltExchangeRate := GetExchangeRate(v_numDomainId,v_numCurrencyID);
      end if;
      insert into OpportunityMaster(numContactId,
  numDivisionId,
  txtComments,
  numCampainID,
  bitPublicFlag,
  tintSource,
  vcpOppName,
  intPEstimatedCloseDate,
  monPAmount,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numDomainId,
  numrecowner,
  lngPConclAnalysis,
  tintopptype,
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate)
  Values(v_numContactId,
  v_numDivisionId,
  coalesce(v_Comments, ''),
  v_CampaignID,
  v_bitPublicFlag,
  v_tintSource,
  v_vcPOppName,
  v_dtEstimatedCloseDate,
  v_monPAmount,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numDomainId,
  v_numUserCntID,
  v_lngPConclAnalysis,
  v_tintOppType,
  v_numSalesOrPurType,
  v_numCurrencyID,
  v_fltExchangeRate);
  
      v_numOppID := CURRVAL('OpportunityMaster_seq');  
  ---- insert recurring id 
      if (v_numRecurringId > 0) then
  
         PERFORM USP_RecurringAddOpportunity(v_numOppID,v_numRecurringId,null,0::SMALLINT,null,null);
      end if;                                              
  ---- inserting Items                                                                           
                                                         
      if SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
  
         SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
         insert into
         OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost)
         select
         v_numOppID,
   X.numItemCode AS numItemCode,
   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 THEN(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = X.numItemCode AND numWareHouseID IN(SELECT numDefaultWareHouseID FROM eCommerceDTL WHERE numDomainID = v_numDomainId))  WHEN 0 THEN(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = X.numItemCode LIMIT 1) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
   X.ItemType,X.DropShip,(SELECT vcItemName FROM Item WHERE X.numItemCode = X.numItemCode),(SELECT vcModelID FROM Item WHERE X.numItemCode = X.numItemCode),(SELECT vcManufacturer FROM Item WHERE X.numItemCode = X.numItemCode),(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = X.numItemCode AND II.numDomainId = v_numDomainId AND II.bitDefault = 1 LIMIT 1),
   (select coalesce(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode = VN.numItemCode AND VN.numVendorID = IT.numVendorID WHERE VN.numItemCode = X.numItemCode)
         from(SELECT *FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item[Op_Flag=1]','numoppitemtCode |numItemCode |numUnitHour |monPrice |monTotAmount |vcItemDesc |numWarehouseItmsID |ItemType |DropShip |WebApiId') SWA_OpenXml(numoppitemtCode NUMERIC(9,0),numItemCode NUMERIC(9,0),numUnitHour DOUBLE PRECISION,
            monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),
            numWarehouseItmsID NUMERIC(9,0),ItemType VARCHAR(30),
            DropShip BOOLEAN,WebApiId INTEGER)) X;
         select   WebApiId INTO v_WebApiId FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item','WebApiId') SWA_OpenXml(WebApiId INTEGER)     LIMIT 1;
         IF(v_WebApiId = 1) then
		
            UPDATE WebAPIDetail SET vcNinthFldValue = TIMEZONE('UTC',now()) WHERE numDomainID = v_numDomainId AND WebApiId = v_WebApiId;
         end if;
         IF(v_vcSource IS NOT NULL) then
		
            insert into OpportunityLinking(numParentOppID,numChildOppID,vcSource,numParentOppBizDocID) values(null,v_numOppID,v_vcSource,null);
         end if;
         Update OpportunityItems
         set numItemCode = X.numItemCode,numOppId = v_numOppID,numUnitHour = x.numUnitHour,
         monPrice = x.monPrice,monTotAmount = x.monTotAmount,vcItemDesc = X.vcItemDesc,
         numWarehouseItmsID = X.numWarehouseItmsID,bitDropShip = X.DropShip
         from(SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item[Op_Flag=2]','numoppitemtCode |numItemCode |numUnitHour |monPrice |monTotAmount |vcItemDesc |numWarehouseItmsID |DropShip') SWA_OpenXml(numoppitemtCode NUMERIC(9,0),numItemCode NUMERIC(9,0),numUnitHour DOUBLE PRECISION,
         monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),
         numWarehouseItmsID NUMERIC(9,0),DropShip BOOLEAN)) X where numOppItemID = X.numOppItemID;
         insert into OppWarehouseSerializedItem(numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)
         select v_numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(SELECT * FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/SerialNo','numWarehouseItmsDTLID |numoppitemtCode |numWItmsID') SWA_OpenXml(numWarehouseItmsDTLID NUMERIC(9,0),numoppitemtCode NUMERIC(9,0),numWItmsID NUMERIC(9,0))) X;
         update OppWarehouseSerializedItem
         set numOppItemID = X.numoppitemtCode
         from(SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/SerialNo','numWarehouseItmsDTLID |numoppitemtCode |numWItmsID') SWA_OpenXml(numWarehouseItmsDTLID NUMERIC(9,0),numoppitemtCode NUMERIC(9,0),numWItmsID NUMERIC(9,0))
         join OpportunityItems O on O.numWarehouseItmsID = numWItmsID and O.numOppId = v_numOppID) X
         where(numOppID = v_numOppID and numWarehouseItmsDTLID = X.DTLID);      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
    
                              
                          
         PERFORM SWF_Xml_RemoveDocument(v_hDocItem);
      end if;
      v_TotalAmount := 0;
      select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_numOppID;
      update OpportunityMaster set  monPAmount = v_TotalAmount
      where numOppId = v_numOppID;
      insert into OpportunityStageDetails(numoppid,
  numStagePercentage,
  vcstagedetail,
  vcComments,
  numDomainID,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numAssignTo,
  bintStageComDate,
  bintDueDate,
  bitAlert,
  bitstagecompleted)
  values(v_numOppID,
  100,
  'Deal Closed',
  '',
  v_numDomainId,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  0,
  0,
  0,
  false,
  false);
  
      insert into OpportunityStageDetails(numoppid,
  numStagePercentage,
  vcstagedetail,
  vcComments,
  numDomainID,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numAssignTo,
  bintStageComDate,
  bintDueDate,
  bitAlert,
  bitstagecompleted)
  values(v_numOppID,
  0,
  'Deal Lost',
  '',
 v_numDomainId,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
   0,
   0,
  0,
  false,
  false);
   else                  
  --Reverting back the warehouse items                  
  
      select   tintoppstatus INTO v_tintOppStatus from OpportunityMaster where numOppId = v_numOppID;
      if v_tintOppStatus = 1 then
        
         PERFORM USP_RevertDetailsOpp(v_numOppID,0,0::SMALLINT,v_numUserCntID);
      end if;
      update OpportunityMaster
      set
      vcpOppName = v_vcPOppName,txtComments = coalesce(v_Comments,''),bitPublicFlag = v_bitPublicFlag,
      numCampainID = v_CampaignID,tintSource = v_tintSource,
      intPEstimatedCloseDate = v_dtEstimatedCloseDate,numModifiedBy = v_numUserCntID,
      bintModifiedDate = TIMEZONE('UTC',now()),lngPConclAnalysis = v_lngPConclAnalysis,
      monPAmount = v_monPAmount,tintActive = v_tintActive,numSalesOrPurType = v_numSalesOrPurType
      where numOppId = v_numOppID;               
        ---Updating if organization is assigned to someone                                                      
      v_tempAssignedTo := null;
      select   coalesce(numassignedto,0) INTO v_tempAssignedTo from OpportunityMaster where numOppId = v_numOppID;
      if (v_tempAssignedTo <> v_numAssignedTo and  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
  
         update OpportunityMaster set numassignedto = v_numAssignedTo,numAssignedBy = v_numUserCntID where numOppId = v_numOppID;
      ELSEIF (v_numAssignedTo = 0)
      then
  
         update OpportunityMaster set numassignedto = 0,numAssignedBy = 0 where numOppId = v_numOppID;
      end if;       
  ---- update recurring id 
      if (v_numRecurringId > 0) then
  
         PERFORM USP_RecurringAddOpportunity(v_numOppID,v_numRecurringId,null,0::SMALLINT,null,null);
      end if;                                                               
  ---- Updating Items                                                                     
      if SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
  
         SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
         Delete From OppWarehouseSerializedItem WHERE numOppID = v_numOppID;
         delete from OpportunityItems where numOppId = v_numOppID and numoppitemtCode not in(SELECT numoppitemtCode FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item','numoppitemtCode') SWA_OpenXml(numoppitemtCode NUMERIC(9,0)));
         insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip)
         select
         v_numOppID,
	   X.numItemCode AS numItemCode,
	   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
	   CASE X.numWarehouseItmsID WHEN 0 THEN(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = X.numItemCode LIMIT 1) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
	   X.ItemType,X.DropShip from(SELECT *FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item[Op_Flag=1]','numoppitemtCode |numItemCode |numUnitHour |monPrice |monTotAmount |vcItemDesc |numWarehouseItmsID |ItemType |DropShip |WebApiId') SWA_OpenXml(numoppitemtCode NUMERIC(9,0),numItemCode NUMERIC(9,0),numUnitHour DOUBLE PRECISION,
            monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),
            numWarehouseItmsID NUMERIC(9,0),ItemType VARCHAR(30),
            DropShip BOOLEAN,WebApiId INTEGER)) X;  
--   select @numOppID,X.numItemCode,x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip from(                                                                          
--   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
--   WITH  (                      
--    numoppitemtCode numeric(9),                                     
--    numItemCode numeric(9),                                                                          
--    numUnitHour numeric(9),                                                                          
--    monPrice DECIMAL(20,5),                                                                       
--    monTotAmount DECIMAL(20,5),                                                                          
--    vcItemDesc varchar(1000),                                    
--    numWarehouseItmsID numeric(9),                              
--    Op_Flag tinyint,                            
--    ItemType varchar(30),      
-- DropShip bit                  
--     ))X                                     
                                  
         Update OpportunityItems
         set numItemCode = X.numItemCode,numOppId = v_numOppID,numUnitHour = x.numUnitHour,
         monPrice = x.monPrice,monTotAmount = x.monTotAmount,vcItemDesc = X.vcItemDesc,
         numWarehouseItmsID = X.numWarehouseItmsID,bitDropShip = X.DropShip
         from(SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Item[Op_Flag=2]','numoppitemtCode |numItemCode |numUnitHour |monPrice |monTotAmount |vcItemDesc |numWarehouseItmsID |DropShip') SWA_OpenXml(numoppitemtCode NUMERIC(9,0),numItemCode NUMERIC(9,0),numUnitHour DOUBLE PRECISION,
         monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),
         numWarehouseItmsID NUMERIC(9,0),DropShip BOOLEAN)) X where numOppItemID = X.numOppItemID;                                  
                                    
         
-- delete from OpportunityKitItems where numOppKitItem in (select numoppitemtCode from OpportunityItems where numOppId=@numOppID)     
-- and numOppChildItemID not in                       
-- (SELECT numOppChildItemID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
-- WITH  (numOppChildItemID numeric(9)))    
                             
         insert into OppWarehouseSerializedItem(numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)
         select v_numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(SELECT * FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/SerialNo','numWarehouseItmsDTLID |numoppitemtCode |numWItmsID') SWA_OpenXml(numWarehouseItmsDTLID NUMERIC(9,0),numoppitemtCode NUMERIC(9,0),numWItmsID NUMERIC(9,0))) X;
         update OppWarehouseSerializedItem set numOppItemID = X.numoppitemtCode
         from(SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/SerialNo','numWarehouseItmsDTLID |numoppitemtCode |numWItmsID') SWA_OpenXml(numWarehouseItmsDTLID NUMERIC(9,0),numoppitemtCode NUMERIC(9,0),numWItmsID NUMERIC(9,0))
         join OpportunityItems O on O.numWarehouseItmsID = numWItmsID and O.numOppId = v_numOppID) X
         where(numOppID = v_numOppID and numWarehouseItmsDTLID = X.DTLID);       
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=1]',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
    
--   update OpportunityKitItems set numOppKitItem=X.numoppitemtCode,numWareHouseItemId=X.numWarehouseItmsID,numChildItem=X.numItemCode, intQuantity=X.numQtyItemsReq                               
--   from (SELECT numoppitemtCode,numItemCode,numQtyItemsReq,numOppChildItemID as OppChildItemID,numWarehouseItmsID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=2]',2)                                                                         
--   WITH(numOppChildItemID numeric(9),numoppitemtCode numeric(9),numItemCode numeric(9),numQtyItemsReq numeric(9),numWarehouseItmsID numeric(9)))X                                
--   where numOppChildItemID=X.OppChildItemID     
       
                                                                      
         PERFORM SWF_Xml_RemoveDocument(v_hDocItem);
      end if;
   end if;
   RETURN;
END; $$;


