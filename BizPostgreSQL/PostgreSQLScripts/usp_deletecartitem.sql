-- Stored procedure definition script USP_DeleteCartItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCartItem(v_numUserCntId NUMERIC(18,0),
    v_numDomainId NUMERIC(18,0),
    v_vcCookieId VARCHAR(100),
    v_numCartId NUMERIC(18,0) DEFAULT 0,
    v_bitDeleteAll BOOLEAN DEFAULT false,
	v_numSiteID NUMERIC(18,0) DEFAULT 0,
	v_numItemCode NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTempItemCode  NUMERIC(18,0);
BEGIN
   IF v_bitDeleteAll = false then
	
      IF v_numCartId <> 0 then
		
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
				--AND bitrequired = 1
         UPDATE
         CartItems
         SET
         PromotionDesc = '',PromotionID = 0,fltDiscount = 0,monTotAmount =(monPrice*numUnitHour),
         monTotAmtBefDiscount =(monPrice*numUnitHour),vcCoupon = NULL,bitParentPromotion = false
         WHERE
         PromotionID =(SELECT  PromotionID FROM CartItems WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId AND numCartId = v_numCartId AND vcCookieId = v_vcCookieId AND coalesce(bitParentPromotion,false) = true LIMIT 1)
         AND numDomainId = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND vcCookieId = v_vcCookieId
         AND coalesce(bitParentPromotion,false) = false;

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	

         select   numItemCode INTO v_numTempItemCode FROM CartItems WHERE numCartId = v_numCartId;
         DELETE FROM CartItems
         WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId
         AND (numCartId = v_numCartId
         OR numItemCode IN(SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = v_numTempItemCode AND numDomainId = v_numDomainId));
      ELSEIF coalesce(v_numItemCode,0) > 0
      then
		
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
         UPDATE
         CartItems
         SET
         PromotionDesc = '',PromotionID = 0,fltDiscount = 0,monTotAmount =(monPrice*numUnitHour),
         monTotAmtBefDiscount =(monPrice*numUnitHour),vcCoupon = NULL,bitParentPromotion = false
         WHERE
         PromotionID =(SELECT  PromotionID FROM CartItems WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId AND numItemCode = v_numItemCode AND vcCookieId = v_vcCookieId AND coalesce(bitParentPromotion,false) = true LIMIT 1)
         AND numDomainId = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND vcCookieId = v_vcCookieId
         AND coalesce(bitParentPromotion,false) = false;
         DELETE FROM CartItems WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId AND vcCookieId = v_vcCookieId AND numItemCode = v_numItemCode;
      ELSE
         UPDATE
         CartItems
         SET
         PromotionDesc = '',PromotionID = 0,fltDiscount = 0,monTotAmount =(monPrice*numUnitHour),
         monTotAmtBefDiscount =(monPrice*numUnitHour),vcCoupon = NULL,bitParentPromotion = false
         WHERE
         numDomainId = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND vcCookieId = v_vcCookieId;
         DELETE FROM CartItems
         WHERE numDomainId = v_numDomainId AND numUserCntId = 0 AND vcCookieId = v_vcCookieId;
      end if;
   ELSE
      DELETE FROM CartItems WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId	AND vcCookieId <> '';
   end if;

   PERFORM USP_PromotionOffer_ApplyItemPromotionToECommerce(v_numDomainId,v_numUserCntId,v_numSiteID,v_vcCookieId,false,'');
   RETURN;
END; $$;


