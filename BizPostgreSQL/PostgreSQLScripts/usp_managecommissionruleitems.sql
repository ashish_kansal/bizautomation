-- Stored procedure definition script USP_ManageCommissionRuleItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCommissionRuleItems(v_numComRuleID NUMERIC(9,0) DEFAULT 0,    
v_tintAppRuleType SMALLINT DEFAULT NULL,  
v_numValue NUMERIC(9,0) DEFAULT 0,  
v_numComRuleItemID NUMERIC(9,0) DEFAULT 0,  
v_byteMode SMALLINT DEFAULT NULL,
v_numProfile NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numDomainID  NUMERIC(9,0);
   v_tintAssignTo  SMALLINT;
   v_tintComAppliesTo  SMALLINT;
   v_tintComOrgType  SMALLINT;
   v_bitItemDuplicate  BOOLEAN DEFAULT false;
   v_bitOrgDuplicate  BOOLEAN DEFAULT false;
   v_bitContactDuplicate  BOOLEAN DEFAULT false;
BEGIN
   -- BEGIN TRANSACTION
IF v_byteMode = 0 then
	
      IF v_numValue <> 0 then
         select   numDomainID, tintAssignTo, coalesce(tintComAppliesTo,0), coalesce(tintComOrgType,0) INTO v_numDomainID,v_tintAssignTo,v_tintComAppliesTo,v_tintComOrgType FROM
         CommissionRules WHERE
         numComRuleID = v_numComRuleID;
			

			/*Duplicate Rule values checking */
			-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
         IF(SELECT COUNT(*) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID) > 0 then
			
				-- ITEM
            IF v_tintComAppliesTo = 1 OR v_tintComAppliesTo = 2 then
				
					-- ITEM ID OR ITEM CLASSIFICATION
               IF(SELECT COUNT(*) FROM CommissionRuleItems WHERE numValue = v_numValue AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND numComRuleID <> v_numComRuleID)) > 0 then
					
                  v_bitItemDuplicate := true;
               end if;
            ELSEIF v_tintComAppliesTo = 3
            then
				
					-- ALL ITEMS
               v_bitItemDuplicate := true;
            end if;

				--ORGANIZATION
            IF v_tintComOrgType = 1 then
				
					-- ORGANIZATION ID
               IF(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue = v_numValue AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND numComRuleID <> v_numComRuleID)) > 0 then
					
                  v_bitOrgDuplicate := true;
               end if;
            ELSEIF v_tintComOrgType = 2
            then
				
					-- ORGANIZATION RELATIONSHIP OR PROFILE
               IF(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue = v_numValue AND numProfile = v_numProfile AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND numComRuleID <> v_numComRuleID)) > 0 then
					
                  v_bitOrgDuplicate := true;
               end if;
            ELSEIF v_tintComOrgType = 3
            then
				
					-- ALL ORGANIZATIONS
               v_bitOrgDuplicate := true;
            end if;

				--CONTACT
            IF(SELECT
            COUNT(*)
            FROM
            CommissionRuleContacts
            WHERE
            numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleContacts WHERE numComRuleId = v_numComRuleID)
            AND numComRuleId IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND numComRuleID <> v_numComRuleID)) > 0 then
				
               v_bitContactDuplicate := true;
            end if;
         end if;
         IF (coalesce(v_bitItemDuplicate,false) = true AND coalesce(v_bitOrgDuplicate,false) = true AND coalesce(v_bitContactDuplicate,false) = true) then
			
            RAISE EXCEPTION 'DUPLICATE';
            RETURN;
         end if;
         IF NOT EXISTS(SELECT * FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID and numValue = v_numValue and tintType = v_tintAppRuleType) then
			
            IF v_tintAppRuleType > 2 then
				
               IF v_tintAppRuleType = 3 then
					
                  DELETE FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND tintType = 4;
                  UPDATE CommissionRules SET tintComOrgType = 1 WHERE numComRuleID = v_numComRuleID;
                  INSERT INTO CommissionRuleOrganization(numComRuleID,numValue,numProfile,tintType)  values(v_numComRuleID,v_numValue,0,v_tintAppRuleType);
               ELSEIF v_tintAppRuleType = 4
               then
					
                  DELETE FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND tintType = 3;
                  UPDATE CommissionRules SET tintComOrgType = 2 WHERE numComRuleID = v_numComRuleID;
                  INSERT INTO CommissionRuleOrganization(numComRuleID,numValue,numProfile,tintType)  values(v_numComRuleID,v_numValue,v_numProfile,v_tintAppRuleType);
               end if;
            ELSE
               IF v_tintAppRuleType = 1 then
					
                  delete from CommissionRuleItems where tintType = 2 and numComRuleID = v_numComRuleID;
                  UPDATE CommissionRules SET tintComAppliesTo = 1 WHERE numComRuleID = v_numComRuleID;
               ELSEIF v_tintAppRuleType = 2
               then
						
                  delete from CommissionRuleItems where tintType = 1 and numComRuleID = v_numComRuleID;
                  UPDATE CommissionRules SET tintComAppliesTo = 2 WHERE numComRuleID = v_numComRuleID;
               end if;
               INSERT INTO CommissionRuleItems(numComRuleID,numValue,tintType)	VALUES(v_numComRuleID,v_numValue,v_tintAppRuleType);
            end if;
         end if;
      end if;
   ELSEIF v_byteMode = 1
   then
	
      IF (v_tintAppRuleType = 1 OR v_tintAppRuleType = 2) then
         DELETE FROM CommissionRuleItems WHERE numComRuleItemID = v_numComRuleItemID;
      ELSE
         DELETE FROM CommissionRuleOrganization WHERE numComRuleOrgID = v_numComRuleItemID;
      end if;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


