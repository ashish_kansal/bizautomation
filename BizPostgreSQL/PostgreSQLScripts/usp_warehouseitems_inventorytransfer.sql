-- Stored procedure definition script USP_WarehouseItems_InventoryTransfer for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WarehouseItems_InventoryTransfer(v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_numFromWarehouseItemID NUMERIC(18,0),
v_numToWarehouseItemID	NUMERIC(18,0),
v_numQty NUMERIC(18,0),
v_strItems VARCHAR(3000) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOnHand  DOUBLE PRECISION;
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;
   v_numItemCode  NUMERIC(18,0);
   v_vcTemp  TEXT DEFAULT '';

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_description  TEXT DEFAULT '';
   v_posComma  INTEGER;
   v_strKeyVal  VARCHAR(20);

   v_posBStart  INTEGER;
   v_posBEnd  INTEGER;
   v_strQty  INTEGER;
   v_strName  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   numOnHand, numItemID INTO v_numOnHand,v_numItemCode FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numFromWarehouseItemID;
      IF coalesce(v_numOnHand,0) < v_numQty then
	
         RAISE NOTICE 'INSUFFICIENT_ONHAND_QUANTITY';
      ELSE
		-- IF SERIAL/LOT ITEM THAN CHECK SERIAL/LOT NUMBER ARE VALID AND ITS QTY ARE VALID
		
         select   bitSerialized, bitLotNo INTO v_bitSerial,v_bitLot FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
         IF v_bitSerial = true OR v_bitLot = true then
            v_strItems := RTRIM(v_strItems);
            IF SUBSTR(v_strItems,length(v_strItems) -1+1) != ',' then 
               v_strItems := coalesce(v_strItems,'') || ',';
            end if;
            v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);
            WHILE v_posComma > 1 LOOP
               v_strKeyVal := ltrim(rtrim(SUBSTR(v_strItems,1,v_posComma -1)));
               IF v_bitLot = true then
				
                  v_posBStart := coalesce(POSITION(substring(v_strKeyVal from E'\\(') IN v_strKeyVal),0);
                  v_posBEnd := coalesce(POSITION(substring(v_strKeyVal from E'\\)') IN v_strKeyVal),0);
                  IF(v_posBStart > 1 AND v_posBEnd > 1) then
					
                     v_strName := CAST(LTRIM(RTRIM(SUBSTR(v_strKeyVal,1,v_posBStart -1))) AS NUMERIC(18,0));
                     v_strQty := CAST(LTRIM(RTRIM(SUBSTR(v_strKeyVal,v_posBStart+1,LENGTH(v_strKeyVal) -v_posBStart -1))) AS INTEGER);
                  ELSE
                     v_strName := CAST(v_strKeyVal AS NUMERIC(18,0));
                     v_strQty := 1;
                  end if;
                  IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numFromWarehouseItemID AND numWareHouseItmsDTLID = v_strName AND numQty >= v_strQty) then
					
                     SELECT CONCAT(v_vcTemp,(CASE WHEN LENGTH(v_vcTemp) = 0 THEN '' ELSE ',' END),coalesce(vcSerialNo,''),
                     '(',v_strQty,')') INTO v_vcTemp FROM WareHouseItmsDTL  WHERE
                     numWareHouseItemID = v_numFromWarehouseItemID
                     AND numWareHouseItmsDTLID = v_strName LIMIT 1;
                     UPDATE
                     WareHouseItmsDTL SET numQty = numQty -v_strQty  WHERE
                     numWareHouseItemID = v_numFromWarehouseItemID
                     AND numWareHouseItmsDTLID = v_strName;
                     IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numToWarehouseItemID AND numWareHouseItmsDTLID = v_strName) then
						
                        UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0)+v_strQty WHERE numWareHouseItemID = v_numToWarehouseItemID AND numWareHouseItmsDTLID = v_strName;
                     ELSE
                        INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
								vcSerialNo,
								numQty,
								dExpirationDate,
								bitAddedFromPO)
                        SELECT
                        v_numToWarehouseItemID,
								vcSerialNo,
								v_strQty,
								dExpirationDate,
								bitAddedFromPO
                        FROM
                        WareHouseItmsDTL
                        WHERE
                        numWareHouseItemID = v_numFromWarehouseItemID AND numWareHouseItmsDTLID = v_strName;
                     end if;
                  ELSE
                     RAISE NOTICE 'INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY';
                  end if;
               ELSE
                  v_strName := CAST(v_strKeyVal AS NUMERIC(18,0));
                  v_strQty := 1;
                  IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numFromWarehouseItemID AND numWareHouseItmsDTLID = v_strName AND numQty = 1) then
					
                     SELECT coalesce(v_vcTemp,'') ||(CASE WHEN LENGTH(v_vcTemp) = 0 THEN '' ELSE ',' END) || coalesce(vcSerialNo,'') INTO v_vcTemp FROM WareHouseItmsDTL  WHERE
                     numWareHouseItemID = v_numFromWarehouseItemID
                     AND numWareHouseItmsDTLID = v_strName LIMIT 1;
                     UPDATE
                     WareHouseItmsDTL SET numWareHouseItemID = v_numToWarehouseItemID  WHERE
                     numWareHouseItemID = v_numFromWarehouseItemID
                     AND numWareHouseItmsDTLID = v_strName;
                  ELSE
                     RAISE NOTICE 'INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY';
                  end if;
               end if;
               v_strItems := SUBSTR(v_strItems,v_posComma+1,LENGTH(v_strItems) -v_posComma);
               v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);
            END LOOP;
         end if;
         IF v_bitSerial = true OR v_bitLot = true then
		
            v_description := CONCAT('Inventory Transfer - Transferred (Qty:',v_numQty,', Serial/Lot#:',')');
         ELSE
            v_description := CONCAT('Inventory Transfer - Transferred (Qty:',v_numQty,')');
         end if;
         UPDATE
         WareHouseItems
         SET
         numOnHand = coalesce(numOnHand,0) -v_numQty,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numFromWarehouseItemID;
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numFromWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
         SWV_RefCur := null);
         IF v_bitSerial = true OR v_bitLot = true then
		
            v_description := CONCAT('Inventory Transfer - Receieved (Qty:',v_numQty,', Serial/Lot#:',')');
         ELSE
            v_description := CONCAT('Inventory Transfer - Receieved (Qty:',v_numQty,')');
         end if;
         UPDATE
         WareHouseItems
         SET
         numOnHand =(CASE WHEN numBackOrder >= v_numQty THEN numOnHand ELSE coalesce(numOnHand,0)+(v_numQty -coalesce(numBackOrder,0)) END),numAllocation =(CASE WHEN numBackOrder >= v_numQty THEN coalesce(numAllocation,0)+v_numQty ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),
         numBackOrder =(CASE WHEN numBackOrder >= v_numQty THEN coalesce(numBackOrder,0) -v_numQty ELSE 0 END),dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numToWarehouseItemID;
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numToWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



