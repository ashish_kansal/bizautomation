-- Function definition script fn_IsAOIExists for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_IsAOIExists(v_vcContactAOIList VARCHAR(250), v_vcAoiList VARCHAR(250))
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intFirstComma  INTEGER;
   v_intNextComma  INTEGER;
   v_bintValue  BIGINT;
   v_RetVal  BOOLEAN;
BEGIN
   v_RetVal := false;

   IF v_vcAoiList = '' then
	
      RETURN true;
   end if;
   v_intFirstComma := POSITION(',' IN v_vcAoiList);
   v_intNextComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intFirstComma+1)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   IF v_intNextComma = 0 then
	
      v_bintValue := CAST(SUBSTR(v_vcAoiList,v_intFirstComma+1,LENGTH(v_vcAoiList) -v_intFirstComma) AS BIGINT);
      IF v_bintValue > 0 then
		
         IF POSITION(',' || SUBSTR(CAST(v_bintValue AS VARCHAR(30)),1,30) || ',' IN v_vcContactAOIList) > 0 then
			
            v_RetVal := true;
            RETURN v_RetVal;
         end if;
      end if;
      v_intFirstComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intNextComma)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
   end if;
	
   WHILE v_intFirstComma > 0 LOOP
      IF v_intNextComma = 0 then
		
         v_bintValue := CAST(SUBSTR(v_vcAoiList,v_intFirstComma+1,LENGTH(v_vcAoiList) -v_intFirstComma) AS BIGINT);
         IF v_bintValue > 0 then
			
            IF POSITION(',' || SUBSTR(CAST(v_bintValue AS VARCHAR(30)),1,30) || ',' IN v_vcContactAOIList) > 0 then
				
               v_RetVal := true;
               RETURN v_RetVal;
            end if;
         end if;
         EXIT;
      ELSE
         v_bintValue := CAST(SUBSTR(v_vcAoiList,v_intFirstComma+1,v_intNextComma -v_intFirstComma -1) AS BIGINT);
         IF v_bintValue > 0 then
			
            IF POSITION(',' || SUBSTR(CAST(v_bintValue AS VARCHAR(30)),1,30) || ',' IN v_vcContactAOIList) > 0 then
				
               v_RetVal := true;
               RETURN v_RetVal;
            end if;
         end if;
      end if;
      v_intFirstComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intNextComma)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
      v_intNextComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intFirstComma+1)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAoiList,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   END LOOP;

   RETURN v_RetVal;
END; $$;

