-- Stored procedure definition script usp_RenameAOI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_RenameAOI(v_numAOIID NUMERIC,  
 v_vcNewName VARCHAR(50),  
 v_numUserID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE AOIMaster
   SET vcAOIName = v_vcNewName,numModifiedBy = v_numUserID,bintmodifiedDate = TIMEZONE('UTC',now())
   WHERE numAOIID = v_numAOIID;
   RETURN;
END; $$;


