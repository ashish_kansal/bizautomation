CREATE OR REPLACE PROCEDURE USP_SalesFulfillmentUpdateInventory(v_numDomainID NUMERIC(9,0),
    v_numOppID NUMERIC(9,0),
    v_numOppBizDocsId NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
    v_tintMode SMALLINT)
LANGUAGE plpgsql --1:Release 2:Revert
   AS $$
   DECLARE
   v_numDomain  NUMERIC(9,0);
   v_onAllocation  DOUBLE PRECISION;
   v_numWarehouseItemID  NUMERIC;
   v_numOppItemID  NUMERIC;
   v_numUnits  DOUBLE PRECISION;    
   v_numOldQtyShipped  DOUBLE PRECISION;
   v_numNewQtyShipped  DOUBLE PRECISION;
		      
   v_Kit  BOOLEAN;
   v_vcItemName  VARCHAR(300);                   
   v_vcError  VARCHAR(500); 
   v_description  VARCHAR(100);
         
   v_minRowNumber  NUMERIC(9,0);
   v_maxRowNumber  NUMERIC(9,0);
         
   v_minKitRowNumber  NUMERIC(9,0);
   v_maxKitRowNumber  NUMERIC(9,0);
   v_KitWareHouseItemID  NUMERIC(9,0);
   v_KitonAllocation  DOUBLE PRECISION;
   v_KitQtyShipped  DOUBLE PRECISION;
   v_KiyQtyItemsReq_Orig  DOUBLE PRECISION;
   v_KiyQtyItemsReq  DOUBLE PRECISION;  
   v_Kitdescription  VARCHAR(100);
   v_KitNewQtyShipped  DOUBLE PRECISION;
   v_KitOppChildItemID  NUMERIC(9,0);
						
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numAuthInvoice  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
BEGIN
   -- BEGIN TRANSACTION
v_numDomain := v_numDomainID;
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS AS
      SELECT
      OBDI.numOppItemID,
			coalesce(OBDI.numWarehouseItmsID,0) AS numWarehouseItemID,
			coalesce(OI.numQtyShipped,0) AS numQtyShipped,
			OBDI.numUnitHour,
			I.vcItemName,
			(CASE WHEN bitKitParent = true AND bitAssembly = true THEN false WHEN bitKitParent = true THEN true ELSE false END) AS KIT,
			ROW_NUMBER() OVER(ORDER BY OBDI.numOppItemID) AS RowNumber
		
      FROM
      OpportunityBizDocs OBD
      JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OBD.numoppid = v_numOppID
      AND OBD.numOppBizDocsId = v_numOppBizDocsId
      AND OI.numOppId = v_numOppID
      AND (I.charItemType = 'P' OR 1 =(CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
      WHEN coalesce(I.bitKitParent,false) = true THEN 1 ELSE 0 END))
      and (OI.bitDropShip = fale or OI.bitDropShip is null) AND coalesce(OI.bitWorkOrder,false) = false;
   select   MIN(RowNumber), MAX(RowNumber) INTO v_minRowNumber,v_maxRowNumber FROM
   tt_TEMPITEMS;
   WHILE  v_minRowNumber <= v_maxRowNumber LOOP
      select   numWarehouseItemID, numOppItemID, coalesce(numQtyShipped,0), numUnitHour, Kit INTO v_numWarehouseItemID,v_numOppItemID,v_numOldQtyShipped,v_numUnits,v_Kit FROM
      tt_TEMPITEMS WHERE
      RowNumber = v_minRowNumber;
      IF v_tintMode = 1 then --1:Release
			
         v_numNewQtyShipped := v_numUnits -v_numOldQtyShipped;
         IF v_numNewQtyShipped > 0 then
				
            v_description := CONCAT('SO Qty Shipped (Qty:',v_numUnits,' Shipped:',v_numNewQtyShipped,')');
            IF v_Kit = true then
					
               DROP TABLE IF EXISTS tt_TEMPKITS CASCADE;
               CREATE TEMPORARY TABLE tt_TEMPKITS AS
                  SELECT
                  OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							coalesce(numAllocation,0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber
						
                  FROM
                  OpportunityKitItems OKI
                  JOIN
                  Item I
                  ON
                  OKI.numChildItemID = I.numItemCode
                  JOIN
                  WareHouseItems WI
                  ON
                  WI.numWareHouseItemID = OKI.numWareHouseItemId
                  WHERE
                  charitemtype = 'P'
                  AND OKI.numWareHouseItemId > 0
                  AND OKI.numWareHouseItemId IS NOT NULL
                  AND numOppItemID = v_numOppItemID;
               IF(SELECT COUNT(*) FROM tt_TEMPKITS WHERE numAllocation -((v_numUnits*numQtyItemsReq_Orig) -numQtyShipped) < 0) > 0 then
						
                  v_vcError := 'You do not have enough KIT inventory to support this shipment(' || coalesce(v_vcItemName,'') || '). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.';
                  RAISE NOTICE 'NotSufficientQty_Allocation';
               ELSE
                  select   MIN(RowNumber), MAX(RowNumber) INTO v_minKitRowNumber,v_maxKitRowNumber FROM
                  tt_TEMPKITS;
                  WHILE v_minKitRowNumber <= v_maxKitRowNumber LOOP
                     select   numOppChildItemID, numWareHouseItemID, numQtyItemsReq, numQtyShipped, numQtyItemsReq_Orig, numAllocation INTO v_KitOppChildItemID,v_KitWareHouseItemID,v_KiyQtyItemsReq,v_KitQtyShipped,
                     v_KiyQtyItemsReq_Orig,v_KitonAllocation FROM
                     tt_TEMPKITS WHERE
                     RowNumber = v_minKitRowNumber;
                     v_KitNewQtyShipped :=(v_numNewQtyShipped*v_KiyQtyItemsReq_Orig) -v_KitQtyShipped;
                     v_Kitdescription := CONCAT('SO KIT Qty Shipped (Qty:',v_KiyQtyItemsReq,' Shipped:',v_KitNewQtyShipped,
                     ')');
                     v_KitonAllocation := v_KitonAllocation -v_KitNewQtyShipped;
                     IF (v_KitonAllocation >= 0) then
								
                        UPDATE
                        WareHouseItems
                        SET
                        numAllocation = v_KitonAllocation,dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_KitWareHouseItemID;
									 --  numeric(9, 0)
											 --  numeric(9, 0)
											 --  tinyint
											 --  varchar(100)
                        UPDATE
                        OpportunityKitItems
                        SET
                        numQtyShipped = v_numUnits*v_KiyQtyItemsReq_Orig
                        WHERE
                        numOppChildItemID = v_KitOppChildItemID;
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_KitWareHouseItemID,v_numReferenceID := v_numOppID,
                        v_tintRefType := 3::SMALLINT,v_vcDescription := v_Kitdescription,
                        v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
                     end if;
                     v_minKitRowNumber := v_minKitRowNumber+1;
                  END LOOP;
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  varchar(100)
                  UPDATE
                  OpportunityItems
                  SET
                  numQtyShipped = v_numUnits
                  WHERE
                  numoppitemtCode = v_numOppItemID;
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppID,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
                  v_numDomainID := v_numDomain,SWV_RefCur := null);
               end if;
            ELSE
               select   coalesce(numAllocation,0) INTO v_onAllocation FROM
               WareHouseItems WHERE
               numWareHouseItemID = v_numWarehouseItemID;
               v_onAllocation := v_onAllocation -v_numNewQtyShipped;
               IF (v_onAllocation >= 0) then
						
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_onAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  varchar(100)
                  UPDATE
                  OpportunityItems
                  SET
                  numQtyShipped = v_numUnits
                  WHERE
                  numoppitemtCode = v_numOppItemID AND numOppId = v_numOppID;
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppID,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
                  v_numDomainID := v_numDomain,SWV_RefCur := null);
               ELSE
                  v_vcError := 'You do not have enough inventory to support this shipment(' || coalesce(v_vcItemName,'') || '). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.';
                  RAISE NOTICE 'NotSufficientQty_Allocation';
               end if;
            end if;
         end if;
      ELSEIF v_tintMode = 2
      then --2:Revert
         select   coalesce(numAuthoritativeSales,0) INTO v_numAuthInvoice FROM
         AuthoritativeBizDocs WHERE
         AuthoritativeBizDocs.numDomainId = v_numDomainID;
         IF EXISTS(SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
         WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppID  AND OBD.numBizDocId IN(v_numAuthInvoice)) then
				
            RAISE NOTICE 'AlreadyInvoice_DoNotReturn';
            RETURN;
         end if;
         IF v_numOldQtyShipped > 0 then
				
            v_description := CONCAT('SO Qty Shipped Return (Qty:',v_numUnits,' Shipped Return:',v_numOldQtyShipped,
            ')');
            IF v_Kit = true then
					
               DROP TABLE IF EXISTS tt_TEMPKITSRETURN CASCADE;
               CREATE TEMPORARY TABLE tt_TEMPKITSRETURN AS
                  SELECT
                  OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							coalesce(numAllocation,0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber
						
                  FROM
                  OpportunityKitItems OKI
                  JOIN
                  Item I
                  ON
                  OKI.numChildItemID = I.numItemCode
                  JOIN
                  WareHouseItems WI
                  ON
                  WI.numWareHouseItemID = OKI.numWareHouseItemId
                  WHERE
                  charitemtype = 'P'
                  AND OKI.numWareHouseItemId > 0
                  AND OKI.numWareHouseItemId IS NOT NULL
                  AND numOppItemID = v_numOppItemID;
               select   MIN(RowNumber), MAX(RowNumber) INTO v_minKitRowNumber,v_maxKitRowNumber FROM
               tt_TEMPKITSRETURN;
               WHILE  v_minKitRowNumber <= v_maxKitRowNumber LOOP
                  select   numOppChildItemID, numWareHouseItemID, numQtyItemsReq, numQtyShipped, numQtyItemsReq_Orig, numAllocation INTO v_KitOppChildItemID,v_KitWareHouseItemID,v_KiyQtyItemsReq,v_KitQtyShipped,
                  v_KiyQtyItemsReq_Orig,v_KitonAllocation FROM
                  tt_TEMPKITS WHERE
                  RowNumber = v_minKitRowNumber;
                  v_KitNewQtyShipped :=(v_numOldQtyShipped*v_KiyQtyItemsReq_Orig);
                  v_Kitdescription := CONCAT('SO KIT Qty Shipped Return (Qty:',v_KiyQtyItemsReq,' Shipped:',v_KitNewQtyShipped,
                  ')');
                  v_KitonAllocation := v_KitonAllocation+v_KitNewQtyShipped;
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_KitonAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_KitWareHouseItemID;
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  varchar(100)
                  UPDATE
                  OpportunityKitItems
                  SET
                  numQtyShipped = 0
                  WHERE
                  numOppChildItemID = v_KitOppChildItemID;
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_KitWareHouseItemID,v_numReferenceID := v_numOppID,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_Kitdescription,
                  v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
                  v_minKitRowNumber := v_minKitRowNumber+1;
               END LOOP;
						 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
               UPDATE
               OpportunityItems
               SET
               numQtyShipped = v_numUnits
               WHERE
               numoppitemtCode = v_numOppItemID;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppID,
               v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
               v_numDomainID := v_numDomain,SWV_RefCur := null);
            ELSE
               select   coalesce(numAllocation,0) INTO v_onAllocation FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWarehouseItemID;
               v_onAllocation := v_onAllocation+v_numOldQtyShipped;
               IF (v_onAllocation >= 0) then
						
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_onAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  varchar(100)
                  UPDATE
                  OpportunityItems
                  SET
                  numQtyShipped = 0
                  WHERE
                  numoppitemtCode = v_numOppItemID
                  AND numOppId = v_numOppID;
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppID,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
                  v_numDomainID := v_numDomain,SWV_RefCur := null);
               end if;
            end if;
         ELSE
            v_vcError := 'You have not shipped qty for (' || coalesce(v_vcItemName,'') || ').';
            RAISE NOTICE 'NoQty_ForShipped';
         end if;
      end if;
      v_minRowNumber := v_minRowNumber+1;
   END LOOP;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


