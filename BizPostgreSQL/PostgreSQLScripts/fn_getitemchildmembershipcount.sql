-- Function definition script fn_GetItemChildMembershipCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemChildMembershipCount(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCount  INTEGER DEFAULT 0;
BEGIN
   select   COUNT(vcItemName) INTO v_numCount FROM(SELECT
      Item.vcItemName,
			CASE
      WHEN coalesce(Item.bitAssembly,false) = true THEN 'Assembly'
      WHEN coalesce(Item.bitKitParent,false) = true THEN 'Kit'
      END AS ParentItemType,
			coalesce(ItemDetails.numQtyItemsReq,0) AS numQtyItemsReq
      FROM
      ItemDetails
      INNER JOIN
      Item
      ON
      ItemDetails.numItemKitID = Item.numItemCode
      WHERE
      numChildItemID = v_numItemCode
      UNION ALL
      SELECT
      ItemGroups.vcItemGroup AS vcItemName,
			CAST('Item Group' AS VARCHAR(3)) AS ParentItemType,
			0 AS numQtyItemsReq
      FROM
      Item
      INNER JOIN
      ItemGroups
      ON
      Item.numItemGroup = ItemGroups.numItemGroupID
      WHERE
      Item.numDomainID = v_numDomainID
      AND numItemCode = v_numItemCode
      UNION ALL
      SELECT
      Item.vcItemName,
			CAST('Related Items' AS VARCHAR(3)) AS ParentItemType,
			0 AS numQtyItemsReq
      FROM
      SimilarItems
      INNER JOIN
      Item
      ON
      SimilarItems.numParentItemCode = Item.numItemCode
      WHERE
      SimilarItems.numDomainId = v_numDomainID
      AND SimilarItems.numItemCode = v_numItemCode) AS Temp;

   RETURN v_numCount;
END; $$;

