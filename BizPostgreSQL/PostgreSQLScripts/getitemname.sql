-- Function definition script GetItemName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemName(v_numItemCode NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemName  VARCHAR(100);
BEGIN
   select   coalesce(vcItemName,'-') INTO v_vcItemName from Item where numItemCode = v_numItemCode;   


   return coalesce(v_vcItemName,'-');
END; $$;

