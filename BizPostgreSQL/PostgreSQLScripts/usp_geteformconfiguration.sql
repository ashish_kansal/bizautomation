-- Stored procedure definition script USP_GetEFormConfiguration for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEFormConfiguration(v_numSpecDocID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from  EformConfiguration   where numSpecDocID = v_numSpecDocID;
END; $$;












