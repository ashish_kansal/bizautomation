CREATE OR REPLACE FUNCTION ES_ItemCategory_ID_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
	v_bitElasticSearch  BOOLEAN;
	v_action  CHAR(1) DEFAULT 'I';
BEGIN
	v_bitElasticSearch := coalesce((SELECT 
										ECD.bitElasticSearch 
									FROM
									Item I 
									INNER JOIN eCommerceDTL ECD ON (ECD.numDomainID = i.numDomainID AND ECD.bitElasticSearch = true)
									WHERE 
										i.numItemCode = COALESCE(New.numItemID,0) OR i.numItemCode = COALESCE(OLD.numItemID,0)
									LIMIT 1),false);

	IF(v_bitElasticSearch = true) then
		IF (TG_OP = 'INSERT') THEN
			INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID)--, numSiteID) 
            SELECT 'Item',Item.numItemCode::VARCHAR,'Insert',Item.numDomainID--,S.numSiteID 
            FROM Item WHERE Item.numItemCode = NEW.numItemID;
		ELSE
			IF(NOT EXISTS(SELECT 1 FROM ElasticSearchBizCart WHERE vcModule = 'Item' AND vcValue = OLD.numItemID::VARCHAR)) then
			
               INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID)
               SELECT 'Item',Item.numItemCode::VARCHAR,'Update',Item.numDomainID
               FROM Item
               WHERE Item.numItemCode = OLD.numItemID;
            end if;
		END IF;        
    end if;

	RETURN NULL;
END; $$;
CREATE TRIGGER ES_ItemCategory_ID AFTER INSERT OR DELETE ON ItemCategory FOR EACH ROW EXECUTE PROCEDURE ES_ItemCategory_ID_TrFunc();


