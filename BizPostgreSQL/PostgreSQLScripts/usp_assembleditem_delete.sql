-- Stored procedure definition script USP_AssembledItem_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AssembledItem_Delete(v_ID NUMERIC(18,0),
	v_numQty NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE AssembledItem SET numDisassembledQty = coalesce(numDisassembledQty,0)+v_numQty  WHERE ID = v_ID;

   IF(SELECT COUNT(*) FROM AssembledItem WHERE ID = v_ID AND numAssembledQty = coalesce(numDisassembledQty,0)) > 0 then
	
      DELETE FROM AssembledItem WHERE ID = v_ID;
   end if;
   RETURN;
END; $$;



