-- Stored procedure definition script usp_scheduler for PostgreSQL
CREATE OR REPLACE FUNCTION usp_scheduler(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
                        
--Variable Declaration for Recurring Table        
   AS $$
   DECLARE
   v_numMaxScheduleId  NUMERIC(9,0);                        
   v_numMinScheduleId  NUMERIC(9,0);     
   v_numScheduleId  NUMERIC(9,0);                          
   v_varRecurringTemplateName  VARCHAR(50);                          
   v_chrIntervalType  CHAR(1);                          
   v_tintIntervalDays  SMALLINT;                          
   v_tintMonthlyType  SMALLINT;                          
   v_tintFirstDet  SMALLINT;                          
   v_tintWeekDays  SMALLINT;                          
   v_tintMonths  SMALLINT;                          
   v_dtStartDate  TIMESTAMP;                          
   v_dtEndDate  TIMESTAMP;      
   v_numNoTransactionsRecurring  NUMERIC(9,0);       
   v_numNoTransactions  NUMERIC(9,0);      
   v_bitEndType  BOOLEAN;         
   v_bitEndTransactionType  BOOLEAN;                       
   v_DNextDate  TIMESTAMP;                          
   v_DLastDate  TIMESTAMP;                          
   v_DateDiff  INTEGER;                          
   v_CurrentDate  TIMESTAMP;                          
   v_LastRecurringDate  TIMESTAMP;                       
   v_ReportId  INTEGER;    
   v_ContactName  VARCHAR(50);    
   v_ContactEmail  VARCHAR(200);    
   v_ReportName  VARCHAR(200);    
   v_numDomainID  NUMERIC(9,0);    
                         
   v_Day  SMALLINT;                      
   v_Month  SMALLINT;                      
   v_Year  INTEGER;                      
--Declare @Date as varchar(100)                   
   v_MonthNextDate  SMALLINT;
   v_EndDateDiff  INTEGER;
   v_ToMailId  VARCHAR(1000);
   SWV_RowCount INTEGER;
BEGIN
      
    
    
--declare #ScheduleTempTable TABLE (    
--ScheduleID numeric (5) NOT NULL ,    
--ReportId numeric (5) NOT NULL     
--)    
   BEGIN
      CREATE TEMP SEQUENCE tt_ScheduleTempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_SCHEDULETEMPTABLE CASCADE;
   create TEMPORARY TABLE tt_SCHEDULETEMPTABLE 
   (  
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      ScheduleID NUMERIC(5,0) NOT NULL,
      ReportId NUMERIC(5,0) NOT NULL,
      ContactName  VARCHAR(50),
      ContactEmail  VARCHAR(200),
      ReportName  VARCHAR(50),
      ToEmail VARCHAR(1000),
      DomainID NUMERIC(9,0)
   );    
                       
                         
                          
   select   max(numScheduleid), min(numScheduleid) INTO v_numMaxScheduleId,v_numMinScheduleId From CustRptScheduler where  EXTRACT(DAY FROM dtStartDate) <= EXTRACT(DAY FROM TIMEZONE('UTC',now()))     
  --AND DAY([dtEndDate]) >= DAY(getutcdate())    
   and (bitrecurring is null or      bitrecurring = false);                
    
   v_numScheduleId := v_numMinScheduleId;                     
   RAISE NOTICE '@numMaxScheduleId  %',SUBSTR(CAST(v_numMaxScheduleId AS VARCHAR(10)),1,10);    
   RAISE NOTICE '@numMinScheduleId  %',SUBSTR(CAST(v_numMinScheduleId AS VARCHAR(10)),1,10);    
    
   While v_numScheduleId > 0 LOOP
      RAISE NOTICE '=====================================================================';
      RAISE NOTICE '=====================================================================';
      RAISE NOTICE '@numScheduleId %',SUBSTR(CAST(v_numScheduleId AS VARCHAR(10)),1,10);
      If v_numScheduleId <> 0 then
      
         select   vcReportName, numReportId, chrIntervalType, tintIntervalDays, tintMonthlyType, tintFirstDet, tintWeekDays, tintMonths, dtStartDate, dtEndDate, bitEndType, bitEndTransactionType, coalesce(numNoTransaction,0), coalesce(LastRecurringDate,'Jan  1 1753 12:00AM'), coalesce(numNoTransactionCmp,0), fn_GetContactName(numCreatedBy), fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),numCreatedBy), CustRptScheduler.numDomainID INTO v_ReportName,v_ReportId,v_chrIntervalType,v_tintIntervalDays,v_tintMonthlyType,
         v_tintFirstDet,v_tintWeekDays,v_tintMonths,v_dtStartDate,v_dtEndDate,
         v_bitEndType,v_bitEndTransactionType,v_numNoTransactionsRecurring,
         v_LastRecurringDate,v_numNoTransactions,v_ContactName,v_ContactEmail,
         v_numDomainID From CustRptScheduler
         join CustomReport on CustRptScheduler.numReportId = CustomReport.numCustomReportId Where numScheduleId = v_numScheduleId;
         RAISE NOTICE '@chrIntervalType  %',SUBSTR(CAST(v_chrIntervalType AS VARCHAR(10)),1,10);
         RAISE NOTICE '@dtStartDate  %',SUBSTR(CAST(v_dtStartDate AS VARCHAR(30)),1,30);
         If v_chrIntervalType = 'D' then
                    -- 'Begin for Daily                          
            If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                          
               v_DNextDate := v_dtStartDate;
               RAISE NOTICE '@DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(100)),1,100);
               If v_DNextDate < TIMEZONE('UTC',now()) then
                  v_DNextDate := TIMEZONE('UTC',now());
               end if;
            Else
               v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'day' as interval);
            end if;
            RAISE NOTICE '%',v_DNextDate;
            v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
            RAISE NOTICE '%',v_DateDiff;
            RAISE NOTICE '%',v_bitEndType;
            RAISE NOTICE '%',v_dtEndDate;
            v_EndDateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_dtEndDate);
            RAISE NOTICE '%',v_EndDateDiff;
            If v_DateDiff = 0 And (v_bitEndType = true Or((v_bitEndType = false and v_EndDateDiff <= 0) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                          
               v_CurrentDate := TIMEZONE('UTC',now());
               RAISE NOTICE 'in';
               RAISE NOTICE '%',v_numScheduleId;
               RAISE NOTICE '%',v_ReportId;
               insert into tt_SCHEDULETEMPTABLE(ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)
			 values(v_numScheduleId,v_ReportId,v_ContactName,v_ContactEmail,CAST(v_ReportName AS VARCHAR(50)),GetScheduledEmails(v_numScheduleId),v_numDomainID);
            end if;
         end if; -- 'End for Daily     
    
    
         If v_chrIntervalType = 'M' then
                     
            RAISE NOTICE '%',v_chrIntervalType;
            RAISE NOTICE 'Monthly Type%',SUBSTR(CAST(v_tintMonthlyType AS VARCHAR(3)),1,3);
            RAISE NOTICE 'Last RecurringDate%',SUBSTR(CAST(v_LastRecurringDate AS VARCHAR(20)),1,20);
            If v_tintMonthlyType = 0 then
                           -- tinymonthly Begin                      
               If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                                
                  v_DNextDate := v_dtStartDate;
                  If v_DNextDate < TIMEZONE('UTC',now()) then
                     v_DNextDate := TIMEZONE('UTC',now());
                  end if;
                  v_Day := EXTRACT(DAY FROM v_DNextDate);
                  v_Month := EXTRACT(MONTH FROM v_DNextDate);
                  v_Year := EXTRACT(YEAR FROM v_DNextDate);
                  RAISE NOTICE '%',v_Day;
                  RAISE NOTICE '%',v_Month;
                  RAISE NOTICE '%',v_Year;
                  v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_Month -1 || 'month' as interval);
                  RAISE NOTICE '%',v_DNextDate;
               Else
                  v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'month' as interval);
               end if;
               v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
               RAISE NOTICE 'DateDiff%',SUBSTR(CAST(v_DateDiff AS VARCHAR(10)),1,10);
               If v_DateDiff = 0 And (v_bitEndType = true Or((v_bitEndType = false and v_EndDateDiff <= 0) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
         
                  v_CurrentDate := TIMEZONE('UTC',now());
                  RAISE NOTICE 'in';
                  RAISE NOTICE '%',v_numScheduleId;
                  RAISE NOTICE '%',v_ReportId;
                  insert into tt_SCHEDULETEMPTABLE(ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)
			 values(v_numScheduleId,v_ReportId,v_ContactName,v_ContactEmail,CAST(v_ReportName AS VARCHAR(50)),GetScheduledEmails(v_numScheduleId),v_numDomainID);
               end if;
            end if;   -- tinymonthly End--0                       
                      
            If v_tintMonthlyType = 1 then
                           
               If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                                 
                  v_DNextDate := v_dtStartDate;
                  If v_DNextDate < TIMEZONE('UTC',now()) then
                     v_DNextDate := TIMEZONE('UTC',now());
                  end if;
                  v_Month := EXTRACT(MONTH FROM v_DNextDate);
                  v_Year := EXTRACT(YEAR FROM v_DNextDate);
                  RAISE NOTICE '@Month = %',SUBSTR(CAST(v_Month AS VARCHAR(10)),1,10);
                  RAISE NOTICE '%',v_Year;
                  RAISE NOTICE '@tintFirstDet=%',SUBSTR(CAST(v_tintFirstDet AS VARCHAR(10)),1,10);
                  RAISE NOTICE '@tintWeekDays=%',SUBSTR(CAST(v_tintWeekDays AS VARCHAR(10)),1,10);
                  RAISE NOTICE '@DNextDate=%',SUBSTR(CAST(v_DNextDate AS VARCHAR(30)),1,30);
                  v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_Month -1 || 'month' as interval);
                  RAISE NOTICE 'DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(30)),1,30);
                  v_DNextDate := fn_RecurringTemplate(v_tintFirstDet,v_tintWeekDays,v_DNextDate);
                  RAISE NOTICE '%',v_DNextDate;
                  v_MonthNextDate := EXTRACT(MONTH FROM v_DNextDate);
                  RAISE NOTICE '@MonthNextDate=%',SUBSTR(CAST(v_MonthNextDate AS VARCHAR(20)),1,20);
               Else
                  v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'month' as interval);
                  v_Month := EXTRACT(MONTH FROM TIMEZONE('UTC',now()));
                  RAISE NOTICE '@Month = %',SUBSTR(CAST(v_Month AS VARCHAR(10)),1,10);
                  v_MonthNextDate := EXTRACT(MONTH FROM v_DNextDate);
                  RAISE NOTICE '@MonthNextDate=%',SUBSTR(CAST(v_MonthNextDate AS VARCHAR(20)),1,20);
                  v_DNextDate := fn_RecurringTemplate(v_tintFirstDet,v_tintWeekDays,v_DNextDate);
                  RAISE NOTICE '%',v_DNextDate;
               end if;
               v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
               RAISE NOTICE 'DateDifference%',SUBSTR(CAST(v_DateDiff AS VARCHAR(30)),1,30);
               If v_DateDiff = 0 And v_Month = v_MonthNextDate And ((v_bitEndType = true Or (v_bitEndType = false And TIMEZONE('UTC',now()) <= v_dtEndDate) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                  v_CurrentDate := TIMEZONE('UTC',now());
                  RAISE NOTICE 'in';
                  RAISE NOTICE '%',v_numScheduleId;
                  RAISE NOTICE '%',v_ReportId;
                  v_ToMailId := GetScheduledEmails(v_numScheduleId);
                  insert into tt_SCHEDULETEMPTABLE(ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)
			 values(v_numScheduleId,v_ReportId,v_ContactName,v_ContactEmail,CAST(v_ReportName AS VARCHAR(50)),v_ToMailId,v_numDomainID);
               end if;
            end if;
         end if;  --End for Monthly      
        
    
    
         If v_chrIntervalType = 'Y' then
                      
            RAISE NOTICE '%',v_chrIntervalType;
            RAISE NOTICE '%',v_LastRecurringDate;
            If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                             
               If v_dtStartDate < TIMEZONE('UTC',now()) then
                  v_dtStartDate := TIMEZONE('UTC',now());
               end if;
               v_Year := EXTRACT(YEAR FROM v_dtStartDate);
               RAISE NOTICE '%',v_Year;
               v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_tintMonths -1 || 'month' as interval);
               RAISE NOTICE '%',v_DNextDate;
            Else
               v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(CAST(1 AS NUMERIC) || 'year' as interval);
               RAISE NOTICE '%',v_DNextDate;
            end if;
            v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);                          
                        -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                      
            RAISE NOTICE '%',v_DateDiff;
            If v_DateDiff = 0 And (v_bitEndType = true Or((v_bitEndType = false and v_EndDateDiff <= 0) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                          
               v_CurrentDate := TIMEZONE('UTC',now());
               RAISE NOTICE 'in';
               RAISE NOTICE '%',v_numScheduleId;
               RAISE NOTICE '%',v_ReportId;
               insert into tt_SCHEDULETEMPTABLE(ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)
			 values(v_numScheduleId,v_ReportId,v_ContactName,v_ContactEmail,CAST(v_ReportName AS VARCHAR(50)),GetScheduledEmails(v_numScheduleId),v_numDomainID);
            end if;
         end if;
      end if; --End Of ScheduleId <> 0     
      select   numScheduleid INTO v_numScheduleId From CustRptScheduler Where numScheduleid > v_numScheduleId  And numScheduleid <= v_numMaxScheduleId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      If SWV_RowCount = 0 then 
         v_numScheduleId := 0;
      end if;
   END LOOP;    

   open SWV_RefCur for select * from tt_SCHEDULETEMPTABLE;

   RETURN;
END; $$;












