CREATE OR REPLACE FUNCTION USP_AssCntCase(v_numCaseId NUMERIC(9,0) DEFAULT 0 ,
v_numDomainID NUMERIC(9,0) DEFAULT 0           
,INOUT SWV_RefCur refcursor DEFAULT NULL       
)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  D.numDivisionID,a.numContactId,vcCompanyName || ', ' || coalesce(Lst.vcData,'-') as Company,
  a.vcFirstName || ' ' || a.vcLastname as Name,        
  --a.numPhone +', '+ a.numPhoneExtension as Phone,  
  case when a.numPhone <> '' then a.numPhone || case when a.numPhoneExtension <> '' then ' - ' || a.numPhoneExtension else '' end  else '' end as Phone,
  a.vcEmail as Email,
  b.vcData as ContactRole,
  CCont.numRole as ContRoleId,
  coalesce(bitPartner,false) as bitPartner,
  a.numContactType,CASE WHEN CCont.bitSubscribedEmailAlert = true THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM CaseContacts CCont
   join AdditionalContactsInformation a on
   a.numContactId = CCont.numContactID
   join DivisionMaster D
   on a.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   left join Listdetails b
   on b.numListItemID = CCont.numRole
   left join Listdetails Lst
   on Lst.numListItemID = C.numCompanyType
   WHERE CCont.numCaseID = v_numCaseId    and a.numDomainID = v_numDomainID;
END; $$;






