-- Stored procedure definition script USP_Item_GetPriceLevelEcommHtml for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetPriceLevelEcommHtml(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcHtml  TEXT DEFAULT '';
   v_numCurrencyID  NUMERIC(18,0);
   v_numBaseUnit  NUMERIC(18,0);
   v_numSaleUnit  NUMERIC(18,0);
   v_monListPrice  DECIMAL(20,5);
   v_monVendorCost  DECIMAL(20,5);
   v_fltExchangeRate  DOUBLE PRECISION; 
   v_fltUOMConversionFactor  DOUBLE PRECISION;
   v_varCurrSymbol  VARCHAR(100);

   v_vcUnitName  VARCHAR(100);
BEGIN
   IF(SELECT numDefaultSalesPricing FROM Domain WHERE numDomainId = v_numDomainID) = 1 then
	

		-- FIRST CHECK SITE CURRENY
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPPriceLevel_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPPRICELEVEL CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPPRICELEVEL
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numQtyFrom DOUBLE PRECISION,
         numQtyTo DOUBLE PRECISION,
         vcPrice VARCHAR(30),
         bitLastRow BOOLEAN
      );
      select   numCurrencyID INTO v_numCurrencyID FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteID;
      IF NOT EXISTS(SELECT numPricingID FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND coalesce(numCurrencyID,0) = coalesce(v_numCurrencyID,0)) then
		
         select   numCurrencyID INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainID;
      end if;
      IF NOT EXISTS(SELECT numPricingID FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND coalesce(numCurrencyID,0) = coalesce(v_numCurrencyID,0)) then
		
         v_numCurrencyID := 0;
      end if;
      IF EXISTS(SELECT numPricingID FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND coalesce(numCurrencyID,0) = coalesce(v_numCurrencyID,0)) then
         v_fltExchangeRate := coalesce((SELECT fltExchangeRate FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = coalesce(v_numCurrencyID,0)),1);
         IF coalesce(v_fltExchangeRate,0) = 0 then
			
            v_fltExchangeRate := 1;
         end if;
         select   coalesce(numBaseUnit,0), coalesce(numSaleUnit,0), (CASE
         WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
         THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
         ELSE(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(Item.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(Item.monListPrice,0) END)
         END), coalesce((SELECT(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monCost,0) END)*fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID = Item.numVendorID AND Vendor.numItemCode = Item.numItemCode),0) INTO v_numBaseUnit,v_numSaleUnit,v_monListPrice,v_monVendorCost FROM
         Item WHERE
         numDomainID = v_numDomainID
         AND numItemCode = v_numItemCode;
         v_fltUOMConversionFactor := fn_UOMConversion(v_numSaleUnit,v_numItemCode,v_numDomainID,v_numBaseUnit);
         IF coalesce(v_fltUOMConversionFactor,0) = 0 then
			
            v_fltUOMConversionFactor := 1;
         end if;
         v_monListPrice := v_monListPrice*v_fltUOMConversionFactor;
         v_monVendorCost := v_monVendorCost*v_fltUOMConversionFactor;
         v_varCurrSymbol := coalesce((SELECT varCurrSymbol FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = v_numCurrencyID),'$');
         INSERT INTO tt_TEMPPRICELEVEL(numQtyFrom
				,numQtyTo
				,vcPrice)
         SELECT
         intFromQty
				,intToQty
				,(CASE tintRuleType
         WHEN 1 -- Deduct From List Price
         THEN(CASE tintDiscountType
            WHEN 1 -- PERCENT
            THEN TO_CHAR(coalesce((CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100)) ELSE 0 END),0),'FM999,999,999,999,990.99999')
            WHEN 2 -- FLAT AMOUNT
            THEN CONCAT(TO_CHAR((CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END),'FM999,999,999,999,990.99999'),' off on total amount')
            WHEN 3 -- NAMED PRICE
            THEN TO_CHAR((CASE WHEN v_numCurrencyID <> numCurrencyID THEN(CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER)*v_fltUOMConversionFactor) ELSE(coalesce(decDiscount,0)*v_fltUOMConversionFactor) END),
               'FM999,999,999,999,990.99999')
            END)
         WHEN 2 -- Add to primary vendor cost
         THEN(CASE tintDiscountType
            WHEN 1  -- PERCENT
            THEN TO_CHAR(v_monVendorCost+(v_monVendorCost*(coalesce(decDiscount,0)/100)),
               'FM999,999,999,999,990.99999')
            WHEN 2 -- FLAT AMOUNT
            THEN CONCAT(TO_CHAR((CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END),'FM999,999,999,999,990.99999'),' extra on total amount')
            WHEN 3 -- NAMED PRICE
            THEN TO_CHAR((CASE WHEN v_numCurrencyID <> numCurrencyID THEN(CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER)*v_fltUOMConversionFactor) ELSE(coalesce(decDiscount,0)*v_fltUOMConversionFactor) END),
               'FM999,999,999,999,990.99999')
            END)
         WHEN 3 -- Named price
         THEN
            TO_CHAR((CASE WHEN v_numCurrencyID <> numCurrencyID THEN(CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER)*v_fltUOMConversionFactor) ELSE(coalesce(decDiscount,0)*v_fltUOMConversionFactor) END),
            'FM999,999,999,999,990.99999')
         END)
         FROM
         PricingTable
         WHERE
         numItemCode = v_numItemCode
         AND coalesce(numPriceRuleID,0) = 0
         AND coalesce(numCurrencyID,0) = coalesce(v_numCurrencyID,0)
         ORDER BY
         numPricingID;
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPPRICELEVEL) then
         v_vcUnitName := coalesce((SELECT vcUnitName FROM UOM WHERE numDomainID = v_numDomainID AND numUOMId = v_numSaleUnit),'each');
         v_vcHtml := CONCAT('<table class="table table-bordered table-pricelevel"><tr><th>Qty (',v_vcUnitName,
         ')</th><th>Price per ',v_vcUnitName,'</th></tr>');
         v_vcHtml := CONCAT(v_vcHtml,COALESCE((SELECT string_agg(CONCAT('<tr><td>',TO_CHAR(numQtyFrom,'FM999,999,999,999,990.99999'),' - ',TO_CHAR(numQtyTo,'FM999,999,999,999,990.99999'),
         '</td><td><span>',v_varCurrSymbol,'</span>',vcPrice,'</td></tr>'),'' ORDER BY ID)
         
         FROM
         tt_TEMPPRICELEVEL),''));
         v_vcHtml := CONCAT(v_vcHtml,'</table>');
      end if;
   end if;

   open SWV_RefCur for SELECT v_vcHtml AS vcHtml;
END; $$;












