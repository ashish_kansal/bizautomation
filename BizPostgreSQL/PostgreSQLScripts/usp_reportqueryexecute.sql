DROP FUNCTION IF EXISTS USP_ReportQueryExecute;

CREATE OR REPLACE FUNCTION USP_ReportQueryExecute
(
	v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
	v_numReportID NUMERIC(18,0),
    v_ClientTimeZoneOffset INTEGER, 
    v_textQuery TEXT,
	v_numCurrentPage NUMERIC(18,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintReportType  INTEGER;
   v_numReportModuleID  INTEGER;
BEGIN
   select   coalesce(tintReportType,0), coalesce(numReportModuleID,0) INTO v_tintReportType,v_numReportModuleID FROM ReportListMaster WHERE numReportID = v_numReportID;

   IF POSITION('WareHouseItems.numBackOrder' IN CAST(v_textQuery AS TEXT)) > 0 AND v_numReportModuleID <> 6 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'WareHouseItems.numBackOrder','(CASE WHEN COALESCE((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID=WareHouseItems.numItemID AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0) >= WareHouseItems.numBackOrder THEN 0 ELSE COALESCE(WareHouseItems.numBackOrder,0) END)');
   end if;
	
   IF POSITION('OpportunityItems.vcOpenBalance' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityItems.vcOpenBalance','(CASE WHEN OpportunityMaster.tintOppType=2 THEN (COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numUnitHourReceived,0)) ELSE (COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyShipped,0)) END)');
   end if;	

   IF POSITION('COALESCE(OpportunityItems.numRemainingQtyToPick,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityItems.numRemainingQtyToPick,0)',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0))');
   end if;	

   IF POSITION('OpportunityItems.numRemainingQtyToPick' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityItems.numRemainingQtyToPick',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0))');
   end if;		

   IF POSITION('COALESCE(OpportunityItems.numRemainingQtyToShip,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityItems.numRemainingQtyToShip,0)',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyShipped,0))');
   end if;	

   IF POSITION('OpportunityItems.numRemainingQtyToShip' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityItems.numRemainingQtyToShip',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyShipped,0))');
   end if;	

   IF POSITION('COALESCE(OpportunityItems.numRemainingQtyToInvoice,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityItems.numRemainingQtyToInvoice,0)',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND COALESCE(bitAuthoritativeBizDocs, 0) = 1 AND COALESCE(OpportunityBizDocs.numBizDocID,0) = 287),0))');
   end if;	

   IF POSITION('OpportunityItems.numRemainingQtyToInvoice' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityItems.numRemainingQtyToInvoice',
      '(COALESCE(OpportunityItems.numUnitHour,0) - COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND COALESCE(bitAuthoritativeBizDocs, 0) = 1 AND COALESCE(OpportunityBizDocs.numBizDocID,0) = 287),0))');
   end if;	

   IF POSITION('AND OpportunityItems.monPendingSales' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'AND OpportunityItems.monPendingSales',
      'AND (COALESCE((COALESCE(COALESCE(fn_UOMConversion(COALESCE(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, COALESCE(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - COALESCE(OpportunityItems.numQtyShipped, 0) ) * (COALESCE(OpportunityItems.monPrice, 0)), 0))');
   end if;	
	
   IF POSITION('AND OpportunityItems.numPendingToBill' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'AND OpportunityItems.numPendingToBill',
      'AND COALESCE((COALESCE(OpportunityItems.numUnitHourReceived, 0)) - (COALESCE(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND COALESCE(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)');
   end if;	
		
   IF POSITION('AND OpportunityItems.numPendingtoReceive' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'AND OpportunityItems.numPendingtoReceive',
      'AND COALESCE((COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND COALESCE(bitAuthoritativeBizDocs, 0) = 1), 0)) - (COALESCE(OpportunityItems.numUnitHourReceived, 0)), 0)');
   end if;

   IF POSITION('AND WareHouseItems.vcLocation' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID');
   end if;

   IF POSITION('ReturnItems.monPrice' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))');
   end if;

   IF POSITION('OpportunityItems.monPrice' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))');
   end if;

   IF POSITION('COALESCE(category.numCategoryID,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(category.numCategoryID,0)','(SELECT COALESCE((SELECT string_agg(Category.vcCategoryName,'','') FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode),''''))');
   end if;

   IF POSITION('FormatedDateFromDate(OpportunityBizDocs.dtFullPaymentDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL,@numDomainID)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'FormatedDateFromDate(OpportunityBizDocs.dtFullPaymentDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL,@numDomainID)','(CASE 
																																															WHEN COALESCE(OpportunityMaster.tintOppType,0) = 2 AND COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0)
																																															THEN FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN COALESCE(OpportunityMaster.tintOppType,0) = 1 AND COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0)
																																															THEN FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppID=OpportunityBizDocs.numOppID AND DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE null::TIMESTAMP
																																														END)');
   end if;

	--KEEP IT BELOW ABOVE REPLACEMENT
   IF POSITION('OpportunityBizDocs.dtFullPaymentDate' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityBizDocs.dtFullPaymentDate',
      'CAST((CASE 
																												WHEN COALESCE(OpportunityMaster.tintOppType,0) = 2 AND COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN COALESCE(OpportunityMaster.tintOppType,0) = 1 AND COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppID=OpportunityBizDocs.numOppID AND DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE null::TIMESTAMP
																											END) AS DATE)');
   end if;

   IF POSITION('FormatedDateFromDate(OpportunityMaster.vcLastSalesOrderDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL,@numDomainID)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'FormatedDateFromDate(OpportunityMaster.vcLastSalesOrderDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL,@numDomainID)','FormatedDateFromDate((SELECT MAX(OMInner.bintCreatedDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL) FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1),@numDomainID)');
   end if;

	--KEEP IT BELOW ABOVE REPLACEMENT
   IF POSITION('OpportunityMaster.vcLastSalesOrderDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityMaster.vcLastSalesOrderDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL',
      'CAST((SELECT MAX(OMInner.bintCreatedDate + (-@ClientTimeZoneOffset || '' minute'')::INTERVAL) AS bintCreatedDate FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1) AS DATE)');
   end if;
	--KEEP IT BELOW ABOVE REPLACEMENT
   IF POSITION('OpportunityMaster.vcLastSalesOrderDate' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityMaster.vcLastSalesOrderDate',
      'CAST((SELECT MAX(OMInner.bintCreatedDate) FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1) AS DATE)');
   end if;

   IF POSITION('COALESCE(OpportunityMaster.monProfit,''0'')' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityMaster.monProfit,''0'')',
      'COALESCE(GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1::SMALLINT),0)');
   end if;

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
   IF POSITION('OpportunityMaster.monProfit' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityMaster.monProfit','COALESCE(GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1::SMALLINT),0)');
   end if;

   IF POSITION('COALESCE(OpportunityBizDocItems.monProfit,''0'')' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityBizDocItems.monProfit,''0'')',
      'COALESCE(GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1::SMALLINT),0)');
   end if;

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
   IF POSITION('OpportunityBizDocItems.monProfit' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityBizDocItems.monProfit','COALESCE(GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1::SMALLINT),0)');
   end if;

   IF POSITION('COALESCE(PT.numItemCode,0)=Item.numItemCode' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(PT.numItemCode,0)=Item.numItemCode',
      'Item.numItemCode=PT.numItemCode');
   end if;

   IF POSITION('COALESCE(OpportunityMaster.numProfitPercent,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityMaster.numProfitPercent,0)',
      'COALESCE(GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2::SMALLINT),0)');
   end if;
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
   IF POSITION('OpportunityMaster.numProfitPercent' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityMaster.numProfitPercent',
      'COALESCE(GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2::SMALLINT),0)');
   end if;

   IF POSITION('COALESCE(OpportunityBizDocItems.numProfitPercent,0)' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'COALESCE(OpportunityBizDocItems.numProfitPercent,0)',
      'COALESCE(GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,2::SMALLINT),0)');
   end if;
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
   IF POSITION('OpportunityBizDocItems.numProfitPercent' IN CAST(v_textQuery AS TEXT)) > 0 then
	
      v_textQuery := REPLACE(CAST(v_textQuery AS TEXT),'OpportunityBizDocItems.numProfitPercent',
      'COALESCE(GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,2::SMALLINT),0)');
   end if;


   IF POSITION('vw_item_pricelevel' IN CAST(v_textQuery AS TEXT)) > 0  THEN --AND NOT EXISTS (SELECT id FROM materializedviewrefreshlog WHERE DateDiff('minute',dtrefresheddate,timezone('utc',now())) <= 60)
		REFRESH MATERIALIZED VIEW vw_item_pricelevel;
		-- IF NOT EXISTS (SELECT FROM materializedviewrefreshlog WHERE vcviewname='vw_item_pricelevel') THEN
			-- INSERT INTO materializedviewrefreshlog (vcviewname,dtrefresheddate) VALUES ('vw_item_pricelevel',timezone('utc',now()));
		-- ELSE
			-- UPDATE materializedviewrefreshlog SET dtrefresheddate=timezone('utc',now()) WHERE vcviewname='vw_item_pricelevel';
		-- END IF;
   END IF;

	IF coalesce(v_numCurrentPage,0) > 0
	AND coalesce(v_tintReportType,0) <> 3
	AND coalesce(v_tintReportType,0) <> 4
	AND coalesce(v_tintReportType,0) <> 5
	AND POSITION('LIMIT' IN CAST(v_textQuery AS TEXT)) = 0
	AND(SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID = v_numReportID) = 0 then
		v_textQuery := CONCAT(v_textQuery,' OFFSET ',((v_numCurrentPage -1)*100), 'LIMIT 100');
	ELSE
		v_textQuery := v_textQuery;
	END IF;
	 
	
	v_textQuery := regexp_replace(v_textQuery,'\+'' - ''\+','|| '' - '' ||','gi');
	v_textQuery := regexp_replace(v_textQuery,'coalesce(warehouseitems.numavailable,0)','coalesce(warehouseitems.numonhand,0) + coalesce(warehouseitems.numallocation,0)','gi');
	v_textQuery := regexp_replace(v_textQuery,'warehouseitems.numavailable','coalesce(warehouseitems.numonhand,0) + coalesce(warehouseitems.numallocation,0)','gi');
	v_textQuery := regexp_replace(v_textQuery,'OpportunityBizDocs.dtDueDate','FormatedDateFromDate(OpportunityBizDocs.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays::INT FROM BillingTerms WHERE numTermsID = COALESCE(OpportunityMaster.intBillingDays,0)), 0)),@numDomainId)','gi');
	v_textQuery := regexp_replace(v_textQuery,'\+'' - ''\+','|| '' - '' ||','gi');
	v_textQuery := regexp_replace(v_textQuery,'@numdomainid',v_numDomainID::VARCHAR,'gi');
	v_textQuery := regexp_replace(v_textQuery,'@numUserCntID',v_numUserCntID::VARCHAR,'gi');
	v_textQuery := regexp_replace(v_textQuery,'-@ClientTimeZoneOffset','-1 * ' || v_ClientTimeZoneOffset::VARCHAR,'gi');
	v_textQuery := regexp_replace(v_textQuery,'@ClientTimeZoneOffset',v_ClientTimeZoneOffset::VARCHAR,'gi');
	v_textQuery := regexp_replace(v_textQuery,'CustomItemPriceTable.numdomainid = Item.numdomainid AND','','gi');
	v_textQuery := regexp_replace(v_textQuery,'CAST\(numFromQtyBefore AS VARCHAR\)','','gi');
	v_textQuery := regexp_replace(v_textQuery,'CAST\(numFromQtyAfter AS VARCHAR\)','','gi');
	v_textQuery := regexp_replace(v_textQuery,'CAST\(numToQtyBefore AS VARCHAR\)','','gi');
	v_textQuery := regexp_replace(v_textQuery,'CAST\(numToQtyAfter AS VARCHAR\)','','gi');
	v_textQuery := regexp_replace(v_textQuery,''''' AS numFromQtyBefore','0 AS numFromQtyBefore','gi');
	v_textQuery := regexp_replace(v_textQuery,''''' AS numFromQtyAfter','0 AS numFromQtyAfter','gi');
	v_textQuery := regexp_replace(v_textQuery,''''' AS numToQtyBefore','0 AS numToQtyBefore','gi');
	v_textQuery := regexp_replace(v_textQuery,''''' AS numToQtyAfter','0 AS numToQtyAfter','gi');
	 

	RAISE NOTICE '%',CAST(v_textQuery AS TEXT);

	OPEN SWV_RefCur FOR EXECUTE v_textQuery;
   
	RETURN;
END; $$;
