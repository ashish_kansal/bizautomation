-- Stored procedure definition script usp_getApproverItemUnitPrice for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getApproverItemUnitPrice(v_numDomainId NUMERIC(18,2) DEFAULT 0,
  v_numoppId NUMERIC(18,2) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listIds  TEXT;
BEGIN
   select   COALESCE(coalesce(v_listIds,'') || ',','') || SUBSTR(CAST(U.vcUserName AS VARCHAR(100)),1,100) INTO v_listIds FROM UnitPriceApprover AS UP
   LEFT JOIN UserMaster AS U ON UP.numUserID = U.numUserId WHERE UP.numDomainID = v_numDomainId;
   open SWV_RefCur for SELECT v_listIds AS UserNames;
END; $$;














