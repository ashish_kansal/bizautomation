-- Stored procedure definition script USP_ManageProcesstoOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageProcesstoOpportunity(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numProcessId NUMERIC(18,0) DEFAULT 0,      
v_numCreatedBy NUMERIC(18,0) DEFAULT 0,
v_numOppId NUMERIC(18,0) DEFAULT 0,   
v_numProjectId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewProcessId  NUMERIC(18,0) DEFAULT 0;
BEGIN
   INSERT  INTO Sales_process_List_Master(Slp_Name,
                                                 numDomainID,
                                                 Pro_Type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numProjectId)
   SELECT Slp_Name,
            numDomainID,
            Pro_Type,
            numCreatedby,
            dtCreatedon,
            numModifedby,
            dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,v_numOppId,numTaskValidatorId,v_numProjectId FROM Sales_process_List_Master WHERE Slp_Id = v_numProcessId;    

   v_numNewProcessId := CURRVAL('sales_process_list_master_seq');  
   IF(v_numOppId > 0) then
	
      UPDATE OpportunityMaster SET numBusinessProcessID = v_numNewProcessId WHERE numOppId = v_numOppId;
   end if;
   IF(v_numProjectId > 0) then
	
      UPDATE ProjectsMaster SET numBusinessProcessID = v_numNewProcessId WHERE numProId = v_numProjectId;
   end if;

   INSERT INTO StagePercentageDetails(numStagePercentageId,
		tintConfiguration,
		vcStageName,
		numdomainid,
		numCreatedBy,
		bintCreatedDate,
		numModifiedBy,
		bintModifiedDate,
		slp_id,
		numAssignTo,
		vcMileStoneName,
		tintPercentage,
		tinProgressPercentage,
		dtStartDate,
		numParentStageID,
		intDueDays,
		numProjectid,
		numOppid,
		vcDescription,
		bitIsDueDaysUsed,
		numTeamId,
		bitRunningDynamicMode,
		numStageOrder,
		numParentStageDetailsId)
   SELECT
   numStagePercentageId,
		tintConfiguration,
		vcStageName,
		numdomainid,
		v_numCreatedBy,
		LOCALTIMESTAMP,
		v_numCreatedBy,
		LOCALTIMESTAMP,
		v_numNewProcessId,
		numAssignTo,
		vcMileStoneName,
		tintPercentage,
		tinProgressPercentage,
		LOCALTIMESTAMP,
		numParentStageID,
		intDueDays,
		v_numProjectId,
		v_numOppId,
		vcDescription,
		bitIsDueDaysUsed,
		numTeamId,
		bitRunningDynamicMode,
		numStageOrder,
		numStageDetailsId
   FROM
   StagePercentageDetails
   WHERE
   slp_id = v_numProcessId;	

   INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
		vcTaskName,
		numHours,
		numMinutes,
		numAssignTo,
		numDomainID,
		numCreatedBy,
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask,
		numOrder,
		numReferenceTaskId)
   SELECT
   CASE WHEN v_numOppId > 0
   THEN(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numOppid = v_numOppId AND SFD.numParentStageDetailsId = SP.numStageDetailsId LIMIT 1)
   WHEN v_numProjectId > 0
   THEN(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numProjectid = v_numProjectId AND SFD.numParentStageDetailsId = SP.numStageDetailsId LIMIT 1) ELSE 0 END,
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		v_numDomainID,
		v_numCreatedBy,
		LOCALTIMESTAMP,
		v_numOppId,
		v_numProjectId,
		0,
		true,
		CASE WHEN SP.bitRunningDynamicMode = true THEN false ELSE true END,
		numOrder,
		numTaskId
   FROM
   StagePercentageDetailsTask AS ST
   LEFT JOIN
   StagePercentageDetails As SP
   ON
   ST.numStageDetailsId = SP.numStageDetailsId
   WHERE
   coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numProjectId,0) = 0 AND
   ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
      StagePercentageDetails As ST
      WHERE
      ST.slp_id = v_numProcessId)
   ORDER BY ST.numOrder;
RETURN;
END; $$; 



