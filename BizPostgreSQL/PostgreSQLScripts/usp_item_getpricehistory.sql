-- Stored procedure definition script USP_Item_GetPriceHistory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetPriceHistory(v_numDomainID NUMERIC(18,0),
	v_tintOppType SMALLINT,
	v_numItemCode NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER,
	v_numDivisionID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		
   OpportunityMaster.numOppId,
		FormatedDateFromDate(OpportunityMaster.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS vcCreatedDate,
		vcpOppName,
		numUnitHour/fn_UOMConversion(OpportunityItems.numUOMId,OpportunityItems.numItemCode,v_numDomainID,Item.numBaseUnit) AS numUnitHour,
		monPrice AS monPrice,
		numUOMId,
		fn_GetUOMName(numUOMId) AS vcUOMName,
		fn_UOMConversion(numUOMId,OpportunityItems.numItemCode,v_numDomainID,Item.numBaseUnit) AS decUOMConversionFactor,
		(CASE WHEN coalesce(OpportunityItems.bitDiscountType,false) = true THEN CONCAT(CAST(coalesce(OpportunityItems.fltDiscount,0) AS DOUBLE PRECISION),' Flat Off') ELSE CONCAT(CAST(coalesce(OpportunityItems.fltDiscount,0) AS DOUBLE PRECISION),'%') END) as vcDiscount,
		coalesce(OpportunityItems.fltDiscount,0) AS fltDiscount,
		OpportunityItems.bitDiscountType
   FROM
   OpportunityMaster
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND OpportunityItems.numItemCode = v_numItemCode
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND tintopptype = v_tintOppType
   AND (numDivisionId = v_numDivisionID OR coalesce(v_numDivisionID,0) = 0)
   ORDER BY
   OpportunityMaster.numOppId DESC LIMIT 30;
END; $$;












