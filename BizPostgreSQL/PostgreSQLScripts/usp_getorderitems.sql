DROP FUNCTION IF EXISTS USP_GetOrderItems;

CREATE OR REPLACE FUNCTION USP_GetOrderItems(v_tintMode SMALLINT DEFAULT 0,
                                     v_numOppItemID NUMERIC(9,0) DEFAULT 0,
                                     v_numOppID NUMERIC(9,0) DEFAULT 0,
                                     v_numDomainId NUMERIC(9,0) DEFAULT NULL,
                                     v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
									 v_vcOppItemIDs VARCHAR(2000) DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
   v_tintUnitsRecommendationForAutoPOBackOrder SMALLINT;
   v_bitincluderequisitions BOOLEAN;
BEGIN
   IF v_tintMode = 1 then --Add Return grid data & For Recurring Get Original Units to split
        
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode AS "numoppitemtCode",
                    OI.numItemCode AS "numItemCode" ,
                    OI.vcItemName AS "vcItemName",
                    COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",
					OI.vcNotes as "txtNotes",coalesce(OI.numCost,0) AS "numCost",
                    OI.numUnitHour AS "numOriginalUnitHour",
					CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OI.numUnitHour) AS NUMERIC(18,2)) AS "numUnitHour",
					OI.monPrice AS "monPrice",
					coalesce(OI.bitMappingRequired,false) AS "bitMappingRequired"
      FROM    OpportunityMaster OM
      INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      WHERE   OM.numDomainId = v_numDomainId
      AND OM.numOppId = v_numOppID;
   end if;
 
   IF v_tintMode = 2 then -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode AS "numoppitemtCode",
                    OI.numOppId AS "numOppId",
                    OI.numItemCode AS "numItemCode",
					OI.bitItemPriceApprovalRequired AS "bitItemPriceApprovalRequired",
                    OI.numUnitHour AS "numOriginalUnitHour",
                    OI.monPrice AS "monPrice",
                    OI.monTotAmount AS "monTotAmount",
                    OI.numSourceID AS "numSourceID",
					coalesce(OI.numCost,0) AS "numCost",
                    COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",
					OI.vcNotes AS "txtNotes",
                    OI.numWarehouseItmsID AS "numWarehouseItmsID",
                    OI.vcType AS "vcType",
                    OI.vcAttributes AS "vcAttributes",
                    OI.bitDropShip AS "bitDropShip",
                    coalesce(OI.numUnitHourReceived,0) AS "numUnitHourReceived",
                    OI.bitDiscountType AS "bitDiscountType",
                    OI.fltDiscount AS "fltDiscount",
                    OI.monTotAmtBefDiscount AS "monTotAmtBefDiscount",
                    coalesce(OI.numQtyShipped,0) AS "numQtyShipped",
                    OI.vcItemName AS "vcItemName",
                    OI.vcModelID AS "vcModelID",
                    OI.vcPathForTImage AS "vcPathForTImage",
                    OI.vcManufacturer AS "vcManufacturer",
                    OI.monVendorCost AS "monVendorCost",
                    coalesce(OI.numUOMId,0) AS "numUOMId",
                    coalesce(OI.numUOMId,0) AS "numUOM", -- used from add edit order
                    coalesce(U.vcUnitName,'') AS "vcUOMName",
                    fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,NULL::NUMERIC) AS "UOMConversionFactor",
                    OI.bitWorkOrder AS "bitWorkOrder",
                    coalesce(OI.numVendorWareHouse,0) as "numVendorWareHouse",
                    OI.numShipmentMethod AS "numShipmentMethod",
                    OI.numSOVendorId AS "numSOVendorId",
                    OI.numProjectID AS "numProjectID",
                    OI.numClassID AS "numClassID",
                    OI.numProjectStageID AS "numProjectStageID",
                    OI.numToWarehouseItemID AS "numToWarehouseItemID",
                    WItems.numWareHouseID AS "numWareHouseID",
                    W.vcWareHouse AS "Warehouse",
					CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OI.numUnitHour) AS NUMERIC(18,2)) AS "numUnitHour",
					coalesce(OI.numClassID,0) as "numClassID",
					coalesce(OI.numProjectID,0) as "numProjectID",
					coalesce(fn_GetUOMName(I.numBaseUnit),'') AS "vcBaseUOMName",
					OI.bitDropShip AS "bitAllowDropShip",
					I.numVendorID AS "numVendorID",
					coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.txtItemDesc,'') AS "txtItemDesc",
					coalesce(I.vcModelID,'') AS "vcModelID",
					coalesce(I.vcManufacturer,'') AS "vcManufacturer",
					coalesce(I.numBarCodeId,'') AS "numBarCodeId",
					coalesce(I.bitSerialized,false) AS "bitSerialized",
					coalesce(I.bitKitParent,false) AS "bitKitParent",
					coalesce(I.bitAssembly,false) AS "bitAssembly",
					coalesce(I.IsArchieve,false) AS "IsArchieve",
					coalesce(I.bitLotNo,false) AS "bitLotNo",
					coalesce(WItems.monWListPrice,0) AS "monWListPrice",
					coalesce(I.bitTaxable,false) AS "bitTaxable",
					coalesce(I.numItemClassification,0) AS "numItemClassification",
                    I.charItemType AS "charItemType",
                    I.bitKitParent AS "bitKitParent",
					I.bitAssembly AS "bitAssembly",
					I.bitSerialized AS "bitSerialized",
					coalesce(I.fltWeight,0) AS "fltWeight",
					coalesce(I.fltLength,0) AS "fltLength",
					coalesce(I.fltWidth,0) AS "fltWidth",
					coalesce(I.fltHeight,0) AS "fltHeight",
					CASE WHEN I.numItemGroup > 0 AND coalesce(I.bitMatrix,false) = false THEN coalesce(WItems.vcWHSKU,coalesce(I.vcSKU,'')) ELSE coalesce(I.vcSKU,'') END AS "vcSKU",
					(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) AS "monAverageCost",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numOnHand),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnHand",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numonOrder),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnOrder",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numAllocation),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numAllocation",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numBackOrder),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numBackOrder",
					coalesce(OI.vcAttrValues,'') AS "vcAttrValues",
					coalesce(OI.numSortOrder,0) AS "numSortOrder"
					,(CASE WHEN OM.tintopptype = 2 THEN coalesce(vcNotes,'') ELSE '' END) AS "vcVendorNotes"
					,CPN.CustomerPartNo AS "CustomerPartNo"
					,coalesce(OI.vcASIN,'') AS "vcASIN"
					,coalesce(OI.vcSKU,'') AS "vcSKUOpp"
      FROM    OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
      INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
      LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
      LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
      LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode
      AND CPN.numCompanyId = DM.numCompanyID
      WHERE   OI.numOppId = v_numOppID
      AND (OI.numoppitemtCode = v_numOppItemID
      OR v_numOppItemID = 0)
      AND OM.numDomainId = v_numDomainId;
   end if;
 
   IF v_tintMode = 3 then -- Get item for SO to PO and vice versa creation
      select   tintopptype INTO v_tintOppType FROM OpportunityMaster WHERE numOppId = v_numOppID;
	  SELECT COALESCE(tintUnitsRecommendationForAutoPOBackOrder,1),COALESCE(bitincluderequisitions,false) INTO v_tintUnitsRecommendationForAutoPOBackOrder,v_bitincluderequisitions FROM Domain WHERE numDomainID=v_numDomainId;
      IF v_tintOppType = 1 then
         BEGIN
            CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEMS
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numItemCode NUMERIC(18,0),
            vcItemName VARCHAR(300),
            txtNotes TEXT,
            charItemType CHAR,
            vcModelID VARCHAR(100),
            vcItemDesc TEXT,
            vcType VARCHAR(20),
            vcPathForTImage VARCHAR(200),
            numWarehouseItmsID NUMERIC(18,0),
            vcAttributes VARCHAR(500),
            bitDropShip BOOLEAN,
            numOrigUnitHour DOUBLE PRECISION,
			numOnHand DOUBLE PRECISION,
            numUnitHour DOUBLE PRECISION,
            numUOM NUMERIC(18,0),
            vcUOMName VARCHAR(100),
            UOMConversionFactor DECIMAL(18,5),
            monPrice DECIMAL(20,5),
            numVendorID NUMERIC(18,0),
            numCost DECIMAL(30,16),
            vcPartNo VARCHAR(300),
            monVendorCost DECIMAL(20,5),
            numSOVendorId NUMERIC(18,0),
            numClassID NUMERIC(18,0),
            numProjectID NUMERIC(18,0),
            tintLevel SMALLINT, 
            vcAttrValues VARCHAR(500),
            ItemRequiredDate TIMESTAMP,
            numPOID NUMERIC(18,0), 
            numPOItemID NUMERIC(18,0),
            vcPOName VARCHAR(300),
			numItemRepQuantity NUMERIC
         );
         INSERT INTO
         tt_TEMPITEMS(numItemCode, vcItemName, txtNotes, charItemType, vcModelID, vcItemDesc, vcType, vcPathForTImage, numWarehouseItmsID, vcAttributes, bitDropShip, numOrigUnitHour, numOnHand, numUnitHour, numUOM, vcUOMName, UOMConversionFactor, monPrice, numVendorID, numCost, vcPartNo, monVendorCost, numSOVendorId, numClassID, numProjectID, tintLevel, vcAttrValues, ItemRequiredDate, numPOID, numPOItemID, vcPOName, numItemRepQuantity)
         SELECT
         OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,COALESCE(OI.vcItemDesc,''),OI.vcType,coalesce(OI.vcPathForTImage,''),coalesce(OI.numWarehouseItmsID,0),coalesce(OI.vcAttributes,''),
					coalesce(OI.bitDropShip,false),coalesce(OI.numUnitHour,0),COALESCE(WI.numOnHand,0) + COALESCE(WI.numAllocation,0),OI.numUnitHour,coalesce(U.numUOMId,0),coalesce(U.vcUnitName,''),1 AS UOMConversionFactor,
					OI.monPrice,coalesce(I.numVendorID,0),coalesce(OI.numCost,0),coalesce(V.vcPartNo,''),coalesce(V.monCost,0),coalesce(OI.numSOVendorId,0),0,0,0,coalesce(OI.vcAttrValues,'')
					,(CASE WHEN coalesce(OI.ItemReleaseDate,null) = null THEN
            coalesce(OM.dtReleaseDate,'1900-01-01 00:00:00')
         ELSE OI.ItemReleaseDate
         END),coalesce(OMPO.numOppID,0),1,CAST(coalesce(OMPO.vcpOppName,'') AS VARCHAR(300)),
		 (CASE
			WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
			THEN(CASE WHEN coalesce(fltReorderQty,0) > coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																																																															SUM(numUnitHour)
																																																															FROM
																																																															OpportunityItems
																																																															INNER JOIN
																																																															OpportunityMaster
																																																															ON
																																																															OpportunityItems.numOppId = OpportunityMaster.numOppId
																																																															WHERE
																																																															OpportunityMaster.numDomainId = v_numDomainID
																																																															AND OpportunityMaster.tintopptype = 2
																																																															AND OpportunityMaster.tintoppstatus = 0
																																																															AND OpportunityItems.numItemCode = I.numItemCode
																																																															AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
			WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
			THEN(CASE
					WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) AND coalesce(fltReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(fltReorderQty,0)
					WHEN coalesce(intMinQty,0) >= coalesce(fltReorderQty,0) AND coalesce(intMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(intMinQty,0)
					WHEN coalesce(numBackOrder,0) >= coalesce(fltReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(intMinQty,0) THEN coalesce(numBackOrder,0)
					ELSE coalesce(fltReorderQty,0)
				END) - (coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																											SUM(numUnitHour)
																											FROM
																											OpportunityItems
																											INNER JOIN
																											OpportunityMaster
																											ON
																											OpportunityItems.numOppId = OpportunityMaster.numOppId
																											WHERE
																											OpportunityMaster.numDomainId = v_numDomainID
																											AND OpportunityMaster.tintopptype = 2
																											AND OpportunityMaster.tintoppstatus = 0
																											AND OpportunityItems.numItemCode = I.numItemCode
																											AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
			WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
			THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																														   SUM(numUnitHour)
																														   FROM
																														   OpportunityItems
																														   INNER JOIN
																														   OpportunityMaster
																														   ON
																														   OpportunityItems.numOppId = OpportunityMaster.numOppId
																														   WHERE
																														   OpportunityMaster.numDomainId = v_numDomainID
																														   AND OpportunityMaster.tintopptype = 2
																														   AND OpportunityMaster.tintoppstatus = 0
																														   AND OpportunityItems.numItemCode = I.numItemCode
																														   AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)
			ELSE 0
         END)
         FROM
         OpportunityItems OI
		 INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWarehouseItemID
         INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
         INNER JOIN Item I ON I.numItemCode = OI.numItemCode
         LEFT JOIN UOM U ON U.numUOMId = I.numBaseUnit
         LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
         LEFT JOIN LATERAL (SELECT * FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode=OI.numItemCode AND COALESCE(OIInner.numWarehouseItmsID,0)=COALESCE(OI.numWarehouseItmsID,0) LIMIT 1) SOLIPL ON TRUE
         LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numChildOppID = OMPO.numOppId
         WHERE
         OI.numOppId = v_numOppID
         AND OM.numDomainId = v_numDomainId
         AND coalesce(I.bitKitParent,false) = false
         AND coalesce(I.bitAssembly,false) = false
		 AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode=OI.numItemCode AND COALESCE(OIInner.numWarehouseItmsID,0)=COALESCE(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)
         ORDER BY
         OI.numSortOrder;


			--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
         INSERT INTO
         tt_TEMPITEMS(numItemCode, vcItemName, txtNotes, charItemType, vcModelID, vcItemDesc, vcType, vcPathForTImage, numWarehouseItmsID, vcAttributes, bitDropShip, numOrigUnitHour, numOnHand, numUnitHour, numUOM, vcUOMName, UOMConversionFactor, monPrice, numVendorID, numCost, vcPartNo, monVendorCost, numSOVendorId, numClassID, numProjectID, tintLevel, vcAttrValues, ItemRequiredDate, numPOID, numPOItemID, vcPOName, numItemRepQuantity)
         SELECT
         OKI.numChildItemID,I.vcItemName,CAST('' AS VARCHAR(300)) as txtNotes,I.charItemType,I.vcModelID,I.txtItemDesc,CAST((CASE WHEN I.charItemType = 'P' THEN 'Product' WHEN I.charItemType = 'S' THEN 'Service' END) AS VARCHAR(20)),
					coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),''),coalesce(OKI.numWareHouseItemId,0),
					CAST(coalesce(USP_GetAttributes(OKI.numWareHouseItemId,bitSerialized),'') AS VARCHAR(500)),CAST(0 AS BOOLEAN),coalesce(OKI.numQtyItemsReq,0),COALESCE(WI.numOnHand,0) + COALESCE(WI.numAllocation,0),coalesce(OKI.numQtyItemsReq,0),coalesce(U.numUOMId,0),coalesce(U.vcUnitName,''),
					1,
					(CASE WHEN I.charItemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					coalesce(I.numVendorID,0),coalesce(t1.numCost,0),coalesce(V.vcPartNo,''),coalesce(V.monCost,0),0,0,0,1,CAST('' AS VARCHAR(500)),
					t1.ItemRequiredDate,coalesce(OMPO.numOppID,0),1,CAST(coalesce(OMPO.vcpOppName,'') AS VARCHAR(300)),
					(CASE
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
						THEN(CASE WHEN coalesce(fltReorderQty,0) > coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																																																																		SUM(numUnitHour)
																																																																		FROM
																																																																		OpportunityItems
																																																																		INNER JOIN
																																																																		OpportunityMaster
																																																																		ON
																																																																		OpportunityItems.numOppId = OpportunityMaster.numOppId
																																																																		WHERE
																																																																		OpportunityMaster.numDomainId = v_numDomainID
																																																																		AND OpportunityMaster.tintopptype = 2
																																																																		AND OpportunityMaster.tintoppstatus = 0
																																																																		AND OpportunityItems.numItemCode = I.numItemCode
																																																																		AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
						THEN(CASE
								WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) AND coalesce(fltReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(fltReorderQty,0)
								WHEN coalesce(intMinQty,0) >= coalesce(fltReorderQty,0) AND coalesce(intMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(intMinQty,0)
								WHEN coalesce(numBackOrder,0) >= coalesce(fltReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(intMinQty,0) THEN coalesce(numBackOrder,0)
								ELSE coalesce(fltReorderQty,0)
							END) - (coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																														   SUM(numUnitHour)
																														   FROM
																														   OpportunityItems
																														   INNER JOIN
																														   OpportunityMaster
																														   ON
																														   OpportunityItems.numOppId = OpportunityMaster.numOppId
																														   WHERE
																														   OpportunityMaster.numDomainId = v_numDomainID
																														   AND OpportunityMaster.tintopptype = 2
																														   AND OpportunityMaster.tintoppstatus = 0
																														   AND OpportunityItems.numItemCode = I.numItemCode
																														   AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
						THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																														   SUM(numUnitHour)
																														   FROM
																														   OpportunityItems
																														   INNER JOIN
																														   OpportunityMaster
																														   ON
																														   OpportunityItems.numOppId = OpportunityMaster.numOppId
																														   WHERE
																														   OpportunityMaster.numDomainId = v_numDomainID
																														   AND OpportunityMaster.tintopptype = 2
																														   AND OpportunityMaster.tintoppstatus = 0
																														   AND OpportunityItems.numItemCode = I.numItemCode
																														   AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)
						ELSE 0
					 END)
         FROM
         OpportunityKitItems OKI
		 LEFT JOIN tt_TEMPITEMS t1 ON OKI.numChildItemID = t1.numItemCode AND coalesce(OKI.numWareHouseItemId,0) = coalesce(t1.numWarehouseItmsID,0)
         INNER JOIN WareHouseItems WI ON OKI.numWareHouseItemId = WI.numWareHouseItemID
         INNER JOIN OpportunityMaster OM ON OM.numOppId = OKI.numOppId
         INNER JOIN Item I ON I.numItemCode = OKI.numChildItemID
         LEFT JOIN UOM U ON U.numUOMId = I.numBaseUnit
         LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		 LEFT JOIN LATERAL (SELECT * FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode=I.numItemCode AND COALESCE(OIInner.numWarehouseItmsID,0)=COALESCE(OKI.numWareHouseItemId,0) LIMIT 1) SOLIPL ON TRUE
         LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numChildOppID = OMPO.numOppId
         WHERE
         OKI.numOppId = v_numOppID
         AND OM.numDomainId = v_numDomainId
         AND coalesce(I.bitKitParent,false) = false
         AND coalesce(I.bitAssembly,false) = false
		 AND t1.ID IS NULL;

		 -- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
         UPDATE
			tt_TEMPITEMS t2
         SET
			numOrigUnitHour = coalesce(t2.numOrigUnitHour,0)+coalesce(OKI.numQtyItemsReq,0),numUnitHour = coalesce(t2.numUnitHour,0)+(coalesce(OKI.numQtyItemsReq,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,OM.numDomainId,OKI.numUOMId))
			,monPrice =(CASE
							WHEN coalesce(t2.monPrice,0) <(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)
							THEN t2.monPrice
							ELSE (CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)
						END)
         FROM
			OpportunityKitItems OKI 
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OKI.numWareHouseItemId = WI.numWareHouseItemID 
		INNER JOIN
			OpportunityMaster OM 
		ON 
			OM.numOppId = OKI.numOppId 
		INNER JOIN
			Item I 
		ON 
			I.numItemCode = OKI.numChildItemID 
		LEFT JOIN 
			UOM U 
		ON 
			U.numUOMId = OKI.numUOMId 
			LEFT JOIN Vendor V 
		ON 
			V.numVendorID = I.numVendorID
			AND I.numItemCode = V.numItemCode
		INNER JOIN LATERAL 
		(
			SELECT * FROM tt_TEMPITEMS t3 WHERE t3.numItemCode = OKI.numChildItemID AND coalesce(t3.numWarehouseItmsID,0)=coalesce(OKI.numWareHouseItemId,0) LIMIT 1
		) TempItem ON TRUE
        WHERE
			t2.ID = TempItem.ID
			AND (OKI.numOppId = v_numOppID
				 AND OM.numDomainId = v_numDomainId
				 AND coalesce(I.bitKitParent,false) = false
				 AND coalesce(I.bitAssembly,false) = false
				 AND t2.tintLevel <> 1);

		--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
         INSERT INTO
         tt_TEMPITEMS(numItemCode, vcItemName, txtNotes, charItemType, vcModelID, vcItemDesc, vcType, vcPathForTImage, numWarehouseItmsID, vcAttributes, bitDropShip, numOrigUnitHour, numOnHand, numUnitHour, numUOM, vcUOMName, UOMConversionFactor, monPrice, numVendorID, numCost, vcPartNo, monVendorCost, numSOVendorId, numClassID, numProjectID, tintLevel, vcAttrValues, ItemRequiredDate, numPOID, numPOItemID, vcPOName, numItemRepQuantity)
         SELECT
         OKCI.numItemID,I.vcItemName,CAST('' AS VARCHAR(300)) as txtNotes,I.charItemType,I.vcModelID,I.txtItemDesc,CAST((CASE WHEN I.charItemType = 'P' THEN 'Product' WHEN I.charItemType = 'S' THEN 'Service' END) AS VARCHAR(20)),
					coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),''),coalesce(OKCI.numWareHouseItemId,0),
					CAST(coalesce(USP_GetAttributes(OKCI.numWareHouseItemId,bitSerialized),'') AS VARCHAR(500)),CAST(0 AS BOOLEAN),coalesce(OKCI.numQtyItemsReq,0),COALESCE(WI.numOnHand,0) + COALESCE(WI.numAllocation,0),coalesce(OKCI.numQtyItemsReq,0),coalesce(U.numUOMId,0),coalesce(U.vcUnitName,''),
					1,
					(CASE WHEN I.charItemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					coalesce(I.numVendorID,0),coalesce(t1.numCost,0),coalesce(V.vcPartNo,''),coalesce(V.monCost,0),0,0,0,2,CAST('' AS VARCHAR(500)),
					t1.ItemRequiredDate,coalesce(OMPO.numOppID,0),1,CAST(coalesce(OMPO.vcpOppName,'') AS VARCHAR(300)),
					(CASE
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
						THEN(CASE WHEN coalesce(fltReorderQty,0) > coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																											SUM(numUnitHour)
																											FROM
																											OpportunityItems
																											INNER JOIN
																											OpportunityMaster
																											ON
																											OpportunityItems.numOppId = OpportunityMaster.numOppId
																											WHERE
																											OpportunityMaster.numDomainId = v_numDomainID
																											AND OpportunityMaster.tintopptype = 2
																											AND OpportunityMaster.tintoppstatus = 0
																											AND OpportunityItems.numItemCode = I.numItemCode
																											AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
						THEN(CASE
								WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) AND coalesce(fltReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(fltReorderQty,0)
								WHEN coalesce(intMinQty,0) >= coalesce(fltReorderQty,0) AND coalesce(intMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(intMinQty,0)
								WHEN coalesce(numBackOrder,0) >= coalesce(fltReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(intMinQty,0) THEN coalesce(numBackOrder,0)
								ELSE coalesce(fltReorderQty,0)
							END) - (coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																														   SUM(numUnitHour)
																														   FROM
																														   OpportunityItems
																														   INNER JOIN
																														   OpportunityMaster
																														   ON
																														   OpportunityItems.numOppId = OpportunityMaster.numOppId
																														   WHERE
																														   OpportunityMaster.numDomainId = v_numDomainID
																														   AND OpportunityMaster.tintopptype = 2
																														   AND OpportunityMaster.tintoppstatus = 0
																														   AND OpportunityItems.numItemCode = I.numItemCode
																														   AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
						WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
						THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
																														   SUM(numUnitHour)
																														   FROM
																														   OpportunityItems
																														   INNER JOIN
																														   OpportunityMaster
																														   ON
																														   OpportunityItems.numOppId = OpportunityMaster.numOppId
																														   WHERE
																														   OpportunityMaster.numDomainId = v_numDomainID
																														   AND OpportunityMaster.tintopptype = 2
																														   AND OpportunityMaster.tintoppstatus = 0
																														   AND OpportunityItems.numItemCode = I.numItemCode
																														   AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(fltReorderQty,0) >= coalesce(intMinQty,0) THEN coalesce(fltReorderQty,0) ELSE coalesce(intMinQty,0) END)
						ELSE 0
					 END)
         FROM
         OpportunityKitChildItems OKCI
		 LEFT JOIN tt_TEMPITEMS t1 
		 ON 
		 OKCI.numItemID = t1.numItemCode 
		 AND coalesce(OKCI.numWareHouseItemId,0) = coalesce(t1.numWarehouseItmsID,0)
         INNER JOIN WareHouseItems WI ON OKCI.numWareHouseItemId = WI.numWareHouseItemID
         INNER JOIN OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
         INNER JOIN Item I ON I.numItemCode = OKCI.numItemID
         LEFT JOIN UOM U ON U.numUOMId = I.numBaseUnit
         LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		LEFT JOIN LATERAL (SELECT * FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode=I.numItemCode AND COALESCE(OIInner.numWarehouseItmsID,0)=COALESCE(OKCI.numWareHouseItemId,0) LIMIT 1) SOLIPL ON TRUE
         LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numChildOppID = OMPO.numOppId
         WHERE
         OKCI.numOppId = v_numOppID
         AND OM.numDomainId = v_numDomainId
         AND coalesce(I.bitKitParent,false) = false
         AND coalesce(I.bitAssembly,false) = false
		 AND t1.ID IS NULL;

		-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
         UPDATE
         tt_TEMPITEMS t2
         SET
         numOrigUnitHour = coalesce(t2.numOrigUnitHour,0)+coalesce(OKCI.numQtyItemsReq,0),numUnitHour = coalesce(t2.numUnitHour,0)+(coalesce(OKCI.numQtyItemsReq,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,OM.numDomainId,OKCI.numUOMId)),monPrice =(CASE
         WHEN coalesce(t2.monPrice,0) <(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)
         THEN t2.monPrice
         ELSE(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)
         END)
         FROM
         OpportunityKitChildItems OKCI 
		 LEFT JOIN 
		 WareHouseItems WI 
		 ON 
		 OKCI.numWareHouseItemId = WI.numWareHouseItemID 
		 INNER JOIN 
		 OpportunityMaster OM 
		 ON 
		 OM.numOppId = OKCI.numOppId 
		 INNER JOIN 
		 Item I 
		 ON 
		 I.numItemCode = OKCI.numItemID 
		 LEFT JOIN 
		 UOM U 
		 ON U.numUOMId = OKCI.numUOMId 
		 LEFT JOIN 
		 Vendor V 
		 ON 
		 V.numVendorID = I.numVendorID	
		 AND I.numItemCode = V.numItemCode
		 INNER JOIN LATERAL 
		(
			SELECT * FROM tt_TEMPITEMS t3 WHERE t3.numItemCode = OKCI.numItemID AND coalesce(t3.numWarehouseItmsID,0)=coalesce(OKCI.numWareHouseItemId,0) LIMIT 1
		) TempItem ON TRUE
         WHERE 
		 t2.ID = TempItem.ID
		 AND(OKCI.numOppId = v_numOppID
         AND OM.numDomainId = v_numDomainId
         AND coalesce(I.bitKitParent,false) = false
         AND coalesce(I.bitAssembly,false) = false
         AND t2.tintLevel <> 2);

         open SWV_RefCur for
         SELECT 
			0 AS "numoppitemtCode",
            numItemCode AS "numItemCode",
            vcItemName AS "vcItemName",
            txtNotes AS "txtNotes",
            charItemType AS "charItemType",
            vcModelID AS "vcModelID",
            vcItemDesc AS "vcItemDesc",
            vcType AS "vcType",
            vcPathForTImage AS "vcPathForTImage",
            numWarehouseItmsID AS "numWarehouseItmsID",
            vcAttributes AS "vcAttributes",
            bitDropShip AS "bitDropShip",
            numOrigUnitHour AS "numOrigUnitHour",
			numOnHand AS "numOnHand",
            (CASE WHEN COALESCE(numOnHand,0) >= COALESCE(numUnitHour,0) THEN 0 ELSE COALESCE(numUnitHour,0) - COALESCE(numOnHand,0) END) AS "numUnitHour",
            numUOM AS "numUOM",
            vcUOMName AS "vcUOMName",
            UOMConversionFactor AS "UOMConversionFactor",
            monPrice AS "monPrice",
            numVendorID AS "numVendorID",
            numCost AS "numCost",
            vcPartNo AS "vcPartNo",
            monVendorCost AS "monVendorCost",
            numSOVendorId AS "numSOVendorId",
            numClassID AS "numClassID",
            numProjectID AS "numProjectID",
            tintLevel AS "tintLevel", 
            vcAttrValues AS "vcAttrValues",
            ItemRequiredDate AS "ItemRequiredDate",
            numPOID AS "numPOID", 
            numPOItemID AS "numPOItemID",
            vcPOName AS "vcPOName",
			COALESCE(numItemRepQuantity,0) AS "numItemRepQuantity"
		 
		 FROM tt_TEMPITEMS;
      ELSE
         open SWV_RefCur for
         SELECT  
			0 AS "numoppitemtCode",
                    OI.numOppId AS "numOppId",
					OI.vcNotes as "txtNotes",
					OI.bitItemPriceApprovalRequired AS "bitItemPriceApprovalRequired",
                    COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",
                    coalesce(OI.vcPathForTImage,'') AS "vcPathForTImage",
                    coalesce(OI.numWarehouseItmsID,0) AS "numWarehouseItmsID",
                    OI.vcItemName AS "vcItemName",
                    OI.vcModelID AS "vcModelID",
					coalesce(OI.numCost,0) AS "numCost",
                    coalesce(OI.vcAttributes,'') AS "vcAttributes",
                    OI.vcType AS "vcType",
                    coalesce(OI.bitDropShip,false) AS "bitDropShip",
                    OI.numItemCode AS "numItemCode",
					OI.numUnitHour AS "numOrigUnitHour",
					0 AS "numOnHand",
                    OI.numUnitHour AS "numOriginalUnitHour",
                    coalesce(OI.numUOMId,0) AS "numUOM",
                    coalesce(U.vcUnitName,'') AS "vcUOMName",
                    fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,NULL::NUMERIC) AS "UOMConversionFactor",
                    coalesce(I.numVendorID,0) AS "numVendorID",
                    coalesce(V.monCost,0) AS "monVendorCost",
                    coalesce(V.vcPartNo,'') AS "vcPartNo",
                    OI.monPrice AS "monPrice",
                    coalesce(OI.numSOVendorId,0) AS "numSOVendorId",
                    coalesce(I.charItemType,'N') AS "charItemType",
					CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OI.numUnitHour) AS NUMERIC(18,2)) AS "numUnitHour",
					coalesce(OI.numClassID,0) as numClassID,coalesce(OI.numProjectID,0) as numProjectID,coalesce(I.fltWeight,0) AS fltWeight,coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.fltLength,0) AS "fltLength",
					coalesce(I.fltWidth,0) AS "fltWidth",
					coalesce(I.fltHeight,0) AS "fltHeight",
					coalesce((SELECT vcWHSKU FROM WareHouseItems WHERE numWareHouseItemID = OI.numWarehouseItmsID),coalesce(I.vcSKU,'')) AS "vcSKU",
					coalesce(vcAttrValues,'') AS "vcAttrValues",
					(CASE WHEN coalesce(OI.ItemReleaseDate,null) = null THEN
            coalesce(OM.dtReleaseDate,'1900-01-01 00:00:00.000')
         ELSE  OI.ItemReleaseDate
         END) AS ItemRequiredDate, 0 AS numPOID,0 AS numPOItemID,'' AS "vcPOName", 0 AS "numItemRepQuantity"
         FROM    OpportunityItems OI
         INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
         INNER JOIN Item I ON I.numItemCode = OI.numItemCode
         LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
         LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
         AND I.numItemCode = V.numItemCode
         WHERE   OI.numOppId = v_numOppID
         AND OM.numDomainId = v_numDomainId;
      end if;
   end if;
   IF v_tintMode = 4 then -- Add Edit Order items
        
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode AS "numoppitemtCode",
                    OI.numOppId AS "numOppId",
					OI.vcNotes AS "txtNotes",
                    OI.numItemCode AS "numItemCode",
					OI.bitItemPriceApprovalRequired AS "bitItemPriceApprovalRequired",
					OI.numUnitHour AS "numOrigUnitHour",
                    OI.numUnitHour AS "numUnitHour",
                    OI.monPrice AS "monPrice",
                    OI.monTotAmount AS "monTotAmount",
                    OI.numSourceID AS numSourceID,
                    COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",
					OI.vcNotes AS "vcNotes",
					coalesce(OI.numCost,0) AS "numCost",
                    OI.numWarehouseItmsID AS "numWarehouseItmsID",
                    OI.vcType AS "ItemType",
                    OI.vcAttributes AS "Attributes",
                    OI.bitDropShip AS "DropShip",
					OI.bitDropShip AS "bitDropShip",
                    OI.numUnitHourReceived AS "numUnitHourReceived",
                    OI.bitDiscountType AS "bitDiscountType",
                    OI.fltDiscount AS "fltDiscount",
                    OI.monTotAmtBefDiscount AS "monTotAmtBefDiscount",
					coalesce(OI.monTotAmtBefDiscount,0) -coalesce(OI.monTotAmount,0) AS "TotalDiscountAmount",
                    OI.numQtyShipped AS "numQtyShipped",
                    OI.vcItemName AS "vcItemName",
                    OI.vcModelID AS "vcModelID",
                    OI.vcPathForTImage AS "vcPathForTImage",
                    OI.vcManufacturer AS "vcManufacturer",
                    OI.monVendorCost AS "monVendorCost",
                    coalesce(OI.numUOMId,0) AS "numUOM",
                    coalesce(U.vcUnitName,'') AS "vcUOMName",
                    coalesce(fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,NULL::NUMERIC),1) AS "UOMConversionFactor",
                    OI.bitWorkOrder AS "bitWorkOrder",
                    coalesce(OI.numVendorWareHouse,0) as "numVendorWareHouse",
                    OI.numShipmentMethod AS "numShipmentMethod",
                    OI.numSOVendorId AS "numSOVendorId",
                    OI.numProjectStageID AS "numProjectStageID",
                    OI.numToWarehouseItemID AS "numToWarehouseItemID",
                   UPPER(I.charItemType) AS "charItemType",
                    WItems.numWareHouseID AS "numWareHouseID",
                    W.vcWareHouse AS "Warehouse",
                    0 as "Op_Flag",
					CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OI.numUnitHour) AS NUMERIC(18,2)) AS "numUnitHour",
					coalesce(OI.numClassID,0) as numClassID,coalesce(OI.numProjectID,0) as "numProjectID",
					CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
         AND (OB.bitAuthoritativeBizDocs = 1 OR OB.numBizDocId = 304) AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numoppid = OM.numOppId) > 0 THEN 1 ELSE 0 END  AS "bitIsAuthBizDoc",
					coalesce(OI.numUnitHourReceived,0) AS "numUnitHourReceived",
					coalesce(numQtyShipped,0) AS "numQtyShipped",
					coalesce(fn_GetUOMName(I.numBaseUnit),'') AS "vcBaseUOMName",
					coalesce(I.fltWeight,0) AS "fltWeight",
					coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.fltLength,0) AS "fltLength",
					coalesce(I.fltWidth,0) AS "fltWidth",
					coalesce(I.fltHeight,0) AS "fltHeight",
					coalesce(WItems.vcWHSKU,coalesce(I.vcSKU,'')) AS "vcSKU",
					(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) AS "monAverageCost",
					coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.txtItemDesc,'') AS "txtItemDesc",
					coalesce(I.numBarCodeId,'') AS "numBarCodeId",
					coalesce(I.bitSerialized,false) AS "bitSerialized",
					coalesce(I.bitKitParent,false) AS "bitKitParent",
					coalesce(I.bitAssembly,false) AS "bitAssembly",
					coalesce(I.IsArchieve,false) AS "IsArchieve",
					coalesce(I.bitLotNo,false) AS "bitLotNo",
					coalesce(WItems.monWListPrice,0) AS "monWListPrice",
					coalesce(I.bitTaxable,false) AS "bitTaxable",
					coalesce(I.numItemClassification,0) AS "numItemClassification",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numOnHand),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnHand",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numonOrder),0)  FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnOrder",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numAllocation),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numAllocation",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numBackOrder),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numBackOrder",
					case when coalesce(I.bitAssembly,false) = true then fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS "numMaxWorkOrderQty",
					coalesce((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numOppItemID = OI.numoppitemtCode),0) AS "numSerialNoAssigned",
					(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID WHERE OB.numoppid = OI.numOppId AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numBizDocId = 296) > 0 THEN 1 ELSE 0 END) AS "bitAddedFulFillmentBizDoc",
					coalesce(OI.vcAttrValues,'') AS "AttributeIDs",
					coalesce(I.numContainer,0) AS "numContainer"
					,coalesce(I.numNoItemIntoContainer,0) AS "numContainerQty"
					,coalesce(numPromotionID,0) AS "numPromotionID",
					coalesce(bitPromotionTriggered,false) AS "bitPromotionTriggered",
					coalesce(vcPromotionDetail,'') AS "vcPromotionDetail",
					coalesce(vcPromotionDetail,'') AS "vcPromotionDescription",
					CASE WHEN coalesce(OI.numSortOrder,0) = 0 THEN row_number() OVER(ORDER BY OI.numOppId) ELSE coalesce(OI.numSortOrder,0) END AS "numSortOrder"
					,coalesce(vcNotes,'') AS "vcVendorNotes"
					,coalesce(CPN.CustomerPartNo,'') AS "CustomerPartNo"
					,(CASE WHEN is_date(OI.ItemRequiredDate::VARCHAR) THEN OI.ItemRequiredDate ELSE null END) AS "ItemRequiredDate"
					,(CASE WHEN is_date(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate)::VARCHAR) THEN coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE null END) AS "ItemReleaseDate"
					,(CASE WHEN coalesce(bitMarkupDiscount,false) = false THEN '0' ELSE '1' END) AS "bitMarkupDiscount"
					,coalesce(vcChildKitSelectedItems,'') AS "KitChildItems"
					,(CASE
      WHEN coalesce(I.bitKitParent,false) = true
      THEN(CASE
         WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
         THEN 1
         ELSE 0
         END)
      ELSE 0
      END) AS "bitHasKitAsChild"
					,coalesce(I.bitFreeShipping,false) AS "IsFreeShipping"
					,(CASE
      WHEN coalesce(I.bitKitParent,false) = true
      AND(CASE
      WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
      THEN 1
      ELSE 0
      END) = 0
      THEN
         GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
      ELSE
         coalesce(I.fltWeight,0)
      END) AS "fltItemWeight"
					,coalesce(I.fltWidth,0) AS "fltItemWidth"
					,coalesce(I.fltHeight,0) AS "fltItemHeight"
					,coalesce(I.fltLength,0) AS "fltItemLength"
					,coalesce(OI.numWOQty,0) AS "numWOQty"
					,coalesce(I.bitSOWorkOrder,false) AS "bitAlwaysCreateWO"
					,coalesce(OI.bitMappingRequired,false) AS "bitMappingRequired"
					,coalesce(OI.vcASIN,'') AS "vcASIN"
					,coalesce(OI.vcSKU,'') AS "vcSKUOpp"
					,coalesce(OI.bitDisablePromotion,false) AS "bitDisablePromotion"
      FROM    OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
      INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
      LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
      LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
      LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId = DM.numCompanyID
      WHERE   OI.numOppId = v_numOppID
      AND OM.numDomainId = v_numDomainId
      ORDER BY OI.numSortOrder;

      open SWV_RefCur2 for
      SELECT
      vcSerialNo,
				vcComments AS "Comments",
				numOppItemID AS "numoppitemtCode",
				W.numWareHouseItmsDTLID AS "numWareHouseItmsDTLID",
				numWarehouseItmsID AS "numWItmsID",
				fn_GetAttributes(W.numWareHouseItmsDTLID,1::BOOLEAN) AS "Attributes"
      FROM
      WareHouseItmsDTL W
      JOIN
      OppWarehouseSerializedItem O
      ON
      O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
      WHERE
      numOppID = v_numOppID;
   end if;
        
        
   IF v_tintMode = 5 then -- Get WebApi Order Items to enter WebApi Order line Item Details 
		
      open SWV_RefCur for
      SELECT OI.numoppitemtCode AS "numoppitemtCode",OI.numOppId AS "numOppId",OI.numItemCode AS "numItemCode",OI.vcNotes as "txtNotes",coalesce(OI.numCost,0) AS "numCost",
			coalesce((SELECT vcWHSKU FROM WareHouseItems WHERE WareHouseItems.numWareHouseItemID = OI.numWarehouseItmsID),coalesce(IT.vcSKU,'')) AS "vcSKU",
			COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",OMAPI.vcAPIOppId AS "vcAPIOppId"
      FROM OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      INNER JOIN OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
      INNER JOIN Item IT ON OI.numItemCode = IT.numItemCode
      WHERE OI.numOppId = v_numOppID and OM.numDomainId = v_numDomainId;
   end if;

   IF v_tintMode = 6 then -- Add Edit Order items
        
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode AS "numoppitemtCode",
                    OI.numOppId AS "numOppId",
					OI.vcNotes as "txtNotes",
                    OI.numItemCode AS "numItemCode",
					OI.bitItemPriceApprovalRequired AS "bitItemPriceApprovalRequired",
					OI.numUnitHour AS "numOrigUnitHour",
                    (OI.numUnitHour -coalesce((SELECT SUM(numUnitHour) FROM ReturnItems where
         numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
            WHERE numOppId = v_numOppID) AND numOppItemID = OI.numoppitemtCode),0))
      AS "numUnitHour",
                    OI.monPrice AS "monPrice",
                    OI.monTotAmount AS "monTotAmount",
                    OI.numSourceID AS "numSourceID",
                    COALESCE(OI.vcItemDesc,'') AS "vcItemDesc",
					OI.numCost AS "numCost",
                    OI.numWarehouseItmsID AS "numWarehouseItmsID",
                    OI.vcType AS "ItemType",
                    OI.vcAttributes AS "Attributes",
                    OI.bitDropShip AS "DropShip",
                    OI.numUnitHourReceived AS "numUnitHourReceived",
                    OI.bitDiscountType AS "bitDiscountType",
                    OI.fltDiscount AS "fltDiscount",
                    OI.monTotAmtBefDiscount AS "monTotAmtBefDiscount",
                    OI.numQtyShipped AS "numQtyShipped",
                    OI.vcItemName AS "vcItemName",
                    OI.vcModelID AS "vcModelID",
                    OI.vcPathForTImage AS "vcPathForTImage",
                    OI.vcManufacturer AS "vcManufacturer",
                    OI.monVendorCost AS "monVendorCost",
                    coalesce(OI.numUOMId,0) AS "numUOM",
                    coalesce(U.vcUnitName,'') AS "vcUOMName",
                    coalesce(fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,NULL::NUMERIC),1) AS "UOMConversionFactor",
                    OI.bitWorkOrder AS "bitWorkOrder",
                    coalesce(OI.numVendorWareHouse,0) as "numVendorWareHouse",
                    OI.numShipmentMethod AS "numShipmentMethod",
                    OI.numSOVendorId AS "numSOVendorId",
                    OI.numProjectID AS "numProjectID",
                    OI.numClassID AS "numClassID",
                    OI.numProjectStageID AS "numProjectStageID",
                    OI.numToWarehouseItemID AS "numToWarehouseItemID",
                   UPPER(I.charItemType) AS "charItemType",
                    WItems.numWareHouseID AS "numWareHouseID",
                    W.vcWareHouse AS "Warehouse",
                    0 as "Op_Flag",
					CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OI.numUnitHour) AS NUMERIC(18,2)) AS "numUnitHour",
					coalesce(OI.numClassID,0) as numClassID,coalesce(OI.numProjectID,0) as "numProjectID",
					CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
         AND (OB.bitAuthoritativeBizDocs = 1 OR OB.numBizDocId = 304) AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numoppid = OM.numOppId) > 0 THEN 1 ELSE 0 END  AS "bitIsAuthBizDoc",
					coalesce(OI.numUnitHourReceived,0) AS "numUnitHourReceived",coalesce(numQtyShipped,0) AS "numQtyShipped",coalesce(fn_GetUOMName(I.numBaseUnit),'') AS "vcBaseUOMName",coalesce(I.fltWeight,0) AS "fltWeight",coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.fltLength,0) AS "fltLength",
					coalesce(I.fltWidth,0) AS "fltWidth",
					coalesce(I.fltHeight,0) AS "fltHeight",
					coalesce(WItems.vcWHSKU,coalesce(I.vcSKU,'')) AS "vcSKU",
					(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) AS "monAverageCost",
					coalesce(I.bitFreeShipping,false) AS "bitFreeShipping",
					coalesce(I.txtItemDesc,'') AS "txtItemDesc",
					coalesce(I.vcModelID,'') AS "vcModelID",
					coalesce(I.vcManufacturer,'') AS "vcManufacturer",
					coalesce(I.numBarCodeId,'') AS "numBarCodeId",
					coalesce(I.bitSerialized,false) AS "bitSerialized",
					coalesce(I.bitKitParent,false) AS "bitKitParent",
					coalesce(I.bitAssembly,false) AS "bitAssembly",
					coalesce(I.IsArchieve,false) AS "IsArchieve",
					coalesce(I.bitLotNo,false) AS "bitLotNo",
					coalesce(WItems.monWListPrice,0) AS "monWListPrice",
					coalesce(I.bitTaxable,false) AS "bitTaxable",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numOnHand),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnHand",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numonOrder),0)FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numOnOrder",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numAllocation),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numAllocation",
					CASE WHEN I.charItemType = 'P' THEN(SELECT coalesce(SUM(numBackOrder),0) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE 0 END AS "numBackOrder",
					case when coalesce(I.bitAssembly,false) = true then fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS "numMaxWorkOrderQty",
					coalesce((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numOppItemID = OI.numoppitemtCode),0) AS "numSerialNoAssigned",
					(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID WHERE OB.numoppid = OI.numOppId AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numBizDocId = 296) > 0 THEN 1 ELSE 0 END) AS "bitAddedFulFillmentBizDoc",
					coalesce(OI.vcAttrValues,'') AS "AttributeIDs"
					,(CASE WHEN OM.tintopptype = 2 THEN coalesce(vcNotes,'') ELSE '' END) AS "vcVendorNotes"
					,(CASE WHEN coalesce(bitMarkupDiscount,false) = false THEN '0' ELSE '1' END) AS "bitMarkupDiscount"
--					0 AS [Tax0],0 AS bitTaxable0
      FROM    OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
      LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
      LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
      WHERE   OI.numOppId = v_numOppID
      AND OM.numDomainId = v_numDomainId
      AND(OI.numUnitHour -coalesce((SELECT SUM(numUnitHour) FROM ReturnItems where
         numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
            WHERE numOppId = v_numOppID) AND numOppItemID = OI.numoppitemtCode),0)) > 0;

      open SWV_RefCur2 for
      SELECT  vcSerialNo,
				vcComments AS "Comments",
				numOppItemID AS "numoppitemtCode",
				W.numWareHouseItmsDTLID AS "numWareHouseItmsDTLID",
				numWarehouseItmsID AS "numWItmsID",
				fn_GetAttributes(W.numWareHouseItmsDTLID,1::BOOLEAN) AS "Attributes"
      FROM    WareHouseItmsDTL W
      JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
      WHERE   numOppID = v_numOppID AND numOppItemID IN(SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = v_numOppID AND(OI.numUnitHour -coalesce((SELECT SUM(numUnitHour) FROM ReturnItems where
            numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
               WHERE numOppId = v_numOppID) AND numOppItemID = OI.numoppitemtCode),0)) > 0);
   end if;
   RETURN;
END; $$;


