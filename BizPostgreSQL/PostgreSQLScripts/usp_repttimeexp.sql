CREATE OR REPLACE FUNCTION USP_ReptTimeExp(v_numDomainId NUMERIC(9,0) DEFAULT 0,          
v_FromDate TIMESTAMP DEFAULT NULL,          
v_ToDate TIMESTAMP DEFAULT NULL,          
v_byteMode SMALLINT DEFAULT 0,          
v_tintType SMALLINT DEFAULT NULL,          
v_numUserCntID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numUserID,A.vcFirstName || ' ' || A.vcLastname as Name,
 fn_GetListItemName(A.vcPosition) as "Position",
 MG.vcFirstName || ' ' || MG.vcLastname as MGRName,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillTime,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)*monAmount/60)
         from timeandexpense where numCategory = 1 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillTimeAmt,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as NonBillTime,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate)*monHourlyRate as NonBillTimeAmt,
 (select sum(monAmount)
         from timeandexpense where numCategory = 2 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillExp,
 (select sum(monAmount)
         from timeandexpense where numCategory = 2 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as NonBillExp,
 (select sum(DATE_PART('day',(case when dtToDate < v_ToDate then dtToDate else v_ToDate end)+INTERVAL '1 day' -(case when dtFromDate > v_FromDate then dtFromDate else v_FromDate end)) -(case when bitFromFullDay = true then 0 else .5 end) -(case when bittofullday = true then 0 else .5 end))
         from timeandexpense where numCategory = 3 and bitApproved = false and A.numContactId = U.numUserDetailId and (dtFromDate between v_FromDate and v_ToDate or dtToDate between v_FromDate and v_ToDate)) as NonAppLeave,
 CAST((select sum(DATE_PART('day',(case when dtToDate < v_ToDate then dtToDate else v_ToDate end)+INTERVAL '1 day' -(case when dtFromDate > v_FromDate then dtFromDate else v_FromDate end)) -(case when bitFromFullDay = true then 0 else .5 end) -(case when bittofullday = true then 0 else .5 end))
         from timeandexpense where numCategory = 3
         and bitApproved = true and A.numContactId = U.numUserDetailId and
(dtFromDate between v_FromDate and v_ToDate or
         dtToDate between v_FromDate and v_ToDate)) AS VARCHAR(30)) ||
      case when NULLIF(MG.vcFirstName,'') is null then ',-' else ', ' || MG.vcFirstName || ' ' ||
         MG.vcLastname end  as AppLeave
      from UserMaster U
      join AdditionalContactsInformation A
      on A.numContactId = U.numUserDetailId
      left join AdditionalContactsInformation MG
      on MG.numContactId = A.numManagerID where A.numContactId = v_numUserCntID and U.numDomainID = v_numDomainId;
   ELSEIF v_byteMode = 2
   then

      open SWV_RefCur for
      select numUserID,A.vcFirstName || ' ' || A.vcLastname as Name,
 fn_GetListItemName(A.vcPosition) as "Position",
 MG.vcFirstName || ' ' || MG.vcLastname as MGRName,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillTime,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)*monAmount/60)
         from timeandexpense where numCategory = 1 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillTimeAmt,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as NonBillTime,
 (select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
         from timeandexpense where numCategory = 1 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate)*monHourlyRate as NonBillTimeAmt,
 (select sum(monAmount)
         from timeandexpense where numCategory = 2 and numtype = 1 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as BillExp,
 (select sum(monAmount)
         from timeandexpense where numCategory = 2 and numtype = 2 and A.numContactId = U.numUserDetailId and
         dtFromDate between v_FromDate and v_ToDate and  dtToDate between v_FromDate and v_ToDate) as NonBillExp,
 (select sum(DATE_PART('day',(case when dtToDate < v_ToDate then dtToDate else v_ToDate end)+INTERVAL '1 day' -(case when dtFromDate > v_FromDate then dtFromDate else v_FromDate end)) -(case when bitFromFullDay = true then 0 else .5 end) -(case when bittofullday = true then 0 else .5 end))
         from timeandexpense where numCategory = 3 and bitApproved = false and A.numContactId = U.numUserDetailId and (dtFromDate between v_FromDate and v_ToDate or dtToDate between v_FromDate and v_ToDate)) as NonAppLeave,
 CAST((select sum(DATE_PART('day',(case when dtToDate < v_ToDate then dtToDate else v_ToDate end)+INTERVAL '1 day' -(case when dtFromDate > v_FromDate then dtFromDate else v_FromDate end)) -(case when bitFromFullDay = true then 0 else .5 end) -(case when bittofullday = true then 0 else .5 end))
         from timeandexpense where numCategory = 3 and bitApproved = true and A.numContactId = U.numUserDetailId
         and (dtFromDate between v_FromDate and v_ToDate or dtToDate between v_FromDate and v_ToDate)) AS VARCHAR(30)) || case when NULLIF(MG.vcFirstName,'')
      is null then ',-' else ', ' || MG.vcFirstName || ' ' || MG.vcLastname end  as AppLeave
      from UserMaster U
      join AdditionalContactsInformation A
      on A.numContactId = U.numUserDetailId
      left join AdditionalContactsInformation MG
      on MG.numContactId = A.numManagerID where MG.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainId and F.tintType = v_tintType);
   end if;
   RETURN;
END; $$;


