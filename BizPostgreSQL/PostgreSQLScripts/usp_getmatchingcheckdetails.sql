CREATE OR REPLACE FUNCTION USP_GetMatchingCheckDetails(v_monAmount DECIMAL(20,5),
v_numDomainId NUMERIC(18,0),
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		CH.numCheckHeaderID
		,CD.numCheckDetailID
		,CH.numChartAcntId
		,CH.dtCheckDate
		,CH.vcMemo
		,ROUND(ABS(CD.monAmount::NUMERIC),2) AS monAmount
		,CD.numClassID
		,CD.numProjectID
		,CD.numCustomerId
		,CD.numChartAcntId
		,C.vcCompanyName 
		FROM CheckHeader CH
      INNER JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
      INNER JOIN DivisionMaster D on CD.numCustomerId = D.numDivisionID
      INNER JOIN CompanyInfo C on D.numCompanyID = C.numCompanyId
      WHERE CH.numDomainID = v_numDomainId AND CD.monAmount = ROUND(CAST(ABS(v_monAmount) AS NUMERIC),2) OR  CD.monAmount < ROUND(CAST(ABS(v_monAmount) AS NUMERIC),2)
      AND CH.dtCheckDate BETWEEN v_dtFromDate AND v_dtToDate;
   RETURN;
END; $$;













