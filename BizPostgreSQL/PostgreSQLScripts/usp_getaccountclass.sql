-- Stored procedure definition script USP_GetAccountClass for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAccountClass(v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
    v_numDivisionID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountClass  NUMERIC(18,0) DEFAULT 0;
   v_tintDefaultClassType  INTEGER DEFAULT 0;
BEGIN
   select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainID; 

   IF v_tintDefaultClassType = 1 then --USER
	
      select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
      UserMaster UM
      JOIN
      Domain D
      ON
      UM.numDomainID = D.numDomainId WHERE
      D.numDomainId = v_numDomainID
      AND UM.numUserDetailId = v_numUserCntID;
   ELSEIF v_tintDefaultClassType = 2
   then --COMPANY
	
      select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
      DivisionMaster DM WHERE
      DM.numDomainID = v_numDomainID
      AND DM.numDivisionID = v_numDivisionID;
   ELSE
      v_numAccountClass := 0;
   end if;

   open SWV_RefCur for SELECT v_numAccountClass;
END; $$;












