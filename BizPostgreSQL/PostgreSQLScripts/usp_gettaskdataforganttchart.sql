-- Stored procedure definition script USP_GetTaskDataForGanttChart for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTaskDataForGanttChart(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numOppId NUMERIC(18,0) DEFAULT 0,
v_numProjectID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   S.vcMileStoneName AS MileStoneName,
	S.vcStageName,
	T.vcTaskName,
	S.numStageDetailsId,
	T.numTaskId,
	S.tintPercentage AS tinProgressPercentage,
	T.bitSavedTask,
	T.bitTaskClosed,
	S.bitIsDueDaysUsed,
	T.numHours,
	CAST(T.numHours/8 AS VARCHAR(30)) AS TaskDays,
	CASE WHEN T.numAssignTo > 0 THEN(SELECT  vcFirstName || ' ' || vcLastname FROM AdditionalContactsInformation WHERE numContactId = T.numAssignTo LIMIT 1) ELSE '-' END AS vcContactName,
	S.dtStartDate As StageStartDate,
	CAST(CASE WHEN S.bitIsDueDaysUsed = true THEN CAST(S.dtStartDate AS DATE)+CAST(S.intDueDays || 'day' as interval)
   ELSE CAST(S.dtStartDate AS DATE)+CAST((SELECT  CAST(Items AS DECIMAL(18,2)) FROM Split(fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),'_') LIMIT 1) || 'day' as interval) END AS TIMESTAMP) AS StageEndDate
   FROM
   StagePercentageDetails AS S,
	StagePercentageDetailsTask AS T
   LEFT JOIN ProjectProcessStageDetails AS PPD
   ON T.numStageDetailsId = PPD.numStageDetailsId AND (PPD.numOppId = v_numOppId OR PPD.numProjectId = v_numProjectID) AND (T.numOppId = v_numOppId OR T.numProjectId = v_numProjectID)
   WHERE
   T.numStageDetailsId = S.numStageDetailsId AND
   1 =(CASE WHEN v_numOppId > 0 AND S.numOppid = v_numOppId AND T.numOppId = v_numOppId THEN 1 WHEN v_numProjectID > 0 AND S.numProjectid = v_numProjectID AND T.numProjectId = v_numProjectID THEN 1 ELSE 0 END) AND
   T.bitSavedTask = true AND
   S.numdomainid = v_numDomainID AND
   T.numDomainID = v_numDomainID
   ORDER BY
   S.vcMileStoneName,S.numStageOrder,T.numOrder;
END; $$;
