-- Function definition script GetCustFldValueRtNull for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldValueRtNull(v_numFldId NUMERIC(9,0),v_pageId SMALLINT,v_numRecordId NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(100);
BEGIN
   if v_pageId = 1 then

      select   Fld_Value INTO v_vcValue from CFW_FLD_Values where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;    
    
   if v_pageId = 4 then

      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Cont where fld_id = v_numFldId and RecId = v_numRecordId;
   end if;    
    
   if v_pageId = 3 then

      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Case where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;    
    
    
   if v_pageId = 5 then

      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Item where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;    
    
   if v_pageId = 2 or v_pageId = 6 then

      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Opp where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;    
    
   if v_pageId = 7 or v_pageId = 8 then

      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Product where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;   
  
   if v_pageId = 9 then

      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Serialized_Items where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;   

   if v_pageId = 11 then

      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Pro where Fld_ID = v_numFldId and RecId = v_numRecordId;
   end if;   
    
   if v_vcValue = '0' then 
      v_vcValue := null;
   end if;   
    
   RETURN v_vcValue;
END; $$;

