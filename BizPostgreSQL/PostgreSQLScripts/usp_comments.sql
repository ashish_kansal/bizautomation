-- Stored procedure definition script USP_Comments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Comments(v_byteMode SMALLINT DEFAULT 0,    
v_numCaseId NUMERIC(9,0) DEFAULT 0,             
v_vcHeading VARCHAR(1000) DEFAULT '',    
v_txtComments TEXT DEFAULT '',    
v_numContactID NUMERIC(9,0) DEFAULT 0,    
v_numCommentID NUMERIC(9,0) DEFAULT 0,
v_numStageID NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      IF v_numCaseId > 0 then
	
         open SWV_RefCur for
         select numCommentID,vcHeading || ' (' || CAST(FormatedDateFromDate((bintCreatedDate),v_numDomainId) AS VARCHAR(20)) || ') - ' || fn_GetContactName(numCreatedby) as vcHeading,txtComments from Comments where numCaseID = v_numCaseId  order by    numCommentID desc;
      ELSEIF v_numStageID > 0
      then
	
         open SWV_RefCur for
         select numCommentID,vcHeading || ' (' || CAST(FormatedDateFromDate((bintCreatedDate),v_numDomainId) AS VARCHAR(20)) || ') - ' || fn_GetContactName(numCreatedby) as vcHeading,txtComments from Comments where numStageID = v_numStageID order by    numCommentID desc;
      end if;
   end if;    
    
   if v_byteMode = 1 then

      IF v_numStageID = 0 then 
         v_numStageID := NULL;
      end if;
      IF v_numCaseId = 0 then 
         v_numCaseId := NULL;
      end if;
      IF v_numCommentID IS NULL then 
         v_numCommentID := 0;
      end if;
      if v_numCommentID = 0 then

         insert into Comments(numCaseID,vcHeading,txtComments,numCreatedby,bintCreatedDate,numStageID)
values(v_numCaseId,v_vcHeading,v_txtComments,v_numContactID,TIMEZONE('UTC',now()),v_numStageID) RETURNING numCommentID INTO v_numCommentID;

         open SWV_RefCur for
         select v_numCommentID;
      else
         update Comments set vcHeading = v_vcHeading,txtComments = v_txtComments where numCommentID = v_numCommentID;
         open SWV_RefCur for
         select v_numCommentID;
      end if;
   end if;       
    
   if v_byteMode = 2 then

      delete from Comments where numCommentID = v_numCommentID;
      open SWV_RefCur for
      select 1;
   end if;
   RETURN;
END; $$;


