-- Function definition script function_string_to_table for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION function_string_to_table(v_string TEXT,
    v_delimiter CHAR(1))
RETURNS TABLE
(
   data VARCHAR(256)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_start  INTEGER;
   v_end  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_FUNCTION_STRING_TO_TABLE_OUTPUT CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FUNCTION_STRING_TO_TABLE_OUTPUT
   (
      data VARCHAR(256)
   );
   v_start := 1;
   v_end := POSITION(v_delimiter IN v_string);

   WHILE v_start < LENGTH(v_string) LOOP
      IF v_end = 0 then
         v_end := LENGTH(v_string)+1;
      end if;
      INSERT INTO tt_FUNCTION_STRING_TO_TABLE_OUTPUT(data)
        VALUES(SUBSTR(v_string, v_start, v_end -v_start));
        
      v_start := v_end::bigint+1;
      v_end := CASE WHEN POSITION(v_delimiter IN SUBSTR(v_string,v_start)) > 0 THEN POSITION(v_delimiter IN SUBSTR(v_string,v_start))+v_start -1 ELSE 0 END;
   END LOOP;

   RETURN QUERY (SELECT * FROM tt_FUNCTION_STRING_TO_TABLE_OUTPUT);
END; $$;

