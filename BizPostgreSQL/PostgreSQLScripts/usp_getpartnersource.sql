-- Stored procedure definition script USP_GetPartnerSource for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPartnerSource(v_numDomainID  NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   D.numDivisionID
		,D.vcPartnerCode || '-' || C.vcCompanyName AS vcPartner
   FROM
   DivisionMaster AS D
   LEFT JOIN
   CompanyInfo AS C
   ON
   D.numCompanyID = C.numCompanyId
   WHERE
   D.numDomainID = v_numDomainID
   AND D.vcPartnerCode <> ''
   ORDER BY
   vcCompanyName;
END; $$;












