DROP FUNCTION IF EXISTS USP_PayrollLiabilityList;

CREATE OR REPLACE FUNCTION USP_PayrollLiabilityList(v_numDomainId NUMERIC(18,0),
    v_numDepartmentId NUMERIC(18,0),
    v_numComPayPeriodID NUMERIC(18,0),
    v_ClientTimeZoneOffset INTEGER,
	v_numOrgUserCntID NUMERIC(18,0),
	v_tintUserRightType INTEGER DEFAULT 3,
	v_tintUserRightForUpdate INTEGER DEFAULT 3,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtStartDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP;
   v_decTotalHrsWorked  DECIMAL(10,2);
   v_monReimburse  DECIMAL(20,5);
   v_monCommPaidInvoice  DECIMAL(20,5);
   v_monCommPaidInvoiceDepositedToBank  DECIMAL(20,5);
   v_monCommUNPaidInvoice  DECIMAL(20,5);
   v_monCommPaidCreditMemoOrRefund  DECIMAL(20,5);

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numUserCntID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_bitCommissionContact  BOOLEAN;
   v_bitPartner  BOOLEAN;
BEGIN
   select   CAST(dtStart AS TIMESTAMP) + make_interval(secs => 0.000 ), CAST(dtEnd AS TIMESTAMP)+make_interval(secs => -0.003 ) INTO v_dtStartDate,v_dtEndDate FROM
   CommissionPayPeriod WHERE
   numComPayPeriodID = v_numComPayPeriodID;
	
	-- TEMPORARY TABLE TO HOLD DATA
   BEGIN
      CREATE TEMP SEQUENCE tt_tempHrs_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPHRS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPHRS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numUserId NUMERIC,
      vcteritorries VARCHAR(1000),
      vcTaxID VARCHAR(100),
      vcUserName VARCHAR(100),
      vcdepartment VARCHAR(100),
      bitLinkVisible BOOLEAN,
      bitpayroll BOOLEAN,
      numUserCntID NUMERIC,
      vcEmployeeId VARCHAR(50),
      monHourlyRate DECIMAL(20,5),
      decRegularHrs DECIMAL(20,5),
      decPaidLeaveHrs DECIMAL(20,5),
      monRegularHrsPaid DECIMAL(20,5),
      monAdditionalAmountPaid DECIMAL(20,5),
      monExpense DECIMAL(20,5),
      monReimburse DECIMAL(20,5),
      monReimbursePaid DECIMAL(20,5),
      monCommPaidInvoice DECIMAL(20,5),
      monCommPaidInvoiceDeposited DECIMAL(20,5),
      monCommUNPaidInvoice DECIMAL(20,5),
      monCommPaidCreditMemoOrRefund DECIMAL(20,5),
      monOverPayment DECIMAL(20,5),
      monCommissionEarned DECIMAL(20,5),
      monCommissionPaid DECIMAL(20,5),
      monInvoiceSubTotal DECIMAL(20,5),
      monOrderSubTotal  DECIMAL(20,5),
      monTotalAmountDue DECIMAL(20,5),
      monAmountPaid DECIMAL(20,5),
      bitCommissionContact BOOLEAN,
      numDivisionID NUMERIC(18,0),
      bitPartner BOOLEAN,
      tintCRMType SMALLINT
   );

	-- GET EMPLOYEE OF DOMAIN
   INSERT INTO tt_TEMPHRS(numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact)
   SELECT
   UM.numUserId,
		CONCAT(',',COALESCE((SELECT string_agg(COALESCE(numTerritoryID,0)::VARCHAR,',') FROM UserTerritory WHERE numUserCntID = UM.numUserDetailId AND numDomainID = v_numDomainId),''),','),
		coalesce(adc.vcTaxID,''),
		CAST(coalesce(adc.vcFirstName || ' ' || adc.vcLastname,'-') AS VARCHAR(100)) AS vcUserName,
		fn_GetListItemName(adc.vcDepartment),
		CAST((CASE v_tintUserRightForUpdate
   WHEN 0 THEN 0
   WHEN 1 THEN CASE WHEN UM.numUserDetailId = v_numOrgUserCntID THEN 1 ELSE 0 END
   WHEN 2 THEN CASE WHEN UM.numUserDetailId IN(SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> v_numOrgUserCntID AND numTerritoryID IN(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numOrgUserCntID)) THEN 1 ELSE 0 END
   ELSE 1
   END) AS BOOLEAN),
		coalesce(UM.bitpayroll,false) AS bitpayroll,
		adc.numContactId AS numUserCntID,
		coalesce(UM.vcEmployeeId,'') AS vcEmployeeId,
		coalesce(UM.monHourlyRate,0),
		CAST(0 AS BOOLEAN)
   FROM
   UserMaster UM
   JOIN
   AdditionalContactsInformation adc
   ON
   adc.numDomainID = v_numDomainId
   AND adc.numContactId = UM.numUserDetailId
   WHERE
   UM.numDomainID = v_numDomainId AND (adc.vcDepartment = v_numDepartmentId OR v_numDepartmentId = 0)
   AND coalesce(UM.bitpayroll,false) = true
   AND 1 =(CASE v_tintUserRightType
   WHEN 1
   THEN CASE WHEN UM.numUserDetailId = v_numOrgUserCntID THEN 1 ELSE 0 END
   WHEN 2 THEN CASE WHEN UM.numUserDetailId IN(SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> v_numOrgUserCntID AND numTerritoryID IN(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numOrgUserCntID)) THEN 1 ELSE 0 END
   ELSE 1
   END) ORDER BY UM.numUserId;

	-- GET COMMISSION CONTACTS OF DOMAIN
   INSERT INTO tt_TEMPHRS(numUserId,vcteritorries,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact)
   SELECT
   0,'',
		coalesce(A.vcTaxID,''),
		CAST(coalesce(A.vcFirstName || ' ' || A.vcLastname || '(' || C.vcCompanyName || ')',
   '-') AS VARCHAR(100)),
		A.numContactId,
		CAST('' AS VARCHAR(50)),
		CAST('-' AS VARCHAR(100)),
		CAST((CASE WHEN v_tintUserRightForUpdate = 0 OR v_tintUserRightForUpdate = 1 OR v_tintUserRightForUpdate = 2 THEN 0 ELSE 1 END) AS BOOLEAN),
		CAST(0 AS BOOLEAN),
		0,
		CAST(1 AS BOOLEAN)
   FROM
   CommissionContacts CC
   JOIN DivisionMaster D ON CC.numDivisionID = D.numDivisionID
   JOIN AdditionalContactsInformation A ON D.numDivisionID = A.numDivisionId
   JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
   WHERE
   CC.numDomainID = v_numDomainId
   AND 1 =(CASE
   WHEN v_tintUserRightType = 1 THEN(CASE WHEN EXISTS(SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID = EA.numExtranetID WHERE EAD.numDomainID = v_numDomainId AND EAD.numContactID = COALESCE(v_numOrgUserCntID,0)::VARCHAR AND EA.numDivisionID = D.numDivisionID) THEN 1 ELSE 0 END)
   WHEN v_tintUserRightType = 2 THEN 0
   ELSE 1
   END);


	-- GET PARTNERS OF DOMAIN
   INSERT INTO tt_TEMPHRS(numUserId,vcteritorries,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType)
   SELECT
   0,'',
		coalesce(A.vcTaxID,''),
		coalesce(C.vcCompanyName,'-'),
		0,
		CAST('' AS VARCHAR(50)),
		CAST('-' AS VARCHAR(100)),
		CAST((CASE WHEN v_tintUserRightForUpdate = 0 OR v_tintUserRightForUpdate = 1 OR v_tintUserRightForUpdate = 2 THEN 0 ELSE 1 END) AS BOOLEAN),
		CAST(0 AS BOOLEAN),
		0,
		D.numDivisionID,
		CAST(1 AS BOOLEAN),
		CAST(1 AS BOOLEAN),
		tintCRMType
   FROM
   DivisionMaster AS D
   LEFT JOIN
   CompanyInfo AS C
   ON
   D.numCompanyID = C.numCompanyId
   LEFT JOIN
   AdditionalContactsInformation A
   ON
   A.numDivisionId = D.numDivisionID
   AND coalesce(A.bitPrimaryContact,true) = true
   WHERE
   D.numDomainID = v_numDomainId
   AND D.vcPartnerCode <> ''
   AND 1 =(CASE WHEN v_tintUserRightType = 1 OR v_tintUserRightType = 2 THEN 0 ELSE 1 END)
   ORDER BY
   vcCompanyName;


   select   COUNT(*) INTO v_COUNT FROM tt_TEMPHRS;

	-- LOOP OVER EACH EMPLOYEE
   WHILE v_i <= v_COUNT LOOP
      v_decTotalHrsWorked := 0;
      v_monReimburse := 0;
      v_monCommPaidInvoice := 0;
      v_monCommPaidInvoiceDepositedToBank := 0;
      v_monCommUNPaidInvoice := 0;
      v_numUserCntID := 0;
      v_bitCommissionContact := false;
      v_monCommPaidCreditMemoOrRefund := 0;
      select   coalesce(numUserCntID,0), coalesce(bitCommissionContact,false), coalesce(numDivisionID,0), coalesce(bitPartner,false) INTO v_numUserCntID,v_bitCommissionContact,v_numDivisionID,v_bitPartner FROM
      tt_TEMPHRS WHERE
      ID = v_i;
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
      IF v_bitCommissionContact = false then
		
			-- CALCULATE REGULAR HRS
         select   coalesce(SUM(numHours),0) INTO v_decTotalHrsWorked FROM
         TimeAndExpenseCommission WHERE
         numComPayPeriodID = v_numComPayPeriodID
         AND numUserCntID = v_numUserCntID
         AND (numType = 1 OR numType = 2)
         AND numCategory = 1;
         v_decTotalHrsWorked := coalesce(v_decTotalHrsWorked,0)+coalesce((SELECT
         SUM(numHours)
         FROM
         StagePercentageDetailsTaskCommission
         WHERE
         numComPayPeriodID = v_numComPayPeriodID
         AND numUserCntID = v_numUserCntID),0);

			-- CALCULATE REIMBURSABLE EXPENSES
         select   coalesce(SUM(monTotalAmount),0) INTO v_monReimburse FROM
         TimeAndExpenseCommission WHERE
         numComPayPeriodID = v_numComPayPeriodID
         AND numUserCntID = v_numUserCntID
         AND numCategory = 2
         AND bitReimburse = true;
      end if;

		-- CALCULATE COMMISSION PAID INVOICE
      select   coalesce(SUM(BC.numComissionAmount),0) INTO v_monCommPaidInvoice FROM
      BizDocComission BC
      INNER JOIN
      OpportunityBizDocs oppBiz
      ON
      BC.numOppBizDocId = oppBiz.numOppBizDocsId WHERE
      BC.numComPayPeriodID = v_numComPayPeriodID
      AND ((BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = v_numDivisionID AND BC.tintAssignTo = 3))
      AND oppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount; 

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
      select   coalesce(SUM(BC.numComissionAmount),0) INTO v_monCommPaidInvoiceDepositedToBank FROM
      BizDocComission BC
      INNER JOIN
      OpportunityBizDocs oppBiz
      ON
      BC.numOppBizDocId = oppBiz.numOppBizDocsId
      CROSS JOIN LATERAL(SELECT
         MAX(DM.dtDepositDate) AS dtDepositDate
         FROM
         DepositMaster DM
         JOIN DepositeDetails DD	ON DM.numDepositId = DD.numDepositID
         WHERE
         DM.tintDepositePage = 2
         AND DM.numDomainId = v_numDomainId
         AND DD.numOppID = oppBiz.numOppID
         AND DD.numOppBizDocsID = oppBiz.numOppBizDocsID
         AND DM.tintDepositePage = 2
         AND 1 =(CASE WHEN DM.tintDepositeToType = 2 THEN(CASE WHEN coalesce(DM.bitDepositedToAcnt,false) = true THEN 1 ELSE 0 END) ELSE 1 END)) TEMPDeposit WHERE
      BC.numComPayPeriodID = v_numComPayPeriodID
      AND ((BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = v_numDivisionID AND BC.tintAssignTo = 3))
      AND oppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount;
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
      select   coalesce(SUM(BC.numComissionAmount),0) INTO v_monCommUNPaidInvoice FROM
      BizDocComission BC
      INNER JOIN
      OpportunityBizDocs oppBiz
      ON
      BC.numOppBizDocId = oppBiz.numOppBizDocsId WHERE
      BC.numComPayPeriodID = v_numComPayPeriodID
      AND ((BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = v_numDivisionID AND BC.tintAssignTo = 3))
      AND oppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount;

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
      v_monCommPaidCreditMemoOrRefund := coalesce((SELECT
      SUM(monCommissionReversed)
      FROM
      SalesReturnCommission
      WHERE
      numComPayPeriodID = v_numComPayPeriodID
      AND 1 =(CASE
      WHEN coalesce(v_bitPartner,false) = true
      THEN(CASE WHEN numUserCntID = v_numDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
      ELSE(CASE WHEN numUserCntID = v_numUserCntID AND coalesce(tintAssignTo,0) <> 3 THEN 1 ELSE 0 END)
      END)),0);

      UPDATE
      tt_TEMPHRS TH
      SET
      decRegularHrs = coalesce(v_decTotalHrsWorked,0),monReimburse = coalesce(v_monReimburse,0),
      monCommPaidInvoice = coalesce(v_monCommPaidInvoice,0),
      monCommPaidInvoiceDeposited = coalesce(v_monCommPaidInvoiceDepositedToBank,0),monCommUNPaidInvoice = coalesce(v_monCommUNPaidInvoice,0),monCommPaidCreditMemoOrRefund = coalesce(v_monCommPaidCreditMemoOrRefund,0),
      monCommissionEarned =(CASE
      WHEN coalesce(bitPartner,false) = true
      THEN coalesce((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID = v_numComPayPeriodID AND BizDocComission.numUserCntID = v_numDivisionID AND BizDocComission.tintAssignTo = 3),0)
      ELSE coalesce((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID = v_numComPayPeriodID AND BizDocComission.numUserCntID = v_numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
      END),monOverPayment =(CASE
      WHEN coalesce(bitPartner,false) = true
      THEN coalesce((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID = BCInner.numComissionID WHERE BCInner.numDomainId = v_numDomainId AND (coalesce(BCPD.bitDifferencePaid,false) = false OR coalesce(BCPD.numComPayPeriodID,0) = v_numComPayPeriodID) AND BCInner.numUserCntID = v_numDivisionID AND BCInner.tintAssignTo = 3),0)
      ELSE coalesce((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID = BCInner.numComissionID WHERE BCInner.numDomainId = v_numDomainId AND (coalesce(BCPD.bitDifferencePaid,false) = false OR coalesce(BCPD.numComPayPeriodID,0) = v_numComPayPeriodID) AND BCInner.numUserCntID = v_numUserCntID AND BCInner.tintAssignTo <> 3),0)
      END),
      monInvoiceSubTotal = coalesce((SELECT 
				SUM(monInvoiceSubTotal) monSubTotal
			FROM 
			(
				SELECT 
					numOppBizDocId,numOppBizDocItemID,monInvoiceSubTotal 
				FROM
					BizDocComission 
				WHERE 
					numComPayPeriodID=v_numComPayPeriodID 
					AND BizDocComission.numUserCntID= (CASE WHEN COALESCE(TH.bitPartner,false) = true THEN v_numDivisionID ELSE v_numUserCntID END) 
					AND 1 = (CASE 
								WHEN COALESCE(TH.bitPartner,false) = true 
								THEN (CASE WHEN BizDocComission.tintAssignTo=3 THEN 1 ELSE 0 END) 
								ELSE (CASE WHEN BizDocComission.tintAssignTo<>3 THEN 1 ELSE 0 END) 
							END) 
				GROUP BY 
					numOppBizDocId,numOppBizDocItemID,monInvoiceSubTotal) T1),0),monOrderSubTotal = coalesce((SELECT 
				SUM(monOrderSubTotal) monSubTotal
			FROM 
			(
				SELECT 
					numOppBizDocId,numOppBizDocItemID,monOrderSubTotal 
				FROM
					BizDocComission 
				WHERE 
					numComPayPeriodID=v_numComPayPeriodID 
					AND BizDocComission.numUserCntID=(CASE WHEN COALESCE(TH.bitPartner,false) = true THEN v_numDivisionID ELSE v_numUserCntID END) 
					AND 1 = (CASE 
								WHEN COALESCE(TH.bitPartner,false) = true 
								THEN (CASE WHEN BizDocComission.tintAssignTo=3 THEN 1 ELSE 0 END) 
								ELSE (CASE WHEN BizDocComission.tintAssignTo<>3 THEN 1 ELSE 0 END) 
							END) 
				GROUP BY 
					numOppBizDocId,numOppBizDocItemID,monOrderSubTotal) T1),0)
      WHERE((numUserCntID = v_numUserCntID AND coalesce(bitPartner,false) = false)
      OR(numDivisionID = v_numDivisionID AND coalesce(bitPartner,false) = true));
      v_i := v_i::bigint+1;
   END LOOP;

	-- CALCULATE PAID AMOUNT
	UPDATE 
		tt_TEMPHRS AS TMain
	SET 
		monAmountPaid=coalesce(T1.monTotalAmt,0)
		,monRegularHrsPaid=coalesce(T1.monHourlyAmt,0)
		,monAdditionalAmountPaid=coalesce(T1.monAdditionalAmt,0)
		,monReimbursePaid=coalesce(T1.monReimbursableExpenses,0)
		,monCommissionPaid=coalesce(T1.monCommissionAmt,0)
		,monTotalAmountDue= (coalesce(temp.decRegularHrs,0) * coalesce(temp.monHourlyRate,0)) + coalesce(temp.monReimburse,0) + coalesce(temp.monCommissionEarned,0) + coalesce(temp.monOverPayment,0) - coalesce(temp.monCommPaidCreditMemoOrRefund,0)
	FROM 
		tt_TEMPHRS temp
	LEFT JOIN
	(
		SELECT
			numUserCntID
			,numDivisionID
			,monHourlyAmt
			,monAdditionalAmt
			,monReimbursableExpenses
			,monCommissionAmt
			,monTotalAmt
		FROM
			PayrollHeader PH
		INNER JOIN
			PayrollDetail PD 
		ON 
			PH.numPayrollHeaderID = PD.numPayrollHeaderID
		WHERE
			PH.numComPayPeriodID = v_numComPayPeriodID
	) T1
	ON
		1 = (CASE 
				WHEN COALESCE(bitPartner,false) = true  
				THEN (CASE WHEN temp.numDivisionID = T1.numDivisionID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN temp.numUserCntID = T1.numUserCntID THEN 1 ELSE 0 END)
			END)
	WHERE
		 TMain.ID = temp.ID;

	UPDATE 
		tt_TEMPHRS 
	SET 
		monHourlyRate = (CASE WHEN monHourlyRate IS NULL THEN 0 ELSE monHourlyRate END),
		decRegularHrs = (CASE WHEN decRegularHrs IS NULL THEN 0 ELSE decRegularHrs END),
		decPaidLeaveHrs = (CASE WHEN decPaidLeaveHrs IS NULL THEN 0 ELSE decPaidLeaveHrs END),
		monRegularHrsPaid = (CASE WHEN monRegularHrsPaid IS NULL THEN 0 ELSE monRegularHrsPaid END),
		monAdditionalAmountPaid = (CASE WHEN monAdditionalAmountPaid IS NULL THEN 0 ELSE monAdditionalAmountPaid END),
		monExpense = (CASE WHEN monExpense IS NULL THEN 0 ELSE monExpense END),
		monReimburse = (CASE WHEN monReimburse IS NULL THEN 0 ELSE monReimburse END),
		monReimbursePaid = (CASE WHEN monReimbursePaid IS NULL THEN 0 ELSE monReimbursePaid END),
		monCommPaidInvoice = (CASE WHEN monCommPaidInvoice IS NULL THEN 0 ELSE monCommPaidInvoice END),
		monCommPaidInvoiceDeposited = (CASE WHEN monCommPaidInvoiceDeposited IS NULL THEN 0 ELSE monCommPaidInvoiceDeposited END),
		monCommUNPaidInvoice = (CASE WHEN monCommUNPaidInvoice IS NULL THEN 0 ELSE monCommUNPaidInvoice END),
		monCommPaidCreditMemoOrRefund = (CASE WHEN monCommPaidCreditMemoOrRefund IS NULL THEN 0 ELSE monCommPaidCreditMemoOrRefund END),
		monOverPayment = (CASE WHEN monOverPayment IS NULL THEN 0 ELSE monOverPayment END),
		monCommissionEarned = (CASE WHEN monCommissionEarned IS NULL THEN 0 ELSE monCommissionEarned END),
		monCommissionPaid = (CASE WHEN monCommissionPaid IS NULL THEN 0 ELSE monCommissionPaid END),
		monInvoiceSubTotal = (CASE WHEN monInvoiceSubTotal IS NULL THEN 0 ELSE monInvoiceSubTotal END),
		monOrderSubTotal = (CASE WHEN monOrderSubTotal IS NULL THEN 0 ELSE monOrderSubTotal END),
		monTotalAmountDue = (CASE WHEN monTotalAmountDue IS NULL THEN 0 ELSE monTotalAmountDue END),
		monAmountPaid = (CASE WHEN monAmountPaid IS NULL THEN 0 ELSE monAmountPaid END);
 
   open SWV_RefCur for SELECT * FROM tt_TEMPHRS ORDER BY ID;
   RETURN;
END; $$;












