CREATE OR REPLACE FUNCTION SolutionMaster_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('KnowledgeBase', OLD.numSolnID, 'Delete', OLD.numDomainID);
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('KnowledgeBase', NEW.numSolnID, 'Update', NEW.numDomainID);
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('KnowledgeBase', NEW.numSolnID, 'Insert', NEW.numDomainID);
	END IF;

	RETURN NULL;
END; $$;
CREATE TRIGGER SolutionMaster_IUD AFTER INSERT OR UPDATE OR DELETE ON SolutionMaster FOR EACH ROW EXECUTE PROCEDURE SolutionMaster_IUD_TrFunc();


