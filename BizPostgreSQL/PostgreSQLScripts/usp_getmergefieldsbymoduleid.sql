-- Stored procedure definition script USP_GetMergeFieldsByModuleID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMergeFieldsByModuleID(v_numModuleID NUMERIC(18,0),
v_tintMode SMALLINT DEFAULT 0,
v_tintModuleType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      SELECT  EMF.intEmailMergeFields,
		EMF.vcMergeField,
		EMF.vcMergeFieldValue,
		EMM.vcModuleName
      FROM    EmailMergeFields EMF
      INNER JOIN EmailMergeModule EMM ON EMF.numModuleID = EMM.numModuleID
      WHERE   EMF.numModuleID = v_numModuleID AND EMM.tintModuleType = v_tintModuleType AND EMM.tintModuleType = EMF.tintModuleType--- ISNULL(EMF.tintModuleType,0)
      ORDER BY vcMergeField asc;
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      SELECT numModuleID,vcModuleName FROM EmailMergeModule WHERE tintModuleType = v_tintModuleType;
   end if;
   RETURN;
END; $$;


