-- Stored procedure definition script USP_LandedCostVendorsDelete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LandedCostVendorsDelete(v_numDomainId NUMERIC(18,0),
    v_numLCVendorId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE
   FROM   LandedCostVendors
   WHERE  numLCVendorId = v_numLCVendorId and numDomainID = v_numDomainId;
   RETURN;
END; $$;


