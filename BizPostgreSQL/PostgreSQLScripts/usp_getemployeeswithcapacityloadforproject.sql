-- Stored procedure definition script USP_GetEmployeesWithCapacityLoadForProject for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmployeesWithCapacityLoadForProject(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0)
	,v_tintDateRange SMALLINT -- 1:Today, 2:Week, 3:Month
	,v_dtFromDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId AS "numEmployeeID"
		,CONCAT(A.vcFirstName,' ',A.vcLastname) as "vcEmployeeName"
		,GetProjectCapacityLoad(v_numDomainID,A.numContactId,0,v_numProId,0,v_tintDateRange,v_dtFromDate,
   v_ClientTimeZoneOffset) AS "numCapacityLoad"
		,'A+' AS "vcGrade"
   FROM
   UserMaster UM
   INNER JOIN
   AdditionalContactsInformation A
   ON
   UM.numUserDetailId = A.numContactId
   WHERE
   UM.numDomainID = v_numDomainID
   AND UM.numDomainID = A.numDomainID
   AND UM.intAssociate = 1;
   RETURN;
END; $$;


/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/26/2008 18:13:14 ******/













