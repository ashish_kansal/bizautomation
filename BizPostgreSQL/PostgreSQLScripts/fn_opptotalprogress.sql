-- Function definition script fn_OppTotalProgress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_OppTotalProgress(v_numOppId  NUMERIC)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Percentage  DECIMAL(10,0);
BEGIN
   select   sum(tintPercentage) INTO v_Percentage from OpportunityStageDetails where numoppid = v_numOppId and bitstagecompleted = true;

   return COALESCE(v_Percentage,0) || '%';
END; $$;

