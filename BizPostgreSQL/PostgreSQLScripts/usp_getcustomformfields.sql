-- Stored procedure definition script usp_getCustomFormFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustomFormFields(v_numDomainId NUMERIC(9,0),
    v_vcLocID VARCHAR(50)--Comma seperated location id from CFW_Loc_Master
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPAVAILABLEFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAVAILABLEFIELDS
   (
      numFormFieldId  VARCHAR(20),
      vcFormFieldName VARCHAR(50),
      vcFieldType VARCHAR(15),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcDbColumnName VARCHAR(50),
      vcListItemType CHAR(1),
      intColumnNum INTEGER,
      intRowNum INTEGER,
      boolRequired BOOLEAN,
      vcFieldDataType CHAR(1),
      boolAOIField SMALLINT,
      vcLookBackTableName VARCHAR(50)
   );
  
   
   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,
          vcFieldDataType,boolAOIField,vcLookBackTableName)
   SELECT  CAST(CAST(Fld_id AS VARCHAR(15)) || 'C' AS VARCHAR(20)) AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                   CAST(CASE GRP_ID WHEN 4 then 'C'
   WHEN 1 THEN 'D'
   WHEN 9 THEN CAST(Fld_id AS VARCHAR(15))
   ELSE 'C'
   END AS VARCHAR(15)) AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    coalesce(C.numlistid,0) AS numListID,
                    Fld_label AS vcDbColumnName,
                    CASE WHEN C.numlistid > 0 THEN 'L'
   ELSE ''
   END AS vcListItemType,
                    0 AS intColumnNum,
                    0 AS intRowNum,
                    coalesce(V.bitIsRequired,false) as boolRequired,
                    CASE WHEN C.numlistid > 0 THEN 'N'
   ELSE 'V'
   END AS vcFieldDataType,
                    0 AS boolAOIField,
                    CAST('' AS VARCHAR(50)) AS vcLookBackTableName
   FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldId = C.Fld_id
   WHERE   C.numDomainID = v_numDomainId
   AND GRP_ID IN(SELECT id from SplitIDs(v_vcLocID,','));

        
  		
open SWV_RefCur for SELECT * FROM tt_TEMPAVAILABLEFIELDS; 
        
   RETURN;
END; $$;
        












