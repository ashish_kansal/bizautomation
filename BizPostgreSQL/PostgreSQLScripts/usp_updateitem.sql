-- Stored procedure definition script USP_UpdateItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateItem(v_OppItemCode NUMERIC(9,0),
v_ItemCode NUMERIC(9,0),
v_Units NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monPrice  DECIMAL(20,5);
   v_TotalmonPrice  DECIMAL(20,5);     
   v_OppId  NUMERIC(9,0);
   v_TotalAmount  DECIMAL(20,5);
BEGIN
   v_TotalmonPrice := CAST(GetPrice(v_ItemCode,v_Units) AS DECIMAL(20,5)); 
   v_monPrice := CAST(GetPrice(v_ItemCode,1) AS DECIMAL(20,5));

   update OpportunityItems set numItemCode = v_ItemCode,numUnitHour = v_Units,monPrice = coalesce(v_monPrice,0),
   monTotAmount = coalesce(v_TotalmonPrice,0)
   where numoppitemtCode = v_OppItemCode;

   select   numOppId INTO v_OppId from OpportunityItems where numoppitemtCode = v_OppItemCode;
   select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_OppId;
   update OpportunityMaster set  monPAmount = v_TotalAmount
   where numOppId = v_OppId;
   RETURN;
END; $$;


