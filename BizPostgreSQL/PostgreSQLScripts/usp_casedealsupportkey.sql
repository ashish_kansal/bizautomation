-- Stored procedure definition script usp_CaseDealSupportKey for PostgreSQL
CREATE OR REPLACE FUNCTION usp_CaseDealSupportKey(v_numDivID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numOppId from OpportunityMaster  where numDivisionId = v_numDivID;
END; $$;













