-- Stored procedure definition script USP_Category_GetParentCategories for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Category_GetParentCategories(v_numDomainID NUMERIC(18,0)
	,v_numCategoryID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   with recursive CTE(numCategoryID,numDepCategory,vcCategoryName,tintLevel) AS(SELECT
   Category.numCategoryID AS numCategoryID
			,Category.numDepCategory AS numDepCategory
			,Category.vcCategoryName AS vcCategoryName
			,0  AS tintLevel
   FROM
   Category
   WHERE
   Category.numDomainID = v_numDomainID
   AND Category.numCategoryID = v_numCategoryID
   UNION ALL
   SELECT
   Category.numCategoryID AS numCategoryID
			,Category.numDepCategory AS numDepCategory
			,Category.vcCategoryName AS vcCategoryName
			,C.tintLevel+1 AS tintLevel
   FROM
   Category
   INNER JOIN
   CTE C
   ON
   Category.numCategoryID = C.numDepCategory)
   SELECT * FROM CTE ORDER BY tintLevel DESC;
END; $$;













