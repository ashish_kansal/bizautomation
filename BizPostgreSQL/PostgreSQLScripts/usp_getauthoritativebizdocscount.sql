-- Stored procedure definition script USP_GetAuthoritativeBizDocsCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAuthoritativeBizDocsCount(v_numDivisionID NUMERIC(9,0) DEFAULT 0,
v_numContactId NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numDivisionID > 0 then
	
      open SWV_RefCur for
      select  Count(*) from  OpportunityMaster  Opp
      inner join OpportunityBizDocs OppBiz on Opp.numOppId = OppBiz.numoppid
      Where (OppBiz.numBizDocId in(Select numAuthoritativePurchase  from AuthoritativeBizDocs Where numDomainId = v_numDomainId)
      Or OppBiz.numBizDocId in(Select numAuthoritativeSales  from AuthoritativeBizDocs Where numDomainId = v_numDomainId))
      And Opp.numDivisionId = v_numDivisionID;
   end if;
   If v_numContactId > 0 then
	
      open SWV_RefCur for
      select  Count(*) from  OpportunityMaster  Opp
      inner join OpportunityBizDocs OppBiz on Opp.numOppId = OppBiz.numoppid
      Where (OppBiz.numBizDocId in(Select numAuthoritativePurchase  from AuthoritativeBizDocs Where numDomainId = v_numDomainId)
      Or OppBiz.numBizDocId in(Select numAuthoritativeSales  from AuthoritativeBizDocs Where numDomainId = v_numDomainId))
      And Opp.numContactId = v_numContactId;
   end if;
   RETURN;
END; $$;


