-- Stored procedure definition script USP_GetWareHouseItemID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWareHouseItemID(v_numItemKitID NUMERIC(9,0),  
v_vcText VARCHAR(100) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWareHouseItemID  NUMERIC(9,0);
BEGIN
   select   WI.numWareHouseItemID INTO v_numWareHouseItemID from Warehouses W JOIN  WareHouseItems WI ON W.numWareHouseID = WI.numWareHouseID where W.numDomainID = v_numDomainID AND WI.numDomainID = v_numDomainID AND WI.numItemID = v_numItemKitID and W.vcWareHouse = v_vcText    LIMIT 1;

   IF v_numWareHouseItemID IS NULL then

      select   WI.numWareHouseItemID INTO v_numWareHouseItemID from WareHouseItems WI where WI.numDomainID = v_numDomainID AND WI.numItemID = v_numItemKitID    LIMIT 1;
   end if;

   open SWV_RefCur for SELECT coalesce(v_numWareHouseItemID,0) as "v_numWareHouseItemID";

   RETURN;
END; $$;












