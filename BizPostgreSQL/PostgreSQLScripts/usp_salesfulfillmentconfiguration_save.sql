-- Stored procedure definition script USP_SalesFulfillmentConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SalesFulfillmentConfiguration_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_bitActive BOOLEAN,
	v_bitRule1IsActive BOOLEAN,
	v_numRule1BizDoc NUMERIC(18,0),
	v_tintRule1Type SMALLINT,
	v_numRule1OrderStatus NUMERIC(18,0),
	v_bitRule2IsActive BOOLEAN,
	v_numRule2OrderStatus NUMERIC(18,0),
	v_numRule2SuccessOrderStatus NUMERIC(18,0),
	v_numRule2FailOrderStatus NUMERIC(18,0),
	v_bitRule3IsActive BOOLEAN,
	v_numRule3OrderStatus NUMERIC(18,0),
	v_numRule3FailOrderStatus NUMERIC(18,0),
	v_bitRule4IsActive BOOLEAN,
	v_numRule4OrderStatus NUMERIC(18,0),
	v_numRule4FailOrderStatus NUMERIC(18,0),
	v_bitRule5IsActive BOOLEAN,
	v_numRule5OrderStatus NUMERIC(18,0),
	v_bitRule6IsActive BOOLEAN,
	v_numRule6OrderStatus NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numSFCID FROM SalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) then
	
      UPDATE
      SalesFulfillmentConfiguration
      SET
      numDomainID = v_numDomainID,bitActive = v_bitActive,bitRule1IsActive = v_bitRule1IsActive,
      numRule1BizDoc = v_numRule1BizDoc,tintRule1Type = v_tintRule1Type,
      numRule1OrderStatus = v_numRule1OrderStatus,bitRule2IsActive = v_bitRule2IsActive,
      numRule2OrderStatus = v_numRule2OrderStatus,numRule2SuccessOrderStatus = v_numRule2SuccessOrderStatus,
      numRule2FailOrderStatus = v_numRule2FailOrderStatus,
      bitRule3IsActive = v_bitRule3IsActive,numRule3OrderStatus = v_numRule3OrderStatus,
      numRule3FailOrderStatus = v_numRule3FailOrderStatus,
      bitRule4IsActive = v_bitRule4IsActive,numRule4OrderStatus = v_numRule4OrderStatus,
      numRule4FailOrderStatus = v_numRule4FailOrderStatus,bitRule5IsActive = v_bitRule5IsActive,
      numRule5OrderStatus = v_numRule5OrderStatus,
      bitRule6IsActive = v_bitRule6IsActive,numRule6OrderStatus = v_numRule6OrderStatus,
      dtModified = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID
      WHERE
      numDomainID = v_numDomainID;
   ELSE
      INSERT INTO SalesFulfillmentConfiguration(numDomainID
			,bitActive
			,bitRule1IsActive
			,numRule1BizDoc
			,tintRule1Type
			,numRule1OrderStatus
			,bitRule2IsActive
			,numRule2OrderStatus
			,numRule2SuccessOrderStatus
			,numRule2FailOrderStatus
			,bitRule3IsActive
			,numRule3OrderStatus
			,numRule3FailOrderStatus
			,bitRule4IsActive
			,numRule4OrderStatus
			,numRule4FailOrderStatus
			,bitRule5IsActive
			,numRule5OrderStatus
			,bitRule6IsActive
			,numRule6OrderStatus
			,dtCreated
			,numCreatedBy)
		VALUES(v_numDomainID
			,v_bitActive
			,v_bitRule1IsActive
			,v_numRule1BizDoc
			,v_tintRule1Type
			,v_numRule1OrderStatus
			,v_bitRule2IsActive
			,v_numRule2OrderStatus
			,v_numRule2SuccessOrderStatus
			,v_numRule2FailOrderStatus
			,v_bitRule3IsActive
			,v_numRule3OrderStatus
			,v_numRule3FailOrderStatus
			,v_bitRule4IsActive
			,v_numRule4OrderStatus
			,v_numRule4FailOrderStatus
			,v_bitRule5IsActive
			,v_numRule5OrderStatus
			,v_bitRule6IsActive
			,v_numRule6OrderStatus
			,TIMEZONE('UTC',now())
			,v_numUserCntID);
   end if;
   RETURN;
END; $$;



