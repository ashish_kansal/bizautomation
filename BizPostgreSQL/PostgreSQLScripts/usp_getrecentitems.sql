CREATE OR REPLACE FUNCTION usp_GetRecentItems(v_numUserCntID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_v1  INTEGER;
BEGIN
   IF v_numUserCntID = 0 then
	
		--get blank table to avoid error
      open SWV_RefCur for
      select 0 AS numRecId
      from Communication
      where  1 = 0;
      RETURN;
   end if;
	

   if exists(select  coalesce(intLastViewedRecord,10) FROM Domain WHERE numDomainId IN(SELECT numDomainID FROM AdditionalContactsInformation WHERE numContactId = v_numUserCntID)) then
		
      select   coalesce(intLastViewedRecord,10) INTO v_v1 FROM Domain WHERE numDomainId IN(SELECT numDomainID FROM AdditionalContactsInformation WHERE numContactId = v_numUserCntID);
   else
      v_v1 := 10;
   end if;

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      numRecordId NUMERIC(18,0),
      bintvisiteddate TIMESTAMP,
      numRecentItemsID NUMERIC(18,0),
      chrRecordType CHAR(10)
   );

   INSERT INTO
   tt_TEMPTABLE
   SELECT DISTINCT numrecordId,
		bintvisiteddate,
		numRecentItemsID,
		chrRecordType
   FROM
   RecentItems
   WHERE
   numUserCntID = v_numUserCntID
   AND bitdeleted = false
   ORDER BY
   bintvisiteddate DESC
   LIMIT v_v1;

   open SWV_RefCur for
   SELECT  DM.numDivisionID AS numRecID ,coalesce(CMP.vcCompanyName,'') as RecName,
		 DM.tintCRMType,
		 CMP.numCompanyId AS numCompanyID,
		 ADC.numContactId AS numContactID,
		 DM.numDivisionID,'C'   as Type,bintvisiteddate  ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,
		 CAST(0 AS NUMERIC(18,0)) as numOppID,
		'~/images/Icon/organization.png'  AS vcImageURL
   FROM  CompanyInfo CMP
   join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId
   LEFT join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID AND coalesce(ADC.bitPrimaryContact,false) = true
   join tt_TEMPTABLE R on R.numRecordID = DM.numDivisionID
   WHERE chrRecordType = 'C'
   union
   SELECT  DM.numDivisionID AS numRecID,
	coalesce(coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,'-'),
   '') as RecName,
		 DM.tintCRMType,
		 CMP.numCompanyId AS numCompanyID,
		 ADC.numContactId AS numContactID,
		 DM.numDivisionID,'U',bintvisiteddate     ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,
		 CAST(0 AS NUMERIC(18,0)) as numOppID,
		 '~/images/Icon/contact.png' AS vcImageURL
   FROM  CompanyInfo CMP
   join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId
   join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID
   join tt_TEMPTABLE R on numRecordID = ADC.numContactId   where chrRecordType = 'U'
   union
   select  numOppId as numRecID,coalesce(vcpOppName,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),numDivisionId,'O',bintvisiteddate  ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,
	CAST(0 AS NUMERIC(18,0)) as numOppID,
	CASE
   WHEN tintopptype = 1 AND tintoppstatus = 0 THEN '~/images/Icon/Sales opportunity.png'
   WHEN tintopptype = 1 AND tintoppstatus = 2 THEN '~/images/Icon/Sales opportunity.png'
   WHEN tintopptype = 1 AND tintoppstatus = 1 THEN '~/images/Icon/Sales order.png'
   WHEN tintopptype = 2 AND tintoppstatus = 0 THEN '~/images/Icon/Purchase opportunity.png'
   WHEN tintopptype = 2 AND tintoppstatus = 1 THEN '~/images/Icon/Purchase order.png'
   ELSE '~/images/icons/cart.png'
   END AS vcImageURL from OpportunityMaster
   join tt_TEMPTABLE  on numRecordID = numOppId    where  chrRecordType = 'O'
   union
   select  numProId as numRecID,coalesce(vcProjectName,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),numDivisionID,'P',bintvisiteddate  ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/project.png' AS vcImageURL from ProjectsMaster
   join tt_TEMPTABLE  on numRecordID = numProId  where  chrRecordType = 'P'
   union
   select  numCaseId as numRecID,
	coalesce(vcCaseNumber,'')  as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),numDivisionID,'S',bintvisiteddate ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/case.png' AS vcImageURL  from Cases
   join tt_TEMPTABLE  on numRecordID = numCaseId   where  chrRecordType = 'S'
   union
   select numCommId as numRecID,
	coalesce(case when bitTask = 971 then 'C - '
   when bitTask = 972 then 'T - '
   when bitTask = 973 then 'N - '
   when bitTask = 974 then 'F - '
   else CAST(fn_GetListItemName(bitTask) AS VARCHAR(2))
   end || CAST(textDetails AS VARCHAR(100)),'')
   as RecName,CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),numDivisionID,'A',bintvisiteddate
	 , CaseId, caseTimeid, caseExpid,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/action.png' AS vcImageURL
   from Communication
   join tt_TEMPTABLE  on numRecordID = numCommId   where  chrRecordType = 'A'
   union
   select  numItemCode as numRecID,
	coalesce(vcItemName,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),CAST(0 AS NUMERIC(18,0)),'I',bintvisiteddate ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item
   join tt_TEMPTABLE  on numRecordID = numItemCode   where  chrRecordType = 'I'
   union
   select  numItemCode as numRecID,
	coalesce(vcItemName,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),CAST(0 AS NUMERIC(18,0)),'AI',bintvisiteddate ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item
   join tt_TEMPTABLE  on numRecordID = numItemCode   where  chrRecordType = 'AI'
   union
   select  numGenericDocId as numRecID,
	coalesce(vcDocName,'')  as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),CAST(0 AS NUMERIC(18,0)),'D',bintvisiteddate ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/Doc.png' AS vcImageURL  from GenericDocuments
   join tt_TEMPTABLE  on numRecordID = numGenericDocId   where  chrRecordType = 'D'
   union
   select  numCampaignId as numRecID,coalesce(vcCampaignName,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),CAST(0 AS NUMERIC(18,0)) AS numDivisionID,'M',bintvisiteddate  ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID,'~/images/Icon/campaign.png' AS vcImageURL from CampaignMaster
   join tt_TEMPTABLE  on numRecordID = numCampaignId    where  chrRecordType = 'M'
   union
   select  numReturnHeaderID as numRecID,coalesce(vcRMA,'') as RecName,
	CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),numDivisionID,'R',bintvisiteddate  ,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,CAST(0 AS NUMERIC(18,0)) as numOppID, CASE
   WHEN tintReturnType = 1 THEN '~/images/Icon/Sales Return.png'
   WHEN tintReturnType = 2 THEN '~/images/Icon/Purchase Return.png'
   WHEN tintReturnType = 3 THEN '~/images/Icon/CreditMemo.png'
   WHEN tintReturnType = 4 THEN '~/images/Icon/Refund.png'
   ELSE
      '~/images/Icon/ReturnLastView.png'
   END AS vcImageURL from ReturnHeader
   join tt_TEMPTABLE  on numRecordID = numReturnHeaderID  where  chrRecordType = 'R'
   UNION
   SELECT
   numOppBizDocsId as numRecID,
		coalesce(vcBizDocID,'') as RecName,
		CAST(0 AS SMALLINT),0,CAST(0 AS NUMERIC(18,0)),CAST(0 AS NUMERIC(18,0)) AS numDivisionID,'B',bintvisiteddate,CAST(0 AS NUMERIC(18,0)) as caseId,CAST(0 AS NUMERIC(18,0)) as caseTimeId,CAST(0 AS NUMERIC(18,0)) as caseExpId,OpportunityMaster.numOppId as numOppID,
		(CASE
   WHEN OpportunityMaster.tintopptype = 1 THEN '~/images/Icon/Sales BizDoc.png'
   WHEN OpportunityMaster.tintopptype = 2 THEN '~/images/Icon/Purchase BizDoc.png'
   ELSE
      '~/images/Icon/Doc.png'
   END) AS vcImageURL
   FROM
   OpportunityBizDocs
   JOIN
   OpportunityMaster
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   JOIN
   tt_TEMPTABLE
   ON
   numOppBizDocsId = numRecordId
   WHERE
   chrRecordType = 'B'
   union
   SELECT
   numWOId AS numRecID
		,coalesce(vcWorkOrderName,'') as RecName
		,CAST(0 AS SMALLINT)
		,0
		,CAST(0 AS NUMERIC(18,0))
		,CAST(0 AS NUMERIC(18,0)) AS numDivisionID
		,'W'
		,bintvisiteddate
		,CAST(0 AS NUMERIC(18,0)) as caseId
		,CAST(0 AS NUMERIC(18,0)) as caseTimeId
		,CAST(0 AS NUMERIC(18,0)) as caseExpId
		,CAST(0 AS NUMERIC(18,0)) as numOppID
		,'~/images/Icon/WorkOrder.png' AS vcImageURL
   FROM
   WorkOrder
   JOIN
   tt_TEMPTABLE
   ON
   numRecordID = numWOId
   WHERE
   chrRecordType = 'W'
   order by 8 desc;
END; $$;


