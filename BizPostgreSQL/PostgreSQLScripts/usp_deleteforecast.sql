-- Stored procedure definition script usp_DeleteForecast for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteForecast(v_numForecastID1 NUMERIC(9,0),
	v_numForecastID2 NUMERIC(9,0),
	v_numForecastID3 NUMERIC(9,0),
	v_numUserID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0) DEFAULT 0 
--  
 )
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE from Forecast where numForecastID in(v_numForecastID1,v_numForecastID2,v_numForecastID3) and numCreatedBy = v_numUserID
   and numDomainID = v_numDomainID;
   RETURN;
END; $$;


