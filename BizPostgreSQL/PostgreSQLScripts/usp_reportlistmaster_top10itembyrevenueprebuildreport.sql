-- Stored procedure definition script USP_ReportListMaster_Top10ItemByRevenuePreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ItemByRevenuePreBuildReport(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   numItemCode
		,vcItemName
		,SUM(monTotAmount) AS TotalAmount
   FROM(SELECT
      I.numItemCode
			,I.vcItemName
			,coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month') TEMP
   GROUP BY
	numItemCode
	,vcItemName
   HAVING
   SUM(monTotAmount) > 0
   ORDER BY
   SUM(monTotAmount) DESC LIMIT 10;
END; $$;












