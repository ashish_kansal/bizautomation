CREATE OR REPLACE FUNCTION USP_ExtDocumentList
(
	v_numDivisionID NUMERIC(9,0) DEFAULT 0,
    v_numSearchCondition NUMERIC(9,0) DEFAULT 0,
    v_cSection CHAR(1) DEFAULT '0',
    v_SortChar CHAR(1) DEFAULT '0',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_tintMode SMALLINT DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSql TEXT;                 
	v_firstRec  INTEGER;         
	v_lastRec  INTEGER;
BEGIN
	IF v_tintMode = 0 then
		BEGIN
			CREATE TEMP SEQUENCE tt_tempTable_seq;
			EXCEPTION WHEN OTHERS THEN
			NULL;
		END;

		v_strSql := 'select 
						* 
					from
					(
						select 
							VcFileName,
							vcDocName,          
							fn_GetListItemName(numDocCategory) as Category,          
							GenericDocuments.bintCreatedDate  as CreatedDate,
							GenericDocuments.bintCreatedDate as Date,
							CAST(GenericDocuments.bintModifiedDate AS VARCHAR(20)) || '' '' || fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,
							''Organization'' as Section,
							fn_GetComapnyName(numRecID) as Name,
							vcFileType,
							numGenericDocID,
							numRecID,
							vcDocumentSection,
							numDocCategory,
							(select count(*) from DocumentWorkflow  where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending
						from 
							GenericDocuments          
						where 
							vcDocumentSection = ''A'' and numRecID =' || v_numDivisionID || '          
						union           
						select 
							VcFileName,
							vcDocName,          
							fn_GetListItemName(numDocCategory) as Category,          
							GenericDocuments.bintCreatedDate as CreatedDate,          
							GenericDocuments.bintCreatedDate as Date,          
							CAST(GenericDocuments.bintModifiedDate AS VARCHAR(20)) || '' '' || fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
							''Contact'' as Section,vcFirstName || '' '' || VcLastName as Name,          
							vcFileType,
							numGenericDocID,
							numRecID,
							vcDocumentSection,
							numDocCategory,
							(select count(*) from DocumentWorkflow  where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending
						from 
							GenericDocuments          
						join 
							AdditionalContactsInformation          
						on 
							numRecID = numContactID          
						where 
							vcDocumentSection = ''C'' 
							and numDivisionID =' || v_numDivisionID || '          
						union
						select 
							VcFileName,
							vcDocName,
							fn_GetListItemName(numDocCategory) as Category,
							GenericDocuments.bintCreatedDate as CreatedDate,          
							GenericDocuments.bintCreatedDate as Date,          
							CAST(GenericDocuments.bintModifiedDate AS VARCHAR(20)) || '' '' || fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
							''Opportunity'' as Section,
							vcPOppName as Name,          
							vcFileType,
							numGenericDocID,
							numRecID,
							vcDocumentSection,
							numDocCategory,
							(select count(*) from DocumentWorkflow  where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending
						from 
							GenericDocuments          
						join 
							OpportunityMaster          
						on 
							numRecID = numOppID          
						where 
							vcDocumentSection = ''O'' 
							and numDivisionID =' || v_numDivisionID || '          
						union
						select 
							VcFileName,
							vcDocName,
							fn_GetListItemName(numDocCategory) as Category,         
							GenericDocuments.bintCreatedDate as CreatedDate,
							GenericDocuments.bintCreatedDate as Date,
							CAST(GenericDocuments.bintModifiedDate AS VARCHAR(20)) || '' '' || fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,
							''Project'' as Section,
							vcProjectName as Name,
							vcFileType,
							numGenericDocID,
							CAST(0 AS NUMERIC(18,0)) as numOppBizDocsId,
							vcDocumentSection,
							numDocCategory,
							(select count(*) from DocumentWorkflow  where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
							(select count(*) from DocumentWorkflow where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending
						from 
							GenericDocuments          
						join 
							ProjectsMaster  
						on 
							numRecID = numProID          
						where 
							vcDocumentSection = ''P'' 
							and numDivisionID =' || v_numDivisionID || '          
						union
						select 
							CAST('''' AS VARCHAR(200)) as VcFileName,
							vcBizDocID  as vcDocName,
							fn_GetListItemName(numBizDocId) as Category,  
							dtCreatedDate as CreatedDate,          
							dtCreatedDate as Date,          
							CAST(Biz.dtModifiedDate AS VARCHAR(20)) || '' '' || fn_GetUserName(Biz.numModifiedBy) as Modified,          
							''BizDocs'' as Section,
							vcPOppName as Name,
							CAST(''-'' AS VARCHAR(10)) as vcFileType,          
							Biz.numOppId as numGenericDocID,
							numOppBizDocsId as numRecID,
							CAST(''B'' AS VARCHAR(50)) as vcDocumentSection,
							numBizDocId as numDocCategory,
							CAST(0 AS INTEGER) AS Approved,
							CAST(0 AS INTEGER) AS Declined,
							CAST(0 AS INTEGER) AS Pending
						from 
							OpportunityBizDocs Biz          
						join 
							OpportunityMaster Opp        
						on 
							Opp.numOppID = Biz.numOppId          
						where 
							numDivisionID =' || v_numDivisionID || ') X Where 1 = 1';
		IF v_SortChar <> '0' then
			v_strSql := coalesce(v_strSql,'') || ' And X.vcDocName ilike ''' || coalesce(v_SortChar,'') || '%''';
		end if;
		IF v_numSearchCondition <> 0 then
			v_strSql := coalesce(v_strSql,'') || 'and X.numDocCategory =' || v_numSearchCondition;
		end if;
		IF v_cSection <> '0' then
			v_strSql := coalesce(v_strSql,'') || 'and X.vcDocumentSection = ''' || coalesce(v_cSection,'') || '''';
		end if;
		
		v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      
		RAISE NOTICE '%',v_strSql;
		
		EXECUTE 'DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      
				CREATE TEMPORARY TABLE tt_TEMPTABLE
				(
					ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
					PRIMARY KEY,
					VcFileName VARCHAR(100),
					vcDocName VARCHAR(100),
					Category VARCHAR(100),
					CreatedDate VARCHAR(100),
					Date VARCHAR(50),
					Modified VARCHAR(150),
					Section VARCHAR(50),
					Name VARCHAR(100),
					vcFileType VARCHAR(10),
					numGenericDocID VARCHAR(15),
					numRecID VARCHAR(15),
					vcDocumentSection VARCHAR(1),
					numDocCategory VARCHAR(15),
					Approved INTEGER,
					Declined INTEGER,
					Pending INTEGER
				);
		
				INSERT INTO tt_TEMPTABLE
                (
                    VcFileName,
                    vcDocName,
                    Category,
                    CreatedDate,
                    Date,
                    Modified,
                    Section,
                    Name,
                    vcFileType,
                    numGenericDocID,
                    numRecID,
                    vcDocumentSection,
                    numDocCategory,
                    Approved,
                    Declined,
                    Pending
                )' || v_strSql;

		v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
		v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

		open SWV_RefCur for SELECT * FROM tt_TEMPTABLE WHERE ID > v_firstRec AND ID < v_lastRec;

		SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTABLE;
   end if; 


-- Get All Pending documents which requires users approval / declined 
	IF v_tintMode = 1 then
		open SWV_RefCur for
		SELECT  
			numDocID,
			numContactID,
			tintApprove,
			dtApprovedOn,
			cDocType,
			vcComment,
			dtCreatedDate,
			SD.VcDocName,
			SD.numGenericDocID,
			SD.vcDocumentSection,
			SD.numRecID,
			fn_GetListItemName(numDocCategory) AS Category,
			SD.numCreatedBy AS numRecOwner
		FROM 
			DocumentWorkflow DW
		INNER JOIN 
			GenericDocuments SD 
		ON 
			DW.numDocID = SD.numGenericDocID
		WHERE 
			numContactID = v_numUserCntID
			AND tintApprove = 0        
		UNION
		SELECT 
			numDocID,
            DW.numContactID,
            tintApprove,
            dtApprovedOn,
            cDocType,
            vcComment,
            DW.dtCreatedDate,
            BD.vcBizDocID,
            BD.numOppBizDocsId AS numGenericDocID,
            cDocType AS vcFileType,
            BD.numoppid AS numRecID,
            LD.vcData AS Category,
            OM.numrecowner AS numRecOwner
		FROM 
			DocumentWorkflow DW
		INNER JOIN 
			OpportunityBizDocs BD 
		ON 
			DW.numDocID = BD.numOppBizDocsId
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OM.numOppId = BD.numoppid
		INNER JOIN 
			Listdetails LD 
		ON 
			LD.numListItemID = BD.numBizDocId
			AND LD.numListID = 27
		WHERE 
			DW.numContactID = v_numUserCntID
			AND cDocType = 'B'
			AND tintApprove = 0;
   end if;

   RETURN;
END; $$;


