-- Stored procedure definition script USP_GetOrderExpenseItemsAddBill for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOrderExpenseItemsAddBill(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF coalesce(v_numOppItemID,0) > 0 then
	
		open SWV_RefCur for
		SELECT
			OI.numoppitemtCode AS "numoppitemtCode"
			,coalesce(OI.vcItemName,I.vcItemName) AS "vcItemName"
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numoppitemtCode = v_numOppItemID;
	ELSE
		open SWV_RefCur for
		SELECT
			OI.numoppitemtCode AS "numoppitemtCode"
			,coalesce(OI.vcItemName,I.vcItemName) AS "vcItemName"
		FROM
		OpportunityItems OI
		INNER JOIN
		Item I
		ON
		OI.numItemCode = I.numItemCode
		WHERE
		OI.numOppId = v_numOppID
		AND coalesce(I.charItemType,'') = 'S'
		AND coalesce(bitExpenseItem,false) = true
		AND OI.numoppitemtCode NOT IN(SELECT BD.numOppItemID FROM BillDetails BD WHERE numOppID = v_numOppID)
		ORDER BY
		coalesce(OI.numSortOrder,0);
	end if;
	RETURN;
END; $$;


