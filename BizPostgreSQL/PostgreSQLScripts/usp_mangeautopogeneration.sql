-- Stored procedure definition script USP_MangeAutoPOGeneration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MangeAutoPOGeneration(v_numParentOppID NUMERIC(9,0),
v_numOppBizDocID NUMERIC(9,0),
v_numItemCode NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),
v_numOppBizDocItemID NUMERIC(9,0),
v_monDealAmount DECIMAL(20,5),
v_monPaidAmount DECIMAL(20,5))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewOppId  NUMERIC(9,0);
   v_numContactId  NUMERIC(9,0);
   v_numDivisionId  NUMERIC(9,0);
   v_numUserCntID  NUMERIC(9,0);
   v_vcCompanyName  VARCHAR(250);
   v_monCost  DECIMAL(20,5);
   v_CloseDate  TIMESTAMP;
   v_vcPOppName  VARCHAR(500);
   v_numBizDocId  NUMERIC(9,0);
   v_numCurrencyCode  NUMERIC;
   v_TotalAmount  DECIMAL(20,5);
   v_numOppBizDocsId  NUMERIC(9,0);
   v_numOldBizDocId  NUMERIC(9,0);
   v_USP_CreateBizDocs_numOppBizDocsId NUMERIC(9,0);
   v_USP_CreateBizDocs_monCreditAmount DECIMAL(20,5);
   v_USP_CreateBizDocs_monDealAmount DECIMAL(20,5);
BEGIN
   select   numCurrencyID INTO v_numCurrencyCode from Domain where numDomainId = v_numDomainID;

   v_CloseDate := LOCALTIMESTAMP;

--delete from  temp_AutoOrder;



--insert into temp_AutoOrder
--select
--@numParentOppID ,
--@numOppBizDocID ,
--@numItemCode ,
--@numDomainID ,
--@numOppBizDocItemID ,
--@numNewOPpId ,
--@numDivisionID

------ FOR THE RULE WHERE BIZDOC PAID FULLY

   v_numDivisionId := 0;
--if @monDealAmount=@monPaidAmount
	
				
   select   DV.numDivisionID, vcCompanyName, B.numCreatedBy, AC.numContactId, monCost INTO v_numDivisionId,v_vcCompanyName,v_numUserCntID,v_numContactId,v_monCost from OpportunityMaster A
   inner join OpportunityBizDocs B
   on B.numoppid = A.numOppId
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
   inner join(select * from Item D where D.numItemCode = v_numItemCode) D
   on D.numItemCode = v_numItemCode
   inner join Vendor E
   on E.numVendorID = D.numVendorID and E.numItemCode = D.numItemCode
   inner join
   OrderAutoRule F
   on F.numDomainID = A.numDomainId --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
   inner join DivisionMaster DV
   on DV.numDivisionID = E.numVendorID
   inner join CompanyInfo CF
   on CF.numCompanyId = DV.numCompanyID inner join
   AdditionalContactsInformation AC
   on AC.numDivisionId = DV.numDivisionID and coalesce(AC.bitPrimaryContact,false) = true where A.numOppId = v_numParentOppID and D.numItemCode = v_numItemCode and
   A.numDomainId = v_numDomainID and D.charItemType = 'P' and
   btFullPaid = true and B.monAmountPaid = v_monDealAmount and
   B.numBizDocStatus =(case coalesce(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and
   AC.numDomainID = v_numDomainID and
   B.numOppBizDocsId = v_numOppBizDocID;

   insert into Temp_Cheking (vcdesc) select 'Insert Amount Match: Div ' || cast(v_numDivisionId as VARCHAR(50));

   v_numNewOppId := 0;

   select   OM.numOppId INTO v_numNewOppId from OpportunityLinking OL inner join
   OpportunityMaster OM on OM.numOppId = OL.numChildOppID where numParentOppBizDocID = v_numOppBizDocID and numParentOppID = v_numParentOppID and
   OM.numDomainId = v_numDomainID and numDivisionId = v_numDivisionId;
						   
					--update temp_autoOrder set numDivisionID =@numDivisionId;

   v_vcPOppName := '';
   if v_numDivisionId > 0 then
						
      v_vcPOppName := coalesce(v_vcCompanyName,'') || '-PO-' || TO_CHAR(TIMEZONE('UTC',now()),'Month');
      if v_numNewOppId = 0 then
								
         SELECT * INTO v_numNewOppId,v_vcPOppName FROM USP_OppManage(v_numNewOppId,v_numContactId := v_numContactId,v_numDivisionId := v_numDivisionId,
         v_tintSource := 0,v_vcPOppName := v_vcPOppName,v_numUserCntID := v_numUserCntID,
         v_numDomainID := v_numDomainID,v_dtEstimatedCloseDate := v_CloseDate,
         v_CampaignID := 0,v_lngPConclAnalysis := 0,v_tintOppType := 2::SMALLINT,
         v_tintActive := 1::SMALLINT,v_numSalesOrPurType := 0,
         v_numRecurringId := 0,v_numCurrencyID := v_numCurrencyCode,v_DealCompleted := 0:: VARCHAR); 


									--update temp_autoOrder set numNewOPpId =@numNewOppId;

									                                                                          
									--@Comments ='',                                                                          
									--@bitPublicFlag =0,                                                                          
									                                                                                 
									--@monPAmount  =0,                                                 
									--@numAssignedTo =0,                                                                                                        
									                                                                                                                                                                                   
									--@strItems =null,                                                                          
									--@strMilestone =null,                                                                          
         insert into OpportunityLinking(numParentOppID,numChildOppID,numParentOppBizDocID) values(v_numParentOppID,v_numNewOppId,v_numOppBizDocID);
									
         insert into Temp_Cheking (vcdesc) select 'Insert Amount Match: New Opp ' || cast(v_numNewOppId as VARCHAR(50));
      end if;
      if v_numNewOppId > 0 then
											
         if not exists(select numItemCode from OpportunityItems where numOppId = v_numNewOppId and numItemCode = v_numItemCode) then
													
            insert into Temp_Cheking (vcdesc) select 'Insert Amount Match: BizDoc Opp Items - ' || cast(v_numOppBizDocItemID as VARCHAR(50));
            insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
            select v_numNewOppId,v_numItemCode,numUnitHour,v_monCost,v_monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
            from OpportunityBizDocItems where numOppBizDocItemID = v_numOppBizDocItemID;
         end if;
      end if;
   end if;

------------------- END OF AMOUTN NOT MATCH

--------------- INSERTING INTO BIZDOC AND BIZDOC ITEMS-----------------------

   if v_numNewOppId > 0 then
					
      select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_numNewOppId;
      update OpportunityMaster set  monPAmount = v_TotalAmount,tintoppstatus = 1
      where numOppId = v_numNewOppId;
      if not exists(select numoppid from OpportunityBizDocs where numoppid = v_numNewOppId) then
					
							
							 --@monShipCost =0,
         select   numAuthoritativePurchase INTO v_numBizDocId from AuthoritativeBizDocs where numDomainId = v_numDomainID;
         v_USP_CreateBizDocs_numOppBizDocsId := 0;
         v_USP_CreateBizDocs_monCreditAmount := 0;
         v_USP_CreateBizDocs_monDealAmount := 0;
         PERFORM USP_CreateBizDocs(v_numOppId := v_numNewOppId,v_numBizDocId := v_numBizDocId,v_numUserCntID := v_numUserCntID,
         v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
         v_vcComments := '',v_bitPartialFulfillment := false,v_strBizDocItems := '',
         v_bitDiscountType := 0:: VARCHAR,v_fltDiscount := 0:: VARCHAR,
         v_numShipVia := 0,v_vcTrackingURL := '',v_bitBillingTerms := 0:: VARCHAR,
         v_intBillingDays := 0:: VARCHAR,v_bitInterestType := 0:: VARCHAR,v_fltInterest := v_USP_CreateBizDocs_monCreditAmount:: VARCHAR,
         v_numShipDoc := 0:: VARCHAR,
         v_numBizDocStatus := v_USP_CreateBizDocs_monDealAmount,
         v_bitRecurringBizDoc := false,v_vcBizDocName := '');
      else
         v_numOldBizDocId := 0;
         select   B.numOppBizDocsId INTO v_numOldBizDocId from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocID = B.numOppBizDocsId where numItemCode = v_numItemCode AND B.numoppid = v_numNewOppId;
         select   numOppBizDocsId INTO v_numOppBizDocID from OpportunityBizDocs where numoppid = v_numNewOppId;
         if v_numOldBizDocId = 0 then
						
            insert into Temp_Cheking (vcdesc) select 'Insert BizDocs 2nd Item - ';
            insert into
            OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
            select v_numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice*1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
            where  numOppId = v_numNewOppId AND numItemCode = v_numItemCode;
         end if;
      end if;
   end if;
------------------------------------- END OF BIZDOC ITEM INSERT -------------------
------------------------------------- START AMOUNT NOT MATCH -------------------
   insert into Temp_Cheking (vcdesc) select 'Insert Amount Not Match';
   v_numDivisionId := 0;

   select   DV.numDivisionID, vcCompanyName, B.numCreatedBy, AC.numContactId, monCost INTO v_numDivisionId,v_vcCompanyName,v_numUserCntID,v_numContactId,v_monCost from OpportunityMaster A
   inner join OpportunityBizDocs B
   on B.numoppid = A.numOppId
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
   inner join(select * from Item D where D.numItemCode = v_numItemCode) D
   on D.numItemCode = v_numItemCode
   inner join Vendor E
   on E.numVendorID = D.numVendorID and E.numItemCode = D.numItemCode
   inner join
   OrderAutoRule F
   on F.numDomainID = A.numDomainId --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
   inner join DivisionMaster DV
   on DV.numDivisionID = E.numVendorID
   inner join CompanyInfo CF
   on CF.numCompanyId = DV.numCompanyID inner join
   AdditionalContactsInformation AC
   on AC.numDivisionId = DV.numDivisionID and coalesce(AC.bitPrimaryContact,false) = true where A.numOppId = v_numParentOppID and D.numItemCode = v_numItemCode and
   A.numDomainId = v_numDomainID and D.charItemType = 'P' and
   btFullPaid = false and
   B.numBizDocStatus =(case coalesce(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and
   AC.numDomainID = v_numDomainID and
   B.numOppBizDocsId = v_numOppBizDocID;

   insert into Temp_Cheking (vcdesc)  select 'Insert Amount Not Match ' || cast(v_numDivisionId as VARCHAR(100));

   v_numNewOppId := 0;

   select   OM.numOppId INTO v_numNewOppId from OpportunityLinking OL inner join
   OpportunityMaster OM on OM.numOppId = OL.numChildOppID where numParentOppBizDocID = v_numOppBizDocID and numParentOppID = v_numParentOppID and
   OM.numDomainId = v_numDomainID and numDivisionId = v_numDivisionId;


			


   v_vcPOppName := '';
   if v_numDivisionId > 0 then
				
      v_vcPOppName := coalesce(v_vcCompanyName,'') || '-PO-' || TO_CHAR(TIMEZONE('UTC',now()),'Month');
      if v_numNewOppId = 0 then
						
         SELECT * INTO v_numNewOppId,v_vcPOppName FROM USP_OppManage(v_numNewOppId,v_numContactId := v_numContactId,v_numDivisionId := v_numDivisionId,
         v_tintSource := 0,v_vcPOppName := v_vcPOppName,v_numUserCntID := v_numUserCntID,
         v_numDomainID := v_numDomainID,v_dtEstimatedCloseDate := v_CloseDate,
         v_CampaignID := 0,v_lngPConclAnalysis := 0,v_tintOppType := 2::SMALLINT,
         v_tintActive := 1::SMALLINT,v_numSalesOrPurType := 0,
         v_numRecurringId := 0,v_numCurrencyID := v_numCurrencyCode,v_DealCompleted := 0:: VARCHAR);  
							
							--update temp_autoOrder set numNewOPpId =@numNewOppId;
							
							                                                                          
							--@Comments ='',                                                                          
							--@bitPublicFlag =0,                                                                          
							                                                                                 
							--@monPAmount  =0,                                                 
							--@numAssignedTo =0,                                                                                                        
							                                                                                                                                                                                   
							--@strItems =null,                                                                          
							--@strMilestone =null,                                                                          
         insert into OpportunityLinking(numParentOppID,numChildOppID,numParentOppBizDocID) values(v_numParentOppID,v_numNewOppId,v_numOppBizDocID);
      end if;
      if v_numNewOppId > 0 then
									
         if not exists(select numItemCode from OpportunityItems where numOppId = v_numNewOppId and numItemCode = v_numItemCode) then
										
            insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
            select v_numNewOppId,v_numItemCode,numUnitHour,v_monCost,v_monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
            from OpportunityBizDocItems where numOppBizDocItemID = v_numOppBizDocItemID;
         end if;
      end if;
   end if;
------------------- END OF AMOUNT NOT MATCH------------------------------------------

   if v_numNewOppId > 0 then
					
      select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_numNewOppId;
      update OpportunityMaster set  monPAmount = v_TotalAmount,tintoppstatus = 1
      where numOppId = v_numNewOppId;
      if not exists(select numoppid from OpportunityBizDocs where numoppid = v_numNewOppId) then
					
							
							 --@monShipCost =0,
         select   numAuthoritativePurchase INTO v_numBizDocId from AuthoritativeBizDocs where numDomainId = v_numDomainID;
         v_USP_CreateBizDocs_numOppBizDocsId := 0;
         v_USP_CreateBizDocs_monCreditAmount := 0;
         v_USP_CreateBizDocs_monDealAmount := 0;
         PERFORM USP_CreateBizDocs(v_numOppId := v_numNewOppId,v_numBizDocId := v_numBizDocId,v_numUserCntID := v_numUserCntID,
         v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
         v_vcComments := '',v_bitPartialFulfillment := false,v_strBizDocItems := '',
         v_bitDiscountType := 0:: VARCHAR,v_fltDiscount := 0:: VARCHAR,
         v_numShipVia := 0,v_vcTrackingURL := '',v_bitBillingTerms := 0:: VARCHAR,
         v_intBillingDays := 0:: VARCHAR,v_bitInterestType := 0:: VARCHAR,v_fltInterest := v_USP_CreateBizDocs_monCreditAmount:: VARCHAR,
         v_numShipDoc := 0:: VARCHAR,
         v_numBizDocStatus := v_USP_CreateBizDocs_monDealAmount,
         v_bitRecurringBizDoc := false,v_vcBizDocName := '');
      else
         v_numOldBizDocId := 0;
         select   B.numOppBizDocsId INTO v_numOldBizDocId from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocID = B.numOppBizDocsId where numItemCode = v_numItemCode AND B.numoppid = v_numNewOppId;
         select   numOppBizDocsId INTO v_numOppBizDocID from OpportunityBizDocs where numoppid = v_numNewOppId;
         if v_numOldBizDocId = 0 then
						
            insert into Temp_Cheking (vcdesc) select 'Insert BizDocs 2nd Item - ';
            insert into
            OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
            select v_numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice*1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
            where  numOppId = v_numNewOppId AND numItemCode = v_numItemCode;
         end if;
      end if;
   end if;
   RETURN;
-------------------------------------------
---======================================================

END; $$; -- THE FINAL END



