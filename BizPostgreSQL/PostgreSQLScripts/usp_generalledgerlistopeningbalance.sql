-- Stored procedure definition script USP_GeneralLedgerListOpeningBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GeneralLedgerListOpeningBalance(v_dtFromDate TIMESTAMP,                                                                      
v_dtToDate TIMESTAMP,      
v_numDomainId NUMERIC(9,0),
INOUT v_monBalance DECIMAL(20,5) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(20000);                                                                                                                                                      
   v_strChildCategory  VARCHAR(20000);                                                                                                              
   v_numAcntTypeId  INTEGER;                                                                                                                                                 
   v_OpeningBal  VARCHAR(8000);                                                                            
   v_numAcntOpeningBal  INTEGER;
   SWV_ExecDyn  VARCHAR(20000);
   v_TotRecs  NUMERIC(9,0);                                                                                                                            
   v_i  NUMERIC(9,0);                                                                                                                                  
   v_numCustId  VARCHAR(9);                                                                                                                           
   v_numPayment  DECIMAL(20,5);                                                                                     
   v_numDeposit  DECIMAL(20,5);                                                                                             
   v_numBalanceAmt  DECIMAL(20,5);                        
   v_bitBalance  BOOLEAN;                                                                                                 
   v_numCheckId  NUMERIC(9,0);             
   v_numCashCreditCardId  NUMERIC(9,0);                                                                                                
--Declare @numCashCreditCardId as numeric(9)                                                                                                                        
   v_j  NUMERIC(9,0);                                                                                                                                  
   v_numCustomerId  NUMERIC(9,0);                                                                                               
   v_bitChargeBack  BOOLEAN;                                                            
   v_bitMoneyOut  BOOLEAN;
BEGIN
   v_OpeningBal := 'Opening Balance';                                                                                                                                  
   v_strSQL := '';                                                                                                                              
--Create a Temporary table to hold data                                                                                                                                                                      
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCustomerId NUMERIC(9,0),
      JournalId NUMERIC(9,0),
      TransactionType VARCHAR(500),
      EntryDate TIMESTAMP,
      CompanyName VARCHAR(8000),
      CheckId NUMERIC(9,0),
      CashCreditCardId NUMERIC(9,0),
      Memo VARCHAR(8000),
      Payment DECIMAL(20,5),
      Deposit DECIMAL(20,5),
      numBalance DECIMAL(20,5),
      numChartAcntId NUMERIC(9,0),
      numTransactionId NUMERIC(9,0),
      AcntTypeDescription  VARCHAR(50),
      numOppID NUMERIC(9,0),
      numOppBizDocsId NUMERIC(9,0),
      numDepositId NUMERIC(9,0),
      numBizDocsPaymentDetId NUMERIC(9,0)
   );                                                                                                            
                                                                                                                        
   if v_dtFromDate = '1900-01-01 00:00:00.000' then 
      v_dtFromDate := CAST('Jan  1 1753 12:00:00:000AM' AS TIMESTAMP);
   end if;                                                              
                                                                     
                                                                                             
        
                                   
   v_strSQL := '   Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                                
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then ''Cash''                                                                        
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then  ''Charge''                                                                           
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = false then ''Credit''                              
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 1 then ''BizDocs Invoice''                         
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 2 then ''BizDocs Purchase''                       
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''                             
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 1 then ''Receive Amt''                    
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 2 then ''Vendor Amt''                                                                         
Else Case When GJH.numJournal_Id <> 0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                     
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,             
GJH.numCashCreditCardId as CashCreditCardId,                                                                                                  
GJD.varDescription as Memo,                                                                                            
(case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit ,         
(case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,                                                                                                        
null AS numBalance,  
coalesce(GJD.numChartAcntId,0) as numChartAcntId,    
GJD.numTransactionId as numTransactionId,                                                       
LD.vcData as AcntTypeDescription,                            
GJH.numOppId as numOppId,                              
GJH.numOppBizDocsId as numOppBizDocsId,                          
GJH.numDepositId as numDepositId,                        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId                                                                     
From General_Journal_Header as GJH                                                                                                                             
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id = GJD.numJournalId                              
Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID                                                                                           
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId                                                                 
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId                                                                                
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId                                                                            
Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId                                                                            
Left outer join ListDetails LD on CA.numAcntType = LD.numListItemID                  
Left outer join OpportunityMaster Opp on GJH.numOppId = Opp.numOppId  
Where  GJH.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) ||
   ' And (CA.numAcntType = 815 or CA.numAcntType = 816 or CA.numAcntType = 820  or CA.numAcntType = 821  or CA.numAcntType = 822  or CA.numAcntType = 825 or CA.numAcntType = 827)
 And GJH.datEntry_Date <= ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || '''
  ';                                       
                                                                                                         
                                                                                                           
                          
   v_strSQL := v_strSQL || ' union all Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                              
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''                                                                          
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''                                                            
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=false then ''Credit''                               
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''                         
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''                            
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''                           
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''                    
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''                                                                         
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                    
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,                                                                          
GJH.numCashCreditCardId as CashCreditCardId,                                  
GJD.varDescription as Memo,                                                             
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,                                                        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,                                                         
null numBalance,  
coalesce(GJD.numChartAcntId,0) as numChartAcntId,  
GJD.numTransactionId as numTransactionId,  
LD.vcData as AcntTypeDescription,  
GJH.numOppId as numOppId,  
GJH.numOppBizDocsId as numOppBizDocsId,                          
GJH.numDepositId as numDepositId,                        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId   
From General_Journal_Header as GJH                                                                                                                  
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId                                                                                                           
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID                                                                                                                                         
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                                                                  
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId                                                                             
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId                                                                            
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId                  
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId                 
Left outer join ListDetails LD on CA.numAcntType= LD.numListItemID                                                                      
Where  GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) ||
   ' And (CA.numAcntType=813 or CA.numAcntType=814 or CA.numAcntType=817  or CA.numAcntType=818  or CA.numAcntType=819  or CA.numAcntType=823 or CA.numAcntType=824 Or CA.numAcntType=826)
 And GJH.datEntry_Date<=''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || '''
 order by EntryDate';                                                          
 
                                      
                                                                          
   RAISE NOTICE '%',(v_strChildCategory);                                   
   RAISE NOTICE '%',(v_strSQL);                                                            
--Exec (@strSQL)                                                                                                         
                                                               
                           
   SWV_ExecDyn := 'CREATE TEMP TABLE IF NOT EXISTS tt_TEMPTABLE AS ' || coalesce(v_strSQL,'');
   EXECUTE SWV_ExecDyn;                                                                                                                                  
   v_bitBalance := false;                                                                            
   v_i := 1;                                                                                                                               
   v_j := 0;                                                                                                                        
                      
                                                                                                 
                                                                                                                             
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                              
   RAISE NOTICE '%',(v_TotRecs);                                                                                                                    
   While v_i <= v_TotRecs LOOP
      select   coalesce(numCustomerId,0), coalesce(Payment,0), coalesce(Deposit,0), coalesce(CheckId,0), coalesce(CashCreditCardId,0) INTO v_numCustId,v_numPayment,v_numDeposit,v_numCheckId,v_numCashCreditCardId From tt_TEMPTABLE Where ID = v_i;
      RAISE NOTICE '@numDeposit%',SUBSTR(CAST(v_numDeposit AS VARCHAR(30)),1,30);
      RAISE NOTICE '@numPayment%',SUBSTR(CAST(v_numPayment AS VARCHAR(30)),1,30);
      RAISE NOTICE '@CheckId1%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
      RAISE NOTICE '@numCashCreditCardId%',SUBSTR(CAST(v_numCashCreditCardId AS VARCHAR(50)),1,50);
      select   bitChargeBack, bitMoneyOut INTO v_bitChargeBack,v_bitMoneyOut From CashCreditCardDetails Where numCashCreditId = v_numCashCreditCardId;
      RAISE NOTICE '%',v_bitChargeBack;
      RAISE NOTICE '%',v_bitMoneyOut;
      If v_i = 1 then
 
         If v_numPayment <> 0 then
  
            Update  tt_TEMPTABLE Set numBalance = -v_numPayment Where ID = v_i;
            If v_numAcntTypeId = 815 then
       
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If  v_numCashCreditCardId <> 0 then
            
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                  end if;
               Else
                  RAISE NOTICE 'Shlok - SP';
                  Update  tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
         Else
            RAISE NOTICE 'SP';
            Update  tt_TEMPTABLE Set numBalance = v_numDeposit Where ID = v_i;
            RAISE NOTICE '@numAcntTypeId%',SUBSTR(CAST(v_numAcntTypeId AS VARCHAR(30)),1,30);
            If v_numAcntTypeId = 815 then
    
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
        
                  If  v_numCashCreditCardId <> 0 then
            
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
            end if;
         end if;
      end if;
      If v_i > 1 then

         If v_numPayment <> 0 then

            v_j := v_i -1;
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update  tt_TEMPTABLE Set numBalance = v_numBalanceAmt -v_numPayment Where ID = v_i;
            If v_numAcntTypeId = 815 then
     
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If v_numCashCreditCardId <> 0 then
      
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
         Else
            v_j := v_i -1;
            RAISE NOTICE '@CheckId2%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update  tt_TEMPTABLE Set numBalance = v_numBalanceAmt+v_numDeposit Where ID = v_i;
            If v_numAcntTypeId = 815 then

               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If v_numCashCreditCardId <> 0 then
            
                     if v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
         end if;
      end if;
      v_i := v_i+1;
   END LOOP;             
        
   select   numBalance INTO v_monBalance From tt_TEMPTABLE Where ID = v_TotRecs;  
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


