-- FUNCTION: public.usp_getembeddedcostitems(numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getembeddedcostitems(numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getembeddedcostitems(
	v_numembeddedcostid numeric,
	v_numdomainid numeric,
	INOUT swv_refcur refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:46:46 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF EXISTS(SELECT * FROM    EmbeddedCostItems
   WHERE   numEmbeddedCostID = v_numEmbeddedCostID) then
            
      open SWV_RefCur for
      SELECT 
                        --ECI.[numEmbeddedCostID],
      BDI.numOppBizDocItemID,
                        coalesce(ECI.monAmount,0) AS monAmount,
                        I.fltHeight,
                        I.fltLength,
                        I.fltWeight,
                        I.fltWidth,
                        BDI.numUnitHour,
                        I.vcUnitofMeasure,
                        OI.vcItemName,
                        OI.vcModelID
      FROM    OpportunityBizDocItems BDI
      INNER JOIN OpportunityItems OI ON OI.numoppitemtCode = CAST(BDI.numOppItemID As Numeric)
      INNER JOIN Item I ON I.numItemCode = BDI.numItemCode
      LEFT JOIN EmbeddedCostItems ECI ON ECI.numOppBizDocItemID = BDI.numOppBizDocItemID
      AND ECI.numEmbeddedCostID = v_numEmbeddedCostID
      WHERE
      BDI.numOppBizDocID IN(SELECT numOppBizDocID FROM EmbeddedCost WHERE numEmbeddedCostID = v_numEmbeddedCostID)
      AND
      I.numDomainID = v_numDomainID;
   ELSE
      open SWV_RefCur for
      SELECT
      BDI.numOppBizDocItemID,
                        0 AS monAmount,
                        I.fltHeight,
                        I.fltLength,
                        I.fltWeight,
                        I.fltWidth,
                        BDI.numUnitHour,
                        I.vcUnitofMeasure,
                        OI.vcItemName,
                        OI.vcModelID
      FROM    OpportunityBizDocItems BDI
      INNER JOIN OpportunityItems OI ON OI.numoppitemtCode = CAST(BDI.numOppItemID As Numeric)
      INNER JOIN Item I ON I.numItemCode = BDI.numItemCode
      WHERE
      BDI.numOppBizDocID IN(SELECT numOppBizDocID FROM EmbeddedCost WHERE numEmbeddedCostID = v_numEmbeddedCostID)
      AND I.numDomainID = v_numDomainID;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getembeddedcostitems(numeric, numeric, refcursor)
    OWNER TO postgres;
