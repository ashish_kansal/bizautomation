-- Stored procedure definition script USP_BankReconcileMatchRule_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BankReconcileMatchRule_Delete(v_numDomainID NUMERIC(18,0),
	v_numRuleID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  INTEGER;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   tintOrder INTO v_tintOrder FROM BankReconcileMatchRule WHERE numDomainID = v_numDomainID AND ID = v_numRuleID;
      DELETE FROM BankReconcileMatchRuleCondition WHERE numRuleID = v_numRuleID;
      UPDATE BankReconcileMatchRule SET tintOrder = tintOrder -1 WHERE numDomainID = v_numDomainID AND tintOrder > v_tintOrder;
      DELETE FROM BankReconcileMatchRule WHERE numDomainID = v_numDomainID AND ID = v_numRuleID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;

