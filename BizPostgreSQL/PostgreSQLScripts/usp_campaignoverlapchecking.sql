-- Stored procedure definition script usp_CampaignOverlapChecking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Obsolete Procedure, reason - bug id 465 #2
CREATE OR REPLACE FUNCTION usp_CampaignOverlapChecking(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_intLaunchDate TIMESTAMP DEFAULT NULL,        
v_intEndDate TIMESTAMP DEFAULT NULL,      
v_byteMode SMALLINT DEFAULT NULL,      
v_numCampaignID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then ---When Adding a new record      

      open SWV_RefCur for
      select count(*) from CampaignMaster where numDomainID = v_numDomainID and (v_intLaunchDate between intLaunchDate and intEndDate 
      or v_intEndDate  between intLaunchDate and intEndDate) AND bitIsOnline <> true;
   end if;      
      
   if v_byteMode = 1 then -- Editing a Record      

      open SWV_RefCur for
      select count(*) from CampaignMaster where numDomainID = v_numDomainID and numCampaignID <> v_numCampaignID and (v_intLaunchDate between intLaunchDate and intEndDate 
      or v_intEndDate  between intLaunchDate and intEndDate) AND	bitIsOnline <> true;
   end if;

   IF v_byteMode = 2 then -- Online Campaign No need to validate date range

      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


