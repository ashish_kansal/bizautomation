-- Stored procedure definition script USP_UpdatePortalWorkFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatePortalWorkFlow(v_tintCreateOppStatus SMALLINT,
v_numStatus NUMERIC(9,0),
v_numRelationShip NUMERIC(9,0),
v_tintCreateBizDoc SMALLINT,
v_numBizDocName NUMERIC(9,0),
v_tintAccessAllowed SMALLINT,
v_vcOrderSuccessfullPage VARCHAR(300),
v_vcOrderUnsucessfullPage VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update PortalWorkflow set
   tintCreateOppStatus = v_tintCreateOppStatus,numStatus = v_numStatus,numRelationShip = v_numRelationShip,
   tintCreateBizDoc = v_tintCreateBizDoc,numBizDocName = v_numBizDocName,
   tintAccessAllowed = v_tintAccessAllowed,vcOrderSuccessfullPage = v_vcOrderSuccessfullPage,
   vcOrderUnsucessfullPage = v_vcOrderUnsucessfullPage;
   RETURN;
END; $$;


