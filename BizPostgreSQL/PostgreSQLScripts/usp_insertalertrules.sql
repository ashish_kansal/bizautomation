-- Stored procedure definition script usp_InsertAlertRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertAlertRules(v_emailAlertID NUMERIC(9,0) DEFAULT 0, 
	v_numContactid NUMERIC(9,0) DEFAULT 0,
	v_numalerttype SMALLINT DEFAULT 1,
	v_numage INTEGER DEFAULT 0,
	v_numbirthdate BIGINT DEFAULT 0,
	v_charemailid VARCHAR(50) DEFAULT '',
	v_numCreatedBy NUMERIC(18,0) DEFAULT 1,
	v_bintCreatedDate BIGINT DEFAULT 0,
	v_charFlag CHAR(1) DEFAULT NULL  --This is for checking Insert(I),Delete(D),Update(U)
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF (v_charFlag = 'D') then
	
      UPDATE EmailAlert set alertDeletedBy = v_numCreatedBy,alertDeletedDate = v_bintCreatedDate where emailalertid = v_emailAlertID;
   ELSEIF (v_charFlag = 'U')
   then
	
      UPDATE EmailAlert set numContactId = v_numContactid,alerttype = v_numalerttype,age = v_numage,emailid = v_charemailid,
      alertModifiedBy = v_numCreatedBy,alertModifiedDate = v_bintCreatedDate where emailalertid = v_emailAlertID;
   ELSE
      INSERT INTO EmailAlert  VALUES(v_numContactid,v_numalerttype,v_numage,v_charemailid,v_numCreatedBy,v_bintCreatedDate,0,0,0,0,0);
   end if;
   RETURN;
END; $$;


