-- Function definition script GetECamLastAct for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetECamLastAct(v_numContactID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LastActivity  VARCHAR(100);
BEGIN
   select   vcDocName || ', ' || to_char(bintSentON, 'Mon dd yyyy HH12:MI AM') INTO v_LastActivity from ConECampaign Con
   join ConECampaignDTL CDTL
   on CDTL.numConECampID = Con.numConEmailCampID
   join ECampaignDTLs EDTL
   on CDTL.numECampDTLID = EDTL.numECampDTLId
   join GenericDocuments G
   on numGenericDocID = numEmailTemplate where Con.numContactID = v_numContactID and bitEngaged = true and bitSend = true   order by numConECampDTLID desc LIMIT 1;

   return coalesce(v_LastActivity,'-');
END; $$;

