-- Function definition script GetCOGSValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCOGSValue(v_numDomainID NUMERIC(9,0),
				v_dtFromDate TIMESTAMP,
				v_dtToDate TIMESTAMP)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monCOGS  DECIMAL(20,5);
BEGIN
   select   sum(COGS) INTO v_monCOGS from View_COGSReport where numDomainID = v_numDomainID and dtCreatedDate between v_dtFromDate and v_dtToDate;
			
   RETURN v_monCOGS;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[GetCommisionDtls]    Script Date: 07/26/2008 18:12:56 ******/

