-- Function definition script GetDaysInMonth for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDaysInMonth(v_date    TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN CASE 
			WHEN EXTRACT(MONTH FROM v_date) IN (1,3,5,7,8,10,12) 
			THEN 31
			WHEN EXTRACT(MONTH FROM v_date) IN (4,6,9,11) 
			THEN 30
			ELSE 
				CASE 
					WHEN (MOD(CAST(EXTRACT(YEAR FROM v_date) AS INTEGER),4) = 0 AND MOD(CAST(EXTRACT(YEAR FROM v_date) AS INTEGER),100) <> 0) OR (MOD(CAST(EXTRACT(YEAR FROM v_date) AS INT),400)  = 0)
					THEN 29
					ELSE 28
				END
			END;
END; $$;

