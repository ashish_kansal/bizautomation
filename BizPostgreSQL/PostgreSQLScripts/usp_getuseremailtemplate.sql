-- Stored procedure definition script USP_GetUserEmailtemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--exec [USP_GetUserEmailtemplate] 17,72,0
CREATE OR REPLACE FUNCTION USP_GetUserEmailtemplate(v_numUserID NUMERIC(18,0) DEFAULT 0,
    v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_isMarketing BOOLEAN DEFAULT false,
	v_numGroupId NUMERIC(18,0) DEFAULT 0,
	v_numCategoryId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dynamicQuery  TEXT DEFAULT '';
BEGIN
   IF v_isMarketing = true then
    
      v_dynamicQuery := CONCAT('SELECT 
									vcDocName AS "vcDocName",
									numGenericDocID AS "numGenericDocID",
									vcSubject AS "vcSubject",
									vcDocDesc AS "vcDocDesc",
									VcFileName AS "VcFileName"
								FROM 
									GenericDocuments
								WHERE 
									numDocCategory = (CASE WHEN ',coalesce(v_numCategoryId,0),
      ' = 0 THEN 369 ELSE ',coalesce(v_numCategoryId,0),
      ' END)
									AND numDomainId = ',v_numDomainID,'  AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''	');
   end if;
    
   IF v_isMarketing = false then
	
		--1 =generic,2=specific
      v_dynamicQuery := CONCAT('SELECT 
										vcDocName AS "vcDocName",
										numGenericDocID AS "numGenericDocID",
										vcSubject AS "vcSubject",
										vcDocDesc AS "vcDocDesc",
										VcFileName AS "VcFileName"
									FROM 
										GenericDocuments
									WHERE 
										numDocCategory = (CASE WHEN ',coalesce(v_numCategoryId,0),' = 0 THEN 369 ELSE ',coalesce(v_numCategoryId,0),
      ' END)
										AND tintDocumentType =1 
										AND numDomainId = ',v_numDomainID);
   end if;

   IF(v_numGroupId > 0) then
	
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || '  AND (' || SUBSTR(CAST(v_numGroupId AS VARCHAR(30)),1,30) || ' IN (SELECT Items FROM Split(ISNULL(vcGroupsPermission,''''),'','')) OR ISNULL(vcGroupsPermission,''0'')=''0'') ';
   end if;

	--PRINT @dynamicQuery
   OPEN SWV_RefCur FOR EXECUTE v_dynamicQuery;
   RETURN;
END; $$;


