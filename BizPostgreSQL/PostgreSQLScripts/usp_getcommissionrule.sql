-- FUNCTION: public.usp_getcommissionrule(numeric, smallint, numeric, refcursor)

-- DROP FUNCTION public.usp_getcommissionrule(numeric, smallint, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcommissionrule(
	v_numcomruleid numeric DEFAULT 0,
	v_bytemode smallint DEFAULT NULL::smallint,
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:50:50 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF v_byteMode = 0 then
        
      open SWV_RefCur for
      SELECT
      numDomainID,
				numComRuleID,
                vcCommissionName,
                coalesce(tinComDuration,0) AS tinComDuration,
                coalesce(tintComBasedOn,0) AS tintComBasedOn,
                coalesce(tintComType,0) AS tintComType,
				coalesce(tintComAppliesTo,0) AS tintComAppliesTo,
				CASE coalesce(tintComAppliesTo,0)
      WHEN 1 THEN(SELECT COUNT(*) FROM CommissionRuleItems CI WHERE CI.numComRuleID = CR.numComRuleID AND CI.tintType = 1)
      WHEN 2 THEN(SELECT COUNT(*) FROM CommissionRuleItems CI WHERE CI.numComRuleID = CR.numComRuleID AND CI.tintType = 2)
      END AS ItemCount,
				coalesce(tintComOrgType,0) AS tintComOrgType,
                CASE WHEN (coalesce(tintComOrgType,0) = 1 OR coalesce(tintComOrgType,0) = 2) THEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID)
      END AS OrganizationCount,
				coalesce(tintAssignTo,0) AS tintAssignTo
      FROM    CommissionRules CR
      WHERE   numComRuleID = v_numComRuleID;
   ELSEIF v_byteMode = 1
   then
            
      open SWV_RefCur for
      SELECT
      PB.numComRuleID,
                    PB.vcCommissionName,
					coalesce(PP.Priority,0) AS Priority,
					PB.tintComBasedOn,
					PB.tinComDuration,
					PB.tintComType,
                    CASE
      WHEN tintComAppliesTo = 3 THEN 'All Items'
      WHEN tintComAppliesTo IN(1,2) THEN GetCommissionContactItemList(PB.numComRuleID,PB.tintComAppliesTo)
      ELSE ''
      END AS ItemList,
                    CASE WHEN tintComOrgType = 3 THEN 'All Customers'
      ELSE GetCommissionContactItemList(PB.numComRuleID,CASE tintComOrgType
         WHEN 1 THEN 3
         WHEN 2 THEN 4
         END::SMALLINT)
      END AS CustomerList,
                    GetCommissionContactItemList(PB.numComRuleID,0::SMALLINT) AS ContactList
      FROM    CommissionRules PB
      LEFT OUTER JOIN PriceBookPriorities PP ON PP.Step2Value = PB.tintComAppliesTo AND PP.Step3Value = PB.tintComOrgType
      WHERE   PB.numDomainID = v_numDomainID
      ORDER BY vcCommissionName ASC;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getcommissionrule(numeric, smallint, numeric, refcursor)
    OWNER TO postgres;
