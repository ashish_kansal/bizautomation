DROP FUNCTION IF EXISTS usp_getaddressdetails;

CREATE OR REPLACE FUNCTION public.usp_getaddressdetails(
	v_tintmode smallint,
	v_numdomainid numeric,
	v_numrecordid numeric,
	v_numaddressid numeric,
	v_tintaddresstype smallint,
	v_tintaddressof smallint,
	INOUT swv_refcur refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF v_tintMode = 1 then
    
      open SWV_RefCur for
      SELECT  numAddressID AS "numAddressID",
                vcAddressName AS "vcAddressName",
                vcStreet AS "vcStreet",
                vcCity AS "vcCity",
                vcPostalCode AS "vcPostalCode",
                numState AS "numState",
                numCountry AS "numCountry",
                bitIsPrimary AS "bitIsPrimary",
				bitResidential AS "bitResidential",
                tintAddressOf AS "tintAddressOf",
                tintAddressType AS "tintAddressType",
                (CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END) || coalesce(vcStreet,'') || '<br/> ' || coalesce(vcCity,'') || ', '
      || coalesce(fn_GetState(numState),'') || ', '
      || coalesce(vcPostalCode,'') || ' <br>'
      || coalesce(fn_GetListItemName(numCountry),'') AS "vcFullAddress"
				,coalesce((SELECT  numWareHouseID FROM Warehouses WHERE numDomainID = v_numDomainID AND numAddressID = AddressDetails.numAddressID LIMIT 1),0) AS "numWarehouseID"
				,coalesce(numContact,0) AS "numContact"
				,coalesce(bitAltContact,false) AS "bitAltContact"
				,coalesce(vcAltContact,'') AS "vcAltContact"
				,(CASE
				  WHEN tintAddressOf = 2
				  THEN coalesce((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE DM.numDomainID = v_numDomainID AND DM.numDivisionID = AddressDetails.numRecordID),'')
				  WHEN tintAddressOf = 1 
				  THEN coalesce((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE ACI.numDomainID = v_numDomainID AND ACI.numContactID = AddressDetails.numRecordID),'')
				  ELSE coalesce((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE ACI.numDomainID = v_numDomainID AND ACI.numContactID = AddressDetails.numRecordID),'')
				  END) AS "vcCompanyName"
				,(CASE
				  WHEN tintAddressOf = 2
				  THEN coalesce((SELECT DM.numDivisionID FROM DivisionMaster DM WHERE DM.numDomainID = v_numDomainID AND DM.numDivisionID = AddressDetails.numRecordID),0)
				  WHEN tintAddressOf = 1 
				  THEN coalesce((SELECT DM.numDivisionID FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID WHERE ACI.numDomainID = v_numDomainID AND ACI.numContactID = AddressDetails.numRecordID),0)
				  ELSE coalesce((SELECT DM.numDivisionID FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID WHERE ACI.numDomainID = v_numDomainID AND ACI.numContactID = AddressDetails.numRecordID),0)
				  END) AS "numDivisionID"
				,numRecordID AS "numRecordID"
      FROM    AddressDetails
      WHERE   numDomainID = v_numDomainID
      AND numAddressID = v_numAddressID;
   end if;
   IF v_tintMode = 2 then
    
      open SWV_RefCur for
      SELECT  numAddressID AS "numAddressID",
                vcAddressName AS "vcAddressName",
				vcStreet AS "vcStreet",
                vcCity AS "vcCity",
                coalesce(vcPostalCode,'') AS "vcPostalCode",
                numState AS "numState",
                numCountry AS "numCountry",
				(CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END) ||  coalesce(vcStreet,'') || '<br /> ' || coalesce(vcCity,'') || ', '
      || coalesce(fn_GetState(numState),'') || ' '
      || coalesce(vcPostalCode,'') || ' <br />'
      || coalesce(fn_GetListItemName(numCountry),'') AS "vcFullAddress"
				,coalesce(numContact,0) AS "numContact"
				,coalesce(bitAltContact,false) AS "bitAltContact"
				,coalesce(vcAltContact,'') AS "vcAltContact"
				,(CASE WHEN v_tintAddressOf = 2 THEN coalesce((SELECT  numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AddressDetails.numRecordID LIMIT 1),0) ELSE 0 END) AS "numPrimaryContact"
				,coalesce(vcStreet || ', ','')  || coalesce(vcCity || ', ','')
      || coalesce(fn_GetState(numState) || ', ','')
      || coalesce(vcPostalCode || ', ','')
      || coalesce(fn_GetListItemName(numCountry),'') AS "FullAddress"
				,(CASE
      WHEN tintAddressOf = 2
      THEN coalesce((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE DM.numDomainID = v_numDomainID AND DM.numDivisionID = AddressDetails.numRecordID),'')
      ELSE coalesce((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE ACI.numDomainID = v_numDomainID AND DM.numDivisionID = AddressDetails.numRecordID),
         '')
      END) AS "vcCompanyName",
	  bitIsPrimary AS "bitIsPrimary"
      FROM    AddressDetails
      WHERE   numDomainID = v_numDomainID
      AND numRecordID = v_numRecordID
      AND tintAddressOf = v_tintAddressOf
	  AND tintaddresstype = v_tintaddresstype
      ORDER BY bitIsPrimary desc;
   end if;
   IF v_tintMode = 3 then
        
      open SWV_RefCur for
      SELECT  numAddressID AS "numAddressID",
                    vcAddressName AS "vcAddressName",
                    vcStreet AS "vcStreet",
                    vcCity AS "vcCity",
                    vcPostalCode AS "vcPostalCode",
                    numState AS "numState",
                    numCountry AS "numCountry",
                    bitIsPrimary AS "bitIsPrimary",
					bitResidential AS "bitResidential",
                    tintAddressOf AS "tintAddressOf",
                    tintAddressType AS "tintAddressType",
                    (CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END) || coalesce(vcStreet,'') || '<br /> ' || coalesce(vcCity,'') || ', '
      || coalesce(fn_GetState(numState),'') || ' '
      || coalesce(vcPostalCode,'') || ' <br />'
      || coalesce(fn_GetListItemName(numCountry),'') AS "vcFullAddress"
      FROM    AddressDetails
      WHERE   numDomainID = v_numDomainID
      AND numRecordID = v_numRecordID
      AND tintAddressOf = v_tintAddressOf
      AND tintAddressType = v_tintAddressType
      AND bitIsPrimary = true
      ORDER BY bitIsPrimary desc;
   end if;

   IF v_tintMode = 4 then
        
      open SWV_RefCur for
      SELECT  numAddressID AS "numAddressID",
                    vcAddressName AS "vcAddressName",
					coalesce(vcPostalCode,'') AS "vcPostalCode"
					,coalesce(numContact,0) AS "numContact"
					,coalesce(bitAltContact,false) AS "bitAltContact"
					,coalesce(vcAltContact,'') AS "vcAltContact"
					,coalesce(vcStreet || ', ','') || coalesce(vcCity || ', ','')
      || coalesce(fn_GetState(numState) || ', ','')
      || coalesce(vcPostalCode || ', ','')
      || coalesce(fn_GetListItemName(numCountry),'') AS "FullAddress"
					, coalesce((SELECT  vcStateCode FROM ShippingStateMaster where vcStateName = coalesce(fn_GetState(numState),'') LIMIT 1),'') AS "StateAbbr"
					, coalesce((SELECT  vcCountryCode from ShippingCountryMaster where vcCountryName = coalesce(fn_GetListItemName(numCountry),'') LIMIT 1),'') AS "CountryAbbr"
      FROM    AddressDetails
      WHERE   numDomainID = v_numDomainID
      AND numRecordID = v_numRecordID	
                    --AND tintAddressOf = @tintAddressOf
                    --AND tintAddressType = @tintAddressType
      AND numAddressID = v_numAddressID
      ORDER BY bitIsPrimary desc;
   end if;

   IF v_tintMode = 5 then
		 open SWV_RefCur for
            SELECT  numAddressID AS "numAddressID",
                    vcAddressName AS "vcAddressName",
                    vcStreet AS "vcStreet",
                    vcCity AS "vcCity",
                    vcPostalCode AS "vcPostalCode",
                    numState AS "numState",
                    numCountry AS "numCountry",
                    bitIsPrimary AS "bitIsPrimary",
					bitResidential AS "bitResidential",
                    tintAddressOf AS "tintAddressOf",
                    tintAddressType AS "tintAddressType",
                    (CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END) || COALESCE(vcStreet,'') || '<br /> ' || COALESCE(vcCity,'') || ', '
                            || COALESCE(fn_GetState(numState),'') || ' '
                            || COALESCE(vcPostalCode,'') || ' <br />'
                            || COALESCE(fn_GetListItemName(numCountry),'') AS "vcFullAddress"
				 ,COALESCE((SELECT numWarehouseID FROM Warehouses WHERE numDomainID=v_numDomainID AND numAddressID=AddressDetails.numAddressID LIMIT 1),0) AS "numWarehouseID"
				 ,COALESCE(numContact,0) AS "numContact"
				 ,COALESCE(bitAltContact,false) AS "bitAltContact"
				 ,COALESCE(vcAltContact,'') AS "vcAltContact"
            FROM    AddressDetails
            WHERE   numDomainID = v_numDomainID AND tintAddressType=v_tintAddressType AND bitIsPrimary=1 AND numRecordID=v_numRecordID;
            
   end if;


   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getaddressdetails(smallint, numeric, numeric, numeric, smallint, smallint, refcursor)
    OWNER TO postgres;
