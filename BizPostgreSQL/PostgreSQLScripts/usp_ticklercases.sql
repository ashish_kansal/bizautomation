-- Stored procedure definition script USP_TicklerCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TicklerCases(v_numUserCntID NUMERIC(9,0) DEFAULT null,                          
v_numDomainID NUMERIC(9,0) DEFAULT null,                          
v_startDate TIMESTAMP DEFAULT NULL,                          
v_endDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSelectedEmployess  VARCHAR(100);
   v_numCriteria  INTEGER;

   v_strSql  VARCHAR(8000);
BEGIN
   select   vcSelectedEmployeeCases, numCriteriaCases INTO v_vcSelectedEmployess,v_numCriteria FROM
   TicklerListFilterConfiguration WHERE
   numDomainID = v_numDomainID AND
   numUserCntID = v_numUserCntID;


   v_strSql := 'SELECT     
	C.numCaseId,                          
	AddC.vcFirstName || '' '' || AddC.vcLastName as Contact,
	(CASE 
		WHEN AddC.numPhone <> '''' 
		THEN AddC.numPhone ||(CASE 
								WHEN AddC.numPhoneExtension <> '''' 
								THEN '' - '' || AddC.numPhoneExtension 
								ELSE '''' 
							  END)  
		ELSE '''' 
	END) AS Phone,                      
	AddC.vcEmail,                                                                        
	C.intTargetResolveDate,                           
	C.vcCaseNumber,                           
	Com.vcCompanyName,                          
	C.textSubject ,                                                 
	Div.numTerID,    
	C.numRecOwner,                                               
	fn_GetContactName(C.numAssignedTo) as AssignedTo                         
FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId                            
WHERE  
	C.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' AND 
	C.numStatus <> 136 AND 
	(intTargetResolveDate >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and intTargetResolveDate <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''')';

   IF v_numCriteria  = 1 then --Action Items	Assigned

      v_strSql := coalesce(v_strSql,'') || ' AND C.numAssignedTo IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')';
   ELSEIF v_numCriteria = 2
   then --Action Items Owned

      v_strSql := coalesce(v_strSql,'') || ' AND C.numCreatedBy IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' AND (C.numAssignedTo IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or C.numCreatedBy IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))';
   end if;
                   
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY 
	intTargetResolveDate DESC';

   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


