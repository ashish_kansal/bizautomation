CREATE OR REPLACE FUNCTION USP_GetOpenOrderDetails
(
	v_numDivisionID NUMERIC(9,0) DEFAULT 0
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		cast(SUM(monTotAmount) as VARCHAR(255)) AS TotalPOAmount
		,cast(SUM(numUnitHOur) as VARCHAR(255)) AS TotalQty
		,cast(SUM(coalesce(I.fltWeight,0)*coalesce(numUnitHOur,0)) as VARCHAR(255)) AS TotalWeight
		,COALESCE((select string_agg(' <b>Buy</b> ' || CASE WHEN u.intType = 1 THEN '$' || CAST(u.vcBuyingQty AS TEXT) ELSE CAST(u.vcBuyingQty AS TEXT) END || ' ' || CASE WHEN u.intType = 1 THEN 'Amount' WHEN u.intType = 2 THEN 'Quantity' WHEN u.intType = 3 THEN 'Lbs' ELSE '' END || ' <b>Get</b> ' ||(CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END),'<br/>' order by u.numPurchaseIncentiveId) 
				from 
					PurchaseIncentives u
				where 
					u.numPurchaseIncentiveId = numPurchaseIncentiveId 
					AND numDivisionId = v_numDivisionID
				),'') AS Purchaseincentives
	FROM 
		OpportunityItems as OPI 
	LEFT JOIN
		Item AS I 
	ON 
		OPI.numItemCode = I.numItemCode 
	WHERE
		numOppId IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionId = v_numDivisionID AND bintClosedDate IS NULL);
END; $$;











