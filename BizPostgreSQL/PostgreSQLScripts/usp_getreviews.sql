-- Stored procedure definition script USP_GetReviews for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReviews(v_numReviewId NUMERIC(18,0) DEFAULT 0, 
v_numDomainId NUMERIC(18,0) DEFAULT NULL ,
v_numSiteId NUMERIC(18,0) DEFAULT NULL ,      
v_vcReferenceId VARCHAR(100) DEFAULT NULL,
v_tintTypeId INTEGER DEFAULT NULL ,
v_Mode INTEGER DEFAULT NULL ,
v_numContactId NUMERIC(18,0) DEFAULT NULL ,
v_SearchMode INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
 
	  --Check where is mode 4 used
	  
	  -- Mode 6 used in Permalink Page...Review get by ReviewId 
	  -- Mode 6 Usage :  And also used when was this review Helpful on cliend side clicked....
   IF v_Mode = 1 then
      v_strSql :=
      'SELECT  
	         * ,
	        (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  AND bitHelpful = true) AS vcHelpfulVote ,
	        (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId) AS vcTotalVote ,
            (SELECT(vcFirstName || '' '' || vcLastName) FROM AdditionalContactsInformation WHERE numContactId = R.numContactId LIMIT 1) AS vcContactName 
             FROM Review AS R  
             WHERE      numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(100)),1,100)  || '   
                    AND numSiteId  =  ' || SUBSTR(CAST(v_numSiteId AS VARCHAR(100)),1,100)  || '  
                    AND vcReferenceId = ''' || SUBSTR(CAST(v_vcReferenceId AS VARCHAR(100)),1,100)  || '''
                    AND tintTypeId = ' || SUBSTR(CAST(v_tintTypeId AS VARCHAR(100)),1,100)  || ' 
                    AND bitHide = false';
      IF (v_SearchMode = 0) then
				
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY dtCreatedDate desc';
      ELSE
         v_strSql := coalesce(v_strSql,'') || 'ORDER BY vcHelpfulVote desc';
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   ELSEIF v_Mode = 2
   then --To view List on Admin side and show was this review helpful on client side
      
      open SWV_RefCur for
      SELECT
      numReviewId ,
	  	    (SELECT(vcFirstName || ' ' || vcFirstName) FROM AdditionalContactsInformation WHERE numContactId = R.numContactId LIMIT 1) AS vcContactName ,
	  	      (CASE
      WHEN LENGTH(vcReviewTitle) > 25
      then SUBSTR(vcReviewTitle,0,25) || ' ' ||  '[...]'
      ELSE
         vcReviewTitle
      END) AS vcReviewTitle  ,
	  	      (CASE
      WHEN LENGTH(vcReviewComment) > 25
      then SUBSTR(vcReviewComment,0,25) || ' ' ||  '[...]'
      ELSE
         vcReviewComment
      END) AS vcReviewComment  ,
	  	     bitEmailMe ,
	  	     bitHide ,
	  	     vcIpaddress ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND coalesce(bitHelpful,false) <> false) AS vcTotalHelpful  ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND bitHelpful = true) AS vcHelpful  ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND bitAbuse = true) AS vcTotalAbuse  ,
	  	     dtCreatedDate
      FROM Review R WHERE
      numDomainID = v_numDomainId
      AND numSiteId  = v_numSiteId
      AND tintTypeId = v_tintTypeId;
   ELSEIF v_Mode = 3
   then --To view one record on admin side when Edit made 
	  
      open SWV_RefCur for
      SELECT * ,
	  	    (CASE
      WHEN tintTypeId = 1
      then  'Category'
      When tintTypeId = 2
      THEN 'Product'
      When tintTypeId = 3
      THEN 'URL'
      ELSE
         vcReviewComment
      END) AS vcReviewType
      FROM Review
      WHERE        numReviewId = v_numReviewId
      AND  numDomainID = v_numDomainId
      AND numSiteId  = v_numSiteId;
   ELSEIF v_Mode = 4
   then
	  
      open SWV_RefCur for
      SELECT * FROM Review
      WHERE numContactId = v_numContactId AND numReviewId = v_numReviewId;
   ELSEIF v_Mode = 5
   then
	  
      open SWV_RefCur for
      SELECT COUNT(*) AS intReviewCount FROM Review
      WHERE
      numDomainID = v_numDomainId
      AND numSiteId  = v_numSiteId
      AND tintTypeId = v_tintTypeId
      AND vcReferenceId = v_vcReferenceId
      AND bitHide = 0;
   ELSEIF v_Mode = 6
   then
	  
      open SWV_RefCur for
      SELECT * ,
	         (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  AND bitHelpful = true) AS vcHelpfulVote ,
	         (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId) AS vcTotalVote ,
            (SELECT(vcFirstName || ' ' || vcLastname) FROM AdditionalContactsInformation WHERE numContactId = R.numContactId LIMIT 1) AS vcContactName
      FROM Review AS R
      WHERE
      bitHide = 0 AND
      numReviewId = v_numReviewId;
   end if;
   RETURN;
END; $$;

-- select * from Review
--SELECT * FROM ReviewDetail
--INSERT INTO ReviewDetail
--VALUES(3,1,NULL,1)
--UPDATE ReviewDetail SET bitHelpful = 1 WHERE numReviewDetailId = 1
--SELECT * FROM ReviewDetail
--SELECT * FROM Ratings


/****** Object:  StoredProcedure [dbo].[USP_GetSalesFulfillment]    Script Date: 05/15/2009 08:05:02 ******/



