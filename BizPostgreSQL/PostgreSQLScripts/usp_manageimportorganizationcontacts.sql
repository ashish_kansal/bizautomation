-- Stored procedure definition script USP_ManageImportOrganizationContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageImportOrganizationContacts(v_numDomainID NUMERIC,
    INOUT v_numCompanyID NUMERIC ,
    INOUT v_numDivisionID NUMERIC ,
    v_numNonBizCompanyID VARCHAR(50),
    v_tintMode SMALLINT DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
            
      INSERT  INTO ImportOrganizationContactReference(numDomainID,
                          numNonBizCompanyID,
                          numCompanyID,
                          numDivisionID)
                VALUES(v_numDomainID,
                          v_numNonBizCompanyID,
                          v_numCompanyID,
                          v_numDivisionID);
   end if;
   IF v_tintMode = 1 then
            
      select   numCompanyID, numDivisionID INTO v_numCompanyID,v_numDivisionID FROM    ImportOrganizationContactReference WHERE   numNonBizCompanyID = v_numNonBizCompanyID
      AND numDomainID = v_numDomainID    LIMIT 1;
   end if;
   RETURN;
END; $$;


