-- Stored procedure definition script USP_AddMoneyOutBill for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddMoneyOutBill(v_numPaymentMethod NUMERIC(9,0),
    v_monAmount DECIMAL(20,5),
    v_vcReference VARCHAR(200),
    v_vcMemo VARCHAR(50),
    v_numExpenseAccount NUMERIC(9,0),
    v_numDivisionID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_UserContactId NUMERIC(9,0),
    v_dtDueDate TIMESTAMP,
    v_numCurrencyID NUMERIC(9,0),
    INOUT v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0 ,
    INOUT v_fltExchangeRate DOUBLE PRECISION DEFAULT 0 ,
    v_numEmbeddedCostID NUMERIC(9,0) DEFAULT 0, --optional , will be used only when posting bill for embedded cost
    v_tintPaymentType SMALLINT DEFAULT 2,--2 for regular bill, 4 for bill against liability account
    v_numLiabilityAccount NUMERIC(9,0) DEFAULT NULL,
    v_numCommissionID NUMERIC(9,0) DEFAULT 0,
    v_numOppBizDocID NUMERIC(9,0) DEFAULT 0,
    v_numClassID NUMERIC(9,0) DEFAULT NULL,
    v_numProjectID NUMERIC(9,0) DEFAULT NULL,
	v_numContactID NUMERIC(9,0) DEFAULT NULL,
	v_numTermsID		NUMERIC(18,0) DEFAULT NULL,
	v_dtBillDate		TIMESTAMP DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
 

--IF  @tintPaymentType=5
--BEGIn
--	Select @numCurrencyID=isnull(OPP.numCurrencyID,0) From OpportunityMaster OPP
--			join OpportunityBizDocs oppBizDoc on  oppBizDoc.numOppId=OPP.numOppId
--			Where numOppBizDocsId=@numOppBizDocID and  OPP.numDomainID=@numDomainID  
--END   

   if v_numCurrencyID = 0 then
      v_fltExchangeRate := 1;
   else
      v_fltExchangeRate := GetExchangeRate(v_numDomainID,v_numCurrencyID);
   end if;
     
	

   Insert  into OpportunityBizDocsDetails(numBizDocsId,
              numPaymentMethod,
              monAmount,
              numDomainId,
              vcReference,
              vcMemo,
              bitIntegratedToAcnt,
              bitAuthoritativeBizDocs,
              bitDeferredIncome,
              numCreatedBy,
              dtCreationDate,
              numExpenseAccount,
              numDivisionID,
              numCurrencyID,
              fltExchangeRate,
              tintPaymentType,
              numLiabilityAccount,
              numCommissionID,
              numClassID,
              numProjectID,numContactID,numTermsID,dtBillDate)
    Values(v_numOppBizDocID,
              v_numPaymentMethod,
              v_monAmount,
              v_numDomainID,
              v_vcReference,
              v_vcMemo,
              0,
              1,
              0,
              v_UserContactId,
              TIMEZONE('UTC',now()),
              v_numExpenseAccount,
              v_numDivisionID,
              v_numCurrencyID,
              v_fltExchangeRate,
              v_tintPaymentType,
              v_numLiabilityAccount,
              v_numCommissionID,
              v_numClassID,
              v_numProjectID,v_numContactID,v_numTermsID,v_dtBillDate);

    
   v_numBizDocsPaymentDetId := CURRVAL('OpportunityBizDocsDetails_seq');

   Insert  into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,
              monAmount,
              dtDueDate,
              bitIntegrated)
    Values(v_numBizDocsPaymentDetId,
              v_monAmount,
              v_dtDueDate,
              0); 
	-- will be used only when posting bill for embedded cost            
	
   IF v_numEmbeddedCostID > 0 then
	
      UPDATE EmbeddedCost SET numBizDocsPaymentDetId = v_numBizDocsPaymentDetId WHERE numEmbeddedCostID = v_numEmbeddedCostID;
   end if;


--Payroll : Update BizDocComission,opportunitybizdocsdetails and OpportunityBizDocsPaymentDetails
   If v_tintPaymentType = 6 then

--Update OpportunityBizDocsPaymentDetails set bitIntegrated=1 where bitIntegrated=0 and 
--		numBizDocsPaymentDetId in (select numBizDocsPaymentDetId from opportunitybizdocsdetails where 
--			numDomainId = @numDomainId AND bitAuthoritativeBizDocs = 1	
--			and numContactID=@numContactID and bitIntegratedToAcnt=0 and tintPaymentType = 5 and numBizDocsId > 0)

      Update BizDocComission BC set bitCommisionPaid = true,numBizDocsPaymentDetId = v_numBizDocsPaymentDetId
      FROM OpportunityMaster Opp
      where(BC.numOppBizDocId = oppBiz.numOppBizDocsId and BC.numUserCntID = v_numContactID) AND(Opp.numDomainId = v_numDomainID and
      BC.numUserCntID = v_numContactID
      AND BC.bitCommisionPaid = false --added by chintan BugID 982
      AND oppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount);
   end if;
   RETURN;
END; $$;


