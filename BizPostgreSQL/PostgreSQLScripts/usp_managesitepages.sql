
CREATE OR REPLACE FUNCTION USP_ManageSitePages(v_numPageID     NUMERIC(9,0)  DEFAULT 0,
          v_vcPageName    VARCHAR(50) DEFAULT NULL,
          v_vcPageURL     VARCHAR(1000) DEFAULT NULL,
          v_tintPageType  SMALLINT DEFAULT NULL,
          v_vcPageTitle   VARCHAR(1000) DEFAULT NULL,
          v_numTemplateID NUMERIC(9,0) DEFAULT NULL,
          v_numSiteID     NUMERIC(9,0) DEFAULT NULL,
          v_numDomainID   NUMERIC(9,0) DEFAULT NULL,
          v_numUserCntID  NUMERIC(9,0) DEFAULT NULL,
          v_strItems      XML  DEFAULT NULL,
          v_bitIsActive   BOOLEAN DEFAULT NULL,
          v_IsMaintainScroll BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN

   IF v_numPageID = 0
   AND (NOT EXISTS(SELECT vcPageName
   FROM   SitePages
   WHERE  numSiteID = v_numSiteID
   AND (vcPageName = v_vcPageName
   OR vcPageURL = v_vcPageURL))) then
    
      INSERT INTO SitePages(vcPageName,
                  vcPageURL,
                  tintPageType,
                  vcPageTitle,
                  numTemplateID,
                  numSiteID,
                  numDomainID,
                  numCreatedBy,
                  dtCreateDate,
                  numModifiedBy,
                  dtModifiedDate,
                  bitIsActive,
                  bitIsMaintainScroll)
      VALUES(v_vcPageName,
                  v_vcPageURL,
                  v_tintPageType,
                  v_vcPageTitle,
                  v_numTemplateID,
                  v_numSiteID,
                  v_numDomainID,
                  v_numUserCntID,
                  TIMEZONE('UTC',now()),
                  v_numUserCntID,
                  TIMEZONE('UTC',now()),
                  v_bitIsActive,
                  v_IsMaintainScroll);
      
      v_numPageID := CURRVAL('SitePages_seq');
      open SWV_RefCur for
      SELECT v_numPageID;
   ELSE
      UPDATE SitePages
      SET    vcPageName = v_vcPageName,vcPageURL = v_vcPageURL,tintPageType = v_tintPageType,
      vcPageTitle = v_vcPageTitle,numTemplateID = v_numTemplateID,
      numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
      bitIsActive = v_bitIsActive,bitIsMaintainScroll = v_IsMaintainScroll
      WHERE  numPageID = v_numPageID
      AND numSiteID = v_numSiteID
      AND numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT v_numPageID;
   end if;
   DELETE FROM StyleSheetDetails
   WHERE       numPageID = v_numPageID;
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      INSERT INTO StyleSheetDetails(numPageID,
                  numCssID)
      SELECT v_numPageID,
             X.numCssID
      FROM
	  XMLTABLE
	(
		'NewDataSet/Item'
        PASSING 
			v_strItems 
		COLUMNS
			id FOR ORDINALITY,
			numCssID NUMERIC PATH 'numCssID'
	) X;
	  
   end if;

/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
   RETURN;
END; $$;


