-- Stored procedure definition script USP_ManageEmailRights for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEmailRights(v_numContactID NUMERIC(9,0),  
v_strUsers TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete from emailusers where numContactID = v_numContactID;    
      
   insert into emailusers(numContactID,numUserCntID)
	select 
		v_numContactID,X.UserID 
	from
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strUsers AS XML)
			COLUMNS
				id FOR ORDINALITY,
				UserID NUMERIC(9,0) PATH 'UserID'
		) AS X;  
      
   RETURN;
END; $$;


