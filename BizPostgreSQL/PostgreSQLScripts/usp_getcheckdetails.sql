-- Stored procedure definition script USP_GetCheckDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckDetails(v_numCheckId NUMERIC(9,0) DEFAULT 0,      
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From CheckDetails CD
   Left outer join DivisionMaster DM on CD.numCustomerId = DM.numDivisionID
   Left outer join CompanyInfo CI on DM.numCompanyID = CI.numCompanyId
   Where CD.numCheckId = v_numCheckId And CD.numDomainId = v_numDomainId;
END; $$;












