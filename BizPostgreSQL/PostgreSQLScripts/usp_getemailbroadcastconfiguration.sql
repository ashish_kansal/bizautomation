-- Stored procedure definition script usp_GetEmailBroadcastConfiguration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION usp_GetEmailBroadcastConfiguration(v_numDomainID NUMERIC(18,0),
	v_numConfigurationID NUMERIC(18,0) , 
	v_intMode INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intMode = 1 then
	
      open SWV_RefCur for
      SELECT * FROM EmailBroadcastConfiguration WHERE numDomainID = v_numDomainID;
   ELSEIF v_intMode = 2
   then
    
      open SWV_RefCur for
      SELECT * FROM EmailBroadcastConfiguration WHERE numConfigurationID = v_numConfigurationID;
   ELSEIF v_intMode = 3
   then
    
      open SWV_RefCur for
      SELECT * FROM EmailBroadcastConfiguration WHERE numDomainID = v_numDomainID And numConfigurationID <> v_numConfigurationID;
   end if;
   RETURN;
END; $$;	
	


