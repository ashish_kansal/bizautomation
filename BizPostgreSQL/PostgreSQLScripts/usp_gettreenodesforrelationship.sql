-- Stored procedure definition script USP_GetTreeNodesForRelationship for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE OR REPLACE FUNCTION USP_GetTreeNodesForRelationship(v_numGroupID NUMERIC(18,0),
	  v_numTabID NUMERIC(18,0),
      v_numDomainID NUMERIC(18,0),
	  v_bitVisibleOnly BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numModuleID  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;
   v_oldParentID  INTEGER;
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
select   numModuleID INTO v_numModuleID FROM    PageNavigationDTL WHERE   numTabID = v_numTabID    LIMIT 1; 
 

   DROP TABLE IF EXISTS tt_RELATIONSHIPTREE CASCADE;
   CREATE TEMPORARY TABLE tt_RELATIONSHIPTREE ON COMMIT DROP AS
      SELECT
      ROW_NUMBER() OVER(ORDER BY numOrder) AS ID,
	* FROM(SELECT
      PND.numPageNavID AS numPageNavID,
		null AS numListItemID,
		coalesce(TNA.bitVisible,true) AS bitVisible,
		coalesce(PND.vcPageNavName,'') AS vcNodeName,
		coalesce(vcNavURL,'') AS vcNavURL,
        vcImageURL,
		coalesce(TNA.tintType,1) AS tintType,
		PND.numParentID AS numParentID,
		PND.numParentID AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,1000) AS numOrder,
		0 AS isUpdated
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      TNA.numPageNavID = PND.numPageNavID AND
      TNA.numDomainID = v_numDomainID AND
      TNA.numGroupID = v_numGroupID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID = PND.numPageNavID
      WHERE
      1 = 1
      AND coalesce(PND.bitVisible,false) = true
      AND numModuleID = v_numModuleID
      UNION
      SELECT
      null AS numPageNavID,
		Listdetails.numListItemID,
		coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
		Listdetails.vcData AS vcNodeName,
		'../prospects/frmCompanyList.aspx?RelId=' || CAST(Listdetails.numListItemID AS VARCHAR(10)) AS vcNavURL,
		'' as vcImageURL,
		1 AS tintType,
		6 AS numParentID,
		6 AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,2000) AS numOrder,
		0 AS isUpdated
      FROM
      Listdetails
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = Listdetails.numListItemID AND
      TreeNodeOrder.numParentID = 6 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      numListID = 5 AND
		(Listdetails.numDomainid = v_numDomainID  OR constFlag = true) AND
		(coalesce(bitDelete,false) = false OR constFlag = true) AND
      Listdetails.numListItemID <> 46
      UNION
      SELECT
      null AS numPageNavID,
		L2.numListItemID,
		coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
		L2.vcData AS vcNodeName,
		'../prospects/frmCompanyList.aspx?RelId='
      || CAST(FRDTL.numPrimaryListItemID AS VARCHAR(10)) || '&profileid='
      || CAST(numSecondaryListItemID AS VARCHAR(10)) as vcNavURL,
        '' as vcImageURL,
		1 AS tintType,
		FRDTL.numPrimaryListItemID AS numParentID,
		FRDTL.numPrimaryListItemID AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,3000) AS numOrder,
		0 AS isUpdated
      FROM
      FieldRelationship FR
      join
      FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
      join
      Listdetails L1 on numPrimaryListItemID = L1.numListItemID
      join
      Listdetails L2 on numSecondaryListItemID = L2.numListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      numPrimaryListItemID <> 46
      and FR.numPrimaryListID = 5
      and FR.numSecondaryListID = 21
      and FR.numDomainID = v_numDomainID
      and L2.numDomainid = v_numDomainID
      UNION
      SELECT
      null AS numPageNavID,
		L2.numListItemID,
		coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
		L2.vcData AS vcNodeName,
		'../prospects/frmProspectList.aspx?numProfile=' || CAST(L2.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '' AS vcImageURL,
		1 AS tintType,
		11 AS numParentID,
		11 AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,4000) AS numOrder,
		0 AS isUpdated
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 11 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE    FRD.numPrimaryListItemID = 46 
      AND  FR.numDomainID = v_numDomainID 
      UNION
      SELECT
      null AS numPageNavID,
		L2.numListItemID,
		coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
		L2.vcData AS vcNodeName,
		'../account/frmAccountList.aspx?numProfile=' || CAST(L2.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '' AS vcImageURL,
		1 AS tintType,
		12 AS numParentID,
		12 AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,5000) AS numOrder,
		0 AS isUpdated
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 12 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE    FRD.numPrimaryListItemID = 46 
      AND  FR.numDomainID = v_numDomainID 
      UNION    
	--Select Contact
      SELECT
      null AS numPageNavID,
		Listdetails.numListItemID,
		coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
		Listdetails.vcData AS vcNodeName,
		'../contact/frmContactList.aspx?ContactType=' || CAST(Listdetails.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '' AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		coalesce(TreeNodeOrder.numOrder,6000) AS numOrder,
		0 AS isUpdated
      FROM
      Listdetails
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = Listdetails.numListItemID AND
      TreeNodeOrder.numParentID = 13 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE   numListID = 8
      AND (constFlag = true
      OR Listdetails.numDomainid = v_numDomainID)
      UNION
      SELECT
      null AS numPageNavID,
		101 AS numListItemID,
		coalesce((SELECT
         bitVisible
         FROM
         TreeNodeOrder
         WHERE
         TreeNodeOrder.numListItemID = 101 AND
         TreeNodeOrder.numParentID = 13 AND
         TreeNodeOrder.numTabID = v_numTabID AND
         TreeNodeOrder.numDomainID = v_numDomainID AND
         TreeNodeOrder.numGroupID = v_numGroupID AND
         TreeNodeOrder.numPageNavID IS NULL),true) AS bitVisible,
		'Primary Contact' AS vcNodeName,
		'../Contact/frmcontactList.aspx?ContactType=101',
        '' AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		coalesce((SELECT
         numOrder
         FROM
         TreeNodeOrder
         WHERE
         TreeNodeOrder.numListItemID = 101 AND
         TreeNodeOrder.numParentID = 13 AND
         TreeNodeOrder.numTabID = v_numTabID AND
         TreeNodeOrder.numDomainID = v_numDomainID AND
         TreeNodeOrder.numGroupID = v_numGroupID AND
         TreeNodeOrder.numPageNavID IS NULL),7000) AS numOrder,
		 0 AS isUpdated) AS TEMP
      ORDER BY
      TEMP.numOrder;

   select   COUNT(ID) INTO v_Count FROM tt_RELATIONSHIPTREE;

   WHILE v_i <= v_Count LOOP
      IF(SELECT coalesce(numPageNavID,0) FROM tt_RELATIONSHIPTREE WHERE ID = v_i) <> 0 then
         select   numPageNavID INTO v_oldParentID FROM tt_RELATIONSHIPTREE WHERE ID = v_i;
      ELSE
         select   numListItemID INTO v_oldParentID FROM tt_RELATIONSHIPTREE WHERE ID = v_i;
      end if;
      UPDATE tt_RELATIONSHIPTREE SET numParentID = v_i,isUpdated = 1 WHERE numParentID = v_oldParentID AND isUpdated = 0;
      v_i := v_i::bigint+1;
   END LOOP;


   IF v_bitVisibleOnly = true then
      open SWV_RefCur for
      SELECT
      CAST(ID AS INTEGER) As ID,
			numPageNavID,
			numListItemID,
			bitVisible,
			vcNodeName,
			vcNavURL,
			vcImageURL,
			tintType,
			(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INTEGER) END) AS numParentID,
			numOriginalParentID,
			numOrder
      FROM
      tt_RELATIONSHIPTREE
      WHERE
      bitVisible = true
      AND (numParentID IN(SELECT ID FROM tt_RELATIONSHIPTREE WHERE bitVisible = true) OR numParentID = 0);
   ELSE
      open SWV_RefCur for
      SELECT CAST(ID AS INTEGER) As ID,numPageNavID,numListItemID,bitVisible,vcNodeName,tintType,(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INTEGER) END) AS numParentID,numOriginalParentID, numOrder FROM tt_RELATIONSHIPTREE WHERE numParentID IN(SELECT ID FROM tt_RELATIONSHIPTREE) OR coalesce(numParentID,0) = 0;
   end if;
	
   RETURN;
END; $$;


