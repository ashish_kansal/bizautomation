CREATE OR REPLACE FUNCTION USP_DeleteItem
(
	v_ItemCode NUMERIC(9,0),
	v_numDomianID NUMERIC(9,0)
)
RETURNS VOID LANGUAGE plpgsql  
AS $$
   DECLARE
     my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_error INTEGER;
BEGIN
   SWV_error := 0;
   BEGIN

IF(SELECT COUNT(*) FROM OpportunityItems OI JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      WHERE OM.numDomainId = v_numDomianID AND numItemCode = v_ItemCode) > 0 then
 	 
         RAISE EXCEPTION 'OpportunityItems_Depend';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM OpportunityKitItems WHERE numChildItemID = v_ItemCode) > 0 then
 	 
         RAISE EXCEPTION 'OpportunityKitItems_Depend';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM WorkOrder WHERE numItemCode = v_ItemCode) > 0 then
 	
         RAISE EXCEPTION 'WorkOrder_Depend';
         RETURN;
      end if;
 --  numeric(18, 0)
	 --  tinyint
      IF(SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = v_ItemCode) > 0 then
 	 
         RAISE EXCEPTION 'KitItems_Depend';
         RETURN;
      end if;
      PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID := v_numDomianID,v_tintRecordType := 1::SMALLINT,v_numRecordID := v_ItemCode); --  numeric(18, 0)

      DELETE FROM AssembledItem WHERE numDomainID = v_numDomianID AND numItemCode = v_ItemCode;
      DELETE FROM AssembledItem WHERE numDomainID = v_numDomianID AND ID IN(SELECT coalesce(numAssembledItemID,0) FROM AssembledItemChilds WHERE numItemCode = v_ItemCode);
      DELETE FROM OpportunityItems WHERE numItemCode = v_ItemCode AND numOppId IS null;
      DELETE FROM ItemImages WHERE numItemCode = v_ItemCode AND numDomainId = v_numDomianID;
      DELETE FROM RecentItems WHERE numRecordID = v_ItemCode AND chrRecordType = 'I';
      DELETE FROM SimilarItems WHERE numParentItemCode = v_ItemCode AND numDomainId = v_numDomianID;
      delete from CompanyAssets where numItemCode = v_ItemCode;
      DELETE FROM Vendor WHERE numItemCode = v_ItemCode;
      DELETE FROM ItemDiscountProfile WHERE numItemID = v_ItemCode;
      DELETE FROM ItemDetails WHERE numItemKitID = v_ItemCode;
      DELETE FROM ItemDetails WHERE numChildItemID = v_ItemCode;
      DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID IN(SELECT numWareHouseItemID FROM WareHouseItems
      where numItemID = v_ItemCode);
      DELETE FROM WareHouseItmsDTL WHERE numWareHouseItemID IN(SELECT numWareHouseItemID FROM WareHouseItems
      where numItemID = v_ItemCode);
      delete from WareHouseItems
      where numItemID = v_ItemCode;
      DELETE FROM ItemCategory WHERE numItemID = v_ItemCode;
--Delete all journal related to item
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP AS
         SELECT numJournalId  FROM General_Journal_Details WHERE numDomainId = v_numDomianID AND numItemID = v_ItemCode;
----SELECT * FROM #temp
      DELETE FROM General_Journal_Details WHERE numDomainId = v_numDomianID AND numJournalId IN(SELECT numJournalId FROM tt_TEMP);
      DELETE FROM General_Journal_Header WHERE numDomainId = v_numDomianID AND numJOurnal_Id IN(SELECT numJournalId FROM tt_TEMP);
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      delete from Item where numItemCode = v_ItemCode AND numDomainID = v_numDomianID;
      DELETE FROM ItemAPI WHERE numItemID = v_ItemCode AND numDomainId = v_numDomianID;
--For Lucene index 
      UPDATE LuceneItemsIndex SET bitItemDeleted = true WHERE numItemCode = v_ItemCode AND numDomainID = v_numDomianID;

EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
         IF SWV_error <> '0' then
            NULL;
            -- ROLLBACK
end if;
         SWV_error := 0;
   END;
END; $$; 


