-- Stored procedure definition script USP_CFWDtlFieldDelete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CFWDtlFieldDelete(v_numFieldDtlID NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from CFW_Fld_Dtl where numFieldDtlID = v_numFieldDtlID;
   RETURN;
END; $$;


