-- Stored procedure definition script USP_StagePercentageDetailsTaskNotes_GetByTaskID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTaskNotes_GetByTaskID(v_numTaskID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		ID AS "ID"
		,numTaskID AS "numTaskID"
		,vcNotes AS "vcNotes"
		,bitDone AS "bitDone" 
	FROM 
		StagePercentageDetailsTaskNotes 
	WHERE 
		numTaskID = v_numTaskID;
END; $$;












