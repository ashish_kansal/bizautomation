-- Stored procedure definition script USP_HelpDisplayOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_HelpDisplayOrder(v_strItems TEXT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      UPDATE HelpMaster SET intDisplayOrder = X.intDisplayOrder
      FROM 
	  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numHelpID NUMERIC(9,0) PATH 'numHelpID',
					intDisplayOrder INTEGER PATH 'intDisplayOrder'
			) AS X
      WHERE HelpMaster.numHelpID = X.numHelpID;
   end if;
   RETURN;
END; $$;



