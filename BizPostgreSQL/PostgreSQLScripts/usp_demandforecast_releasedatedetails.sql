-- Stored procedure definition script USP_DemandForecast_ReleaseDateDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DemandForecast_ReleaseDateDetails(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_dtReleaseDate DATE
	,v_CurrentPage INTEGER
    ,v_PageSize INTEGER
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numOppID NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      vcCompanyName VARCHAR(300),
      numWOID NUMERIC(18,0),
      dtCreatedDate TIMESTAMP,
      vcOppName VARCHAR(500),
      numOrderedQty DOUBLE PRECISION,
      numQtyToShip DOUBLE PRECISION,
      dtReleaseDate VARCHAR(50)
   );
   INSERT INTO tt_TEMP(numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate)
   SELECT
   OpportunityMaster.numOppId
		,OpportunityMaster.numDivisionId
		,coalesce(CompanyInfo.vcCompanyName,'')
		,0
		,OpportunityMaster.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OpportunityMaster.numOppId,
   ');">',OpportunityMaster.vcpOppName,'</a>')
		,OpportunityItems.numUnitHour
		,OpportunityItems.numUnitHour -coalesce(numQtyShipped,0)
		,CAST(FormatedDateFromDate(coalesce(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
   FROM
   OpportunityMaster
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.tintopptype = 1
   AND OpportunityMaster.tintoppstatus = 1
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   AND(coalesce(OpportunityItems.numUnitHour,0)  -coalesce(OpportunityItems.numQtyShipped,0)) > 0
   AND coalesce(OpportunityItems.bitDropShip,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) = false
   AND coalesce(Item.bitKitParent,false) = false
   AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
   AND OpportunityItems.numItemCode = v_numItemCode
   AND OpportunityItems.numWarehouseItmsID = v_numWarehouseItemID
   AND coalesce(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) = v_dtReleaseDate;

	-- USED IN KIT
   INSERT INTO tt_TEMP(numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate)
   SELECT
   OM.numOppId
		,OM.numDivisionId
		,coalesce(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcpOppName,
   ' (Kit Memeber)','</a>')
		,OKI.numQtyItemsReq
		,OKI.numQtyItemsReq -coalesce(OKI.numQtyShipped,0)
		,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster
   ON
   OM.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OI.numoppitemtCode = OKI.numOppItemID
   INNER JOIN
   Item
   ON
   OKI.numChildItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKI.numWareHouseItemId = WI.numWareHouseItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OI.bitWorkOrder,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) = false
   AND coalesce(Item.bitKitParent,false) = false
   AND(coalesce(OKI.numQtyItemsReq,0) -coalesce(OKI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND OKI.numChildItemID = v_numItemCode
   AND OKI.numWareHouseItemId = v_numWarehouseItemID
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) = v_dtReleaseDate;

	-- USED IN KIT WITHIN KIT
   INSERT INTO tt_TEMP(numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate)
   SELECT
   OM.numOppId
		,OM.numDivisionId
		,coalesce(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcpOppName,
   ' (Kit Memeber)','</a>')
		,OKCI.numQtyItemsReq
		,OKCI.numQtyItemsReq -coalesce(OKCI.numQtyShipped,0)
		,CAST(FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) AS VARCHAR(50))
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster
   ON
   OM.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitChildItems OKCI
   ON
   OI.numoppitemtCode = OKCI.numOppItemID
   INNER JOIN
   Item
   ON
   OKCI.numItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKCI.numWareHouseItemId = WI.numWareHouseItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OI.bitWorkOrder,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) = false
   AND coalesce(Item.bitKitParent,false) = false
   AND(coalesce(OKCI.numQtyItemsReq,0) -coalesce(OKCI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND OKCI.numItemID = v_numItemCode
   AND OKCI.numWareHouseItemId = v_numWarehouseItemID
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) = v_dtReleaseDate;
		
	-- USED IN WORK ORDER
   INSERT INTO tt_TEMP(numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate)
   SELECT
   OM.numOppId
		,coalesce(OM.numDivisionId,0)
		,coalesce(CompanyInfo.vcCompanyName,'')
		,WO.numWOId
		,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
		,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcpOppName ELSE 'Work Order' END,' (Assembly Memeber)')
		,WOD.numQtyItemsReq
		,WOD.numQtyItemsReq
		,CAST(CASE WHEN OM.numOppId IS NOT NULL THEN FormatedDateFromDate(coalesce(OI.ItemReleaseDate,OM.dtReleaseDate),v_numDomainID) ELSE FormatedDateFromDate(WO.dtmEndDate,v_numDomainID) END AS VARCHAR(50))
   FROM
   WorkOrderDetails WOD
   INNER JOIN
   Item
   ON
   WOD.numChildItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   WOD.numWarehouseItemID = WI.numWareHouseItemID
   INNER JOIN
   WorkOrder WO
   ON
   WOD.numWOId = WO.numWOId
   LEFT JOIN
   OpportunityItems OI
   ON
   WO.numOppItemID = OI.numoppitemtCode
   LEFT JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   LEFT JOIN
   DivisionMaster
   ON
   OM.numDivisionId = DivisionMaster.numDivisionID
   LEFT JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   WO.numDomainId = v_numDomainID
   AND WO.numWOStatus <> 23184 -- NOT COMPLETED
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) = false
   AND coalesce(Item.bitKitParent,false) = false
   AND coalesce(WOD.numQtyItemsReq,0) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
   AND 1 =(CASE WHEN OM.numOppId IS NOT NULL THEN(CASE WHEN OM.tintoppstatus = 1 THEN 1 ELSE 0 END) ELSE 1 END)
   AND WOD.numChildItemID = v_numItemCode
   AND WOD.numWarehouseItemID = v_numWarehouseItemID
   AND 1 =(CASE
   WHEN OI.numoppitemtCode IS NOT NULL
   THEN(CASE
      WHEN coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) = v_dtReleaseDate
      THEN 1
      ELSE 0
      END)
   ELSE(CASE
      WHEN CAST(WO.dtmEndDate AS DATE) = v_dtReleaseDate
      THEN 1
      ELSE 0
      END)
   END);

	
open SWV_RefCur for SELECT * FROM tt_TEMP ORDER BY dtCreatedDate;
END; $$;












