-- Stored procedure definition script USP_GetUsedDiscountCodes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUsedDiscountCodes(v_numPromotionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  CodeUsageLimit as "CodeUsageLimit", vcDiscountCode as "vcDiscountCode"
   FROM DiscountCodes DC
   INNER JOIN DiscountCodeUsage DCU
   ON DC.numDiscountId = DCU.numDiscountId
   WHERE numPromotionID = v_numPromotionID
   AND DCU.intCodeUsed > 0;
END; $$;












