-- Function definition script fn_GetComapnyName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetComapnyName(v_numDivisionID NUMERIC)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyID NUMERIC;
   v_vcDivision TEXT;
BEGIN
   SELECT numCompanyID, CASE WHEN LENGTH(coalesce(vcDivisionName,'')) > 1 THEN ', ' || coalesce(vcDivisionName,'') ELSE '' END INTO v_numCompanyID,v_vcDivision from DivisionMaster where numDivisionID = v_numDivisionID;

   return(select vcCompanyName ||  coalesce(v_vcDivision,'') from CompanyInfo  where numCompanyId = v_numCompanyID);
END; $$;

