-- Stored procedure definition script USP_GeneralLedgerList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Created By Siva                                                                                                                                                                  
CREATE OR REPLACE FUNCTION USP_GeneralLedgerList(v_numChartAcntId NUMERIC(9,0),                                                                                                                                                                        
v_dtFromDate TIMESTAMP,                                                                                    
v_dtToDate TIMESTAMP,                    
v_BoolOpeningBalance BOOLEAN,          
v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);                                                                                                                                                                    
   v_strChildCategory  VARCHAR(5000);                                                                                                                            
   v_numAcntTypeId  INTEGER;                                                                                                                                                               
   v_OpeningBal  VARCHAR(8000);                                                                                          
   v_numAcntOpeningBal  INTEGER;
   SWV_ExecDyn  VARCHAR(5000);
   v_TotRecs  NUMERIC(9,0);                                            
   
   
      
                                                                                              
   v_i  NUMERIC(9,0);                                                                                                                                                
   v_numCustId  VARCHAR(9);                                    
   v_numPayment  NUMERIC(19,4);                                                                                                   
   v_numDeposit  NUMERIC(19,4);                                                                                                
   v_numBalanceAmt  NUMERIC(19,4);                                      
   v_bitBalance  BOOLEAN;                                                                                                               
   v_numCheckId  NUMERIC(9,0);                           
   v_numCashCreditCardId  NUMERIC(9,0);                                                                                                              
--Declare @numCashCreditCardId as numeric(9)                                                                                                                                      
   v_j  NUMERIC(9,0);                                                                                                                                                
   v_numCustomerId  NUMERIC(9,0);                                                                                                             
   v_bitChargeBack  BOOLEAN;                                                                          
   v_bitMoneyOut  BOOLEAN;
BEGIN
   v_OpeningBal := 'Opening Balance';                                                                                                                                                
   v_strSQL := '';                                                                                                                                            
--Create a Temporary table to hold data                                                                                                                                                                                    
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCustomerId NUMERIC(9,0),
      JournalId NUMERIC(9,0),
      TransactionType VARCHAR(500),
      EntryDate TIMESTAMP,
      CompanyName VARCHAR(8000),
      CheckId NUMERIC(9,0),
      CashCreditCardId NUMERIC(9,0),
      Memo VARCHAR(8000),
      Payment NUMERIC(19,4),
      Deposit NUMERIC(19,4),
      numBalance NUMERIC(19,4),
      numChartAcntId NUMERIC(9,0),
      numTransactionId NUMERIC(9,0),
      AcntTypeDescription  VARCHAR(50),
      numOppID NUMERIC(9,0),
      numOppBizDocsId NUMERIC(9,0),
      numDepositId NUMERIC(9,0),
      numBizDocsPaymentDetId NUMERIC(9,0),
      numCategoryHDRID NUMERIC(9,0),
      tintTEType   NUMERIC(9,0),
      numCategory  NUMERIC(9,0),
      numUserCntID   NUMERIC(9,0),
      dtFromDate TIMESTAMP
   );                                                                                                                          
                                                                                                                                      
   if v_dtFromDate = '1900-01-01 00:00:00.000' then 
      v_dtFromDate := CAST('Jan  1 1753 12:00:00:000AM' AS TIMESTAMP);
   end if;                                                                            
                                                                                   
   v_strChildCategory := fn_ChildCategory(v_numChartAcntId::VARCHAR(100),v_numDomainId) || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(10)),1,10);                                                                                               
   select   coalesce(numAcntType,0) INTO v_numAcntTypeId from Chart_Of_Accounts Where numAccountId = v_numChartAcntId And numDomainId = v_numDomainId;                                                                      
   select   Count(*) INTO v_numAcntOpeningBal From General_Journal_Details where numChartAcntId = v_numChartAcntId And numDomainId = v_numDomainId;                                                                                                       
   if v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then

      v_strSQL := '   Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                                              
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then ''Cash''                                                                                      
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then  ''Charge''                                                                                         
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 1 then ''BizDocs Invoice''                                       
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 2 then ''BizDocs Purchase''                                     
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''                                           
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 1 then ''Receive Amt''                                  
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 2 then ''Vendor Amt''            
Else Case when coalesce(GJH.numCategoryHDRID,0) <> 0 then ''Time And Expenses''                                                                                 
Else Case When GJH.numJournal_Id <> 0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                                   
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,                          
GJH.numCashCreditCardId as CashCreditCardId,                                  
GJD.varDescription as Memo,                  
(case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit ,                       
(case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,                                                                                                                      
null AS numBalance,                
coalesce(GJD.numChartAcntId,0) as numChartAcntId,                  
GJD.numTransactionId as numTransactionId,                                                                     
LD.vcData as AcntTypeDescription,                                          
GJH.numOppId as numOppId,                                            
GJH.numOppBizDocsId as numOppBizDocsId,                                        
GJH.numDepositId as numDepositId,                                      
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,      
coalesce(GJH.numCategoryHDRID,0) as numCategoryHDRID,    
 coalesce(TE.tintTEType,0) as tintTEType,    
TE.numCategory as numCategory,    
TE.numUserCntID as numUserCntID,  
TE.dtFromDate as dtFromDate                                                                                   
From General_Journal_Header as GJH                                                                                                                                           
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id = GJD.numJournalId                                            
Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID                                                                                                         
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId                                                                               
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId                                                                                              
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId                                                                                          
Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId                                                                                          
Left outer join ListDetails LD on CA.numAcntType = LD.numListItemID                                
Left outer join OpportunityMaster Opp on GJH.numOppId = Opp.numOppId    
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID = TE.numCategoryHDRID                                                                                   
Where  GJD.numChartAcntId in(' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(5)),1,5) || ') And GJH.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' order by EntryDate';
   Else
      v_strSQL := ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                                            
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''                           
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''                                                                          
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''                                       
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''                                          
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''                                         
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''                   
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''       
Else Case when coalesce(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''                                                       
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                                  
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,                                                                                        
GJH.numCashCreditCardId as CashCreditCardId,                                                
GJD.varDescription as Memo,                                                                           
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,                                                                      
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,                                                                       
null numBalance,                
coalesce(GJD.numChartAcntId,0) as numChartAcntId,                
GJD.numTransactionId as numTransactionId,                
LD.vcData as AcntTypeDescription,                
GJH.numOppId as numOppId,                
GJH.numOppBizDocsId as numOppBizDocsId,                                        
GJH.numDepositId as numDepositId,                                      
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,      
coalesce(GJH.numCategoryHDRID,0) as numCategoryHDRID,    
 coalesce(TE.tintTEType,0) as tintTEType,    
TE.numCategory as numCategory,    
TE.numUserCntID as numUserCntID,  
TE.dtFromDate as dtFromDate                  
From General_Journal_Header as GJH                                                                                                                                
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId                                                                                                                         
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID                                                                                                                                                       
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                                                                                
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId                                                                                           
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId                                                                                          
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId                                
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId                               
Left outer join ListDetails LD on CA.numAcntType= LD.numListItemID     
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID                                                                                     
Where GJD.numChartAcntId in (' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(5)),1,5) || ') And GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' order by EntryDate';
   end if;                                                                    
                                                                                        
   RAISE NOTICE '%',(v_strChildCategory);                                   
   RAISE NOTICE '%',(v_strSQL);                                                                          
--Exec (@strSQL)                                                                                                                       
                                                                             
                         
   SWV_ExecDyn := 'CREATE TEMP TABLE IF NOT EXISTS tt_TEMPTABLE AS ' || coalesce(v_strSQL,'');
   EXECUTE SWV_ExecDyn;                                                                                                                                          
   v_bitBalance := false;                                                                                          
   v_i := 1;                                                                                                                                             
   v_j := 0;                                                                                                                                      
                                    
                                                                                                               
                                                                                                                                           
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                            
   RAISE NOTICE '%',(v_TotRecs);                                                                                                                                  
   While v_i <= v_TotRecs LOOP
      select   coalesce(numCustomerId,0), coalesce(Payment,cast(0 as NUMERIC(19,4))), coalesce(Deposit,cast(0 as NUMERIC(19,4))), coalesce(CheckId,0), coalesce(CashCreditCardId,0) INTO v_numCustId,v_numPayment,v_numDeposit,v_numCheckId,v_numCashCreditCardId From tt_TEMPTABLE Where ID = v_i;
      RAISE NOTICE '@numDeposit%',SUBSTR(CAST(v_numDeposit AS VARCHAR(30)),1,30);
      RAISE NOTICE '@numPayment%',SUBSTR(CAST(v_numPayment AS VARCHAR(30)),1,30);
      RAISE NOTICE '@CheckId1%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
      RAISE NOTICE '@numCashCreditCardId%',SUBSTR(CAST(v_numCashCreditCardId AS VARCHAR(50)),1,50);
      select   bitChargeBack, bitMoneyOut INTO v_bitChargeBack,v_bitMoneyOut From CashCreditCardDetails Where numCashCreditId = v_numCashCreditCardId And numDomainID = v_numDomainId;
      RAISE NOTICE '%',v_bitChargeBack;
      RAISE NOTICE '%',v_bitMoneyOut;
      If v_i = 1 then
 
         If v_numPayment <> 0 then
  
            Update  tt_TEMPTABLE Set numBalance = -v_numPayment Where ID = v_i;
            If v_numAcntTypeId = 815 then
       
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If  v_numCashCreditCardId <> 0 then
            
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                  end if;
               Else
                  RAISE NOTICE 'Shlok - SP';
                  Update  tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
         Else
            RAISE NOTICE 'SP';
            Update  tt_TEMPTABLE Set numBalance = v_numDeposit Where ID = v_i;
            RAISE NOTICE '@numAcntTypeId%',SUBSTR(CAST(v_numAcntTypeId AS VARCHAR(30)),1,30);
            If v_numAcntTypeId = 815 then
    
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
        
                  If  v_numCashCreditCardId <> 0 then
            
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
            end if;
         end if;
      end if;
      If v_i > 1 then

         If v_numPayment <> 0 then

            v_j := v_i -1;
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update  tt_TEMPTABLE Set numBalance = v_numBalanceAmt -v_numPayment Where ID = v_i;
            If v_numAcntTypeId = 815 then
     
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If v_numCashCreditCardId <> 0 then
      
                     If v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            If v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
            end if;
         Else
            v_j := v_i -1;
            RAISE NOTICE '@CheckId2%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update  tt_TEMPTABLE Set numBalance = v_numBalanceAmt+v_numDeposit Where ID = v_i;
            If v_numAcntTypeId = 815 then

               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  If v_numCashCreditCardId <> 0 then
     
                     if v_bitChargeBack = true or v_bitMoneyOut = false then
                        Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
            If v_numAcntTypeId <> 814 And v_numAcntTypeId <> 815 then
               Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
            end if;
         end if;
      end if;
      v_i := v_i+1;
   END LOOP;         
   If v_BoolOpeningBalance = false then
      open SWV_RefCur for
      Select
      numCustomerId ,
	 JournalId  ,
	 TransactionType ,
	FormatedDateFromDate(EntryDate::VARCHAR(50),v_numDomainId) AS EntryDate ,
	 CompanyName ,
	 CheckId  ,
	 CashCreditCardId  ,
	Memo ,
	 Payment ,
	 Deposit ,
	 numBalance ,
	 numChartAcntId  ,
	 numTransactionId  ,
	 AcntTypeDescription  ,
	 numOppID  ,
	 numOppBizDocsId  ,
	 numDepositId  ,
	 numBizDocsPaymentDetId  ,
	 numCategoryHDRID  ,
	 tintTEType    ,
	 numCategory   ,
	 numUserCntID    ,
	 dtFromDate  from tt_TEMPTABLE
      Where EntryDate >= CAST('' || CAST(v_dtFromDate AS VARCHAR(300)) || '' AS timestamp)  And EntryDate <= CAST('' || CAST(v_dtToDate AS VARCHAR(300)) || '' AS timestamp);
   Else
      open SWV_RefCur for
      Select  numCustomerId ,
 JournalId  ,
 TransactionType ,
FormatedDateFromDate(EntryDate::VARCHAR(50),v_numDomainId) AS EntryDate ,
 CompanyName ,
 CheckId  ,
 CashCreditCardId  ,
Memo ,
 Payment ,
 Deposit ,
 numBalance ,
 numChartAcntId  ,
 numTransactionId  ,
 AcntTypeDescription  ,
 numOppID  ,
 numOppBizDocsId  ,
 numDepositId  ,
 numBizDocsPaymentDetId  ,
 numCategoryHDRID  ,
 tintTEType    ,
 numCategory   ,
 numUserCntID    ,
 dtFromDate   from tt_TEMPTABLE Where EntryDate < CAST('' || CAST(v_dtFromDate AS VARCHAR(300)) || '' AS timestamp);
   end if;                     
   RETURN;
END; $$;


