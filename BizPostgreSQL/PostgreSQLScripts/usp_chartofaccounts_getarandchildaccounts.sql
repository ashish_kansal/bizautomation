-- Stored procedure definition script USP_ChartOfAccounts_GetARAndChildAccounts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ChartOfAccounts_GetARAndChildAccounts(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE

   v_numAccountID  NUMERIC(18,0);
   v_vcAccountCode  VARCHAR(300);
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numAccountID NUMERIC(18,0),
      vcAccountName VARCHAR(300),
      numDefaultARAccountID NUMERIC(18,0)
   );
   select   Chart_Of_Accounts.numAccountId, Chart_Of_Accounts.vcAccountCode INTO v_numAccountID,v_vcAccountCode FROM
   AccountingCharges
   INNER JOIN
   Chart_Of_Accounts
   ON
   AccountingCharges.numAccountID = Chart_Of_Accounts.numAccountId WHERE
   AccountingCharges.numDomainID = v_numDomainID
   AND Chart_Of_Accounts.numDomainId = v_numDomainID
   AND numChargeTypeId = 4;

   IF coalesce(v_numAccountID,0) > 0 then
	
      INSERT INTO tt_TEMP(numAccountID
			,vcAccountName
			,numDefaultARAccountID)
      SELECT
      numAccountId
			,vcAccountName
			,v_numAccountID
      FROM
      Chart_Of_Accounts
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitActive,false) = true
      AND vcAccountCode ilike coalesce(v_vcAccountCode,'-') || '%';
   end if;

   open SWV_RefCur for SELECT * FROM tt_TEMP;
END; $$;












