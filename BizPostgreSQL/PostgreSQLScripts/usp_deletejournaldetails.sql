DROP FUNCTION IF EXISTS USP_DeleteJournalDetails;

CREATE OR REPLACE FUNCTION USP_DeleteJournalDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,                        
v_numJournalID NUMERIC(9,0) DEFAULT 0,                          
v_numBillPaymentID NUMERIC(9,0) DEFAULT 0,                    
v_numDepositID NUMERIC(9,0) DEFAULT 0,          
v_numCheckHeaderID NUMERIC(9,0) DEFAULT 0,
v_numBillID NUMERIC(9,0) DEFAULT 0,
v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0,
v_numReturnID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_datEntry_Date  TIMESTAMP;
BEGIN
IF v_numDepositID > 0 then

	--Throw error messae if Deposite to Default Undeposited Funds Account
      IF EXISTS(SELECT numDepositId FROM DepositMaster WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID
      AND tintDepositeToType = 2 AND tintDepositePage IN(2,3) AND bitDepositedToAcnt = true) then
	
         RAISE EXCEPTION 'Undeposited_Account';
         RETURN;
      end if;
	
	--Throw error messae if Deposite use in Refund
      IF EXISTS(SELECT numDepositId FROM DepositMaster WHERE numDepositId = v_numDepositID And monRefundAmount > 0) then
	
         RAISE EXCEPTION 'Refund_Payment';
         RETURN;
      end if;
      select   coalesce(DM.dtDepositDate,DM.dtCreationDate) INTO v_datEntry_Date FROM DepositMaster AS DM WHERE DM.numDepositId = v_numDepositID AND DM.numDomainId = v_numDomainID;
	--Validation of closed financial year
      RAISE NOTICE '%',v_datEntry_Date;
      PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_datEntry_Date);

	--Update SO bizdocs amount Paid
      UPDATE OpportunityBizDocs OBD SET monAmountPaid = OBD.monAmountPaid -DD.monAmountPaid
      FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositID=DD.numDepositID 
      WHERE DD.numOppBizDocsID = OBD.numOppBizDocsId AND(DM.numDomainId = v_numDomainID AND DM.numDepositId = v_numDepositID AND DM.tintDepositePage IN(2,3));
	
	
	--Reset Integration with accounting
      UPDATE DepositMaster SET bitDepositedToAcnt = false WHERE numDepositId IN(SELECT numChildDepositID FROM DepositeDetails WHERE numDepositID = v_numDepositID AND numChildDepositID > 0);
	
	--Delete Transaction History
      DELETE FROM TransactionHistory WHERE numTransHistoryID IN(SELECT numTransHistoryID FROM DepositMaster WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID);
      DELETE FROM DepositeDetails WHERE numDepositID = v_numDepositID;
      IF EXISTS(SELECT 1 FROM DepositMaster WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID AND coalesce(numReturnHeaderID,0) > 0) then
	
         UPDATE DepositMaster SET monAppliedAmount = 0 WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID;
      ELSE
         IF v_numJournalID = 0 then
		
            select   numJOurnal_Id INTO v_numJournalID FROM General_Journal_Header WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID;
         end if;
         Delete From General_Journal_Details Where numJournalId = v_numJournalID And numDomainId = v_numDomainID;
         Delete From General_Journal_Header Where numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID;
         DELETE FROM DepositMaster WHERE numDepositId = v_numDepositID And numDomainId = v_numDomainID;
      end if;
   ELSEIF v_numBillID > 0
   then

	--Throw error message if amount paid
      IF EXISTS(SELECT numBillID FROM BillHeader WHERE numBillID = v_numBillID AND coalesce(monAmtPaid,0) > 0) then
	
         RAISE EXCEPTION 'BILL_PAID'; --Bill is paid, can not delete bill
         RETURN;
      end if;
      IF v_numJournalID = 0 then
	
         select   numJOurnal_Id INTO v_numJournalID FROM General_Journal_Header WHERE numBillID = v_numBillID And numDomainId = v_numDomainID;
      end if;
      Delete From General_Journal_Details Where numJournalId = v_numJournalID And numDomainId = v_numDomainID;
      Delete From General_Journal_Header Where numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID;
      DELETE FROM ProjectsOpportunities WHERE numBillId = v_numBillID AND numDomainId = v_numDomainID;
      DELETE FROM BillDetails WHERE numBillID = v_numBillID;
      DELETE FROM BillHeader WHERE numBillID = v_numBillID And numDomainId = v_numDomainID;
   ELSEIF v_numBillPaymentID > 0
   then

	--Throw error messae if Bill Payment use in Refund
      IF EXISTS(SELECT numBillPaymentID FROM BillPaymentHeader WHERE numBillPaymentID = v_numBillPaymentID And monRefundAmount > 0) then
	
         RAISE EXCEPTION 'Refund_Payment';
         RETURN;
      end if;
      UPDATE BillHeader SET bitIsPaid = false WHERE numBillID IN(SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID AND numBillID > 0);


	--Update Add Bill amount Paid
      UPDATE BillHeader BH SET monAmtPaid = monAmtPaid -BPD.monAmount FROM BillPaymentDetails BPD
      WHERE BPD.numBillID = BH.numBillID AND(numBillPaymentID = v_numBillPaymentID AND BPD.tintBillType = 2);

	--Update PO bizdocs amount Paid
      UPDATE OpportunityBizDocs OBD SET monAmountPaid = OBD.monAmountPaid -BPD.monAmount FROM BillPaymentDetails BPD
      WHERE BPD.numOppBizDocsID = OBD.numOppBizDocsId AND(numBillPaymentID = v_numBillPaymentID AND BPD.tintBillType = 1);
	
	--Delete if check entry
      DELETE FROM CheckHeader WHERE tintReferenceType = 8 AND numReferenceID = v_numBillPaymentID And numDomainID = v_numDomainID;
      IF v_numJournalID = 0 then
	
         select   numJOurnal_Id INTO v_numJournalID FROM General_Journal_Header WHERE numBillPaymentID = v_numBillPaymentID And numDomainId = v_numDomainID;
      end if;
      Delete From General_Journal_Details Where numJournalId = v_numJournalID And numDomainId = v_numDomainID;
      Delete From General_Journal_Header Where numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID;
      DELETE FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID;
      DELETE FROM BillPaymentHeader WHERE numBillPaymentID = v_numBillPaymentID And numDomainId = v_numDomainID AND coalesce(numReturnHeaderID,0) = 0;
      UPDATE BillPaymentHeader SET monAppliedAmount = 0 WHERE numBillPaymentID = v_numBillPaymentID And numDomainId = v_numDomainID AND coalesce(numReturnHeaderID,0) != 0;
   ELSEIF v_numCheckHeaderID > 0
   then

      IF v_numJournalID = 0 then
	
         select   numJOurnal_Id INTO v_numJournalID FROM General_Journal_Header WHERE numCheckHeaderID = v_numCheckHeaderID And numDomainId = v_numDomainID;
      end if;
      Delete From General_Journal_Details Where numJournalId = v_numJournalID And numDomainId = v_numDomainID;
      Delete From General_Journal_Header Where numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID;
      DELETE FROM CheckDetails WHERE numCheckHeaderID = v_numCheckHeaderID;
      DELETE FROM CheckHeader WHERE numCheckHeaderID = v_numCheckHeaderID And numDomainID = v_numDomainID;
   ELSEIF v_numCategoryHDRID > 0
   then

      PERFORM USP_DeleteTimExpLeave(v_numCategoryHDRID := v_numCategoryHDRID,v_numDomainID := v_numDomainID);
   ELSEIF v_numReturnID > 0
   then
      PERFORM USP_DeleteSalesReturnDetails(v_numReturnHeaderID := v_numReturnID,v_numReturnStatus := 14879,v_numDomainID := v_numDomainID,
      v_numUserCntID := v_numUserCntID);
   end if;
   IF EXISTS(SELECT numItemID FROM General_Journal_Details WHERE numJournalId = v_numJournalID And numDomainId = v_numDomainID AND (chBizDocItems = 'IA1' OR chBizDocItems = 'OE1') AND (coalesce(numCreditAmt,0) <> 0 OR coalesce(numDebitAmt,0) <> 0)) then

      RAISE EXCEPTION 'InventoryAdjustment';
      RETURN;
   end if;
   IF EXISTS(SELECT numJournalId FROM General_Journal_Details WHERE numJournalId = v_numJournalID And numDomainId = v_numDomainID AND chBizDocItems = 'OE') then

      RAISE EXCEPTION 'OpeningBalance';
      RETURN;
   end if;
   Delete From General_Journal_Details Where numJournalId IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID
   AND coalesce(numBillID,0) = 0 AND coalesce(numBillPaymentID,0) = 0 AND coalesce(numPayrollDetailID,0) = 0
   AND coalesce(numCheckHeaderID,0) = 0 AND coalesce(numReturnID,0) = 0
   AND coalesce(numDepositId,0) = 0);
   Delete From General_Journal_Header Where numJOurnal_Id = v_numJournalID And numDomainId = v_numDomainID
   AND coalesce(numBillID,0) = 0 AND coalesce(numBillPaymentID,0) = 0 AND coalesce(numPayrollDetailID,0) = 0
   AND coalesce(numCheckHeaderID,0) = 0 AND coalesce(numReturnID,0) = 0
   AND coalesce(numDepositId,0) = 0;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;

END; $$;


