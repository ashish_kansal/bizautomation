-- Stored procedure definition script USP_ReportDashboard_UpdateLastCacheDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportDashboard_UpdateLastCacheDate(v_numDomainID NUMERIC(18,0)
	,v_numDashboardID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   ReportDashboard
   SET
   dtLastCacheDate = TIMEZONE('UTC',now())
   WHERE
   numDomainID = v_numDomainID
   AND numDashBoardID = v_numDashboardID;
   RETURN;
END; $$;


