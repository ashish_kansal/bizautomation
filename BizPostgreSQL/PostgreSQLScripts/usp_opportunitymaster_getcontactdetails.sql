-- Stored procedure definition script USP_OpportunityMaster_GetContactDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetContactDetails(v_cntIds VARCHAR(1000),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  ADC.numDivisionId,ADC.numContactId,vcGivenName,vcFirstName,vcLastname, c.vcCompanyName,dm.vcDivisionName
   FROM AdditionalContactsInformation ADC
   JOIN DivisionMaster dm ON ADC.numDivisionId = dm.numDivisionID
   JOIN CompanyInfo c ON dm.numCompanyID = c.numCompanyId
   where ADC.numContactId in(select Id from SplitIDs(v_cntIds,','));
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_PortalAddItem]    Script Date: 07/26/2008 16:20:32 ******/

