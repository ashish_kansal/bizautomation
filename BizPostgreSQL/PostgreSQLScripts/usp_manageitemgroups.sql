-- Stored procedure definition script USP_ManageItemGroups for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageItemGroups(v_numDomainID NUMERIC(9,0) DEFAULT 0,       
v_strOpt VARCHAR(4000) DEFAULT '',    
v_strAttr VARCHAR(4000) DEFAULT '',     
v_numItemGroupID NUMERIC(9,0) DEFAULT NULL,      
v_vcName VARCHAR(100) DEFAULT NULL ,
v_bitCombineAssemblies BOOLEAN DEFAULT NULL,
v_numMapToDropdownFld NUMERIC(18,0) DEFAULT 0,
v_numProfileItem NUMERIC(18,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc1  INTEGER;        
   v_hDoc2  INTEGER;
BEGIN
   if v_numItemGroupID = 0 then
 
      insert into ItemGroups(vcItemGroup,numDomainID,bitCombineAssemblies,numMapToDropdownFld,numProfileItem)
  values(v_vcName,v_numDomainID,v_bitCombineAssemblies,v_numMapToDropdownFld,v_numProfileItem);
  
      v_numItemGroupID := CURRVAL('ItemGroups_seq');
      SELECT * INTO v_hDoc1 FROM SWF_Xml_PrepareDocument();
      insert into ItemGroupsDTL(numItemGroupID,numOppAccAttrID,tintType)
      select v_numItemGroupID,X.numOppAccAttrID,1 
	  from
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strOpt AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppAccAttrID NUMERIC(9,0) PATH 'numOppAccAttrID'
		) AS X;

      insert into ItemGroupsDTL(numItemGroupID,numOppAccAttrID,tintType)
      select v_numItemGroupID,X.numOppAccAttrID,2 
	  from
	   XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strAttr AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppAccAttrID NUMERIC(9,0) PATH 'numOppAccAttrID'
		) AS X;

   ELSEIF v_numItemGroupID > 0
   then
 
      Update ItemGroups
      set vcItemGroup = v_vcName,numDomainID = v_numDomainID,bitCombineAssemblies = v_bitCombineAssemblies,
      numMapToDropdownFld = v_numMapToDropdownFld,numProfileItem = v_numProfileItem
      where numItemGroupID = v_numItemGroupID;    

 --delete from ItemGroupsDTL where numItemGroupID= @numItemGroupID
      update ItemGroupsDTL set
      numQtyItemsReq = X.QtyItemsReq,numWareHouseItemId = X.WareHouseItemId,numListid = X.ListItemId
      From
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strOpt AS XML)
			COLUMNS
				id FOR ORDINALITY,
				OppAccAttrID NUMERIC(9,0) PATH 'OppAccAttrID',
				QtyItemsReq NUMERIC(9,0) PATH 'QtyItemsReq',
				WareHouseItemId NUMERIC(9,0) PATH 'WareHouseItemId',
				ListItemId NUMERIC(9,0) PATH 'ListItemId'
		) AS X
      where  numItemGroupID = v_numItemGroupID and numOppAccAttrID = X.OppAccAttrID;
                                                                           
--  insert into ItemGroupsDTL                                          
--     (numItemGroupID,numOppAccAttrID,tintType,numQtyItemsReq,numWareHouseItemId,numDefaultSelect)                                   
--    select @numItemGroupID,X.numOppAccAttrID,1,X.numQtyItemsReq,X.numWareHouseItemId,X.numDefaultSelect from(                                          
--    SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table1',2)                                          
--    WITH  (numOppAccAttrID numeric(9),numQtyItemsReq numeric(9),numWareHouseItemId numeric(9),numDefaultSelect numeric(9)))X                                                               
--    EXEC sp_xml_removedocument @hDoc1   
  
      insert into ItemGroupsDTL(numItemGroupID,numOppAccAttrID,tintType)
      select v_numItemGroupID,X.numOppAccAttrID,2 
	  from
	  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strAttr AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppAccAttrID NUMERIC(9,0) PATH 'numOppAccAttrID'
		) AS X
		where X.numOppAccAttrID not in(select numOppAccAttrID from ItemGroupsDTL where numItemGroupID = v_numItemGroupID);                                                         
    
	--Bug fix ID 1720 #6
	-- delete saved attributes
      DELETE FROM CFW_Fld_Values_Serialized_Items WHERE
      Fld_ID NOT IN(SELECT numOppAccAttrID FROM 
	   XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strAttr AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppAccAttrID NUMERIC(9,0) PATH 'numOppAccAttrID'
		))
      AND RecId IN(SELECT numWareHouseItemID FROM  WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID IN(SELECT numItemCode FROM  Item WHERE numDomainID = v_numDomainID AND numItemGroup = v_numItemGroupID));
      DELETE FROM ItemGroupsDTL WHERE tintType = 2 AND numItemGroupID = v_numItemGroupID AND
      numOppAccAttrID NOT IN(SELECT numOppAccAttrID FROM XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strAttr AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppAccAttrID NUMERIC(9,0) PATH 'numOppAccAttrID'
		));
      
   end if;      
   open SWV_RefCur for select v_numItemGroupID;
END; $$;












