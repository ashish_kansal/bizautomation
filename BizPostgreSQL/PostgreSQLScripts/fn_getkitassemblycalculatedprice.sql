CREATE OR REPLACE FUNCTION fn_GetKitAssemblyCalculatedPrice
(
	v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numQty DOUBLE PRECISION
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_tintKitAssemblyPriceBasedOn SMALLINT
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0)
	,v_vcSelectedKitChildItems TEXT
	,v_numCurrencyID NUMERIC(18,0)
	,v_fltExchangeRate DOUBLE PRECISION
)
RETURNS TABLE
(
   bitSuccess BOOLEAN,
   monPrice DECIMAL(20,5),
   monMSRPPrice DECIMAL(20,5)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monListPrice  DECIMAL(20,5);
   v_numWarehouseID  NUMERIC(18,0);

   v_KitAssemblyPrice  DECIMAL(20,5) DEFAULT 0;
   v_monCalculatedPrice  DECIMAL(20,5) DEFAULT 0;
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE CASCADE;
   

   select   coalesce(monListPrice,0) INTO v_monListPrice FROM
   Item WHERE
   numItemCode = v_numItemCode;

   IF coalesce(v_numWarehouseItemID,0) > 0 then
	
      select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
   ELSE
      select   numWareHouseID INTO v_numWarehouseID FROM Warehouses WHERE numDomainID = v_numDomainID    LIMIT 1;
   end if;

   CREATE TEMPORARY TABLE IF NOT EXISTS tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS
   (
      numItemCode NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numUOMId NUMERIC(18,0),
      bitKitParent BOOLEAN,
      bitFirst BOOLEAN
   );

   DELETE FROM tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS;

   IF coalesce(v_numOppItemID,0) > 0 then
	
      INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS(numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent)
      SELECT
      OKI.numChildItemID
			,(OKI.numQtyItemsReq_Orig*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))
			,OKI.numUOMId
			,coalesce(I.bitKitParent,false)
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      WHERE
      OKI.numOppId = v_numOppID
      AND OKI.numOppItemID = v_numOppItemID
      AND 1 =(CASE
      WHEN coalesce(I.bitKitParent,false) = true
      THEN(CASE
         WHEN coalesce(I.bitCalAmtBasedonDepItems,false) = true
         THEN(CASE WHEN coalesce(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
         ELSE 1
         END)
      ELSE 1
      END);
      INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS(numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent)
      SELECT
      OKCI.numItemID
			,(OKI.numQtyItemsReq_Orig*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,IOKI.numBaseUnit),1))*(OKCI.numQtyItemsReq_Orig*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,IOKCI.numBaseUnit),1))
			,OKCI.numUOMId
			,coalesce(IOKCI.bitKitParent,false)
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      Item IOKCI
      ON
      OKCI.numItemID = IOKCI.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OKCI.numOppChildItemID = OKI.numOppChildItemID
      INNER JOIN
      Item IOKI
      ON
      OKI.numChildItemID = IOKI.numItemCode
      WHERE
      OKCI.numOppId = v_numOppID
      AND OKCI.numOppItemID = v_numOppItemID
      AND coalesce(IOKI.bitKitParent,false) = true
      AND coalesce(IOKI.bitCalAmtBasedonDepItems,false) = true;
   ELSE
      DROP TABLE IF EXISTS tt_TEMPEXISTINGITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPEXISTINGITEMS
      (
         vcItem VARCHAR(100)
      );
      INSERT INTO tt_TEMPEXISTINGITEMS(vcItem)
      SELECT
      OutParam
      FROM
      SplitString(v_vcSelectedKitChildItems,',');
      DROP TABLE IF EXISTS tt_TEMPSELECTEDKITCHILDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSELECTEDKITCHILDS
      (
         ChildKitItemID NUMERIC(18,0),
         ChildKitWarehouseItemID NUMERIC(18,0),
         ChildKitChildItemID NUMERIC(18,0),
         ChildKitChildWarehouseItemID NUMERIC(18,0),
         ChildQty DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPSELECTEDKITCHILDS(ChildKitItemID
			,ChildKitChildItemID
			,ChildQty)
      SELECT
			CAST(split_part(vcItem,'-',1) AS NUMERIC)
			,CAST(split_part(vcItem,'-',2) AS NUMERIC)
			,CAST(split_part(vcItem,'-',3) AS DOUBLE PRECISION)
      FROM(SELECT
         vcItem
         FROM
         tt_TEMPEXISTINGITEMS) As x;
      UPDATE
      tt_TEMPSELECTEDKITCHILDS
      SET
      ChildKitWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ChildKitItemID AND numWareHouseID = v_numWarehouseID LIMIT 1),ChildKitChildWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ChildKitChildItemID AND numWareHouseID = v_numWarehouseID LIMIT 1);
      IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0 then
		
         INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS(numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent)
         SELECT
         I.numItemCode
				,ChildQty
				,I.numBaseUnit
				,CAST(0 AS BOOLEAN)
         FROM
         tt_TEMPSELECTEDKITCHILDS T1
         INNER JOIN
         Item I
         ON
         T1.ChildKitChildItemID = I.numItemCode
         WHERE
         coalesce(ChildKitItemID,0) = 0;
      ELSE
         INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS(numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent) with recursive CTE(numItemCode,vcItemName,bitKitParent,bitCalAmtBasedonDepItems,numQtyItemsReq,
         numUOMId) AS(SELECT
         ID.numChildItemID AS numItemCode,
					I.vcItemName AS vcItemName,
					coalesce(I.bitKitParent,false) AS bitKitParent,
					coalesce(I.bitCalAmtBasedonDepItems,false) AS bitCalAmtBasedonDepItems,
					CAST((coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQtyItemsReq,
					coalesce(numUOMId,0) AS numUOMId
         FROM
         ItemDetails ID
         INNER JOIN
         Item I
         ON
         ID.numChildItemID = I.numItemCode
         WHERE
         numItemKitID = v_numItemCode
         AND 1 =(CASE
         WHEN coalesce(I.bitKitParent,false) = true AND LENGTH(coalesce(v_vcSelectedKitChildItems,'')) > 0 THEN(CASE
            WHEN numChildItemID IN(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS)
            THEN 1
            ELSE 0
            END)
         ELSE(CASE
            WHEN(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0
            THEN(CASE
               WHEN numChildItemID IN(SELECT ChildKitChildItemID FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0)
               THEN 1
               ELSE 0
               END)
            ELSE 1
            END)
         END)
         UNION ALL
         SELECT
         ID.numChildItemID AS numItemCode,
					I.vcItemName AS vcItemName,
					coalesce(I.bitKitParent,false) AS bitKitParent,
					coalesce(I.bitCalAmtBasedonDepItems,false) AS bitCalAmtBasedonDepItems,
					(CASE
         WHEN coalesce(((SELECT T2.ChildQty FROM tt_TEMPSELECTEDKITCHILDS T2 WHERE coalesce(Temp1.bitKitParent,false) = true AND T2.ChildKitItemID = ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)),0) > 0
         THEN coalesce(((SELECT T2.ChildQty FROM tt_TEMPSELECTEDKITCHILDS T2 WHERE coalesce(Temp1.bitKitParent,false) = true AND T2.ChildKitItemID = ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)),0)
         ELSE CAST((coalesce(Temp1.numQtyItemsReq,0)*coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION)
         END) AS numQtyItemsReq,
					coalesce(ID.numUOMID,0) AS numUOMId
         FROM
         CTE As Temp1
         INNER JOIN
         ItemDetails ID
         ON
         ID.numItemKitID = Temp1.numItemCode
         INNER JOIN
         Item I
         ON
         ID.numChildItemID = I.numItemCode
         WHERE
         Temp1.bitKitParent = true
         AND Temp1.bitCalAmtBasedonDepItems = true
         AND EXISTS(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS T2 WHERE coalesce(Temp1.bitKitParent,false) = true AND T2.ChildKitItemID = ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)) SELECT
         numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
         FROM
         CTE;
      end if;
   end if;

   IF EXISTS(SELECT ID.numItemCode FROM tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS ID INNER JOIN Item I ON ID.numItemCode = I.numItemCode WHERE I.charItemType = 'P' AND(SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numWareHouseID = v_numWarehouseID) = 0) then
		CREATE TEMPORARY TABLE tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE
	   (
		  bitSuccess BOOLEAN,
		  monPrice DECIMAL(20,5),
		  monMSRPPrice DECIMAL(20,5)
	   ) ON COMMIT DROP;

      INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE(bitSuccess
			,monPrice
			,monMSRPPrice)
		VALUES(false
			,0
			,0);
   ELSE
      v_monCalculatedPrice := coalesce((SELECT
											(CASE 
												WHEN v_tintKitAssemblyPriceBasedOn = 4 
												THEN coalesce(v_monListPrice,0)+coalesce(SUM(CalculatedPrice),0) 
												ELSE coalesce(SUM(CalculatedPrice),0) 
											END)
										FROM
										(SELECT
											 COALESCE(CASE
														 WHEN I.charItemType = 'P'
														 THEN
															(CASE
																WHEN coalesce(I.bitAssembly,false) = true AND coalesce(I.bitCalAmtBasedonDepItems,false) = true THEN
																	COALESCE((SELECT t.monPrice FROM fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,I.numItemCode,ID.numQtyItemsReq,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'',v_numCurrencyID,v_fltExchangeRate) t),0)
																WHEN COALESCE(I.bitKitParent,false) = true AND COALESCE(I.bitCalAmtBasedonDepItems,false) = true THEN
																	(CASE 
																		WHEN COALESCE(I.tintKitAssemblyPriceBasedOn,0) = 4 
																		THEN
																			(CASE
																				WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = I.numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
																				THEN COALESCE((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
																				ELSE (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(I.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(I.monListPrice,0) END)
																			END) 
																		ELSE 0 
																	END)
																ELSE
																	(CASE 
																		v_tintKitAssemblyPriceBasedOn
																		WHEN 2 THEN (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(I.monAverageCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(I.monAverageCost,0) END)
																		WHEN 3 THEN (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monCost,0) END) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																		ELSE (CASE
																				WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = I.numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
																				THEN COALESCE((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
																				ELSE (CASE 
																						WHEN v_fltExchangeRate <> 1 THEN 
																						CAST((coalesce(I.monListPrice,0)/v_fltExchangeRate) AS INTEGER) 
																						ELSE coalesce(I.monListPrice,0) 
																					END)
																			END)
																	END)
																END)
															ELSE
																(CASE v_tintKitAssemblyPriceBasedOn
																	WHEN 4 
																	THEN (CASE
																			WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = I.numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
																			THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
																			ELSE(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(I.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(I.monListPrice,0) END)
																		END)
																	WHEN 2 
																	THEN (CASE 
																			WHEN v_fltExchangeRate <> 1 
																			THEN CAST((coalesce(I.monAverageCost,0)/v_fltExchangeRate) AS INTEGER) 
																			ELSE coalesce(I.monAverageCost,0) 
																		END)
																	WHEN 3 
																	THEN (CASE 
																			WHEN v_fltExchangeRate <> 1 
																			THEN CAST((coalesce(V.monCost,0)/v_fltExchangeRate) AS INTEGER) 
																			ELSE COALESCE(V.monCost,0) 
																		END) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																	ELSE
																		(CASE
																			WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = I.numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
																			THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
																			ELSE (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(I.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(I.monListPrice,0) END)
																		END)
																END)
														END,0)*ID.numQtyItemsReq AS CalculatedPrice
										 FROM
											tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPITEMS ID
										 INNER JOIN
											Item I
										 ON
											ID.numItemCode = I.numItemCode
										 LEFT JOIN
											Vendor V
										 ON
											I.numVendorID = V.numVendorID
											AND I.numItemCode = V.numItemCode
										 LEFT JOIN LATERAL
										 (
											SELECT 
												* 
											FROM
												WareHouseItems WI
											WHERE
												WI.numItemID = I.numItemCode
												AND WI.numWareHouseID = v_numWarehouseID
											ORDER BY
												WI.numWareHouseItemID 
											LIMIT 1
										) AS WI on TRUE) T1),0);
			
		CREATE TEMPORARY TABLE tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE
	   (
		  bitSuccess BOOLEAN,
		  monPrice DECIMAL(20,5),
		  monMSRPPrice DECIMAL(20,5)
	   ) ON COMMIT DROP;

      INSERT INTO tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE(bitSuccess
			,monPrice
			,monMSRPPrice)
      SELECT
      true
			,monFinalPrice
			,v_monCalculatedPrice
      FROM
      fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,true,v_monCalculatedPrice,
      v_numWarehouseItemID,v_vcSelectedKitChildItems,v_numCurrencyID);
   end if;

   RETURN QUERY (SELECT * FROM tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE);
   
   DROP TABLE IF EXISTS tt_FN_GETKITASSEMBLYCALCULATEDPRICE_TEMPPRICE CASCADE;
END; $$;

