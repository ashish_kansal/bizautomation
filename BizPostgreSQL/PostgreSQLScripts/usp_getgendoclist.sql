-- Stored procedure definition script USP_GetGenDocList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetGenDocList(v_numDocCategory NUMERIC DEFAULT 0,
v_numDomainID NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_numDocCategory <> 0 then

      open SWV_RefCur for
      select numGenericDocID,VcDocName,* from GenericDocuments where cUrlType = 'L'  and  numDocCategory =  v_numDocCategory AND tintDocumentType = 1
      AND numDomainId = v_numDomainID;
   else
      open SWV_RefCur for
      select numGenericDocID,VcDocName,* from GenericDocuments where cUrlType = 'L' AND tintDocumentType = 1
      AND numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


