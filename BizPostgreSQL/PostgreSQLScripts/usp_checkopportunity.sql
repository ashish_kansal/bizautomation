-- Stored procedure definition script USP_CheckOpportunity for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckOpportunity(v_numDivisionId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select count(*) from OpportunityMaster where numDivisionId = v_numDivisionId and tintoppstatus = 1;
END; $$;












