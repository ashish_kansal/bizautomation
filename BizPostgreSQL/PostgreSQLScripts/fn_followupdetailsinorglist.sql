DROP FUNCTION IF EXISTS fn_followupdetailsinorglist;

CREATE OR REPLACE FUNCTION public.fn_followupdetailsinorglist(
	v_numcontactid numeric,
	v_numfollowup integer,
	v_numdomainid numeric)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_RetFollowup  TEXT;
BEGIN
   v_RetFollowup := '';

   IF(v_numFollowup = 1) then
  
      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = true) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL
            THEN CONCAT(GenericDocuments.VcDocName || ' ',coalesce(FormatedDateFromDate(ConECampaignDTL.bintSentON,v_numDomainID),
               ''),(CASE
               WHEN coalesce(ConECampaignDTL.bitSend,false) = true AND coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 0 THEN ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:red;">Failed</span>'
               WHEN coalesce(ConECampaignDTL.bitSend,false) = true AND coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 1 THEN ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:green;">Success</span>'
               ELSE ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:purple;">Pending</span>'
               END),(CASE
               WHEN (coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 1 AND coalesce(ConECampaignDTL.bitEmailRead,false) = true) THEN ' <span style="color:black;font-weight:bold">Read:</span><span style="color:green;">Yes</span>'
               WHEN (coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 1 AND coalesce(ConECampaignDTL.bitEmailRead,false) = false) THEN ' <span style="color:black;font-weight:bold">Read:</span><span style="color:purple;">Pending</span>'
               ELSE ' <span style="color:black;font-weight:bold">Read:</span><span style="color:purple;"></span>'
               END))
            WHEN tblActionItemData.RowID IS NOT NULL
            THEN CONCAT(tblActionItemData.Activity,' ',coalesce(FormatedDateFromDate(ConECampaignDTL.bintSentON,v_numDomainID),
               ''))--,
								--' <img alt="Action Items" height="16px" width="16px" title="Action Items" src="../images/MasterList-16.gif">')
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   ELSEIF (v_numFollowup = 2)
   then

      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = false) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN CONCAT(GenericDocuments.VcDocName,' ' || coalesce(FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,v_numDomainID),
               ''))
            WHEN tblActionItemData.RowID IS NOT NULL THEN CONCAT(tblActionItemData.Activity,' ' || coalesce(FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,v_numDomainID),
               ''))
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MIN(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = false))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   ELSEIF (v_numFollowup = 3)
   then
  
      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = true) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN(SELECT vcEmailLog FROM ConECampaignDTL WHERE coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true)
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   end if;

   RETURN COALESCE(v_RetFollowup,'');
END;
$BODY$;

ALTER FUNCTION public.fn_followupdetailsinorglist(numeric, integer, numeric)
    OWNER TO postgres;
