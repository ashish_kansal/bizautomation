-- Stored procedure definition script USP_WarehouseItems_BulkRemove for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WarehouseItems_BulkRemove(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numWareHouseItemID  NUMERIC(18,0);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TableWarehouseLocation_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLEWAREHOUSELOCATION CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEWAREHOUSELOCATION
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseItemID NUMERIC(18,0)
   );
   INSERT INTO tt_TABLEWAREHOUSELOCATION(numWarehouseItemID)
   SELECT
   numWareHouseItemID
   FROM
   WareHouseItems
   WHERE
   numDomainID = v_numDomainID
   AND coalesce(numWLocationID,0) > 0
   AND coalesce(numOnHand,0) = 0
   AND coalesce(numAllocation,0) = 0
   AND coalesce(numBackOrder,0) = 0
   AND coalesce(numonOrder,0) = 0
   AND NOT EXISTS(SELECT OI.numoppitemtCode FROM OpportunityMaster OM INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId WHERE OM.numDomainId = v_numDomainID AND OI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID)
   AND NOT EXISTS(SELECT OIRL.ID FROM OpportunityItemsReceievedLocation OIRL WHERE OIRL.numDomainId = v_numDomainID AND OIRL.numWarehouseItemID = WareHouseItems.numWareHouseItemID)
   AND NOT EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI WHERE OKI.numWareHouseItemId = WareHouseItems.numWareHouseItemID)
   AND NOT EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI WHERE OKCI.numWareHouseItemId = WareHouseItems.numWareHouseItemID);

		
   select   COUNT(*) INTO v_iCount FROM tt_TABLEWAREHOUSELOCATION;

   WHILE v_i <= v_iCount LOOP
      select   numWarehouseItemID INTO v_numWareHouseItemID FROM tt_TABLEWAREHOUSELOCATION WHERE ID = v_i;
      DELETE from WareHouseItmsDTL WHERE numWareHouseItemID = v_numWareHouseItemID;
      DELETE FROM WareHouseItems_Tracking WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numWareHouseItemID;
      DELETE from WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numWareHouseItemID;
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;



