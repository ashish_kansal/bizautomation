-- Stored procedure definition script USP_ManageBroadcastDTLs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBroadcastDTLs(v_numBroID NUMERIC(9,0) DEFAULT 0,  
v_strBroadCast TEXT DEFAULT NULL ,  
v_numTotalSussessfull NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete from BroadCastDtls where numBroadcastID = v_numBroID;         

   insert into BroadCastDtls(numBroadcastID,numDivisionID,numContactID,tintSucessfull)
   select v_numBroID,X.numDivisionId,x.numContactId,0 
   from
    XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strBroadCast AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numDivisionId NUMERIC(18,0) PATH 'numDivisionId',
				numContactId NUMERIC(18,0) PATH 'numContactId',
				Status SMALLINT PATH 'Status'
		) AS X;

   RETURN;
END; $$;



