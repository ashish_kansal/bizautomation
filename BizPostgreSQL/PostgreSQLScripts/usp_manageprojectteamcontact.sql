CREATE OR REPLACE FUNCTION USP_ManageProjectTeamContact(v_numProjectTeamId NUMERIC(9,0) DEFAULT 0,
v_strContactId TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_numProjectTeamId > 0 then
      DELETE FROM ProjectTeam WHERE numProjectTeamId = v_numProjectTeamId;
      IF SUBSTR(CAST(v_strContactId AS VARCHAR(10)),1,10) <> '' then

        INSERT INTo ProjectTeam
		(numProjectTeamId, numContactId)
        SELECT  v_numProjectTeamId,X.numContactId
        FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strContactId AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numContactId NUMERIC(18,0) PATH 'numContactId'
		) AS X;
      end if;
   end if;
   RETURN;
END; $$;








