-- Stored procedure definition script USP_ShippingService_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShippingService_Delete(v_numShippingServiceID NUMERIC(18,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingService WHERE numShippingServiceID = v_numShippingServiceID and numDomainID = v_numDomainID;
   RETURN;
END; $$;


