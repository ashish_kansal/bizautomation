-- Stored procedure definition script USP_UpdateSpecDocuments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateSpecDocuments(v_VcFileName VARCHAR(100),
v_numSpecificDocID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update SpecificDocuments set VcFileName = v_VcFileName
   where numSpecificDocID = v_numSpecificDocID;
   RETURN;
END; $$;


