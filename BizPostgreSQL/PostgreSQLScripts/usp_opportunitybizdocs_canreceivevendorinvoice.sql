-- Stored procedure definition script USP_OpportunityBizDocs_CanReceiveVendorInvoice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_CanReceiveVendorInvoice(v_numVendorInvoiceBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityBizDocItems OBDI
   INNER JOIN
   OpportunityItems OI
   ON
   OBDI.numOppItemID = OI.numoppitemtCode
   WHERE
   numOppBizDocID = v_numVendorInvoiceBizDocID
   AND(coalesce(OBDI.numUnitHour,0) -coalesce(OBDI.numVendorInvoiceUnitReceived,0)) >(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0))) > 0 then
	
      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


