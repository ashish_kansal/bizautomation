-- Stored procedure definition script USP_MassSalesFulfillment_UpdateOrderStauts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_UpdateOrderStauts(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_tintViewID SMALLINT
	,v_tintPackingMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numStatus  NUMERIC(18,0) DEFAULT 0;
   v_canUpdateStatus  BOOLEAN DEFAULT 0;
BEGIN
   IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) then
	
      select(CASE
      WHEN v_tintViewID = 1 THEN coalesce(numOrderStatusPicked,0)
      WHEN v_tintViewID = 2 AND v_tintPackingMode = 1 THEN coalesce(numOrderStatusPacked1,0)
      WHEN v_tintViewID = 2 AND v_tintPackingMode = 2 THEN coalesce(numOrderStatusPacked2,0)
      WHEN v_tintViewID = 2 AND v_tintPackingMode = 3 THEN coalesce(numOrderStatusPacked3,0)
      WHEN v_tintViewID = 2 AND v_tintPackingMode = 4 THEN coalesce(numOrderStatusPacked4,0)
      WHEN v_tintViewID = 3 THEN coalesce(numOrderStatusInvoiced,0)
      WHEN v_tintViewID = 4 THEN coalesce(numOrderStatusPaid,0)
      WHEN v_tintViewID = 5 THEN coalesce(numOrderStatusClosed,0)
      ELSE 0
      END) INTO v_numStatus FROM
      MassSalesFulfillmentConfiguration WHERE
      numDomainID = v_numDomainID;
   end if;

   IF v_tintViewID = 1 AND NOT EXISTS(SELECT
   numoppitemtCode
   FROM
   OpportunityItems
   WHERE
   numOppId = v_numOppID
   AND coalesce(numWarehouseItmsID,0) > 0
   AND coalesce(bitDropShip,false) = false
   AND coalesce(numUnitHour,0) <> coalesce(numQtyPicked,0)) then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 2 AND v_tintPackingMode = 1
   then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 2 AND v_tintPackingMode = 2 AND NOT EXISTS(SELECT
   numoppitemtCode
   FROM
   OpportunityItems
   WHERE
   numOppId = v_numOppID
   AND coalesce(numWarehouseItmsID,0) > 0
   AND coalesce(bitDropShip,false) = false
   AND coalesce(numUnitHour,0) <> coalesce((SELECT
      SUM(numUnitHour)
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      WHERE
      OBD.numoppid = OpportunityItems.numOppId
      AND OBD.numBizDocId = 296
      AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),
   0))
   then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 2 AND v_tintPackingMode = 4 AND NOT EXISTS(SELECT
   numoppitemtCode
   FROM
   OpportunityItems
   WHERE
   numOppId = v_numOppID
   AND coalesce(numWarehouseItmsID,0) > 0
   AND coalesce(bitDropShip,false) = false
   AND coalesce(numUnitHour,0) <> coalesce((SELECT
      SUM(SRI.intBoxQty)
      FROM
      ShippingReport SR
      INNER JOIN
      ShippingBox SB
      ON
      SR.numShippingReportID = SB.numShippingReportID
      INNER JOIN
      ShippingReportItems SRI
      ON
      SB.numBoxID = SRI.numBoxID
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID
      WHERE
      SR.numOppID = OpportunityItems.numOppId
      AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),0))
   then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 3 AND NOT EXISTS(SELECT
   numoppitemtCode
   FROM
   OpportunityItems
   WHERE
   numOppId = v_numOppID
   AND coalesce(numUnitHour,0) <> coalesce((SELECT
      SUM(numUnitHour)
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      WHERE
      OBD.numoppid = OpportunityItems.numOppId
      AND OBD.numBizDocId = 287
      AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),0))
   then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 4 AND EXISTS(SELECT
   numOppId
   FROM
   OpportunityMaster
   WHERE
   numOppId = v_numOppID
   AND monDealAmount = coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numoppid = OpportunityMaster.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0))
   then
	
      v_canUpdateStatus := true;
   ELSEIF v_tintViewID = 5
   then
	
      v_canUpdateStatus := true;
   end if;
	

   IF coalesce(v_numStatus,0) > 0 AND v_canUpdateStatus = true then
	
      UPDATE
      OpportunityMaster
      SET
      numStatus = v_numStatus
      WHERE
      numDomainId = v_numDomainID
      AND numOppId = v_numOppID;
   end if;
   RETURN;
END; $$;


