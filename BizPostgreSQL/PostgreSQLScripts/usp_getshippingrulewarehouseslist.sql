-- Stored procedure definition script USP_GetShippingRuleWarehousesList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingRuleWarehousesList(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcWareHouse AS "vcWareHouse",numWareHouseID AS "numWareHouseID" FROM Warehouses W
   WHERE numDomainID = v_numDomainID;
END; $$;












