-- Stored procedure definition script USP_GetSiteMenu for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSiteMenu(v_numMenuID NUMERIC(9,0),
          v_numSiteID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numMenuID as VARCHAR(255)),
         cast(vcTitle as VARCHAR(255)),
         cast(CASE
   WHEN numPageID > 0 THEN(SELECT cast(vcPageURL as VARCHAR(255))
         FROM   SitePages
         WHERE  numPageID = SM.numPageID)
   ELSE vcNavigationURL
   END as VARCHAR(255)) AS vcNavigationURL,
         cast(coalesce(numPageID,0) as VARCHAR(255)) AS numPageID,
         cast(tintLevel as VARCHAR(255)),
         cast(numParentID as VARCHAR(255)),
         cast(numSiteID as VARCHAR(255)),
         cast(numDomainID as VARCHAR(255)),
         cast(bitStatus as VARCHAR(255)),
         cast(intDisplayOrder as VARCHAR(255))
   FROM   SiteMenu SM
   WHERE  (numMenuID = v_numMenuID
   OR v_numMenuID = 0)
   AND numSiteID = v_numSiteID
   ORDER BY SM.intDisplayOrder Asc;
END; $$;














