-- Stored procedure definition script USP_DashboardTemplate_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DashboardTemplate_Save(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_bitUpdate BOOLEAN
	,v_numTemplateID NUMERIC(18,0)
	,v_vcTemplateName VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_NewTemplateID  NUMERIC(18,0);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF v_bitUpdate = false then
         INSERT INTO DashboardTemplate(numDomainID
			,vcTemplateName
			,dtCreatedDate
			,numCreatedBy)
		VALUES(v_numDomainID
			,v_vcTemplateName
			,TIMEZONE('UTC',now())
			,v_numUserCntID);
		
         v_NewTemplateID := CURRVAL('DashboardTemplate_seq');
         INSERT INTO DashboardTemplateReports(numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth)
         SELECT
         numDomainID
			,v_NewTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
         FROM
         ReportDashboard
         WHERE
         numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID
         AND coalesce(numDashboardTemplateID,0) = coalesce(v_numTemplateID,0);
         DELETE FROM ReportDashboard WHERE numDomainID = v_numDomainID AND coalesce(numDashboardTemplateID,0) = coalesce(v_numTemplateID,0) AND numUserCntID = v_numUserCntID AND coalesce(bitNewAdded,false) = true;
      ELSE
         UPDATE DashboardTemplate SET dtModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numTemplateID = v_numTemplateID;
         DELETE FROM DashboardTemplateReports WHERE numDashboardTemplateID = v_numTemplateID;
         INSERT INTO DashboardTemplateReports(numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth)
         SELECT
         numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
         FROM
         ReportDashboard
         WHERE
         numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID
         AND numDashboardTemplateID = v_numTemplateID;
         UPDATE ReportDashboard SET bitNewAdded = false WHERE numDomainID = v_numDomainID AND numDashboardTemplateID = v_numTemplateID AND numUserCntID = v_numUserCntID;

		-- DELETE DASHBOARD REPORT CONFIGURATION FOR ALL USERS WHO ARE USING EDITED DASHBOARD TEMPLATE EXCEPT CURRENT USER(@numUserCntID)
         DELETE FROM
         ReportDashboard
         WHERE
         numDomainID = v_numDomainID
         AND numDashboardTemplateID = v_numTemplateID
         AND numUserCntID IN(SELECT numUserDetailId FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId <> v_numUserCntID);
         INSERT INTO ReportDashboard(numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID)
         SELECT
         numDomainID
			,numReportID
			,numUserDetailId
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
         FROM
         DashboardTemplateReports
         CROSS JOIN LATERAL(SELECT
            numUserDetailId
            FROM
            UserMaster
            WHERE
            numDomainID = v_numDomainID
            AND numDashboardTemplateID = v_numTemplateID
            AND numUserDetailId <> v_numUserCntID) DashboardTemplateAssignee
         WHERE
         numDomainID = v_numDomainID
         AND numDashboardTemplateID = v_numTemplateID;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION

   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


