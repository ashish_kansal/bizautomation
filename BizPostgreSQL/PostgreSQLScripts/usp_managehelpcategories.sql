-- Stored procedure definition script USP_ManageHelpCategories for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageHelpCategories(INOUT v_numHelpCategoryID NUMERIC DEFAULT 0 ,
	v_vcCatecoryName VARCHAR(100) DEFAULT NULL,
	v_bitLinkPage BOOLEAN DEFAULT NULL,
	v_numPageID NUMERIC DEFAULT NULL,
	v_numParentCatID NUMERIC DEFAULT NULL,
	v_tintLevel SMALLINT DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numHelpCategoryID = 0 then 
      INSERT INTO HelpCategories(vcCatecoryName,
		bitLinkPage,
		numPageID,
		numParentCatID,
		tintLevel)
	VALUES(v_vcCatecoryName,
		v_bitLinkPage,
		v_numPageID,
		v_numParentCatID,
		v_tintLevel);
	
      v_numHelpCategoryID := CURRVAL('HelpCategories_seq');
   ELSE
      UPDATE HelpCategories SET
      vcCatecoryName = v_vcCatecoryName,bitLinkPage = v_bitLinkPage,numPageID = v_numPageID,
      numParentCatID = v_numParentCatID,tintLevel = v_tintLevel
      WHERE numHelpCategoryID = v_numHelpCategoryID;
   end if;
   RETURN;
END; $$;


