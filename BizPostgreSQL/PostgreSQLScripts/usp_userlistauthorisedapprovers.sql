-- Stored procedure definition script USP_UserListAuthorisedApprovers for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UserListAuthorisedApprovers(v_UserName VARCHAR(100) DEFAULT '',                  
 v_tintGroupType SMALLINT DEFAULT NULL,                  
 v_SortChar CHAR(1) DEFAULT '0',                  
 v_CurrentPage INTEGER DEFAULT NULL,                  
 v_PageSize INTEGER DEFAULT NULL,                  
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                  
 v_columnName VARCHAR(50) DEFAULT NULL,                  
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,            
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,          
 v_Status CHAR(1) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                  
   v_firstRec  INTEGER;                   
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numUserID VARCHAR(15),
      vcEmailID VARCHAR(100),
      Name VARCHAR(100),
      vcGroupName VARCHAR(100),
      vcDomainName VARCHAR(50),
      Active VARCHAR(10),
      numDefaultWarehouse NUMERIC(18,0)
   );                  
   v_strSql := 'SELECT numUserID, coalesce(vcEmailID,''-'') as vcEmailID,                  
coalesce(ADC.vcFirstName,'''') || '' '' || coalesce(ADC.vcLastName,'''') as Name,                  
coalesce(vcGroupName,''-'') as vcGroupName, vcDomainName, case when bitActivateFlag = true then ''Active'' else ''Suspended'' end as bitActivateFlag, numDefaultWarehouse                          
   FROM UserMaster                    
   join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactId = UserMaster.numUserDetailId                   
   left join AuthenticationGroupMaster GM                   
   on GM.numGroupID = UserMaster.numGroupID              
    where  UserMaster.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and bitActivateFlag =' || CASE WHEN coalesce(v_Status,'') = '1' THEN 'true' ELSE 'false' END || ' and numContactId in(select numLevel1Authority from ApprovalConfig where numLevel1Authority <> 0 AND numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ')';               
                  
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And (vcUserName ilike ''' || coalesce(v_SortChar,'') || '%'' OR vcEmailID ilike ''' || coalesce(v_SortChar,'') || '%'')';
   end if;

   if v_UserName <> '' then  
      v_strSql := coalesce(v_strSql,'') || ' And (vcUserName ilike ''' || coalesce(v_UserName,'') || '%'' OR vcEmailID ilike ''' || coalesce(v_UserName,'') || '%'')';
   end if;

   if v_columnName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                  
   RAISE NOTICE '%',v_strSql;               
   EXECUTE 'insert into tt_TEMPTABLE(                  
 numUserID, 
      vcEmailID,                  
      Name,                  
      vcGroupName,                  
 vcDomainName,Active,numDefaultWarehouse)                  
' || v_strSql;                   
                  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                  
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                  
   open SWV_RefCur for
   select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;                  
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                  
   RETURN;
END; $$;            
          

