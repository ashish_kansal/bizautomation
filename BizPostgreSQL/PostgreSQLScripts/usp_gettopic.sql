-- Stored procedure definition script USP_GetTopic for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTopic(v_intRecordType INTEGER DEFAULT 0,
	v_numRecordId NUMERIC(18,0) DEFAULT NULL,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_bitExternalUser BOOLEAN DEFAULT false,
	v_numTopicId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dynamicQuery  TEXT DEFAULT '';
BEGIN
   v_dynamicQuery := ' select 
			numTopicId AS "numTopicId", 
			intRecordType AS "intRecordType", 
			numRecordId AS "numRecordId", 
			vcTopicTitle AS "vcTopicTitle", 
			numCreateBy AS "numCreateBy", 
			dtmCreatedOn AS "dtmCreatedOn", 
			numUpdatedBy AS "numUpdatedBy", 
			dtmUpdatedOn AS "dtmUpdatedOn", 
			bitIsInternal AS "bitIsInternal", 
			TC.numDomainId AS "numDomainId",
			(select Count(*) FROM MessageMaster where numTopicId = TC.numTopicId) AS "MessageCount",
			fn_GetContactName(TC.numCreateBy) AS "CreatedBy",
			fn_GetContactName(TC.numUpdatedBy) AS "UpdatedBy",
			C.vcCompanyName AS "CreatedByOrganization",
			CUP.vcCompanyName AS "UpdatedByOrganization",
			coalesce(LI.vcData,''-'') AS "CreatedByDesignation",
			coalesce(LIUP.vcData,''-'') AS "UpdatedByDesignation",
			(select
				   distinct  
					COALESCE((
						select string_agg(TM.vcAttachmentName || ''$@:#'' || CAST(TM.numAttachmentId AS VARCHAR(100)),'','' order by TM.vcAttachmentName)
						from TopicMessageAttachments TM
						where TM.vcAttachmentName = vcAttachmentName AND numTopicId = TC.numTopicId
					),'''') as userlist
				from TopicMessageAttachments WHERE numTopicId = TC.numTopicId
				group by vcAttachmentName) AS "vcAttachmentName"
			From TopicMaster AS TC
			LEFT JOIN AdditionalContactsInformation AS ADC ON ADC.numContactId = TC.numCreateBy
			LEFT JOIN AdditionalContactsInformation AS ADCUP ON ADCUP.numContactId = TC.numUpdatedBy
			LEFT JOIN DivisionMaster AS Div ON Div.numDivisionID = ADC.numDivisionId
			LEFT JOIN DivisionMaster AS DivUP ON DivUP.numDivisionID = ADCUP.numDivisionId
			LEFT JOIN CompanyInfo as C on Div.numCompanyID = C.numCompanyId
			LEFT JOIN CompanyInfo as CUP on DivUP.numCompanyID = CUP.numCompanyId
			LEFT JOIN ListDetails AS LI ON LI.numListItemID = ADC.vcPosition
			LEFT JOIN ListDetails AS LIUP ON LIUP.numListItemID = ADCUP.vcPosition ';
   v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' Where TC.numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(500)),1,500) || ' ';
   IF(v_numTopicId > 0) then
		
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND numTopicId = ' || SUBSTR(CAST(v_numTopicId AS VARCHAR(500)),1,500) || '';
   end if;
   IF(v_numRecordId > 0) then
		
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND numRecordId = ' || SUBSTR(CAST(v_numRecordId AS VARCHAR(500)),1,500) || '';
   end if;
   IF(v_intRecordType > 0) then
		
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND intRecordType = ' || SUBSTR(CAST(v_intRecordType AS VARCHAR(500)),1,500) || '';
   end if;
   IF(v_bitExternalUser = true) then
		
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND coalesce(bitIsInternal,false) = false ';
   end if;
   v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' 	 ORDER BY dtmCreatedOn desc  ';
   OPEN SWV_RefCur FOR EXECUTE v_dynamicQuery;
   RETURN;
END; $$;


