-- Stored procedure definition script USP_ElasticSearchBizCart_GetItemCustomField for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ElasticSearchBizCart_GetItemCustomField(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   open SWV_RefCur for SELECT
   Fld_id as numFormFieldId
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID
   WHEN 4 then 'C'
   WHEN 1 THEN 'D'
   WHEN 9 THEN CAST(Fld_id AS VARCHAR(15))
   ELSE 'C'
   END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,coalesce(C.numlistid,0) AS numListID
		,CASE
   WHEN C.numlistid > 0 THEN 'L'
   ELSE ''
   END AS vcListItemType
		,ROW_NUMBER() OVER(ORDER BY Fld_id ASC) AS intRowNum
   FROM
   CFW_Fld_Master C
   LEFT JOIN
   CFW_Validation V
   ON
   V.numFieldId = C.Fld_id
   WHERE
   C.numDomainID = v_numDomainId
   AND GRP_ID IN(5,9);
END; $$;












