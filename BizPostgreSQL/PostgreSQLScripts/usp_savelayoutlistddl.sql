-- Stored procedure definition script USP_SaveLayoutListddl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveLayoutListddl(v_numUserCntID NUMERIC DEFAULT 0,                          
 v_numDomainID NUMERIC DEFAULT 0,                 
v_strRow TEXT DEFAULT '',              
v_intcoulmn NUMERIC DEFAULT 0,          
v_Ctype CHAR DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	delete  FROM PageLayoutDTL where numUserCntID = v_numUserCntID and numDomainID = v_numDomainID and intCoulmn >= v_intcoulmn  and numFieldId  in(select numFieldId from PageLayout where Ctype = v_Ctype);              
                
	if SUBSTR(CAST(v_strRow AS VARCHAR(10)),1,10) <> '' then
		insert into PageLayoutDTL
		(
			numFieldId,tintRow,intCoulmn,numUserCntID,numDomainID,bitCustomField
		)
		select 
			numFieldId,tintRow,intColumn,numUserCntID,numDomainID,bitCustomField
		from
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFieldID NUMERIC(18,0) PATH 'numFieldID',
				tintRow NUMERIC(9,0) PATH 'tintRow',
				intColumn NUMERIC(9,0) PATH 'intColumn',
				numUserCntId NUMERIC(9,0) PATH 'numUserCntId',
				numDomainID NUMERIC(9,0) PATH 'numDomainID',
				bitCustomField BOOLEAN PATH 'bitCustomField'
		) AS X;
	end if;                  
 
   RETURN;
END; $$;


