DROP FUNCTION IF EXISTS USP_GetPromotionOfferDetailsForOrder;

CREATE OR REPLACE FUNCTION USP_GetPromotionOfferDetailsForOrder(v_numPromotionID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   numProId
		,vcProName
		,numDomainId
		,(CASE WHEN dtValidFrom IS NOT NULL AND coalesce(bitNeverExpires,false) = false THEN dtValidFrom+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE dtValidFrom END) AS dtValidFrom
		,(CASE WHEN dtValidTo IS NOT NULL AND coalesce(bitNeverExpires,false) = false THEN dtValidTo+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE dtValidTo END) AS dtValidTo
		,bitNeverExpires
		,bitApplyToInternalOrders
		,bitAppliesToSite
		,bitRequireCouponCode
		,COALESCE((SELECT string_agg(vcDiscountCode,',')  FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId),'') AS vcDiscountCode
		,coalesce((SELECT  CodeUsageLimit FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId LIMIT 1),0) AS CodeUsageLimit
		,numCreatedBy
		,dtCreated
		,numModifiedBy
		,dtModified
		,tintDiscountType
		,IsOrderBasedPromotion
		,coalesce(bitUseForCouponManagement,false) AS bitUseForCouponManagement
   FROM
   PromotionOffer
   WHERE
   numProId = v_numPromotionID AND numDomainId = v_numDomainID;

   open SWV_RefCur2 for
   SELECT numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = v_numPromotionID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;



