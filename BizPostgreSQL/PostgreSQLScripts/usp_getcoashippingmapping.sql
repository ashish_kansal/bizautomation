-- Stored procedure definition script USP_GetCOAShippingMapping for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCOAShippingMapping(v_numShippingMethodID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   open SWV_RefCur for SELECT  cast(numShippingMappingID as VARCHAR(255)),
            cast(numShippingMethodID as VARCHAR(255)),
            cast(numIncomeAccountID as VARCHAR(255)),
            COA.vcAccountName,
             cast(SM.numDomainID as VARCHAR(255))
   FROM    COAShippingMapping SM
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = SM.numIncomeAccountID
   WHERE   numShippingMethodID = v_numShippingMethodID
   AND SM.numDomainID = v_numDomainID;

   RETURN;
END; $$;












