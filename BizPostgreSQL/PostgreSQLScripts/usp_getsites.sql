-- Stored procedure definition script USP_GetSites for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSites(v_numSiteID   NUMERIC(9,0)  DEFAULT 0,
          v_numDomainID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numSiteID,
         vcSiteName,
         vcDescription,
         vcHostName,
         numDomainID,
         bitIsActive,
		 CASE WHEN bitIsActive = true THEN 'Inactivate'
   ELSE 'Activate'
   END AS IsActive,
		 coalesce(vcLiveURL,'') AS vcLiveURL,
		 CASE LENGTH(coalesce(vcLiveURL,'')) WHEN 0 THEN  coalesce(vcHostName,'') || '.bizautomation.com' ELSE vcLiveURL END AS vcPreviewURL,coalesce(numCurrencyID,0) AS numCurrencyID,
		 coalesce(bitOnePageCheckout,false) AS bitOnePageCheckout,
		 coalesce(tintRateType,0) AS tintRateType,
		COALESCE((SELECT string_agg(CAST(numCategoryID AS VARCHAR),',') FROM SiteCategories WHERE numSiteID = v_numSiteID),'') AS vcItemCategories,
		bitHtml5,
		vcMetaTags,
		bitSSLRedirect
   FROM   Sites
   WHERE  (numSiteID = v_numSiteID
   OR v_numSiteID = 0)
   AND numDomainID = v_numDomainID;
END; $$;

