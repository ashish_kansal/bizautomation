-- Stored procedure definition script USP_GetSimilarRequiredItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSimilarRequiredItem(v_numDomainID NUMERIC(9,0),
	v_numParentItemCode NUMERIC(9,0),
	v_numWarehouseID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT I.vcItemName AS vcItemName,I.txtItemDesc AS txtItemDesc,I.numSaleUnit,coalesce(I.numContainer,0) AS numContainer,coalesce(I.bitAllowDropShip,false) AS bitAllowDropShip
	,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID, coalesce(vcRelationship,'') AS vcRelationship
	,ROUND(coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0),2) AS monListPrice
	,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as fltUOMConversionFactor, I.vcSKU, I.vcManufacturer
	,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc, coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell
	,coalesce(bitRequired,false) AS bitRequired, coalesce(vcUpSellDesc,'') AS vcUpSellDesc, fn_GetUOMName(I.numSaleUnit) AS vcUOMName, coalesce(numOnHand,0) AS numOnHand
	,(SELECT  CASE WHEN P.tintDiscountType = 1
      THEN(I.monListPrice*fltDiscountValue)/100
      WHEN P.tintDiscountType = 2
      THEN fltDiscountValue
      WHEN P.tintDiscountType = 3
      THEN fltDiscountValue*I.monListPrice
      ELSE 0 END
      FROM PromotionOffer AS P
      LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
	--WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID 
      WHERE
	(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
      I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
      AND P.numDomainId = v_numDomainID
      AND coalesce(bitEnabled,false) = true
      AND coalesce(bitAppliesToSite,false) = true
      AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
      AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
      AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers,SI.numParentItemCode
   FROM SimilarItems SI --Category 
   INNER JOIN Item I ON I.numItemCode = SI.numItemCode
   LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode
   LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID
   LEFT JOIN LATERAL(SELECT * FROM
      WareHouseItems
      WHERE
      numItemID = I.numItemCode
      AND (numWareHouseID = v_numWarehouseID OR coalesce(v_numWarehouseID,0) = 0) LIMIT 1) AS W on TRUE
   WHERE
   SI.numDomainId = v_numDomainID
   AND SI.numParentItemCode = v_numParentItemCode
   AND bitRequired = true
   AND 1 =(CASE WHEN I.charItemType = 'P' THEN(CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END); 
   RETURN;
END; $$;

