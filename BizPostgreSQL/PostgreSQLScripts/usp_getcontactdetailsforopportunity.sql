-- Stored procedure definition script usp_GetContactDetailsForOpportunity for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactDetailsForOpportunity(v_numOppId NUMERIC
--   
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numContactID as VARCHAR(255)) FROM OpportunityContact WHERE numOppId = v_numOppId;
END; $$;












