-- Stored procedure definition script USP_DeleteInktomi for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteInktomi()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTrackinID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numTrackinID := 0;
   select   numTrackingID INTO v_numTrackinID from TrackingVisitorsHDR where (vcUserDomain ilike '%inktomisearch.com' or vcUserDomain ilike '%ask.com'  or vcUserDomain ilike  '%yahoo.net' or vcUserDomain ilike  '%chello.nl' or vcUserDomain ilike  '%linksmanager.com' or vcUserDomain ilike  '%googlebot.com') and vcCrawler = 'False'    LIMIT 1;
   while v_numTrackinID > 0 LOOP
      update TrackingVisitorsHDR set vcCrawler = 'True' where numTrackingID = v_numTrackinID;
      select   numTrackingID INTO v_numTrackinID from TrackingVisitorsHDR where (vcUserDomain ilike '%inktomisearch.com' or vcUserDomain ilike '%ask.com'  or vcUserDomain ilike  '%yahoo.net' or vcUserDomain ilike  '%chello.nl' or vcUserDomain ilike  '%linksmanager.com' or vcUserDomain ilike  '%googlebot.com') and vcCrawler = 'False' and numTrackingID > v_numTrackinID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numTrackinID := 0;
      end if;
   END LOOP;
   RETURN;
END; $$;


