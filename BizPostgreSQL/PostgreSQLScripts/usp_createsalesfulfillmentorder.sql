-- Stored procedure definition script USP_CreateSalesFulfillmentOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateSalesFulfillmentOrder(v_numDomainID NUMERIC(9,0),
      v_numOppID NUMERIC(9,0),
      v_numUserCntID NUMERIC(9,0),
      v_numBizDocStatus NUMERIC(9,0),
      v_tintMode SMALLINT,
	  INOUT v_numOppAuthBizDocsId NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppBizDocsId  NUMERIC(9,0);
   v_numPackingSlipBizDocId  NUMERIC(9,0);
   v_numAuthInvoice  NUMERIC(9,0);
   v_UTCDATE  TIMESTAMP;
		
		--Create FulfillmentBizDocs
   v_numInventoryInvoice  NUMERIC(9,0);
   v_numNonInventoryFOInvoice  NUMERIC(9,0);
   v_numServiceFOInvoice  NUMERIC(9,0);
		
   v_ErrorMessage  VARCHAR(4000);
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_numBizDocStatusReleaseInventory  NUMERIC(9,0);
					
					
   v_USP_CreateBizDocs_monCreditAmount DECIMAL(20,5);
   v_USP_CreateBizDocs_monDealAmount DECIMAL(20,5);
   v_USP_CreateBizDocs_numOppBizDocsId NUMERIC(9,0);
BEGIN
   BEGIN --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  varchar(1000)
								 --  bit
								 --  text
								--@monShipCost = 0, --  DECIMAL(20,5)
								 --  numeric(9, 0)
								 --  varchar(1000)
								 --  datetime
								 --  numeric(9, 0)
								 --  bit
								 --  varchar(50)
								 --  tinyint
								 --  int
								 --  bit
								 --  numeric(9, 0)
								 --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								 --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								 --  float
								 --  numeric(9, 0)
								  --bit
								 --bit
				
		
      v_numOppBizDocsId := 0;
      v_UTCDATE := TIMEZONE('UTC',now());
		
		--Create FulfillmentBizDocs
      select   coalesce(numBizDocTypeID,0) INTO v_numInventoryInvoice FROM OpportunityFulfillmentBizDocs WHERE numDomainID = v_numDomainID AND ItemType = 'P';
      select   coalesce(numBizDocTypeID,0) INTO v_numNonInventoryFOInvoice FROM OpportunityFulfillmentBizDocs WHERE numDomainID = v_numDomainID AND ItemType = 'N';
      select   coalesce(numBizDocTypeID,0) INTO v_numServiceFOInvoice FROM OpportunityFulfillmentBizDocs WHERE numDomainID = v_numDomainID AND ItemType = 'S';
		
		--Create Auth BizDocs
      select   coalesce(AuthoritativeBizDocs.numAuthoritativeSales,0) INTO v_numAuthInvoice From AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId = v_numDomainID; 
			
		--Create Packing Slip
      select   Listdetails.numListItemID INTO v_numPackingSlipBizDocId FROM Listdetails WHERE Listdetails.vcData = 'Packing Slip' AND Listdetails.constFlag = 1 AND Listdetails.numListID = 27;
      IF v_numInventoryInvoice > 0 then
		
				 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  bit
							 --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  datetime
							 --  numeric(9, 0)
							 --  bit
							 --  varchar(50)
							 --  tinyint
							 --  int
							 --  bit
							 --  numeric(9, 0)
							 --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							 --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							 --  float
							 --  numeric(9, 0)
         v_USP_CreateBizDocs_monCreditAmount := 0;
         v_USP_CreateBizDocs_monDealAmount := 0;
         SELECT * INTO v_numOppBizDocsId FROM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numInventoryInvoice,v_numUserCntID := v_numUserCntID,
         v_numOppBizDocsId := v_numOppBizDocsId,v_vcComments := '',
         v_bitPartialFulfillment := true,v_strBizDocItems := '',
         v_numShipVia := 0,v_vcTrackingURL := '',v_dtFromDate := v_UTCDATE,v_numBizDocStatus := 0,
         v_bitRecurringBizDoc := false,v_numSequenceId := '',
         v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
         v_ClientTimeZoneOffset := 0,v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,
         v_bitRentalBizDoc := false,v_numBizDocTempID := 0,
         v_vcTrackingNo := '',v_vcRefOrderNo := '',v_OMP_SalesTaxPercent := 0,
         v_numFromOppBizDocsId := 0,v_bitTakeSequenceId := true);
      end if;
      IF v_numInventoryInvoice != v_numNonInventoryFOInvoice AND v_numNonInventoryFOInvoice > 0 then
			
				 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  bit
							 --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  datetime
							 --  numeric(9, 0)
							 --  bit
							 --  varchar(50)
							 --  tinyint
							 --  int
							 --  bit
							 --  numeric(9, 0)
							 --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							 --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							 --  float
							 --  numeric(9, 0)
         v_USP_CreateBizDocs_numOppBizDocsId := 0;
         v_USP_CreateBizDocs_monCreditAmount := 0;
         v_USP_CreateBizDocs_monDealAmount := 0;
         PERFORM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numNonInventoryFOInvoice,v_numUserCntID := v_numUserCntID,
         v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
         v_vcComments := '',v_bitPartialFulfillment := true,
         v_strBizDocItems := '',v_numShipVia := 0,v_vcTrackingURL := '',v_dtFromDate := v_UTCDATE,
         v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,
         v_numSequenceId := '',v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
         v_ClientTimeZoneOffset := 0,v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,
         v_bitRentalBizDoc := false,
         v_numBizDocTempID := 0,v_vcTrackingNo := '',v_vcRefOrderNo := '',
         v_OMP_SalesTaxPercent := 0,v_numFromOppBizDocsId := 0,v_bitTakeSequenceId := true);
      end if;
      IF v_numInventoryInvoice != v_numServiceFOInvoice AND v_numNonInventoryFOInvoice != v_numServiceFOInvoice AND v_numServiceFOInvoice > 0 then
				
						 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  bit
							 --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  datetime
							 --  numeric(9, 0)
							 --  bit
							 --  varchar(50)
							 --  tinyint
							 --  int
							 --  bit
							 --  numeric(9, 0)
							 --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							 --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							 --  float
							 --  numeric(9, 0)
         v_USP_CreateBizDocs_numOppBizDocsId := 0;
         v_USP_CreateBizDocs_monCreditAmount := 0;
         v_USP_CreateBizDocs_monDealAmount := 0;
         PERFORM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numServiceFOInvoice,v_numUserCntID := v_numUserCntID,
         v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
         v_vcComments := '',v_bitPartialFulfillment := true,v_strBizDocItems := '',
         v_numShipVia := 0,v_vcTrackingURL := '',v_dtFromDate := v_UTCDATE,
         v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,v_numSequenceId := '',
         v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
         v_ClientTimeZoneOffset := 0,v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,
         v_bitRentalBizDoc := false,
         v_numBizDocTempID := 0,v_vcTrackingNo := '',v_vcRefOrderNo := '',v_OMP_SalesTaxPercent := 0,
         v_numFromOppBizDocsId := 0,v_bitTakeSequenceId := true);
      end if;
					 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  bit
							 --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							 --  numeric(9, 0)
							 --  varchar(1000)
							 --  datetime
							 --  numeric(9, 0)
							 --  bit
							 --  varchar(50)
							 --  tinyint
							 --  int
							 --  bit
							 --  numeric(9, 0)
							 --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							 --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							 --  float
							 --  numeric(9, 0)
							  --bit
      IF v_tintMode = 1 then
					
						--Upadte FulfillmentBizDocs Status ro Release Allocation
         v_numBizDocStatusReleaseInventory := 0;
         select   coalesce(numBizDocStatus,0) INTO v_numBizDocStatusReleaseInventory FROM OpportunityFulfillmentRules WHERE numDomainID = v_numDomainID AND numRuleID = 3;
         IF coalesce(v_numBizDocStatusReleaseInventory,0) > 0 then
				 		
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  numeric(18, 0)
									 --  numeric(18, 0)
									 --  text
            PERFORM USP_SalesFulfillmentWorkflow(v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,v_numOppBizDocsId := v_numOppBizDocsId,
            v_numUserCntID := v_numUserCntID,v_numBizDocStatus := v_numBizDocStatusReleaseInventory); --  numeric(9, 0)
							
            PERFORM USP_UpdateSingleFieldValue(v_tintMode := 27::SMALLINT,v_numUpdateRecordID := v_numOppBizDocsId,v_numUpdateValueID := v_numBizDocStatusReleaseInventory,
            v_vcText := '',v_numDomainID := v_numDomainID);
         end if;
      ELSEIF v_tintMode = 2
      then
					
							 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  numeric(18, 0)
									 --  numeric(18, 0)
									 --  text
         PERFORM USP_SalesFulfillmentWorkflow(v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,v_numOppBizDocsId := v_numOppBizDocsId,
         v_numUserCntID := v_numUserCntID,v_numBizDocStatus := v_numBizDocStatus); --  numeric(9, 0)
							
         PERFORM USP_UpdateSingleFieldValue(v_tintMode := 27::SMALLINT,v_numUpdateRecordID := v_numOppBizDocsId,v_numUpdateValueID := v_numBizDocStatus,
         v_vcText := '',v_numDomainID := v_numDomainID);
      end if;
      v_USP_CreateBizDocs_monCreditAmount := 0;
      v_USP_CreateBizDocs_monDealAmount := 0;
      SELECT * INTO v_numOppAuthBizDocsId FROM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numAuthInvoice,v_numUserCntID := v_numUserCntID,
      v_numOppBizDocsId := v_numOppAuthBizDocsId,v_vcComments := '',
      v_bitPartialFulfillment := true,v_strBizDocItems := '',v_numShipVia := 0,
      v_vcTrackingURL := '',v_dtFromDate := '2013-1-2 10:55:29.610',
      v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,v_numSequenceId := '',
      v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
      v_ClientTimeZoneOffset := 0,v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,
      v_bitRentalBizDoc := false,v_numBizDocTempID := 0,
      v_vcTrackingNo := '',v_vcRefOrderNo := '',v_OMP_SalesTaxPercent := 0,
      v_numFromOppBizDocsId := v_numOppBizDocsId,v_bitTakeSequenceId := true,
      v_bitAllItems := true); --bit
					
      v_USP_CreateBizDocs_numOppBizDocsId := 0;
      v_USP_CreateBizDocs_monCreditAmount := 0;
      v_USP_CreateBizDocs_monDealAmount := 0;
      PERFORM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numPackingSlipBizDocId,v_numUserCntID := v_numUserCntID,
      v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
      v_vcComments := '',v_bitPartialFulfillment := true,v_strBizDocItems := '',
      v_numShipVia := 0,v_vcTrackingURL := '',v_dtFromDate := '2013-1-2 10:56:4.477',
      v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,
      v_numSequenceId := '',v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
      v_ClientTimeZoneOffset := 0,
      v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,v_bitRentalBizDoc := false,
      v_numBizDocTempID := 0,v_vcTrackingNo := '',v_vcRefOrderNo := '',
      v_OMP_SalesTaxPercent := 0,v_numFromOppBizDocsId := v_numOppBizDocsId,
      v_bitTakeSequenceId := true,v_bitAllItems := false);
      EXCEPTION WHEN OTHERS THEN
         v_ErrorMessage := SQLERRM;
         v_ErrorSeverity := ERROR_SEVERITY();
         v_ErrorState := ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
     -- Message text.
                -- Severity.
                -- State.
         RAISE NOTICE '%',v_ErrorMessage;
   END;
   RETURN;
END; $$;


