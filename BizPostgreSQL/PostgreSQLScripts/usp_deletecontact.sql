-- Stored procedure definition script usp_DeleteContact for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteContact(v_numContactID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql      
--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
   AS $$
   DECLARE
   v_Email  VARCHAR(1000); 
   v_AddEmail  VARCHAR(1000);

   v_bitPrimaryContact  BOOLEAN;   
   v_numDomainID  NUMERIC;
   v_error  VARCHAR(1000);
BEGIN
   select   vcEmail INTO v_Email FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
   select   vcAsstEmail INTO v_AddEmail FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
--------Date:17-Oct-2011 By:Pinkal Patel----------------------
   select   coalesce(bitPrimaryContact,false), numDomainID INTO v_bitPrimaryContact,v_numDomainID from AdditionalContactsInformation where numContactId = v_numContactID;    


   IF EXISTS(select * from  UserMaster where numUserDetailId = v_numContactID and numDomainID = v_numDomainID and bitactivateflag = true) then

      RAISE EXCEPTION 'USER';
      RETURN;
   end if;
   if v_bitPrimaryContact = true then    /*Deleting Primary Contact is not allowed*/

      RAISE EXCEPTION 'PRIMARY_CONTACT';
      RETURN;
   end if;
   IF EXISTS(select * from OpportunityMaster WHERE numContactId = v_numContactID) then
  
      RAISE EXCEPTION 'CHILD_OPP';
      RETURN;
   end if;
   IF EXISTS(SELECT * FROM Cases WHERE numContactId = v_numContactID) then
  
      RAISE EXCEPTION 'CHILD_CASE';
      RETURN;
   end if;
	BEGIN  
 
		DELETE FROM CompanyAssociations WHERE numDomainID = v_numDomainID AND numContactID = v_numContactID;
		DELETE FROM ImportActionItemReference WHERE numContactID = v_numContactID;
		delete FROM ConECampaignDTL where numConECampID in(select numConEmailCampID from ConECampaign where numContactID = v_numContactID);
		delete FROM ConECampaign where numContactID = v_numContactID;
		delete FROM AOIContactLink where numContactID = v_numContactID;
		delete FROM CaseContacts where numContactID = v_numContactID;
		delete FROM ProjectsContacts where numContactID = v_numContactID;
		delete FROM OpportunityContact where numContactID = v_numContactID;
		delete FROM UserTeams where numUserCntID = v_numContactID;
		delete FROM UserTerritory where numUserCntID = v_numContactID;
		DELETE FROM UserMaster WHERE numUserDetailId = v_numContactID AND bitactivateflag = false;
		DELETE FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
		--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
		--IF @Email <> ''
		--BEGIN 
      UPDATE EmailMaster SET numContactID = NULL WHERE vcEmailId = v_Email AND numContactID = v_numContactID;
		--END 
		--IF @AddEmail<>''
		--BEGIN 
      UPDATE EmailMaster SET numContactID = NULL WHERE vcEmailId = v_AddEmail AND numContactID = v_numContactID;
		--END 
		--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
      open SWV_RefCur for
      select 1;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK 
v_error := SQLERRM;
         RAISE EXCEPTION '%',v_error;
         RETURN;
   END;
END; $$;


