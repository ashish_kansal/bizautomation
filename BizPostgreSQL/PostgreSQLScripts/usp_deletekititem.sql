-- Stored procedure definition script USP_DeleteKitItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteKitItem(v_numItemDetailID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ItemDetails where numItemDetailID = v_numItemDetailID;
   RETURN;
END; $$;



