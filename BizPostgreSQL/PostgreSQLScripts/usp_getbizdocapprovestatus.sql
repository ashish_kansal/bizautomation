-- FUNCTION: public.usp_getbizdocapprovestatus(numeric, numeric, boolean, numeric, refcursor)

-- DROP FUNCTION public.usp_getbizdocapprovestatus(numeric, numeric, boolean, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbizdocapprovestatus(
	v_numbizdocid numeric DEFAULT 0,
	v_numdomainid numeric DEFAULT NULL::numeric,
	v_btdoctype boolean DEFAULT NULL::boolean,
	v_numstatus numeric DEFAULT NULL::numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:09:09 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for select  v_numBizDocID as numDocId,cast(A.numContactId as VARCHAR(255)),CAST('' AS CHAR(1)) as VcComment,v_numStatus as Status,cast(B.dtApprovedOn as VARCHAR(255)),CAST('' AS CHAR(1)) as cDocType,vcFirstName || ' ' || vcLastname as Name ,cast(vcCompanyName as VARCHAR(255))
   from BizDocAction A join BizActionDetails B
   on A.numBizActionId = B.numBizActionId join AdditionalContactsInformation C
   on C.numContactId = A.numContactId join DivisionMaster DM
   on DM.numDivisionID = C.numDivisionId  join CompanyInfo CI
   on CI.numCompanyId = DM.numCompanyID 
--join DocumentWorkflow DW
--		on DW.numDocId=numOppBizDocsId and DW.numContactID=A.numContactID			
   where B.numOppBizDocsId = v_numBizDocID and
   A.numDomainID = v_numDomainID and
  CASE WHEN B.btDocType=1 THEN true ELSE false END = v_btDocType and
   B.btStatus = v_numStatus
   Union
   select  v_numBizDocID  as numDocId,cast(A.numContactId as VARCHAR(255)),CAST('' AS CHAR(1)) as VcComment,v_numStatus as Status,cast(B.dtApprovedOn as VARCHAR(255)),CAST('' AS CHAR(1)) as cDocType,vcFirstName || ' ' || vcLastname as Name  ,cast(vcCompanyName as VARCHAR(255))
   from BizDocAction A join BizActionDetails B
   on A.numBizActionId = B.numBizActionId join AdditionalContactsInformation C
   on C.numContactId = A.numContactId join DivisionMaster DM
   on DM.numDivisionID = C.numDivisionId  join CompanyInfo CI
   on CI.numCompanyId = DM.numCompanyID
   where B.numOppBizDocsId = v_numBizDocID and
   A.numDomainID = v_numDomainID and
   CASE WHEN B.btDocType=1 THEN true ELSE false END = v_btDocType and
   B.btStatus = v_numStatus;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getbizdocapprovestatus(numeric, numeric, boolean, numeric, refcursor)
    OWNER TO postgres;
