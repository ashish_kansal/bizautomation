-- Stored procedure definition script USP_GetDivisionFromComName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivisionFromComName(v_vcCompanyName VARCHAR(100) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC;
BEGIN
   v_numDivisionID := 0;  
  
   select   numDivisionID INTO v_numDivisionID from CompanyInfo C
   join DivisionMaster D on
   C.numCompanyId = D.numCompanyID where vcWebSite ilike '%' || coalesce(v_vcCompanyName,'') || '%'    LIMIT 1; 
  
   open SWV_RefCur for select v_numDivisionID;
END; $$;












