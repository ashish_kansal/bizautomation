-- Stored procedure definition script USP_ManageAssetSerial for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAssetSerial(v_numDomainID NUMERIC(9,0),
    v_strFieldList TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   INSERT  INTO CompanyAssetSerial(numAssetItemID,
                  vcSerialNo,
                  numBarCodeId,
				  vcModelId,
				  dtPurchase,
				  dtWarrante,
				  vcLocation)SELECT    numAssetItemID,
                            vcSerialNo,
                            numBarCodeId,
							vcModelId,
							dtPurchase,
							dtWarrante,
							vcLocation
      FROM 
	   XMLTABLE
		(
			'NewDataSet/SerializedAsset'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				Op_Flag SMALLINT PATH 'Op_Flag',
				numAssetItemID NUMERIC(18,0) PATH 'numAssetItemID',
				vcSerialNo VARCHAR(100) PATH 'vcSerialNo',
				numBarCodeId NUMERIC(18,0) PATH 'numBarCodeId',
				vcModelId VARCHAR(100) PATH 'vcModelId',
				dtPurchase TIMESTAMP PATH 'dtPurchase',
				dtWarrante TIMESTAMP PATH 'dtWarrante',
				vcLocation VARCHAR(250) PATH 'vcLocation'
		) AS X
		WHERE
			COALESCE(Op_Flag,0) = 1;
                
                                                                       
                                              
   UPDATE  CompanyAssetSerial
   SET     numAssetItemID = X.numAssetItemID,vcSerialNo = X.vcSerialNo,numBarCodeId = X.numBarCodeId,
   vcModelId = X.vcModelId,dtPurchase = X.dtPurchase,dtWarrante = X.dtWarrante,
   vcLocation = X.vcLocation
   FROM
   XMLTABLE
		(
			'NewDataSet/SerializedAsset'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				Op_Flag SMALLINT PATH 'Op_Flag',
				numAssetItemDTLID NUMERIC(18,0) PATH 'numAssetItemDTLID',
				numAssetItemID NUMERIC(18,0) PATH 'numAssetItemID',
				vcSerialNo VARCHAR(100) PATH 'vcSerialNo',
				numBarCodeId NUMERIC(18,0) PATH 'numBarCodeId',
				vcModelId VARCHAR(100) PATH 'vcModelId',
				dtPurchase TIMESTAMP PATH 'dtPurchase',
				dtWarrante TIMESTAMP PATH 'dtWarrante',
				vcLocation VARCHAR(250) PATH 'vcLocation'
		) AS X
		WHERE
			COALESCE(Op_Flag,0) = 0
			AND CompanyAssetSerial.numAssetItemDTLID = X.numAssetItemDTLID;
   RETURN;
END; $$;
                      



