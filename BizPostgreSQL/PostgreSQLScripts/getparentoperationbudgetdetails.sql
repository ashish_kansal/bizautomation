-- Stored procedure definition script GetParentOperationBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetParentOperationBudgetDetails(v_numBudgetId NUMERIC(9,0) DEFAULT 0,                
v_numDomainId NUMERIC(9,0) DEFAULT 0,            
v_intFiscalYear INTEGER DEFAULT 0,            
v_sintByte SMALLINT DEFAULT 0,
v_intType INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Date  TIMESTAMP;
BEGIN
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);
   RETURN; 
--@sintByte =0 -> Operation Budget @sintByte=1 -> Market Budget @sintByte=2 -> Procurement Budget                  
--if @sintByte =0            
-- Begin             
-- Select distinct  OBD.numChartAcntId as numChartAcntId,COA.vcCatgyName as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(@numBudgetId,@Date,OBD.numChartAcntId,@sintByte,@numDomainId),0) as monAmount From OperationBudgetMaster OBM                  
-- inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId         
-- inner join Chart_of_Accounts COA on OBD.numChartAcntId=COA.numAccountId         
-- Where OBD.numBudgetId=@numBudgetId And OBM.numDomainId=@numDomainId                   
-- End            
--if @sintByte=1            
--Begin            
-- Select distinct MBD.numListItemID as numChartAcntId,LD.vcData as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(MBD.numMarketId,@Date,MBD.numListItemID,@sintByte,@numDomainId),0) as monAmount From MarketBudgetMaster MBM                  
-- inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId              
-- inner join ListDetails LD on MBD.numListItemID= LD.numListItemID Where -- MBD.intYear=@intFiscalYear And 
-- MBM.numDomainId=@numDomainId              
--End            
--if @sintByte=2            
--Begin           
--           
-- Select distinct PBD.numItemGroupId  as numChartAcntId,IG.vcItemGroup as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(PBM.numProcurementBudgetId,@Date,PBD.numItemGroupId,@sintByte,@numDomainId),0) as monAmount From ProcurementBudgetMaster PBM                  
-- inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId            
-- inner join ItemGroups IG on IG.numItemGroupID=PBD.numItemGroupId            
-- Where --PBD.intYear=@intFiscalYear And 
--PBM.numDomainId=@numDomainId              
--End            
END; $$;


