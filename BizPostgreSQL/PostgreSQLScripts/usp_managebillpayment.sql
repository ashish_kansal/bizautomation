CREATE OR REPLACE FUNCTION USP_ManageBillPayment(INOUT v_numBillPaymentID NUMERIC(18,0) DEFAULT 0 ,
    v_dtPaymentDate TIMESTAMP DEFAULT NULL ,
    v_numAccountID NUMERIC(18,0) DEFAULT NULL ,
    v_numPaymentMethod NUMERIC(18,0) DEFAULT NULL ,
    v_numUserCntID NUMERIC(18,0) DEFAULT NULL ,
    v_numDomainID NUMERIC(18,0) DEFAULT NULL ,
    v_strItems TEXT DEFAULT NULL,
    v_tintMode INTEGER DEFAULT 0,
    v_monPaymentAmount DECIMAL(20,5) DEFAULT 0,
    v_numDivisionID NUMERIC(18,0) DEFAULT NULL,
    v_numReturnHeaderID NUMERIC(18,0) DEFAULT NULL,
    v_numCurrencyID NUMERIC(18,0) DEFAULT 0,
    v_fltExchangeRate NUMERIC DEFAULT 1,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBillPaymentID_X  NUMERIC(18,0);
   v_dtPaymentDate_X  TIMESTAMP;
   v_numAccountID_X  NUMERIC(18,0);
   v_numPaymentMethod_X  NUMERIC(18,0);
   v_numUserCntID_X  NUMERIC(18,0);
   v_numDomainID_X  NUMERIC(18,0);
   v_strItems_X  TEXT;
   v_tintMode_X  SMALLINT DEFAULT 0;
   v_monPaymentAmount_X  DECIMAL(20,5);
   v_numDivisionID_X  NUMERIC(18,0);
   v_numReturnHeaderID_X  NUMERIC(18,0);
   v_numCurrencyID_X  NUMERIC(18,0);
   v_fltExchangeRate_X  DOUBLE PRECISION;
   v_numAccountClass_X  NUMERIC(18,0);

   v_hDocItem  INTEGER;
   v_strMsg  VARCHAR(200);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_numOppBizDocsId  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN

   v_numBillPaymentID_X := v_numBillPaymentID;
   v_dtPaymentDate_X := v_dtPaymentDate;
   v_numAccountID_X := v_numAccountID;
   v_numPaymentMethod_X := v_numPaymentMethod;
   v_numUserCntID_X := v_numUserCntID;
   v_numDomainID_X := v_numDomainID;
   v_strItems_X := v_strItems;
   v_tintMode_X := v_tintMode;
   v_monPaymentAmount_X := v_monPaymentAmount;
   v_numDivisionID_X := v_numDivisionID;
   v_numReturnHeaderID_X := v_numReturnHeaderID;
   v_numCurrencyID_X := v_numCurrencyID;
   v_fltExchangeRate_X := v_fltExchangeRate;
   v_numAccountClass_X := v_numAccountClass;
    
   BEGIN
      -- BEGIN TRAN
IF coalesce(v_numCurrencyID_X,0) = 0 then
			
         v_fltExchangeRate_X := 1;
         select   coalesce(numCurrencyID,0) INTO v_numCurrencyID_X FROM Domain WHERE numDomainId = v_numDomainID_X;
      end if;
      IF v_tintMode_X = 1 then
			
				--Validation of closed financial year
         PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID_X,v_dtPaymentDate_X);
         IF EXISTS(SELECT  numBillPaymentID FROM    BillPaymentHeader WHERE   numBillPaymentID = v_numBillPaymentID_X) then
					
            UPDATE  BillPaymentHeader
            SET     dtPaymentDate = v_dtPaymentDate_X,numAccountID = v_numAccountID_X,numPaymentMethod = v_numPaymentMethod_X,
            numModifiedDate = TIMEZONE('UTC',now()),
            numModifiedBy = v_numUserCntID_X,monPaymentAmount = v_monPaymentAmount_X,
            numCurrencyID = v_numCurrencyID_X,fltExchangeRate = v_fltExchangeRate_X
            WHERE   numBillPaymentID = v_numBillPaymentID_X AND numDomainId = v_numDomainID_X;
         ELSE
            INSERT  INTO BillPaymentHeader(dtPaymentDate ,numAccountID ,numPaymentMethod ,dtCreateDate ,numCreatedBy ,numModifiedDate ,
								  numModifiedBy,numDomainId,monPaymentAmount,numDivisionId,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass)
						VALUES(v_dtPaymentDate_X ,v_numAccountID_X ,v_numPaymentMethod_X ,TIMEZONE('UTC',now()) ,v_numUserCntID_X ,TIMEZONE('UTC',now()) ,
								  v_numUserCntID_X,v_numDomainID_X,v_monPaymentAmount_X,v_numDivisionID_X,v_numReturnHeaderID_X,v_numCurrencyID_X,v_fltExchangeRate_X,v_numAccountClass_X) RETURNING numBillPaymentID INTO v_numBillPaymentID;
						            
            v_numBillPaymentID_X := v_numBillPaymentID;
         end if;
      end if;
      IF SUBSTR(CAST(v_strItems_X AS VARCHAR(10)),1,10) <> '' then

--						INSERT  INTO [dbo].[BillPaymentDetails]
--								( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount])
         DROP TABLE IF EXISTS tt_TEMPManageBillPayment CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPManageBillPayment AS
            SELECT  X.tintBillType ,coalesce(X.numOppBizDocsID,0) AS  numOppBizDocsID,coalesce(X.numBillID,0) AS numBillID ,X.monAmount 
            FROM
			XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems_X AS XML)
			COLUMNS
				id FOR ORDINALITY,
				tintBillType SMALLINT PATH 'tintBillType',
				numOppBizDocsID NUMERIC(18,0) PATH 'numOppBizDocsID',
				numBillID NUMERIC(18,0) PATH 'numBillID',
				monAmount DECIMAL(20,5) PATH 'monAmount'
		) AS X;
      
         IF v_tintMode_X = 1 then
								
						--Revert back all amount for BillHeader
            UPDATE BillHeader BH SET monAmtPaid = monAmtPaid -BPD.monAmount,bitIsPaid = CASE WHEN monAmountDue -(monAmtPaid -BPD.monAmount) = 0 THEN true ELSE false END
            FROM BillPaymentDetails BPD
            WHERE(BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID) AND BPD.numBillPaymentID = v_numBillPaymentID_X; 
						 
						 --Revert back all amount for OpportunityBizDocs
            UPDATE OpportunityBizDocs OBD SET monAmountPaid = monAmountPaid -BPD.monAmount
            FROM BillPaymentDetails BPD
            WHERE(BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID) AND BPD.numBillPaymentID = v_numBillPaymentID_X;
						
						--Delete all BillPaymentDetails entries 				 
            DELETE FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X;
         end if;
         UPDATE BillPaymentDetails BPD SET monAmount = BPD.monAmount+X.monAmount,dtAppliedDate = TIMEZONE('UTC',now())
         FROM tt_TEMPManageBillPayment X
         WHERE BPD.numBillID = X.numBillID AND(BPD.numBillPaymentID = v_numBillPaymentID_X AND X.tintBillType = 2);
         UPDATE BillPaymentDetails BPD SET monAmount = BPD.monAmount+X.monAmount,dtAppliedDate = TIMEZONE('UTC',now())
         FROM tt_TEMPManageBillPayment X
         WHERE BPD.numOppBizDocsID = X.numOppBizDocsId AND(BPD.numBillPaymentID = v_numBillPaymentID_X AND X.tintBillType = 1);
         INSERT  INTO BillPaymentDetails(numBillPaymentID ,tintBillType ,numOppBizDocsID ,numBillID ,monAmount,dtAppliedDate)
         SELECT v_numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsId ,X.numBillID ,X.monAmount,TIMEZONE('UTC',now())
         FROM tt_TEMPManageBillPayment X WHERE X.tintBillType = 1 AND X.numOppBizDocsId NOT IN(SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X)
         UNION
         SELECT v_numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsId ,X.numBillID ,X.monAmount,TIMEZONE('UTC',now())
         FROM tt_TEMPManageBillPayment X WHERE X.tintBillType = 2 AND X.numBillID NOT IN(SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X);
         UPDATE BillHeader SET monAmtPaid = monAmtPaid+X.monAmount,bitIsPaid = CASE WHEN monAmountDue -(monAmtPaid+X.monAmount) = 0 THEN true ELSE false END
         FROM  tt_TEMPManageBillPayment X
         WHERE X.numBillID > 0 AND BillHeader.numBillID = X.numBillID;
         UPDATE OpportunityBizDocs SET monAmountPaid = monAmountPaid+monAmount
         FROM tt_TEMPManageBillPayment X
         WHERE X.numOppBizDocsId > 0 AND OpportunityBizDocs.numOppBizDocsId = X.numOppBizDocsId;
				
				--Add to OpportunityAutomationQueue if full Amount Paid	
         INSERT INTO OpportunityAutomationQueue(numDomainID, numOppId, numOppBizDocsId, numBizDocStatus, numCreatedBy, dtCreatedDate, tintProcessStatus,numRuleID)
         SELECT OM.numDomainId, OM.numOppId, OBD.numOppBizDocsId, 0, v_numUserCntID_X, TIMEZONE('UTC',now()), 1,15
         FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
         WHERE OM.numDomainId = v_numDomainID_X AND OBD.numOppBizDocsId IN(SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X AND coalesce(numOppBizDocsID,0) > 0)
         AND coalesce(OBD.monDealAmount,0) = coalesce(OBD.monAmountPaid,0) AND coalesce(OBD.monAmountPaid,0) > 0;

				--Add to OpportunityAutomationQueue if Balance due
         INSERT INTO OpportunityAutomationQueue(numDomainID, numOppId, numOppBizDocsId, numBizDocStatus, numCreatedBy, dtCreatedDate, tintProcessStatus,numRuleID)
         SELECT OM.numDomainId, OM.numOppId, OBD.numOppBizDocsId, 0, v_numUserCntID_X, TIMEZONE('UTC',now()), 1,16
         FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
         WHERE OM.numDomainId = v_numDomainID_X AND OBD.numOppBizDocsId IN(SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X AND coalesce(numOppBizDocsID,0) > 0)
         AND coalesce(OBD.monDealAmount,0) > coalesce(OBD.monAmountPaid,0) AND coalesce(OBD.monAmountPaid,0) > 0;
         DROP TABLE IF EXISTS tt_TEMPManageBillPayment CASCADE;
         UPDATE BillPaymentHeader SET monAppliedAmount =(SELECT coalesce(SUM(coalesce(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X)
         WHERE numBillPaymentID = v_numBillPaymentID_X;
      ELSEIF v_tintMode_X = 2
      then
			
				--Revert back all amount for BillHeader
         UPDATE BillHeader BH SET monAmtPaid = monAmtPaid -BPD.monAmount,bitIsPaid = CASE WHEN monAmountDue -(monAmtPaid -BPD.monAmount) = 0 THEN true ELSE false END
         FROM BillPaymentDetails BPD
         WHERE(BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID) AND BPD.numBillPaymentID = v_numBillPaymentID_X; 
						 
				--Revert back all amount for OpportunityBizDocs
         UPDATE OpportunityBizDocs OBD SET monAmountPaid = monAmountPaid -BPD.monAmount
         FROM BillPaymentDetails BPD
         WHERE(BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID) AND BPD.numBillPaymentID = v_numBillPaymentID_X;
						
				--Delete all BillPaymentDetails entries 				 
         DELETE FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X;
         UPDATE BillPaymentHeader SET monAppliedAmount =(SELECT coalesce(SUM(coalesce(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID = v_numBillPaymentID_X)
         WHERE numBillPaymentID = v_numBillPaymentID_X;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
          GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   
         RETURN;
   END;	

   BEGIN
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPManageBillPayment_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPManageBillPayment CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPManageBillPayment
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppBizDocsId NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPManageBillPayment(numOppBizDocsId)
      SELECT
      numOppBizDocsId
      FROM
      BillPaymentDetails
      WHERE
      numBillPaymentID = v_numBillPaymentID_X
      AND coalesce(numOppBizDocsId,0) > 0;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPManageBillPayment;
      WHILE v_i <= v_iCount LOOP
         select   numOppBizDocsId INTO v_numOppBizDocsId FROM tt_TEMPManageBillPayment WHERE ID = v_i;
         SELECT * INTO SWV_RCur,SWV_RCur2 FROM USP_OpportunityBizDocs_CT(v_numDomainID,v_numUserCntID,v_numOppBizDocsId,SWV_RefCur := 'SWV_RCur',
         SWV_RefCur2 := 'SWV_RCur2');
         v_i := v_i::bigint+1;
      END LOOP;
      EXCEPTION WHEN OTHERS THEN
	-- DO NOT RAISE ERROR
         NULL;
   END;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/



