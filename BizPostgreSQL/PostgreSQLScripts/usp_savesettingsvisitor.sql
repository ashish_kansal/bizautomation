-- Stored procedure definition script USP_SaveSettingsVisitor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveSettingsVisitor(v_MinimumPages INTEGER,        
v_ReportType INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM TrackSettingsVisitor;  
       
   insert into TrackSettingsVisitor(numMinPages,numReportType)
values(v_MinimumPages,v_ReportType);
RETURN;
END; $$;


