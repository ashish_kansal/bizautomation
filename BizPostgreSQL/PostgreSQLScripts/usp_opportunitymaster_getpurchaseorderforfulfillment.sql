-- Stored procedure definition script USP_OpportunityMaster_GetPurchaseOrderForFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetPurchaseOrderForFulfillment(v_numDomainID NUMERIC(18,0)
	,v_tintSearchType SMALLINT
	,v_vcSearchText VARCHAR(300)
	,v_intOffset INTEGER
	,v_intPageSize INTEGER
	,INOUT v_intTotalRecords INTEGER , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      RecordID NUMERIC(18,0),
      RecordValue VARCHAR(300)
   );
   IF v_tintSearchType = 1 then -- Item
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT
      numItemCode
			,CONCAT(vcItemName,CASE WHEN LENGTH(coalesce(Item.vcSKU,'')) > 0 THEN CONCAT(' (SKU:',Item.vcSKU,')') ELSE '' END)
      FROM
      Item
      WHERE
      numDomainID = v_numDomainID
      AND vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSEIF v_tintSearchType = 2
   then -- Warehouse
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT
      numWareHouseID
			,vcWareHouse
      FROM
      Warehouses
      WHERE
      numDomainID = v_numDomainID
      AND vcWareHouse ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSEIF v_tintSearchType = 3
   then -- Purchase Order
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT
      numOppId
			,CAST(vcpOppName AS VARCHAR(300))
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 2
      AND tintoppstatus = 1
      AND coalesce(bitStockTransfer,false) = false
      AND vcpOppName ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSEIF v_tintSearchType = 4
   then -- Vendor
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT
      numDivisionID
			,vcCompanyName
      FROM
      CompanyInfo
      INNER JOIN
      DivisionMaster
      ON
      CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
      WHERE
      CompanyInfo.numDomainID = v_numDomainID
      AND vcCompanyName ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSEIF v_tintSearchType = 5
   then -- BizDoc
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT
      numOppBizDocsId
			,CAST(vcBizDocID AS VARCHAR(300))
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityMaster
      ON
      OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND tintopptype = 2
      AND tintoppstatus = 1
      AND coalesce(bitStockTransfer,false) = false
      AND OpportunityBizDocs.vcBizDocID ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSEIF v_tintSearchType = 6
   then -- SKU
	
      INSERT INTO tt_TEMP(RecordID
			,RecordValue)
      SELECT DISTINCT
      numItemCode
			,CONCAT(vcItemName,CASE WHEN LENGTH(coalesce(Item.vcSKU,'')) > 0 THEN CONCAT(' (SKU:',Item.vcSKU,')') ELSE '' END)
      FROM
      Item
      LEFT JOIN
      WareHouseItems
      ON
      Item.numItemCode = WareHouseItems.numItemID
      WHERE
      Item.numDomainID = v_numDomainID
      AND (vcSKU ilike '%' || coalesce(v_vcSearchText,'') || '%' OR vcWHSKU ilike '%' || coalesce(v_vcSearchText,'') || '%');
   end if;

	open SWV_RefCur for
	SELECT 
		ID AS "ID",
		RecordID AS "RecordID",
		RecordValue AS "RecordValue" 
	FROM tt_TEMP WHERE ID BETWEEN v_intOffset::bigint+1 AND v_intOffset::bigint+v_intPageSize::bigint;
	
   SELECT COUNT(*) INTO v_intTotalRecords FROM tt_TEMP;
   RETURN;
END; $$;


