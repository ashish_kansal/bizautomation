CREATE OR REPLACE FUNCTION usp_updateProjOppStageEvent(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
-----------Checking Opportunity Events and Updating  
   update OpportunityStageDetails set numStage  = case when bitChgStatus = true then numChgStatus else numStage end,bitstagecompleted = case when bitClose = true then true else bitstagecompleted end,bintStageComDate =  case when bitClose = true then
      case when bitstagecompleted = true then bintStageComDate else TIMEZONE('UTC',now()) end
   else bintStageComDate end
   where numOppStageId in(select numOppStageId from
   OpportunityStageDetails
   where numCommActId in(select numCommId from Communication where dtEndTime between LOCALTIMESTAMP  and LOCALTIMESTAMP+INTERVAL '1 hour')
   and (bitChgStatus = true or bitClose = true) and numStartDate <> 0 and numEvent in(2)
   union
   select numOppStageId from
   OpportunityStageDetails where numCommActId in(select ActivityID from Activity where
      StartDateTimeUtc+CAST(Duration || 'second' as interval)  between TIMEZONE('UTC',now()) and TIMEZONE('UTC',now())+INTERVAL '1 hour')
   and (bitChgStatus = true or bitClose = true) and numStartDate <> 0 and numEvent in(3));   
  
  
-----------Checking  Projects Events and Updating  
  
   update ProjectsStageDetails set numStage = case when bitChgStatus = true then numChgStatus else numStage end,bitStageCompleted = case when bitClose = true then true else bitStageCompleted end,bintStageComDate =  case when bitClose = true then
      case when bitStageCompleted = true then bintStageComDate else TIMEZONE('UTC',now()) end
   else bintStageComDate end
   where numProStageId in(select numProStageId from
   ProjectsStageDetails
   where numCommActId in(select numCommId from Communication where dtEndTime between LOCALTIMESTAMP  and LOCALTIMESTAMP+INTERVAL '1 hour')
   and (bitChgStatus = true or bitClose = true) and numStartDate <> 0 and numEvent in(2)
   union
   select numProStageId from
   ProjectsStageDetails where numCommActId in(select ActivityID from Activity where
      StartDateTimeUtc+CAST(Duration || 'second' as interval)  between TIMEZONE('UTC',now()) and TIMEZONE('UTC',now())+INTERVAL '1 hour')
   and (bitChgStatus = true or bitClose = true) and numStartDate <> 0 and numEvent in(3));   


-----------Checking  SendEmailTemplate


   drop table IF EXISTS tt_OPPCLOSED CASCADE;
   Create TEMPORARY TABLE tt_OPPCLOSED
   (
      numOppId NUMERIC(9,0)
   );    

   drop table IF EXISTS tt_PROCLOSED CASCADE;
   Create TEMPORARY TABLE tt_PROCLOSED
   (
      numProId NUMERIC(9,0)
   );

   drop table IF EXISTS tt_CHANGESTATUS CASCADE;
   Create TEMPORARY TABLE tt_CHANGESTATUS
   (
      numStageId NUMERIC(9,0),
      vcFrom  VARCHAR(100),	
      vcTo  VARCHAR(100),	
      vcCC VARCHAR(1),
      vcSubject  VARCHAR(1000),
      vcBody  TEXT,
      numDomainId  NUMERIC(9,0),
      chrType  CHAR(1)
   );
  
--Getting Opportunities Closed
--================================================--================================================
   insert INTO tt_OPPCLOSED
   select distinct(numOppId) from OpportunityStageDetails where numStagePercentage in(0,100) and bitstagecompleted = true;  
--================================================--================================================
--Getting Projects Closed
--================================================--================================================
   insert INTO tt_PROCLOSED
   select distinct(numProId) from ProjectsStageDetails where numstagepercentage in(0,100) and bitStageCompleted = true; 
--================================================--================================================
--Getting Details and inserting into TempTable
--================================================--================================================
   insert INTO tt_CHANGESTATUS
--From opportunities--================================================
   select numOppStageId,CAST(Adc2.vcEmail AS VARCHAR(100)) as vcFrom,CAST(ADC1.vcEmail AS VARCHAR(100)) as vcTo,'' as vcCC,vcSubject,
replace(replace(replace(replace(replace(replace(coalesce(CAST(vcDocDesc AS TEXT),''),'##OppID##',coalesce(vcpOppName,'')),'##Stage##',numOppStageId::VARCHAR),'##Organization##',
   fn_GetComapnyName(ADC1.numDivisionId)),'##vcCompanyName##',
   fn_GetComapnyName(ADC1.numDivisionId)),'##ContactName##',coalesce(ADC1.vcFirstName || ' ' || ADC1.vcLastname,'')),'##Email##',
   coalesce(ADC1.vcEmail,'')) as vcBody ,
OppMas.numDomainId,'O'
   from OpportunityStageDetails OppStage1
   join OpportunityMaster OppMas  on OppStage1.numoppid = OppMas.numOppId
   join AdditionalContactsInformation ADC1 on ADC1.numContactId = OppMas.numContactId
   join AdditionalContactsInformation Adc2  on Adc2.numContactId = OppStage1.numAssignTo
   join GenericDocuments Gdoc on numGenericDocID = numET
   where numEvent = 1
   and numStartDate <> 0 and numStagePErcentage not in(0,100) and numET <> 0  and OppStage1.numAssignTo <> 0
   and OppStage1.numoppid not in(select numOppId from tt_OPPCLOSED)
   and fn_checkEventDateRange(bintDueDate,OppStage1.bintCreatedDate,numstartDate,numstartTime,numStartTimePeriod,
   OppMas.numOppId,numOppStageId,'O') = true
   union all
--From Projects--================================================
   select numProStageId,CAST(Adc2.vcEmail AS VARCHAR(100)) as vcFrom,CAST(ADC1.vcEmail AS VARCHAR(100)) as vcTo,'' as vcCC,vcSubject,
replace(replace(replace(replace(replace(replace(coalesce(CAST(vcDocDesc AS TEXT),''),'##OppID##',coalesce(vcProjectName,'')),'##Stage##',numProStageId::VARCHAR),'##Organization##',
   fn_GetComapnyName(ADC1.numDivisionId)),'##vcCompanyName##',
   fn_GetComapnyName(ADC1.numDivisionId)),'##ContactName##',
   coalesce(ADC1.vcFirstName || ' ' || ADC1.vcLastname,'')),'##Email##',
   coalesce(ADC1.vcEmail,'')) as vcBody ,
ProMas.numdomainId,'P'
   from ProjectsStageDetails ProStage1
   join ProjectsMaster ProMas  on ProStage1.numProId = ProMas.numProId
   join AdditionalContactsInformation ADC1 on ADC1.numContactId = ProMas.numCustPrjMgr
   join AdditionalContactsInformation Adc2  on Adc2.numContactId = ProStage1.numAssignTo
   join GenericDocuments Gdoc on numGenericDocID = numET
   where numEvent = 1
   and numStartDate <> 0 and numStagePErcentage not in(0,100) and numET <> 0  and ProStage1.numAssignTo <> 0
   and ProStage1.numProId not in(select numProId from tt_PROCLOSED)
   and fn_checkEventDateRange(bintDueDate,ProStage1.bintCreatedDate,numstartDate,numstartTime,numStartTimePeriod,
   ProMas.numProId,numProStageId,'P') = true;
--================================================--================================================
--Updating Opportunities
--================================================--================================================
   update OpportunityStageDetails set numStage  = case when bitChgStatus = true then numChgStatus else numStage end,bitstagecompleted = case when bitClose = true then true else bitstagecompleted end,bintStageComDate =  case when bitClose = true then
      case when bitstagecompleted = true then bintStageComDate else TIMEZONE('UTC',now()) end
   else bintStageComDate end
   where numOppStageId in(select numStageId from tt_CHANGESTATUS where chrType = 'O');  
--================================================--================================================
--Updating Projects
--================================================--================================================

   update ProjectsStageDetails set numStage = case when bitChgStatus = true then numChgStatus else numStage end,bitStageCompleted = case when bitClose = true then true else bitStageCompleted end,bintStageComDate =  case when bitClose = true then
      case when bitStageCompleted = true then bintStageComDate else TIMEZONE('UTC',now()) end
   else bintStageComDate end
   where numProStageId in(select numStageId from tt_CHANGESTATUS where chrType = 'P');  
--================================================--================================================
--retuning Mail details
--================================================--================================================
   open SWV_RefCur for select * from tt_CHANGESTATUS;

--================================================--================================================
--Drop  Temp Tables
--================================================--================================================
  
   drop table IF EXISTS tt_OPPCLOSED CASCADE;
   drop table IF EXISTS tt_PROCLOSED CASCADE;
   RETURN;
END; $$;












