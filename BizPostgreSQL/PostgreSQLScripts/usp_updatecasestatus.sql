-- Stored procedure definition script Usp_UpdateCaseStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_UpdateCaseStatus()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intCnt  INTEGER;
   v_intRows  INTEGER;
   v_dtResolveDate  TIMESTAMP;
   v_dtTodayDate  TIMESTAMP;
   v_numCaseID  NUMERIC(18,0);
   v_numDomainID  NUMERIC(18,0);
BEGIN
   v_dtTodayDate := GetUTCDateWithoutTime();
--	1008  > Open
--	134   > Pending
--	135   > Overdue
--	136   > Closed
--	22931 > test

   UPDATE Cases SET numStatus = 135  WHERE numStatus = 134 AND v_dtTodayDate > intTargetResolveDate;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/



