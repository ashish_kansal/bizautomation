-- Stored procedure definition script usp_InsertQBSynch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertQBSynch(v_numOppID NUMERIC DEFAULT 0,
	v_numCompanyID NUMERIC DEFAULT 0,
	v_numDomainID NUMERIC DEFAULT 0
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO QBSynch(numOppID,numCompanyID,numDomainID)
	VALUES(v_numOppID,v_numCompanyID,v_numDomainID);
RETURN;
END; $$;


