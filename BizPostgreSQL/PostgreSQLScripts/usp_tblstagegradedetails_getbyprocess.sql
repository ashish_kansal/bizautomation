-- Stored procedure definition script USP_tblStageGradeDetails_GetByProcess for PostgreSQL
CREATE OR REPLACE FUNCTION USP_tblStageGradeDetails_GetByProcess(v_numDomainID NUMERIC(18,0)
	,v_numProcessID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   tblStageGradeDetails.numTaskID
		,tblStageGradeDetails.numAssigneId
		,tblStageGradeDetails.vcGradeId
   FROM
   StagePercentageDetails
   INNER JOIN
   tblStageGradeDetails
   ON
   StagePercentageDetails.numStageDetailsId = tblStageGradeDetails.numStageDetailsId
   WHERE
   StagePercentageDetails.numdomainid = v_numDomainID
   AND StagePercentageDetails.slp_id = v_numProcessID;
END; $$;












