-- Stored procedure definition script USP_ApproveOpportunityItemsPrice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE OR REPLACE FUNCTION USP_ApproveOpportunityItemsPrice(v_numOppId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_ApprovalType INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	--	 SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
   IF(v_ApprovalType = 2) then
	
      PERFORM USP_ItemUnitPriceApprovalUpdateConfigurePrice(v_numOppId := v_numOppId,v_numDomainID := v_numDomainID);
   end if;


   UPDATE
   OpportunityMaster
   SET
   tintoppstatus = CASE WHEN bitIsInitalSalesOrder = true THEN 1 ELSE tintoppstatus END
   WHERE
   numOppId = v_numOppId;
	
   UPDATE
   OpportunityItems
   SET
   bitItemPriceApprovalRequired = false
   WHERE
   numOppId = v_numOppId;
   RETURN;
END; $$;


