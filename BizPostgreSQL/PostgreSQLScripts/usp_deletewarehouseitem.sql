-- Stored procedure definition script USP_DeleteWarehouseItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DeleteWarehouseItem(v_numDomainID NUMERIC(18,0),
  v_numUserCntID NUMERIC(18,0),
  v_numWareHouseItmsDTLID NUMERIC(18,0),
  v_numWareHouseItemID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
	DECLARE
	v_numItemCode  NUMERIC(18,0);
	v_Total  DOUBLE PRECISION;
	
	v_numOnHand  DOUBLE PRECISION;
	v_numOnAllocation  DOUBLE PRECISION;
	v_numBackOrder  DOUBLE PRECISION;
	
	v_numOPPID  NUMERIC(18,0);
	v_numOppItemID  NUMERIC(18,0);
	v_numReferenceID  NUMERIC(18,0);
	v_tintReference  SMALLINT;
	v_description  VARCHAR(100);
		
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

	v_i  INTEGER DEFAULT 1;
	v_numOIRLID  NUMERIC(18,0);
	v_numRemainingQty  DOUBLE PRECISION;
	v_Count  INTEGER;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   numItemID INTO v_numItemCode FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numWareHouseItemID;
      select   numQty -coalesce((SELECT
         SUM(w.numQty)
         FROM
         OppWarehouseSerializedItem w
         INNER JOIN
         OpportunityMaster opp
         ON
         w.numOppID = opp.numOppId
         WHERE
         coalesce(opp.tintopptype,0) <> 2
         AND 1 =(CASE WHEN coalesce(opp.bitStockTransfer,false) = true THEN CASE WHEN coalesce(w.bitTransferComplete,false) = false THEN 1 ELSE 0 END ELSE 1 END)
         AND w.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID),0) INTO v_Total FROM
      WareHouseItmsDTL WHERE
      numWareHouseItmsDTLID = v_numWareHouseItmsDTLID AND numWareHouseItemID = v_numWareHouseItemID;
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numBackOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      IF v_numOnHand >= v_Total then
	
         UPDATE
         WareHouseItems
         SET
         numOnHand = coalesce(numOnHand,0) -v_Total,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      ELSE
         UPDATE
         WareHouseItems
         SET
         numOnHand = 0,numAllocation =(CASE WHEN v_numOnAllocation >(v_Total -v_numOnHand) THEN v_numOnAllocation -(v_Total -v_numOnHand) ELSE 0 END),
         numBackOrder =(CASE WHEN v_numOnAllocation >(v_Total -v_numOnHand) THEN v_numBackOrder+(v_Total -v_numOnHand) ELSE v_numBackOrder+v_numOnAllocation END),dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      end if;
      UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0) -v_Total  WHERE  numWareHouseItmsDTLID = v_numWareHouseItmsDTLID AND numWareHouseItemID = v_numWareHouseItemID;
      select   OM.numOppId, OWSI.numOppItemID INTO v_numOPPID,v_numOppItemID FROM OppWarehouseSerializedItem OWSI INNER JOIN OpportunityMaster OM ON OWSI.numOppID = OM.numOppId AND tintopptype = 2 WHERE numWarehouseItmsDTLID = v_numWareHouseItmsDTLID;
      v_numReferenceID :=(CASE WHEN coalesce(v_numOPPID,0) > 0 THEN v_numOPPID ELSE v_numItemCode END);
      v_tintReference :=(CASE WHEN coalesce(v_numOPPID,0) > 0 THEN 4 ELSE 1 END);
      v_description := CONCAT('Serial/Lot# Deleted(Qty:',v_Total,')');
	 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
      IF coalesce(v_numOPPID,0) > 0 then
	
         UPDATE
         OpportunityItems OI
         SET
         numDeletedReceievedQty = coalesce(OI.numDeletedReceievedQty,0)+v_Total
         FROM
         OppWarehouseSerializedItem OWSI
         WHERE(OI.numOppId = OWSI.numOppID
         AND OI.numoppitemtCode = OWSI.numOppItemID) AND OWSI.numWarehouseItmsDTLID = v_numWareHouseItmsDTLID;
         IF EXISTS(SELECT ID FROM OpportunityItemsReceievedLocation WHERE numOppID = v_numOPPID AND numOppItemID = v_numOppItemID) then
            BEGIN
               CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
               EXCEPTION WHEN OTHERS THEN
                  NULL;
            END;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
            CREATE TEMPORARY TABLE tt_TEMP
            (
               ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
               numOIRLID NUMERIC(18,0),
               numReceievedQty DOUBLE PRECISION,
               numDeletedReceievedQty DOUBLE PRECISION
            );
            INSERT INTO tt_TEMP(numOIRLID
				,numReceievedQty
				,numDeletedReceievedQty)
            SELECT
            ID,
				coalesce(numUnitReceieved,0),
				coalesce(numDeletedReceievedQty,0)
            FROM
            OpportunityItemsReceievedLocation
            WHERE
            numOppID = v_numOPPID
            AND numOppItemID = v_numOppItemID
            AND numWarehouseItemID = v_numWareHouseItemID
            AND(coalesce(numUnitReceieved,0) -coalesce(numDeletedReceievedQty,0)) > 0;
            select   COUNT(*) INTO v_Count FROM tt_TEMP;
            WHILE v_i <= v_Count AND v_Total > 0 LOOP
               select   numOIRLID, numReceievedQty -numDeletedReceievedQty INTO v_numOIRLID,v_numRemainingQty FROM tt_TEMP WHERE ID = v_i;
               IF v_numRemainingQty >= v_Total then
				
                  UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = coalesce(numDeletedReceievedQty,0)+v_Total WHERE ID = v_numOIRLID;
                  v_Total := 0;
               ELSE
                  UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = coalesce(numDeletedReceievedQty,0)+(v_Total -v_numRemainingQty)  WHERE ID = v_numOIRLID;
                  v_Total := v_Total -v_numRemainingQty;
               end if;
               v_i := v_i::bigint+1;
            END LOOP;
            IF v_Total > 0 then
			
               RAISE EXCEPTION 'DELETED_RECEIEVED_QTY_MISMATCH';
            end if;
         end if;
      end if;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numReferenceID,
      v_tintRefType := v_tintReference::SMALLINT,v_vcDescription := v_description,
      v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
      SWV_RefCur := null);
      open SWV_RefCur for SELECT 1;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
            GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;













