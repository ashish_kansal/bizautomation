-- Stored procedure definition script USP_Item_ImportPriceLevel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_ImportPriceLevel(v_numItemCode NUMERIC(18,0)
	,v_tintRuleType INTEGER
	,v_tintDiscountType INTEGER
	,v_monPriceLevel1 DECIMAL(20,5)
	,v_monPriceLevel2 DECIMAL(20,5)
	,v_monPriceLevel3 DECIMAL(20,5)
	,v_monPriceLevel4 DECIMAL(20,5)
	,v_monPriceLevel5 DECIMAL(20,5)
	,v_monPriceLevel6 DECIMAL(20,5)
	,v_monPriceLevel7 DECIMAL(20,5)
	,v_monPriceLevel8 DECIMAL(20,5)
	,v_monPriceLevel9 DECIMAL(20,5)
	,v_monPriceLevel10 DECIMAL(20,5)
	,v_monPriceLevel11 DECIMAL(20,5)
	,v_monPriceLevel12 DECIMAL(20,5)
	,v_monPriceLevel13 DECIMAL(20,5)
	,v_monPriceLevel14 DECIMAL(20,5)
	,v_monPriceLevel15 DECIMAL(20,5)
	,v_monPriceLevel16 DECIMAL(20,5)
	,v_monPriceLevel17 DECIMAL(20,5)
	,v_monPriceLevel18 DECIMAL(20,5)
	,v_monPriceLevel19 DECIMAL(20,5)
	,v_monPriceLevel20 DECIMAL(20,5)
	,v_intPriceLevel1FromQty INTEGER
	,v_intPriceLevel1ToQty INTEGER
	,v_intPriceLevel2FromQty INTEGER
	,v_intPriceLevel2ToQty INTEGER
	,v_intPriceLevel3FromQty INTEGER
	,v_intPriceLevel3ToQty INTEGER
	,v_intPriceLevel4FromQty INTEGER
	,v_intPriceLevel4ToQty INTEGER
	,v_intPriceLevel5FromQty INTEGER
	,v_intPriceLevel5ToQty INTEGER
	,v_intPriceLevel6FromQty INTEGER
	,v_intPriceLevel6ToQty INTEGER
	,v_intPriceLevel7FromQty INTEGER
	,v_intPriceLevel7ToQty INTEGER
	,v_intPriceLevel8FromQty INTEGER
	,v_intPriceLevel8ToQty INTEGER
	,v_intPriceLevel9FromQty INTEGER
	,v_intPriceLevel9ToQty INTEGER
	,v_intPriceLevel10FromQty INTEGER
	,v_intPriceLevel10ToQty INTEGER
	,v_intPriceLevel11FromQty INTEGER
	,v_intPriceLevel11ToQty INTEGER
	,v_intPriceLevel12FromQty INTEGER
	,v_intPriceLevel12ToQty INTEGER
	,v_intPriceLevel13FromQty INTEGER
	,v_intPriceLevel13ToQty INTEGER
	,v_intPriceLevel14FromQty INTEGER
	,v_intPriceLevel14ToQty INTEGER
	,v_intPriceLevel15FromQty INTEGER
	,v_intPriceLevel15ToQty INTEGER
	,v_intPriceLevel16FromQty INTEGER
	,v_intPriceLevel16ToQty INTEGER
	,v_intPriceLevel17FromQty INTEGER
	,v_intPriceLevel17ToQty INTEGER
	,v_intPriceLevel18FromQty INTEGER
	,v_intPriceLevel18ToQty INTEGER
	,v_intPriceLevel19FromQty INTEGER
	,v_intPriceLevel19ToQty INTEGER
	,v_intPriceLevel20FromQty INTEGER
	,v_intPriceLevel20ToQty INTEGER
	,v_vcPriceLevel1Name VARCHAR(300)
	,v_vcPriceLevel2Name VARCHAR(300)
	,v_vcPriceLevel3Name VARCHAR(300)
	,v_vcPriceLevel4Name VARCHAR(300)
	,v_vcPriceLevel5Name VARCHAR(300)
	,v_vcPriceLevel6Name VARCHAR(300)
	,v_vcPriceLevel7Name VARCHAR(300)
	,v_vcPriceLevel8Name VARCHAR(300)
	,v_vcPriceLevel9Name VARCHAR(300)
	,v_vcPriceLevel10Name VARCHAR(300)
	,v_vcPriceLevel11Name VARCHAR(300)
	,v_vcPriceLevel12Name VARCHAR(300)
	,v_vcPriceLevel13Name VARCHAR(300)
	,v_vcPriceLevel14Name VARCHAR(300)
	,v_vcPriceLevel15Name VARCHAR(300)
	,v_vcPriceLevel16Name VARCHAR(300)
	,v_vcPriceLevel17Name VARCHAR(300)
	,v_vcPriceLevel18Name VARCHAR(300)
	,v_vcPriceLevel19Name VARCHAR(300)
	,v_vcPriceLevel20Name VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_localRuleType  SMALLINT;
   v_localDiscountType  SMALLINT;

   v_MaxPriceLevelIDWithSomeValue  INTEGER;
	
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF v_monPriceLevel1 = -1
   AND v_monPriceLevel2 = -1
   AND v_monPriceLevel3 = -1
   AND v_monPriceLevel4 = -1
   AND v_monPriceLevel5 = -1
   AND v_monPriceLevel6 = -1
   AND v_monPriceLevel7 = -1
   AND v_monPriceLevel8 = -1
   AND v_monPriceLevel9 = -1
   AND v_monPriceLevel10 = -1
   AND v_monPriceLevel11 = -1
   AND v_monPriceLevel12 = -1
   AND v_monPriceLevel13 = -1
   AND v_monPriceLevel14 = -1
   AND v_monPriceLevel15 = -1
   AND v_monPriceLevel16 = -1
   AND v_monPriceLevel17 = -1
   AND v_monPriceLevel18 = -1
   AND v_monPriceLevel19 = -1
   AND v_monPriceLevel20 = -1 then
	
      RETURN;
   end if;

   IF coalesce(v_intPriceLevel1FromQty,0) = 0 AND coalesce(v_intPriceLevel1ToQty,0) = 0 then
	
      v_intPriceLevel1FromQty := -1;
      v_intPriceLevel1ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel2FromQty,0) = 0 AND coalesce(v_intPriceLevel2ToQty,0) = 0 then
	
      v_intPriceLevel2FromQty := -1;
      v_intPriceLevel2ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel3FromQty,0) = 0 AND coalesce(v_intPriceLevel3ToQty,0) = 0 then
	
      v_intPriceLevel3FromQty := -1;
      v_intPriceLevel3ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel4FromQty,0) = 0 AND coalesce(v_intPriceLevel4ToQty,0) = 0 then
	
      v_intPriceLevel4FromQty := -1;
      v_intPriceLevel4ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel5FromQty,0) = 0 AND coalesce(v_intPriceLevel5ToQty,0) = 0 then
	
      v_intPriceLevel5FromQty := -1;
      v_intPriceLevel5ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel6FromQty,0) = 0 AND coalesce(v_intPriceLevel6ToQty,0) = 0 then
	
      v_intPriceLevel6FromQty := -1;
      v_intPriceLevel6ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel7FromQty,0) = 0 AND coalesce(v_intPriceLevel7ToQty,0) = 0 then
	
      v_intPriceLevel7FromQty := -1;
      v_intPriceLevel7ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel8FromQty,0) = 0 AND coalesce(v_intPriceLevel8ToQty,0) = 0 then
	
      v_intPriceLevel8FromQty := -1;
      v_intPriceLevel8ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel9FromQty,0) = 0 AND coalesce(v_intPriceLevel9ToQty,0) = 0 then
	
      v_intPriceLevel9FromQty := -1;
      v_intPriceLevel9ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel10FromQty,0) = 0 AND coalesce(v_intPriceLevel10ToQty,0) = 0 then
	
      v_intPriceLevel10FromQty := -1;
      v_intPriceLevel10ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel11FromQty,0) = 0 AND coalesce(v_intPriceLevel11ToQty,0) = 0 then
	
      v_intPriceLevel11FromQty := -1;
      v_intPriceLevel11ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel12FromQty,0) = 0 AND coalesce(v_intPriceLevel12ToQty,0) = 0 then
	
      v_intPriceLevel12FromQty := -1;
      v_intPriceLevel12ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel13FromQty,0) = 0 AND coalesce(v_intPriceLevel13ToQty,0) = 0 then
	
      v_intPriceLevel13FromQty := -1;
      v_intPriceLevel13ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel14FromQty,0) = 0 AND coalesce(v_intPriceLevel14ToQty,0) = 0 then
	
      v_intPriceLevel14FromQty := -1;
      v_intPriceLevel14ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel15FromQty,0) = 0 AND coalesce(v_intPriceLevel15ToQty,0) = 0 then
	
      v_intPriceLevel15FromQty := -1;
      v_intPriceLevel15ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel16FromQty,0) = 0 AND coalesce(v_intPriceLevel16ToQty,0) = 0 then
	
      v_intPriceLevel16FromQty := -1;
      v_intPriceLevel16ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel17FromQty,0) = 0 AND coalesce(v_intPriceLevel17ToQty,0) = 0 then
	
      v_intPriceLevel17FromQty := -1;
      v_intPriceLevel17ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel18FromQty,0) = 0 AND coalesce(v_intPriceLevel18ToQty,0) = 0 then
	
      v_intPriceLevel18FromQty := -1;
      v_intPriceLevel18ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel19FromQty,0) = 0 AND coalesce(v_intPriceLevel19ToQty,0) = 0 then
	
      v_intPriceLevel19FromQty := -1;
      v_intPriceLevel19ToQty := -1;
   end if;
   IF coalesce(v_intPriceLevel20FromQty,0) = 0 AND coalesce(v_intPriceLevel20ToQty,0) = 0 then
	
      v_intPriceLevel20FromQty := -1;
      v_intPriceLevel20ToQty := -1;
   end if;



   IF coalesce(v_intPriceLevel1FromQty,0) = 0 OR coalesce(v_intPriceLevel1ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_1';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel2FromQty,0) = 0 OR coalesce(v_intPriceLevel2ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_2';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel3FromQty,0) = 0 OR coalesce(v_intPriceLevel3ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_3';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel4FromQty,0) = 0 OR coalesce(v_intPriceLevel4ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_4';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel5FromQty,0) = 0 OR coalesce(v_intPriceLevel5ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_5';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel6FromQty,0) = 0 OR coalesce(v_intPriceLevel6ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_6';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel7FromQty,0) = 0 OR coalesce(v_intPriceLevel7ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_7';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel8FromQty,0) = 0 OR coalesce(v_intPriceLevel8ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_8';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel9FromQty,0) = 0 OR coalesce(v_intPriceLevel9ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_9';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel10FromQty,0) = 0 OR coalesce(v_intPriceLevel10ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_10';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel11FromQty,0) = 0 OR coalesce(v_intPriceLevel11ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_11';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel12FromQty,0) = 0 OR coalesce(v_intPriceLevel12ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_12';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel13FromQty,0) = 0 OR coalesce(v_intPriceLevel13ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_13';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel14FromQty,0) = 0 OR coalesce(v_intPriceLevel14ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_14';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel15FromQty,0) = 0 OR coalesce(v_intPriceLevel15ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_15';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel16FromQty,0) = 0 OR coalesce(v_intPriceLevel16ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_16';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel17FromQty,0) = 0 OR coalesce(v_intPriceLevel17ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_17';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel18FromQty,0) = 0 OR coalesce(v_intPriceLevel18ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_18';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel19FromQty,0) = 0 OR coalesce(v_intPriceLevel19ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_19';
      RETURN;
   end if;
   IF coalesce(v_intPriceLevel20FromQty,0) = 0 OR coalesce(v_intPriceLevel20ToQty,0) = 0 then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_20';
      RETURN;
   end if;


   DROP TABLE IF EXISTS tt_DEFUALT CASCADE;
   CREATE TEMPORARY TABLE tt_DEFUALT
   (
      PriceLevelID INTEGER,
      tintRuleType SMALLINT,
      tintDiscountType SMALLINT,
      monPrice DECIMAL(20,5),
      intFromQty INTEGER,
      intToQty INTEGER,
      vcName VARCHAR(300)
   );

   INSERT INTO tt_DEFUALT(PriceLevelID
		,monPrice
		,intFromQty
		,intToQty
		,vcName)
	VALUES(1,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(2,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (3,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(4,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (5,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(6,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (7,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (8,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(9,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (10,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(11,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (12,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(13,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (14,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (15,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(16,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (17,-1,-1,-1,CAST('-1' AS VARCHAR(300))),(18,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (19,-1,-1,-1,CAST('-1' AS VARCHAR(300))),
   (20,-1,-1,-1,CAST('-1' AS VARCHAR(300)));

	-- FIRST REPLACE -1 WITH EXISTING ITEM PRICES, REPLEACE -1 IN FROM AND TO QTY ONLY WHEN RuleType is Deduct From List Price or Add to vendor Cost
	UPDATE
		tt_DEFUALT t1
	SET
		monPrice = coalesce(t3.decDiscount,t1.monPrice)
		,tintRuleType =(CASE WHEN coalesce(v_tintRuleType,0) <> -1 THEN v_tintRuleType ELSE coalesce(t3.tintRuleType,t1.tintRuleType) END)
		,tintDiscountType =(CASE WHEN coalesce(v_tintDiscountType,0) <> -1 THEN v_tintDiscountType ELSE coalesce(t3.tintDiscountType,t1.tintDiscountType) END)
		,intFromQty =(CASE WHEN t3.tintRuleType = 1 OR t3.tintRuleType = 2 THEN t3.intFromQty ELSE t1.intFromQty END)
		,intToQty =(CASE WHEN t3.tintRuleType = 1 OR t3.tintRuleType = 2 THEN t3.intToQty ELSE t1.intToQty END),vcName = coalesce(t3.vcName,'')
	FROM
		tt_DEFUALT t2
	LEFT JOIN
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) AS RowID
			,decDiscount
			,intFromQty
			,intToQty
			,tintRuleType
			,tintDiscountType
			,vcName
		FROM
			PricingTable 
		WHERE 
			numItemCode = v_numItemCode
			AND coalesce(numCurrencyID,0) = 0
			AND coalesce(numPriceRuleID,0) = 0
	) t3
	ON
		t2.PriceLevelID = t3.RowID
	WHERE 
		t1.PriceLevelID = t2.PriceLevelID;

   select   tintRuleType, tintDiscountType INTO v_localRuleType,v_localDiscountType FROM tt_DEFUALT AS t2 ORDER BY PriceLevelID ASC LIMIT 1;

   IF coalesce(v_localRuleType,0) = 0 then
	
      RAISE EXCEPTION 'INVALID_PRICE_RULE_TYPE';
      RETURN;
   ELSEIF (v_localRuleType = 1 OR v_localRuleType = 2) AND v_localDiscountType NOT IN(1,2,3)
   then
	
      RAISE EXCEPTION 'INVALID_PRICE_LEVEL_DISCOUNT_TYPE';
      RETURN;
   ELSEIF v_localRuleType = 3
   then
	
      v_tintDiscountType := 0;
   end if;

	-- REPLACE -1 WITH NEW PRICES IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY FROM IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY TO IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel1,0) <> -1 THEN coalesce(v_monPriceLevel1,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel1FromQty,0) <> -1 THEN coalesce(v_intPriceLevel1FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel1ToQty,0) <> -1 THEN coalesce(v_intPriceLevel1ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel1Name,'') <> '-1' THEN coalesce(v_vcPriceLevel1Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 1;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel2,0) <> -1 THEN coalesce(v_monPriceLevel2,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel2FromQty,0) <> -1 THEN coalesce(v_intPriceLevel2FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel2ToQty,0) <> -1 THEN coalesce(v_intPriceLevel2ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel2Name,'') <> '-1' THEN coalesce(v_vcPriceLevel2Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 2;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel3,0) <> -1 THEN coalesce(v_monPriceLevel3,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel3FromQty,0) <> -1 THEN coalesce(v_intPriceLevel3FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel3ToQty,0) <> -1 THEN coalesce(v_intPriceLevel3ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel3Name,'') <> '-1' THEN coalesce(v_vcPriceLevel3Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 3;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel4,0) <> -1 THEN coalesce(v_monPriceLevel4,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel4FromQty,0) <> -1 THEN coalesce(v_intPriceLevel4FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel4ToQty,0) <> -1 THEN coalesce(v_intPriceLevel4ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel4Name,'') <> '-1' THEN coalesce(v_vcPriceLevel4Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 4;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel5,0) <> -1 THEN coalesce(v_monPriceLevel5,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel5FromQty,0) <> -1 THEN coalesce(v_intPriceLevel5FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel5ToQty,0) <> -1 THEN coalesce(v_intPriceLevel5ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel5Name,'') <> '-1' THEN coalesce(v_vcPriceLevel5Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 5;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel6,0) <> -1 THEN coalesce(v_monPriceLevel6,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel6FromQty,0) <> -1 THEN coalesce(v_intPriceLevel6FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel6ToQty,0) <> -1 THEN coalesce(v_intPriceLevel6ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel6Name,'') <> '-1' THEN coalesce(v_vcPriceLevel6Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 6;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel7,0) <> -1 THEN coalesce(v_monPriceLevel7,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel7FromQty,0) <> -1 THEN coalesce(v_intPriceLevel7FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel7ToQty,0) <> -1 THEN coalesce(v_intPriceLevel7ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel7Name,'') <> '-1' THEN coalesce(v_vcPriceLevel7Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 7;

   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel8,0) <> -1 THEN coalesce(v_monPriceLevel8,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel8FromQty,0) <> -1 THEN coalesce(v_intPriceLevel8FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel8ToQty,0) <> -1 THEN coalesce(v_intPriceLevel8ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel8Name,'') <> '-1' THEN coalesce(v_vcPriceLevel8Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 8;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel9,0) <> -1 THEN coalesce(v_monPriceLevel9,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel9FromQty,0) <> -1 THEN coalesce(v_intPriceLevel9FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel9ToQty,0) <> -1 THEN coalesce(v_intPriceLevel9ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel9Name,'') <> '-1' THEN coalesce(v_vcPriceLevel9Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 9;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel10,0) <> -1 THEN coalesce(v_monPriceLevel10,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel10FromQty,0) <> -1 THEN coalesce(v_intPriceLevel10FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel10ToQty,0) <> -1 THEN coalesce(v_intPriceLevel10ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel10Name,'') <> '-1' THEN coalesce(v_vcPriceLevel10Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 10;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel11,0) <> -1 THEN coalesce(v_monPriceLevel11,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel11FromQty,0) <> -1 THEN coalesce(v_intPriceLevel11FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel11ToQty,0) <> -1 THEN coalesce(v_intPriceLevel11ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel11Name,'') <> '-1' THEN coalesce(v_vcPriceLevel11Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 11;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel12,0) <> -1 THEN coalesce(v_monPriceLevel12,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel12FromQty,0) <> -1 THEN coalesce(v_intPriceLevel12FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel12ToQty,0) <> -1 THEN coalesce(v_intPriceLevel12ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel12Name,'') <> '-1' THEN coalesce(v_vcPriceLevel12Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 12;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel13,0) <> -1 THEN coalesce(v_monPriceLevel13,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel13FromQty,0) <> -1 THEN coalesce(v_intPriceLevel13FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel13ToQty,0) <> -1 THEN coalesce(v_intPriceLevel13ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel13Name,'') <> '-1' THEN coalesce(v_vcPriceLevel13Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 13;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel14,0) <> -1 THEN coalesce(v_monPriceLevel14,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel14FromQty,0) <> -1 THEN coalesce(v_intPriceLevel14FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel14ToQty,0) <> -1 THEN coalesce(v_intPriceLevel14ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel14Name,'') <> '-1' THEN coalesce(v_vcPriceLevel14Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 14;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel15,0) <> -1 THEN coalesce(v_monPriceLevel15,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel15FromQty,0) <> -1 THEN coalesce(v_intPriceLevel15FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel15ToQty,0) <> -1 THEN coalesce(v_intPriceLevel15ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel15Name,'') <> '-1' THEN coalesce(v_vcPriceLevel15Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 15;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel16,0) <> -1 THEN coalesce(v_monPriceLevel16,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel16FromQty,0) <> -1 THEN coalesce(v_intPriceLevel16FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel16ToQty,0) <> -1 THEN coalesce(v_intPriceLevel16ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel16Name,'') <> '-1' THEN coalesce(v_vcPriceLevel16Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 16;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel17,0) <> -1 THEN coalesce(v_monPriceLevel17,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel17FromQty,0) <> -1 THEN coalesce(v_intPriceLevel17FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel17ToQty,0) <> -1 THEN coalesce(v_intPriceLevel17ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel17Name,'') <> '-1' THEN coalesce(v_vcPriceLevel17Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 17;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel18,0) <> -1 THEN coalesce(v_monPriceLevel18,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel18FromQty,0) <> -1 THEN coalesce(v_intPriceLevel18FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel18ToQty,0) <> -1 THEN coalesce(v_intPriceLevel18ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel18Name,'') <> '-1' THEN coalesce(v_vcPriceLevel18Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 18;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel19,0) <> -1 THEN coalesce(v_monPriceLevel19,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel19FromQty,0) <> -1 THEN coalesce(v_intPriceLevel19FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel19ToQty,0) <> -1 THEN coalesce(v_intPriceLevel19ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel19Name,'') <> '-1' THEN coalesce(v_vcPriceLevel19Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 19;
	
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN coalesce(v_monPriceLevel20,0) <> -1 THEN coalesce(v_monPriceLevel20,0) ELSE monPrice END),intFromQty =(CASE WHEN coalesce(v_intPriceLevel20FromQty,0) <> -1 THEN coalesce(v_intPriceLevel20FromQty,0) ELSE intFromQty END),
   intToQty =(CASE WHEN coalesce(v_intPriceLevel20ToQty,0) <> -1 THEN coalesce(v_intPriceLevel20ToQty,0) ELSE intToQty END),
   vcName =(CASE WHEN coalesce(v_vcPriceLevel20Name,'') <> '-1' THEN coalesce(v_vcPriceLevel20Name,cast(0 as TEXT)) ELSE vcName END)
   WHERE
   PriceLevelID = 20;
	
   select   MAX(PriceLevelID) INTO v_MaxPriceLevelIDWithSomeValue FROM tt_DEFUALT WHERE (monPrice <> -1 OR intFromQty <> -1 OR intToQty <> -1);

	-- NOW REPLACE PRICE -1 TO 0 IN ALL PRICE LEVELS WHICH ARE LESS THAN @MaxPriceLevelIDWithSomeValue 
	-- (THIS REQUIRED BECAUSE IF USER HAS PROVIDED PRICE LEVEL 1 AND PRICE LEVEL 6 COLUMN IN IMPORT FILE THEN WE HAVE TO ADD 0 FROM PRICE LEVEL 2,3,4,5)
   UPDATE
   tt_DEFUALT
   SET
   monPrice =(CASE WHEN monPrice = -1 THEN 0 ELSE monPrice END),intFromQty =(CASE WHEN intFromQty = -1 THEN 0 ELSE intFromQty END),
   intToQty =(CASE WHEN intToQty = -1 THEN 0 ELSE intToQty END),
   vcName =(CASE WHEN vcName = '-1' THEN '' ELSE vcName END)
   WHERE
   PriceLevelID <= v_MaxPriceLevelIDWithSomeValue
   AND (monPrice = -1 OR intFromQty = -1 OR intToQty = -1);

   IF EXISTS(SELECT * FROM tt_DEFUALT WHERE PriceLevelID <= v_MaxPriceLevelIDWithSomeValue AND tintRuleType IN(1,2) AND (coalesce(intFromQty,0) = 0 OR coalesce(intToQty,0) = 0)) then
	
      RAISE EXCEPTION 'QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0';
      RETURN;
   end if;

   IF EXISTS(SELECT * FROM tt_DEFUALT WHERE PriceLevelID <= v_MaxPriceLevelIDWithSomeValue AND tintRuleType IN(1,2) AND coalesce(tintDiscountType,0) = 0) then
	
      RAISE EXCEPTION 'INVALID_PRICE_LEVEL_DISCOUNT_TYPE';
      RETURN;
   end if;

   BEGIN
      -- BEGIN TRANSACTION

DELETE FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numCurrencyID,0) = 0;
      INSERT INTO PricingTable(numPriceRuleID,numItemCode,vcName,decDiscount,tintRuleType,tintDiscountType,intFromQty,intToQty)
      SELECT
      0,v_numItemCode,vcName,monPrice,tintRuleType,(CASE WHEN tintRuleType = 3 THEN 0 ELSE tintDiscountType END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intFromQty END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intToQty END)
      FROM
      tt_DEFUALT
      WHERE
      PriceLevelID <= v_MaxPriceLevelIDWithSomeValue;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;


