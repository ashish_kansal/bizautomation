-- Stored procedure definition script USP_ReportListMaster_PendingOrdersStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ReportListMaster_PendingOrdersStatus(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN

-- Pending to bill ---------------------------
   open SWV_RefCur for
   select
   sum(Temp.Pendingtobill*Temp.AverageCost) as PendingToBill
   from(select
      coalesce((coalesce(OpportunityItems.numUnitHourReceived,0)) -(coalesce((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numoppid = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode   AND coalesce(bitAuthoritativeBizDocs,0) = 1),0)),0) as PendingToBill
,coalesce(Item.monAverageCost,'0') as AverageCost
      FROM OpportunityMaster
      inner join DivisionMaster on OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      inner join CompanyInfo on CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
      inner join AdditionalContactsInformation on OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
      inner Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId
      Left Join Item on OpportunityItems.numItemCode = Item.numItemCode
      Left Join Vendor on Vendor.numDomainID = v_numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode = Vendor.numItemCode
      Left Join WareHouseItems  on WareHouseItems.numDomainID = v_numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID
      LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID
      Left Join Warehouses on Warehouses.numDomainID = v_numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
      WHERE 1 = 1
      AND OpportunityMaster.numDomainId = v_numDomainID
      AND (cast(OpportunityMaster.tintshipped as VARCHAR(1))  in('0') AND ((OpportunityMaster.tintopptype = 2 and OpportunityMaster.tintoppstatus = 1)) AND OpportunityItems.numUnitHourReceived > 0)) Temp;
--------------------- Billed but not received-----------------------------------------

   open SWV_RefCur2 for
   select 
--OBDI.numItemCode,OBDI.numUnitHour ,OI.numUnitHourReceived, I.monAverageCost, 
   sum(
--(OBDI.numUnitHour * I.monAverageCost) - (ISNULL( OI.numUnitHourReceived,0) * I.monAverageCost) 
--(OBDI.numUnitHour - OI.numUnitHourReceived) * I.monAverageCost
(OBDI.numUnitHour -OI.numUnitHourReceived)*OI.monPrice)
   as BilledButNotReceived
   from OpportunityMaster OM
   inner join OpportunityBizDocs OBD on OBD.numoppid = OM.numOppId
   inner join OpportunityBizDocItems OBDI on OBDI.numOppBizDocID = OBD.numOppBizDocsId
   inner join OpportunityItems OI on OI.numOppId = OM.numOppId and OBDI.numOppItemID = OI.numoppitemtCode
   left join Item I on OBDI.numItemCode = I.numItemCode
--left join WareHouseItems WI on WI.numItemID = I.numItemCode
--left join Warehouses W on W.numWareHouseID = WI.numWareHouseID
   where OM.tintopptype = 2
   AND OM.numDomainId = v_numDomainID
   AND  cast(OM.tintshipped as VARCHAR(1)) IN('0')
   AND  OM.tintopptype = 2
   AND OM.tintoppstatus = 1
   and OBD.bitAuthoritativeBizDocs = 1
   and I.monAverageCost > 0
   and(OBDI.numUnitHour -OI.numUnitHourReceived) > 0;
					--and (OI.numUnitHour - isnull(OI.numQtyReceived,0))>0 -- this condition was exist in PO fulfillment SP.
					--and (OBDI.numUnitHour * isnull(I.monAverageCost,0)) - (ISNULL( OI.numUnitHourReceived,0) * isnull(I.monAverageCost,0)) > 0 
					--and W.numWareHouseID = 1115 --select for all warehouse 



------------------------------Sold But Not Shipped----------------------------------------------
   open SWV_RefCur3 for
   select
   sum(Temp3.Pendingsales) as PendingSales
   from(Select
      coalesce(OpportunityItems.numUnitHour,0) as units,
coalesce(coalesce(fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,Item.numDomainID, 
      coalesce(OpportunityItems.numUOMId,0)),
      1)*OpportunityItems.numUnitHour,0) as ordered,
coalesce(OpportunityItems.numQtyShipped,0) as shipped,
coalesce(OpportunityItems.monPrice,'0') as unitprice,
coalesce((coalesce(coalesce(fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,Item.numDomainID, 
      coalesce(OpportunityItems.numUOMId,0)),
      1)*OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyShipped,0))*(coalesce(OpportunityItems.monPrice,'0')),0) as PendingSales
      FROM OpportunityMaster
      inner join DivisionMaster on OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      inner join CompanyInfo on CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
      inner join AdditionalContactsInformation on OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
      inner Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId
      Left Join Item on OpportunityItems.numItemCode = Item.numItemCode
      Left Join Vendor on Vendor.numDomainID = v_numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode = Vendor.numItemCode
      Left Join WareHouseItems  on WareHouseItems.numDomainID = v_numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID
      LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID
      Left Join Warehouses on Warehouses.numDomainID = v_numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
      WHERE 1 = 1
      AND OpportunityMaster.numDomainId = v_numDomainID AND (((OpportunityMaster.tintopptype = 1 and OpportunityMaster.tintoppstatus = 1)) AND Item.charItemType  in('P') AND cast(OpportunityMaster.tintshipped as VARCHAR(1))  in('0'))) Temp3
   where Pendingsales > 0;
   RETURN;
 --ORDER BY Pendingsales desc

END; $$;


