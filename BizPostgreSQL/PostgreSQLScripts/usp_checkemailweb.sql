-- Stored procedure definition script USP_CheckEmailWeb for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckEmailWeb(v_vcEmail VARCHAR(50) DEFAULT '',      
v_vcWebsite VARCHAR(100)  DEFAULT '',      
 v_numDomainId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select  vcEmail from AdditionalContactsInformation   where LOWER(vcEmail) = LOWER(v_vcEmail)
   and numDomainID =(case when v_vcEmail = '' then 0 else v_numDomainId      end) LIMIT 1;  
      
      
      
   open SWV_RefCur2 for
   select  vcWebSite from CompanyInfo where vcWebSite ilike '%' || coalesce(v_vcWebsite,'') || '%'
   and numDomainID =(case when v_vcWebsite = '' then -1 else v_numDomainId      end) LIMIT 1;
   RETURN;
END; $$;


