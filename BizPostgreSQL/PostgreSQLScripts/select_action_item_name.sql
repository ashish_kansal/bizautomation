-- Stored procedure definition script Select_Action_Item_Name for PostgreSQL
CREATE OR REPLACE FUNCTION Select_Action_Item_Name(v_numDomainID NUMERIC(9,0)  ,
v_numUserCntID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select RowID AS "RowID", TemplateName AS "TemplateName" From tblActionItemData  where numDomainID = v_numDomainID
   and (numCreatedBy = v_numUserCntID or v_numUserCntID = 0);
END; $$;













