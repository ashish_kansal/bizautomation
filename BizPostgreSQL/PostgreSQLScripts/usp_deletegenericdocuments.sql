CREATE OR REPLACE FUNCTION USP_DeleteGenericDocuments(v_numGenericDocID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	DELETE FROM GenericDocuments WHERE numGenericDocID = v_numGenericDocID;
	RETURN;
END; $$;



