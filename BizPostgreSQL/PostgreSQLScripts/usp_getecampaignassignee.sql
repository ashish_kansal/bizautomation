-- Stored procedure definition script USP_GetECampaignAssignee for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetECampaignAssignee(v_numECampaignAssigneeID NUMERIC(9,0)  DEFAULT 0,
               v_numDomainID            NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numECampaignAssigneeID as VARCHAR(255)),
         cast(EA.numECampaignID as VARCHAR(255)),
         cast(EA.numEmailGroupID as VARCHAR(255)),
         FormatedDateFromDate(dtStartDate,EA.numDomainID) AS dtStartDate,
         cast(EA.numDomainID as VARCHAR(255)),
         cast(E.vcECampName as VARCHAR(255)),
         cast(EG.vcEmailGroupName as VARCHAR(255))
   FROM   ECampaignAssignee EA
   JOIN ECampaign E
   ON EA.numECampaignID = E.numECampaignID
   JOIN ProfileEmailGroup EG ON EA.numEmailGroupID = EG.numEmailGroupID
   WHERE  (numECampaignAssigneeID = v_numECampaignAssigneeID
   OR v_numECampaignAssigneeID = 0)
   AND EA.numDomainID = v_numDomainID;
END; $$;












