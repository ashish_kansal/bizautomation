-- Stored procedure definition script uso_cfwfldDetails for PostgreSQL
CREATE OR REPLACE FUNCTION uso_cfwfldDetails(v_fldid NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  fld_type,
        fld_label,
        fld_tab_ord,
        grp_id,
        subgrp,
        vcURL,
		DTL.bitIsRequired,
		DTL.bitIsNumeric,
		DTL.bitIsAlphaNumeric,
		DTL.bitIsEmail,
		DTL.bitIsLengthValidation,
		DTL.intMaxLength,
		DTL.intMinLength,
		DTL.bitFieldMessage,
		DTL.vcFieldMessage
		,coalesce(M.vcToolTip,'') AS vcToolTip
		,coalesce(bitAutocomplete,false) AS bitAutocomplete
   FROM    CFW_Fld_Master M
   LEFT JOIN CFW_Validation DTL ON M.Fld_id = DTL.numFieldId
   WHERE   fld_id = v_fldid;
END; $$;













