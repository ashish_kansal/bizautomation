-- Stored procedure definition script USP_MANAGE_SHIPPING_LABEL_RULE_CHILD for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_MANAGE_SHIPPING_LABEL_RULE_CHILD(v_numChildShipID NUMERIC(18,0),
	v_numShippingRuleID NUMERIC(18,0),
	v_numFromValue NUMERIC(18,2),
	v_numToValue NUMERIC(18,2),
	v_numDomesticShipID NUMERIC(18,0),
	v_numInternationalShipID NUMERIC(18,0),
	v_numPackageTypeID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numChildShipID FROM ShippingLabelRuleChild WHERE numChildShipID = v_numChildShipID) then

      UPDATE ShippingLabelRuleChild SET
      numShippingRuleID = v_numShippingRuleID,numFromValue = v_numFromValue,
      numToValue = v_numToValue,numDomesticShipID = v_numDomesticShipID,numInternationalShipID = v_numInternationalShipID,
      numPackageTypeID = v_numPackageTypeID
      WHERE
      numChildShipID = v_numChildShipID;
   ELSE
      INSERT INTO ShippingLabelRuleChild(numShippingRuleID,
		numFromValue,
		numToValue,
		numDomesticShipID,
		numInternationalShipID,
		numPackageTypeID) VALUES(v_numShippingRuleID,
		v_numFromValue,
		v_numToValue,
		v_numDomesticShipID,
		v_numInternationalShipID,
		v_numPackageTypeID);
   end if;
   RETURN;
END; $$;



