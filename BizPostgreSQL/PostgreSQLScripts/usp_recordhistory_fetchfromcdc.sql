-- Stored procedure definition script USP_RecordHistory_FetchFromCDC for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecordHistory_FetchFromCDC()
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	-- GET Opp/Order History
   PERFORM USP_RecordHistory_FetchFromCDCOppOrder();
   RETURN;
END; $$;



