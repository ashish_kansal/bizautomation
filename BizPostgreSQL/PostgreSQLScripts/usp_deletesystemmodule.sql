-- Stored procedure definition script usp_DeleteSystemModule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteSystemModule(v_numModuleID INTEGER   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ModuleMaster
   WHERE numModuleID = v_numModuleID;
   RETURN;
END; $$;


