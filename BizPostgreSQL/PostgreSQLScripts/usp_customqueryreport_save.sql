-- Stored procedure definition script USP_CustomQueryReport_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_Save(v_numReportID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_vcReportName VARCHAR(300),
	v_vcReportDescription VARCHAR(300),
	v_vcEmailTo VARCHAR(1000),
	v_tintEmailFrequency SMALLINT,
	v_vcQuery TEXT,
	v_vcCSS TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numReportID,0) = 0 then --INSERT
	
      INSERT INTO CustomQueryReport(numDomainID,
			vcReportName,
			vcReportDescription,
			vcEmailTo,
			tintEmailFrequency,
			vcQuery,
			vcCSS,
			dtCreatedDate)
		VALUES(v_numDomainID,
			v_vcReportName,
			v_vcReportDescription,
			v_vcEmailTo,
			v_tintEmailFrequency,
			v_vcQuery,
			v_vcCSS,
			LOCALTIMESTAMP);
   ELSE
      UPDATE
      CustomQueryReport
      SET
      numDomainID = v_numDomainID,vcReportName = v_vcReportName,vcReportDescription = v_vcReportDescription,
      vcEmailTo = v_vcEmailTo,tintEmailFrequency = v_tintEmailFrequency,
      vcQuery = v_vcQuery,vcCSS = v_vcCSS
      WHERE
      numReportID = v_numReportID;
   end if;
   RETURN;
END; $$;

