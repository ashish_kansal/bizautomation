-- Stored procedure definition script USP_StagePercentageDetailsTaskTimeLog_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTaskTimeLog_Delete(v_numDomainID NUMERIC(18,0)
	,v_ID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_ID = -1 then
	
      DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
      UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract = false,bitTaskClosed = false WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
   ELSE
      DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND ID = v_ID;
   end if;
   RETURN;
END; $$;


