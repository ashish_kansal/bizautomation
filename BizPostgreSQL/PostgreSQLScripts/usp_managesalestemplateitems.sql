-- Stored procedure definition script USP_ManageSalesTemplateItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageSalesTemplateItems(v_numSalesTemplateItemID NUMERIC DEFAULT 0,
    v_numSalesTemplateID NUMERIC DEFAULT NULL,
    v_strItems TEXT DEFAULT NULL,
    v_numDomainID NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN

   IF v_numSalesTemplateItemID = 0 then
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then

         INSERT  INTO SalesTemplateItems(numSalesTemplateID,
                              numItemCode,
                              numUnitHour,
                              monPrice,
                              monTotAmount,
                              numSourceID,
                              numWarehouseID,
                              Warehouse,
                              numWarehouseItmsID,
                              ItemType,
                              Attributes,
                              AttrValues,
                              FreeShipping,
                              Weight,
                              Op_Flag,
                              bitDiscountType,
                              fltDiscount,
                              monTotAmtBefDiscount,
                              ItemURL,
                              numDomainID,
                              numUOM,
                              vcUOMName,
                              UOMConversionFactor)
         SELECT  v_numSalesTemplateID,
                                    X.numItemCode,
                                    X.numUnitHour,
                                    X.monPrice,
                                    X.monTotAmount,
                                    X.numSourceID,
                                    X.numWarehouseId,
                                    X.Warehouse,
                                    X.numWarehouseItmsID,
                                    X.vcItemType,
                                    X.Attributes,
                                    X.AttrValues,
                                    X.FreeShipping,
                                    X.Weight,
                                    X.Op_Flag,
                                    X.bitDiscountType,
                                    X.fltDiscount,
                                    X.monTotAmtBefDiscount,
                                    X.ItemURL,
                                    v_numDomainID,
                                    X.numUOM,
									X.vcUOMName,
									X.UOMConversionFactor
         FROM
		 XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numItemCode NUMERIC(18,0) PATH 'numItemCode'
					,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
					,monPrice DECIMAL(20,5) PATH 'monPrice'
					,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					,numSourceID NUMERIC(18,0) PATH 'numSourceID'
					,numWarehouseId NUMERIC(18,0) PATH 'numWarehouseId'
					,Warehouse VARCHAR(50) PATH 'Warehouse'
					,numWarehouseItmsID NUMERIC(18,0) PATH 'numWarehouseItmsID'
					,vcItemType VARCHAR(50) PATH 'vcItemType'
					,Attributes VARCHAR(1000) PATH 'Attributes'
					,AttrValues VARCHAR(1000) PATH 'AttrValues'
					,FreeShipping BOOLEAN PATH 'FreeShipping'
					,Weight DOUBLE PRECISION PATH 'Weight'
					,Op_Flag SMALLINT PATH 'Op_Flag'
					,bitDiscountType BOOLEAN PATH 'bitDiscountType'
					,fltDiscount DOUBLE PRECISION PATH 'fltDiscount'
					,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
					,ItemURL VARCHAR(200) PATH 'ItemURL'
					,numUOM NUMERIC PATH 'numUOM'
					,vcUOMName VARCHAR(50) PATH 'vcUOMName'
					,UOMConversionFactor DECIMAL(30,16) PATH 'UOMConversionFactor'
			) X
         WHERE   X.numItemCode NOT IN(SELECT  numItemCode
            FROM    SalesTemplateItems
            WHERE   numSalesTemplateID = v_numSalesTemplateID);
   
      end if;
      open SWV_RefCur for SELECT  CURRVAL('SalesTemplateItems_seq') AS InsertedID;
   end if;
END; $$;			
    
    












