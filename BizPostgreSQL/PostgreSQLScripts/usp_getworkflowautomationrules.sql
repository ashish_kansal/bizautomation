-- Stored procedure definition script USP_GetWorkflowAutomationRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_GetWorkflowAutomationRules(v_numDomainID NUMERIC(18,0),
	v_tintMode SMALLINT DEFAULT 0,
	v_numAutomationID NUMERIC(18,0) DEFAULT 0,
	v_numRuleID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
If v_tintMode = 0 then

      open SWV_RefCur for
      SELECT
      numAutomationID,
	numRuleID,
	numBizDocTypeId,
	numBizDocStatus1,
	numBizDocStatus2,
	tintOppType,
	numOrderStatus,
	numDomainID,
	numUserCntID,
	numEmailTemplate,
	numBizDocCreatedFrom,
	numOpenRecievePayment,
	numCreditCardOption,
	dtCreated,
	dtModified
      FROM
      WorkflowAutomation
      WHERE (numAutomationID = v_numAutomationID or v_numAutomationID = 0)
      AND (numRuleID = v_numRuleID or v_numRuleID = 0) AND bitIsActive = true AND numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      SELECT
      numAutomationID,
	numRuleID,
	numBizDocTypeId,
	numBizDocStatus1,
	numBizDocStatus2,
	tintOppType,
	numOrderStatus,
	numDomainID,
	numUserCntID,
	numEmailTemplate,
	numBizDocCreatedFrom,
	numOpenRecievePayment,
	numCreditCardOption,
	dtCreated,
	dtModified
      FROM
      WorkflowAutomation
      WHERE (numRuleID = v_numRuleID or v_numRuleID = 0)
      AND bitIsActive = true
      AND numDomainID = v_numDomainID
      AND numBizDocCreatedFrom IN(1,2);
   end if;
   RETURN;
END; $$;



