-- Function definition script GetBizDocVendorCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetBizDocVendorCost(v_numOppBizDocID NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_VendorCost  DECIMAL(10,2);
BEGIN
   select   SUM(coalesce(V.monCost,0)*coalesce(BI.numUnitHour,0)) INTO v_VendorCost FROM    OpportunityBizDocItems BI
   INNER JOIN Item I ON I.numItemCode = BI.numItemCode
   LEFT OUTER JOIN Vendor V ON V.numVendorID = I.numVendorID
   AND V.numItemCode = I.numItemCode WHERE   BI.numOppBizDocID = v_numOppBizDocID;

   RETURN v_VendorCost;
END; $$; 
/****** Object:  UserDefinedFunction [dbo].[GetCashInflowForMonths]    Script Date: 07/26/2008 18:12:55 ******/

