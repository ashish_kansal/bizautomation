-- Stored procedure definition script spCalculateAge for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION spCalculateAge()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Age  INTEGER;
BEGIN
   v_Age := extract(year from LOCALTIMESTAMP) -extract(year from '03/04/1964':: date);
   RAISE NOTICE '%','You are ' || SUBSTR(CAST(v_Age AS VARCHAR(3)),1,3)  || ' Years old';
   RETURN;
END; $$;


