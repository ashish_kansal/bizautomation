-- Stored procedure definition script USP_ModifyContract for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ModifyContract(v_numContractId BIGINT ,      
v_numDivisionID BIGINT,      
v_vcContractName VARCHAR(250),      
v_bintStartDate TIMESTAMP,      
v_bintExpDate TIMESTAMP,      
v_numIncidents BIGINT,      
v_numHours BIGINT,      
v_vcNotes TEXT,      
v_numEmailDays BIGINT,      
v_numEmailIncidents BIGINT,      
v_numEmailHours BIGINT,      
v_numUserCntID BIGINT,      
v_numDomainID BIGINT,      
v_numAmount BIGINT,    
v_bitAmount BOOLEAN,    
v_bitDays BOOLEAN,    
v_bitHours BOOLEAN,    
v_bitInci BOOLEAN ,  
v_bitEDays BOOLEAN,    
v_bitEHours BOOLEAN,    
v_bitEInci BOOLEAN      ,  
 v_numTemplateId BIGINT  ,
v_rateHr DECIMAL(10,2))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update ContractManagement
   set numDivisionID = v_numDivisionID,vcContractName = v_vcContractName,bintStartDate = v_bintStartDate,
   bintExpDate = v_bintExpDate,numincidents = v_numIncidents,
   numHours = v_numHours,vcNotes = v_vcNotes,numEmailDays = v_numEmailDays,
   numEmailIncidents = v_numEmailIncidents,numEmailHours = v_numEmailHours,
   numAmount = v_numAmount,bitAmount = v_bitAmount,bitDays = v_bitDays,
   bitHour = v_bitHours,bitIncidents  = v_bitInci,bitEDays = v_bitEDays,bitEHour = v_bitEHours,
   bitEIncidents  = v_bitEInci,numTemplateId = v_numTemplateId,
   decrate =  v_rateHr
   where
   numContractId = v_numContractId and
   numUserCntID = v_numUserCntID and
   numdomainId = v_numDomainID;
   RETURN;
END; $$;


