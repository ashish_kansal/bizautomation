-- Stored procedure definition script usp_getCustomReports4MyReports for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustomReports4MyReports(v_numUserCntID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numCustReportID as VARCHAR(255)), vcReportName || ' (' || vcReportDescription ||
   ')' AS vcReportName FROM CustRptConfigMaster
   WHERE numCreatedBy = v_numUserCntID
   ORDER BY numCreatedDate DESC;
END; $$;












