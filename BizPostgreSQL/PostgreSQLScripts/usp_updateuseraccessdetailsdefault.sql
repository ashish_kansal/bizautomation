-- Stored procedure definition script usp_UpdateUserAccessDetailsDefault for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--created by anoop jayaraj                      
Create or replace FUNCTION usp_UpdateUserAccessDetailsDefault(v_numUserID NUMERIC(9,0),
 v_numDomainID NUMERIC(9,0),                              
 v_byteMode SMALLINT,  
 v_tintDefaultRemStatus SMALLINT,                                   
 v_numDefaultTaskType NUMERIC(9,0),
 v_bitOutlook BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_byteMode = 1 then
	
      UPDATE UserMaster SET tintDefaultRemStatus = v_tintDefaultRemStatus  WHERE numUserId = v_numUserID and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 2
   then
	
      UPDATE UserMaster SET numDefaultTaskType = v_numDefaultTaskType  WHERE numUserId = v_numUserID and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 3
   then
	
      UPDATE UserMaster SET bitOutlook = v_bitOutlook  WHERE numUserId = v_numUserID and numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;
          
  


