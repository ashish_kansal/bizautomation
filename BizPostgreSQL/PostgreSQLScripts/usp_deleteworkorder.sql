-- Stored procedure definition script USP_DeleteWorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteWorkOrder(v_numWOId NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_numDomainID NUMERIC(18,0),
v_bitOrderDelete BOOLEAN,
v_tintMode SMALLINT --1: Order/Item, 2: Pick List
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_COUNT  INTEGER;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;
   v_numOppId  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_numWOStatus  NUMERIC(18,0);
   v_onHand  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;
   v_onBackOrder  DOUBLE PRECISION;
   v_Description  VARCHAR(1000);

   v_numReferenceID  NUMERIC(18,0);
   v_tintRefType  SMALLINT;

  
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
BEGIN
   -- BEGIN TRANSACTION
BEGIN
      CREATE TEMP SEQUENCE tt_WorkOrder_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_WORKORDER CASCADE;
   CREATE TEMPORARY TABLE tt_WORKORDER
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0)
   );

	--GET ALL WORK ORDER CREATED FOR ORDER ASSEMBLY ITEM IN BOTTOM TO TOP ORDER
   INSERT INTO tt_WORKORDER(numWOID)   with recursive CTE(numLevel,numWOID) AS(SELECT
   CAST(1 AS INTEGER) AS numLevel,
			numWOId AS numWOID
   FROM
   WorkOrder
   WHERE
   numParentWOID = v_numWOId
   AND numWOStatus <> 23184
   UNION ALL
   SELECT
   c.numLevel+1 AS numLevel,
			WorkOrder.numWOId AS numWOID
   FROM
   WorkOrder
   INNER JOIN
   CTE c
   ON
   WorkOrder.numParentWOID = c.numWOID AND
   WorkOrder.numWOStatus <> 23184) SELECT numWOID FROM CTE ORDER BY numLevel DESC;
   select   COUNT(*) INTO v_COUNT FROM tt_WORKORDER;
   IF v_COUNT > 0 then
	
      RAISE EXCEPTION 'CHILD_WORK_ORDER_EXISTS';
      RETURN;
   end if;
   IF EXISTS(SELECT
   WorkOrderPickedItems.ID
   FROM
   WorkOrderDetails
   INNER JOIN
   WorkOrderPickedItems
   ON
   WorkOrderDetails.numWODetailId = WorkOrderPickedItems.numWODetailId
   WHERE
   WorkOrderDetails.numWOId = v_numWOId) then
	
      RAISE EXCEPTION 'WORK_ORDER_BOM_PICKED';
      RETURN;
   end if;
   IF coalesce((SELECT numQtyBuilt FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOID = v_numWOId),0) > 0 then
	
      RAISE NOTICE 'WORK_ORDER_QTY_BUILT';
      RETURN;
   end if;
   select   coalesce(numOppId,0) INTO v_numOppId FROM WorkOrder WHERE numWOId = v_numWOId AND WorkOrder.numDomainId = v_numDomainID;
   IF coalesce(v_numOppId,0) > 0 then
	
      v_numReferenceID := v_numOppId;
      v_tintRefType := 3;
   ELSE
      v_numReferenceID := v_numWOId;
      v_tintRefType := 2;
   end if;

	--IF WORK ORDER IS CREATED FROM SALES ORDER AND WORK ORDER IS DELETED NOT WHOLE ORDER THEN EXECUTE FOLLOWING IF CODE
   UPDATE
   OpportunityItems opp
   SET
   bitWorkOrder = false,numWOQty = 0
   FROM
   WorkOrder wo
   WHERE(coalesce(wo.numOppId,0) = opp.numOppId
   AND coalesce(wo.numOppItemID,0) = opp.numoppitemtCode
   AND coalesce(numOppChildItemID,0) = 0
   AND coalesce(numOppKitChildItemID,0) = 0) AND(wo.numWOId = v_numWOId
   AND wo.numDomainId = v_numDomainID);
   select   numWareHouseItemId, numQtyItemsReq, coalesce(numOppId,0), numItemCode, numWOStatus INTO v_numWareHouseItemID,v_numQtyItemsReq,v_numOppId,v_numItemCode,v_numWOStatus FROM
   WorkOrder WHERE
   numWOId = v_numWOId
   AND WorkOrder.numDomainId = v_numDomainID;

	--IF NOT COMPLETED THEN DELETE
   IF v_numWOStatus <> 23184 then
      IF v_numOppId > 0 then
		
         IF v_tintMode = 2 then
			
            v_Description := CONCAT('SO-WO Pick List Deleted (Qty:',v_numQtyItemsReq,')');
         ELSE
            v_Description := CONCAT('SO-WO Work Order Deleted (Qty:',v_numQtyItemsReq,')');
         end if;
      ELSE
         v_Description := CONCAT('Work Order Deleted (Qty:',v_numQtyItemsReq,')');
      end if;

		--GET CURRENT INEVENTORY STATUS
      select   coalesce(numOnHand,0), coalesce(numonOrder,0), coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_onHand,v_onOrder,v_onAllocation,v_onBackOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;

		--CHANGE INVENTORY
      IF v_onOrder < v_numQtyItemsReq then
         v_onOrder := 0;
      ELSE
         v_onOrder := v_onOrder -v_numQtyItemsReq;
      end if;
      PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_onHand,v_numOnAllocation := v_onAllocation,v_numOnBackOrder := v_onBackOrder,
      v_numOnOrder := v_onOrder,v_numWareHouseItemID := v_numWareHouseItemID,
      v_numReferenceID := v_numReferenceID,v_tintRefType := v_tintRefType::SMALLINT,
      v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
      v_Description := v_Description);
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numWarehouseItemID NUMERIC(18,0),
         numQtyItemsReq DOUBLE PRECISION
      );
      INSERT INTO tt_TEMP(numWarehouseItemID, numQtyItemsReq)  SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId = v_numWOId AND coalesce(numWareHouseItemId,0) > 0;
      select   COUNT(*) INTO v_iCount FROM tt_TEMP;
      WHILE v_i <= v_iCount LOOP
         select   coalesce(numWarehouseItemID,0), coalesce(numQtyItemsReq,0) INTO v_numWareHouseItemID,v_numQtyItemsReq FROM tt_TEMP WHERE ID = v_i;
         IF v_numOppId > 0 then
			
            IF v_tintMode = 2 then
				
               v_Description := CONCAT('Items Allocation Reverted O-WO Pick List Deleted (Qty:',v_numQtyItemsReq,
               ')');
            ELSE
               v_Description := CONCAT('Items Allocation Reverted SO-WO Work Order Deleted (Qty:',v_numQtyItemsReq,
               ')');
            end if;
         ELSE
            v_Description := CONCAT('Items Allocation Reverted Work Order Deleted (Qty:',v_numQtyItemsReq,
            ')');
         end if;

			--GET CURRENT INEVENTORY STATUS
         select   coalesce(numOnHand,0), coalesce(numonOrder,0), coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_onHand,v_onOrder,v_onAllocation,v_onBackOrder FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWareHouseItemID;

			--CHANGE INVENTORY
         IF v_numQtyItemsReq >= v_onBackOrder then
			
            v_numQtyItemsReq := v_numQtyItemsReq -v_onBackOrder;
            v_onBackOrder := 0;
            IF (v_onAllocation -v_numQtyItemsReq >= 0) then
               v_onAllocation := v_onAllocation -v_numQtyItemsReq;
            end if;
            v_onHand := v_onHand+v_numQtyItemsReq;
         ELSEIF v_numQtyItemsReq < v_onBackOrder
         then
			
            v_onBackOrder := v_onBackOrder -v_numQtyItemsReq;
         end if;

			--UPDATE INVENTORY AND WAREHOUSE TRACKING
         PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_onHand,v_numOnAllocation := v_onAllocation,v_numOnBackOrder := v_onBackOrder,
         v_numOnOrder := v_onOrder,v_numWareHouseItemID := v_numWareHouseItemID,
         v_numReferenceID := v_numReferenceID,v_tintRefType := v_tintRefType::SMALLINT,
         v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
         v_Description := v_Description);
         v_i := v_i::bigint+1;
      END LOOP;
      DELETE FROM WorkOrderDetails WHERE numWOId = v_numWOId;
      DELETE FROM WorkOrder WHERE numWOId = v_numWOId AND numDomainId = v_numDomainID;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;

