-- Stored procedure definition script USP_GetProjectTotalProgress for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProjectTotalProgress(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numProId NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT -1)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalProgress  INTEGER;
   v_numOppID  NUMERIC;
BEGIN
   IF Not Exists(select * from StagePercentageDetails where v_numProId =(case v_tintMode when 0 then numOppid
   when 1 then numProjectid end) and tinProgressPercentage < 100) then

      v_TotalProgress := 100;
   ELSE


--select * from tFinal

      
      with recursive SW(numStagePercentageId,numStagePercentage,StageProgress) as(select cast(SPD.numStagePercentageId as VARCHAR(255)) AS numStagePercentageId,SPM.numstagepercentage AS numStagePercentage,cast(cast(SPD.tintPercentage as INTEGER)*cast(coalesce(SPD.tinProgressPercentage,1)  as INTEGER) as INTEGER)/100 AS StageProgress
      FROM StagePercentageDetails SPD
      join stagepercentagemaster SPM on SPD.numStagePercentageId = SPM.numstagepercentageid
      where v_numProId =(case v_tintMode when 0 then SPD.numOppid
      when 1 then SPD.numProjectid end)),MW as(select cast(numStagePercentageId as VARCHAR(255)),numStagePercentage, SUM(StageProgress)
      as MileStoneProgress from SW  group by numStagePercentageId,numStagePercentage),
      tRowNumber as(SELECT *,ROW_NUMBER() OVER(ORDER BY numStagePercentage) AS ROWNUMBER FROM MW),tFinal as((select *,cast(numStagePercentage as INTEGER) as diff,
CAST((cast(numStagePercentage as INTEGER)*MileStoneProgress) AS decimal)/100 as total from tRowNumber LIMIT 1)
      union all select f.*,cast((f.numStagePercentage -p.numStagePercentage) as INTEGER) as diff,
CAST((cast((f.numStagePercentage -p.numStagePercentage) as INTEGER)*f.MileStoneProgress) AS decimal)/100 as total
      from tRowNumber f INNER JOIN tFinal p ON p.ROWNUMBER =(f.ROWNUMBER -1))
      select   coalesce(sum(total),0) INTO v_TotalProgress from tFinal;
   end if;

   IF EXISTS(SELECT * FROM ProjectProgress WHERE  v_numProId =(case v_tintMode when 0 then numOppId when 1 then numProId end)  AND numDomainID = v_numDomainID) then
	
      UPDATE ProjectProgress SET intTotalProgress = v_TotalProgress WHERE  v_numProId =(case v_tintMode when 0 then numOppId
      when 1 then numProId end)  AND numDomainID = v_numDomainID;
   ELSE
      IF v_tintMode = 0 then
		 
         v_numOppID := v_numProId;
         v_numProId := null;
      ELSEIF v_tintMode = 1
      then
         v_numOppID := NULL;
      end if;
      INSERT INTO ProjectProgress(numProId,
										   numOppId,
										   numDomainID,
										   intTotalProgress)
		 VALUES( /* numProId - numeric(18, 0) */ v_numProId,
							/* numOppId - numeric(18, 0) */ v_numOppID,
							/* numDomainId - numeric(18, 0) */ v_numDomainID,
							/* intTotalProgress - int */ v_TotalProgress);
   end if;




   RETURN;
END; $$;












