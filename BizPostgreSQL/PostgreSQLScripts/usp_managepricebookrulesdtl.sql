-- Stored procedure definition script USP_ManagePriceBookRulesDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePriceBookRulesDTL(v_numPricRuleID NUMERIC(9,0) DEFAULT 0,    
v_tintAppRuleType SMALLINT DEFAULT NULL,  
v_numValue NUMERIC(9,0) DEFAULT 0,  
v_numProfile NUMERIC(9,0) DEFAULT 0,  
v_numPriceRuleDTLID NUMERIC(9,0) DEFAULT 0,  
v_byteMode SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintRuleFor  SMALLINT;
   v_numDomainID  NUMERIC(9,0);
   v_Step2  SMALLINT;
   v_Step3  SMALLINT;
BEGIN
   select   tintRuleFor INTO v_tintRuleFor from PriceBookRules where numPricRuleID = v_numPricRuleID;

   if v_byteMode = 0 then

      if v_numValue <> 0 then
         if not exists(select * from PriceBookRuleDTL where numRuleID = v_numPricRuleID and numValue = v_numValue and numProfile = v_numProfile and tintType = v_tintAppRuleType) then
	
            IF v_tintAppRuleType > 2 then
		
               IF v_tintAppRuleType = 3 then
			
                  delete from PriceBookRuleDTL where tintType = 4 and numRuleID = v_numPricRuleID;
                  UPDATE PriceBookRules SET tintStep3 = 1 WHERE numPricRuleID = v_numPricRuleID;
               ELSEIF v_tintAppRuleType = 4
               then
			
                  delete from PriceBookRuleDTL where tintType = 3 and numRuleID = v_numPricRuleID;
                  UPDATE PriceBookRules SET tintStep3 = 2 WHERE numPricRuleID = v_numPricRuleID;
               end if;
			
			/*Duplicate Rule values checking */
               select   numDomainID, tintStep2, tintStep3 INTO v_numDomainID,v_Step2,v_Step3 FROM PriceBookRules WHERE numPricRuleID = v_numPricRuleID;
               IF(SELECT COUNT(*)
               FROM   PriceBookRules P
               LEFT OUTER JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
               LEFT OUTER JOIN PriceBookRuleItems PRI ON PRI.numRuleID = PDTL.numRuleID
               WHERE  P.numPricRuleID <> v_numPricRuleID and P.tintRuleFor = v_tintRuleFor
               AND P.numDomainID = v_numDomainID
               AND ( (P.tintStep2 = v_Step2
               AND PDTL.numValue = v_numValue
               AND P.tintStep3 = v_Step3
               AND PRI.numValue IN(SELECT  numValue FROM    PriceBookRuleItems WHERE   numRuleID = v_numPricRuleID)
               AND coalesce(PDTL.numProfile,0) = v_numProfile)
               OR (P.tintStep3 = v_Step3
               AND PRI.numValue = v_numValue
               AND P.tintStep2 = v_Step2
               AND PDTL.numValue IN(SELECT   numValue FROM     PriceBookRuleDTL WHERE numRuleID = v_numPricRuleID)
               AND coalesce(PDTL.numProfile,0) = v_numProfile))) > 0 then
				
                  RAISE EXCEPTION 'DUPLICATE';
                  RETURN;
               end if;
               insert into PriceBookRuleDTL(numRuleID,numValue,numProfile,tintType)
					values(v_numPricRuleID,v_numValue,v_numProfile,v_tintAppRuleType);
            ELSE
               IF v_tintAppRuleType = 1 then
			
                  delete from PriceBookRuleItems where tintType = 2 and numRuleID = v_numPricRuleID;
                  UPDATE PriceBookRules SET tintStep2 = 1 WHERE numPricRuleID = v_numPricRuleID;
               ELSEIF v_tintAppRuleType = 2
               then
			
                  delete from PriceBookRuleItems where tintType = 1 and numRuleID = v_numPricRuleID;
                  UPDATE PriceBookRules SET tintStep2 = 2 WHERE numPricRuleID = v_numPricRuleID;
               end if;
			
			
			/*Duplicate Rule values checking */
               select   numDomainID, tintStep2, tintStep3 INTO v_numDomainID,v_Step2,v_Step3 FROM PriceBookRules WHERE numPricRuleID = v_numPricRuleID;
               IF(SELECT COUNT(*)
               FROM   PriceBookRules P
               LEFT OUTER JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
               LEFT OUTER JOIN PriceBookRuleItems PRI ON PRI.numRuleID = PDTL.numRuleID
               WHERE  P.numPricRuleID <> v_numPricRuleID and P.tintRuleFor = v_tintRuleFor
               AND P.numDomainID = v_numDomainID
               AND ( (P.tintStep2 = v_Step2
               AND PDTL.numValue = v_numValue
               AND P.tintStep3 = v_Step3
               AND PRI.numValue IN(SELECT  numValue FROM    PriceBookRuleItems WHERE   numRuleID = v_numPricRuleID)
               AND coalesce(PDTL.numProfile,0) = v_numProfile)
               OR (P.tintStep3 = v_Step3
               AND PRI.numValue = v_numValue
               AND P.tintStep2 = v_Step2
               AND PDTL.numValue IN(SELECT   numValue FROM     PriceBookRuleDTL WHERE numRuleID = v_numPricRuleID)
               AND coalesce(PDTL.numProfile,0) = v_numProfile))) > 0 then
				
                  RAISE EXCEPTION 'DUPLICATE';
                  RETURN;
               end if;
               insert into PriceBookRuleItems(numRuleID,numValue,tintType)
					values(v_numPricRuleID,v_numValue,v_tintAppRuleType);
            end if;
         end if;
      end if;
   ELSEIF v_byteMode = 1
   then

      IF (v_tintAppRuleType = 1 OR v_tintAppRuleType = 2) then
         DELETE FROM PriceBookRuleItems WHERE numPriceBookItemID = v_numPriceRuleDTLID;
      ELSE
         delete from PriceBookRuleDTL where numPriceBookRuleDTLID = v_numPriceRuleDTLID;
      end if;
   end if;
END; $$;


