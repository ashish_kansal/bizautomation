-- Stored procedure definition script USP_ManageRoles for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRoles(v_byteMode SMALLINT,  
v_RolesID NUMERIC(9,0) DEFAULT 0,  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,  
v_fltPercentage DOUBLE PRECISION DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      delete from UserRoles where numUserCntID = v_numUserCntID and numRole = v_RolesID;
      insert into UserRoles(numRole,numUserCntID,fltPercentage)
        values(v_RolesID,v_numUserCntID,v_fltPercentage);
        
      open SWV_RefCur for
      select 0;
   end if;  
  
   if v_byteMode = 1 then

      delete from UserRoles where numUserCntID = v_numUserCntID and numRole = v_RolesID;
      open SWV_RefCur for
      select 1;
   end if;  
  
   if v_byteMode = 2 then

      open SWV_RefCur for
      select * from UserRoles
      join Listdetails on
      numRole = numListItemID where numUserCntID = v_numUserCntID;
   end if;
   RETURN;
END; $$;


