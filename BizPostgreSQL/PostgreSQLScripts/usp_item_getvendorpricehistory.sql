-- Stored procedure definition script USP_Item_GetVendorPriceHistory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetVendorPriceHistory(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numVendorID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   FormatedDateFromDate(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS vcCreatedDate
		,cast(coalesce(OM.vcpOppName,cast(0 as TEXT)) as VARCHAR(100)) AS vcPOppName
		,fn_UOMConversion(OI.numUOMId,v_numItemCode,v_numItemCode,I.numBaseUnit) AS numUnitHour
		,fn_GetUOMName(numUOMId) AS vcUOMName
		,CAST(coalesce(OI.monPrice,0) AS DECIMAL(20,5)) AS monPrice
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numDivisionId = v_numVendorID
   AND OI.numItemCode = v_numItemCode
   AND tintopptype = 2
   AND tintoppstatus = 1
   AND coalesce(bitStockTransfer,false) = false
   ORDER BY
   OM.numOppId DESC LIMIT 100;
   RETURN;
END; $$;













