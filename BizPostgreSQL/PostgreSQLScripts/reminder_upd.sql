CREATE OR REPLACE FUNCTION Reminder_Upd
(
	v_Status INTEGER,
	v_ActivityID INTEGER
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		Activity
	SET
		Status = v_Status
		,EnableReminder = false
	WHERE
		(Activity.ActivityID = v_ActivityID  AND Activity.RecurrenceID =(-999));
   RETURN;
END; $$;


