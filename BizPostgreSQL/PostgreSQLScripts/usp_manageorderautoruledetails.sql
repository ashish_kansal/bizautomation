-- Stored procedure definition script USP_ManageOrderAutoRuleDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageOrderAutoRuleDetails(v_numRuleID INTEGER,
v_numItemTypeID NUMERIC,
v_numItemClassID NUMERIC,
v_numBillToID INTEGER,
v_numShipToID INTEGER,
v_btFullPaid BOOLEAN,
v_numBizDocStatus NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO OrderAutoRuleDetails  
   (numRuleID,numItemTypeID,numItemClassID,numBillToID,numShipToID,btFullPaid,numBizDocStatus,btActive)
   SELECT v_numRuleID,v_numItemTypeID,v_numItemClassID,v_numBillToID,v_numShipToID,v_btFullPaid,v_numBizDocStatus,true;

open SWV_RefCur for SELECT CURRVAL('OrderAutoRuleDetails_seq');


   delete from OrderAutoRule;
   RETURN;
END; $$;













