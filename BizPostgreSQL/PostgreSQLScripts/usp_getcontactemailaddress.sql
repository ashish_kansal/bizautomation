CREATE OR REPLACE FUNCTION USP_GetContactEmailAddress
(            
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_keyWord VARCHAR(100) DEFAULT '',               
	v_CurrentPage INTEGER DEFAULT NULL,                  
	v_PageSize INTEGER DEFAULT NULL,               
	INOUT v_TotRecs INTEGER  DEFAULT NULL,
	v_columnName VARCHAR(50) DEFAULT NULL,                  
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_bitFlag BOOLEAN DEFAULT false,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql                                    
AS $$
	DECLARE
	v_strSql VARCHAR(4000);
	v_firstRec INTEGER;
	v_lastRec INTEGER;
BEGIN
	IF v_bitFlag = false then
		v_strSql :=  'SELECT numcontactid AS "numcontactID",
			CompanyName AS "CompanyName",
			Company AS "Company",
			vcEmail AS "vcEmail",
			vcFirstName AS "vcFirstName",
			vcLastName AS "vcLastName",
			numDomainId AS "numDomainId" FROM View_SelectEmailContact WHERE (vcEmail LIKE '''|| v_keyWord ||'%'' OR vcFirstName LIKE '''|| v_keyWord ||'%'' OR vcLastName LIKE '''|| v_keyWord ||'%'' OR CompanyName LIKE '''|| v_keyWord ||'%'') and numDomainID= ' || v_numDomainID || ' LIMIT ' || COALESCE(v_PageSize,0);

		RAISE NOTICE '%',v_strSql;
		OPEN SWV_RefCur FOR EXECUTE v_strSql;
	ELSE
		drop table IF EXISTS tt_TEMPTABLE CASCADE;
		Create TEMPORARY TABLE tt_TEMPTABLE 
		(
			numcontactid NUMERIC,
			CompanyName VARCHAR(255),
			Company VARCHAR(200),
			vcEmail VARCHAR(50),
			vcFirstName VARCHAR(50),
			vcLastName VARCHAR(50),
			numDomainId NUMERIC(18,0),
			ROWNUMBER NUMERIC(18,0),
			bintCreatedDate TIMESTAMP
		);

		v_strSql := '';

		IF v_columnName = 'bintCreatedDate' then
			v_columnName := 'ADC.' || coalesce(v_columnName,'');
		ELSE
			v_columnName := 'vwSM.' || coalesce(v_columnName,'');
		end if;
      
		v_strSql := 'insert into tt_TEMPTABLE SELECT vwSM.*,ROW_NUMBER() OVER (Order by '|| coalesce(v_columnName,'') || '  ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,ADC.bintCreatedDate FROM View_SelectEmailContact vwSM left join AdditionalContactsInformation ADC on ADC.numContactId =  vwSM.numContactId WHERE (vwSM.vcEmail LIKE '''|| v_keyWord ||'%'' OR vwSM.vcFirstName LIKE '''|| v_keyWord ||'%'' OR vwSM.vcLastName LIKE '''|| v_keyWord ||'%''   OR vwSM.CompanyName LIKE '''|| v_keyWord ||'%'') and vwSM.numDomainID= ' || COALESCE(v_numDomainID,0) || ' And LENGTH(COALESCE(vwSM.vcEmail,'''')) > 3';
		v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ';';
		RAISE NOTICE '%',v_strSql;
		EXECUTE v_strSql;

		v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
		v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

		v_strSql := 'select numcontactid AS "numcontactid",
			CompanyName AS "CompanyName",
			Company AS "Company",
			vcEmail AS "vcEmail",
			vcFirstName AS "vcFirstName",
			vcLastName AS "vcLastName",
			numDomainId AS "numDomainId"
			ROWNUMBER AS "ROWNUMBER",
			bintCreatedDate AS "bintCreatedDate" from tt_TEMPTABLE WHERE RowNumber > ' || COALESCE(v_firstRec,0) || ' and RowNumber < ' || COALESCE(v_lastRec,0) || '';

		RAISE NOTICE '%',v_strSql;
		OPEN SWV_RefCur FOR EXECUTE v_strSql;

		SELECT COUNT(*) INTO v_TotRecs from tt_TEMPTABLE;
   end if;
   RETURN;
END; $$;


