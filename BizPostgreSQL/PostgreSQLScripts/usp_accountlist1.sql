DROP FUNCTION IF EXISTS USP_AccountList1;
CREATE OR REPLACE FUNCTION USP_AccountList1(v_CRMType NUMERIC,                                                              
		v_numUserCntID NUMERIC,                                                              
		v_tintUserRightType SMALLINT,                                                              
		v_tintSortOrder NUMERIC DEFAULT 4,                                                                                  
		v_SortChar CHAR(1) DEFAULT '0',  
		v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                       
		v_CurrentPage INTEGER DEFAULT NULL,                                                            
		v_PageSize INTEGER DEFAULT NULL,        
		 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                           
		v_columnName TEXT DEFAULT NULL,                                                            
		v_columnSortOrder VARCHAR(10) DEFAULT NULL    ,                    
		v_numProfile NUMERIC DEFAULT NULL   ,                  
		v_bitPartner BOOLEAN DEFAULT NULL,       
		v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
		v_bitActiveInActive BOOLEAN DEFAULT NULL,
		v_vcRegularSearchCriteria TEXT DEFAULT '',
		v_vcCustomSearchCriteria TEXT DEFAULT ''   ,
		v_SearchText VARCHAR(100) DEFAULT '', 
		INOUT SWV_RefCur refcursor default null,
		INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPerformanceFilter  SMALLINT;

   v_Nocolumns  SMALLINT DEFAULT 0;                

   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_strColumns  TEXT DEFAULT '';
   v_tintOrder  SMALLINT DEFAULT 0;                                                 
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcAssociatedControlType  VARCHAR(20);     
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(200);                      
   v_WhereCondition  TEXT DEFAULT '';                   
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;
   v_bitAllowEdit  BOOLEAN;                   
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;    
   v_vcColumnName  VARCHAR(500);             
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT ''; 
	   
   v_strShareRedordWith  VARCHAR(300);
   v_strExternalUser  TEXT DEFAULT '';
   v_StrSql  TEXT DEFAULT '';
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_firstRec  INTEGER;                                                            
   v_lastRec  INTEGER;                                                            
   v_strFinal  TEXT;
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'^LIKE?','ilike','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'^LIKE?','ilike','gi');

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=1') IN v_vcRegularSearchCriteria),0) > 0 then --Last 3 Months
	
      v_tintPerformanceFilter := 1;
   ELSEIF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=2') IN v_vcRegularSearchCriteria),0) > 0
   then --Last 6 Months
	
      v_tintPerformanceFilter := 2;
   ELSEIF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=3') IN v_vcRegularSearchCriteria),0) > 0
   then --Last 1 Year
	
      v_tintPerformanceFilter := 3;
   ELSE
      v_tintPerformanceFilter := 0;
   end if;

   IF POSITION('AD.numBillCountry' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry');
   ELSEIF POSITION('AD.numShipCountry' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry');
   ELSEIF POSITION('AD.vcBillCity' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity');
   ELSEIF POSITION('AD.vcShipCity' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity');
   end if;
   IF POSITION('ADC.vcLastFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcLastFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=true))');
   end if;
   IF POSITION('ADC.vcNextFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=false))');
   end if;     

	

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );   

   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 36
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 2
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 36
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 2) TotalRows;

   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 36 AND numRelCntType = 2 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;


	--If MasterConfiguration is available then load it otherwise load default columns
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      36,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,2,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 36 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = 2 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      36,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,2,1,true,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 36 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 2 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 36 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = 2 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 36 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 2 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      if v_Nocolumns = 0 then
		
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         36,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,2,1,0,intColumnWidth
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 36
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID
         ORDER BY
         tintOrder ASC;
      end if;
      INSERT INTO
      tt_TEMPFORM
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 36
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 2
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 36
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 2
      AND coalesce(bitCustom,false) = true
      ORDER BY
      tintOrder asc;
   end if;


   v_strColumns := ' COALESCE(ADC.numContactId,0)numContactId,COALESCE(DM.numDivisionID,0)numDivisionID,COALESCE(DM.numTerID,0)numTerID,COALESCE(ADC.numRecOwner,0)numRecOwner,COALESCE(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail ';   

	--Custom field 
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
   tt_TEMPFORM    ORDER BY
   tintOrder ASC LIMIT 1;            
                             
   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         IF v_vcDbColumnName = 'bitEcommerceAccess' then
			
            v_strColumns := coalesce(v_strColumns,'') || ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS "' || coalesce(v_vcColumnName,'') || '"';
         end if;

			----- Added by Priya For Followups(14Jan2018)---- 
			
			----- Added by Priya For Followups(14Jan2018)---- 


         if v_vcDbColumnName = 'vcLastFollowup' then
			
            v_strColumns := coalesce(v_strColumns,'') || ',  COALESCE((CASE WHEN (ADC.numECampaignID > 0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcNextFollowup'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',  COALESCE((CASE WHEN  (ADC.numECampaignID >0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox' or v_vcAssociatedControlType = 'ListBox'
         then
			
            IF v_vcDbColumnName = 'vcSignatureType' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END) '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
            IF v_vcDbColumnName = 'numDefaultShippingServiceID' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR COALESCE(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numPartenerSource'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(DM.numPartenerSource) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource';
            ELSEIF v_vcListItemType = 'LI'
            then
				
               IF v_numListID = 40 then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF LENGTH(v_SearchText) > 0 then
						
                     v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
                  end if;
                  IF v_vcDbColumnName = 'numShipCountry' then
						
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry ';
                  ELSE
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry ';
                  end if;
               ELSEIF v_numListID = 5
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',(CASE WHEN CMP.numCompanyType=47 THEN CONCAT(L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData END)' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF LENGTH(v_SearchText) > 0 then
						
                     v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSE
                  v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF LENGTH(v_SearchText) > 0 then
						
                     v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'S'
            then
				
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               IF v_vcDbColumnName = 'numShipState' then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',ShipState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=true and AD3.numDomainID= DM.numDomainID '
                  || ' left Join State ShipState on ShipState.numStateID=AD3.numState ';
               ELSEIF v_vcDbColumnName = 'numBillState'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',BillState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=true and AD4.numDomainID= DM.numDomainID '
                  || ' left Join State BillState on BillState.numStateID=AD4.numState ';
               ELSE
                  v_strColumns := coalesce(v_strColumns,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'C'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcCampaignName' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcCampaignName ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CampaignMaster C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numCampaignId=DM.' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
            ELSEIF v_vcListItemType = 'SYS'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			v_strColumns := coalesce(v_strColumns,'')
				|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => -1)  AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => 1)  AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || '),'|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
          
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox' AND v_vcDbColumnName = 'vcBillCity'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',AD5.vcCity' || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || ' AD5.vcCity ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
            end if;
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=true and AD5.numDomainID= DM.numDomainID ';
         ELSEIF v_vcAssociatedControlType = 'TextBox' AND  v_vcDbColumnName = 'vcShipCity'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',AD6.vcCity' || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || ' AD6.vcCity ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
            end if;
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=true and AD6.numDomainID= DM.numDomainID ';
         ELSEIF v_vcAssociatedControlType = 'Label' and v_vcDbColumnName = 'vcLastSalesOrderDate'
         then
			
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON TRUE ';
            v_strColumns := coalesce(v_strColumns,'') || ',' || CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate  + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || '),',v_numDomainID,'),'''')') || ' "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcCompactContactDetails'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ' ,'''' AS vcCompactContactDetails';
         ELSEIF v_vcAssociatedControlType = 'Label' and v_vcDbColumnName = 'vcPerformance'
         then
			
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || CONCAT(' LEFT JOIN LATERAL (SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',v_tintPerformanceFilter,
            ' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -3) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																								WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -6) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																								WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(years => -1) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ON TRUE ');
            v_strColumns := coalesce(v_strColumns,'') || ', (CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(to_char(TempPerformance.monDealAmount,''FM9,999,999,999,999,999,990.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'TextBox'  OR v_vcAssociatedControlType = 'Label'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ','
            || case
            when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
            when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
            else v_vcDbColumnName
            end || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) ||
               case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when
               v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end
               || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('' + fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) "' || coalesce(v_vcColumnName,'') || '"';
         ELSE
            v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then
		
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName FROM
         CFW_Fld_Master WHERE
         CFW_Fld_Master.Fld_id = v_numFieldId;
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
			
            v_strColumns := coalesce(v_strColumns,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'''')='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,''0'')::INT end "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
					on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId    ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=(CASE WHEN isnumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value ELSE ''0'' END)::NUMERIC ';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'',''))),'''')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=DM.numDivisionId ';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
      tt_TEMPFORM WHERE
      tintOrder > v_tintOrder -1   ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;     

	  
   v_strShareRedordWith := CONCAT(' DM.numDivisionID IN (SELECT SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=',v_numDomainID,' AND SR.numModuleID=1 AND SR.numAssignedTo=',
   v_numUserCntID,') ');

   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,'::VARCHAR AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))');
   
   v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE(VIE.Total,0) as TotalEmail ';
   v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE(VOA.OpenActionItemCount,0) as TotalActionItem ';

   v_strColumns := coalesce(v_strColumns,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 COALESCE( (CASE WHEN 
				COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) 
				= COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),'''')



	ELSE '''' 
	END)) as FollowupFlag ';

   v_strColumns := coalesce(v_strColumns,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		CONCAT(''('',CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) AS varchar)

			,''/'', CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true),0)AS varchar),'')''
			,''<a onclick=openDrip('' , (cast(COALESCE((SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactID AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true AND ADC.bitPrimaryContact = true),0)AS varchar)),'');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'') 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr ';
   
   v_StrSql := ' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=DM.numDivisionID 
					left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = ' || SUBSTR(CAST(v_numDomainID as VARCHAR(30)),1,30) || ' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE   ON VIE.numDomainID = ' || SUBSTR(CAST(v_numDomainID as VARCHAR(30)),1,30) || ' AND  VIE.numContactId = ADC.numContactId ' || coalesce(v_WhereCondition,'');

   IF v_tintSortOrder = 9 then 
      v_StrSql := coalesce(v_StrSql,'') || ' join Favorites F on F.numContactid=DM.numDivisionID ';
   end if; 
	
	 -------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 36 AND DFCS.numFormID = 36 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then
	
      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
	----------------------------                   
 
   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      v_strColumns := coalesce(v_strColumns,'') || ',tCS.vcColorScheme';
   end if;

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      if v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
		v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
		
         v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
      end if;
   end if;

   IF v_columnName ilike 'CFW.Cust%' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','') || ' ';
      v_columnName := 'CFW.Fld_Value';
   ELSEIF v_columnName ilike 'DCust%'
   then
    
      v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' || REPLACE(v_columnName,'DCust','');
      v_StrSql := coalesce(v_StrSql,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=(CASE WHEN isnumeric(CFW.Fld_Value) THEN CFW.Fld_Value ELSE ''0'' END)::NUMERIC ';
      v_columnName := 'LstCF.vcData';
   ELSEIF v_columnName = 'Opp.vcLastSalesOrderDate'
   then
	
      v_columnName := 'TempLastOrder.bintCreatedDate';
   ELSEIF v_columnName = 'DMSC.vcSignatureType'
   then
	
      v_columnName := '(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END)';
   end if;
	
   IF LOWER(v_columnName) = LOWER('CMP.vcPerformance') then
	
      v_columnName := 'TempPerformance.monDealAmount';
   end if;

   v_StrSql := coalesce(v_StrSql,'') || ' WHERE  (COALESCE(ADC.bitPrimaryContact,false)=true   OR ADC.numContactID IS NULL)
							AND CMP.numDomainID=DM.numDomainID     
							AND (CMP.numCompanyType=46 OR (CMP.numCompanyType=47 AND EXISTS (SELECT numOppID FROM OpportunityMaster OM WHERE OM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 1 AND OM.tintOppStatus = 1)))                                    
							AND DM.tintCRMType= ' || SUBSTR(CAST(v_CRMType AS VARCHAR(2)),1,2) || '                                            
							AND DM.numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);

   v_StrSql := coalesce(v_StrSql,'') || ' AND DM.bitActiveInActive=' || SUBSTR(CAST(v_bitActiveInActive AS VARCHAR(15)),1,15);
	
   IF LENGTH(v_SearchQuery) > 0 then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
   end if;	

   IF v_bitPartner = true then
	
      v_StrSql := coalesce(v_StrSql,'') || 'and (DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numCreatedBy=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
   end if;
	                                                         
   IF v_SortChar <> '0' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' And CMP.vcCompanyName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;

   IF v_tintUserRightType = 1 then
	
      v_StrSql := coalesce(v_StrSql,'') || CONCAT(' AND (DM.numRecOwner = ',v_numUserCntID,' OR DM.numAssignedTo=',v_numUserCntID,
      ' OR ',v_strShareRedordWith,' OR ',v_strExternalUser,')');
   ELSEIF v_tintUserRightType = 2
   then
	
      v_StrSql := coalesce(v_StrSql,'') || CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',v_numUserCntID,' ) or DM.numAssignedTo=',v_numUserCntID,
      ' or ',v_strShareRedordWith,')');
   end if;
	               
   IF v_numProfile <> 0 then
	
      v_StrSql := coalesce(v_StrSql,'') || ' and cmp.vcProfile = ' || SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
   end if;
	                                                      
   IF v_tintSortOrder = 1 then
      v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=2 ';
   ELSEIF v_tintSortOrder = 2
   then
      v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=3 ';
   ELSEIF v_tintSortOrder = 3
   then
      v_StrSql := coalesce(v_StrSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
   ELSEIF v_tintSortOrder = 5
   then
      v_StrSql := coalesce(v_StrSql,'') || ' order by numCompanyRating desc ';
   ELSEIF v_tintSortOrder = 6
   then
      v_StrSql := coalesce(v_StrSql,'') || ' AND DM.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 7
   then
      v_StrSql := coalesce(v_StrSql,'') || ' and DM.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 8
   then
      v_StrSql := coalesce(v_StrSql,'') || ' and DM.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                  
                                                   
        
   IF v_vcRegularSearchCriteria <> '' then
	
      IF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance') IN v_vcRegularSearchCriteria),0) > 0 then
		
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
         v_StrSql := v_StrSql;
      ELSE
         v_StrSql := coalesce(v_StrSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
      end if;
   end if;


   IF v_vcCustomSearchCriteria <> '' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;  

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                  


   RAISE NOTICE '%',v_strFinal;

	-- EXECUTE FINAL QUERY
	drop table IF EXISTS tt_TempAccountList1 CASCADE;
   v_strFinal := CONCAT('CREATE TEMPORARY TABLE tt_TempAccountList1 AS  SELECT COUNT(*) OVER() AS TotalRows,',v_strColumns,v_StrSql,' ORDER BY ',v_columnName,' ',v_columnSortOrder,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint,' ROWS FETCH NEXT ',v_PageSize,' ROWS ONLY;');
   EXECUTE v_strFinal;

   open SWV_RefCur for EXECUTE CONCAT('SELECT * FROM tt_TempAccountList1');
   EXECUTE 'SELECT COALESCE((SELECT TotalRows FROM tt_TempAccountList1 LIMIT 1),0);' INTO v_TotRecs;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;
	
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/



