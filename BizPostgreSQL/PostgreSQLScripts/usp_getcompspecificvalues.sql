-- Stored procedure definition script USP_GetCompSpecificValues for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCompSpecificValues(v_numCommID NUMERIC(9,0) DEFAULT 0,        
v_numOppID NUMERIC(9,0) DEFAULT 0,        
v_numProID NUMERIC(9,0) DEFAULT 0,        
v_numCasesID NUMERIC(9,0) DEFAULT 0,        
INOUT v_numDivID NUMERIC(9,0) DEFAULT 0 ,        
INOUT v_numCompID NUMERIC(9,0) DEFAULT 0 ,        
INOUT v_numContID NUMERIC(9,0) DEFAULT 0 ,        
INOUT v_tintCRMType NUMERIC(9,0) DEFAULT 0 ,        
INOUT v_numTerID NUMERIC(9,0) DEFAULT 0 ,        
INOUT v_vcCompanyName VARCHAR(100) DEFAULT '' ,        
INOUT v_vcContactName VARCHAR(100) DEFAULT '' ,        
INOUT v_vcEmail VARCHAR(100) DEFAULT '' ,        
INOUT v_vcPhone VARCHAR(20) DEFAULT '' ,
v_charModule CHAR(1) DEFAULT NULL,        
INOUT v_vcNoOfEmployeesId VARCHAR(30) DEFAULT '' ,
INOUT v_numCurrencyID NUMERIC(9,0) DEFAULT 0 ,
INOUT v_bitOnCreditHold BOOLEAN DEFAULT false ,
INOUT v_numBillingDays BIGINT DEFAULT 0 ,
INOUT v_numDefaultExpenseAccountID NUMERIC(18,0) DEFAULT 0 ,
INOUT v_numAccountClassID NUMERIC(18,0) DEFAULT 0 ,
INOUT v_numRecordOwner NUMERIC(18,0) DEFAULT 0 ,
INOUT v_vcPhoneExt VARCHAR(500) DEFAULT '' ,
INOUT v_vcCustomerRelation VARCHAR(500) DEFAULT '')
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_charModule = 'A' then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,''), coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), coalesce(numPhoneExtension,''), (SELECT  coalesce(vcData,'') FROM Listdetails WHERE numListID = 5 AND numListItemID = Com.numCompanyType LIMIT 1), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcPhoneExt,v_vcCustomerRelation,
      v_vcNoOfEmployeesId,v_numCurrencyID,v_bitOnCreditHold,v_numBillingDays,
      v_numDefaultExpenseAccountID from Communication C
      join AdditionalContactsInformation ADC
      on ADC.numContactId = C.numContactId
      join DivisionMaster D
      on D.numDivisionID = ADC.numDivisionId
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID where numCommId = v_numCommID;
   ELSEIF v_charModule = 'O'
   then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,'-'), coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcNoOfEmployeesId,v_numCurrencyID,
      v_bitOnCreditHold,v_numBillingDays,v_numDefaultExpenseAccountID from OpportunityMaster O
      join AdditionalContactsInformation ADC
      on ADC.numContactId = O.numContactId
      join DivisionMaster D
      on D.numDivisionID = ADC.numDivisionId
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID where numOppId = v_numOppID;
   ELSEIF v_charModule = 'P'
   then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,'-'), coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcNoOfEmployeesId,v_numCurrencyID,
      v_bitOnCreditHold,v_numBillingDays,v_numDefaultExpenseAccountID from ProjectsMaster P
      left join AdditionalContactsInformation ADC
      on P.numCustPrjMgr = ADC.numContactId
      join DivisionMaster D
      on D.numDivisionID = P.numDivisionId
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID where numProID = v_numProID;
   ELSEIF v_charModule = 'S'
   then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,'-'), coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcNoOfEmployeesId,v_numCurrencyID,
      v_bitOnCreditHold,v_numBillingDays,v_numDefaultExpenseAccountID from Cases C
      join AdditionalContactsInformation ADC
      on ADC.numContactId = C.numContactId
      join DivisionMaster D
      on D.numDivisionID = ADC.numDivisionId
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID where numCaseID = v_numCasesID;
   ELSEIF v_charModule = 'D'
   then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,'-'), coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0), numAccountClassID, coalesce(D.numRecOwner,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcNoOfEmployeesId,v_numCurrencyID,
      v_bitOnCreditHold,v_numBillingDays,v_numDefaultExpenseAccountID,
      v_numAccountClassID,v_numRecordOwner from DivisionMaster D
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID
      left join AdditionalContactsInformation ADC
      on ADC.numDivisionId = D.numDivisionID
      AND coalesce(ADC.bitPrimaryContact,false) = true where D.numDivisionID = v_numDivID;
   end if;       
   if v_charModule = 'C' then

      select   coalesce(D.numDivisionID,0), coalesce(D.numCompanyID,0), coalesce(ADC.numContactId,0), coalesce(tintCRMType,0), coalesce(numTerID,0), coalesce(vcCompanyName,'-'), coalesce(vcFirstName,'-') || ' ' || coalesce(vcLastname,''), coalesce(vcEmail,''), coalesce(numPhone,''), GetListIemName(Com.numNoOfEmployeesId), coalesce(D.numCurrencyID,0), coalesce(D.bitOnCreditHold,false), coalesce(D.numBillingDays,0), coalesce(numDefaultExpenseAccountID,0) INTO v_numDivID,v_numCompID,v_numContID,v_tintCRMType,v_numTerID,v_vcCompanyName,
      v_vcContactName,v_vcEmail,v_vcPhone,v_vcNoOfEmployeesId,v_numCurrencyID,
      v_bitOnCreditHold,v_numBillingDays,v_numDefaultExpenseAccountID from DivisionMaster D
      join AdditionalContactsInformation ADC
      on ADC.numDivisionId = D.numDivisionID
      join CompanyInfo Com
      on Com.numCompanyId = D.numCompanyID where ADC.numContactId = v_numContID;
   end if;
   RETURN;
END; $$;


