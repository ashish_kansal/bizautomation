-- Function definition script fn_GetItemGroupsName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemGroupsName(v_numItemGroupID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemGroup  VARCHAR(100);
BEGIN
   select   coalesce(vcItemGroup,'') INTO v_vcItemGroup from ItemGroups where numItemGroupID = v_numItemGroupID;

   return coalesce(v_vcItemGroup,'-');
END; $$;

