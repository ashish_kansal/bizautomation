-- Stored procedure definition script USP_OppDeleteItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppDeleteItem(v_OppItemCode NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppId  NUMERIC(9,0);
   v_TotalAmount  DECIMAL(20,5);
BEGIN
   select   numOppId INTO v_OppId from OpportunityItems where numoppitemtCode = v_OppItemCode;
   delete from OpportunityItems where numoppitemtCode = v_OppItemCode; 

   select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_OppId;
   update OpportunityMaster set  monPAmount = v_TotalAmount
   where numOppId = v_OppId;
   RETURN;
END; $$;


