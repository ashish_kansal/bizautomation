CREATE OR REPLACE FUNCTION Reminder_UpdByRecurrence
(
	v_LastReminderDateTimeUtc TIMESTAMP,
	v_RecurrenceID INTEGER
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		Recurrence
	SET
		LastReminderDateTimeUtc = v_LastReminderDateTimeUtc
	WHERE
		Recurrence.recurrenceid = v_RecurrenceID;
   
	RETURN;
END; $$;


