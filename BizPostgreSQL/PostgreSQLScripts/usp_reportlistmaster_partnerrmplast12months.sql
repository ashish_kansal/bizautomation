CREATE OR REPLACE FUNCTION USP_ReportListMaster_PartnerRMPLast12Months(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_ProfitCost  INTEGER;
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID, numCost INTO v_numShippingItemID,v_numDiscountItemID,v_ProfitCost FROM Domain WHERE numDomainId = v_numDomainID;

   open SWV_RefCur for
   SELECT
   CI.vcCompanyName
		,SUM(OM.monDealAmount) AS monDealAmount
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numPartner = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   OM.numDomainId = v_numDomainID
   AND coalesce(OM.tintopptype,0) = 1
   AND coalesce(OM.tintoppstatus,0) = 1
   AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
   GROUP BY
   CI.vcCompanyName
   HAVING
   SUM(monDealAmount) > 0
   ORDER BY
   SUM(monDealAmount) DESC;
		
   open SWV_RefCur2 for
   SELECT
   vcCompanyName
		,(SUM(Profit)/SUM(monTotAmount))*100 AS BlendedProfit
   FROM(SELECT
      CI.vcCompanyName
			,coalesce(monTotAmount,0) AS monTotAmount
			,coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numPartner = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP2
   GROUP BY
   vcCompanyName
   HAVING(SUM(Profit)/SUM(monTotAmount))*100 > 0
   ORDER BY(SUM(Profit)/SUM(monTotAmount))*100 DESC;

   open SWV_RefCur3 for
   SELECT * FROM(SELECT
      CI.vcCompanyName
			,SUM(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT)) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numPartner = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)
      GROUP BY
      CI.vcCompanyName) TEMP2
   ORDER BY
   Profit DESC;
   RETURN;
END; $$;


