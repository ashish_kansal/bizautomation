CREATE OR REPLACE FUNCTION USP_GetItemOrderDependencies(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
	v_numItemCode NUMERIC(9,0) DEFAULT 0,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_tintOppType SMALLINT DEFAULT 0,
	v_byteMode SMALLINT DEFAULT 0,
	v_numWareHouseId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemName  VARCHAR(100);
   v_OnOrder  NUMERIC(18,0);
   v_vcShippedReceivedStatus  VARCHAR(100) DEFAULT '';
   v_strSQL  TEXT DEFAULT '';
   v_strWHERE  TEXT;
BEGIN
   IF v_byteMode = 0 then
      drop table IF EXISTS tt_TEMP CASCADE;
      Create TEMPORARY TABLE tt_TEMP 
      (
         numOppID NUMERIC(18,0), 
         vcPOppName VARCHAR(100), 
         tintOppType SMALLINT, 
         bintCreatedDate TIMESTAMP, 
         bintAccountClosingDate TIMESTAMP, 
         OppType VARCHAR(20),
         OppStatus VARCHAR(20), 
         vcCompanyName VARCHAR(200), 
         vcItemName VARCHAR(200), 
         numReturnHeaderID NUMERIC(18,0), 
         vcWareHouse VARCHAR(100),
         numQtyOrdered DOUBLE PRECISION, 
         numQtyReleasedReceived DOUBLE PRECISION, 
         numQtyOnAllocationOnOrder DOUBLE PRECISION
      );

	-- NOT Sales and Purchase Return
      IF v_tintOppType <> 5 AND v_tintOppType <> 6 AND coalesce(v_tintOppType,0) <> 11 then
	
		--Regular Item
         INSERT INTO
         tt_TEMP
         SELECT
         OM.numOppId
			,OM.vcpOppName
			,OM.tintopptype
			,OM.bintCreatedDate
			,OM.bintAccountClosingDate
			,CAST(CASE
         WHEN OM.tintopptype = 1
         THEN CASE WHEN OM.tintoppstatus = 0 THEN 'Sales Opp' ELSE 'Sales Order' END
         ELSE CASE WHEN OM.tintoppstatus = 0 THEN 'Purchase Opp' ELSE 'Purchase Order' END
         END AS VARCHAR(20)) as OppType
			,CAST(CASE coalesce(OM.tintshipped,0) WHEN 0 THEN 'Open' WHEN 1 THEN 'Closed' END AS VARCHAR(20)) AS OppStatus
			,CAST(CI.vcCompanyName || Case when coalesce(DM.numCompanyDiff,0) > 0 then  '  ' || fn_GetListItemName(DM.numCompanyDiff) || ':' || coalesce(DM.vcCompanyDiff,'') else '' end AS VARCHAR(200)) as vcCompanyname
			,CAST('' AS VARCHAR(200))
			,0
			,W.vcWareHouse
			,coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),Opp.numItemCode,I.numDomainID,coalesce(Opp.numUOMId,0)),1)*numUnitHour AS numQtyOrdered
			,coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),Opp.numItemCode,I.numDomainID,coalesce(Opp.numUOMId,0)),1)*(CASE WHEN OM.tintopptype = 1 THEN coalesce(numQtyShipped,0) ELSE coalesce(numUnitHourReceived,0) END) AS numQtyReleasedReceived
			,(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),Opp.numItemCode,I.numDomainID,coalesce(Opp.numUOMId,0)),1)*numUnitHour) -(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),Opp.numItemCode,I.numDomainID,coalesce(Opp.numUOMId,0)),1)*(CASE WHEN OM.tintopptype = 1 THEN coalesce(numQtyShipped,0) ELSE coalesce(numUnitHourReceived,0) END)) As numQtyOnAllocationOnOrder
         FROM
         OpportunityItems Opp
         INNER JOIN OpportunityMaster OM ON Opp.numOppId = OM.numOppId
         INNER JOIN Item I ON Opp.numItemCode = I.numItemCode
         INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
         LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
         LEFT JOIN WareHouseItems WHI ON WHI.numWareHouseItemID = Opp.numWarehouseItmsID
         LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
         WHERE
         OM.numDomainId = v_numDomainID
         AND I.numDomainID = v_numDomainID
         AND I.numItemCode = v_numItemCode
         AND 1 =(CASE WHEN v_numWareHouseId > 0 THEN
            Case WHEN W.numWareHouseID = v_numWareHouseId then 1 else 0 end
         Else 1 end)
         AND 1 =(CASE
         WHEN coalesce(v_tintOppType,0) = 1 -- Sales Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 1 AND coalesce(OM.tintoppstatus,0) = 1 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 2 -- Purchase Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 2 AND coalesce(OM.tintoppstatus,0) = 1 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 3 -- Sales Opportunity
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 1 AND coalesce(OM.tintoppstatus,0) = 0 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 4 -- Purchase Opportunity
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 2 AND coalesce(OM.tintoppstatus,0) = 0 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 7 -- Open Sales Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 1 AND coalesce(OM.tintoppstatus,0) = 1 AND coalesce(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 8 -- Closed Sales Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 1 AND coalesce(OM.tintoppstatus,0) = 1 AND coalesce(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 9 -- Open Purchase Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 2 AND coalesce(OM.tintoppstatus,0) = 1 AND coalesce(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
         WHEN coalesce(v_tintOppType,0) = 10 -- Closed Purchase Order
         THEN CASE WHEN coalesce(OM.tintopptype,0) = 2 AND coalesce(OM.tintoppstatus,0) = 1 AND coalesce(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
         ELSE 1
         END)
         ORDER BY
         OM.bintCreatedDate desc;
      end if;
      v_strWHERE := ' WHERE 1=1 ';
      IF v_tintOppType = 5 then
	
         v_strWHERE := coalesce(v_strWHERE,'') || ' AND RH.tintReturnType = 1 ';
      ELSEIF v_tintOppType = 6
      then
	
         v_strWHERE := coalesce(v_strWHERE,'') || ' AND RH.tintReturnType = 2 ';
      ELSEIF v_tintOppType = 0
      then
	
         v_strWHERE := coalesce(v_strWHERE,'') || ' AND RH.tintReturnType IN (1,2) ';
      end if;


	-- NOT Sales and Purchase Return
      IF v_tintOppType <> 5 AND v_tintOppType <> 6 AND coalesce(v_tintOppType,0) <> 11 then
	
         v_strSQL := CONCAT('INSERT INTO 
								tt_TEMP
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE COALESCE(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName || Case when COALESCE(DM.numCompanyDiff,0)>0 then  ''  '' || fn_getlistitemname(DM.numCompanyDiff) || '':'' || COALESCE(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName || Case when IParent.bitAssembly=true then '' (Assembly Member)'' When IParent.bitKitParent=true then '' (Kit Member)'' else '''' end
								,0 numReturnHeaderID
								,W.vcWareHouse
								,COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN COALESCE(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN COALESCE(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numChildItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								COALESCE(I.bitAssembly,false)=false 
								AND OKI.numChildItemID=',v_numItemCode,'  
								AND om.numDomainId = ',v_numDomainID,
         ' 
								AND 1 = (CASE WHEN ',v_numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',
         v_numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',v_numDomainID,'  
								AND 1=(CASE 
										WHEN ',
         coalesce(v_tintOppType,0),' = 1
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),
         ' = 2
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 3
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 4
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 7
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 8
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',coalesce(v_tintOppType,0),
         ' = 9
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',coalesce(v_tintOppType,0),' = 10
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)');
         v_strSQL := CONCAT(v_strSQL,'; INSERT INTO 
								tt_TEMP
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE COALESCE(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName || Case when COALESCE(DM.numCompanyDiff,0)>0 then  ''  '' || fn_getlistitemname(DM.numCompanyDiff) || '':'' || COALESCE(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName || Case when IParent.bitAssembly=true then '' (Assembly Member)'' When IParent.bitKitParent=true then '' (Kit Member)'' else '''' end
								,0 numReturnHeaderID
								,W.vcWareHouse
								,COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN COALESCE(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (COALESCE(fn_UOMConversion(COALESCE(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, COALESCE(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN COALESCE(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitChildItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								COALESCE(I.bitAssembly,false)=false 
								AND OKI.numItemID=',v_numItemCode,'  
								AND om.numDomainId = ',v_numDomainID,
         ' 
								AND 1 = (CASE WHEN ',v_numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',
         v_numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',v_numDomainID,
         '  
								AND 1=(CASE 
										WHEN ',coalesce(v_tintOppType,0),
         ' = 1
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',coalesce(v_tintOppType,0),
         ' = 2
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 3
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 4
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 7
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',
         coalesce(v_tintOppType,0),' = 8
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 1 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',coalesce(v_tintOppType,0),
         ' = 9
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',coalesce(v_tintOppType,0),' = 10
										THEN CASE WHEN COALESCE(OM.tintOppType,0) = 2 AND COALESCE(OM.tintOppStatus,0)=1 AND COALESCE(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)');
      end if;
      IF v_tintOppType = 5 AND v_tintOppType = 6 OR v_tintOppType = 0 then
	
         v_strSQL := coalesce(v_strSQL,'') || CONCAT('; INSERT INTO tt_TEMP
										SELECT 
											RH.numReturnHeaderID numoppid
											,RH.vcRMA vcPoppName
											,RH.tintReturnType tintOppType
											,RH.dtCreatedDate bintCreatedDate
											,NULL bintAccountClosingDate
											,CASE RH.tintReturnType WHEN 1 THEN ''Sales Return'' WHEN 2 THEN ''Purchase Return'' ELSE '''' END OppType
											,'''' OppStatus
											,CI.vcCompanyName || CASE WHEN COALESCE(DM.numCompanyDiff,0) > 0 THEN ''  '' || fn_getlistitemname(DM.numCompanyDiff) || '':'' || COALESCE(DM.vcCompanyDiff,'''') ELSE '''' END AS vcCompanyname
											,I.vcItemName || CASE WHEN I.bitAssembly = true THEN '' (Assembly)'' WHEN I.bitKitParent = true THEN '' (Kit)'' ELSE '''' END
											,RH.numReturnHeaderID
											,W.vcWareHouse
											,0
											,0
											,0
										FROM ReturnHeader AS RH 
										INNER JOIN ReturnItems AS RI  ON RH.numReturnHeaderID = RI.numReturnHeaderID AND RH.tintReturnType IN (1,2)
										INNER JOIN Item AS I ON RI.numItemCode = I.numItemCode AND I.numDomainID = RH.numDomainId
										INNER JOIN divisionMaster DM ON RH.numDivisionID = DM.numDivisionID
										LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID 
										LEFT JOIN WareHouseItems WHI ON WHI.numWareHouseItemID = RI.numWareHouseItemID  
										LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
										',v_strWHERE,
         ' AND RH.numDomainId = ',v_numDomainID,' AND RI.numItemCode = ',
         v_numItemCode,' ORDER BY bintCreatedDate desc');
      end if;

	-- Stock Transfer
      IF coalesce(v_tintOppType,0) = 0 OR coalesce(v_tintOppType,0) = 11 then
	
         INSERT INTO
         tt_TEMP
         SELECT
         MST.ID
			,CONCAT('Stock Transfer (To Warehouse:',CONCAT(WTo.vcWareHouse,CASE WHEN LENGTH(coalesce(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END),
         ')')
			,0
			,MST.dtTransferredDate
			,NULL
			,CAST('Stock Transfer' AS VARCHAR(20)) as OppType
			,CAST('Closed' AS VARCHAR(20)) AS OppStatus
			,CAST('' AS VARCHAR(200)) as vcCompanyname
			,CAST('' AS VARCHAR(200))
			,0
			,CONCAT(WFrom.vcWareHouse,CASE WHEN LENGTH(coalesce(WLFrom.vcLocation,'')) > 0 THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END)
			,MST.numQty AS numQtyOrdered
			,MST.numQty AS numQtyReleasedReceived
			,0 As numQtyOnAllocationOnOrder
         FROM
         MassStockTransfer MST
         INNER JOIN
         Item I
         ON
         MST.numItemCode = I.numItemCode
         INNER JOIN
         WareHouseItems WHIFrom
         ON
         WHIFrom.numWareHouseItemID = MST.numFromWarehouseItemID
         INNER JOIN
         Warehouses WFrom
         ON
         WFrom.numWareHouseID = WHIFrom.numWareHouseID
         LEFT JOIN
         WarehouseLocation WLFrom
         ON
         WHIFrom.numWLocationID = WLFrom.numWLocationID
         INNER JOIN
         WareHouseItems WHITo
         ON
         WHITo.numWareHouseItemID = MST.numToWarehouseItemID
         INNER JOIN
         Warehouses WTo
         ON
         WTo.numWareHouseID = WHITo.numWareHouseID
         LEFT JOIN
         WarehouseLocation WLTo
         ON
         WHITo.numWLocationID = WLTo.numWLocationID
         WHERE
         MST.numDomainId = v_numDomainID
         AND MST.numItemCode = v_numItemCode
         AND I.numDomainID = v_numDomainID;
      end if;
      RAISE NOTICE '%',CAST(v_strSQL AS TEXT);
      EXECUTE v_strSQL;
      open SWV_RefCur for
      select numOppID,vcPOppName,tintOppType,
case when CAST(bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
      when CAST(bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
      when CAST(bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
      else FormatedDateFromDate(bintCreatedDate::TIMESTAMP +  make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) end  AS CreatedDate,
case when CAST(bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
      when CAST(bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
      when CAST(bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
      else FormatedDateFromDate(bintAccountClosingDate::TIMESTAMP +  make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) end  AS AccountClosingDate,
 OppType,OppStatus,vcCompanyName,vcItemName,numReturnHeaderID, vcWareHouse, coalesce(numQtyOrdered,0) AS numOnOrder, coalesce(numQtyOnAllocationOnOrder,0) AS numAllocation,coalesce(numQtyReleasedReceived,0) AS numQtyReleasedReceived  from tt_TEMP
      ORDER by bintCreatedDate desc;

   ELSEIF v_byteMode = 1
   then --Parent Items of selected item

      open SWV_RefCur for
      with recursive CTE(numItemCode,vcItemName,bitAssembly,bitKitParent)
      AS(select numItemKitID AS numItemCode,vcItemName AS vcItemName,bitAssembly AS bitAssembly,bitKitParent AS bitKitParent
      from Item
      INNER join ItemDetails Dtl on numItemKitID = numItemCode
      where  numChildItemID = v_numItemCode
      UNION ALL
      select Dtl.numItemKitID AS numItemCode ,i.vcItemName AS vcItemName,i.bitAssembly AS bitAssembly,i.bitKitParent AS bitKitParent
      from Item i
      INNER JOIN ItemDetails Dtl on Dtl.numItemKitID = i.numItemCode
      INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
      where Dtl.numChildItemID != v_numItemCode)
      select numItemCode,vcItemName,Case when bitAssembly = true then 'Assembly' When bitKitParent = true then 'Kit' end as ItemType
      from CTE;
   end if;
   RETURN;
END; $$;


