-- Stored procedure definition script USP_MapTransactionJournalUpdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Added By : Joseph ******/
/****** Updates Table General Journal Detail while Mapping Downloaded Transactions to existing Transactions. Set bitCleared to True if it is not Reconciled******/

CREATE OR REPLACE FUNCTION USP_MapTransactionJournalUpdate(v_bitDeposit BOOLEAN DEFAULT false,
	v_bitCheck BOOLEAN DEFAULT false,
	v_Id NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE General_Journal_Details SET bitCleared = true WHERE numTransactionId IN(SELECT GJD.numTransactionId FROM General_Journal_Details GJD INNER JOIN
   General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
   WHERE GJH.numDepositId = v_Id AND GJD.bitReconcile = false AND v_bitDeposit = true
   UNION
   SELECT GJD.numTransactionId  FROM General_Journal_Details GJD INNER JOIN
   General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
   WHERE GJH.numCheckHeaderID = v_Id AND GJD.bitReconcile = false AND v_bitCheck = true);
   RETURN;
END; $$;



