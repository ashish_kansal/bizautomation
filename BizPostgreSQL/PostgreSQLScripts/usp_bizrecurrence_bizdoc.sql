DROP FUNCTION IF EXISTS USP_BizRecurrence_BizDoc;
  
Create or replace FUNCTION USP_BizRecurrence_BizDoc(v_numRecConfigID NUMERIC(18,0), 
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0),
	INOUT v_numRecurBizDocID NUMERIC(18,0) ,
	INOUT v_bitAuthorative BOOLEAN ,
	v_bitBizDocRecur BOOLEAN,
	v_numFrequency SMALLINT,
	v_numRecurParentOppID NUMERIC(18,0),
	v_Date DATE)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocId  INTEGER;
   v_numAuthBizDocId  INTEGER;
   v_numDivisionID  NUMERIC(9,0);
   v_numDomainID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;	
   v_numNewOppBizDocID  NUMERIC(18,0);
   v_fltExchangeRateBizDoc  DOUBLE PRECISION;
   v_numSequenceId  NUMERIC(18,0);
	
   v_tintShipped  SMALLINT;	
   v_dtShipped  TIMESTAMP;
   v_bitAuthBizdoc  BOOLEAN;
        
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   v_dtNextRecurrenceDate  DATE DEFAULT NULL;
BEGIN
   -- BEGIN TRANSACTION
BEGIN
	--DECLARE @Date AS DATE = GETDATE()
      select   fltExchangeRate INTO v_fltExchangeRateBizDoc FROM OpportunityMaster where numOppId = v_numOppID;
      select   numDomainId, numDivisionId, tintopptype INTO v_numDomainID,v_numDivisionID,v_tintOppType FROM OpportunityMaster WHERE numOppId = v_numOppID;
      select   numBizDocId INTO v_numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocID;
      select   coalesce(MAX(CAST(coalesce(OBD.numSequenceId,cast(0 as TEXT)) AS BIGINT)),
      0)+1 INTO v_numSequenceId FROM
      OpportunityMaster OM
      JOIN
      OpportunityBizDocs OBD
      ON
      OM.numOppId = OBD.numoppid WHERE
      OM.numDomainId = v_numDomainID AND OBD.numBizDocId = v_numBizDocId;

	--GET Authorative Biz Doc ID
      select   CASE v_tintOppType
      WHEN 1 THEN numAuthoritativeSales
      WHEN 2 THEN numAuthoritativePurchase
      ELSE numAuthoritativePurchase
      END INTO v_numAuthBizDocId FROM
      AuthoritativeBizDocs WHERE
      numDomainId = v_numDomainID;
      select   coalesce(tintshipped,0), numDomainId INTO v_tintShipped,v_numDomainID FROM OpportunityMaster WHERE numOppId = v_numOppID;
      IF v_tintShipped = 1 then
         v_dtShipped := TIMEZONE('UTC',now());
      ELSE
         v_dtShipped := null;
      end if;
      IF v_tintOppType = 1 AND v_numBizDocId = v_numAuthBizDocId then
         v_bitAuthBizdoc := true;
      ELSEIF v_tintOppType = 2 AND v_numBizDocId = v_numAuthBizDocId
      then
         v_bitAuthBizdoc := true;
      ELSE
         v_bitAuthBizdoc := false;
      end if;


	-- CREATE COPY OF PARENT BIZDOC
      INSERT INTO OpportunityBizDocs(numoppid,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
		dtFromDate,numBizDocStatus,bitAuthoritativeBizDocs,numBizDocTempID,vcRefOrderNo,
		numSequenceId,numBizDocStatusOLD,bitAutoCreated,numMasterBizdocSequenceID,fltExchangeRateBizDoc)
      SELECT
      v_numOppID,v_numBizDocId,v_numUserCntID,v_Date,v_numUserCntID,v_Date,'',OBD.bitPartialFulfilment,
		DATETIMEFROMPARTS(EXTRACT(YEAR FROM v_Date),EXTRACT(MONTH FROM v_Date),EXTRACT(DAY FROM v_Date),
      23,59,59,0),0,CAST(v_bitAuthBizdoc AS INTEGER),numBizDocTempID,vcRefOrderNo,
		CAST(v_numSequenceId AS VARCHAR(50)),0,true,CAST(v_numSequenceId AS VARCHAR(50)),v_fltExchangeRateBizDoc
      FROM
      OpportunityBizDocs OBD
      WHERE
      OBD.numOppBizDocsId = v_numOppBizDocID;
      v_numNewOppBizDocID := CURRVAL('OpportunityBizDocs_seq');
      SELECT * INTO SWV_RCur,SWV_RCur2 FROM USP_OpportunityBizDocs_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numNewOppBizDocID,
      SWV_RefCur := 'SWV_RCur',SWV_RefCur2 := 'SWV_RCur2');

	-- UPDATE BIZ DOC NAME 
      PERFORM USP_UpdateBizDocNameTemplate(v_tintOppType::SMALLINT,v_numDomainID,v_numNewOppBizDocID);
      IF v_numBizDocId = 297 OR v_numBizDocId = 299 then
	
         IF NOT EXISTS(SELECT * FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2) then
		
            INSERT INTO NameTemplate(numDomainID,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
            SELECT
            v_numDomainID,2,v_numBizDocId,UPPER(SUBSTR(GetListIemName(v_numBizDocId::NUMERIC(9,0)),0,4)) || '-',1,4;
            v_numSequenceId := 1;
         ELSE
            select   numSequenceId INTO v_numSequenceId FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2;
            UPDATE NameTemplate SET numSequenceId = coalesce(numSequenceId,0)+1 WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2;
         end if;
      end if;
      UPDATE OpportunityBizDocs SET vcBizDocID = vcBizDocName || coalesce(SUBSTR(CAST(v_numSequenceId AS VARCHAR(10)),1,10),''),monDealAmount = coalesce(GetDealAmount(v_numOppID,v_Date::TIMESTAMP,v_numNewOppBizDocID),
      0) WHERE  numOppBizDocsId = v_numNewOppBizDocID;
      IF v_bitBizDocRecur = true then
	
		-- Insert newly created bizdoc to transaction 
         INSERT INTO RecurrenceTransaction(numRecConfigID,numRecurrOppBizDocID,dtCreatedDate)	VALUES(v_numRecConfigID,v_numNewOppBizDocID,LOCALTIMESTAMP);

		-- Insert newly created bizdoc to transaction histrory table. Records are not deleted in this table when opp or bizdoc is deleted 
		
         INSERT INTO RecurrenceTransactionHistory(numRecConfigID,numRecurrOppBizDocID,dtCreatedDate)	VALUES(v_numRecConfigID,v_numNewOppBizDocID,LOCALTIMESTAMP);

		--Increase value of number of transaction completed by 1
		
         UPDATE RecurrenceConfiguration SET numTransaction = coalesce(numTransaction,0)+1 WHERE numRecConfigID = v_numRecConfigID;
         INSERT INTO OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
         SELECT
         v_numNewOppBizDocID,
			OBDI.numOppItemID,
			OBDI.numItemCode,
			(CASE
         WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN TEMP.QtytoFulFill
         ELSE OBDI.numUnitHour
         END) AS numUnitHour,
			OBDI.monPrice,
			(CASE
         WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN((OI.monTotAmount/OI.numUnitHour)*TEMP.QtytoFulFill)
         ELSE OBDI.monTotAmount
         END) AS monTotAmount,
			OBDI.vcItemDesc,
			OBDI.numWarehouseItmsID,
			OBDI.vcType,
			OBDI.vcAttributes,
			OBDI.bitDropShip,
			OBDI.bitDiscountType,
			(CASE
         WHEN OBDI.numUnitHour > TEMP.QtytoFulFill
         THEN(CASE
            WHEN OBDI.bitDiscountType = false THEN OBDI.fltDiscount
            WHEN OBDI.bitDiscountType = true THEN((OI.fltDiscount/OI.numUnitHour)*TEMP.QtytoFulFill)
            END)
         ELSE OBDI.fltDiscount
         END) As fltDiscount,
			OBDI.monTotAmtBefDiscount,
			OBDI.vcNotes,
			OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,
			OBDI.dtRentalStartDate,
			OBDI.dtRentalReturnDate,
			OBDI.tintTrackingStatus
         FROM
         OpportunityBizDocs OBD
         JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         JOIN
         OpportunityItems OI
         ON
         OBDI.numOppItemID = OI.numoppitemtCode
         INNER JOIN(SELECT * FROM(SELECT
               OI.numoppitemtCode,
					OI.numUnitHour AS QtyOrdered,
					(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill
               FROM
               OpportunityItems OI
               LEFT JOIN
               OpportunityMaster OM
               ON
               OI.numOppId = OM.numOppId
               LEFT JOIN
               OpportunityBizDocItems OBI
               ON
               OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocID IN(SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numoppid = OI.numOppId AND coalesce(OB.bitAuthoritativeBizDocs,0) = 1)
               LEFT JOIN
               WareHouseItems WI
               ON
               WI.numWareHouseItemID = OI.numWarehouseItmsID
               INNER JOIN
               Item I
               ON
               I.numItemCode = OI.numItemCode
               WHERE
               OI.numOppId = v_numOppID
               GROUP BY
               OI.numoppitemtCode,OI.numUnitHour) X
            WHERE
            X.QtytoFulFill > 0) AS TEMP
         ON
         OBDI.numOppItemID = TEMP.numoppitemtCode
         WHERE
         OBDI.numOppBizDocID = v_numOppBizDocID;	
	
		--Set isCompleted to true if total quantity of all items is used in recurring bizdoc
         IF(SELECT
         COUNT(*)
         FROM
         OpportunityBizDocItems OBDI
         INNER JOIN(SELECT * FROM(SELECT
               OI.numoppitemtCode,
							OI.numUnitHour AS QtyOrdered,
							(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill
               FROM
               OpportunityItems OI
               LEFT JOIN
               OpportunityMaster OM
               ON
               OI.numOppId = OM.numOppId
               LEFT JOIN
               OpportunityBizDocItems OBI
               ON
               OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocID IN(SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numoppid = OI.numOppId AND coalesce(OB.bitAuthoritativeBizDocs,0) = 1)
               LEFT JOIN
               WareHouseItems WI
               ON
               WI.numWareHouseItemID = OI.numWarehouseItmsID
               INNER JOIN
               Item I
               ON
               I.numItemCode = OI.numItemCode
               WHERE
               OI.numOppId = v_numOppID
               GROUP BY
               OI.numoppitemtCode,OI.numUnitHour) X
            WHERE
            X.QtytoFulFill > 0) AS TEMP
         ON
         OBDI.numOppItemID = TEMP.numoppitemtCode
         WHERE
         OBDI.numOppBizDocID = v_numOppBizDocID) > 0 then
			
				--Get next recurrence date for bizdoc 
            v_dtNextRecurrenceDate :=(CASE v_numFrequency
            WHEN 1 THEN v_Date+INTERVAL '1 day' --Daily
            WHEN 2 THEN v_Date+INTERVAL '7 day' --Weekly
            WHEN 3 THEN v_Date+INTERVAL '1 month' --Monthly
            WHEN 4 THEN v_Date+INTERVAL '4 month'  --Quarterly
            END);
            UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = v_dtNextRecurrenceDate WHERE numRecConfigID = v_numRecConfigID;
         ELSE
            UPDATE RecurrenceConfiguration SET bitCompleted = true WHERE numRecConfigID = v_numRecConfigID;
         end if;
         UPDATE OpportunityBizDocs SET bitRecurred = true WHERE numOppBizDocsId = v_numNewOppBizDocID;
      ELSE
		-- INSERTS BizDoc Items
         INSERT INTO OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
         SELECT
         v_numNewOppBizDocID,OINew.numoppitemtCode,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,
			OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
         FROM
         OpportunityBizDocs OBD
         JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         JOIN
         OpportunityItems OI
         ON
         OBDI.numOppItemID = OI.numoppitemtCode
         JOIN
         OpportunityItems OINew
         ON
         OINew.numRecurParentOppItemID = OI.numoppitemtCode
         JOIN
         Item I
         ON
         OBDI.numItemCode = I.numItemCode
         WHERE
         OBD.numoppid = v_numRecurParentOppID
         AND OBD.numOppBizDocsId = v_numOppBizDocID;
      end if;
      IF coalesce((SELECT numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numNewOppBizDocID),0) = 55206 then
	
         UPDATE
         OpportunityItems OI
         SET
         numQtyPicked = coalesce((SELECT
         SUM(OBDI.numUnitHour)
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         AND OI.numoppitemtCode = OBDI.numOppItemID
         WHERE
         OBD.numoppid = OI.numOppId
         AND OBD.numBizDocId = 55206),0)
		
         WHERE
         OI.numOppId = v_numOppID;
      end if;
      v_numRecurBizDocID := v_numNewOppBizDocID;
      v_bitAuthorative := v_bitAuthBizdoc;
      EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END;

   -- COMMIT
RETURN;
END; $$;  
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/



