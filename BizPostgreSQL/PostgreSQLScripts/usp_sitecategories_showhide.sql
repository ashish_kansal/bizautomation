-- Stored procedure definition script USP_SiteCategories_ShowHide for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SiteCategories_ShowHide(v_numDomainID NUMERIC(18,0)
	,v_numCategoryProfileID NUMERIC(18,0)               
	,v_numCategoryID NUMERIC(18,0)                     
	,v_bitShow BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TABLESITES CASCADE;
   CREATE TEMPORARY TABLE tt_TABLESITES
   (
      numSiteID NUMERIC(18,0)
   );
   INSERT INTO tt_TABLESITES(numSiteID)
   SELECT
   numSiteID
   FROM
   CategoryProfileSites
   WHERE
   numCategoryProfileID = v_numCategoryProfileID;

   DROP TABLE IF EXISTS tt_TABLECATEGORIES CASCADE;
   CREATE TEMPORARY TABLE tt_TABLECATEGORIES
   (
      numCategoryID NUMERIC(18,0)
   );

   INSERT INTO tt_TABLECATEGORIES(numCategoryID) with recursive CTE(numCategoryID) AS(SELECT
   numCategoryID AS numCategoryID
   FROM
   Category
   WHERE
   numDomainID = v_numDomainID
   AND numCategoryProfileID = v_numCategoryProfileID
   AND numCategoryID = v_numCategoryID
   UNION ALL
   SELECT
   C1.numCategoryID AS numCategoryID
   FROM
   Category C1
   INNER JOIN
   CTE C
   ON
   C1.numDepCategory = C.numCategoryID
   WHERE
   C1.numDomainID = v_numDomainID
   AND C1.numCategoryProfileID = v_numCategoryProfileID) SELECT
   numCategoryID
   FROM
   CTE;

   DROP TABLE IF EXISTS tt_TABLESITECATEGORIES CASCADE;
   CREATE TEMPORARY TABLE tt_TABLESITECATEGORIES
   (
      numSiteID NUMERIC(18,0),
      numCategoryID NUMERIC(18,0)
   );

   INSERT INTO tt_TABLESITECATEGORIES(numSiteID
		,numCategoryID)
   SELECT
   numSiteID
		,numCategoryID
   FROM
   tt_TABLESITES,tt_TABLECATEGORIES;

   IF coalesce(v_bitShow,false) = false then
	
      DELETE FROM SiteCategories SC using tt_TABLESITECATEGORIES TSC where SC.numSiteID = TSC.numSiteID
      AND SC.numCategoryID = TSC.numCategoryID;
      UPDATE Category SET bitVisible = false WHERE numDomainID = v_numDomainID AND numCategoryID IN(SELECT numCategoryID FROM tt_TABLECATEGORIES);
   ELSE
      INSERT INTO SiteCategories(numSiteID
			,numCategoryID)
      SELECT
      TSC.numSiteID
			,TSC.numCategoryID
      FROM
      tt_TABLESITECATEGORIES TSC
      LEFT JOIN
      SiteCategories SC
      ON
      TSC.numSiteID = SC.numSiteID
      AND TSC.numCategoryID = SC.numCategoryID
      WHERE
      SC.numSiteID IS NULL;
      UPDATE Category SET bitVisible = true WHERE numDomainID = v_numDomainID AND numCategoryID IN(SELECT numCategoryID FROM tt_TABLECATEGORIES);
   end if;
   RETURN;
END; $$;


