-- Stored procedure definition script USP_ActionItemlist for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ActionItemlist(v_numUserCntID NUMERIC(9,0) DEFAULT null,                                          
v_numDomainID NUMERIC(9,0) DEFAULT null,                                          
v_dtStartDate TIMESTAMP DEFAULT NULL,                                                
v_dtEndDate TIMESTAMP DEFAULT NULL,                                           
v_RepType SMALLINT DEFAULT NULL,                                          
v_Condition CHAR(1) DEFAULT NULL,                                          
v_bitTask NUMERIC(9,0) DEFAULT NULL,                                          
v_CurrentPage INTEGER DEFAULT NULL,                                            
v_PageSize INTEGER DEFAULT NULL,                                            
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                            
v_columnName VARCHAR(50) DEFAULT NULL,                                            
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                        
v_strUserIDs VARCHAR(1000) DEFAULT NULL,                
v_TeamType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
   v_firstRec  INTEGER;                                             
   v_lastRec  INTEGER;
BEGIN
   if v_strUserIDs = '' then 
      v_strUserIDs := '0';
   end if;                                           
                                            
   if v_RepType <> 3 then
      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      drop table IF EXISTS tt_TEMPTABLE CASCADE;
      create TEMPORARY TABLE tt_TEMPTABLE 
      ( 
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         closeDate VARCHAR(20),
         itemDesc VARCHAR(1000),
         Phone VARCHAR(100),
         Name VARCHAR(200),
         vcCompanyName VARCHAR(110),
         vcEmail VARCHAR(50),
         Task VARCHAR(20)
      );
      v_strSql := 'SELECT                                           
 Comm.dtStartTime AS closeDate,                                           
 CAST(Comm.textDetails AS VARCHAR(1000)) AS itemDesc,                                          
 AddC.vcFirstName || '' '' || AddC.vcLastName as Name,        
  case when AddC.numPhone <> '''' then AddC.numPhone || case when AddC.numPhoneExtension <> '''' then '' - '' || AddC.numPhoneExtension else '''' end  else '''' end as Phone,                                                                                    
  
    
      
                                                                  
  Comp.vcCompanyName,                                           
 AddC.vcEmail,                                          
 case when Comm.bitTask = 971 then  ''Communication''  when Comm.bitTask = 972 then  ''Task''  else ''Unknown'' end   AS Task                                           
 FROM  Communication Comm                                           
 JOIN AdditionalContactsInformation AddC                                           
 ON Comm.numContactId = AddC.numContactId                                           
 JOIN DivisionMaster Div                                           
 ON AddC.numDivisionId = Div.numDivisionID                                           
 INNER JOIN CompanyInfo  Comp                                           
 ON Div.numCompanyID = Comp.numCompanyId                                            
 WHERE Comm.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);                                          
                                          
-- Action Item  scheduled                                          
      if v_Condition = 'S' and v_RepType = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' and bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                 
        and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.numCreatedBy  in (select numContactID from AdditionalContactsInformation                                                
 where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || '))';
      end if;
      if v_Condition = 'S' and v_RepType = 2 then 
         v_strSql := coalesce(v_strSql,'') || ' and bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                            
        and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.numCreatedBy =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      end if;                                          
                                          
-- Action Item  Completed                                          
      if v_Condition = 'C' and v_RepType = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' and Comm.bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                            
 and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.bitClosedFlag=true and Comm.numCreatedBy in (select numContactID from AdditionalContactsInformation                                              
 where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || '))';
      end if;
      if v_Condition = 'C' and v_RepType = 2 then  
         v_strSql := coalesce(v_strSql,'') || ' and bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                            
 and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.bitClosedFlag=true and Comm.numCreatedBy =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      end if;                                          
                                          
-- Action Item  Past Due                                          
      if v_Condition = 'P' and v_RepType = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' and Comm.bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || '                                           
 and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || '''                                           
 and Comm.bitClosedFlag=false and Comm.numCreatedBy in (select numContactID from AdditionalContactsInformation                                              
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '                                           
and numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ')) and dtStartTime< ''' || CAST(TIMEZONE('UTC',now()) AS VARCHAR(20)) || '''';
      end if;
      if v_Condition = 'P' and v_RepType = 2 then  
         v_strSql := coalesce(v_strSql,'') || ' and Comm.bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || '                
 and dtStartTime< ''' || CAST(TIMEZONE('UTC',now()) AS VARCHAR(20)) || '''                                          
 and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || '''                                           
 and Comm.bitClosedFlag=false and Comm.numCreatedBy =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      end if;
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      RAISE NOTICE '%',v_strSql;
      EXECUTE 'insert into tt_TEMPTABLE(                                            
 closeDate,                                            
      itemDesc,                                            
      Name,      
      Phone,                                            
      vcCompanyName,                                            
      vcEmail,                                          
      Task)                                          
' || v_strSql;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      open SWV_RefCur for
      select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;
      select count(*) INTO v_TotRecs from tt_TEMPTABLE;
   end if;                                        
   if v_RepType = 3 then

      v_strSql := 'SELECT ADC1.numContactId,isnull(vcFirstName,''-'') as vcFirstName,isnull(vcLastName,''-'') as vcLastName,(SELECT                                           
 count(*)                                        
 FROM  Communication Comm ';
      if v_Condition = 'S' then  
         v_strSql := coalesce(v_strSql,'') || ' where bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                            
        and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                           
WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      if v_Condition = 'C' then   
         v_strSql := coalesce(v_strSql,'') || ' where bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || ' and Comm. dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || '''                                            
 and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || ''' and Comm.bitClosedFlag=true and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                            
WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      if v_Condition = 'P' then  
         v_strSql := coalesce(v_strSql,'') || ' where Comm.bitTask=' || SUBSTR(CAST(v_bitTask AS VARCHAR(20)),1,20) || '               
 and dtStartTime< ''' || CAST(TIMEZONE('UTC',now()) AS VARCHAR(20)) || '''                                           
 and Comm.dtCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(20)),1,20) || '''                                           
 and Comm.bitClosedFlag=false and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                            
 WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   end if;
   RETURN;
END; $$;


