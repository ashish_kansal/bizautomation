-- Stored procedure definition script USP_WareHouseItems_CheckForDuplicateAttr for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WareHouseItems_CheckForDuplicateAttr(v_numDomainID NUMERIC(18,0),
	v_numWareHouseID NUMERIC(18,0),
	v_numWLocationID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_strFieldList TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitDuplicate  BOOLEAN DEFAULT 0;
   v_strAttribute  VARCHAR(300) DEFAULT '';
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER; 
   v_numFldID  NUMERIC(18,0);
   v_numFldValue  VARCHAR(300);
BEGIN
   IF OCTET_LENGTH(v_strFieldList) > 2 then
	
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS

      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         Fld_ID NUMERIC(18,0),
         Fld_Value VARCHAR(300)
      );
      INSERT INTO tt_TEMPTABLE(Fld_ID,
			Fld_Value)
      SELECT * FROM
	  XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strFieldList AS XML)
				COLUMNS
					Fld_ID NUMERIC(18,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(300) PATH 'Fld_Value'
			);

      select   COUNT(*) INTO v_COUNT FROM tt_TEMPTABLE;
      WHILE v_i <= v_COUNT LOOP
         select   Fld_ID, Fld_Value INTO v_numFldID,v_numFldValue FROM tt_TEMPTABLE WHERE ID = v_i;
         IF SWF_IsNumeric(v_numFldValue) = true then
			
            select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute FROM CFW_Fld_Master WHERE Fld_id = v_numFldID AND Grp_id = 9;
            IF LENGTH(v_strAttribute) > 0 then
				
               select   coalesce(v_strAttribute,'') || '  - ' || vcData || ',' INTO v_strAttribute FROM Listdetails WHERE numListItemID = v_numFldValue;
            end if;
         ELSE
            select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute from  CFW_Fld_Master where Fld_id = v_numFldID AND Grp_id = 9;
            IF LENGTH(v_strAttribute) > 0 then
				
               v_strAttribute := coalesce(v_strAttribute,'') || '  - -,';
            end if;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      IF(SELECT
      COUNT(*)
      FROM
      WareHouseItems WI
      WHERE
      WI.numWareHouseID = v_numWareHouseID
      AND coalesce(WI.numWLocationID,0) = coalesce(v_numWLocationID,0)
      AND WI.numItemID = v_numItemCode
      AND WI.numDomainID = v_numDomainID
      AND fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) = v_strAttribute) > 0 then
		
         v_bitDuplicate := true;
      end if;

   end if;	  

   open SWV_RefCur for SELECT v_bitDuplicate;
END; $$;













