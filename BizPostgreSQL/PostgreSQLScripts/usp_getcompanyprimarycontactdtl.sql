-- Stored procedure definition script usp_GetCompanyPrimaryContactDTl for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyPrimaryContactDTl(v_numDivisionID NUMERIC DEFAULT 0                             
--                                                    
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  A.vcFirstName,
            A.vcLastname,
            (A.numPhone || ',' || A.numPhoneExtension) AS numPhone,
            A.vcEmail,
            C.vcCompanyName,
            A.vcFax,
            A.charSex,
            A.bintDOB,
            GetAge(A.bintDOB,TIMEZONE('UTC',now())) AS Age,
            A.txtNotes,
            A.numHomePhone,
            (A.VcAsstFirstName || ' ' || A.vcAsstLastName) AS vcAsstName,
            (A.numAsstPhone || ',' || A.numAsstExtn) AS numAsstPhone,
            A.vcAsstEmail,
            A.charSex,
		   coalesce(vcStreet,'') || coalesce(vcCity,'') || ' ,'
   || coalesce(fn_GetState(numState),'') || ' '
   || coalesce(vcPostalCode,'') || ' <br>'
   || coalesce(fn_GetListItemName(numCountry),'') AS Address,
            C.vcWebSite
   FROM    AdditionalContactsInformation A
   INNER JOIN DivisionMaster D ON A.numDivisionId = D.numDivisionID
   INNER JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
   LEFT JOIN AddressDetails AD ON AD.numRecordID = A.numContactId
   AND AD.tintAddressOf = 1
   AND AD.tintAddressType = 0
   AND AD.bitIsPrimary = true
   AND AD.numDomainID = A.numDomainID
   LEFT JOIN UserMaster U ON U.numUserDetailId = A.numContactId
   WHERE   A.numContactId IN(SELECT  numRecOwner
      FROM    CompanyInfo CMP INNER JOIN DivisionMaster DM ON CMP.numCompanyId = DM.numCompanyID
      WHERE   DM.numDivisionID = v_numDivisionID);
END; $$;












