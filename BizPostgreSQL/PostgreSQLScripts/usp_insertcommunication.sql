DROP FUNCTION IF EXISTS usp_InsertCommunication;

CREATE OR REPLACE FUNCTION usp_InsertCommunication(v_numCommId NUMERIC DEFAULT 0,                            
v_bitTask NUMERIC DEFAULT NULL,                                        
v_numContactId NUMERIC DEFAULT NULL,                            
v_numDivisionId NUMERIC DEFAULT NULL,                            
v_txtDetails TEXT DEFAULT NULL,                            
v_numOppId NUMERIC DEFAULT 0,                            
v_numAssigned NUMERIC DEFAULT 0,                                                
v_numUserCntID NUMERIC DEFAULT NULL,                                                                                                                                               
v_numDomainId NUMERIC DEFAULT NULL,                                                
v_bitClosed SMALLINT DEFAULT NULL,                                                
v_vcCalendarName VARCHAR(100) DEFAULT '',                                                                                                     
v_dtStartTime TIMESTAMP DEFAULT NULL,                 
v_dtEndtime TIMESTAMP DEFAULT NULL,                                                                             
v_numActivity NUMERIC(9,0) DEFAULT NULL,                                              
v_numStatus NUMERIC(9,0) DEFAULT NULL,                                              
v_intSnoozeMins INTEGER DEFAULT NULL,                                              
v_tintSnoozeStatus SMALLINT DEFAULT NULL,                                            
v_intRemainderMins INTEGER DEFAULT NULL,                                            
v_tintRemStaus SMALLINT DEFAULT NULL,                                  
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,                  
v_bitOutLook SMALLINT DEFAULT NULL,            
v_bitSendEmailTemplate BOOLEAN DEFAULT NULL,            
v_bitAlert BOOLEAN DEFAULT NULL,            
v_numEmailTemplate NUMERIC(9,0) DEFAULT 0,            
v_tintHours SMALLINT DEFAULT 0,          
v_CaseID  NUMERIC DEFAULT 0 ,        
v_CaseTimeId NUMERIC DEFAULT 0,    
v_CaseExpId NUMERIC DEFAULT 0  ,                     
v_ActivityId NUMERIC DEFAULT 0 ,
v_bitFollowUpAnyTime BOOLEAN DEFAULT NULL,
v_strAttendee TEXT DEFAULT NULL,
v_numLinkedOrganization NUMERIC(18,0) DEFAULT 0,
v_numLinkedContact NUMERIC(18,0) DEFAULT 0,
v_strContactIds VARCHAR(500) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CheckRecord  BOOLEAN;
   v_tempAssignedTo  NUMERIC(18,0);
BEGIN
   v_CheckRecord := true;                

   IF NOT EXISTS(SELECT * FROM Communication WHERE numCommId = v_numCommId) then
      v_CheckRecord := false;
   end if;                                                

   IF v_numCommId = 0 OR v_CheckRecord = false then
	
      IF(v_strContactIds = '') then
		
         IF coalesce(v_numDivisionId,0) = 0 then
			
            IF coalesce(v_numContactId,0) > 0 then
				
               select   numDivisionId INTO v_numDivisionId FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
               IF coalesce(v_numDivisionId,0) = 0 then
					
                  RAISE NOTICE 'COMPANY_NOT_FOUND_FOR_CONTACT';
                  RETURN;
               end if;
            ELSE
               RAISE NOTICE 'COMPANY_NOT_FOUND';
               RETURN;
            end if;
         end if;
         IF coalesce(v_numContactId,0) = 0 then
			
            RAISE NOTICE 'CONTACT_NOT_FOUND';
            RETURN;
         end if;
      end if;
      IF v_numAssigned = 0 then
         v_numAssigned := v_numUserCntID;
      end if;
      IF(v_strContactIds = '') then
		
         INSERT INTO Communication(bitTask,
				numContactId,
				numDivisionID,
				textDetails,
				intSnoozeMins,
				intRemainderMins,
				numStatus,
				numActivity,
				numAssign,
				tintSnoozeStatus,
				tintRemStatus,
				numOppId,
				numCreatedBy,
				dtCreatedDate,
				nummodifiedby,
				dtModifiedDate,
				numDomainID,
				bitClosedFlag,
				vcCalendarName,
				bitOutlook,
				dtStartTime,
				dtEndTime,
				numAssignedBy,
				bitsendEmailTemp,
				numEmailTemplate,
				tinthours,
				bitalert,
				CaseId,
				caseTimeid,
				caseExpid,
				numActivityId,
				bitFollowUpAnyTime)
			VALUES(v_bitTask,
				v_numContactId,
				v_numDivisionId,
				coalesce(v_txtDetails, ''),
				v_intSnoozeMins,
				v_intRemainderMins,
				v_numStatus,
				v_numActivity,
				v_numAssigned,
				v_tintSnoozeStatus,
				v_tintRemStaus,
				v_numOppId,
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numDomainId,
				(CASE WHEN COALESCE(v_bitClosed,0) = 1 THEN true ELSE false END),
				v_vcCalendarName,
				(CASE WHEN COALESCE(v_bitOutLook,0) = 1 THEN true ELSE false END),
				v_dtStartTime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),
				v_dtEndtime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),
				v_numUserCntID,
				v_bitSendEmailTemplate,
				v_numEmailTemplate,
				v_tintHours,
				v_bitAlert,
				v_CaseID,
				v_CaseTimeId,
				v_CaseExpId,
				v_ActivityId,
				v_bitFollowUpAnyTime);
			
         v_numCommId := CURRVAL('communication_seq');
         IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId = v_numCommId AND numContactId = v_numContactId) then
			
            INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(v_numCommId,v_numContactId);
         end if;
         IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId = v_numCommId AND numContactId = v_numAssigned) then
			
            INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(v_numCommId,v_numAssigned);
         end if;
         IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId = v_numCommId AND numContactId = v_numUserCntID) then
			
            INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(v_numCommId,v_numUserCntID);
         end if;
      ELSE
         INSERT INTO Communication(bitTask,
				numContactId,
				numDivisionID,
				textDetails,
				intSnoozeMins,
				intRemainderMins,
				numStatus,
				numActivity,
				numAssign,
				tintSnoozeStatus,
				tintRemStatus,
				numOppId,
				numCreatedBy,
				dtCreatedDate,
				nummodifiedby,
				dtModifiedDate,
				numDomainID,
				bitClosedFlag,
				vcCalendarName,
				bitOutlook,
				dtStartTime,
				dtEndTime,
				numAssignedBy,
				bitsendEmailTemp,
				numEmailTemplate,
				tinthours,
				bitalert,
				CaseId,
				caseTimeid,
				caseExpid,
				numActivityId,
				bitFollowUpAnyTime)
         SELECT
         v_bitTask,
				CAST(Items AS NUMERIC(9,0)),
				(SELECT  numDivisionId FROM AdditionalContactsInformation WHERE numContactId = CAST(Items AS NUMERIC(9,0)) LIMIT 1),
				coalesce(v_txtDetails,''),
				v_intSnoozeMins,
				v_intRemainderMins,
				v_numStatus,
				v_numActivity,
				v_numAssigned,
				v_tintSnoozeStatus,
				v_tintRemStaus,
				v_numOppId,
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numDomainId,
				(CASE WHEN COALESCE(v_bitClosed,0) = 1 THEN true ELSE false END),
				v_vcCalendarName,
				(CASE WHEN COALESCE(v_bitOutLook,0) = 1 THEN true ELSE false END),
				v_dtStartTime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),
				v_dtEndtime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),
				v_numUserCntID,
				v_bitSendEmailTemplate,
				v_numEmailTemplate,
				v_tintHours,
				v_bitAlert,
				v_CaseID,
				v_CaseTimeId,
				v_CaseExpId,
				v_ActivityId ,v_bitFollowUpAnyTime
         FROM
         Split(v_strContactIds,',')
         WHERE
         Items <> '' AND Items <> '0';
         v_numCommId := CURRVAL('communication_seq');
      end if;
      IF coalesce(v_numLinkedOrganization,0) > 0 then
		
         INSERT INTO CommunicationLinkedOrganization  VALUES(v_numCommId,v_numLinkedOrganization,v_numLinkedContact);
      end if;
   ELSE
    ---Updating if Action Item is assigned to someone                      
		
      v_tempAssignedTo := null;
      select   coalesce(numAssign,0) INTO v_tempAssignedTo FROM Communication WHERE numCommId = v_numCommId;
      IF (v_tempAssignedTo <> v_numAssigned AND v_numAssigned <> cast(NULLIF('0','') as NUMERIC)) then
		
         UPDATE Communication SET numAssignedBy = v_numUserCntID WHERE numCommId = v_numCommId;
      ELSEIF (v_numAssigned = 0)
      then
		
         UPDATE Communication SET numAssignedBy = 0 WHERE numCommId = v_numCommId;
      end if;
      IF v_vcCalendarName = '' then
		
         UPDATE
         Communication
         SET
         bitTask = v_bitTask,textDetails = v_txtDetails,intSnoozeMins = v_intSnoozeMins,
         numStatus = v_numStatus,numActivity = v_numActivity,tintSnoozeStatus = v_tintSnoozeStatus,
         intRemainderMins = v_intRemainderMins,tintRemStatus = v_tintRemStaus,
         numAssign = v_numAssigned,nummodifiedby = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
         bitClosedFlag = (CASE WHEN COALESCE(v_bitClosed,0) = 1 THEN true ELSE false END),
         bitOutlook = (CASE WHEN COALESCE(v_bitOutLook,0) = 1 THEN true ELSE false END),dtStartTime = v_dtStartTime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),dtEndTime = v_dtEndtime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),bitsendEmailTemp = v_bitSendEmailTemplate,
         numEmailTemplate = v_numEmailTemplate,tinthours = v_tintHours,
         bitalert = v_bitAlert,CaseId = v_CaseID,caseTimeid = v_CaseTimeId,
         caseExpid = v_CaseExpId,numActivityId = v_ActivityId,bitFollowUpAnyTime = v_bitFollowUpAnyTime,
         dtEventClosedDate = Case When coalesce(v_bitClosed,0) = 1 then TIMEZONE('UTC',now()) else null end
         WHERE
         numCommId = v_numCommId;
      ELSE
         UPDATE
         Communication
         SET
         bitTask = v_bitTask,textDetails = v_txtDetails,intSnoozeMins = v_intSnoozeMins,
         numStatus = v_numStatus,numActivity = v_numActivity,tintSnoozeStatus = v_tintSnoozeStatus,
         intRemainderMins = v_intRemainderMins,tintRemStatus = v_tintRemStaus,
         numAssign = v_numAssigned,nummodifiedby = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
         bitClosedFlag = (CASE WHEN COALESCE(v_bitClosed,0) = 1 THEN true ELSE false END),
         bitOutlook = (CASE WHEN COALESCE(v_bitOutLook,0) = 1 THEN true ELSE false END),dtStartTime = v_dtStartTime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),dtEndTime = v_dtEndtime+CAST(v_ClientTimeZoneOffset || 'minute' as interval),vcCalendarName = v_vcCalendarName,
         bitsendEmailTemp = v_bitSendEmailTemplate,numEmailTemplate = v_numEmailTemplate,
         tinthours = v_tintHours,bitalert = v_bitAlert,CaseId = v_CaseID,
         caseTimeid = v_CaseTimeId,caseExpid = v_CaseExpId,numActivityId = v_ActivityId,
         bitFollowUpAnyTime = v_bitFollowUpAnyTime,dtEventClosedDate = Case When coalesce(v_bitClosed,0) = 1 then TIMEZONE('UTC',now()) else null end
         WHERE
         numCommId = v_numCommId;
      end if;
      IF coalesce(v_numLinkedOrganization,0) > 0 then
		
         IF EXISTS(SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID = v_numCommId) then
			
            UPDATE CommunicationLinkedOrganization SET numDivisionID = v_numLinkedOrganization,numContactID = v_numLinkedContact WHERE numCommID = v_numCommId;
         ELSE
            INSERT INTO CommunicationLinkedOrganization  VALUES(v_numCommId,v_numLinkedOrganization,v_numLinkedContact);
         end if;
      ELSE
         DELETE FROM CommunicationLinkedOrganization WHERE numCommID = v_numCommId;
      end if;
   end if; 
   
   open SWV_RefCur for SELECT v_numCommId;
END; $$;












