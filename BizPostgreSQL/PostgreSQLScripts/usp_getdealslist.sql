-- Stored procedure definition script USP_GetDealsList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDealsList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                            
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                            
 v_tintSalesUserRightType SMALLINT DEFAULT 0,                                   
 v_tintPurchaseUserRightType SMALLINT DEFAULT 0,                                            
 v_tintSortOrder SMALLINT DEFAULT 4,                                          
 v_SortChar CHAR(1) DEFAULT '0',                                            
 v_FirstName VARCHAR(100) DEFAULT '',                                            
 v_LastName VARCHAR(100) DEFAULT '',                                            
 v_CustName VARCHAR(100) DEFAULT '' ,                                                  
 v_CurrentPage INTEGER DEFAULT NULL,                                            
 v_PageSize INTEGER DEFAULT NULL,                                            
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                            
 v_columnName VARCHAR(50) DEFAULT NULL,                                            
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                          
 v_numCompanyID NUMERIC(9,0) DEFAULT 0 ,                
 v_bitPartner BOOLEAN DEFAULT false  ,        
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL ,    
 v_tintShipped SMALLINT DEFAULT NULL  ,  
v_intOrder    SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                            
                                            
--Create a Temporary table to hold data                                            
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                            
   v_firstRec  INTEGER;                                             
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numOppID NUMERIC(9,0),
      vcPOppName VARCHAR(100),
      vcCompanyName VARCHAR(50),
      vcFirstName VARCHAR(50),
      vcusername VARCHAR(50),
      tintOppType VARCHAR(1),
      bintAccountClosingDate TIMESTAMP,
      monPAmount DECIMAL(20,5),
      STAGE VARCHAR(20),
      bintCreatedDate TIMESTAMP
   );                            
                            
                                            
   v_strSql := 'SELECT  Opp.numOppId,                            
  Opp.vcPOppName,                            
  C.vcCompanyName,                            
  ADC.vcFirstName,                        
  ADC1.vcFirstName,                                                                   
  Opp.tintOppType,                            
  Opp.bintAccountClosingDate,                            
  Opp.monPAmount,                               
  GetOppStatus(Opp.numOppId) as STAGE,                            
  Opp.bintCreatedDate                                         
  FROM OpportunityMaster Opp                                             
  INNER JOIN AdditionalContactsInformation ADC                                             
  ON Opp.numContactId = ADC.numContactId                                             
  INNER JOIN DivisionMaster Div                                             
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                             
  INNER JOIN CompanyInfo C                                             
  ON Div.numCompanyID = C.numCompanyId ';                
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || 'left join OpportunityContact OppCont on OppCont.numOppId = Opp.numOppId and OppCont.bitPartner = true and OppCont.numContactId =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
   end if;                
                
                                                
   v_strSql := coalesce(v_strSql,'') || 'left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numRecOwner                                             
  WHERE Opp.tintOppStatus = 1 and Opp.tintshipped =' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || ' and  Opp.tintOppType = 1                                            
  and Div.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';     
                  
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                             
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''';
   end if;                  
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and C.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;                 
   if v_intOrder <> 0 then
 
      if v_intOrder = 1 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = false ';
      end if;
      if v_intOrder = 2 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = true';
      end if;
   end if;     
                                           
   if v_numCompanyID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And Div.numCompanyID =' || SUBSTR(CAST(v_numCompanyID AS VARCHAR(15)),1,15);
   end if;                                            
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                    
   if v_tintSalesUserRightType = 0 then 
      v_strSql := coalesce(v_strSql,'') || ' AND Opp.tintOppType = 0';
   end if;                                            
   if v_tintSalesUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in             
(select distinct(numOppId) from OpportunityStageDetails where  numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( OppCont.bitPartner=1 and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end
      || ')';
   ELSEIF v_tintSalesUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or div.numTerID=0 or             
Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
   end if;                                         
   if v_tintSortOrder <> cast(NULLIF('0','') as SMALLINT) then  
      v_strSql := coalesce(v_strSql,'') || '  AND Opp.tintOppType= ' || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1);
   end if;                                   
                                 
                                  
   v_strSql := coalesce(v_strSql,'') || ' union SELECT  Opp.numOppId,                            
  Opp.vcPOppName,                            
  C.vcCompanyName,                            
  ADC.vcFirstName,                        
  ADC1.vcFirstName ,                                                                   
  Opp.tintOppType,                            
  Opp.bintAccountClosingDate,                            
  Opp.monPAmount,                               
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                            
  opp.bintCreatedDate                                              
  FROM OpportunityMaster Opp                                             
  INNER JOIN AdditionalContactsInformation ADC                                             
  ON Opp.numContactId = ADC.numContactId                                             
  INNER JOIN DivisionMaster Div                                             
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                             
  INNER JOIN CompanyInfo C                                             
   ON Div.numCompanyID = C.numCompanyId ';                
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || 'left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
   end if;                
                
                                                
   v_strSql := coalesce(v_strSql,'') || 'left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                             
  WHERE Opp.tintOppstatus=1 and Opp.tintShipped=' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || '   and Div.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                   
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                             
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                  
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and C.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                                          
   if v_numCompanyID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And Div.numCompanyID =' || SUBSTR(CAST(v_numCompanyID AS VARCHAR(15)),1,15);
   end if;                                            
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;    
  
  
   if v_intOrder <> 0 then
 
      if v_intOrder = 1 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = false ';
      end if;
      if v_intOrder = 2 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = true';
      end if;
   end if;   
  
                                           
   if v_tintPurchaseUserRightType = 0 then 
      v_strSql := coalesce(v_strSql,'') || ' AND opp.tintOppType =0';
   end if;                                      
   if v_tintPurchaseUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in             
(select distinct(numOppId) from OpportunityStageDetails where               
numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end || ')';
   ELSEIF v_tintPurchaseUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or div.numTerID=0 or             
Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
   end if;                                                                        
            
            
   if v_tintSortOrder <> cast(NULLIF('0','') as SMALLINT) then  
      v_strSql := coalesce(v_strSql,'') || ' AND Opp.tintOppType= ' || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1);
   end if;                                                              
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY  ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                                                           
   RAISE NOTICE '%',v_strSql;                                          
   EXECUTE 'insert into tt_TEMPTABLE(numOppId,                            
    vcPOppName,                            
    vcCompanyName,                            
    vcFirstName,                            
    vcusername,                            
    tintOppType,                            
    bintAccountClosingDate,                            
    monPAmount,                            
    STAGE,bintCreatedDate )                                            
' || v_strSql;                                             
                                            
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                           
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                              
                            
   open SWV_RefCur for
   SELECT  Opp.numOppId,
  Opp.vcpOppName AS Name,
  GetOppStatus(Opp.numOppId) as STAGE,
  Opp.intPEstimatedCloseDate AS CloseDate,
  Opp.numrecowner,
  Opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as bintCreatedDate ,
  ADC.vcFirstName || ' ' || ADC.vcLastname AS Contact,
  C.vcCompanyName AS Company,
  C.numCompanyId,
  Div.tintCRMType,
  ADC.numContactId,
  Div.numDivisionID,
  Div.numTerID,
  Opp.monPAmount,
  ADC1.vcFirstName || ' ' || ADC1.vcLastname as UserName,
 Opp.tintopptype,
  Opp.bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as bintAccountClosingDate,
  fn_GetContactName(Opp.numassignedto) || '/ ' || fn_GetContactName(Opp.numAssignedBy) as AssignedToBy
   FROM OpportunityMaster Opp
   INNER JOIN AdditionalContactsInformation ADC
   ON Opp.numContactId = ADC.numContactId
   INNER JOIN DivisionMaster Div
   ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
   INNER JOIN CompanyInfo C
   ON Div.numCompanyID = C.numCompanyId
   left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numrecowner
   join tt_TEMPTABLE T on T.numOppID = Opp.numOppId
   WHERE ID > v_firstRec and ID < v_lastRec order by ID;                            
                            
                                          
   RETURN;
END; $$;


