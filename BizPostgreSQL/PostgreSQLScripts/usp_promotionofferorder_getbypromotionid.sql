-- Stored procedure definition script USP_PromotionOfferOrder_GetByPromotionID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromotionOfferOrder_GetByPromotionID(v_numDomainID NUMERIC(18,0)
	,v_numPromotionID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT * FROM PromotionOffer WHERE numDomainId = v_numDomainID AND numProId = v_numPromotionID; 
	 	
   open SWV_RefCur2 for
   SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = v_numPromotionID ORDER BY numProOfferOrderID;

   open SWV_RefCur3 for
   SELECT * FROM DiscountCodes WHERE numPromotionID = v_numPromotionID;
   RETURN;
END; $$;


