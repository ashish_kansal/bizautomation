-- Stored procedure definition script GetDivisionName for PostgreSQL
CREATE OR REPLACE FUNCTION GetDivisionName(v_numDivisionID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select vcDivisionName from DivisionMaster where numDivisionID = v_numDivisionID;
END; $$;













