CREATE OR REPLACE FUNCTION USP_ForeCastOPP(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                          
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                        
 v_tintType SMALLINT DEFAULT 1 ,                  
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,          
 v_tintFirstMonth SMALLINT DEFAULT 0,          
 v_tintSecondMonth SMALLINT DEFAULT 0,          
 v_tintThirdMonth SMALLINT DEFAULT 0,          
 v_intYear INTEGER DEFAULT 0 ,         
 v_byteMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 or v_byteMode = 2 then

      open SWV_RefCur for
      SELECT X.Mon,
	   X.ForeCastAmount,
	   X.QuotaAmt,
	   X.sold,
	   X.Pipeline,
	   X.Probability,
	   ((X.Pipeline*X.Probability)/100) AS ProbableAmt
      FROM(Select M.Mon,coalesce(monForecastAmount,0) as ForeCastAmount,coalesce(monQuota,0) as QuotaAmt,      
 --SOLD
 coalesce((SELECT sum(monPAmount) FROM  OpportunityMaster WHERE tintopptype = 1 and  tintoppstatus = 1
            and 1 =(Case WHEN v_byteMode = 0 then 1
            else Case when numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end)
               when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end) then 1 else 0 end end)
            and numDomainId = v_numDomainID And EXTRACT(month FROM bintCreatedDate) = M.Mon And EXTRACT(year FROM bintCreatedDate) = v_intYear),0) AS sold,
 
 --in Pipeline
 coalesce((SELECT sum(monPAmount) FROM OpportunityMaster WHERE tintopptype = 1 And  tintoppstatus = 0
            and 1 =(Case WHEN v_byteMode = 0 then 1
            else case when numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end)
               when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end) then 1 else 0 end end)
            and numDomainId = v_numDomainID And  EXTRACT(month FROM bintCreatedDate) = M.Mon And EXTRACT(year FROM bintCreatedDate) = v_intYear),0) As Pipeline,
 
  -- Probability
 coalesce((Select Avg(coalesce(intTotalProgress,0)) FROM OpportunityMaster Opp inner join ProjectProgress PP on Opp.numOppId = PP.numOppId
            Where Opp.tintopptype = 1 And   Opp.tintoppstatus = 0
            and 1 =(Case WHEN v_byteMode = 0 then 1
            else case when numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end)
               when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end) then 1 else 0 end end)
            and Opp.numDomainId = v_numDomainID And  EXTRACT(month FROM bintCreatedDate) = M.Mon And EXTRACT(year FROM bintCreatedDate) = v_intYear),0) As Probability,       
--ProbableAmt
-- isnull((Select Sum(X.Amount)   From (Select   ( monPAmount*(Select AVG(isnull(intTotalProgress,0))  from dbo.ProjectProgress PP
-- Where OpportunityMaster.numOppId=PP.numOppId ))/100 as Amount  From OpportunityMaster                         
-- Where tintOppType=1 And month(intPEstimatedCloseDate)=M.Mon and year(intPEstimatedCloseDate)=@intYear       
-- and  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) when @numDivisionID>0 then     
--	(select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)  And tintoppStatus=0 and tintshipped=0 and numDomainID=@numDomainID)X ),0) 
0 As ProbableAmt
         From Forecast F
         right join(select v_tintFirstMonth as Mon union select v_tintSecondMonth union select v_tintThirdMonth) M
         on F.tintMonth = M.Mon and sintYear = v_intYear and numDomainID = v_numDomainID and tintForecastType = 1    and numCreatedBy =   v_numUserCntID) X;
   ELSEIF v_byteMode = 1
   then

      open SWV_RefCur for
      Select M.Mon,coalesce(monForecastAmount,0) as ForeCastAmount,coalesce(monQuota,0) as QuotaAmt
      From Forecast F
      right join(select v_tintFirstMonth as Mon union select v_tintSecondMonth union select v_tintThirdMonth) M
      on F.tintMonth = M.Mon and sintYear = v_intYear and numDomainID = v_numDomainID and tintForecastType = 1;
   end if;
   RETURN;
END; $$;


