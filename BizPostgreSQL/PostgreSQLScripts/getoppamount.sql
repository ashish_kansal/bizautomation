-- Function definition script GetOppAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppAmount(v_numUserCntID NUMERIC(9,0),v_numDomainID NUMERIC(9,0),v_dtDateFrom TIMESTAMP,v_dtDateTo TIMESTAMP)
RETURNS DECIMAL LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppAmount  DECIMAL;
BEGIN
   select   sum(OM.monPAmount) INTO v_OppAmount from OpportunityMaster OM where OM.tintopptype = 1 and tintoppstatus = 1 and OM.numDomainId = v_numDomainID
   and  OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo  and OM.numrecowner = v_numUserCntID;
  
   RETURN v_OppAmount;
END; $$;

