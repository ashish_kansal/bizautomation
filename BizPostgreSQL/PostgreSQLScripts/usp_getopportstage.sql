-- Stored procedure definition script usp_GetOpportStage for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetOpportStage(v_numOppID  NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM stageOpportunity
   WHERE numOppID = v_numOppID;
END; $$;












