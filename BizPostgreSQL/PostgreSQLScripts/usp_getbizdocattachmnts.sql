-- Stored procedure definition script usp_getbizdocattachmnts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getbizdocattachmnts(v_numBizDocID NUMERIC(9,0) DEFAULT 0,      
 v_numDomainId NUMERIC(9,0) DEFAULT 0,      
 v_numAttachmntID NUMERIC(9,0) DEFAULT 0,
 v_numBizDocTempID NUMERIC(9,0) DEFAULT 0     ,
 v_intAddReferenceDocument NUMERIC(10,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if (v_numAttachmntID = 0 and   v_numBizDocID <> 0) then

      if not exists(select *,'' AS vcFileName from BizDocAttachments where numBizDocID = v_numBizDocID and numDomainID = v_numDomainId and vcDocName = 'BizDoc' and coalesce(numBizDocTempID,0) = v_numBizDocTempID) then
  
         insert into BizDocAttachments(numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)
   values(v_numBizDocID,v_numDomainId,'BizDoc',1,'BizDoc',v_numBizDocTempID);
      end if;
      IF(v_intAddReferenceDocument = 1) then
  
         open SWV_RefCur for
         select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,(SELECT  vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numBizDocID LIMIT 1) AS vcFileName from BizDocAttachments
         where numBizDocID = v_numBizDocID and numDomainID = v_numDomainId and coalesce(numBizDocTempID,0) = v_numBizDocTempID
         UNION
         SELECT numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,'' as vcFileName from BizDocAttachments
         where vcURL <> 'BizDoc' AND numBizDocID =(SELECT  numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numBizDocID LIMIT 1) and numDomainID = v_numDomainId and coalesce(numBizDocTempID,0) =(SELECT  numBizDocTempID FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numBizDocID LIMIT 1) order by 6;
      ELSE
         open SWV_RefCur for
         select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,'' AS vcFileName from BizDocAttachments
         where numBizDocID = v_numBizDocID and numDomainID = v_numDomainId and coalesce(numBizDocTempID,0) = v_numBizDocTempID order by numOrder;
      end if;
   ELSEIF v_numAttachmntID > 0
   then
      open SWV_RefCur for
      select *,'' AS vcFileName from BizDocAttachments where numAttachmntID = v_numAttachmntID;
   end if;
   RETURN;
END; $$;


