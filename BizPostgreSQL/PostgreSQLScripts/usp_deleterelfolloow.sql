-- Stored procedure definition script USP_DeleteRelfolloow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteRelfolloow(v_numRelFolID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete  from RelFollowupStatus  where numRelFolID = v_numRelFolID;
   RETURN;
END; $$;


