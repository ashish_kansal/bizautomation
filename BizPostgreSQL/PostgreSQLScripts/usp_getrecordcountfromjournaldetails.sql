-- Stored procedure definition script USP_GetRecordCountFromJournalDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRecordCountFromJournalDetails(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                      
 v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select  Count(*) From General_Journal_Header  GJH
   Inner Join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where (GJD.numChartAcntId = v_numChartAcntId or GJH.numChartAcntId = v_numChartAcntId)
   And  GJD.numDomainId = v_numDomainId;
END; $$;












