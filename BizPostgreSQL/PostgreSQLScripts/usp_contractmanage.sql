-- Stored procedure definition script USP_ContractManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ContractManage(INOUT v_numContractId BIGINT ,      
v_numDivisionID BIGINT,      
v_vcContractName VARCHAR(250),      
v_bintStartDate TIMESTAMP,      
v_bintExpDate TIMESTAMP,      
v_numIncidents BIGINT,      
v_numHours BIGINT,      
v_vcNotes TEXT,      
v_numEmailDays BIGINT,      
v_numEmailIncidents BIGINT,      
v_numEmailHours BIGINT,      
v_numUserCntID BIGINT,      
v_numDomainID BIGINT,      
v_numAmount BIGINT,    
v_bitAmount BOOLEAN,    
v_bitDays BOOLEAN,    
v_bitHours BOOLEAN,    
v_bitInci BOOLEAN    ,  
v_bitEDays BOOLEAN,    
v_bitEHours BOOLEAN,    
v_bitEInci BOOLEAN,      
v_decRate DECIMAL(20,5), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   insert into  ContractManagement(numDivisionID ,
vcContractName ,
bintStartDate ,
bintExpDate ,
numincidents ,
numHours ,
vcNotes ,
numEmailDays ,
numEmailIncidents ,
numEmailHours ,
numUserCntID ,
numdomainId,
bintcreatedOn,
numAmount ,
bitAmount,
bitDays ,
bitHour,
bitIncidents ,decrate)
values(v_numDivisionID ,
v_vcContractName ,
v_bintStartDate ,
v_bintExpDate ,
v_numIncidents ,
v_numHours ,
v_vcNotes ,
v_numEmailDays ,
v_numEmailIncidents ,
v_numEmailHours ,
v_numUserCntID ,
v_numDomainID,
TIMEZONE('UTC',now()) ,
v_numAmount,
v_bitAmount,
v_bitDays ,
v_bitHours,
v_bitInci ,v_decRate);      

   v_numContractId := CURRVAL('ContractManagement_seq');      
   open SWV_RefCur for
   SELECT
   v_numContractId;
   RETURN;
END; $$;


