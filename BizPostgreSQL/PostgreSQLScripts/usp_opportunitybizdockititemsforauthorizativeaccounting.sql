-- Stored procedure definition script USP_OpportunityBizDocKitItemsForAuthorizativeAccounting for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocKitItemsForAuthorizativeAccounting(v_numOppId NUMERIC(18,0),
	v_numOppBizDocsId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocId  NUMERIC(18,0);
BEGIN
   select   numBizDocId INTO v_numBizDocId FROM
   OpportunityBizDocs WHERE
   numOppBizDocsId = v_numOppBizDocsId;

   IF v_numBizDocId = 296 then --FULFILLMENT BIZDOC
	
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
      INSERT INTO OpportunityBizDocKitItems(numOppBizDocID,
			numOppBizDocItemID,
			numOppChildItemID,
			numChildItemID,
			monAverageCost)
      SELECT
      OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,(CASE
      WHEN coalesce(I.charItemType,'') = 'S' AND coalesce(I.bitExpenseItem,false) = true
      THEN(CASE
         WHEN coalesce(IMain.tintKitAssemblyPriceBasedOn,0) = 3
         THEN coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
         ELSE coalesce(I.monListPrice,0)
         END)
      ELSE
         coalesce(I.monAverageCost,0)
      END)
      FROM
      OpportunityBizDocItems OBI
      INNER JOIN
      OpportunityItems OI
      ON
      OBI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item IMain
      ON
      OI.numItemCode = IMain.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OKI.numOppItemID = OI.numoppitemtCode
      AND OI.numOppId = OKI.numOppId
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      LEFT JOIN
      Vendor V
      ON
      I.numVendorID = V.numVendorID
      AND I.numItemCode = V.numItemCode
      WHERE
      OBI.numOppBizDocID = v_numOppBizDocsId
      AND OBI.numOppBizDocItemID NOT IN(SELECT numOppBizDocItemID FROM OpportunityBizDocKitItems WHERE numOppBizDocID = v_numOppBizDocsId);
      INSERT INTO OpportunityBizDocKitChildItems(numOppBizDocID,
			numOppBizDocItemID,
			numOppBizDocKitItemID,
			numOppKitChildItemID,
			numChildItemID,
			monAverageCost)
      SELECT
      OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OBKI.numOppBizDocItemID
			,OKCI.numOppKitChildItemID
			,OKCI.numItemID
			,(CASE
      WHEN coalesce(I.charItemType,'') = 'S' AND coalesce(I.bitExpenseItem,false) = true
      THEN(CASE
         WHEN coalesce(IMain.tintKitAssemblyPriceBasedOn,0) = 3
         THEN coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
         ELSE coalesce(I.monListPrice,0)
         END)
      ELSE
         coalesce(I.monAverageCost,0)
      END)
      FROM
      OpportunityBizDocItems OBI
      INNER JOIN
      OpportunityBizDocKitItems OBKI
      ON
      OBI.numOppBizDocItemID = OBKI.numOppBizDocItemID
      INNER JOIN
      Item IMain
      ON
      OBKI.numChildItemID = IMain.numItemCode
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OBI.numOppItemID = OKCI.numOppItemID
      AND OBKI.numOppChildItemID = OKCI.numOppChildItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode
      LEFT JOIN
      Vendor V
      ON
      I.numVendorID = V.numVendorID
      AND I.numItemCode = V.numItemCode
      WHERE
      OBI.numOppBizDocID = v_numOppBizDocsId
      AND OBI.numOppBizDocItemID NOT IN(SELECT numOppBizDocItemID FROM OpportunityBizDocKitChildItems WHERE numOppBizDocID = v_numOppBizDocsId);
   end if;

     --OpportunityKitItems
   open SWV_RefCur for SELECT
   I.charItemType as type,
		CAST(I.numItemCode AS VARCHAR(30)) as ItemCode,
		OBKI.numOppChildItemID AS numoppitemtCode,
		(coalesce(OBI.numUnitHour,0)*(OKI.numQtyItemsReq_Orig*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,I.numDomainID,I.numBaseUnit),1))) as Unit,
		coalesce(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		coalesce(I.numAssetChartAcntId,0) as itemInventoryAsset,
		coalesce(I.numCOGsChartAcntId,0) AS itemCoGs
		,coalesce(I.bitExpenseItem,false) as bitExpenseItem
		,coalesce(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(OBKI.monAverageCost,0) END) AS ShippedAverageCost,
		coalesce(OI.bitDropShip,false) AS bitDropShip,
		NULLIF(OI.numProjectID,0) AS numProjectID,
		NULLIF(OI.numClassID,0) AS numClassID
   FROM
   OpportunityBizDocItems OBI
   INNER JOIN
   OpportunityBizDocKitItems OBKI
   ON
   OBKI.numOppBizDocItemID = OBI.numOppBizDocItemID
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKI.numOppChildItemID = OBKI.numOppChildItemID
   INNER JOIN
   OpportunityItems OI
   ON
   OKI.numOppItemID = OI.numoppitemtCode
   INNER JOIN
   Item IMain
   ON
   OI.numItemCode = IMain.numItemCode
   INNER JOIN
   Item I
   ON
   OBKI.numChildItemID = I.numItemCode
   WHERE
   OI.numOppId = v_numOppId
   AND OBI.numOppBizDocID = v_numOppBizDocsId
   AND coalesce(I.bitKitParent,false) = false
   AND coalesce(IMain.bitAssembly,false) = false
   UNION ALL
   SELECT
   I.charItemType as type,
		CAST(I.numItemCode AS VARCHAR(30)) as ItemCode,
		OBKCI.numOppKitChildItemID AS numoppitemtCode,
		(coalesce(OBI.numUnitHour,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,I.numDomainID,I.numBaseUnit),1))) as Unit,
		coalesce(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		coalesce(I.numAssetChartAcntId,0) as itemInventoryAsset,
		coalesce(I.numCOGsChartAcntId,0) AS itemCoGs
		,coalesce(I.bitExpenseItem,false) as bitExpenseItem
		,coalesce(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(OBKCI.monAverageCost,0) END) AS ShippedAverageCost,
		coalesce(OI.bitDropShip,false) AS bitDropShip,
		NULLIF(OI.numProjectID,0) AS numProjectID,
		NULLIF(OI.numClassID,0) AS numClassID
   FROM
   OpportunityBizDocItems OBI
   INNER JOIN
   OpportunityBizDocKitChildItems OBKCI
   ON
   OBKCI.numOppBizDocItemID = OBI.numOppBizDocItemID
   INNER JOIN
   OpportunityKitChildItems OKCI
   ON
   OKCI.numOppKitChildItemID = OBKCI.numOppKitChildItemID
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKCI.numOppChildItemID = OKI.numOppChildItemID
   INNER JOIN
   OpportunityItems OI
   ON
   OKI.numOppItemID = OI.numoppitemtCode
   INNER JOIN
   Item IMain
   ON
   OI.numItemCode = IMain.numItemCode
   INNER JOIN
   Item I
   ON
   OBKCI.numChildItemID = I.numItemCode
   WHERE
   OI.numOppId = v_numOppId
   AND OBI.numOppBizDocID = v_numOppBizDocsId;
END; $$;
