-- Stored procedure definition script usp_GetListModule for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetListModule(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from  ListModule WHERE bitVisible = true;
END; $$; 
