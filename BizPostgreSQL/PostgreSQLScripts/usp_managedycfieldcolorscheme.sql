-- Stored procedure definition script USP_ManageDycFieldColorScheme for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDycFieldColorScheme(v_numFieldColorSchemeID NUMERIC(9,0) DEFAULT 0,
    v_numFieldID NUMERIC(9,0) DEFAULT 0,
    v_numFormID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_vcFieldValue VARCHAR(100) DEFAULT '0',
    v_vcFieldValue1 VARCHAR(100) DEFAULT '0',
    v_vcColorScheme VARCHAR(50) DEFAULT 0,
    v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then --Select
	
      open SWV_RefCur for
      SELECT DFCS.numFieldColorSchemeID,VDC.vcFieldName,DFCS.numFieldID,DFCS.numFormID,DFCS.numDomainID,VDC.vcAssociatedControlType,
		CASE WHEN VDC.vcAssociatedControlType IN('SelectBox','ListBox') THEN
         CASE WHEN VDC.vcDbColumnName = 'tintSource' then fn_GetOpportunitySourceValue(DFCS.vcFieldValue::NUMERIC,DFCS.vcFieldValue1::SMALLINT,VDC.numDomainID)
         ELSE fn_getSelectBoxValue(VDC.vcListItemType,DFCS.vcFieldValue::NUMERIC,VDC.vcDbColumnName) END
      WHEN VDC.vcAssociatedControlType = 'CheckBox' THEN CASE WHEN DFCS.vcFieldValue = '1' THEN 'Yes' ELSE 'No' END
      ELSE DFCS.vcFieldValue END AS vcFieldValue,coalesce(DFCS.vcFieldValue1,'0') AS vcFieldValue1,DFCS.vcColorScheme,DFM.vcFormName
      from DycFieldColorScheme DFCS join View_DynamicDefaultColumns VDC ON DFCS.numFormID = VDC.numFormId AND DFCS.numFieldID = VDC.numFieldID
      JOIN dynamicFormMaster DFM ON DFM.numFormID = DFCS.numFormID
      where DFCS.numDomainID = v_numDomainID AND VDC.numFormId = v_numFormID AND VDC.numDomainID = v_numDomainID and coalesce(VDC.bitAllowGridColor,false) = true;
   ELSEIF v_tintMode = 1
   then --Insert
	
		--Check if color scheme is set for other field then throw error
      IF EXISTS(SELECT numFieldID FROM DycFieldColorScheme WHERE numFormID = v_numFormID AND numDomainID = v_numDomainID) then
		
         IF(SELECT DISTINCT numFieldID FROM DycFieldColorScheme WHERE numFormID = v_numFormID AND numDomainID = v_numDomainID) <> v_numFieldID then
				
            RAISE EXCEPTION 'Field_DEPENDANT';
            RETURN;
         end if;
      end if;  
		
		--Check if color scheme is already exists for FieldValue then throw error
      IF EXISTS(SELECT numFieldID FROM DycFieldColorScheme WHERE numFieldID = v_numFieldID AND numFormID = v_numFormID AND numDomainID = v_numDomainID AND vcFieldValue = v_vcFieldValue AND vcFieldValue1 = v_vcFieldValue1) then
		
         RAISE EXCEPTION 'Value_DEPENDANT';
         RETURN;
      end if; 
		
		--Otherwise insert record
      INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(v_numFieldID,v_numFormID,v_numDomainID,v_vcFieldValue,v_vcFieldValue1,v_vcColorScheme);
   ELSEIF v_tintMode = 2
   then --Delete
	
      DELETE FROM DycFieldColorScheme WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND numFieldColorSchemeID = v_numFieldColorSchemeID;
   ELSEIF v_tintMode = 3
   then --Sales Fulfillment color
	
      open SWV_RefCur for
      SELECT DFCS.numFieldColorSchemeID,DFCS.numFieldID,DFCS.numFormID,DFCS.numDomainID,
			 LD.vcData AS vcFieldValue,DFCS.vcColorScheme
      from DycFieldColorScheme DFCS JOIN Listdetails LD ON DFCS.vcFieldValue = CAST(LD.numListItemID AS VARCHAR)
      where DFCS.numDomainID = v_numDomainID AND DFCS.numFormID = 64;
   ELSEIF v_tintMode = 4
   then --Delete All Saved Data For that Form
	
      DELETE FROM DycFieldColorScheme WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID;
   end if;
    
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
END; $$;


