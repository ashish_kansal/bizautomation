-- Stored procedure definition script usp_GetSpecificContactInfo for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSpecificContactInfo(v_numContactId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	--SELECT vcFirstname,vcLastName,numPhone,vcEmail,numContactType  FROM additionalContactsinformation WHERE
   open SWV_RefCur for SELECT a.vcFirstName,a.vcLastname,a.numPhone,a.vcEmail,b.vcData,a.numContactType FROM AdditionalContactsInformation a INNER JOIN Listdetails b ON a.numContactType = b.numListItemID WHERE
   a.numContactId = v_numContactId;
END; $$;












