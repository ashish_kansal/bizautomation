-- Stored procedure definition script Usp_AddCustomFldsToAll for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_AddCustomFldsToAll(v_FieldId NUMERIC(9,0) DEFAULT 0,      
v_RelaionId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_order  NUMERIC;
BEGIN
   if not exists(select * from CFW_Fld_Dtl where numFieldId = v_FieldId and numRelation = v_RelaionId) then

      select   max(numOrder) INTO v_order from CFW_Fld_Dtl where numFieldId = v_FieldId and numRelation = v_RelaionId;
      if v_order is null then 
         v_order := 0;
      end if;
      v_order := v_order+1;
      insert into CFW_Fld_Dtl(numFieldId,numRelation,numOrder)values(v_FieldId,v_RelaionId,v_order);
   end if;
   RETURN;
END; $$;


