-- Stored procedure definition script USP_GetItemUOMConversion for PostgreSQL
Create or replace FUNCTION USP_GetItemUOMConversion(v_numDomainID NUMERIC(9,0),    
 v_numItemCode NUMERIC(9,0),
 v_numUOMId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT fn_UOMConversion(v_numUOMId,v_numItemCode,v_numDomainID,numBaseUnit) AS BaseUOMConversion,
   fn_UOMConversion(numBaseUnit,v_numItemCode,v_numDomainID,CASE WHEN coalesce(numPurchaseUnit,0) > 0 THEN numPurchaseUnit ELSE numBaseUnit END) AS PurchaseUOMConversion,
   fn_UOMConversion(numBaseUnit,v_numItemCode,v_numDomainID,CASE WHEN coalesce(numSaleUnit,0) > 0 THEN numSaleUnit ELSE numBaseUnit END) AS SaleUOMConversion,
   fn_GetUOMName(numBaseUnit) AS vcBaseUOMName
   FROM Item WHERE numDomainID = v_numDomainID and numItemCode = v_numItemCode;
END; $$;
   












