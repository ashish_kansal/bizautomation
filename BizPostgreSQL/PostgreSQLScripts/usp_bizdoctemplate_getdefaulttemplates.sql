-- Stored procedure definition script Usp_BizDocTemplate_GetDefaultTemplates for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_BizDocTemplate_GetDefaultTemplates(v_numDomainID NUMERIC,  
 v_numBizDocID NUMERIC,  
 v_numOppType NUMERIC,   
 v_tintTemplateType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
    -- Insert statements for procedure here  
   open SWV_RefCur for SELECT 'col1' FROM BizDocTemplate WHERE bitDefault = true AND numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND  tintTemplateType = v_tintTemplateType;
END; $$;  


