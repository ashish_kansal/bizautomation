DROP FUNCTION IF EXISTS USP_GetOpportunityList1;

CREATE OR REPLACE FUNCTION USP_GetOpportunityList1(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_tintUserRightType SMALLINT DEFAULT 0,
	v_tintSortOrder SMALLINT DEFAULT 4,
	v_dtLastDate TIMESTAMP DEFAULT NULL,
	v_OppType SMALLINT DEFAULT NULL,
	v_CurrentPage INTEGER DEFAULT NULL,
	v_PageSize INTEGER DEFAULT NULL,
	v_columnName TEXT DEFAULT NULL,
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_numDivisionID NUMERIC(9,0) DEFAULT 0,
	v_bitPartner BOOLEAN DEFAULT false,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_inttype SMALLINT DEFAULT NULL,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '',
	v_SearchText VARCHAR(300) DEFAULT '',
	v_SortChar CHAR(1) DEFAULT '0',
	v_tintDashboardReminderType SMALLINT DEFAULT 0,
	v_tintOppStatus SMALLINT DEFAULT 0, 
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  SMALLINT DEFAULT 0;
   v_numFormId  INTEGER;
   v_Nocolumns  SMALLINT DEFAULT 0;  

   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
   v_strColumns  TEXT;
   v_tintOrder  SMALLINT DEFAULT 0;                                                  
   v_vcFieldName  VARCHAR(50);                                                      
   v_vcListItemType  VARCHAR(3);                                                 
   v_vcListItemType1  VARCHAR(1);                                                     
   v_vcAssociatedControlType  VARCHAR(20);                                                      
   v_numListID  NUMERIC(9,0);                                                      
   v_vcDbColumnName  VARCHAR(200);                          
   v_WhereCondition TEXT DEFAULT '';                          
   v_vcLookBackTableName  VARCHAR(2000);                    
   v_bitCustom  BOOLEAN;              
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;   
   v_bitAllowEdit BOOLEAN;                 
   v_vcColumnName  VARCHAR(500);                  
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT '';   

   v_strShareRedordWith  VARCHAR(300);
   v_strExternalUser  TEXT DEFAULT '';
   v_strSql  TEXT DEFAULT '';                                       
                                                                                 
   v_vcCSOrigDbCOlumnName  VARCHAR(50) DEFAULT '';
   v_vcCSLookBackTableName  VARCHAR(50) DEFAULT '';
   v_vcCSAssociatedControlType  VARCHAR(50) DEFAULT '';

   v_CustomControlType  VARCHAR(10);      
   v_strFinal  TEXT;
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'^LIKE?','ilike','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'^LIKE?','ilike','gi');

   IF v_inttype = 1 then
	
      v_PageId := 2;
      v_numFormId := 38;
   ELSEIF v_inttype = 2
   then
	
      v_PageId := 6;
      v_numFormId := 40;
   end if;

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = v_numFormId
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = v_numFormId) TotalRows;
	
   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = v_numFormId AND numRelCntType = v_numFormId AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numFormId,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numFormId,1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = false;
      INSERT INTO
      tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      IF v_Nocolumns = 0 then
		
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         v_numFormId,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,v_numFormId,1,false,intColumnWidth
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = v_numFormId
         AND bitDefault = false
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID
         ORDER BY
         tintOrder ASC;
      end if;
      INSERT INTO
      tt_TEMPFORM
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      AND numRelCntType = v_numFormId
      UNION
      SELECT
      tintRow+1 AS tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      AND numRelCntType = v_numFormId
      ORDER BY
      tintOrder ASC;
   end if;
	
   v_strColumns := ' ADC.numContactId, DM.numDivisionID, COALESCE(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, COALESCE(Opp.numBusinessProcessID,0) AS numBusinessProcessID,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail ';
        
	
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
   tt_TEMPFORM    ORDER BY
   tintOrder asc LIMIT 1;            
                                                   
   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'Opp.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityRecurring' then
            v_Prefix := 'OPR.';
         end if;
         IF v_vcLookBackTableName = 'CompanyInfo' then
            v_Prefix := 'cmp.';
         end if;
         IF v_vcLookBackTableName = 'ProjectProgress' then
            v_Prefix := 'PP';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         IF v_vcAssociatedControlType = 'SelectBox' then
			
            IF v_vcDbColumnName = 'numStatus' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=true AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,COALESCE(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus ';
            end if;
            IF v_vcDbColumnName = 'numPartner' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(Opp.numPartner) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner';
            ELSEIF v_vcDbColumnName = 'numPartnerContact'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(Opp.numPartenerContact) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(A.vcFirstName,'' '',A.vcLastName) FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact';
            ELSEIF v_vcDbColumnName = 'tintSource'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0)::SMALLINT,Opp.numDomainID) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0)::SMALLINT,Opp.numDomainID) ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcDbColumnName = 'tintOppStatus'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(CASE COALESCE(Opp.tintOppStatus,0) WHEN 1 THEN ''Deal Won'' WHEN 2 THEN ''Deal Lost'' ELSE ''Open'' END)' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numContactID'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
			ELSEIF v_vcDbColumnName = 'numProjectID'
            then
				v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((select vcprojectname from projectsmaster where numproid=' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '),'''') "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'LI'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'BP'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',SPLM.Slp_Name' || ' "'	|| coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'SPLM.Slp_Name ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID ';
            ELSEIF v_vcListItemType = 'PP'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', CASE WHEN PP.numOppID >0 then COALESCE(PP.intTotalProgress,0)::VARCHAR ELSE GetListIemName(OPP.numPercentageComplete) END ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || '(CASE WHEN PP.numOppID >0 then CONCAT(COALESCE(PP.intTotalProgress,0),''%'') ELSE GetListIemName(OPP.numPercentageComplete) END) ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcListItemType = 'WI'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', Case when COALESCE(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || '(Case when COALESCE(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join WebAPI on WebAPI.WebApiId=COALESCE(OL.numWebApiId,0)
															left join Sites on COALESCE(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			 v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),'
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
			
            IF v_vcDbColumnName = 'vcCompactContactDetails' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,'''' AS vcCompactContactDetails';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || CASE
               WHEN v_vcDbColumnName = 'numAge' then 'EXTRACT(year FROM timezone(''utc'', now()))::INTEGER -EXTRACT(YEAR FROM bintDOB)::INTEGER'
               WHEN v_vcDbColumnName = 'monPAmount' then 'COALESCE((SELECT varCurrSymbol FROM Currency WHERE numCurrencyID = Opp.numCurrencyID),'''') || '' '' || CAST(CAST(monPAmount AS DECIMAL(20,5)) AS VARCHAR)'
               WHEN v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
               WHEN v_vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               END || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || CASE
               WHEN v_vcDbColumnName = 'numAge' then 'EXTRACT(year FROM timezone(''utc'', now()))::INTEGER -EXTRACT(YEAR FROM bintDOB)::INTEGER'
               WHEN v_vcDbColumnName = 'monPAmount' then 'COALESCE((SELECT varCurrSymbol FROM Currency WHERE numCurrencyID = Opp.numCurrencyID),'''') || '' '' || CAST(CAST(monPAmount AS DECIMAL(20,5)) AS VARCHAR)'
               WHEN v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
               WHEN v_vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               END || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',' ||  coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Label'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',' || CASE
            WHEN v_vcDbColumnName = 'CalAmount' THEN 'getdealamount(Opp.numOppId,timezone(''utc'', now()),0)'
            WHEN v_vcDbColumnName = 'vcShipStreet' THEN 'fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'vcBizDocsList' THEN 'COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''#^#'',vcBizDocID),''$^$'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId),'''')'
            else coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
            END || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || CASE
               WHEN v_vcDbColumnName = 'CalAmount' THEN 'getdealamount(Opp.numOppId,timezone(''utc'', now()),0)'
               WHEN v_vcDbColumnName = 'vcShipStreet' THEN 'fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'vcBizDocsList' THEN 'COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''#^#'',vcBizDocID),''$^$'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId),'''')'
               else coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               END || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('' + fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('' + fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         end if;
      Else
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
			
            v_strColumns := coalesce(v_strColumns,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
					on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',(CASE WHEN is_date(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value::TIMESTAMP,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') ELSE '''' END) "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid    ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
									on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid     ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=(CASE WHEN SWF_IsNumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value::NUMERIC ELSE 0 END)';
         ELSE
            v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValueOpp(',v_numFieldId,',opp.numOppID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
         IF LENGTH(v_SearchText) > 0 then
			
            v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || CONCAT('GetCustFldValueOpp(',v_numFieldId,',opp.numOppID) ILIKE ''%',v_SearchText,
            '%''');
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
      tt_TEMPFORM WHERE
      tintOrder > v_tintOrder -1   ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_tintOrder := 0;
      end if;
   END LOOP;                            
		
	 
   v_strShareRedordWith := ' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=3 AND SR.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
                                
   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,'::VARCHAR AND EA.numDivisionID=DM.numDivisionID))');
								             
   v_strSql := ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' || coalesce(v_WhereCondition,'');
                                     

	-------Change Row Color-------
   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   INSERT INTO
   tt_TEMPCOLORSCHEME
   SELECT
   DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType
   FROM
   DycFieldColorScheme DFCS
   JOIN
   DycFormField_Mapping DFFM
   ON
   DFCS.numFieldID = DFFM.numFieldID
   JOIN
   DycFieldMaster DFM
   ON
   DFM.numModuleID = DFFM.numModuleID
   AND DFM.numFieldID = DFFM.numFieldID
   WHERE
   DFCS.numDomainID = v_numDomainID
   AND DFFM.numFormID = v_numFormId
   AND DFCS.numFormID = v_numFormId
   AND coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then
	
      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType FROM tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      v_strColumns := coalesce(v_strColumns,'') || ',tCS.vcColorScheme';
   end if;

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      IF v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      IF v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      IF v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      IF v_vcCSLookBackTableName = 'OpportunityMaster' then
         v_Prefix := 'Opp.';
      end if;
	  IF v_vcCSAssociatedControlType = 'DateField' then
		
         v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN tt_TEMPCOLORSCHEME tCS ON CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now()) + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)
				 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now()) + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
		
         v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS VARCHAR)';
      end if;
   end if;      

   IF v_columnName ilike 'CFW.Cust%' then
	
      v_strSql := coalesce(v_strSql,'') || ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','') || ' ';
      SELECT fld_type INTO v_CustomControlType FROM CFW_Fld_Master CFWM where CFWM.Fld_id = REPLACE(v_columnName,'CFW.Cust','')::NUMERIC;
      IF v_CustomControlType = 'DateField' then
		
         v_columnName := 'CAST((CASE WHEN CFW.Fld_Value = ''0'' THEN NULL ELSE CFW.Fld_Value END) AS Date)';
      ELSE
         v_columnName := 'CFW.Fld_Value';
      end if;
   ELSEIF v_columnName ilike 'DCust%'
   then
	
      v_strSql := coalesce(v_strSql,'') || ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= ' || replace(v_columnName,'DCust','');
      v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails LstCF on numListItemID=(CASE WHEN SWF_IsNumeric(CFW.Fld_Value) THEN CFW.Fld_Value::NUMERIC ELSE 0 END)  ';
      v_columnName := 'LstCF.vcData';
   end if;
                                        
   IF v_bitPartner = true then
	
      v_strSql := coalesce(v_strSql,'') || ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId';
   end if;

   v_strSql := CONCAT(v_strSql,' WHERE Opp.numDomainID=',v_numDomainID,' AND Opp.tintOppType= ',
   v_OppType,' AND Opp.tintOppstatus=',coalesce(v_tintOppStatus,0),' ');                                                              

   IF v_SortChar <> '0' then
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName ILIKE ''' || coalesce(v_SortChar,'') || '%''';
   end if;
                                
   IF v_numDivisionID <> 0 then
	
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   end if;
	                                                                
   IF v_tintUserRightType = 1 then
	
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner = '
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ' or Opp.numAssignedTo= '
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')'
      || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end
      || ' or ' || coalesce(v_strShareRedordWith,'')
      || ' OR ' || coalesce(v_strExternalUser,'') || ')';
   ELSEIF v_tintUserRightType = 2
   then
	
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0 or Opp.numAssignedTo= '
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
   end if;           
	
	                                                  
   IF v_tintSortOrder = 1 then
	
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner = '
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ' or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')'
      || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end
      || ' or ' || coalesce(v_strShareRedordWith,'')
      || ' OR ' || coalesce(v_strExternalUser,'') || ')';
   ELSEIF v_tintSortOrder = 2
   then
	
      v_strSql := coalesce(v_strSql,'') || 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
      || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))'
      || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))' else '' end
      || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
   ELSEIF v_tintSortOrder = 4
   then
	
      v_strSql := coalesce(v_strSql,'') || ' AND CAST(Opp.bintCreatedDate AS DATE) > CAST(''' || CAST(v_dtLastDate + make_interval(days => -8) AS VARCHAR) || ''' AS DATE)' ;
   end if;
                                                 

   IF v_vcRegularSearchCriteria <> '' then
	
      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;

   IF v_vcCustomSearchCriteria <> '' then
	
      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;

   IF LENGTH(v_SearchQuery) > 0 then
	
      v_strSql := coalesce(v_strSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
   end if;

   IF coalesce(v_tintDashboardReminderType,0) = 4 then
	
      v_strSql := CONCAT(v_strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',
      v_numDomainID,'
																	AND OMInner.numDomainId=',
      v_numDomainID,' 
																	AND OMInner.tintOppType = 1
																	AND COALESCE(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 18
   then
	
      v_strSql := CONCAT(v_strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',
      v_numDomainID,'
																	AND OMInner.numDomainId=',
      v_numDomainID,' 
																	AND OMInner.tintOppType = 2
																	AND COALESCE(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ');
   end if;    
                              
	-- EXECUTE FINAL QUERY
   v_strFinal := CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',v_strColumns,v_strSql,' ORDER BY ',
   v_columnName,' ',v_columnSortOrder,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint,
   ' ROWS FETCH NEXT ',v_PageSize,' ROWS ONLY');

   RAISE NOTICE '%',v_strFinal;
   OPEN SWV_RefCur FOR EXECUTE v_strFinal; 

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;

	
   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   RETURN;
END; $$;


