-- Stored procedure definition script USP_ScheduledReportsGroup_UpdateDataCacheStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_UpdateDataCacheStatus(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_tintDataCacheStatus SMALLINT
	,v_vcExceptionMessage TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   ScheduledReportsGroup
   SET
   tintDataCacheStatus = v_tintDataCacheStatus,vcExceptionMessage = v_vcExceptionMessage
   WHERE
   numDomainID = v_numDomainID AND ID = v_numSRGID;
   RETURN;
END; $$;


