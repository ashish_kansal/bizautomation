-- Stored procedure definition script USP_GetPLSummary for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPLSummary(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  NUMERIC(19,4);
   v_PLOPENING  NUMERIC(19,4);
   v_PLCHARTID  NUMERIC(8,0);

--select * from view_journal where numDomainid=72

BEGIN
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
CAST(coalesce(COA.numOriginalOpeningBal,0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date < v_dtFromDate),cast(0 as NUMERIC(19,4))) AS NUMERIC(19,4)) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as CREDIT
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId AND
      (COA.vcAccountCode ilike '0103%' OR
   COA.vcAccountCode ilike '0104%');

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 coalesce(SUM(Opening),cast(0 as NUMERIC(19,4))) as Opening,
coalesce(Sum(Debit),cast(0 as NUMERIC(19,4))) as Debit,coalesce(Sum(Credit),cast(0 as NUMERIC(19,4))) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0103%' OR
   ATD.vcAccountCode ilike '0104%')
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;
--------------------------------------------------------
-- GETTING P&L VALUE

   v_CURRENTPL := CAST(0 AS NUMERIC(19,4));
   v_PLOPENING := CAST(0 AS NUMERIC(19,4));

   select   coalesce(SUM(Opening),cast(0 as NUMERIC(19,4)))+coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) INTO v_CURRENTPL FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0103','0104');

--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM
--#PLOutPut P WHERE 
--vcAccountCode IN ('0104')

   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;

   select   coalesce(numOriginalOpeningBal,0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date < v_dtFromDate),cast(0 as NUMERIC(19,4))) INTO v_PLOPENING FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;

   v_CURRENTPL := v_CURRENTPL+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ
   WHERE VJ.numDomainID = v_numDomainId AND
   VJ.numAccountId = v_PLCHARTID AND
   datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4)));

   v_CURRENTPL := v_CURRENTPL*(-1);
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING

-----------------------------------------------------------------

   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      Balance NUMERIC(19,4),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );

   INSERT INTO tt_PLSHOW
   SELECT *,Opening+Debit -Credit as Balance,
CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE P.vcAccountCode
   END AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   UNION
   SELECT *,Opening+Debit -Credit as Balance,
CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE O.vcAccountCode
   END AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, 2 as Type
   FROM tt_PLOUTPUT O

--COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode

   UNION
   select v_PLCHARTID,'Profit / (Loss) Opening :',0,'','051',0,0,0,v_PLOPENING ,'','Profit / (Loss) Opening :',3
   UNION
   Select v_PLCHARTID,'Profit / (Loss) Current :',0,'','052',0,0,0, v_CURRENTPL,'','Profit / (Loss) Current :',4
   UNION
   Select  v_PLCHARTID, 'Net Profit/(Loss) :',0,'','053',0,0,0, v_PLOPENING+v_CURRENTPL,'','Net Profit/(Loss) :',5;



open SWV_RefCur for select * from tt_PLSHOW A ORDER BY A.vcAccountCode;

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;

   RETURN;
END; $$;