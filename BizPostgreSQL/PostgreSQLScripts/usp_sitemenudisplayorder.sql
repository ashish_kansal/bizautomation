-- Stored procedure definition script USP_SiteMenuDisplayOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SiteMenuDisplayOrder(v_strItems TEXT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      UPDATE SiteMenu SET intDisplayOrder = X.intDisplayOrder
      FROM 
	  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numMenuID NUMERIC(9,0) PATH 'numMenuID',
					intDisplayOrder INTEGER PATH 'intDisplayOrder'
			) AS X 
      WHERE SiteMenu.numMenuID = X.numMenuID;
  
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_SubmitSalseReturnPayment]    Script Date: 01/22/2009 01:41:51 ******/



