-- Stored procedure definition script usp_OrganizationContactsCustomFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_OrganizationContactsCustomFields(v_numRefreshTimeInterval NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tempOrgContactTable  VARCHAR(100);
   v_vcColumnListNew  TEXT;                        
   v_vcColumnListNewOrg  TEXT;                        
   v_vcColumnListNewCont  TEXT;
   v_OrgAndContTempCustomTableColumns  TEXT;                        
   v_OrgTempCustomTableQuery  TEXT;                        
   v_ContTempCustomTableQuery  TEXT;
   v_vwOrgContactTableColumns  TEXT;                   
                
   v_vwOrgContactTableColumnsAndDataType  TEXT;                   
                
   v_OrgContCreateQuery1  TEXT;          
   v_OrgContCreateQuery2  TEXT;          
   v_OrgContTempCustomTableDataEntryQuery1st  TEXT;                 
   v_OrgContTempCustomTableDataEntryQuery2nd  TEXT;                  
   v_OrgContTempCustomTableDataEntryQuery3rd  TEXT;                   
   v_OrgContTempCustomTableDataEntryQuery4th  TEXT;
   SWV_ExecDyn  VARCHAR(5000);
BEGIN
   v_tempOrgContactTable := 'tt_ORGCONTACTCUSTOMFIELD CASCADE';                        
                  
   v_vcColumnListNewOrg := fn_OrgAndContCustomFieldsColumns(1::SMALLINT,1::SMALLINT);                        
 --PRINT 'ColumnListNewOrg: ' + @vcColumnListNewOrg                        
   v_vcColumnListNewCont := fn_OrgAndContCustomFieldsColumns(4::SMALLINT,1::SMALLINT);                        
 --PRINT 'ColumnListNewCont: ' + @vcColumnListNewCont                        
   IF v_vcColumnListNewOrg Is Not NULL AND v_vcColumnListNewCont Is Not NULL then
      v_vcColumnListNew := coalesce(v_vcColumnListNewOrg,'') ||  ', ' || coalesce(v_vcColumnListNewCont,'');
   ELSEIF v_vcColumnListNewOrg Is NULL AND v_vcColumnListNewCont Is Not NULL
   then
      v_vcColumnListNew := v_vcColumnListNewCont;
   ELSEIF v_vcColumnListNewCont Is NULL AND v_vcColumnListNewOrg Is Not NULL
   then
      v_vcColumnListNew := v_vcColumnListNewOrg;
   end if;          
                       
                        
 --PRINT @vcColumnListNew                        
                        
   IF NOT EXISTS(SELECT 'x' FROM INFORMATION_SCHEMA.COLUMNS WHERE   table_name = v_tempOrgContactTable) then
                          
   --Print 'Table To Be Created'                        
                   
    --EXEC ('SELECT * FROM ' + @tempOrgContactTable)                        
      v_OrgTempCustomTableQuery := fn_OrgAndContCustomFieldsColumns(1::SMALLINT,0::SMALLINT);
      v_ContTempCustomTableQuery := fn_OrgAndContCustomFieldsColumns(4::SMALLINT,0::SMALLINT);
      IF v_OrgTempCustomTableQuery Is Not NULL AND v_ContTempCustomTableQuery Is Not NULL then
         v_OrgAndContTempCustomTableColumns := coalesce(v_OrgTempCustomTableQuery,'') ||  ', ' || coalesce(v_ContTempCustomTableQuery,'');
      ELSEIF v_OrgTempCustomTableQuery Is NULL AND v_ContTempCustomTableQuery Is Not NULL
      then
         v_OrgAndContTempCustomTableColumns := v_ContTempCustomTableQuery;
      ELSEIF v_ContTempCustomTableQuery Is NULL AND v_OrgTempCustomTableQuery Is Not NULL
      then
         v_OrgAndContTempCustomTableColumns := v_OrgTempCustomTableQuery;
      end if;
      select   Coalesce(coalesce(v_vwOrgContactTableColumns,'') || ', ','') || column_name INTO v_vwOrgContactTableColumns FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name =(SELECT table_name FROM INFORMATION_SCHEMA.COLUMNS
         WHERE routine_name  = 'vw_OrgContactCustomReport'
         AND xType = 'V')
      AND colid > 3   ORDER BY colid;                 
   --PRINT @vwOrgContactTableColumns                
                
      select   Coalesce(coalesce(v_vwOrgContactTableColumnsAndDataType,'') || ', ','') || c.column_name || ' ' || t.name ||  CASE WHEN t.name <> 'tinyInt' Then
         CASE WHEN (c.prec IS NULL OR t.name = 'bit') THEN ''
         ELSE
            '(' || SUBSTR(Cast(c.prec As VARCHAR(30)),1,30) || CASE WHEN c.scale IS NULL THEN ')' ELSE ',' || SUBSTR(Cast(c.scale AS VARCHAR(30)),1,30) || ')' END
         END
      ELSE
         ''
      END INTO v_vwOrgContactTableColumnsAndDataType FROM INFORMATION_SCHEMA.COLUMNS c, (SELECT name, type, xtype, xusertype, usertype FROM systypes) t WHERE c.table_name =(SELECT table_name FROM INFORMATION_SCHEMA.COLUMNS
         WHERE routine_name  = 'vw_OrgContactCustomReport'
         AND xType = 'V')                 
   --AND t.type = c.type                
      AND t.xusertype = c.xusertype
      AND t.usertype = c.usertype
      AND colid > 3   ORDER BY colid;                 
   --PRINT @vwOrgContactTableColumnsAndDataType          
          
      v_OrgContCreateQuery1 := 'CREATE TABLE ' || coalesce(v_tempOrgContactTable,'') || '(numCompanyID Numeric, numDivisionId Numeric, numContactId Numeric, vcColumnList NVarchar(2000), CreatedDateTime DateTime, ' || coalesce(v_vwOrgContactTableColumnsAndDataType,'');
      v_OrgContCreateQuery2 := CASE WHEN NULLIF(v_OrgAndContTempCustomTableColumns,'') IS Not NULL THEN ', ' || coalesce(v_OrgAndContTempCustomTableColumns,'') ELSE '' END || ')';
      RAISE NOTICE '%',v_OrgContCreateQuery1;
      RAISE NOTICE '%',v_OrgContCreateQuery2;
      EXECUTE coalesce(v_OrgContCreateQuery1,'') || coalesce(v_OrgContCreateQuery2,'');
      v_OrgContTempCustomTableDataEntryQuery1st := 'INSERT INTO ' || coalesce(v_tempOrgContactTable,'') || ' (numCompanyID, numDivisionId, numContactId, vcColumnList, CreatedDateTime, ' || coalesce(v_vwOrgContactTableColumns,'') ||
      CASE WHEN (NULLIF(v_OrgTempCustomTableQuery,'') Is Not NULL) AND (NULLIF(v_ContTempCustomTableQuery,'') Is Not NULL) THEN
         ', ' || fn_OrgAndContCustomFieldsColumns(1::SMALLINT,1::SMALLINT) || ', ' || fn_OrgAndContCustomFieldsColumns(4::SMALLINT,1::SMALLINT) || ') '
      WHEN (NULLIF(v_OrgTempCustomTableQuery,'') Is NULL) AND (NULLIF(v_ContTempCustomTableQuery,'') Is Not NULL) THEN
         ', ' || fn_OrgAndContCustomFieldsColumns(4::SMALLINT,1::SMALLINT) || ') '
      WHEN (NULLIF(v_ContTempCustomTableQuery,'') Is NULL) AND (NULLIF(v_OrgTempCustomTableQuery,'') Is Not NULL) THEN
         ', ' || fn_OrgAndContCustomFieldsColumns(1::SMALLINT,1::SMALLINT) || ') '
      ELSE
         ') '
      END;
      v_OrgContTempCustomTableDataEntryQuery2nd := ' SELECT numCompanyId, numDivisionId, numContactId, ''' || CASE WHEN NULLIF(v_vcColumnListNew,'') IS NOT NULL THEN v_vcColumnListNew ELSE '' END || ''', TIMEZONE(''UTC'',now()), ' || coalesce(v_vwOrgContactTableColumns,'');
      IF (v_OrgTempCustomTableQuery Is Not NULL)  AND (v_ContTempCustomTableQuery Is Not NULL) then
         v_OrgContTempCustomTableDataEntryQuery3rd := ', ' || fn_OrgAndContCustomFieldsColumns(1::SMALLINT,2::SMALLINT)  || ', ' || fn_OrgAndContCustomFieldsColumns(4::SMALLINT,2::SMALLINT);
      ELSEIF (v_OrgTempCustomTableQuery Is NULL) AND (v_ContTempCustomTableQuery Is Not NULL)
      then
         v_OrgContTempCustomTableDataEntryQuery3rd := ', ' || fn_OrgAndContCustomFieldsColumns(4::SMALLINT,2::SMALLINT);
      ELSEIF (v_ContTempCustomTableQuery Is NULL) AND (v_OrgTempCustomTableQuery Is Not NULL)
      then
         v_OrgContTempCustomTableDataEntryQuery3rd := ', ' || fn_OrgAndContCustomFieldsColumns(1::SMALLINT,2::SMALLINT);
      ELSE
         v_OrgContTempCustomTableDataEntryQuery3rd := '';
      end if;
      v_OrgContTempCustomTableDataEntryQuery4th := ' FROM vw_OrgContactCustomReport ORDER BY numCompanyId, numDivisionId, numContactId';
      RAISE NOTICE '%',v_OrgContTempCustomTableDataEntryQuery1st;
      RAISE NOTICE '%',v_OrgContTempCustomTableDataEntryQuery2nd;
      RAISE NOTICE '%',v_OrgContTempCustomTableDataEntryQuery3rd;
      RAISE NOTICE '%',v_OrgContTempCustomTableDataEntryQuery4th;
      EXECUTE coalesce(v_OrgContTempCustomTableDataEntryQuery1st,'') || coalesce(v_OrgContTempCustomTableDataEntryQuery2nd,'') || coalesce(v_OrgContTempCustomTableDataEntryQuery3rd,'') || coalesce(v_OrgContTempCustomTableDataEntryQuery4th,'');
   ELSE
      IF (NOT EXISTS(SELECT  vcColumnList FROM tt_ORGCONTACTCUSTOMFIELD WHERE vcColumnList =  v_vcColumnListNew LIMIT 1))  OR ((SELECT  numCompanyID FROM tt_ORGCONTACTCUSTOMFIELD WHERE(EXTRACT(DAY FROM TIMEZONE('UTC',now()) -CreatedDateTime)*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -CreatedDateTime)*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -CreatedDateTime)) > v_numRefreshTimeInterval LIMIT 1) > 0) then
         SWV_ExecDyn := 'DROP TABLE IF EXISTS ' ||  coalesce(v_tempOrgContactTable,'');
         EXECUTE SWV_ExecDyn;
         SWV_ExecDyn := 'usp_OrganizationContactsCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
         EXECUTE SWV_ExecDyn;
      end if;
   end if;
   RETURN;
END; $$;


