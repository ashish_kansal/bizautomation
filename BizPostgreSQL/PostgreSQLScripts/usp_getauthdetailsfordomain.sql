CREATE OR REPLACE FUNCTION USP_GetAuthDetailsForDomain(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $BODY$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPAUTH CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAUTH AS
      SELECT   AGM.numDomainID ,
                AGM.numGroupID ,
                PM.numModuleID ,
                PM.numPageID ,
                0 AS intExportAllowed ,
                0 AS intPrintAllowed ,
                0 AS intViewAllowed ,
                0 AS intAddAllowed ,
                0 AS intUpdateAllowed ,
                0 AS intDeleteAllowed,
				PM.vcPageDesc,
				(SELECT MM.vcModuleName FROM ModuleMaster MM WHERE numModuleID = PM.numModuleID) AS vcModuleName,
				false AS bitCustomRelationship

      FROM AuthenticationGroupMaster AGM CROSS JOIN PageMaster PM
      WHERE AGM.numDomainID = v_numDomainID
      UNION
      SELECT
      AGM.numDomainID ,
    AGM.numGroupID ,
    32 ,
    TEMP.numListItemID,
    0 AS intExportAllowed ,
    0 AS intPrintAllowed ,
    0 AS intViewAllowed ,
    0 AS intAddAllowed ,
    0 AS intUpdateAllowed ,
    0 AS intDeleteAllowed,
	TEMP.vcData || ' List' AS vcPageDesc,
	(SELECT MM.vcModuleName FROM ModuleMaster MM WHERE numModuleID = 32) AS vcModuleName,
	true AS bitCustomRelationship
      FROM
      AuthenticationGroupMaster AGM
      CROSS JOIN(SELECT
         numListItemID,
			vcData
         FROM
         Listdetails
         WHERE
         numListID = 5 AND
			(coalesce(constFlag,false) = true OR (numDomainid = v_numDomainID AND coalesce(bitDelete,false) = false)) AND
         numListItemID <> 46) AS TEMP
      WHERE
      AGM.numDomainID = v_numDomainID;

   UPDATE tt_TEMPAUTH T SET
   intExportAllowed = coalesce(GA.intExportAllowed,0),intPrintAllowed = coalesce(GA.intPrintAllowed,0),
   intViewAllowed = coalesce(GA.intViewAllowed,0),
   intAddAllowed = coalesce(GA.intAddAllowed,0),intUpdateAllowed = coalesce(GA.intUpdateAllowed,0),
   intDeleteAllowed = coalesce(GA.intDeleteAllowed,0)
   from GroupAuthorization GA WHERE(GA.numPageID = T.numPageID AND GA.numDomainID = T.numDomainID AND GA.numGroupID = T.numGroupID AND GA.numModuleID = T.numModuleID) AND T.bitCustomRelationship = false;

   UPDATE tt_TEMPAUTH T SET
   intExportAllowed = coalesce(GA.intExportAllowed,0),intPrintAllowed = coalesce(GA.intPrintAllowed,0),
   intViewAllowed = coalesce(GA.intViewAllowed,3),
   intAddAllowed = coalesce(GA.intAddAllowed,0),intUpdateAllowed = coalesce(GA.intUpdateAllowed,0),
   intDeleteAllowed = coalesce(GA.intDeleteAllowed,0)
   from GroupAuthorization GA WHERE(GA.numPageID = T.numPageID AND GA.numDomainID = T.numDomainID AND GA.numGroupID = T.numGroupID AND GA.numModuleID = T.numModuleID) AND T.bitCustomRelationship = true;

 
   OPEN SWV_RefCur FOR SELECT * FROM tt_TEMPAUTH;
END;
$BODY$;
