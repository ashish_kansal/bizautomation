-- Stored procedure definition script USP_GetBillDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE OR REPLACE FUNCTION USP_GetBillDetails(v_numBillID NUMERIC(18,0),
    v_numDomainID NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCurrencyID  NUMERIC(18,0);
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
select   numCurrencyID INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainID;

   open SWV_RefCur for
   SELECT  BH.numBillID,
            numDivisionID,
            fn_GetComapnyName(numDivisionID) AS vcCompanyName,
            BH.numAccountID,
            BH.dtBillDate,
            BH.numTermsID,
            BH.monAmountDue,
            BH.dtDueDate,
            BH.vcMemo,
            BH.vcReference,
            BH.monAmtPaid,
            BH.bitIsPaid,
            BH.numDomainId,
            BH.numCreatedBy,
            BH.dtCreatedDate,
            BH.numModifiedBy,
            BH.dtModifiedDate,coalesce(GJD.numTransactionId,0) AS numTransactionId,
            coalesce(PO.numProId,0) AS numProjectID,coalesce(PO.numStageID,0) AS numStageID,
            (SELECT vcTerms FROM BillingTerms WHERE numTermsID = BH.numTermsID AND numDomainID = BH.numDomainId)  AS vcTermName,
			coalesce(BH.numCurrencyID,v_numCurrencyID) AS numCurrencyID,
			coalesce(BH.fltExchangeRate,1) AS fltExchangeRate
   FROM    BillHeader BH
   LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType = 3
   AND GJD.numReferenceID = BH.numBillID
   LEFT OUTER JOIN ProjectsOpportunities PO ON PO.numBillId = BH.numBillID
   WHERE   BH.numBillID = v_numBillID
   AND BH.numDomainId = v_numDomainID;
    
    
   open SWV_RefCur2 for
   SELECT  BD.numBillDetailID,
            BD.numExpenseAccountID,
            BD.monAmount,
            BD.vcDescription,
            BD.numProjectID,
            BD.numClassID,
            BD.numCampaignID,
            BD.numBillID
			,coalesce(BD.numOppItemID,0) AS numOppItemID
			,coalesce(BD.numOppID,0) AS numOppID
			,coalesce(GJD.numTransactionId,0) AS numTransactionId
   FROM    BillDetails BD
   INNER JOIN BillHeader BH ON BD.numBillID = BH.numBillID
   LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType = 4 AND GJD.numReferenceID = BD.numBillDetailID
   WHERE   BH.numBillID = v_numBillID
   AND BH.numDomainId = v_numDomainID;
   RETURN;
END; $$;
            


