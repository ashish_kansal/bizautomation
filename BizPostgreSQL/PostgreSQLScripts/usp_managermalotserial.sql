-- Stored procedure definition script USP_ManageRMALotSerial for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRMALotSerial(v_numDomainId NUMERIC(9,0) DEFAULT 0,  
    v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,  
    v_numWareHouseItemID NUMERIC(9,0) DEFAULT 0,
    v_numReturnItemID NUMERIC(9,0) DEFAULT 0,
    v_tintMode SMALLINT DEFAULT NULL,
    v_strItems VARCHAR(1000) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_posComma  INTEGER;
   v_strKeyVal  VARCHAR(20);

   v_minROWNUMBER  INTEGER;
   v_maxROWNUMBER  INTEGER;
   v_numWareHouseItmsDTLID  NUMERIC(9,0);
   v_numQty  NUMERIC(9,0);
   v_posBStart  INTEGER;
   v_posBEnd  INTEGER;
   v_strQty  VARCHAR(20);
   v_strName  VARCHAR(20);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempLotSerial_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPLOTSERIAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPLOTSERIAL
   (
      ROWNUMBER INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseItemDTLID NUMERIC(18,0),
      numQty INTEGER
   );                       
  
   v_strItems := RTRIM(v_strItems);

   IF SUBSTR(v_strItems,length(v_strItems) -1+1) != ',' then
      v_strItems := coalesce(v_strItems,'') || ',';
   end if;

   v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);

   WHILE v_posComma > 1 LOOP
      v_strKeyVal := LTRIM(RTRIM(SUBSTR(v_strItems,1,v_posComma -1)));
      v_posBStart := coalesce(POSITION(substring(v_strKeyVal from E'\\(') IN v_strKeyVal),0);
      v_posBEnd := coalesce(POSITION(substring(v_strKeyVal from E'\\)') IN v_strKeyVal),0);
      IF(v_posBStart > 1 AND v_posBEnd > 1) then
		
         v_strName := LTRIM(RTRIM(SUBSTR(v_strKeyVal,1,v_posBStart -1)));
         v_strQty := LTRIM(RTRIM(SUBSTR(v_strKeyVal,v_posBStart+1,LENGTH(v_strKeyVal) -v_posBStart -1)));
      ELSE
         v_strName := v_strKeyVal;
         v_strQty := CAST(1 AS VARCHAR(20));
      end if;
      INSERT INTO tt_TEMPLOTSERIAL(numWarehouseItemDTLID,
			numQty)
		VALUES(CAST(v_strName AS NUMERIC(18,0)),
			case when v_strQty = '' then 0 else v_strQty:: INTEGER end);
		 
      v_strItems := SUBSTR(v_strItems,v_posComma+1,LENGTH(v_strItems) -v_posComma);
      v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);
   END LOOP;

   DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID = v_numReturnHeaderID AND numReturnItemID = v_numReturnItemID;

   select   MIN(ROWNUMBER), MAX(ROWNUMBER) INTO v_minROWNUMBER,v_maxROWNUMBER FROM tt_TEMPLOTSERIAL;

   WHILE  v_minROWNUMBER <= v_maxROWNUMBER LOOP
      select   numWarehouseItemDTLID, numQty INTO v_numWareHouseItmsDTLID,v_numQty FROM tt_TEMPLOTSERIAL X WHERE X.ROWNUMBER = v_minROWNUMBER;
      INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,
			numReturnHeaderID,
			numReturnItemID,
			numWarehouseItmsID,
			numQty)
      SELECT
      v_numWareHouseItmsDTLID,
			v_numReturnHeaderID,
			v_numReturnItemID,
			v_numWareHouseItemID,
			v_numQty;
      v_minROWNUMBER := v_minROWNUMBER::bigint+1;
   END LOOP;	
				
   DROP TABLE IF EXISTS tt_TEMPLOTSERIAL CASCADE;
   RETURN;
END; $$;


