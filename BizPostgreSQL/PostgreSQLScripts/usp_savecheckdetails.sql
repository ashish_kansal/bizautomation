-- Stored procedure definition script USP_SaveCheckDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCheckDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                       
v_numCheckHeaderID NUMERIC(9,0) DEFAULT 0,                                                                                          
v_strRow TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc3  INTEGER;
BEGIN
	If SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then
		DELETE FROM 
			CheckDetails 
		WHERE 
			numCheckHeaderID = v_numCheckHeaderID
			AND numCheckDetailID NOT IN (SELECT 
											X.numCheckDetailID
										FROM 
										XMLTABLE
										(
											'NewDataSet/Table1'
											PASSING 
												CAST(v_strRow AS XML)
											COLUMNS
												id FOR ORDINALITY,
												numCheckDetailID NUMERIC(18,0) PATH 'numCheckDetailID'
										) AS X WHERE COALESCE(X.numCheckDetailID,0) > 0); 
			                                                           
		--Update transactions
		UPDATE 
			CheckDetails  
		Set 
			numChartAcntId = X.numChartAcntId
			,monAmount = X.monAmount
			,vcDescription = X.vcDescription
			,numCustomerId = X.numCustomerId
			,numClassID = X.numClassID
			,numProjectID = X.numProjectID
		From
		(
			SELECT 
				* 
			FROM 
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numCheckDetailID NUMERIC(18,0) PATH 'numCheckDetailID',
					numChartAcntId NUMERIC(18,0) PATH 'numChartAcntId',
					monAmount DECIMAL(20,5) PATH 'monAmount',
					vcDescription TEXT PATH 'vcDescription',
					numCustomerId NUMERIC(18,0) PATH 'numCustomerId',
					numClassID NUMERIC(18,0) PATH 'numClassID',
					numProjectID NUMERIC(18,0) PATH 'numProjectID'
			) AS T
			WHERE
				COALESCE(T.numCheckDetailID,0) > 0
		) X
		Where 
			CheckDetails.numCheckDetailID = X.numCheckDetailID 
			AND numCheckHeaderID = v_numCheckHeaderID;                                                                                              
			               
		--Insert New transactions if any
		Insert Into CheckDetails(numCheckHeaderID,numChartAcntId,monAmount,vcDescription,numCustomerId,numClassID,numProjectID)
		Select 
			v_numCheckHeaderID
			,numChartAcntId
			,monAmount
			,vcDescription
			,numCustomerId
			,numClassID
			,numProjectID 
		from
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numCheckDetailID NUMERIC(18,0) PATH 'numCheckDetailID',
				numChartAcntId NUMERIC(18,0) PATH 'numChartAcntId',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				vcDescription TEXT PATH 'vcDescription',
				numCustomerId NUMERIC(18,0) PATH 'numCustomerId',
				numClassID NUMERIC(18,0) PATH 'numClassID',
				numProjectID NUMERIC(18,0) PATH 'numProjectID'
		) AS T
		WHERE
			COALESCE(T.numCheckDetailID,0) = 0;
   end if;
   RETURN;
END; $$;                                   
	                                                                                      
/****** Object:  StoredProcedure [dbo].[USP_SaveFixedAssetDetails]    Script Date: 07/26/2008 16:21:02 ******/



