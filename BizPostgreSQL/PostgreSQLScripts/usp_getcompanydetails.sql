-- Stored procedure definition script usp_GetCompanydetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCompanydetails(               
--              
v_numcntid NUMERIC DEFAULT 0,        
 v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_numcntid > 0 then

      open SWV_RefCur for
      SELECT CMP.numCompanyId, CMP.vcCompanyName,
  fn_GetListItemName(DM.numStatusID) as status,              
--  (select vctername from TerritoryMaster where  TerritoryMaster.numterid=DM.numTerID) as Territory, 
'' as Territory,
  fn_GetListItemName(CMP.numCompanyRating) as Rating,
  fn_GetListItemName(CMP.numCompanyIndustry) as Industry,
  fn_GetListItemName(CMP.numCompanyCredit) as Credit,
  fn_GetListItemName(CMP.numCompanyType) as Type,
  (select vcGrpName from Groups where Groups.numGrpId = DM.numGrpId) AS grpname,
  DM.vcDivisionName,
   AD1.vcStreet AS vcBillstreet,
   AD1.vcCity AS vcBillCity,
   AD1.numState AS vcBilState,
   AD1.vcPostalCode AS vcBillPostCode,
   AD1.numCountry AS vcBillCountry,
   AD2.vcStreet AS vcShipStreet,
   AD2.vcCity AS vcShipCity,
   AD2.numState AS vcShipState,
   AD2.vcPostalCode AS vcShipPostCode,
   AD2.numCountry AS vcShipCountry,        
--  DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,               
--  DM.vcBillPostCode, DM.vcBillCountry, DM.bitSameAddr,               
--  DM.vcShipStreet, DM.vcShipCity, DM.vcShipState,
--DM.vcShipPostCode, DM.vcShipCountry,
  CMP.vcWebSite,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,
  DM.numTerID,
  CMP.numCompanyIndustry, CMP.numCompanyCredit,
  CMP.txtComments, coalesce(CMP.vcWebSite,'-') as vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesId,
  fn_GetListItemName(CMP.vcProfile) as vcProfile,-- AI.numDivisionID,              
  DM.numGrpId, CMP.vcHow,CMP.numCompanyId,DM.numDivisionID,DM.tintCRMType,AI.numContactId
      FROM  CompanyInfo CMP INNER JOIN DivisionMaster DM ON CMP.numCompanyId = DM.numCompanyID
      LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
      AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN AddressDetails AD2 ON AD1.numDomainID = DM.numDomainID
      AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      LEFT JOIN AdditionalContactsInformation AI ON DM.numDivisionID = AI.numDivisionId
      WHERE
      numContactId = v_numcntid;
   end if;
        
   if  v_numDivisionID > 0 then

      open SWV_RefCur for
      SELECT CMP.numCompanyId, CMP.vcCompanyName,
  fn_GetListItemName(DM.numStatusID) as status,
'' as Territory,
  fn_GetListItemName(CMP.numCompanyRating) as Rating,
  fn_GetListItemName(CMP.numCompanyIndustry) as Industry,
  fn_GetListItemName(CMP.numCompanyCredit) as Credit,
  fn_GetListItemName(CMP.numCompanyType) as Type,
  (select vcGrpName from Groups where Groups.numGrpId = DM.numGrpId) AS grpname,
  DM.vcDivisionName, 
--  DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,               
--  DM.vcBillPostCode, DM.vcBillCountry, DM.bitSameAddr,               
--  DM.vcShipStreet, DM.vcShipCity, DM.vcShipState, 
--  DM.vcShipPostCode, DM.vcShipCountry, 
  CMP.vcWebSite,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,
    DM.numTerID,
  CMP.numCompanyIndustry, CMP.numCompanyCredit,
  CMP.txtComments,coalesce(CMP.vcWebSite,'-') as vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesId,
  fn_GetListItemName(CMP.vcProfile) as vcProfile,-- AI.numDivisionID,              
  DM.numGrpId, CMP.vcHow,CMP.numCompanyId,DM.numDivisionID,DM.tintCRMType   , AI.numContactId,CMP.numCompanyType
      FROM  CompanyInfo CMP
      INNER JOIN DivisionMaster DM ON CMP.numCompanyId = DM.numCompanyID
      LEFT JOIN AdditionalContactsInformation AI ON DM.numDivisionID = AI.numDivisionId
      WHERE
      DM.numDivisionID = v_numDivisionID and coalesce(AI.bitPrimaryContact,false) = true;
   end if;
   RETURN;
END; $$;


