-- Stored procedure definition script USP_ProcurementBudgetForOpenDeal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProcurementBudgetForOpenDeal(v_numItemCode NUMERIC(9,0) DEFAULT 0,    
 v_numDomainId NUMERIC(9,0) DEFAULT 0,    
 INOUT v_bitPurchaseDeal BOOLEAN  DEFAULT NULL,    
 INOUT v_monmonthlyAmt DOUBLE PRECISION DEFAULT 0 ,    
 INOUT v_monTotalYearlyAmt DOUBLE PRECISION DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppMonthAmount  DECIMAL(20,5);    
   v_OppAmount  DECIMAL(20,5);    
   v_BudgetAmt  DECIMAL(20,5);    
   v_TotalBudgetAmt  DECIMAL(20,5);    
   v_ItemGroupId  NUMERIC(9,0);    
 --Select @numProcurementBudgetId=numProcurementBudgetId,@bitPurchaseDeal=bitPurchaseDeal From ProcurementBudgetMaster Where  intFiscalYear=year(getutcdate()) And numDomainId=@numDomainId  
BEGIN
   select   bitPurchaseDeal INTO v_bitPurchaseDeal From ProcurementBudgetMaster Where  numDomainID = v_numDomainId;    --intFiscalYear=year(getutcdate()) And
   select   coalesce(numItemGroup,0) INTO v_ItemGroupId From Item Where numItemCode = v_numItemCode And numDomainID = v_numDomainId;    
   If v_bitPurchaseDeal = true then
 
      select   sum(monPAmount) INTO v_OppMonthAmount From OpportunityMaster Opp
      inner join OpportunityItems OI on Opp.numOppId = OI.numOppId Where Opp.tintopptype = 2 And Opp.tintoppstatus = 1 And OI.numItemCode = v_numItemCode And Opp.numDomainId = v_numDomainId
      And EXTRACT(month FROM Opp.bintAccountClosingDate) = EXTRACT(month FROM TIMEZONE('UTC',now())) and EXTRACT(Year FROM Opp.bintAccountClosingDate) = EXTRACT(Year FROM TIMEZONE('UTC',now()));
      select   sum(monPAmount) INTO v_OppAmount From OpportunityMaster Opp
      inner join OpportunityItems OI on Opp.numOppId = OI.numOppId Where Opp.tintopptype = 2 And Opp.tintoppstatus = 1 And OI.numItemCode = v_numItemCode And Opp.numDomainId = v_numDomainId
      And EXTRACT(month FROM Opp.bintAccountClosingDate) <= EXTRACT(month FROM TIMEZONE('UTC',now())) and EXTRACT(Year FROM Opp.bintAccountClosingDate) = EXTRACT(Year FROM TIMEZONE('UTC',now()));
      select   coalesce(monAmount,0) INTO v_BudgetAmt From ProcurementBudgetMaster PBM
      inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId Where PBD.intYear = EXTRACT(year FROM TIMEZONE('UTC',now())) And PBD.tintMonth = EXTRACT(month FROM TIMEZONE('UTC',now())) And PBD.numItemGroupId = v_ItemGroupId And PBM.numDomainID = v_numDomainId;
      select   sum(coalesce(monAmount,0)) INTO v_TotalBudgetAmt From ProcurementBudgetMaster PBM
      inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId Where PBD.intYear = EXTRACT(year FROM TIMEZONE('UTC',now())) And PBD.numItemGroupId = v_ItemGroupId And PBM.numDomainID = v_numDomainId;

   
    
----     select @BudgetAmt=(case when  month(getutcdate())=1 then monJan       
----  when month(getutcdate())=2 then monFeb    
----  when month(getutcdate())=3 then monMar    
----  when month(getutcdate())=4 then monApr    
----  when month(getutcdate())=5 then monMay    
----  when month(getutcdate())=6 then monJune    
----  when month(getutcdate())=7 then monJuly    
----  when month(getutcdate())=8 then monAug    
----  when month(getutcdate())=9 then monSep    
----  when month(getutcdate())=10 then monOct    
----  when month(getutcdate())=11 then monNov    
----  when month(getutcdate())=12 then monDec    
----  End) ,@TotalBudgetAmt=monTotal From ProcurementbudgetMaster PBM    
----  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId    
----  Where PBM.intFiscalYear=year(getutcdate()) And PBD.numItemGroupId=@ItemGroupId    
      
      v_monmonthlyAmt := coalesce(v_BudgetAmt,0) -coalesce(v_OppMonthAmount,0);
      RAISE NOTICE '@monmonthlyAmt==%',SUBSTR(CAST(v_monmonthlyAmt AS VARCHAR(10)),1,10);    
--  print  @TotalBudgetAmt    
      v_monTotalYearlyAmt := coalesce(v_TotalBudgetAmt,0) -coalesce(v_OppAmount,0);
      RAISE NOTICE '@monTotalYearlyAmt==%',SUBSTR(CAST(v_monTotalYearlyAmt AS VARCHAR(10)),1,10);
      v_bitPurchaseDeal := v_bitPurchaseDeal;
   end if;
   RETURN;
END; $$;


