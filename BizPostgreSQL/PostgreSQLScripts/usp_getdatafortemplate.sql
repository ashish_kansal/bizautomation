-- Stored procedure definition script usp_getDataForTemplate for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDataForTemplate(v_intType INTEGER,
v_numid NUMERIC
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from vwContactDetails where numContactID = v_numid;
END; $$;












