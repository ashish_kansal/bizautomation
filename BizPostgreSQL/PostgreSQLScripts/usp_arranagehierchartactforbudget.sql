-- Stored procedure definition script USP_ArranageHierChartActForBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Created By Siva       
    
CREATE OR REPLACE FUNCTION USP_ArranageHierChartActForBudget(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,                      
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountId  NUMERIC(9,0);
   v_vcCatgyName  VARCHAR(100);
   v_numAcntType  NUMERIC(9,0);
   v_numParentChartAcntId  NUMERIC(9,0);
BEGIN
   SELECT vcCatgyName INTO v_vcCatgyName FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId;             
   SELECT coalesce(numAcntType,0) INTO v_numAcntType FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId;        
   SELECT numParntAcntId INTO v_numParentChartAcntId FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId;        
                   
   insert into HierChartOfActForBudget(numChartOfAcntID,numParentChartAcntId,numDomainID,vcCategoryName,numAcntType)
 values(v_numParntAcntId,v_numParentChartAcntId,v_numDomainID,REPEAT(' ', (v_NESTLEVEL -3)*4) || coalesce(v_vcCatgyName, ''),v_numAcntType);       
                  
 
   SELECT MIN(numAcntId) INTO v_numAccountId FROM HierarchyArrangeDetails WHERE numParentAcntId = v_numParntAcntId;                      
                      
   WHILE v_numAccountId IS NOT NULL LOOP
      PERFORM USP_ArranageHierChartActForBudget(v_numAccountId,v_numDomainID);
      SELECT MIN(numAcntId) INTO v_numAccountId FROM HierarchyArrangeDetails WHERE numParentAcntId = v_numParntAcntId AND numAcntId > v_numAccountId;
   END LOOP;
   RETURN;                 
 --Delete From HierarchyArrangeDetails           
END; $$;


