-- Stored procedure definition script USP_ManageBizDocSummary for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageBizDocSummary(v_numDomainID NUMERIC(9,0),
v_numFormID NUMERIC(9,0),
v_numBizDocID NUMERIC(9,0),
v_strFormID VARCHAR(1000),
v_numBizDocTempID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;                            
   v_strPosition  VARCHAR(1000);
BEGIN
   delete from DycFrmConfigBizDocsSumm where numFormID = v_numFormID and  numBizDocID = v_numBizDocID and numDomainID = v_numDomainID and coalesce(numBizDocTempID,0) = v_numBizDocTempID;
                                     
   while coalesce(POSITION(substring(v_strFormID from E'\\,') IN v_strFormID),0) <> 0 LOOP -- Begin for While Loop                            
      v_separator_position := coalesce(POSITION(substring(v_strFormID from E'\\,') IN v_strFormID),0);
      v_strPosition := SUBSTR(v_strFormID,1,v_separator_position -1);
      v_strFormID := OVERLAY(v_strFormID placing '' from 1 for v_separator_position);
      insert into DycFrmConfigBizDocsSumm(numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
     values(v_numFormID,v_numBizDocID,CAST(v_strPosition AS NUMERIC),v_numDomainID,v_numBizDocTempID);
   END LOOP;  

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numFormID NUMERIC(18,0) NOT NULL,
      numBizDocID NUMERIC(18,0) NOT NULL,
      numFieldID NUMERIC(18,0) NOT NULL,
      numDomainID NUMERIC(18,0) NOT NULL,
      numBizDocTempID NUMERIC(18,0) 
   ); 

   insert into tt_TEMP
   (
		numFormID,
      numBizDocID,
      numFieldID,
      numDomainID,
      numBizDocTempID
   )
   select * from DycFrmConfigBizDocsSumm Ds where numBizDocID = v_numBizDocID and Ds.numFormID = v_numFormID
   and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID;




open SWV_RefCur for select Ds.numFieldID as numFormFieldId,coalesce(DFFM.vcFieldName,DFM.vcFieldName)  as vcFormFieldName
   from DycFormField_Mapping DFFM
   JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
   join tt_TEMP Ds on Ds.numFieldID = DFFM.numFieldID
   where numBizDocID = v_numBizDocID and Ds.numFormID = v_numFormID and (DFFM.numFormID = v_numFormID or DFFM.numDomainID is null)
   and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID order by Ds.ID;
END; $$;












