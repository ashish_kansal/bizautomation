-- Stored procedure definition script USP_GetMailRefreshToken for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMailRefreshToken(v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numDomainID,0) > 0 AND coalesce(v_numUserCntID,0) > 0 then
	
      open SWV_RefCur for
      SELECT
      tintMailProvider
			,vcMailAccessToken
			,vcMailRefreshToken
			,vcPSMTPUserName
			,(CASE WHEN(EXTRACT(DAY FROM dtTokenExpiration -TIMEZONE('UTC',now()))*60*24+EXTRACT(HOUR FROM dtTokenExpiration -TIMEZONE('UTC',now()))*60+EXTRACT(MINUTE FROM dtTokenExpiration -TIMEZONE('UTC',now()))) > 5 THEN false ELSE true END) AS bitExpired
      FROM
      UserMaster
      WHERE
      numDomainID = v_numDomainID
      AND numUserDetailId = v_numUserCntID;
   ELSE
      open SWV_RefCur for
      SELECT
      tintMailProvider
			,vcMailAccessToken
			,vcMailRefreshToken
			,vcPSMTPUserName
			,(CASE WHEN(EXTRACT(DAY FROM dtTokenExpiration -TIMEZONE('UTC',now()))*60*24+EXTRACT(HOUR FROM dtTokenExpiration -TIMEZONE('UTC',now()))*60+EXTRACT(MINUTE FROM dtTokenExpiration -TIMEZONE('UTC',now()))) > 5 THEN false ELSE true END) AS bitExpired
      FROM
      Domain
      WHERE
      numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


