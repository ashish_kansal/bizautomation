-- Stored procedure definition script usp_DeleteSystemGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteSystemGroup(v_numGroupID INTEGER  
-- 
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM AuthenticationGroupMaster
   WHERE numGroupID = v_numGroupID;
   RETURN;
END; $$;


