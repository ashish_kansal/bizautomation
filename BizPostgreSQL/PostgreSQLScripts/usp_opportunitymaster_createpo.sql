-- Stored procedure definition script USP_OpportunityMaster_CreatePO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_CreatePO(INOUT v_numOppID NUMERIC(18,0) ,
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numUnitHour DOUBLE PRECISION,
	v_numWarehouseItemID NUMERIC(18,0),
	v_bitFromWorkOrder BOOLEAN,
	v_tintOppStatus SMALLINT,
	v_numOrderStatus NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ErrorMessage  VARCHAR(4000);
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
    
   v_tintMinOrderQty  INTEGER;
   v_numUnitPrice  DECIMAL(20,5);
   v_numContactID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numVendorID  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;    
   v_numCurrencyID  NUMERIC(18,0);
   v_vcPOppName  VARCHAR(1000) DEFAULT '';                         
   v_hDocItem  INTEGER;                                                                                                                            
   v_TotalAmount  DECIMAL(20,5);     
   v_numAssignedTo  NUMERIC(18,0);       
   v_dtEstimatedCloseDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());        
   v_bitBillingTerms  BOOLEAN;
   v_intBillingDays  INTEGER;
   v_bitInterestType  BOOLEAN;
   v_fltInterest  NUMERIC;
   v_intShippingCompany INTEGER;
   v_numDefaultShippingServiceID  NUMERIC(18,0);
   v_strItems  TEXT DEFAULT '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>';

   v_numNewOppID  NUMERIC(18,0) DEFAULT 0;
   v_USP_OppManage_vcPOppName VARCHAR(100);
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN

   BEGIN
      -- BEGIN TRAN
select   numVendorID INTO v_numDivisionID FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
      IF coalesce(v_numDivisionID,0) > 0 then
         select(CASE WHEN coalesce(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END), coalesce(numBillingDays,0), (CASE WHEN coalesce(tintInterestType,0) = 1 THEN 1 ELSE 0 END), coalesce(fltInterest,0), coalesce(numAssignedTo,0), coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0) INTO v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_numAssignedTo,
         v_intShippingCompany,v_numDefaultShippingServiceID FROM
         DivisionMaster WHERE
         numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
         select   numContactId INTO v_numContactID FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND bitPrimaryContact = true;
         IF v_numContactID IS NULL then
			
            select   numContactId INTO v_numContactID FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID    LIMIT 1;
         end if;
         select   coalesce(intMinQty,0) INTO v_tintMinOrderQty FROm Vendor WHERE numVendorID = v_numDivisionID AND numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
         v_numUnitHour := v_numUnitHour+v_tintMinOrderQty::bigint;
         v_numUnitHour := CASE WHEN v_numUnitHour = 0 THEN 1 ELSE v_numUnitHour END;
         v_numUnitPrice := fn_FindVendorCost(v_numItemCode,v_numDivisionID,v_numDomainID,v_numUnitHour);
         select   CONCAT(v_strItems,'<Item><Op_Flag>1</Op_Flag>','<numoppitemtCode>',1,'</numoppitemtCode>',
         '<numItemCode>',numItemCode,'</numItemCode>','<numUnitHour>',
         v_numUnitHour,'</numUnitHour>','<monPrice>',v_numUnitPrice,'</monPrice>',
         '<monTotAmount>',(v_numUnitHour*v_numUnitPrice),'</monTotAmount>',
         '<vcItemDesc>',txtItemDesc,'</vcItemDesc>','<numWarehouseItmsID>',
         v_numWarehouseItemID,'</numWarehouseItmsID>','<ItemType>',charItemType,
         '</ItemType>','<DropShip>',0,'</DropShip>','<bitDiscountType>',1,'</bitDiscountType>',
         '<fltDiscount>',0,'</fltDiscount>','<monTotAmtBefDiscount>',
         (v_numUnitHour*v_numUnitPrice),'</monTotAmtBefDiscount>','<vcItemName>',
         vcItemName,'</vcItemName>','<numUOM>',coalesce(numBaseUnit,0),
         '</numUOM>','<bitWorkOrder>0</bitWorkOrder>','<numVendorWareHouse>0</numVendorWareHouse>',
         '<numShipmentMethod>0</numShipmentMethod>','<numSOVendorId>0</numSOVendorId>',
         '<numProjectID>0</numProjectID>','<numProjectStageID>0</numProjectStageID>',
         '<numToWarehouseItemID>0</numToWarehouseItemID>',
         '<Attributes></Attributes>','<AttributeIDs></AttributeIDs>',
         '<vcSKU>',vcSKU,'</vcSKU>','<bitItemPriceApprovalRequired>0</bitItemPriceApprovalRequired>',
         '<numPromotionID>0</numPromotionID>','<bitPromotionTriggered>0</bitPromotionTriggered>',
         '<vcPromotionDetail></vcPromotionDetail>',
         '<numSortOrder>',1,'</numSortOrder>') INTO v_strItems FROM
         Item WHERE
         numItemCode = v_numItemCode;
         v_strItems := CONCAT(v_strItems,'</Item>');
         v_strItems := CONCAT(v_strItems,'</NewDataSet>');
         v_USP_OppManage_vcPOppName := '';

		 SELECT * INTO v_numNewOppID,v_USP_OppManage_vcPOppName FROM USP_OppManage (v_numOppID := v_numNewOppID,v_numContactId := v_numContactID,v_numDivisionId := v_numDivisionID,v_tintSource := 0,v_vcPOppName := v_USP_OppManage_vcPOppName,
									v_Comments := '',v_bitPublicFlag := false,v_numUserCntID := v_numUserCntID,v_monPAmount := 0,v_numAssignedTo := v_numAssignedTo,
									v_numDomainID := v_numDomainID,v_strItems := v_strItems,v_strMilestone := '',v_dtEstimatedCloseDate := v_dtEstimatedCloseDate,v_CampaignID := 0,
									v_lngPConclAnalysis := 0,v_tintOppType := 2::SMALLINT,v_tintActive := 0::SMALLINT,v_numSalesOrPurType := 0,
									v_numRecurringId := 0,v_numCurrencyID := v_numCurrencyID,v_DealStatus := v_tintOppStatus::SMALLINT,v_numStatus := v_tintOppStatus,v_vcOppRefOrderNo := '',
									v_numBillAddressId := 0,v_numShipAddressId := 0,v_bitStockTransfer := false,v_WebApiId := 0,
									v_tintSourceType := 0::SMALLINT,v_bitDiscountType := false,v_fltDiscount := 0,v_bitBillingTerms := v_bitBillingTerms,
									v_intBillingDays := v_intBillingDays,v_bitInterestType := v_bitInterestType,v_fltInterest := v_fltInterest,v_tintTaxOperator := 0::SMALLINT,
									v_numDiscountAcntType := 0,v_vcWebApiOrderNo := '',v_vcCouponCode := '',v_vcMarketplaceOrderID := '',
									v_vcMarketplaceOrderDate := NULL,v_vcMarketplaceOrderReportId := '',v_numPercentageComplete := 0,
									v_bitUseShippersAccountNo := false,v_bitUseMarkupShippingRate := false,
									v_numMarkupShippingRate := 0,v_intUsedShippingCompany := v_intShippingCompany,v_numShipmentMethod := 0,v_dtReleaseDate := NULL,v_numPartner := 0, v_tintClickBtn := 0,v_numPartenerContactId := 0
									,v_numAccountClass := 0, v_numWillCallWarehouseID := 0,v_numVendorAddressID := 0, v_numShipFromWarehouse := 0, v_numShippingService := v_numDefaultShippingServiceID,
									v_ClientTimeZoneOffset := 0,v_vcCustomerPO := '',v_bitDropShipAddress := false,v_PromCouponCode := NULL,v_numPromotionId := 0,v_dtExpectedDate := null,v_numProjectID := 0);


		 RAISE NOTICE '%',v_numNewOppID;
         PERFORM USP_OpportunityMaster_CT(v_numDomainID,v_numUserCntID,v_numNewOppID);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
		
 -- DO NOT RAISE ERROR
v_numNewOppID := 0;
   END;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/



