-- Stored procedure definition script usp_GetTerritoryID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetTerritoryID(   
--
v_vcTerrName VARCHAR(50) DEFAULT '',
	v_numCreatedby NUMERIC(9,0) DEFAULT 0,
	v_numDomainId NUMERIC(9,0) DEFAULT 0,
	v_bintCreatedDate BIGINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTerId  NUMERIC;
BEGIN
   IF v_vcTerrName <> '' then
		
      select   numTerID INTO v_numTerId from TerritoryMaster WHERE  upper(vcTerName) = upper(v_vcTerrName);
      IF v_numTerId is null then
				
         INSERT into TerritoryMaster(vcTerName,numCreatedBy,bintCreatedDate,numDomainid)
					values(v_vcTerrName,v_numCreatedby,v_bintCreatedDate,v_numDomainId);
					
         open SWV_RefCur for
         SELECT CURRVAL('territorymaster_seq');
      ELSE
         open SWV_RefCur for
         SELECT v_numTerId;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


