-- Stored procedure definition script USP_GetCheckMainDetailsId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckMainDetailsId(v_numCheckId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);
BEGIN
   v_numJournalId := 0;  
	  
   select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numCheckId = v_numCheckId;  
	  
   open SWV_RefCur for Select numTransactionId From General_Journal_Details
   Where numJournalId = v_numJournalId And bitMainCheck = true And numDomainId = v_numDomainId;
END; $$;












