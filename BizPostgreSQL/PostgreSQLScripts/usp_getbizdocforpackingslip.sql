-- Stored procedure definition script USP_GetBizDocForPackingSlip for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBizDocForPackingSlip(v_numOppId NUMERIC(9,0)  DEFAULT NULL,
	v_numDomainID NUMERIC(9,0)  DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
  
	--SELECT vcBizDocID FROM OpportunityBizDocs WHERE numOppId = @numOppId AND vcBizDocName = 'PAC-'
   open SWV_RefCur for SELECT vcBizDocID FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = 29397;
END; $$;
--created by Prasanta

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0












