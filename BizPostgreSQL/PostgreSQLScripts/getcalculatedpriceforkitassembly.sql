-- Function definition script GetCalculatedPriceForKitAssembly for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCalculatedPriceForKitAssembly(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numQty DOUBLE PRECISION
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_tintKitAssemblyPriceBasedOn SMALLINT
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0)
	,v_vcSelectedKitChildItems TEXT
	,v_numCurrencyID NUMERIC(18,0)
	,v_fltExchangeRate DOUBLE PRECISION)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monCalculatedPrice  DECIMAL(20,5) DEFAULT 0;
BEGIN
   IF coalesce(v_numWarehouseItemID,0) = 0 then
	
      v_numWarehouseItemID := coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode LIMIT 1),0);
   end if;

   select   coalesce(monPrice,0) INTO v_monCalculatedPrice FROM
   fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,v_numWarehouseItemID,
   v_tintKitAssemblyPriceBasedOn,v_numOppID,v_numOppItemID,v_vcSelectedKitChildItems,
   v_numCurrencyID,v_fltExchangeRate);    

   RETURN v_monCalculatedPrice;
END; $$;

