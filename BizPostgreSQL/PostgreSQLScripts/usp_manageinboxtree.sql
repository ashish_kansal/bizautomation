-- Stored procedure definition script Usp_ManageInboxTree for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_ManageInboxTree(v_numUserCntID NUMERIC(9,0),    
v_numDomainID NUMERIC(9,0),    
v_NodeID NUMERIC(9,0),    
v_NodeName VARCHAR(5000),    
v_mode SMALLINT,    
v_ParentId NUMERIC(9,0),
v_ParentNodeText VARCHAR(5000) DEFAULT '',
v_numLastUid NUMERIC(18,0) DEFAULT 0,
v_bitHidden BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
		
   v_numSortOrder  NUMERIC;      
      
   v_hDocItem  INTEGER;
BEGIN
   if v_mode = 0 then

      IF Not Exists(Select 'col1' from  InboxTreeSort where numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
		 
         open SWV_RefCur for
         select numNodeID,numParentId,vcName as vcNodeName ,coalesce(numLastUid,0) AS numLastUid,coalesce(numLastUid,0) AS numLastUid
			 ,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         from InboxTree where (numDomainID = 0 or numDomainID = v_numDomainID)
         and (numUserCntId = v_numUserCntID or numUserCntId = 0)
         order by numNodeID,numParentId;
      ELSE
         open SWV_RefCur for
         SELECT numNodeID,numParentID,CASE WHEN coalesce(vcNodeName,'') = 'INBOX' THEN 'Inbox' ELSE coalesce(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,coalesce(numLastUid,0) AS numLastUid
			,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         FROM InboxTreeSort
         WHERE numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID 
         ORDER BY numSortOrder ASC;
      end if;
   end if;      
   if v_mode = 1 then
    
BEGIN
		--Changed by sachin sadhu|| Date:21stJan2014
    --Purpose:Bug on creating Folders||VirginDrinks
         select   MAX(numSortOrder)+1 INTO v_numSortOrder FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID;
         IF(v_ParentNodeText <> '') then
		
            SELECT  numNodeID INTO v_ParentId FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND vcNodeName = v_ParentNodeText     LIMIT 1;
         end if;
         IF(v_ParentId = 0) then
		
            select   numNodeID INTO v_ParentId FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numParentID IS NULL;
         end if;
         IF EXISTS(SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numParentID = v_ParentId AND LOWER(vcNodeName) = LOWER(v_NodeName)) then
		
            RAISE EXCEPTION 'FOLDER_ALREADY_EXISTS';
            RETURN;
         ELSE
            INSERT INTO InboxTree(vcName
				,numParentId
				,numDomainID,numUserCntId)
			VALUES(v_NodeName
				,v_ParentId
				,v_numDomainID,v_numUserCntID);   
			--To Save values into InboxTree Sort
			
            INSERT INTO InboxTreeSort(vcNodeName
				,numParentID
				,numDomainID,numUserCntID,numSortOrder)
			VALUES(v_NodeName
				,v_ParentId
				,v_numDomainID,v_numUserCntID,v_numSortOrder);
         end if;
         EXCEPTION WHEN OTHERS THEN
              GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
      -- COMMIT
end if;      
      
   if v_mode = 2 then

      update  EmailHistory set numNodeId = 2 where numDomainID = v_numDomainID and numUserCntId = v_numUserCntID and  numNodeId in(select numNodeID from InboxTree where (numNodeID = v_NodeID or numParentId = v_NodeID)  and
      numDomainID = v_numDomainID and numUserCntId = v_numUserCntID);
      delete FROM InboxTree where (numNodeID = v_NodeID or numParentId = v_NodeID)  and
      numDomainID = v_numDomainID and numUserCntId = v_numUserCntID;    
--Update Email History with [InboxTreeSort]
      update  EmailHistory set numNodeId = 2 where numDomainID = v_numDomainID and numUserCntId = v_numUserCntID and  numNodeId in(select numNodeID from InboxTreeSort where (numNodeID = v_NodeID or numParentID = v_NodeID)  and
      numDomainID = v_numDomainID and numUserCntID = v_numUserCntID);
      delete FROM InboxTreeSort where (numNodeID = v_NodeID or numParentID = v_NodeID)  and
      numDomainID = v_numDomainID and numUserCntID = v_numUserCntID;
   end if;  
   if v_mode = 3 then
      BEGIN
         -- BEGIN TRAN
DROP TABLE IF EXISTS tt_TEMPFOLDER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPFOLDER
         (
            vcFolderName VARCHAR(300),
            vcParentFolderName VARCHAR(300)
         );
  
         INSERT INTO tt_TEMPFOLDER(vcFolderName
			,vcParentFolderName)
         SELECT
         vcFolderName
			,vcParentFolderName
         FROM
		  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_NodeName AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcFolderName VARCHAR(300) PATH 'vcFolderName',
				vcParentFolderName VARCHAR(300) PATH 'vcParentFolderName'
		) AS X;
  
         DELETE FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND vcNodeName NOT IN(SELECT vcFolderName FROM tt_TEMPFOLDER);

		 IF (SELECT COUNT(*) FROM tt_TEMPFOLDER) = 0 THEN
			INSERT INTO InboxTreeSort(vcNodeName
				,numLastUid
				,numParentID
				,numDomainID,numUserCntID,numSortOrder)
			 SELECT
			 DISTINCT
			 SUBSTR(Items,POSITION('#' IN Items)+1,LENGTH(Items)),
					0,
					0,
					v_numDomainID,
					v_numUserCntID ,
					0
					--ROW_NUMBER() OVER( ORDER BY Items )
			 FROM Split(v_NodeName,',') WHERE Items <> '' AND
			 SUBSTR(Items,POSITION('#' IN Items)+1,LENGTH(Items)) <> '' AND
			 SUBSTR(Items,POSITION('#' IN Items)+1,LENGTH(Items)) NOT IN(SELECT coalesce(vcNodeName,'') FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID); 
		 END IF;

			--To Save values into InboxTree Sort
         INSERT INTO InboxTreeSort(vcNodeName
				,numLastUid
				,numParentID
				,numDomainID,numUserCntID,numSortOrder)
         SELECT
         DISTINCT
         vcFolderName,
				0,
				(SELECT  numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND vcNodeName = vcParentFolderName LIMIT 1),
				v_numDomainID,
				v_numUserCntID,
				0
				--ROW_NUMBER() OVER( ORDER BY Items )
         FROM
         tt_TEMPFOLDER TF
         WHERE
         coalesce(TF.vcFolderName,'') <> ''
         AND TF.vcFolderName NOT IN(SELECT coalesce(vcNodeName,'') FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID);
         IF((SELECT COUNT(*) FROM ImapUserDetails WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND vcImapServerUrl = 'outlook.office365.com') > 0) then
				
            UPDATE InboxTreeSort SET vcNodeName = 'Inbox' WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND vcNodeName = 'INBOX';
         end if;

         -- COMMIT
EXCEPTION WHEN OTHERS THEN
           GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END;
   end if;
   if v_mode = 4 then

      IF((SELECT  coalesce(numLastUid,0) FROM InboxTreeSort WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID LIMIT 1) = 0) then
	
         UPDATE InboxTreeSort SET numOldEmailLastUid = v_numLastUid WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
      end if;
      UPDATE InboxTree SET numLastUid = v_numLastUid WHERE numUserCntId = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
      UPDATE InboxTreeSort SET numLastUid = v_numLastUid WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
   end if;
   if v_mode = 5 then

      UPDATE InboxTree SET numOldEmailLastUid = v_numLastUid WHERE numUserCntId = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
      UPDATE InboxTreeSort SET numOldEmailLastUid = v_numLastUid WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
   end if;


   if v_mode = 7 then

      IF Not Exists(Select 'col1' from  InboxTreeSort where numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
		 
         open SWV_RefCur for
         select numNodeID,numParentId,vcName as vcNodeName ,coalesce(numLastUid,0) AS numLastUid,coalesce(numLastUid,0) AS numLastUid
			 ,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         from InboxTree where (numDomainID = 0 or numDomainID = v_numDomainID)
         and (numUserCntId = v_numUserCntID or numUserCntId = 0)    AND  numNodeID = v_NodeID
         order by numNodeID,numParentId;
      ELSE
         open SWV_RefCur for
         SELECT numNodeID,numParentID,coalesce(vcNodeName,'') as vcNodeName ,numSortOrder, bitSystem, numFixID ,coalesce(numLastUid,0) AS numLastUid
			,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         FROM InboxTreeSort
         WHERE numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID      AND   numNodeID = v_NodeID
         ORDER BY numSortOrder ASC;
      end if;
   end if;
   if v_mode = 8 then

      UPDATE InboxTreeSort SET bitHidden = v_bitHidden WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
   end if;

   if v_mode = 9 then

      UPDATE InboxTreeSort SET numParentID = 0,numSortOrder =(SELECT MIN(coalesce(numSortOrder,0) -1) FROM InboxTreeSort WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID) WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID = v_NodeID;
      UPDATE InboxTreeSort SET numSortOrder =(coalesce(numSortOrder,0)+1) WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numNodeID <> v_NodeID AND coalesce(numSortOrder,0) > 0;
   end if;
    
   if v_mode = 10 then

      IF Not Exists(Select 'col1' from  InboxTreeSort where numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
		 
         open SWV_RefCur for
         select numNodeID,numParentId,vcName as vcNodeName ,coalesce(numLastUid,0) AS numLastUid,coalesce(numLastUid,0) AS numLastUid
			 ,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         from InboxTree where (numDomainID = 0 or numDomainID = v_numDomainID)
         and (numUserCntId = v_numUserCntID or numUserCntId = 0)
         order by numNodeID,numParentId;
      ELSE
         open SWV_RefCur for
         SELECT numNodeID,numParentID,CASE WHEN coalesce(vcNodeName,'') = 'INBOX' THEN 'Inbox' ELSE coalesce(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,coalesce(numLastUid,0) AS numLastUid
			,coalesce(numOldEmailLastUid,0) AS numOldEmailLastUid
         FROM InboxTreeSort
         WHERE numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID 
         AND coalesce(bitHidden,false) = false
         ORDER BY numSortOrder ASC;
      end if;
   end if;
END; $$;  


