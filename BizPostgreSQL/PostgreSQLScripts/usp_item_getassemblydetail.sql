-- Stored procedure definition script USP_Item_GetAssemblyDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_GetAssemblyDetail(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTempWarehouseID  NUMERIC(18,0);
BEGIN
   select   numWareHouseID INTO v_numTempWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;

   open SWV_RefCur for
   SELECT
   FormatedDateFromDate(TIMEZONE('UTC',now())+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID) AS dtShipDate
		,fn_GetAssemblyPossibleWOQty(v_numItemCode,v_numWarehouseItemID) AS numMaxWOQty
		,CASE WHEN coalesce(Item.bitCalAmtBasedonDepItems,false) = true THEN coalesce((SELECT monMSRPPrice FROM fn_GetKitAssemblyCalculatedPrice(v_numDomainID,0,v_numItemCode,1,v_numWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,
         0,0,'',0,1)),0) ELSE coalesce(monListPrice,0) END AS monPrice
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode;

   open SWV_RefCur2 for
   SELECT
   coalesce(ItemDetails.sintOrder,0)
		,Item.numItemCode
		,CONCAT(Item.vcItemName,(CASE WHEN LENGTH(coalesce(Item.vcSKU,'')) > 0 THEN CONCAT(' (',Item.vcSKU,')')  ELSE '' END)) AS vcItemName
		,CAST((1*coalesce(ItemDetails.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ItemDetails.numUOMID,0),Item.numItemCode,v_numDomainID,coalesce(Item.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQuantity
		,coalesce(Item.monListPrice,0) AS monListPrice
   FROM
   ItemDetails
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   WHERE
   numItemKitID = v_numItemCode;
   RETURN;
END; $$;


