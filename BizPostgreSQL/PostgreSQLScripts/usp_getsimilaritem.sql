-- Stored procedure definition script USP_GetSimilarItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSimilarItem(v_numDomainID NUMERIC(9,0),
v_numParentItemCode NUMERIC(9,0),
v_byteMode SMALLINT,
v_vcCookieId TEXT DEFAULT '',
v_numUserCntID NUMERIC(18,0) DEFAULT 0,
v_numSiteID NUMERIC(18,0) DEFAULT 0,
v_numOppID NUMERIC(18,0) DEFAULT 0,
v_numWarehouseID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPromotionID  NUMERIC(18,0) DEFAULT 0;
   v_vcPromotionDescription  TEXT DEFAULT '';
   v_intPostSellDiscount  INTEGER DEFAULT 0;
   v_vcPromotionName  VARCHAR(100) DEFAULT '';
BEGIN
   If v_byteMode = 1 then

      open SWV_RefCur for
      select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode;
   ELSEIF v_byteMode = 3
   then

      open SWV_RefCur for
      SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS monListPrice,
fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc,coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell, coalesce(bitRequired,false) AS bitRequired,coalesce(vcUpSellDesc,'') AS vcUpSellDesc
,(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
         WHERE C.vcCookieId = v_vcCookieId AND C.numUserCntId = v_numUserCntID AND
(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers
      FROM
      Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
      WareHouseItems W ON I.numItemCode = W.numItemID
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode;
   ELSEIF v_byteMode = 2
   then
      open SWV_RefCur for
      SELECT DISTINCT I.vcItemName AS vcItemName,I.txtItemDesc AS txtItemDesc,I.numSaleUnit,coalesce(I.numContainer,0) AS numContainer,coalesce(I.bitAllowDropShip,false) AS bitAllowDropShip
	,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID, coalesce(vcRelationship,'') AS vcRelationship
	,ROUND(CAST(coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS NUMERIC),2) AS monListPrice
	,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as fltUOMConversionFactor, I.vcSKU, I.vcManufacturer
	,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc, coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell
	,coalesce(bitRequired,false) AS bitRequired, coalesce(vcUpSellDesc,'') AS vcUpSellDesc, fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.charItemType = 'P' THEN coalesce(numOnHand,0) ELSE -1 END as numOnHand
	,(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
	--WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID 
         WHERE
	(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers,SI.numParentItemCode
      FROM SimilarItems SI --Category 
      INNER JOIN Item I ON I.numItemCode = SI.numItemCode
      LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode
      LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND (numWareHouseID = v_numWarehouseID OR coalesce(v_numWarehouseID,0) = 0) LIMIT 1) AS W on TRUE
      WHERE
      SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode;
   ELSEIF v_byteMode = 4
   then -- Pre up sell

      select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
      open SWV_RefCur for
      SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS monListPrice,
fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc,coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell, coalesce(bitRequired,false) AS bitRequired,coalesce(vcUpSellDesc,'') AS vcUpSellDesc,
(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
         WHERE C.vcCookieId = v_vcCookieId AND C.numUserCntId = v_numUserCntID AND
(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers
      FROM
      Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
      SimilarItems SI ON I.numItemCode = SI.numItemCode
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND (numWareHouseID = v_numWarehouseID OR v_numWarehouseID = 0) LIMIT 1) AS W on TRUE
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode AND coalesce(bitPreUpSell,false) = true
      AND 1 =(CASE WHEN I.charItemType = 'P' THEN(CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END);
   ELSEIF v_byteMode = 5
   then -- Post up sell
      select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
      select   coalesce(P.intPostSellDiscount,0), C.numPromotionID, CONCAT('Post sell Discount: ',OM.vcpOppName), P.vcProName INTO v_intPostSellDiscount,v_numPromotionID,v_vcPromotionDescription,v_vcPromotionName FROM
      OpportunityItems AS C
      INNER JOIN
      OpportunityMaster OM
      ON
      C.numOppId = OM.numOppId
      LEFT JOIN
      PromotionOffer AS P
      ON
      P.numProId = C.numPromotionID WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numOppId = v_numOppID
      AND coalesce(OM.bitPostSellDiscountUsed,false) = false
      AND coalesce(C.numPromotionID,0) > 0
      AND coalesce(P.bitDisplayPostUpSell,false) = true
      AND coalesce(P.intPostSellDiscount,0) BETWEEN 1 AND 100    LIMIT 1;
      open SWV_RefCur for
      SELECT DISTINCT
      I.numItemCode
		,I.vcItemName AS vcItemName
		,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
		,I.txtItemDesc AS txtItemDesc
		,I.vcModelID
		,Category.vcCategoryName
		,1 AS numUnitHour
		,coalesce(CASE WHEN I.charItemType = 'P' THEN monWListPrice ELSE monListPrice END,0) AS monListPrice
		,coalesce(CASE WHEN I.charItemType = 'P' THEN(monWListPrice -(CAST((monWListPrice*(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_intPostSellDiscount ELSE 0 END)) AS decimal)/100)) ELSE(monListPrice -(CAST((monListPrice*(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_intPostSellDiscount ELSE 0 END)) AS decimal)/100)) END,0) AS monOfferListPrice
		,coalesce(W.numWarehouseItemID,0) AS numWarehouseItemID
		,(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_intPostSellDiscount ELSE 0 END) AS fltDiscount
		,false AS bitDiscountType
		,(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_numPromotionID ELSE 0 END) AS numPromotionID
		,(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_vcPromotionDescription ELSE '' END) AS vcPromotionDescription
		,(CASE WHEN OI.numPromotionID = v_numPromotionID THEN v_vcPromotionName ELSE '' END) AS vcPromotionName
		,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor
		,I.vcSKU
		,I.vcManufacturer
		,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc
		,coalesce(bitPreUpSell,false) AS bitPreUpSell
		,coalesce(bitPostUpSell,false) AS bitPostUpSell
		,coalesce(bitRequired,false) AS bitRequired
		,coalesce(vcUpSellDesc,'') AS vcUpSellDesc
      FROM
      Category
      INNER JOIN
      ItemCategory
      ON
      Category.numCategoryID = ItemCategory.numCategoryID
      INNER JOIN
      Item I
      ON
      ItemCategory.numItemID = I.numItemCode
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      SimilarItems SI
      ON
      SI.numParentItemCode = OI.numItemCode
      AND SI.bitPostUpSell = true
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND numWareHouseID = v_numWarehouseID LIMIT 1) AS W on TRUE
      WHERE
      SI.numDomainId = v_numDomainID
      AND OM.numDomainId = v_numDomainID
      AND OM.numOppId = v_numOppID
      AND 1 =(CASE WHEN charItemType = 'P' THEN CASE WHEN coalesce(W.numWareHouseItemID,0) > 0 THEN 1 ELSE 0 END ELSE 1 END);
      UPDATE OpportunityMaster SET bitPostSellDiscountUsed = true WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
   ELSEIF v_byteMode = 6
   then -- Pre up sell for E-Commerce

      select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
	 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
      open SWV_RefCur for
      SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID, coalesce(vcRelationship,'') AS vcRelationship
	,ROUND(CAST(coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS NUMERIC),2) AS monListPrice
	,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc, coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell
	,coalesce(bitRequired,false) AS bitRequired, coalesce(vcUpSellDesc,'') AS vcUpSellDesc, fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.charItemType = 'P' THEN coalesce(numOnHand,0) ELSE -1 END as numOnHand
	,(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
         WHERE C.vcCookieId = v_vcCookieId AND C.numUserCntId = v_numUserCntID AND
	(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers,SI.numParentItemCode
      FROM SimilarItems SI --Category 
      INNER JOIN Item I ON I.numItemCode = SI.numItemCode
      LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode
      LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND (numWareHouseID = v_numWarehouseID OR v_numWarehouseID = 0) LIMIT 1) AS W on TRUE
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode AND coalesce(bitPreUpSell,false) = true
      AND 1 =(CASE WHEN I.charItemType = 'P' THEN(CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END);
   ELSEIF v_byteMode = 7
   then -- Post up sell for E-Commerce

      select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
      open SWV_RefCur for
      SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID, coalesce(vcRelationship,'') AS vcRelationship
	,ROUND(CAST(coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS NUMERIC),2) AS monListPrice
	,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc, coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell
	,coalesce(bitRequired,false) AS bitRequired, coalesce(vcUpSellDesc,'') AS vcUpSellDesc, fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.charItemType = 'P' THEN coalesce(numOnHand,0) ELSE -1 END as numOnHand
	,(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
         WHERE C.vcCookieId = v_vcCookieId AND C.numUserCntId = v_numUserCntID AND
	(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers, PO.vcProName
	,CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN CAST(fltOfferTriggerValue AS VARCHAR(30)) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
      WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
      WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
         '"')
      END,
      ' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END,CASE
      WHEN tintDiscoutBaseOn = 1 THEN
         CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 2)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 2 THEN
         CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 2)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
      ELSE ''
      END) AS vcPromotionRule, PO.numProId, SI.numParentItemCode
      FROM SimilarItems SI --Category 
      INNER JOIN Item I ON I.numItemCode = SI.numItemCode
      LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode
      LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID
      LEFT JOIN PromotionOfferItems AS POI ON POI.numValue = I.numItemCode
      LEFT JOIN PromotionOffer PO on PO.numProId = POI.numProId
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND (numWareHouseID = v_numWarehouseID OR v_numWarehouseID = 0) LIMIT 1) AS W on TRUE
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode AND coalesce(bitPostUpSell,false) = true
      AND 1 =(CASE WHEN I.charItemType = 'P' THEN(CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END);
   ELSEIF v_byteMode = 8
   then -- Pre up sell for Internal Orders

      select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
      open SWV_RefCur for
      SELECT DISTINCT
      I.vcItemName AS vcItemName
		,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage
		,I.txtItemDesc AS txtItemDesc
		,SI.*
		,coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID
		,coalesce(vcRelationship,'') AS vcRelationship
		,ROUND(CAST(coalesce(CASE WHEN I.charItemType = 'P' THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*W.monWListPrice END ELSE 1.00000*monListPrice END,0) AS NUMERIC),2) AS monListPrice
		,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
		,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc, coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell
		,coalesce(bitRequired,false) AS bitRequired
		,coalesce(vcUpSellDesc,'') AS vcUpSellDesc
		,fn_GetUOMName(I.numSaleUnit) AS vcUOMName
		,CASE WHEN I.charItemType = 'P' THEN coalesce(numOnHand,0) ELSE -1 END as numOnHand
		,(SELECT  CASE WHEN P.tintDiscountType = 1
         THEN(I.monListPrice*fltDiscountValue)/100
         WHEN P.tintDiscountType = 2
         THEN fltDiscountValue
         WHEN P.tintDiscountType = 3
         THEN fltDiscountValue*I.monListPrice
         ELSE 0 END
         FROM PromotionOffer AS P
         LEFT JOIN CartItems AS C ON P.numProId = C.PromotionID
         WHERE C.vcCookieId = v_vcCookieId AND C.numUserCntId = v_numUserCntID AND
		(I.numItemCode IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 1 AND PIS.tintRecordType = 6) OR
         I.numItemClassification IN(SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId = P.numProId AND PIS.tintType = 2 AND PIS.tintRecordType = 6))
         AND P.numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END) LIMIT 1) AS PromotionOffers
		,SI.numParentItemCode
		,(CASE WHEN coalesce(bitKitParent,false) = true
      THEN(CASE
         WHEN((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)) > 0
         THEN 1
         ELSE 0
         END)
      ELSE
         0
      END) AS bitHasChildKits
		,Category.vcCategoryName
      FROM SimilarItems SI --Category 
      INNER JOIN Item I ON I.numItemCode = SI.numItemCode
      LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode
      LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID
      LEFT JOIN LATERAL(SELECT * FROM
         WareHouseItems
         WHERE
         numItemID = I.numItemCode
         AND (numWareHouseID = v_numWarehouseID OR coalesce(v_numWarehouseID,0) = 0) LIMIT 1) AS W on TRUE
      WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numParentItemCode AND coalesce(bitPreUpSell,false) = true
      AND 1 =(CASE WHEN I.charItemType = 'P' THEN(CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END);
   end if;
   RETURN;
END; $$;





