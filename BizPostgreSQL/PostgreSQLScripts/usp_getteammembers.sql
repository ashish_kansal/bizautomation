-- Stored procedure definition script USP_GetTeamMembers for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTeamMembers(v_numUserCntId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,    
v_intType SMALLINT DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  numContactId as  numUserID, vcFirstName || ' ' || vcLastname as Name from AdditionalContactsInformation
   join UserMaster on numContactId = numUserDetailId
   where bitActivateFlag = true and numTeam in(select cast(F.numTeam as NUMERIC(18,0)) from ForReportsByTeam F
      where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_intType);
END; $$;












