-- Stored procedure definition script USP_UpdateForecastAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateForecastAmt(v_numForecastAmount DECIMAL(20,5),
v_numForecastID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Forecast
   set monForecastAmount = v_numForecastAmount
   where numForecastID = v_numForecastID;
   RETURN;
END; $$;


