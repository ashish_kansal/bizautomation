DROP FUNCTION IF EXISTS Activity_FutureDate;

CREATE OR REPLACE FUNCTION Activity_FutureDate
(
	v_OrganizerName VARCHAR(64),-- resource name (will be used to lookup ID)  
	v_StartDateTimeUtc TIMESTAMP, -- the start date before which no activities are retrieved  
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql  
AS $$
	DECLARE
	v_ResourceID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_OrganizerName) = true then
      v_ResourceID := CAST(v_OrganizerName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if; 

	OPEN SWV_RefCur FOR 
	SELECT   
		Activity.AllDayEvent,   
		Activity.ActivityDescription,   
		Activity.Duration,   
		Activity.Location,   
		Activity.ActivityID,   
		Activity.StartDateTimeUtc,  
		Activity.Subject,   
		Activity.EnableReminder,   
		Activity.ReminderInterval,  
		Activity.ShowTimeAs,  
		Activity.Importance,  
		Activity.Status,  
		Activity.RecurrenceID,  
		Activity.VarianceID,  
		Activity._ts,  
		Activity.ItemId,  
		Activity.ChangeKey
		,Activity.GoogleEventId  
	FROM   
		Activity 
	INNER JOIN 
		ActivityResource 
	ON 
		Activity.ActivityID = ActivityResource.ActivityID
	WHERE   
		ActivityResource.ResourceID = v_ResourceID
		AND (( Activity.StartDateTimeUtc >= v_StartDateTimeUtc OR ( Activity.RecurrenceID <> -999 )) AND (Activity.OriginalStartDateTimeUtc IS NULL));  
END; $$;


