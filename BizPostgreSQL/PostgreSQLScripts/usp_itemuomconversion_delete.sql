-- Stored procedure definition script USP_ItemUOMConversion_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemUOMConversion_Delete(v_numItemUOMConvID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ItemUOMConversion WHERE numItemUOMConvID = v_numItemUOMConvID;
   RETURN;
END; $$;

