-- Function definition script udf_SpacesForCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION udf_SpacesForCases(v_string TEXT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE --Don't put space to left of first even if it's a capital
   v_len  INTEGER DEFAULT LENGTH(v_string);
   v_iterator  INTEGER DEFAULT 2;
BEGIN
   while v_iterator <= LENGTH(v_string) LOOP
      if coalesce(POSITION(substring(SUBSTR(v_string,v_iterator,1) from '^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$') IN SUBSTR(v_string,v_iterator,1)),0) <> 0 then
		
         v_string := OVERLAY(v_string placing '_' from v_iterator for 0);
         v_iterator := v_iterator::bigint+1;
      end if;
      v_iterator := v_iterator::bigint+1;
   END LOOP;

   return v_string;
END; $$;

