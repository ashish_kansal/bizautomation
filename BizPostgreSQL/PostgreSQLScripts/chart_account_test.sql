DROP FUNCTION IF EXISTS Chart_Account_Test;

CREATE OR REPLACE FUNCTION Chart_Account_Test(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPCHATest CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      select numAccountId
      ,numParntAcntTypeID
      ,numOriginalOpeningBal
      ,numOpeningBal
      ,dtOpeningDate
      ,bitActive
      ,bitFixed
      ,numDomainId
      ,numListItemID
      ,bitDepreciation
      ,bitOpeningBalanceEquity
      ,monEndingOpeningBal
      ,monEndingBal
      ,dtEndStatementDate
      ,chBizDocItems
      ,numAcntTypeId
      ,vcAccountCode
      ,vcAccountName
      ,vcAccountDescription
      ,bitProfitLoss
      ,dtDepreciationCostDate
      ,monDepreciationCost
      ,vcNumber
      ,bitIsSubAccount
      ,numParentAccId
      ,intLevel
      ,IsBankAccount
      ,vcStartingCheckNumber
      ,vcBankRountingNumber
      ,IsConnected
      ,numBankDetailID

      from Chart_Of_Accounts;

   open SWV_RefCur for select * from tt_TEMP;
   drop table IF EXISTS tt_TEMP CASCADE;
   RETURN;
END; $$;













