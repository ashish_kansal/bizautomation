-- Stored procedure definition script USP_ManageGoogleContact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageGoogleContact(v_numDomainID NUMERIC(9,0),  
--@vcName as varchar(100)='',
v_vcEmail VARCHAR(50) DEFAULT '',
v_vcPhone VARCHAR(15) DEFAULT ''
--@txtNotes as text=''
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--@numUserCntId as numeric(9),
   AS $$
   DECLARE
   v_vcdomain  VARCHAR(20);
   v_numCompanyID  NUMERIC(9,0);
   v_numDivisionId  NUMERIC(9,0);
   v_numContactId  NUMERIC(9,0);
BEGIN
   v_vcdomain := case when POSITION('@' IN v_vcEmail) > 0 then SUBSTR(v_vcEmail,POSITION('@' IN v_vcEmail)+1,LENGTH(v_vcEmail) -POSITION('@' IN v_vcEmail)) ELSE '' END;
   v_vcPhone := REPLACE(REPLACE(REPLACE(coalesce(v_vcPhone,''),'(',''),')',''),'-','');
 
 --Check with Email Domain and Phone No

 
   open SWV_RefCur for SELECT  cast(C.numCompanyId as VARCHAR(255)),D.numDivisionID from AdditionalContactsInformation A
   join DivisionMaster D  on D.numDivisionID = A.numDivisionId  join CompanyInfo C  on C.numCompanyId = D.numCompanyID
   where (coalesce(vcEmail,'') ilike '%' || coalesce(v_vcdomain,'') OR 1 =(CASE WHEN LENGTH(v_vcPhone) > 0 then CASE WHEN REPLACE(REPLACE(REPLACE(coalesce(numPhone,cast(0 as TEXT)),'(',''),')',''),
      '-','') = v_vcPhone THEN 1 ELSE 0 END ELSE 0 END))
   AND A.numDomainID = v_numDomainID
   AND v_vcdomain NOT IN(SELECT cast(coalesce(vcRenamedListName,vcData) as VARCHAR(255)) as vcData FROM Listdetails Ld
      left join listorder LO on Ld.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = 321 and (constFlag = true or Ld.numDomainid = v_numDomainID)) LIMIT 1;
END; $$;
-- EXEC USP_ManageHelpCategories 0,'asda',1,3,0,0












