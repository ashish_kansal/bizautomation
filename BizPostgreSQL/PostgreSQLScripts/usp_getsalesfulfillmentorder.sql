DROP FUNCTION IF EXISTS USP_GetSalesFulfillmentOrder;

CREATE OR REPLACE FUNCTION USP_GetSalesFulfillmentOrder
(
	v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_SortCol VARCHAR(50) DEFAULT NULL,
    v_SortDirection VARCHAR(4) DEFAULT NULL,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_numUserCntID NUMERIC DEFAULT NULL,
    v_vcBizDocStatus VARCHAR(500) DEFAULT NULL,
    v_vcBizDocType VARCHAR(500) DEFAULT NULL,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT 0,
    v_bitIncludeHistory BOOLEAN DEFAULT false,
    v_vcOrderStatus VARCHAR(500) DEFAULT NULL,
    v_vcOrderSource VARCHAR(1000) DEFAULT NULL,
	v_numOppID NUMERIC(18,0) DEFAULT 0,
	v_vcShippingService TEXT DEFAULT '',
	v_numShippingZone NUMERIC(18,0) DEFAULT 0,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '',
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null,
	INOUT SWV_RefCur3 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_firstRec  INTEGER;                                                              
	v_lastRec  INTEGER;
	v_tintCommitAllocation  SMALLINT;
	v_tintDecimalPoints  SMALLINT;
	v_Nocolumns  SMALLINT DEFAULT 0;
	v_numFormId  INTEGER DEFAULT 23;
	v_IsMasterConfAvailable  BOOLEAN DEFAULT false;
	v_numUserGroup  NUMERIC(18,0);
	v_strColumns  TEXT DEFAULT '';
	v_FROM  TEXT DEFAULT '';
	v_WHERE  TEXT DEFAULT '';

	v_strSql  TEXT;
   v_SELECT  TEXT DEFAULT 'CREATE TEMPORARY TABLE tt_TEMPGetSalesFulfillmentOrder AS WITH bizdocs AS 
									(SELECT
										Opp.numOppID
										,Opp.numRecOwner
										,Opp.vcpOppName
										,DM.tintCRMType
										,COALESCE(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,COALESCE(Opp.intUsedShippingCompany,COALESCE(DM.intShippingCompany,90)) AS numShipVia
										,COALESCE(Opp.intUsedShippingCompany,COALESCE(DM.intShippingCompany,90)) as intUsedShippingCompany
										,COALESCE(Opp.numShippingService,0) numShippingService
										,CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId=ShippingReport.numShippingReportId WHERE ShippingReport.numOppID=Opp.numOppID AND LENGTH(COALESCE(vcTrackingNumber,'''')) > 0) > 0 THEN true ELSE false END AS bitTrackingNumGenerated
										,Opp.monDealAmount
										,(CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = Opp.numDomainId AND numOppId=Opp.numOppId AND COALESCE(bitExecuted,false)=false) > 0 THEN true ELSE false END) as bitPendingExecution
										,COALESCE(Opp.monDealAmount,0) AS monDealAmount1';

   v_tintOrder  SMALLINT;
   v_vcFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(1);
   v_vcAssociatedControlType  VARCHAR(30);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(40);
   v_vcLookBackTableName  VARCHAR(2000);
   v_bitCustom  BOOLEAN;
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;   
   v_bitAllowEdit  BOOLEAN;                 
   v_vcColumnName  VARCHAR(500);
   v_ListRelID  NUMERIC(9,0); 

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_numShippingServiceItemID  NUMERIC;
   v_Prefix  VARCHAR(5);
BEGIN
	DROP TABLE IF EXISTS tt_TEMPGetSalesFulfillmentOrder CASCADE;

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                             
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);      
	
   select   coalesce(tintCommitAllocation,1), coalesce(tintDecimalPoints,0) INTO v_tintCommitAllocation,v_tintDecimalPoints FROM
   Domain WHERE
   numDomainId = v_numDomainId;                                                              

   IF LENGTH(coalesce(v_SortCol,'')) = 0 OR POSITION('OBD.' IN v_SortCol) > 0 OR v_SortCol = 'dtCreatedDate' then
      v_SortCol := 'Opp.bintCreatedDate';
   end if;
		
   IF LENGTH(coalesce(v_SortDirection,'')) = 0 then
      v_SortDirection := 'DESC';
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TempForm_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1), 
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainId
      AND tintPageType = 1
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainId
      AND tintPageType = 1) TotalRows;

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID = v_numFormId AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainId,v_numUserCntID,0,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainId,v_numUserCntID,0,1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO
      tt_TEMPFORM(tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType)
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      IF v_Nocolumns = 0 then
		
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         v_numFormId,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainId,v_numUserCntID,0,1,false,intColumnWidth
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = v_numFormId
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainId
         ORDER BY
         tintOrder asc;
      end if;
      INSERT INTO
      tt_TEMPFORM(tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType)
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainId
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainId
      AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      ORDER BY
      tintOrder asc;
   end if;

   v_FROM := ' FROM 
						OpportunityMaster Opp
					INNER JOIN 
						DivisionMaster DM 
					ON 
						Opp.numDivisionId = DM.numDivisionID
					INNER JOIN 
						CompanyInfo cmp 
					ON 
						cmp.numCompanyID=DM.numCompanyID
					LEFT JOIN
						AdditionalContactsInformation ADC
					ON
						Opp.numContactID=ADC.numContactID
					LEFT JOIN
						DivisionMasterShippingConfiguration DMSC
					ON
						DM.numDivisionID=DMSC.numDivisionID';
					
   v_WHERE := CONCAT(' WHERE 
							(Opp.numOppID = ',v_numOppID,' OR ',v_numOppID,' = 0) 
							AND Opp.tintopptype = 1 
							AND Opp.tintOppStatus=1 ',(CASE WHEN v_numDomainId <> 214 THEN ' AND (POSITION(LOWER(''Shippable'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0 OR POSITION(LOWER(''Not Applicable'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0) ' ELSE '' END), 
   ' 
							AND Opp.numDomainID = ',
   v_numDomainId); --CONCAT(' AND 1 = IsSalesOrderReadyToFulfillment(Opp.numDomainID,Opp.numOppID,',@tintCommitAllocation,')') For domain 214 -- Removed because of performance problem

   IF v_SortCol ilike 'CFW.Cust%' then
	
      v_FROM := coalesce(v_FROM,'') || ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' || REPLACE(v_SortCol,'CFW.Cust','') || ' ';
      v_SortCol := 'CFW.Fld_Value';
   end if; 
      
   IF POSITION('OBD.monAmountPaid' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('OBD.monAmountPaid like ''%0%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)) ');
      end if;
      IF POSITION('OBD.monAmountPaid like ''%1%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monDealAmount,0)) = SUM(COALESCE(monAmountPaid,0)) AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ');
      end if;
      IF POSITION('OBD.monAmountPaid like ''%2%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(COALESCE(monDealAmount,0)) - SUM(COALESCE(monAmountPaid,0))) > 0 AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('OBD.monAmountPaid like ''%3%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ');
      end if;
   end if;
	
   IF POSITION('OI.vcInventoryStatus' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('OI.vcInventoryStatus=1' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' POSITION(LOWER(''Not Applicable'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=2' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' POSITION(LOWER(''Shipped'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=3' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' POSITION(LOWER(''BO'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0 AND POSITION(LOWER(''Shippable'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) = 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=4' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' POSITION(LOWER(''Shippable'') in LOWER(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT))) > 0 ');
      end if;
   end if;

   IF POSITION('fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2::SMALLINT)' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2::SMALLINT)',
      '(CASE 
																																		WHEN Opp.tintShipToType IS NULL OR Opp.tintShipToType = 1
																																		THEN
																																			(SELECT  
																																				COALESCE(fn_GetState(AD.numState),'''')
																																			FROM 
																																				AddressDetails AD 
																																			WHERE 
																																				AD.numDomainID=Opp.numDomainID AND AD.numRecordID=Opp.numDivisionID 
																																				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=true)
																																		WHEN Opp.tintShipToType = 0
																																		THEN
																																			(SELECT 
																																				COALESCE(fn_GetState(AD.numState),'''')
																																			FROM 
																																				CompanyInfo [Com1] 
																																			JOIN 
																																				DivisionMaster div1 
																																			ON 
																																				com1.numCompanyID = div1.numCompanyID
																																			JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
																																			JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=true
																																			WHERE  D1.numDomainID = Opp.numDomainID)
																																		WHEN Opp.tintShipToType = 2 OR Opp.tintShipToType = 3
																																		THEN
																																			(SELECT
																																				COALESCE(fn_GetState(numShipState),'''')
																																			FROM 
																																				OpportunityAddress 
																																			WHERE 
																																				numOppID = Opp.numOppId)
																																		ELSE ''''
																																	END)');
   end if;
	

   IF v_vcRegularSearchCriteria <> '' then
	
      v_WHERE := coalesce(v_WHERE,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;

   IF v_vcCustomSearchCriteria <> '' then
	
      v_WHERE := coalesce(v_WHERE,'') || ' AND ' ||  coalesce(v_vcCustomSearchCriteria,'');
   end if;

   v_tintOrder := 0;
   select   COUNT(*) INTO v_iCount FROM tt_TEMPFORM;

   WHILE v_i <= v_iCount LOOP
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
      tt_TEMPFORM WHERE
      ID = v_i;
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'Opp.';
         end if;
         IF v_vcLookBackTableName = 'CompanyInfo' then
            v_Prefix := 'cmp.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMasterShippingConfiguration' then
            v_Prefix := 'DMSC.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         IF v_vcAssociatedControlType = 'SelectBox' then
			
            IF v_vcDbColumnName = 'numPartner' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner';
            ELSEIF v_vcDbColumnName = 'tintSource'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numContactID'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numShipState'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2::SMALLINT)' || ' "' || coalesce(v_vcColumnName,'') || '"';
			ELSEIF v_vcListItemType = 'LI'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_FROM := coalesce(v_FROM,'') || ' LEFT JOIN ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'SGT'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',(CASE CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS NUMERIC) 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'PSS'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR COALESCE(numDomainID,0)=0) AND numShippingServiceID = ' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '),'''') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            IF v_vcDbColumnName = 'dtReleaseDate' then
				
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ')  + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ')  + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ','
               || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when  CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ','
               || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea'
         then
            
            IF v_vcDbColumnName = 'vcCompanyName' then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetListItemName(Opp.numstatus) vcOrderStatus';
            ELSEIF v_vcDbColumnName = 'numShipState'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2::SMALLINT)' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Label'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',' || CASE
            WHEN v_vcDbColumnName = 'vcFulfillmentStatus' THEN 'COALESCE((SELECT string_agg(vcMessage,''<br />'') FROM SalesFulfillmentLog WHERE numDomainID = Opp.numDomainId AND numOppID=Opp.numOppId AND COALESCE(bitSuccess,false) = true),'''')'
            WHEN v_vcDbColumnName = 'vcPricedBoxedTracked' THEN 'COALESCE((SELECT string_agg(OpportunityBizDocs.numOppID || ''~'' || numOppBizDocsId || ''~'' || COALESCE(numShippingReportId,0) || ''~'' || (CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId) > 0 THEN 1 ELSE 0 END), '', '') FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT COALESCE(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=Opp.numDomainID) AND OpportunityBizDocs.numOppId=Opp.numOppID AND OpportunityBizDocs.bitShippingGenerated=true),'''')'
            WHEN v_vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
            WHEN v_vcDbColumnName = 'vcInventoryStatus' THEN 'COALESCE(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT),'''')'
            WHEN v_vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',v_tintDecimalPoints,
               ')')
            WHEN v_vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',v_tintDecimalPoints,
               ')')
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') END || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSE
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value  "'
            || coalesce(v_vcColumnName,'') || '"';
            v_FROM := coalesce(v_FROM,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',case when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=0 then 0 when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=1 then 1 end   "'
            || coalesce(v_vcColumnName,'') || '"';
            v_FROM := coalesce(v_FROM,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',FormatedDateFromDate(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,'
            || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
            || ')  "'
            || coalesce(v_vcColumnName,'') || '"';
            v_FROM := coalesce(v_FROM,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid    ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
            
            v_vcDbColumnName := 'DCust'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'')
            || ',L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.vcData'
            || ' "'
            || coalesce(v_vcColumnName,'') || '"';
            v_FROM := coalesce(v_FROM,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid ';
            v_FROM := coalesce(v_FROM,'')
            || ' left Join ListDetails L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value';
         ELSE
            v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValueOpp(',v_numFieldId,',opp.numOppID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

		
   select   coalesce(numShippingServiceItemID,0) INTO v_numShippingServiceItemID FROM Domain WHERE numDomainId = v_numDomainId;
		
   IF  LENGTH(coalesce(v_vcOrderStatus,'')) > 0 then
		
      v_WHERE := coalesce(v_WHERE,'') || coalesce(v_vcOrderStatus,'');
   end if;

   IF LENGTH(coalesce(v_vcShippingService,'')) > 0 then
		
      v_WHERE := coalesce(v_WHERE,'') || ' and COALESCE(Opp.numShippingService,0) in (SELECT Items FROM Split(''' || coalesce(v_vcShippingService,'') || ''','',''))';
   end if;

   IF coalesce(v_numShippingZone,0) > 0 then
		
      v_WHERE := coalesce(v_WHERE,'') || ' AND 1 = (CASE 
												WHEN COALESCE((SELECT numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2) LIMIT 1),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM State WHERE numDomainID=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || ' AND numStateID =  (SELECT numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2) LIMIT 1) AND numShippingZone=' || SUBSTR(CAST(v_numShippingZone as VARCHAR(30)),1,30) || ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)';
   end if;
		
   IF LENGTH(coalesce(v_vcOrderSource,'')) > 0 then
		
      v_WHERE := coalesce(v_WHERE,'') || ' and (' || coalesce(v_vcOrderSource,'') || ')';
   end if;
		
   v_WHERE := coalesce(v_WHERE,'') || ' AND (Opp.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || ' = 0) ';
		
   v_strSql := CONCAT(v_SELECT,v_strColumns,v_FROM,v_WHERE,' ORDER BY ',v_SortCol,' ',v_SortDirection,' LIMIT ',v_PageSize,' OFFSET ',((v_CurrentPage::bigint -1)*v_PageSize::bigint),') SELECT * FROM bizdocs;');

   RAISE NOTICE '%',CAST(v_strSql AS TEXT);

   EXECUTE v_strSql;

   OPEN SWV_RefCur FOR EXECUTE CONCAT('SELECT 
											OM.*
											,CAST((Case WHEN COALESCE(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID
										FROM 
											tt_TEMPGetSalesFulfillmentOrder OM 
										LEFT JOIN 
										(
											SELECT 
												numOppId
												,numoppitemtCode
												,COALESCE(vcItemDesc, '''') vcItemDesc
												,monTotAmount
												,Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
											FROM 
												OpportunityItems OI 
											WHERE 
												OI.numOppId in (SELECT numOppId FROM tt_TEMPGetSalesFulfillmentOrder) 
												AND numItemCode=',v_numShippingServiceItemID,'
										) OI 
										ON 
											OI.row=1 
											AND OM.numOppId = OI.numOppId;');
        
	v_strSql := CONCAT('SELECT 
							opp.numOppId
							,Opp.vcitemname AS vcItemName
							,COALESCE(FormatedDateFromDate(opp.ItemReleaseDate,',v_numDomainId,'),'''')  AS vcItemReleaseDate
							,COALESCE((SELECT vcPathForTImage FROM ItemImages WHERE numDomainId=',v_numDomainId,' AND ItemImages.numItemCode=i.numItemCode AND bitDefault=true AND bitIsImage=true LIMIT 1),'''') AS vcImage
							,CASE 
								WHEN charitemType = ''P'' THEN ''Product''
								WHEN charitemType = ''S'' THEN ''Service''
							END AS charitemType
							,CASE WHEN charitemType = ''P'' THEN ''Inventory''
								WHEN charitemType = ''N'' THEN ''Non-Inventory''
								WHEN charitemType = ''S'' THEN ''Service''
							END AS vcItemType
							,CASE WHEN COALESCE(numItemGroup,0) > 0 THEN WI.vcWHSKU ELSE i.vcSKU END vcSKU
							,USP_GetAttributes(opp.numWarehouseItmsID,bitSerialized) AS vcAttributes
							,COALESCE(W.vcWareHouse, '''') AS Warehouse,WL.vcLocation,opp.numUnitHour AS numUnitHourOrig
							,Opp.bitDropShip AS DropShip
							,COALESCE((SELECT  
											string_agg(vcSerialNo
																|| CASE WHEN COALESCE(I.bitLotNo, false) = true
																		THEN ''(''
																			|| COALESCE(oppI.numQty,0)
																			|| '')''
																		ELSE ''''
																	END,'' ,'' ORDER BY vcSerialNo)
										FROM    OppWarehouseSerializedItem oppI
												JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
										WHERE   oppI.numOppID = Opp.numOppId
												AND oppI.numOppItemID = Opp.numoppitemtCode),'''') AS SerialLotNo
							,COALESCE(bitSerialized,false) AS bitSerialized
							,COALESCE(bitLotNo,false) AS bitLotNo
							,Opp.numWarehouseItmsID
							,opp.numoppitemtCode
						FROM 
							OpportunityItems opp 
						JOIN 
							tt_TEMPGetSalesFulfillmentOrder OM 
						ON 
							opp.numoppid=OM.numoppid 
						LEFT JOIN 
							Item i 
						ON 
							opp.numItemCode = i.numItemCode
						INNER JOIN 
							WareHouseItems WI 
						ON 
							opp.numWarehouseItmsID = WI.numWareHouseItemID
							AND WI.numDomainID = ',v_numDomainId,'
						INNER JOIN 
							Warehouses W 
						ON 
							WI.numDomainID = W.numDomainID
							AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN 
							WarehouseLocation WL 
							ON WL.numWLocationID= WI.numWLocationID;');

   RAISE NOTICE '%',CAST(v_strSql AS TEXT);
   OPEN SWV_RefCur2 FOR EXECUTE v_strSql;
       
      
        
        /*Get total count */
   v_strSql :=  ' (SELECT COUNT(*) ' || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ')';
        
   RAISE NOTICE '%',v_strSql;
   EXECUTE v_strSql INTO v_TotRecs; 
        
   open SWV_RefCur3 for
   SELECT * FROM tt_TEMPFORM;
   RETURN;
END; $$;
  


