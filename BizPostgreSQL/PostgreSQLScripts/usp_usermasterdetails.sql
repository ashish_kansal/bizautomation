CREATE OR REPLACE FUNCTION USP_UserMasterDetails(v_numUserID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   SELECT 
   U.numUserId
   ,U.vcUserName as vcUserName
   ,U.numDomainID
   ,D.vcDomainName
	,U.numUserDetailId
	,LD.vcData as vcDepartment,
	coalesce(AD.vcStreet,'') || ' ' || coalesce(AD.vcCity,'') || ' ' || coalesce(AD.vcPostalCode,'') as HomeAddress,
	coalesce(ACI.numHomePhone,'') as numHomePhone,
	coalesce(ACI.numCell,'') as CellNo,
	coalesce(ACI.numPhone,'') as OtherPhone,
	coalesce(ACI.vcAsstEmail,'') as vcAsstEmail,
	coalesce(ACI.txtNotes,'') as txtNotes,
	U.bitHourlyRate
	,U.monHourlyRate
	,coalesce(U.bitSalary,false) AS bitSalary,
	U.bitPaidLeave as bitPaidLeave,
	U.fltPaidLeaveHrs as fltPaidLeaveHrs,
	U.fltPaidLeaveEmpHrs as fltPaidLeaveEmpHrs,
	U.numDailyHours as numDailyHours
	,U.bitOverTime as bitOverTime,
	U.numLimDailHrs as numLimDailHrs,
	U.bitManagerOfOwner,
	U.fltManagerOfOwner,
	U.bitCommOwner,
	U.fltCommOwner,
	U.bitCommAssignee,
	U.fltCommAssignee,
	U.monOverTimeRate as monOverTimeRate,
	U.bitMainComm as bitMainComm,
	U.fltMainCommPer as fltMainCommPer,
	U.bitRoleComm as bitRoleComm,
	(select count(*) from UserRoles where numUserCntID = U.numUserId)  as Roles,
	U.bitEmpNetAccount as bitEmpNetAccount,
	U.dtHired as dtHired,
	U.dtReleased  as dtReleased,
	coalesce(U.vcEmployeeId,'')  as vcEmployeeId,
	coalesce(U.vcEmergencyContact,'')  as vcEmergencyContact,
	U.dtSalaryDateTime as dtSalaryDatetime,
	U.bitpayroll as bitPayroll,
	ACI.numContactId as numUserCntId,
	U.fltEmpRating as fltEmpRating,
	U.dtEmpRating as dtEmpRating,
	U.monNetPay as monNetPay,
	U.monAmtWithheld as monAmtWithheld,
	U.bitOverTimeHrsDailyOrWeekly as bitOverTimeHrsDailyOrWeekly
   FROM UserMaster U
   join Domain D on U.numDomainID = D.numDomainId
   Left outer join AdditionalContactsInformation ACI on U.numUserDetailId = ACI.numContactId
   Left outer join Listdetails LD on ACI.vcDepartment = LD.numListItemID And LD.numListID = 19
   LEFT OUTER JOIN AddressDetails AD ON AD.numRecordID = ACI.numContactId AND AD.tintAddressOf = 1 AND AD.tintAddressType = 0 AND AD.bitIsPrimary = true AND AD.numDomainID = ACI.numDomainID
   Where U.numUserId = v_numUserID;
END; $$;
