-- Stored procedure definition script USP_ManageFinancialYear for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFinancialYear(INOUT v_numFinYearId NUMERIC(9,0) DEFAULT 0 ,
    v_numDomainId NUMERIC(9,0) DEFAULT NULL,
    v_dtPeriodFrom TIMESTAMP DEFAULT NULL,
    v_dtPeriodTo TIMESTAMP DEFAULT NULL,
    v_vcFinYearDesc VARCHAR(50) DEFAULT NULL,
    v_bitCloseStatus BOOLEAN DEFAULT NULL,
    v_bitAuditStatus BOOLEAN DEFAULT NULL,
    v_bitCurrentYear BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_bitCurrentYear = true then
        
      UPDATE  FinancialYear
      SET     bitCurrentYear = false
      WHERE   numDomainId = v_numDomainId;
   end if;
   IF v_numFinYearId = 0 then
        
      INSERT  INTO FinancialYear(numDomainId,
                      dtPeriodFrom,
                      dtPeriodTo,
                      vcFinYearDesc,
                      bitCloseStatus,
                      bitAuditStatus,
                      bitCurrentYear)
            VALUES(v_numDomainId,
                      v_dtPeriodFrom,
                      v_dtPeriodTo,
                      v_vcFinYearDesc,
                      v_bitCloseStatus,
                      v_bitAuditStatus,
                      v_bitCurrentYear);
            
      v_numFinYearId := CURRVAL('FinancialYear_seq');
   ELSE
      UPDATE  FinancialYear
      SET     numDomainId = v_numDomainId,dtPeriodFrom = v_dtPeriodFrom,dtPeriodTo = v_dtPeriodTo,
      vcFinYearDesc = v_vcFinYearDesc,
                    bitCurrentYear = v_bitCurrentYear
      WHERE   numFinYearId = v_numFinYearId;
      open SWV_RefCur for
      SELECT  v_numFinYearId;
   end if;

   RETURN;
END; $$;


