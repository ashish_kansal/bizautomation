-- Stored procedure definition script USP_TimeAndExpense_GetTimeEntriesDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TimeAndExpense_GetTimeEntriesDetail(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID INTEGER
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT *
		,v_numUserCntID AS numUserCntID
   FROM
   fn_GetPayrollEmployeeTime(v_numDomainID,v_numUserCntID,v_dtFromDate,v_dtToDate,v_ClientTimeZoneOffset)
   ORDER BY
   dtDate;
END; $$;












