-- Function definition script GetTabViewStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTabViewStatus(v_numTabID NUMERIC(9,0),v_numGroupID NUMERIC(9,0),v_numRelationShip NUMERIC(9,0))
RETURNS Boolean LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Allow  Boolean;
   v_tintGroupType  SMALLINT;
BEGIN
   select   tintGroupType INTO v_tintGroupType from AuthenticationGroupMaster where numGroupID = v_numGroupID;
   if v_tintGroupType = 2 then 
      select   bitallowed INTO v_Allow from GroupTabDetails where numGroupID = v_numGroupID and numTabId = v_numTabID and numRelationShip = v_numRelationShip;
   else 
      select   bitallowed INTO v_Allow from GroupTabDetails where numGroupID = v_numGroupID and numTabId = v_numTabID;
   end if;
	
   if v_Allow is null then 
      v_Allow := 0;
   end if;

   RETURN v_Allow;
END; $$;

