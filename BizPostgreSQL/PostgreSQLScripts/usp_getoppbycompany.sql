-- Stored procedure definition script USP_GetOppbyCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOppbyCompany(v_numDivisionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numOppId,vcpOppName from OpportunityMaster where numDivisionId = v_numDivisionID and tintoppstatus <> 2;
END; $$;












