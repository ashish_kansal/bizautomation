-- Stored procedure definition script USP_GetWorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetWorkOrder(v_numDomainID NUMERIC(18,0),
v_ClientTimeZoneOffset  INTEGER,
v_numWOStatus NUMERIC(18,0),
v_SortExpression VARCHAR(50),
v_CurrentPage INTEGER,                                                                            
v_PageSize INTEGER,
v_numUserCntID NUMERIC(9,0),
v_vcWOId  VARCHAR(4000),
v_tintFilter SMALLINT DEFAULT 0,
v_numOppID NUMERIC(18,0) DEFAULT NULL,
v_numOppItemID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempWOID  NUMERIC(18,0);
   v_TOTALROWCOUNT  NUMERIC(18,0);
BEGIN
   v_vcWOId := coalesce(v_vcWOId,'');
	
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;   
    
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      ItemLevel INTEGER, 
      numParentWOID NUMERIC(18,0), 
      numWOID NUMERIC(18,0), 
      ID VARCHAR(1000), 
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(500), 
      numQty DOUBLE PRECISION, 
      numWarehouseItemID NUMERIC(18,0), 
      vcWareHouse VARCHAR(200), 
      numOnHand DOUBLE PRECISION,
      numOnOrder DOUBLE PRECISION, 
      numAllocation DOUBLE PRECISION, 
      numBackOrder DOUBLE PRECISION, 
      vcInstruction VARCHAR(2000),
      numAssignedTo NUMERIC(18,0), 
      vcAssignedTo VARCHAR(500), 
      numCreatedBy NUMERIC(18,0), 
      vcCreated VARCHAR(500),
      bintCompliationDate TIMESTAMP, 
      numWOStatus NUMERIC(18,0), 
      numOppID NUMERIC(18,0), 
      vcOppName VARCHAR(1000),
      numPOID NUMERIC(18,0), 
      vcPOName VARCHAR(1000), 
      bitWorkOrder BOOLEAN, 
      bitReadyToBuild BOOLEAN
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWO_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWO CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWO
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0)
   );

   DROP TABLE IF EXISTS tt_TEMPWORKORDER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWORKORDER
   (
      ItemLevel INTEGER, 
      RowNumber INTEGER, 
      numParentWOID NUMERIC(18,0), 
      numWOID NUMERIC(18,0), 
      ID VARCHAR(1000), 
      numItemCode NUMERIC(18,0),
      numOppID NUMERIC(18,0), 
      numWarehouseItemID NUMERIC(18,0), 
      numQty DOUBLE PRECISION, 
      numWOStatus NUMERIC(18,0), 
      vcInstruction VARCHAR(2000),
      numAssignedTo NUMERIC(18,0), 
      bintCreatedDate TIMESTAMP, 
      bintCompliationDate TIMESTAMP, 
      numCreatedBy NUMERIC(18,0),
      bitWorkOrder BOOLEAN, 
      bitReadyToBuild BOOLEAN
   ); 

   INSERT INTO
   tt_TEMPWORKORDER
   SELECT
   1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',coalesce(numWOId,cast('0' as NUMERIC)),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS DOUBLE PRECISION),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		true AS bitWorkOrder,
		(CASE WHEN WorkOrder.numWOStatus = 23184 THEN true ELSE false END) AS bitReadyToBuild
   FROM
   WorkOrder
   WHERE
   numDomainId = v_numDomainID
   AND (numOppID = v_numOppID OR coalesce(v_numOppID,0) = 0)
   AND (numOppItemID = v_numOppItemID OR coalesce(v_numOppItemID,0) = 0)
   AND 1 =(CASE WHEN coalesce(v_numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = v_numWOStatus THEN 1 ELSE 0 END END)
   AND 1 =(Case when v_numUserCntID > 0 then case when coalesce(numAssignedTo,0) = v_numUserCntID then 1 else 0 end else 1 end)
   AND 1 =(Case when LENGTH(v_vcWOId) > 0 then Case when numWOId in (SELECT Id from SplitIds(v_vcWOId,',')) then 1 else 0 end else 1 end)
   AND (coalesce(numParentWOID,0) = 0 OR numParentWOID NOT IN(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID))
   ORDER BY
   bintCreatedDate DESC,numWOId ASC;

   INSERT
   INTO tt_TEMP(ItemLevel, numParentWOID, numWOID, ID, numItemCode, vcItemName, numQty, numWarehouseItemID, vcWareHouse, numOnHand, numOnOrder, numAllocation, numBackOrder, vcInstruction, numAssignedTo, vcAssignedTo, numCreatedBy, vcCreated, bintCompliationDate, numWOStatus, numOppID, vcOppName, numPOID, vcPOName, bitWorkOrder, bitReadyToBuild)   with recursive CTEWorkOrder(ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,
   vcInstruction,numAssignedTo,bintCreatedDate,numCreatedBy,bintCompliationDate,
   numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS(SELECT
   ItemLevel AS ItemLevel,
			numParentWOID AS numParentWOID,
			numWOId AS numWOID,
			ID AS ID,
			numItemCode AS numItemKitID,
			numQty AS numQtyItemsReq,
			numWareHouseItemId AS numWarehouseItemID,
			vcInstruction AS vcInstruction,
			numAssignedTo AS numAssignedTo,
			bintCreatedDate AS bintCreatedDate,
			numCreatedBy AS numCreatedBy,
			bintCompliationDate AS bintCompliationDate,
			numWOStatus AS numWOStatus,
			numOppId AS numOppID,
			true AS bitWorkOrder,
			bitReadyToBuild AS bitReadyToBuild
   FROM
   tt_TEMPWORKORDER t
   UNION ALL
   SELECT
   c.ItemLevel+2 AS ItemLevel,
			c.numWOID AS numParentWOID,
			WorkOrder.numWOId AS numWOID,
			CAST(CONCAT(c.ID,'-','#',coalesce(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
			WorkOrder.numItemCode AS numItemKitID,
			CAST(WorkOrder.numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
			WorkOrder.numWareHouseItemId AS numWarehouseItemID,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)) AS vcInstruction,
			WorkOrder.numAssignedTo AS numAssignedTo,
			WorkOrder.bintCreatedDate AS bintCreatedDate,
			WorkOrder.numCreatedBy AS numCreatedBy,
			WorkOrder.bintCompliationDate AS bintCompliationDate,
			WorkOrder.numWOStatus AS numWOStatus,
			WorkOrder.numOppId AS numOppID,
			true AS bitWorkOrder,
			(CASE WHEN WorkOrder.numWOStatus = 23184 THEN true ELSE false END) AS bitReadyToBuild
   FROM
   WorkOrder
   INNER JOIN
   CTEWorkOrder c
   ON
   WorkOrder.numParentWOID = c.numWOID) SELECT
   ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numonOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		CAST(fn_GetContactName(coalesce(CTEWorkOrder.numAssignedTo,0)) AS VARCHAR(500)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(coalesce(fn_GetContactName(CTEWorkOrder.numCreatedBy),'&nbsp;&nbsp;-') || ',' || CAST(CTEWorkOrder.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS VARCHAR(500)) ,
		bintCompliationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		CAST(OpportunityMaster.vcpOppName AS VARCHAR(1000)),
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
   FROM
   CTEWorkOrder
   INNER JOIN
   Item
   ON
   CTEWorkOrder.numItemKitID = Item.numItemCode
   LEFT JOIN
   WareHouseItems
   ON
   CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   OpportunityMaster
   ON
   CTEWorkOrder.numOppID = OpportunityMaster.numOppId;

   INSERT INTO
   tt_TEMP(ItemLevel, numParentWOID, numWOID, ID, numItemCode, vcItemName, numQty, numWarehouseItemID, vcWareHouse, numOnHand, numOnOrder, numAllocation, numBackOrder, vcInstruction, numAssignedTo, vcAssignedTo, numCreatedBy, vcCreated, bintCompliationDate, numWOStatus, numOppID, vcOppName, numPOID, vcPOName, bitWorkOrder, bitReadyToBuild)
   SELECT
   t1.ItemLevel::bigint+1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numonOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		bintCompliationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		CAST(OpportunityMaster.vcpOppName AS VARCHAR(1000)),
		false AS bitWorkOrder,
		CAST(CASE
   WHEN Item.charItemType = 'P' AND coalesce(t1.bitReadyToBuild,false) = false
   THEN
      CASE
      WHEN(CASE WHEN coalesce(t1.numOppID,0) > 0 THEN 1 ELSE 0 END) = 1 AND v_tintCommitAllocation = 2
      THEN(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
      ELSE(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END)
      END
   ELSE 1
   END AS BOOLEAN)
   FROM
   WorkOrderDetails
   INNER JOIN
   tt_TEMP t1
   ON
   WorkOrderDetails.numWOId = t1.numWOID
   INNER JOIN
   Item
   ON
   WorkOrderDetails.numChildItemID = Item.numItemCode
   AND 1 =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMP TInner WHERE TInner.bitWorkOrder = true AND TInner.numParentWOID = t1.numWOID AND TInner.numItemCode = Item.numItemCode) > 0 THEN 0 ELSE 1 END)
   LEFT JOIN
   WareHouseItems
   ON
   WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   OpportunityMaster
   ON
   WorkOrderDetails.numPOID = OpportunityMaster.numOppId;

   INSERT INTO tt_TEMPWO(numWOID) SELECT numWOID FROM tt_TEMP WHERE bitWorkOrder = true ORDER BY ItemLevel DESC;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPWO;

   WHILE v_i <= v_iCount LOOP
      select   numWOID INTO v_numTempWOID FROM tt_TEMPWO WHERE cast(NULLIF(ID,0) as INTEGER) = v_i;
      UPDATE
      tt_TEMP T1
      SET
      bitReadyToBuild =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMP TInner WHERE TInner.numParentWOID = T1.numWOID AND bitReadyToBuild = false) > 0 THEN false ELSE true END)
		
      WHERE
      numWOID = v_numTempWOID;
      v_i := v_i::bigint+1;
   END LOOP;

   If v_tintFilter = 2 then --Buildable Orders
	
      DELETE FROM tt_TEMP WHERE 1 =(CASE WHEN bitReadyToBuild = true THEN 0 ELSE 1 END) AND bitWorkOrder = true;
   ELSEIF v_tintFilter = 3
   then --Un-Buildable Orders
	
      DELETE FROM tt_TEMP WHERE 1 =(CASE WHEN bitReadyToBuild = true THEN 1 ELSE 0 END) AND bitWorkOrder = true;
   end if;

   DROP TABLE IF EXISTS tt_TEMPPAGINGID CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPAGINGID
   (
      ID INTEGER,
      numWOID NUMERIC(18,0),
      numParentWOID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMPPAGINGID(ID
		,numWOID
		,numParentWOID)
   SELECT
   numID
		,numWOID
		,numParentWOID
   FROM
   tt_TEMP
   WHERE
   coalesce(numParentWOID,0) = 0
   ORDER BY
   numWOID;

   open SWV_RefCur for
   with recursive CTEID(numID,numWOID,numParentWOID) AS(SELECT
   ID AS numID
			,numWOID AS numWOID
			,numParentWOID AS numParentWOID
   FROM
   tt_TEMPPAGINGID
   UNION ALL
   SELECT
			T.numID AS numID
			,T.numWOID AS numWOID
			,T.numParentWOID AS numParentWOID
   FROM
   tt_TEMP T
   INNER JOIN
   CTEID C
   ON
   T.numParentWOID = C.numWOID
   WHERE
   coalesce(T.numParentWOID,0) > 0)
   SELECT 
		T1.numID AS "numID",
		T1.ItemLevel AS "ItemLevel", 
		T1.numParentWOID AS "numParentWOID", 
		T1.numWOID AS "numWOID", 
		T1.ID AS "ID", 
		T1.numItemCode AS "numItemCode",
		T1.vcItemName AS "vcItemName", 
		T1.numQty AS "numQty", 
		T1.numWarehouseItemID AS "numWarehouseItemID", 
		T1.vcWareHouse AS "vcWareHouse", 
		T1.numOnHand AS "numOnHand",
		T1.numOnOrder AS "numOnOrder", 
		T1.numAllocation AS "numAllocation", 
		T1.numBackOrder AS "numBackOrder", 
		T1.vcInstruction AS "vcInstruction",
		T1.numAssignedTo AS "numAssignedTo", 
		T1.vcAssignedTo AS "vcAssignedTo", 
		T1.numCreatedBy AS "numCreatedBy", 
		T1.vcCreated AS "vcCreated",
		T1.bintCompliationDate AS "bintCompliationDate", 
		T1.numWOStatus AS "numWOStatus", 
		T1.numOppID AS "numOppID", 
		T1.vcOppName AS "vcOppName",
		T1.numPOID AS "numPOID", 
		T1.vcPOName AS "vcPOName", 
		T1.bitWorkOrder AS "bitWorkOrder", 
		T1.bitReadyToBuild AS "bitReadyToBuild"
   FROM tt_TEMP T1 INNER JOIN CTEID ON T1.numID = CTEID.numID ORDER BY T1.numParentWOID,T1.ItemLevel;

   select   COUNT(*) INTO v_TOTALROWCOUNT FROM tt_TEMP WHERE coalesce(numParentWOID,0) = 0;

   open SWV_RefCur2 for
   SELECT v_TOTALROWCOUNT AS TotalRowCount;
   RETURN;
END; $$;


