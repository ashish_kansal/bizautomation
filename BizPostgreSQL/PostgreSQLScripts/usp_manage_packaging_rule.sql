-- Stored procedure definition script USP_MANAGE_PACKAGING_RULE for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_MANAGE_PACKAGING_RULE(v_numPackagingRuleID NUMERIC(18,0),
	v_vcRuleName VARCHAR(1000),
	v_numPackagingTypeID NUMERIC(18,0),
	v_numFromQty NUMERIC(18,0),
	v_numShipClassId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numShippingRuleID NUMERIC(18,0)--added by sachin ::Date-31stJuly||To consolidate packaging n shipping rule
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT 'col1' FROM PackagingRules WHERE vcRuleName = v_vcRuleName AND numPackagingRuleID <> v_numPackagingRuleID AND numDomainID = v_numDomainID) then
	
		 -- Message text.
					 -- Severity.
					 -- State.
      RAISE EXCEPTION 'ERROR: Rule Name already exists. Please provide another Rule Name.';
      RETURN;
   end if;

   IF EXISTS(SELECT * FROM PackagingRules WHERE numPackagingTypeID = v_numPackagingTypeID
   AND numShipClassId = v_numShipClassId
   AND numDomainID = v_numDomainID
   AND numFromQty = v_numFromQty
   AND numPackagingRuleID <> v_numPackagingRuleID) then
	
		 -- Message text.
				    -- Severity.
				    -- State.
      RAISE EXCEPTION 'ERROR: Rule Detail already exists in another rule. Please use existing rule OR create a new rule detail.';
      RETURN;
   end if;
			
   IF EXISTS(SELECT numPackagingRuleID FROM PackagingRules WHERE numPackagingRuleID = v_numPackagingRuleID) then

      UPDATE PackagingRules SET
      vcRuleName = v_vcRuleName,numPackagingTypeID = v_numPackagingTypeID,numFromQty = v_numFromQty,
      numShipClassId = v_numShipClassId,numDomainID = v_numDomainID,
      numShippingRuleID = v_numShippingRuleID
      WHERE
      numPackagingRuleID = v_numPackagingRuleID;
   ELSE
      INSERT INTO PackagingRules(vcRuleName,
		numPackagingTypeID,
		numFromQty,
		numShipClassId,
		numDomainID,
		numShippingRuleID) VALUES(v_vcRuleName,
		v_numPackagingTypeID,
		v_numFromQty,
		v_numShipClassId,
		v_numDomainID,
		v_numShippingRuleID);
   end if;
END; $$;




