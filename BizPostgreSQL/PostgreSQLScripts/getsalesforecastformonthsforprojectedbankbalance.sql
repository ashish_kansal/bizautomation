-- Function definition script GetSalesForeCastForMonthsForProjectedBankBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetSalesForeCastForMonthsForProjectedBankBalance(v_intType INTEGER,v_month INTEGER,v_year INTEGER,v_numDomainId NUMERIC(9,0),v_numUserCntID NUMERIC(9,0),v_sintForecast INTEGER)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monOppAmt  DECIMAL(20,5);                
   v_TotalPercentage  DOUBLE PRECISION;                
   v_TotalAmt  DECIMAL(20,5);                
   v_UnclosedDealCount  INTEGER;                
   v_Percentage  DOUBLE PRECISION;                
   v_currentYear  INTEGER;
BEGIN
   v_TotalAmt := 0;
   if v_intType = 0 then
      v_currentYear := EXTRACT(year FROM TIMEZONE('UTC',now()));
   ELSEIF v_intType = -1
   then
      v_currentYear := EXTRACT(year FROM TIMEZONE('UTC',now())) -1;
   ELSEIF v_intType = 1
   then
      v_currentYear := EXTRACT(year FROM TIMEZONE('UTC',now()))+1;
   end if;  
           
   if v_sintForecast = 1 then

      select   sum(coalesce(monForecastAmount,0)) INTO v_TotalAmt from Forecast FC Where tintForecastType = 1     
    --(sintYear<@year or (tintmonth<=@Month and sintYear=@Year))     
      and ((tintMonth between EXTRACT(month FROM TIMEZONE('UTC',now())) And(case when v_currentYear = v_year then v_month else 0 end) And  sintYear = v_year)
      or (tintMonth between 1 and(case when v_currentYear < v_year then  v_month else 0 end) and sintYear = v_year) Or (tintMonth between EXTRACT(month FROM TIMEZONE('UTC',now())) and(case when v_currentYear < v_year then  12 else 0 end)  And sintYear = v_year::bigint -1))
      and numDomainID = v_numDomainId;
   Else
      select   Sum(coalesce(monPAmount,0)) INTO v_monOppAmt From OpportunityMaster Where tintopptype = 1 And tintoppstatus <> 2 and  numrecowner = v_numUserCntID And tintoppstatus = 0        
---- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))      
      and ((EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) And(case when v_currentYear = v_year then v_month else 0 end) And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year)
      or (EXTRACT(month FROM intPEstimatedCloseDate) between 1 and(case when v_currentYear < v_year then  v_month else 0 end) and EXTRACT(Year FROM intPEstimatedCloseDate) = v_year) Or (EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) and(case when v_currentYear < v_year then  12
      else 0 end)  And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year::bigint -1))
      and numDomainId = v_numDomainId;
      select   sum(coalesce(tintPercentage,0)) INTO v_TotalPercentage from OpportunityMaster Opp
      inner join  OpportunityStageDetails OSD on Opp.numOppId = OSD.numoppid Where tintopptype = 1 And  bitstagecompleted = true and tintoppstatus <> 2 and  Opp.numrecowner = v_numUserCntID  And Opp.tintoppstatus = 0                           
---- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))      
      and ((EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) And(case when v_currentYear = v_year then v_month else 0 end) And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year)
      or (EXTRACT(month FROM intPEstimatedCloseDate) between 1 and(case when v_currentYear < v_year then  v_month else 0 end) and EXTRACT(Year FROM intPEstimatedCloseDate) = v_year) Or (EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) and(case when v_currentYear < v_year then  12
      else 0 end)  And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year::bigint -1))
      and Opp.numDomainId = v_numDomainId;
      select   Count(*) INTO v_UnclosedDealCount From OpportunityMaster where tintopptype = 1 And tintoppstatus <> 2 and  numrecowner = v_numUserCntID And tintoppstatus = 0                           
-- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))     
      and ((EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) And(case when v_currentYear = v_year then v_month else 0 end) And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year)
      or (EXTRACT(month FROM intPEstimatedCloseDate) between 1 and(case when v_currentYear < v_year then  v_month else 0 end) and EXTRACT(Year FROM intPEstimatedCloseDate) = v_year) Or (EXTRACT(month FROM intPEstimatedCloseDate) between EXTRACT(month FROM TIMEZONE('UTC',now())) and(case when v_currentYear < v_year then  12
      else 0 end)  And EXTRACT(Year FROM intPEstimatedCloseDate) = v_year::bigint -1))
      and numDomainId = v_numDomainId;
      v_Percentage :=(Case When v_TotalPercentage = 0 then 1 Else v_TotalPercentage End)/(Case When v_UnclosedDealCount = 0 then 1 Else v_UnclosedDealCount End);                
 ----Print @UnclosedDealCount                
 ----Print @monOppAmt                
 ----Print @Percentage                
 --Print @Percentage                
      v_TotalAmt := coalesce(v_monOppAmt,0)*v_Percentage/100;
   end if;            
             
--Print @TotalAmt                
   Return v_TotalAmt;
END; $$;

