-- Function definition script SWF_Xml_RemoveDocument for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SWF_Xml_RemoveDocument(integer)
RETURNS boolean
AS 'libispgxml'
LANGUAGE C STRICT VOLATILE

