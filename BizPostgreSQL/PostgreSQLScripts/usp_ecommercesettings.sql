DROP FUNCTION IF EXISTS USP_EcommerceSettings;

CREATE OR REPLACE FUNCTION USP_EcommerceSettings(v_numDomainID NUMERIC(9,0),              
v_vcPaymentGateWay INTEGER ,                
v_vcPGWUserId VARCHAR(100) DEFAULT '' ,                
v_vcPGWPassword VARCHAR(100) DEFAULT NULL,              
v_bitShowInStock BOOLEAN DEFAULT NULL,              
v_bitShowQOnHand BOOLEAN DEFAULT NULL,
v_bitCheckCreditStatus BOOLEAN DEFAULT NULL,
v_numDefaultWareHouseID NUMERIC(9,0) DEFAULT NULL,
v_numRelationshipId NUMERIC(9,0) DEFAULT NULL,
v_numProfileId NUMERIC(9,0) DEFAULT NULL,
v_bitHidePriceBeforeLogin	BOOLEAN DEFAULT NULL,
v_bitAuthOnlyCreditCard BOOLEAN DEFAULT false,
v_bitSendMail BOOLEAN DEFAULT true,
v_vcGoogleMerchantID VARCHAR(1000)  DEFAULT '',
v_vcGoogleMerchantKey VARCHAR(1000) DEFAULT '',
v_IsSandbox BOOLEAN DEFAULT NULL ,
v_numAuthrizedOrderStatus NUMERIC DEFAULT NULL,
v_numSiteID NUMERIC(18,0) DEFAULT NULL,
v_vcPaypalUserName VARCHAR(50)  DEFAULT '',
v_vcPaypalPassword VARCHAR(50)  DEFAULT '',
v_vcPaypalSignature VARCHAR(500)  DEFAULT '',
v_IsPaypalSandbox BOOLEAN DEFAULT NULL,
v_bitSkipStep2 BOOLEAN DEFAULT false,
v_bitDisplayCategory BOOLEAN DEFAULT false,
v_bitShowPriceUsingPriceLevel BOOLEAN DEFAULT false,
v_bitEnableSecSorting BOOLEAN DEFAULT false,
v_bitSortPriceMode   BOOLEAN DEFAULT false,
v_numPageSize    INTEGER DEFAULT 15,
v_numPageVariant  INTEGER DEFAULT 6,
v_bitAutoSelectWarehouse BOOLEAN DEFAULT false,
v_bitPreSellUp BOOLEAN DEFAULT false, 
v_bitPostSellUp BOOLEAN DEFAULT false, 
v_numDefaultClass NUMERIC(18,0) DEFAULT 0,
v_vcPreSellUp VARCHAR(200) DEFAULT NULL,
v_vcPostSellUp VARCHAR(200) DEFAULT NULL,
v_vcRedirectThankYouUrl VARCHAR(300) DEFAULT NULL,
v_tintPreLoginProceLevel SMALLINT DEFAULT 0,
v_bitHideAddtoCart BOOLEAN DEFAULT true,
v_bitShowPromoDetailsLink BOOLEAN DEFAULT true,
v_tintWarehouseAvailability SMALLINT DEFAULT 1,
v_bitDisplayQtyAvailable BOOLEAN DEFAULT false,
v_vcSalesOrderTabs VARCHAR(200) DEFAULT '',
v_vcSalesQuotesTabs VARCHAR(200) DEFAULT '',
v_vcItemPurchaseHistoryTabs VARCHAR(200) DEFAULT '',
v_vcItemsFrequentlyPurchasedTabs VARCHAR(200) DEFAULT '',
v_vcOpenCasesTabs VARCHAR(200) DEFAULT '',
v_vcOpenRMATabs VARCHAR(200) DEFAULT '',
v_bitSalesOrderTabs BOOLEAN DEFAULT false,
v_bitSalesQuotesTabs BOOLEAN DEFAULT false,
v_bitItemPurchaseHistoryTabs BOOLEAN DEFAULT false,
v_bitItemsFrequentlyPurchasedTabs BOOLEAN DEFAULT false,
v_bitOpenCasesTabs BOOLEAN DEFAULT false,
v_bitOpenRMATabs BOOLEAN DEFAULT false,
v_bitSupportTabs BOOLEAN DEFAULT false,
v_vcSupportTabs VARCHAR(200) DEFAULT '',
v_bitElasticSearch BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE 
		v_numTempSiteID NUMERIC(18,0);
BEGIN
   IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID = v_numDomainID AND numSiteId = v_numSiteID) then
	
      INSERT INTO eCommerceDTL(numDomainID,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            numRelationshipId,
            numProfileId,
            bitHidePriceBeforeLogin,
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			bitPreSellUp,
			bitPostSellUp,
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable,
			bitElasticSearch)
		VALUES(v_numDomainID,
            v_vcPaymentGateWay,
            v_vcPGWUserId,
            v_vcPGWPassword,
            v_bitShowInStock,
            v_bitShowQOnHand,
            v_numDefaultWareHouseID,
            v_bitCheckCreditStatus,
            v_numRelationshipId,
            v_numProfileId,
            v_bitHidePriceBeforeLogin,
            v_bitAuthOnlyCreditCard,
            v_bitSendMail,
            v_vcGoogleMerchantID,
            v_vcGoogleMerchantKey ,
            v_IsSandbox,
            v_numAuthrizedOrderStatus,
			v_numSiteID,
			v_vcPaypalUserName,
			v_vcPaypalPassword,
			v_vcPaypalSignature,
			v_IsPaypalSandbox,
			v_bitSkipStep2,
			v_bitDisplayCategory,
			v_bitShowPriceUsingPriceLevel,
			v_bitEnableSecSorting,
			v_bitSortPriceMode,
			v_numPageSize,
			v_numPageVariant,
			v_bitAutoSelectWarehouse,
			v_bitPreSellUp,
			v_bitPostSellUp,
			v_numDefaultClass,
			v_vcPreSellUp,
			v_vcPostSellUp,
			v_vcRedirectThankYouUrl,
			v_tintPreLoginProceLevel,
			v_bitHideAddtoCart,
			v_bitShowPromoDetailsLink,
			v_tintWarehouseAvailability,
			v_bitDisplayQtyAvailable,
			v_bitElasticSearch) RETURNING numSiteID INTO v_numTempSiteID;
		
      IF(v_bitElasticSearch = true) then
		
         IF(NOT EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId <> v_numSiteID AND numDomainID = v_numDomainID AND bitElasticSearch = true)) then
			
            INSERT INTO ElasticSearchBizCart(numDomainID
					,vcValue
					,vcAction
					,vcModule
					--,numSiteID
				)
            SELECT
            v_numDomainID
					,numItemID
					,'Insert'
					,'Item'
					--,@numSiteID  
            FROM
            SiteCategories
            INNER JOIN
            ItemCategory
            ON
            SiteCategories.numCategoryID = ItemCategory.numCategoryID
            INNER JOIN
            Item
            ON
            ItemCategory.numItemID = Item.numItemCode
            WHERE
            SiteCategories.numSiteID = v_numSiteID
            AND coalesce(Item.IsArchieve,0) = 0
            GROUP BY
            numItemID;
         end if;
      end if;
      IF coalesce((SELECT numDefaultSiteID FROM Domain WHERE numDomainId = v_numDomainID),0) = 0 then
		
         UPDATE Domain SET numDefaultSiteID = v_numTempSiteID WHERE numDomainId = v_numDomainID;
      end if;
   ELSE
      IF(v_bitElasticSearch = true) then
		
         INSERT INTO ElasticSearchBizCart(numDomainID
				,vcValue
				,vcAction
				,vcModule
				--,numSiteID
			)
         SELECT
         v_numDomainID
				,numItemID
				,'Insert'
				,'Item'
				--,@numSiteID  
         FROM
         SiteCategories
         INNER JOIN
         ItemCategory
         ON
         SiteCategories.numCategoryID = ItemCategory.numCategoryID
         INNER JOIN
         Item
         ON
         ItemCategory.numItemID = Item.numItemCode
         WHERE
         SiteCategories.numSiteID = v_numSiteID
         AND coalesce(Item.IsArchieve,0) = 0
         GROUP BY
         numItemID;
      end if;
		--ELSE IF(EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId = @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
		--BEGIN
		--	DELETE FROM ElasticSearchBizCart WHERE numDomainID = @numDomainID --And numSiteID = @numSiteID
		--END	

      UPDATE
      eCommerceDTL
      SET
      vcPaymentGateWay = v_vcPaymentGateWay,vcPGWManagerUId = v_vcPGWUserId,vcPGWManagerPassword = v_vcPGWPassword,
      bitShowInStock = v_bitShowInStock,bitShowQOnHand = v_bitShowQOnHand,
      bitCheckCreditStatus = v_bitCheckCreditStatus,
      bitHidePriceBeforeLogin = v_bitHidePriceBeforeLogin,bitAuthOnlyCreditCard = v_bitAuthOnlyCreditCard,
      bitSendEmail = v_bitSendMail,vcGoogleMerchantID = v_vcGoogleMerchantID,
      vcGoogleMerchantKey =  v_vcGoogleMerchantKey,
      IsSandbox = v_IsSandbox,numAuthrizedOrderStatus = v_numAuthrizedOrderStatus,
      numSiteId = v_numSiteID,vcPaypalUserName = v_vcPaypalUserName,
      vcPaypalPassword = v_vcPaypalPassword,vcPaypalSignature = v_vcPaypalSignature,
      IsPaypalSandbox = v_IsPaypalSandbox,bitSkipStep2 = v_bitSkipStep2,
      bitDisplayCategory = v_bitDisplayCategory,bitShowPriceUsingPriceLevel = v_bitShowPriceUsingPriceLevel,
      bitEnableSecSorting = v_bitEnableSecSorting,
      bitSortPriceMode = v_bitSortPriceMode,numPageSize = v_numPageSize,
      numPageVariant = v_numPageVariant,bitAutoSelectWarehouse = v_bitAutoSelectWarehouse,
      bitPreSellUp = v_bitPreSellUp,bitPostSellUp = v_bitPostSellUp,
      vcPreSellUp = v_vcPreSellUp,vcPostSellUp = v_vcPostSellUp,vcRedirectThankYouUrl = v_vcRedirectThankYouUrl,
      bitHideAddtoCart = v_bitHideAddtoCart,
      bitShowPromoDetailsLink = v_bitShowPromoDetailsLink,tintWarehouseAvailability = v_tintWarehouseAvailability,
      bitDisplayQtyAvailable = v_bitDisplayQtyAvailable,
      bitElasticSearch = v_bitElasticSearch
      WHERE
      numDomainID = v_numDomainID
      AND numSiteId = v_numSiteID;
      IF(v_numDefaultWareHouseID > 0) then
		
         UPDATE
         eCommerceDTL
         SET
         numDefaultWareHouseID = v_numDefaultWareHouseID
         WHERE
         numDomainID = v_numDomainID
         AND numSiteId = v_numSiteID;
      end if;
      IF(v_numRelationshipId > 0) then
		
         UPDATE
         eCommerceDTL
         SET
         numRelationshipId = v_numRelationshipId
         WHERE
         numDomainID = v_numDomainID
         AND numSiteId = v_numSiteID;
      end if;
      IF(v_numDefaultClass > 0) then
		
         UPDATE
         eCommerceDTL
         SET
         numDefaultClass = v_numDefaultClass
         WHERE
         numDomainID = v_numDomainID
         AND numSiteId = v_numSiteID;
      end if;
      IF(v_numProfileId > 0) then
		
         UPDATE
         eCommerceDTL
         SET
         numProfileId = v_numProfileId
         WHERE
         numDomainID = v_numDomainID
         AND numSiteId = v_numSiteID;
      end if;
      IF(v_tintPreLoginProceLevel > 0) then
		
         UPDATE
         eCommerceDTL
         SET
         tintPreLoginProceLevel = v_tintPreLoginProceLevel
         WHERE
         numDomainID = v_numDomainID
         AND numSiteId = v_numSiteID;
      end if;
   end if;

   UPDATE
   Domain
   SET
   vcSalesOrderTabs = v_vcSalesOrderTabs,vcSalesQuotesTabs = v_vcSalesQuotesTabs,
   vcItemPurchaseHistoryTabs = v_vcItemPurchaseHistoryTabs,vcItemsFrequentlyPurchasedTabs = v_vcItemsFrequentlyPurchasedTabs,
   vcOpenCasesTabs = v_vcOpenCasesTabs,
   vcOpenRMATabs = v_vcOpenRMATabs,vcSupportTabs = v_vcSupportTabs,
   bitSalesOrderTabs = v_bitSalesOrderTabs,bitSalesQuotesTabs = v_bitSalesQuotesTabs,
   bitItemPurchaseHistoryTabs = v_bitItemPurchaseHistoryTabs,
   bitItemsFrequentlyPurchasedTabs = v_bitItemsFrequentlyPurchasedTabs,bitOpenCasesTabs = v_bitOpenCasesTabs,
   bitOpenRMATabs = v_bitOpenRMATabs,bitSupportTabs = v_bitSupportTabs
   WHERE
   numDomainId = v_numDomainID;
   RETURN;
END; $$;


