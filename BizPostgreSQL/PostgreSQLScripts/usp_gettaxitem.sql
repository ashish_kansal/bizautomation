-- Stored procedure definition script USP_GetTaxItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTaxItem(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcTaxName as "vcTaxName",numTaxItemID as "numTaxItemID",numChartOfAcntID as "numChartOfAcntID",C.vcAccountName as "vcAccountName" from TaxItems TI LEFT OUTER JOIN Chart_Of_Accounts C
   ON C.numAccountId = TI.numChartofAcntID
   Where TI.numDomainID = v_numDomainID;
END; $$;
