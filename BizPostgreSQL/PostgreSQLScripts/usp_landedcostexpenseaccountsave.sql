CREATE OR REPLACE FUNCTION USP_LandedCostExpenseAccountSave(v_numDomainId NUMERIC(18,0) ,
    v_strItems TEXT,
	v_vcLanedCostDefault TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strMsg  VARCHAR(200);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   -- BEGIN TRANSACTION
   BEGIN
UPDATE Domain SET vcLanedCostDefault = v_vcLanedCostDefault WHERE
   numDomainId = v_numDomainId;
   IF CAST(v_strItems AS TEXT) <> '' then
            
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP AS
		SELECT 
			* 
		FROM 
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				numLCExpenseId NUMERIC(18,0) PATH 'numLCExpenseId',
				vcDescription VARCHAR(500) PATH 'vcDescription',
				numAccountId NUMERIC(18,0) PATH 'numAccountId'
		) AS X;

      DELETE  FROM LandedCostExpenseAccount
      WHERE   numDomainID = v_numDomainId
      AND numLCExpenseId NOT IN(SELECT  numLCExpenseId
      FROM    tt_TEMP
      WHERE   numLCExpenseId > 0);
      UPDATE  LandedCostExpenseAccount LCEA
      SET     vcDescription = T.vcDescription,numAccountId = T.numAccountID
      FROM    tt_TEMP T
      WHERE   LCEA.numLCExpenseId = T.numLCExpenseId AND(T.numLCExpenseId > 0
      AND LCEA.numDomainID = v_numDomainId);
      INSERT  INTO LandedCostExpenseAccount(numDomainID ,
                          vcDescription ,
                          numAccountId)
      SELECT  v_numDomainId ,
                                vcDescription ,
                                numAccountID
      FROM    tt_TEMP
      WHERE   numLCExpenseId = 0;
   
   end if;
   -- COMMIT

/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
	END;
END; $$;


