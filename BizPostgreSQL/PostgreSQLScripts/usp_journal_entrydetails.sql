-- Stored procedure definition script USP_Journal_EntryDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Journal_EntryDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numJournalId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  GJD.numTransactionId AS TransactionId,
                GJD.numChartAcntId AS numChartAcntId,
                CA.numParntAcntTypeID AS numAcntType,
                GJD.numDebitAmt AS numDebitAmt,
                GJD.numCreditAmt AS numCreditAmt,
                GJD.varDescription AS varDescription,
                GJD.numCustomerId AS numCustomerId,
                GJD.numJournalId,
                FormatedDateFromDate(GJH.datEntry_Date,GJH.numDomainId) AS datEntry_Date,
				GJH.datEntry_Date AS datEntryDate,
                fn_GetRelationship_DivisionId(CI.numCompanyType,CI.numDomainID) || ' - ' || CAST(CI.numCompanyId AS VARCHAR(400)) AS varRelation,
                GJD.numCampaignID AS numCampaignID,
				coalesce(GJD.numProjectID,0) AS numProjectId, 
				coalesce(GJD.numClassID,0) AS numClassId,
                coalesce(GJD.bitReconcile,false) AS bitReconcile,
				coalesce(GJD.bitCleared,false) AS bitCleared,
				coalesce(GJD.numReconcileID,0) AS numReconcileID,
                GJH.numJournalReferenceNo,
                coalesce(GJD.chBizDocItems,'') AS chBizDocItems,
                GJH.varDescription AS vcNarration,
                coalesce(GJH.numOppId,0) AS numOppId,
				coalesce(GJH.numOppBizDocsId,0) AS numOppBizDocsId,
				coalesce(GJH.numDepositId,0) AS numDepositId,
                coalesce(GJH.numReturnID,0) AS numReturnID,
				coalesce(GJH.numCheckHeaderID,0) AS numCheckHeaderID,
				coalesce(GJH.numCategoryHDRID,0) AS numCategoryHDRID,
                coalesce(GJH.numBillID,0) AS numBillID,
				coalesce(GJH.numBillPaymentID,0) AS numBillPaymentID,
				coalesce(GJH.numPayrollDetailID,0) AS numPayrollDetailID
   FROM    General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts CA ON GJD.numChartAcntId = CA.numAccountId
   LEFT OUTER JOIN CompanyInfo CI ON GJD.numCustomerId = CI.numCompanyId
   WHERE   GJH.numDomainId = v_numDomainId
   AND GJH.numJOurnal_Id = v_numJournalId;
END; $$;












