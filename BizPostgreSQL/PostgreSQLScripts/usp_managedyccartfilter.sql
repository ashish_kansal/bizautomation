-- Stored procedure definition script usp_ManageDycCartFilter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION usp_ManageDycCartFilter(v_numFieldID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numFilterType INTEGER,
	v_bitCustomField BOOLEAN,
	v_tintOrder SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT numFieldID, numFormID, numSiteID FROM DycCartFilters WHERE numFieldID = v_numFieldID AND numFormID = v_numFormID AND numSiteID = v_numSiteID) then

      UPDATE DycCartFilters SET
      numDomainID = v_numDomainID,numFilterType = v_numFilterType,bitCustomField = v_bitCustomField,
      tintOrder = v_tintOrder
      WHERE
      numFieldID = v_numFieldID
      AND numFormID = v_numFormID
      AND numSiteID = v_numSiteID;
   ELSE
      INSERT INTO DycCartFilters(numFieldID,
		numFormID,
		numSiteID,
		numDomainID,
		numFilterType,
		bitCustomField,
		tintOrder) VALUES(v_numFieldID,
		v_numFormID,
		v_numSiteID,
		v_numDomainID,
		v_numFilterType,
		v_bitCustomField,
		v_tintOrder);
   end if;
   RETURN;
END; $$;


