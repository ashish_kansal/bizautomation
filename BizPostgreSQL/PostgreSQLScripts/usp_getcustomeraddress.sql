-- Stored procedure definition script usp_GetCustomerAddress for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCustomerAddress(v_numDivID NUMERIC
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
--	vcBillStreet, vcBillCity, vcBilState, vcBillPostCode, vcBillCountry, 
--		vcShipStreet, vcShipCity, vcShipState, vcShipPostCode, vcShipCountry,
   cast(coalesce(AD1.vcStreet,'') as VARCHAR(255)) as vcBillStreet,
  cast(coalesce(AD1.vcCity,'') as VARCHAR(255)) as vcBillCity,
  cast(coalesce(AD1.numState,0) as VARCHAR(255)) as vcBilState,
  cast(coalesce(AD1.vcPostalCode,'') as VARCHAR(255)) as vcBillPostCode,
  cast(coalesce(AD1.numCountry,0) as VARCHAR(255))  as vcBillCountry,
  cast(coalesce(AD2.vcStreet,'') as VARCHAR(255)) as vcShipStreet,
  cast(coalesce(AD2.vcCity,'') as VARCHAR(255)) as vcShipCity,
  cast(coalesce(AD2.numState,0) as VARCHAR(255))  as vcShipState,
  cast(coalesce(AD2.vcPostalCode,'') as VARCHAR(255)) as vcShipPostCode,
  cast(coalesce(AD2.numCountry,0) as VARCHAR(255)) AS vcShipCountry
   FROM DivisionMaster DM
   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
   LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
   AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
   WHERE numDivisionID = v_numDivID;
END; $$;












