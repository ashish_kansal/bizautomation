CREATE OR REPLACE FUNCTION USP_WorkOrder_GetMilestones(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		vcMileStoneName as "vcMileStoneName"
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',coalesce(stagepercentagemaster.numstagepercentage,0),'% of total)') AS "vcMileStone"
		,GetTotalProgress(v_numDomainID,v_numWOID,1::SMALLINT,2::SMALLINT,vcMileStoneName,0) AS "numTotalProgress"
	FROM
		StagePercentageDetails
	LEFT JOIN
		stagepercentagemaster
	ON
		StagePercentageDetails.numParentStageID = stagepercentagemaster.numstagepercentageid
   WHERE
		StagePercentageDetails.numdomainid = v_numDomainID
		AND StagePercentageDetails.numWorkOrderId = v_numWOID
	GROUP BY
		vcMileStoneName,stagepercentagemaster.numstagepercentage;
END; $$;












