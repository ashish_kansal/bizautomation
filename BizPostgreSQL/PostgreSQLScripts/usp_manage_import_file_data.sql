-- Stored procedure definition script USP_MANAGE_IMPORT_FILE_DATA for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MANAGE_IMPORT_FILE_DATA(INOUT v_intImportFileID			BIGINT	,	
   v_vcImportFileName		VARCHAR(1000),
   v_numMasterID				NUMERIC,
   v_numRecordAdded			NUMERIC,
   v_numRecordUpdated		NUMERIC,
   v_numErrors				NUMERIC,
   v_numDuplicates			NUMERIC,
   v_dtCreateDate			TIMESTAMP,
   v_dtImportDate			TIMESTAMP,
   v_numDomainID				NUMERIC,
   v_numUserContactID		NUMERIC,
   v_tintStatus				SMALLINT,
   v_btHasSendEmail			BOOLEAN,
   v_vcHistory_Added_Value	TEXT,
   v_dtHistoryDateTime		TIMESTAMP,
   v_intMode					INTEGER,
   v_tintItemLinkingID INTEGER,
   v_tintImportType SMALLINT,
   v_bitSingleOrder BOOLEAN,
   v_bitExistingOrganization BOOLEAN,
   v_numDivisionID NUMERIC(18,0),
   v_numContactID NUMERIC(18,0),
   v_vcCompanyName VARCHAR(300),
   v_vcContactFirstName VARCHAR(100),
   v_vcContactLastName VARCHAR(100),
   v_vcContactEmail VARCHAR(100),
   v_bitMatchOrganizationID BOOLEAN,
   v_numMatchFieldID NUMERIC(18,0))
LANGUAGE plpgsql
		--> 0 - FOR INSERT
		--> 1 - FOR UPDATE
		--> 2 - FOR UPDATE ROLLBACK STATUS
   AS $$
   DECLARE
   v_numHistoryID  NUMERIC;
BEGIN
   IF v_intMode = 0 then -- FOR INSERT
		
      INSERT INTO Import_File_Master(vcImportFileName
				,numMasterID
				,numRecordAdded
				,numRecordUpdated
				,numErrors
				,numDuplicates
				,dtCreateDate
				,dtImportDate
				,numDomainID
				,numUserContactID
				,tintStatus
				,btHasSendEmail
				,tintItemLinkingID
				,tintImportType
				,bitSingleOrder
				,bitExistingOrganization
				,numDivisionID
				,numContactID
				,vcCompanyName
				,vcContactFirstName
				,vcContactLastName
				,vcContactEmail
				,bitMatchOrganizationID
				,numMatchFieldID)
			 VALUES(v_vcImportFileName
				,v_numMasterID
				,v_numRecordAdded
				,v_numRecordUpdated
				,v_numErrors
				,v_numDuplicates
				,TIMEZONE('UTC',now())
				,TIMEZONE('UTC',now())
				,v_numDomainID
				,v_numUserContactID
				,v_tintStatus
				,v_btHasSendEmail
				,v_tintItemLinkingID
				,v_tintImportType
				,v_bitSingleOrder
				,v_bitExistingOrganization
				,v_numDivisionID
				,v_numContactID
				,v_vcCompanyName
				,v_vcContactFirstName
				,v_vcContactLastName
				,v_vcContactEmail
				,v_bitMatchOrganizationID
				,v_numMatchFieldID) RETURNING intImportFileID INTO v_intImportFileID;
			
      INSERT INTO Import_History(intImportFileID,vcHistory_Added_Value ,dtHistoryDateTime)
			VALUES(v_intImportFileID,v_vcHistory_Added_Value,v_dtHistoryDateTime);
   end if;  
		
   IF v_intMode = 1 then -- FOR UPDATE	         
      UPDATE Import_History SET vcHistory_Added_Value = v_vcHistory_Added_Value
      WHERE intImportFileID = v_intImportFileID;
      select   numHistoryID INTO v_numHistoryID FROM Import_History WHERE intImportFileID = v_intImportFileID;
      UPDATE Import_File_Master SET vcImportFileName = v_vcImportFileName,numMasterID = v_numMasterID,numRecordAdded = v_numRecordAdded,
      numRecordUpdated = v_numRecordUpdated,numErrors = v_numErrors,
      numDuplicates = v_numDuplicates,numHistoryID = v_numHistoryID,
      dtImportDate = TIMEZONE('UTC',now()),tintStatus = v_tintStatus,
      btHasSendEmail = v_btHasSendEmail
      WHERE intImportFileID = v_intImportFileID
      AND numDomainID = v_numDomainID
      AND numUserContactID =  v_numUserContactID;
   end if;	
		
   IF v_intMode = 2 then -- UPDATE STATUS FOR ROLLBACK
		
      UPDATE Import_File_Master SET tintStatus = v_tintStatus
      WHERE intImportFileID = v_intImportFileID
      AND numDomainID = v_numDomainID
      AND numUserContactID = v_numUserContactID
      AND coalesce(numRecordAdded,0) > 0;
   end if;
   RETURN;
END; $$;


