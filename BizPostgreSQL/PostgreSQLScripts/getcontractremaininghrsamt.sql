-- Function definition script getContractRemainingHrsAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION getContractRemainingHrsAmt(v_mode BOOLEAN,v_numContractId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql                  
 ------@mode = 1 for Amount ,0 for Hours                  
   AS $$
   DECLARE
   v_Hours  DECIMAL(10,2) DEFAULT 0;                 
   v_Amount  DECIMAL(10,2) DEFAULT 0;                 
   v_ContractHours  DECIMAL(10,2) DEFAULT 0;                
   v_ContractAmount  DECIMAL(10,2) DEFAULT 0;
   v_ret  DECIMAL(10,2);
BEGIN
   select   coalesce(numHours,0), coalesce(numAmount,0) INTO v_ContractHours,v_ContractAmount FROM
   ContractManagement WHERE
   numContractId = v_numContractId;          

   select   v_ContractAmount+coalesce(sum(monAmount),0) INTO v_ContractAmount FROM
   OpportunityBizDocsDetails WHERE
   numContractId =  v_numContractId; 
        
   IF v_mode = false then
	
      select   coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))/60),0) INTO v_Hours FROM
      timeandexpense WHERE
      numtype = 1
      AND numCategory = 1
      AND numcontractId = v_numContractId;
      IF v_ContractHours > v_Hours then
		
         v_Hours := v_ContractHours -v_Hours;
      ELSE
         v_Hours := 0;
      end if;
   end if;         
           
   IF v_mode = true then
	
      select   coalesce(SUM(coalesce(monAmount,0)),0) INTO v_Amount FROM
      timeandexpense WHERE
      numtype = 1
      AND numCategory = 2
      AND numcontractId = v_numContractId;
      select   v_Amount+coalesce(sum(monDealAmount),0) INTO v_Amount FROM(SELECT
         CASE
         WHEN PO.numOppBizDocID > 0
         THEN GetDealAmount(PO.numOppId,LOCALTIMESTAMP,PO.numOppBizDocID)
         WHEN PO.numBillId > 0
         THEN coalesce((SELECT coalesce(monAmount,0) FROM OpportunityBizDocsDetails WHERE numBizDocsPaymentDetId = PO.numBillId),0)
         ELSE 0
         END AS monDealAmount
         FROM
         ProjectsOpportunities PO
         WHERE
         numProId in(select numProId from ProjectsMaster where numcontractId = v_numContractId)) AS X;
      IF v_ContractAmount > v_Amount then
		
         v_Amount := v_ContractAmount -v_Amount;
      ELSE
         v_Amount := 0;
      end if;
   end if;          
                        
   IF v_mode = true then
      v_ret := v_Amount;
   ELSE
      v_ret := v_Hours;
   end if;                  
	
   RETURN v_ret;
END; $$;

