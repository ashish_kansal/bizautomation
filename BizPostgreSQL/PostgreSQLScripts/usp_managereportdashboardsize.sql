-- Stored procedure definition script USP_ManageReportDashboardSize for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageReportDashboardSize(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_tintColumn SMALLINT DEFAULT NULL, 
v_tintSize SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update ReportDashBoardSize set
   tintSize = v_tintSize
   WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND tintColumn = v_tintColumn;
   RETURN;
END; $$; 


