-- Stored procedure definition script USP_ManageExportData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageExportData(v_numDomainID NUMERIC(9,0),
v_vcExportTables VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM ExportDataSettings WHERE numDomainID = v_numDomainID) then
	
      UPDATE ExportDataSettings SET vcExportTables = v_vcExportTables,dtLastExportedOn = LOCALTIMESTAMP
      WHERE numDomainID = v_numDomainID;
   ELSE
      INSERT INTO ExportDataSettings(numDomainID,
			vcExportTables,
			dtLastExportedOn) VALUES(  
			/* numDomainID - numeric(18, 0) */ v_numDomainID,
			/* vcExportTables - varchar(100) */ v_vcExportTables,
			/* dtLastExportedOn - datetime */ LOCALTIMESTAMP);
   end if;
   RETURN;
END; $$;
---Created By Anoop Jayaraj



