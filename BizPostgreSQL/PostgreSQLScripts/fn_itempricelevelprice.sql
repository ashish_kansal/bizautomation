DROP FUNCTION IF EXISTS fn_ItemPriceLevelPrice;

CREATE OR REPLACE FUNCTION fn_ItemPriceLevelPrice(v_numItemCode NUMERIC(18,0)
	,v_monListPrice DECIMAL(20,5)
	,v_monVendorCost DECIMAL(20,5)
)
RETURNS TABLE     
(    
	numItemCode NUMERIC(18,0),
   vcPriceLevelType VARCHAR(300),
   vcDiscountType VARCHAR(300),
   monPriceLevel1 VARCHAR,
   monPriceLevel2 VARCHAR,
   monPriceLevel3 VARCHAR,
   monPriceLevel4 VARCHAR,
   monPriceLevel5 VARCHAR,
   monPriceLevel6 VARCHAR,
   monPriceLevel7 VARCHAR,
   monPriceLevel8 VARCHAR,
   monPriceLevel9 VARCHAR,
   monPriceLevel10 VARCHAR,
   monPriceLevel11 VARCHAR,
   monPriceLevel12 VARCHAR,
   monPriceLevel13 VARCHAR,
   monPriceLevel14 VARCHAR,
   monPriceLevel15 VARCHAR,
   monPriceLevel16 VARCHAR,
   monPriceLevel17 VARCHAR,
   monPriceLevel18 VARCHAR,
   monPriceLevel19 VARCHAR,
   monPriceLevel20 VARCHAR
) LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN QUERY (SELECT 
 	tp_PIVOT_crosstab.row_id AS numItemCode
	,tp_PIVOT_crosstab.vcPriceLevelType
	,tp_PIVOT_crosstab.vcDiscountType
    ,tp_PIVOT_crosstab.monPriceLevel1::VARCHAR, 
   tp_PIVOT_crosstab.monPriceLevel2::VARCHAR, tp_PIVOT_crosstab.monPriceLevel3::VARCHAR, tp_PIVOT_crosstab.monPriceLevel4::VARCHAR, tp_PIVOT_crosstab.monPriceLevel5::VARCHAR, 
   tp_PIVOT_crosstab.monPriceLevel6::VARCHAR, tp_PIVOT_crosstab.monPriceLevel7::VARCHAR, tp_PIVOT_crosstab.monPriceLevel8::VARCHAR, tp_PIVOT_crosstab.monPriceLevel9::VARCHAR, 
   tp_PIVOT_crosstab.monPriceLevel10::VARCHAR, tp_PIVOT_crosstab.monPriceLevel11::VARCHAR, tp_PIVOT_crosstab.monPriceLevel12::VARCHAR, tp_PIVOT_crosstab.monPriceLevel13::VARCHAR , 
   tp_PIVOT_crosstab.monPriceLevel14::VARCHAR, tp_PIVOT_crosstab.monPriceLevel15::VARCHAR, tp_PIVOT_crosstab.monPriceLevel16::VARCHAR, tp_PIVOT_crosstab.monPriceLevel17::VARCHAR , 
   tp_PIVOT_crosstab.monPriceLevel18::VARCHAR, tp_PIVOT_crosstab.monPriceLevel19::VARCHAR, tp_PIVOT_crosstab.monPriceLevel20::VARCHAR
   
   FROM crosstab(CONCAT('SELECT ',v_numItemCode,' AS row_id,(CASE tintRuleType
   WHEN 1
   THEN ''Deduct From List Price''
   WHEN 2
   THEN ''Add to Primary Vendor Cost''
   WHEN 3
   THEN ''Named Price''
   ELSE ''''
   END) vcPriceLevelType,(CASE
   WHEN tintDiscountType = 1 AND (tintRuleType = 1 OR tintRuleType = 2)
   THEN ''Percentage''
   WHEN tintDiscountType = 2 AND (tintRuleType = 1 OR tintRuleType = 2)
   THEN ''Flat Amount''
   WHEN tintDiscountType = 3 AND (tintRuleType = 1 OR tintRuleType = 2)
   THEN ''Named Price''
   ELSE ''''
   END) AS vcDiscountType, CONCAT(''monPriceLevel'',Id) Id
			,(CASE 
				WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN CONCAT(CAST(CAST(COALESCE(',COALESCE(v_monListPrice,0),',0) - (COALESCE(',COALESCE(v_monListPrice,0),',0) * (decDiscount /100)) AS DECIMAL(20,5)) AS VARCHAR(300)),'' ('',intFromQty,''-'',intToQty,'')'')
				WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN CONCAT(CAST(COALESCE(',COALESCE(v_monListPrice,0),',0) - decDiscount AS VARCHAR(300)),'' ('',intFromQty,''-'',intToQty,'')'')
				WHEN tintRuleType=1 AND tintDiscountType=3 --Deduct from List price & Named Price
				THEN CAST(CAST(decDiscount AS DECIMAL(20,5)) AS VARCHAR)
				WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN CONCAT(CAST(CAST(COALESCE(',COALESCE(v_monVendorCost,0),',0) + (COALESCE(',COALESCE(v_monVendorCost,0),',0) * (decDiscount /100)) AS DECIMAL(20,5)) AS VARCHAR(300)),'' ('',intFromQty,''-'',intToQty,'')'')
				WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN CONCAT(CAST(COALESCE(',COALESCE(v_monVendorCost,0),',0) + decDiscount AS VARCHAR(300)),'' ('',intFromQty,''-'',intToQty,'')'')
				WHEN tintRuleType=2 AND tintDiscountType=3 -- Add to Primary Vendor Cost & Named Price
				THEN CAST(CAST(decDiscount AS DECIMAL(20,5)) AS VARCHAR)
				WHEN tintRuleType=3 --Named Price
				THEN CAST(CAST(decDiscount AS DECIMAL(20,5)) AS VARCHAR)
			END) AS monPrice FROM
		(SELECT 
				ROW_NUMBER() OVER(ORDER BY numPricingID) Id
				,numItemCode
				,numPriceRuleID
				,intFromQty
				,intToQty
				,tintRuleType
				,tintDiscountType
				,decDiscount
		
			FROM 
				PricingTable
			WHERE 
				PricingTable.numItemCode=',v_numItemCode,'
				AND COALESCE(numCurrencyID,0) = 0
				AND COALESCE(numPriceRuleID,0) = 0) TEMP GROUP BY Id, monPrice,tintRuleType
				,tintDiscountType'), $q$ VALUES('monPriceLevel1'), 
   ('monPriceLevel2'), ('monPriceLevel3'), ('monPriceLevel4'), ('monPriceLevel5'), 
   ('monPriceLevel6'), ('monPriceLevel7'), ('monPriceLevel8'), ('monPriceLevel9'), 
   ('monPriceLevel10'), ('monPriceLevel11'), ('monPriceLevel12'), ('monPriceLevel13'), 
   ('monPriceLevel14'), ('monPriceLevel15'), ('monPriceLevel16'), ('monPriceLevel17'), 
   ('monPriceLevel18'), ('monPriceLevel19'), ('monPriceLevel20') $q$)
   As tp_PIVOT_crosstab(row_id NUMERIC(18,0),vcPriceLevelType VARCHAR, vcDiscountType VARCHAR, monPriceLevel1 VARCHAR, 
   monPriceLevel2 VARCHAR, monPriceLevel3 VARCHAR, monPriceLevel4 VARCHAR, monPriceLevel5 VARCHAR, 
   monPriceLevel6 VARCHAR, monPriceLevel7 VARCHAR, monPriceLevel8 VARCHAR, monPriceLevel9 VARCHAR, 
   monPriceLevel10 VARCHAR, monPriceLevel11 VARCHAR, monPriceLevel12 VARCHAR, monPriceLevel13  VARCHAR, 
   monPriceLevel14 VARCHAR, monPriceLevel15 VARCHAR, monPriceLevel16 VARCHAR, monPriceLevel17  VARCHAR, 
   monPriceLevel18 VARCHAR, monPriceLevel19 VARCHAR, monPriceLevel20 VARCHAR));
END; $$;

