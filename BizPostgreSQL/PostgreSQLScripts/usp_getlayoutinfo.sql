-- Stored procedure definition script usp_GetLayoutInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetLayoutInfo(v_numUserCntId NUMERIC(9,0) DEFAULT 0,                    
v_intcoulmn INTEGER DEFAULT NULL,                    
v_numDomainID NUMERIC(9,0) DEFAULT 0 ,          
v_Ctype CHAR DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(v_intcoulmn <> 0) then

      open SWV_RefCur for
      SELECT DTL.intCoulmn,PL.vcFieldName,DTL.numFieldId  FROM PageLayoutDTL DTL
      join PageLayout PL on DTL.numFieldId = PL.numFieldId
      WHERE DTL.numUserCntID = v_numUserCntId and  DTL.numDomainID = v_numDomainID  and DTL.intCoulmn = v_intcoulmn and PL.Ctype = v_Ctype
      order by DTL.intCoulmn;
   end if;              
              
                
   if v_intcoulmn = 0 then

      open SWV_RefCur for
      select vcFieldName,numFieldId from PageLayout where Ctype = v_Ctype and numFieldId not in(select DTL.numFieldId from PageLayoutDTL DTL join PageLayout PL on PL.numFieldId = DTL.numFieldId where PL.Ctype = v_Ctype and DTL.numUserCntID = v_numUserCntId and  DTL.numDomainID = v_numDomainID) order by intColumn,tintrow;
   end if;
   RETURN;
END; $$;


