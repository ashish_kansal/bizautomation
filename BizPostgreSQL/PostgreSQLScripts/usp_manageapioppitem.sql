-- Stored procedure definition script USP_ManageAPIOppItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAPIOppItem(v_WebApiOrderItemId NUMERIC(18,0),
v_numDomainId	NUMERIC(18,0),
v_numWebApiId	INTEGER,
v_numOppId	NUMERIC(18,0),
v_numOppItemId NUMERIC(18,0),
v_vcAPIOppId	VARCHAR(50),
v_vcApiOppItemId VARCHAR(50),
v_tintStatus SMALLINT DEFAULT 0,
v_numCreatedby	NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   IF v_WebApiOrderItemId = 0 then
	  
      select   COUNT(*) INTO v_Check FROM   WebApiOppItemDetails WHERE  numDomainID = v_numDomainId AND numWebApiId = v_numWebApiId
      AND vcAPIOppId = v_vcAPIOppId AND vcApiOppItemId = v_vcApiOppItemId;
      IF v_Check = 0 then
		 
         INSERT INTO WebApiOppItemDetails(numDomainID
				   ,numWebApiId
				   ,numOppId
				   ,numOppItemId
				   ,vcAPIOppId
				   ,vcApiOppItemId
				   ,tintStatus
				   ,numCreatedby
				   ,dtCreated)
			 VALUES(v_numDomainId,
				   v_numWebApiId,
				   v_numOppId,
				   v_numOppItemId,
				   v_vcAPIOppId,
				   v_vcApiOppItemId,
				   v_tintStatus,
				   v_numCreatedby,
				   TIMEZONE('UTC',now()));
      end if;
   ELSEIF v_WebApiOrderItemId <> 0
   then
			
      select   COUNT(*) INTO v_Check FROM   WebApiOppItemDetails WHERE  numDomainID = v_numDomainId AND numWebApiId = v_numWebApiId
      AND vcAPIOppId = v_vcAPIOppId AND vcApiOppItemId = v_vcApiOppItemId;
      IF v_Check = 0 then
					
         INSERT INTO WebApiOppItemDetails(numDomainID
							   ,numWebApiId
							   ,numOppId
							   ,numOppItemId
							   ,vcAPIOppId
							   ,vcApiOppItemId
							   ,tintStatus
							   ,numCreatedby
							   ,dtCreated,
							   dtModified)
						 VALUES(v_numDomainId,
							   v_numWebApiId,
							   v_numOppId,
							   v_numOppItemId,
							   v_vcAPIOppId,
							   v_vcApiOppItemId,
							   v_tintStatus,
							   v_numCreatedby,
							   TIMEZONE('UTC',now()),
							   TIMEZONE('UTC',now()));
      ELSE
         UPDATE WebApiOppItemDetails
         SET    tintStatus = v_tintStatus,dtModified = TIMEZONE('UTC',now())
         WHERE  numDomainID = v_numDomainId AND numWebApiId = v_numWebApiId
         AND vcAPIOppId = v_vcAPIOppId AND vcApiOppItemId = v_vcApiOppItemId;
      end if;
   end if;
   RETURN;
END; $$;


