DROP FUNCTION IF EXISTS USP_FeaturedItem;

CREATE OR REPLACE FUNCTION USP_FeaturedItem
(
	v_numNewDomainID NUMERIC(9,0),
    v_numReportId NUMERIC(9,0),
    v_numSiteId NUMERIC(9,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strsql TEXT; 
	v_DefaultWarehouseID NUMERIC(9,0);
	v_query TEXT;
BEGIN
	SELECT numDefaultWareHouseID INTO v_DefaultWarehouseID FROM    eCommerceDTL WHERE   numDomainID = v_numNewDomainID;
	SELECT textQuery INTO v_query FROM CustomReport WHERE CustomReport.numCustomReportId = v_numReportId;

	v_strsql := 'INSERT INTO tt_TEMPTABLE select X.##Items## from (' || coalesce(v_query,'') || ') X';
	v_strsql := REPLACE(v_strsql,'@numDomainId',CAST(v_numNewDomainID AS TEXT));
	v_strsql := REPLACE(v_strsql,'@numDomainID',CAST(v_numNewDomainID AS TEXT));
	v_strsql := regexp_replace(v_strsql,'##Items##','"##Items##"','gi');  
 
	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPTABLE 
	( 
		ItemCode NUMERIC(9,0) 
	); 

	EXECUTE  v_strsql;

	OPEN SWV_RefCur FOR
	SELECT 
		X.*
		,(SELECT vcCategoryName FROM Category WHERE numCategoryID = X.numCategoryID LIMIT 1) AS vcCategoryName
	FROM
	(
		SELECT  
			I.numItemCode ,
			I.vcItemName ,
			I.txtItemDesc ,
            (SELECT numCategoryID FROM ItemCategory WHERE numItemID = I.numItemCode LIMIT 1) AS numCategoryID
			,CASE 
				WHEN EXISTS(SELECT vcPathForTImage FROM ItemImages II WHERE II.bitDefault = true AND II.numItemCode = I.numItemCode LIMIT 1)
				THEN (SELECT vcPathForTImage FROM ItemImages II WHERE II.bitDefault = true AND II.numItemCode = I.numItemCode LIMIT 1)
				WHEN EXISTS(SELECT vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode LIMIT 1)
				THEN (SELECT vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode LIMIT 1)
				ELSE ''
			END AS vcPathForTImage
			,coalesce(CASE 
						WHEN I.charItemType = 'P' 
							THEN CASE WHEN I.bitSerialized = true THEN 1.00000*monListPrice ELSE 1.00000*coalesce((SELECT W.monWListPrice FROM WareHouseItems W WHERE W.numDomainID = v_numNewDomainID AND W.numItemID = I.numItemCode AND W.numWareHouseID = v_DefaultWarehouseID LIMIT 1),NULL) END
						ELSE 1.00000*monListPrice
					END,0) AS monListPrice
			,fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) AS UOMConversionFactor
			,I.vcSKU
			,I.vcManufacturer
			,coalesce(I.vcModelID,'') AS vcModelID
			,(
				CASE WHEN COALESCE(bitKitParent,false) = true
				THEN
					(CASE 
						WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND COALESCE(IInner.bitKitParent,false) = true)
						THEN true 
						ELSE false
					END)
				ELSE
					false
				END
			) bitHasChildKits
		FROM 
			Item AS I
		WHERE 
			I.numItemCode IN (SELECT ItemCode FROM tt_TEMPTABLE)
			AND I.numDomainID = v_numNewDomainID) AS X;
   RETURN;
END; $$;
