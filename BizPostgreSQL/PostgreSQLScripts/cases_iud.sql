CREATE OR REPLACE FUNCTION Cases_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
DECLARE 
	v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Case', OLD.numCaseId, 'Delete', OLD.numDomainID);
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numCaseId;
		v_numUserCntID := OLD.numModifiedBy;
		v_numDomainID := OLD.numDomainID;
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Case', NEW.numCaseId, 'Update', NEW.numDomainID);
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numCaseId;
		v_numUserCntID := NEW.numModifiedBy;
		v_numDomainID := NEW.numDomainID;

		IF OLD.numAssignedBy <> NEW.numAssignedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedBy,');
		END IF;
		IF OLD.numAssignedTo <> NEW.numAssignedTo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedTo,');
		END IF;
		IF OLD.vcCaseNumber <> NEW.vcCaseNumber THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcCaseNumber,');
		END IF;
		IF OLD.numContractId <> NEW.numContractId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numContractId,');
		END IF;
		IF OLD.bintCreatedDate <> NEW.bintCreatedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintCreatedDate,');
		END IF;
		IF OLD.textDesc <> NEW.textDesc THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'textDesc,');
		END IF;
		IF OLD.textInternalComments <> NEW.textInternalComments THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'textInternalComments,');
		END IF;
		IF OLD.numOrigin <> NEW.numOrigin THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numOrigin,');
		END IF;
		IF OLD.numPriority <> NEW.numPriority THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numPriority,');
		END IF;
		IF OLD.numReason <> NEW.numReason THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numReason,');
		END IF;
		IF OLD.numRecOwner <> NEW.numRecOwner THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numRecOwner,');
		END IF;
		IF OLD.intTargetResolveDate <> NEW.intTargetResolveDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intTargetResolveDate,');
		END IF;
		IF OLD.numStatus <> NEW.numStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numStatus,');
		END IF;
		IF OLD.textSubject <> NEW.textSubject THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'textSubject,');
		END IF;
		IF OLD.numType <> NEW.numType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numType,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');

	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Case', NEW.numCaseId, 'Insert', NEW.numDomainID);
		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.numCaseId;
		v_numUserCntID := New.numCreatedBy;
		v_numDomainID := NEW.numDomainID;
	END IF;

	 PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 72,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER Cases_IUD AFTER INSERT OR UPDATE OR DELETE ON Cases FOR EACH ROW EXECUTE PROCEDURE Cases_IUD_TrFunc();


