-- Stored procedure definition script usp_getCustomTags for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustomTags(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcMergeField as VARCHAR(255)),cast(vcMergeFieldValue as VARCHAR(255)) from EmailMergeFields;
END; $$;












