CREATE OR REPLACE FUNCTION USP_GetSalesTemplateItemDtl
(
	v_numSalesTemplateID NUMERIC(18,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numSalesTemplateItemID as "numSalesTemplateItemID"
		,STI.numItemCode as "numItemCode"
		,vcItemName as "vcItemName"
		,vcSKU as "vcSKU"
		,txtItemDesc as "txtItemDesc"
   FROM SalesTemplateItems STI
   JOIN Item I ON STI.numItemCode = I.numItemCode AND STI.numDomainID = I.numDomainID
   WHERE STI.numSalesTemplateID = v_numSalesTemplateID AND STI.numDomainID = v_numDomainID;
END; $$;












