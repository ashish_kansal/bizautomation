-- Stored procedure definition script USP_WorkScheduleDaysOff_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkScheduleDaysOff_Save(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numWorkScheduleID NUMERIC(18,0)
	,v_dtDayOffFrom DATE
	,v_dtDayOffTo DATE)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT ID FROM WorkSchedule WHERE numDomainID = v_numDomainID AND ID = coalesce(v_numWorkScheduleID,0)) then
	
      INSERT INTO WorkSchedule(numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays)
		VALUES(v_numDomainID
			,v_numUserCntID
			,0
			,0
			,0
			,0
			,'00:00:00'
			,'');
		
      v_numWorkScheduleID := CURRVAL('WorkSchedule_seq');
   end if;
	
   INSERT INTO WorkScheduleDaysOff(numWorkScheduleID
		,dtDayOffFrom
		,dtDayOffTo)
	VALUES(v_numWorkScheduleID
		,v_dtDayOffFrom
		,v_dtDayOffTo);
RETURN;
END; $$;


