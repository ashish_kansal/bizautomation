-- Function definition script GetWorkOrderLabourCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWorkOrderLabourCost(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monLabourCost  DECIMAL(20,5);
BEGIN
   v_monLabourCost := coalesce((SELECT
   SUM(GetTimeSpendOnTaskInMinutes(v_numDomainID,SPDT.numTaskId)::bigint*(coalesce(SPDT.monHourlyRate,0)/60))
   FROM
   StagePercentageDetailsTask SPDT
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND SPDT.numWorkOrderId = v_numWOID),0);

   RETURN v_monLabourCost;
END; $$;

