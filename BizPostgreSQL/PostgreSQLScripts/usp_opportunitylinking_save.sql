-- Stored procedure definition script USP_OpportunityLinking_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityLinking_Save(v_numParentOppID NUMERIC(18,0)
	,v_numChildOppID NUMERIC(18,0)
	,v_vcSource VARCHAR(50)
	,v_numParentOppBizDocID NUMERIC(18,0)
	,v_numParentProjectID NUMERIC(18,0)
	,v_numPromotionId NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numWebApiId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO OpportunityLinking(numParentOppID
		,numChildOppID
		,vcSource
		,numParentOppBizDocID
		,numParentProjectID
		,numPromotionId
		,numSiteID
		,numWebApiId)
	VALUES(v_numParentOppID
		,v_numChildOppID
		,v_vcSource
		,v_numParentOppBizDocID
		,v_numParentProjectID
		,v_numPromotionId
		,v_numSiteID
		,v_numWebApiId);
RETURN;
END; $$;


