DROP FUNCTION IF EXISTS USP_UpdatedomainDetails;

CREATE OR REPLACE FUNCTION USP_UpdatedomainDetails(
	p_numDomainID NUMERIC(9,0) DEFAULT 0,
	p_parameterJson JSON DEFAULT null
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	v_vcDomainName VARCHAR(50) DEFAULT '';                              
	v_vcDomainDesc VARCHAR(100) DEFAULT '';                                      
	v_bitExchangeIntegration BOOLEAN DEFAULT NULL;
	v_bitAccessExchange BOOLEAN DEFAULT NULL;                                      
	v_vcExchUserName VARCHAR(100) DEFAULT '';                                     
	v_vcExchPassword VARCHAR(100) DEFAULT '';                                     
	v_vcExchPath VARCHAR(200) DEFAULT '';                                     
	v_vcExchDomain VARCHAR(100) DEFAULT '';                                   
	v_tintCustomPagingRows SMALLINT DEFAULT NULL;                                  
	v_vcDateFormat VARCHAR(30) DEFAULT '';                               
	v_numDefCountry NUMERIC(9,0) DEFAULT NULL;                                    
	v_tintComposeWindow SMALLINT DEFAULT NULL;                                    
	v_sintStartDate SMALLINT DEFAULT NULL;                                 
	v_sintNoofDaysInterval SMALLINT DEFAULT NULL;                                   
	v_tintAssignToCriteria SMALLINT DEFAULT NULL;                          
	v_bitIntmedPage BOOLEAN DEFAULT NULL;                                   
	v_tintFiscalStartMonth SMALLINT DEFAULT NULL;                                   
	v_tintPayPeriod SMALLINT DEFAULT NULL;                    
	v_numCurrencyID NUMERIC(9,0) DEFAULT NULL;              
	v_charUnitSystem CHAR(1) DEFAULT '';           
	v_vcPortalLogo VARCHAR(100) DEFAULT '';       
	v_tintChrForComSearch SMALLINT DEFAULT NULL;            
	v_intPaymentGateWay INTEGER DEFAULT NULL; -- DO NOT UPDATE VALUE FROM THIS PARAMETER
	v_tintChrForItemSearch SMALLINT DEFAULT NULL;
	v_ShipCompany NUMERIC(9,0) DEFAULT NULL;
	v_bitMultiCurrency BOOLEAN DEFAULT NULL;
	v_bitCreateInvoice SMALLINT DEFAULT null;
	v_numDefaultSalesBizDocID NUMERIC(9,0) DEFAULT NULL;
	v_numDefaultPurchaseBizDocID NUMERIC(9,0) DEFAULT NULL;
	v_bitSaveCreditCardInfo BOOLEAN DEFAULT NULL;
	v_intLastViewedRecord INTEGER DEFAULT 10;
	v_tintCommissionType SMALLINT DEFAULT NULL;
	v_tintBaseTax SMALLINT DEFAULT NULL;
	v_bitEmbeddedCost BOOLEAN DEFAULT NULL;
	v_numDefaultSalesOppBizDocId NUMERIC DEFAULT NULL;
	v_tintSessionTimeOut SMALLINT DEFAULT 1;
	v_tintDecimalPoints SMALLINT DEFAULT NULL;
	v_bitCustomizePortal BOOLEAN DEFAULT false;
	v_tintBillToForPO SMALLINT DEFAULT NULL;
	v_tintShipToForPO SMALLINT DEFAULT NULL;
	v_tintOrganizationSearchCriteria SMALLINT DEFAULT NULL;
	v_bitDocumentRepositary BOOLEAN DEFAULT false;
	v_tintComAppliesTo SMALLINT DEFAULT 0;
	v_bitGtoBContact BOOLEAN DEFAULT false;
	v_bitBtoGContact BOOLEAN DEFAULT false;
	v_bitGtoBCalendar BOOLEAN DEFAULT false;
	v_bitBtoGCalendar BOOLEAN DEFAULT false;
	v_bitExpenseNonInventoryItem BOOLEAN DEFAULT false;
	v_bitInlineEdit BOOLEAN DEFAULT false;
	v_bitRemoveVendorPOValidation BOOLEAN DEFAULT false;
	v_tintBaseTaxOnArea SMALLINT DEFAULT 0;
	v_bitAmountPastDue BOOLEAN DEFAULT false;
	v_monAmountPastDue DECIMAL(20,5) DEFAULT NULL;
	v_bitSearchOrderCustomerHistory BOOLEAN DEFAULT false;
	v_tintCalendarTimeFormat SMALLINT DEFAULT NULL;
	v_tintDefaultClassType SMALLINT DEFAULT NULL;
	v_tintPriceBookDiscount SMALLINT DEFAULT 0;
	v_bitAutoSerialNoAssign BOOLEAN DEFAULT false;
	v_bitIsShowBalance BOOLEAN DEFAULT false;
	v_numIncomeAccID NUMERIC(18,0) DEFAULT 0;
	v_numCOGSAccID NUMERIC(18,0) DEFAULT 0;
	v_numAssetAccID NUMERIC(18,0) DEFAULT 0;
	v_IsEnableProjectTracking BOOLEAN DEFAULT false;
	v_IsEnableCampaignTracking BOOLEAN DEFAULT false;
	v_IsEnableResourceScheduling BOOLEAN DEFAULT false;
	v_ShippingServiceItem  NUMERIC(9,0) DEFAULT 0;
	v_numSOBizDocStatus NUMERIC(9,0) DEFAULT 0;
	v_bitAutolinkUnappliedPayment BOOLEAN DEFAULT false;
	v_numDiscountServiceItemID NUMERIC(9,0) DEFAULT 0;
	v_vcShipToPhoneNumber VARCHAR(50) DEFAULT '';
	v_vcGAUserEmailId VARCHAR(50) DEFAULT '';
	v_vcGAUserPassword VARCHAR(50) DEFAULT '';
	v_vcGAUserProfileId VARCHAR(50) DEFAULT '';
	v_bitDiscountOnUnitPrice BOOLEAN DEFAULT false;
	v_intShippingImageWidth INTEGER DEFAULT 0;
	v_intShippingImageHeight INTEGER DEFAULT 0;
	v_numTotalInsuredValue NUMERIC(10,2) DEFAULT 0;
	v_numTotalCustomsValue NUMERIC(10,2) DEFAULT 0;
	v_vcHideTabs VARCHAR(1000) DEFAULT '';
	v_bitUseBizdocAmount BOOLEAN DEFAULT false;
	v_bitDefaultRateType BOOLEAN DEFAULT false;
	v_bitIncludeTaxAndShippingInCommission  BOOLEAN DEFAULT false;
	v_bitPurchaseTaxCredit BOOLEAN DEFAULT false;
	v_bitLandedCost BOOLEAN DEFAULT false;
	v_vcLanedCostDefault VARCHAR(10) DEFAULT '';
	v_bitMinUnitPriceRule BOOLEAN DEFAULT false;
	v_numAbovePercent NUMERIC(18,2) DEFAULT NULL;
	v_numBelowPercent NUMERIC(18,2) DEFAULT NULL;
	v_numBelowPriceField NUMERIC(18,0) DEFAULT NULL;
	v_vcUnitPriceApprover VARCHAR(100) DEFAULT NULL;
	v_numDefaultSalesPricing SMALLINT DEFAULT NULL;
	v_bitReOrderPoint BOOLEAN DEFAULT false;
	v_numReOrderPointOrderStatus NUMERIC(18,0) DEFAULT 0;
	v_numPODropShipBizDoc NUMERIC(18,0) DEFAULT 0;
	v_numPODropShipBizDocTemplate NUMERIC(18,0) DEFAULT 0;
	v_IsEnableDeferredIncome BOOLEAN DEFAULT false;
	v_bitApprovalforTImeExpense BOOLEAN DEFAULT false;
	v_numCost NUMERIC(9,0) DEFAULT 0;
	v_intTimeExpApprovalProcess INTEGER DEFAULT 0;
	v_bitApprovalforOpportunity BOOLEAN DEFAULT false;
	v_intOpportunityApprovalProcess INTEGER DEFAULT 0;
	v_numDefaultSalesShippingDoc NUMERIC(18,0) DEFAULT 0;
	v_numDefaultPurchaseShippingDoc NUMERIC(18,0) DEFAULT 0;
	v_bitchkOverRideAssignto BOOLEAN DEFAULT false;
	v_vcPrinterIPAddress VARCHAR(15) DEFAULT '';
	v_vcPrinterPort VARCHAR(5) DEFAULT '';
	v_numDefaultSiteID NUMERIC(18,0) DEFAULT 0;
	v_bitRemoveGlobalLocation BOOLEAN DEFAULT false;
	v_numAuthorizePercentage NUMERIC(18,0) DEFAULT 0;
	v_bitEDI BOOLEAN DEFAULT false;
	v_bit3PL BOOLEAN DEFAULT false;
	v_bitAllowDuplicateLineItems BOOLEAN DEFAULT false;
	v_tintCommitAllocation SMALLINT DEFAULT 1;
	v_tintInvoicing SMALLINT DEFAULT 1;
	v_tintMarkupDiscountOption SMALLINT DEFAULT 1;
	v_tintMarkupDiscountValue SMALLINT DEFAULT 1;
	v_bitCommissionBasedOn BOOLEAN DEFAULT false;
	v_tintCommissionBasedOn SMALLINT DEFAULT 1;
	v_bitDoNotShowDropshipPOWindow BOOLEAN DEFAULT false;
	v_tintReceivePaymentTo SMALLINT DEFAULT 2;
	v_numReceivePaymentBankAccount NUMERIC(18,0) DEFAULT 0;
	v_numOverheadServiceItemID NUMERIC(18,0) DEFAULT 0;
	v_bitDisplayCustomField BOOLEAN DEFAULT false;
	v_bitFollowupAnytime BOOLEAN DEFAULT false;
	v_bitpartycalendarTitle BOOLEAN DEFAULT false;
	v_bitpartycalendarLocation BOOLEAN DEFAULT false;
	v_bitpartycalendarDescription BOOLEAN DEFAULT false;
	v_bitREQPOApproval BOOLEAN DEFAULT false;
	v_bitARInvoiceDue BOOLEAN DEFAULT false;
	v_bitAPBillsDue BOOLEAN DEFAULT false;
	v_bitItemsToPickPackShip BOOLEAN DEFAULT false;
	v_bitItemsToInvoice BOOLEAN DEFAULT false;
	v_bitSalesOrderToClose BOOLEAN DEFAULT false;
	v_bitItemsToPutAway BOOLEAN DEFAULT false;
	v_bitItemsToBill BOOLEAN DEFAULT false;
	v_bitPosToClose BOOLEAN DEFAULT false;
	v_bitPOToClose BOOLEAN DEFAULT false;
	v_bitBOMSToPick BOOLEAN DEFAULT false;
	v_vchREQPOApprovalEmp  VARCHAR(500) DEFAULT '';
	v_vchARInvoiceDue VARCHAR(500) DEFAULT '';
	v_vchAPBillsDue VARCHAR(500) DEFAULT '';
	v_vchItemsToPickPackShip VARCHAR(500) DEFAULT '';
	v_vchItemsToInvoice VARCHAR(500) DEFAULT '';
	v_vchPriceMarginApproval VARCHAR(500) DEFAULT '';
	v_vchSalesOrderToClose VARCHAR(500) DEFAULT '';
	v_vchItemsToPutAway VARCHAR(500) DEFAULT '';
	v_vchItemsToBill VARCHAR(500) DEFAULT '';
	v_vchPosToClose VARCHAR(500) DEFAULT '';
	v_vchPOToClose VARCHAR(500) DEFAULT '';
	v_vchBOMSToPick VARCHAR(500) DEFAULT '';
	v_decReqPOMinValue NUMERIC DEFAULT 0;
	v_bitUseOnlyActionItems BOOLEAN DEFAULT false;
	v_bitDisplayContractElement BOOLEAN DEFAULT NULL;
	v_vcEmployeeForContractTimeElement VARCHAR(500) DEFAULT '';
	v_numARContactPosition NUMERIC(18,0) DEFAULT 0;
	v_bitShowCardConnectLink BOOLEAN DEFAULT false;
	v_vcBluePayFormName VARCHAR(500) DEFAULT '';
	v_vcBluePaySuccessURL VARCHAR(500) DEFAULT '';
	v_vcBluePayDeclineURL VARCHAR(500) DEFAULT '';
	v_bitUseDeluxeCheckStock BOOLEAN DEFAULT true;
	v_bitEnableSmartyStreets BOOLEAN DEFAULT false;
	v_vcSmartyStreetsAPIKeys VARCHAR(500) DEFAULT '';
	v_bitUsePreviousEmailBizDoc BOOLEAN DEFAULT false;
	v_bitInventoryInvoicing BOOLEAN DEFAULT false;
	v_tintCommitAllocationOld  SMALLINT;
	v_tintCommissionVendorCostType SMALLINT DEFAULT 1;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempWareHouseID  NUMERIC(18,0);
   v_numTempWareHouseItemID  NUMERIC(18,0);
BEGIN
	IF COALESCE(p_parameterJson::VARCHAR,'') = '' THEN
		RAISE EXCEPTION 'INVALID_JSON';
		RETURN;
	ELSE
			SELECT  
				(p_parameterJson::json->>'v_vcDomainName')::VARCHAR(50),                              
				(p_parameterJson::json->>'v_vcDomainDesc')::VARCHAR(100),
				(p_parameterJson::json->>'v_bitExchangeIntegration')::BOOLEAN,
				(p_parameterJson::json->>'v_bitAccessExchange')::BOOLEAN,
				(p_parameterJson::json->>'v_vcExchUserName')::VARCHAR(100),
				(p_parameterJson::json->>'v_vcExchPassword')::VARCHAR(100),
				(p_parameterJson::json->>'v_vcExchPath')::VARCHAR(200),
				(p_parameterJson::json->>'v_vcExchDomain')::VARCHAR(100),
				(p_parameterJson::json->>'v_tintCustomPagingRows')::SMALLINT,
				(p_parameterJson::json->>'v_vcDateFormat')::VARCHAR(30),
				(p_parameterJson::json->>'v_numDefCountry')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_tintComposeWindow')::SMALLINT,
				(p_parameterJson::json->>'v_sintStartDate')::SMALLINT,
				(p_parameterJson::json->>'v_sintNoofDaysInterval')::SMALLINT,
				(p_parameterJson::json->>'v_tintAssignToCriteria')::SMALLINT,
				(p_parameterJson::json->>'v_bitIntmedPage')::BOOLEAN,
				(p_parameterJson::json->>'v_tintFiscalStartMonth')::SMALLINT,
				(p_parameterJson::json->>'v_tintPayPeriod')::SMALLINT,
				(p_parameterJson::json->>'v_numCurrencyID')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_charUnitSystem')::CHAR(1),
				(p_parameterJson::json->>'v_vcPortalLogo')::VARCHAR(100),
				(p_parameterJson::json->>'v_tintChrForComSearch')::SMALLINT,
				(p_parameterJson::json->>'v_intPaymentGateWay')::INTEGER,
				(p_parameterJson::json->>'v_tintChrForItemSearch')::SMALLINT,
				(p_parameterJson::json->>'v_ShipCompany')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_bitMultiCurrency')::BOOLEAN,
				(p_parameterJson::json->>'v_bitCreateInvoice')::SMALLINT,
				(p_parameterJson::json->>'v_numDefaultSalesBizDocID')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_numDefaultPurchaseBizDocID')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_bitSaveCreditCardInfo')::BOOLEAN,
				(p_parameterJson::json->>'v_intLastViewedRecord')::INTEGER,
				(p_parameterJson::json->>'v_tintCommissionType')::SMALLINT,
				(p_parameterJson::json->>'v_tintBaseTax')::SMALLINT,
				(p_parameterJson::json->>'v_bitEmbeddedCost')::BOOLEAN,
				(p_parameterJson::json->>'v_numDefaultSalesOppBizDocId')::NUMERIC,
				(p_parameterJson::json->>'v_tintSessionTimeOut')::SMALLINT,
				(p_parameterJson::json->>'v_tintDecimalPoints')::SMALLINT,
				(p_parameterJson::json->>'v_bitCustomizePortal')::BOOLEAN,
				(p_parameterJson::json->>'v_tintBillToForPO')::SMALLINT,
				(p_parameterJson::json->>'v_tintShipToForPO')::SMALLINT,
				(p_parameterJson::json->>'v_tintOrganizationSearchCriteria')::SMALLINT,
				(p_parameterJson::json->>'v_bitDocumentRepositary')::BOOLEAN,
				(p_parameterJson::json->>'v_tintComAppliesTo')::SMALLINT,
				(p_parameterJson::json->>'v_bitGtoBContact')::BOOLEAN,
				(p_parameterJson::json->>'v_bitBtoGContact')::BOOLEAN,
				(p_parameterJson::json->>'v_bitGtoBCalendar')::BOOLEAN,
				(p_parameterJson::json->>'v_bitBtoGCalendar')::BOOLEAN,
				(p_parameterJson::json->>'v_bitExpenseNonInventoryItem')::BOOLEAN,
				(p_parameterJson::json->>'v_bitInlineEdit')::BOOLEAN,
				(p_parameterJson::json->>'v_bitRemoveVendorPOValidation')::BOOLEAN,
				(p_parameterJson::json->>'v_tintBaseTaxOnArea')::SMALLINT,
				(p_parameterJson::json->>'v_bitAmountPastDue')::BOOLEAN,
				(p_parameterJson::json->>'v_monAmountPastDue')::DECIMAL(20,5),
				(p_parameterJson::json->>'v_bitSearchOrderCustomerHistory')::BOOLEAN,
				(p_parameterJson::json->>'v_tintCalendarTimeFormat')::SMALLINT,
				(p_parameterJson::json->>'v_tintDefaultClassType')::SMALLINT,
				(p_parameterJson::json->>'v_tintPriceBookDiscount')::SMALLINT,
				(p_parameterJson::json->>'v_bitAutoSerialNoAssign')::BOOLEAN,
				(p_parameterJson::json->>'v_bitIsShowBalance')::BOOLEAN,
				(p_parameterJson::json->>'v_numIncomeAccID')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numCOGSAccID')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numAssetAccID')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_IsEnableProjectTracking')::BOOLEAN,
				(p_parameterJson::json->>'v_IsEnableCampaignTracking')::BOOLEAN,
				(p_parameterJson::json->>'v_IsEnableResourceScheduling')::BOOLEAN,
				(p_parameterJson::json->>'v_ShippingServiceItem')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_numSOBizDocStatus')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_bitAutolinkUnappliedPayment')::BOOLEAN,
				(p_parameterJson::json->>'v_numDiscountServiceItemID')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_vcShipToPhoneNumber')::VARCHAR(50),
				(p_parameterJson::json->>'v_vcGAUserEmailId')::VARCHAR(50),
				(p_parameterJson::json->>'v_vcGAUserPassword')::VARCHAR(50),
				(p_parameterJson::json->>'v_vcGAUserProfileId')::VARCHAR(50),
				(p_parameterJson::json->>'v_bitDiscountOnUnitPrice')::BOOLEAN,
				(p_parameterJson::json->>'v_intShippingImageWidth')::INTEGER,
				(p_parameterJson::json->>'v_intShippingImageHeight')::INTEGER,
				(p_parameterJson::json->>'v_numTotalInsuredValue')::NUMERIC(10,2),
				(p_parameterJson::json->>'v_numTotalCustomsValue')::NUMERIC(10,2),
				(p_parameterJson::json->>'v_vcHideTabs')::VARCHAR(1000),
				(p_parameterJson::json->>'v_bitUseBizdocAmount')::BOOLEAN,
				(p_parameterJson::json->>'v_bitDefaultRateType')::BOOLEAN,
				(p_parameterJson::json->>'v_bitIncludeTaxAndShippingInCommission')::BOOLEAN,
				(p_parameterJson::json->>'v_bitPurchaseTaxCredit')::BOOLEAN,
				(p_parameterJson::json->>'v_bitLandedCost')::BOOLEAN,
				(p_parameterJson::json->>'v_vcLanedCostDefault')::VARCHAR(10),
				(p_parameterJson::json->>'v_bitMinUnitPriceRule')::BOOLEAN,
				(p_parameterJson::json->>'v_numAbovePercent')::NUMERIC(18,2),
				(p_parameterJson::json->>'v_numBelowPercent')::NUMERIC(18,2),
				(p_parameterJson::json->>'v_numBelowPriceField')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_vcUnitPriceApprover')::VARCHAR(100),
				(p_parameterJson::json->>'v_numDefaultSalesPricing')::SMALLINT,
				(p_parameterJson::json->>'v_bitReOrderPoint')::BOOLEAN,
				(p_parameterJson::json->>'v_numReOrderPointOrderStatus')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numPODropShipBizDoc')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numPODropShipBizDocTemplate')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_IsEnableDeferredIncome')::BOOLEAN,
				(p_parameterJson::json->>'v_bitApprovalforTImeExpense')::BOOLEAN,
				(p_parameterJson::json->>'v_numCost')::NUMERIC(9,0),
				(p_parameterJson::json->>'v_intTimeExpApprovalProcess')::INTEGER,
				(p_parameterJson::json->>'v_bitApprovalforOpportunity')::BOOLEAN,
				(p_parameterJson::json->>'v_intOpportunityApprovalProcess')::INTEGER,
				(p_parameterJson::json->>'v_numDefaultSalesShippingDoc')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numDefaultPurchaseShippingDoc')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_bitchkOverRideAssignto')::BOOLEAN,
				(p_parameterJson::json->>'v_vcPrinterIPAddress')::VARCHAR(15),
				(p_parameterJson::json->>'v_vcPrinterPort')::VARCHAR(5),
				(p_parameterJson::json->>'v_numDefaultSiteID')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_bitRemoveGlobalLocation')::BOOLEAN,
				(p_parameterJson::json->>'v_numAuthorizePercentage')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_bitEDI')::BOOLEAN,
				(p_parameterJson::json->>'v_bit3PL')::BOOLEAN,
				(p_parameterJson::json->>'v_bitAllowDuplicateLineItems')::BOOLEAN,
				(p_parameterJson::json->>'v_tintCommitAllocation')::SMALLINT,
				(p_parameterJson::json->>'v_tintInvoicing')::SMALLINT,
				(p_parameterJson::json->>'v_tintMarkupDiscountOption')::SMALLINT,
				(p_parameterJson::json->>'v_tintMarkupDiscountValue')::SMALLINT,
				(p_parameterJson::json->>'v_bitCommissionBasedOn')::BOOLEAN,
				(p_parameterJson::json->>'v_tintCommissionBasedOn')::SMALLINT,
				(p_parameterJson::json->>'v_bitDoNotShowDropshipPOWindow')::BOOLEAN,
				(p_parameterJson::json->>'v_tintReceivePaymentTo')::SMALLINT,
				(p_parameterJson::json->>'v_numReceivePaymentBankAccount')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_numOverheadServiceItemID')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_bitDisplayCustomField')::BOOLEAN,
				(p_parameterJson::json->>'v_bitFollowupAnytime')::BOOLEAN,
				(p_parameterJson::json->>'v_bitpartycalendarTitle')::BOOLEAN,
				(p_parameterJson::json->>'v_bitpartycalendarLocation')::BOOLEAN,
				(p_parameterJson::json->>'v_bitpartycalendarDescription')::BOOLEAN,
				(p_parameterJson::json->>'v_bitREQPOApproval')::BOOLEAN,
				(p_parameterJson::json->>'v_bitARInvoiceDue')::BOOLEAN,
				(p_parameterJson::json->>'v_bitAPBillsDue')::BOOLEAN,
				(p_parameterJson::json->>'v_bitItemsToPickPackShip')::BOOLEAN,
				(p_parameterJson::json->>'v_bitItemsToInvoice')::BOOLEAN,
				(p_parameterJson::json->>'v_bitSalesOrderToClose')::BOOLEAN,
				(p_parameterJson::json->>'v_bitItemsToPutAway')::BOOLEAN,
				(p_parameterJson::json->>'v_bitItemsToBill')::BOOLEAN,
				(p_parameterJson::json->>'v_bitPosToClose')::BOOLEAN,
				(p_parameterJson::json->>'v_bitPOToClose')::BOOLEAN,
				(p_parameterJson::json->>'v_bitBOMSToPick')::BOOLEAN,
				(p_parameterJson::json->>'v_vchREQPOApprovalEmp')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchARInvoiceDue')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchAPBillsDue')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchItemsToPickPackShip')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchItemsToInvoice')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchPriceMarginApproval')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchSalesOrderToClose')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchItemsToPutAway')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchItemsToBill')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchPosToClose')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchPOToClose')::VARCHAR(500),
				(p_parameterJson::json->>'v_vchBOMSToPick')::VARCHAR(500),
				(p_parameterJson::json->>'v_decReqPOMinValue')::VARCHAR(500),
				(p_parameterJson::json->>'v_bitUseOnlyActionItems')::BOOLEAN,
				(p_parameterJson::json->>'v_bitDisplayContractElement')::BOOLEAN,
				(p_parameterJson::json->>'v_vcEmployeeForContractTimeElement')::VARCHAR(500),
				(p_parameterJson::json->>'v_numARContactPosition')::NUMERIC(18,0),
				(p_parameterJson::json->>'v_bitShowCardConnectLink')::BOOLEAN,
				(p_parameterJson::json->>'v_vcBluePayFormName')::VARCHAR(500),
				(p_parameterJson::json->>'v_vcBluePaySuccessURL')::VARCHAR(500),
				(p_parameterJson::json->>'v_vcBluePayDeclineURL')::VARCHAR(500),
				(p_parameterJson::json->>'v_bitUseDeluxeCheckStock')::BOOLEAN,
				(p_parameterJson::json->>'v_bitEnableSmartyStreets')::BOOLEAN,
				(p_parameterJson::json->>'v_vcSmartyStreetsAPIKeys')::VARCHAR(500),
				(p_parameterJson::json->>'v_bitUsePreviousEmailBizDoc')::BOOLEAN,
				(p_parameterJson::json->>'v_bitInventoryInvoicing')::BOOLEAN,
				(p_parameterJson::json->>'v_tintCommitAllocationOld')::SMALLINT,
				(p_parameterJson::json->>'v_tintCommissionVendorCostType')::SMALLINT
			INTO
				v_vcDomainName,                             
				v_vcDomainDesc,                               
				v_bitExchangeIntegration,
				v_bitAccessExchange,                                
				v_vcExchUserName,                                    
				v_vcExchPassword,                                 
				v_vcExchPath,                                  
				v_vcExchDomain,                                
				v_tintCustomPagingRows,                             
				v_vcDateFormat,                              
				v_numDefCountry,                                   
				v_tintComposeWindow,                                 
				v_sintStartDate,                              
				v_sintNoofDaysInterval,                                   
				v_tintAssignToCriteria,                         
				v_bitIntmedPage,                          
				v_tintFiscalStartMonth,                                
				v_tintPayPeriod,                   
				v_numCurrencyID,             
				v_charUnitSystem,     
				v_vcPortalLogo,    
				v_tintChrForComSearch,       
				v_intPaymentGateWay, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
				v_tintChrForItemSearch,
				v_ShipCompany,
				v_bitMultiCurrency,
				v_bitCreateInvoice,
				v_numDefaultSalesBizDocID,
				v_numDefaultPurchaseBizDocID,
				v_bitSaveCreditCardInfo,
				v_intLastViewedRecord,
				v_tintCommissionType,
				v_tintBaseTax,
				v_bitEmbeddedCost,
				v_numDefaultSalesOppBizDocId,
				v_tintSessionTimeOut,
				v_tintDecimalPoints,
				v_bitCustomizePortal,
				v_tintBillToForPO,
				v_tintShipToForPO,
				v_tintOrganizationSearchCriteria,
				v_bitDocumentRepositary,
				v_tintComAppliesTo,
				v_bitGtoBContact,
				v_bitBtoGContact,
				v_bitGtoBCalendar,
				v_bitBtoGCalendar,
				v_bitExpenseNonInventoryItem,
				v_bitInlineEdit,
				v_bitRemoveVendorPOValidation,
				v_tintBaseTaxOnArea,
				v_bitAmountPastDue,
				v_monAmountPastDue,
				v_bitSearchOrderCustomerHistory,
				v_tintCalendarTimeFormat,
				v_tintDefaultClassType,
				v_tintPriceBookDiscount,
				v_bitAutoSerialNoAssign,
				v_bitIsShowBalance,
				v_numIncomeAccID,
				v_numCOGSAccID,
				v_numAssetAccID,
				v_IsEnableProjectTracking,
				v_IsEnableCampaignTracking,
				v_IsEnableResourceScheduling,
				v_ShippingServiceItem,
				v_numSOBizDocStatus,
				v_bitAutolinkUnappliedPayment,
				v_numDiscountServiceItemID,
				v_vcShipToPhoneNumber,
				v_vcGAUserEmailId,
				v_vcGAUserPassword,
				v_vcGAUserProfileId,
				v_bitDiscountOnUnitPrice,
				v_intShippingImageWidth,
				v_intShippingImageHeight,
				v_numTotalInsuredValue,
				v_numTotalCustomsValue,
				v_vcHideTabs,
				v_bitUseBizdocAmount,
				v_bitDefaultRateType,
				v_bitIncludeTaxAndShippingInCommission,
				v_bitPurchaseTaxCredit,
				v_bitLandedCost,
				v_vcLanedCostDefault,
				v_bitMinUnitPriceRule,
				v_numAbovePercent,
				v_numBelowPercent,
				v_numBelowPriceField,
				v_vcUnitPriceApprover,
				v_numDefaultSalesPricing,
				v_bitReOrderPoint,
				v_numReOrderPointOrderStatus,
				v_numPODropShipBizDoc,
				v_numPODropShipBizDocTemplate,
				v_IsEnableDeferredIncome,
				v_bitApprovalforTImeExpense,
				v_numCost,
				v_intTimeExpApprovalProcess,
				v_bitApprovalforOpportunity,
				v_intOpportunityApprovalProcess,
				v_numDefaultSalesShippingDoc,
				v_numDefaultPurchaseShippingDoc,
				v_bitchkOverRideAssignto,
				v_vcPrinterIPAddress,
				v_vcPrinterPort,
				v_numDefaultSiteID,
				v_bitRemoveGlobalLocation,
				v_numAuthorizePercentage,
				v_bitEDI,
				v_bit3PL,
				v_bitAllowDuplicateLineItems,
				v_tintCommitAllocation,
				v_tintInvoicing,
				v_tintMarkupDiscountOption,
				v_tintMarkupDiscountValue,
				v_bitCommissionBasedOn,
				v_tintCommissionBasedOn,
				v_bitDoNotShowDropshipPOWindow,
				v_tintReceivePaymentTo,
				v_numReceivePaymentBankAccount,
				v_numOverheadServiceItemID,
				v_bitDisplayCustomField,
				v_bitFollowupAnytime,
				v_bitpartycalendarTitle,
				v_bitpartycalendarLocation,
				v_bitpartycalendarDescription,
				v_bitREQPOApproval,
				v_bitARInvoiceDue,
				v_bitAPBillsDue,
				v_bitItemsToPickPackShip,
				v_bitItemsToInvoice,
				v_bitSalesOrderToClose,
				v_bitItemsToPutAway,
				v_bitItemsToBill,
				v_bitPosToClose,
				v_bitPOToClose,
				v_bitBOMSToPick,
				v_vchREQPOApprovalEmp,
				v_vchARInvoiceDue,
				v_vchAPBillsDue,
				v_vchItemsToPickPackShip,
				v_vchItemsToInvoice,
				v_vchPriceMarginApproval,
				v_vchSalesOrderToClose,
				v_vchItemsToPutAway,
				v_vchItemsToBill,
				v_vchPosToClose,
				v_vchPOToClose,
				v_vchBOMSToPick,
				v_decReqPOMinValue,
				v_bitUseOnlyActionItems,
				v_bitDisplayContractElement,
				v_vcEmployeeForContractTimeElement,
				v_numARContactPosition,
				v_bitShowCardConnectLink,
				v_vcBluePayFormName,
				v_vcBluePaySuccessURL,
				v_vcBluePayDeclineURL,
				v_bitUseDeluxeCheckStock,
				v_bitEnableSmartyStreets,
				v_vcSmartyStreetsAPIKeys,
				v_bitUsePreviousEmailBizDoc,
				v_bitInventoryInvoicing,
			   v_tintCommitAllocationOld,
			   v_tintCommissionVendorCostType;
	END IF;


   IF coalesce(v_bitMinUnitPriceRule,false) = true AND coalesce(v_intOpportunityApprovalProcess,0) = 0 then
	
      RAISE EXCEPTION 'INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION';
      RETURN;
   end if;
   If coalesce(v_bitApprovalforTImeExpense,false) = true AND(SELECT
   COUNT(*)
   FROM
   UserMaster
   LEFT JOIN
   ApprovalConfig
   ON
   ApprovalConfig.numUserId = UserMaster.numUserDetailId
   WHERE
   UserMaster.numDomainID = p_numDomainID
   AND numModule = 1
   AND coalesce(numGroupID,0) > 0
   AND coalesce(bitActivateFlag,false) = true
   AND coalesce(numLevel1Authority,0) = 0
   AND coalesce(numLevel2Authority,0) = 0
   AND coalesce(numLevel3Authority,0) = 0
   AND coalesce(numLevel4Authority,0) = 0
   AND coalesce(numLevel5Authority,0) = 0) > 0 then
	
      RAISE EXCEPTION 'INCOMPLETE_TIMEEXPENSE_APPROVAL';
      RETURN;
   end if;
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocationOld FROM Domain WHERE numDomainId = p_numDomainID;
   IF v_tintCommitAllocation <> v_tintCommitAllocationOld AND(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId = p_numDomainID AND tintopptype = 1 AND tintoppstatus = 1 AND coalesce(tintshipped,0) = 0) > 0 then
	
      RAISE EXCEPTION 'NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER';
      RETURN;
   end if;
   UPDATE
   Domain
   SET
   vcDomainName = v_vcDomainName,vcDomainDesc = v_vcDomainDesc,bitExchangeIntegration = v_bitExchangeIntegration,
   bitAccessExchange = v_bitAccessExchange,
   vcExchUserName = v_vcExchUserName,vcExchPassword = v_vcExchPassword,vcExchPath = v_vcExchPath,
   vcExchDomain = v_vcExchDomain,tintCustomPagingRows = v_tintCustomPagingRows,
   vcDateFormat = v_vcDateFormat,numDefCountry = v_numDefCountry,
   tintComposeWindow = v_tintComposeWindow,sintStartDate = v_sintStartDate,
   sintNoofDaysInterval = v_sintNoofDaysInterval,tintAssignToCriteria = v_tintAssignToCriteria,
   bitIntmedPage = v_bitIntmedPage,tintFiscalStartMonth = v_tintFiscalStartMonth,
   tintPayPeriod = v_tintPayPeriod,vcCurrency = coalesce((select varCurrSymbol from Currency Where numCurrencyID = v_numCurrencyID),''),
   numCurrencyID = v_numCurrencyID,charUnitSystem = v_charUnitSystem,
   vcPortalLogo = v_vcPortalLogo,tintChrForComSearch = v_tintChrForComSearch,
   tintChrForItemSearch = v_tintChrForItemSearch,numShipCompany = v_ShipCompany,
   bitMultiCurrency = v_bitMultiCurrency,bitCreateInvoice = v_bitCreateInvoice,
   numDefaultSalesBizDocId = v_numDefaultSalesBizDocID,
   bitSaveCreditCardInfo = v_bitSaveCreditCardInfo,intLastViewedRecord = v_intLastViewedRecord,
   tintCommissionType = v_tintCommissionType,tintBaseTax = v_tintBaseTax,
   numDefaultPurchaseBizDocId = v_numDefaultPurchaseBizDocID,
   bitEmbeddedCost = v_bitEmbeddedCost,numDefaultSalesOppBizDocId = v_numDefaultSalesOppBizDocId,
   tintSessionTimeOut = v_tintSessionTimeOut,tintDecimalPoints = v_tintDecimalPoints,
   bitCustomizePortal = v_bitCustomizePortal,
   tintBillToForPO = v_tintBillToForPO,tintShipToForPO = v_tintShipToForPO,
   tintOrganizationSearchCriteria = v_tintOrganizationSearchCriteria,bitDocumentRepositary = v_bitDocumentRepositary,
   bitGtoBContact = v_bitGtoBContact,
   bitBtoGContact = v_bitBtoGContact,bitGtoBCalendar = v_bitGtoBCalendar,
   bitBtoGCalendar = v_bitBtoGCalendar,bitExpenseNonInventoryItem = v_bitExpenseNonInventoryItem,
   bitInlineEdit = v_bitInlineEdit,bitRemoveVendorPOValidation = v_bitRemoveVendorPOValidation,
   tintBaseTaxOnArea = v_tintBaseTaxOnArea,
   bitAmountPastDue = v_bitAmountPastDue,monAmountPastDue = v_monAmountPastDue,
   bitSearchOrderCustomerHistory = v_bitSearchOrderCustomerHistory,
   tintCalendarTimeFormat = v_tintCalendarTimeFormat,tintDefaultClassType = v_tintDefaultClassType,
   tintPriceBookDiscount = v_tintPriceBookDiscount,
   bitAutoSerialNoAssign = v_bitAutoSerialNoAssign,bitIsShowBalance = v_bitIsShowBalance,
   numIncomeAccID = v_numIncomeAccID,numCOGSAccID = v_numCOGSAccID,
   numAssetAccID = v_numAssetAccID,IsEnableProjectTracking = v_IsEnableProjectTracking,
   IsEnableCampaignTracking = v_IsEnableCampaignTracking,
   IsEnableResourceScheduling = v_IsEnableResourceScheduling,numShippingServiceItemID = v_ShippingServiceItem,
   numSOBizDocStatus = v_numSOBizDocStatus,
   bitAutolinkUnappliedPayment = v_bitAutolinkUnappliedPayment,
   numDiscountServiceItemID = v_numDiscountServiceItemID,vcShipToPhoneNo = v_vcShipToPhoneNumber,
   vcGAUserEMail = v_vcGAUserEmailId,vcGAUserPassword = v_vcGAUserPassword,
   vcGAUserProfileId = v_vcGAUserProfileId,bitDiscountOnUnitPrice = v_bitDiscountOnUnitPrice,
   intShippingImageWidth = v_intShippingImageWidth,
   intShippingImageHeight = v_intShippingImageHeight,
   numTotalInsuredValue = v_numTotalInsuredValue,numTotalCustomsValue = v_numTotalCustomsValue,
   vcHideTabs = v_vcHideTabs,bitUseBizdocAmount = v_bitUseBizdocAmount,
   bitDefaultRateType = v_bitDefaultRateType,bitIncludeTaxAndShippingInCommission = v_bitIncludeTaxAndShippingInCommission,
   bitPurchaseTaxCredit = v_bitPurchaseTaxCredit,bitLandedCost = v_bitLandedCost,
   vcLanedCostDefault = v_vcLanedCostDefault,bitMinUnitPriceRule = v_bitMinUnitPriceRule,
   numDefaultSalesPricing = v_numDefaultSalesPricing,
   numPODropShipBizDoc = v_numPODropShipBizDoc,numPODropShipBizDocTemplate = v_numPODropShipBizDocTemplate,
   IsEnableDeferredIncome = v_IsEnableDeferredIncome,
   bitApprovalforTImeExpense = v_bitApprovalforTImeExpense,numCost = v_numCost,
   intTimeExpApprovalProcess = v_intTimeExpApprovalProcess,bitApprovalforOpportunity = v_bitApprovalforOpportunity,
   intOpportunityApprovalProcess = v_intOpportunityApprovalProcess,
   numDefaultPurchaseShippingDoc = v_numDefaultPurchaseShippingDoc,
   numDefaultSalesShippingDoc = v_numDefaultSalesShippingDoc,
   bitchkOverRideAssignto = v_bitchkOverRideAssignto,vcPrinterIPAddress = v_vcPrinterIPAddress,
   vcPrinterPort = v_vcPrinterPort,numDefaultSiteID = coalesce(v_numDefaultSiteID,0),
   bitRemoveGlobalLocation = v_bitRemoveGlobalLocation,
   numAuthorizePercentage = v_numAuthorizePercentage,
   bitEDI = coalesce(v_bitEDI,false),bit3PL = coalesce(v_bit3PL,false),
   bitAllowDuplicateLineItems = coalesce(v_bitAllowDuplicateLineItems,false),tintCommitAllocation = coalesce(v_tintCommitAllocation,1),tintInvoicing = coalesce(v_tintInvoicing,1),
   tintMarkupDiscountOption = coalesce(v_tintMarkupDiscountOption,1),
   tintMarkupDiscountValue = coalesce(v_tintMarkupDiscountValue,1),
   bitCommissionBasedOn = coalesce(v_bitCommissionBasedOn,false),
   tintCommissionBasedOn = coalesce(v_tintCommissionBasedOn,1),
   bitDoNotShowDropshipPOWindow = coalesce(v_bitDoNotShowDropshipPOWindow,false),tintReceivePaymentTo = coalesce(v_tintReceivePaymentTo,2),
   numReceivePaymentBankAccount = coalesce(v_numReceivePaymentBankAccount,0),numOverheadServiceItemID = v_numOverheadServiceItemID,bitDisplayCustomField = v_bitDisplayCustomField,
   bitFollowupAnytime = v_bitFollowupAnytime,
   bitpartycalendarTitle = v_bitpartycalendarTitle,bitpartycalendarLocation = v_bitpartycalendarLocation,
   bitpartycalendarDescription = v_bitpartycalendarDescription,
   bitREQPOApproval = v_bitREQPOApproval,bitARInvoiceDue = v_bitARInvoiceDue,
   bitAPBillsDue = v_bitAPBillsDue,bitItemsToPickPackShip = v_bitItemsToPickPackShip,
   bitItemsToInvoice = v_bitItemsToInvoice,bitSalesOrderToClose = v_bitSalesOrderToClose,
   bitItemsToPutAway = v_bitItemsToPutAway,
   bitItemsToBill = v_bitItemsToBill,bitPosToClose = v_bitPosToClose,
   bitPOToClose = v_bitPOToClose,bitBOMSToPick = v_bitBOMSToPick,vchREQPOApprovalEmp = v_vchREQPOApprovalEmp,
   vchARInvoiceDue = v_vchARInvoiceDue,vchAPBillsDue = v_vchAPBillsDue,
   vchItemsToPickPackShip = v_vchItemsToPickPackShip,
   vchItemsToInvoice = v_vchItemsToInvoice,vchSalesOrderToClose = v_vchSalesOrderToClose,
   vchItemsToPutAway = v_vchItemsToPutAway,vchItemsToBill = v_vchItemsToBill,
   vchPosToClose = v_vchPosToClose,vchPOToClose = v_vchPOToClose,
   vchBOMSToPick = v_vchBOMSToPick,decReqPOMinValue = v_decReqPOMinValue,bitUseOnlyActionItems = v_bitUseOnlyActionItems,
   bitDisplayContractElement = v_bitDisplayContractElement,
   vcEmployeeForContractTimeElement = v_vcEmployeeForContractTimeElement,
   numARContactPosition = v_numARContactPosition,
   bitShowCardConnectLink = v_bitShowCardConnectLink,vcBluePayFormName = v_vcBluePayFormName,
   vcBluePaySuccessURL = v_vcBluePaySuccessURL,vcBluePayDeclineURL = v_vcBluePayDeclineURL,
   bitUseDeluxeCheckStock = v_bitUseDeluxeCheckStock,
   bitEnableSmartyStreets = v_bitEnableSmartyStreets,vcSmartyStreetsAPIKeys = v_vcSmartyStreetsAPIKeys,
   bitUsePreviousEmailBizDoc = v_bitUsePreviousEmailBizDoc,
   bitInventoryInvoicing = v_bitInventoryInvoicing,
   tintCommissionVendorCostType=v_tintCommissionVendorCostType
   WHERE
   numDomainId = p_numDomainID;
   DELETE FROM UnitPriceApprover WHERE numDomainID = p_numDomainID;
   IF(LENGTH(v_vchPriceMarginApproval) > 0) then
	
      INSERT INTO UnitPriceApprover(numDomainID,
			numUserID)
      SELECT p_numDomainID,Id FROM SplitIds(v_vchPriceMarginApproval,',');
   end if;
   IF coalesce(v_bitRemoveGlobalLocation,false) = true then
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPGlobalWarehouse_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPGLOBALWAREHOUSE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPGLOBALWAREHOUSE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numItemCode NUMERIC(18,0),
         numWarehouseID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(28,0)
      );
      INSERT INTO tt_TEMPGLOBALWAREHOUSE(numItemCode
			,numWarehouseID
			,numWarehouseItemID)
      SELECT
      numItemID
			,numWareHouseID
			,numWareHouseItemID
      FROM
      WareHouseItems
      WHERE
      numDomainID = p_numDomainID
      AND numWLocationID = -1
      AND coalesce(numOnHand,0) = 0
      AND coalesce(numonOrder,0) = 0
      AND coalesce(numAllocation,0) = 0
      AND coalesce(numBackOrder,0) = 0;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPGLOBALWAREHOUSE;
      WHILE v_i <= v_iCount LOOP
         select   numItemCode, numWarehouseID, numWarehouseItemID INTO v_numTempItemCode,v_numTempWareHouseID,v_numTempWareHouseItemID FROM tt_TEMPGLOBALWAREHOUSE WHERE ID = v_i;
         BEGIN
            SELECT * INTO v_numTempWareHouseItemID FROM USP_AddUpdateWareHouseForItems(v_numTempItemCode,v_numTempWareHouseID,v_numTempWareHouseItemID,'',0,0,
            0,'','',p_numDomainID,'','','',0,3::SMALLINT,0,0,NULL,0);
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
   EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;



