CREATE OR REPLACE FUNCTION usp_GetSalesOppSerializLot(v_numOppID NUMERIC(9,0),
      v_numOppItemID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT oppM.vcpOppName,numoppitemtCode,opp.vcItemName,coalesce(SUM(coalesce(oppI.numQty,0)),0) AS numQty,coalesce(bitSerialized,false) AS bitSerialized,coalesce(bitLotNo,false) AS bitLotNo
   FROM OpportunityMaster oppM JOIN OpportunityItems opp ON oppM.numOppId = opp.numOppId JOIN Item i ON i.numItemCode = opp.numItemCode
   LEFT JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode = oppI.numOppItemID AND oppI.numOppID = opp.numOppId)
   WHERE opp.numOppId = v_numOppID AND opp.numoppitemtCode = v_numOppItemID
   GROUP BY numoppitemtCode,opp.vcItemName,bitSerialized,bitLotNo,vcpOppName;
END; $$;
