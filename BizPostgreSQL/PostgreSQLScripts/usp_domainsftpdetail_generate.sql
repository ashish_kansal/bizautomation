-- Stored procedure definition script USP_DomainSFTPDetail_Generate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DomainSFTPDetail_Generate(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TP1Username  VARCHAR(20);
   v_TP1Password  VARCHAR(20);
   v_TP2Username  VARCHAR(20);
   v_TP2Password  VARCHAR(20);

    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

   IF NOT EXISTS(SELECT ID FROM DomainSFTPDetail WHERE numDomainID = v_numDomainID) then
      BEGIN
         -- BEGIN TRANSACTION
with recursive CTETP1Username(n) AS(SELECT CAST(1 AS INTEGER) AS n UNION ALL SELECT n+1 AS n FROM CTETP1Username WHERE n < 68)
         SELECT
			SUBSTR((SELECT 
						string_agg(vcValue,'') 
					FROM 
					(
						SELECT 
							SUBSTR('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1) vcValue 
						FROM 
							CTETP1Username 
						ORDER BY
							uuid_generate_v4()) X
					),1,10) INTO v_TP1Username;
        
         with recursive CTETP1Password(n) AS(SELECT CAST(1 AS INTEGER) AS n UNION ALL SELECT n+1 AS n FROM CTETP1Password WHERE n < 68)
         SELECT
		 SUBSTR((SELECT 
						string_agg(vcValue,'') 
					FROM 
					(
						SELECT 
							SUBSTR('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1) vcValue 
						FROM 
							CTETP1Password 
						ORDER BY
							uuid_generate_v4()) X
					),1,10) INTO v_TP1Password;
         with recursive CTETP2Username(n) AS(SELECT CAST(1 AS INTEGER) AS n UNION ALL SELECT n+1 AS n FROM CTETP2Username WHERE n < 68)
         SELECT
		 SUBSTR((SELECT 
						string_agg(vcValue,'') 
					FROM 
					(
						SELECT 
							SUBSTR('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1) vcValue 
						FROM 
							CTETP2Username 
						ORDER BY
							uuid_generate_v4()) X
					),1,10) INTO v_TP2Username;
         with recursive CTETP2Password(n) AS(SELECT CAST(1 AS INTEGER) AS n UNION ALL SELECT n+1 AS n FROM CTETP2Password WHERE n < 68)
         SELECT
		 SUBSTR((SELECT 
						string_agg(vcValue,'') 
					FROM 
					(
						SELECT 
							SUBSTR('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1) vcValue 
						FROM 
							CTETP2Password 
						ORDER BY
							uuid_generate_v4()) X
					),1,10) INTO v_TP2Password;

         INSERT INTO DomainSFTPDetail(numDomainID,vcUsername,vcPassword,tintType) VALUES(v_numDomainID,v_TP1Username,v_TP1Password,1);
			
         INSERT INTO DomainSFTPDetail(numDomainID,vcUsername,vcPassword,tintType) VALUES(v_numDomainID,v_TP2Username,v_TP2Password,2);
		
         -- COMMIT

EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   end if;
   RETURN;
END; $$;


