-- Stored procedure definition script USP_GetChartAcntDetailsForTransaction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForTransaction(v_numAccountId NUMERIC(9,0),                                  
v_numDomainId NUMERIC(9,0),                          
v_dtFromDate TIMESTAMP,                        
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);                        
   v_i  INTEGER;
   v_numParentAccountId  NUMERIC(9,0);
BEGIN
   v_strSQL := '';      
   
   Select numAccountId INTO v_numParentAccountId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1       
       
   select   count(*) INTO v_i From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                               
   GJH.numDomainId = v_numDomainId And GJH.datEntry_Date <= CAST('' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '' AS timestamp);               
   RAISE NOTICE '%',v_i;        
   if v_i = 0 then
  
      v_strSQL := CONCAT(' Select distinct numAccountId as numAccountId,'''' as CategoryName,numOpeningBal as OpeningBalance                            
   From Chart_Of_Accounts COA                            
   Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                            
   Where  COA.numDomainId =',v_numDomainId,'And COA.numAccountId <>',COALESCE(v_numParentAccountId,0),' And COA.numOpeningBal is not null   And COA.numAccountId =',v_numAccountId);
      
	  v_strSQL := CONCAT(coalesce(v_strSQL,''),'  And COA.dtOpeningDate>=''',v_dtFromDate,''' And  COA.dtOpeningDate<=''',v_dtToDate,'''');
   Else
      v_strSQL := CONCAT(' Select distinct numAccountId as numAccountId,'''' as CategoryName,numOpeningBal as OpeningBalance                            
   From Chart_Of_Accounts COA                            
   Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                            
   Where  COA.numDomainId =' ,v_numDomainId,' And COA.numAccountId <>',COALESCE(v_numParentAccountId,0), ' And COA.numOpeningBal is not null  And COA.numAccountId =',v_numAccountId);
   end if;                  
   RAISE NOTICE '%',v_strSQL;                        
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


