DROP FUNCTION IF EXISTS USP_GetOppAddress;

CREATE OR REPLACE FUNCTION USP_GetOppAddress(v_numOppID NUMERIC(9,0) DEFAULT 0,
    v_byteMode SMALLINT DEFAULT NULL,
    v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintType  SMALLINT;
   v_numDomainID  NUMERIC(9,0); 
   v_numDivisionID  NUMERIC(9,0);
   v_numContactID  NUMERIC(9,0);
   v_Name  VARCHAR(100);
   v_Phone  VARCHAR(100);
   v_numAddressID  NUMERIC(18,0);
   v_vcCompanyName  VARCHAR(100);
BEGIN
   IF v_byteMode = 0 then -- Get Billing Address
        
      select   tintBillToType, numDomainId, numDivisionId, numContactId INTO v_tintType,v_numDomainID,v_numDivisionID,v_numContactID FROM    OpportunityMaster WHERE   numOppId = v_numOppID;
      select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), (CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END) INTO v_Name,v_Phone FROM    AdditionalContactsInformation
      INNER JOIN
      DivisionMaster
      ON
      AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID WHERE   numContactId = v_numContactID;
      IF (v_tintType IS NULL
      OR v_tintType = 1) then
         open SWV_RefCur for
         SELECT  v_Name AS Name,
                        v_Phone AS Phone,
                        fn_GetComapnyName(v_numDivisionID) AS Company,
                        v_numDivisionID AS DivisionID,
                        AD.vcStreet AS Street,
                        AD.vcCity AS City,
                        AD.numState AS State,
                        AD.vcPostalCode AS PostCode,
                        AD.numCountry AS Country,
						AD.numContact,
						(CASE WHEN coalesce(AD.bitAltContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
						AD.bitAltContact,
						AD.vcAltContact
         FROM    AddressDetails AD
         LEFT JOIN
         AdditionalContactsInformation
         ON
         AD.numContact = AdditionalContactsInformation.numContactId
         WHERE   AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2
         AND AD.tintAddressType = 1
         AND AD.bitIsPrimary = true;
      ELSEIF v_tintType = 0
      then
         open SWV_RefCur for
         SELECT  v_Name AS Name,
                            v_Phone AS Phone,
                            Com1.vcCompanyName AS Company,
                            v_numDivisionID AS DivisionID,
                            AD1.vcStreet AS Street,
                            AD1.vcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN coalesce(AD1.bitAltContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact
         FROM    CompanyInfo Com1
         JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
         JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
         AND AD1.numRecordID = div1.numDivisionID
         AND tintAddressOf = 2
         AND tintAddressType = 1
         AND bitIsPrimary = true
         LEFT JOIN
         AdditionalContactsInformation
         ON
         AD1.numContact = AdditionalContactsInformation.numContactId
         WHERE   D1.numDomainId = v_numDomainID;
      ELSEIF v_tintType = 2
      then
         open SWV_RefCur for
         SELECT  
				COALESCE(numBillAddressID,0) numAddressID,
				v_Name AS Name,
                v_Phone AS Phone,
                vcBillCompanyName AS Company,
                numBillCompanyID AS DivisionID,
                vcBillStreet AS Street,
                vcBillCity AS City,
                numBillState AS State,
                vcBillPostCode AS PostCode,
                numBillCountry AS Country,
                GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName,
				numBillingContact AS numContact,
				(CASE WHEN coalesce(bitAltBillingContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
				bitAltBillingContact AS bitAltContact,
				vcAltBillingContact AS vcAltContact
         FROM    OpportunityAddress
         LEFT JOIN
         AdditionalContactsInformation
         ON
         OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
         WHERE   numOppID = v_numOppID;
      ELSEIF v_tintType = 3
      then
         open SWV_RefCur for
         SELECT  
			COALESCE(numBillAddressID,0) numAddressID,
			v_Name AS Name,
                                    v_Phone AS Phone,
                                    vcBillCompanyName AS Company,
                                    numBillCompanyID AS DivisionID,
                                    vcBillStreet AS Street,
                                    vcBillCity AS City,
                                    numBillState AS State,
                                    vcBillPostCode AS PostCode,
                                    numBillCountry AS Country,
                                    GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName,
									numBillingContact AS numContact,
									(CASE WHEN coalesce(bitAltBillingContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
									bitAltBillingContact AS bitAltContact,
									vcAltBillingContact AS vcAltContact
         FROM    OpportunityAddress
         LEFT JOIN
         AdditionalContactsInformation
         ON
         OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
         WHERE   numOppID = v_numOppID;
      end if;
   ELSEIF v_byteMode = 1
   then -- Get Shipping Address
            
      select   tintShipToType, numDomainId, numDivisionId, numContactId INTO v_tintType,v_numDomainID,v_numDivisionID,v_numContactID FROM    OpportunityMaster WHERE   numOppId = v_numOppID;
      select   coalesce(vcFirstName,'') || ' '
      || coalesce(vcLastname,''), (CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END) INTO v_Name,v_Phone FROM    AdditionalContactsInformation
      INNER JOIN
      DivisionMaster
      ON
      AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID WHERE   numContactId = v_numContactID;
      IF (v_tintType IS NULL OR v_tintType = 1) then
				
         open SWV_RefCur for
         SELECT  v_Name AS Name,
                            v_Phone AS Phone,
                            fn_GetComapnyName(v_numDivisionID) AS Company,
                            v_numDivisionID AS DivisionID,
                            AD.vcStreet AS Street,
                            AD.vcCity AS City,
                            AD.numState AS State,
                            AD.vcPostalCode AS PostCode,
                            AD.numCountry AS Country,
							AD.numContact,
							(CASE WHEN coalesce(AD.bitAltContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
							AD.bitAltContact,
							AD.vcAltContact,
							fn_GetComapnyName(v_numDivisionID) AS vcShipCompanyName
         FROM    AddressDetails AD
         LEFT JOIN
         AdditionalContactsInformation
         ON
         AD.numContact = AdditionalContactsInformation.numContactId
         WHERE   AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2
         AND AD.tintAddressType = 2
         AND AD.bitIsPrimary = true;
      ELSEIF v_tintType = 0
      then
				
         open SWV_RefCur for
         SELECT(CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN vcAltContact
         WHEN coalesce(numContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_Name
         END) AS Name,
                            (CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_Phone
         END) AS Phone,
                            com1.vcCompanyName AS Company,
                            v_numDivisionID AS DivisionID,
                            AD1.vcStreet AS Street,
                            AD1.vcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN coalesce(AD1.bitAltContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact,
							com1.vcCompanyName AS vcShipCompanyName
         FROM    CompanyInfo com1
         JOIN DivisionMaster div1 ON com1.numCompanyId = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
         JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
         AND AD1.numRecordID = div1.numDivisionID
         AND tintAddressOf = 2
         AND tintAddressType = 2
         AND bitIsPrimary = true
         LEFT JOIN
         AdditionalContactsInformation
         ON
         AD1.numContact = AdditionalContactsInformation.numContactId
         WHERE   D1.numDomainId = v_numDomainID;
      ELSEIF v_tintType = 2
      then
				
         open SWV_RefCur for
         SELECT
		 COALESCE(numShipAddressID,0) numAddressID,
		 (CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN vcAltShippingContact
         WHEN coalesce(numShippingContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_Name
         END) AS Name,
                            (CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numShippingContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_Phone
         END) AS Phone,
                            fn_GetComapnyName(v_numDivisionID) AS Company,
							v_numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN coalesce(bitAltShippingContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
         FROM    OpportunityAddress
         LEFT JOIN
         AdditionalContactsInformation
         ON
         OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
         WHERE   numOppID = v_numOppID;
      ELSEIF v_tintType = 3
      then
				
         open SWV_RefCur for
         SELECT
		 COALESCE(numShipAddressID,0) numAddressID,
		 (CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN vcAltShippingContact
         WHEN coalesce(numShippingContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_Name
         END) AS Name,
                                (CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numShippingContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_Phone
         END) AS Phone,
                            fn_GetComapnyName(v_numDivisionID) AS Company,
							v_numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN coalesce(bitAltShippingContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
         FROM    OpportunityAddress
         LEFT JOIN
         AdditionalContactsInformation
         ON
         OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
         WHERE   numOppID = v_numOppID;
      end if;
   end if;
    
   IF v_byteMode = 2 then -- Get Billing Address
        
      select   numDomainID, numDivisionId, numContactID INTO v_numDomainID,v_numDivisionID,v_numContactID FROM    ReturnHeader WHERE   numReturnHeaderID = v_numReturnHeaderID;
      select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), numPhone INTO v_Name,v_Phone FROM    AdditionalContactsInformation WHERE   numContactId = v_numContactID;
      open SWV_RefCur for
      SELECT  
		COALESCE(numBillAddressID,0) numAddressID,
		v_Name AS Name,
                    v_Phone AS Phone,
                    vcBillCompanyName AS Company,
                    v_numDivisionID AS DivisionID,
                    vcBillStreet AS Street,
                    vcBillCity AS City,
                    numBillState AS State,
                    vcBillPostCode AS PostCode,
                    numBillCountry AS Country,
                    GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName
      FROM    OpportunityAddress
      WHERE   numReturnHeaderID = v_numReturnHeaderID;
   end if;
	
   IF v_byteMode = 3 then -- Get Shipping Address
      select   numAddressID, numDivisionId INTO v_numAddressID,v_numDivisionID FROM    ProjectsMaster WHERE   numProId = v_numOppID;
      select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), numPhone, numContactId, '' INTO v_Name,v_Phone,v_numContactID,v_vcCompanyName FROM    AdditionalContactsInformation WHERE numDomainID = v_numDomainID
      AND numDivisionId = v_numDivisionID
      AND coalesce(bitPrimaryContact,false) = true;
      open SWV_RefCur for
      SELECT(CASE
      WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
      THEN vcAltContact
      WHEN coalesce(numContact,0) > 0
      THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
      ELSE v_Name
      END) AS Name,
                    (CASE
      WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
      THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
      WHEN coalesce(numContact,0) > 0
      THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
      ELSE v_Phone
      END) AS Phone,
                    v_vcCompanyName AS Company,
                    v_numDivisionID AS DivisionID,
                    vcStreet AS Street,
                    vcCity AS City,
                    numState AS State,
                    vcPostalCode AS PostCode,
                    numCountry AS Country,
                    GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS OppCompanyName,
					(CASE WHEN coalesce(bitAltContact,false) = false THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastname) ELSE '' END) AS vcContact,
					bitAltContact,
					vcAltContact,
					numContact
      FROM   AddressDetails
      LEFT JOIN
      AdditionalContactsInformation
      ON
      AddressDetails.numContact = AdditionalContactsInformation.numContactId
      WHERE  numRecordID = v_numOppID AND tintaddressof=4 AND tintaddresstype=2;
   end if;
   RETURN;
END; $$;        
        



