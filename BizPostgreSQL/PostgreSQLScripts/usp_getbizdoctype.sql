-- Stored procedure definition script USP_GetBizDocType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBizDocType(v_BizDocType NUMERIC(9,0) DEFAULT 0,                  
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDomainID = 0 then

      open SWV_RefCur for
      select vcData,numListItemID from Listdetails where numListID = 27 AND (numListItemID = v_BizDocType OR v_BizDocType = 0);
   ELSE
      IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then
         PERFORM USP_GetMasterListItems(27,v_numDomainID,SWV_RefCur);
      ELSE
         open SWV_RefCur for
         SELECT Ld.numListItemID, coalesce(vcRenamedListName,vcData) as vcData
         FROM BizDocFilter BDF INNER JOIN Listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
         left join listorder LO on Ld.numListItemID = LO.numListItemID AND LO.numDomainId = v_numDomainID
         WHERE Ld.numListID = 27 AND BDF.tintBizocType = v_BizDocType AND BDF.numDomainID = v_numDomainID
         AND (constFlag = true or Ld.numDomainid = v_numDomainID) ORDER BY LO.intSortOrder;
      end if;
   end if;
   RETURN;
END; $$;


