DROP FUNCTION IF EXISTS USP_DemandForecast_GetByItemVendor;

CREATE OR REPLACE FUNCTION USP_DemandForecast_GetByItemVendor
(
	v_numDomainID NUMERIC(9,0),
	v_numItemCode NUMERIC(9,0),
	v_numVendorID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numQty NUMERIC,
	INOUT SWV_RefCur refcursor,
	INOUT SWV_RefCur2 refcursor
)
LANGUAGE plpgsql
   AS $$
	DECLARE
		v_numCurrencyID NUMERIC(18,0);
		v_monVendorCost DECIMAL(20,5);
		v_numItemClassification NUMERIC(18,0);
		v_numCountry NUMERIC(18,0);
		v_numState NUMERIC(18,0);
		v_numVendorPrimaryShipToAddress NUMERIC(18,0);
BEGIN
	SELECT numCurrencyID INTO v_numCurrencyID FROM Domain WHERE numDomainID = v_numDomainID;
	SELECT numItemClassification INTO v_numItemClassification FROM Item WHERE numItemCode = v_numItemCode;
	SELECT numAddressID INTO v_numVendorPrimaryShipToAddress FROM AddressDetails WHERE numRecordID=v_numVendorID AND tintAddressOf=2 AND tintAddressType=2 AND COALESCE(bitIsPrimary,false)=true;

	SELECT
		AD.numState
		,AD.numCountry
	INTO
		v_numState
		,v_numCountry
	FROM 
		WareHouseItems WI 
	INNER JOIN 
		Warehouses W 
	ON 
		WI.numWareHouseID=W.numWareHouseID 
	INNER JOIN 
		AddressDetails AD 
	ON 
		W.numAddressID=AD.numAddressID 
	WHERE 
		numWareHouseItemID=v_numWarehouseItemID;

	v_monVendorCost := COALESCE((SELECT monCost FROM Vendor WHERE numVendorID = v_numVendorID AND numItemCode = v_numItemCode LIMIT 1),0);

	IF EXISTS (SELECT 
					VCT.numvendorcosttableid 
				FROM 
					Item I
				INNER JOIN
					Vendor V 
				ON
					I.numItemCode = V.numItemCode
				INNER JOIN 
					VendorCostTable VCT 
				ON 
					V.numVendorTCode=VCT.numVendorTCode 
				WHERE
					V.numDomainID=v_numDomainID
					AND V.numItemCode=v_numItemCode 
					AND V.numVendorID=v_numVendorID
					AND VCT.numCurrencyID = v_numCurrencyID
					AND v_numQty BETWEEN VCT.intFromQty * fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,v_numDomainID,I.numBaseUnit) AND VCT.intToQty * fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,v_numDomainID,I.numBaseUnit)) THEN
		SELECT 
			COALESCE(VCT.monDynamicCost,v_monVendorCost)
		INTO
			v_monVendorCost
		FROM 
			Item I
		INNER JOIN
			Vendor V 
		ON
			I.numItemCode = V.numItemCode
		INNER JOIN 
			VendorCostTable VCT 
		ON 
			V.numVendorTCode=VCT.numVendorTCode 
		WHERE
			V.numDomainID=v_numDomainID
			AND V.numItemCode=v_numItemCode 
			AND V.numVendorID=v_numVendorID
			AND VCT.numCurrencyID = v_numCurrencyID
			AND v_numQty BETWEEN VCT.intFromQty * fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,v_numDomainID,I.numBaseUnit) AND VCT.intToQty * fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,v_numDomainID,I.numBaseUnit);
	END IF;

	OPEN SWV_RefCur FOR
	SELECT
		v_monVendorCost AS "monCost"
		,(SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numVendorID ORDER BY coalesce(bitPrimaryContact,false) DESC LIMIT 1) AS "numContactID";

	OPEN SWV_RefCur2 FOR
	SELECT
		LD.numListItemID AS "numListItemID"
		,LD.vcData AS "vcData"
		,COALESCE(TEMPLeadTimes.numLeadDays,-1) AS "numListValue"
		,COALESCE(TEMPLeadTimes.vcShippingServices,'') AS "vcShippingServices"
		,(CASE WHEN TEMPLeadTimes.numLeadDays IS NOT NULL THEN CONCAT('(',TEMPLeadTimes.numLeadDays,' Days)') ELSE 'NA' END) AS "vcLeadDays"
	FROM
		Listdetails LD
	LEFT JOIN LATERAL
	(
		SELECT 
			(VLT.vcdata->'DaysToArrive')::INT AS numLeadDays
			,REPLACE(REPLACE((VLT.vcdata->'ShippingServices')::VARCHAR,'[',''),']','') AS vcShippingServices
		FROM
			VendorLeadTimes VLT
		WHERE
			VLT.numdivisionid = v_numVendorID
			AND v_numQty BETWEEN (VLT.vcdata->'FromQuantity')::INT AND (VLT.vcdata->'ToQuantity')::INT
			AND VLT.vcdata->'ItemGroups' @> COALESCE(v_numItemClassification,0)::VARCHAR::JSONB
			AND VLT.vcdata->'ShipFromLocations' @> COALESCE(v_numVendorPrimaryShipToAddress,0)::VARCHAR::JSONB
			AND (VLT.vcdata->'ShipVia')::INT::numeric = LD.numListItemID
			AND (VLT.vcdata->'ShipToCountry')::INT::numeric = COALESCE(v_numCountry,0)
			AND (jsonb_array_length(VLT.vcdata->'ShipToStates') = 0 OR VLT.vcdata->'ShipToStates' @> COALESCE(v_numState,0)::VARCHAR::JSONB)
		ORDER BY
			VLT.numvendorleadtimesid DESC
		LIMIT 1
	) TEMPLeadTimes ON TRUE
   WHERE
		(LD.numDomainid = v_numDomainID OR coalesce(LD.constFlag,false) = true)
		AND LD.numListID = 82
   ORDER BY
		LD.vcData ASC;

   RETURN;
END; $$;



