-- Stored procedure definition script USP_BizDocsViewedDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDocsViewedDtls(v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numOppBizDocs NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update OpportunityBizDocs set numViewedBy = v_numContactID,dtViewedDate = TIMEZONE('UTC',now())
   where numOppBizDocsId = v_numOppBizDocs;
   RETURN;
END; $$;


