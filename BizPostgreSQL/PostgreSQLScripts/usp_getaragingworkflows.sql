-- Stored procedure definition script USP_GetARAgingWorkFlows for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetARAgingWorkFlows(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM WorkFlowMaster WFM
   WHERE WFM.intDays <> 0 AND coalesce(WFM.vcDateField,'') = ''
   AND WFM.bitActive = true AND WFM.tintWFTriggerOn = 6;
END; $$;














