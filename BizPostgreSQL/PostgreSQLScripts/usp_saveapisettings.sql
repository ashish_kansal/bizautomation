-- Stored procedure definition script USP_SaveAPISettings for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveAPISettings(v_WebApiId    INTEGER,
               v_numDomainId NUMERIC(9,0),
               v_vcValue1    VARCHAR(50)  DEFAULT NULL,
               v_vcValue2    VARCHAR(50)  DEFAULT NULL,
               v_vcValue3    VARCHAR(50)  DEFAULT NULL,
               v_vcValue4    VARCHAR(50)  DEFAULT NULL,
               v_vcValue5    VARCHAR(50)  DEFAULT NULL,
               v_vcValue6    VARCHAR(50)  DEFAULT NULL,
               v_vcValue7    VARCHAR(50)  DEFAULT NULL,
                v_vcValue8    VARCHAR(1000)  DEFAULT NULL,
               v_vcValue9    VARCHAR(50)  DEFAULT NULL,
               v_vcValue10   VARCHAR(50)  DEFAULT NULL,
			   v_vcValue11	VARCHAR(50) DEFAULT NULL,
			    v_vcValue12	VARCHAR(50) DEFAULT NULL,
			   v_bitEnableAPI BOOLEAN DEFAULT NULL,
				v_numBizDocId NUMERIC DEFAULT NULL,
				v_numBizDocStatusId NUMERIC DEFAULT NULL,
				v_numOrderStatus NUMERIC DEFAULT NULL,
				v_numFBABizDocId NUMERIC DEFAULT NULL,
				v_numFBABizDocStatusId NUMERIC DEFAULT NULL,
				v_numUnShipmentOrderStatus NUMERIC DEFAULT NULL,
				v_numRecordOwner NUMERIC DEFAULT NULL,
				v_numAssignTo NUMERIC DEFAULT NULL,
				v_numWareHouseID NUMERIC DEFAULT NULL,
				v_numRelationshipId NUMERIC DEFAULT NULL,
				v_numProfileId NUMERIC DEFAULT NULL,
				v_numExpenseAccountId NUMERIC DEFAULT NULL,
			    v_numDiscountItemMapping NUMERIC DEFAULT NULL,
				v_numSalesTaxItemMapping NUMERIC DEFAULT NULL,
				v_bitEnableItemUpdate BOOLEAN DEFAULT NULL,
				v_bitEnableOrderImport BOOLEAN DEFAULT NULL,
				v_bitEnableTrackingUpdate BOOLEAN DEFAULT NULL,
				v_bitEnableInventoryUpdate BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   IF v_numDomainId > 0 then
      select   COUNT(*) INTO v_Check FROM COARelationships WHERE numDomainID = v_numDomainId
      AND numRelationshipID = v_numRelationshipId AND coalesce(numARAccountId,0) > 0 AND coalesce(numAPAccountId,0) > 0;
      IF v_Check = 0 AND v_bitEnableAPI = true then
		
         RAISE NOTICE 'AR_and_AP_Relationship_NotSet';
         RETURN;
      end if;
      IF NOT EXISTS(SELECT * FROM   WebAPIDetail WHERE  WebApiId = v_WebApiId AND numDomainID = v_numDomainId) then
          
         INSERT INTO WebAPIDetail(WebApiId,
                        numDomainID)
            VALUES(v_WebApiId,
                        v_numDomainId);
      end if;
      UPDATE WebAPIDetail
      SET    vcFirstFldValue = v_vcValue1,vcSecondFldValue = v_vcValue2,vcThirdFldValue = v_vcValue3,
      vcFourthFldValue = v_vcValue4,vcFifthFldValue = v_vcValue5,
      vcSixthFldValue = v_vcValue6,vcSeventhFldValue = v_vcValue7,vcEighthFldValue = v_vcValue8,
      vcNinthFldValue = v_vcValue9,vcTenthFldValue = v_vcValue10,
      vcEleventhFldValue = v_vcValue11,vcTwelfthFldValue =  v_vcValue12,
      bitEnableAPI = v_bitEnableAPI,numRelationshipId = v_numRelationshipId,
      numProfileId = v_numProfileId,numAssignTo = v_numAssignTo,numBizDocId = v_numBizDocId,
      numBizDocStatusId = v_numBizDocStatusId,numOrderStatus = v_numOrderStatus,
      numFBABizDocId = v_numFBABizDocId,numFBABizDocStatusId = v_numFBABizDocStatusId,
      numUnShipmentOrderStatus = v_numUnShipmentOrderStatus,
      numRecordOwner = v_numRecordOwner,numwarehouseID = v_numWareHouseID,
      numExpenseAccountId = v_numExpenseAccountId,numDiscountItemMapping = v_numDiscountItemMapping,
      numSalesTaxItemMapping = v_numSalesTaxItemMapping,
      bitEnableItemUpdate = v_bitEnableItemUpdate,bitEnableOrderImport = v_bitEnableOrderImport,
      bitEnableTrackingUpdate = v_bitEnableTrackingUpdate,
      bitEnableInventoryUpdate = v_bitEnableInventoryUpdate
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   end if;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_SaveImportConfg]    Script Date: 07/26/2008 16:21:03 ******/



