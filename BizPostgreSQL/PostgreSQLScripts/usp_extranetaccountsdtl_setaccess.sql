-- Stored procedure definition script USP_ExtranetAccountsDtl_SetAccess for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtranetAccountsDtl_SetAccess(v_numDomainID NUMERIC(18,0)
	,v_vcRecords TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intNoofBusinessPortalUsers  INTEGER;
   v_numBusinessPortalUsers  INTEGER;
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   coalesce(intNoofBusinessPortalUsers,0) INTO v_intNoofBusinessPortalUsers FROM
      Subscribers WHERE
      numTargetDomainID = v_numDomainID;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numExtranetDtlID NUMERIC(18,0),
         tintAccessAllowed SMALLINT,
         bitPartnerAccess BOOLEAN
      );
 
      INSERT INTO tt_TEMP(numExtranetDtlID
         ,tintAccessAllowed
         ,bitPartnerAccess)
      SELECT
      numExtranetDtlID
         ,tintAccessAllowed 
         ,CAST(bitPartnerAccess AS BOOLEAN)
      FROM
	  XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcRecords AS XML)
				COLUMNS
					numExtranetDtlID NUMERIC(18,0) PATH 'numExtranetDtlID',
					tintAccessAllowed SMALLINT PATH 'tintAccessAllowed',
					bitPartnerAccess BOOLEAN PATH 'bitPartnerAccess'
			) X;


      v_numBusinessPortalUsers := coalesce((SELECT COUNT(numExtranetDtlID) FROM ExtranetAccountsDtl WHERE numDomainID = v_numDomainID AND coalesce(bitPartnerAccess,false) = true AND numExtranetDtlID NOT IN(SELECT numExtranetDtlID FROM tt_TEMP)),0);
      v_numBusinessPortalUsers := coalesce(v_numBusinessPortalUsers,0)+coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE coalesce(bitPartnerAccess,false) = true),0);
      IF v_numBusinessPortalUsers > v_intNoofBusinessPortalUsers then
	
         RAISE EXCEPTION 'BUSINESS_PORTAL_USERS_EXCEED';
      ELSE
         UPDATE
         ExtranetAccountsDtl EAD
         SET
         tintAccessAllowed = T.tintAccessAllowed,bitPartnerAccess = T.bitPartnerAccess
         FROM
         tt_TEMP T
         WHERE
         EAD.numExtranetDtlID = T.numExtranetDtlID AND EAD.numDomainID = v_numDomainID;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


