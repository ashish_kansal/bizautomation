-- Stored procedure definition script Reminder_Snooze for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Reminder_Snooze(v_LastSnoozDateTimeUtc TIMESTAMP, 
	v_ActivityID	INTEGER			-- the key of the Activity to be updated
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Activity
   SET
   LastSnoozDateTimeUtc = v_LastSnoozDateTimeUtc
   WHERE
   Activity.ActivityID = v_ActivityID;
   RETURN;
END; $$;


