DROP FUNCTION IF EXISTS fn_getCustomDataValue;

CREATE OR REPLACE FUNCTION fn_getCustomDataValue(v_vcSqlValue  VARCHAR(100),v_intOptions  NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetVal  VARCHAR(100);
BEGIN
   v_RetVal := '';
   v_vcSqlValue := (CASE WHEN v_vcSqlValue='true' THEN '1' WHEN v_vcSqlValue='false' THEN '0' ELSE v_vcSqlValue END);
   select   vcOptionName INTO v_RetVal from CustomReptOptValues where intOptions = v_intOptions and vcSQLValue ilike v_vcSqlValue;
   RETURN v_RetVal;
END; $$;

