-- Stored procedure definition script USP_GetSoltionForCases for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSoltionForCases(v_numCaseId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		S.numSolnID
		,vcSolnTitle as "Solution Name"
		,coalesce(txtSolution,'') as "Solution Descrition"
		,coalesce(txtSolution,'') as ShortDesc 
	from CaseSolutions C
   join SolutionMaster S
   on S.numSolnID = C.numSolnID
   where C.numCaseId = v_numCaseId;
END; $$;












