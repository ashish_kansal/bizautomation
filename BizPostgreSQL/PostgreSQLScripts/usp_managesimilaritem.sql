-- Stored procedure definition script USP_ManageSimilarItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSimilarItem(v_numDomainID NUMERIC(9,0),
	v_numParentItemCode NUMERIC(9,0),
	v_numItemCode NUMERIC(9,0),
	v_vcRelationship VARCHAR(500),
	v_bitPreUpSell BOOLEAN,
	v_bitPostUpSell BOOLEAN, 
	v_bitRequired BOOLEAN,
	v_vcUpSellDesc VARCHAR(1000),
	v_byteMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
	
      IF NOT EXISTS(SELECT * FROM SimilarItems WHERE numDomainId = v_numDomainID AND numItemCode = v_numItemCode AND numParentItemCode = v_numParentItemCode) then
		
         INSERT INTO SimilarItems(numDomainId,
				numItemCode,
				numParentItemCode,
				vcRelationship,
				bitPreUpSell,
				bitPostUpSell,
				bitRequired,
				vcUpSellDesc) VALUES(  
				/* numDomainID - numeric(18, 0) */ v_numDomainID,
				/* vcExportTables - varchar(100) */ v_numItemCode,
				/* dtLastExportedOn - datetime */ v_numParentItemCode,
				v_vcRelationship,
				v_bitPreUpSell,
				v_bitPostUpSell,
				v_bitRequired,
				v_vcUpSellDesc);
      end if;
   ELSEIF v_byteMode = 1
   then
	
      UPDATE SimilarItems
      SET bitPreUpSell = v_bitPreUpSell,bitPostUpSell = v_bitPostUpSell,bitRequired =	v_bitRequired
      WHERE numParentItemCode = v_numParentItemCode
      AND numItemCode = v_numItemCode
      AND numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


