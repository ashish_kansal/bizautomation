-- Stored procedure definition script USP_GetTaxForCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTaxForCompany(v_numDivID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBillCountry  NUMERIC(9,0);
   v_numBillState  NUMERIC(9,0); 
   v_numDomainID  NUMERIC(9,0); 
   v_TaxPercentage  DECIMAL;
BEGIN
   select   coalesce(numState,0), coalesce(numCountry,0), DM.numDomainID INTO v_numBillState,v_numBillCountry,v_numDomainID from DivisionMaster DM   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true where numDivisionID = v_numDivID; 

   v_TaxPercentage := 0;
   if v_numBillCountry > 0 and v_numBillState > 0 then

      if v_numBillState > 0 then
    
         if exists(select count(*) from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState and numDomainId = v_numDomainID) then
            select   decTaxPercentage INTO v_TaxPercentage from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState and numDomainId = v_numDomainID;
         else
            select   decTaxPercentage INTO v_TaxPercentage from TaxDetails where numCountryID = v_numBillCountry and numStateID = 0 and numDomainId = v_numDomainID;
         end if;
      end if;
   ELSEIF v_numBillCountry > 0
   then
      select   decTaxPercentage INTO v_TaxPercentage from TaxDetails where numCountryID = v_numBillCountry and numStateID = 0 and numDomainId = v_numDomainID;
   end if;
    

   open SWV_RefCur for select v_TaxPercentage;
END; $$;












