-- Function definition script GetOrderShipReceiveStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOrderShipReceiveStatus(v_numOppID NUMERIC(18,0),
	  v_tintOppType NUMERIC(18,0),
	  v_tintshipped SMALLINT,
	  v_tintDecimalPoints SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcShippedReceivedStatus  VARCHAR(100) DEFAULT '';

   v_TotalQty  NUMERIC DEFAULT 0;
   v_numQtyShipped  NUMERIC DEFAULT 0;
   v_numQtyReceived  NUMERIC DEFAULT 0;
BEGIN
   IF v_tintOppType = 1 then --SALES ORDER
      v_TotalQty := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID),0);

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
      IF coalesce(v_tintshipped,0) = 1 then
		
         v_numQtyShipped := v_TotalQty;
      ELSE
         v_numQtyShipped := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID),0);
      end if;
      v_vcShippedReceivedStatus :=(CASE WHEN  v_TotalQty = v_numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) || CONCAT((CASE v_tintDecimalPoints
      WHEN 1 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.9' ELSE '' END))
      WHEN 2 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99' ELSE '' END))
      WHEN 3 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.999' ELSE '' END))
      WHEN 4 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.9999' ELSE '' END))
      WHEN 5 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99999' ELSE '' END))
      ELSE
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99' ELSE '' END))
      END),' / ',(CASE v_tintDecimalPoints
      WHEN 1 THEN
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.9' ELSE '' END))
      WHEN 2 THEN
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.99' ELSE '' END))
      WHEN 3 THEN
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.999' ELSE '' END))
      WHEN 4 THEN
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.9999' ELSE '' END))
      WHEN 5 THEN
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.99999' ELSE '' END))
      ELSE
         TO_CHAR(v_numQtyShipped,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyShipped,1) <> 0 THEN '.99' ELSE '' END))
      END)) || '</font></b>';
   ELSEIF v_tintOppType = 2
   then --PURCHASE ORDER
      v_TotalQty := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID),0);
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
      IF coalesce(v_tintshipped,0) = 1 then
		
         v_numQtyReceived := v_TotalQty;
      ELSE
         v_numQtyReceived := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID),0);
      end if;
      v_vcShippedReceivedStatus :=(CASE WHEN  v_TotalQty = v_numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) || CONCAT((CASE v_tintDecimalPoints
      WHEN 1 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.9' ELSE '' END))
      WHEN 2 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99' ELSE '' END))
      WHEN 3 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.999' ELSE '' END))
      WHEN 4 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990.' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '9999' ELSE '' END))
      WHEN 5 THEN
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99999' ELSE '' END))
      ELSE
         TO_CHAR(v_TotalQty,'FM9,999,999,999,990' || (CASE WHEN MOD(v_TotalQty,1) <> 0 THEN '.99' ELSE '' END))
      END),' / ',(CASE v_tintDecimalPoints
      WHEN 1 THEN
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.9' ELSE '' END))
      WHEN 2 THEN
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.99' ELSE '' END))
      WHEN 3 THEN
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.999' ELSE '' END))
      WHEN 4 THEN
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.9999' ELSE '' END))
      WHEN 5 THEN
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.99999' ELSE '' END))
      ELSE
         TO_CHAR(v_numQtyReceived,'FM9,999,999,999,990' || (CASE WHEN MOD(v_numQtyReceived,1) <> 0 THEN '.99' ELSE '' END))
      END)) || '</font></b>';
   end if;

   RETURN v_vcShippedReceivedStatus;
END; $$;

