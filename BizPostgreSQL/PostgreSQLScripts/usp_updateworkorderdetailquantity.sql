-- Stored procedure definition script USP_UpdateWorkOrderDetailQuantity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateWorkOrderDetailQuantity(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numUnitHour DOUBLE PRECISION,
	v_numWOId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPickedQty  DOUBLE PRECISION DEFAULT 0;
   v_numOppID  NUMERIC(18,0);
   v_numCurrentQty  DOUBLE PRECISION;
   v_numWarehouseItmsID  NUMERIC(18,0);

   v_Description  VARCHAR(300);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   v_numPickedQty := coalesce((SELECT
   MAX(numPickedQty)
   FROM(SELECT
      WorkOrderDetails.numWODetailId
										,CEIL(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig:: NUMERIC) AS numPickedQty
      FROM
      WorkOrderDetails
      INNER JOIN
      WorkOrderPickedItems
      ON
      WorkOrderDetails.numWODetailId = WorkOrderPickedItems.numWODetailId
      WHERE
      WorkOrderDetails.numWOId = v_numWOId
      AND coalesce(WorkOrderDetails.numWarehouseItemID,0) > 0
      GROUP BY
      WorkOrderDetails.numWODetailId,WorkOrderDetails.numQtyItemsReq_Orig) TEMP),
   0);

   IF v_numUnitHour < coalesce((v_numPickedQty),0) then
	
      RAISE EXCEPTION 'WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY';
      RETURN;
   end if;

   IF EXISTS(SELECT
   SPDTTL.ID
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   StagePercentageDetailsTaskTimeLog SPDTTL
   ON
   SPDT.numTaskId = SPDTTL.numTaskId
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND SPDT.numWorkOrderId = v_numWOId) then
	
      RAISE EXCEPTION 'TASKS_ARE_ALREADY_STARTED';
      RETURN;
   end if;

   IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOId AND numWOStatus = 23184) then
	
      RAISE EXCEPTION 'WORKORDR_COMPLETED';
      RETURN;
   end if;

   BEGIN
      -- BEGIN TRANSACTION
PERFORM USP_RevertInventoryWorkOrder(v_numDomainID,v_numUserCntID,v_numWOId);
      select   WorkOrder.numOppId, numQtyItemsReq, numWareHouseItemId INTO v_numOppID,v_numCurrentQty,v_numWarehouseItmsID FROM
      WorkOrder WHERE
      numWOId = v_numWOId;
      WITH CTE(numWODetailId,numItemKitID,numItemCode,numCalculatedQty)
      AS(SELECT
      dtl.numWODetailId AS numWODetailId
				,CAST(v_numItemCode AS NUMERIC(18,0)) AS numItemKitID
				,numItemCode AS numItemCode
				,CAST((dtl.numQtyItemsReq_Orig*v_numUnitHour) AS DOUBLE PRECISION) AS numCalculatedQty
      FROM
      Item
      INNER JOIN
      WorkOrderDetails dtl
      ON
      numChildItemID = numItemCode
      WHERE
      numItemKitID = v_numItemCode
      AND dtl.numWOId = v_numWOId
      UNION ALL
      SELECT
      dtl.numWODetailId AS numWODetailId
				,CAST(dtl.numItemKitID AS NUMERIC(18,0)) AS numItemKitID
				,i.numItemCode AS numItemCode
				,CAST((dtl.numQtyItemsReq_Orig*c.numCalculatedQty) AS DOUBLE PRECISION) AS numCalculatedQty
      FROM
      Item i
      INNER JOIN
      WorkOrderDetails dtl
      ON
      dtl.numChildItemID = i.numItemCode
      INNER JOIN
      CTE c
      ON
      dtl.numItemKitID = c.numItemCode
      WHERE
      dtl.numWOId = v_numWOId)
      UPDATE
      WorkOrderDetails WOD
      SET
      numQtyItemsReq = numCalculatedQty
      FROM
      CTE c
      WHERE
      WOD.numWODetailId = c.numWODetailId AND WOD.numWOId = v_numWOId;
      PERFORM USP_ManageInventoryWorkOrder(v_numWOId,v_numDomainID,v_numUserCntID);

		--UPDATE ON ORDER OF ASSEMBLY
      UPDATE
      WareHouseItems
      SET
      numonOrder =(coalesce(numonOrder,0) -v_numCurrentQty)+v_numUnitHour,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItmsID; 
	
		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  tinyint
		 --  varchar(100)
      v_Description := CONCAT('Work Order Edited (Qty:',v_numUnitHour,')');
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWarehouseItmsID,v_numReferenceID := v_numWOId,
      v_tintRefType := 2::SMALLINT,v_vcDescription := v_Description,v_numModifiedBy := v_numUserCntID,
      v_numDomainID := v_numDomainID,SWV_RefCur := NULL,
      SWV_RefCur2 := NULL);
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;



