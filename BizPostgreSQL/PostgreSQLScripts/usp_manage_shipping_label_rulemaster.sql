-- Stored procedure definition script USP_MANAGE_SHIPPING_LABEL_RULEMASTER for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_MANAGE_SHIPPING_LABEL_RULEMASTER(INOUT v_numShippingRuleID NUMERIC(18,0) ,
	v_vcRuleName	VARCHAR(100),
	v_tintShipBasedOn SMALLINT,
	v_numSourceCompanyID NUMERIC(18,0),
	v_numSourceShipID NUMERIC(18,0),
	v_intItemAffected INTEGER,
	v_tintType SMALLINT,
	v_numDomainID NUMERIC(18,0))
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM ShippingLabelRuleMaster WHERE vcRuleName = v_vcRuleName AND numShippingRuleID <> v_numShippingRuleID AND numDomainID = v_numDomainID) then
	
	     -- Message text.
				    -- Severity.
				    -- State.
      RAISE EXCEPTION 'ERROR: Rule Name already exists. Please provide another Rule Name.';
      RETURN;
   end if;

   IF EXISTS(SELECT * FROM ShippingLabelRuleMaster WHERE numShippingRuleID <> v_numShippingRuleID
   AND vcRuleName = v_vcRuleName
   AND tintShipBasedOn = v_tintShipBasedOn
   AND numSourceCompanyID = v_numSourceCompanyID
   AND numSourceCompanyID = v_numSourceCompanyID
   AND numSourceShipID = v_numSourceShipID
   AND intItemAffected = v_intItemAffected
   AND tintType = v_tintType
   AND numDomainID = v_numDomainID) then
	
		 -- Message text.
				    -- Severity.
				    -- State.
      RAISE EXCEPTION 'ERROR: Rule Detail already exists in another rule. Please use existing rule OR create a new rule detail.';
      RETURN;
   end if;	
	
   IF EXISTS(SELECT numShippingRuleID FROM ShippingLabelRuleMaster WHERE numShippingRuleID = v_numShippingRuleID) then

      UPDATE ShippingLabelRuleMaster SET
      vcRuleName = v_vcRuleName,tintShipBasedOn = v_tintShipBasedOn,numSourceCompanyID = v_numSourceCompanyID,
      numSourceShipID = v_numSourceShipID,
      intItemAffected = v_intItemAffected,tintType = v_tintType,numDomainID = v_numDomainID
      WHERE
      numShippingRuleID = v_numShippingRuleID;
   ELSE
      INSERT INTO ShippingLabelRuleMaster(vcRuleName,
		tintShipBasedOn,
		numSourceCompanyID,
		numSourceShipID,
		intItemAffected,
		tintType,
		numDomainID) VALUES(v_vcRuleName,
		v_tintShipBasedOn,
		v_numSourceCompanyID,
		v_numSourceShipID,
		v_intItemAffected,
		v_tintType,
		v_numDomainID);
	
      v_numShippingRuleID := CURRVAL('ShippingLabelRuleMaster_seq');
   end if;

   DELETE FROM ShippingLabelRuleChild WHERE numShippingRuleID = v_numShippingRuleID;
END; $$; 
--SELECT * FROM [AccountTypeDetail] Where len(vcaccountcode)>4
--DELETE FROM [AccountTypeDetail] WHERE [numAccountTypeID]>2232


