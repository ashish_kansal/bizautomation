DROP FUNCTION IF EXISTS usp_vendorcosttable_updatecost;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_updatecost
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numvendortcode NUMERIC(18,0)
	,p_numvendorid NUMERIC(18,0)
	,p_numitemcode NUMERIC(18,0)
	,p_numcurrencyid NUMERIC(18,0)
	,p_numvendorcosttableid NUMERIC(18,0)
	,p_monStaticCost DECIMAL(20,5)
	,p_monDynamicCost DECIMAL(20,5)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		VendorCostTable
	SET
		numStaticCostModifiedBy=(CASE WHEN monStaticCost <> p_monStaticCost THEN p_numusercntid ELSE numStaticCostModifiedBy END)
		,dtStaticCostModifiedDate=(CASE WHEN monStaticCost <> p_monStaticCost THEN timezone('utc', now()) ELSE dtStaticCostModifiedDate END)
		,monStaticCost = p_monStaticCost
		,numDynamicCostModifiedBy=(CASE WHEN monDynamicCost <> p_monDynamicCost THEN p_numusercntid ELSE numDynamicCostModifiedBy END)
		,dtDynamicCostModifiedDate=(CASE WHEN monDynamicCost <> p_monDynamicCost THEN timezone('utc', now()) ELSE dtDynamicCostModifiedDate END)
		,bitIsDynamicCostByOrder=(CASE WHEN monDynamicCost <> p_monDynamicCost THEN false ELSE bitIsDynamicCostByOrder END)
		,monDynamicCost = p_monDynamicCost
	FROM
		Vendor V
	WHERE
		V.numVendorTcode = VendorCostTable.numVendorTcode
		AND V.numvendortcode = p_numvendortcode
		AND V.numDomainID = p_numdomainid
		AND V.numVendorID = p_numvendorid
		AND V.numItemCode = p_numitemcode
		AND VendorCostTable.numvendorcosttableid = p_numvendorcosttableid;
END; $$;