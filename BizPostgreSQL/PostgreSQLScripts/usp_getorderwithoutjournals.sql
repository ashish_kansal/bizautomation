-- Stored procedure definition script USP_GetOrderWithoutJournals for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOrderWithoutJournals(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
   OM.numOppId ,
                OM.numDomainId ,
                OM.numDivisionId ,
                OM.vcpOppName ,
                OM.dtItemReceivedDate ,
                OM.monDealAmount ,
                OM.tintopptype ,
                OM.numCreatedBy
   FROM    OpportunityMaster AS OM
   JOIN OpportunityItems AS OI ON OM.numOppId = OI.numOppId
   WHERE   1 = 1
		--AND [OM].[tintshipped] = 0
   AND OM.numDomainId = 169
   AND OM.tintopptype = 2
		--AND OM.[dtItemReceivedDate] >= '2013-09-30 00:00:59' 
   AND coalesce(OI.numUnitHour,0) = coalesce(OI.numUnitHourReceived,0)
   AND OI.numOppId NOT IN(SELECT  cast(coalesce(numOppId,0) as NUMERIC(18,0))
      FROM    General_Journal_Header AS GJH
      WHERE   GJH.numDomainId = OM.numDomainId
      AND coalesce(GJH.numOppBizDocsId,0) = 0);
END; $$;











