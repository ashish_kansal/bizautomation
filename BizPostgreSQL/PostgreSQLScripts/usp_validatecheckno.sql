-- Stored procedure definition script USP_ValidateCheckNo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ValidateCheckNo(v_numDomainId NUMERIC(18,0),
      v_numChartAcntId NUMERIC(18,0),
      v_vcCheckNos VARCHAR(1000) ,
      v_numCheckHeaderID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT numCheckNo as "numCheckNo" FROM CheckHeader WHERE numDomainID = v_numDomainId AND numChartAcntId = v_numChartAcntId AND
   numCheckNo IN(SELECT Id FROM SplitIds(v_vcCheckNos,',')) AND (numCheckHeaderID != v_numCheckHeaderID OR v_numCheckHeaderID = 0)
   ORDER BY numCheckNo;
END; $$;
    












