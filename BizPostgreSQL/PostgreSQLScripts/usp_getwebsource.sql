-- Stored procedure definition script USP_GetWebSource for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWebSource(v_numDomainID NUMERIC(9,0),
                v_dtStartDate TIMESTAMP,
                v_dtEndDate   TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_total  NUMERIC;
BEGIN
   DROP TABLE IF EXISTS WebSource CASCADE;
   CREATE TEMPORARY TABLE WebSource ON COMMIT DROP AS
      SELECT   
      vcDomainName,
             SUM(1) AS percentage
             
      FROM     TrackingVisitorsHDR
      WHERE    numDomainID = v_numDomainID
      AND dtcreated >= v_dtStartDate
      AND dtcreated <= v_dtEndDate
      GROUP BY vcDomainName
      ORDER BY SUM(1) DESC LIMIT 5;
    
    
   select   SUM(percentage) INTO v_total FROM WebSource;
--    PRINT @total
    
   
   open SWV_RefCur for SELECT   vcDomainName  AS "Source",
			cast(coalesce(percentage::bigint*100/v_total,0) as INTEGER) AS "percentage"
   FROM WebSource
   GROUP BY vcDomainName,percentage
   ORDER BY percentage DESC;

   
   RETURN;
END; $$;


