-- Stored procedure definition script USP_CustomReportTest for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CustomReportTest(v_ParamnumDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCustomReportID  NUMERIC;
   v_vcReportName  VARCHAR(100);
   v_textQuery  TEXT;
   v_numCreatedBy  NUMERIC;
   v_numDomainID  NUMERIC;
   v_ClientTimeZoneOffset  NUMERIC;
   v_vcError  TEXT;
   v_numROWCOUNT  NUMERIC;
   v_sql1  TEXT;
   v_sql  TEXT;
		
   Cust_Reports CURSOR FOR 
   SELECT numCustomReportId,vcReportName,textQuery,numCreatedBy,numdomainId FROM CustomReport WHERE numdomainId = v_ParamnumDomainID;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   create TEMPORARY TABLE tt_TEMP
   (
      numCustomReportID NUMERIC,
      vcReportName VARCHAR(100),
      numDomainID NUMERIC,
      numCreatedBy  NUMERIC,
      numROWCOUNT NUMERIC,
      vcError TEXT
   );
    
   v_ClientTimeZoneOffset := -330;
   OPEN Cust_Reports;

   FETCH NEXT FROM Cust_Reports into v_numCustomReportID,v_vcReportName,v_textQuery,v_numCreatedBy,v_numDomainID;
   WHILE FOUND LOOP
      v_sql := v_textQuery;
      v_sql1 := 'declare v_numUserCntId NUMERIC; v_ClientTimeZoneOffset INTEGER; v_numUserCntId :=' || v_numCreatedBy || ';';
      v_sql1 := coalesce(v_sql1,'') || ' v_ClientTimeZoneOffset:=' || v_ClientTimeZoneOffset || ';';

      IF POSITION('/*sum*/' IN v_sql) > 0 then
         v_sql1 := coalesce(v_sql1,'') || ' select @numROWCOUNT=count(*) from (' || SUBSTR(v_sql,0,POSITION('/*sum*/' IN v_sql)) || ') as temp';
      ELSE
         v_sql1 := coalesce(v_sql1,'') || ' select @numROWCOUNT=count(*) from (' || coalesce(v_sql,'') || ') as temp';
      end if;
      
	  v_sql1 := replace(v_sql1,'@numDomainId',v_numDomainID:: TEXT);
  
      v_vcError := '';
      v_numROWCOUNT := 0;
      BEGIN
         EXECUTE v_sql1 INTO v_numROWCOUNT;
         EXCEPTION WHEN OTHERS THEN
            v_vcError := SQLERRM;
      END;
      INSERT INTO tt_TEMP  values(v_numCustomReportID,v_vcReportName,v_numDomainID,v_numCreatedBy,v_numROWCOUNT,v_vcError);
					
      FETCH NEXT FROM Cust_Reports into v_numCustomReportID,v_vcReportName,v_textQuery,v_numCreatedBy,v_numDomainID;
   END LOOP;

   CLOSE Cust_Reports;
			

   open SWV_RefCur for SELECT * FROM tt_TEMP;
   RETURN;
END; $$;
