DROP FUNCTION IF EXISTS USP_GetItemPriceBasedOnMethod;

CREATE OR REPLACE FUNCTION USP_GetItemPriceBasedOnMethod(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_tintOppType SMALLINT,
	v_numItemCode NUMERIC(18,0),
	v_tintPriceMethod SMALLINT,
	v_monListPrice DECIMAL(20,5),
	v_monVendorCost DECIMAL(20,5),
	v_numQty DOUBLE PRECISION,
	v_fltUOMConversionFactor DOUBLE PRECISION,
	v_numCurrencyID NUMERIC(18,0),
	INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPriceLevel  SMALLINT;
   v_tintRuleType  SMALLINT;
   v_tintDisountType  SMALLINT;
   v_decDiscount  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5) DEFAULT 0;
   v_monFinalPrice  DECIMAL(20,5) DEFAULT NULL;
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_numItemClassification  NUMERIC(18,0);
   v_tintPriceBookDiscount  SMALLINT;
   v_numPriceRuleID  NUMERIC(18,0);
   v_tintPriceRuleType  SMALLINT;
   v_tintPricingMethod  SMALLINT;
   v_tintPriceBookDiscountType  SMALLINT;
   v_decPriceBookDiscount  DOUBLE PRECISION;
   v_intQntyItems  INTEGER;
   v_decMaxDedPerAmt  DOUBLE PRECISION;
   v_bitRoundTo BOOLEAN;
   v_tintRoundTo SMALLINT;
	v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
BEGIN
   select   numItemClassification INTO v_numItemClassification FROM Item WHERE numItemCode = v_numItemCode;

   select   tintPriceBookDiscount INTO v_tintPriceBookDiscount FROM Domain WHERE numDomainId = v_numDomainID;

	-- GET ORGANIZATION DETAL
   select   numCompanyType, vcProfile, coalesce(D.tintPriceLevel,0) INTO v_numRelationship,v_numProfile,v_tintPriceLevel FROM
   DivisionMaster D
   JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID WHERE
   numDivisionID = v_numDivisionID;    
   
   IF EXISTS (SELECT 
					VendorCostTable.numvendorcosttableid
				FROM 
					Vendor 
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Vendor.numDomainID=v_numDomainID
					AND Vendor.numVendorID=v_numDivisionID 
					AND Vendor.numItemCode=v_numItemCode 
					AND VendorCostTable.numCurrencyID = v_numCurrencyID
					AND COALESCE(v_numQty,0) * COALESCE(v_fltUOMConversionFactor,1) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty) THEN
		SELECT 
			COALESCE(VendorCostTable.monDynamicCost,v_monVendorCost)
			,COALESCE(VendorCostTable.monStaticCost,v_monVendorCost)
		INTO 
			v_monVendorDynamicCost
			,v_monVendorStaticCost
		FROM 
			Vendor 
		INNER JOIN 
			VendorCostTable 
		ON 
			Vendor.numvendortcode=VendorCostTable.numvendortcode 
		WHERE
			Vendor.numDomainID=v_numDomainID
			AND Vendor.numVendorID=v_numDivisionID 
			AND Vendor.numItemCode=v_numItemCode 
			AND VendorCostTable.numCurrencyID = v_numCurrencyID
			AND COALESCE(v_numQty,0) * COALESCE(v_fltUOMConversionFactor,1) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty;
	ELSE
		v_monVendorDynamicCost := v_monVendorCost;
		v_monVendorStaticCost := v_monVendorCost;
	END IF;
	
	

	IF v_tintOppType = 2 THEN
		IF v_tintPriceMethod = 1 OR v_tintPriceMethod=2 then -- PRICE LEVEL
			SELECT 
				VendorCostTable.monDynamicCost
			INTO 
				v_monPrice
			FROM 
				Vendor 
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Vendor.numDomainID=v_numDomainID
				AND Vendor.numVendorID=v_numDivisionID 
				AND Vendor.numItemCode=v_numItemCode 
				AND VendorCostTable.numCurrencyID = v_numCurrencyID
				AND COALESCE(v_numQty,0) * COALESCE(v_fltUOMConversionFactor,1) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty;

			open SWV_RefCur for
			  SELECT
			  coalesce(v_monPrice,v_monListPrice) AS monPrice,
					0 AS tintRuleType,
					1 AS tintDisountType,
					0 AS decDiscount;
		ELSEIF v_tintPriceMethod = 3 THEN
			SELECT 
				monPrice
				, coalesce(OpportunityItems.bitDiscountType,true)
				, coalesce(OpportunityItems.fltDiscount,0)
				,3
			INTO 
				v_monPrice
				,v_tintDisountType
				,v_decDiscount
				,v_tintRuleType 
			FROM
				OpportunityItems
			JOIN
				OpportunityMaster
			ON
				OpportunityItems.numOppId = OpportunityMaster.numOppId 
			WHERE
				OpportunityMaster.numDomainId = v_numDomainID
				AND tintopptype = v_tintOppType
				AND tintoppstatus = 1
				AND numItemCode = v_numItemCode   ORDER BY
				OpportunityMaster.numOppId DESC LIMIT 1;

			  open SWV_RefCur for
			  SELECT(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_monPrice,0) ELSE coalesce(v_monFinalPrice,0) END) AS monPrice,
					coalesce(v_tintRuleType,1) AS tintRuleType,
					(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_tintDisountType,1) ELSE 1 END) AS tintDisountType,
					(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_decDiscount,0) ELSE 0 END) AS decDiscount;
		END IF;
	ELSE
		IF v_tintPriceMethod = 1 then -- PRICE LEVEL
	
			  IF(SELECT COUNT(*) FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numCurrencyID,0) = 0 AND coalesce(numPriceRuleID,0) = 0) > 0 then
		
				 select   1, 0, tintRuleType, (CASE tintRuleType
				 WHEN 1 -- Deduct From List Price
				 THEN
					CASE tintDiscountType
					WHEN 1 -- PERCENT
					THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100)) ELSE 0 END)
					WHEN 2 -- FLAT AMOUNT
					THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty*v_fltUOMConversionFactor) -coalesce(decDiscount,0) > 0 THEN((v_monListPrice*v_numQty*v_fltUOMConversionFactor) -coalesce(decDiscount,0))/(v_numQty*v_fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
					WHEN 3 -- NAMED PRICE
					THEN coalesce(decDiscount,0)
					END
				 WHEN 2 -- Add to primary vendor cost
				 THEN
					CASE tintDiscountType
					WHEN 1  -- PERCENT
					THEN (CASE 
							WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
							WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
							ELSE v_monVendorCost 
						END) +((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *(coalesce(decDiscount,0)/100))*v_fltUOMConversionFactor
					WHEN 2 -- FLAT AMOUNT
					THEN(CASE WHEN((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *v_numQty*v_fltUOMConversionFactor)+coalesce(decDiscount,0) > 0 THEN(((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *v_numQty*v_fltUOMConversionFactor)+coalesce(decDiscount,0))/(v_numQty*v_fltUOMConversionFactor)  ELSE 0 END)
					WHEN 3 -- NAMED PRICE
					THEN coalesce(decDiscount,0)
					END
				 WHEN 3 -- Named price
				 THEN
					coalesce(decDiscount,0)
				 END) INTO v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPrice FROM(SELECT
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
							* FROM
					PricingTable
					WHERE
					PricingTable.numItemCode = v_numItemCode
					AND coalesce(PricingTable.numPriceRuleID,0) = 0
					AND coalesce(numCurrencyID,0) = 0) TEMP WHERE
				 1 =(CASE
				 WHEN coalesce(v_tintPriceLevel,0) > 0
				 THEN(CASE WHEN Id = v_tintPriceLevel THEN 1 ELSE 0 END)
				 ELSE(CASE WHEN ((v_numQty*v_fltUOMConversionFactor) >= intFromQty AND(v_numQty*v_fltUOMConversionFactor) <= intToQty) THEN 1 ELSE 0 END)
				 END);
			  end if;
			  open SWV_RefCur for
			  SELECT
			  coalesce(v_monPrice,v_monListPrice) AS monPrice,
					coalesce(v_tintRuleType,1) AS tintRuleType,
					1 AS tintDisountType,
					0 AS decDiscount;
		   ELSEIF v_tintPriceMethod = 2 AND(SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID = v_numDomainID AND tintRuleFor = v_tintOppType) > 0
		   then -- PRICE RULE
			  select 
				numPricRuleID, tintPricingMethod, p.tintRuleType, p.tintDiscountType, p.decDiscount, p.intQntyItems, p.decMaxDedPerAmt,P.bitRoundTo,p.tintRoundTo 
				INTO 
				v_numPriceRuleID,v_tintPricingMethod,v_tintPriceRuleType,v_tintPriceBookDiscountType,v_decPriceBookDiscount,v_intQntyItems,v_decMaxDedPerAmt,v_bitRoundTo,v_tintRoundTo 
			FROM
			  PriceBookRules p
			  LEFT JOIN
			  PriceBookRuleDTL PDTL
			  ON
			  p.numPricRuleID = PDTL.numRuleID
			  LEFT JOIN
			  PriceBookRuleItems PBI
			  ON
			  p.numPricRuleID = PBI.numRuleID
			  LEFT JOIN
			  PriceBookPriorities PP
			  ON
			  PP.Step2Value = p.tintStep2
			  AND PP.Step3Value = p.tintStep3 WHERE
			  p.numDomainID = v_numDomainID
			  AND tintRuleFor = v_tintOppType
			  AND (
							((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
			  OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
			  OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

			  OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
			  OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
			  OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

			  OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
			  OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND tintStep3 = 3) -- Priority 8
			  OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
						)   ORDER BY
			  PP.Priority ASC LIMIT 1;
			  IF coalesce(v_numPriceRuleID,0) > 0 then
		
				 IF v_tintPricingMethod = 1 then -- BASED ON PRICE TABLE
			
					select   tintDiscountType, coalesce(decDiscount,0), tintRuleType, (CASE tintRuleType WHEN 1 THEN v_monListPrice WHEN 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)  END), (CASE tintRuleType
					WHEN 1 -- Deduct From List Price
					THEN
					   CASE tintDiscountType
					   WHEN 1 -- PERCENT
					   THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100)) ELSE 0 END)
					   WHEN 2 -- FLAT AMOUNT
					   THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty*v_fltUOMConversionFactor) -coalesce(decDiscount,0) > 0 THEN((v_monListPrice*v_numQty*v_fltUOMConversionFactor) -coalesce(decDiscount,0))/(v_numQty*v_fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
					   WHEN 3 -- NAMED PRICE
					   THEN coalesce(decDiscount,0)
					   END
					WHEN 2 -- Add to primary vendor cost
					THEN
					   CASE tintDiscountType
					   WHEN 1  -- PERCENT
					   THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) +((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *(coalesce(decDiscount,0)/100))
					   WHEN 2 -- FLAT AMOUNT
					   THEN(CASE WHEN((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *v_numQty*v_fltUOMConversionFactor)+coalesce(decDiscount,0) > 0 THEN(((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *v_numQty*v_fltUOMConversionFactor)+coalesce(decDiscount,0))/(v_numQty*v_fltUOMConversionFactor)  ELSE 0 END)
					   WHEN 3 -- NAMED PRICE
					   THEN coalesce(decDiscount,0)
					   END
					WHEN 3 -- Named price
					THEN
					   CASE
					   WHEN coalesce(v_tintPriceLevel,0) > 0
					   THEN(SELECT
							 coalesce(decDiscount,0)
							 FROM(SELECT
								ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
																	decDiscount
								FROM
								PricingTable
								WHERE
								PricingTable.numItemCode = v_numItemCode
								AND coalesce(numCurrencyID,0) = 0
								AND tintRuleType = 3) TEMP
							 WHERE
							 Id = v_tintPriceLevel)
					   ELSE
						  coalesce(decDiscount,0)
					   END
					END) INTO v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPrice,v_monFinalPrice FROM
					PricingTable WHERE
					numPriceRuleID = v_numPriceRuleID
					AND coalesce(numCurrencyID,0) = 0
					AND v_numQty*v_fltUOMConversionFactor >= intFromQty AND v_numQty*v_fltUOMConversionFactor <= intToQty;
				 ELSEIF v_tintPricingMethod = 2
				 then -- BASED ON RULE
			
					v_monPrice :=(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)  ELSE v_monListPrice END);
					IF (v_tintPriceBookDiscountType = 1) then -- Percentage 
				
					   v_tintDisountType := v_tintPriceBookDiscountType;
					   v_decDiscount := v_decPriceBookDiscount*(v_numQty/v_intQntyItems::bigint);
					   v_decDiscount := v_decPriceBookDiscount*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)  ELSE v_monListPrice END)/100;
					   v_decMaxDedPerAmt := v_decMaxDedPerAmt*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)  ELSE v_monListPrice END)/100;
					   IF (v_decDiscount > v_decMaxDedPerAmt) then
						  v_decDiscount := v_decMaxDedPerAmt;
					   end if;
					   IF (v_tintPriceRuleType = 1) then
						  v_monFinalPrice :=(v_monListPrice -v_decDiscount);
					   end if;
					   IF (v_tintPriceRuleType = 2) then
						  v_monFinalPrice :=((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) +v_decDiscount);
					   end if;
					end if;
					IF (v_tintPriceBookDiscountType = 2) then -- Flat discount 
				
					   v_decDiscount := v_decPriceBookDiscount*((v_numQty*v_fltUOMConversionFactor)/v_intQntyItems::bigint);
					   IF (v_decDiscount > v_decMaxDedPerAmt) then
						  v_decDiscount := v_decMaxDedPerAmt;
					   end if;
					   IF (v_tintPriceRuleType = 1) then
						  v_monFinalPrice :=((v_monListPrice*v_numQty*v_fltUOMConversionFactor) -v_decDiscount)/(v_numQty*v_fltUOMConversionFactor);
					   end if;
					   IF (v_tintPriceRuleType = 2) then
						  v_monFinalPrice :=(((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) *v_numQty*v_fltUOMConversionFactor)+v_decDiscount)/(v_numQty*v_fltUOMConversionFactor);
					   end if;
					end if;
				 end if;
			  end if;
			  
			  OPEN SWV_RefCur FOR
			  SELECT 
				(CASE 
					WHEN v_monFinalPrice IS NULL 
					THEN v_monListPrice 
					ELSE (CASE 
							WHEN v_tintPriceBookDiscount = 1 
							THEN coalesce(v_monPrice,0) 
							ELSE (CASE 
									WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
									THEN ROUND(coalesce(v_monFinalPrice,0)) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
									ELSE coalesce(v_monFinalPrice,0) 
								END) 
						END) 
					END) AS monPrice,
					coalesce(v_tintRuleType,1) AS tintRuleType,
					(CASE WHEN v_monFinalPrice IS NULL THEN 1 ELSE(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_tintPriceBookDiscountType,1) ELSE 1 END) END) AS tintDisountType,
					(CASE WHEN v_monFinalPrice IS NULL THEN 0 ELSE(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_decPriceBookDiscount,0) ELSE 0 END) END) AS decDiscount;
		   ELSEIF v_tintPriceMethod = 3
		   then -- Last Price
	
			  select   monPrice, coalesce(OpportunityItems.bitDiscountType,true), coalesce(OpportunityItems.fltDiscount,0), 3 INTO v_monPrice,v_tintDisountType,v_decDiscount,v_tintRuleType FROM
			  OpportunityItems
			  JOIN
			  OpportunityMaster
			  ON
			  OpportunityItems.numOppId = OpportunityMaster.numOppId WHERE
			  OpportunityMaster.numDomainId = v_numDomainID
			  AND tintopptype = v_tintOppType
			  AND tintoppstatus = 1
			  AND numItemCode = v_numItemCode   ORDER BY
			  OpportunityMaster.numOppId DESC LIMIT 1;
			  open SWV_RefCur for
			  SELECT(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_monPrice,0) ELSE coalesce(v_monFinalPrice,0) END) AS monPrice,
					coalesce(v_tintRuleType,1) AS tintRuleType,
					(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_tintDisountType,1) ELSE 1 END) AS tintDisountType,
					(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_decDiscount,0) ELSE 0 END) AS decDiscount;
		   end if;
	END IF;

   
   RETURN;
END; $$;



