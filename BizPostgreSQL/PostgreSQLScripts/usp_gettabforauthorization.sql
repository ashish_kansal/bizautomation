-- Stored procedure definition script USP_GetTabForAuthorization for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTabForAuthorization(
--@numLocationID as numeric(9)=0       
v_numGroupID NUMERIC(9,0) DEFAULT 0  ,    
v_numRelationShip NUMERIC(9,0) DEFAULT 0,
v_numProfileID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
          
    
--if @numLocationID=0
--  begin    
   open SWV_RefCur for
   SELECT   numTabId,
            CASE WHEN bitFixed = true
   THEN CASE WHEN EXISTS(SELECT numTabName
         FROM   TabDefault
         WHERE  numDomainID = v_numDomainID
         AND numTabId = TM.numTabId
         AND tintTabType = TM.tintTabType)
      THEN(SELECT 
            numTabName
            FROM      TabDefault
            WHERE     numDomainID = v_numDomainID
            AND numTabId = TM.numTabId
            AND tintTabType = TM.tintTabType LIMIT 1)
      ELSE TM.numTabName
      END
   ELSE TM.numTabName
   END AS numTabname,
            tintTabType
   FROM     TabMaster TM
   WHERE    (numDomainID = v_numDomainID
   OR bitFixed = true)
   AND tintTabType =(SELECT  tintGroupType
      FROM    AuthenticationGroupMaster
      WHERE   numGroupID = v_numGroupID)
   AND numTabName NOT IN('POS','OmniBiz');    
			  
   open SWV_RefCur2 for
   SELECT   T.numTabId,
            CASE WHEN bitFixed = true
   THEN CASE WHEN EXISTS(SELECT numTabName
         FROM   TabDefault
         WHERE  numDomainID = v_numDomainID
         AND numTabId = T.numTabId
         AND tintTabType = T.tintTabType)
      THEN(SELECT 
            numTabName
            FROM      TabDefault
            WHERE     numDomainID = v_numDomainID
            AND numTabId = T.numTabId
            AND tintTabType = T.tintTabType LIMIT 1)
      ELSE T.numTabName
      END
   ELSE T.numTabName
   END AS numTabname,
            tintTabType,* FROM     TabMaster T
   JOIN GroupTabDetails G ON G.numTabId = T.numTabId
   WHERE    (numDomainID = v_numDomainID
   OR bitFixed = true) AND coalesce(tintType,0) = 0
   AND numGroupID = v_numGroupID
   AND coalesce(numRelationShip,0) = v_numRelationShip
   AND coalesce(numProfileID,0) = v_numProfileID
   AND numTabName NOT IN('POS','OmniBiz')
   ORDER BY numOrder;
   RETURN;
END; $$;


