-- Stored procedure definition script usp_GetBankStatementHeader for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetBankStatementHeader(v_numStatementID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT
   cast(numStatementID as VARCHAR(255)),
	cast(numBankDetailID as VARCHAR(255)),
	cast(vcFIStatementID as VARCHAR(255)),
	cast(monAvailableBalance as VARCHAR(255)),
	cast(dtAvailableBalanceDate as VARCHAR(255)),
	cast(numCurrencyID as VARCHAR(255)),
	cast(monLedgerBalance as VARCHAR(255)),
	cast(dtLedgerBalanceDate as VARCHAR(255)),
	cast(dtStartDate as VARCHAR(255)),
	cast(dtEndDate as VARCHAR(255)),
	cast(dtCreatedDate as VARCHAR(255)),
	cast(dtModifiedDate as VARCHAR(255))
   FROM
   BankStatementHeader
   WHERE
	(numStatementID = v_numStatementID
   or v_numStatementID = 0);
END; $$;














