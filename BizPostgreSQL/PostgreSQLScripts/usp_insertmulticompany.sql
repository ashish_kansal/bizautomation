-- Stored procedure definition script USP_InsertMultiCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertMultiCompany(v_numNewDomainID INTEGER,
v_numParentDomainID INTEGER,
v_numParentType INTEGER,
v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDomainCode  VARCHAR(50);
   v_vcStatus  VARCHAR(1);
   v_vcParentDomCode  VARCHAR(50);

--select cast( substring('01',2,1)  as numeric)
-- select * from DOmain

BEGIN
   if v_numParentType = 1 then
	
      select   '0' ||   cast(coalesce(max(cast(SUBSTR(vcDomainCode,1,LENGTH(vcDomainCode)) as NUMERIC))+1,
      1) as VARCHAR(30)) INTO v_vcDomainCode from Domain where numSubscriberID = v_numSubscriberID and numParentDomainID = 0;
      update Domain set numSubscriberID = v_numSubscriberID,numParentDomainID = 0,vcDomainCode = v_vcDomainCode where numDomainId = v_numNewDomainID;
      v_vcStatus := 'U';
   ELSEIF v_numParentType = 2
   then
	
      select   vcDomainCode INTO v_vcParentDomCode from Domain where numDomainId = v_numParentDomainID;
      select   coalesce(v_vcParentDomCode,'') || '0' ||  cast(coalesce(max(cast(SUBSTR(vcDomainCode,LENGTH(v_vcParentDomCode)+1,LENGTH(vcDomainCode)) as NUMERIC))+1,1) as VARCHAR(30)) INTO v_vcDomainCode from Domain where numParentDomainID = v_numParentDomainID and numSubscriberID = v_numSubscriberID;
      update Domain set numSubscriberID = v_numSubscriberID,numParentDomainID = v_numParentDomainID, 
      vcDomainCode = v_vcDomainCode where numDomainId = v_numNewDomainID;
      v_vcStatus := 'U';
   ELSEIF v_numParentType = 3
   then
	
      select   vcDomainCode INTO v_vcParentDomCode from Domain where numDomainId = v_numNewDomainID;
      update Domain set numSubscriberID = Null,numParentDomainID = Null,vcDomainCode = Null where numSubscriberID = v_numSubscriberID and vcDomainCode ilike coalesce(v_vcParentDomCode,'') || '%';
      v_vcStatus := 'U';
   end if;
   open SWV_RefCur for select v_vcStatus;
END; $$;

--exec USP_INVENTORYDASHBOARD @numDomainID=72,@PeriodFrom='01/Jan/2008',@PeriodTo='31/Dec/2008',@Type=2,@Top=2












