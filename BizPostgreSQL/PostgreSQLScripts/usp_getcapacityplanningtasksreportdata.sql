CREATE OR REPLACE FUNCTION USP_GetCapacityPlanningTasksReportData(v_numDomainID NUMERIC(18,0)                            
	,v_tintProcessType SMALLINT
	,v_numProcessID NUMERIC(18,0)
	,v_vcMilestone VARCHAR(300)
	,v_numStageDetailsId NUMERIC(18,0)
	,v_vcTaskIDs VARCHAR(200)
	,v_vcGradeIDs VARCHAR(200)
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_tintView SMALLINT --1:Day, 2:Week, 3:Month
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor , INOUT SWV_RefCur6 refcursor , INOUT SWV_RefCur7 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTempTaskID  NUMERIC(18,0);
   v_numTempParentTaskID  NUMERIC(18,0);
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numWorkOrderID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;

   v_numWorkScheduleID  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_dtStartDate  TIMESTAMP;
BEGIN
   open SWV_RefCur for
   SELECT
   vcMileStoneName AS "vcMileStoneName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR vcMileStoneName = v_vcMilestone)
   GROUP BY
   vcMileStoneName;

   open SWV_RefCur2 for
   SELECT
   numStageDetailsId AS "numStageDetailsId"
		,vcMileStoneName AS "vcMileStoneName"
		,vcStageName AS "vcStageName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR vcMileStoneName = v_vcMilestone)
   AND (coalesce(v_numStageDetailsId,0) = 0 OR numStageDetailsId = v_numStageDetailsId);


   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numStageDetailsId NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      vcTaskName VARCHAR(300),
      numTaskTimeInMinutes NUMERIC(18,0)
   );

   INSERT INTO
   tt_TEMP
   SELECT
   SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0)
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   SPD.numStageDetailsId = SPDT.numStageDetailsId
   WHERE
   SPD.numdomainid = v_numDomainID
   AND SPD.slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR SPD.vcMileStoneName = v_vcMilestone)
   AND (coalesce(v_numStageDetailsId,0) = 0 OR SPD.numStageDetailsId = v_numStageDetailsId)
   AND (coalesce(v_vcTaskIDs,'') = '' OR SPDT.numTaskId IN(SELECT ID FROM SplitIDs(v_vcTaskIDs,',')));

   open SWV_RefCur3 for
   SELECT  numStageDetailsId AS "numStageDetailsId",
      numTaskID AS "numTaskID",
      vcTaskName AS "vcTaskName",
      numTaskTimeInMinutes AS "numTaskTimeInMinutes" FROM tt_TEMP;

   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numWorkOrderID NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );

   DROP TABLE IF EXISTS tt_TEMPTIMELOG CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTIMELOG
   (
      numWrokOrder NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      numReferenceTaskId NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numAssignTo NUMERIC(18,0),
      numProductiveMinutes NUMERIC(18,0),
      dtDate TIMESTAMP,
      numTotalMinutes NUMERIC(18,0)
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numReferenceTaskId NUMERIC(18,0),
      numWorkOrderID NUMERIC(18,0),
      numAssignTo NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      intTaskType INTEGER,
      dtStartDate TIMESTAMP,
      dtFinishDate TIMESTAMP,
      numQtyItemsReq DOUBLE PRECISION,
      numProcessedQty DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPTASKS(numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty)
   SELECT
   SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(coalesce(numQtyItemsReq,0) -coalesce((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE
   WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId) > 0
   THEN(SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId)
   ELSE coalesce(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
   END)
		,WorkOrder.numQtyItemsReq
		,coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   WorkOrder
   ON
   SPDT.numWorkOrderId = WorkOrder.numWOId
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4) = 0 -- Task in not finished
		--TODO:uncomment if we have performance hit on live 
		--AND (ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)
   AND(coalesce(numQtyItemsReq,0) -coalesce((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0;

   INSERT INTO tt_TEMPTASKASSIGNEE(numWorkOrderID
		,numAssignedTo)
   SELECT
   numWorkOrderID
		,numAssignTo
   FROM
   tt_TEMPTASKS
   GROUP BY
   numWorkOrderID,numAssignTo;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

   WHILE v_i <= v_iCount LOOP
      select   numTaskID, numReferenceTaskId, numAssignTo, numTaskTimeInMinutes, intTaskType, dtStartDate, numWorkOrderID, numQtyItemsReq INTO v_numTempTaskID,v_numTempParentTaskID,v_numTaskAssignee,v_numTotalTaskInMinutes,
      v_intTaskType,v_dtStartDate,v_numWorkOrderID,v_numQtyItemsReq FROM
      tt_TEMPTASKS WHERE
      ID = v_i;

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
      select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), tmStartOfDay, CONCAT(',',vcWorkDays,','), CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval) INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_tmStartOfDay,v_vcWorkDays,
      v_dtStartDate FROM
      WorkSchedule WS
      INNER JOIN
      UserMaster
      ON
      WS.numUserCntID = UserMaster.numUserDetailId WHERE
      WS.numUserCntID = v_numTaskAssignee;
      IF v_intTaskType = 1 AND EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee;
      ELSEIF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
      then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee;
      end if;
      UPDATE tt_TEMPTASKS SET dtStartDate = v_dtStartDate WHERE ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtStartDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
					-- CHECK TIME LEFT FOR DAY BASED
               v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));
               IF v_numTimeLeftForDay > 0 then
					
                  INSERT INTO tt_TEMPTIMELOG(numWrokOrder
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes)
						VALUES(v_numWorkOrderID
							,v_numTempTaskID
							,v_numTempParentTaskID
							,v_numQtyItemsReq
							,v_numTaskAssignee
							,v_numProductiveTimeInMinutes
							,v_dtStartDate
							,(CASE WHEN v_numTimeLeftForDay > v_numTotalTaskInMinutes THEN v_numTotalTaskInMinutes ELSE v_numTimeLeftForDay END));
						
                  IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
						
                     v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                  ELSE
                     v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
                  end if;
                  v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
               ELSE
                  v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
               end if;
            ELSE
               v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
            end if;
         END LOOP;
      end if;
      UPDATE tt_TEMPTASKS SET dtFinishDate = v_dtStartDate WHERE ID = v_i;
      UPDATE tt_TEMPTASKASSIGNEE SET dtLastTaskCompletionTime = v_dtStartDate WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee;
      v_i := v_i::bigint+1;
   END LOOP;

   IF v_tintView = 1 then
	
      open SWV_RefCur4 for
      SELECT
      numReferenceTaskId AS "numTaskID"
			,TT.numTaskID AS "numActualTaskID"
			,WO.numWOId AS "numWOId"
			,coalesce(WO.vcWorkOrderName,'-') AS "vcWorkOrderName"
			,I.numItemCode AS "numItemCode"
			,coalesce(I.vcItemName,'-') AS "vcItemName"
			,TT.numAssignTo AS "numAssignTo"
			,coalesce(AdditionalContactsInformation.numTeam,0) AS "numTeam"
			,coalesce((SELECT vcGradeId FROM tblStageGradeDetails WHERE tblStageGradeDetails.numTaskID = numReferenceTaskId AND tblStageGradeDetails.numAssigneId = TT.numAssignTo),'') AS "vcGrade"
			,coalesce(TT.numProcessedQty,0) AS "numProcessedQty"
			,coalesce(WO.numQtyItemsReq,0) -coalesce(TT.numProcessedQty,0) AS "numRemainingQty"
			,CONCAT(TO_CHAR(coalesce(TT.numTaskTimeInMinutes,0)/60,'00'),':',TO_CHAR(MOD(coalesce(TT.numTaskTimeInMinutes,0),60),'00')) AS "vcTimeRequired"
			,(CASE
      WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = TT.numTaskID) > 0
      THEN FormatedDateFromDate(TT.dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) || ' ' || SUBSTR(TO_CHAR(TT.dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'hh24:mi:ss'),1,5) || ' ' || SUBSTR(TO_CHAR(TT.dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'Mon dd yyyy hh:miAM'),length(TO_CHAR(TT.dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'Mon dd yyyy hh:miAM')) -2+1)
      ELSE ''
      END) AS "dtStartDate"
			,(CASE
      WHEN TT.dtFinishDate IS NOT NULL
      THEN FormatedDateFromDate(TT.dtFinishDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) || ' ' || SUBSTR(TO_CHAR(TT.dtFinishDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'hh24:mi:ss'),1,5) || ' ' || SUBSTR(TO_CHAR(TT.dtFinishDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'Mon dd yyyy hh:miAM'),length(TO_CHAR(TT.dtFinishDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'Mon dd yyyy hh:miAM')) -2+1)
      ELSE ''
      END) AS "dtFinishDate"
			,(CASE
      WHEN OI.numoppitemtCode > 0
      THEN(CASE
         WHEN OI.ItemReleaseDate IS NOT NULL
         THEN FormatedDateFromDate(CAST(OI.ItemReleaseDate AS TIMESTAMP),v_numDomainID) || ' ' || SUBSTR(TO_CHAR(CAST(OI.ItemReleaseDate AS TIMESTAMP),'hh24:mi:ss'),1,
            5) || ' ' || SUBSTR(TO_CHAR(CAST(OI.ItemReleaseDate AS TIMESTAMP),'Mon dd yyyy hh:miAM'),length(TO_CHAR(CAST(OI.ItemReleaseDate AS TIMESTAMP),'Mon dd yyyy hh:miAM')) -2+1)
         ELSE ''
         END)
      ELSE(CASE
         WHEN WO.dtmEndDate IS NOT NULL
         THEN FormatedDateFromDate(WO.dtmEndDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) || ' ' || SUBSTR(TO_CHAR(WO.dtmEndDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
            'hh24:mi:ss'),1,5) || ' ' || SUBSTR(TO_CHAR(WO.dtmEndDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
            'Mon dd yyyy hh:miAM'),length(TO_CHAR(WO.dtmEndDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
            'Mon dd yyyy hh:miAM')) -2+1)
         ELSE ''
         END)
      END) AS "dtReleaseDate"
      FROM
      tt_TEMP T
      INNER JOIN
      tt_TEMPTASKS TT
      ON
      T.numTaskID = TT.numReferenceTaskId
      INNER JOIN
      AdditionalContactsInformation
      ON
      TT.numAssignTo = AdditionalContactsInformation.numContactId
      INNER JOIN
      WorkOrder WO
      ON
      TT.numWorkOrderID = WO.numWOId
      LEFT JOIN
      OpportunityItems OI
      ON
      WO.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      WO.numItemCode = I.numItemCode
      WHERE
      TT.numTaskID IN(SELECT TTL.numTaskID FROM tt_TEMPTIMELOG TTL WHERE dtDate BETWEEN v_dtFromDate AND v_dtToDate);

      open SWV_RefCur5 for
      SELECT
      numUserDetailId AS "numUserDetailId"
			,CONCAT(coalesce(vcFirstName,'-'),' ',coalesce(vcLastname,'-')) AS "vcUserName"
			,coalesce(numTeam,0) AS "numTeam"
			,CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"numTaskID":',numTaskId,',','"vcGradeId":"',vcGradeId,'"}'),',')
         
         FROM
         tblStageGradeDetails
         WHERE
         numAssigneId = UserMaster.numUserDetailId),''),']') AS "vcTaskGrades"
			,coalesce((SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS "numCapacityLoad"
         FROM
         tt_TEMPTIMELOG TTL
         WHERE
         coalesce(TTL.numProductiveMinutes,0) > 0
         AND TTL.numAssignTo = UserMaster.numUserDetailId
         AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate
         GROUP BY
         TTL.numAssignTo,TTL.numProductiveMinutes),0) AS numCapacityLoad
      FROM
      UserMaster
      INNER JOIN
      AdditionalContactsInformation
      ON
      UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
      WHERE
      UserMaster.numDomainID = v_numDomainID
      AND AdditionalContactsInformation.numDomainID = v_numDomainID
      AND bitActivateFlag = true;

      open SWV_RefCur6 for
      SELECT
      numListItemID AS "numTeam"
			,coalesce(vcData,'-') AS "vcTeamName"
			,coalesce((SELECT
         AVG(TEMP.numCapacityLoad)
         FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad
            FROM
            tt_TEMPTIMELOG TTL
            INNER JOIN
            AdditionalContactsInformation ADC
            ON
            TTL.numAssignTo = ADC.numContactId
            WHERE
            ADC.numTeam	= ListDetails.numListItemID
            AND coalesce(TTL.numProductiveMinutes,0) > 0
            AND TTL.numAssignTo = ADC.numContactId
            AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate
            GROUP BY
            TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "numCapacityLoad"
      FROM
      Listdetails
      WHERE
      numListID = 35
      AND numDomainid = v_numDomainID;

      open SWV_RefCur7 for
      SELECT
      T.numTaskID AS "numTaskID"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A+' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "APlus"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "A"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A-' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "AMinus"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B+' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "BPlus"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "B"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B-' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "BMinus"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C+' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "CPlus"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "C"
			,coalesce((SELECT AVG(numCapacityLoad) FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad FROM tt_TEMPTIMELOG TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C-' AND TTL.dtDate BETWEEN v_dtFromDate AND v_dtToDate AND coalesce(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS "CMinus"
      FROM
      tt_TEMP T;
   ELSEIF v_tintView = 2 OR v_tintView = 3
   then
      DROP TABLE IF EXISTS tt_TEMPRESULT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPRESULT
      (
         dtDate DATE,
         numTaskID NUMERIC(18,0),
         numTotalTasks NUMERIC(18,0),
         numMaxTasks NUMERIC(18,0),
         numAssigneeCapacityLoad NUMERIC(18,0),
         numGradeCapacity NUMERIC(18,0),
         numGradeCapacityLoad NUMERIC(18,0)
      );
      WHILE v_dtFromDate <= v_dtToDate LOOP
         INSERT INTO tt_TEMPRESULT(dtDate
				,numTaskID
				,numTotalTasks
				,numMaxTasks
				,numAssigneeCapacityLoad
				,numGradeCapacity
				,numGradeCapacityLoad)
         SELECT
         v_dtFromDate
				,numTaskID
				,coalesce((SELECT
            SUM(TTL.numQtyItemsReq)
            FROM
            tt_TEMPTIMELOG TTL
            WHERE
            TTL.numReferenceTaskId = T.numTaskID
            AND CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)
            GROUP BY
            TTL.numReferenceTaskId),0)
				,coalesce((SELECT
            SUM(numMaxTask)
            FROM(SELECT(CASE
               WHEN coalesce(TTL.numProductiveMinutes,0) > coalesce(SUM(TTL.numTotalMinutes),0)
               THEN((coalesce(TTL.numProductiveMinutes,0) -coalesce(SUM(TTL.numTotalMinutes),0))/coalesce(T.numTaskTimeInMinutes,0))
               ELSE coalesce(TTL.numProductiveMinutes,0)/coalesce(T.numTaskTimeInMinutes,0)
               END) AS numMaxTask
               FROM
               tt_TEMPTIMELOG TTL
               WHERE
               coalesce(TTL.numProductiveMinutes,0) > 0
               AND CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)
               GROUP BY
               TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0)
				,coalesce((SELECT
            AVG(numCapacityLoad)
            FROM(SELECT(CASE WHEN coalesce(TTL.numProductiveMinutes,0) > 0 THEN(SUM(TTL.numTotalMinutes)*100)/TTL.numProductiveMinutes ELSE 0 END) AS numCapacityLoad
               FROM
               tt_TEMPTIMELOG TTL
               WHERE
               coalesce(TTL.numProductiveMinutes,0) > 0
               AND CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)
               GROUP BY
               TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0)
				,coalesce((SELECT
            SUM(numMaxTask)
            FROM(SELECT(CASE
               WHEN(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) > coalesce(TTL.numTotalMinutes,0)
               THEN(((coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) -coalesce(TTL.numTotalMinutes,0))/coalesce(T.numTaskTimeInMinutes,0))
               ELSE(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0))/coalesce(T.numTaskTimeInMinutes,0)
               END) AS numMaxTask
               FROM
               tblStageGradeDetails TSGD
               INNER JOIN
               WorkSchedule WS
               ON
               TSGD.numAssigneId = WS.numUserCntID
               LEFT JOIN LATERAL(SELECT
                  SUM(numTotalMinutes) AS numTotalMinutes
                  FROM
                  tt_TEMPTIMELOG TTL
                  WHERE
                  CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)
                  AND TTL.numAssignTo = TSGD.numAssigneId) TTL on TRUE
               WHERE
               TSGD.numTaskID = T.numTaskID
               AND(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) > 0
               AND (coalesce(v_vcGradeIDs,'') = '' OR vcGradeId IN(SELECT OutParam FROM SplitString(v_vcGradeIDs,',')))
               GROUP BY
               TSGD.numAssigneId,WS.numProductiveHours,WS.numProductiveMinutes,TTL.numTotalMinutes) TEMP),
         0)
				,coalesce((SELECT
            AVG(numCapacityLoad)
            FROM(SELECT(CASE WHEN(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) > 0 THEN(coalesce(TTL.numTotalMinutes,0)*100)/(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) ELSE 0 END) AS numCapacityLoad
               FROM
               tblStageGradeDetails TSGD
               INNER JOIN
               WorkSchedule WS
               ON
               TSGD.numAssigneId = WS.numUserCntID
               LEFT JOIN LATERAL(SELECT
                  SUM(numTotalMinutes) AS numTotalMinutes
                  FROM
                  tt_TEMPTIMELOG TTL
                  WHERE
                  CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE)
                  AND TTL.numAssignTo = TSGD.numAssigneId) TTL on TRUE
               WHERE
               TSGD.numTaskID = T.numTaskId
               AND(coalesce(WS.numProductiveHours,0)*60+coalesce(WS.numProductiveMinutes,0)) > 0
               AND (coalesce(v_vcGradeIDs,'') = '' OR vcGradeId IN(SELECT OutParam FROM SplitString(v_vcGradeIDs,',')))
               GROUP BY
               TSGD.numAssigneId,WS.numProductiveHours,WS.numProductiveMinutes,TTL.numTotalMinutes) TEMP),0)
         FROM
         tt_TEMP T;
         v_dtFromDate := v_dtFromDate+INTERVAL '1 day';
      END LOOP;

		--SELECT * FROM @TempTimeLog ORDER BY numWrokOrder,numTaskID,dtDate
      open SWV_RefCur4 for
      SELECT dtDate AS "dtDate",
         numTaskID AS "numTaskID",
         numTotalTasks AS "numTotalTasks",
         numMaxTasks AS "numMaxTasks",
         numAssigneeCapacityLoad AS "numAssigneeCapacityLoad",
         numGradeCapacity AS "numGradeCapacity",
         numGradeCapacityLoad AS "numGradeCapacityLoad" FROM tt_TEMPRESULT;
   end if;
   RETURN;
END; $$;


