-- Stored procedure definition script USP_GetGoogleMap for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetGoogleMap(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	  v_xmlStr VARCHAR(5000) DEFAULT NULL,
	  v_miles INTEGER DEFAULT NULL,
      v_zipcode VARCHAR(20) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPCONTACTID CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCONTACTID ON COMMIT DROP AS
      select Items  from Split(v_xmlStr,',');
   DROP TABLE IF EXISTS tt_TEMPZIPCODE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPZIPCODE AS
      select * from funZips(v_zipcode,v_miles);

   open SWV_RefCur for Select  ADC.numContactId,cast(vcCompanyName as VARCHAR(255)),cast(coalesce(vcFirstName,'') as VARCHAR(50)) AS vcFirstName,cast(coalesce(vcLastname,'') as VARCHAR(50)) AS vcLastName,cast(ZC.Longitude as VARCHAR(255)),cast(ZC.Latitude as VARCHAR(255)),
coalesce(AD1.vcStreet,'')+coalesce(AD1.vcCity,'') || ' ,'
   || coalesce(fn_GetState(AD1.numState),'') || ' '
   || coalesce(AD1.vcPostalCode,'') || ' <br>'
   || coalesce(fn_GetListItemName(AD1.numCountry),'') AS vcFullAddress,cast(tZ.Distance as VARCHAR(255))
   FROM AdditionalContactsInformation ADC join tt_TEMPCONTACTID tC on tC.Items = ADC.numContactId
   JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
   left join AddressDetails AD1 on AD1.numDomainID = v_numDomainID and AD1.numRecordID = ADC.numContactId AND AD1.tintAddressOf = 1
   join ZipCodes ZC on(REPEAT('0',5 -LENGTH(ZC.ZipCode)) || cast(ZC.ZipCode as VARCHAR(10))) = AD1.vcPostalCode
   join tt_TEMPZIPCODE tZ on tZ.ZipCode = ZC.ZipCode
   where DM.numDomainID  = v_numDomainID
   Union
   Select  ADC.numContactId,cast(vcCompanyName as VARCHAR(255)),cast(coalesce(vcFirstName,'') as VARCHAR(50)) AS vcFirstName,cast(coalesce(vcLastname,'') as VARCHAR(50)) AS vcLastName,cast(ZC.Longitude as VARCHAR(255)),cast(ZC.Latitude as VARCHAR(255)),
coalesce(AD2.vcStreet,'')+coalesce(AD2.vcCity,'') || ' ,'
   || coalesce(fn_GetState(AD2.numState),'') || ' '
   || coalesce(AD2.vcPostalCode,'') || ' <br>'
   || coalesce(fn_GetListItemName(AD2.numCountry),'') AS vcFullAddress,cast(tZ.Distance as VARCHAR(255))
   FROM AdditionalContactsInformation ADC join tt_TEMPCONTACTID tC on tC.Items = ADC.numContactId
   JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
   left join AddressDetails AD2 on AD2.numDomainID = v_numDomainID and AD2.numRecordID = ADC.numDivisionId AND AD2.tintAddressOf = 2
   join ZipCodes ZC on(REPEAT('0',5 -LENGTH(ZC.ZipCode)) || cast(ZC.ZipCode as VARCHAR(10))) = AD2.vcPostalCode
   join tt_TEMPZIPCODE tZ on tZ.ZipCode = ZC.ZipCode
   where DM.numDomainID  = v_numDomainID
   order by 8;

   drop table IF EXISTS tt_TEMPCONTACTID CASCADE;
   RETURN;
END; $$;












