-- Stored procedure definition script USP_GetShippingServiceTypes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetShippingServiceTypes(v_numDomainID NUMERIC,
    v_numRuleID NUMERIC,
    v_numServiceTypeID NUMERIC DEFAULT 0,
    v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 2 then
    
      IF NOT EXISTS(SELECT * FROM
      ShippingServiceTypes
      WHERE
      numDomainID = v_numDomainID
      AND intNsoftEnum > 0
      AND coalesce(numRuleID,0) = 0) then
		
         INSERT INTO ShippingServiceTypes(numShippingCompanyID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID)
         SELECT  numShippingCompanyID,
							vcServiceName,
							intNsoftEnum,
							intFrom,
							intTo,
							fltMarkup,
							bitMarkupType,
							monRate,
							bitEnabled,
							NULL,
							v_numDomainID
         FROM    ShippingServiceTypes
         WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND coalesce(numRuleID,0) = 0;
      end if;
      open SWV_RefCur for
      SELECT DISTINCT * FROM(SELECT
         numServiceTypeID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID,
				numShippingCompanyID,
				CASE WHEN numShippingCompanyID = 91 THEN 'Fedex'
         WHEN numShippingCompanyID = 88 THEN 'UPS'
         WHEN numShippingCompanyID = 90 THEN 'USPS'
         ELSE 'Other'
         END AS vcShippingCompany
         FROM
         ShippingServiceTypes
         WHERE
         numDomainID = v_numDomainID
         AND coalesce(numRuleID,0) = 0
         AND (numServiceTypeID = v_numServiceTypeID OR v_numServiceTypeID = 0)
         AND intNsoftEnum > 0
         UNION ALL
         SELECT	101,'Amazon Standard',101,0,0,0,true,0,true,0,0,0,CAST('Amazon' AS VARCHAR(5)) AS vcShippingCompany
         UNION ALL
         SELECT	102,'ShippingMethodStandard',102,0,0,0,true,0,true,0,0,0,CAST('E-Bay' AS VARCHAR(5)) AS vcShippingCompany
         UNION ALL
         SELECT	103,'Other',103,0,0,0,true,0,true,0,0,0,CAST('E-Bay' AS VARCHAR(5)) AS vcShippingCompany) TABLE1
      WHERE
			(numServiceTypeID = v_numServiceTypeID OR v_numServiceTypeID = 0)
      ORDER BY
      vcShippingCompany;
   end if;
   IF v_tintMode = 1 then
    
      open SWV_RefCur for
      SELECT
      numServiceTypeID,
            vcServiceName,
            intNsoftEnum,
            intFrom,
            intTo,
            fltMarkup,
            bitMarkupType,
            CAST(monRate AS DECIMAL(20,2)) AS monRate,
            bitEnabled,
            numRuleID,
            numDomainID,
			(select
         distinct
         COALESCE((select string_agg(LDI.vcData,',' order by LDI.vcData)
            from Listdetails LDI
            where LDI.vcData = vcData AND LDI.numListID = 36 and LDI.numDomainid = v_numDomainID  AND numListItemID IN(SELECT Id FROM SplitIDs(vcItemClassification,','))
            ),'') as vcData
         from Listdetails AS LD WHERE LD.numListID = 36 and LD.numDomainid = v_numDomainID AND numListItemID IN(SELECT Id FROM SplitIDs(vcItemClassification,','))
         group by vcData) As vcItemClassificationName
			,vcItemClassification
      FROM
      ShippingServiceTypes
      WHERE
      numDomainID = v_numDomainID
      AND numRuleID = v_numRuleID
      AND (numServiceTypeID = v_numServiceTypeID OR v_numServiceTypeID = 0)
      AND coalesce(intNsoftEnum,0) = 0;
   end if;
   RETURN;
END; $$;


