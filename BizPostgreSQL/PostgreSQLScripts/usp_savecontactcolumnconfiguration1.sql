-- Stored procedure definition script USP_SaveContactColumnConfiguration1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveContactColumnConfiguration1(v_numDomainID NUMERIC(9,0),        
	v_numUserCntId NUMERIC(9,0),      
	v_FormId SMALLINT ,    
	v_numtype NUMERIC(9,0),    
	v_strXml   TEXT DEFAULT '',
	v_numViewID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc1  INTEGER;
BEGIN
   IF v_FormId = 96 OR  v_FormId = 97 then
	
      DELETE FROM
      DycFormConfigurationDetails
      WHERE
      numDomainID = v_numDomainID
      AND numFormID = v_FormId
      AND tintPageType = 1
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(numViewId,0) = v_numViewID;
   ELSE
      DELETE FROM
      DycFormConfigurationDetails
      WHERE
      numDomainID = v_numDomainID
      AND numFormID = v_FormId
      AND numUserCntID = v_numUserCntId
      AND tintPageType = 1
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(numViewId,0) = v_numViewID;
   end if;
    
   INSERT INTO DycFormConfigurationDetails(numFormID
		,numFieldID
		,intRowNum
		,intColumnNum
		,numUserCntID
		,numDomainID
		,bitCustom
		,tintPageType
		,numRelCntType
		,numViewId)
   SELECT
   v_FormId
		,X.numFieldID
		,X.tintOrder
		,1
		,CASE WHEN v_FormId = 96 OR  v_FormId = 97 THEN 0 ELSE v_numUserCntId END
		,v_numDomainID
		,X.bitCustom,1
		,v_numtype
		,v_numViewID
	FROM
	 XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strXml AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numFieldID NUMERIC(18,0) PATH 'numFieldID',
			tintOrder SMALLINT PATH 'tintOrder',
			bitCustom BOOLEAN PATH 'bitCustom'
	) AS X;                            

   RETURN;
END; $$;


