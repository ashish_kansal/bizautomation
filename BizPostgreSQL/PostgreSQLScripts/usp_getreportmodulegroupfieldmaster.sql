-- Stored procedure definition script USP_GetReportModuleGroupFieldMaster for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReportModuleGroupFieldMaster(v_numDomainID NUMERIC(18,0),
	v_numReportID NUMERIC(18,0),
	v_bitIsReportRun BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

--Get numReportModuleGroupID
   AS $$
   DECLARE
   v_numReportModuleGroupID  NUMERIC(18,0);
BEGIN
   v_numReportModuleGroupID := 0;
   select   numReportModuleGroupID INTO v_numReportModuleGroupID FROM ReportListMaster WHERE numDomainID = v_numDomainID AND numReportID = v_numReportID;


   IF v_numReportModuleGroupID > 0 then

      DROP TABLE IF EXISTS tt_TEMPFIELD CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFIELD
      (
         numReportFieldGroupID NUMERIC,
         vcFieldGroupName VARCHAR(100),
         numFieldID NUMERIC,
         vcFieldName VARCHAR(50),
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldDataType CHAR(1),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC,
         bitCustom BOOLEAN,
         bitAllowSorting BOOLEAN,
         bitAllowGrouping BOOLEAN,
         bitAllowAggregate BOOLEAN,
         bitAllowFiltering BOOLEAN,
         vcLookBackTableName VARCHAR(50)
      );

--Regular Fields
      INSERT INTO tt_TEMPFIELD
      SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,RGMM.numFieldId,RGMM.vcFieldName,
		   DFM.vcDbColumnName,DFM.vcOrigDbCOlumnName,RGMM.vcFieldDataType,RGMM.vcAssociatedControlType,
		   coalesce(DFM.vcListItemType,''),coalesce(DFM.numListID,0),false AS bitCustom,RGMM.bitAllowSorting,RGMM.bitAllowGrouping,
		   RGMM.bitAllowAggregate,RGMM.bitAllowFiltering,DFM.vcLookBackTableName
      FROM ReportFieldGroupMaster RFGM
      JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID = RFMM.numReportFieldGroupID
      JOIN ReportFieldGroupMappingMaster RGMM ON RGMM.numReportFieldGroupID = RFGM.numReportFieldGroupID
      JOIN DycFieldMaster DFM ON DFM.numFieldID = RGMM.numFieldId
      WHERE RFMM.numReportModuleGroupID = v_numReportModuleGroupID AND RFGM.bitActive = true
      AND coalesce(RFGM.bitCustomFieldGroup,false) = false;
      IF v_numReportModuleGroupID = 13 then

         INSERT INTO tt_TEMPFIELD
         SELECT 3 AS numReportFieldGroupID,CAST('Milestone Stages Field' AS VARCHAR(100)) AS vcFieldGroupName,0 AS numStageDetailsId,CAST('Last Milestone Completed' AS VARCHAR(50)) as vcStageName,
		   CAST('Last Milestone Completed' AS VARCHAR(50)) AS vcDbColumnName,CAST('OppMileStoneCompleted' AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   CAST('' AS VARCHAR(3)) AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('StagePercentageDetails' AS VARCHAR(50)) AS vcLookBackTableName
         UNION
         SELECT 3 AS numReportFieldGroupID,CAST('Milestone Stages Field' AS VARCHAR(100)) AS vcFieldGroupName,1 AS numStageDetailsId,CAST('Last Stage Completed' AS VARCHAR(50)) AS vcStageName,
		   CAST('Last Stage Completed' AS VARCHAR(50)) AS vcDbColumnName,CAST('OppStageCompleted' AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   CAST('' AS VARCHAR(3)) AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('StagePercentageDetails' AS VARCHAR(50)) AS vcLookBackTableName
         UNION
         SELECT 3 AS numReportFieldGroupID,CAST('Milestone Stages Field' AS VARCHAR(100)) AS vcFieldGroupName,2 AS numStageDetailsId,CAST('Total Progress' AS VARCHAR(50)) AS vcStageName,
		   CAST('Total Progress' AS VARCHAR(50)) AS vcDbColumnName,CAST('intTotalProgress' AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   CAST('' AS VARCHAR(3)) AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('ProjectProgress' AS VARCHAR(50)) AS vcLookBackTableName;
      end if;
      IF v_numReportModuleGroupID = 9 then

         INSERT INTO tt_TEMPFIELD
         SELECT 25 AS numReportFieldGroupID,CAST('BizDocs Items Tax Field' AS VARCHAR(100)) AS vcFieldGroupName,TI.numTaxItemID,TI.vcTaxName,
		   vcTaxName AS vcDbColumnName,vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   CAST('' AS VARCHAR(3)) AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('TaxItems' AS VARCHAR(50)) AS vcLookBackTableName
         from TaxItems TI
         where TI.numDomainID = v_numDomainID
         UNION
         SELECT 25 AS numReportFieldGroupID,CAST('BizDocs Items Tax Field' AS VARCHAR(100)) AS vcFieldGroupName,0 AS numTaxItemID,CAST('Sales Tax' AS VARCHAR(50)) AS vcTaxName,
		   CAST('Sales Tax' AS VARCHAR(50)) AS vcDbColumnName,CAST('Sales Tax' AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('TaxItems' AS VARCHAR(50)) AS vcLookBackTableName;
      end if;
      IF v_numReportModuleGroupID = 8 OR v_numReportModuleGroupID = 9 then

         INSERT INTO tt_TEMPFIELD
         SELECT 26 AS numReportFieldGroupID,CAST('BizDocs Tax Field' AS VARCHAR(100)) AS vcFieldGroupName,TI.numTaxItemID,CAST('Total ' || TI.vcTaxName AS VARCHAR(50)),
		   CAST('Total ' || vcTaxName AS VARCHAR(50)) AS vcDbColumnName,CAST('Total ' || vcTaxName AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   CAST('' AS VARCHAR(3)) AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('TaxItems' AS VARCHAR(50)) AS vcLookBackTableName
         from TaxItems TI
         where TI.numDomainID = v_numDomainID
         UNION
         SELECT 26 AS numReportFieldGroupID,CAST('BizDocs Tax Field' AS VARCHAR(100)) AS vcFieldGroupName,0 AS numTaxItemID,CAST(CAST('Total Sales Tax' AS VARCHAR(6)) AS VARCHAR(50)) AS vcTaxName,
		   CAST(CAST('Total Sales Tax' AS VARCHAR(6)) AS VARCHAR(50)) AS vcDbColumnName,CAST(CAST('Total Sales Tax' AS VARCHAR(6)) AS VARCHAR(50)) AS vcOrigDbColumnName,'M' AS vcFieldDataType,CAST('TextBox' AS VARCHAR(50)) AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,false AS bitCustom,false AS bitAllowSorting,false AS bitAllowGrouping,
		   true AS bitAllowAggregate,true AS bitAllowFiltering,CAST('TaxItems' AS VARCHAR(50)) AS vcLookBackTableName;
      end if;

--Custom Fields			
      INSERT INTO tt_TEMPFIELD
      SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,coalesce(CFM.Fld_id,0) as numFieldID,CFM.FLd_label as vcFieldName,
	   CAST('' AS VARCHAR(50)) as vcDbColumnName,CAST('' AS VARCHAR(50)) AS vcOrigDbColumnName,CASE CFM.fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.fld_type AS vcAssociatedControlType,CAST(CASE WHEN CFM.fld_type = 'SelectBox' OR CFM.fld_type = 'CheckBoxList' THEN 'LI' ELSE '' END AS VARCHAR(3)) AS vcListItemType,coalesce(CFM.numlistid,0) as numListID,
	   true AS bitCustom,true AS bitAllowSorting,CAST(CASE WHEN CFM.fld_type = 'SelectBox' THEN 1 ELSE 0 END AS BOOLEAN) AS bitAllowGrouping,
	   false AS bitAllowAggregate,true AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
      FROM ReportFieldGroupMaster RFGM
      JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID = RFMM.numReportFieldGroupID
      JOIN CFW_Fld_Master CFM ON CFM.Grp_id = RFGM.numGroupID
      LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp = CAST(CGM.Grp_id AS VARCHAR)
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
      WHERE RFMM.numReportModuleGroupID = v_numReportModuleGroupID AND RFGM.bitActive = true
      AND coalesce(RFGM.bitCustomFieldGroup,false) = true AND coalesce(RFGM.numGroupID,0) > 0
      AND CFM.numDomainID = v_numDomainID;
      IF coalesce(v_bitIsReportRun,false) = true AND(SELECT COUNT(*) FROM tt_TEMPFIELD WHERE vcOrigDbColumnName IN('monPriceLevel1','monPriceLevel2','monPriceLevel3','monPriceLevel4','monPriceLevel5',
      'monPriceLevel6','monPriceLevel7','monPriceLevel8','monPriceLevel9',
      'monPriceLevel10','monPriceLevel11','monPriceLevel12','monPriceLevel13',
      'monPriceLevel14','monPriceLevel15','monPriceLevel16','monPriceLevel17',
      'monPriceLevel18','monPriceLevel19','monPriceLevel20')) > 0 then
	
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 1),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 1),'') ELSE 'Price Level 1' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel1';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 2),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 2),'') ELSE 'Price Level 2' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel2';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 3),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 3),'') ELSE 'Price Level 3' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel3';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 4),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 4),'') ELSE 'Price Level 4' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel4';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 5),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 5),'') ELSE 'Price Level 5' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel5';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 6),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 6),'') ELSE 'Price Level 6' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel6';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 7),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 7),'') ELSE 'Price Level 7' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel7';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 8),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 8),'') ELSE 'Price Level 8' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel8';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 9),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 9),'') ELSE 'Price Level 9' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel9';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 10),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 10),'') ELSE 'Price Level 10' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel10';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 11),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 11),'') ELSE 'Price Level 11' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel11';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 12),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 12),'') ELSE 'Price Level 12' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel12';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 13),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 13),'') ELSE 'Price Level 13' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel13';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 14),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 14),'') ELSE 'Price Level 14' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel14';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 15),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 15),'') ELSE 'Price Level 15' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel15';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 16),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 16),'') ELSE 'Price Level 16' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel16';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 17),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 17),'') ELSE 'Price Level 17' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel17';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 18),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 18),'') ELSE 'Price Level 18' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel18';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 19),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 19),'') ELSE 'Price Level 19' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel19';
         UPDATE
         tt_TEMPFIELD
         SET
         vcFieldName =(CASE WHEN LENGTH(coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 20),'')) > 0 THEN coalesce((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID = v_numDomainID AND tintPriceLevel = 20),'') ELSE 'Price Level 20' END)
         WHERE
         vcOrigDbColumnName = 'monPriceLevel20';
      end if;
      open SWV_RefCur for SELECT * FROM tt_TEMPFIELD ORDER BY numReportFieldGroupID,vcFieldName;

   end if;
END; $$;












