DROP FUNCTION IF EXISTS USP_LoadWarehouseAttributesEcomm;

CREATE OR REPLACE FUNCTION USP_LoadWarehouseAttributesEcomm(v_numItemCode NUMERIC(18,0) DEFAULT NULL,        
v_strAtrr VARCHAR(1000) DEFAULT '',    
v_numWareHouseID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);   
       
   v_strSQL  TEXT;    
   v_strAtrrTemp  TEXT; 
   v_CharType  CHAR(1);
   v_bitMatrix  BOOLEAN;
   v_numItemGroup  NUMERIC(18,0);
   v_vcItemName  VARCHAR(500);
   v_Cnt  INTEGER;
   v_vcAttribute  VARCHAR(100);
   v_SplitOn  CHAR(1) DEFAULT ',';
   v_bitShowInStock BOOLEAN;
   v_bitShowQOnHand BOOLEAN;
   v_bitAllowBackOrder BOOLEAN;
BEGIN
	SELECT 
		numDomainID
		,charItemType
		,coalesce(bitMatrix,false)
		,coalesce(numItemGroup,0)
		,vcItemName 
	INTO 
		v_numDomainID
		,v_CharType
		,v_bitMatrix
		,v_numItemGroup
		,v_vcItemName 
	FROM
		Item 
	WHERE
		numItemCode = v_numItemCode;  

   SELECT 
		coalesce(bitShowInStock,false)
		,coalesce(bitShowQOnHand,false) 
	INTO
		v_bitShowInStock
		,v_bitShowQOnHand
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=v_numDomainID;
  
	v_strAtrrTemp := v_strAtrr;       

   IF v_strAtrr != '' then
      v_Cnt := 1;
      IF v_bitMatrix = true then
		
         v_strSQL := 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE';
      ELSE
         v_strSQL := 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE';
      end if;
      WHILE (POSITION(v_SplitOn IN v_strAtrr) > 0) LOOP
         IF v_bitMatrix = true then
			
            v_strSQL := coalesce(v_strSQL,'') ||(CASE WHEN v_Cnt = 1 THEN '' ELSE ' OR ' END) || ' fld_value=CAST(''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || ''' AS NUMERIC)';
         ELSE
            v_strSQL := coalesce(v_strSQL,'') ||(CASE WHEN v_Cnt = 1 THEN '' ELSE ' OR ' END) || ' fld_value=''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         end if;
         v_strAtrr := SUBSTR(v_strAtrr,POSITION(v_SplitOn IN v_strAtrr)+1,LENGTH(v_strAtrr));
         v_Cnt := v_Cnt::bigint+1;
      END LOOP;
      IF v_bitMatrix = true then
		
         v_strSQL := CONCAT(v_strSQL,(CASE WHEN v_Cnt = 1 THEN '' ELSE ' OR ' END),' fld_value=CAST(''',
         v_strAtrr,''' AS NUMERIC) AND coalesce(bitMatrix,false) = true AND coalesce(Item.numItemGroup,0) = ',v_numItemGroup,' AND Item.vcItemName=''', regexp_replace(v_vcItemName,'''','''''','gi'),
         '''',' GROUP BY Item.numItemCode HAVING count(*) > ',v_Cnt::bigint -1);
      ELSE
         v_strSQL := coalesce(v_strSQL,'') ||(CASE WHEN v_Cnt = 1 THEN '' ELSE ' OR ' END) || ' fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
      end if;
   end if; 


   IF v_strAtrrTemp <> '' then
	
      IF v_bitMatrix = true then

		EXECUTE 'SELECT COALESCE((SELECT bitAllowBackOrder FROM Item Where numItemCode IN (' || coalesce(v_strSQL,'') || ') LIMIT 1),false)' INTO v_bitAllowBackOrder;
		
         v_strSQL := 'SELECT
						numItemID
						,WareHouseItems.numWareHouseItemId
						,coalesce(sum(numOnHand),0) as numOnHand
						,(' || CASE
								 WHEN v_CharType <> 'S'
								 THEN '(Case 
											when ' || v_bitShowInStock::VARCHAR || '=true then (Case 
																			when ' || v_bitAllowBackOrder::VARCHAR ||'=true then ''In Stock'' 
																			else (Case 
																					when coalesce(sum(numOnHand),0) > 0 then CONCAT(''In Stock'',(Case when ' || v_bitShowQOnHand::VARCHAR || '=true then CONCAT(''('',CAST(coalesce(sum(numOnHand),0) AS VARCHAR),'')'') else '''' end))     
																					else ''<font color=red>Out Of Stock</font>'' 
																					end 
																				) 
																			end
																		)    
											else ''''
											end
										)'
         ELSE '''' END || ') as InStock
								,monWListPrice  
							FROM 
								WareHouseItems        
							JOIN 
								Warehouses 
							ON 
								Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
							WHERE 
								numItemID IN (' || coalesce(v_strSQL,'') || ' )
								AND WareHouseItems.numWareHouseID=' || coalesce(v_numWareHouseID,0)::VARCHAR || ' GROUP BY numItemID,numWareHouseItemId,numOnHand,monWListPrice
							ORDER BY
								numItemID
							LIMIT 1';
      ELSE
		EXECUTE 'SELECT COALESCE((SELECT bitAllowBackOrder FROM Item Where numItemCode=' || coalesce(v_numItemCode,0)::VARCHAR || '),false)' INTO v_bitAllowBackOrder;

         v_strSQL := 'select numItemID, WareHouseItems.numWareHouseItemId,coalesce(sum(numOnHand),0) as numOnHand,(' || case when v_CharType <> 'S' then
            '(Case when ' || v_bitShowInStock::VARCHAR || '=true then ( Case when '|| v_bitAllowBackOrder::VARCHAR ||'=true then ''In Stock'' else     
						 (Case when coalesce(sum(numOnHand),0)>0 then CONCAT(''In Stock'',(Case when ' || v_bitShowQOnHand::VARCHAR || '=true then CONCAT(''('',CAST(coalesce(sum(numOnHand),0) AS VARCHAR),'')'') else '''' end))     
						 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end || ') as InStock,monWListPrice  from WareHouseItems        
						   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						   where numItemID=' || coalesce(v_numItemCode,0)::VARCHAR || ' and WareHouseItems.numWareHouseID=' || coalesce(v_numWareHouseID,0)::VARCHAR || ' and numWareHouseItemId in (' || coalesce(v_strSQL,'') || ') group by numItemID,numWareHouseItemId,numOnHand,monWListPrice';
      end if;
   ELSEIF v_strAtrrTemp = ''
   then
	
      IF v_bitMatrix = true then
			EXECUTE 'SELECT COALESCE((SELECT bitAllowBackOrder FROM Item Where numItemCode IN (' || coalesce(v_strSQL,'') || ') LIMIT 1),false)' INTO v_bitAllowBackOrder;

         v_strSQL := 'select numItemID, WareHouseItems.numWareHouseItemId,coalesce(sum(numOnHand),0) as numOnHand,(' || case when v_CharType <> 'S' then
            '(Case when ' || v_bitShowInStock::VARCHAR || '=true then ( Case when :bitAllowBackOrderr=true then ''In Stock'' else     
						(Case when coalesce(sum(numOnHand),0)>0 then CONCAT(''In Stock'',(Case when ' || v_bitShowQOnHand::VARCHAR || '=true then CONCAT(''('',CAST(coalesce(sum(numOnHand),0) AS VARCHAR),'')'') else '''' end))    
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end || ') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID IN (' || coalesce(v_strSQL,'') || ' )  and WareHouseItems.numWareHouseID=' || coalesce(v_numWareHouseID,0)::VARCHAR || ' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice ORDER BY numItemID LIMIT 1';
      ELSE
			EXECUTE 'SELECT COALESCE((SELECT bitAllowBackOrder FROM Item Where numItemCode=' || coalesce(v_numItemCode,0)::VARCHAR || '),false)' INTO v_bitAllowBackOrder;

         v_strSQL := 'select numItemID, WareHouseItems.numWareHouseItemId,coalesce(sum(numOnHand),0) as numOnHand,(' || case when v_CharType <> 'S' then
            '(Case when ' || v_bitShowInStock::VARCHAR || '=true then ( Case when '|| v_bitAllowBackOrder::VARCHAR ||'=true then ''In Stock'' else     
						(Case when coalesce(sum(numOnHand),0)>0 then CONCAT(''In Stock'',(Case when ' || v_bitShowQOnHand::VARCHAR || '=true then CONCAT(''('',CAST(coalesce(sum(numOnHand),0) AS VARCHAR),'')'') else '''' end))     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end || ') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID        
						where numItemID =' || coalesce(v_numItemCode,0)::VARCHAR || '  and WareHouseItems.numWareHouseID =' || coalesce(v_numWareHouseID,0)::VARCHAR || ' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice';
      end if;
   end if; 
   RAISE NOTICE '%',v_strSQL;
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


