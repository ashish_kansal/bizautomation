-- Stored procedure definition script USP_UpdateContactImage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_UpdateContactImage(v_numDomainID NUMERIC,  
 v_numContactID NUMERIC,  
 v_vcImageName VARCHAR(200))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	  
  
   UPDATE
   AdditionalContactsInformation
   SET
   vcImageName = v_vcImageName
   WHERE
   numDomainID = v_numDomainID
   AND numContactId = v_numContactID;
   RETURN;
END; $$;  




