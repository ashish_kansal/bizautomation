-- Stored procedure definition script USP_GetCommissionCreditMemoOrRefundDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCommissionCreditMemoOrRefundDetail(v_numUserId NUMERIC(18,0) DEFAULT 0,
    v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_numDomainId NUMERIC(18,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_numComPayPeriodID NUMERIC(18,0) DEFAULT NULL,
	v_numComRuleID NUMERIC(18,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		RH.numReturnHeaderID AS "numReturnHeaderID"
		,CI.vcCompanyName AS "vcCompanyName"
		,RH.vcRMA AS "vcRMA"
		,RH.vcBizDocName AS "vcBizDocName"
		,RH.tintReturnType AS "tintReturnType"
		,RH.tintReceiveType AS "tintReceiveType"
		,RH.monBizDocAmount AS "monBizDocAmount"
		,SUM(decCommission) AS "decCommission"
   FROM
   SalesReturnCommission SRC
   INNER JOIN
   ReturnHeader RH
   ON
   SRC.numReturnHeaderID = RH.numReturnHeaderID
   INNER JOIN
   DivisionMaster DM
   ON
   RH.numDivisionId = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   SRC.numComPayPeriodID = v_numComPayPeriodID
   AND SRC.numUserCntID = v_numUserCntID
   AND (coalesce(v_numComRuleID,0) = 0 OR SRC.numComRuleID = v_numComRuleID)
   AND RH.numDomainID = v_numDomainId
   GROUP BY
   RH.numReturnHeaderID,CI.vcCompanyName,RH.vcRMA,RH.vcBizDocName,RH.tintReturnType,
   RH.tintReceiveType,RH.monBizDocAmount;
END; $$;












