-- Stored procedure definition script USP_GetCommissionTable for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCommissionTable(v_numComRuleID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numComRuleDtlID as VARCHAR(255)),
                cast(numComRuleID as VARCHAR(255)),
                cast(intFrom as VARCHAR(255)),
                cast(intTo as VARCHAR(255)),
                cast(decCommission as VARCHAR(255))
   FROM    CommissionRuleDtl
   WHERE   numComRuleID = v_numComRuleID;
END; $$;
    












