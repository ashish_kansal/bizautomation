DROP FUNCTION IF EXISTS USP_OpportunityMaster_GetItems;

CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetItems(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_OpportunityId NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT NULL,
    v_ClientTimeZoneOffset  INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);
   v_TaxPercentage  DOUBLE PRECISION;
   v_tintOppType  SMALLINT;
   v_fld_Name  VARCHAR(500);
   v_fld_ID  NUMERIC(9,0);
   v_numBillCountry  NUMERIC(9,0);
   v_numBillState  NUMERIC(9,0);
   v_strSQL  TEXT DEFAULT '';

   v_avgCost  INTEGER;
   v_tintCommitAllocation  SMALLINT;
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   IF v_OpportunityId > 0 then
      select   numDivisionId, tintopptype INTO v_DivisionID,v_tintOppType FROM
      OpportunityMaster WHERE
      numOppId = v_OpportunityId;
      BEGIN
         CREATE TEMP SEQUENCE tt_TempForm_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      drop table IF EXISTS tt_TEMPFORM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFORM
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         tintOrder SMALLINT,
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldName VARCHAR(50),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC(9,0),
         vcLookBackTableName VARCHAR(50),
         bitCustomField BOOLEAN,
         numFieldId NUMERIC,
         bitAllowSorting BOOLEAN,
         bitAllowEdit BOOLEAN,
         bitIsRequired BOOLEAN,
         bitIsEmail BOOLEAN,
         bitIsAlphaNumeric BOOLEAN,
         bitIsNumeric BOOLEAN,
         bitIsLengthValidation BOOLEAN,
         intMaxLength INTEGER,
         intMinLength INTEGER,
         bitFieldMessage BOOLEAN,
         vcFieldMessage VARCHAR(500),
         ListRelID NUMERIC(9,0),
         intColumnWidth INTEGER,
         intVisible INTEGER,
         vcFieldDataType CHAR(1),
         numUserCntID NUMERIC(18,0)
      );
      IF(SELECT
      COUNT(*)
      FROM
      View_DynamicColumns DC
      WHERE
      DC.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND
      coalesce(DC.bitSettingField,false) = true AND coalesce(DC.bitDeleted,false) = false AND coalesce(DC.bitCustom,false) = false) > 0
      OR(SELECT
      COUNT(*)
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID = v_numUserCntID AND numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0 AND coalesce(bitCustom,false) = true) > 0 then
		
         INSERT INTO
         tt_TEMPFORM(tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, intVisible, vcFieldDataType, numUserCntID)
         SELECT
         coalesce(DC.tintRow,0)+1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,coalesce(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName
				,CAST(DDF.bitCustom AS BOOLEAN)
				,DC.numFieldID
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage AS vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
         FROM
         View_DynamicDefaultColumns DDF
         LEFT JOIN
         View_DynamicColumns DC
         ON
         DC.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END)
         AND DDF.numFieldID = DC.numFieldID
         AND DC.numUserCntID = v_numUserCntID
         AND DC.numDomainID = v_numDomainId
         AND numRelCntType = 0
         AND tintPageType = 1
         WHERE
         DDF.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END)
         AND DDF.numDomainID = v_numDomainId
         AND coalesce(DDF.bitSettingField,false) = true
         AND coalesce(DDF.bitDeleted,false) = false
         AND coalesce(DDF.bitCustom,0) = 0
         UNION
         SELECT
         tintRow+1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,false as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,CAST(1 AS INTEGER)
				,''
				,0
         FROM
         View_DynamicCustomColumns
         WHERE
         numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END)
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainId
         AND tintPageType = 1
         AND numRelCntType = 0
         AND coalesce(bitCustom,false) = true;
      ELSE
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			
         select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
			
            v_IsMasterConfAvailable := true;
         end if;

			--If MasterConfiguration is available then load it otherwise load default columns
         IF v_IsMasterConfAvailable = true then
			
            INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
            SELECT(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,v_numDomainId,v_numUserCntID,0,1,0,intColumnWidth
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
            UNION
            SELECT(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,v_numDomainId,v_numUserCntID,0,1,1,intColumnWidth
            FROM
            View_DynamicCustomColumnsMasterConfig
            WHERE
            View_DynamicCustomColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
            INSERT INTO tt_TEMPFORM(tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, intVisible, vcFieldDataType, numUserCntID)
            SELECT(intRowNum+1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,v_numUserCntID
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
            UNION
            SELECT
            tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,
					 vcAssociatedControlType,'' as vcListItemType,numListID,''
					,bitCustom,numFieldId,bitAllowSorting,CAST(0 AS BOOLEAN) as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',CAST(0 AS NUMERIC(9,0))
            FROM
            View_DynamicCustomColumnsMasterConfig
            WHERE
            View_DynamicCustomColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
            ORDER BY
            tintOrder ASC;
         end if;
      end if;
      select   coalesce(numCost,0), coalesce(tintCommitAllocation,1) INTO v_avgCost,v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainId;

		--------------Make Dynamic Query--------------
      v_strSQL := coalesce(v_strSQL,'') || 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numOnOrder
									,WItems.numBackOrder
									,CAST(COALESCE(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,COALESCE(Opp.bitItemPriceApprovalRequired,false) AS bitItemPriceApprovalRequired
									,COALESCE(Opp.numSOVendorID,0) AS numVendorID
									,'
      || CONCAT('COALESCE(Opp.monTotAmount,0) - (COALESCE(Opp.numUnitHour,0) * (CASE ',v_avgCost,
      ' WHEN 3 THEN COALESCE(V.monCost,0) / fn_UOMConversion(numPurchaseUnit,I.numItemCode,',v_numDomainId,',numBaseUnit) WHEN 2 THEN (CASE WHEN COALESCE(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE COALESCE(V.monCost,0) / fn_UOMConversion(numPurchaseUnit,I.numItemCode,',v_numDomainId,
      ',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') || '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,COALESCE(I.bitAsset,false) as bitAsset
									,COALESCE(I.bitRental,false) as bitRental
									,COALESCE(I.bitSerialized,false) bitSerialized
									,COALESCE(I.bitLotNo,false) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,COALESCE(Opp.bitDiscountType,false) bitDiscountType
									,fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || ', NULL) AS UOMConversionFactor
									,COALESCE(Opp.bitWorkOrder,false) bitWorkOrder
									,COALESCE(Opp.numUnitHourReceived,0) numUnitHourReceived
									,COALESCE((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN true
											ELSE false
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN true
											ELSE false
										END) AS bitAddedFulFillmentBizDoc
									,COALESCE(numPromotionID,0) AS numPromotionID
									,COALESCE(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM UOM WHERE numUOMId = opp.numUOMId AND COALESCE(bitEnabled,false) = true)  AS  vcBaseUOMName
									,Opp.numUnitHour AS numOriginalUnitHour 
									,COALESCE(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN COALESCE(bitKitParent,false) = true AND COALESCE(bitAssembly,false) = true THEN 0 WHEN COALESCE(bitKitParent,false) = true THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType ilike ''p'' AND COALESCE(bitDropShip, false) = false AND COALESCE(bitAsset,false)=false)
												THEN 
													CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN COALESCE(bitKitParent,false) = true AND COALESCE(bitAssembly,false) = true THEN false  WHEN COALESCE(bitKitParent,false) = true THEN true ELSE false END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS numShipClass
									,COALESCE(Opp.numQtyShipped,0) numQtyShipped
									,COALESCE(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,COALESCE(OM.fltExchangeRate,0) AS fltExchangeRate
									,COALESCE(OM.numCurrencyID,0) AS numCurrencyID
									,COALESCE(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1
										THEN CAST(COALESCE(Opp.numQtyShipped,0) AS VARCHAR)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1
										THEN CAST(COALESCE(Opp.numUnitHourReceived,0) AS VARCHAR)
										ELSE '''' 
									END) AS vcShippedReceived
									,COALESCE(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,COALESCE(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,COALESCE(i.numCOGsChartAcntId,0) AS itemCoGs
									,COALESCE(Opp.numRentalIN,0) as numRentalIN
									,COALESCE(Opp.numRentalLost,0) as numRentalLost
									,COALESCE(Opp.numRentalOut,0) as numRentalOut
									,COALESCE(bitFreeShipping,false) AS IsFreeShipping
									,Opp.bitDiscountType,COALESCE(Opp.monLandedCost,0) as monLandedCost
									, COALESCE(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,COALESCE(WItems.numWarehouseID,0) AS numWarehouseID
									,COALESCE(Opp.numShipToAddressID,0) AS numShipToAddressID
									,CASE 
										WHEN COALESCE(I.bitKitParent,false)=true 
										THEN CAST(COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) 
										ELSE CAST(COALESCE((SELECT SUM(COALESCE(WIInner.numOnHand,0)) FROM WarehouseItems WIInner WHERE WIInner.numItemID=I.numItemCode AND WIInner.numWarehouseID=W.numWarehouseID),0) AS FLOAT) 
									END numOnHand
									,COALESCE(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,COALESCE(Opp.vcType,'''') ItemType
									,COALESCE(Opp.vcType,'''') vcType
									,COALESCE(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,COALESCE(bitDropShip, false) as DropShip
									,CASE COALESCE(bitDropShip, false) WHEN true THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( fn_UOMConversion(COALESCE(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, COALESCE(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( fn_UOMConversion(COALESCE(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, COALESCE(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,COALESCE(Opp.vcSKU,COALESCE(WItems.vcWHSKU,COALESCE(I.vcSKU,''''))) vcSKU
									,Opp.vcManufacturer
									,fn_GetListItemName(I.numItemClassification) numItemClassification
									,(SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup) numItemGroup
									,COALESCE(u.vcUnitName, '''') numUOMId
									,(CASE 
										WHEN COALESCE(I.bitKitParent,false)=true  
												AND (CASE 
														WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND COALESCE(IInner.bitKitParent,false)=true)
														THEN 1
														ELSE 0
													END) = 0
										THEN 
											GetKitWeightBasedOnItemSelection(I.numItemCode,Opp.vcChildKitSelectedItems)
										ELSE 
											COALESCE(I.fltWeight,0) 
									END) fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,COALESCE(I.numContainer,0) AS numContainer
									,COALESCE(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = false
										 THEN REPLACE(CAST(COALESCE(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount AS DECIMAL(20,5))::VARCHAR, '' '', '''') || '' (''
											  || CAST(TO_CHAR(Opp.fltDiscount,''FM9,999,999,999,990.0099'') AS VARCHAR(50)) || ''%)''
										 ELSE REPLACE(CAST(COALESCE(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount AS DECIMAL(20,5))::VARCHAR, '' '', '''')
									END AS DiscAmt
									,COALESCE(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT 
														COALESCE(OBI.numOppBizDocItemID, 0)
												FROM    OpportunityBizDocs OBD
														INNER JOIN OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBD.numOppID=OM.numOppID 
														AND COALESCE(OBD.bitAuthoritativeBizDocs, 0) = 1
														AND OBI.numOppItemID = Opp.numoppitemtCode
														LIMIT 1
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,COALESCE(Opp.numClassID, 0) numClassID
									,COALESCE(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,COALESCE((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN COALESCE(Opp.bitMarkupDiscount,false) = false THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,COALESCE(bitWinningBid,false) bitWinningBid, COALESCE(numParentOppItemID,0) numParentOppItemID,COALESCE(Opp.numQtyReceived,0) numQtyReceived
									,COALESCE(Opp.monAvgCost,0) monAverageCost
									,COALESCE(Opp.bitMappingRequired,false) bitMappingRequired
									,(CASE 
										WHEN OM.tintOppType = 2 AND EXISTS (SELECT Vendor.numVendorTCode FROM Vendor WHERE Vendor.numVendorID=OM.numDivisionID AND Vendor.numItemCode=Opp.numItemCode) 
										THEN CONCAT(''<a href="javascript:OpenVendorCostTable('',(SELECT Vendor.numVendorTCode FROM Vendor WHERE Vendor.numVendorID=OM.numDivisionID AND Vendor.numItemCode=Opp.numItemCode LIMIT 1),'','',OM.numDivisionID,'','',Opp.numItemCode,'')">Cost Table</a>'')
										ELSE '''' 
									END) vcCostTableLink
									,(CASE 
										WHEN OM.tintOppType = 2 AND EXISTS (SELECT Vendor.numVendorTCode FROM Vendor INNER JOIN VendorCostTable ON Vendor.numVendorTCode = VendorCostTable.numVendorTCode  WHERE Vendor.numVendorID=OM.numDivisionID AND Vendor.numItemCode=Opp.numItemCode AND VendorCostTable.numCurrencyID=OM.numCurrencyID AND Opp.numUnitHour BETWEEN COALESCE(VendorCostTable.intFromQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AND COALESCE(VendorCostTable.intToQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AND VendorCostTable.dtDynamicCostModifiedDate IS NOT NULL) 
										THEN COALESCE((SELECT CONCAT(''<br/>'',(CASE 
																	WHEN CAST(VendorCostTable.dtDynamicCostModifiedDate+CAST(-1 *' || COALESCE(v_ClientTimeZoneOffset,0) || ' || ''minute'' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
																	THEN ''<b><font color=red>Today</font></b>'' 
																	WHEN CAST(VendorCostTable.dtDynamicCostModifiedDate+CAST(-1 *' || COALESCE(v_ClientTimeZoneOffset,0) || ' || ''minute'' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL ''-1 day'' AS DATE) 
																	THEN ''<b><font color=purple>YesterDay</font></b>'' 
																	WHEN CAST(VendorCostTable.dtDynamicCostModifiedDate+CAST(-1 *' || COALESCE(v_ClientTimeZoneOffset,0) || ' || ''minute'' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL ''1 day'' AS DATE) 
																	THEN ''<b><font color=orange>Tommorow</font></b>'' 
																	ELSE FormatedDateFromDate(VendorCostTable.dtDynamicCostModifiedDate+CAST(-1 *' || COALESCE(v_ClientTimeZoneOffset,0) || ' || ''minute'' as interval),' || COALESCE(v_numDomainId,0) || ') 
																END))
															FROM Vendor 
															INNER JOIN VendorCostTable ON Vendor.numVendorTCode = VendorCostTable.numVendorTCode  
															WHERE 
																Vendor.numVendorID=OM.numDivisionID 
																AND Vendor.numItemCode=Opp.numItemCode 
																AND VendorCostTable.numCurrencyID=OM.numCurrencyID 
																AND Opp.numUnitHour BETWEEN COALESCE(VendorCostTable.intFromQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AND COALESCE(VendorCostTable.intToQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1)) ,'''')
										ELSE '''' 
									END) vcDynamicCostLastUpdated,
									(CASE 
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 
										THEN COALESCE((SELECT STRING_AGG(CONCAT(''<a onclick="return OpenBizInvoice('',Opp.numOppId,'','',OB.numOppBizDocsId,'',0)" href="#">'',OB.vcBizDocID,'' ('',COALESCE(OBI.numUnitHour,0),'')</a>''),'', '') FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId=Opp.numOppId AND OB.numBizDocId=numBizDocId AND OBI.numOppItemID=Opp.numOppItemtCode),'''') 
										ELSE ''''
									END) AS vcQtyAddedToBills
									,';
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'vcWorkOrderStatus' AND coalesce(numUserCntID,0) = v_numUserCntID) then
			   
         v_strSQL := coalesce(v_strSQL,'') || CONCAT('GetWorkOrderStatus(om.numDomainID,',v_tintCommitAllocation,'::SMALLINT,Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,');
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'vcBackordered' AND coalesce(numUserCntID,0) = v_numUserCntID) then
			   
         v_strSQL := coalesce(v_strSQL,'') || '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND COALESCE(bitDropShip, false)=false AND COALESCE(bitAsset,false)=false)
														THEN 
															CAST(CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN COALESCE(bitKitParent,false) = true AND COALESCE(bitAssembly,false) = true THEN false  WHEN COALESCE(bitKitParent,false) = true THEN true ELSE false END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, ';
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'vcItemReleaseDate' AND coalesce(numUserCntID,0) = v_numUserCntID) then
			   
         v_strSQL := coalesce(v_strSQL,'') || '(CASE WHEN LOWER(I.charItemType) = ''p'' AND COALESCE(bitDropShip, false) = false THEN COALESCE(FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,';
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'SerialLotNo' AND coalesce(numUserCntID,0) = v_numUserCntID) then
			   
         v_strSQL := coalesce(v_strSQL,'') || 'COALESCE((SELECT  
													string_agg(vcSerialNo || CASE WHEN COALESCE(I.bitLotNo, false) = true THEN '' ('' || CAST(oppI.numQty AS VARCHAR) || '')'' ELSE ''''END,'', '' ORDER BY vcSerialNo)
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode),'''') AS vcSerialLotNo,';
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'vcInclusionDetails' AND coalesce(numUserCntID,0) = v_numUserCntID) then
			   
         v_strSQL := coalesce(v_strSQL,'') || CONCAT('GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',v_tintCommitAllocation,'::SMALLINT,false) AS vcInclusionDetails,');
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'OppItemShipToAddress') then
			   
         v_strSQL := coalesce(v_strSQL,'') ||  '(CASE WHEN Opp.numShipToAddressID > 0 THEN COALESCE((AD.vcStreet),'''') || COALESCE(AD.vcCity,'''')  || '' <br>''
                            || COALESCE(fn_GetState(AD.numState),'''') 
                            || COALESCE(AD.vcPostalCode,'''')  || '' <br>'' 
                            || COALESCE(fn_GetListItemName(AD.numCountry),'''')
							ELSE '''' END ) AS OppItemShipToAddress,';
      end if;   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
      select   vcFieldName, numFieldId INTO v_fld_Name,v_fld_ID FROM tt_TEMPFORM WHERE coalesce(bitCustomField,false) = true   ORDER BY numFieldId LIMIT 1;
      WHILE v_fld_ID > 0 LOOP
         v_strSQL := coalesce(v_strSQL,'') || CONCAT('GetCustFldValueOppItems(',v_fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as "',
         v_fld_Name,'",');
         select   vcFieldName, numFieldId INTO v_fld_Name,v_fld_ID FROM tt_TEMPFORM WHERE coalesce(bitCustomField,false) = true AND numFieldId > v_fld_ID   ORDER BY numFieldId LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_fld_ID := 0;
         end if;
      END LOOP;
      v_strSQL := SUBSTR(v_strSQL,1,LENGTH(v_strSQL) -1) || '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || '
				AND Opp.numOppId = ' || SUBSTR(CAST(v_OpportunityId AS VARCHAR(15)),1,15) || '
		ORDER BY opp.numSortOrder,numoppitemtCode ASC ';
  
	  RAISE NOTICE '%', v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;

      open SWV_RefCur2 for
      SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWareHouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				fn_GetAttributes(W.numWareHouseItmsDTLID,1::BOOLEAN) AS Attributes
      FROM    WareHouseItmsDTL W
      JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
      WHERE   numOppID = v_OpportunityId;

      open SWV_RefCur3 for
      select * from tt_TEMPFORM order by tintOrder;
   end if;
   RETURN;
END; $$;


