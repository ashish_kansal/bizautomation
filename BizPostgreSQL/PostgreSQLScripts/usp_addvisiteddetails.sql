-- Stored procedure definition script USP_AddVisitedDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddVisitedDetails(v_numRecordID NUMERIC(9,0) DEFAULT 0,        
v_chrRecordType CHAR(1) DEFAULT '',        
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCheckRec  NUMERIC(9,0);
BEGIN
   IF v_numUserCntID = 0 then
      RETURN;
   end if;
 --Commented by chintan as it creates junk data which not being used any where else. so its better to delete row insted seting bitdeleted=1
--update    RecentItems set bitdeleted = 1 where numRecordID=@numRecordID  and chrRecordType= @chrRecordType
   DELETE FROM RecentItems WHERE numRecordID = v_numRecordID  and chrRecordType = v_chrRecordType AND numUserCntID = v_numUserCntID;

        
        
--select top 1 @numCheckRec=numRecordID  from RecentItems        
--where numUserCntID=@numUserCntID  order by numRecentItemsID desc         
--if @@rowcount=0 set @numCheckRec=0   
--begin       
-- if @numCheckRec<>@numRecordID        
-- begin          
   insert into RecentItems(numRecordID,chrRecordType,bintVisitedDate,numUserCntID)
  values(v_numRecordID,v_chrRecordType,TIMEZONE('UTC',now()),v_numUserCntID);
END; $$;        
-- end        
--end


