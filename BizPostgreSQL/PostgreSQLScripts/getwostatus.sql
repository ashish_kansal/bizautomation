-- Function definition script GetWOStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWOStatus(v_numDomainID NUMERIC(18,0)
	,v_vcWorkOrderIds VARCHAR(1000))
RETURNS TABLE
(
   ID INTEGER,
   numWOID NUMERIC(18,0),
   numItemCode NUMERIC(18,0),
   numQtyToBuild DOUBLE PRECISION,
   numWarehouseID NUMERIC(18,0),
   numWorkOrderStatus SMALLINT,
   vcWorkOrderStatus VARCHAR(300)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numWOID  NUMERIC(18,0);
   v_numQtyToBuild  DOUBLE PRECISION;
   v_numQtyCanBeBuild  DOUBLE PRECISION;
   v_numQtyBuildable  DOUBLE PRECISION;
   v_numWarehouseID  NUMERIC(18,0);
   v_vcWorkOrderStatus  VARCHAR(100);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_GetWOStatus_TEMPWorkOrder_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_GETWOSTATUS_TEMPWORKORDER;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETWOSTATUS_TEMPWORKORDER
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numQtyToBuild DOUBLE PRECISION,
      numWarehouseID NUMERIC(18,0),
      numWorkOrderStatus SMALLINT,
      vcWorkOrderStatus VARCHAR(300)
   );
   INSERT INTO tt_GETWOSTATUS_TEMPWORKORDER(numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus)
   SELECT
   WorkOrder.numWOId
		,WorkOrder.numItemCode
		,coalesce(WorkOrder.numQtyItemsReq,0)
		,WareHouseItems.numWareHouseID
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 0 ELSE NULL END)
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN '<span style="color:#00b050">Build Completed</span>' ELSE NULL END)
   FROM
   WorkOrder
   INNER JOIN
   WareHouseItems
   ON
   WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   OpportunityItems
   ON
   WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   ORDER BY
   coalesce(WorkOrder.dtmEndDate,OpportunityItems.ItemReleaseDate) ASC,WorkOrder.dtmStartDate ASC,
   WorkOrder.bintCreatedDate ASC,coalesce(WorkOrder.numParentWOID,0) DESC;

   DROP TABLE IF EXISTS tt_WORKORDERITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_WORKORDERITEMS
   (
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numAvailable DOUBLE PRECISION
   );

   INSERT INTO tt_WORKORDERITEMS(numItemCode
		,numWarehouseID
		,numAvailable)
   SELECT
   TEMP.numItemCode
		,WI.numWareHouseID
		,SUM(WI.numOnHand)+SUM(WI.numAllocation)
   FROM(SELECT
      I.numItemCode
			,TWO.numWarehouseID
      FROM
      tt_GETWOSTATUS_TEMPWORKORDER TWO
      INNER JOIN
      WorkOrderDetails WOD
      ON
      TWO.numWOID = WOD.numWOId
      INNER JOIN
      Item I
      ON
      WOD.numChildItemID = I.numItemCode
      AND coalesce(WOD.numQtyItemsReq_Orig,0) > 0
      GROUP BY
      I.numItemCode,TWO.numWarehouseID) TEMP
   INNER JOIN
   WareHouseItems WI
   ON
   TEMP.numItemCode = WI.numItemID
   AND TEMP.numWarehouseID = WI.numWareHouseID
   GROUP BY
   TEMP.numItemCode,WI.numWareHouseID;
		

   select   COUNT(*) INTO v_iCount FROM tt_GETWOSTATUS_TEMPWORKORDER;

   WHILE v_i <= v_iCount LOOP
      select   T.numWOID, T.numQtyToBuild, T.numWarehouseID, T.vcWorkOrderStatus INTO v_numWOID,v_numQtyToBuild,v_numWarehouseID,v_vcWorkOrderStatus FROM
      tt_GETWOSTATUS_TEMPWORKORDER T WHERE
      T.ID = v_i;
      IF v_vcWorkOrderStatus IS NULL then
		
         v_numQtyCanBeBuild := coalesce((SELECT
         MIN(numQtyCanBeBuild)
         FROM(SELECT
            I.numItemCode
												,WOD.numQtyItemsReq
												,WOD.numQtyItemsReq_Orig
												,WOI.numAvailable
												,(CASE
            WHEN coalesce(WOI.numAvailable,0) >=(coalesce(WOD.numQtyItemsReq,0) -coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId = WOD.numWODetailId),0))
            THEN v_numQtyToBuild -FLOOR(coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId = WOD.numWODetailId),0)/WOD.numQtyItemsReq_Orig:: NUMERIC)
            WHEN coalesce(WOI.numAvailable,0) > 0
            THEN FLOOR(coalesce(WOI.numAvailable,0)/WOD.numQtyItemsReq_Orig)
            ELSE 0
            END) AS numQtyCanBeBuild
            FROM
            WorkOrderDetails WOD
            INNER JOIN
            Item I
            ON
            WOD.numChildItemID = I.numItemCode
            INNER JOIN
            tt_WORKORDERITEMS WOI
            ON
            I.numItemCode = WOI.numItemCode
            AND WOI.numWarehouseID = v_numWarehouseID
            WHERE
            WOD.numWOId = v_numWOID
            AND coalesce(WOD.numWarehouseItemID,0) > 0
            AND coalesce(WOD.numQtyItemsReq_Orig,0) > 0) TEMP1),0);
         v_numQtyBuildable := coalesce((SELECT
         MIN(numQtyBuildable)
         FROM(SELECT
            FLOOR(SUM(WOPI.numPickedQty)/WOD.numQtyItemsReq_Orig:: NUMERIC) AS numQtyBuildable
            FROM
            WorkOrderDetails WOD
            INNER JOIN
            WorkOrderPickedItems WOPI
            ON
            WOD.numWODetailId = WOPI.numWODetailId
            WHERE
            WOD.numWOId = v_numWOID
            AND coalesce(WOD.numWarehouseItemID,0) > 0
            AND coalesce(WOD.numQtyItemsReq_Orig,0) > 0
            GROUP BY
            WOD.numWODetailId,WOD.numQtyItemsReq_Orig) TEMP1),0);
         UPDATE
         tt_GETWOSTATUS_TEMPWORKORDER
         SET
         numWorkOrderStatus =(CASE
         WHEN v_numQtyBuildable = v_numQtyToBuild
         THEN 3
         WHEN v_numQtyCanBeBuild >= v_numQtyToBuild
         THEN 1
         WHEN v_numQtyCanBeBuild > 0
         THEN 2
         ELSE 0
         END),vcWorkOrderStatus =(CASE
         WHEN v_numQtyBuildable = v_numQtyToBuild
         THEN '<span style="color:#00b050">Fully Buildable</span>'
         WHEN v_numQtyCanBeBuild >= v_numQtyToBuild AND v_numQtyBuildable = 0
         THEN '<span style="color:#7030a0">Fully Pickable</span>'
         ELSE
            CONCAT((CASE WHEN v_numQtyCanBeBuild > 0 THEN CONCAT('<span style="color:#7030a0">Pickable(',v_numQtyCanBeBuild,')</span>')  ELSE '' END),(CASE WHEN v_numQtyBuildable > 0 THEN CONCAT(' <span style="color:#00b050">Buildable(',v_numQtyBuildable,')</span>') ELSE '' END),
            (CASE WHEN(v_numQtyToBuild -coalesce((SELECT
            MIN(numPickedQty)
            FROM(SELECT
               WODInner.numWODetailId
																						,FLOOR(SUM(WOPI.numPickedQty)/WODInner.numQtyItemsReq_Orig:: NUMERIC) AS numPickedQty
               FROM
               WorkOrderDetails WODInner
               LEFT JOIN
               WorkOrderPickedItems WOPI
               ON
               WODInner.numWODetailId = WOPI.numWODetailId
               WHERE
               WODInner.numWOId = v_numWOID
               AND coalesce(WODInner.numWarehouseItemID,0) > 0
               AND coalesce(WODInner.numQtyItemsReq_Orig,0) > 0
               GROUP BY
               WODInner.numWODetailId,WODInner.numQtyItemsReq_Orig) TEMP),0) -v_numQtyCanBeBuild) > 0 THEN CONCAT(' <span style="color:#ff0000">BO(',v_numQtyToBuild -v_numQtyCanBeBuild,
               ')</span>') ELSE '' END))
         END)
         WHERE
         tt_GETWOSTATUS_TEMPWORKORDER.numWOID = v_numWOID;

         UPDATE
         tt_WORKORDERITEMS WOI
         SET
         numAvailable = coalesce(WOI.numAvailable,0) -coalesce(WOD.numQtyItemsReq,0)
         FROM
         WorkOrderDetails WOD
		 INNER JOIN
				Item I
			ON
				WOD.numChildItemID = I.numItemCode
         WHERE (I.numItemCode = WOI.numItemCode
         AND WOI.numWarehouseID = v_numWarehouseID
         AND coalesce(WOD.numQtyItemsReq_Orig,0) > 0) AND WOD.numWOId = v_numWOID;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   RETURN QUERY (SELECT * FROM tt_GETWOSTATUS_TEMPWORKORDER);
END; $$;

