-- Function definition script get_month_start for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION get_month_start(v_date TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_date,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_date,'1900-01-01':: DATE))) || 'month' as interval);
END; $$;

