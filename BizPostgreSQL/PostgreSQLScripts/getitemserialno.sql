-- Function definition script GetItemSerialNo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemSerialNo(v_numItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSerial  TEXT;
BEGIN
   select string_agg(WareHouseItmsDTL.vcSerialNo,', ' ORDER BY WareHouseItems.numWareHouseItemID,WareHouseItmsDTL.numWareHouseItmsDTLID) INTO v_vcSerial FROM
   Item
   LEFT JOIN
   WareHouseItems
   ON
   Item.numItemCode = WareHouseItems.numItemID
   LEFT JOIN
   WareHouseItmsDTL
   ON
   WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE
   numItemCode = v_numItemCode
   AND WareHouseItmsDTL.numQty > 0
   AND coalesce(WareHouseItems.numWareHouseItemID,0) = 0 OR  coalesce(WareHouseItems.numWareHouseItemID,0) = v_numWarehouseItemID   ;
  
   RETURN coalesce(v_vcSerial,'');
END; $$;

