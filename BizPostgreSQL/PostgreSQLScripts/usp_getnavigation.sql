-- Stored procedure definition script USP_GetNavigation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetNavigation(v_numDomainID NUMERIC(18,0),
	v_numGroupID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql

	/********  MAIN MENU  ********/

   AS $$
   DECLARE
   v_numTabID  NUMERIC(18,0);
   v_numModuleID  NUMERIC(18,0);
   v_numTempID NUMERIC(18,0);
BEGIN
   open SWV_RefCur for
   SELECT
   T.numTabId,
		coalesce((SELECT  numTabName FROM TabDefault WHERE numDomainID = v_numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType LIMIT 1),
   T.numTabName) AS vcTabName,
		CASE
   WHEN(SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainId = v_numDomainID AND numGroupId = v_numGroupID AND numTabId = T.numTabId AND coalesce(bitInitialPage,false) = true) > 0
   THEN
      REPLACE(coalesce((SELECT  Link FROM ShortCutBar WHERE (numDomainID = v_numDomainID OR bitdefault = true) AND numTabId = T.numTabId AND ID =(SELECT  numLinkId FROM ShortCutGrpConf WHERE numDomainId = v_numDomainID AND numGroupId = v_numGroupID AND numTabId = T.numTabId AND coalesce(bitInitialPage,false) = true LIMIT 1) LIMIT 1),''),'../','')
   ELSE
      coalesce(vcURL,'')
   END AS vcURL,
		coalesce(vcAddURL,'') AS vcAddURL,
		coalesce(bitAddIsPopUp,false) AS bitAddIsPopUp,
		numOrder
   FROM
   TabMaster T
   JOIN
   GroupTabDetails G ON G.numTabId = T.numTabId
   WHERE
		(numDomainID = v_numDomainID OR bitFixed = true)
   AND numGroupID = v_numGroupID
   AND coalesce(G.tintType,0) <> 1
   AND tintTabType = 1
   AND T.numTabId NOT IN(2,68)
   ORDER BY
   numOrder;   


	/********  SUB MENU  ********/

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER DEFAULT nextval('tt_TEMP_seq'),
      numOrigParentID NUMERIC(18,0),
      numParentID NUMERIC(18,0),
      numListItemID NUMERIC(18,0),
      numTabID NUMERIC(18,0),
      numPageNavID NUMERIC(18,0),
      vcNodeName VARCHAR(500),
      vcURL VARCHAR(1000),
      vcAddURL VARCHAR(1000),
      bitAddIsPopUp BOOLEAN,
      "order" INTEGER,
      bitVisible BOOLEAN,
      vcImageURL VARCHAR(1000)
   );

   BEGIN /******************** OPPORTUNITY/ORDER ***********************/

      v_numTabID := 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			0,
			0,
			v_numTabID,
			PND.numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') AS vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND PND.numTabID = TNA.numTabID
      WHERE
      coalesce(TNA.bitVisible,false) = true
      AND coalesce(PND.bitVisible,false) = true
      AND numModuleID = v_numModuleID
      AND TNA.numDomainID = v_numDomainID
      AND numGroupID = v_numGroupID
      AND PND.numParentID <> 0
      ORDER BY
      numPageNavID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			0,
			0,
			v_numTabID,
			0,
			CAST(coalesce(vcData,'') || 's' AS VARCHAR(500)) AS vcPageNavName,
			CAST('../Opportunity/frmOpenBizDocs.aspx?BizDocName=' || vcData || '&BizDocID=' || CAST(LD.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
			CAST('' AS VARCHAR(1000)),
			CAST(0 AS BOOLEAN),
			0,
			CAST(1 AS BOOLEAN) ,
			CAST('' AS VARCHAR(1000))
      FROM
      Listdetails LD
      JOIN
      TreeNavigationAuthorization TNA
      ON
      TNA.numTabID = 1
      AND LD.numListItemID = TNA.numPageNavID
      AND coalesce(bitVisible,false) = true
      WHERE
      numListID = 27
      AND coalesce(TNA.numDomainID,0) = v_numDomainID
      AND coalesce(TNA.numGroupID,0) = v_numGroupID
      ORDER BY
      numPageNavID;
   END;

   BEGIN /******************** Email ***************************/
      v_numTabID := 44;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,false) = true
      AND coalesce(PND.bitVisible,false) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** MARKETING ***************************/
      v_numTabID := 3;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,false) = true
      AND coalesce(PND.bitVisible,false) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** SUPPORT/CASE ***************************/

      v_numTabID := 4;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** REPORTS ***************************/

      v_numTabID := 6;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** RELATIONSHIP ***********************/

      v_numTabID := 7;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			coalesce(PND.vcPageNavName,'') AS vcNodeName,
			coalesce(vcNavURL,'') AS vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			coalesce(TreeNodeOrder.numOrder,1000) AS numOrder,
			coalesce(TNA.bitVisible,true) AS bitVisible,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      TNA.numPageNavID = PND.numPageNavID AND
      TNA.numDomainID = v_numDomainID AND
      TNA.numGroupID = v_numGroupID
      AND TNA.numTabID = v_numTabID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID = PND.numPageNavID
      WHERE
      coalesce(PND.bitVisible,false) = true
      AND coalesce(TNA.bitVisible,false) = true
      AND PND.numParentID <> 0
      AND numModuleID = v_numModuleID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			0 AS numParentID,
			Listdetails.numListItemID,
			v_numTabID,
			0 AS numPageNavID,
			Listdetails.vcData AS vcNodeName,
			CAST('../prospects/frmCompanyList.aspx?RelId=' || CAST(Listdetails.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)),
			CAST(CASE WHEN Listdetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',Listdetails.numListItemID,'&FormID=36') END AS VARCHAR(1000)),
			(CASE WHEN Listdetails.numListItemID = 93 THEN false ELSE true END),
			coalesce(TreeNodeOrder.numOrder,2000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      Listdetails
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = Listdetails.numListItemID AND
      TreeNodeOrder.numParentID = 6 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      numListID = 5 AND
			(Listdetails.numDomainid = v_numDomainID  OR constFlag = true) AND
			(coalesce(bitDelete,false) = false OR constFlag = true) AND
      Listdetails.numListItemID <> 46;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT ID FROM tt_TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,
			v_numTabID,
			0,
			L2.vcData AS vcNodeName,
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',
      numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CAST(CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,
         '&profileid=',numSecondaryListItemID,'&FormID=36') END AS VARCHAR(1000)),
			--1,
			(CASE WHEN L2.numListItemID = 37257 THEN false ELSE true END),
			coalesce(TreeNodeOrder.numOrder,3000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      FieldRelationship FR
      join
      FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
      join
      Listdetails L1 on numPrimaryListItemID = L1.numListItemID
      join
      Listdetails L2 on numSecondaryListItemID = L2.numListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      numPrimaryListItemID <> 46
      and FR.numPrimaryListID = 5
      and FR.numSecondaryListID = 21
      and FR.numDomainID = v_numDomainID
      and L2.numDomainid = v_numDomainID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			coalesce((SELECT ID FROM tt_TEMP WHERE numPageNavID = 11),0),
			L2.numListItemID,
			v_numTabID,
			11,
			L2.vcData AS vcNodeName,
			CAST('../prospects/frmProspectList.aspx?numProfile=' || CAST(L2.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,
      '&FormID=35'),
			true,
			coalesce(TreeNodeOrder.numOrder,4000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 11 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      FRD.numPrimaryListItemID = 46
      AND FR.numDomainID = v_numDomainID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT ID FROM tt_TEMP WHERE numPageNavID = 12),
			L2.numListItemID,
			v_numTabID,
			12,
			L2.vcData AS vcNodeName,
			CAST('../account/frmAccountList.aspx?numProfile=' || CAST(L2.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,
      '&FormID=36'),
			true,
			coalesce(TreeNodeOrder.numOrder,5000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 12 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      FRD.numPrimaryListItemID = 46
      AND FR.numDomainID = v_numDomainID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT ID FROM tt_TEMP WHERE numListItemID = FRD.numPrimaryListItemID AND numPageNavID = 11),
			L2.numListItemID,
			v_numTabID,
			0,
			L2.vcData AS vcNodeName,
			CAST('../account/frmProspectList.aspx?numProfile=' || CAST(FRD.numPrimaryListItemID AS VARCHAR(10)) || '&numTerritoryID=' || CAST(L2.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,
      '&FormID=35','&numTerritoryID=',L2.numListItemID),
			true,
			coalesce(TreeNodeOrder.numOrder,7000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 11 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      FRD.numPrimaryListItemID IN(SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID = v_numDomainID AND numPrimaryListItemID = 46)
      AND FR.numDomainID = v_numDomainID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT ID FROM tt_TEMP WHERE numListItemID = FRD.numPrimaryListItemID AND numPageNavID = 12),
			L2.numListItemID,
			v_numTabID,
			0,
			L2.vcData AS vcNodeName,
			CAST('../account/frmAccountList.aspx?numProfile=' || CAST(FRD.numPrimaryListItemID AS VARCHAR(10)) || '&numTerritoryID=' || CAST(L2.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,
      '&FormID=36','&numTerritoryID=',L2.numListItemID),
			true,
			coalesce(TreeNodeOrder.numOrder,8000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      FieldRelationship AS FR
      INNER JOIN
      FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
      INNER JOIN
      Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
      INNER JOIN
      Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = L2.numListItemID AND
      TreeNodeOrder.numParentID = 12 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      FRD.numPrimaryListItemID IN(SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID = v_numDomainID AND numPrimaryListItemID = 46)
      AND FR.numDomainID = v_numDomainID;
   END;

   BEGIN /******************** DOCUMENTS ***********************/
      v_numTabID := 8;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			0,
			0,
			v_numTabID,
			0,
			CAST('Regular Documents' AS VARCHAR(500)) as vcPageNavName,
			CAST('../Documents/frmRegularDocList.aspx?Category=0' AS VARCHAR(1000)),
			CAST('~/Documents/frmGenDocPopUp.aspx' AS VARCHAR(1000)),
			true,
			1,
			true,
			CAST('' AS VARCHAR(1000));

	   SELECT CURRVAL('tt_TEMP_seq') INTO v_numTempID;

      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			v_numTempID,
			Ld.numListItemID,
			v_numTabID,
			0,
			vcData AS vcPageNavName,
			CAST('../Documents/frmRegularDocList.aspx?Category=' || cast(Ld.numListItemID as VARCHAR(10)) AS VARCHAR(1000)),
			CAST(CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END AS VARCHAR(1000)),
			(CASE WHEN vcData = 'Email Template' THEN true ELSE false END),
			coalesce(intSortOrder,Ld.sintOrder) AS SortOrder,
			true,
			CAST('' AS VARCHAR(1000))
      FROM
      Listdetails Ld
      LEFT JOIN
      listorder LO
      ON
      Ld.numListItemID = LO.numListItemID
      AND LO.numDomainId = v_numDomainID
      WHERE
      Ld.numListID = 29
      AND (constFlag = true OR Ld.numDomainid = v_numDomainID);
   END;

   BEGIN /******************** ACCOUNTING **********************/
	
      v_numTabID := 45;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') AS vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true ,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND numModuleID = v_numModuleID
      AND TNA.numDomainID = v_numDomainID
      AND numGroupID = v_numGroupID
      AND numParentID <> 0
      ORDER BY
      numParentID,PND.numPageNavID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(CASE numFinancialViewID
      WHEN 26770 THEN 96 /*Profit & Loss*/
      WHEN 26769 THEN 176 /*Income & Expense*/
      WHEN 26768 THEN 98 /*Cash Flow Statement*/
      WHEN 26767 THEN 97 /*Balance Sheet*/
      WHEN 26766 THEN 81 /*A/R & A/P Aging*/
      WHEN 26766 THEN 82 /*A/R & A/P Aging*/
      ELSE 0
      END),
			numFRID,
			v_numTabID,
			0,
			vcReportName,
			CAST(CASE numFinancialViewID
      WHEN 26770 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 96 AND numModuleID = 35)  /*Profit & Loss*/
      WHEN 26769 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 176 AND numModuleID = 35) /*Income & Expense*/
      WHEN 26768 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 98 AND numModuleID = 35) /*Cash Flow Statement*/
      WHEN 26767 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 97 AND numModuleID = 35) /*Balance Sheet*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 81 AND numModuleID = 35) /*A/R & A/P Aging*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 82 AND numModuleID = 35) /*A/R & A/P Aging*/
      ELSE ''
      END ||
      CASE POSITION('?' IN(CASE numFinancialViewID
      WHEN 26770 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 96 AND numModuleID = 35)  /*Profit & Loss*/
      WHEN 26769 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 176 AND numModuleID = 35) /*Income & Expense*/
      WHEN 26768 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 98 AND numModuleID = 35) /*Cash Flow Statement*/
      WHEN 26767 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 97 AND numModuleID = 35) /*Balance Sheet*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 81 AND numModuleID = 35) /*A/R & A/P Aging*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 82 AND numModuleID = 35) /*A/R & A/P Aging*/
      ELSE ''
      END))
      WHEN 0 THEN '?FRID='
      ELSE '&FRID='
      END || CAST(numFRID AS VARCHAR(30)) AS VARCHAR(1000)),
			CAST('' AS VARCHAR(1000)),
			false,
			0,
			true,
			CAST('' AS VARCHAR(1000))
      FROM
      FinancialReport
      WHERE
      numDomainID = v_numDomainID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** ITEMS **********************/

      v_numTabID := 80;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			numParentID,
			0,
			v_numTabID,
			PND.numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,false) = true
      AND coalesce(PND.bitVisible,false) = true
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      AND numParentID <> 0
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** Activities ***************************/

      v_numTabID := 36;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** HUMAN RESOURCE ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Human Resources' AND numDomainID = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** CONTRACTS ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Contracts' AND numDomainID = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** Procurement ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Procurement' AND numDomainID = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;
   BEGIN /******************** CONTACTS ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Contacts' AND numDomainID = 1 AND tintTabType = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT coalesce(ID,0) FROM tt_TEMP WHERE numPageNavID = 13),
			Listdetails.numListItemID,
			v_numTabID,
			0,
			Listdetails.vcData AS vcNodeName,
			CAST('../contact/frmContactList.aspx?ContactType=' || CAST(Listdetails.numListItemID AS VARCHAR(10)) AS VARCHAR(1000)) AS vcNavURL,
		   CAST('~/Contact/newcontact.aspx' AS VARCHAR(1000)),
		   true,
			coalesce(TreeNodeOrder.numOrder,6000) AS numOrder,
			coalesce(TreeNodeOrder.bitVisible,true) AS bitVisible,
			CAST('' AS VARCHAR(1000))
      FROM
      Listdetails
      LEFT JOIN
      TreeNodeOrder
      ON
      TreeNodeOrder.numListItemID = Listdetails.numListItemID AND
      TreeNodeOrder.numParentID = 13 AND
      TreeNodeOrder.numTabID = v_numTabID AND
      TreeNodeOrder.numDomainID = v_numDomainID AND
      TreeNodeOrder.numGroupID = v_numGroupID AND
      TreeNodeOrder.numPageNavID IS NULL
      WHERE
      numListID = 8
      AND (constFlag = true OR Listdetails.numDomainid = v_numDomainID);
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      0,
			(SELECT coalesce(ID,0) FROM tt_TEMP WHERE numPageNavID = 13),
			101 AS numListItemID,
			v_numTabID,
			0,
			CAST('Primary Contact' AS VARCHAR(500)) AS vcNodeName,
			CAST('../Contact/frmcontactList.aspx?ContactType=101' AS VARCHAR(1000)),
		 CAST('~/Contact/newcontact.aspx' AS VARCHAR(1000)),
		   true,
			coalesce((SELECT
         numOrder
         FROM
         TreeNodeOrder
         WHERE
         TreeNodeOrder.numListItemID = 101 AND
         TreeNodeOrder.numParentID = 13 AND
         TreeNodeOrder.numTabID = v_numTabID AND
         TreeNodeOrder.numDomainID = v_numDomainID AND
         TreeNodeOrder.numGroupID = v_numGroupID AND
         TreeNodeOrder.numPageNavID IS NULL),7000) AS numOrder,
			coalesce((SELECT
         bitVisible
         FROM
         TreeNodeOrder
         WHERE
         TreeNodeOrder.numListItemID = 101 AND
         TreeNodeOrder.numParentID = 13 AND
         TreeNodeOrder.numTabID = v_numTabID AND
         TreeNodeOrder.numDomainID = v_numDomainID AND
         TreeNodeOrder.numGroupID = v_numGroupID AND
         TreeNodeOrder.numPageNavID IS NULL),true) AS bitVisible,
					CAST('' AS VARCHAR(1000));
      UPDATE
      tt_TEMP t1
      SET
      numParentID = 13
		
      WHERE
      numTabID = v_numTabID AND numParentID IS NULL;
   END;

   BEGIN /******************** Manufacturing ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Manufacturing' AND numDomainID = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;

   BEGIN /******************** Projects ***************************/

      SELECT  numTabId INTO v_numTabID FROM TabMaster WHERE numTabName = 'Projects' AND numDomainID = 1 AND tintTabType = 1     LIMIT 1;
      select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = v_numTabID    LIMIT 1;
      INSERT INTO
      tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
      SELECT
      coalesce(PND.numParentID,0),
			PND.numParentID,
			0,
			v_numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			coalesce(vcNavURL,'') as vcNavURL,
			coalesce(PND.vcAddURL,''),
			coalesce(PND.bitAddIsPopUp,false),
			0,
			true,
			coalesce(PND.vcImageURL,'')
      FROM
      PageNavigationDTL PND
      LEFT JOIN
      TreeNavigationAuthorization TNA
      ON
      PND.numPageNavID = TNA.numPageNavID
      AND TNA.numTabID = v_numTabID
      WHERE
      coalesce(TNA.bitVisible,true) = true
      AND coalesce(PND.bitVisible,true) = true
      AND PND.numParentID <> 0
      AND PND.numModuleID = v_numModuleID
      AND numGroupID = v_numGroupID
      ORDER BY
      numParentID,intSortOrder,numPageNavID;
      UPDATE
      tt_TEMP t1
      SET
      numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numPageNavID = t1.numParentID AND t2.numTabID = v_numTabID),0)
		
      WHERE
      numTabID = v_numTabID;
   END;


   WITH RECURSIVE CTE(ID) AS(SELECT
   ID AS ID
   FROM
   tt_TEMP t1
   WHERE
   coalesce(numOrigParentID,0) > 0
   AND numOrigParentID NOT IN(SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID = 0 AND numTabID = t1.numTabID)
   AND numOrigParentID NOT IN(SELECT numPageNavID FROM tt_TEMP t2 WHERE t2.numTabID = t1.numTabID)
   UNION ALL
   SELECT
   t2.ID AS ID
   FROM
   tt_TEMP t2
   JOIN
   CTE c
   ON
   t2.numParentID = c.ID) DELETE FROM tt_TEMP WHERE ID IN(SELECT ID FROM CTE);

	
	/** Admin Menu **/
   INSERT INTO
   tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
   SELECT
   numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		coalesce(vcNavURL,'') as vcNavURL,
		CAST('' AS VARCHAR(1000)),
		false,
		coalesce(PND.intSortOrder,0) AS sintOrder,
		true,
		coalesce(vcImageURL,'')
   FROM
   PageNavigationDTL PND
   JOIN
   TreeNavigationAuthorization TNA
   ON
   PND.numPageNavID = TNA.numPageNavID
   AND PND.numTabID = TNA.numTabID
   WHERE
   coalesce(TNA.bitVisible,false) = true
   AND coalesce(PND.bitVisible,false) = true
   AND PND.numModuleID = 13 --AND bitVisible=1  
   AND TNA.numDomainID = v_numDomainID
   AND TNA.numGroupID = v_numGroupID
   ORDER BY
   PND.numParentID,PND.intSortOrder,(CASE WHEN PND.numPageNavID = 79 THEN 2000 ELSE 0 END),
   numPageNavID;   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
   UPDATE
   tt_TEMP
   SET
   numParentID = 0
   WHERE
   numTabID = -1
   AND numPageNavID =(SELECT numPageNavID FROM tt_TEMP WHERE numTabID = -1 AND numParentID = 0);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMP WHERE numTabID = -1 AND coalesce(numPageNavID,0) <> 0 AND numParentID NOT IN(SELECT numPageNavID FROM tt_TEMP WHERE numTabID = -1);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMP WHERE numParentID = 0 AND numTabID = -1;

	-- UPDATE PARENT DETAIL
   UPDATE
   tt_TEMP t1
   SET
   numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numTabID = -1 AND t2.numPageNavID = t1.numParentID),0)
	
   WHERE
   t1.numTabID = -1;


	/** Advance Search **/
   INSERT INTO
   tt_TEMP(numOrigParentID, numParentID, numListItemID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", bitVisible, vcImageURL)
   SELECT
   numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		coalesce(vcNavURL,'') as vcNavURL,
		CAST('' AS VARCHAR(1000)),
		false,
		coalesce(PND.intSortOrder,0) AS sintOrder,
		true,
		coalesce(vcImageURL,'')
   FROM
   PageNavigationDTL PND
   JOIN
   TreeNavigationAuthorization TNA
   ON
   PND.numPageNavID = TNA.numPageNavID
   AND PND.numTabID = TNA.numTabID
   WHERE
   coalesce(TNA.bitVisible,false) = true
   AND coalesce(PND.bitVisible,false) = true
   AND PND.numModuleID = 9
   AND TNA.numDomainID = v_numDomainID
   AND TNA.numGroupID = v_numGroupID
   ORDER BY
   PND.numParentID,sintOrder,numPageNavID;   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
   UPDATE
   tt_TEMP
   SET
   numParentID = 0
   WHERE
   numTabID = -3
   AND numPageNavID =(SELECT numPageNavID FROM tt_TEMP WHERE numTabID = -3 AND numParentID = 0);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMP WHERE numTabID = -3 AND coalesce(numPageNavID,0) <> 0 AND numParentID NOT IN(SELECT numPageNavID FROM tt_TEMP WHERE numTabID = -3);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMP WHERE numParentID = 0 AND numTabID = -3;

	-- UPDATE PARENT DETAIL
   UPDATE
   tt_TEMP t1
   SET
   numParentID = coalesce((SELECT ID FROM tt_TEMP t2 WHERE t2.numTabID = -3 AND t2.numPageNavID = t1.numParentID),0)
	
   WHERE
   t1.numTabID = -3;

   UPDATE
   tt_TEMP t1
   SET
   bitVisible =(SELECT bitVisible FROM tt_TEMP t2 WHERE t2.ID = t1.numParentID)
	
   WHERE
   coalesce(t1.numParentID,0) > 0;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMP WHERE bitVisible = true ORDER BY numTabID,numParentID,"order",ID;
   RETURN;
END; $$;


