-- Stored procedure definition script USP_PaymentGateway_GetCardConnect for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PaymentGateway_GetCardConnect(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		PaymentGatewayDTLID.intPaymentGateWay
		,PaymentGatewayDTLID.vcFirstFldValue
		,PaymentGatewayDTLID.vcSecndFldValue
		,(CASE WHEN coalesce(PaymentGatewayDTLID.vcThirdFldValue,'') = '' THEN PaymentGateway.vcGateWayName ELSE PaymentGatewayDTLID.vcThirdFldValue END) AS vcThirdFldValue
		,PaymentGatewayDTLID.bitTest
   FROM
   PaymentGatewayDTLID
   INNER JOIN
   PaymentGateway
   ON
   PaymentGatewayDTLID.intPaymentGateWay = PaymentGateway.intPaymentGateWay
   WHERE
   numDomainID = v_numDomainID
   AND numSiteId = v_numSiteID
   AND PaymentGatewayDTLID.intPaymentGateWay IN(9,10);
END; $$;
