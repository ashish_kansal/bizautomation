CREATE OR REPLACE FUNCTION Usp_GetBizDocsInOpp(v_numOppId NUMERIC(9,0),
    v_bitAuth BOOLEAN DEFAULT false,
    v_tintMode SMALLINT DEFAULT 0,
    v_numOppBizDocID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
            
      open SWV_RefCur for
      SELECT  numOppBizDocsId,
                        vcBizDocID,
                        CASE WHEN LENGTH(vcBizDocName) > 2 THEN vcBizDocName
      ELSE vcBizDocID
      END AS vcBizDocName,
                        GetListIemName(numBizDocId) AS vcBizDoc
      FROM    OpportunityBizDocs
      WHERE   numoppid = v_numOppId
      AND bitAuthoritativeBizDocs =(case v_bitAuth when true then 1 else 0 end);
   end if;
   IF v_tintMode = 1 then
		
      open SWV_RefCur for
      SELECT		numOppBizDocsId,
                        vcBizDocID,
                        CASE WHEN LENGTH(vcBizDocName) > 2 THEN vcBizDocName
      ELSE vcBizDocID
      END || ' (' || GetListIemName(numBizDocId) || ')' AS vcBizDocName
      FROM    OpportunityBizDocs
      WHERE   numoppid = v_numOppId
      AND numOppBizDocsId <> v_numOppBizDocID;
   end if;
   RETURN;
END; $$;



