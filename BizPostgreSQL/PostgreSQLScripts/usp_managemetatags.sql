-- Stored procedure definition script USP_ManageMetaTags for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageMetaTags(v_numMetaID NUMERIC(9,0) DEFAULT 0,
	v_tintMetatagFor SMALLINT DEFAULT NULL, -- 1 for Page, 2 for Items,
	v_numReferenceID NUMERIC(9,0) DEFAULT NULL,
	v_vcMetaKeywords VARCHAR(4000) DEFAULT NULL,
	v_vcMetaDescription VARCHAR(4000) DEFAULT NULL,
	v_vcPageTitle VARCHAR(4000) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_numMetaID = 0 then
	
      IF NOT EXISTS(SELECT numMetaID FROM MetaTags WHERE numReferenceID = v_numReferenceID AND tintMetatagFor = v_tintMetatagFor) then
		
         INSERT INTO MetaTags(vcPageTitle,
	   			vcMetaKeywords,
				vcMetaDescription,
				tintMetatagFor,
				numReferenceID)
			VALUES(v_vcPageTitle,
	   			v_vcMetaKeywords,
				v_vcMetaDescription,
				v_tintMetatagFor,
				v_numReferenceID);
      ELSE
         UPDATE
         MetaTags
         SET
         vcPageTitle = v_vcPageTitle,vcMetaKeywords = v_vcMetaKeywords,vcMetaDescription = v_vcMetaDescription,
         tintMetatagFor = v_tintMetatagFor,numReferenceID = v_numReferenceID
         WHERE
         numReferenceID = v_numReferenceID
         AND tintMetatagFor = v_tintMetatagFor;
      end if;
   ELSE
      UPDATE
      MetaTags
      SET
      vcPageTitle = v_vcPageTitle,vcMetaKeywords = v_vcMetaKeywords,vcMetaDescription = v_vcMetaDescription,
      tintMetatagFor = v_tintMetatagFor,numReferenceID = v_numReferenceID
      WHERE
      numMetaID = v_numMetaID;
   end if;
   RETURN;
END; $$;


