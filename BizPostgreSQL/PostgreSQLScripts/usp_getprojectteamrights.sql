CREATE OR REPLACE FUNCTION USP_GetProjectTeamRights(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numProId NUMERIC(9,0) DEFAULT 0,
	v_tintUserType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintUserType = 1 then --Internal User

      open SWV_RefCur for
      SELECT A.numContactId,A.numContactId AS numContactId,coalesce(A.vcFirstName,'') || ' ' || coalesce(A.vcLastname,'') as vcUserName
,coalesce(TR.numRights,1) AS numRights,coalesce(TR.numProjectRole,0) AS numProjectRole,A.vcEmail AS Email
      from UserMaster UM
      join AdditionalContactsInformation A on UM.numUserDetailId = A.numContactId
      left join ProjectTeamRights TR on TR.numContactId = A.numContactId and TR.numProId = v_numProId and TR.tintUserType = 1
      where UM.numDomainID = v_numDomainID and UM.numDomainID = A.numDomainID
      AND bitActivateFlag = true;
   ELSEIF v_tintUserType = 2
   then --External User

      open SWV_RefCur for
      SELECT A.numContactId,A.numContactId AS numContactId,coalesce(A.vcFirstName,'') || ' ' || coalesce(A.vcLastname,'') as vcUserName
,coalesce(TR.numRights,0) AS numRights,coalesce(TR.numProjectRole,0) AS numProjectRole,
 coalesce(C.vcCompanyName,'-') as vcCompanyName,A.vcEmail AS Email
      from ProjectTeamRights TR join AdditionalContactsInformation A on TR.numContactId = A.numContactId
      join DivisionMaster DM on DM.numDivisionID = A.numDivisionId
      join CompanyInfo C on DM.numCompanyID = C.numCompanyId
      where A.numDomainID = v_numDomainID and TR.numProId = v_numProId and TR.tintUserType = 2;
   end if;
   RETURN;
END; $$;


