-- Stored procedure definition script USP_ItemCurrencyPrice_GetByItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemCurrencyPrice_GetByItem(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Currency.numCurrencyID
		,Currency.vcCurrencyDesc
		,Currency.chrCurrency
		,coalesce(ItemCurrencyPrice.monListPrice,0) AS monListPrice
   FROM
   Currency
   INNER JOIN
   Domain
   ON
   Currency.numDomainId = Domain.numDomainId
   LEFT JOIN
   ItemCurrencyPrice
   ON
   ItemCurrencyPrice.numDomainID = v_numDomainID
   AND ItemCurrencyPrice.numItemCode = v_numItemCode
   AND Currency.numCurrencyID = ItemCurrencyPrice.numCurrencyID
   WHERE
   Currency.numDomainId = v_numDomainID
   AND coalesce(bitEnabled,false) = true
   AND coalesce(Currency.numCurrencyID,0) <> coalesce(Domain.numCurrencyID,0)
   ORDER BY
   vcCurrencyDesc;
END; $$;












