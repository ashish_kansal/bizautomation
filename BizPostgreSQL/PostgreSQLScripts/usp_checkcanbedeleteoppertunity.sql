-- Stored procedure definition script USP_CheckCanbeDeleteOppertunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckCanbeDeleteOppertunity(v_numOppId NUMERIC(18,0),
	INOUT v_tintError SMALLINT DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_OppType  VARCHAR(2);
   v_OppStatus  SMALLINT;
   v_itemcode  NUMERIC;     
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numWLocationID  NUMERIC(18,0);                           
   v_numUnits  DOUBLE PRECISION;                                            
   v_numQtyShipped  DOUBLE PRECISION;
   v_onHand  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;                                                                                                                           
   v_numoppitemtCode  NUMERIC(9,0);
   v_Kit  BOOLEAN;                   
   v_LotSerial  BOOLEAN;                   
   v_numQtyReceived  DOUBLE PRECISION;
   v_tintShipped  SMALLINT;
   SWV_RowCount INTEGER;
BEGIN
   v_tintError := 0;          
   select   tintopptype, tintoppstatus, tintshipped, numDomainId INTO v_OppType,v_OppStatus,v_tintShipped,v_numDomainID FROM
   OpportunityMaster WHERE
   numOppId = v_numOppId;
 
   IF cast(NULLIF(v_OppType,'') as INTEGER) = 2 AND v_OppStatus = 1 then
	
      select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true AND bitAssembly = true THEN 0 WHEN bitKitParent = true THEN 1 ELSE 0 END), (CASE WHEN I.bitLotNo = true OR I.bitSerialized = true THEN 1 ELSE 0 END) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
      v_numWareHouseItemID,v_numWLocationID,v_Kit,v_LotSerial FROM
      OpportunityItems OI
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      AND numOppId = v_numOppId
      AND (bitDropShip = false OR bitDropShip is NULL) WHERE
      charitemtype = 'P'   ORDER BY
      OI.numoppitemtCode LIMIT 1;
      WHILE v_numoppitemtCode > 0 LOOP
         IF EXISTS(SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomainID AND numOppID = v_numoppitemtCode AND numOppItemID = v_numoppitemtCode) then
			
            IF(SELECT
            COUNT(*)
            FROM
            OpportunityItemsReceievedLocation OIRL
            INNER JOIN
            WareHouseItems WI
            ON
            OIRL.numWarehouseItemID = WI.numWareHouseItemID
            WHERE
            OIRL.numDomainId = v_numDomainID
            AND numOppID = v_numOppId
            AND numOppItemID = v_numoppitemtCode
            GROUP BY
            OIRL.numWarehouseItemID,WI.numOnHand
            HAVING
            WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0 then
				
               IF v_tintError = 0 then 
                  v_tintError := 1;
               end if;
            ELSE
               v_numUnits := v_numUnits -(SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomainID AND numOppID = v_numoppitemtCode AND numOppItemID = v_numoppitemtCode);
            end if;
         end if;
         IF v_tintError = 0 then
			
            select   coalesce(numOnHand,0), coalesce(numonOrder,0) INTO v_onHand,v_onOrder FROM
            WareHouseItems WHERE
            numWareHouseItemID = v_numWareHouseItemID;
            IF v_onHand < v_numQtyReceived then
				
               IF v_tintError = 0 then 
                  v_tintError := 1;
               end if;
            end if;
            IF v_onOrder <(v_numUnits -v_numQtyReceived) then
				
               if v_tintError = 0 then 
                  v_tintError := 1;
               end if;
            end if;
         end if;
         select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), numWarehouseItmsID, coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true AND bitAssembly = true THEN 0 WHEN bitKitParent = true THEN 1 ELSE 0 END), (CASE WHEN I.bitLotNo = true OR I.bitSerialized = true THEN 1 ELSE 0 END) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
         v_numWareHouseItemID,v_numWLocationID,v_Kit,v_LotSerial FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         AND numOppId = v_numOppId WHERE
         charitemtype = 'P'
         AND OI.numoppitemtCode > v_numoppitemtCode
         AND (bitDropShip = false OR bitDropShip IS NULL)   ORDER BY
         OI.numoppitemtCode LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numoppitemtCode := 0;
         end if;
      END LOOP;
   end if;
   RETURN;
END; $$;


