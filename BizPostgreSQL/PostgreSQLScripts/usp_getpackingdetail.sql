-- Stored procedure definition script USP_GetPackingDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPackingDetail(v_numDomainID NUMERIC(9,0),
	v_vcBizDocsIds TEXT,
	v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- PICK LIST
   IF v_tintMode = 1 then
	
      open SWV_RefCur for
      SELECT
      coalesce(vcItemName,'') AS vcItemName,
			coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND ItemImages.numItemCode = Item.numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') AS vcImage,
			coalesce(vcModelID,'') AS vcModelID,
			coalesce(txtItemDesc,'') AS txtItemDesc,
			coalesce(vcWareHouse,'') AS vcWareHouse,
			coalesce(vcLocation,'') AS vcLocation,
			(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
			CAST(SUM(numUnitHour) AS DOUBLE PRECISION) AS numUnitHour
      FROM
      OpportunityBizDocItems
      INNER JOIN
      WareHouseItems
      ON
      OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
      INNER JOIN
      Warehouses
      ON
      WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocID IN(SELECT * FROM SplitIDs(v_vcBizDocsIds,','))
      GROUP BY
      Item.numItemCode,vcItemName,vcModelID,txtItemDesc,vcWareHouse,vcLocation,
      (CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
      ORDER BY
      vcWareHouse ASC,vcLocation ASC,vcItemName ASC;
	-- GROUP LIST
   ELSEIF v_tintMode = 2
   then
	
      open SWV_RefCur for
      SELECT
      coalesce(vcItemName,'') AS vcItemName,
			coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND ItemImages.numItemCode = Item.numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') AS vcImage,
			coalesce(vcModelID,'') AS vcModelID,
			coalesce(txtItemDesc,'') AS txtItemDesc,
			OpportunityBizDocs.numOppBizDocsId,
			(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			CAST(numUnitHour AS DOUBLE PRECISION) AS numUnitHour
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityBizDocItems
      ON
      OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
      INNER JOIN
      WareHouseItems
      ON
      OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
      INNER JOIN
      Warehouses
      ON
      WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocID IN(SELECT * FROM SplitIDs(v_vcBizDocsIds,','))
      ORDER BY
      vcWareHouse ASC,vcLocation ASC,vcItemName ASC;
   end if;
   RETURN;
END; $$;


