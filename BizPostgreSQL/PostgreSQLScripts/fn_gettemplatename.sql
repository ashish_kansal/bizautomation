-- Function definition script fn_getTemplateName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getTemplateName(v_numGenericDocID NUMERIC,v_numDomainId NUMERIC)
RETURNS VARCHAR(300) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDocName  VARCHAR(300);
BEGIN
   v_vcDocName := '';

   select   coalesce(VcDocName,'') INTO v_vcDocName from GenericDocuments where    numGenericDocID =  v_numGenericDocID And numDomainId = v_numDomainId;  

   return v_vcDocName;
END; $$;

