-- Stored procedure definition script usp_GetSurveyContactRespondentsChoices for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyContactRespondentsChoices(v_numSurId NUMERIC,
    v_numRespondentId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(sqm.vcQuestion as VARCHAR(255)),
                fn_SurveyContactRespondentsChoices(v_numSurId,v_numRespondentId,sr.numQuestionID) AS vcAnsLabel
   FROM
   SurveyQuestionMaster sqm
   INNER JOIN
   SurveyAnsMaster sam
   ON
   sqm.numSurID = sam.numSurID
   INNER JOIN
   SurveyResponse sr
   ON
   sr.numParentSurID = sam.numSurID
   AND sr.numQuestionID = sqm.numQID
   AND sr.numQuestionID = sam.numQID
   AND sr.numAnsID = sam.numAnsID
   WHERE
   sr.numSurID = v_numSurId
   AND sr.numRespondantID = v_numRespondentId
   GROUP BY
   sr.numQuestionID,sqm.vcQuestion;
END; $$;












