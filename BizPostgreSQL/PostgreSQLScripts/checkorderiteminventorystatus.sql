-- Function definition script CheckOrderItemInventoryStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckOrderItemInventoryStatus(v_numItemCode NUMERIC(18,0) DEFAULT NULL,
	v_numQuantity DOUBLE PRECISION DEFAULT 0,
	v_numOppItemCode NUMERIC(18,0) DEFAULT NULL,
	v_numWarehouseItemID NUMERIC(18,0) DEFAULT NULL,
	v_bitKitParent BOOLEAN DEFAULT NULL)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitBackOrder  DOUBLE PRECISION DEFAULT 0;
   v_numAllocation  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION DEFAULT 0;
   v_tintCommitAllocation  SMALLINT;
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseItemID = v_numWarehouseItemID;

   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Item INNER JOIN Domain ON Item.numDomainID = Domain.numDomainId WHERE numItemCode = v_numItemCode;
   select   coalesce(numQtyShipped,0) INTO v_numQtyShipped FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemCode;

   IF v_bitKitParent = true then
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         numItemCode NUMERIC(18,0),
         numParentItemID NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         vcItemName VARCHAR(300),
         bitKitParent BOOLEAN,
         numWarehouseItmsID NUMERIC(18,0),
         numQtyItemsReq DOUBLE PRECISION,
         numQtyItemReq_Orig DOUBLE PRECISION,
         numAllocation DOUBLE PRECISION,
         numOnHand DOUBLE PRECISION,
         numPrentItemBackOrder INTEGER,
         numWOID NUMERIC(18,0)
      );
      INSERT INTO
      tt_TEMPTABLE
      SELECT
      OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig*(coalesce(OI.numUnitHour,0) -coalesce(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			coalesce(WI.numAllocation,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),
      0),
			coalesce(WI.numOnHand,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			coalesce((SELECT numWOId FROM WorkOrder WHERE numOppId = OKI.numOppId AND numOppItemID = OKI.numOppItemID AND numOppChildItemID = OKI.numOppChildItemID AND coalesce(numOppKitChildItemID,0) = 0 AND coalesce(numParentWOID,0) = 0),0)
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      OpportunityItems OI
      ON
      OKI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      WHERE
      OI.numoppitemtCode = v_numOppItemCode;
      INSERT INTO
      tt_TEMPTABLE
      SELECT
      OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig*t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			coalesce(WI.numAllocation,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),
      0),
			coalesce(WI.numOnHand,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			coalesce((SELECT numWOId FROM WorkOrder WHERE numOppId = OKCI.numOppId AND numOppItemID = OKCI.numOppItemID AND numOppChildItemID = OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND coalesce(numParentWOID,0) = 0),0)
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      tt_TEMPTABLE t1
      ON
      OKCI.numOppChildItemID = t1.numOppChildItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode;
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
      UPDATE
      tt_TEMPTABLE
      SET
      numPrentItemBackOrder =(CASE
      WHEN coalesce(numWOID,0) > 0
      THEN(CASE
         WHEN GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq) >= numQtyItemsReq
         THEN 0
         ELSE(CASE WHEN numQtyItemReq_Orig > 0 THEN(numQtyItemsReq/numQtyItemReq_Orig) -CAST(GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/numQtyItemReq_Orig AS INTEGER) ELSE 0 END)
         END)
      ELSE(CASE
         WHEN v_tintCommitAllocation = 2
         THEN(CASE
            WHEN numOnHand >= numQtyItemsReq
            THEN 0
            ELSE(CASE WHEN numQtyItemReq_Orig > 0 THEN(numQtyItemsReq/numQtyItemReq_Orig) -CAST(numOnHand/numQtyItemReq_Orig AS INTEGER) ELSE 0 END)
            END)
         ELSE(CASE
            WHEN numAllocation >= numQtyItemsReq
            THEN 0
            ELSE(CASE WHEN numQtyItemReq_Orig > 0 THEN(numQtyItemsReq/numQtyItemReq_Orig) -CAST(numAllocation/numQtyItemReq_Orig AS INTEGER) ELSE 0 END)
            END)
         END)
      END)
      WHERE
      bitKitParent = false;

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
      UPDATE
      tt_TEMPTABLE t2
      SET
      numPrentItemBackOrder =(CASE WHEN t2.numQtyItemReq_Orig > 0 THEN CEIL((SELECT MAX(numPrentItemBackOrder) FROM tt_TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode)/t2.numQtyItemReq_Orig) ELSE 0 END)
		
      WHERE
      bitKitParent = true;
      v_bitBackOrder := coalesce((SELECT MAX(numPrentItemBackOrder) FROM tt_TEMPTABLE WHERE coalesce(numParentItemID,0) = 0),0);
   ELSE
      select   coalesce(numAllocation,0) INTO v_numAllocation FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWarehouseItemID;
      v_numAllocation := coalesce(v_numAllocation,0)+coalesce((SELECT
      SUM(coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0))
      FROM
      WareHouseItems
      WHERE
      numItemID = v_numItemCode
      AND numWareHouseID = v_numWarehouseID
      AND numWareHouseItemID <> v_numWarehouseItemID),
      0);
      IF(v_numQuantity -v_numQtyShipped) > v_numAllocation then
         v_bitBackOrder :=(v_numQuantity -v_numQtyShipped) -v_numAllocation;
      ELSE
         v_bitBackOrder := 0;
      end if;
   end if;

   RETURN v_bitBackOrder;
END; $$;

