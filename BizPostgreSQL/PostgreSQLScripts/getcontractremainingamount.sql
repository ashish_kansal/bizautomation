-- Function definition script GetContractRemainingAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetContractRemainingAmount(v_numContractId NUMERIC,v_numDivisionId NUMERIC,v_numDomainId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue1  DECIMAL(10,2);
BEGIN
   select   coalesce(sum(monAmount),0) INTO v_vcRetValue1 from  timeandexpense TM where
   TM.numcontractId = v_numContractId
   and numtype = 1 and numCategory = 2
   and  TM.numDomainID = v_numDomainId;            
  
   RETURN coalesce(v_vcRetValue1,0);
END; $$;

