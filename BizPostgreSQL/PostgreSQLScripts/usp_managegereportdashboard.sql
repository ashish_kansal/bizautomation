DROP FUNCTION IF EXISTS USP_ManagegeReportDashboard;
CREATE OR REPLACE FUNCTION USP_ManagegeReportDashboard(v_numDomainID NUMERIC(18,0),
v_numDashBoardID NUMERIC(18,0),
v_numReportID NUMERIC(18,0),
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_tintReportType SMALLINT DEFAULT NULL,
v_tintChartType SMALLINT DEFAULT NULL,
v_vcHeaderText VARCHAR(100) DEFAULT '',
v_vcFooterText VARCHAR(100) DEFAULT '',
v_vcXAxis VARCHAR(50) DEFAULT NULL,
v_vcYAxis VARCHAR(50) DEFAULT NULL,
v_vcAggType VARCHAR(50) DEFAULT NULL,
v_tintReportCategory SMALLINT DEFAULT NULL,
v_numDashboardTemplateID NUMERIC(18,0) DEFAULT NULL,
v_vcTimeLine VARCHAR(50) DEFAULT NULL
,v_vcGroupBy VARCHAR(50) DEFAULT NULL
,v_vcTeritorry TEXT DEFAULT NULL
,v_vcFilterBy SMALLINT DEFAULT NULL
,v_vcFilterValue TEXT DEFAULT NULL
,v_dtFromDate DATE DEFAULT NULL
,v_dtToDate DATE DEFAULT NULL
,v_numRecordCount INTEGER DEFAULT NULL
,v_tintControlField INTEGER DEFAULT NULL
,v_vcDealAmount TEXT DEFAULT NULL
,v_tintOppType INTEGER DEFAULT NULL
,v_lngPConclAnalysis TEXT DEFAULT NULL
,v_bitTask TEXT DEFAULT NULL
,v_tintTotalProgress INTEGER DEFAULT NULL
,v_tintMinNumber INTEGER DEFAULT NULL
,v_tintMaxNumber INTEGER DEFAULT NULL
,v_tintQtyToDisplay INTEGER DEFAULT NULL
,v_vcDueDate TEXT DEFAULT NULL
,v_bitDealWon BOOLEAN DEFAULT NULL
,v_bitGrossRevenue BOOLEAN DEFAULT NULL
,v_bitRevenue BOOLEAN DEFAULT NULL
,v_bitGrossProfit BOOLEAN DEFAULT NULL
,v_bitCustomTimeline BOOLEAN DEFAULT NULL
,v_bitBilledButNotReceived BOOLEAN DEFAULT NULL 
,v_bitReceviedButNotBilled BOOLEAN DEFAULT NULL
,v_bitSoldButNotShipped BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintRow  SMALLINT;
BEGIN
   IF v_numDashBoardID = 0 then
      v_tintRow := 0;
      select   coalesce(MAX(tintRow),0)+1 INTO v_tintRow FROM ReportDashboard WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numDashboardTemplateID,0) = coalesce(v_numDashboardTemplateID,0);
      INSERT INTO ReportDashboard(numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,dtFromDate
			,dtToDate
			,numRecordCount
			,tintControlField
			,vcDealAmount
			,tintOppType
			,lngPConclAnalysis
			,bitTask
			,tintTotalProgress
			,tintMinNumber
			,tintMaxNumber
			,tintQtyToDisplay
			,vcDueDate
			,bitDealWon
			,bitGrossRevenue
			,bitRevenue
			,bitGrossProfit
			,bitCustomTimeline
			,bitBilledButNotReceived
			,bitReceviedButNotBilled
			,bitSoldButNotShipped)
		VALUES(v_numDomainID
			,v_numReportID
			,v_numUserCntID
			,v_tintReportType
			,v_tintChartType
			,v_vcHeaderText
			,v_vcFooterText
			,v_tintRow
			,1
			,v_vcXAxis
			,v_vcYAxis
			,v_vcAggType
			,v_tintReportCategory
			,7
			,8
			,v_numDashboardTemplateID
			,true
			,v_vcTimeLine
			,v_vcGroupBy
			,v_vcTeritorry
			,v_vcFilterBy
			,v_vcFilterValue
			,v_dtFromDate
			,v_dtToDate
			,v_numRecordCount
			,v_tintControlField
			,v_vcDealAmount
			,v_tintOppType
			,v_lngPConclAnalysis
			,v_bitTask
			,v_tintTotalProgress
			,v_tintMinNumber
			,v_tintMaxNumber
			,v_tintQtyToDisplay
			,v_vcDueDate
			,v_bitDealWon
			,v_bitGrossRevenue
			,v_bitRevenue
			,v_bitGrossProfit
			,v_bitCustomTimeline
			,v_bitBilledButNotReceived
			,v_bitReceviedButNotBilled
			,v_bitSoldButNotShipped);
   ELSEIF v_numDashBoardID > 0
   then
	
      UPDATE
      ReportDashboard
      SET
      numReportID = v_numReportID,tintReportType = v_tintReportType,tintChartType = v_tintChartType,
      vcHeaderText = v_vcHeaderText,vcFooterText = v_vcFooterText,
      vcXAxis = v_vcXAxis,vcYAxis = v_vcYAxis,vcAggType = v_vcAggType,tintReportCategory = v_tintReportCategory,
      vcTimeLine = v_vcTimeLine,vcGroupBy = v_vcGroupBy,
      vcTeritorry = v_vcTeritorry,vcFilterBy = v_vcFilterBy,vcFilterValue = v_vcFilterValue,
      dtFromDate = v_dtFromDate,dtToDate = v_dtToDate,numRecordCount = v_numRecordCount,
      tintControlField = v_tintControlField,vcDealAmount = v_vcDealAmount,
      tintOppType = v_tintOppType,lngPConclAnalysis = v_lngPConclAnalysis,
      bitTask = v_bitTask,tintTotalProgress = v_tintTotalProgress,
      tintMinNumber = v_tintMinNumber,tintMaxNumber = v_tintMaxNumber,tintQtyToDisplay = v_tintQtyToDisplay,
      vcDueDate = v_vcDueDate,bitDealWon = v_bitDealWon,
      bitGrossRevenue = v_bitGrossRevenue,bitRevenue = v_bitRevenue,bitGrossProfit = v_bitGrossProfit,
      bitCustomTimeline = v_bitCustomTimeline,
      bitBilledButNotReceived = v_bitBilledButNotReceived,bitReceviedButNotBilled = v_bitReceviedButNotBilled,
      bitSoldButNotShipped = v_bitSoldButNotShipped
      WHERE
      numDashBoardID = v_numDashBoardID
      AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;	


