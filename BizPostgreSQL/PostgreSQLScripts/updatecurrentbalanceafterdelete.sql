CREATE OR REPLACE FUNCTION UpdateCurrentBalanceAfterDelete_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
	v_OnumChartAccountId  NUMERIC(18,0);
	v_ODebitAmount  DECIMAL(20,5);
	v_OCreditAmount  DECIMAL(20,5);
	v_numDomainId  NUMERIC(18,0); 
	BizDoc_Cursor CURSOR FOR SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from old_table;
BEGIN
    v_ODebitAmount := 0;
    v_OCreditAmount := 0;

    OPEN BizDoc_Cursor;
    FETCH NEXT FROM BizDoc_Cursor INTO v_OnumChartAccountId,v_ODebitAmount,v_OCreditAmount,v_numDomainId;

    WHILE FOUND LOOP
         UPDATE 	Chart_Of_Accounts SET numOpeningBal = coalesce(numOpeningBal,0) -v_ODebitAmount+v_OCreditAmount
         where numAccountId = v_OnumChartAccountId
         AND numDomainId = v_numDomainId;
         FETCH NEXT FROM BizDoc_Cursor
         into v_OnumChartAccountId,v_ODebitAmount,v_OCreditAmount,v_numDomainId;
    END LOOP;
    
	CLOSE BizDoc_Cursor;

	RETURN NULL;
END; $$;
CREATE TRIGGER UpdateCurrentBalanceAfterDelete AFTER DELETE ON General_Journal_Details REFERENCING OLD TABLE AS old_table FOR EACH ROW EXECUTE PROCEDURE UpdateCurrentBalanceAfterDelete_TrFunc();


