-- Stored procedure definition script usp_LeadDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_LeadDetails(v_numContactID NUMERIC(9,0),  
 v_numDomainID NUMERIC(9,0),  
v_ClientTimeZoneOffset  INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(cmp.vcCompanyName as VARCHAR(255)),
  DM.vcDivisionName,
  ADC.vcFirstName,
  ADC.vcLastname,
  cmp.vcWebSite,
  ADC.vcEmail,                                                                                   
  DM.numFollowUpStatus,
  DM.numTerID,                                                                                            
  ADC.vcPosition,
  ADC.numPhone,
  ADC.numPhoneExtension,
  cmp.numNoOfEmployeesId,
  cmp.numAnnualRevID,
  cmp.vcHow,
  cmp.vcProfile,
  ADC.txtNotes,
  DM.numCreatedBy,
  DM.numGrpId ,
  cmp.txtComments,
  cmp.numCompanyType,
 ADC.vcTitle,
  DM.vcComPhone,
  DM.vcComFax,
  coalesce(fn_GetContactName(DM.numCreatedBy),'&nbsp;&nbsp;-') || ' ' || CAST(DM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS vcCreatedBy,
  DM.numModifiedBy,
  coalesce(fn_GetContactName(DM.numModifiedBy),'&nbsp;&nbsp;-') || ' ' || CAST(DM.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS vcModifiedBy,
  DM.numRecOwner,
  cast(coalesce(fn_GetContactName(DM.numRecOwner),'&nbsp;&nbsp;-') as VARCHAR(100)) AS vcRecordOwner,
  DM.numAssignedBy,
  DM.numAssignedTo,
  DM.numCampaignID   ,
    getCompanyAddress(DM.numDivisionID,1::SMALLINT,DM.numDomainID) as address,DM.vcPartnerCode AS vcPartnerCode   ,
	cast(coalesce(DM.numPartenerSource,0) as NUMERIC(18,0)) AS numPartenerSourceId,
 D3.vcPartnerCode || '-' || C3.vcCompanyName as numPartenerSource,
 cast(coalesce(DM.numPartenerContact,0) as NUMERIC(18,0)) AS numPartenerContactId,
 A1.vcFirstName || ' ' || A1.vcLastname AS numPartenerContact,
 (select  count(*) from GenericDocuments where numRecID = DM.numDivisionID and  vcDocumentSection = 'A' AND tintDocumentType = 2) as DocumentCount,
(CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactId,1,v_numDomainID) ELSE '' END) AS vcLastFollowup,
(CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactId,2,v_numDomainID) ELSE '' END) AS vcNextFollowup,
cast(coalesce(DM.vcBuiltWithJson,'') as TEXT) AS vcBuiltWithJson
--case when DM.vcBillStreet is null then '' when DM.vcBillStreet = ''then '' else vcBillStreet end +         
-- case when DM.vcBillCity is null then '' when DM.vcBillCity ='' then '' else DM.vcBillCity end +         
-- case when dbo.fn_GetState(DM.vcBilState) is null then '' when dbo.fn_GetState(DM.vcBilState) ='' then '' else ','+ dbo.fn_GetState(DM.vcBilState)end +         
-- case when DM.vcBillPostCode is null then '' when DM.vcBillPostCode ='' then '' else ','+DM.vcBillPostCode end +         
-- case when dbo.fn_GetListName(DM.vcBillCountry,0) is null then '' when dbo.fn_GetListName(DM.vcBillCountry,0) ='' then '' else ','+dbo.fn_GetListName(DM.vcBillCountry,0) end  as address                                         
   FROM
   CompanyInfo cmp join DivisionMaster DM
   on cmp.numCompanyId = DM.numCompanyID
   join  AdditionalContactsInformation ADC
   on  DM.numDivisionID = ADC.numDivisionId
   LEFT JOIN DivisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
   LEFT JOIN CompanyInfo C3 ON C3.numCompanyId = D3.numCompanyID
   LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId
   WHERE
   DM.tintCRMType = 0
   AND ADC.numContactId = v_numContactID  and ADC.numDomainID = v_numDomainID;
END; $$;












