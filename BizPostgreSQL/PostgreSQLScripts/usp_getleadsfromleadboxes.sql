-- Stored procedure definition script usp_GetLeadsFromLeadBoxes for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetLeadsFromLeadBoxes(v_numDomainId NUMERIC 
--  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(cmp.numCompanyId as VARCHAR(255)),
		cast(cmp.vcCompanyName as VARCHAR(255)),
		DM.vcDivisionName,
		DM.numDivisionID,
		ADC.numContactId,
		ADC.vcFirstName,
		ADC.vcLastname,
		ADC.numPhone || ' (' || ADC.numPhoneExtension || ')' AS numPhone,
		ADC.vcEmail
   FROM
   CompanyInfo cmp
   INNER JOIN
   DivisionMaster DM
   ON
   cmp.numCompanyId = DM.numCompanyID
   INNER JOIN
   AdditionalContactsInformation ADC
   ON
   ADC.numDivisionId = DM.numDivisionID
   WHERE
   DM.tintCRMType = 0
   AND DM.bitLeadBoxFlg = true
   AND DM.numDomainID = v_numDomainId
   ORDER BY
   cmp.vcCompanyName;
END; $$;












