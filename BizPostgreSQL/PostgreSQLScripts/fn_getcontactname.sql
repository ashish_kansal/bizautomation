-- Function definition script fn_GetContactName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetContactName(v_numContactID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetName  VARCHAR(100);
BEGIN
   v_RetName := '';
   select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') INTO v_RetName from AdditionalContactsInformation where numContactId = v_numContactID;
   RETURN coalesce(v_RetName,'-');
END; $$;

