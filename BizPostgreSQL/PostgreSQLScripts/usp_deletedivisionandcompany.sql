-- Stored procedure definition script usp_DeleteDivisionAndCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteDivisionAndCompany(v_numDivisionId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyId  NUMERIC;
BEGIN
   IF NOT EXISTS(SELECT * FROM  AdditionalContactsInformation WHERE numDivisionId = v_numDivisionId) then

      select   numCompanyID INTO v_numCompanyId FROM DivisionMaster WHERE  numDivisionID = v_numDivisionId;
   end if;
   RETURN;
END; $$;


