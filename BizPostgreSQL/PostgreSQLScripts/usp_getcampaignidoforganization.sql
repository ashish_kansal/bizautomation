-- Stored procedure definition script Usp_getCampaignIdOfOrganization for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_getCampaignIdOfOrganization(v_numDivisionID	NUMERIC(18,0),
	v_numDomainID	NUMERIC(10,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(coalesce(numCampaignID,0) as NUMERIC(18,0)) AS numCampaignID FROM DivisionMaster
   WHERE numDivisionID = v_numDivisionID
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;    













