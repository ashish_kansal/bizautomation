-- Stored procedure definition script USP_GetColumnConfiguration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetColumnConfiguration(v_numDomainID NUMERIC(9,0),          
 v_numUserCntId NUMERIC(9,0),        
 v_FormId SMALLINT ,      
v_numtype NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select numFormFieldId,vcFormFieldName from DynamicFormFieldMaster where numFormId = v_FormId
   order by vcFormFieldName;       
  
   open SWV_RefCur2 for
   select A.numFormFieldId,vcFieldName as vcFormFieldName,vcDbColumnName from InitialListColumnConf A
   join View_DynamicDefaultColumns D
   on D.numFieldID = A.numFormFieldId
   where D.numDomainID = v_numDomainID and D.numDomainID = v_numDomainID and A.numFormId = v_FormId  and numtype = v_numtype
   and numUserCntId = v_numUserCntId and A.numDomainID = v_numDomainID  order by A.tintOrder;
   RETURN;
END; $$;


