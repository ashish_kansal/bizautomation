-- Stored procedure definition script USP_ProCaseEmailList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProCaseEmailList(v_tintSortOrder NUMERIC DEFAULT 4,                      
v_dtFromDate TIMESTAMP DEFAULT NULL,                     
v_dtToDate TIMESTAMP DEFAULT NULL,                    
v_ProCaseNo VARCHAR(100) DEFAULT '',                    
v_SeachKeyword VARCHAR(100) DEFAULT '',                      
v_CurrentPage INTEGER DEFAULT NULL,                    
v_PageSize INTEGER DEFAULT NULL,                    
INOUT v_TotRecs INTEGER  DEFAULT NULL,                    
v_columnName VARCHAR(50) DEFAULT NULL,                    
v_columnSortOrder VARCHAR(10) DEFAULT NULL  ,  
v_numdomainId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(5000);                    
                    
                    
   v_SelColumn  VARCHAR(20);          
   v_firstRec  INTEGER;                                    
   v_lastRec  INTEGER;
BEGIN
   v_SeachKeyword := replace(v_SeachKeyword,'','''''');                
                    
                   
--Create a Temporary table to hold data                    
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numEmailHstrID VARCHAR(15)
   );                    
                     
                     
                    
                    
   v_SelColumn := ',' || coalesce(v_columnName,'');          
   if v_columnName = 'numEmailHstrID' then 
      v_SelColumn := '';
   end if;          
   v_strSql := 'select distinct(X.numEmailHstrID) from (select HDR.numEmailHstrID' || coalesce(v_SelColumn,'') || ' from EmailHistory HDR          
  left join EmailHStrToBCCAndCC DTL          
  on DTL.numEmailHstrID=HDR.numEmailHstrID      
join EmailMaster EM on DTL.numEmailId = EM.numEmailId                         
  where HDR.numdomainId = ' || SUBSTR(CAST(v_numdomainId AS VARCHAR(9)),1,9) || ' and (bintCreatedOn between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')                 
and (vcBody like ''%' || coalesce(v_ProCaseNo,'') || '%''                    
or vcSubject like ''%' || coalesce(v_ProCaseNo,'') || '%'')';          
          
          
   if v_SeachKeyword <> '' then
 
      if v_tintSortOrder = 0 then  
         v_strSql := coalesce(v_strSql,'') || '                     
  and (Em.vcEmailId like ''%' || coalesce(v_SeachKeyword,'') || '%''                                      
   or vcBody like ''%' || coalesce(v_SeachKeyword,'') || '%''                                  
   or vcSubject like ''%' || coalesce(v_SeachKeyword,'') || '%'')';
      ELSEIF v_tintSortOrder = 2
      then  
         v_strSql := coalesce(v_strSql,'') || ' and  DTL.tintType=4 and  Em.vcEmailId like ''%' || coalesce(v_SeachKeyword,'') || '%''';
      ELSEIF v_tintSortOrder = 3
      then  
         v_strSql := coalesce(v_strSql,'') || ' and DTL.tintType=1 and Em.vcEmailId like ''%' || coalesce(v_SeachKeyword,'') || '%''';
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' and vcSubject like ''%' || coalesce(v_SeachKeyword,'') || '%''';
      ELSEIF v_tintSortOrder = 5
      then  
         v_strSql := coalesce(v_strSql,'') || ' and vcBody like ''%' || coalesce(v_SeachKeyword,'') || '%''';
      end if;
   end if;                       
                     
                   
                    
   v_strSql := coalesce(v_strSql,'') || ')X ORDER BY X.' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');        
                                                                                   
   RAISE NOTICE '%',v_strSql;                                 
   EXECUTE 'insert into tt_TEMPTABLE (numEmailHstrID)           
 ' || v_strSql;                     
                    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                    
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                    
   open SWV_RefCur for
   select E.numEmailHstrID,GetEmaillAdd(E.numEmailHstrID,4::SMALLINT) as vcFromEmail,
 GetEmaillAdd(E.numEmailHstrID,1::SMALLINT) as vcMessageTo,
VcSubject, bintCreatedOn,bintCreatedOn as CreatedOn,
coalesce(CAST(numNoofTimes AS VARCHAR(15)),'-') as numNoofTimes from tt_TEMPTABLE T
   JOIN EmailHistory E
   ON E.numEmailHstrID = T.numEmailHstrID
   where ID > v_firstRec and ID < v_lastRec;                                    
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                    
   RETURN;
END; $$;


