-- Stored procedure definition script USP_GetBankMasterList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBankMasterList(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN
      open SWV_RefCur for SELECT cast(numBankMasterID as VARCHAR(255)),cast(vcFIName as VARCHAR(255)) FROM BankMaster;
   END;
   RETURN;
END; $$;













