-- Stored procedure definition script USP_ManageSavedSearch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSavedSearch(INOUT v_numSearchID NUMERIC  ,
    v_vcSearchName VARCHAR(50),
    v_vcSearchQuery VARCHAR(8000),
    v_intSlidingDays INTEGER DEFAULT 0,
    v_vcTimeExpression VARCHAR(1000) DEFAULT NULL,
    v_numFormID NUMERIC(18,0) DEFAULT NULL,
    v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
    v_numDomainID NUMERIC(18,0) DEFAULT NULL,
    v_tintMode SMALLINT DEFAULT NULL,
    v_vcSearchQueryDisplay VARCHAR(8000) DEFAULT NULL,
	v_vcSearchConditionJson TEXT DEFAULT NULL,
	v_vcSharedWithUsers TEXT DEFAULT NULL,
	v_vcDisplayColumns TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
    
      open SWV_RefCur for
      SELECT
      numSearchID AS "numSearchID",
            vcSearchName AS "vcSearchName",
            vcSearchQuery AS "vcSearchQuery",
            intSlidingDays AS "intSlidingDays",
            vcTimeExpression AS "vcTimeExpression",
            S.numFormId AS "numFormId",
            numUserCntID AS "numUserCntID",
            numDomainID AS "numDomainID",
            vcSearchQueryDisplay AS "vcSearchQueryDisplay"
			,CASE DFM.numFormID WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END AS "vcFormName"
			,vcSearchConditionJson AS "vcSearchConditionJson"
			,vcDisplayColumns AS "vcDisplayColumns"
			,COALESCE((SELECT
         string_agg(numUserCntID::VARCHAR,',')
         FROM
         SavedSearch SS
         WHERE
         SS.numDomainID = v_numDomainID
         AND numSharedFromSearchID = S.numSearchID),'') AS vcSharedWith
      FROM
      SavedSearch S
      INNER JOIN
      dynamicFormMaster DFM
      ON
      DFM.numFormID = S.numFormId
      WHERE
			(numSearchID = v_numSearchID OR v_numSearchID = 0)
      AND numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntID
      AND (S.numFormId = v_numFormID OR v_numFormID = 0);
   ELSEIF v_tintMode = 2
   then
    
      IF EXISTS(select * from SavedSearch where numSearchID = v_numSearchID) then
        
         UPDATE
         SavedSearch
         SET
         vcSearchName = v_vcSearchName,vcSearchQuery = v_vcSearchQuery,intSlidingDays = v_intSlidingDays,
         vcTimeExpression = v_vcTimeExpression,numFormId = v_numFormID,
         numUserCntID = v_numUserCntID,numDomainID = v_numDomainID,
         vcSearchQueryDisplay = v_vcSearchQueryDisplay,vcSearchConditionJson = v_vcSearchConditionJson,
         vcDisplayColumns = v_vcDisplayColumns
         WHERE
         numSearchID = v_numSearchID;
         DELETE FROM SavedSearch WHERE numDomainID = v_numDomainID AND numSharedFromSearchID = v_numSearchID;
         IF LENGTH(coalesce(v_vcSharedWithUsers,cast(0 as TEXT))) > 0 then
			
            INSERT  INTO SavedSearch(vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormId
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns)
            SELECT
            v_vcSearchName,
					v_vcSearchQuery,
					v_intSlidingDays,
					v_vcTimeExpression,
					v_numFormID,
					UserMaster.numUserDetailId,
					v_numDomainID,
					v_vcSearchQueryDisplay,
					v_vcSearchConditionJson,
					v_numSearchID,
					v_vcDisplayColumns
            FROM
            SplitIDs(v_vcSharedWithUsers,',') AS TEMPTable
            INNER JOIN
            UserMaster
            ON
            TEMPTable.Id = UserMaster.numUserDetailId
            WHERE
            UserMaster.numDomainID = v_numDomainID;
         end if;
      ELSE
         INSERT  INTO SavedSearch(vcSearchName
				,vcSearchQuery
				,intSlidingDays
				,vcTimeExpression
				,numFormId
				,numUserCntID
				,numDomainID
				,vcSearchQueryDisplay
				,vcSearchConditionJson
				,vcDisplayColumns)
			VALUES(v_vcSearchName,
                v_vcSearchQuery,
                v_intSlidingDays,
                v_vcTimeExpression,
                v_numFormID,
                v_numUserCntID,
                v_numDomainID,
                v_vcSearchQueryDisplay,
				v_vcSearchConditionJson,
				v_vcDisplayColumns);
			
         v_numSearchID := CURRVAL('SavedSearch_seq');
         IF LENGTH(coalesce(v_vcSharedWithUsers,cast(0 as TEXT))) > 0 then
			
            INSERT  INTO SavedSearch(vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormId
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns)
            SELECT
            v_vcSearchName,
					v_vcSearchQuery,
					v_intSlidingDays,
					v_vcTimeExpression,
					v_numFormID,
					UserMaster.numUserDetailId,
					v_numDomainID,
					v_vcSearchQueryDisplay,
					v_vcSearchConditionJson,
					v_numSearchID,
					v_vcDisplayColumns
            FROM
            SplitIDs(v_vcSharedWithUsers,',') AS TEMPTable
            INNER JOIN
            UserMaster
            ON
            TEMPTable.Id = UserMaster.numUserDetailId
            WHERE
            UserMaster.numDomainID = v_numDomainID;
         end if;
      end if;
   ELSEIF v_tintMode = 3
   then
    
      DELETE FROM SavedSearch WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND numSearchID = v_numSearchID;
   end if;
   RETURN;
END; $$;

