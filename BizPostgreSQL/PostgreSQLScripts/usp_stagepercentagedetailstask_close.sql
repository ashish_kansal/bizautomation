-- Stored procedure definition script USP_StagePercentageDetailsTask_Close for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTask_Close(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppId  INTEGER;
   v_numProjectId  INTEGER;
   v_numWorkOrderId  INTEGER;
   v_intTotalProgress  INTEGER DEFAULT 0;
   v_intTotalTaskCount  INTEGER DEFAULT 0;
   v_intTotalTaskClosed  INTEGER DEFAULT 0;
BEGIN
   IF EXISTS(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND coalesce(bitTaskClosed,false) = false) then
      UPDATE
      StagePercentageDetailsTask
      SET
      bitTaskClosed = true
      WHERE
      numDomainID = v_numDomainID
      AND numTaskId = v_numTaskID;
      select   coalesce(numOppId,0), coalesce(numProjectId,0), coalesce(numWorkOrderId,0) INTO v_numOppId,v_numProjectId,v_numWorkOrderId FROM
      StagePercentageDetailsTask WHERE
      numDomainID = v_numDomainID
      AND numTaskId = v_numTaskID;
      IF(v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderId > 0) then
         SELECT COUNT(*) INTO v_intTotalTaskCount FROM StagePercentageDetailsTask WHERE (numOppId = v_numOppId AND numProjectId = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderId) AND bitSavedTask = true;
         SELECT COUNT(*) INTO v_intTotalTaskClosed FROM StagePercentageDetailsTask WHERE (numOppId = v_numOppId AND numProjectId = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderId) AND bitSavedTask = true AND bitTaskClosed = true;
         IF(v_intTotalTaskCount > 0 AND v_intTotalTaskClosed > 0) then
			
            v_intTotalProgress := ROUND(CAST((CAST((v_intTotalTaskClosed::bigint*100) AS decimal)/v_intTotalTaskCount::bigint) AS NUMERIC),0);
         end if;
         IF((SELECT COUNT(*) FROM ProjectProgress WHERE (coalesce(numOppId,0) = v_numOppId AND coalesce(numProId,0) = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderId) AND numDomainID = v_numDomainID) > 0) then
			
            UPDATE ProjectProgress SET intTotalProgress = v_intTotalProgress WHERE (coalesce(numOppId,0) = v_numOppId AND coalesce(numProId,0) = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderId) AND numDomainID = v_numDomainID;
         ELSE
            IF(v_numOppId = 0) then
				
               v_numOppId := NULL;
            end if;
            IF(v_numProjectId = 0) then
				
               v_numProjectId := NULL;
            end if;
            IF (v_numWorkOrderId = 0) then
				
               v_numWorkOrderId := NULL;
            end if;
            INSERT INTO ProjectProgress(numOppId,
					numProId,
					numWorkOrderId,
					numDomainID,
					intTotalProgress)VALUES(v_numOppId,
					v_numProjectId,
					v_numWorkOrderId,
					v_numDomainID,
					v_intTotalProgress);
         end if;
      end if;
   end if;
   RETURN;
END; $$;


