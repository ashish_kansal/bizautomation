-- Stored procedure definition script USP_ReportListMaster_APPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_APPreBuildReport(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalAP  DECIMAL(20,5);
   v_APWithin30Days  DECIMAL(20,5) DEFAULT 0;
   v_APWithin31to60Days  DECIMAL(20,5) DEFAULT 0;
   v_APWithin61to90Days  DECIMAL(20,5) DEFAULT 0;
   v_APPastDue  DECIMAL(20,5) DEFAULT 0;
   v_dtFromDate  TIMESTAMP DEFAULT '1990-01-01 00:00:00.000';
   v_dtToDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_AuthoritativePurchaseBizDocId  INTEGER;
BEGIN
   v_dtFromDate := v_dtFromDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);
   v_dtToDate :=  v_dtToDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);

   select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM
   AuthoritativeBizDocs WHERE
   numDomainId = v_numDomainID;
  
   DROP TABLE IF EXISTS tt_TEMPAPRECORD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAPRECORD
   (
      DealAmount DECIMAL(20,5),
      dtDueDate TIMESTAMP,
      AmountPaid DECIMAL(20,5),
      monUnAppliedAmount DECIMAL(20,5)
   );

   BEGIN
      INSERT INTO tt_TEMPAPRECORD(DealAmount
			,dtDueDate
			,AmountPaid)
      SELECT
      coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount
			,dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER) ELSE 0 END || 'day' as interval) AS dtDueDate
			,coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0) AS AmountPaid
      FROM
      OpportunityMaster Opp
      INNER JOIN
      OpportunityBizDocs OB
      ON
      OB.numoppid = Opp.numOppId
      LEFT OUTER JOIN
      Currency C
      ON
      Opp.numCurrencyID = C.numCurrencyID
      WHERE
      tintopptype = 2
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND OB.bitAuthoritativeBizDocs = true
      AND coalesce(OB.tintDeferred,0) <> 1
      AND numBizDocId = v_AuthoritativePurchaseBizDocId
      AND OB.dtCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY
      OB.numoppid,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,OB.dtCreatedDate,
      Opp.bitBillingTerms,Opp.intBillingDays,OB.monDealAmount,Opp.numCurrencyID,
      OB.dtFromDate,OB.monAmountPaid
      HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) > 0
      UNION ALL 
		--Add Bill Amount
      SELECT
      coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0) AS DealAmount,
            BH.dtDueDate AS dtDueDate,
            coalesce(SUM(BH.monAmtPaid),0) AS AmountPaid
      FROM
      BillHeader BH
      WHERE
      BH.numDomainId = v_numDomainID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
      AND BH.dtBillDate BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY
      BH.numDivisionId,BH.dtDueDate
      HAVING
      coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0) > 0
      UNION ALL
		--Add Write Check entry (As Amount Paid)
      SELECT
      CAST(0 AS DOUBLE PRECISION) AS DealAmount,
            CH.dtCheckDate AS dtDueDate,
            coalesce(SUM(CD.monAmount),0) AS AmountPaid
      FROM
      CheckHeader CH
      INNER JOIN
      CheckDetails CD
      ON
      CH.numCheckHeaderID = CD.numCheckHeaderID
      INNER JOIN
      Chart_Of_Accounts COA
      ON
      COA.numAccountId = CD.numChartAcntId
      WHERE
      CH.numDomainID = v_numDomainID
      AND CH.tintReferenceType = 1
      AND COA.vcAccountCode ilike '01020102%'
      AND CH.dtCheckDate BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY
      CD.numCustomerId,CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
      UNION ALL
      SELECT
      CASE WHEN GJD.numDebitAmt > 0 THEN -1*GJD.numDebitAmt ELSE GJD.numCreditAmt END,
            GJH.datEntry_Date,
            CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      General_Journal_Header GJH
      INNER JOIN
      General_Journal_Details GJD
      ON
      GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN
      Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE
      GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike '01020102%'
      AND coalesce(numOppId,0) = 0
      AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJD.numCustomerId,0) > 0
      AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numCheckHeaderID,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
      AND coalesce(GJH.numPayrollDetailID,0) = 0
      AND GJH.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate
      UNION ALL
		--Add Commission Amount
      SELECT
      CAST(coalesce(SUM(BDC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) AS DOUBLE PRECISION)  AS DealAmount,
            OBD.dtCreatedDate AS dtDueDate,
            CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      BizDocComission BDC
      INNER JOIN
      OpportunityBizDocs OBD
      ON
      BDC.numOppBizDocId = OBD.numOppBizDocsId
      INNER JOIN
      OpportunityMaster Opp
      ON
      Opp.numOppId = OBD.numoppid
      INNER JOIN
      Domain D
      ON
      D.numDomainId = BDC.numDomainId
      LEFT JOIN
      PayrollTracking PT
      ON
      BDC.numComissionID = PT.numComissionID
      WHERE
      Opp.numDomainId = v_numDomainID
      AND BDC.numDomainId = v_numDomainID
      AND OBD.dtCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      AND OBD.bitAuthoritativeBizDocs = true
      GROUP BY
      D.numDivisionId,OBD.numoppid,BDC.numOppBizDocId,Opp.numCurrencyID,BDC.numComissionAmount,
      OBD.dtCreatedDate
      HAVING
      coalesce(BDC.numComissionAmount,0) > 0
      UNION ALL --Standalone Refund against Account Receivable
      SELECT
      CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt*-1 ELSE GJD.numCreditAmt END AS DealAmount,
			GJH.datEntry_Date
			,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      ReturnHeader RH
      JOIN
      General_Journal_Header GJH
      ON
      RH.numReturnHeaderID = GJH.numReturnID
      JOIN
      General_Journal_Details GJD
      ON
      GJH.numJOurnal_Id = GJD.numJournalId
      INNER JOIN
      Chart_Of_Accounts COA
      ON
      COA.numAccountId = GJD.numChartAcntId
      WHERE
      RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike '01020102%'
      AND monBizDocAmount > 0
      AND coalesce(RH.numBillPaymentIDRef,0) > 0
      AND RH.dtCreatedDate  BETWEEN v_dtFromDate AND v_dtToDate;
      INSERT INTO tt_TEMPAPRECORD(DealAmount,
			dtDueDate,
			AmountPaid,
			monUnAppliedAmount)
      SELECT
      0,
			NULL,
			0,
			SUM(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0))
      FROM
      BillPaymentHeader BPH
      WHERE
      BPH.numDomainId = v_numDomainID
      AND(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) > 0
      AND BPH.dtPaymentDate  BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY
      BPH.numDivisionId;
      SELECT SUM(DealAmount) INTO v_APWithin30Days FROM tt_TEMPAPRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 0 AND 30;
      SELECT SUM(DealAmount) INTO v_APWithin31to60Days FROM tt_TEMPAPRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 31 AND 60;
      SELECT SUM(DealAmount) INTO v_APWithin61to90Days FROM tt_TEMPAPRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 61 AND 90;
      SELECT SUM(DealAmount) INTO v_APPastDue FROM tt_TEMPAPRECORD WHERE GetUTCDateWithoutTime() > dtDueDate;
      EXCEPTION WHEN OTHERS THEN
		-- DO NOT RAISE ERROR
         NULL;
   END;

   open SWV_RefCur for SELECT
   cast(coalesce(v_TotalAP,0) as DECIMAL(20,5)) AS TotalAP
		,cast(coalesce(v_APWithin30Days,0) as DECIMAL(20,5)) AS APWithin30Days
		,cast(coalesce(v_APWithin31to60Days,0) as DECIMAL(20,5)) AS APWithin31to60Days
		,cast(coalesce(v_APWithin61to90Days,0) as DECIMAL(20,5)) AS APWithin61to90Days
		,cast(coalesce(v_APPastDue,0) as DECIMAL(20,5)) AS APPastDue;
END; $$;












