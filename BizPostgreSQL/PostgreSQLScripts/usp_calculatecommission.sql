DROP FUNCTION IF EXISTS USP_CalculateCommission;

CREATE OR REPLACE FUNCTION USP_CalculateCommission(v_numComPayPeriodID NUMERIC(18,0),
	 v_numDomainID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER)
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
	 
	-- NOTE: CALLED FROM USP_CommissionPayPeriod_Save WITH TRANSACTION

   AS $$
   DECLARE
   v_dtPayStart  DATE;
   v_dtPayEnd  DATE;

   v_numDiscountServiceItemID  NUMERIC(18,0);
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_tintCommissionType  SMALLINT;
   v_bitCommissionBasedOn  BOOLEAN;
   v_tintCommissionBasedOn  SMALLINT;
   v_bitIncludeTaxAndShippingInCommission  BOOLEAN;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numComRuleID  NUMERIC(18,0);
   v_tintComBasedOn  SMALLINT;
   v_tintComAppliesTo  SMALLINT;
   v_tintComOrgType  SMALLINT;
   v_tintComType  SMALLINT;
   v_tintAssignTo  SMALLINT;
   v_numTotalAmount  DECIMAL(20,5);
   v_numTotalUnit  DOUBLE PRECISION;
BEGIN


   DROP TABLE IF EXISTS tt_TEMPBIZCOMMISSION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPBIZCOMMISSION
   (
      numUserCntID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numOppBizDocID NUMERIC(18,0),
      numOppBizDocItemID NUMERIC(18,0),
      monCommission DECIMAL(20,5),
      numComRuleID NUMERIC(18,0),
      tintComType SMALLINT,
      tintComBasedOn SMALLINT,
      decCommission DOUBLE PRECISION,
      tintAssignTo SMALLINT,
      numOppItemID NUMERIC(18,0),
      monInvoiceSubTotal DECIMAL(20,5),
      monOrderSubTotal DECIMAL(20,5)
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TABLEPAID_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLEPAID CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEPAID
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numDivisionID NUMERIC(18,0),
      numRelationship NUMERIC(18,0),
      numProfile NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      numRecordOwner NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numItemClassification NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numOppBizDocID NUMERIC(18,0),
      numOppBizDocItemID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monTotAmount DECIMAL(20,5),
      monInvoiceSubTotal DECIMAL(20,5),
      monOrderSubTotal DECIMAL(20,5),
      monVendorCost DECIMAL(20,5),
      monAvgCost DECIMAL(20,5),
      numPartner NUMERIC(18,0),
      numPOID NUMERIC(18,0),
      numPOItemID NUMERIC(18,0),
      monPOCost DECIMAL(20,5)
   );

   select   dtStart, dtEnd INTO v_dtPayStart,v_dtPayEnd FROM
   CommissionPayPeriod WHERE
   numComPayPeriodID = v_numComPayPeriodID;

	--GET COMMISSION GLOBAL SETTINGS
   select   numDiscountServiceItemID, numShippingServiceItemID, tintCommissionType, bitIncludeTaxAndShippingInCommission, coalesce(tintCommissionBasedOn,1), coalesce(bitCommissionBasedOn,false) INTO v_numDiscountServiceItemID,v_numShippingServiceItemID,v_tintCommissionType,
   v_bitIncludeTaxAndShippingInCommission,v_tintCommissionBasedOn,v_bitCommissionBasedOn FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   DELETE FROM BizDocComission BC using OpportunityMaster OM where BC.numOppID = OM.numOppId AND BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = false
   AND OM.numOppId IS NULL;

   DELETE FROM BizDocComission BC using OpportunityItems OI where BC.numOppItemID = OI.numoppitemtCode AND BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppItemID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = false
   AND OI.numoppitemtCode IS NULL;

   DELETE FROM BizDocComission BC using OpportunityBizDocs OBD where BC.numOppBizDocId = OBD.numOppBizDocsId AND BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppBizDocId,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = false
   AND OBD.numOppBizDocsId IS NULL;

   DELETE FROM BizDocComission BC using OpportunityBizDocItems OBDI where BC.numOppBizDocItemID = OBDI.numOppBizDocItemID AND BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppBizDocItemID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = false
   AND OBDI.numOppBizDocItemID IS NULL;

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
   IF v_tintCommissionType = 3 then --Sales Order Sub-Total Amount
	
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppItemID IN(SELECT
      OI.numoppitemtCode
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND CAST(OM.bintCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);
      INSERT INTO
      tt_TABLEPAID(numDivisionID, numRelationship, numProfile, numAssignedTo, numRecordOwner, numItemCode, numItemClassification, numOppID, numOppItemID, numOppBizDocID, numOppBizDocItemID, numUnitHour, monTotAmount, monInvoiceSubTotal, monOrderSubTotal, monVendorCost, monAvgCost, numPartner, numPOID, numPOItemID, monPOCost)
      SELECT
      OM.numDivisionId,
			coalesce(CompanyInfo.numCompanyType,0),
			coalesce(CompanyInfo.vcProfile,0),
			OM.numassignedto,
			OM.numrecowner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppId,
			OI.numoppitemtCode,
			0,
			0,
			coalesce(OI.numUnitHour,0),
			(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 OR coalesce(OM.fltExchangeRate,0) = 1 THEN coalesce(OI.monTotAmount,0) ELSE ROUND(CAST(coalesce(OI.monTotAmount,0)*OM.fltExchangeRate AS NUMERIC),
         2) END),
			0,
			(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 OR coalesce(OM.fltExchangeRate,0) = 1 THEN coalesce(OI.monTotAmount,0) ELSE ROUND(CAST(coalesce(OI.monTotAmount,0)*OM.fltExchangeRate AS NUMERIC),
         2) END),
			(coalesce(OI.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OI.numUnitHour,0)),
			(coalesce(OI.monAvgCost,0)*coalesce(OI.numUnitHour,0)),
			OM.numPartner,
			coalesce((SELECT 
         OMInner.numOppId
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode LIMIT 1),(SELECT 
												OMInner.numOppId
											 FROM
											 OpportunityLinking OL
											 INNER JOIN
											 OpportunityMaster OMInner
											 ON
											 OL.numChildOppID = OMInner.numOppId
											 INNER JOIN
											 OpportunityItems OIInner
											 ON
											 OMInner.numOppId = OIInner.numOppId
											 WHERE
											 OL.numParentOppID = OM.numOppId
											 AND OIInner.numItemCode = OI.numItemCode
											 AND coalesce(OIInner.vcAttrValues,'') = coalesce(OI.vcAttrValues,'') LIMIT 1)),
			coalesce((SELECT 
				OIInner.numoppitemtCode
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode LIMIT 1),(SELECT 
														OIInner.numoppitemtCode
													 FROM
													 OpportunityLinking OL
													 INNER JOIN
													 OpportunityMaster OMInner
													 ON
													 OL.numChildOppID = OMInner.numOppId
													 INNER JOIN
													 OpportunityItems OIInner
													 ON
													 OMInner.numOppId = OIInner.numOppId
													 WHERE
													 OL.numParentOppID = OM.numOppId
													 AND OIInner.numItemCode = OI.numItemCode
													 AND coalesce(OIInner.vcAttrValues,'') = coalesce(OI.vcAttrValues,'') LIMIT 1)),
			coalesce(coalesce((SELECT 
				OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode LIMIT 1),(SELECT 
															OIInner.monPrice
														FROM
														OpportunityLinking OL
														INNER JOIN
														OpportunityMaster OMInner
														ON
														OL.numChildOppID = OMInner.numOppId
														INNER JOIN
														OpportunityItems OIInner
														ON
														OMInner.numOppId = OIInner.numOppId
														WHERE
														OL.numParentOppID = OM.numOppId
														AND OIInner.numItemCode = OI.numItemCode
														AND coalesce(OIInner.vcAttrValues,'') = coalesce(OI.vcAttrValues,'') LIMIT 1)),0)
      FROM
      OpportunityMaster OM
      INNER JOIN
      DivisionMaster
      ON
      OM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item
      ON
      OI.numItemCode = Item.numItemCode
      LEFT JOIN
      BizDocComission
      ON
      OM.numOppId = BizDocComission.numOppID
      AND OI.numoppitemtCode = BizDocComission.numOppItemID
      AND coalesce(bitCommisionPaid,false) = true 
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND CAST(OM.bintCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd
      AND BizDocComission.numComissionID IS NULL
      AND OI.numItemCode NOT IN(v_numDiscountServiceItemID)  /*DO NOT INCLUDE DISCOUNT ITEM*/
      AND 1 =(CASE
      WHEN OI.numItemCode = v_numShippingServiceItemID
      THEN
         CASE
         WHEN coalesce(v_bitIncludeTaxAndShippingInCommission,false) = true
         THEN
            1
         ELSE
            0
         END
      ELSE
         1
      END);
   ELSEIF v_tintCommissionType = 2
   then --Invoice Sub-Total Amount (Paid or Unpaid)
	
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(numOppBizDocId,0) = 0
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppItemID IN(SELECT
      OI.numoppitemtCode
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND CAST(OM.bintCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppBizDocItemID IN(SELECT
      coalesce(numOppBizDocItemID,0)
      FROM
      OpportunityBizDocs OppBiz
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppBiz.numoppid = OppM.numOppId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
      LEFT JOIN LATERAL(SELECT
         MAX(DM.dtDepositDate) AS dtDepositDate
         FROM
         DepositMaster DM
         JOIN
         DepositeDetails DD
         ON
         DM.numDepositId = DD.numDepositID
         WHERE
         DM.tintDepositePage = 2
         AND DM.numDomainId = v_numDomainID
         AND DD.numOppID = OppBiz.numOppID
         AND DD.numOppBizDocsID = OppBiz.numOppBizDocsID) TempDepositMaster on TRUE
      WHERE
      OppM.numDomainId = v_numDomainID
      AND OppM.tintopptype = 1
      AND OppM.tintoppstatus = 1
      AND OppBiz.bitAuthoritativeBizDocs = 1
      AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd) OR CAST(OppBiz.dtCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd));
      INSERT INTO
      tt_TABLEPAID(numDivisionID, numRelationship, numProfile, numAssignedTo, numRecordOwner, numItemCode, numItemClassification, numOppID, numOppItemID, numOppBizDocID, numOppBizDocItemID, numUnitHour, monTotAmount, monInvoiceSubTotal, monOrderSubTotal, monVendorCost, monAvgCost, numPartner, numPOID, numPOItemID, monPOCost)
      SELECT
      OM.numDivisionId,
			coalesce(CompanyInfo.numCompanyType,0),
			coalesce(CompanyInfo.vcProfile,0),
			OM.numassignedto,
			OM.numrecowner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppId,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			coalesce(OppBizItems.numUnitHour,0),
			(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 OR coalesce(OM.fltExchangeRate,0) = 1 THEN coalesce(OppBizItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppBizItems.monTotAmount,0)*OM.fltExchangeRate AS NUMERIC),2) END),
			(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 OR coalesce(OM.fltExchangeRate,0) = 1 THEN coalesce(OppBizItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppBizItems.monTotAmount,0)*OM.fltExchangeRate AS NUMERIC),2) END),
			(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 OR coalesce(OM.fltExchangeRate,0) = 1 THEN coalesce(OppMItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppMItems.monTotAmount,0)*OM.fltExchangeRate AS NUMERIC),2) END),
			(coalesce(OppMItems.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OppBizItems.numUnitHour,0)),
			(coalesce(OppMItems.monAvgCost,0)*coalesce(OppBizItems.numUnitHour,0)),
			OM.numPartner,
			coalesce((SELECT 
         OMInner.numOppId
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
         OMInner.numOppId
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),
			coalesce((SELECT 
				OIInner.numoppitemtCode
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
				OIInner.numoppitemtCode
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),
			coalesce(coalesce((SELECT 
				OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
				OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),0)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityBizDocs OBD
      ON
      OM.numOppId = OBD.numoppid
      INNER JOIN
      DivisionMaster
      ON
      OM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
      INNER JOIN
      OpportunityItems OppMItems
      ON
      OppBizItems.numOppItemID = OppMItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OppBizItems.numItemCode = Item.numItemCode
      LEFT JOIN
      BizDocComission BDC1
      ON
      OM.numOppId = BDC1.numOppID
      AND OppMItems.numoppitemtCode = BDC1.numOppItemID
      AND coalesce(bitCommisionPaid,false) = true
      LEFT JOIN
      BizDocComission BDC2
      ON
      OM.numOppId = BDC2.numOppID
      AND OppMItems.numoppitemtCode = BDC2.numOppItemID
      AND coalesce(BDC2.bitCommisionPaid,false) = true
      AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId
      AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND OBD.bitAuthoritativeBizDocs = 1
      AND CAST(OBD.dtCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd
      AND OppBizItems.numItemCode NOT IN(v_numDiscountServiceItemID)  /*DO NOT INCLUDE DISCOUNT ITEM*/
      AND BDC1.numComissionID IS NULL
      AND BDC2.numComissionID IS NULL
      AND 1 =(CASE
      WHEN OppBizItems.numItemCode = v_numShippingServiceItemID
      THEN
         CASE
         WHEN coalesce(v_bitIncludeTaxAndShippingInCommission,false) = true
         THEN
            1
         ELSE
            0
         END
      ELSE
         1
      END);
   ELSE
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(numOppBizDocId,0) = 0
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppItemID IN(SELECT
      OI.numoppitemtCode
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND CAST(OM.bintCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppBizDocItemID IN(SELECT
      coalesce(numOppBizDocItemID,0)
      FROM
      OpportunityBizDocs OppBiz
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppBiz.numoppid = OppM.numOppId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
      LEFT JOIN LATERAL(SELECT
         MAX(DM.dtDepositDate) AS dtDepositDate
         FROM
         DepositMaster DM
         JOIN
         DepositeDetails DD
         ON
         DM.numDepositId = DD.numDepositID
         WHERE
         DM.tintDepositePage = 2
         AND DM.numDomainId = v_numDomainID
         AND DD.numOppID = OppBiz.numOppID
         AND DD.numOppBizDocsID = OppBiz.numOppBizDocsID) TempDepositMaster on TRUE
      WHERE
      OppM.numDomainId = v_numDomainID
      AND OppM.tintopptype = 1
      AND OppM.tintoppstatus = 1
      AND OppBiz.bitAuthoritativeBizDocs = 1
      AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd) OR CAST(OppBiz.dtCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd));
      INSERT INTO
      tt_TABLEPAID(numDivisionID, numRelationship, numProfile, numAssignedTo, numRecordOwner, numItemCode, numItemClassification, numOppID, numOppItemID, numOppBizDocID, numOppBizDocItemID, numUnitHour, monTotAmount, monInvoiceSubTotal, monOrderSubTotal, monVendorCost, monAvgCost, numPartner, numPOID, numPOItemID, monPOCost)
      SELECT
      OppM.numDivisionId,
			coalesce(CompanyInfo.numCompanyType,0),
			coalesce(CompanyInfo.vcProfile,0),
			OppM.numassignedto,
			OppM.numrecowner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppId,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			coalesce(OppBizItems.numUnitHour,0),
			(CASE WHEN coalesce(OppM.fltExchangeRate,0) = 0 OR coalesce(OppM.fltExchangeRate,0) = 1 THEN coalesce(OppBizItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppBizItems.monTotAmount,0)*OppM.fltExchangeRate AS NUMERIC),2) END),
			(CASE WHEN coalesce(OppM.fltExchangeRate,0) = 0 OR coalesce(OppM.fltExchangeRate,0) = 1 THEN coalesce(OppBizItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppBizItems.monTotAmount,0)*OppM.fltExchangeRate AS NUMERIC),2) END),
			(CASE WHEN coalesce(OppM.fltExchangeRate,0) = 0 OR coalesce(OppM.fltExchangeRate,0) = 1 THEN coalesce(OppMItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppMItems.monTotAmount,0)*OppM.fltExchangeRate AS NUMERIC),2) END),
			(coalesce(OppMItems.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OppBizItems.numUnitHour,0)),
			(coalesce(OppMItems.monAvgCost,0)*coalesce(OppBizItems.numUnitHour,0)),
			OppM.numPartner,
			coalesce((SELECT 
         OMInner.numOppId
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OppM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
         OMInner.numOppId
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OppM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),
			coalesce((SELECT 
				OIInner.numoppitemtCode
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OppM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
			OIInner.numoppitemtCode
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OppM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),
			coalesce(coalesce((SELECT 
				OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OppM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1),(SELECT 
				OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OppM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND coalesce(OIInner.vcAttrValues,'') = coalesce(OppMItems.vcAttrValues,'') LIMIT 1)),0)
      FROM
      OpportunityBizDocs OppBiz
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppBiz.numoppid = OppM.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OppM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
      INNER JOIN
      OpportunityItems OppMItems
      ON
      OppBizItems.numOppItemID = OppMItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OppBizItems.numItemCode = Item.numItemCode
      LEFT JOIN
      BizDocComission BDC1
      ON
      OppM.numOppId = BDC1.numOppID
      AND OppMItems.numoppitemtCode = BDC1.numOppItemID
      AND coalesce(bitCommisionPaid,false) = true
      LEFT JOIN
      BizDocComission BDC2
      ON
      OppM.numOppId = BDC2.numOppID
      AND OppMItems.numoppitemtCode = BDC2.numOppItemID
      AND coalesce(BDC2.bitCommisionPaid,false) = true
      AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId
      AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
      LEFT JOIN LATERAL(SELECT
         MAX(DM.dtDepositDate) AS dtDepositDate
         FROM
         DepositMaster DM
         JOIN
         DepositeDetails DD
         ON
         DM.numDepositId = DD.numDepositID
         WHERE
         DM.tintDepositePage = 2
         AND DM.numDomainId = v_numDomainID
         AND DD.numOppID = OppBiz.numOppID
         AND DD.numOppBizDocsID = OppBiz.numOppBizDocsID) TempDepositMaster on TRUE 
      WHERE
      OppM.numDomainId = v_numDomainID
      AND OppM.tintopptype = 1
      AND OppM.tintoppstatus = 1
      AND OppBiz.bitAuthoritativeBizDocs = 1
      AND BDC1.numComissionID IS NULL
      AND BDC2.numComissionID IS NULL
      AND coalesce(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
      AND (TempDepositMaster.dtDepositDate IS NOT NULL AND CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd)
      AND OppBizItems.numItemCode NOT IN(v_numDiscountServiceItemID)  /*DO NOT INCLUDE DISCOUNT ITEM*/
      AND 1 =(CASE
      WHEN OppBizItems.numItemCode = v_numShippingServiceItemID
      THEN
         CASE
         WHEN coalesce(v_bitIncludeTaxAndShippingInCommission,false) = true
         THEN
            1
         ELSE
            0
         END
      ELSE
         1
      END);
   end if;	

	--GET COMMISSION RULE OF DOMAIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TempCommissionRule_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPCOMMISSIONRULE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONRULE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numComRuleID NUMERIC(18,0),
      tintComBasedOn SMALLINT,
      tintComType SMALLINT,
      tintComAppliesTo SMALLINT,
      tintComOrgType SMALLINT,
      tintAssignTo SMALLINT
   );

   INSERT INTO
   tt_TEMPCOMMISSIONRULE(numComRuleID, tintComBasedOn, tintComType, tintComAppliesTo, tintComOrgType, tintAssignTo)
   SELECT
   CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
   FROM
   CommissionRules CR
   LEFT JOIN
   PriceBookPriorities PBP
   ON
   CR.tintComAppliesTo = PBP.Step2Value
   AND CR.tintComOrgType = PBP.Step3Value
   WHERE
   CR.numDomainID = v_numDomainID
   AND coalesce(PBP.Priority,0) > 0
   ORDER BY
   coalesce(PBP.Priority,0) ASC;

	-- LOOP ALL COMMISSION RULES
   select   COUNT(*) INTO v_COUNT FROM tt_TEMPCOMMISSIONRULE;

   DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEM
   (
      ID INTEGER,
      UniqueID INTEGER,
      numUserCntID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numOppBizDocID NUMERIC(18,0),
      numOppBizDocItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monTotAmount DECIMAL(20,5),
      monVendorCost DECIMAL(20,5),
      monAvgCost DECIMAL(20,5),
      numOppItemID NUMERIC(18,0),
      monInvoiceSubTotal DECIMAL(20,5),
      monOrderSubTotal DECIMAL(20,5),
      numPOID NUMERIC(18,0),
      numPOItemID NUMERIC(18,0),
      monPOCost DECIMAL(20,5)
   );

	

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
   WHILE v_i <= v_COUNT LOOP
      DELETE FROM tt_TEMPITEM;

		--FETCH COMMISSION RULE
      select   numComRuleID, tintComBasedOn, tintComType, tintComAppliesTo, tintComOrgType, tintAssignTo INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tintComAppliesTo,v_tintComOrgType,
      v_tintAssignTo FROM tt_TEMPCOMMISSIONRULE WHERE ID = v_i;

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
      INSERT INTO
      tt_TEMPITEM
      SELECT
      ROW_NUMBER() OVER(ORDER BY ID),
			ID,
			(CASE v_tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal,
			numPOID,
			numPOItemID,
			monPOCost
      FROM
      tt_TABLEPAID
      WHERE
      1 =(CASE v_tintComAppliesTo
					--SPECIFIC ITEMS
      WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
      WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
      ELSE 1
      END)
      AND 1 =(CASE v_tintComOrgType
						-- SPECIFIC ORGANIZATION
      WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
      WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numRelationship AND coalesce(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
      ELSE 1
      END)
      AND 1 =(CASE v_tintAssignTo 
						-- ORDER ASSIGNED TO
      WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
      WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
      WHEN 3 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
      ELSE 0
      END);


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
      INSERT INTO tt_TEMPBIZCOMMISSION(numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal)
      SELECT
      numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE v_tintComType
      WHEN 1 --PERCENT
      THEN
         CASE
         WHEN coalesce(v_bitCommissionBasedOn,false) = true
         THEN(CASE
            WHEN numPOItemID IS NOT NULL
            THEN(coalesce(monTotAmount,0) -coalesce(monPOCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
            ELSE
               CASE v_tintCommissionBasedOn
											--ITEM GROSS PROFIT (VENDOR COST)
               WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
											--ITEM GROSS PROFIT (AVERAGE COST)
               WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
               END
            END)
         ELSE
            monTotAmount*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
         END
      ELSE  --FLAT
         T2.decCommission
      END,
			v_numComRuleID,
			v_tintComType,
			v_tintComBasedOn,
			T2.decCommission,
			v_tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
      FROM
      tt_TEMPITEM AS T1
      CROSS JOIN LATERAL(SELECT 
         coalesce(decCommission,0) AS decCommission
         FROM
         CommissionRuleDtl
         WHERE
         numComRuleID = v_numComRuleID
         AND coalesce(decCommission,0) > 0
         AND 1 =(CASE
         WHEN v_tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
         THEN(CASE WHEN T1.monTotAmount BETWEEN intFrom:: DECIMAL(20,5) AND intTo:: DECIMAL(20,5) THEN 1 ELSE 0 END)
         WHEN v_tintComBasedOn = 2 --BASED ON UNITS SOLD
         THEN(CASE WHEN T1.numUnitHour BETWEEN intFrom:: DOUBLE PRECISION AND intTo:: DOUBLE PRECISION THEN 1 ELSE 0 END)
         ELSE 0
         END) LIMIT 1) AS T2
      WHERE(CASE
      WHEN coalesce(v_bitCommissionBasedOn,false) = true
      THEN(CASE
         WHEN numPOItemID IS NOT NULL
         THEN(coalesce(monTotAmount,0) -coalesce(monPOCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
         ELSE
            CASE v_tintCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
            WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
								--ITEM GROSS PROFIT (AVERAGE COST)
            WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
            END
         END)
      ELSE
         monTotAmount*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
      END) > 0
      AND(SELECT
         COUNT(*)
         FROM
         tt_TEMPBIZCOMMISSION TempBDC
         WHERE
         TempBDC.numUserCntID = T1.numUserCntID
         AND TempBDC.numOppID = T1.numOppID
         AND TempBDC.numOppBizDocID = T1.numOppBizDocID
         AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
         AND TempBDC.numOppItemID = T1.numOppItemID
         AND tintAssignTo = v_tintAssignTo) = 0;
      v_i := v_i::bigint+1;
      v_numComRuleID := 0;
      v_tintComBasedOn := 0;
      v_tintComAppliesTo := 0;
      v_tintComOrgType := 0;
      v_tintComType := 0;
      v_tintAssignTo := 0;
      v_numTotalAmount := 0;
      v_numTotalUnit := 0;
   END LOOP;

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
   INSERT INTO BizDocComission(numDomainId,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn)
   SELECT
   v_numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		true,
		v_numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		v_bitCommissionBasedOn,
		v_tintCommissionBasedOn
   FROM
   tt_TEMPBIZCOMMISSION;
RETURN;
END; $$;


