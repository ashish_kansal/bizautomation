-- Stored procedure definition script USP_ManageItemCategories for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemCategories(v_numCategoryID NUMERIC(9,0) DEFAULT 0,  
v_numSubCategory NUMERIC(9,0) DEFAULT 0,  
v_numItemCode NUMERIC(9,0) DEFAULT 0,  
v_tintChecked SMALLINT DEFAULT 0,  
v_byteMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_level  SMALLINT;
BEGIN
   if v_byteMode = 0 then

      if v_tintChecked = 1 then
         select   tintLevel INTO v_level from Category where numCategoryID = v_numCategoryID;
         update Category set tintLevel =(v_level+1),numDepCategory = v_numCategoryID
         where numCategoryID = v_numSubCategory;
      else
         update Category set tintLevel = 1,numDepCategory = 0 where numCategoryID = v_numSubCategory;
      end if;
   end if;  
  
   if v_byteMode = 1 then

      if v_tintChecked = 1 then
 
         if not exists(select * from ItemCategory where numItemID = v_numItemCode and numCategoryID = v_numCategoryID) then
            insert into ItemCategory(numItemID,numCategoryID) values(v_numItemCode,v_numCategoryID);
         end if;
      else
         delete from ItemCategory where numItemID = v_numItemCode and numCategoryID = v_numCategoryID;
      end if;
   end if;

   if v_byteMode = 3 then

      if v_tintChecked = 1 then
 
         if not exists(select * from ItemCategory where numItemID = v_numItemCode and numCategoryID = v_numCategoryID) then
		
            insert into ItemCategory(numItemID,numCategoryID) values(v_numItemCode,v_numCategoryID);
         else
            update  ItemCategory set numItemID = v_numItemCode,numCategoryID = v_numCategoryID where  numItemID = v_numItemCode and numCategoryID = v_numCategoryID;
         end if;
      else
         delete from ItemCategory where numItemID = v_numItemCode;
      end if;
   end if;  
 
   if v_byteMode = 4 then --for item category update through csv

      delete from ItemCategory where numItemID = v_numItemCode;
      IF v_numCategoryID > 0 AND(SELECT COUNT(*) FROM Item WHERE numItemCode = v_numItemCode) > 0 AND(SELECT COUNT(*) FROM Category WHERE numCategoryID = v_numCategoryID) > 0 then
	
         insert into ItemCategory(numItemID,numCategoryID) values(v_numItemCode,v_numCategoryID);
      end if;
   end if;
   RETURN;
END; $$;



