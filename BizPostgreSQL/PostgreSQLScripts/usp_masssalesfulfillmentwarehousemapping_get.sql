-- Stored procedure definition script USP_MassSalesFulfillmentWarehouseMapping_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWarehouseMapping_Get(v_numDomainID NUMERIC(18,0)
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER
	,v_vcSources TEXT
	,v_vcCountries TEXT
	,v_vcStates TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS numTotalRecords
		,coalesce(LDOrderSource.vcData,'-') AS vcOrderSource
   FROM
   MassSalesFulfillmentWarehouseMapping MSFWM
   LEFT JOIN
   Listdetails LDOrderSource
   ON
   LDOrderSource.numListID = 9
   AND MSFWM.numOrderSource = LDOrderSource.numListItemID
   ORDER BY
   ID;
END; $$;












