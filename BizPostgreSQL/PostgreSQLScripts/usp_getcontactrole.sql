-- Stored procedure definition script usp_GetContactRole for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetContactRole(          
  --          
v_numDomainID NUMERIC,            
 v_dtDateFrom TIMESTAMP,            
 v_dtDateTo TIMESTAMP,            
 v_intOppStatus NUMERIC,          
 v_numUserCntID NUMERIC DEFAULT 0,                    
 v_intType NUMERIC DEFAULT 0,          
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 1 then          
 --All Records Rights            
 
      open SWV_RefCur for
      select AI.numContactId,DM.numDivisionID,CI.numCompanyId,DM.tintCRMType,OM.numOppId,OM.vcpOppName as OpportunityName,GetOppLstMileStonePercentage(OM.numOppId) as Status,
(AI.vcLastname || ' ' || AI.vcFirstName) as ContactName, monPAmount as Amount,
CI.vcCompanyName as CompanyName,
LD.vcData as Role
      from OpportunityMaster OM
      join  OpportunityContact OC
      on OC.numOppId = OM.numOppId
      left join Listdetails LD
      on LD.numListItemID = numRole
      join AdditionalContactsInformation AI
      on AI.numContactId = OC.numContactID
      join DivisionMaster DM
      on DM.numDivisionID = OM.numDivisionId
      join CompanyInfo CI
      on CI.numCompanyId = DM.numCompanyID
      WHERE OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intOppStatus
      AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
      and OM.numrecowner = v_numUserCntID;
   end if;            
            
   If v_tintRights = 2 then
 
      open SWV_RefCur for
      select AI.numContactId,DM.numDivisionID,CI.numCompanyId,DM.tintCRMType,OM.numOppId,OM.vcpOppName as OpportunityName,GetOppLstMileStonePercentage(OM.numOppId) as Status,
(AI.vcLastname || ' ' || AI.vcFirstName) as ContactName, monPAmount as Amount,
CI.vcCompanyName as CompanyName,
LD.vcData as Role
      from OpportunityMaster OM
      join  OpportunityContact OC
      on OC.numOppId = OM.numOppId
      left join Listdetails LD
      on LD.numListItemID = numRole
      join AdditionalContactsInformation AI
      on AI.numContactId = OC.numContactID
      join DivisionMaster DM
      on DM.numDivisionID = OM.numDivisionId
      join CompanyInfo CI
      on CI.numCompanyId = DM.numCompanyID
      Left outer join AdditionalContactsInformation ACI
      on OM.numrecowner = ACI.numContactId
      WHERE OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intOppStatus
      AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
      and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop          
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;            
            
   If v_tintRights = 3 then
 
      open SWV_RefCur for
      select AI.numContactId,DM.numDivisionID,CI.numCompanyId,DM.tintCRMType,OM.numOppId,OM.vcpOppName as OpportunityName,GetOppLstMileStonePercentage(OM.numOppId) as Status,
(AI.vcLastname || ' ' || AI.vcFirstName) as ContactName, monPAmount as Amount,
CI.vcCompanyName as CompanyName,
LD.vcData as Role
      from OpportunityMaster OM
      join  OpportunityContact OC
      on OC.numOppId = OM.numOppId
      left join Listdetails LD
      on LD.numListItemID = numRole
      join AdditionalContactsInformation AI
      on AI.numContactId = OC.numContactID
      join DivisionMaster DM
      on DM.numDivisionID = OM.numDivisionId
      join CompanyInfo CI
      on CI.numCompanyId = DM.numCompanyID
      WHERE OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intOppStatus
      AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
      and DM.numTerID  in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop           
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;
   RETURN;
END; $$;


