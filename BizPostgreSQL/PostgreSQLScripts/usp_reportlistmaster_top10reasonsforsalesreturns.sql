-- Stored procedure definition script USP_ReportListMaster_Top10ReasonsForSalesReturns for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ReasonsForSalesReturns(v_numDomainID NUMERIC(18,0),
	v_numRecordCount INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalSalesReturn  DOUBLE PRECISION DEFAULT 0.0;
BEGIN
   select   COUNT(numReturnHeaderID) INTO v_TotalSalesReturn FROM
   ReturnHeader WHERE
   numDomainID = v_numDomainID
   AND tintReturnType IN(1,3);
	
   open SWV_RefCur for SELECT Reason
		,(NoOfSalesReturn::bigint*100)/(CASE WHEN coalesce(v_TotalSalesReturn,0) = 0 THEN 1 ELSE v_TotalSalesReturn END) AS TotalReturnPercent
   FROM(SELECT
      coalesce(Listdetails.vcData,'-') AS Reason
			,COUNT(numReturnHeaderID) AS NoOfSalesReturn
      FROM
      ReturnHeader
      LEFT JOIN
      Listdetails
      ON
      ReturnHeader.numReturnReason = Listdetails.numListItemID
      AND Listdetails.numListID = 48
      AND (Listdetails.numDomainid = v_numDomainID OR coalesce(Listdetails.constFlag,false) = true)
      WHERE
      ReturnHeader.numDomainID = v_numDomainID
      AND tintReturnType IN(1,3)
      GROUP BY
      Listdetails.vcData) TEMP
   ORDER BY
   NoOfSalesReturn DESC;
END; $$;












