-- Stored procedure definition script Usp_ManageTerms for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION Usp_ManageTerms(v_numTermsID					BIGINT,
	v_vcTerms					VARCHAR(100),
	v_tintApplyTo				SMALLINT,
	v_numNetDueInDays			INTEGER,
	v_numDiscount				NUMERIC(10,2),
	v_numDiscountPaidInDays		INTEGER,
	v_numListItemID				NUMERIC(18,0),
	v_numDomainID				NUMERIC(18,0),
	v_bitActive					BOOLEAN,
	v_numInterestPercentage		NUMERIC(10,2), 
	v_numInterestPercentageIfPaidAfterDays NUMERIC(10,2),  
	v_numCreatedBy				NUMERIC(18,0), 
	v_dtCreatedDate				TIMESTAMP, 
	v_numModifiedBy				NUMERIC(18,0), 
	v_dtModifiedDate				TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM BillingTerms WHERE vcTerms = v_vcTerms AND numTermsID <> v_numTermsID AND numDomainID = v_numDomainID) then
	
	     -- Message text.
				    -- Severity.
				    -- State.
      RAISE EXCEPTION 'ERROR: Term details already exists. Please provide another Term.';
      RETURN;
   end if;	
	
   IF v_dtCreatedDate IS NULL then
      v_dtCreatedDate := TIMEZONE('UTC',now());
   end if;

   IF v_dtModifiedDate IS NULL then
      v_dtModifiedDate := TIMEZONE('UTC',now());
   end if;

   IF EXISTS(SELECT numTermsID FROM BillingTerms WHERE numTermsID = v_numTermsID) then
		
      UPDATE BillingTerms SET vcTerms = v_vcTerms,tintApplyTo = v_tintApplyTo,numNetDueInDays = v_numNetDueInDays,
      numDiscount = v_numDiscount,numDiscountPaidInDays = v_numDiscountPaidInDays,
      numListItemID = v_numListItemID,bitActive = v_bitActive,
      numInterestPercentage = v_numInterestPercentage,numInterestPercentageIfPaidAfterDays = v_numInterestPercentageIfPaidAfterDays,numModifiedBy = v_numModifiedBy,
      dtModifiedDate = v_dtModifiedDate
      WHERE numTermsID = v_numTermsID
      AND numDomainID = v_numDomainID;
   ELSE
      INSERT INTO BillingTerms(vcTerms,
				tintApplyTo,
				numNetDueInDays,
				numDiscount,
				numDiscountPaidInDays,
				numListItemID,
				numDomainID,
				bitActive,
				numInterestPercentage,
				numInterestPercentageIfPaidAfterDays,
				numCreatedBy,
				dtCreatedDate,
				numModifiedBy,
				dtModifiedDate)
			VALUES(v_vcTerms ,
				v_tintApplyTo ,
				v_numNetDueInDays,
				v_numDiscount,
				v_numDiscountPaidInDays,
				v_numListItemID,
				v_numDomainID,
				v_bitActive,
				v_numInterestPercentage,
				v_numInterestPercentageIfPaidAfterDays,
				v_numCreatedBy,
				v_dtCreatedDate,
				v_numModifiedBy,
				v_dtModifiedDate);
   end if;
END; $$;

