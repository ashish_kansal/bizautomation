CREATE OR REPLACE FUNCTION StagePercentageDetailsTaskTimeLog_Insert_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numContractsLogId  NUMERIC(18,0);
   v_numContractID  NUMERIC(18,0);
   v_numUsedTime  INTEGER;
   v_balanceContractTime  INTEGER;
   v_numTaskID  NUMERIC(18,0);
BEGIN
   BEGIN
      IF EXISTS(SELECT new_table.ID FROM new_table  WHERE tintAction = 4) then
	
		-- REMOVE EXISTING CONTRACT LOG ENTRY
         IF EXISTS(SELECT
         CL.numContractId
         FROM
         ContractsLog CL
         INNER JOIN
         new_table
         ON
         CL.numDivisionID = new_table.numDomainID
         AND CL.numReferenceId = new_table.numTaskID
         AND vcDetails = '1') then
		
            select   numContractsLogId, CL.numContractId, numUsedTime INTO v_numContractsLogId,v_numContractID,v_numUsedTime FROM
            ContractsLog CL
            INNER JOIN
            Contracts C
            ON
            CL.numContractId = C.numContractId
            INNER JOIN
            new_table
            ON
            C.numDomainId = new_table.numDomainID
            AND CL.numReferenceId = new_table.numTaskID
            AND vcDetails = '1';
            UPDATE Contracts SET timeUsed = coalesce(timeUsed,0) -coalesce(v_numUsedTime,0),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContractID;
            DELETE FROM ContractsLog WHERE numContractsLogId = v_numContractsLogId;
         end if;
      end if;
      IF EXISTS(SELECT
      C.numContractId
      FROM
      new_table
      INNER JOIN StagePercentageDetailsTask SPDT ON new_table.numTaskID = SPDT.numTaskId
      INNER JOIN ProjectsMaster PM ON SPDT.numProjectId = PM.numProId
      INNER JOIN Contracts C ON PM.numDivisionId = C.numDivisonId
      WHERE
      C.numDomainId = new_table.numDomainID
      AND C.numDivisonId = PM.numDivisionId
      AND C.intType = 1) then
	
         IF EXISTS(SELECT new_table.ID FROM new_table  WHERE tintAction = 4) then
		
            select   C.numContractId, new_table.numTaskID INTO v_numContractID,v_numTaskID FROM
            new_table
            INNER JOIN StagePercentageDetailsTask SPDT ON new_table.numTaskID = SPDT.numTaskId
            INNER JOIN ProjectsMaster PM ON SPDT.numProjectId = PM.numProId
            INNER JOIN Contracts C ON PM.numDivisionId = C.numDivisonId WHERE
            C.numDomainId = new_table.numDomainID
            AND C.numDivisonId = PM.numDivisionId
            AND C.intType = 1   ORDER BY
            C.numContractId DESC LIMIT 1;

			-- GET UPDATED TIME
            v_numUsedTime := CAST(GetTimeSpendOnTaskByProject(v_numDomainID,v_numTaskID,2::SMALLINT) AS INTEGER);

			-- CREATE NEW ENTRY FOR
            UPDATE Contracts SET timeUsed = timeUsed+(coalesce(v_numUsedTime,0)*60),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContractID;
            SELECT coalesce((coalesce(timeLeft,0) -coalesce(timeUsed,0)),0) INTO v_balanceContractTime FROM Contracts WHERE numContractId = v_numContractID;
            INSERT INTO ContractsLog(intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId)
            SELECT
            1,Contracts.numDivisonId,v_numTaskID,TIMEZONE('UTC',now()),0,1,(coalesce(v_numUsedTime,0)*60),v_balanceContractTime,numContractId
            FROM
            Contracts
            WHERE
            numContractId = v_numContractID;
         end if;
      end if;
   END;
   RETURN NULL;
   END; $$;
CREATE TRIGGER StagePercentageDetailsTaskTimeLog_Insert
AFTER INSERT
ON StagePercentageDetailsTaskTimeLog
REFERENCING NEW TABLE AS new_table
FOR EACH ROW
   EXECUTE PROCEDURE StagePercentageDetailsTaskTimeLog_Insert_TrFunc();


