-- Stored procedure definition script USP_GetCountryfromIP for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCountryfromIP(v_IPNo BIGINT DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(ConName as VARCHAR(255)) from IPToCountry where IPFrom <= v_IPNo and IPTo >= v_IPNo;
END; $$;












