-- Stored procedure definition script usp_GetDuplicateDivision for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDuplicateDivision(v_numCompanyID NUMERIC,
v_vcDivisionName VARCHAR(100)
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


--select * from DivisionMaster where 
--numCompanyID = @numCompanyIDand 
--vcDivisionName = @vcDivisionName
--and DivisionMaster.tintCRMType in (1,2)



   open SWV_RefCur for select DM.vcDivisionName, cast(CI.vcCompanyName as VARCHAR(255)),
  cast(coalesce(AD1.vcStreet,'') as VARCHAR(255)) as vcBillStreet,
  cast(coalesce(AD1.vcCity,'') as VARCHAR(255)) as vcBillCity,
  cast(coalesce(AD1.numState,0) as VARCHAR(255)) as vcBilState,
  cast(coalesce(AD1.vcPostalCode,'') as VARCHAR(255)) as vcBillPostCode,
  cast(coalesce(AD1.numCountry,0) as VARCHAR(255))  as vcBillCountry,
DM.tintCRMType,  cast(CI.numCompanyId as VARCHAR(255)), DM.numDivisionID
   from CompanyInfo CI, DivisionMaster DM
   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
   where CI.numCompanyId = DM.numCompanyID
   and CI.numCompanyId = v_numCompanyID  and DM.vcDivisionName = v_vcDivisionName and DM.bitPublicFlag = false and DM.tintCRMType in(1,2);
END; $$;












