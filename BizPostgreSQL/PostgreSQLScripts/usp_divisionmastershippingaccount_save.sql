-- Stored procedure definition script USP_DivisionMasterShippingAccount_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DivisionMasterShippingAccount_Save(v_numUserCntID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numShipViaID NUMERIC(18,0)
	,v_vcAccountNumber VARCHAR(100)
	,v_vcStreet VARCHAR(300)
	,v_vcCity VARCHAR(100)
	,v_numCountry NUMERIC(18,0)
	,v_numState NUMERIC(18,0)
	,v_vcZipCode VARCHAR(50))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID = v_numDivisionID AND numShipViaID = v_numShipViaID) then
	
      UPDATE
      DivisionMasterShippingAccount
      SET
      vcAccountNumber = v_vcAccountNumber,vcStreet = v_vcStreet,vcCity = v_vcCity,
      numCountry = v_numCountry,numState = v_numState,vcZipCode = v_vcZipCode,numModifiedBy = v_numUserCntID,
      dtModifiedDate = TIMEZONE('UTC',now())
      WHERE
      numDivisionID = v_numDivisionID
      AND numShipViaID = v_numShipViaID;
   ELSE
      INSERT INTO DivisionMasterShippingAccount(numDivisionID
			,numShipViaID
			,vcAccountNumber
			,vcStreet
			,vcCity
			,numCountry
			,numState
			,vcZipCode
			,numCreatedBy
			,dtCreatedDate)
		VALUES(v_numDivisionID
			,v_numShipViaID
			,v_vcAccountNumber
			,v_vcStreet
			,v_vcCity
			,v_numCountry
			,v_numState
			,v_vcZipCode
			,v_numUserCntID
			,TIMEZONE('UTC',now()));
   end if;
   RETURN;
END; $$;


