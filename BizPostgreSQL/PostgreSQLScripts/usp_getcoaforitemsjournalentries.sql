-- Stored procedure definition script USP_GetCOAforItemsJournalEntries for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCOAforItemsJournalEntries(v_numDomainId      NUMERIC(9,0)  DEFAULT 0,
               v_numItemId		 NUMERIC(9,0)  DEFAULT 0,
               v_tintMode         SMALLINT  DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then -- select all account Ids for  Item's Journal Entries
      
      open SWV_RefCur for SELECT GJD.numChartAcntId as numAccountId FROM General_Journal_Details GJD
      WHERE GJD.numItemID = v_numItemId and GJD.numDomainId = v_numDomainId
      GROUP by GJD.numChartAcntId;
   end if;
END; $$;














