-- Stored procedure definition script USP_TrailBalanceList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TrailBalanceList(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                
 v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                  
 v_dtFromDate TIMESTAMP DEFAULT NULL,                                          
 v_dtToDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select CA.numAccountId as numAccountId,CA.vcAccountName as AcntTypeDescription,CA.numAcntTypeId as numAcntType,
 fn_GetCurrentOpeningBalanceForTrailBalance(v_numChartAcntId,v_dtFromDate,v_dtToDate,v_numDomainId) As Amount
   from Chart_Of_Accounts CA  Left outer join Listdetails LD on CA.numAcntTypeId = LD.numListItemID
   Where  CA.numAccountId = v_numChartAcntId And  CA.numDomainId = v_numDomainId;
END; $$;












