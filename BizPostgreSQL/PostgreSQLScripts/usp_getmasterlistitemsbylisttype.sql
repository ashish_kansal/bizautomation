-- Stored procedure definition script usp_GetMasterListitemsByListType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetMasterListitemsByListType(v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numListID NUMERIC(9,0) DEFAULT 0   ,
v_numListType NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Ld.numListItemID,
		vcData,
		Ld.constFlag,
		bitDelete,
		coalesce(intSortOrder,0) AS intSortOrder ,
		coalesce(numListType,0) as numListType ,
		(SELECT vcData FROM Listdetails WHERE numListID = 27 and numListItemID = Ld.numListType) as vcListType,
		Case when Ld.numListType = 1 then 'Sales' when Ld.numListType = 2 then 'Purchase' else 'All' end as vcOrderType
   FROM Listdetails   Ld
   LEFT JOIN listorder LO on Ld.numListItemID = LO.numListItemID  and LO.numDomainId = v_numDomainID
   WHERE (Ld.numDomainid = v_numDomainID or Ld.constFlag = true) and Ld.numListID = v_numListID   and coalesce(Ld.numListType,0) In(0,v_numListType)
   ORDER BY intSortOrder;
END; $$;













