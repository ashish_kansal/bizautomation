-- Function definition script fn_DemandForecastDaysDisplay for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_DemandForecastDaysDisplay(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_numItemCode NUMERIC(18,0)
	,v_numLeadDays NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_numForecastDays NUMERIC(18,0)
	,v_bitShowHistoricSales BOOLEAN
	,v_numHistoricalAnalysisPattern NUMERIC(18,0)
	,v_bitBasedOnLastYear BOOLEAN
	,v_bitIncludeOpportunity BOOLEAN
	,v_numOpportunityPercentComplete NUMERIC(18,0)
	,v_tintDemandPlanBasedOn SMALLINT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcResult  VARCHAR(1000) DEFAULT '';


   v_fltDemandBasedOnReleaseDate  DOUBLE PRECISION DEFAULT 0;
   v_fltDemandBasedOnHistoricalSales  DOUBLE PRECISION DEFAULT 0;
   v_fltDemandBasedOnOpportunity  DOUBLE PRECISION DEFAULT 0;
   v_dtFromDate  TIMESTAMP;
   v_dtToDate  TIMESTAMP;
   v_numAnalysisDays  INTEGER DEFAULT 0;
   v_numBackOrder  DOUBLE PRECISION;
   v_numExpectedQty  DOUBLE PRECISION;
BEGIN
   v_fltDemandBasedOnReleaseDate := coalesce((SELECT
   SUM(numUnitHour -coalesce(numQtyShipped,0))
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND(coalesce(OI.numUnitHour,0) -coalesce(numQtyShipped,0)) > 0
   AND OI.numItemCode = v_numItemCode
   AND OI.numWarehouseItmsID = v_numWarehouseItemID
   AND coalesce(bitDropship,false) = false
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)),0);
	

	-- KIT CHILD ITEMS
   v_fltDemandBasedOnReleaseDate := coalesce(v_fltDemandBasedOnReleaseDate,0)+coalesce((SELECT
   SUM(OKI.numQtyItemsReq -coalesce(OKI.numQtyShipped,0))
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OI.numoppitemtCode = OKI.numOppItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND OKI.numChildItemID = v_numItemCode
   AND OKI.numWareHouseItemId = v_numWarehouseItemID
   AND coalesce(OI.bitWorkOrder,false) = false
   AND(coalesce(OKI.numQtyItemsReq,0) -coalesce(OKI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)),0);

	-- KIT CHILD ITEMS OF CHILD KITS
   v_fltDemandBasedOnReleaseDate := coalesce(v_fltDemandBasedOnReleaseDate,0)+coalesce((SELECT
   SUM(OKCI.numQtyItemsReq -coalesce(OKCI.numQtyShipped,0))
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitChildItems OKCI
   ON
   OI.numoppitemtCode = OKCI.numOppItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND OKCI.numItemID = v_numItemCode
   AND OKCI.numWareHouseItemId = v_numWarehouseItemID
   AND coalesce(OI.bitWorkOrder,false) = false
   AND(coalesce(OKCI.numQtyItemsReq,0) -coalesce(OKCI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)),0);

	--ITEMS USED IN WORK ORDER
   v_fltDemandBasedOnReleaseDate := coalesce(v_fltDemandBasedOnReleaseDate,0)+coalesce((SELECT
   SUM(WOD.numQtyItemsReq)
   FROM
   WorkOrderDetails WOD
   INNER JOIN
   WorkOrder WO
   ON
   WOD.numWOId = WO.numWOId
   LEFT JOIN
   OpportunityItems OI
   ON
   WO.numOppItemID = OI.numoppitemtCode
   LEFT JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   WHERE
   WO.numDomainId = v_numDomainID
   AND WO.numWOStatus <> 23184 -- NOT COMPLETED
   AND WOD.numChildItemID = v_numItemCode
   AND WOD.numWarehouseItemID = v_numWarehouseItemID
   AND coalesce(WOD.numQtyItemsReq,0) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
   AND 1 =(CASE WHEN OM.numOppId IS NOT NULL THEN(CASE WHEN OM.tintoppstatus = 1 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE
   WHEN OI.numoppitemtCode IS NOT NULL
   THEN(CASE
      WHEN coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      THEN 1
      ELSE 0
      END)
   ELSE(CASE
      WHEN WO.bintCompliationDate <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      THEN 1
      ELSE 0
      END)
   END)),
   0);


   IF coalesce(v_bitShowHistoricSales,false) = true then
	
		-- Get Days of Historical Analysis
      select   numDays INTO v_numAnalysisDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = v_numHistoricalAnalysisPattern;
      IF coalesce(v_bitBasedOnLastYear,false) = true then --last week from last year
		
         v_dtToDate := TIMEZONE('UTC',now())+INTERVAL '-1 day'+INTERVAL '-1 year';
         v_dtFromDate := TIMEZONE('UTC',now())+CAST(-(v_numAnalysisDays) || 'day' as interval)+INTERVAL '-1 year';
      ELSE
         v_dtToDate := TIMEZONE('UTC',now())+INTERVAL '-1 day';
         v_dtFromDate := TIMEZONE('UTC',now())+CAST(-(v_numAnalysisDays) || 'day' as interval);
      end if;
      v_fltDemandBasedOnHistoricalSales := coalesce((SELECT
      SUM(numUnitHour)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND numItemCode = v_numItemCode
      AND numWarehouseItmsID = v_numWarehouseItemID
      AND coalesce(OI.numUnitHour,0) > 0
      AND coalesce(bitDropship,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE)),0);
		

		-- KIT CHILD ITEMS
      v_fltDemandBasedOnHistoricalSales := coalesce(v_fltDemandBasedOnHistoricalSales,0)+coalesce((SELECT
      SUM(OKI.numQtyItemsReq)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND OKI.numChildItemID = v_numItemCode
      AND OKI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OKI.numQtyItemsReq,0) > 0
      AND coalesce(OI.bitWorkOrder,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE)),0);
		

		-- KIT CHILD ITEMS OF CHILD KITS
      v_fltDemandBasedOnHistoricalSales := coalesce(v_fltDemandBasedOnHistoricalSales,0)+coalesce((SELECT
      SUM(OKCI.numQtyItemsReq)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OI.numoppitemtCode = OKCI.numOppItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND OKCI.numItemID = v_numItemCode
      AND OKCI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OKCI.numQtyItemsReq,0) > 0
      AND coalesce(OI.bitWorkOrder,false) = false
      AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE)),0);

		--ITEMS USED IN WORK ORDER
      v_fltDemandBasedOnHistoricalSales := coalesce(v_fltDemandBasedOnHistoricalSales,0)+coalesce((SELECT
      SUM(WOD.numQtyItemsReq)
      FROM
      WorkOrderDetails WOD
      INNER JOIN
      WorkOrder WO
      ON
      WOD.numWOId = WO.numWOId
      LEFT JOIN
      OpportunityItems OI
      ON
      WO.numOppItemID = OI.numoppitemtCode
      LEFT JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      WHERE
      WO.numDomainId = v_numDomainID
      AND WOD.numChildItemID = v_numItemCode
      AND WOD.numWarehouseItemID = v_numWarehouseItemID
      AND coalesce(WOD.numQtyItemsReq,0) > 0
      AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE)),0);
      v_fltDemandBasedOnHistoricalSales :=(v_fltDemandBasedOnHistoricalSales/v_numAnalysisDays::bigint)*v_numForecastDays;
   end if;

   IF coalesce(v_bitIncludeOpportunity,false) = true then
	
      v_fltDemandBasedOnOpportunity := coalesce((SELECT
      SUM(OI.numUnitHour*(PP.intTotalProgress/100))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND numItemCode = v_numItemCode
      AND numWarehouseItmsID = v_numWarehouseItemID
      AND coalesce(bitDropship,false) = false
      AND(coalesce(OI.numUnitHour,0)*(PP.intTotalProgress/100)) > 0
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >=  v_numOpportunityPercentComplete),0);

		-- KIT CHILD ITEMS
      v_fltDemandBasedOnOpportunity := coalesce(v_fltDemandBasedOnOpportunity,0)+coalesce((SELECT
      SUM(OKI.numQtyItemsReq*(PP.intTotalProgress/100))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND OKI.numChildItemID = v_numItemCode
      AND OKI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND(coalesce(OKI.numQtyItemsReq,0)*(PP.intTotalProgress/100)) > 0
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete),0);
		

		-- KIT CHILD ITEMS OF CHILD KITS
      v_fltDemandBasedOnOpportunity := coalesce(v_fltDemandBasedOnOpportunity,0)+coalesce((SELECT
      SUM(OKCI.numQtyItemsReq*(PP.intTotalProgress/100))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OI.numoppitemtCode = OKCI.numOppItemID
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND OKCI.numItemID = v_numItemCode
      AND OKCI.numWareHouseItemId = v_numWarehouseItemID
      AND coalesce(OI.bitWorkOrder,false) = false
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
      AND(coalesce(OKCI.numQtyItemsReq,0)*(PP.intTotalProgress/100)) > 0
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete),0);

		--ITEMS USED IN WORK ORDER
      v_fltDemandBasedOnOpportunity := coalesce(v_fltDemandBasedOnOpportunity,0)+coalesce((SELECT
      SUM(WOD.numQtyItemsReq*(PP.intTotalProgress/100))
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      WorkOrder WO
      ON
      OI.numoppitemtCode = WO.numOppItemID
      INNER JOIN
      WorkOrderDetails WOD
      ON
      WO.numWOId = WOD.numWOId
      INNER JOIN
      ProjectProgress PP
      ON
      OM.numOppId = PP.numOppId
      WHERE
      OM.tintopptype = 1
      AND OM.tintoppstatus = 0
      AND WO.numDomainId = v_numDomainID
      AND WO.numWOStatus <> 23184 -- NOT COMPLETED
      AND WOD.numChildItemID = v_numItemCode
      AND WOD.numWarehouseItemID = v_numWarehouseItemID
      AND(coalesce(WOD.numQtyItemsReq,0)*(PP.intTotalProgress/100)) > 0
      AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
      AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)
      AND PP.intTotalProgress >= v_numOpportunityPercentComplete),0);
   end if;


   IF v_tintDemandPlanBasedOn = 2 then
      v_numBackOrder := coalesce((SELECT numBackOrder FROM WareHouseItems	WHERE numWareHouseItemID = v_numWarehouseItemID),0);
      v_numExpectedQty := coalesce((SELECT
      SUM(OI.numUnitHour)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.tintopptype = 2
      AND OM.tintoppstatus = 1
      AND numItemCode = v_numItemCode
      AND numWarehouseItmsID = v_numWarehouseItemID
      AND OI.ItemRequiredDate IS NOT NULL
      AND OI.ItemRequiredDate <= TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numForecastDays || 'day' as interval)),0);
      IF v_numBackOrder > v_numExpectedQty then
		
         v_numBackOrder := v_numBackOrder -v_numExpectedQty;
         v_numExpectedQty := 0;
         v_vcResult := CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',v_numForecastDays,
         '" onclick="return OpenDFRecords(1,',v_numForecastDays,
         ',',v_numItemCode,',',v_numWarehouseItemID,',0,0,0)"><span class="badge bg-red">',
         v_numBackOrder*-1,'</span></a><input id="hRelease',v_numForecastDays,
         '" type="hidden" value="',v_fltDemandBasedOnReleaseDate,'"></input></li>',
         (CASE WHEN v_bitShowHistoricSales = true THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',',v_numAnalysisDays,
            ',',coalesce(v_bitBasedOnLastYear,false),',0)"><span class="badge bg-yellow">',
            v_fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN v_bitIncludeOpportunity = true THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',0,0,',
            v_numOpportunityPercentComplete,''')"><span class="badge bg-green">',
            v_fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>');
      ELSE
         v_numExpectedQty := v_numExpectedQty -v_numBackOrder;
         v_vcResult := CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',v_numForecastDays,
         '" onclick="return OpenDFRecords(1,',v_numForecastDays,
         ',',v_numItemCode,',',v_numWarehouseItemID,',0,0,0)"><span class="badge bg-green">',
         v_numExpectedQty,'</span></a><input id="hRelease',v_numForecastDays,
         '" type="hidden" value="',v_fltDemandBasedOnReleaseDate,'"></input></li>',
         (CASE WHEN v_bitShowHistoricSales = true THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',',v_numAnalysisDays,
            ',',coalesce(v_bitBasedOnLastYear,false),',0)"><span class="badge bg-yellow">',
            v_fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN v_bitIncludeOpportunity = true THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',0,0,',
            v_numOpportunityPercentComplete,''')"><span class="badge bg-green">',
            v_fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>');
      end if;
   ELSE
      v_vcResult := CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',v_numForecastDays,
      '" onclick="return OpenDFRecords(1,',v_numForecastDays,
      ',',v_numItemCode,',',v_numWarehouseItemID,',0,0,0)"><span class="badge ',
      CASE WHEN v_fltDemandBasedOnReleaseDate > 0 THEN 'bg-red'  ELSE 'bg-light-blue' END,'">',CASE WHEN v_fltDemandBasedOnReleaseDate > 0 THEN v_fltDemandBasedOnReleaseDate*-1 ELSE v_fltDemandBasedOnReleaseDate END,
      '</span></a><input id="hRelease',v_numForecastDays,'" type="hidden" value="',
      v_fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN v_bitShowHistoricSales = true THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',',v_numAnalysisDays,
         ',',coalesce(v_bitBasedOnLastYear,false),',0)"><span class="badge bg-yellow">',
         v_fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN v_bitIncludeOpportunity = true THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',v_numForecastDays,',',v_numItemCode,',',v_numWarehouseItemID,',0,0,',
         v_numOpportunityPercentComplete,''')"><span class="badge bg-green">',
         v_fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),
      '</ul>');
   end if;

   RETURN v_vcResult;
END; $$;

