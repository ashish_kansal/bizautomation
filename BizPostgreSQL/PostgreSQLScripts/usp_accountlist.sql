-- Stored procedure definition script USP_AccountList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AccountList(v_CRMType NUMERIC,                                              
v_numUserCntID NUMERIC,                                              
v_tintUserRightType SMALLINT,                                              
v_tintSortOrder NUMERIC DEFAULT 4,                                                                  
v_SortChar CHAR(1) DEFAULT '0',                        
v_numDomainID NUMERIC(9,0) DEFAULT 0,                                              
v_FirstName VARCHAR(100) DEFAULT '',                                              
v_LastName VARCHAR(100) DEFAULT '',                                              
v_CustName VARCHAR(100) DEFAULT '',                                            
v_CurrentPage INTEGER DEFAULT NULL,                                            
v_PageSize INTEGER DEFAULT NULL,                                            
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                            
v_columnName VARCHAR(50) DEFAULT NULL,                                            
v_columnSortOrder VARCHAR(10) DEFAULT NULL    ,    
v_numProfile NUMERIC DEFAULT NULL   ,  
v_bitPartner BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                            
                                            
----Create a Temporary table to hold data                                            
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                      
-- numDivisionID numeric(9)                                         
-- )                                            
--                                            
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                            
   v_lastRec  INTEGER;                                            
   v_strSql  VARCHAR(5000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);  
                                         
   v_strSql := 'With tblAccount AS (SELECT ';                                            
   if v_tintSortOrder = 7 or v_tintSortOrder = 8 then  
      v_strSql := coalesce(v_strSql,'') || ' top 20 ';
   end if;                                            
   v_strSql := coalesce(v_strSql,'') || '  ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,                                             
     DM.numDivisionID                                              
    FROM  CompanyInfo CMP                                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID';                                    
                                    
   if v_tintSortOrder = 9 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactid=DM.numDivisionID ';
   end if;                                    
                                    
   v_strSql := coalesce(v_strSql,'') || '                                                
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                            
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                             
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus            
    left join  AdditionalContactsInformation AD1 on AD1.numContactID=DM.numAssignedTo                                            
  WHERE  ISNULL(ADC.bitPrimaryContact,false)=true                                         
    AND (DM.bitPublicFlag=false OR(DM.bitPublicFlag=true and DM.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))                                            
    AND DM.tintCRMType= ' || SUBSTR(CAST(v_CRMType AS VARCHAR(2)),1,2) || '                            
    AND DM.numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                      
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                             
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                                            
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and CMP.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                                            
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CMP.vcCompanyName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;    
  
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || 'and (DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ||
      ' or DM.numCreatedBy=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
   end if;  
                                       
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0 or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;
  
    
   if v_numProfile  <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and cmp.vcProfile = ' || SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
   end if;     
                                              
                                             
                                        
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                  

--+ ' ORDER BY DM.bintCreateddate desc '                                      
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numStatusID=2 ';
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numStatusID=3 ';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND ADC.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(30)) || '''';
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and DM.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 8
   then  
      v_strSql := coalesce(v_strSql,'') || ' and DM.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;
--+ ' ORDER BY DM.bintmodifieddate desc '                                                
                                            
--if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                 
--else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                    
--else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                
--else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
--else if @tintSortOrder=5  set @strSql=@strSql + ' , ' + @columnName +' '+ @columnSortOrder                                            
--else if @tintSortOrder=6  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
--else 
   if (v_tintSortOrder = 7 and v_columnName != 'DM.bintcreateddate') then
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF (v_tintSortOrder = 8 and v_columnName != 'DM.bintcreateddate')
   then
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 9
   then  
      v_strSql := coalesce(v_strSql,'') ||
      ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and cType=''O'' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                                             
                                   
--insert into #tempTable (                                            
--   numDivisionID                                         
--  )                                            
                                      
   v_strSql := coalesce(v_strSql,'') || ')         
                                  
                                  
SELECT RowNumber,CMP.vcCompanyName + '' - <I>'' + DM.vcDivisionName + ''</I>'' as CompanyName,                                               
     DM.numTerID,                                              
     ADC.vcFirstName + '' '' + ADC.vcLastName as PrimaryContact,                                           
     dbo.fn_GetListItemName(CMP.numNoOfEmployeesId) as Employees,                                              
        case when ADC.numPhone<>'''' then + ADC.numPhone +case when ADC.numPhoneExtension<>'''' then ''-'' + ADC.numPhoneExtension else '''' end           
     else '''' end as [Phone],
     ADC.vcEmail As vcEmail,                                              
     CMP.numCompanyID AS numCompanyID,                                              
     DM.numDivisionID As numDivisionID,                                              
     ADC.numContactID AS numContactID,                                                    
     LD.vcData as vcRating,                                
     LD1.vcData as Follow,                               
   DM.numCreatedby AS numCreatedby,DM.numRecOwner,                    
     dbo.fn_GetContactName(numAssignedTo)+''/''+dbo.fn_GetContactName(numAssignedBy) AssignedToBy                                            
    FROM  CompanyInfo CMP                                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                  
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                            
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                             
    left join ListDetails LD1 on LD1.numListItemID=DM.numFollowUpStatus                                                  
    join tblAccount T on T.numDivisionID=DM.numDivisionID                                             
  WHERE ISNULL(ADC.bitPrimaryContact,false)=true and RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '                   
 union select 0,null,count(*),null,null,null,null,null,null,null,null,null,null,null,null from tblAccount order by RowNumber'; 


   RAISE NOTICE '%',(v_strSql);
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;                                  
                                           
--drop table #tempTable


