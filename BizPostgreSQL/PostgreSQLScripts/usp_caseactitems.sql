CREATE OR REPLACE FUNCTION USP_CaseActItems(v_bytemode SMALLINT DEFAULT 0,                                                  
v_numdivisionid NUMERIC(9,0) DEFAULT 0,                                                  
v_numContactId NUMERIC(9,0) DEFAULT 0 ,                                              
v_bitTask SMALLINT DEFAULT 0,                                              
v_FromDate TIMESTAMP DEFAULT NULL,                                              
v_ToDate TIMESTAMP DEFAULT NULL,                                              
v_KeyWord VARCHAR(100) DEFAULT '',                                              
v_CurrentPage INTEGER DEFAULT NULL,                                              
v_PageSize INTEGER DEFAULT NULL,                                            
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Debasish to enable calculation of date according to client machine                                            
INOUT v_TotRecs INTEGER  DEFAULT NULL ,    
v_CaseId INTEGER DEFAULT NULL, 
INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                                                                           
AS $$
	DECLARE
	v_strSql TEXT;                                                                                      
	v_firstRec  INTEGER;                                              
	v_lastRec  INTEGER;
BEGIN
	BEGIN
		CREATE TEMP SEQUENCE tt_tempTable_seq;
		EXCEPTION WHEN OTHERS THEN NULL;
	END;
	drop table IF EXISTS tt_TEMPTABLE CASCADE;
	Create TEMPORARY TABLE tt_TEMPTABLE 
	( 
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
		numCommId VARCHAR(15)
	);                                                 
	
	if v_numdivisionid > 0 then
		v_strSql := 'SELECT  
						numCommId 
					FROM 
						Communication                                             
					where 
						bitclosedflag = ' || v_bytemode::int::bool || '                                              
						and Communication.numdivisionid = ' || v_numdivisionid || ' and caseId =' || v_CaseId;
		if v_KeyWord <> '' then 
			v_strSql := coalesce(v_strSql,'') ||  'and  textDetails ilike ''' || coalesce(v_KeyWord,'') || '%''';
		end if;
	end if;                                                  
                                                  
	if v_numContactId > 0 then
		v_strSql := 'SELECT ';
		
		if v_FromDate IS NOT NULL AND v_FromDate = '1900-01-01 00:00:00.000' then 
			v_strSql := coalesce(v_strSql,'') || ' top 10';
		end if;
      
		v_strSql := coalesce(v_strSql,'') || ' Communication.numCommId 
                FROM  Communication                                               
				where bitclosedflag = ' || v_bytemode::int::bool || '                                              
				and Communication.numContactId = ' || v_numContactId || ' and caseId =' || v_CaseId;

		if v_KeyWord <> '' then 
			v_strSql := coalesce(v_strSql,'') ||  'and  textDetails like ''' || coalesce(v_KeyWord,'') || '%''';
		end if;
	end if;                                              
                                              
                                              
	if v_FromDate IS NOT NULL AND v_FromDate > '1900-01-01 00:00:00.000' then 
		v_strSql := coalesce(v_strSql,'') || ' and dtStartTime  >= ''' || v_FromDate|| '''';

		If v_ToDate IS NOT NULL AND v_ToDate > '1900-01-01 00:00:00.000' then 
			v_strSql := coalesce(v_strSql,'') || ''' and dtStartTime  <= ''' || v_ToDate || '''';
		END IF;
	end if;                                              
	if   v_bitTask > 0 then 
		v_strSql := coalesce(v_strSql,'') || ' and  bitTask  = ' || v_bitTask || '';
	end if;                                     
                                              
	EXECUTE 'insert into tt_TEMPTABLE (numCommId) ' || v_strSql;                         
                                            
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                              
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                              
                                    
                                          
	open SWV_RefCur for
	select 
		C.numCommId
		,C.numCommId
		,coalesce(A2.vcFirstName,'') || ' ' || coalesce(A2.vcLastname,'') as vcusername
		,A1.numContactId
		,textDetails
		,coalesce(vcData,'-') as Activity
		,A1.vcFirstName || ' ' || A1.vcLastname as Name
		,case when A1.numPhone <> '' then A1.numPhone || case when A1.numPhoneExtension <> '' then ' - ' || A1.numPhoneExtension else '' end  else '' end as Phone
		,case when C.bitTask = 1 then 'Communication' when C.bitTask = 3 then 'Task' when C.bitTask = 4 then 'Notes' when C.bitTask = 5 then 'Follow up Status' end AS bitTask
		,dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) As CloseDate
		,dtCreatedDate As CreatedDate
		,C.CaseId
		,(select vcCaseNumber from Cases where Cases.numCaseId = C.CaseId LIMIT 1) as vcCasenumber
		,coalesce(caseTimeid,0) as caseTimeid
		,coalesce(caseExpid,0) as caseExpid
   from tt_TEMPTABLE T
   join Communication C
   on C.numCommId = cast(NULLIF(T.numCommId,'') as NUMERIC(18,0))
   JOIN AdditionalContactsInformation  A1
   ON C.numContactId = A1.numContactId
   left join AdditionalContactsInformation A2 on A2.numContactId = C.numAssign
   left join Listdetails on numActivity = numListID
   where ID > v_firstRec and ID < v_lastRec order by  C.numCommId  Desc;                                                          
                                            
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                              
   RETURN;
END; $$;


