-- Stored procedure definition script USP_MapBankStatementTransactions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Added By : Joseph ******/
/****** Updates Table BankStatementTransactions with Reference ID and Reference Type of Transaction ******/

CREATE OR REPLACE FUNCTION USP_MapBankStatementTransactions(v_numTransMappingId NUMERIC(18,0),
	v_numTransactionID NUMERIC(18,0),
	v_tintReferenceType SMALLINT,
	v_numReferenceID NUMERIC(18,0),
	v_bitIsActive BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT numTransMappingId FROM BankTransactionMapping WHERE numTransMappingId = v_numTransMappingId) then

      UPDATE BankTransactionMapping SET
      numTransactionID = v_numTransactionID,tintReferenceType = v_tintReferenceType,
      numReferenceID = v_numReferenceID,bitIsActive = v_bitIsActive,
      dtModifiedDate = LOCALTIMESTAMP
      WHERE
      numTransMappingId = v_numTransMappingId;
   ELSE
      INSERT INTO BankTransactionMapping(numTransactionID,
		tintReferenceType,
		numReferenceID,
		bitIsActive,
		dtCreatedDate) VALUES(v_numTransactionID,
		v_tintReferenceType,
		v_numReferenceID,
		v_bitIsActive,
		LOCALTIMESTAMP);
   end if;
   RETURN;
END; $$;




