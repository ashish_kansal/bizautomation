-- Stored procedure definition script USP_ItemCurrencyPrice_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemCurrencyPrice_Save(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_vcRecords TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	--VALIDATE PROMOTION OF COUPON CODE
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numCurrencyID NUMERIC(18,0),
      monListPrice DECIMAL(20,5)
   );

   INSERT INTO tt_TEMP(numCurrencyID,
		monListPrice)
   SELECT
   numCurrencyID,
		monListPrice
   FROM
   XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_vcRecords AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numCurrencyID NUMERIC(18,0) PATH 'numCurrencyID',
			monListPrice DECIMAL(20,5) PATH 'monListPrice'
	) AS X;

   UPDATE
   ItemCurrencyPrice ICP
   SET
   monListPrice = coalesce(T.monListPrice,0)
   FROM
   tt_TEMP T
   WHERE
   ICP.numCurrencyID = T.numCurrencyID AND(ICP.numDomainID = v_numDomainID
   AND ICP.numItemCode = v_numItemCode);
		
   INSERT INTO ItemCurrencyPrice(numDomainID
		,numItemCode
		,numCurrencyID
		,monListPrice)
   SELECT
   v_numDomainID
		,v_numItemCode
		,T.numCurrencyID
		,coalesce(T.monListPrice,0)
   FROM
   tt_TEMP T
   WHERE
   numCurrencyID NOT IN(SELECT numCurrencyID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode);
RETURN;
END; $$;


