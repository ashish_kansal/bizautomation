-- Function definition script GetCommissionPaidCreditMemoOrRefund for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCommissionPaidCreditMemoOrRefund(v_numDomainID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER,
	 v_dtStartDate DATE,
	 v_dtEndDate DATE)
RETURNS TABLE
(
   numUserCntID NUMERIC(18,0),
   numReturnHeaderID NUMERIC(18,0),
   tintReturnType SMALLINT,
   numReturnItemID NUMERIC(18,0),
   monCommission DECIMAL(20,5),
   numComRuleID NUMERIC(18,0),
   tintComType SMALLINT,
   tintComBasedOn SMALLINT,
   decCommission DOUBLE PRECISION,
   tintAssignTo SMALLINT
) LANGUAGE plpgsql   
	--GET COMMISSION RULE OF DOMAIN
   AS $$
   DECLARE
   v_numDiscountServiceItemID  NUMERIC(18,0);
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_tintCommissionType  SMALLINT;
   v_bitIncludeTaxAndShippingInCommission  BOOLEAN;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numComRuleID  NUMERIC(18,0);
   v_tintComBasedOn  SMALLINT;
   v_tintComAppliesTo  SMALLINT;
   v_tintComOrgType  SMALLINT;
   v_tintComType  SMALLINT;
   v_tintAssignTo  SMALLINT;
   v_decCommission  DECIMAL(20,5);
   v_numTotalAmount  DECIMAL(20,5);
   v_numTotalUnit  DOUBLE PRECISION;
BEGIN
   DROP TABLE IF EXISTS tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT
   (
      numUserCntID NUMERIC(18,0),
      numReturnHeaderID NUMERIC(18,0),
      tintReturnType SMALLINT,
      numReturnItemID NUMERIC(18,0),
      monCommission DECIMAL(20,5),
      numComRuleID NUMERIC(18,0),
      tintComType SMALLINT,
      tintComBasedOn SMALLINT,
      decCommission DOUBLE PRECISION,
      tintAssignTo SMALLINT
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TempCommissionRule_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPCOMMISSIONRULE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONRULE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numComRuleID NUMERIC(18,0),
      tintComBasedOn SMALLINT,
      tintComType SMALLINT,
      tintComAppliesTo SMALLINT,
      tintComOrgType SMALLINT,
      tintAssignTo SMALLINT
   );
   INSERT INTO
   tt_TEMPCOMMISSIONRULE(numComRuleID, tintComBasedOn, tintComType, tintComAppliesTo, tintComOrgType, tintAssignTo)
   SELECT
   CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
   FROM
   CommissionRules CR
   LEFT JOIN
   PriceBookPriorities PBP
   ON
   CR.tintComAppliesTo = PBP.Step2Value
   AND CR.tintComOrgType = PBP.Step3Value
   WHERE
   CR.numDomainID = v_numDomainID
   AND coalesce(PBP.Priority,0) > 0
   ORDER BY
   coalesce(PBP.Priority,0) ASC,CR.numComRuleID;

	--GET COMMISSION GLOBAL SETTINGS
   select   numDiscountServiceItemID, numShippingServiceItemID, tintCommissionType, bitIncludeTaxAndShippingInCommission INTO v_numDiscountServiceItemID,v_numShippingServiceItemID,v_tintCommissionType,
   v_bitIncludeTaxAndShippingInCommission FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   BEGIN
      CREATE TEMP SEQUENCE tt_TABLEPAID_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLEPAID CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEPAID
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numDivisionID NUMERIC(18,0),
      numRelationship NUMERIC(18,0),
      numProfile NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      numRecordOwner NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numItemClassification NUMERIC(18,0),
      numReturnHeaderID NUMERIC(18,0),
      tintReturnType SMALLINT,
      numReturnItemID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monTotAmount DECIMAL(20,5),
      monVendorCost DECIMAL(20,5),
      monAvgCost DECIMAL(20,5),
      numPartner NUMERIC(18,0)
   );

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
   INSERT INTO
   tt_TABLEPAID(numDivisionID, numRelationship, numProfile, numAssignedTo, numRecordOwner, numItemCode, numItemClassification, numReturnHeaderID, tintReturnType, numReturnItemID, numUnitHour, monTotAmount, monVendorCost, monAvgCost, numPartner)
   SELECT
   DivisionMaster.numDivisionID,
		coalesce(CompanyInfo.numCompanyType,0),
		coalesce(CompanyInfo.vcProfile,0),
		(CASE WHEN coalesce(OMSalesReturn.numassignedto,0) > 0 THEN OMSalesReturn.numassignedto ELSE OMCreditMemo.numassignedto END),
		(CASE WHEN coalesce(OMSalesReturn.numrecowner,0) > 0 THEN OMSalesReturn.numrecowner ELSE OMCreditMemo.numrecowner END),
		Item.numItemCode,
		Item.numItemClassification,
		ReturnHeader.numReturnHeaderID,
		ReturnHeader.tintReturnType,
		(CASE WHEN ReturnHeader.tintReturnType IN(1,2) THEN ReturnItems.numReturnItemID ELSE 0 END),
		(CASE WHEN ReturnHeader.tintReturnType IN(1,2) THEN coalesce(ReturnItems.numUnitHour,0) ELSE 0 END),
		(CASE WHEN ReturnHeader.tintReturnType IN(1,2) THEN coalesce(ReturnItems.monTotAmount,0) ELSE ReturnHeader.monBizDocAmount END),
		(CASE WHEN ReturnHeader.tintReturnType IN(1,2) THEN(coalesce(ReturnItems.monVendorCost,coalesce((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID = Vendor.numVendorID AND Vendor.numItemCode = Item.numItemCode WHERE Item.numItemCode = ReturnItems.numItemCode),0))*coalesce(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN ReturnHeader.tintReturnType IN(1,2) THEN(coalesce(ReturnItems.monAverageCost,Item.monAverageCost)*coalesce(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN coalesce(OMSalesReturn.numPartner,0) > 0 THEN OMSalesReturn.numPartner ELSE OMCreditMemo.numPartner END)
   FROM
   ReturnHeader
   INNER JOIN
   DivisionMaster
   ON
   ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN
   OpportunityMaster OMSalesReturn
   ON
   OMSalesReturn.numOppId = ReturnHeader.numOppId
   LEFT JOIN
   OpportunityMaster OMCreditMemo
   ON
   OMCreditMemo.numOppId = ReturnHeader.numReferenceSalesOrder
   LEFT JOIN
   ReturnItems
   ON
   ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
   LEFT JOIN
   OpportunityItems
   ON
   ReturnItems.numOppItemID = OpportunityItems.numoppitemtCode
   LEFT JOIN
   Item
   ON
   ReturnItems.numItemCode = Item.numItemCode
   LEFT JOIN LATERAL(SELECT
      MAX(datEntry_Date) AS dtDepositDate
      FROM
      General_Journal_Header
      WHERE
      numDomainId = v_numDomainID
      AND numReturnID = ReturnHeader.numReturnHeaderID) TempDepositMaster on TRUE
   WHERE
   ReturnHeader.numDomainID = v_numDomainID
   AND ReturnHeader.numReturnStatus = 303
   AND (coalesce(ReturnHeader.numOppId,0) > 0 OR coalesce(ReturnHeader.numReferenceSalesOrder,0) > 0)
   AND (TempDepositMaster.dtDepositDate IS NOT NULL AND CAST(TempDepositMaster.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtStartDate AND v_dtEndDate)
   AND (ReturnItems.numItemCode IS NULL OR ReturnItems.numItemCode NOT IN(v_numDiscountServiceItemID))  /*DO NOT INCLUDE DISCOUNT ITEM*/
   AND 1 =(CASE
   WHEN ReturnItems.numItemCode = v_numShippingServiceItemID
   THEN
      CASE
      WHEN coalesce(v_bitIncludeTaxAndShippingInCommission,false) = true
      THEN
         1
      ELSE
         0
      END
   ELSE
      1
   END);/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */

		-- LOOP ALL COMMISSION RULES
   select   COUNT(*) INTO v_COUNT FROM tt_TEMPCOMMISSIONRULE;

   DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEM
   (
      ID INTEGER,
      UniqueID INTEGER,
      numUserCntID NUMERIC(18,0),
      numReturnHeaderID NUMERIC(18,0),
      tintReturnType SMALLINT,
      numReturnItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monTotAmount DECIMAL(20,5),
      monVendorCost DECIMAL(20,5),
      monAvgCost DECIMAL(20,5)
   );

		

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
   WHILE v_i <= v_COUNT LOOP
      DELETE FROM tt_TEMPITEM;

			--FETCH COMMISSION RULE
      select   tt_TEMPCOMMISSIONRULE.numComRuleID, tt_TEMPCOMMISSIONRULE.tintComBasedOn, tt_TEMPCOMMISSIONRULE.tintComType, tt_TEMPCOMMISSIONRULE.tintComAppliesTo, tt_TEMPCOMMISSIONRULE.tintComOrgType, tt_TEMPCOMMISSIONRULE.tintAssignTo INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tintComAppliesTo,v_tintComOrgType,
      v_tintAssignTo FROM tt_TEMPCOMMISSIONRULE WHERE ID = v_i;

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
      INSERT INTO
      tt_TEMPITEM
      SELECT
      ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				(CASE v_tintAssignTo WHEN 1 THEN tt_TABLEPAID.numAssignedTo WHEN 2 THEN tt_TABLEPAID.numRecordOwner ELSE tt_TABLEPAID.numPartner END),
				tt_TABLEPAID.numReturnHeaderID,
				tt_TABLEPAID.tintReturnType,
				tt_TABLEPAID.numReturnItemID,
				tt_TABLEPAID.numItemCode,
				tt_TABLEPAID.numUnitHour,
				tt_TABLEPAID.monTotAmount,
				tt_TABLEPAID.monVendorCost,
				tt_TABLEPAID.monAvgCost
      FROM
      tt_TABLEPAID
      WHERE
      1 =(CASE
      WHEN tt_TABLEPAID.tintReturnType = 3 OR tt_TABLEPAID.tintReturnType = 4 THEN 1
      ELSE(CASE v_tintComAppliesTo
							--SPECIFIC ITEMS
         WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE CommissionRuleItems.numComRuleID = v_numComRuleID AND coalesce(CommissionRuleItems.numValue,0) = tt_TABLEPAID.numItemCode) > 0 THEN 1 ELSE 0 END
							-- ITEM WITH SPECIFIC CLASSIFICATIONS
         WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE CommissionRuleItems.numComRuleID = v_numComRuleID AND coalesce(CommissionRuleItems.numValue,0) = tt_TABLEPAID.numItemClassification) > 0 THEN 1 ELSE 0 END
							-- ALL ITEMS
         ELSE 1
         END)
      END)
      AND 1 =(CASE v_tintComOrgType
							-- SPECIFIC ORGANIZATION
      WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE CommissionRuleOrganization .numComRuleID = v_numComRuleID AND coalesce(CommissionRuleOrganization .numValue,0) = tt_TABLEPAID.numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
      WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE CommissionRuleOrganization .numComRuleID = v_numComRuleID AND coalesce(CommissionRuleOrganization .numValue,0) = tt_TABLEPAID.numRelationship AND coalesce(CommissionRuleOrganization.numProfile,0) = tt_TABLEPAID.numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
      ELSE 1
      END)
      AND 1 =(CASE v_tintAssignTo 
							-- ORDER ASSIGNED TO
      WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE CommissionRuleContacts.numComRuleID = v_numComRuleID AND CommissionRuleContacts.numValue = tt_TABLEPAID.numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
      WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE CommissionRuleContacts.numComRuleID = v_numComRuleID AND CommissionRuleContacts.numValue = tt_TABLEPAID.numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
      WHEN 3 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE CommissionRuleContacts.numComRuleID = v_numComRuleID AND CommissionRuleContacts.numValue = tt_TABLEPAID.numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
      ELSE 0
      END);

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
      IF(SELECT COUNT(*) FROM tt_TEMPITEM) > 0 then
			
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
         IF v_tintComBasedOn = 1 then
				
            select   SUM(monTotAmount) INTO v_numTotalAmount FROM tt_TEMPITEM;
            IF(SELECT COUNT(*) FROM CommissionRuleDtl WHERE CommissionRuleDtl.numComRuleID = v_numComRuleID AND v_numTotalAmount BETWEEN CommissionRuleDtl.intFrom:: DECIMAL(20,5) AND CommissionRuleDtl.intTo:: DECIMAL(20,5)) > 0 then
					
               select   coalesce(CommissionRuleDtl.decCommission,0) INTO v_decCommission FROM CommissionRuleDtl WHERE CommissionRuleDtl.numComRuleID = v_numComRuleID AND v_numTotalAmount BETWEEN CommissionRuleDtl.intFrom:: DECIMAL(20,5) AND CommissionRuleDtl.intTo:: DECIMAL(20,5)    LIMIT 1;
            end if;
				--BASED ON UNITS SOLD
         ELSEIF v_tintComBasedOn = 2
         then
				
            select   SUM(numUnitHour) INTO v_numTotalUnit FROM tt_TEMPITEM;
            IF(SELECT COUNT(*) FROM CommissionRuleDtl WHERE CommissionRuleDtl.numComRuleID = v_numComRuleID AND v_numTotalUnit BETWEEN CommissionRuleDtl.intFrom:: DOUBLE PRECISION AND CommissionRuleDtl.intTo:: DOUBLE PRECISION) > 0 then
					
               select   coalesce(CommissionRuleDtl.decCommission,0) INTO v_decCommission FROM CommissionRuleDtl WHERE CommissionRuleDtl.numComRuleID = v_numComRuleID AND v_numTotalUnit BETWEEN CommissionRuleDtl.intFrom:: DOUBLE PRECISION AND CommissionRuleDtl.intTo:: DOUBLE PRECISION    LIMIT 1;
            end if;
         end if;

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
         IF coalesce(v_decCommission,0) > 0 then
				
					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
            INSERT INTO tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT(numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo)
            SELECT
            T1.numUserCntID,
						T1.numReturnHeaderID,
						T1.tintReturnType,
						T1.numReturnItemID,
						CASE v_tintComType
            WHEN 1 --PERCENT
            THEN
               CASE T1.tintReturnType
               WHEN 3 THEN T1.monTotAmount*(v_decCommission/CAST(100 AS DOUBLE PRECISION))
               WHEN 4 THEN T1.monTotAmount*(v_decCommission/CAST(100 AS DOUBLE PRECISION))
               ELSE
                  CASE v_tintCommissionType
												--TOTAL AMOUNT PAID
                  WHEN 1 THEN T1.monTotAmount*(v_decCommission/CAST(100 AS DOUBLE PRECISION))
												--ITEM GROSS PROFIT (VENDOR COST)
                  WHEN 2 THEN(coalesce(T1.monTotAmount,0) -coalesce(T1.monVendorCost,0))*(v_decCommission/CAST(100 AS DOUBLE PRECISION))
												--ITEM GROSS PROFIT (AVERAGE COST)
                  WHEN 3 THEN(coalesce(T1.monTotAmount,0) -coalesce(T1.monAvgCost,0))*(v_decCommission/CAST(100 AS DOUBLE PRECISION))
                  END
               END
            ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
               0 -- COMMISSION CALCULATION AS BITTOM
            END,
						v_numComRuleID,
						v_tintComType,
						v_tintComBasedOn,
						v_decCommission,
						v_tintAssignTo
            FROM
            tt_TEMPITEM AS T1
            WHERE
            1 =(CASE T1.tintReturnType
            WHEN 3 THEN 1
            WHEN 4 THEN 1
            ELSE(CASE v_tintCommissionType 
										--TOTAL PAID INVOICE
               WHEN 1 THEN CASE WHEN coalesce(T1.monTotAmount,0) > 0 THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (VENDOR COST)
               WHEN 2 THEN CASE WHEN(coalesce(T1.monTotAmount,0) -coalesce(T1.monVendorCost,0)) > 0 THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (AVERAGE COST)
               WHEN 3 THEN CASE WHEN(coalesce(T1.monTotAmount,0) -coalesce(T1.monAvgCost,0)) > 0 THEN 1 ELSE 0 END
               ELSE
                  0
               END)
            END)
            AND(SELECT
               COUNT(*)
               FROM
               tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT
               WHERE
               tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT.numUserCntID = T1.numUserCntID
               AND tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT.numReturnHeaderID = T1.numReturnHeaderID
               AND tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT.tintReturnType = T1.tintReturnType
               AND tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT.numReturnItemID = T1.numReturnItemID) = 0;
         end if;
      end if;
      v_i := v_i::bigint+1;
      v_numComRuleID := 0;
      v_tintComBasedOn := 0;
      v_tintComAppliesTo := 0;
      v_tintComOrgType := 0;
      v_tintComType := 0;
      v_tintAssignTo := 0;
      v_decCommission := 0;
      v_numTotalAmount := 0;
      v_numTotalUnit := 0;
   END LOOP;

		-- UPDATE FLAT COMMMISSION AMOUNT 
   UPDATE
   tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT T1
   SET
   monCommission = T1.decCommission/(SELECT COUNT(*) FROM tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT Tinner WHERE Tinner.numUserCntID = T1.numUserCntID AND Tinner.tintComType = 2)
		
   WHERE
   T1.tintComType = 2;
	
   RETURN QUERY (SELECT * FROM tt_GETCOMMISSIONPAIDCREDITMEMOORREFUND_TEMPCOMMISSIONPAIDCREDIT);
END; $$;

