DROP FUNCTION IF EXISTS USP_ValidateItemAccountsForEmbeddedKits;

CREATE OR REPLACE FUNCTION USP_ValidateItemAccountsForEmbeddedKits(v_numKitId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPValidateItemAccountsForEmbeddedKits CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPValidateItemAccountsForEmbeddedKits AS
	WITH RECURSIVE CTE(numItemKitID,numItemCode,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,StageLevel,charItemType)
	AS
	(
	select CAST(0 AS NUMERIC(18,0)),numItemCode,numCOGsChartAcntId
	,numAssetChartAcntId,numIncomeChartAcntId,1,charItemType
	from item                                
	INNER join ItemDetails Dtl on numChildItemID=numItemCode
	where  numItemKitID=v_numKitId

	UNION ALL

	select Dtl.numItemKitID,i.numItemCode,i.numCOGsChartAcntId
	,i.numAssetChartAcntId,i.numIncomeChartAcntId,c.StageLevel + 1,i.charItemType
	from item i                               
	INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
	INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
	where Dtl.numChildItemID!=v_numKitId
	) SELECT * FROM CTE;

   IF EXISTS(SELECT * FROM tt_TEMPValidateItemAccountsForEmbeddedKits
   where 0 =(CASE WHEN charItemType = 'P' OR charItemType = 'A' THEN
      CASE WHEN numAssetChartAcntId > 0 AND numCOGsChartAcntId > 0 AND numIncomeChartAcntId > 0 THEN 1 ELSE 0 END
   WHEN charItemType = 'N' OR charItemType = 'S' THEN
      CASE WHEN numIncomeChartAcntId > 0 THEN 1 ELSE 0 END END)) then

      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;
                     
  




