DROP FUNCTION IF EXISTS GetUnitPriceAfterPriceLevelApplication;
CREATE OR REPLACE FUNCTION GetUnitPriceAfterPriceLevelApplication(v_numDomainID NUMERIC(18,0), 
	v_numItemCode NUMERIC(18,0), 
	v_numQuantity DOUBLE PRECISION, 
	v_numWarehouseItemID NUMERIC(18,0), 
	v_isPriceRule BOOLEAN, 
	v_numPriceRuleID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0))
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_finalUnitPrice  DOUBLE PRECISION DEFAULT 0;
   v_tintRuleType  INTEGER;
   v_tintVendorCostType SMALLINT;
   v_tintDiscountType  INTEGER;
   v_decDiscount  DOUBLE PRECISION;
   v_ItemPrice  DOUBLE PRECISION;
   v_monCalculatePrice  DECIMAL(20,5);
   v_tintPriceLevel  INTEGER DEFAULT 0;
   v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
BEGIN
   v_tintRuleType := 0;
   v_tintVendorCostType := 1;
   v_tintDiscountType := 0;
   v_decDiscount  := 0;
   v_ItemPrice := 0;

   DROP TABLE IF EXISTS tt_TEMPPRICE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPRICE
   (
      bitSuccess BOOLEAN,
      monPrice DECIMAL(20,5)
   );

   select   coalesce(tintPriceLevel,0) INTO v_tintPriceLevel FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;

   If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then

      INSERT INTO tt_TEMPPRICE(bitSuccess
		,monPrice)
      SELECT
      bitSuccess
		,monPrice
      FROM
      fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQuantity,v_numWarehouseItemID,
      (SELECT coalesce(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode)::SMALLINT,0,0,'',
      0,1::DOUBLE PRECISION);
      IF(SELECT bitSuccess FROM tt_TEMPPRICE) = true then
	
         SELECT monPrice INTO v_monCalculatePrice FROM tt_TEMPPRICE;
      end if;
   end if;

   IF v_isPriceRule = true then

      select   tintRuleType, tintVendorCostType, tintDiscountType, decDiscount INTO v_tintRuleType,v_tintVendorCostType,v_tintDiscountType,v_decDiscount FROM
      PricingTable WHERE
      numPriceRuleID = v_numPriceRuleID
      AND coalesce(numCurrencyID,0) = 0
      AND v_numQuantity BETWEEN intFromQty:: DOUBLE PRECISION AND intToQty:: DOUBLE PRECISION    LIMIT 1;
   ELSE
      IF coalesce(v_tintPriceLevel,0) > 0 then
	
         select   tintRuleType, tintVendorCostType, tintDiscountType, decDiscount INTO v_tintRuleType,v_tintVendorCostType,v_tintDiscountType,v_decDiscount FROM(SELECT
            ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
				tintRuleType,
				tintDiscountType,
				decDiscount
            FROM
            PricingTable
            WHERE
            PricingTable.numItemCode = v_numItemCode
            AND coalesce(numCurrencyID,0) = 0) TEMP WHERE
         Id = v_tintPriceLevel;
      ELSE
         select   tintRuleType, tintVendorCostType, tintDiscountType, decDiscount INTO v_tintRuleType,v_tintVendorCostType,v_tintDiscountType,v_decDiscount FROM
         PricingTable WHERE
         numItemCode = v_numItemCode
         AND coalesce(numCurrencyID,0) = 0
         AND (v_numQuantity BETWEEN intFromQty:: DOUBLE PRECISION AND intToQty:: DOUBLE PRECISION OR tintRuleType = 3)    LIMIT 1;
      end if;
   end if;

	

   IF v_tintRuleType > 0 AND v_tintDiscountType > 0 then

      IF v_tintRuleType = 1 then -- Deduct from List price
	
         IF(SELECT coalesce(charItemType,'') FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode) = 'P' then
            If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then -- Kit OR Assembly
               v_ItemPrice := v_monCalculatePrice;
            ELSE
               select   coalesce(monWListPrice,0) INTO v_ItemPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numWarehouseItemID;
            end if;
         ELSE
            select   coalesce(monListPrice,0) INTO v_ItemPrice FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
         end if;
         IF v_tintDiscountType = 1 then -- Percentage
		
            v_finalUnitPrice := v_ItemPrice -(v_ItemPrice*(v_decDiscount/100));
         ELSEIF v_tintDiscountType = 2
         then -- Flat Amount
		
            v_finalUnitPrice := v_ItemPrice -v_decDiscount;
         end if;
      ELSEIF v_tintRuleType = 2
      then -- Add to Primary Vendor Cost
	
         If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then -- Kit OR Assembly
            v_ItemPrice := v_monCalculatePrice;
         ELSE
            select coalesce(monCost,0) INTO v_ItemPrice FROM Vendor WHERE numItemCode = v_numItemCode AND numVendorID =(SELECT numVendorID FROM Item WHERE numItemCode = v_numItemCode);

			IF EXISTS (SELECT 
							VendorCostTable.numvendorcosttableid
						FROM 
							Item
						INNER JOIN
							Vendor 
						ON
							Item.numVendorID = Vendor.numVendorID
						INNER JOIN 
							VendorCostTable 
						ON 
							Vendor.numvendortcode=VendorCostTable.numvendortcode 
						WHERE
							Item.numItemCode = v_numItemCode
							AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
							AND COALESCE(v_numQuantity,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit)) THEN
				SELECT 
					COALESCE(VendorCostTable.monDynamicCost,v_ItemPrice)
					,COALESCE(VendorCostTable.monStaticCost,v_ItemPrice)
				INTO 
					v_monVendorDynamicCost
					,v_monVendorStaticCost
				FROM 
					Item
				INNER JOIN
					Vendor 
				ON
					Item.numVendorID = Vendor.numVendorID
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Item.numItemCode = v_numItemCode
					AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
					AND COALESCE(v_numQuantity,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit);
			ELSE
				v_monVendorDynamicCost := v_ItemPrice;
				v_monVendorStaticCost := v_ItemPrice;
			END IF;
         end if;
         IF v_tintDiscountType = 1 then -- Percentage
		
            v_finalUnitPrice := (CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)+((CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)*(v_decDiscount/100));
         ELSEIF v_tintDiscountType = 2
         then -- Flat Amount
		
            v_finalUnitPrice := (CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)+v_decDiscount;
         end if;
      ELSEIF v_tintRuleType = 3
      then -- Named Price
	
         v_finalUnitPrice := v_decDiscount;
      end if;
   end if;

   return v_finalUnitPrice;
END; $$;

