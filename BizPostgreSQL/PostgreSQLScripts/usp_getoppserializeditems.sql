-- Stored procedure definition script USP_GetOppSerializedItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOppSerializedItems(v_numOppID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numOppItemTCode as VARCHAR(255)),OpportunityItems.vcItemName || ' -- ' || vcWareHouse as vcItemName  from OpportunityItems
   join Item
   on Item.numItemCode = OpportunityItems.numItemCode
   join WareHouseItems WI
   on WI.numWareHouseItemID = OpportunityItems.numWarehouseItmsID
   join  Warehouses W
   on W.numWareHouseID = WI.numWareHouseID
   where numOppID = v_numOppID and (bitSerialized = true OR bitLotNo = true);
END; $$;












