-- Stored procedure definition script USP_ManageProjectCommission for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageProjectCommission(v_numDomainId NUMERIC,
      v_numProId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommissionType  SMALLINT;
   v_tintComAppliesTo  SMALLINT;
   v_monTotalGrossProfit  DECIMAL(20,5);
   v_monTotalExpense  DECIMAL(20,5);
   v_monTotalIncome  DECIMAL(20,5);
   v_dtCompletionDate  TIMESTAMP;
   v_numassignedto  NUMERIC;
   v_numrecowner  NUMERIC;
   v_numDivisionID  NUMERIC;
   v_bitCommContact  BOOLEAN;
   v_numUserCntID  NUMERIC;
   v_decCommission  DECIMAL(18,2);
   v_monCommission  DECIMAL(20,5);
   v_numComRuleID  NUMERIC;
   v_tintComBasedOn  SMALLINT;
   v_tinComDuration  SMALLINT;
   v_tintComType  SMALLINT;
   v_dFrom  TIMESTAMP;
   v_dTo  TIMESTAMP;
   v_monTotalProjectGrossProfit  DECIMAL(20,5);
   v_monTotalProjectIncome  DECIMAL(20,5);
   v_i  INTEGER;
   v_monProTotalGrossProfit  DECIMAL(20,5);
   v_monProTotalIncome  DECIMAL(20,5);
   v_intFromNextTier  DECIMAL(18,2);
							
   v_nexttier  VARCHAR(100); 

--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
BEGIN
   select   coalesce(tintCommissionType,1), coalesce(tintComAppliesTo,3) INTO v_tintCommissionType,v_tintComAppliesTo FROM Domain WHERE numDomainId = v_numDomainId;

--If Commission set for Project Gross Profit
   IF v_tintCommissionType = 3 then

	
	--If Project Status set as Completed (27492 : from ListDetails) 
      IF EXISTS(SELECT * FROM ProjectsMaster WHERE numdomainId = v_numDomainId AND numProId = v_numProId AND numProjectStatus = 27492) then
         v_monTotalGrossProfit := 0;
         v_monTotalExpense := 0;
         v_monTotalIncome := 0;
         v_dtCompletionDate := TIMEZONE('UTC',now());
		
--		SELECT @monProjectGrossProfit=ISNULL(monBalance,0) FROM dbo.GetProjectIncomeExpense(@numDomainId,@numProId)
		
         select   coalesce(monTotalGrossProfit,0), coalesce(monTotalExpense,0), coalesce(monTotalIncome,0), dtCompletionDate INTO v_monTotalGrossProfit,v_monTotalExpense,v_monTotalIncome,v_dtCompletionDate FROM ProjectsMaster WHERE numdomainId = v_numDomainId AND numProId = v_numProId;
		
		--If Project Gross Profit (+)
         IF v_monTotalGrossProfit > 0 then
		  
			
			--Get AssignTo and Record Owner of Oppertunity
            select   numAssignedTo, numRecOwner INTO v_numassignedto,v_numrecowner FROM ProjectsMaster WHERE numdomainId = v_numDomainId AND numProId = v_numProId;
            v_i := 1;
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
            WHILE v_i < 3 LOOP
               IF v_i = 1 then
                  v_numUserCntID := v_numassignedto;
               ELSEIF v_i = 2
               then
                  v_numUserCntID := v_numrecowner;
               end if;
               v_monProTotalGrossProfit := 0;
               v_monProTotalIncome := 0;
               select   coalesce(monProTotalGrossProfit,0), coalesce(monProTotalIncome,0) INTO v_monProTotalGrossProfit,v_monProTotalIncome FROM BizDocComission WHERE numDomainId = v_numDomainId AND numProId = v_numProId AND numUserCntID = v_numUserCntID;
               v_monProTotalGrossProfit := v_monTotalGrossProfit -v_monProTotalGrossProfit;
               v_monProTotalIncome := v_monTotalIncome -v_monProTotalIncome;
               IF v_monProTotalGrossProfit > 0 then
					
						--Check For Commission Contact	
                  IF EXISTS(SELECT UM.numUserId from UserMaster UM  join AdditionalContactsInformation A  on UM.numUserDetailId = A.numContactId
                  where UM.numDomainID = v_numDomainId and UM.numDomainID = A.numDomainID AND A.numContactId = v_numUserCntID) then
							
                     v_bitCommContact := false;
                     v_numDivisionID := 0;
                  ELSE
                     v_bitCommContact := true;
                     select   A.numDivisionId INTO v_numDivisionID from AdditionalContactsInformation A where A.numContactId = v_numUserCntID;
                  end if;  		
							
						--All Items	 tintComAppliesTo=3
                  IF EXISTS(SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
                  WHERE CR.tintComAppliesTo = 3 AND CR.numDomainID = v_numDomainId AND CR.tintAssignTo = v_i AND CRC.numValue =(CASE v_bitCommContact WHEN false THEN v_numUserCntID WHEN true THEN v_numDivisionID END) AND CRC.bitCommContact = v_bitCommContact) AND v_tintComAppliesTo = 3 then
							
                     select   CR.numComRuleID, CR.tintComBasedOn, CR.tinComDuration, CR.tintComType INTO v_numComRuleID,v_tintComBasedOn,v_tinComDuration,v_tintComType FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE CR.tintComAppliesTo = 3 AND CR.numDomainID = v_numDomainId
                     AND CR.tintAssignTo = v_i AND CRC.numValue =(CASE v_bitCommContact WHEN false THEN v_numUserCntID WHEN true THEN v_numDivisionID END) AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
                  end if;
                  IF v_numComRuleID > 0 then
						
							--Get From and To date for Commission Calculation
                     IF v_tinComDuration = 1 then --Month
                        v_dFrom := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval);
                        v_dTo := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE)))+1 || 'month' as interval)+INTERVAL '-1 day';
                     ELSEIF v_tinComDuration = 2
                     then --Quarter
                        v_dFrom := '1900-01-01':: date  + make_interval(months => DATEDIFF('month', '1900-01-01'::TIMESTAMP, now()::TIMESTAMP) / 3 * 3);
                        v_dTo := '1900-01-01':: date + make_interval(months => ((DATEDIFF('month', '1900-01-01'::TIMESTAMP, now()::TIMESTAMP) / 3) + 1) * 3) + make_interval(days => -1);
                     ELSEIF v_tinComDuration = 3
                     then --Year
                        v_dFrom := '1900-01-01':: date+CAST(extract(year from LOCALTIMESTAMP) -extract(year from 0:: date) || 'year' as interval);
                        v_dTo := '1900-01-01':: date+CAST(extract(year from LOCALTIMESTAMP) -extract(year from 0:: date)+1 || 'year' as interval)+INTERVAL '-1 day';
                     end if; 
							
							
							--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
                     select   coalesce(SUM(PM.monTotalGrossProfit),0), coalesce(SUM(PM.monTotalIncome),0) INTO v_monTotalProjectGrossProfit,v_monTotalProjectIncome FROM ProjectsMaster PM WHERE PM.numdomainId = v_numDomainId AND PM.numProId <> v_numProId AND PM.numProjectStatus = 27492
                     AND 1 =(CASE v_i WHEN 1 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN numAssignedTo = v_numUserCntID THEN 1 ELSE 0 END
                        WHEN true THEN CASE WHEN numAssignedTo IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numDivisionID) THEN 1 ELSE 0 END  END
                     WHEN 2 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN numRecOwner = v_numUserCntID THEN 1 ELSE 0 END
                        WHEN true THEN CASE WHEN numRecOwner IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numDivisionID) THEN 1 ELSE 0 END END END)
                     AND PM.dtCompletionDate BETWEEN v_dFrom AND v_dTo;
                     IF v_tintComBasedOn = 3 then --Project Gross Profit Amt
							
                        v_monTotalProjectGrossProfit := v_monTotalGrossProfit+v_monTotalProjectGrossProfit;
                     ELSEIF v_tintComBasedOn = 4
                     then --Project Total Income --
							
                        v_monTotalProjectGrossProfit :=((v_monTotalGrossProfit+v_monTotalProjectGrossProfit)/(v_monTotalIncome+v_monTotalProjectIncome))*100;
                     end if;
                     select   decCommission, intTo+1 INTO v_decCommission,v_intFromNextTier FROM CommissionRuleDtl WHERE numComRuleID = v_numComRuleID AND v_monTotalProjectGrossProfit BETWEEN intFrom:: DECIMAL(20,5) AND intTo:: DECIMAL(20,5)    LIMIT 1;
                     select   SUBSTR(CAST(numComRuleDtlID AS VARCHAR(10)),1,10)  || ',' || SUBSTR(CAST(numComRuleID AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(intFrom AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(intTo AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(decCommission AS VARCHAR(10)),1,10) INTO v_nexttier FROM CommissionRuleDtl WHERE numComRuleID = v_numComRuleID AND intFrom >= v_intFromNextTier   ORDER BY intFrom LIMIT 1;
                     IF v_tintComBasedOn = 3 then --Project Gross Profit Amt
							
                        v_monCommission := v_monProTotalGrossProfit*v_decCommission/100;
                     ELSEIF v_tintComBasedOn = 4
                     then --Project Total Income --
							
                        v_monCommission := v_monProTotalIncome*v_decCommission/100;
                     end if;
                     if v_monCommission > 0 then
							
	--							IF NOT exists (SELECT * FROM [BizDocComission] WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntID AND numProId=@numProId)
	--							BEGIN
                        INSERT INTO BizDocComission(numDomainId,numUserCntID,numProId,numComissionAmount,
											numComRuleID,tintComType,tintComBasedOn,decCommission,vcnexttier,dtProCompletionDate,monProTotalExpense,monProTotalIncome,monProTotalGrossProfit)
											values(v_numDomainId,v_numUserCntID,v_numProId,v_monCommission,
												   v_numComRuleID,v_tintComType,v_tintComBasedOn,v_decCommission,v_nexttier,v_dtCompletionDate,v_monTotalExpense,v_monTotalIncome,v_monTotalGrossProfit);
                     end if;
                  end if;
               end if;
               v_numComRuleID := 0;
               v_tintComBasedOn := 0;
               v_tinComDuration := 0;
               v_tintComType := 0;
               v_monCommission := 0;
               v_bitCommContact := false;
               v_numDivisionID := 0;
               v_monTotalProjectGrossProfit := 0;
               v_monTotalProjectIncome := 0;
               v_i := v_i::bigint+1;
            END LOOP;
         end if;
      end if;
   end if;
   RETURN;
END; $$;



