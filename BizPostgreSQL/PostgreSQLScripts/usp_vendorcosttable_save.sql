DROP FUNCTION IF EXISTS usp_vendorcosttable_save;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_save
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numvendortcode NUMERIC(18,0)
	,p_numvendorid NUMERIC(18,0)
	,p_numitemcode NUMERIC(18,0)
	,p_numcurrencyid NUMERIC(18,0)
	,p_vcCostRange JSONB
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	IF p_numvendortcode = -1 THEN
		SELECT numVendorTCode INTO p_numvendortcode FROM Vendor V WHERE V.numDomainID = p_numdomainid AND V.numVendorID = p_numvendorid AND V.numItemCode = p_numitemcode;
	END IF;

	IF EXISTS (SELECT 
					numvendortcode
				FROM 
					Vendor 
				WHERE
					numvendortcode = p_numvendortcode
					AND numDomainID = p_numdomainid
					AND numVendorID = p_numvendorid
					AND numItemCode = p_numitemcode) THEN

		IF p_vcCostRange IS NOT NULL THEN
			UPDATE
				VendorCostTable
			SET
				intFromQty=(costRange->'intFromQty')::INT,
				intToQty=(costRange->'intToQty')::INT,
				monStaticCost=(costRange->'monStaticCost')::DECIMAL,
				monDynamicCost=(costRange->'monDynamicCost')::DECIMAL,
				numStaticCostModifiedBy=(CASE WHEN COALESCE(monStaticCost,0) <> (costRange->'monStaticCost')::DECIMAL THEN p_numusercntid ELSE numStaticCostModifiedBy END),
				dtStaticCostModifiedDate=(CASE WHEN COALESCE(monStaticCost,0) <> (costRange->'monStaticCost')::DECIMAL THEN timezone('utc', now()) ELSE dtStaticCostModifiedDate END),
				numDynamicCostModifiedBy=(CASE WHEN COALESCE(monDynamicCost,0) <> (costRange->'monDynamicCost')::DECIMAL THEN p_numusercntid ELSE numDynamicCostModifiedBy END),
				dtDynamicCostModifiedDate=(CASE WHEN COALESCE(monStaticCost,0) <> (costRange->'monDynamicCost')::DECIMAL THEN timezone('utc', now()) ELSE dtDynamicCostModifiedDate END)
			FROM 
				jsonb_array_elements(p_vcCostRange) costRange
			WHERE
				VendorCostTable.numVendorTcode = p_numvendortcode
				AND VendorCostTable.numCurrencyID = p_numcurrencyid
				AND VendorCostTable.intRow = (costRange->'intRow')::INT;

			INSERT INTO VendorCostTable
			(
				numVendorTcode,
				numCurrencyID,
				intRow,
				intFromQty,
				intToQty,
				monStaticCost,
				monDynamicCost,
				numStaticCostModifiedBy,
				dtStaticCostModifiedDate,
				numDynamicCostModifiedBy,
				dtDynamicCostModifiedDate
			)
			SELECT
				p_numvendortcode
				,p_numcurrencyid
				,(costRange->'intRow')::INT
				,(costRange->'intFromQty')::INT
				,(costRange->'intToQty')::INT
				,(costRange->'monStaticCost')::DECIMAL
				,(costRange->'monDynamicCost')::DECIMAL
				,p_numusercntid
				,timezone('utc', now())
				,p_numusercntid
				,timezone('utc', now())
			FROM
				jsonb_array_elements(p_vcCostRange) costRange
			WHERE
				(costRange->'intRow')::INT NOT IN (SELECT intRow FROM VendorCostTable WHERE numVendorTcode = p_numvendortcode AND VendorCostTable.numCurrencyID = p_numcurrencyid)
			ORDER BY 
				(costRange->'intRow')::INT;
		END IF;
	END IF;
END; $$;