-- Stored procedure definition script USP_ReportListMaster_EmployeeSalesPerformancePanel1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ReportListMaster_EmployeeSalesPerformancePanel1(
	--,@vcRunDisplay VARCHAR(MAX) -- added by hitesh 
v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,v_vcFilterValue TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP; --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))  
   v_dtEndDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval); 
 -------
   v_TotalSalesReturn  DOUBLE PRECISION DEFAULT 0.0;
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM Domain WHERE numDomainId = v_numDomainID;
	---alter sp
 
	----alterd sp-----------

   IF v_vcTimeLine = 'Last12Months' then
 
      v_dtStartDate :=   '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) -12 || 'month' as interval);
   ELSEIF v_vcTimeLine = 'Last6Months'
   then
 
      v_dtStartDate :=   '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) -6 || 'month' as interval);
   ELSEIF v_vcTimeLine = 'Last3Months'
   then

      v_dtStartDate :=   '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) -3 || 'month' as interval);
   ELSEIF v_vcTimeLine = 'Last30Days'
   then
      v_dtStartDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -30), v_dtEndDate));
   ELSEIF v_vcTimeLine = 'Last7Days'
   then
		v_dtStartDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -7), v_dtEndDate));
   ELSEIF v_vcTimeLine = 'Today'
   then

      v_dtStartDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Yesterday'
   then

      v_dtStartDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 day';
   ELSE
      v_dtStartDate := CAST(SUBSTR(v_vcTimeLine,1,POSITION('-' IN v_vcTimeLine) -1) AS TIMESTAMP);
      v_dtEndDate := CAST(SUBSTR(v_vcTimeLine,length(v_vcTimeLine) -(POSITION('-' IN v_vcTimeLine) -1)+1) AS TIMESTAMP);
   end if;
  
	-----------------------------------
	  
	 ------
   open SWV_RefCur for
   SELECT
   UserMaster.numUserDetailId,CAst(v_dtStartDate as DATE) as StartDate,CAST(v_dtEndDate  as DATE) as EndDate,
		UserMaster.vcUserName,v_vcFilterBy as vcFilterBy
		,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numassignedto = TEMP1.numAssignedTo AND tintopptype = 1 AND tintoppstatus = 1 AND bintOppToOrder IS NOT NULL)*100.0/(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numassignedto = TEMP1.numAssignedTo AND tintopptype = 1 AND ((tintoppstatus = 1 AND bintOppToOrder IS NOT NULL) OR tintoppstatus = 2))*1.0 AS WonPercent
   FROM(SELECT DISTINCT
			--numAssignedTo
      CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN numassignedto
		   --else numRecOwner
      WHEN 2
      THEN numrecowner
      else  numassignedto
      End as numAssignedTo
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND ((tintoppstatus = 1 AND bintOppToOrder IS NOT NULL) OR tintoppstatus = 2)
		--	--alter sp
      AND (bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      AND
      1 =(CASE
      WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 AND v_vcFilterBy IN(1,2,3)
      THEN(CASE v_vcFilterBy
         WHEN 1  -- Assign To  
         THEN(CASE WHEN numassignedto IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 2  -- Record Owner  
         THEN(CASE WHEN numrecowner IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
         THEN(CASE WHEN numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT Id FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
         END)
      ELSE 1
      END)) TEMP1
   INNER JOIN
   UserMaster
   ON
   TEMP1.numAssignedTo = UserMaster.numUserDetailId
   WHERE
   UserMaster.numDomainID = v_numDomainID;
		

--#2
   open SWV_RefCur2 for
   SELECT
   UserMaster.numUserDetailId,
		v_vcFilterBy as vcFilterBy,CAst(v_dtStartDate as DATE) as StartDate,CAST(v_dtEndDate  as DATE) as EndDate
		,UserMaster.vcUserName
		,Temp2.Profit
   FROM(SELECT DISTINCT
      CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN numassignedto
		   --else numRecOwner
      WHEN 2
      THEN numrecowner
      else  numassignedto
      End as numAssignedTo
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND (bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      AND
      1 =(CASE
      WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 AND v_vcFilterBy IN(1,2,3)
      THEN(CASE v_vcFilterBy
         WHEN 1  -- Assign To  
         THEN(CASE WHEN numassignedto IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 2  -- Record Owner  
         THEN(CASE WHEN numrecowner IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
         THEN(CASE WHEN numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT Id FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
         END)
      ELSE 1
      END)) TEMP1
   INNER JOIN
   UserMaster
   ON
   TEMP1.numAssignedTo = UserMaster.numUserDetailId
   AND UserMaster.numDomainID = v_numDomainID
   CROSS JOIN LATERAL(SELECT
      SUM(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT)) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numassignedto = TEMP1.numAssignedTo
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate OR OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)) TEMP2
   ORDER BY
   TEMP2.Profit DESC;
--#3
   open SWV_RefCur3 for
   SELECT
   UserMaster.numUserDetailId
		,UserMaster.vcUserName,v_vcFilterBy as vcFilterBy,CAst(v_dtStartDate as DATE) as StartDate,CAST(v_dtEndDate  as DATE) as EndDate
		,AVG(ProfitPercentByOrder) AS AvgGrossProfitMargin
   FROM(SELECT DISTINCT
			--numAssignedTo
      CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN numassignedto
		   --else numRecOwner
      WHEN 2
      THEN numrecowner
      else  numassignedto
      End as numAssignedTo
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND (bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      AND
      1 =(CASE
      WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 AND v_vcFilterBy IN(1,2,3)
      THEN(CASE v_vcFilterBy
         WHEN 1  -- Assign To  
         THEN(CASE WHEN numassignedto IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 2  -- Record Owner  
         THEN(CASE WHEN numrecowner IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
         THEN(CASE WHEN numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT Id FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
         END)
      ELSE 1
      END)) TEMP1
   INNER JOIN
   UserMaster
   ON
   TEMP1.numAssignedTo = UserMaster.numUserDetailId
   AND UserMaster.numDomainID = v_numDomainID
   CROSS JOIN LATERAL(SELECT
      OM.numOppId
			,(SUM(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT))/SUM(coalesce(monTotAmount,0)))*100.0 AS ProfitPercentByOrder
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numassignedto = TEMP1.numAssignedTo
      AND coalesce(monTotAmount,0) <> 0
      AND coalesce(numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      GROUP BY
      OM.numOppId) TEMP2
   GROUP BY
   UserMaster.vcUserName,UserMaster.numUserDetailId
   HAVING
   AVG(ProfitPercentByOrder) > 0
   ORDER BY
   AVG(ProfitPercentByOrder) DESC;


--#4---Gross revenue
   open SWV_RefCur4 for
   SELECT
   UserMaster.numUserDetailId,UserMaster.vcUserName,Temp3.InvoiceTotal,v_vcFilterBy as vcFilterBy,CAst(v_dtStartDate as DATE) as StartDate,CAST(v_dtEndDate  as DATE) as EndDate
   FROM(SELECT DISTINCT
			--numAssignedTo
      CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN numassignedto
		   --else numRecOwner
      WHEN 2
      THEN numrecowner
      else  numassignedto
      End as numAssignedTo
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND (bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      AND
      1 =(CASE
      WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 AND v_vcFilterBy IN(1,2,3)
      THEN(CASE v_vcFilterBy
         WHEN 1  -- Assign To  
         THEN(CASE WHEN numassignedto IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 2  -- Record Owner  
         THEN(CASE WHEN numrecowner IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
         WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
         THEN(CASE WHEN numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT Id FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
         END)
      ELSE 1
      END)) TEMP1
   INNER JOIN
   UserMaster
   ON
   TEMP1.numAssignedTo = UserMaster.numUserDetailId
   AND UserMaster.numDomainID = v_numDomainID
   CROSS JOIN LATERAL(SELECT
      Sum(coalesce(OBD.monDealAmount,0))
      AS InvoiceTotal
      FROM
		
		--	OpportunityItems OI
		--INNER JOIN
		
      OpportunityMaster OM
		--ON
		--	OI.numOppId = OM.numOppID
      Inner join
      OpportunityBizDocs OBD
      ON
      OBD.numoppid = OM.numOppId and OBD.bitAuthoritativeBizDocs = 1
		--INNER JOIN
		--	Item I
		--ON
		--	OI.numItemCode = I.numItemCode
		--Left JOIN 
		--	Vendor V 
		--ON 
		--	V.numVendorID=I.numVendorID 
		--	AND V.numItemCode=I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      and(CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN OM.numassignedto
      WHEN 2
      THEN OM.numrecowner
      else  OM.numassignedto
      End)  = TEMP1.numAssignedTo
			--AND OM.numRecOwner = TEMP1.numAssignedTo
			--AND ISNULL(OI.monTotAmount,0) <> 0
			--AND ISNULL(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OBD.numBizDocId = 287
			
			--AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID) /*if we keep this condition then sales order invoice total will not match. also it was producing duplicates.*/
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      OR
      OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)) TEMP3
   ORDER BY
   TEMP3.InvoiceTotal DESC;
   RETURN;
		-----
END; $$;


