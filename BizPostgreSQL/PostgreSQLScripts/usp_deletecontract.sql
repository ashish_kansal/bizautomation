-- Stored procedure definition script usp_DeleteContract for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteContract(v_numContractId BIGINT  ,
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ContractsContact WHERE numContactId = v_numContractId AND numDomainId = v_numDomainID;
   delete FROM ContractManagement where numContractId = v_numContractId and numdomainId = v_numDomainID;
   RETURN;
END; $$;


