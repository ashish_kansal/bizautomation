-- Function definition script GetIncidents for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetIncidents(v_numcontractid NUMERIC,v_numDomainId NUMERIC,v_numdivisionid NUMERIC)
RETURNS NUMERIC LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retvalue1  INTEGER;
BEGIN
   v_retvalue1 := 0;       
    
--select @retvalue1=isnull(count(*),0) from TimeAndExpense      
--where tintTeType in( 2,3)    
--and numType = 1    
--and numcontractId = @numcontractid    
--and numdomainId = @numDomainId    

   select   coalesce(count(*),0) INTO v_retvalue1 from Cases where numContractID = v_numcontractid and numDomainID = v_numDomainId;
   select   v_retvalue1::bigint+coalesce(count(*),0) INTO v_retvalue1 from ProjectsMaster where numcontractId = v_numcontractid and numdomainId = v_numDomainId;
          
   return v_retvalue1;
END; $$;

