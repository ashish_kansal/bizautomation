-- Stored procedure definition script USP_AddVendor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddVendor(v_numDivisionId NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_numItemCode NUMERIC(9,0) DEFAULT 0,
v_bitIsPrimaryVendor BOOLEAN DEFAULT false,
v_vcPartNo VARCHAR(100) DEFAULT '',
v_intMinQty INTEGER DEFAULT 0,
v_monCost DECIMAL(20,5) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM Vendor WHERE numVendorID = v_numDivisionId AND numItemCode = v_numItemCode) then
	
      INSERT INTO Vendor(numVendorID
			,vcPartNo
			,numDomainID
			,monCost
			,numItemCode
			,intMinQty)
		VALUES(v_numDivisionId
			,v_vcPartNo
			,v_numDomainID
			,v_monCost
			,v_numItemCode
			,v_intMinQty);
   end if;

   IF v_bitIsPrimaryVendor = true OR(SELECT COUNT(*) FROM Vendor WHERE numItemCode = v_numItemCode) = 1 then
	
      UPDATE Item SET numVendorID = v_numDivisionId WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


