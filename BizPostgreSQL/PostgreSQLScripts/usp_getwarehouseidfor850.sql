-- Stored procedure definition script USP_GetWarehouseIdFor850 for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWarehouseIdFor850(v_numDomainID NUMERIC(9,0) DEFAULT 0,        
v_numWareHouseID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numWareHouseID as VARCHAR(255))
   FROM Warehouses W
   LEFT JOIN
   AddressDetails AD
   ON
   W.numAddressID = AD.numAddressID
   WHERE
   W.numDomainID = v_numDomainID
   AND (numWareHouseID = v_numWareHouseID  OR coalesce(v_numWareHouseID,0) = 0) LIMIT 1;
END; $$;












