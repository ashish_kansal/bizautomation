-- Stored procedure definition script USP_GetAcivitybyGoogleEventId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAcivitybyGoogleEventId(v_GoogleEventId TEXT,
 v_ResourceID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Activity.AllDayEvent,
  Activity.ActivityDescription,
  Activity.Duration,
  Activity.Location,
  Activity.ActivityID,
  Activity.StartDateTimeUtc,
  cast(Activity.Subject as VARCHAR(255)),
  Activity.EnableReminder,
  Activity.ReminderInterval,
  Activity.ShowTimeAs,
  Activity.Importance,
  Activity.Status,
  Activity.RecurrenceID,
  Activity.VarianceID,
ActivityResource.ResourceID,
Activity.itemId,
Activity.ChangeKey,
Activity.ItemIDOccur,
  Activity._ts ,  Activity.GoogleEventId
   FROM
   Activity INNER JOIN ActivityResource ON Activity.ActivityID = ActivityResource.ActivityID
   WHERE
   coalesce(Activity.GoogleEventId,'') = v_GoogleEventId AND coalesce(ResourceID,0) = v_ResourceID;
END; $$;   












