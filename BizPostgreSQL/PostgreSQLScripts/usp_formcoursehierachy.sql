-- Stored procedure definition script USP_FormCourseHierachy for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_FormCourseHierachy()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(9,0);  
   v_numLevelID  NUMERIC;  
   v_Level1  VARCHAR(255);  
   v_Level2  VARCHAR(255);  
   v_Level3  VARCHAR(255);  
   v_Level4  VARCHAR(255);  
   v_Level5  VARCHAR(255);  
  
   v_CatID1  NUMERIC;  
   v_CatID2  NUMERIC;  
   v_CatID3  NUMERIC;  
   v_CatID4  NUMERIC;  
   v_CatID5  NUMERIC;
   SWV_RowCount INTEGER;
BEGIN
   v_CatID1 := 0;  
   v_CatID2 := 0;  
   v_CatID3 := 0;  
   v_CatID4 := 0;  
   v_CatID5 := 0;  
  
  
   v_numLevelID := 0;  
   v_numDomainID := 104;  
   select   LevelID, Level1, Level2, Level3, Level4, Level5 INTO v_numLevelID,v_Level1,v_Level2,v_Level3,v_Level4,v_Level5 from CourseLevel     LIMIT 1;  
   while v_numLevelID > 0 LOOP
      If v_Level1 is not null then

         if not exists(select numCategoryID from Category where numDomainID = v_numDomainID and vcCategoryName = v_Level1 and tintLevel = 2) then
 
            insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)
  values(v_Level1,2, 65, false, null, v_numDomainID);
  
            v_CatID1 := CURRVAL('Category_seq');
         else
            SELECT numCategoryID INTO v_CatID1 FROM Category  where numDomainID = v_numDomainID and vcCategoryName = v_Level1 and tintLevel = 2 LIMIT 1;
            update Category SET numDepCategory = 65  where numDomainID = v_numDomainID and vcCategoryName = v_Level1 and tintLevel = 2;
         end if;
      end if;
      If v_Level2 is not null then

         if not exists(select numCategoryID from Category where numDomainID = v_numDomainID and vcCategoryName = v_Level2 and tintLevel = 3) then
 
            insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)
  values(v_Level2,3, v_CatID1, false, null, v_numDomainID);
  
            v_CatID2 := CURRVAL('Category_seq');
         else
            SELECT numCategoryID INTO v_CatID2 FROM Category  where numDomainID = v_numDomainID and vcCategoryName = v_Level2 and tintLevel = 3 LIMIT 1;
            update Category SET numDepCategory = v_CatID1  where numDomainID = v_numDomainID and vcCategoryName = v_Level2 and tintLevel = 3;
         end if;
      end if;
      If v_Level3 is not null then

         if not exists(select numCategoryID from Category where numDomainID = v_numDomainID and vcCategoryName = v_Level3 and tintLevel = 4) then
 
            insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)
  values(v_Level3,4, v_CatID2, false, null, v_numDomainID);
  
            v_CatID3 := CURRVAL('Category_seq');
            RAISE NOTICE '%',v_Level3;
            RAISE NOTICE '%',v_CatID3;
         else
            SELECT numCategoryID INTO v_CatID3 FROM Category  where numDomainID = v_numDomainID and vcCategoryName = v_Level3 and tintLevel = 4 LIMIT 1;
            update Category SET numDepCategory = v_CatID2  where numDomainID = v_numDomainID and vcCategoryName = v_Level3 and tintLevel = 4;
         end if;
      end if;
      If v_Level4 is not null then

         if not exists(select numCategoryID from Category where numDomainID = v_numDomainID and vcCategoryName = v_Level4 and tintLevel = 5) then
 
            insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)
  values(v_Level4,5, v_CatID3, false, null, v_numDomainID);
  
            v_CatID4 := CURRVAL('Category_seq');
         else
            SELECT numCategoryID INTO v_CatID4 FROM Category  where numDomainID = v_numDomainID and vcCategoryName = v_Level4  and tintLevel = 5 LIMIT 1;
            update Category SET numDepCategory = v_CatID3  where numDomainID = v_numDomainID and vcCategoryName = v_Level4  and tintLevel = 5;
         end if;
      end if;
      If v_Level5 is not null then

         if not exists(select numCategoryID from Category where numDomainID = v_numDomainID and vcCategoryName = v_Level5 and tintLevel = 6) then
 
            insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)
  values(v_Level5,6, v_CatID4, false, null, v_numDomainID);
         else
            update Category set numDepCategory = v_CatID4  where numDomainID = v_numDomainID and vcCategoryName = v_Level5  and tintLevel = 6;
         end if;
      end if;
      v_CatID1 := 0;
      v_CatID2 := 0;
      v_CatID3 := 0;
      v_CatID4 := 0;
      v_CatID5 := 0;
      select   LevelID, Level1, Level2, Level3, Level4, Level5 INTO v_numLevelID,v_Level1,v_Level2,v_Level3,v_Level4,v_Level5 from CourseLevel where LevelID > v_numLevelID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numLevelID := 0;
      end if;
   END LOOP;
   RETURN;
END; $$;



