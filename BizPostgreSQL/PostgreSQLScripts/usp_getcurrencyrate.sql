-- Stored procedure definition script USP_GetCurrencyRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCurrencyRate(v_numDomanID NUMERIC(9,0),
v_bitAll BOOLEAN,
v_numCurrencyID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainId = v_numDomanID) then
      INSERT INTO Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol
      ,numDomainId,fltExchangeRate,bitEnabled)
      SELECT vcCurrencyDesc, chrCurrency,varCurrSymbol, v_numDomanID, fltExchangeRate, bitEnabled
      FROM Currency WHERE numDomainId = 0;
   end if;


   IF v_numCurrencyID > 0 then

      open SWV_RefCur for
      SELECT  numCurrencyID ,
            vcCurrencyDesc ,
            chrCurrency ,
            varCurrSymbol ,
            fltExchangeRate ,
            bitEnabled ,
            numCountryId
      FROM    Currency
      WHERE   numDomainId = v_numDomanID
      AND numCurrencyID = v_numCurrencyID;
      RETURN;
   end if;


   IF v_bitAll = true then
      open SWV_RefCur for
      SELECT numCurrencyID,vcCurrencyDesc,chrCurrency,varCurrSymbol
		  ,numDomainId,fltExchangeRate,bitEnabled,coalesce(numCountryId,0) AS numCountryId
      FROM Currency WHERE numDomainId = v_numDomanID
      ORDER BY bitEnabled DESC,vcCurrencyDesc ASC;
   ELSE
      open SWV_RefCur for
      SELECT numCurrencyID,vcCurrencyDesc,chrCurrency,varCurrSymbol
		  ,numDomainId,fltExchangeRate,bitEnabled,coalesce(numCountryId,0) AS numCountryId
      FROM Currency WHERE numDomainId = v_numDomanID AND bitEnabled = true
      ORDER BY bitEnabled DESC,vcCurrencyDesc ASC;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerCreditCardInfo]    Script Date: 05/07/2009 22:00:29 ******/
END; $$;


