-- Stored procedure definition script USP_ManageRuleMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRuleMaster(v_numRuleID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintModuleType SMALLINT DEFAULT NULL, 
    v_vcRuleName VARCHAR(100) DEFAULT NULL,
    v_vcRuleDescription VARCHAR(500) DEFAULT NULL,
    v_tintBasedOn SMALLINT DEFAULT NULL,
    v_tintStatus SMALLINT DEFAULT NULL,
	v_numFormFieldIdChange NUMERIC(9,0) DEFAULT 0,
	v_byteMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 1 then

      Delete from ruleAction  WHERE   numRuleID = v_numRuleID and numDomainID = v_numDomainID;
      Delete from RuleCondition  WHERE   numRuleID = v_numRuleID and numDomainID = v_numDomainID;
      Delete from RuleMaster  WHERE   numRuleID = v_numRuleID and numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT  v_numRuleID;
   ELSE
      IF v_numRuleID = 0 then
        
         INSERT  INTO RuleMaster(numDomainID,tintModuleType,vcRuleName,vcRuleDescription,tintBasedOn,tintStatus,bintCreatedDate,numFormFieldIdChange)
            VALUES(v_numDomainID,v_tintModuleType,v_vcRuleName,v_vcRuleDescription,v_tintBasedOn,v_tintStatus,TIMEZONE('UTC',now()),v_numFormFieldIdChange);
            
         open SWV_RefCur for
         SELECT  CURRVAL('RuleMaster_seq');
      ELSE
         UPDATE  RuleMaster
         SET     tintModuleType = v_tintModuleType,vcRuleName = v_vcRuleName,vcRuleDescription = v_vcRuleDescription,
         tintBasedOn = v_tintBasedOn,tintStatus = v_tintStatus,
         bintModifiedDate = TIMEZONE('UTC',now()),numFormFieldIdChange = v_numFormFieldIdChange
         WHERE   numRuleID = v_numRuleID and numDomainID = v_numDomainID;
         open SWV_RefCur for
         SELECT  v_numRuleID;
      end if;
   end if;
   RETURN;
END; $$;


