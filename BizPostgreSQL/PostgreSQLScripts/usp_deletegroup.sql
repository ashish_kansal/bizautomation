CREATE OR REPLACE FUNCTION usp_DeleteGroup(  
v_numGrpID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivCount  NUMERIC;
BEGIN
   select   Count(*) INTO v_numDivCount FROM DivisionMaster WHERE numGrpId = v_numGrpID;

   IF v_numDivCount = 0 then
		
      DELETE FROM Groups WHERE numGrpId = v_numGrpID;
      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


