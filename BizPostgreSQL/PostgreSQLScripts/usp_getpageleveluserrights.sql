-- Stored procedure definition script usp_GetPageLevelUserRights for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetPageLevelUserRights(             
--          
v_numUserID NUMERIC,          
 --@vcFileName varchar (100),          
 v_numModuleID NUMERIC DEFAULT 0,          
 v_numPageID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(9,0);  
   v_numAdminID  NUMERIC(9,0);  
   v_numUserDetailId  NUMERIC(9,0);
BEGIN
   select   numDomainID, numUserDetailId INTO v_numDomainID,v_numUserDetailId from UserMaster where numUserId = v_numUserID;  
   select   numAdminID INTO v_numAdminID from Domain where numDomainId = v_numDomainID;  

   if v_numAdminID = v_numUserDetailId then
      open SWV_RefCur for
      SELECT  UM.numUserId, UM.vcUserName,
-- @vcFileName as vcFileName,        
   v_numModuleID as numModuleID, v_numPageID as numPageID, 3 As intExportAllowed,
   3 As intPrintAllowed, 3 As intViewAllowed,
  3 As intAddAllowed, 3 As intUpdateAllowed,
   3 As intDeleteAllowed
      FROM UserMaster UM
      WHERE UM.numUserId = v_numUserID LIMIT 1;
   else
      open SWV_RefCur for
      SELECT  UM.numUserId, UM.vcUserName, PM.vcFileName,
   GA.numModuleID, GA.numPageID, MAX(GA.intExportAllowed) As intExportAllowed,
   MAX(GA.intPrintAllowed) As intPrintAllowed, MAX(GA.intViewAllowed) As intViewAllowed,
   MAX(GA.intAddAllowed) As intAddAllowed, MAX(GA.intUpdateAllowed) As intUpdateAllowed,
   MAX(GA.intDeleteAllowed) As intDeleteAllowed
      FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID LEFT JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
      WHERE
      GA.numModuleID = v_numModuleID
      AND GA.numPageID = v_numPageID
      AND UM.numUserId = v_numUserID
      GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID LIMIT 1;
   end if;
   RETURN;
END; $$;


