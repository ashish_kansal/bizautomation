-- Stored procedure definition script USP_EmailHistory_Archive for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistory_Archive(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_numEmailHstrID TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmailArchiveNodeID  NUMERIC(18,0);
   v_strSQL  TEXT;
BEGIN
   IF v_numEmailHstrID = 'All' then
	
		--Moves all emails which are more than 180 days old to Email Archive folder
      UPDATE EmailHistory t1
      SET numNodeId = coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntId AND coalesce(numFixID,0) = 2 AND coalesce(bitSystem,false) = true),0),bitArchived = true
		
      WHERE
      CAST(t1.dtReceivedOn AS DATE) < CAST(LOCALTIMESTAMP+INTERVAL '-180 day' AS DATE) AND
      numNodeId =(SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntId AND bitSystem = true AND numFixID = 1) AND
      coalesce(t1.bitArchived,false) = false;
   ELSE
      select   coalesce(numNodeID,0) INTO v_numEmailArchiveNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 2 AND coalesce(bitSystem,false) = true;
      v_strSQL := CONCAT('UPDATE EmailHistory SET bitArchived = true,numNodeId = ',COALESCE(v_numEmailArchiveNodeID,0),' WHERE numEmailHstrID IN (',coalesce(v_numEmailHstrID,''),')');
      EXECUTE v_strSQL;
   end if;
   RETURN;
END; $$;







