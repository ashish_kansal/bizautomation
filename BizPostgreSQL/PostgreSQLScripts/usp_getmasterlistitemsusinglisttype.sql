-- Stored procedure definition script USP_GetMasterListItemsUsingListType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMasterListItemsUsingListType(v_ListID NUMERIC(9,0) DEFAULT 0,        
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numListType NUMERIC(9,0) DEFAULT NULL,
v_tintOppOrOrder SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitEDI  BOOLEAN;
   v_bit3PL  BOOLEAN;
BEGIN
   IF v_ListID = 176 then
      select   coalesce(bitEDI,false), coalesce(bit3PL,false) INTO v_bitEDI,v_bit3PL FROM Domain WHERE numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT Ld.numListItemID AS "numListItemID", coalesce(vcRenamedListName,vcData) as "vcData",coalesce(Ld.numListType,0) as "numListType" FROM Listdetails Ld
      left join listorder LO
      on Ld.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = v_ListID and (constFlag = true or Ld.numDomainid = v_numDomainID)
      and coalesce(Ld.numListType,0) in(0,v_numListType) AND coalesce(Ld.tintOppOrOrder,0) IN(0,v_tintOppOrOrder)
      AND 1 =(CASE WHEN Ld.numListItemID IN(15445,15446) THEN(CASE WHEN v_bit3PL = true THEN 1 ELSE 0 END) WHEN Ld.numListItemID IN(15447,15448) THEN(CASE WHEN v_bitEDI = true THEN 1 ELSE 0 END) ELSE 1 END)
      order by coalesce(intSortOrder,Ld.sintOrder);
   ELSE
      open SWV_RefCur for
      SELECT Ld.numListItemID AS "numListItemID", coalesce(vcRenamedListName,vcData) as "vcData",coalesce(Ld.numListType,0) as "numListType" FROM Listdetails Ld
      left join listorder LO
      on Ld.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = v_ListID and (constFlag = true or Ld.numDomainid = v_numDomainID)
      and coalesce(Ld.numListType,0) in(0,v_numListType) AND coalesce(Ld.tintOppOrOrder,0) IN(0,v_tintOppOrOrder)
      order by coalesce(intSortOrder,Ld.sintOrder);
   end if;
   RETURN;
END; $$;


