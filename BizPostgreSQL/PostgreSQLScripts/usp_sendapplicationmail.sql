-- Stored procedure definition script USP_SendApplicationMail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SendApplicationMail(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainId  NUMERIC(9,0);              
   v_numContactID  VARCHAR(15);              
   v_numEmailTemplate  VARCHAR(15);              
   v_tintCCManager  VARCHAR(15);              
   v_numDaysAfterDue INTEGER;              
   v_numOppStageID  VARCHAR(15);              
   v_numNOppStageID  VARCHAR(15);              
   v_tintAlertOn  VARCHAR(1);              
   v_intCount  INTEGER;              
   v_vcSubject  VARCHAR(1000);              
   v_vcBody  VARCHAR(8000);              
   v_to  VARCHAR(100);              
   v_cc  VARCHAR(8000);              
   v_ccManager  VARCHAR(100);              
   v_vcFormattedBody  VARCHAR(8000);              
   v_numDivisionId  VARCHAR(15);             
   v_numAlertDDTLId  NUMERIC(9,0);          
   v_numDepartMent  VARCHAR(15);            
          
   v_vcPSMTPServer  VARCHAR(50);
   v_numPSMTPPort  NUMERIC(9,0);
   v_vcPSmtpPassword  VARCHAR(500);
   v_vcPSMTPUserName  VARCHAR(50);
   v_bitPSMTPServer  BOOLEAN;
   v_bitPSMTPSSL  BOOLEAN;
   v_bitPSMTPAuth  BOOLEAN;
           
   v_numAlertDTLid  VARCHAR(15);              
   v_numBizDocs  VARCHAR(15);              
              
   v_numOppBizDocsId  VARCHAR(15);              
   v_DueDate  VARCHAR(15);              
              
            
   v_ProID  VARCHAR(15);              
   v_vcProjectname  VARCHAR(100);              
   v_intPrjMgr  VARCHAR(15);              
   v_ExtPrjMgr  VARCHAR(15);              
----sending mail when project is past due date to internal manager              
              
   v_numCaseId  VARCHAR(15);              
   v_vcCaseNO  VARCHAR(50);              
   v_numRecOwner  VARCHAR(15);              
   v_numAge  VARCHAR(15);              
   v_vcContactName  VARCHAR(15);                
   v_vcEmail  VARCHAR(100);              
          
   v_numWItemCode  VARCHAR(15);              
   v_vcPartNO  VARCHAR(100);              
   v_vcItemName  VARCHAR(100);              
   v_numQtyOnHand  VARCHAR(15);              
   v_Warehouse  VARCHAR(50);           
   v_numContractID  NUMERIC;         
   v_vcContractName  VARCHAR(200);           
   v_RemHours  NUMERIC;            
   v_RemAmount  NUMERIC;            
   v_Days  NUMERIC;            
   v_Incidents  NUMERIC;            
   v_numEmaildays  NUMERIC;            
   v_numemailIncidents  NUMERIC;               
   v_numHours  NUMERIC;            
   v_bitEHour  BOOLEAN;            
   v_bitEIncidents  BOOLEAN;            
   v_bitEDays  BOOLEAN;            
   v_vcTo  VARCHAR(200);
   v_vcStagename  VARCHAR(100);              
   v_vcOppName  VARCHAR(100);              
   v_numAssignTo  VARCHAR(15);
   v_BizDocs  VARCHAR(15);              
   v_contactEmail  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   drop table IF EXISTS tt_TEMPTABLESENDAPPLICATIONMAIL CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLESENDAPPLICATIONMAIL
   (  
      vcTo VARCHAR(2000),
      vcSubject VARCHAR(2000),
      vcBody TEXT,
      vcCC VARCHAR(2000),
      vcFrom VARCHAR(100),
      numDomainID NUMERIC(9,0),
      numConECampDTLID NUMERIC(9,0),
      vcPSMTPServer VARCHAR(50),
      numPSMTPPort NUMERIC(9,0),
      vcPSmtpPassword VARCHAR(500),
      vcPSMTPUserName VARCHAR(50),
      bitPSMTPServer BOOLEAN,
      bitPSMTPSSL BOOLEAN,
      bitPSMTPAuth BOOLEAN
   );                
              
              
----Sending mail to Assignee when the stage past due by fixed days(Opportunity Stages)          
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
   v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDTL.numAlertDTLid = AlertDomainDtl.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 3 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          

   select   vcPSMTPServer, numPSMTPPort, vcPSmtpPassword, vcPSMTPUserName, bitPSMTPSSL, bitPSMTPSSL, bitPSMTPAuth INTO v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,
   v_bitPSMTPSSL,v_bitPSMTPAuth FROM Domain WHERE numDomainID = v_numDomainId;
         
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numOppStageID, vcStageDetail, vcpOppName, numAssignTo INTO v_numOppStageID,v_vcStagename,v_vcOppName,v_numAssignTo from OpportunityStageDetails OppStg
         join OpportunityMaster OppMst
         on OppMst.numOppId = OppStg.numoppid where OppMst.numDomainId = v_numDomainId and  numstagepercentage <> 100 and numstagepercentage <> 0 and tintoppstatus = 0 and bitStageCompleted = false
         and SUBSTR(CAST(bintDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))    LIMIT 1;
         while cast(NULLIF(v_numOppStageID,'') as INTEGER) > 0 LOOP              
   --mail content               
                 
            v_vcFormattedBody := replace(v_vcBody,'##Stage##',v_vcStagename);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##OppID##',v_vcOppName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
            v_to := fn_GetManagerEmail(1::SMALLINT,0::NUMERIC(9,0),v_numAssignTo);              
    --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,NULL,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
   
            select   numOppStageID INTO v_numOppStageID from OpportunityStageDetails OppStg
            join OpportunityMaster OppMst
            on OppMst.numOppId = OppStg.numoppid where OppMst.numDomainId = v_numDomainId and numstagepercentage <> 100 and numstagepercentage <> 0 and tintoppstatus = 0 and bitStageCompleted = false
            and SUBSTR(CAST(bintDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numOppStageId > v_numOppStageID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numOppStageID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;              
                 
   ----Sending mail to Assignee when the stage past due byt fixed days(Project Stages)              
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numProStageId, vcStageDetail, vcProjectName, numAssignTo INTO v_numOppStageID,v_vcStagename,v_vcOppName,v_numAssignTo from ProjectsStageDetails Prostg
         join ProjectsMaster ProMst
         on ProMst.numProId = Prostg.numProId where ProMst.numdomainId = v_numDomainId and numstagepercentage <> 100 and numstagepercentage <> 0 and tintproStatus = 0 and bitStageCompleted = false
         and SUBSTR(CAST(bintDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))    LIMIT 1;
         while cast(NULLIF(v_numOppStageID,'') as INTEGER) > 0 LOOP              
   --mail content               
            v_vcFormattedBody := replace(v_vcBody,'##Stage##',v_vcStagename);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##OppID##',v_vcOppName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
            v_to := fn_GetManagerEmail(1::SMALLINT,0::NUMERIC(9,0),v_numAssignTo);              
      -- exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
   
            select   numProStageId, vcStageDetail, vcProjectName, numAssignTo INTO v_numOppStageID,v_vcStagename,v_vcOppName,v_numAssignTo from ProjectsStageDetails Prostg
            join ProjectsMaster ProMst
            on ProMst.numProId = Prostg.numProId where ProMst.numdomainId = v_numDomainId and numstagepercentage <> 100 and numstagepercentage <> 0 and tintproStatus = 0 and bitStageCompleted = false
            and SUBSTR(CAST(bintDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numProStageId > v_numOppStageID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numOppStageID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
      v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDTL.numAlertDTLid = AlertDomainDtl.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 3 and tintAlertOn = 1 and numEmailTemplate > 0 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;             
             
              
---Sending mail  for Past Due Bills & Invoices               
   select   numAlertDDTLId, AlertDTL.numAlertDTLid, numDaysAfterDue, numEmailTemplate, tintAlertOn, numDepartment, numDomainID, numBizDocs INTO v_numAlertDDTLId,v_numAlertDTLid,v_numDaysAfterDue,v_numEmailTemplate,
   v_tintAlertOn,v_numDepartMent,v_numDomainId,v_numBizDocs from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where numAlertID = 8 and tintAlertOn = 1  and numEmailTemplate > 0    LIMIT 1;           
              
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numOppBizDocsId, bintAccountClosingDate, numDivisionId, numBizDocId INTO v_numOppBizDocsId,v_DueDate,v_numDivisionId,v_BizDocs from OpportunityBizDocs BD
         join OpportunityMaster OM on
         OM.numOppId = BD.numoppid where OM.numDomainId = v_numDomainId and numBizDocId = cast(NULLIF(v_numBizDocs,'') as NUMERIC(18,0)) and SUBSTR(CAST(bintAccountClosingDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))    LIMIT 1;
         while cast(NULLIF(v_numOppBizDocsId,'') as INTEGER) > 0 LOOP
            select   numContactId, vcEmail INTO v_numContactID,v_contactEmail from AdditionalContactsInformation where numDivisionId = cast(NULLIF(v_numDivisionId,'') as NUMERIC(18,0)) and  vcDepartment = cast(NULLIF(v_numDepartMent,'') as NUMERIC(18,0)) and numDomainID = v_numDomainId    LIMIT 1;
            while cast(NULLIF(v_numContactID,'') as INTEGER) > 0 LOOP            
               
  ---Mail Content              
               v_vcFormattedBody := replace(v_vcBody,'##BizDocName##',fn_GetListName(v_BizDocs,0::BOOLEAN));
               v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
               v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
               v_to := v_contactEmail;              
          -- exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
               insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
    
               select   numContactId, vcEmail INTO v_numContactID,v_contactEmail from AdditionalContactsInformation where numDivisionId = cast(NULLIF(v_numDivisionId,'') as NUMERIC(18,0)) and  vcDepartment = cast(NULLIF(v_numDepartMent,'') as NUMERIC(18,0))  and numDomainID = v_numDomainId
               and numContactId > cast(NULLIF(v_numContactID,'') as NUMERIC(18,0))    LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               if SWV_RowCount = 0 then 
                  v_numContactID := CAST(0 AS VARCHAR(15));
               end if;
            END LOOP;
            select   numOppBizDocsId, bintAccountClosingDate, numDivisionId, numBizDocId INTO v_numOppBizDocsId,v_DueDate,v_numDivisionId,v_BizDocs from OpportunityBizDocs BD
            join OpportunityMaster OM on
            OM.numOppId = BD.numoppid where OM.numDomainId = v_numDomainId and numBizDocId = cast(NULLIF(v_numBizDocs,'') as NUMERIC(18,0)) and SUBSTR(CAST(bintAccountClosingDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numOppBizDocsId > cast(NULLIF(v_numOppBizDocsId,'') as NUMERIC(18,0))    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numOppBizDocsId := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, AlertDTL.numAlertDTLid, numDaysAfterDue, numEmailTemplate, tintAlertOn, numDomainID, numDepartment, numBizDocs INTO v_numAlertDDTLId,v_numAlertDTLid,v_numDaysAfterDue,v_numEmailTemplate,
      v_tintAlertOn,v_numDomainId,v_numDepartMent,v_numBizDocs from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where numAlertID = 8 and tintAlertOn = 1   and numEmailTemplate > 0
      and AlertDomainDtl.numAlertDDTLId > numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;              
              
             
              
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
   v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 12 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
   
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numProId, vcProjectName, numDivisionId, numIntPrjMgr INTO v_ProID,v_vcProjectname,v_numDivisionId,v_intPrjMgr from ProjectsMaster where tintProStatus = 0 and
         SUBSTR(CAST(intDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
         and numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_ProID,'') as INTEGER) > 0 LOOP              
    ---Mail Content              
            v_vcFormattedBody := replace(v_vcBody,'##OppID##',v_vcProjectname);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
            v_to := fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),v_intPrjMgr);
            if cast(NULLIF(v_tintCCManager,'') as INTEGER) = 1 then 
               v_cc := fn_GetManagerEmail(1::SMALLINT,0::NUMERIC(9,0),v_intPrjMgr);
            else 
               v_cc := '';
            end if;              
        --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST(v_cc AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
    
            select   numProId, vcProjectName, numDivisionId, numIntPrjMgr INTO v_ProID,v_vcProjectname,v_numDivisionId,v_intPrjMgr from ProjectsMaster where tintProStatus = 0 and
            SUBSTR(CAST(intDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numDomainID = v_numDomainId
            and numProId > v_ProID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_ProID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
      v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 12 and numEmailTemplate > 0 and tintAlertOn = 1 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;          
          
              
              
----sending mail when project is past due date to External manager              
              
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
   v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 13 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
          
   while   v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
 
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numProId, vcProjectName, numDivisionId, numCustPrjMgr INTO v_ProID,v_vcProjectname,v_numDivisionId,v_ExtPrjMgr from ProjectsMaster where tintProStatus = 0 and
         SUBSTR(CAST(intDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
         and numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_ProID,'') as INTEGER) > 0 LOOP              
  ---Mail Content              
            v_vcFormattedBody := replace(v_vcBody,'##OppID##',v_vcProjectname);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
            v_to := fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),v_ExtPrjMgr);
            v_cc := fn_GetCCEmail(12::NUMERIC,v_numDomainId);              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST(v_cc AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
  
            select   numProId, vcProjectName, numDivisionId, numCustPrjMgr INTO v_ProID,v_vcProjectname,v_numDivisionId,v_ExtPrjMgr from ProjectsMaster where tintProStatus = 0 and
            SUBSTR(CAST(intDueDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numDomainID = v_numDomainId
            and numProId > v_ProID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_ProID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
      v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 13 and numEmailTemplate > 0 and tintAlertOn = 1 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;            
            
              
              
----sending mail when case is past due date               
   select   numAlertDDTLId, numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,
   v_numDaysAfterDue,v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 18  and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
 
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numCaseId, vcCaseNumber, numDivisionID, numCreatedby INTO v_numCaseId,v_vcCaseNO,v_numDivisionId,v_numRecOwner from Cases where numStatus <> 136 and
         SUBSTR(CAST(intTargetResolveDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
         and numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_numCaseId,'') as INTEGER) > 0 LOOP              
  ---Mail Content              
            v_vcFormattedBody := replace(v_vcBody,'##CaseID##',v_vcCaseNO);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##DaysLate##',v_numDaysAfterDue);
            v_to := fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),v_numRecOwner);
            if cast(NULLIF(v_tintCCManager,'') as INTEGER) = 1 then 
               v_cc := fn_GetManagerEmail(0::SMALLINT,v_numRecOwner,0::NUMERIC(9,0));
            else 
               v_cc := '';
            end if;              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST(v_cc AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
  
            select   numCaseId, vcCaseNumber, numDivisionID, numCreatedby INTO v_numCaseId,v_vcCaseNO,v_numDivisionId,v_numRecOwner from Cases where numStatus <> 136 and
            SUBSTR(CAST(intTargetResolveDate AS VARCHAR(11)),1,11) = CAST(TIMEZONE('UTC',now())+CAST(-v_numDaysAfterDue || 'day' as interval) AS VARCHAR(11))
            and numDomainID = v_numDomainId
            and numCaseId > v_numCaseId    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numCaseId := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
      v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 18 and tintAlertOn = 1  and numEmailTemplate > 0 and  numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;           
              
            
               
----sending mail to Contacts on fixed age              
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numAge, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numAge,v_numDomainId,
   v_numDaysAfterDue,v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 23 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;            
          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
 
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numContactId, vcFirstName || ' ' || vcLastname, numDivisionId, vcEmail INTO v_numContactID,v_vcContactName,v_numDivisionId,v_vcEmail from AdditionalContactsInformation where EXTRACT(year FROM TIMEZONE('UTC',now())) -EXTRACT(year FROM bintDOB) = cast(NULLIF(v_numAge,'') as INTEGER)
         and numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_numContactID,'') as INTEGER) > 0 LOOP              
  ---Mail Content              
                   
            v_vcFormattedBody := replace(v_vcBody,'##ContactName##',v_vcContactName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Age##',v_numAge);
            v_to := v_vcEmail;
            v_cc := fn_GetCCEmail(23::NUMERIC,v_numDomainId);
            if cast(NULLIF(v_tintCCManager,'') as INTEGER) = 1 then 
               v_ccManager := fn_GetManagerEmail(1::SMALLINT,0::NUMERIC(9,0),v_numContactID);
            else 
               v_ccManager := '';
            end if;
            if v_ccManager <> '' and  v_cc <> '' then 
               v_cc := coalesce(v_cc,'') || ';' || coalesce(v_ccManager,'');
            end if;
            if v_ccManager <> '' and  v_cc = '' then 
               v_cc := v_ccManager;
            end if;              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST(v_cc AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
  
            select   numContactId, vcFirstName || ' ' || vcLastname, numDivisionId, vcEmail INTO v_numContactID,v_vcContactName,v_numDivisionId,v_vcEmail from AdditionalContactsInformation where EXTRACT(year FROM TIMEZONE('UTC',now())) -EXTRACT(year FROM bintDOB) = cast(NULLIF(v_numAge,'') as INTEGER)
            and numDomainID = v_numDomainId
            and numContactId > cast(NULLIF(v_numContactID,'') as NUMERIC(18,0))    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numContactID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numAge, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numAge,v_numDomainId,
      v_numDaysAfterDue,v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 23 and tintAlertOn = 1 and numEmailTemplate > 0 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;          
          
          
          
----Sending mail on every birthday          
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numAge, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numAge,v_numDomainId,
   v_numDaysAfterDue,v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 24 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
 
         select   numContactId, vcFirstName || ' ' || vcLastname, numDivisionId, vcEmail, EXTRACT(year FROM TIMEZONE('UTC',now())) -EXTRACT(year FROM bintDOB) INTO v_numContactID,v_vcContactName,v_numDivisionId,v_vcEmail,v_numAge from AdditionalContactsInformation where EXTRACT(day FROM bintDOB) = EXTRACT(day FROM TIMEZONE('UTC',now())) and EXTRACT(month FROM bintDOB) = EXTRACT(month FROM TIMEZONE('UTC',now()))
         and numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_numContactID,'') as INTEGER) > 0 LOOP
            v_vcFormattedBody := replace(v_vcBody,'##ContactName##',v_vcContactName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Organization##',fn_GetComapnyName(v_numDivisionId));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Age##',v_numAge);
            v_to := v_vcEmail;
            v_cc := fn_GetCCEmail(24::NUMERIC,v_numDomainId);
            if cast(NULLIF(v_tintCCManager,'') as INTEGER) = 1 then 
               v_ccManager := fn_GetManagerEmail(1::SMALLINT,0::NUMERIC(9,0),v_numContactID);
            else 
               v_ccManager := '';
            end if;
            if v_ccManager <> '' and  v_cc <> '' then 
               v_cc := coalesce(v_cc,'') || ';' || coalesce(v_ccManager,'');
            end if;
            if v_ccManager <> '' and  v_cc = '' then 
               v_cc := v_ccManager;
            end if;              
     --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST(v_cc AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
    
            select   numContactId, vcFirstName || ' ' || vcLastname, numDivisionId, vcEmail, EXTRACT(year FROM TIMEZONE('UTC',now())) -EXTRACT(year FROM bintDOB) INTO v_numContactID,v_vcContactName,v_numDivisionId,v_vcEmail,v_numAge from AdditionalContactsInformation where EXTRACT(day FROM bintDOB) = EXTRACT(day FROM TIMEZONE('UTC',now())) and EXTRACT(month FROM bintDOB) = EXTRACT(month FROM TIMEZONE('UTC',now())) and numDomainID = v_numDomainId
            and numContactId > cast(NULLIF(v_numContactID,'') as NUMERIC(18,0))    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numContactID := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numAge, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numAge,v_numDomainId,
      v_numDaysAfterDue,v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 24 and tintAlertOn = 1 and numEmailTemplate > 0 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;              
              
           
          
              
----sending mail when items reaches reorder point              
   select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
   v_tintAlertOn from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 25 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
  
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numWareHouseItemID, vcWareHouse, vcItemName, numOnHand INTO v_numWItemCode,v_Warehouse,v_vcItemName,v_numQtyOnHand from Item I
         join WareHouseItems
         on I.numItemCode = numItemID
         join Warehouses W
         on W.numWareHouseID = WareHouseItems.numWareHouseID where numReorder >= numOnHand   and I.numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_numWItemCode,'') as INTEGER) > 0 LOOP
            RAISE NOTICE '%',v_vcItemName;
            v_vcFormattedBody := replace(v_vcBody,'##ItemName##',v_vcItemName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Units##',v_numQtyOnHand);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Warehouse##',v_Warehouse);
            v_to := fn_GetCCEmail(25::NUMERIC,v_numDomainId);              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
            insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
   
            select   numWareHouseItemID, vcWareHouse, vcItemName, numOnHand INTO v_numWItemCode,v_Warehouse,v_vcItemName,v_numQtyOnHand from Item I
            join WareHouseItems
            on I.numItemCode = numItemID
            join Warehouses W
            on W.numWareHouseID = WareHouseItems.numWareHouseID where numReorder >= numOnHand and numWareHouseItemID > v_numWItemCode    and I.numDomainID = v_numDomainId    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numWItemCode := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, tintCCManager, numDomainID, numDaysAfterDue, tintAlertOn INTO v_numAlertDDTLId,v_numEmailTemplate,v_tintCCManager,v_numDomainId,v_numDaysAfterDue,
      v_tintAlertOn from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 25 and tintAlertOn = 1 and numEmailTemplate > 0 and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;            
          
          
          
          
 ----sending mailto contacts in vendor list             
   v_numContactID := CAST(0 AS VARCHAR(15));          
   select   numAlertDDTLId, numEmailTemplate, numDomainID, tintAlertOn, numDepartMent INTO v_numAlertDDTLId,v_numEmailTemplate,v_numDomainId,v_tintAlertOn,v_numDepartMent from AlertDTL
   join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 26 and tintAlertOn = 1 and numEmailTemplate > 0    LIMIT 1;          
   while v_numAlertDDTLId > 0 LOOP
      if cast(NULLIF(v_tintAlertOn,'') as INTEGER) = 1 then
  
         select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
         select   numWareHouseItemID, vcPartNo, I.numVendorID, vcItemName, numOnHand INTO v_numWItemCode,v_vcPartNO,v_numDivisionId,v_vcItemName,v_numQtyOnHand from Item I
         join Vendor V
         on I.numItemCode = V.numItemCode
         join WareHouseItems
         on I.numItemCode = numItemID where numReorder >= numOnHand and I.numDomainID = v_numDomainId    LIMIT 1;
         while cast(NULLIF(v_numWItemCode,'') as INTEGER) > 0 LOOP
            RAISE NOTICE '%',v_numDepartMent;
            RAISE NOTICE '%',v_numDivisionId;
            v_vcFormattedBody := replace(v_vcBody,'##ItemName##',v_vcItemName);
            v_vcFormattedBody := replace(v_vcFormattedBody,'##PartNo##',coalesce(v_vcPartNO,''));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Vendor##',coalesce(fn_GetComapnyName(v_numDivisionId),''));
            v_vcFormattedBody := replace(v_vcFormattedBody,'##Units##',v_numQtyOnHand);
            select   numContactId, vcEmail INTO v_numContactID,v_contactEmail from AdditionalContactsInformation where numDivisionId = cast(NULLIF(v_numDivisionId,'') as NUMERIC(18,0)) and numDomainID = v_numDomainId   and vcDepartment = cast(NULLIF(v_numDepartMent,'') as NUMERIC(18,0))    LIMIT 1;
            while cast(NULLIF(v_numContactID,'') as INTEGER) > 0 LOOP
               RAISE NOTICE '%',v_numContactID;
               v_to := v_contactEmail;
               insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_to AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,NULL,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
      
               select   numContactId, vcEmail INTO v_numContactID,v_contactEmail from AdditionalContactsInformation where numDivisionId = cast(NULLIF(v_numDivisionId,'') as NUMERIC(18,0))  and vcDepartment = cast(NULLIF(v_numDepartMent,'') as NUMERIC(18,0))
               and numContactId > cast(NULLIF(v_numContactID,'') as NUMERIC(18,0))    LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               if SWV_RowCount = 0 then 
                  v_numContactID := CAST(0 AS VARCHAR(15));
               end if;
            END LOOP;
            select   numWareHouseItemID, vcPartNo, I.numVendorID, vcItemName, numOnHand INTO v_numWItemCode,v_vcPartNO,v_numDivisionId,v_vcItemName,v_numQtyOnHand from Item I
            join Vendor V
            on I.numItemCode = V.numItemCode
            join WareHouseItems
            on I.numItemCode = numItemID where numReorder >= numOnHand    and I.numDomainID = v_numDomainId and numWareHouseItemID > v_numWItemCode    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            if SWV_RowCount = 0 then 
               v_numWItemCode := CAST(0 AS VARCHAR(15));
            end if;
         END LOOP;
      end if;
      select   numAlertDDTLId, numEmailTemplate, numDomainID, tintAlertOn, numDepartMent INTO v_numAlertDDTLId,v_numEmailTemplate,v_numDomainId,v_tintAlertOn,v_numDepartMent from AlertDTL
      join AlertDomainDtl on AlertDomainDtl.numAlertDTLid = AlertDTL.numAlertDTLid where  AlertDomainDtl.numAlertDTLid = 26 and tintAlertOn = 1 and numEmailTemplate > 0  and numAlertDDTLId > v_numAlertDDTLId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertDDTLId := 0;
      end if;
   END LOOP;               
                
          
          
---For Contracts Emails        
                   
        
           
----------script to find remaining hours                               
                                   
-----------------------------------------------                                  
------------script to find remaining Amount                              
                                             
-----------------------------------------------                              
-----------remaining days                            
                             
                            
---------------------------------------------                            
                            
--------------------------------              
   select   numContractId, vcContractName, numDomainID, case when bitHour = true then
      getContractRemainingHrsAmt(1::BOOLEAN,c.numContractId)
   else
      cast('0' as DECIMAL(10,2))
   end, case when bitAmount = true
   then
      getContractRemainingHrsAmt(0::BOOLEAN,c.numContractId)
   else
      cast('0' as DECIMAL(10,2))
   end, case when bitDays = true then
      coalesce(CAST(case when DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) >= 0 then DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) else cast('0' as INTEGER) end AS VARCHAR(100)),cast(0 as TEXT))
   else '0' end, case when bitIncidents = true then
      CAST(CAST(numincidents AS DECIMAL(10,2)) -CAST(GetIncidents(numContractId,numDomainID,c.numDivisionID) AS DECIMAL(10,2)) AS VARCHAR(100))
   else '0' end, numTemplateId, numEmailDays, numEmailIncidents, numHours, bitEHour, bitEIncidents, bitEDays, GetContractsEmails(numContractId) INTO v_numContractID,v_vcContractName,v_numDomainId,v_RemHours,v_RemAmount,
   v_Days,v_Incidents,v_numEmailTemplate,v_numEmaildays,v_numemailIncidents,
   v_numHours,v_bitEHour,v_bitEIncidents,v_bitEDays,v_vcTo from ContractManagement c where (bitEHour = true or bitEIncidents = true or bitEDays = true)    LIMIT 1;            
            
            
   while v_numContractID > 0 LOOP
      select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody from GenericDocuments where CAST(numGenericDocID AS VARCHAR) = v_numEmailTemplate;
      if (v_RemHours  =(case v_bitEHour when true then 1 else 0 end)) or (v_Incidents =(case v_bitEIncidents when true then 1 else 0 end)) or (v_Days  =(case v_bitEDays when true then 1 else 0 end)) then
   
         v_vcFormattedBody   := replace(v_vcBody,'##RemHours##',v_RemHours:: TEXT);
         v_vcFormattedBody   := replace(v_vcFormattedBody,'##Contract##',v_vcContractName);
         v_vcFormattedBody   := replace(v_vcFormattedBody,'##RemIncidents##',v_Incidents:: TEXT);
         v_vcFormattedBody   := replace(v_vcFormattedBody,'##RemDays##',v_Days:: TEXT);
         insert into tt_TEMPTABLESENDAPPLICATIONMAIL  Values(CAST(v_vcTo AS VARCHAR(2000)) , CAST(v_vcSubject AS VARCHAR(2000)),v_vcFormattedBody,CAST('' AS VARCHAR(2000)),CAST('' AS VARCHAR(100)),v_numDomainId,null,v_vcPSMTPServer,v_numPSMTPPort,v_vcPSmtpPassword,v_vcPSMTPUserName,v_bitPSMTPServer,v_bitPSMTPSSL,v_bitPSMTPAuth);
      end if;            
            
-- End            
            
                 
  ----------script to find remaining hours                               
                                     
  -----------------------------------------------                                  
  ------------script to find remaining Amount                              
                                               
  -----------------------------------------------                              
  -----------remaining days                            
                               
                              
  ---------------------------------------------                            
                              
  --------------------------------             
      select   numContractId, vcContractName, numDomainID, case when bitHour = true then
         getContractRemainingHrsAmt(0::BOOLEAN,c.numContractId)
      else
         cast('0' as DECIMAL(10,2))
      end, case when bitAmount = true
      then
         getContractRemainingHrsAmt(1::BOOLEAN,c.numContractId)
      else
         cast('0' as DECIMAL(10,2))
      end, case when bitDays = true then
         coalesce(CAST(case when DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) >= 0 then DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) else cast('0' as INTEGER) end AS VARCHAR(100)),cast(0 as TEXT))
      else '0' end, case when bitIncidents = true then
         CAST(CAST(numincidents AS DECIMAL(10,2)) -CAST(GetIncidents(numContractId,numDomainID,c.numDivisionID) AS DECIMAL(10,2)) AS VARCHAR(100))
      else '0' end, numTemplateId, numEmailDays, numEmailIncidents, numHours, bitEHour, bitEIncidents, bitEDays, GetContractsEmails(numContractId) INTO v_numContractID,v_vcContractName,v_numDomainId,v_RemHours,v_RemAmount,
      v_Days,v_Incidents,v_numEmailTemplate,v_numEmaildays,v_numemailIncidents,
      v_numHours,v_bitEHour,v_bitEIncidents,v_bitEDays,v_vcTo from ContractManagement c where (bitEHour = true or bitEIncidents = true or bitEDays = true)
      and numContractId > v_numContractID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numContractID := 0;
      end if;
   END LOOP;         

   open SWV_RefCur for select * from tt_TEMPTABLESENDAPPLICATIONMAIL;            
      
   RETURN;
END; $$;












