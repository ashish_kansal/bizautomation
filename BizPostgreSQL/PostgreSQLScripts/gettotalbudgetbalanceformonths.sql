-- Function definition script GetTotalBudgetBalanceForMonths for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalBudgetBalanceForMonths(v_Month INTEGER,v_Year INTEGER,v_monAmount DECIMAL(20,5),v_numDomainId NUMERIC(9,0),v_numUserCntID NUMERIC(9,0),v_sintForecast SMALLINT)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monOperBudgetAmt  DECIMAL(20,5);          
   v_monProBudgetAmt  DECIMAL(20,5);          
   v_monMarkBudgetAmt  DECIMAL(20,5);   
   v_monTotalBudgetBalance  DECIMAL(20,5);         
----Set @monCashInflowAmt = dbo.GetCashInflowForMonths(@Month,@Year,@numDomainId)          
----Set @monSalesForeCastAmt=dbo.GetSalesForeCastForMonths(@Month,@Year,@numDomainId,@numUserCntID,@sintForecast)          

--For Operation Budget
BEGIN
   select   sum(coalesce(monAmount,0)) INTO v_monOperBudgetAmt From OperationBudgetMaster OBM
   inner join OperationBudgetDetails OBD on OBM.numBudgetId = OBD.numBudgetID where OBD.tintMonth = v_Month And OBD.intYear = v_Year And OBM.numDomainId = v_numDomainId;
--For Procurement Budget
   select   sum(coalesce(monAmount,0)) INTO v_monProBudgetAmt From ProcurementBudgetMaster PBM
   inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId where PBD.tintMonth = v_Month And PBD.intYear = v_Year And PBM.numDomainID = v_numDomainId;
--For Market Budget     
   select   sum(coalesce(monAmount,0)) INTO v_monMarkBudgetAmt From MarketBudgetMaster MBM
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId where tintMonth = v_Month And intYear = v_Year  And MBM.numDomainID = v_numDomainId;
  
   v_monTotalBudgetBalance := coalesce(v_monOperBudgetAmt,0)+coalesce(v_monProBudgetAmt,0)+coalesce(v_monMarkBudgetAmt,0);          
          
        
   return v_monTotalBudgetBalance;
END; $$;

