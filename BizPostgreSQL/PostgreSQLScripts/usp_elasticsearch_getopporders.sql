DROP FUNCTION IF EXISTS USP_ElasticSearch_GetOppOrders;

CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetOppOrders(v_numDomainID NUMERIC(18,0)
	,v_vcOppIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numOppId AS id
		,'order' AS module
		,coalesce(tintopptype,0) AS "tintOppType"
		,coalesce(tintoppstatus,0) AS "tintOppStatus"
		,coalesce(OpportunityMaster.numassignedto,0) AS "numAssignedTo"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,coalesce(OpportunityMaster.numrecowner,0) AS "numRecOwner"
		,CONCAT(CASE 
					WHEN tintopptype = 1 
					THEN '<b style="color:#00aa50">' 
					ELSE '<b style="color:#ff0000">' 
				END
				,CASE WHEN tintopptype = 1 THEN 'Sales ' ELSE 'Purchase ' END
				,CASE WHEN tintoppstatus = 1 THEN ' (order)' ELSE ' (opportunity)' END
				,':</b> ',vcpOppName,', ',vcCompanyName,', ',monDealAmount) AS displaytext
		,vcpOppName AS text
		,CONCAT('/opportunity/frmOpportunities.aspx?opId=',numOppId) AS url
		,vcpOppName AS "Search_vcPOppName"
   FROM
   OpportunityMaster
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND (NULLIF(v_vcOppIds,'') IS NULL OR OpportunityMaster.numOppId IN(SELECT id FROM SplitIDs(coalesce(v_vcOppIds,''),',')))
   ORDER BY
   numOppId DESC;
END; $$;












