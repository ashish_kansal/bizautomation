-- Function definition script GetPercentageChangeInSalesOppToOrderConversion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPercentageChangeInSalesOppToOrderConversion(v_numDomainID NUMERIC(18,0),
	 v_dtStartDate DATE,
	 v_dtEndDate DATE,
	 v_tintCompareValueOf SMALLINT --1: Number of Conversion, 2: Amount of Conversion
)
RETURNS NUMERIC(18,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SalesOppToOrderConversionChange  NUMERIC(18,2) DEFAULT 0.00;
   v_SalesOppToOrderConversion1  INTEGER;
   v_SalesOppToOrderConversion2  INTEGER;
BEGIN
   select(CASE WHEN v_tintCompareValueOf = 1 THEN COUNT(numOppId) ELSE SUM(monDealAmount) END) INTO v_SalesOppToOrderConversion1 FROM
   OpportunityMaster OM WHERE
   OM.numDomainId = v_numDomainID
   AND OM.bintCreatedDate BETWEEN  v_dtStartDate AND v_dtEndDate
   AND coalesce(OM.tintopptype,0) = 1
   AND coalesce(OM.tintoppstatus,0) = 1
   AND bintOppToOrder IS NOT NULL;

   select(CASE WHEN v_tintCompareValueOf = 1 THEN COUNT(numOppId) ELSE SUM(monDealAmount) END) INTO v_SalesOppToOrderConversion2 FROM
   OpportunityMaster OM WHERE
   OM.numDomainId = v_numDomainID
   AND OM.bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 year' AND v_dtEndDate+INTERVAL '-1 year'
   AND coalesce(OM.tintopptype,0) = 1
   AND coalesce(OM.tintoppstatus,0) = 1
   AND bintOppToOrder IS NOT NULL;


   v_SalesOppToOrderConversionChange := 100.0*(coalesce(v_SalesOppToOrderConversion1,0) -coalesce(v_SalesOppToOrderConversion2,0))/(CASE WHEN coalesce(v_SalesOppToOrderConversion2,0) = 0 THEN 1 ELSE coalesce(v_SalesOppToOrderConversion2,0) END);


   RETURN coalesce(v_SalesOppToOrderConversionChange,0.00);
END; $$;

