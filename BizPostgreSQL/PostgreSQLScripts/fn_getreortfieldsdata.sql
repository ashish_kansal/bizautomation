-- Function definition script fn_GetReortFieldsData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetReortFieldsData()
RETURNS VARCHAR(8000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strFields  VARCHAR(8000);
   v_value  VARCHAR(15);
   v_text  VARCHAR(100);
   v_fldType  VARCHAR(1);
   SWV_RowCount INTEGER;
BEGIN
   v_strFields := '';
   select   numRepFieldID, vcFieldValue, tintFldType INTO v_value,v_text,v_fldType from ReportFields where tintSelected = 1    LIMIT 1; 
   while cast(NULLIF(v_value,'') as INTEGER) > 0 LOOP
      if cast(NULLIF(v_fldType,'') as INTEGER) = 1 then 
         v_text := 'coalesce(dbo.fn_GetListItemName(' || coalesce(v_text,'') || '),''-'')';
      end if;
      if cast(NULLIF(v_fldType,'') as INTEGER) = 2 then 
         v_text := 'coalesce(dbo.fn_GetTerritoryName(' || coalesce(v_text,'') || '),''-'')';
      end if;
      v_strFields := coalesce(v_strFields,'') || coalesce(v_text,'') || ' || '', '' || ';
      select   numRepFieldID, vcFieldValue, tintFldType INTO v_value,v_text,v_fldType from ReportFields where tintSelected = 1 and numRepFieldID > v_value    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_value := CAST(0 AS VARCHAR(15));
      end if;
   END LOOP;
   v_strFields := coalesce(v_strFields,'') || '''- ''';



   return v_strFields;
END; $$;

