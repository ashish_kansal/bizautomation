-- Function definition script fn_GetEmailStringAsJson for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetEmailStringAsJson(v_vcEmailId VARCHAR(4000))
RETURNS VARCHAR(4000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcNewEmailId  VARCHAR(2000) DEFAULT '[';
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_vcContactEmail  VARCHAR(1000);
   v_vcEmailJson  VARCHAR(1000);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPEmails_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPEMAILS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEMAILS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcContactEmail VARCHAR(1000)
   );
   DROP TABLE IF EXISTS tt_TEMPEMAILPART CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEMAILPART
   (
      ID INTEGER,
      vcPartValue VARCHAR(300)
   );
   INSERT INTO tt_TEMPEMAILS(vcContactEmail) SELECT Items FROM SplitByString(v_vcEmailId,'#^#');

   select   COUNT(*) INTO v_iCount FROM tt_TEMPEMAILS;

   WHILE v_i <= v_iCount LOOP
      DELETE FROM tt_TEMPEMAILPART;
      select   vcContactEmail INTO v_vcContactEmail FROM tt_TEMPEMAILS WHERE ID = v_i;
      INSERT INTO tt_TEMPEMAILPART(ID,vcPartValue) SELECT ROW_NUMBER() OVER(ORDER BY(SELECT NULL)),Items FROM SplitByString(v_vcContactEmail,'$^$');
      v_vcEmailJson := CONCAT('{','"id":"',(CASE WHEN coalesce((SELECT COUNT(*) FROM tt_TEMPEMAILPART),0) > 0 THEN coalesce((SELECT EM.numContactID FROM tt_TEMPEMAILPART TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC) = EM.numEmailId WHERE TEP.ID = 1),0) ELSE '0' END),'",','"first_name":"',(CASE
      WHEN coalesce((SELECT COUNT(*) FROM tt_TEMPEMAILPART),0) > 0
      THEN(CASE
         WHEN LTRIM(RTRIM(coalesce((SELECT EM.vcName FROM tt_TEMPEMAILPART TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC) = EM.numEmailId WHERE TEP.ID = 1),
         ''))) = ''
         THEN coalesce((SELECT vcPartValue FROM tt_TEMPEMAILPART WHERE ID = 2),'-')
         ELSE coalesce((SELECT EM.vcName FROM tt_TEMPEMAILPART TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC) = EM.numEmailId WHERE TEP.ID = 1),
            '-')
         END)
      ELSE '-'
      END),
      '",','"last_name":"","email":"',(CASE WHEN coalesce((SELECT COUNT(*) FROM tt_TEMPEMAILPART),0) > 2 THEN coalesce((SELECT vcPartValue FROM tt_TEMPEMAILPART WHERE ID = 3),'-') ELSE '0' END),'","url":"#"}');
      v_vcNewEmailId := CONCAT(v_vcNewEmailId,(CASE WHEN v_vcNewEmailId = '[' THEN '' ELSE ',' END),v_vcEmailJson);
      v_i := v_i::bigint+1;
   END LOOP;

   v_vcNewEmailId := CONCAT(v_vcNewEmailId,']');

   RETURN v_vcNewEmailId;
END; $$;

