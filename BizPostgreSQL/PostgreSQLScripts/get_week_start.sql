CREATE OR REPLACE FUNCTION get_week_start(v_date TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
BEGIN
   return date_trunc('week', v_date::DATE + 1)::date - 1;
END; $$;

