-- Stored procedure definition script USP_GetUsersForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetUsersForReport(v_numUserCntID NUMERIC(9,0),
    v_numDomainId NUMERIC(9,0),
    v_tintType SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintType = 1 then

      open SWV_RefCur for
      SELECT distinct numSelectedUserCntID,
        coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcUserName,
        tintUserType
      FROM    ForReportsByUser F
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = F.numSelectedUserCntID
      join UserMaster UM on ACI.numContactId = UM.numUserDetailId
      LEFT OUTER JOIN UserTeams UT ON UT.numUserCntID = UM.numUserDetailId
      WHERE   F.numContactID = v_numUserCntID
      AND F.numDomainID = v_numDomainId
      AND tintType = v_tintType and UT.numTeam in(SELECT    numTeam
         FROM      UserTeams
         WHERE     numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainId);
   ELSE
      open SWV_RefCur for
      SELECT  numSelectedUserCntID,
        coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcUserName,
        tintUserType
      FROM    ForReportsByUser F
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = F.numSelectedUserCntID
      WHERE   F.numContactID = v_numUserCntID
      AND F.numDomainID = v_numDomainId
      AND tintType = v_tintType;
   end if;
   RETURN;
END; $$;


