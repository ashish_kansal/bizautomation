-- Stored procedure definition script USP_GetTaxOfCartItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTaxOfCartItems(v_numDomainId NUMERIC(9,0) DEFAULT 0 ,
    v_numDivisionId NUMERIC(9,0) DEFAULT 0,
    v_BaseTaxOn INTEGER DEFAULT NULL ,
    v_CookieId VARCHAR(100) DEFAULT NULL,
    v_numCountryID NUMERIC(9,0) DEFAULT NULL,
    v_numStateID NUMERIC(9,0) DEFAULT NULL,
    v_vcCity VARCHAR(100) DEFAULT NULL,
    v_vcZipPostalCode VARCHAR(20) DEFAULT NULL,
    INOUT v_fltTotalTaxAmount NUMERIC DEFAULT NULL ,
    v_numUserCntId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null, INOUT SWV_RefCur4 refcursor default null, INOUT SWV_RefCur5 refcursor default null, INOUT SWV_RefCur6 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sqlQuery  TEXT;
   v_TaxPercentage  NUMERIC(9,0);
 

   v_RowNo  INTEGER;
	
   v_RowsToProcess  INTEGER;
   v_CurrentRow  INTEGER;
BEGIN
   v_sqlQuery := '';
 
   DROP TABLE IF EXISTS tt_CARTITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_CARTITEMS
   (
      numItemCode   NUMERIC(9,0),
      numTaxItemID NUMERIC(9,0),
      numTaxID NUMERIC(18,0),
      numUnitHour NUMERIC(18,0),
      numUOM NUMERIC(18,0),
      monTotAmount DECIMAL(20,5),
      numCartId NUMERIC(18,0),
      bitApplicable BOOLEAN
   );
  
   DROP TABLE IF EXISTS tt_TAXDETAIL CASCADE;
   CREATE TEMPORARY TABLE tt_TAXDETAIL
   (
      numTaxID   NUMERIC(9,0),
      numTaxItemID NUMERIC(9,0),
      numCountryID NUMERIC(9,0),
      numStateID NUMERIC(9,0),
      decTaxPercentage DOUBLE PRECISION,
      tintTaxType SMALLINT,
      numDomainId NUMERIC(9,0),
      vcCity VARCHAR(100),
      vcZipPostal VARCHAR(20)
   );

   DROP TABLE IF EXISTS tt_APPLICABLETAXESFORDIVISION CASCADE;
   CREATE TEMPORARY TABLE tt_APPLICABLETAXESFORDIVISION
   (
      vcTaxName   VARCHAR(100),
      decTaxPercentage DOUBLE PRECISION,
      tintTaxType SMALLINT,
      numTaxItemID NUMERIC(9,0),
      numTaxID NUMERIC(18,0),
      numCountryID NUMERIC(9,0),
      numStateID NUMERIC(9,0),
      numDivisionID NUMERIC(9,0),
      bitApplicable BOOLEAN
   );
  
   DROP TABLE IF EXISTS tt_TAXAMOUNT CASCADE;
   CREATE TEMPORARY TABLE tt_TAXAMOUNT
   (
      numItemCode   NUMERIC(9,0),
      numTaxItemID NUMERIC(9,0),
      bitApplicable BOOLEAN,
      decTaxPercentage DOUBLE PRECISION,
      vcTaxName VARCHAR(100),
      monTotAmount DECIMAL(20,5),
      bitTaxable BOOLEAN,
      fltTaxAmount DOUBLE PRECISION
   );

	-- here union  is used to remove dublicate rows 
   INSERT INTO
   tt_CARTITEMS
   SELECT
   IT.numItemCode ,
		IT.numTaxItemID ,
		coalesce(IT.numTaxID,0),
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount,
		CI.numCartId,
		IT.bitApplicable
   FROM
   ItemTax IT
   INNER JOIN
   CartItems CI
   ON
   IT.numItemCode =  CI.numItemCode
   WHERE
   CI.vcCookieId = v_CookieId
   AND (CI.numUserCntId = v_numUserCntId OR CI.numUserCntId = 0)
   AND CI.numItemCode NOT IN(SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainId = CI.numDomainId)
   UNION
   SELECT
   CI.numItemCode,
		0,
		0,
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount,
		CI.numCartId,
		CAST(1 AS BOOLEAN)
   FROM
   CartItems CI
   INNER JOIN
   Item I
   ON
   CI.numItemCode = I.numItemCode
   WHERE
   CI.vcCookieId = v_CookieId
   AND (CI.numUserCntId = v_numUserCntId OR CI.numUserCntId = 0)
   AND  I.bitTaxable = true
   AND CI.numItemCode NOT IN(SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainId = CI.numDomainId);
 
   open SWV_RefCur for
   SELECT * FROM tt_CARTITEMS;
 
   BEGIN
      CREATE TEMP SEQUENCE tt_Temp1_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1
   (
      row_id INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL PRIMARY KEY,
      TaxItemID NUMERIC
   );

   INSERT INTO tt_TEMP1(TaxItemID)
   SELECT DISTINCT
   numTaxItemID
   FROM
   TaxDetails
   WHERE
   numDomainID = v_numDomainId;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMP1;

   select   COUNT(*) INTO v_RowsToProcess FROM tt_TEMP1;
   v_CurrentRow := 1;
 
   WHILE v_CurrentRow <= v_RowsToProcess LOOP
      IF v_numCountryID > 0 and v_numStateID > 0 then
		
         IF v_numStateID > 0 then
			
            IF EXISTS(SELECT * FROM TaxDetails TD inner JOIN tt_TEMP1 T ON T.TaxItemID = TD.numTaxItemID WHERE numCountryID = v_numCountryID and ((numStateID = v_numStateID  AND
            1 =(Case
            when v_BaseTaxOn = 0 then Case When numStateID = v_numStateID then 1 else 0 end --State
            when v_BaseTaxOn = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
            when v_BaseTaxOn = 2 then Case When vcZipPostal = v_vcZipPostalCode then 1 else 0 end --Zip/Postal
            else 0 end)))
            and numDomainID = v_numDomainId and T.row_id = v_CurrentRow) then
				
               INSERT INTO
               tt_TAXDETAIL
               SELECT 
               numTaxID,
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal
               FROM
               TaxDetails
               WHERE
               numCountryID = v_numCountryID
               AND ((numStateID = v_numStateID  AND
               1 =(Case
               when v_BaseTaxOn = 0 then Case When numStateID = v_numStateID then 1 else 0 end --State
               when v_BaseTaxOn = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
               when v_BaseTaxOn = 2 then Case When vcZipPostal = v_vcZipPostalCode then 1 else 0 end --Zip/Postal
               else 0 end)))
               and numDomainID = v_numDomainId
               and numTaxItemID  =(Select T.TaxItemID from tt_TEMP1 T where T.row_id = v_CurrentRow) LIMIT 1;
            ELSEIF exists(select * from TaxDetails TD inner JOIN tt_TEMP1 T on T.TaxItemID = TD.numTaxItemID
            where numCountryID = v_numCountryID and numStateID = v_numStateID 
            and numDomainID = v_numDomainId  and coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = '' and T.row_id = v_CurrentRow)
            then
				
               INSERT INTO
               tt_TAXDETAIL
               SELECT  
               numTaxID,
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal
               FROM
               TaxDetails TD
               WHERE
               numCountryID = v_numCountryID
               AND numStateID = v_numStateID
               AND numDomainID = v_numDomainId
               AND coalesce(vcCity,'') = '' AND coalesce(vcZipPostal,'') = '' AND numTaxItemID =(Select T.TaxItemID from tt_TEMP1 T where T.row_id = v_CurrentRow) LIMIT 1;
            ELSE
               INSERT INTO
               tt_TAXDETAIL
               SELECT 
               numTaxID,
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal
               FROM
               TaxDetails TD
               WHERE
               numCountryID = v_numCountryID
               AND numStateID = 0
               AND coalesce(vcCity,'') = ''
               AND coalesce(vcZipPostal,'') = ''
               AND numDomainID = v_numDomainId
               AND numTaxItemID  =(Select T.TaxItemID from tt_TEMP1 T where T.row_id = v_CurrentRow) LIMIT 1;
            end if;
         end if;
      ELSEIF v_numCountryID > 0
      then
		
         INSERT INTO
         tt_TAXDETAIL
         SELECT 
         numTaxID,
				numTaxItemID,
				numCountryID,
				numStateID,
				decTaxPercentage,
				tintTaxType,
				numDomainId,
				vcCity,
				vcZipPostal
         FROM
         TaxDetails
         WHERE
         numCountryID = v_numCountryID
         AND numStateID = 0
         AND coalesce(vcCity,'') = ''
         AND coalesce(vcZipPostal,'') = ''
         AND numDomainID = v_numDomainId
         AND numTaxItemID  =(Select T.TaxItemID from tt_TEMP1 T where T.row_id = v_CurrentRow) LIMIT 1;
      end if;
      v_CurrentRow := v_CurrentRow::bigint+1;
   END LOOP;

   open SWV_RefCur3 for
   SELECT * FROM tt_TAXDETAIL;


   IF coalesce(v_numDivisionId,0) = 0 then
	
      INSERT INTO
      tt_APPLICABLETAXESFORDIVISION
      SELECT
      CAST(CASE WHEN TD.numTaxItemID = 0 THEN 'Sales Tax' ELSE TI.vcTaxName END AS VARCHAR(100)) AS vcTaxName
			,TD.decTaxPercentage
			,TD.tintTaxType
			,TD.numTaxItemID
			,TD.numTaxID
			,TD.numCountryID
			,TD.numStateID
			,CAST(0 AS NUMERIC(9,0))
			,CAST(1 AS BOOLEAN)
      FROM
      tt_TAXDETAIL TD
      LEFT JOIN
      TaxItems TI
      ON
      TD.numDomainId = TI.numDomainID
      AND TD.numTaxItemID = TI.numTaxItemID
      WHERE
      TD.numDomainId = v_numDomainId
      UNION
      SELECT CAST(CAST('CRV' AS VARCHAR(9)) AS VARCHAR(100)),TD.decTaxPercentage,TD.tintTaxType,CAST(1 AS NUMERIC(9,0)),numTaxID,TD.numCountryID,TD.numStateID,v_numDivisionId,CAST(1 AS BOOLEAN) FROM tt_TAXDETAIL TD WHERE numTaxItemID = 1;
   ELSE
      INSERT INTO
      tt_APPLICABLETAXESFORDIVISION
      SELECT
      CAST(CASE WHEN DTD.numTaxItemID = 0 THEN 'Sales Tax' ELSE TI.vcTaxName END AS VARCHAR(100)) AS vcTaxName
			,TD.decTaxPercentage
			,TD.tintTaxType
			,DTD.numTaxItemID
			,TD.numTaxID
			,TD.numCountryID
			,TD.numStateID
			,DTD.numDivisionID
			,DTD.bitApplicable
      FROM
      DivisionTaxTypes DTD
      LEFT JOIN
      tt_TAXDETAIL TD
      ON
      DTD.numTaxItemID = TD.numTaxItemID
      LEFT JOIN
      TaxItems TI
      ON
      TD.numDomainId = TI.numDomainID
      AND TD.numTaxItemID = TI.numTaxItemID
      WHERE
      coalesce(DTD.bitApplicable,false) = true
      AND numDivisionID = v_numDivisionId
      AND TD.numDomainId = v_numDomainId
      UNION
      SELECT CAST(CAST('CRV' AS VARCHAR(9)) AS VARCHAR(100)),TD.decTaxPercentage,TD.tintTaxType,1,numTaxID,TD.numCountryID,TD.numStateID,v_numDivisionId,CAST(1 AS BOOLEAN) FROM tt_TAXDETAIL TD WHERE numTaxItemID = 1;
   end if;

   open SWV_RefCur4 for
   SELECT * FROM  tt_APPLICABLETAXESFORDIVISION;
  
   INSERT INTO
   tt_TAXAMOUNT
   SELECT
   I.numItemCode,
		TT.numTaxItemID,
		TT.bitApplicable,
		TT1.decTaxPercentage,
		TT1.vcTaxName,
		TT.monTotAmount,
		I.bitTaxable,
		CASE
   WHEN TT1.tintTaxType = 2 --FLAT AMOUNT
   THEN(CAST(TT1.decTaxPercentage AS DOUBLE PRECISION)*(CAST(numUnitHour AS DOUBLE PRECISION)*fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainId,coalesce(TT.numUOM,0))))
   ELSE --PERCENT
				((CAST(TT.monTotAmount AS DOUBLE PRECISION)*CAST(TT1.decTaxPercentage AS DOUBLE PRECISION))/100)
   END AS fltTaxAmount
   FROM
   tt_CARTITEMS TT
   INNER JOIN
   tt_APPLICABLETAXESFORDIVISION TT1
   ON
   TT.numTaxItemID = TT1.numTaxItemID
   AND 1 =(CASE WHEN TT.numTaxItemID = 1 THEN(CASE WHEN TT.numTaxID = TT1.numTaxID THEN 1 ELSE 0 END) ELSE 1 END)
   INNER JOIN
   Item I
   ON
   I.numItemCode = TT.numItemCode
   WHERE
   I.bitTaxable = true ORDER BY numItemCode;
  
   open SWV_RefCur5 for
   SELECT numItemCode,numTaxItemID ,bitApplicable  ,fltTaxAmount AS fltTaxAmount  FROM  tt_TAXAMOUNT;

   open SWV_RefCur6 for
   SELECT numItemCode ,SUM(fltTaxAmount) AS fltTaxAmount  FROM  tt_TAXAMOUNT GROUP BY numItemCode; 
  
  
   IF EXISTS(SELECT * FROM tt_TAXAMOUNT) then
	
      select   SUM(fltTaxAmount) INTO v_fltTotalTaxAmount FROM  tt_TAXAMOUNT WHERE bitTaxable = true;
   end if;

	
	
	
   RETURN;
END; $$;


