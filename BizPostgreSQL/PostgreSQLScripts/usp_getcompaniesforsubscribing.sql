-- Stored procedure definition script USP_GetCompaniesForSubscribing for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCompaniesForSubscribing(v_vcSearch VARCHAR(100) DEFAULT '',    
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select D.numDivisionID,A.numContactId,cast(vcCompanyName as VARCHAR(255)),cast(coalesce(vcData,'-') as VARCHAR(255)) as Employees,
 cast(vcWebsite as VARCHAR(255)),coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') as Name,vcEmail from CompanyInfo C
   Join DivisionMaster D
   on C.numCompanyId = D.numCompanyID
   Join AdditionalContactsInformation A
   on A.numDivisionId = D.numDivisionID
   left join Listdetails
   on numListItemID = numNoOfEmployeesID
   where coalesce(A.bitPrimaryContact,false) = true and D.numDomainID = v_numDomainID and D.numDivisionID not in(select cast(numDivisionid as NUMERIC(18,0)) from Subscribers where numDomainID = v_numDomainID and bitDeleted = false)
   and vcCompanyName ilike '%' || coalesce(v_vcSearch,'') || '%';
END; $$;












