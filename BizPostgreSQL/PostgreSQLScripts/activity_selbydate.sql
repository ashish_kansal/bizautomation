CREATE OR REPLACE FUNCTION Activity_SelByDate
(
	v_OrganizerName VARCHAR(64), -- resource name (will be used to lookup ID)
	v_StartDateTimeUtc TIMESTAMP, -- the start date before which no activities are retrieved  
	v_EndDateTimeUtc TIMESTAMP, -- the end date, any activities starting after this date are excluded  
	INOUT SWV_RefCur Refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_ResourceID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_OrganizerName) = true then
      v_ResourceID := CAST(v_OrganizerName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if; 

	OPEN SWV_RefCur FOR
	SELECT
		Activity.AllDayEvent,
		Activity.ActivityDescription,
		Activity.Duration,
		Activity.Location,
		Activity.ActivityID,
		Activity.StartDateTimeUtc,
		cast(Activity.Subject as VARCHAR(255)),
		Activity.EnableReminder,
		Activity.ReminderInterval,
		Activity.ShowTimeAs,
		Activity.Importance,
		Activity.Status,
		Activity.RecurrenceID,
		Activity.VarianceID,
		Activity._ts,
		Activity.itemId,
		Activity.ChangeKey,
		cast(coalesce(Activity.Color,0) as SMALLINT) as Color,cast(coalesce(LastSnoozDateTimeUtc,'1900-01-01 00:00:00.000') as TIMESTAMP) AS LastSnoozDateTimeUtc
	FROM
		Activity 
	INNER JOIN 
		ActivityResource 
	ON 
		Activity.ActivityID = ActivityResource.ActivityID
	WHERE
	ActivityResource.ResourceID = v_ResourceID 
	AND (((Activity.StartDateTimeUtc >= v_StartDateTimeUtc  AND  Activity.StartDateTimeUtc < v_EndDateTimeUtc) OR  Activity.RecurrenceID <> -999) AND Activity.OriginalStartDateTimeUtc IS NULL);
END; $$;


