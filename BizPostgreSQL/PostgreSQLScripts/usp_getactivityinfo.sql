-- FUNCTION: public.usp_getactivityinfo(numeric, integer, refcursor)

-- DROP FUNCTION public.usp_getactivityinfo(numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getactivityinfo(
	v_numactivityid numeric DEFAULT 0,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 14:56:56 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for Select  cast(CI.vcCompanyName as VARCHAR(255)),coalesce(AD.vcFirstName,'-') || ' ' || coalesce(AD.vcLastname,'') AS ContactName,AD.vcEmail,AD.numPhone As numPhone,
AD.numDivisionId,AD.numContactId,AC.StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtStartTime,
StartDateTimeUtc+CAST(AC.Duration || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtEndTime,AC.ActivityID As numActivityId,
cast((Select cast(numListItemID as VARCHAR(255)) from Listdetails where vcData = 'Calendar' and numListID = 73 and constFlag = true) as VARCHAR(255)) As bitTask,AC.Priority,
AC.Activity,
AC.FollowUpStatus,
AC.Comments
   from Activity AC
   Inner Join ActivityAttendees AT on AC.ActivityID = AT.ActivityID
   Inner Join CompanyInfo CI on AT.CompanyNameinBiz = CAST(CI.numCompanyId AS VARCHAR(200))
   Inner join AdditionalContactsInformation AD on  AT.DivisionID = AD.numDivisionId ---and AT.ContactID=AD.numContactId
   Where AC.ActivityID = 252157
   order by AttendeeID DESC LIMIT 1;
END;
$BODY$;

ALTER FUNCTION public.usp_getactivityinfo(numeric, integer, refcursor)
    OWNER TO postgres;
