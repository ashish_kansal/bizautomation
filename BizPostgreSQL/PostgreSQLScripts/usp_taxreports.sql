DROP FUNCTION IF EXISTS USP_TaxReports;

CREATE OR REPLACE FUNCTION USP_TaxReports(v_numDomainID NUMERIC(18,0),
    v_dtFromDate TIMESTAMP,
    v_dtToDate TIMESTAMP,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE 
		v_bitDiscountOnUnitPrice Boolean;
BEGIN
	SELECT COALESCE(bitDiscountOnUnitPrice,false) INTO v_bitDiscountOnUnitPrice FROM Domain WHERE numDomainID=v_numDomainID;


   open SWV_RefCur for SELECT
   T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType = 1 then TaxAmount else 0 end) AS SalesTax,
		SUM(Case When T.tintOppType = 2 then TaxAmount else 0 end) AS PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		COALESCE((SELECT SUM(CASE WHEN v_bitDiscountOnUnitPrice=true THEN monTotAmount ELSE monTotAmtBefDiscount END) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
   FROM(SELECT
      OBD.numOppBizDocsId,
			0 AS numReturnHeaderID1,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintopptype,
			coalesce(fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			FormatedDateFromDate(OBD.dtCreatedDate,v_numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			coalesce(fn_getOPPState(OM.numOppId,v_numDomainID,2::SMALLINT),'') AS vcShipState,
			OBD.monDealAmount AS GrandTotal
      FROM OpportunityMaster OM
      JOIN OpportunityBizDocs OBD
      ON OM.numOppId = OBD.numoppid
      JOIN OpportunityMasterTaxItems OMTI
      ON OMTI.numOppId = OBD.numoppid
      LEFT JOIN DivisionMaster DM
      ON OM.numDivisionId = DM.numDivisionID
      LEFT JOIN CompanyInfo CI
      ON DM.numCompanyID = CI.numCompanyId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OBD.bitAuthoritativeBizDocs = 1
      AND OMTI.fltPercentage > 0
      AND OBD.dtFromDate BETWEEN  v_dtFromDate AND v_dtToDate
      UNION
      SELECT
      CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
			RH.numReturnHeaderID,
		    RH.vcRMA,
			OMTI.*,
			OM.tintopptype,
			RH.monTotalTax*-1  AS TaxAmount,
			FormatedDateFromDate(RH.dtCreatedDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			coalesce(fn_getOPPState(OM.numOppId,212::NUMERIC,2::SMALLINT),'') AS vcShipState,
			RH.monBizDocAmount  AS GrandTotal
      FROM
      ReturnHeader RH
      INNER JOIN OpportunityMaster OM ON RH.numOppId = OM.numOppId
      JOIN OpportunityMasterTaxItems OMTI
      ON OMTI.numOppId = RH.numOppId
      LEFT JOIN DivisionMaster DM
      ON RH.numDivisionId = DM.numDivisionID
      LEFT JOIN CompanyInfo CI
      ON DM.numCompanyID = CI.numCompanyId
      WHERE
      RH.numDomainID = v_numDomainID
      AND RH.dtCreatedDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) BETWEEN  v_dtFromDate AND v_dtToDate
      AND RH.numReturnStatus = 303) T
   JOIN(SELECT
      numTaxItemID ,
			vcTaxName 
      FROM
      TaxItems
      WHERE
      numDomainID = v_numDomainID
      UNION
      SELECT
      0,
			'Sales Tax'
      UNION
      SELECT
      1,
			'CRV') TI
   ON T.numTaxItemId = TI.numTaxItemID
   WHERE
   TaxAmount <> 0
   GROUP BY
   T.vcBizDocID,TI.vcTaxName,T.fltPercentage,T.tintTaxType,T.INVCreatedDate,
   T.Organization,T.vcShipState,T.GrandTotal,T.numOppBizDocsId,T.numReturnHeaderID,
   T.numOppId;
END; $$;












