-- Stored procedure definition script USP_GetPrimaryContactOPP for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPrimaryContactOPP(v_OpportunityId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   A.vcFirstName,
A.vcLastname,
CONCAT(A.numPhone,',',A.numPhoneExtension) as numPhone,
 A.vcEmail,
 cast(C.vcCompanyName as VARCHAR(255)),
 A.vcFax,
A.charSex,
 A.bintDOB,
 GetAge(A.bintDOB,TIMEZONE('UTC',now())) as Age,
 A.txtNotes,
 A.numHomePhone,
 CONCAT(A.VcAsstFirstName,' ',A.vcAsstLastName) as vcAsstName,
 CONCAT(A.numAsstPhone,',',A.numAsstExtn) as numAsstPhone,
    A.vcAsstEmail,
 A.charSex,
 coalesce(vcStreet,'') || coalesce(vcCity,'') || ' ,'
   || coalesce(fn_GetState(numState),'') || ' '
   || coalesce(vcPostalCode,'') || ' <br>'
   || coalesce(fn_GetListItemName(numCountry),'') AS Address,
  A.numContactType
   FROM OpportunityMaster opp
   join AdditionalContactsInformation A on
   A.numContactId = opp.numContactId
   join DivisionMaster D
   on A.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   LEFT JOIN AddressDetails AD ON AD.numRecordID = A.numContactId
   AND AD.tintAddressOf = 1
   AND AD.tintAddressType = 0
   AND AD.bitIsPrimary = true
   AND AD.numDomainID = A.numDomainID
   WHERE opp.numOppId = v_OpportunityId;
END; $$;












