-- Stored procedure definition script USP_SaveCustomTabLayoutList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCustomTabLayoutList(v_numUserCntID NUMERIC DEFAULT 0,                                  
v_numDomainID NUMERIC DEFAULT 0,                         
v_strRow TEXT DEFAULT '',                      
v_intcoulmn NUMERIC DEFAULT 0,   
v_numFormID NUMERIC DEFAULT 0,                          
v_numRelCntType NUMERIC DEFAULT 0,
v_tintPageType SMALLINT DEFAULT NULL,
v_numTabID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete  FROM DycFormConfigurationDetails where numUserCntID = v_numUserCntID and numDomainID = v_numDomainID
   and intColumnNum >= v_intcoulmn and  numRelCntType = v_numRelCntType AND numFormID = v_numFormID
   AND tintPageType = v_tintPageType and numAuthGroupID = v_numTabID;

   if SUBSTR(CAST(v_strRow AS VARCHAR(10)),1,10) <> '' then

		INSERT INTO DycFormConfigurationDetails
		(numFormID,numFieldID,intRowNum,intColumnNum,numUserCntID,numDomainID,numRelCntType,bitCustom,tintPageType,numAuthGroupID)
		SELECT 
			v_numFormID,numFieldID,tintRow,intColumn,numUserCntID,numDomainID,numRelCntType,bitCustomField,v_tintPageType,v_numTabID 
		FROM
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFieldID NUMERIC(9,0) PATH 'numFieldID',
				tintRow NUMERIC(9,0) PATH 'tintRow',
				intColumn NUMERIC(9,0) PATH 'intColumn',
				numUserCntId NUMERIC(9,0) PATH 'numUserCntId',
				numDomainID NUMERIC(9,0) PATH 'numDomainID',
				numRelCntType NUMERIC(9,0) PATH 'numRelCntType',
				bitCustomField BOOLEAN PATH 'bitCustomField'
		) AS X; 
	  
   end if;                          

   RETURN;
END; $$;


