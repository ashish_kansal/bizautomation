-- Stored procedure definition script USP_GetSalesTemplateItemsByItemId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSalesTemplateItemsByItemId(v_numSalesTemplateID NUMERIC,
    v_numDomainID NUMERIC,
	v_numItemCode NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numSalesTemplateID > 0) then
	
      open SWV_RefCur for
      SELECT  X.*, (SELECT  vcCategoryName FROM Category WHERE numCategoryID IN(SELECT numCategoryID FROM ItemCategory IC WHERE X.numItemCode = IC.numItemID) LIMIT 1) AS vcCategoryName
      FROM(SELECT  DISTINCT
         STI.numSalesTemplateItemID,
                        STI.numSalesTemplateID,
                        STI.numSalesTemplateItemID AS numoppitemtCode,
						ST.vcTemplateName AS vcTemplateName,
                        STI.numItemCode,
                        STI.numUnitHour,
                        STI.monPrice,
                        STI.monTotAmount,
                        STI.numSourceID,
                        STI.numWarehouseID,
                        STI.Warehouse,
                        STI.numWarehouseItmsID,
                        STI.ItemType,
                        STI.ItemType as vcType,
                        STI.Attributes,
						STI.Attributes as vcAttributes,
                        STI.AttrValues,
                        I.bitFreeShipping AS FreeShipping, --Use from Item table
                        STI.Weight,
                        STI.Op_Flag,
                        STI.bitDiscountType,
                        STI.fltDiscount,
                        STI.monTotAmtBefDiscount,
                        STI.ItemURL,
                        STI.numDomainID,
                        I.txtItemDesc AS vcItemDesc,
                       (SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) AS vcPathForTImage,
                        I.vcItemName,
                        false AS DropShip,
					    false as bitDropShip,
                        I.bitTaxable,
                        coalesce(STI.numUOM,0) AS numUOM,
						coalesce(STI.numUOM,0) as numUOMId,
                        coalesce(STI.vcUOMName,'') AS vcUOMName,
                        coalesce(STI.UOMConversionFactor,1) AS UOMConversionFactor,
						coalesce(STI.numVendorWareHouse,0) as numVendorWareHouse,coalesce(STI.numShipmentMethod,0) as numShipmentMethod,coalesce(STI.numSOVendorId,0) as numSOVendorId,
						false as bitWorkOrder,I.charItemType,fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder
--                        ISNULL(dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numdomainid,0),1) UOMConversionFactor
         FROM      SalesTemplateItems STI
         LEFT JOIN OpportunitySalesTemplate AS ST ON STI.numSalesTemplateID = ST.numSalesTemplateID
         INNER JOIN Item I ON I.numItemCode = STI.numItemCode
         LEFT JOIN UOM U ON U.numUOMId = I.numSaleUnit
         WHERE     STI.numSalesTemplateID = v_numSalesTemplateID
         AND STI.numSalesTemplateItemID = v_numItemCode
         AND STI.numDomainID = v_numDomainID) X;
   ELSE
      open SWV_RefCur for
      SELECT  X.*, (SELECT  vcCategoryName FROM Category WHERE numCategoryID IN(SELECT numCategoryID FROM ItemCategory IC WHERE X.numItemCode = IC.numItemID) LIMIT 1) AS vcCategoryName
      FROM(SELECT  DISTINCT
         I.numItemCode AS numSalesTemplateItemID,
                        0 AS numSalesTemplateID,
                        I.numItemCode AS numoppitemtCode,
						'' AS vcTemplateName,
                        I.numItemCode,
                        1 AS numUnitHour,
                        I.monListPrice AS monPrice,
                        I.monListPrice AS monTotAmount,
                        0 AS numSourceID,
                        0 AS numWarehouseID,
                        '' AS Warehouse,
                        0 AS numWarehouseItmsID,
                        I.charItemType AS ItemType,
                        I.charItemType as vcType,
                        0 AS Attributes,
						'' as vcAttributes,
                        0 AS AttrValues,
                        0 as  FreeShipping, --Use from Item table
                        I.fltWeight AS Weight,
                        0 as Op_Flag,
                        false as bitDiscountType,
                        0 as fltDiscount,
                        0 as monTotAmtBefDiscount,
                        '' as ItemURL,
                        I.numDomainID,
                        I.txtItemDesc AS vcItemDesc,
                       (SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) AS vcPathForTImage,
                        I.vcItemName,
                        'False' AS DropShip,
					    false as bitDropShip,
                        I.bitTaxable,
                        coalesce(I.numBaseUnit,0) AS numUOM,
						coalesce(I.numBaseUnit,0) as numUOMId,
						'' as vcUOMName,
                        0 as  UOMConversionFactor,
						0 as numVendorWareHouse,0 as numShipmentMethod,0 as numSOVendorId,
						false as bitWorkOrder,I.charItemType,fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder
         FROM      Item I
         LEFT JOIN UOM U ON U.numUOMId = I.numSaleUnit
         WHERE     I.numItemCode = v_numItemCode
         AND I.numDomainID = v_numDomainID) X;
   end if;
   RETURN;
END; $$;


