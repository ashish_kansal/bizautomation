-- Stored procedure definition script usp_GenDocDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GenDocDetails(v_byteMode SMALLINT DEFAULT null,                          
v_GenDocId NUMERIC(9,0) DEFAULT null,                
v_numUserCntID NUMERIC(9,0) DEFAULT 0 ,    
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select
      numGenericDocID AS "numGenericDocID",
 VcFileName AS "VcFileName" ,
 VcDocName AS "VcDocName" ,
 numDocCategory AS "numDocCategory",
 vcfiletype AS "vcfiletype",
 numDocStatus AS "numDocStatus",
 vcDocdesc AS "vcDocdesc",
 vcSubject AS "vcSubject",
 coalesce(cUrlType,'') AS "cUrlType",
numCreatedBy AS "numCreatedBy",
case when coalesce(vcDocumentSection,'') = 'M' then 'true' else 'false' end as "isForMarketingDept",
 fn_GetContactName(numCreatedBy) as "RecordOwner",
 fn_GetContactName(numCreatedBy) || ', ' || CAST(bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as "Created" ,
 fn_GetContactName(numModifiedBy) || ', ' || CAST(bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as "Modified",
 (select count(*) from DocumentWorkflow where numDocID = v_GenDocId and numContactID = v_numUserCntID and cDocType = 'D' and tintApprove = 0) as "AppReq" ,
 (select count(*) from DocumentWorkflow
         where numDocID = v_GenDocId and cDocType = 'D' and tintApprove = 1) as "Approved",
 (select count(*) from DocumentWorkflow
         where numDocID = v_GenDocId and cDocType = 'D' and tintApprove = 2) as "Declined",
 (select count(*) from DocumentWorkflow
         where numDocID = v_GenDocId and cDocType = 'D' and tintApprove = 0) as "Pending",
 coalesce(tintCheckOutStatus,0) AS "tintCheckOutStatus",
 coalesce(numLastCheckedOutBy,0) AS "numLastCheckedOutBy",
 fn_GetContactName(numLastCheckedOutBy) || ', ' || CAST(dtCheckOutDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as "CheckedOut",
 coalesce(numModuleId,0) AS numModuleID,coalesce(vcContactPosition,'') AS "vcContactPosition",
 BizDocOppType AS "BizDocOppType",
 BizDocType AS "BizDocType",
 BizDocTemplate AS "BizDocTemplate",
 coalesce(numCategoryId,0) AS "numCategoryId",
 coalesce(bitLastFollowupUpdate,false) AS "bitLastFollowupUpdate",
 coalesce(vcGroupsPermission,'') AS "vcGroupsPermission",
 coalesce(numFollowUpStatusId,0) AS "numFollowUpStatusId",
 coalesce(bitUpdateFollowupStatus,false) AS "bitUpdateFollowupStatus"
      from GenericDocuments where numGenericDocID = v_GenDocId  and    numDomainId = v_numDomainID;
   end if;                          

   if v_byteMode = 1 then

      delete from DocumentWorkflow where numDocID = v_GenDocId and cDocType = 'D';
      delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId = v_GenDocId and btDocType = 2);
      delete from BizActionDetails where numOppBizDocsId = v_GenDocId and btDocType = 2;
      delete from GenericDocuments where numGenericDocID = v_GenDocId  and numDomainId = v_numDomainID;
   end if;          
                        
   if v_byteMode = 2 then

      open SWV_RefCur for
      select  numGenericDocID AS "numGenericDocID",
 VcFileName AS "VcFileName"  from GenericDocuments where numGenericDocID = v_GenDocId and    numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


