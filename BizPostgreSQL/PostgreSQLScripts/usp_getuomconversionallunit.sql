-- Stored procedure definition script USP_GetUOMConversionAllUnit for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUOMConversionAllUnit(v_numDomainID NUMERIC(9,0),    
 v_numUOMId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(u.numUOMId as VARCHAR(255)),cast(u.vcUnitName as VARCHAR(255)),Cast(u.numUOMId AS VARCHAR(20)) || '~' || CAST(fn_UOMConversion(u.numUOMId,0::NUMERIC,v_numDomainID,v_numUOMId) AS VARCHAR(20)) AS UOMConversionFactor FROM
   UOM u INNER JOIN Domain d ON u.numDomainID = d.numDomainId
   WHERE u.numDomainID = v_numDomainID AND d.numDomainId = v_numDomainID AND
   u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END);
/****** Object:  StoredProcedure [dbo].[USP_GetUserCommission_CommRule]    Script Date: 02/28/2009 13:57:17 ******/
   RETURN;
END; $$;












