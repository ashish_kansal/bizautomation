-- Stored procedure definition script USP_DeleteVendor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteVendor(v_numVendorID NUMERIC(9,0),
v_numItemCode NUMERIC(9,0),
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from Vendor
   where numVendorID = v_numVendorID
   AND numItemCode = v_numItemCode
   and numDomainID = v_numDomainID;

   IF exists(SELECT * FROM Item WHERE numItemCode = v_numItemCode AND numVendorID = v_numVendorID AND numDomainID = v_numDomainID) then

      UPDATE Item SET numVendorID = NULL WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;



