-- Stored procedure definition script USP_ManageComapnyTaxTypes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageComapnyTaxTypes(v_numDivisionID NUMERIC(9,0),
v_strCompTaxTypes VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from DivisionTaxTypes where numDivisionID = v_numDivisionID;    
  
   insert into DivisionTaxTypes(numDivisionID,numTaxItemID,bitApplicable)
	select 
		v_numDivisionID
		,X.numTaxItemID
		,X.bitApplicable 
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strCompTaxTypes AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numTaxItemID NUMERIC(9,0) PATH 'numTaxItemID',
			bitApplicable BOOLEAN PATH 'bitApplicable'
	) AS X;

   RETURN;
END; $$;  




