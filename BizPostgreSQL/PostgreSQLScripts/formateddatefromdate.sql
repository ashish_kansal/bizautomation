-- Function definition script FormatedDateFromDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION FormatedDateFromDate(v_date timestamp without time zone,v_numDomainID NUMERIC)
RETURNS VARCHAR(30) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strDateFormat  VARCHAR(30);
   v_strDate  VARCHAR(30);
   v_str4Yr  VARCHAR(4);
   v_str2Yr  VARCHAR(2);
   v_strIntMonth  VARCHAR(2);
   v_strDay  VARCHAR(2);
   v_str3LetMon  VARCHAR(3);
   v_strFullMonth  VARCHAR(15);
BEGIN
   IF swf_isdate(v_date:: text) = 0 then

      v_date := null;
   end if;

   select   vcDateFormat INTO v_strDateFormat from Domain where numDomainId = v_numDomainID;
   v_str4Yr := CAST(EXTRACT(Year FROM CAST(v_date AS TIMESTAMP)) AS VARCHAR(4));
   v_str2Yr := SUBSTR(CAST(EXTRACT(Year FROM CAST(v_date AS TIMESTAMP)) AS VARCHAR(4)),3,4);
   v_strFullMonth := TO_CHAR(CAST(v_date AS TIMESTAMP),'Month');
   v_str3LetMon := TO_CHAR(CAST(v_date AS TIMESTAMP),'Mon');
   v_strIntMonth := CAST(EXTRACT(month FROM CAST(v_date AS TIMESTAMP)) AS VARCHAR(2)); 
   v_strDay := CAST(EXTRACT(day FROM CAST(v_date AS TIMESTAMP)) AS VARCHAR(2));
   if cast(NULLIF(v_strIntMonth,'') as INTEGER) <= 9 then 
      v_strIntMonth := '0' || coalesce(v_strIntMonth,'');
   end if;
   if cast(NULLIF(v_strDay,'') as INTEGER) <= 9 then 
      v_strDay := '0' || coalesce(v_strDay,'');
   end if;   

   v_strDate := Replace(v_strDateFormat,'DD',v_strDay);
   v_strDate := Replace(v_strDate,'YYYY',v_str4Yr);
   v_strDate := Replace(v_strDate,'MM',v_strIntMonth);
   v_strDate := Replace(v_strDate,'YY',v_str2Yr);
   v_strDate := Replace(v_strDate,'MONTH',v_strFullMonth);
   v_strDate := Replace(v_strDate,'MON',v_str3LetMon);


   Return v_strDate;
END; $$;

