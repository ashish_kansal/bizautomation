DROP FUNCTION IF EXISTS fn_GetNewvcEmailString;

CREATE OR REPLACE FUNCTION fn_GetNewvcEmailString(v_vcEmailId VARCHAR(2000),
v_numDomianID NUMERIC(18,0) DEFAULT NULL)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcNewEmailId TEXT;
   v_Email  VARCHAR(200);
   v_EmailName  VARCHAR(200);
   v_EmailAdd  VARCHAR(200);
   v_StartPos  INTEGER;
   v_EndPos  INTEGER;
   v_numEmailid  NUMERIC(9,0);
---DECLARE @numDomainId AS NUMERIC(18, 0)

BEGIN
   v_vcNewEmailId := '';
   IF v_vcEmailId <> '' then
    
      WHILE POSITION('#^#' IN v_vcEmailId) > 0 LOOP
         v_Email := LTRIM(RTRIM(SUBSTR(v_vcEmailId,1,POSITION('#^#' IN v_vcEmailId)+2)));
         IF POSITION('$^$' IN v_Email) > 0 then
                    
            v_EmailName := LTRIM(RTRIM(SUBSTR(v_Email,0,POSITION('$^$' IN v_Email))));
            v_EmailAdd := LTRIM(RTRIM(SUBSTR(v_Email,LENGTH(v_EmailName)+1,POSITION('$^$' IN v_Email)+LENGTH(v_Email) -1)));
            v_EmailAdd := REPLACE(v_EmailAdd,'$^$','');
            v_EmailAdd := REPLACE(v_EmailAdd,'#^#','');
         end if;
         v_vcNewEmailId := coalesce(v_vcNewEmailId,'') ||(SELECT  CAST(coalesce(numEmailId,0) AS VARCHAR(30)) FROM EmailMaster WHERE EmailMaster.vcEmailId = v_EmailAdd AND numDomainID = v_numDomianID LIMIT 1) || '$^$' || coalesce(v_EmailName,'') || '$^$' || coalesce(v_EmailAdd,'')   || '#^#';
				--PRINT @Email
				--SET @vcNewEmailId = @vcNewEmailId + ','
         v_vcEmailId := LTRIM(RTRIM(SUBSTR(v_vcEmailId,POSITION('#^#' IN v_vcEmailId)+3,LENGTH(v_vcEmailId))));
      END LOOP;
   end if;
   IF LENGTH(v_vcNewEmailId) > 0 then
			
      v_vcNewEmailId := SUBSTR(v_vcNewEmailId,1,LENGTH(v_vcNewEmailId) -3);
   end if;                
   RETURN v_vcNewEmailId;
END; $$;

