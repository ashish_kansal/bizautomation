DROP FUNCTION IF EXISTS USP_GetReceivePayment;

CREATE OR REPLACE FUNCTION USP_GetReceivePayment(v_numDomainId NUMERIC(9,0) DEFAULT 0 ,
    v_numDepositID NUMERIC DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,
    v_numCurrencyID NUMERIC(9,0) DEFAULT NULL,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0,
	v_numCurrentPage INTEGER DEFAULT 1, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPageSize  INTEGER DEFAULT 40;

   v_numDiscountItemID  NUMERIC(18,0);
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_BaseCurrencySymbol  VARCHAR(3);
BEGIN
   select   coalesce(numDiscountServiceItemID,0), coalesce(numShippingServiceItemID,0) INTO v_numDiscountItemID,v_numShippingServiceItemID FROM Domain WHERE numDomainId = v_numDomainId;
   RAISE NOTICE '%',v_numDiscountItemID;
			
   v_BaseCurrencySymbol := '$';
   v_BaseCurrencySymbol := COALESCE((select varCurrSymbol FROM Currency WHERE numCurrencyID IN(SELECT numCurrencyID FROM Domain WHERE numDomainId = v_numDomainId)),'$');
    
   DROP TABLE IF EXISTS tt_TEMPORG CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPORG ON COMMIT DROP AS
      SELECT T.numDivisionID,T.bitChild,fn_GetComapnyName(T.numDivisionID) AS vcCompanyName,AD2.vcCity
	,coalesce(fn_GetState(AD2.numState),'') AS vcState
	
      FROM(SELECT v_numDivisionID AS numDivisionID,false AS bitChild
      UNION
      SELECT CA.numDivisionID,CAST(1 AS BOOLEAN) AS bitChild
      FROM CompanyAssociations AS CA
      WHERE CA.numDomainID = v_numDomainId AND  CA.numAssociateFromDivisionID = v_numDivisionID AND CA.bitChildOrg = true) AS T
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = v_numDomainId
      AND AD2.numRecordID = T.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppBizDocID NUMERIC(18,0)
   );

   INSERT INTO tt_TEMP(numOppBizDocID)
   SELECT
   numOppBizDocsId
   FROM(SELECT
      OpportunityBizDocs.numOppBizDocsId
			,T.bitChild
			,OpportunityBizDocs.dtCreatedDate
      FROM
      OpportunityMaster
      INNER JOIN
      tt_TEMPORG AS T
      ON
      OpportunityMaster.numDivisionId = T.numDivisionID
      INNER JOIN
      OpportunityBizDocs
      ON
      OpportunityMaster.numOppId = OpportunityBizDocs.numoppid
      INNER JOIN
      DepositeDetails
      ON
      DepositeDetails.numOppBizDocsID = OpportunityBizDocs.numOppBizDocsId
      WHERE
      OpportunityBizDocs.bitAuthoritativeBizDocs = 1
      AND OpportunityMaster.tintopptype = 1
      AND OpportunityMaster.numDomainId = v_numDomainId
      AND DepositeDetails.numDepositID = v_numDepositID
      AND (coalesce(OpportunityMaster.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
      UNION
      SELECT
      numOppBizDocsId
			,T.bitChild
			,OpportunityBizDocs.dtCreatedDate
      FROM
      OpportunityMaster
      INNER JOIN
      OpportunityBizDocs
      ON
      OpportunityMaster.numOppId = OpportunityBizDocs.numoppid
      INNER JOIN
      tt_TEMPORG AS T
      ON
      OpportunityMaster.numDivisionId = T.numDivisionID
      WHERE
      OpportunityMaster.numDomainId = v_numDomainId
      AND OpportunityMaster.tintopptype = 1
      AND (OpportunityMaster.numCurrencyID = v_numCurrencyID OR  v_numCurrencyID = 0)
      AND (coalesce(OpportunityMaster.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
      AND OpportunityBizDocs.bitAuthoritativeBizDocs = 1
      AND (v_numDepositID = 0 OR (v_numDepositID > 0 AND T.bitChild = false))
      AND OpportunityBizDocs.monDealAmount -OpportunityBizDocs.monAmountPaid > 0
      AND(SELECT COUNT(*) FROM DepositeDetails WHERE numOppID = OpportunityMaster.numOppId AND numOppBizDocsID = OpportunityBizDocs.numOppBizDocsId AND numDepositID = v_numDepositID) = 0) TEMP
   ORDER BY
   bitChild,dtCreatedDate DESC;


   DROP TABLE IF EXISTS tt_TEMPFINAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFINAL
   (
      numOppBizDocID NUMERIC(18,0)
   );

   INSERT INTO tt_TEMPFINAL  SELECT numOppBizDocID FROM tt_TEMP ORDER BY ID OFFSET (v_numCurrentPage - 1) * v_numPageSize FETCH NEXT v_numPageSize ROWS ONLY;


   open SWV_RefCur for
   SELECT
   om.vcpOppName ,
		om.numOppId ,
		OBD.numOppBizDocsId ,
		OBD.vcBizDocID ,
		OBD.numBizDocStatus ,
		OBD.vcComments ,
		OBD.monAmountPaid AS monAmountPaid ,
		OBD.monDealAmount ,
		OBD.monCreditAmount AS monCreditAmount ,
		coalesce(C.varCurrSymbol,'') AS varCurrSymbol ,
		FormatedDateFromDate(dtFromDate,v_numDomainId) AS dtFromDate ,
		dtFromDate AS dtOrigFromDate,
		CASE coalesce(om.bitBillingTerms,false)
   WHEN true
   THEN FormatedDateFromDate(dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(om.intBillingDays,0)),0) AS INTEGER) || 'day' as interval),v_numDomainId)
   WHEN false
   THEN FormatedDateFromDate(OBD.dtFromDate,v_numDomainId)
   END AS dtDueDate ,
		om.numDivisionId ,
		(OBD.monDealAmount -coalesce(OBD.monCreditAmount,0) -coalesce(OBD.monAmountPaid,0)+coalesce(DD.monAmountPaid,0)) AS baldue ,
		T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
		DD.numDepositID ,
		DD.monAmountPaid AS monAmountPaidInDeposite ,
		coalesce(DD.numDepositeDetailID,0) AS numDepositeDetailID,
		OBD.dtCreatedDate,
		om.numContactId,OBD.vcRefOrderNo
		,om.numCurrencyID
		,coalesce(NULLIF(C.fltExchangeRate,0),1) AS fltExchangeRateCurrent
		,coalesce(NULLIF(om.fltExchangeRate,0),1) AS fltExchangeRateOfOrder
		,v_BaseCurrencySymbol AS BaseCurrencySymbol,om.vcpOppName,
		CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numDiscount FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numDiscount,
		CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numDiscountPaidInDays FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numDiscountPaidInDays,
		CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numNetDueInDays FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numNetDueInDays,
		CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocItems OBDI
      JOIN OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
      WHERE 1 = 1 AND numoppid = om.numOppId AND numOppBizDocID = OBD.numOppBizDocsId
      AND OBDI.numItemCode = v_numDiscountItemID
      AND OI.vcItemDesc ilike '%Term Discount%') > 0 THEN 1 ELSE 0 END AS IsDiscountItemAdded,
		(SELECT coalesce(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID = OBD.numOppBizDocsId AND numItemCode = v_numShippingServiceItemID) AS monShippingCharge
		,coalesce(numARAccountID,0) AS numARAccountID
   FROM    OpportunityMaster om
   INNER JOIN OpportunityBizDocs OBD ON om.numOppId = OBD.numoppid
   INNER JOIN tt_TEMPFINAL T1 ON OBD.numOppBizDocsId = T1.numOppBizDocID
   INNER JOIN tt_TEMPORG AS T ON om.numDivisionId = T.numDivisionID
   LEFT OUTER JOIN Listdetails ld ON ld.numListItemID = OBD.numBizDocStatus
   LEFT OUTER JOIN Currency C ON C.numCurrencyID = om.numCurrencyID
   JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
   WHERE
   om.numDomainId = v_numDomainId
   AND om.tintopptype = 1
   AND OBD.bitAuthoritativeBizDocs = 1
   AND (coalesce(om.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
   AND DD.numDepositID = v_numDepositID
   UNION
   SELECT  om.vcpOppName ,
			om.numOppId ,
			OBD.numOppBizDocsId ,
			OBD.vcBizDocID ,
			OBD.numBizDocStatus ,
			OBD.vcComments ,
			OBD.monAmountPaid AS monAmountPaid ,
			OBD.monDealAmount ,
			OBD.monCreditAmount AS monCreditAmount ,
			coalesce(C.varCurrSymbol,'') AS varCurrSymbol ,
			FormatedDateFromDate(dtFromDate,v_numDomainId) AS dtFromDate ,
			dtFromDate AS dtOrigFromDate,
			CASE coalesce(om.bitBillingTerms,false)
   WHEN true
   THEN FormatedDateFromDate(dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(om.intBillingDays,0)),0) AS INTEGER) || 'day' as interval),v_numDomainId)
   WHEN false
   THEN FormatedDateFromDate(OBD.dtFromDate,v_numDomainId)
   END AS dtDueDate ,
			om.numDivisionId ,
			(OBD.monDealAmount -coalesce(OBD.monCreditAmount,0) -coalesce(OBD.monAmountPaid,0)) AS baldue ,
			T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
			0 AS numDepositID ,
			0 AS monAmountPaidInDeposite ,
			0 AS numDepositeDetailID,
			dtCreatedDate,
			om.numContactId,OBD.vcRefOrderNo
			,om.numCurrencyID
			,coalesce(NULLIF(C.fltExchangeRate,0),1) AS fltExchangeRateCurrent
			,coalesce(NULLIF(om.fltExchangeRate,0),1) AS fltExchangeRateOfOrder
			,v_BaseCurrencySymbol AS BaseCurrencySymbol,om.vcpOppName,
			CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numDiscount FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numDiscount,
			CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numDiscountPaidInDays FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numDiscountPaidInDays,
			CASE coalesce(om.bitBillingTerms,false)
   WHEN true THEN(SELECT numNetDueInDays FROM BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainId)
   ELSE 0
   END AS numNetDueInDays,
			CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocItems OBDI
      JOIN OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
      WHERE 1 = 1 AND numoppid = om.numOppId AND numOppBizDocID = OBD.numOppBizDocsId
      AND OBDI.numItemCode = v_numDiscountItemID
      AND OI.vcItemDesc ilike '%Term Discount%') > 0 THEN 1 ELSE 0 END AS IsDiscountItemAdded,
			(SELECT coalesce(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID = OBD.numOppBizDocsId AND numItemCode = v_numShippingServiceItemID) AS monShippingCharge
			,coalesce(numARAccountID,0) AS numARAccountID
   FROM    OpportunityMaster om
   INNER JOIN OpportunityBizDocs OBD ON om.numOppId = OBD.numoppid
   INNER JOIN tt_TEMPFINAL T1 ON OBD.numOppBizDocsId = T1.numOppBizDocID
   INNER JOIN tt_TEMPORG AS T ON om.numDivisionId = T.numDivisionID
   LEFT OUTER JOIN Listdetails ld ON ld.numListItemID = OBD.numBizDocStatus
   LEFT OUTER JOIN Currency C ON C.numCurrencyID = om.numCurrencyID
   WHERE
   om.numDomainId = v_numDomainId
   AND om.tintopptype = 1
   AND OBD.bitAuthoritativeBizDocs = 1
   AND (om.numCurrencyID = v_numCurrencyID OR  v_numCurrencyID = 0)
   AND (coalesce(om.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
   AND (v_numDepositID = 0 OR (v_numDepositID > 0 AND T.bitChild = false))
   AND OBD.monDealAmount -OBD.monAmountPaid > 0
   AND(SELECT COUNT(*) FROM DepositeDetails WHERE numOppID = om.numOppId AND numOppBizDocsID = OBD.numOppBizDocsId AND numDepositID = v_numDepositID) = 0
   ORDER BY
   19,23 DESC;
	
   open SWV_RefCur2 for
   SELECT COUNT(*) AS TotalRecords,v_numPageSize AS PageSize FROM tt_TEMP;

   RETURN;
END; $$;





