-- Stored procedure definition script USP_DeleteCheckCompanyDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCheckCompanyDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,
 v_numCheckCompanyId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Delete from CheckCompanyDetails Where numCheckCompanyId = v_numCheckCompanyId And numDomainId = v_numDomainId;
   RETURN;
END; $$;


