-- Stored procedure definition script USP_PageElementDetail_GetHtml for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PageElementDetail_GetHtml(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numElementID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   coalesce(PageElementDetail.vcHtml,'') AS "vcHtml"
   FROM
   PageElementDetail
   INNER JOIN
   PageElementAttributes
   ON
   PageElementDetail.numAttributeID = PageElementAttributes.numAttributeID
   WHERE
   PageElementDetail.numDomainID = v_numDomainID
   AND PageElementDetail.numSiteID = v_numSiteID
   AND PageElementDetail.numElementID = v_numElementID
   AND PageElementAttributes.vcAttributeName = 'Html Customize';
END; $$;












