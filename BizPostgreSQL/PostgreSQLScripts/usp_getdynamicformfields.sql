-- Stored procedure definition script usp_getDynamicFormFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDynamicFormFields(v_numDomainId NUMERIC(9,0),
    v_cCustomFieldsAssociated VARCHAR(10),
    v_numFormId INTEGER,
	v_numSubFormId NUMERIC(9,0),
    v_numAuthGroupId NUMERIC(9,0),
	v_numBizDocTemplateID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcLocationID  VARCHAR(100);
BEGIN
   v_vcLocationID := '0';
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = v_numFormId;

   DROP TABLE IF EXISTS tt_TEMPAVAILABLEFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAVAILABLEFIELDS
   (
      numFormFieldId  VARCHAR(20),
      vcFormFieldName VARCHAR(100),
      vcFieldType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcDbColumnName VARCHAR(50),
      vcListItemType CHAR(3),
      intColumnNum INTEGER,
      intRowNum INTEGER,
      boolRequired BOOLEAN, 
      numAuthGroupID NUMERIC(18,0),
      vcFieldDataType CHAR(1),
      boolAOIField BOOLEAN,
      numFormID NUMERIC(18,0),
      vcLookBackTableName VARCHAR(50),
      vcFieldAndType VARCHAR(50),
      numSubFormId NUMERIC(18,0),
      numFormGroupId NUMERIC(18,0)
   );
   
   
   DROP TABLE IF EXISTS tt_TEMPADDEDFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPADDEDFIELDS
   (
      numFormFieldId  VARCHAR(20),
      vcFormFieldName VARCHAR(100),
      vcFieldType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcDbColumnName VARCHAR(50),
      vcListItemType CHAR(3),
      intColumnNum INTEGER,
      intRowNum INTEGER,
      boolRequired BOOLEAN, 
      numAuthGroupID NUMERIC(18,0),
      vcFieldDataType CHAR(1),
      boolAOIField BOOLEAN,
      numFormID NUMERIC(18,0),
      vcLookBackTableName VARCHAR(50),
      vcFieldAndType VARCHAR(50),
      numSubFormId NUMERIC(18,0),
      numFormGroupId NUMERIC(18,0)
   );
 
   INSERT INTO tt_TEMPADDEDFIELDS(numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)
   SELECT  CAST(CAST(numFieldID AS VARCHAR(15)) || vcFieldType AS VARCHAR(20)) AS numFormFieldId,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    bitIsRequired as boolRequired,
                    coalesce(numAuthGroupID,0) AS numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
                    coalesce(boolAOIField,false) AS boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONCAT(numFieldID::VARCHAR,'~',coalesce(bitCustom,false)::VARCHAR,'~',coalesce(tintOrder,0)::VARCHAR) AS vcFieldAndType
					,coalesce(numSubFormId,0) AS numSubFormId,
					0 AS numFormGroupId
   FROM    View_DynamicColumns
   WHERE   numFormID = v_numFormId
   AND numAuthGroupID = v_numAuthGroupId
   AND numDomainID = v_numDomainId
   AND coalesce(numRelCntType,0) = v_numBizDocTemplateID
   AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
   AND bitDeleted = false
   UNION
   SELECT  CAST(CAST(numFieldId AS VARCHAR(15)) || 'C' AS VARCHAR(20)) AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType,
					 vcAssociatedControlType,
                    coalesce(numListID,0) AS numListID,
                    vcFieldName AS vcDbColumnName,
                    CAST(CAST(CASE WHEN numListID > 0 THEN 'LI' ELSE '' END AS CHAR(3)) AS CHAR(3)) AS vcListItemType,
                    coalesce(tintColumn,0) as intColumnNum,
                    coalesce(tintRow,0) as intRowNum,
                    coalesce(bitIsRequired,false) as boolRequired,
                    coalesce(numAuthGroupID,0) AS numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
                    coalesce(boolAOIField,false),
                    numFormID,
                    CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(50)) AS vcLookBackTableName,
                    CONCAT(numFieldId::VARCHAR,'~',coalesce(bitCustom,false)::VARCHAR,'~',coalesce(tintOrder,0)::VARCHAR) AS vcFieldAndType
					,coalesce(numSubFormId,0) AS numSubFormId
					,0 AS numFormGroupId
   FROM    View_DynamicCustomColumns
   WHERE   numDomainID = v_numDomainId
   AND numFormID = v_numFormId
   AND numAuthGroupID = v_numAuthGroupId
   AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
   AND Grp_id IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))
   and coalesce(numRelCntType,0) = v_numBizDocTemplateID;         


   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)
   SELECT  CAST(CAST(numFieldID AS VARCHAR(15)) || vcFieldType AS VARCHAR(20)) AS numFormFieldId,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    0 AS intColumnNum,
                    0 AS intRowNum,
                    coalesce(bitRequired,false) AS boolRequired,
                    0 AS numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
                    false AS boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONCAT(numFieldID::VARCHAR,'~',coalesce(bitCustom,0)::VARCHAR,'~',coalesce(tintOrder,0)::VARCHAR) AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
   FROM    View_DynamicDefaultColumns
   WHERE   numFormID = v_numFormId
   AND bitDeleted = false
   AND numDomainID = v_numDomainId
   AND numFieldID NOT IN(SELECT  numFieldID
      FROM    View_DynamicColumns
      WHERE   numFormID = v_numFormId
      AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
      AND numAuthGroupID = v_numAuthGroupId
      AND numDomainID = v_numDomainId
      and coalesce(numRelCntType,0) = v_numBizDocTemplateID)
   UNION
   SELECT  CAST(CAST(Fld_id AS VARCHAR(15)) || 'C' AS VARCHAR(20)) AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    coalesce(L.vcFieldType,'C') as vcFieldType,
                    fld_type as vcAssociatedControlType,
                    coalesce(C.numlistid,0) AS numListID,
                    Fld_label AS vcDbColumnName,
                    CAST(CAST(CASE WHEN C.numlistid > 0 THEN 'LI' ELSE '' END AS CHAR(3)) AS CHAR(3)) AS vcListItemType,
                    0 AS intColumnNum,
                    0 AS intRowNum,
                    coalesce(V.bitIsRequired,false) as boolRequired,
                    0 AS numAuthGroupID,
                    CASE WHEN C.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
                    false AS boolAOIField,
                    CAST(v_numFormId AS NUMERIC(18,0)) AS numFormID,
                    CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(50)) AS vcLookBackTableName,
                    CONCAT(Fld_id::VARCHAR,'~1','~0') AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
   FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldId = C.Fld_id
   JOIN CFW_Loc_Master L on C.Grp_id = L.Loc_id
   WHERE   C.numDomainID = v_numDomainId
   AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))
   AND Fld_id NOT IN(SELECT  numFieldId
      FROM    View_DynamicCustomColumns
      WHERE   numFormID = v_numFormId
      AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
      AND Grp_id IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))
      AND numAuthGroupID = v_numAuthGroupId
      AND numDomainID = v_numDomainId and coalesce(numRelCntType,0) = v_numBizDocTemplateID);
        

		
open SWV_RefCur for SELECT * FROM tt_TEMPAVAILABLEFIELDS WHERE numFormFieldId NOT IN(SELECT numFormFieldId FROM tt_TEMPADDEDFIELDS)
   AND tt_TEMPAVAILABLEFIELDS.vcLookBackTableName =(CASE v_numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(tt_TEMPAVAILABLEFIELDS.vcLookBackTableName,'CSGrid','') END)
   UNION
   SELECT * FROM tt_TEMPADDEDFIELDS WHERE 1 = 1
   AND tt_TEMPADDEDFIELDS.vcLookBackTableName =(CASE v_numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(tt_TEMPADDEDFIELDS.vcLookBackTableName,'CSGrid','') END);

   RETURN;
END; $$;












