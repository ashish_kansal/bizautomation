-- Stored procedure definition script SP_GetChildCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SP_GetChildCategory(v_numAccountId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retval  VARCHAR(8000);  
   v_numParentId  NUMERIC;
   v_numParent  VARCHAR(8000);
BEGIN
   SELECT numParntAcntId INTO v_numParentId FROM Chart_Of_Accounts WHERE numAccountId = v_numAccountId;        
   v_retval := v_retval; 
   While v_numParentId <> 1 LOOP
      PERFORM fn_GetChildCategory(v_numParentId);
      select   numParntAcntId INTO v_numParentId FROM Chart_Of_Accounts WHERE numAccountId = v_numParentId;
   END LOOP;
   RAISE NOTICE '%',v_retval;
   RETURN;
END; $$;  




