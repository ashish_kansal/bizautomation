-- Stored procedure definition script USP_ManageBizDocTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBizDocTemplate(v_numDomainID NUMERIC,
    v_numBizDocID NUMERIC,
    v_numOppType NUMERIC, 
    v_txtBizDocTemplate TEXT,
    v_txtCSS TEXT,
    v_bitEnabled BOOLEAN,
    v_tintTemplateType SMALLINT,
	v_numBizDocTempID NUMERIC,
	v_vcTemplateName VARCHAR(50) DEFAULT '',
	v_bitDefault BOOLEAN DEFAULT false,
	v_numOrientation INTEGER DEFAULT 1,
	v_bitKeepFooterBottom BOOLEAN DEFAULT false,
	v_numRelationship NUMERIC(18,0) DEFAULT 0,
	v_numProfile NUMERIC(18,0) DEFAULT 0,
	v_bitDisplayKitChild BOOLEAN DEFAULT false,
	v_numAccountClass NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      IF (coalesce(v_numRelationship,0) <> 0 OR coalesce(v_numProfile,0) <> 0 OR coalesce(v_numAccountClass,0) <> 0) then
	
         IF(SELECT
         COUNT(numBizDocTempID)
         FROM
         BizDocTemplate
         WHERE
         numDomainID = v_numDomainID
         AND numBizDocID = v_numBizDocID
         AND numOppType = v_numOppType
         AND tintTemplateType = v_tintTemplateType
         AND (coalesce(numProfile,0) > 0 OR coalesce(numRelationship,0) > 0 OR coalesce(numAccountClass,0) > 0)
         AND coalesce(numProfile,0) = coalesce(v_numProfile,0)
         AND numBizDocTempID <> v_numBizDocTempID
         AND coalesce(numRelationship,0) = coalesce(v_numRelationship,0)
         AND coalesce(numAccountClass,0) = coalesce(v_numAccountClass,0)) > 0 then
		
            RAISE EXCEPTION 'BIZDOC_RELATIONSHIP_PROFILE_CLASS_EXISTS';
         end if;
      end if;
      IF v_tintTemplateType = 0 then
	
         IF v_bitDefault = true then
		
            IF(select count(numBizDocTempID) from BizDocTemplate where numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND tintTemplateType = v_tintTemplateType and coalesce(bitDefault,false) = true) > 0 then
			
               Update BizDocTemplate SET bitDefault = false where numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND tintTemplateType = v_tintTemplateType and coalesce(bitDefault,false) = true;
            end if;
         ELSE
            IF(select count(numBizDocTempID) from BizDocTemplate where numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND tintTemplateType = v_tintTemplateType) = 0 then
			
               v_bitDefault := true;
            end if;
         end if;
         IF v_numBizDocTempID > 0 then
			
            UPDATE  BizDocTemplate SET txtBizDocTemplate = v_txtBizDocTemplate,txtCSS = v_txtCSS,bitEnabled = v_bitEnabled,
            vcTemplateName = v_vcTemplateName,bitDefault = v_bitDefault,numOrientation = v_numOrientation,
            bitKeepFooterBottom = v_bitKeepFooterBottom,
            numBizDocID = v_numBizDocID,numRelationship = v_numRelationship,numProfile = v_numProfile,
            bitDisplayKitChild = v_bitDisplayKitChild,numAccountClass = v_numAccountClass
            WHERE numBizDocTempID = v_numBizDocTempID;
         ELSE
            INSERT  INTO BizDocTemplate(numDomainID,numBizDocID,txtBizDocTemplate,txtCSS,bitEnabled,numOppType,tintTemplateType,vcTemplateName,bitDefault,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild,numAccountClass)
				VALUES(v_numDomainID,v_numBizDocID,v_txtBizDocTemplate,v_txtCSS,v_bitEnabled,v_numOppType,v_tintTemplateType,v_vcTemplateName,v_bitDefault,v_numOrientation,v_bitKeepFooterBottom,v_numRelationship,v_numProfile,v_bitDisplayKitChild,v_numAccountClass) RETURNING numBizDocTempID INTO v_numBizDocTempID;
				
            open SWV_RefCur for
            SELECT v_numBizDocTempID AS InsertedID;
         end if;
      ELSE
         IF NOT EXISTS(SELECT * FROM    BizDocTemplate WHERE   numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND tintTemplateType = v_tintTemplateType) then
        
            INSERT  INTO BizDocTemplate(numDomainID,
                      numBizDocID,
                      txtBizDocTemplate,
                      txtCSS,
					  bitEnabled,
					  numOppType,tintTemplateType,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild,numAccountClass)
            VALUES(v_numDomainID,
                      v_numBizDocID,
                      v_txtBizDocTemplate,
                      v_txtCSS,
					  v_bitEnabled,
					  v_numOppType,v_tintTemplateType,v_numOrientation,v_bitKeepFooterBottom,v_numRelationship,v_numProfile,v_bitDisplayKitChild,v_numAccountClass) RETURNING numBizDocTempID INTO v_numBizDocTempID;
            
            open SWV_RefCur for
            SELECT v_numBizDocTempID AS InsertedID;
         ELSE
            UPDATE  BizDocTemplate
            SET
            txtBizDocTemplate = v_txtBizDocTemplate,txtCSS = v_txtCSS,bitEnabled = v_bitEnabled,
            numOppType = v_numOppType,tintTemplateType = v_tintTemplateType,
            numOrientation = v_numOrientation,bitKeepFooterBottom = v_bitKeepFooterBottom,
            numRelationship = v_numRelationship,numProfile = v_numProfile, 
            bitDisplayKitChild = v_bitDisplayKitChild,numAccountClass = v_numAccountClass
            WHERE   numDomainID = v_numDomainID AND numBizDocID = v_numBizDocID AND numOppType = v_numOppType AND tintTemplateType = v_tintTemplateType;
         end if;
      end if;
      EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
   RETURN;
END; $$;


