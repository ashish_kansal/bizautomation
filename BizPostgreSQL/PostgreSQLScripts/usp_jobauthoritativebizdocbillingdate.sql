-- Stored procedure definition script USP_JOBAuthoritativeBizdocBillingDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_JOBAuthoritativeBizdocBillingDate()
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT TO_CHAR(obd.dtCreatedDate,'yyyy/mm/dd') AS dtCreatedDate,TO_CHAR(obd.dtFromDate,'yyyy/mm/dd') AS dtFromDate,TO_CHAR(GJH.datEntry_Date,'yyyy/mm/dd') AS datEntry_Date
,GJH.* FROM OpportunityBizDocs obd JOIN General_Journal_Header GJH ON obd.numoppid = GJH.numOppId
   AND obd.numOppBizDocsId = GJH.numOppBizDocsId WHERE
   TO_CHAR(obd.dtFromDate,'yyyy/mm/dd') != TO_CHAR(GJH.datEntry_Date,'yyyy/mm/dd')
   AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numPayrollDetailID,0) = 0
   AND coalesce(GJH.numCheckHeaderID,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
   AND coalesce(GJH.numDepositId,0) = 0 and monDealAmount = numAmount
   AND obd.bitAuthoritativeBizDocs = 1) then

      RAISE EXCEPTION 'Find_BillingDate_Issue';
   end if;
   RETURN;
END; $$;


