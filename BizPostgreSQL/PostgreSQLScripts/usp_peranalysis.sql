-- Stored procedure definition script usp_PerAnalysis for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_PerAnalysis(                        
--- Type = 1 Team Performance =2 Individual Performance                        
v_numDomainId NUMERIC,                          
 v_numUserCntId NUMERIC,                          
 v_dtStartDate TIMESTAMP,                          
 v_dtEndDate TIMESTAMP,                        
 v_Type Integer,                       
 v_tintType Integer, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppOpen  NUMERIC;                          
   v_numOpplost  NUMERIC;                        
   v_numOpplostAmount  NUMERIC;                        
   v_numOppOpenAmount  NUMERIC;                          
   v_numOppClose  NUMERIC;                          
   v_numOppCloseAmount  NUMERIC;                          
   v_numLeadProm  NUMERIC;                          
   v_numTotProspect  NUMERIC;                          
   v_numNoProsProm  NUMERIC;                          
   v_numTotNoAccount  NUMERIC;                          
   v_numCommSch  NUMERIC;                          
   v_numComComp  NUMERIC;                          
   v_numCommPast  NUMERIC;                          
   v_numTaskSch  NUMERIC;                          
   v_numTaskComp  NUMERIC;                          
   v_numTaskPast  NUMERIC;                          
   v_numCaseOpen  NUMERIC;                          
   v_numCaseComp  NUMERIC;                          
   v_numCasePast  NUMERIC;                                     
   v_numTerritoryID  NUMERIC;
BEGIN
   if v_Type = 1 then

      select   count(*), sum(monPAmount) INTO v_numOppOpen,v_numOppOpenAmount FROM
      OpportunityMaster OM
      INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numrecowner WHERE
      tintopptype = 1
      AND OM.bintCreatedDate  between v_dtStartDate and v_dtEndDate and ADC.numTeam in(SELECT numTeam FROM ForReportsByTeam F WHERE F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType);                      
                        
                        
--Opportunity Lost deals                         
      select   COUNT(*), SUM(monPAmount) INTO v_numOpplost,v_numOpplostAmount FROM
      OpportunityMaster OM
      INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numrecowner WHERE OM.bintCreatedDate  between v_dtStartDate and v_dtEndDate and tintopptype = 1 AND ADC.numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType) and OM.tintoppstatus = 2;                        
                        
                              
  --Opportunity won deals                          
      select   count(*), sum(monPAmount) INTO v_numOppClose,v_numOppCloseAmount FROM OpportunityMaster OM INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numrecowner WHERE OM.bintCreatedDate  between v_dtStartDate and v_dtEndDate and tintopptype = 1 AND ADC.numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType) and OM.tintoppstatus = 1;                        
                        
                        
  --Leads Promoted                          
      select   Count(*) INTO v_numLeadProm FROM DivisionMaster DM
      join AdditionalContactsInformation ADC on
      ADC.numDivisionId = DM.numDivisionID
      join AdditionalContactsInformation ADC1
      on ADC1.numContactId = DM.bintLeadPromBy WHERE coalesce(ADC.bitPrimaryContact,false) = true  and DM.tintCRMType = 1 and bintLeadProm between v_dtStartDate and v_dtEndDate
      AND ADC1.numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType);                        
          
                       
  --Total Prospects Added       
      select   Count(*) INTO v_numTotProspect FROM DivisionMaster D
      join AdditionalContactsInformation A
      on A.numDivisionId = D.numDivisionID WHERE coalesce(A.bitPrimaryContact,false) = true and D.tintCRMType = 1
      and ((D.bintCreatedDate between v_dtStartDate and v_dtEndDate and D.bintLeadPromBy is null and D.numRecOwner in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType)))
      or (bintLeadProm between v_dtStartDate and v_dtEndDate and D.bintLeadPromBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType))));            
          
                        
  --No of prospects Promoted                          
      select   Count(*) INTO v_numNoProsProm FROM DivisionMaster DM
      join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID WHERE coalesce(ADC.bitPrimaryContact,false) = true  and DM.tintCRMType = 2 and bintProsProm between v_dtStartDate and v_dtEndDate and bintProsPromBy in(select numContactId from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                       
                           
  --Total No of Accounts Added                          
      select   Count(*) INTO v_numTotNoAccount FROM DivisionMaster D
      join AdditionalContactsInformation A
      on A.numDivisionId = D.numDivisionID WHERE coalesce(A.bitPrimaryContact,false) = true and D.tintCRMType = 2 and ((D.bintCreatedDate between v_dtStartDate and v_dtEndDate and bintProsPromBy is null and D.numRecOwner in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType))))
      or (bintProsProm between v_dtStartDate and v_dtEndDate and bintProsPromBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType)));                        
                          
  --Comunications Scheduled                          
      select   Count(*) INTO v_numCommSch from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                          
  --Communication Completed                          
      select   Count(*) INTO v_numComComp from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = true and nummodifiedby in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                          
  --Communications Past Due                          
      select   Count(*) INTO v_numCommPast from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = false and numCreatedBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType)) and dtStartTime < TIMEZONE('UTC',now());                           
                
  --Tasks Scheduled                          
      select   Count(*) INTO v_numTaskSch from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                          
  --Tasks Completed                            
      select   Count(*) INTO v_numTaskComp from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = true and nummodifiedby in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                          
  --Tasks Past Due                          
      select   Count(*) INTO v_numTaskPast from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = false and numCreatedBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType)) and dtStartTime < TIMEZONE('UTC',now());         
                          
  --Cases Opened                          
      select   Count(*) INTO v_numCaseOpen from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedby in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                          
  --Cased Completed                          
      select   Count(*) INTO v_numCaseComp from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numStatus = 136 and numModifiedBy in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType));                    
  --Cases Past Due                          
      select   Count(*) INTO v_numCasePast from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numStatus <> 136 and numCreatedby in(select numContactId from AdditionalContactsInformation
         where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_tintType))  and intTargetResolveDate < TIMEZONE('UTC',now());
      open SWV_RefCur for
      SELECT v_numOppOpen AS OppOpened,
 v_numOppOpenAmount AS OppOpenAmt,
 v_numOpplost AS OppLost,
 v_numOpplostAmount AS OppLostAmt,
 v_numOppClose AS OppClosed,
 v_numOppCloseAmount AS OppCloseAmt,
 v_numLeadProm AS LeadProm,
 v_numTotProspect AS TotProsp,
    v_numNoProsProm AS NoProspProm,
 v_numTotNoAccount AS TotNoAccount,
 v_numCommSch AS CommSch,
 v_numComComp AS CommComp,
    v_numCommPast AS CommPast,
 v_numTaskSch AS TaskSch,
 v_numTaskComp AS TaskComp,
 v_numTaskPast AS TaskPast,
    v_numCaseOpen AS CaseOpen,
 v_numCaseComp AS CaseComp,
 v_numCasePast AS CasePast;
   end if;                        
                        
   if v_Type = 2 then

      select   count(*), sum(monPAmount) INTO v_numOppOpen,v_numOppOpenAmount FROM OpportunityMaster OM, DivisionMaster DM WHERE
      DM.numDivisionID = OM.numDivisionId  and tintopptype = 1
      and OM.bintCreatedDate  between v_dtStartDate and v_dtEndDate and OM.numrecowner = v_numUserCntId;                          
                        
                        
--Opportunity lost deals                         
      select   count(*), sum(monPAmount) INTO v_numOpplost,v_numOpplostAmount FROM OpportunityMaster OM WHERE bintCreatedDate  between v_dtStartDate and v_dtEndDate and tintopptype = 1  and OM.numrecowner = v_numUserCntId and tintoppstatus = 2;                        
                              
  --Opportunity won deals                          
      select   count(*), sum(monPAmount) INTO v_numOppClose,v_numOppCloseAmount FROM OpportunityMaster OM WHERE bintCreatedDate  between v_dtStartDate and v_dtEndDate and tintopptype = 1  and OM.numrecowner = v_numUserCntId and tintoppstatus = 1;                    
                        
                        
  --Leads Promoted                          
      select   Count(*) INTO v_numLeadProm FROM DivisionMaster DM
      join AdditionalContactsInformation ADC on
      ADC.numDivisionId = DM.numDivisionID WHERE tintCRMType = 1 AND coalesce(ADC.bitPrimaryContact,false) = true AND bintLeadProm between v_dtStartDate and v_dtEndDate
      and bintLeadPromBy = v_numUserCntId;             
          
                   
                          
  --Total Prospects Added                   
                     
      select   Count(*) INTO v_numTotProspect FROM DivisionMaster D
      join AdditionalContactsInformation A
      on A.numDivisionId = D.numDivisionID WHERE coalesce(A.bitPrimaryContact,false) = true  and D.tintCRMType = 1 and ((D.bintCreatedDate between v_dtStartDate and v_dtEndDate and D.numCreatedBy = v_numUserCntId)
      or (bintLeadProm between v_dtStartDate and v_dtEndDate and bintLeadPromBy = v_numUserCntId));                         
                          
  --No of prospects Promoted                          
      select   Count(*) INTO v_numNoProsProm FROM DivisionMaster WHERE tintCRMType = 2 and bintProsProm between v_dtStartDate and v_dtEndDate and bintProsPromBy = v_numUserCntId;                        
                           
 --Total No of Accounts Added                          
      select   Count(*) INTO v_numTotNoAccount FROM DivisionMaster D
      join AdditionalContactsInformation A
      on A.numDivisionId = D.numDivisionID WHERE coalesce(A.bitPrimaryContact,false) = true  and D.tintCRMType = 2 and ((D.bintCreatedDate between v_dtStartDate and v_dtEndDate and D.numCreatedBy = v_numUserCntId)
      or (bintProsProm between v_dtStartDate and v_dtEndDate and bintProsPromBy = v_numUserCntId));                       
  --Comunications Scheduled                          
      select   Count(*) INTO v_numCommSch from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedBy = v_numUserCntId;                         
  --Communication Completed                          
      select   Count(*) INTO v_numComComp from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = true and numCreatedBy = v_numUserCntId;                          
  --Communications Past Due                          
      select   Count(*) INTO v_numCommPast from Communication where bitTask = 971 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = false and numCreatedBy = v_numUserCntId  and dtStartTime < TIMEZONE('UTC',now());                     
                          
  --Tasks Scheduled                          
      select   Count(*) INTO v_numTaskSch from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedBy = v_numUserCntId;                          
  --Tasks Completed                 
      select   Count(*) INTO v_numTaskComp from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = true and numCreatedBy = v_numUserCntId;                          
  --Tasks Past Due                          
      select   Count(*) INTO v_numTaskPast from Communication where bitTask = 972 and dtCreatedDate between v_dtStartDate and v_dtEndDate and bitClosedFlag = false and numCreatedBy = v_numUserCntId  and dtStartTime < TIMEZONE('UTC',now());                       
    
  --Cases Opened                          
      select   Count(*) INTO v_numCaseOpen from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numCreatedby = v_numUserCntId;                          
  --Cased Completed                          
      select   Count(*) INTO v_numCaseComp from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numStatus = 136 and numCreatedby = v_numUserCntId;                          
  --Cases Past Due                          
      select   Count(*) INTO v_numCasePast from Cases where bintCreatedDate between v_dtStartDate and v_dtEndDate and numStatus <> 136 and numCreatedby = v_numUserCntId  and intTargetResolveDate < TIMEZONE('UTC',now());
      open SWV_RefCur for
      SELECT v_numOppOpen AS OppOpened,
 v_numOppOpenAmount AS OppOpenAmt,
 v_numOpplost AS OppLost,
 v_numOpplostAmount AS OppLostAmt,
 v_numOppClose AS OppClosed,
 v_numOppCloseAmount AS OppCloseAmt,
 v_numLeadProm AS LeadProm,
 v_numTotProspect AS TotProsp,
    v_numNoProsProm AS NoProspProm,
 v_numTotNoAccount AS TotNoAccount,
 v_numCommSch AS CommSch,
 v_numComComp AS CommComp,
    v_numCommPast AS CommPast,
 v_numTaskSch AS TaskSch,
 v_numTaskComp AS TaskComp,
 v_numTaskPast AS TaskPast,
    v_numCaseOpen AS CaseOpen,
 v_numCaseComp AS CaseComp,
 v_numCasePast AS CasePast;
   end if;
   RETURN;
END; $$;


