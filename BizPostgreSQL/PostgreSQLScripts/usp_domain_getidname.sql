-- Stored procedure definition script USP_Domain_GetIDName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Domain_GetIDName(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numDomainId as "numDomainId",
		vcDomainName as "vcDomainName"
   FROM
   Domain
   WHERE
   coalesce(numDomainId,0) > 0
   ORDER BY
   vcDomainName ASC;
END; $$;











