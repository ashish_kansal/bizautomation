CREATE OR REPLACE FUNCTION CheckIfCommissionRuleApplicable(v_numDomainID NUMERIC(18,0), 
	v_numDivisionID NUMERIC(18,0), 
	v_numAssignedTo NUMERIC(18,0), 
	v_numRecOwner NUMERIC(18,0))
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitCommissionRule  BOOLEAN DEFAULT false;

   v_bitOrganizationRuleApplicable  BOOLEAN DEFAULT false;
   v_bitContactRuleApplicable  BOOLEAN DEFAULT false;

   v_numProfile  NUMERIC(18,0);
   v_numRelationship  NUMERIC(18,0);
   v_bitCommContact  BOOLEAN DEFAULT false;
   v_numComDivisionID  NUMERIC(18,0) DEFAULT 0;

	--GET ORGANIZATION RELATIOnSHIP AND PROFILE VALUE
BEGIN
   select   coalesce(numCompanyType,0), coalesce(CompanyInfo.vcProfile,0) INTO v_numRelationship,v_numProfile FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = v_numDivisionID;


	--CHECKS IF ORGANIZATION RULE IS APPLICABLE
	-- tintComOrgType = 1: INDIVIDUAL ORGANIZATION, 2:ORGANIZATION BY RELATIONSHIP PROIFILE, 3: ALL ORGANIZATIONS
	
	--tintType=3: ORGANIZATION ID
   IF(SELECT COUNT(*) FROM CommissionRules WHERE numDomainID = v_numDomainID AND coalesce(tintComOrgType,0) = 3) > 0 then
	
      v_bitOrganizationRuleApplicable := true;
	--tintType=4: RELATIONSHIP & PROIFILE
   ELSEIF(SELECT COUNT(*) FROM CommissionRuleOrganization CRO INNER JOIN CommissionRules CR ON CRO.numComRuleID = CR.numComRuleID  WHERE CR.numDomainID = v_numDomainID AND numValue = v_numRelationship AND numProfile = v_numProfile AND tintType = 4) > 0
   then
	
      v_bitOrganizationRuleApplicable := true;
   ELSEIF(SELECT COUNT(*) FROM CommissionRuleOrganization CRO INNER JOIN CommissionRules CR ON CRO.numComRuleID = CR.numComRuleID  WHERE CR.numDomainID = v_numDomainID AND numValue = v_numDivisionID AND tintType = 3) > 0
   then
	
      v_bitOrganizationRuleApplicable := true;
   end if;

	--CHECKS IF CONTACT RULE IS APPLICABLE
	--Check For Commission Contact	
   IF EXISTS(SELECT  UM.numUserId
   FROM    UserMaster UM
   JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
   WHERE   UM.numDomainID = v_numDomainID
   AND UM.numDomainID = A.numDomainID
   AND (A.numContactId = v_numAssignedTo OR A.numContactId = v_numRecOwner)) then
    
      v_bitCommContact := false;
   ELSE
      v_bitCommContact := true;
      select   A.numDivisionId INTO v_numComDivisionID FROM
      AdditionalContactsInformation A WHERE
			(A.numContactId = v_numAssignedTo OR  A.numContactId = v_numRecOwner);
   end if;  		

   IF coalesce(v_bitCommContact,false) = true then
	
      IF(SELECT COUNT(*) FROM CommissionRuleContacts CRC INNER JOIN CommissionRules CR ON CRC.numComRuleId = CR.numComRuleID WHERE CR.numDomainID = v_numDomainID AND CRC.numValue = v_numComDivisionID) > 0 then
		
         v_bitContactRuleApplicable := true;
      end if;
   ELSE
      IF(SELECT COUNT(*) FROM CommissionRuleContacts CRC INNER JOIN CommissionRules CR ON CRC.numComRuleId = CR.numComRuleID WHERE CR.numDomainID = v_numDomainID AND (CRC.numValue = v_numAssignedTo OR CRC.numValue = v_numRecOwner)) > 0 then
		
         v_bitContactRuleApplicable := true;
      end if;
   end if;

   IF v_bitOrganizationRuleApplicable = true AND v_bitContactRuleApplicable = true then
	
      v_bitCommissionRule := true;
   end if;  

   RETURN v_bitCommissionRule;
END; $$;

