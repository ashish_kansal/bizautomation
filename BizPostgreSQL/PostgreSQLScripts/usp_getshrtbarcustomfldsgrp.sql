-- Stored procedure definition script USP_GetShrtBarCustomFldsGrp for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShrtBarCustomFldsGrp(v_numGroupId NUMERIC,  
v_numDomainId NUMERIC  ,
v_numContactId NUMERIC  DEFAULT 0,
v_numTabId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from ShortCutBar where bitdefault = false and numTabId = v_numTabId  and numGroupID = v_numGroupId
   and numDomainID = v_numDomainId  and numContactId = v_numContactId;
END; $$;












