-- Stored procedure definition script USP_BankReconcileFileData_UpdateStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BankReconcileFileData_UpdateStatus(v_numDomainID NUMERIC(18,0),
	v_numReconcileID NUMERIC(18,0),
    v_strStatement TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(v_strStatement) > 0 then
	
      UPDATE
      BankReconcileFileData
      SET
      bitReconcile = false,bitCleared = X.bitCleared,numTransactionId = X.numTransactionId
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strStatement AS XML)
			COLUMNS
				ID NUMERIC(18,0) PATH 'ID',
				bitCleared BOOLEAN PATH 'bitCleared',
				numTransactionId NUMERIC(18,0) PATH 'numTransactionId'
				
		) AS X
      WHERE
      BankReconcileFileData.ID = X.ID
      AND BankReconcileFileData.numReconcileID = v_numReconcileID
      AND BankReconcileFileData.numDomainID = v_numDomainID;

   end if;
   RETURN;
END; $$;


--Created by anoop jayaraj                                                          


