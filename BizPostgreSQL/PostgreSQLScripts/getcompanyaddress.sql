DROP FUNCTION IF EXISTS getCompanyAddress;

CREATE OR REPLACE FUNCTION getCompanyAddress(v_numDivisionId NUMERIC,v_tintAddressType SMALLINT,v_numDomainID NUMERIC)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_addressname  VARCHAR(300);
   v_street  VARCHAR(100);
   v_City  VARCHAR(100);
   v_State  VARCHAR(100);
   v_PostCode  VARCHAR(100);
   v_vcCountry  VARCHAR(100);
   v_address1  VARCHAR(1000);
   v_add  VARCHAR(100);
BEGIN
   v_add := ',';
   v_address1 := '';

   select COALESCE(vcAddressName,''),coalesce(AD.vcStreet,''), coalesce(AD.vcCity,''), coalesce(fn_GetState(AD.numState),''), coalesce(AD.vcPostalCode,''), coalesce(fn_GetListName(AD.numCountry,0::BOOLEAN),'') 
   INTO v_addressname,v_street,v_City,v_State,v_PostCode,v_vcCountry FROM
   AddressDetails AD where AD.numRecordID = v_numDivisionId AND tintAddressOf = 2 AND tintAddressType = v_tintAddressType
   AND AD.bitIsPrimary = true AND numDomainID = v_numDomainID    LIMIT 1;

-- tintAddressOf => Contact->1,Organization->2,Opportunity->3
-- tintAddressType=> Billto Address-> 1, Shipto Address->2

   v_address1 := coalesce(v_address1,'') || (CASE WHEN LENGTH(COALESCE(v_addressname,'')) > 0 THEN CONCAT('<i>(',v_addressname,') </i>') ELSE '' END) || coalesce(v_street,'');
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := '<br/>';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_City <> '' then coalesce(v_add,'') || coalesce(v_City,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ', ';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_State <> '' then coalesce(v_add,'') || coalesce(v_State,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ', ';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_PostCode <> '' then coalesce(v_add,'') || coalesce(v_PostCode,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := '</br>';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_vcCountry <> '' then coalesce(v_add,'') || coalesce(v_vcCountry,'') else '' end;
--print @address1
   return v_address1;
END; $$;

