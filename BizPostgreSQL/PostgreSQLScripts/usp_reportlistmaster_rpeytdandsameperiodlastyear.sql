-- Stored procedure definition script USP_ReportListMaster_RPEYTDAndSamePeriodLastYear for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_RPEYTDAndSamePeriodLastYear(v_numDomainID NUMERIC(18,0)
	,v_vcFilterBy SMALLINT --added  for Filter the Period of Measure as mentioned in Slide 5
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ExpenseYTD  DECIMAL(20,5);
   v_ExpenseSamePeriodLastYear  DECIMAL(20,5);
   v_ProfitYTD  DECIMAL(20,5);
   v_ProfitSamePeriodLastYear  DECIMAL(20,5);
   v_RevenueYTD  DECIMAL(20,5);
   v_RevenueSamePeriodLastYear  DECIMAL(20,5);
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_dtCYFromDate  TIMESTAMP;                                       
   v_dtCYToDate  TIMESTAMP;
   v_dtLYFromDate  TIMESTAMP;                                       
   v_dtLYToDate  TIMESTAMP;

   v_FinancialYearStartDate  TIMESTAMP;
BEGIN
   v_FinancialYearStartDate := GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP)::INTEGER,v_numDomainID);

	--SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	--SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	---alter sp
   IF v_vcFilterBy = 1 then
	
      v_dtCYFromDate := v_FinancialYearStartDate;
      v_dtCYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003);
      v_dtLYFromDate := v_FinancialYearStartDate+INTERVAL '-1 year';
      v_dtLYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003)+INTERVAL '-1 year'+ make_interval(secs => -0.001);
   ELSEIF v_vcFilterBy = 2
   then
	
      v_dtCYFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval);
      v_dtCYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003);
      v_dtLYFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '-1 year';
      v_dtLYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003)+INTERVAL '-1 year'+ make_interval(secs => -0.001);
   ELSEIF v_vcFilterBy = 3
   then
	
      v_dtCYFromDate := LOCALTIMESTAMP+CAST(2 -(EXTRACT(DOW FROM LOCALTIMESTAMP)+1) || 'day' as interval)+ make_interval(secs => -0.001);
      v_dtCYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003);
      v_dtLYFromDate := LOCALTIMESTAMP+CAST(2 -(EXTRACT(DOW FROM LOCALTIMESTAMP)+1) || 'day' as interval)+ make_interval(secs => -0.001)+INTERVAL '-1 year';
      v_dtLYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003)+INTERVAL '-1 year'+ make_interval(secs => -0.001);
   ELSE
      v_dtCYFromDate := v_FinancialYearStartDate;
      v_dtCYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003);
      v_dtLYFromDate := v_FinancialYearStartDate+INTERVAL '-1 year';
      v_dtLYToDate := '1900-01-01':: date+CAST(datediff('day', '1900-01-01'::TIMESTAMP,now()::TIMESTAMP)+1 || 'day' as interval)+ make_interval(secs => -0.003)+INTERVAL '-1 year'+ make_interval(secs => -0.001);
   end if;
	------
   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID;


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
   select   SUM(numDebitAmt) -SUM(numCreditAmt) INTO v_ExpenseYTD FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId = General_Journal_Header.numJOurnal_Id WHERE General_Journal_Details.numDomainId = v_numDomainID AND datEntry_Date BETWEEN  v_dtCYFromDate AND v_dtCYToDate AND General_Journal_Details.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND bitActive = true AND numAcntTypeId IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode ilike '0104%'));
   select   SUM(numDebitAmt) -SUM(numCreditAmt) INTO v_ExpenseSamePeriodLastYear FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId = General_Journal_Header.numJOurnal_Id WHERE General_Journal_Details.numDomainId = v_numDomainID AND datEntry_Date BETWEEN  v_dtLYFromDate AND v_dtLYToDate AND General_Journal_Details.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND bitActive = true AND numAcntTypeId IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode ilike '0104%'));

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
   select   SUM(Profit) INTO v_ProfitYTD FROM(SELECT
      coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtCYFromDate AND v_dtCYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;

   select   SUM(Profit) INTO v_ProfitSamePeriodLastYear FROM(SELECT
      coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtLYFromDate AND v_dtLYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
   select   SUM(monTotAmount) INTO v_RevenueYTD FROM(SELECT
      coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtCYFromDate AND v_dtCYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1) TEMP;

   select   SUM(monTotAmount) INTO v_RevenueSamePeriodLastYear FROM(SELECT
      coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtLYFromDate AND v_dtLYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1) TEMP;

   open SWV_RefCur for SELECT
   100.0*(coalesce(v_RevenueYTD,0) -coalesce(v_RevenueSamePeriodLastYear,0))/CASE WHEN coalesce(v_RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE v_RevenueSamePeriodLastYear END As RevenueDifference
		,100.0*(coalesce(v_ProfitYTD,0) -coalesce(v_ProfitSamePeriodLastYear,0))/CASE WHEN coalesce(v_ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE v_ProfitSamePeriodLastYear END As ProfitDifference
		,100.0*(coalesce(v_ExpenseYTD,0) -coalesce(v_ExpenseSamePeriodLastYear,0))/CASE WHEN coalesce(v_ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE v_ExpenseSamePeriodLastYear END As ExpenseDifference;
END; $$;












