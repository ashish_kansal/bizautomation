-- Stored procedure definition script usp_GetMaxCaseNo for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetMaxCaseNo(v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcCaseNumber
   FROM Cases
   WHERE numDomainID = v_numDomainID
   ORDER BY vcCaseNumber DESC  LIMIT 1;
END; $$;


--END












