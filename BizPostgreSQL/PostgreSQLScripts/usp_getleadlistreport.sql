-- Stored procedure definition script USP_GetLeadListReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetLeadListReport(v_numGrpID NUMERIC,                                                          
 v_numUserCntID NUMERIC,                                                          
 v_tintUserRightType SMALLINT,                                                                                                   
 v_numFollowUpStatus NUMERIC(9,0) DEFAULT 0 ,                                                        
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                        
 v_columnName VARCHAR(50) DEFAULT '',                                                        
 v_columnSortOrder VARCHAR(10) DEFAULT '',                                
 v_ListType SMALLINT DEFAULT 0,                                
 v_TeamType INTEGER DEFAULT 0,   
 v_TeamMemID NUMERIC(9,0) DEFAULT 0,                            
 v_numDomainID NUMERIC(9,0) DEFAULT 0,          
 v_dtFromDate TIMESTAMP DEFAULT NULL,      
 v_dtToDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(7100);
BEGIN
   v_strSql := 'SELECT                                                                             
   DM.bintCreatedDate,                                                          
   cmp.numCompanyId,                                                          
   cmp.vcCompanyName,                                                          
   DM.vcDivisionName,                                                          
   DM.numDivisionID,                                                          
   ADC.numContactId,                                          
   fn_GetListItemName(cmp.numNoOfEmployeesId) as Employees,                                                                                     
   DM.numRecOwner,                                                          
   G.vcGrpName,                                                    
   DM.numTerID,                                                         
   lst.vcData as FolStatus,                             
   ADC1.vcFirstName || '' '' || ADC1.vcLastName as vcUserName,                      
   fn_GetContactName(numAssignedTo) as AssignedTo                                                                   
  FROM                                                            
  CompanyInfo cmp                                                           
   join DivisionMaster DM                                    
   on cmp.numCompanyId = DM.numCompanyID                                    
   left join ListDetails lst                                     
   on lst.numListItemID = DM.numFollowUpStatus                                                 
   join AdditionalContactsInformation ADC                                    
   on ADC.numDivisionId = DM.numDivisionID                            
   join AdditionalContactsInformation ADC1     
   on ADC1.numContactId = DM.numRecOwner     
   join Groups G    
   on G.numGrpId = DM.numGrpId                                                                  
  WHERE                                                           
   DM.tintCRMType = 0  and coalesce(ADC.bitPrimaryContact,false) = true                                                                                                  
 AND DM.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '    
and DM.bintCreatedDate >= ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || '''    
and DM.bintCreatedDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''    
';    
    
   if v_numGrpID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and DM.numGrpId  ilike ''' || SUBSTR(CAST(v_numGrpID AS VARCHAR(15)),1,15) || '%''';
   end if;                          
                                                               

   if v_numFollowUpStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || '  AND DM.numFollowUpStatus =' || SUBSTR(CAST(v_numFollowUpStatus AS VARCHAR(15)),1,15);
   end if;                                                         
                                 
    
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
   end if;    
   if v_tintUserRightType = 2 then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC1.numTeam in(select F.numTeam from ForReportsByTeam F     
where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(5)),1,5) || ')';
   ELSEIF v_tintUserRightType = 3
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;                                        
      
   if v_ListType = 0 then

      if v_numGrpID = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
      end if;
      if v_numGrpID = 5 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
      end if;
   ELSEIF v_ListType = 1
   then

      if v_TeamMemID = 0 then
 
         if v_numGrpID <> 2 then 
            v_strSql := coalesce(v_strSql,'') || '  AND (DM.numRecOwner  in(select distinct numUserDetailId from UserMaster join AdditionalContactsInformation on numContactID=numUserDetailId                 
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ') ) or DM.numAssignedTo in(select distinct numUserDetailId from UserMaster join AdditionalContactsInformation on numContactID=numUserDetailId                 
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ')))';
         end if;
      else
         if v_numGrpID <> 2 then 
            v_strSql := coalesce(v_strSql,'') || '  AND DM.numRecOwner=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15) || '  or  DM.numAssignedTo=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15);
         end if;
      end if;
   end if;                                        
                
                
   RAISE NOTICE '%',v_strSql;                                        
                                       
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


