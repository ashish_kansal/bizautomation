-- Stored procedure definition script USP_OppUpDateBizDocs_UpdateViewed for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppUpDateBizDocs_UpdateViewed(v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,                      
 v_numContactID NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update OpportunityBizDocs
   set numViewedBy = v_numContactID,dtViewedDate = TIMEZONE('UTC',now())
   where numOppBizDocsId = v_numOppBizDocsId;
   RETURN;
END; $$;                      



