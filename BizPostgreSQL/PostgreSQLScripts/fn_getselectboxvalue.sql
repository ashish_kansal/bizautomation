-- Function definition script fn_getSelectBoxValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getSelectBoxValue(v_vcListItemType VARCHAR(3),v_numlistID NUMERIC(18,0),v_vcDbColumnName VARCHAR(50))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(100);
BEGIN
   IF v_vcListItemType = 'LI' then
				
      select   coalesce(vcData,'') INTO v_listName from Listdetails WHERE numListItemID = v_numlistID;
   ELSEIF v_vcListItemType = 'S'
   then
				 
      select   vcState INTO v_listName from State WHERE numStateID = v_numlistID;
   ELSEIF v_vcListItemType = 'T'
   then
				
      select   vcData INTO v_listName from Listdetails WHERE numListItemID = v_numlistID;
   ELSEIF v_vcListItemType = 'C'
   then
				
      select   vcCampaignName INTO v_listName from CampaignMaster WHERE numCampaignID = v_numlistID;
   ELSEIF v_vcListItemType = 'AG'
   then
				
      select   vcGrpName INTO v_listName from Groups WHERE numGrpId = v_numlistID;
   ELSEIF v_vcListItemType = 'DC'
   then
				
      select   vcECampName INTO v_listName from ECampaign WHERE numECampaignID = v_numlistID;
   ELSEIF v_vcListItemType = 'U' AND v_vcDbColumnName = 'numContractId'
   then
				
      select   vcContractName INTO v_listName FROM ContractManagement WHERE numContractId = v_numlistID;
   ELSEIF v_vcListItemType = 'U' OR v_vcDbColumnName = 'numManagerID'
   then
				
      v_listName := fn_GetContactName(v_numlistID);
   ELSEIF v_vcDbColumnName = 'charSex'
   then
				
      v_listName := CASE WHEN v_numlistID = 'M' then 'Male' when v_numlistID = 'F' then 'Female' else  '-' end;
   ELSEIF v_vcDbColumnName = 'tintOppStatus'
   then
			  
      v_listName := CASE v_numlistID WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END;
   Else
      select   vcData INTO v_listName from Listdetails WHERE numListItemID = v_numlistID;
   end if; 	

   return coalesce(v_listName,'-');
END; $$;

