-- Stored procedure definition script USP_SubmitSalseReturnPayment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SubmitSalseReturnPayment(v_numBizDocsId	NUMERIC(18,0) DEFAULT null,
v_numPaymentMethod	SMALLINT DEFAULT NULL,
v_numPaymentType	SMALLINT DEFAULT NULL,
v_monAmount	DECIMAL(20,5) DEFAULT NULL,
v_numDepoistToChartAcntId	NUMERIC(18,0) DEFAULT null,
v_numDomainId	NUMERIC(18,0) DEFAULT NULL,
v_vcReference	VARCHAR(200) DEFAULT null,
v_vcMemo	VARCHAR(50) DEFAULT null,
v_bitAuthoritativeBizDocs	BOOLEAN DEFAULT true,
v_bitIntegratedToAcnt	BOOLEAN DEFAULT null,
v_bitDeferredIncome	BOOLEAN DEFAULT null,
v_sintDeferredIncomePeriod	SMALLINT DEFAULT null,
v_dtDeferredIncomeStartDate	TIMESTAMP DEFAULT null,
v_NoofDeferredIncomeTransaction	SMALLINT DEFAULT null,
v_bitSalesDeferredIncome	BOOLEAN DEFAULT null,
v_numContractId	NUMERIC(18,0) DEFAULT null,
v_numCreatedBy	NUMERIC(18,0) DEFAULT NULL,
v_numModifiedBy	NUMERIC(18,0) DEFAULT null,
v_dtModifiedDate	TIMESTAMP DEFAULT null,
v_numExpenseAccount	NUMERIC(18,0) DEFAULT null,
v_numDivisionID	NUMERIC(18,0) DEFAULT NULL,
v_dtReturnDate TIMESTAMP DEFAULT null,
v_numReturnID NUMERIC(9,0) DEFAULT NULL,
v_tintPaymentAction SMALLINT DEFAULT NULL,
v_numQtyReturned NUMERIC(9,0) DEFAULT NULL,
v_tintOppType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCurrencyID  NUMERIC(9,0);
   v_fltExchangeRate  DOUBLE PRECISION;
BEGIN
   select   coalesce(numCurrencyID,0) INTO v_numCurrencyID from Returns
   Join OpportunityMaster
   on OpportunityMaster.numOppId = Returns.numReturnID where numReturnID = v_numReturnID;


   if v_numCurrencyID = 0 then 
      v_fltExchangeRate := 1;
   else 
      v_fltExchangeRate := GetExchangeRate(v_numDomainId,v_numCurrencyID);
   end if;
   
   select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster OM join Returns R on OM.numOppId = R.numOppID where R.numReturnID = v_numReturnID;

   IF v_tintPaymentAction = 3 then --Create a sales return in DECIMAL(20,5)-out 

--Payment Type
--1- Purchase Order(While BizDoc) 
--2- Bill(while Entered from Create Bill)  
--3-Sales Returns(wihle comes from Sales return Queue)
      INSERT INTO OpportunityBizDocsDetails(numBizDocsId,
	numPaymentMethod,
	tintPaymentType,
	monAmount,
	numDepoistToChartAcntId,
	numDomainId,
	vcReference,
	vcMemo,
	bitAuthoritativeBizDocs,
	bitIntegratedToAcnt,
	bitDeferredIncome,
	sintDeferredIncomePeriod,
	dtDeferredIncomeStartDate,
	NoofDeferredIncomeTransaction,
	bitSalesDeferredIncome,
	numContractId,
	numCreatedBy,
	dtCreationDate,
	numModifiedBy,
	dtModifiedDate,
	numExpenseAccount,
	numDivisionID,
	numReturnID,
	numCurrencyID,
	fltExchangeRate) VALUES( 
	
	/* numBizDocsId - numeric(18, 0) */ v_numBizDocsId,
	/* numPaymentMethod - tinyint */ v_numPaymentMethod,
	/* numPaymentType - tinyint */ v_numPaymentType,
	/* monAmount - DECIMAL(20,5) */ v_monAmount,
	/* numDepoistToChartAcntId - numeric(18, 0) */ v_numDepoistToChartAcntId,
	/* numDomainId - numeric(18, 0) */ v_numDomainId,
	/* vcReference - nvarchar(200) */ v_vcReference,
	/* vcMemo - nvarchar(50) */ v_vcMemo,
	/* bitAuthoritativeBizDocs - bit */ v_bitAuthoritativeBizDocs,
	/* bitIntegratedToAcnt - bit */ v_bitIntegratedToAcnt,
	/* bitDeferredIncome - bit */ v_bitDeferredIncome,
	/* sintDeferredIncomePeriod - smallint */ v_sintDeferredIncomePeriod,
	/* dtDeferredIncomeStartDate - datetime */ v_dtDeferredIncomeStartDate,
	/* NoofDeferredIncomeTransaction - smallint */ v_NoofDeferredIncomeTransaction,
	/* bitSalesDeferredIncome - bit */ v_bitSalesDeferredIncome,
	/* numContractId - numeric(18, 0) */ v_numContractId,
	/* numCreatedBy - numeric(18, 0) */ v_numCreatedBy,
	/* dtCreationDate - datetime */ LOCALTIMESTAMP ,
	/* numModifiedBy - numeric(18, 0) */ v_numModifiedBy,
	/* dtModifiedDate - datetime */ v_dtModifiedDate,
	/* numExpenseAccount - numeric(18, 0) */ v_numExpenseAccount,
	/* numDivisionID - numeric(18, 0) */ v_numDivisionID,
	v_numReturnID,
	v_numCurrencyID,
	v_fltExchangeRate);
   end if;

--DECLARE @numReturnSatus NUMERIC(9)
--
--SELECT @numReturnSatus = [numListItemID] FROM [ListDetails] WHERE UPPER([vcData]) = UPPER('Under Payment');
	
   UPDATE Returns SET
   dtReturnDate = v_dtReturnDate, 
--[numReturnStatus] = @numReturnSatus,
numqtyreturned = numqtyreturned+v_numQtyReturned,
   numModifiedBy = v_numCreatedBy,dtModifiedDate = LOCALTIMESTAMP
   WHERE numReturnID = v_numReturnID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/



