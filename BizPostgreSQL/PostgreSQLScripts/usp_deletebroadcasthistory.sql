CREATE OR REPLACE FUNCTION USP_DeleteBroadcastHistory(v_numBroadCastId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(select * from Broadcast WHERE numBroadCastId = v_numBroadCastId and bitBroadcasted = false) then
      DELETE FROM BroadCastDtls WHERE numBroadcastID = v_numBroadCastId;
      DELETE FROM Broadcast WHERE numBroadCastId = v_numBroadCastId;
   end if;
   RETURN;
END; $$;




