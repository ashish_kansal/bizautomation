-- Stored procedure definition script USP_GetAccountPayableAging_Details for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAccountPayableAging_Details(v_numDomainId   NUMERIC(9,0)  DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT NULL,
    v_vcFlag        VARCHAR(20) DEFAULT NULL,
	v_numAccountClass NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate DATE DEFAULT NULL,
	v_dtToDate DATE DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,
	INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSqlJournal  VARCHAR(8000);
   v_strSqlOrder  VARCHAR(8000);
   v_strSqlCommission  VARCHAR(8000);
   v_strSqlBill  VARCHAR(8000);
   v_strSqlCheck  VARCHAR(8000);
   v_strReturn  TEXT;
   v_strSqlCredit  TEXT;
	
   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
BEGIN
   select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId =  v_numDomainId;
       ------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM Domain WHERE numDomainId = v_numDomainId;
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
   IF v_dtFromDate IS NULL then
      v_dtFromDate := '1990-01-01 00:00:00.000';
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;
     
    --Get Master data
	 
   v_strSqlBill := 'SELECT  
						  Case When coalesce(BH.bitLandedCost,false)=true then  coalesce(BH.numOppId,0) else 0 end AS numOppId,
						   ''Bill'' vcPOppName,
						   -2 AS tintOppType,-- flag for bills 
						   0 numOppBizDocsId,
						   CASE WHEN length(BH.vcReference)=0 THEN '''' ELSE BH.vcReference END AS vcBizDocID,
						   BH.monAmountDue as TotalAmount,
						   coalesce(TablePayment.monAmtPaid, 0) as AmountPaid,
					       coalesce(BH.monAmountDue, 0) - coalesce(TablePayment.monAmtPaid, 0) as BalanceDue,
						   FormatedDateFromDate(BH.dtDueDate,BH.numDomainID) AS DueDate,
						   false bitBillingTerms,
						   0 intBillingDays,
						   BH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   BH.dtDueDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,BH.numBillID,0 AS numCheckHeaderID
					FROM    
							BillHeader BH
					LEFT JOIN LATERAL
					(
						SELECT
							SUM(monAmount) monAmtPaid
						FROM 
							BillPaymentDetails BPD
						INNER JOIN
							BillPaymentHeader BPH
						ON
							BPD.numBillPaymentID=BPH.numBillPaymentID
						WHERE
							BPD.numBillID=BH.numBillID
							AND CAST(BPH.dtPaymentDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
					) as TablePayment ON TRUE
					WHERE   
							BH.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
							AND BH.numDivisionId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
							AND coalesce(BH.monAmountDue, 0) - coalesce(TablePayment.monAmtPaid, 0) > 0 
							AND (BH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
							AND CAST(BH.dtBillDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';
			 
   v_strSqlCheck := ' UNION
              SELECT  
						   0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS tintOppType,-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS vcBizDocID,
						   coalesce(CD.monAmount, 0) as TotalAmount,
						   coalesce(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   FormatedDateFromDate(CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   false bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 AS numBillID,CH.numCheckHeaderID
					FROM   CheckHeader CH
						JOIN  public.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN public.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
					WHERE CH.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
						AND (CH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
						AND CH.dtCheckDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
                        AND CD.numCustomerId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   v_strSqlOrder := ' UNION 
				SELECT 
               OM.numOppId,
               OM.vcPOppName,
               OM.tintOppType,
               OB.numOppBizDocsId,
               OB.vcBizDocID,
               coalesce(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.monPAmount
               (coalesce(TablePayment.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
               coalesce(OB.monDealAmount * OM.fltExchangeRate
                        - coalesce(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0)  BalanceDue,
               CASE coalesce(OM.bitBillingTerms,false) 
                 
                 WHEN true THEN FormatedDateFromDate(OB.dtFromDate+ interval ''1 day'' * coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0),
                                                          ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                 WHEN false THEN FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               coalesce(C.varCurrSymbol,'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN ' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || ' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,0 as numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM   OpportunityMaster OM
               INNER JOIN DivisionMaster DM
                 ON OM.numDivisionId = DM.numDivisionID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN Currency C
                 ON OM.numCurrencyID = C.numCurrencyID
				 LEFT JOIN LATERAL
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
				) TablePayment ON TRUE
        WHERE  OM.tintOppType = 2
               AND OM.tintOppStatus = 1
               AND om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
               AND OB.numBizDocId = ' || CAST(coalesce(v_AuthoritativePurchaseBizDocId,0) AS VARCHAR(15)) || ' 
               AND DM.numDivisionID = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
               AND OB.bitAuthoritativeBizDocs=1
               AND coalesce(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
			   AND CAST(OB.dtCreatedDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
               AND  coalesce(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND coalesce(OB.monDealAmount * OM.fltExchangeRate
                        - coalesce(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0 ';
		
   v_strSqlCommission := ' UNION 
		 SELECT  OM.numOppId,
               OM.vcPOppName,
               OM.tintOppType,
               OB.numOppBizDocsId,
               OB.vcBizDocID,
               BDC.numComissionAmount,
               0 AmountPaid,
               BDC.numComissionAmount BalanceDue,
               CASE coalesce(OM.bitBillingTerms,false) 
            
                 WHEN true THEN FormatedDateFromDate(OB.dtFromDate + make_interval(days => coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)), 0)),
                                                          ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                 WHEN false THEN FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               coalesce(C.varCurrSymbol,'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN ' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || ' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,BDC.numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM    BizDocComission BDC JOIN  OpportunityBizDocs OB
                ON BDC.numOppBizDocId = OB.numOppBizDocsId
				join OpportunityMaster OM on OM.numOppId=OB.numOppId
				JOIN Domain D on D.numDomainID=BDC.numDomainID
				  LEFT OUTER JOIN Currency C
                 ON OM.numCurrencyID = C.numCurrencyID
				LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=coalesce(OBDD.numBizDocsPaymentDetId,0)
        WHERE   OM.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and 
				 BDC.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' 
				AND coalesce(BDC.bitCommisionPaid,false)=false
				AND OB.bitAuthoritativeBizDocs = 1
				AND coalesce(OBDD.bitIntegratedToAcnt,false) = false
				AND (OM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND OB.dtCreatedDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
                AND D.numDivisionId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15); 		
                
    --Show Impact of AR journal entries in report as well 
    	
   v_strSqlJournal := '    	
		UNION 
		 SELECT 
				GJH.numJournal_Id numOppId,
				''Journal'' vcPOppName,
				-1 tintOppType,
				0 numOppBizDocsId,
				'''' vcBizDocID,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				1 BalanceDue,
				FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
				false bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				coalesce(C.varCurrSymbol, '''') varCurrSymbol,
				1 fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 numBizDocsPaymentDetId,
				1 AS CurrencyCount
				,0 as numComissionID
				,'''' as vcRefOrderNo
				,0 as numBillID
				,0 AS numCheckHeaderID
		 FROM   public.General_Journal_Header GJH
				INNER JOIN public.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN public.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN Currency C ON GJD.numCurrencyID = C.numCurrencyID
		 WHERE  GJH.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
				and GJD.numCustomerID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
				AND COA.vcAccountCode LIKE ''01020102%''
				AND coalesce(numOppId,0)=0 AND coalesce(numOppBizDocsId,0)=0 
				AND coalesce(GJH.numBillID,0)=0 AND coalesce(GJH.numBillPaymentID,0)=0 AND coalesce(GJH.numCheckHeaderID,0)=0 AND coalesce(GJH.numReturnID,0)=0
				AND GJH.numJournal_Id NOT IN (
				--Exclude Bill and Bill payment entries
				SELECT GH.numJournal_Id FROM public.General_Journal_Header GH INNER JOIN public.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
				WHERE GH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
				) AND coalesce(GJH.numBillID,0)=0 AND coalesce(GJH.numBillPaymentID,0)=0 AND coalesce(GJH.numCheckHeaderID,0)=0
				AND (GJH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND GJH.datEntry_Date <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
				';

   v_strReturn := ' UNION ALL
			 SELECT 
				0 AS numOppId,
				''Return'' vcPOppName,
				-1 AS tintOppType,
				0 numOppBizDocsId,
				'''' AS vcBizDocID,
				(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END) as TotalAmount,
				0 as AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END as BalanceDue,
				FormatedDateFromDate(GJH.datEntry_Date,RH.numDomainID) AS DueDate,
				false bitBillingTerms,
				0 intBillingDays,
				RH.dtCreatedDate dtCreatedDate,
				'''' varCurrSymbol,
				1 AS fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 as numBizDocsPaymentDetId
				,GJD.numCurrencyID
				,0 as numComissionID
				,'''' as vcRefOrderNo
				,0
				,0 AS numCheckHeaderID  
			 FROM   public.ReturnHeader RH JOIN public.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN public.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			INNER JOIN DivisionMaster DM ON RH.numDivisionId = DM.numDivisionID
			INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
			LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=RH.numContactId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '  AND COA.vcAccountCode LIKE ''01020102%''
					AND (monBizDocAmount > 0) AND coalesce(RH.numBillPaymentIDRef,0)>0
					AND (Rh.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
					AND GJH.datEntry_Date <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';
   
   v_strSqlCredit := CONCAT(' UNION 
							SELECT 
								BPH.numBillPaymentID AS numOppId,
								''Credit'' vcPOppName,
								-1 AS tintOppType,
								0 numOppBizDocsId,
								'''' AS vcBizDocID,
								coalesce(monPaymentAmount,0) as TotalAmount,
								coalesce(monAppliedAmount,0) as AmountPaid,
								coalesce(monPaymentAmount,0) - coalesce(monAppliedAmount,0) as BalanceDue,
								FormatedDateFromDate(BPH.dtPaymentDate,BPH.numDomainID) AS DueDate,
								false bitBillingTerms,
								0 intBillingDays,
								BPH.dtCreateDate dtCreatedDate,
								'''' varCurrSymbol,
								1 AS fltExchangeRate,
								BPH.dtCreateDate AS dtFromDate,
								0 as numBizDocsPaymentDetId,
								1 AS CurrencyCount
								,0 as numComissionID
								,'''' as vcRefOrderNo
								,0
								,0 AS numCheckHeaderID  
							FROM 
								BillPaymentHeader BPH
							WHERE 
								BPH.numDomainId=',v_numDomainId,'
								AND (BPH.numDivisionId =', 
   v_numDivisionID,' OR ',v_numDivisionID,
   ' IS NULL)
								AND (coalesce(monPaymentAmount,0) - coalesce(monAppliedAmount,0)) > 0
								AND (BPH.numAccountClass=',v_numAccountClass,' OR ',
   v_numAccountClass,'=0)
								AND BPH.dtPaymentDate <= ''',
   v_dtToDate,''''); 

	
    
    
   IF (v_vcFlag = '0+30') then
      
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND BH.dtDueDate BETWEEN public.GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day'' ';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND CH.dtCheckDate BETWEEN public.GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day'' ';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND 
            					dtFromDate  + make_interval(days => (CASE 
                                 WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                 ELSE 0
                               END))   
							   BETWEEN GetUTCDateWithoutTime() AND GetUTCDateWithoutTime()+ interval ''31 day''
               AND GetUTCDateWithoutTime() <= 
			   dtFromDate + make_interval(days => (CASE 
                                                 WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                 ELSE 0
                                               END))
			    ';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND 
               dtFromDate + make_interval(days => (CASE 
                                 WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                 ELSE 0
                               END)) BETWEEN GetUTCDateWithoutTime() AND GetUTCDateWithoutTime()+ interval ''31 day''
               AND GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => (CASE 
                                                 WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                 ELSE 0
                                               END)) ';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime() + interval ''31 day'' ';
   ELSEIF (v_vcFlag = '30+60')
   then
        
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND BH.dtDueDate BETWEEN public.GetUTCDateWithoutTime() + interval ''31 day'' AND public.GetUTCDateWithoutTime() + interval ''61 day''';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND CH.dtCheckDate BETWEEN public.GetUTCDateWithoutTime() + interval ''31 day'' AND public.GetUTCDateWithoutTime() + interval ''61 day''';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                 dtFromDate + make_interval(days => CASE 
                                   WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                   ELSE 0
                                 END) BETWEEN public.GetUTCDateWithoutTime() + interval ''31 day'' AND public.GetUTCDateWithoutTime() + interval ''61 day''
                 AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                   WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                   ELSE 0
                                                 END)';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                 dtFromDate + make_interval(days => CASE 
                                   WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                   ELSE 0
                                 END) BETWEEN public.GetUTCDateWithoutTime() + interval ''31 day'' AND public.GetUTCDateWithoutTime() + interval ''61 day''
                 AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                   WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                   ELSE 0
                                                 END)';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime() + interval ''31 day'' AND public.GetUTCDateWithoutTime() + interval ''61 day''';
   ELSEIF (v_vcFlag = '60+90')
   then
          
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND BH.dtDueDate BETWEEN public.GetUTCDateWithoutTime() + interval ''61 day'' AND public.GetUTCDateWithoutTime() + interval ''91 day''';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND CH.dtCheckDate BETWEEN public.GetUTCDateWithoutTime() + interval ''61 day'' AND public.GetUTCDateWithoutTime() + interval ''91 day''';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                   dtFromDate + make_interval(days => CASE 
                                     WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                     ELSE 0
                                   END) BETWEEN public.GetUTCDateWithoutTime() + interval ''61 day'' AND public.GetUTCDateWithoutTime() + interval ''91 day''
                   AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                     WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                     ELSE 0
                                                   END) ';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                   dtFromDate + make_interval(days => CASE 
                                     WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                     ELSE 0
                                   END) BETWEEN public.GetUTCDateWithoutTime() + interval ''61 day'' AND public.GetUTCDateWithoutTime() + interval ''91 day''
                   AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                     WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                     ELSE 0
                                                   END) ';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime() + interval ''61 day'' AND public.GetUTCDateWithoutTime() + interval ''91 day''';
   ELSEIF (v_vcFlag = '90+')
   then
            
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND BH.dtDueDate > public.GetUTCDateWithoutTime() + interval ''91 day''';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND CH.dtCheckDate > public.GetUTCDateWithoutTime() + interval ''91 day''';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                     dtFromDate + make_interval(days => CASE 
                                       WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                       ELSE 0
                                     END) > public.GetUTCDateWithoutTime() + interval ''91 day''
                     AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                       WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                       ELSE 0
                                                     END)';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                     dtFromDate + make_interval(days => CASE 
                                       WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                       ELSE 0
                                     END) > public.GetUTCDateWithoutTime() + interval ''91 day''
                     AND public.GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                       WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                       ELSE 0
                                                     END)';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND GJH.datEntry_Date > public.GetUTCDateWithoutTime() + interval ''91 day''';
   ELSEIF (v_vcFlag = '0-30')
   then
              
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND datediff(''day'',BH.dtDueDate,public.GetUTCDateWithoutTime()) BETWEEN 0 AND 30';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND datediff(''day'',CH.dtCheckDate ,public.GetUTCDateWithoutTime()) BETWEEN 0 AND 30';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                       public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END)
                       AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                      WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                      ELSE 0
                                                    END),public.GetUTCDateWithoutTime()) BETWEEN 0 AND 30';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                       public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END)
                       AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                      WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                      ELSE 0
                                                    END),public.GetUTCDateWithoutTime()) BETWEEN 0 AND 30';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND datediff(''day'',GJH.datEntry_Date,public.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
   ELSEIF (v_vcFlag = '30-60')
   then
                
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND datediff(''day'',BH.dtDueDate,public.GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND datediff(''day'',CH.dtCheckDate,public.GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                         public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END)
                         AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END),public.GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                         public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END)
                         AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END),public.GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND datediff(''day'',GJH.datEntry_Date,public.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
   ELSEIF (v_vcFlag = '60-90')
   then
                  
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND datediff(''day'',BH.dtDueDate,public.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND datediff(''day'',CH.dtCheckDate,public.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                           public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END)
                           AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END),public.GetUTCDateWithoutTime()) BETWEEN 61 AND 90';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                           public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END)
                           AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END),public.GetUTCDateWithoutTime()) BETWEEN 61 AND 90';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND datediff(''day'',GJH.datEntry_Date,public.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
   ELSEIF (v_vcFlag = '90-')
   then
                    
      v_strSqlBill := coalesce(v_strSqlBill,'') || ' AND datediff(''day'',BH.dtDueDate,public.GetUTCDateWithoutTime()) > 90 ';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || ' AND datediff(''day'',CH.dtCheckDate,public.GetUTCDateWithoutTime()) > 90 ';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || ' AND  
                             public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                              WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                              ELSE 0
                                                            END)
                             AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END),public.GetUTCDateWithoutTime()) > 90';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || ' AND  
                             public.GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                              WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                              ELSE 0
                                                            END)
                             AND datediff(''day'',dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END),public.GetUTCDateWithoutTime()) > 90';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || ' AND datediff(''day'',GJH.datEntry_Date,public.GetUTCDateWithoutTime()) > 90 ';
   ELSE
      v_strSqlBill := coalesce(v_strSqlBill,'') || '';
      v_strSqlCheck := coalesce(v_strSqlCheck,'') || '';
      v_strSqlOrder := coalesce(v_strSqlOrder,'') || '';
      v_strSqlCommission := coalesce(v_strSqlCommission,'') || '';
      v_strSqlJournal := coalesce(v_strSqlJournal,'') || '';
   end if;
                    
                    
   --RAISE NOTICE '%',(v_strSqlBill);
   -- RAISE NOTICE '%',(v_strSqlCheck);
   -- RAISE NOTICE '%',(v_strSqlOrder);
   -- RAISE NOTICE '%',(v_strSqlCommission);
   -- RAISE NOTICE '%',(v_strSqlJournal);
   -- RAISE NOTICE '%',(v_strSqlCredit);
   -- RAISE NOTICE '%',(v_strReturn);
	
   OPEN SWV_RefCur FOR EXECUTE coalesce(v_strSqlBill,'') || coalesce(v_strSqlCheck,'') || coalesce(v_strSqlOrder,'') || coalesce(v_strSqlCommission,'') || coalesce(v_strSqlJournal,'') || coalesce(v_strSqlCredit,'') || coalesce(v_strReturn,'');
   RETURN;
END; $$;


