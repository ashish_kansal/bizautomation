DROP FUNCTION IF EXISTS USP_ElasticSearch_GetProjects;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetProjects(v_numDomainID NUMERIC(18,0)
	,v_vcProjectIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numProId AS id
		,'project' AS module
		,coalesce(ProjectsMaster.numRecOwner,0) AS "numRecOwner"
		,coalesce(ProjectsMaster.numAssignedTo,0) AS "numAssignedTo"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,CONCAT('<b style="color:#7030a0">Project:</b> ',vcProjectName,', ',vcCompanyName) AS displaytext
		,vcProjectName AS text
		,CONCAT('/projects/frmProjects.aspx?frm=ProjectList&ProId=',numProId) AS url
		,coalesce(vcProjectID,'') AS "Search_vcProjectID"
		,coalesce(vcProjectName,'') AS "Search_vcProjectName"
   FROM
   ProjectsMaster
   INNER JOIN
   DivisionMaster
   ON
   ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   ProjectsMaster.numdomainId = v_numDomainID
   AND (NULLIF(v_vcProjectIds,'') IS NULL OR numProId IN(SELECT id FROM SplitIDs(coalesce(v_vcProjectIds,''),',')));
END; $$;

