-- Stored procedure definition script usp_GetContactUSupportKeyForDivision for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactUSupportKeyForDivision(v_numDivID NUMERIC DEFAULT 0
	--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT ACI.numContactId, ACI.vcFirstName, ACI.vcLastname, cast(UKM.numUniversalSupportKey as VARCHAR(255))
   FROM AdditionalContactsInformation ACI INNER JOIN UniversalSupportKeyMaster UKM ON UKM.numDivisionID = ACI.numDivisionId
   WHERE ACI.numDivisionId = v_numDivID;
END; $$;












