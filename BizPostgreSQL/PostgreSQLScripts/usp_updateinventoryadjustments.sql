DROP FUNCTION IF EXISTS USP_UpdateInventoryAdjustments;

CREATE OR REPLACE FUNCTION USP_UpdateInventoryAdjustments(v_numDomainId NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
    v_strItems VARCHAR(8000),
    v_dtAdjustmentDate TIMESTAMP,
	v_fltReorderQty DOUBLE PRECISION,
	v_fltReorderPoint DOUBLE PRECISION,
	v_bitAutomateReorderPoint BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
	-- TRANSACTION IS USED IN CODE
   AS $$
   DECLARE
   v_hDoc  INTEGER;
   v_numOnHand  DOUBLE PRECISION;
   v_numAllocation  DOUBLE PRECISION;
   v_numBackOrder  DOUBLE PRECISION;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numTempItemCode  NUMERIC(18,0);
			
   v_bitLotNo  BOOLEAN DEFAULT 0;  
   v_bitSerialized  BOOLEAN DEFAULT 0;  
   v_monItemAvgCost  DECIMAL(20,5);
   v_monUnitCost  DECIMAL(20,5);
   v_monAvgCost  DECIMAL(20,5); 
   v_TotalOnHand  DOUBLE PRECISION;
   v_numNewQtyReceived  DOUBLE PRECISION;

   v_minRowNo  NUMERIC(18,0);
   v_maxRowNo  NUMERIC(18,0);
   v_numWareHouseID  NUMERIC(9,0);
   v_vcSerialNo  VARCHAR(3000);
   v_numWareHouseItmsDTLID  NUMERIC(9,0);
   v_vcComments  VARCHAR(1000);
   v_OldQty  DOUBLE PRECISION;
   v_numQty  DOUBLE PRECISION;
   v_byteMode  SMALLINT; 		    
   v_posComma  INTEGER;
   v_strKeyVal  VARCHAR(20);
   SWV_RCur REFCURSOR;
   v_posBStart  INTEGER;
   v_posBEnd  INTEGER;
   v_strQty  VARCHAR(20);
   v_strName  VARCHAR(20);
   SWV_RowCount INTEGER;
BEGIN
   UPDATE Item SET fltReorderQty = v_fltReorderQty,bitAutomateReorderPoint = v_bitAutomateReorderPoint WHERE numItemCode = v_numItemCode;
   UPDATE WareHouseItems SET numReorder = v_fltReorderPoint WHERE numItemID = v_numItemCode;


   IF LENGTH(coalesce(v_strItems,'')) > 0 then
      DROP TABLE IF EXISTS tt_TEMPUpdateInventoryAdjustments CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPUpdateInventoryAdjustments ON COMMIT DROP AS
         SELECT * FROM XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numWareHouseItemID NUMERIC(18,0) PATH 'numWareHouseItemID',
					numItemCode NUMERIC(18,0) PATH 'numItemCode',
					intAdjust DOUBLE PRECISION PATH 'intAdjust',
					monAverageCost DECIMAL(20,5) PATH 'monAverageCost'
			) AS X;

      DROP TABLE IF EXISTS tt_TEMPSERIALLOTNO CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSERIALLOTNO ON COMMIT DROP AS
         SELECT *
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo
		
         FROM
		 XMLTABLE
			(
				'NewDataSet/SerialLotNo'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numItemCode NUMERIC(18,0) PATH 'numItemCode',
					numWareHouseID NUMERIC(18,0) PATH 'numWareHouseID',
					numWareHouseItemID NUMERIC(18,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(3000) PATH 'vcSerialNo',
					byteMode SMALLINT PATH 'byteMode'
			) AS X;
        
      select   MIN(numWarehouseItemID) INTO v_numWareHouseItemID FROM
      tt_TEMPUpdateInventoryAdjustments;
      WHILE v_numWareHouseItemID > 0 LOOP
         v_monAvgCost := 0;
         v_TotalOnHand := 0;
         v_numNewQtyReceived := 0;
         select   coalesce(Item.bitLotNo,false), coalesce(Item.bitSerialized,false), (CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(Item.monAverageCost,0) END), (CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(tt_TEMPUpdateInventoryAdjustments.monAverageCost,0) END), coalesce(tt_TEMPUpdateInventoryAdjustments.intAdjust,0) INTO v_bitLotNo,v_bitSerialized,v_monItemAvgCost,v_monUnitCost,v_numNewQtyReceived FROM
         Item
         INNER JOIN
         tt_TEMPUpdateInventoryAdjustments
         ON
         Item.numItemCode = tt_TEMPUpdateInventoryAdjustments.numItemCode
         INNER JOIN
         WareHouseItems
         ON
         tt_TEMPUpdateInventoryAdjustments.numWareHouseItemID = WareHouseItems.numWareHouseItemID WHERE
         tt_TEMPUpdateInventoryAdjustments.numWareHouseItemID = v_numWareHouseItemID
         AND Item.numDomainID = v_numDomainId
         AND WareHouseItems.numDomainID = v_numDomainId    LIMIT 1;
         IF v_bitLotNo = false AND v_bitSerialized = false then
			
            IF(SELECT
            coalesce(numOnHand,0)+coalesce(numAllocation,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust
            FROM
            WareHouseItems WI
            INNER JOIN
            tt_TEMPUpdateInventoryAdjustments
            ON
            WI.numWareHouseItemID = tt_TEMPUpdateInventoryAdjustments.numWareHouseItemID
            WHERE
            WI.numWareHouseItemID = v_numWareHouseItemID AND WI.numDomainID = v_numDomainId) >= 0 then
				
               select   numItemID INTO v_numTempItemCode FROM WareHouseItems WHERE numWareHouseItemID = v_numWareHouseItemID AND numDomainID = v_numDomainId;
								
					--Updating the Average Cost
               select(SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0))) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_numTempItemCode;
               IF v_TotalOnHand+v_numNewQtyReceived > 0 then
					
                  v_monAvgCost :=((v_TotalOnHand*v_monItemAvgCost)+(v_numNewQtyReceived*v_monUnitCost))/(v_TotalOnHand+v_numNewQtyReceived);
               ELSE
                  v_monAvgCost := v_monItemAvgCost;
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END),bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID
               WHERE
               numItemCode = v_numTempItemCode;
					 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
               IF(SELECT intAdjust FROM tt_TEMPUpdateInventoryAdjustments WHERE numWarehouseItemID = v_numWareHouseItemID) >= 0 then
					
                  UPDATE
                  WareHouseItems WI
                  SET
                  numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= tt_TEMPUpdateInventoryAdjustments.intAdjust THEN  0 ELSE(tt_TEMPUpdateInventoryAdjustments.intAdjust -coalesce(numBackOrder,0)) END),numAllocation = coalesce(numAllocation,0)+(CASE WHEN coalesce(numBackOrder,0) >= tt_TEMPUpdateInventoryAdjustments.intAdjust THEN  tt_TEMPUpdateInventoryAdjustments.intAdjust ELSE coalesce(numBackOrder,0) END),numBackOrder =(CASE WHEN coalesce(numBackOrder,0) >= tt_TEMPUpdateInventoryAdjustments.intAdjust THEN(coalesce(numBackOrder,0) -tt_TEMPUpdateInventoryAdjustments.intAdjust) ELSE 0 END),
                  dtModified = LOCALTIMESTAMP
                  FROM
                  tt_TEMPUpdateInventoryAdjustments
                  WHERE
                  WI.numWareHouseItemID = tt_TEMPUpdateInventoryAdjustments.numWareHouseItemID AND(WI.numWareHouseItemID = v_numWareHouseItemID
                  AND WI.numDomainID = v_numDomainId);
               ELSE
                  UPDATE
                  WareHouseItems WI
                  SET
                  numOnHand =(CASE WHEN coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust >= 0 THEN coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust ELSE 0 END),numAllocation =(CASE WHEN coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust < 0 THEN(CASE WHEN coalesce(numAllocation,0)+(coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust) >= 0 THEN coalesce(numAllocation,0)+(coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust) ELSE 0 END) ELSE coalesce(numAllocation,0) END),
                  numBackOrder =(CASE WHEN coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust < 0 THEN coalesce(numBackOrder,0) -(CASE WHEN coalesce(numAllocation,0)+(coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust) >= 0 THEN coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust ELSE coalesce(numAllocation,0)+(coalesce(numOnHand,0)+tt_TEMPUpdateInventoryAdjustments.intAdjust) END) ELSE coalesce(numBackOrder,0) END),dtModified = LOCALTIMESTAMP
                  FROM
                  tt_TEMPUpdateInventoryAdjustments
                  WHERE
                  WI.numWareHouseItemID = tt_TEMPUpdateInventoryAdjustments.numWareHouseItemID AND(WI.numWareHouseItemID = v_numWareHouseItemID
                  AND WI.numDomainID = v_numDomainId);
               end if;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numTempItemCode,
               v_tintRefType := 1::SMALLINT,v_vcDescription := 'Inventory Adjustment',
               v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtAdjustmentDate,
               v_numDomainId := v_numDomainId,SWV_RefCur := null);
            ELSE
               RAISE EXCEPTION 'INVALID_INVENTORY_ADJUSTMENT_NUMBER';
            end if;
         end if;
         select   numWarehouseItemID INTO v_numWareHouseItemID FROM
         tt_TEMPUpdateInventoryAdjustments WHERE
         numWarehouseItemID > v_numWareHouseItemID   ORDER BY
         numWarehouseItemID LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numWareHouseItemID := 0;
         end if;
      END LOOP;
      DROP TABLE IF EXISTS tt_TEMPUpdateInventoryAdjustments CASCADE;
    
    
		-------------Serial/Lot #s----------------
      select   MIN(RowNo), Max(RowNo) INTO v_minRowNo,v_maxRowNo FROM tt_TEMPSERIALLOTNO;
      WHILE v_minRowNo <= v_maxRowNo LOOP
         select   numItemCode, numWareHouseID, numWareHouseItemID, vcSerialNo, byteMode INTO v_numTempItemCode,v_numWareHouseID,v_numWareHouseItemID,v_vcSerialNo,v_byteMode FROM tt_TEMPSERIALLOTNO WHERE RowNo = v_minRowNo;
         v_posComma := 0;
         v_vcSerialNo := RTRIM(v_vcSerialNo);
         IF SUBSTR(v_vcSerialNo,length(v_vcSerialNo) -1+1) != ',' then 
            v_vcSerialNo := coalesce(v_vcSerialNo,'') || ',';
         end if;
         v_posComma := coalesce(POSITION(substring(v_vcSerialNo from E'\\,') IN v_vcSerialNo),
         0);
         WHILE v_posComma > 1 LOOP
            v_strKeyVal := ltrim(rtrim(SUBSTR(v_vcSerialNo,1,v_posComma -1)));
            v_posBStart := coalesce(POSITION(substring(v_strKeyVal from E'\\(') IN v_strKeyVal),0);
            v_posBEnd := coalesce(POSITION(substring(v_strKeyVal from E'\\)') IN v_strKeyVal),0);
            IF(v_posBStart > 1 AND v_posBEnd > 1) then
					
               v_strName := ltrim(rtrim(SUBSTR(v_strKeyVal,1,v_posBStart -1)));
               v_strQty := ltrim(rtrim(SUBSTR(v_strKeyVal,v_posBStart+1,LENGTH(v_strKeyVal) -v_posBStart -1)));
            ELSE
               v_strName := v_strKeyVal;
               v_strQty := CAST(1 AS VARCHAR(20));
            end if;
            v_numWareHouseItmsDTLID := 0;
            v_OldQty := 0;
            v_vcComments := '';
            select   coalesce(numWareHouseItmsDTLID,0), coalesce(numQty,0), coalesce(vcComments,'') INTO v_numWareHouseItmsDTLID,v_OldQty,v_vcComments from WareHouseItmsDTL where numWareHouseItemID = v_numWareHouseItemID and
            vcSerialNo = v_strName AND coalesce(numQty,0) > 0    LIMIT 1;
				 --  numeric(9, 0)
						 --  numeric(9, 0)
						 --  numeric(9, 0)
						 --  varchar(100)
						 -- varchar(1000)
						 --  numeric(18, 0)
						 --  tinyint
						 --  numeric(18, 0)
						 --  numeric(9, 0)
            IF v_byteMode = 0 then --Add
               v_numQty := v_OldQty+v_strQty;
            ELSE --Deduct
               v_numQty := v_OldQty -v_strQty;
            end if;
            SELECT * INTO v_numWareHouseItemID FROM USP_AddUpdateWareHouseForItems(v_numItemCode := v_numTempItemCode,v_numWareHouseID := v_numWareHouseID,
            v_numWareHouseItemID := v_numWareHouseItemID,v_numDomainId := v_numDomainId,
            v_vcSerialNo := v_strName,v_vcComments := v_vcComments,v_numQty := v_numQty,
            v_byteMode := 5::SMALLINT,v_numWareHouseItmsDTLID := v_numWareHouseItmsDTLID,
            v_numUserCntID := v_numUserCntID,v_dtAdjustmentDate := v_dtAdjustmentDate);
            v_vcSerialNo := SUBSTR(v_vcSerialNo,v_posComma+1,LENGTH(v_vcSerialNo) -v_posComma);
            v_posComma := coalesce(POSITION(substring(v_vcSerialNo from E'\\,') IN v_vcSerialNo),
            0);
         END LOOP;
         v_minRowNo := v_minRowNo+1;
      END LOOP;
      DROP TABLE IF EXISTS tt_TEMPSERIALLOTNO CASCADE;
      UPDATE Item SET bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$;




