-- Stored procedure definition script USP_ManageInventoryWorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageInventoryWorkOrder(v_numWOId NUMERIC(18,0),
v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;

  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_Description  VARCHAR(1000);
   v_numChildItemCode  NUMERIC(18,0);
   v_numChildItemQty  DOUBLE PRECISION;
   v_numChildOrgQtyRequired  DOUBLE PRECISION;
   v_bitChildIsAssembly  BOOLEAN;
   v_numQuantityToBuild  DOUBLE PRECISION;
   v_numChildItemWarehouseID  NUMERIC(18,0);
   v_numChildItemWarehouseItemID  NUMERIC(18,0);
   v_numOnHand  DOUBLE PRECISION;
   v_numOnOrder  DOUBLE PRECISION;
   v_numOnAllocation  DOUBLE PRECISION;
   v_numOnBackOrder  DOUBLE PRECISION;
   v_numNewOppID  NUMERIC(18,0) DEFAULT 0;
   v_QtyToBuild  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_bitReOrderPoint  BOOLEAN DEFAULT false;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_tintOppStautsForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_numReOrderPointOrderStatus  NUMERIC(18,0) DEFAULT 0;
   v_tintOppStatus  SMALLINT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
BEGIN
         CREATE TEMP SEQUENCE tt_TEMPITEM_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEM
      (
         RowNo INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
         numItemCode INTEGER,
         numQty DOUBLE PRECISION,
         numOrgQtyRequired DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         bitAssembly BOOLEAN
      );
      INSERT INTO tt_TEMPITEM(numItemCode, numQty, numOrgQtyRequired, numWarehouseItemID, bitAssembly)  SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID = Item.numItemCode WHERE numWOId = v_numWOId AND coalesce(numWareHouseItemId,0) > 0;
      select   COUNT(RowNo) INTO v_Count FROM tt_TEMPITEM;

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
      IF coalesce(v_Count,0) > 0 then
	

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
         WHILE v_i <= v_Count LOOP
            select   numItemCode, numWarehouseItemID, numQty, numOrgQtyRequired, bitAssembly INTO v_numChildItemCode,v_numChildItemWarehouseItemID,v_numChildItemQty,v_numChildOrgQtyRequired,
            v_bitChildIsAssembly FROM tt_TEMPITEM WHERE RowNo = v_i;
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
            select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
            WareHouseItems WHERE
            numWareHouseItemID = v_numChildItemWarehouseItemID;
            v_Description := CONCAT('Items Allocated For Work Order (Qty:',v_numChildItemQty,')');
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
            IF coalesce(v_bitChildIsAssembly,false) = true then
			
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
               IF v_numOnHand >= v_numChildItemQty then
				
                  v_numOnHand := v_numOnHand -v_numChildItemQty;
                  v_numOnAllocation := v_numOnAllocation+v_numChildItemQty;  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                  PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                  v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                  v_numReferenceID := v_numWOId,
                  v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                  v_Description := v_Description);    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
               ELSEIF v_numOnHand < v_numChildItemQty
               then
				
                  v_numOnAllocation := v_numOnAllocation+v_numOnHand;
                  v_numOnBackOrder := v_numOnBackOrder+(v_numChildItemQty -v_numOnHand);
                  v_QtyToBuild :=(v_numChildItemQty -v_numOnHand);
                  v_numQtyShipped := 0;
                  v_numOnHand := 0;   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
                  PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                  v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                  v_numReferenceID := v_numWOId,
                  v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                  v_Description := v_Description);
                  select   numWareHouseID INTO v_numChildItemWarehouseID FROM
                  WareHouseItems WHERE
                  numWareHouseItemID = v_numChildItemWarehouseItemID;
					
					-- CHEKC IF INTERNAL LOCATION OF WAREHOUSE HAVE QUANTITY AVAILABLE
                  IF coalesce((SELECT
                  SUM(numOnHand)
                  FROM
                  WareHouseItems
                  WHERE
                  numItemID = v_numChildItemCode
                  AND numWareHouseID = v_numChildItemWarehouseID
                  AND numWareHouseItemID <> v_numChildItemWarehouseItemID),0) > 0 then
					
                     v_QtyToBuild := v_QtyToBuild -coalesce((SELECT
                     SUM(numOnHand)
                     FROM
                     WareHouseItems
                     WHERE
                     numItemID = v_numChildItemCode
                     AND numWareHouseID = v_numChildItemWarehouseID
                     AND numWareHouseItemID <> v_numChildItemWarehouseItemID),0);
                  end if;
                  IF v_QtyToBuild > 0 then
					
                     PERFORM USP_WorkOrder_InsertRecursive(0,0,0,0,v_numChildItemCode,v_QtyToBuild,v_numChildItemWarehouseItemID,
                     v_numDomainID,v_numUserCntID,v_numQtyShipped,v_numWOId,true);
                  end if;
               end if;
            ELSE
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
               IF v_numOnHand >= v_numChildItemQty then
				
                  v_numOnHand := v_numOnHand -v_numChildItemQty;
                  v_numOnAllocation := v_numOnAllocation+v_numChildItemQty;        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                  PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                  v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                  v_numReferenceID := v_numWOId,
                  v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                  v_Description := v_Description);    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
               ELSEIF v_numOnHand < v_numChildItemQty
               then
                  v_numOnAllocation := v_numOnAllocation+v_numOnHand;
                  v_numOnBackOrder := v_numOnBackOrder+(v_numChildItemQty -v_numOnHand);
                  v_QtyToBuild :=(v_numChildItemQty -v_numOnHand);
                  v_numOnHand := 0; 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                  PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                  v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                  v_numReferenceID := v_numWOId,
                  v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                  v_Description := v_Description); 

					-- AUTO CREATE BACK ORDER PO IF GLOBAL SETTINGS IS CONFIGURED FOR IT
                  select   coalesce(bitReOrderPoint,false), coalesce(numReOrderPointOrderStatus,0), coalesce(tintOppStautsForAutoPOBackOrder,0), coalesce(tintUnitsRecommendationForAutoPOBackOrder,0) INTO v_bitReOrderPoint,v_numReOrderPointOrderStatus,v_tintOppStautsForAutoPOBackOrder,
                  v_tintUnitsRecommendationForAutoPOBackOrder FROM
                  Domain WHERE
                  numDomainId = v_numDomainID;
                  IF v_bitReOrderPoint = true AND (v_tintOppStautsForAutoPOBackOrder = 2 OR v_tintOppStautsForAutoPOBackOrder = 3) then --2:Purchase Opportunity, 3: Purchase Order
					
                     v_tintOppStatus :=(CASE WHEN v_tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END);

						--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
                     SELECT * INTO v_numNewOppID FROM USP_OpportunityMaster_CreatePO(v_numNewOppID,v_numDomainID,v_numUserCntID,v_numChildItemCode,v_QtyToBuild,
                     v_numChildItemWarehouseItemID,true,v_tintOppStatus::SMALLINT,v_numReOrderPointOrderStatus);
                     UPDATE WorkOrderDetails SET numPOID = v_numNewOppID WHERE numWOId = v_numWOId AND numChildItemID = v_numChildItemCode AND numWareHouseItemId = v_numChildItemWarehouseItemID;
                  end if;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



