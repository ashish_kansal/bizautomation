-- Stored procedure definition script USP_GetTerritoryForUsrMst for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTerritoryForUsrMst(v_numUserID NUMERIC(9,0),  
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numlistitemid as VARCHAR(255)),cast(vcdata as VARCHAR(255)) from userterritories
   join Listdetails
   on numlistitemid = numterritory
   where numUserID = v_numUserID
   and userterritories.numDomainID = v_numDomainID;
END; $$;












