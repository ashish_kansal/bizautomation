CREATE OR REPLACE FUNCTION USP_GetSalesOrderItems
(
	p_numSalesOrderId NUMERIC,
    p_numDomainID NUMERIC,
	INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   SELECT  X.*, (SELECT vcCategoryName FROM Category WHERE numCategoryID IN (SELECT numCategoryID FROM ItemCategory IC WHERE X.numItemCode = IC.numItemID) LIMIT 1) AS vcCategoryName
    FROM    ( SELECT  DISTINCT
                        STI.numItemCode,
                        STI.numUnitHour,
                        STI.monPrice,
                        STI.monTotAmount,
                        STI.numSourceID,
                        STI.numWarehouseItmsID,
                        I.bitFreeShipping FreeShipping, --Use from Item table
                        STI.bitDiscountType,
                        STI.fltDiscount,
                        STI.monTotAmtBefDiscount,
                        OM.numDomainID,
                        '' vcItemDesc,
                       (SELECT vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=true LIMIT 1) vcPathForTImage,
                        I.vcItemName,
                        'False' AS DropShip,
					    0 as bitDropShip,
                        I.bitTaxable,
                        COALESCE(STI.numUOMId,0) numUOM,
						COALESCE(STI.numUOMId,0) as numUOMId,
                        COALESCE(U.vcUnitName,'') vcUOMName,
						COALESCE(STI.numVendorWareHouse,0) as numVendorWareHouse,COALESCE(STI.numShipmentMethod,0) as numShipmentMethod,COALESCE(STI.numSOVendorId,0) as numSOVendorId,
						0 as bitWorkOrder,I.charItemType,fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder,
						COALESCE(I.bitLotNo,false) AS bitLotNo
--                        COALESCE(fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numdomainid,0),1) UOMConversionFactor
              FROM      OpportunityItems STI
                        INNER JOIN item I ON I.numItemCode = STI.numItemCode
                        LEFT JOIN UOM U ON U.numUOMID = I.numSaleUnit
						LEFT JOIN OpportunityMaster AS OM ON OM.numOppId=STI.numOppId
              WHERE     STI.numOppId= p_numSalesOrderId
                        AND OM.numDomainId = p_numDomainID
            ) X;
END; $$;