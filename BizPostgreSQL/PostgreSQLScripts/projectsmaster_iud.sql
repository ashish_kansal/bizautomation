CREATE OR REPLACE FUNCTION ProjectsMaster_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_action  CHAR(1) DEFAULT 'I';
   v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
	 v_TempnumProId  NUMERIC;
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Project', OLD.numProId, 'Delete', OLD.numdomainId);
		v_tintWFTriggerOn := 5;
		v_numDomainID := OLD.numDomainID;
		v_numRecordID := OLD.numProId;
		v_numUserCntID := OLD.numModifiedBy;
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Project', NEW.numProId, 'Update', NEW.numdomainId);
		v_tintWFTriggerOn := 2;
		v_numDomainID := NEW.numDomainID;
		v_numRecordID := NEW.numProId;
		v_numUserCntID := NEW.numModifiedBy;


		IF OLD.numAssignedBy <> NEW.numAssignedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedBy,');
		END IF;
		IF OLD.numAssignedTo <> NEW.numAssignedTo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedTo,');
		END IF;
		IF OLD.bintProClosingDate <> NEW.bintProClosingDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintProClosingDate,');
		END IF;
		IF OLD.txtComments <> NEW.txtComments THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'txtComments,');
		END IF;
		IF OLD.numContractId <> NEW.numContractId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numContractId,');
		END IF;
		IF OLD.numCreatedBy <> NEW.numCreatedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCreatedBy,');
		END IF;
		IF OLD.bintCreatedDate <> NEW.bintCreatedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintCreatedDate,');
		END IF;
		IF OLD.numCustPrjMgr <> NEW.numCustPrjMgr THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCustPrjMgr,');
		END IF;
		IF OLD.intDueDate <> NEW.intDueDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intDueDate,');
		END IF;
		IF OLD.numintPrjMgr <> NEW.numintPrjMgr THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numintPrjMgr,');
		END IF;
		IF OLD.numOppId <> NEW.numOppId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numOppId,');
		END IF;
		IF OLD.vcProjectID <> NEW.vcProjectID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcProjectID,');
		END IF;
		IF OLD.vcProjectName <> NEW.vcProjectName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcProjectName,');
		END IF;
		IF OLD.numProjectStatus <> NEW.numProjectStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numProjectStatus,');
		END IF;
		IF OLD.numProjectType <> NEW.numProjectType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numProjectType,');
		END IF;
		IF OLD.numRecOwner <> NEW.numRecOwner THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numRecOwner,');
		END IF;
		IF OLD.numAccountID <> NEW.numAccountID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAccountID,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Project', NEW.numProId, 'Insert', NEW.numdomainId);
		v_tintWFTriggerOn := 1;
		v_numDomainID := NEW.numDomainID;
		v_numRecordID := NEW.numProId;
		v_numUserCntID := NEW.numCreatedBy;
	END IF;

	IF v_tintWFTriggerOn = 1 then
	
      IF EXISTS(SELECT numProId FROM ProjectsMaster_TempDateFields WHERE numProId = v_numRecordID) then
		
         SELECT PM.numProId INTO v_TempnumProId FROM ProjectsMaster PM WHERE  (
				(PM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
				(PM.bintProClosingDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintProClosingDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
				(PM.intDueDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.intDueDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND PM.numProId = v_numRecordID;
         IF(v_TempnumProId IS NOT NULL) then
			
            UPDATE ProjectsMaster_TempDateFields AS PMT
            SET
            bintCreatedDate = PM.bintCreatedDate,bintProClosingDate = PM.bintProClosingDate,
            intDueDate = PM.intDueDate
            FROM
            ProjectsMaster AS PM
            WHERE  PMT.numProId = PM.numProId AND(((PM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(PM.bintProClosingDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintProClosingDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(PM.intDueDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.intDueDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
            AND
            PMT.numProId = v_numRecordID);
         ELSE
            DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = v_numRecordID;
         end if;
      ELSE
         INSERT INTO ProjectsMaster_TempDateFields(numProId, numDomainID, bintCreatedDate, bintProClosingDate,intDueDate)
         SELECT numProId,numdomainId, bintCreatedDate, bintProClosingDate, intDueDate
         FROM ProjectsMaster
         WHERE (
					(CAST(bintCreatedDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintCreatedDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(bintProClosingDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintProClosingDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(intDueDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(intDueDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND numProId = v_numRecordID;
      end if;
   ELSEIF v_tintWFTriggerOn = 2
   then
	
      SELECT PM.numProId INTO v_TempnumProId FROM ProjectsMaster PM WHERE  (
				(PM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(PM.bintProClosingDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintProClosingDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(PM.intDueDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.intDueDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
      AND PM.numProId = v_numRecordID;
      IF(v_TempnumProId IS NOT NULL) then
		
         UPDATE ProjectsMaster_TempDateFields AS PMT
         SET
         bintCreatedDate = PM.bintCreatedDate,bintProClosingDate = PM.bintProClosingDate,
         intDueDate = PM.intDueDate
         FROM
         ProjectsMaster AS PM
         WHERE  PMT.numProId = PM.numProId AND(((PM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(PM.bintProClosingDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.bintProClosingDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(PM.intDueDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND PM.intDueDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND
         PMT.numProId = v_numRecordID);
      ELSE
         DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = v_numRecordID;
      end if;
   ELSEIF v_tintWFTriggerOn = 5
   then
	
      DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = v_numRecordID;
   end if;	

	--DateField WorkFlow Logic
	
   PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 73,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER ProjectsMaster_IUD AFTER INSERT OR UPDATE OR DELETE ON ProjectsMaster FOR EACH ROW EXECUTE PROCEDURE ProjectsMaster_IUD_TrFunc();


