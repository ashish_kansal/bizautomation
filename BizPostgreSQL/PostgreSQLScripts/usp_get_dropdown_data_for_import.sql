CREATE OR REPLACE FUNCTION USP_GET_DROPDOWN_DATA_FOR_IMPORT(v_intImportFileID BIGINT,
	v_numDomainID BIGINT,
	v_tintMode SMALLINT,
	v_numFormID NUMERIC(18,0),
	v_vcFields TEXT,
	INOUT SWV_RefCur refcursor,
	INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
	DECLARE
		v_intRow  INTEGER;
		v_intCnt  INTEGER;
		v_numListIDValue  BIGINT;
		v_numFormFieldIDValue  BIGINT;
		v_bitCustomField  BOOLEAN;
		v_strSQL  TEXT;
		v_strDBColumnName  VARCHAR(1000);
		v_hDocItem  INTEGER;
BEGIN
	DROP TABLE IF EXISTS tt_TEMPFIELDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFIELDS
	(
		numFieldID NUMERIC(18,0),
		intMapColumnNo INTEGER,
		bitCustom BOOLEAN
	);

	IF v_tintMode = 2 then
		INSERT INTO tt_TEMPFIELDS
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			unnest(xpath('//FormFieldID/text()', v_vcFields::xml))::text::numeric as FormFieldID,
			unnest(xpath('//ImportFieldID/text()', v_vcFields::xml))::text::numeric as ImportFieldID,
			unnest(xpath('//IsCustomField/text()', v_vcFields::xml))::text::boolean as IsCustomField;
	ELSE
		SELECT 
			numMasterID INTO v_numFormID 
		FROM 
			Import_File_Master 
		WHERE 
			intImportFileID = v_intImportFileID;

		INSERT INTO tt_TEMPFIELDS
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			numFormFieldID
			,intImportFileID
			,coalesce(bitCustomField,false)
		FROM
			Import_File_Field_Mapping
		WHERE
			intImportFileID = v_intImportFileID;
	END IF;

	DROP TABLE IF EXISTS tt_TEMPDROPDOWNDATA CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPDROPDOWNDATA ON COMMIT DROP AS
    SELECT
		ROW_NUMBER() OVER(ORDER BY intMapColumnNo) AS ID
		,* 
	FROM
	(
		SELECT
			DFFM.numFieldID AS numFormFieldId,
			DFFM.vcAssociatedControlType,
			DYNAMIC_FIELD.vcDbColumnName,
			DFFM.vcFieldName AS vcFormFieldName,
			FIELD_MAP.intMapColumnNo,
			DYNAMIC_FIELD.numListID,
			false AS bitCustomField
		FROM
			tt_TEMPFIELDS FIELD_MAP
		INNER JOIN 
			DycFormField_Mapping DFFM 
		ON 
			FIELD_MAP.numFieldID = DFFM.numFieldID
		INNER JOIN 
			DycFieldMaster DYNAMIC_FIELD 
		ON 
			DYNAMIC_FIELD.numFieldID = DFFM.numFieldID
		WHERE
			coalesce(FIELD_MAP.bitCustom,false) = false
			AND DFFM.vcAssociatedControlType = 'SelectBox'
			AND DFFM.numFormID = v_numFormID
		UNION ALL
		SELECT
			CFm.Fld_id AS numFormFieldId,
			CASE 
				WHEN CFm.fld_type = 'Drop Down List Box'
				THEN 'SelectBox'
				ELSE CFm.fld_type
			END	AS vcAssociatedControlType,
			CFm.FLd_label AS vcDbColumnName,
			CFm.FLd_label AS vcFormFieldName,
			FIELD_MAP.intMapColumnNo  AS intMapColumnNo,
			CFm.numlistid AS numListID,
			true AS bitCustomField
		FROM
			CFW_Fld_Master CFm
		INNER JOIN tt_TEMPFIELDS FIELD_MAP ON CFm.Fld_id = FIELD_MAP.numFieldID AND coalesce(FIELD_MAP.bitCustom,false) = true
		WHERE
			numDomainID = v_numDomainID
	) TABLE1
	WHERE
		vcAssociatedControlType = 'SelectBox'; 
              
	open SWV_RefCur for
	SELECT 
		ID As "ID",
		numFormFieldId AS "numFormFieldId",
		vcAssociatedControlType AS "vcAssociatedControlType",
		vcDbColumnName AS "vcDbColumnName",
		vcFormFieldName AS "vcFormFieldName",
		intMapColumnNo AS "intMapColumnNo",
		numListID AS "numListID",
		bitCustomField AS "bitCustomField"
	FROM 
		tt_TEMPDROPDOWNDATA;   
	
	SELECT COUNT(*) INTO v_intRow FROM tt_TEMPDROPDOWNDATA;
	
	v_intCnt := 0;
	v_strSQL := ''; 

	DROP TABLE IF EXISTS tt_TEMPGROUPS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPGROUPS
	(
		Key TEXT,
		Value TEXT
	); 	
	INSERT INTO tt_TEMPGROUPS(Key,Value)
	SELECT 0 AS Key,'Groups' AS Value
	UNION ALL
	SELECT 1 AS Key,'Prospects' AS Value
	UNION ALL
	SELECT 1 AS Key,'Prospect' AS Value
	UNION ALL
	SELECT 1 AS Key,'Pros' AS Value
	UNION ALL
	SELECT 2 AS Key,'Accounts' AS Value
	UNION ALL
	SELECT 2 AS Key,'Account' AS Value
	UNION ALL
	SELECT 2 AS Key,'Acc' AS Value;

	DROP TABLE IF EXISTS tt_TEMPFIELDSDATA CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFIELDSDATA
	(
		vcFieldID TEXT
		,"Key" TEXT
		,"Value" TEXT
		,vcAbbreviations TEXT
	);

	--SELECT * FROM #tempGroups
	WHILE(v_intCnt < v_intRow) LOOP
		v_intCnt := v_intCnt::bigint+1;
		SELECT 
			numListID, vcDbColumnName, numFormFieldID, bitCustomField INTO v_numListIDValue,v_strDBColumnName,v_numFormFieldIDValue,v_bitCustomField 
		FROM 
			tt_TEMPDROPDOWNDATA 
		WHERE 
			ID = v_intCnt;
      

		IF v_strDBColumnName = 'numItemGroup' THEN 
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),ITEM_GROUP.numItemGroupID AS "Key",ITEM_GROUP.vcItemGroup AS "Value",'' FROM ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'tintCRMType' THEN 
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),Key,Value,'' FROM tt_TEMPGROUPS;
		ELSEIF v_strDBColumnName = 'vcLocation' THEN 
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numWLocationID AS "Key",vcLocation AS "Value",'' FROM WarehouseLocation WHERE numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'vcExportToAPI' THEN 
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),WebApiId AS "Key",vcProviderName AS "Value",'' FROM WebAPI;
		ELSEIF v_strDBColumnName = 'numCategoryID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numCategoryID AS "Key", vcCategoryName AS "Value",'' FROM Category  WHERE numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numGrpID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''), GRP.numGrpID AS "Key",GRP.vcGrpName AS "Value",'' FROM Groups GRP;
		ELSEIF v_strDBColumnName = 'numWareHouseID'	THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''), WAREHOUSE.numWareHouseID AS "Key",WAREHOUSE.vcWareHouse AS "Value",'' FROM Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = v_numDomainID;		 
		ELSEIF v_strDBColumnName = 'numWareHouseItemID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),WI.numWareHouseItemID AS "Key",W.vcWareHouse AS "Value",'' FROM WareHouseItems WI INNER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numChildItemID' OR v_strDBColumnName = 'numItemKitID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),ITEM.numItemCode AS "Key",ITEM.vcItemName AS "Value",'' FROM Item ITEM WHERE ITEM.numDomainID = v_numDomainID;
		ELSEIF (v_strDBColumnName = 'numUOMId' OR v_strDBColumnName = 'numPurchaseUnit' OR v_strDBColumnName = 'numSaleUnit' OR v_strDBColumnName = 'numBaseUnit') THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			) 
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'')
				,numUOMId AS "Key"
				,vcUnitName AS "Value"
				,'' 
			FROM 
				UOM
			JOIN Domain d ON UOM.numDomainID = d.numDomainID 
			WHERE 
				UOM.numDomainId = v_numDomainID
				AND bitEnabled=true
				AND UOM.tintUnitType=(CASE WHEN coalesce(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END);
		ELSEIF (v_strDBColumnName = 'numIncomeChartAcntId' OR v_strDBColumnName = 'numAssetChartAcntId' OR v_strDBColumnName = 'numCOGsChartAcntId') THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numAccountId AS "Key", LTRIM(RTRIM(REPLACE(vcAccountName,coalesce(vcNumber,''),''))) AS "Value",'' FROM Chart_Of_Accounts WHERE numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numCampaignID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numCampaignID AS "Key", vcCampaignName AS "Value",'' FROM CampaignMaster WHERE CampaignMaster.numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numVendorID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),DM.numDivisionID AS "Key", CI.vcCompanyName AS "Value",'' FROM CompanyInfo CI INNER JOIN DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId WHERE DM.numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numAssignedTo' OR v_strDBColumnName = 'numRecOwner' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),A.numContactID AS "Key", A.vcFirstName || ' ' || A.vcLastName AS "Value",'' FROM UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numTermsID' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT DISTINCT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numTermsID AS "Key",vcTerms AS "Value",'' FROM BillingTerms WHERE numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'numShipState' OR v_strDBColumnName = 'numBillState' OR v_strDBColumnName = 'numState' OR v_strDBColumnName = 'State' THEN
			INSERT INTO tt_TEMPFIELDSDATA (vcFieldID,"Key","Value",vcAbbreviations) SELECT DISTINCT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,''),numStateID AS "Key", vcState AS "Value", (CASE WHEN LENGTH(coalesce(vcAbbreviations,'')) > 0 THEN vcAbbreviations ELSE 'NOTAVAILABLE' END) vcAbbreviations FROM State where numDomainID = v_numDomainID;
		ELSEIF v_strDBColumnName = 'charItemType' THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 'A' AS "Key", 'asset' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 'P' AS "Key", 'inventory item' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 'S' AS "Key", 'service' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 'N' AS "Key", 'non-inventory item' AS "Value",'' AS vcAbbreviations;
		ELSEIF v_strDBColumnName = 'tintRuleType' THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 1 AS "Key", 'Deduct from List price' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 2 AS "Key", 'Add to Primary Vendor Cost' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 3 AS "Key", 'Named Price' AS "Value",'' AS vcAbbreviations;
		ELSEIF v_strDBColumnName = 'tintDiscountType' THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			 SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 1 AS "Key", 'Percentage' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 2 AS "Key", 'Flat Amount' AS "Value",'' AS vcAbbreviations
				UNION 				
				SELECT v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID, 3 AS "Key", 'Named Price' AS "Value",'' AS vcAbbreviations;
		ELSEIF v_strDBColumnName = 'numShippingService' THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID
				,ShippingService.numShippingServiceID AS "Key"
				,ShippingService.vcShipmentService AS "Value"
				,coalesce(ShippingServiceAbbreviations.vcAbbreviation,'') vcAbbreviations
			FROM 
				ShippingService 
			LEFT JOIN
				ShippingServiceAbbreviations
			ON
				ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
			WHERE 
				ShippingService.numDomainID= v_numDomainID OR coalesce(ShippingService.numDomainID,0) = 0;
		ELSEIF v_strDBColumnName = 'numChartAcntId' THEN
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID
				,numAccountId AS "Key"
				,vcAccountName AS "Value"
				,''
			FROM 
				Chart_Of_Accounts 
			WHERE 
				numDomainId=v_numDomainID;									
		ELSE
			INSERT INTO tt_TEMPFIELDSDATA
			(
				vcFieldID
				,"Key"
				,"Value"
				,vcAbbreviations
			)
			SELECT 
				v_numFormFieldIDValue || '_' || coalesce(v_strDBColumnName,'') AS vcFieldID
				,LIST.numListItemID AS "Key"
				,vcData AS "Value"
				,''
			FROM 
				ListDetails LIST 
			LEFT JOIN 
				listorder LIST_ORDER 
			ON 
				LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = v_numDomainID
			WHERE 
				(LIST.numDomainID = v_numDomainID OR LIST.constFlag = true) 
				AND LIST.numListID = v_numListIDValue 
				AND (LIST.numDomainID = v_numDomainID  OR constFlag = true)  
			ORDER BY 
				intSortOrder;
		END IF;
	END LOOP;
	
	OPEN SWV_RefCur2 FOR SELECT vcFieldID AS "vcFieldID"
				,"Key"
				,"Value"
				,vcAbbreviations AS "vcAbbreviations" FROM tt_TEMPFIELDSDATA;
   
	
	RETURN;
END; $$;
