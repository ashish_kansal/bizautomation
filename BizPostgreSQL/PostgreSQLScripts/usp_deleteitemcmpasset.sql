-- Stored procedure definition script usp_DeleteItemCmpAsset for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteItemCmpAsset(v_numDivId NUMERIC(9,0),
v_numAItemCode NUMERIC(9,0),
v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM CompanyAssets where numAitemCode = v_numAItemCode and numDomainId = v_numDomainId and numDivID = v_numDivId;
   RETURN;
END; $$;


