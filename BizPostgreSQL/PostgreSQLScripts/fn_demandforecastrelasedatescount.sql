-- Function definition script fn_DemandForecastRelaseDatesCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_DemandForecastRelaseDatesCount(v_numDomainID NUMERIC(18,0),
    v_dtDate DATE,
	v_numDFID NUMERIC(18,0))
RETURNS TABLE
(
   numItemCode NUMERIC(18,0),
   numWarehouseID NUMERIC(18,0),
   numWarehouseItemID NUMERIC(18,0),
   numQty INTEGER,
   numQtySalesOppProgress INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitWarehouseFilter  BOOLEAN DEFAULT false;
   v_bitItemClassificationFilter  BOOLEAN DEFAULT false;
   v_bitItemGroupFilter  BOOLEAN DEFAULT false;
BEGIN
   DROP TABLE IF EXISTS tt_FN_DEMANDFORECASTRELASEDATESCOUNT_TEMPRELEASEDATES;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_DEMANDFORECASTRELASEDATESCOUNT_TEMPRELEASEDATES
   (
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numQty INTEGER,
      numQtySalesOppProgress INTEGER
   );
   IF(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) > 0 then
	
      v_bitWarehouseFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) > 0 then
	
      v_bitItemClassificationFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) > 0 then
	
      v_bitItemGroupFilter := true;
   end if;

	


   DROP TABLE IF EXISTS tt_TEMPORDER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPORDER
   (
      numOppId NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numQty DOUBLE PRECISION,
      numQtySalesOppProgress DOUBLE PRECISION,
      bitKitParent BOOLEAN,
      bitAssembly BOOLEAN
   );

	-- Based on order/opp release date
   INSERT INTO
   tt_TEMPORDER
   SELECT
   OpportunityMaster.numOppId
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WI.numWareHouseID
		,(CASE
   WHEN coalesce(I.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WI.numWareHouseItemID
      END)
   ELSE(SELECT WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = WI.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END) AS numWarehouseItemID
		,OpportunityItems.numUnitHour -coalesce(TEMPItemRelease.numQty,0) AS numQty
		,0 AS numQtySalesOppProgress
		,I.bitKitParent
		,I.bitAssembly
   FROM
   OpportunityMaster
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item I
   ON
   OpportunityItems.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN LATERAL(SELECT
      SUM(OIRD.numQty) AS numQty
      FROM
      OpportunityItemsReleaseDates OIRD
      WHERE
      OIRD.numOppId = OpportunityMaster.numOppId
      AND OIRD.numOppItemID = OpportunityItems.numoppitemtCode
      AND ((OIRD.dtReleaseDate <= v_dtDate AND coalesce(OIRD.tintStatus,0) = 1) OR coalesce(OIRD.tintStatus,0) = 2)) TEMPItemRelease on TRUE
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(tintopptype,0) = 1
   AND OpportunityMaster.dtReleaseDate IS NOT NULL AND CAST(OpportunityMaster.dtReleaseDate AS DATE) <= v_dtDate
   AND coalesce(numReleaseStatus,1) = 1
   AND
		((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WI.numWareHouseID IN(SELECT DemandForecastWarehouse.numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) = 0 OR
   I.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) = 0 OR
   I.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

	-- Based on item release date
   INSERT INTO
   tt_TEMPORDER
   SELECT
   OI.numOppId
		,OI.numoppitemtCode
		,OI.numItemCode
		,WI.numWareHouseID
		,(CASE
   WHEN coalesce(I.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT  WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WI.numWareHouseItemID
      END)
   ELSE(SELECT WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = WI.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END) AS numWarehouseItemID
		,OI.numUnitHour
		,0
		,I.bitKitParent
		,I.bitAssembly
   FROM
   OpportunityItemsReleaseDates OIRD
   INNER JOIN
   OpportunityItems OI
   ON
   OIRD.numOppId = OI.numOppId
   AND OIRD.numOppItemID = OI.numoppitemtCode
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   WHERE
   coalesce(tintStatus,0) = 1
   AND dtReleaseDate <= v_dtDate
   AND
		((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WI.numWareHouseID IN(SELECT DemandForecastWarehouse.numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) = 0 OR
   I.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) = 0 OR
   I.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));


	-- Based on sales opp total progress
   INSERT INTO
   tt_TEMPORDER
   SELECT
   OI.numOppId
		,OI.numoppitemtCode
		,OI.numItemCode
		,WI.numWareHouseID
		,(CASE
   WHEN coalesce(I.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WI.numWareHouseItemID
      END)
   ELSE(SELECT WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = WI.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END) AS numWarehouseItemID
		,0
		,CEIL(CAST(OI.numUnitHour -coalesce(TEMPItemRelease.numQty,0) AS DOUBLE PRECISION)*(PP.intTotalProgress/100)) AS numQty
		,I.bitKitParent
		,I.bitAssembly
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN LATERAL(SELECT
      SUM(OIRD.numQty) AS numQty
      FROM
      OpportunityItemsReleaseDates OIRD
      WHERE
      OIRD.numOppId = OM.numOppId
      AND OIRD.numOppItemID = OI.numoppitemtCode
      AND coalesce(OIRD.tintStatus,0) = 2) TEMPItemRelease ON TRUE
   LEFT JOIN
   ProjectProgress PP
   ON
   OM.numOppId = PP.numOppId
   WHERE
   tintopptype = 1
   AND coalesce(tintoppstatus,0) = 0
   AND intPEstimatedCloseDate IS NOT NULL AND CAST(OM.intPEstimatedCloseDate AS DATE) <= v_dtDate
   AND coalesce(OM.numReleaseStatus,1) = 1
   AND coalesce(PP.intTotalProgress,0) > 0
   AND
		(v_bitWarehouseFilter = false OR
   WI.numWareHouseID IN(SELECT DemandForecastWarehouse.numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		(v_bitItemClassificationFilter = false OR
   I.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		(v_bitItemGroupFilter = false OR
   I.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));
	

	-- GET KIT ITEMS CHILD ITEMS
   INSERT INTO
   tt_TEMPORDER   with recursive CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,bitKitParent,numItemGroup,
   numWarehouseID,numWarehouseItemID,numQty,numQtySalesOppProgress) AS(SELECT
   numOppChildItemID AS numOppChildItemID
			,t1.numOppId AS numOppID
			,t1.numOppItemID AS numOppItemID
			,numChildItemID AS numItemCode
			,I.bitKitParent AS bitKitParent
			,I.numItemGroup AS numItemGroup
			,WI.numWareHouseID AS numWarehouseID
			,WI.numWareHouseItemID AS numWarehouseItemID
			,t1.numQty*numQtyItemsReq_Orig AS numQty
			,t1.numQtySalesOppProgress*numQtyItemsReq_Orig AS numQtySalesOppProgress
   FROM
   tt_TEMPORDER AS t1
   INNER JOIN
   OpportunityKitItems OKI
   ON
   t1.numOppId = OKI.numOppId
   AND t1.numOppItemID = OKI.numOppItemID
   INNER JOIN
   Item I
   ON
   OKI.numChildItemID = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKI.numWareHouseItemId = WI.numWareHouseItemID
   UNION ALL
   SELECT
   OKCI.numOppKitChildItemID AS numOppChildItemID
			,OKCI.numOppId AS numOppID
			,OKCI.numOppItemID AS numOppItemID
			,OKCI.numItemID AS numItemCode
			,I.bitKitParent AS bitKitParent
			,I.numItemGroup AS numItemGroup
			,WI.numWareHouseID AS numWarehouseID
			,WI.numWareHouseItemID AS numWarehouseItemID
			,c.numQty*numQtyItemsReq_Orig AS numQty
			,c.numQtySalesOppProgress*OKCI.numQtyItemsReq_Orig AS numQtySalesOppProgress
   FROM
   OpportunityKitChildItems OKCI
   INNER JOIN
   CTE c
   ON
   OKCI.numOppChildItemID = c.numOppChildItemID
   INNER JOIN
   Item I
   ON
   OKCI.numItemID = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKCI.numWareHouseItemId = WI.numWareHouseItemID) SELECT
   numOppID,
		numOppItemID,
		c.numItemCode,
		c.numWareHouseID,
		(CASE
   WHEN coalesce(c.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         c.numWareHouseItemID
      END)
   ELSE(SELECT WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = c.numItemCode AND WareHouseItems.numWareHouseID = c.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END),
		c.numQty,
		c.numQtySalesOppProgress,
		bitKitParent,
		CAST(0 AS BOOLEAN)
   FROM
   CTE c
   WHERE
   coalesce(bitKitParent,false) = false;

   INSERT INTO
   tt_FN_DEMANDFORECASTRELASEDATESCOUNT_TEMPRELEASEDATES
   SELECT
   T1.numItemCode
		,T1.numWarehouseID
		,T1.numWarehouseItemID
		,SUM(T1.numQty)
		,SUM(T1.numQtySalesOppProgress)
   FROM
   tt_TEMPORDER T1
   WHERE
   coalesce(T1.bitKitParent,false) = false
   AND coalesce(T1.numWarehouseItemID,0) > 0
   GROUP BY
   T1.numItemCode,T1.numWarehouseID,T1.numWarehouseItemID;

	RETURN QUERY (SELECT * FROM tt_FN_DEMANDFORECASTRELASEDATESCOUNT_TEMPRELEASEDATES);
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_GetAttributes]    Script Date: 07/26/2008 18:12:28 ******/

