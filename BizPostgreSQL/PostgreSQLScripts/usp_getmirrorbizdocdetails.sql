DROP FUNCTION IF EXISTS USP_GetMirrorBizDocDetails;

CREATE OR REPLACE FUNCTION USP_GetMirrorBizDocDetails(v_numReferenceID NUMERIC(9,0) DEFAULT 0,
      v_numReferenceType SMALLINT DEFAULT NULL,
      v_numDomainId NUMERIC(9,0) DEFAULT NULL,
      v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null, INOUT SWV_RefCur4 refcursor default null, INOUT SWV_RefCur5 refcursor default null, INOUT SWV_RefCur6 refcursor default null, INOUT SWV_RefCur7 refcursor default null)
LANGUAGE plpgsql
--        DECLARE @numBizDocID AS NUMERIC
   AS $$
   DECLARE
   v_numBizDocType  NUMERIC;
   v_numBizDocTempID  NUMERIC(9,0);
   v_numDivisionID  NUMERIC(9,0);
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   SWV_RCur3 REFCURSOR;
   SWV_RCur4 REFCURSOR;
   SWV_RCur5 REFCURSOR;
   SWV_RCur6 REFCURSOR;
BEGIN
   IF v_numReferenceType = 1 then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Order' AND constFlag = true;
   ELSEIF v_numReferenceType = 2
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Order' AND constFlag = true;
   ELSEIF v_numReferenceType = 3
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Opportunity' AND constFlag = true;
   ELSEIF v_numReferenceType = 4
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Opportunity' AND constFlag = true;
   ELSEIF v_numReferenceType = 5
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'RMA' AND constFlag = true;
   ELSEIF v_numReferenceType = 6
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'RMA' AND constFlag = true;
   ELSEIF v_numReferenceType = 7
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Sales Credit Memo' AND constFlag = true;
   ELSEIF v_numReferenceType = 8
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Purchase Credit Memo' AND constFlag = true;
   ELSEIF v_numReferenceType = 9
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Refund Receipt' AND constFlag = true;
   ELSEIF v_numReferenceType = 10
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Credit Memo' AND constFlag = true;
   ELSEIF v_numReferenceType = 11
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Invoice' AND constFlag = true;
   end if;
	 
   IF v_numReferenceType = 1 OR v_numReferenceType = 2 OR v_numReferenceType = 3 OR v_numReferenceType = 4 then
		
		      
			---------------------------------------------------------------------------------------
      
      open SWV_RefCur for
      SELECT Mst.vcpOppName AS vcBizDocID,
						 Mst.vcpOppName AS OppName,Mst.vcpOppName AS BizDcocName,v_numBizDocType AS BizDoc,coalesce(Mst.numrecowner,0) AS Owner,
						 CMP.vcCompanyName AS CompName,Mst.numContactId,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(coalesce(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numoppid = Mst.numOppId) AS monAmountPaid,
						 coalesce(Mst.txtComments,'') AS vcComments,
						 CAST(Mst.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS dtCreatedDate,
						 fn_GetContactName(Mst.numCreatedBy) AS numCreatedby,
						 fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 coalesce(Mst.bitBillingTerms,false) AS tintBillingTerms,
						 coalesce(Mst.intBillingDays,0) AS numBillingDays,
						 coalesce(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 coalesce(Mst.bitInterestType,false) AS tintInterestType,
						 coalesce(Mst.fltInterest,0) AS fltInterest,
						 tintopptype,
						 v_numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL AS bintShippedDate,
						 coalesce(Mst.monShipCost,0) AS monShipCost,
						 coalesce(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 coalesce(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
						 Mst.numDivisionId,
						  coalesce(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 coalesce(Mst.fltDiscount,0) AS fltDiscount,
						 coalesce(Mst.bitDiscountType,false) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE
      WHEN Mst.intUsedShippingCompany IS NULL
      THEN '-'
      WHEN Mst.intUsedShippingCompany = -1
      THEN 'Will-call'
      ELSE fn_GetListItemName(Mst.intUsedShippingCompany::NUMERIC)
      END AS ShipVia,
						 coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainId OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
						 0 AS ShippingReportCount,
						 false AS bitPartialFulfilment,
						 vcpOppName as vcBizDocName,
						 fn_GetContactName(Mst.numModifiedBy)  || ', '
      || CAST(Mst.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
						 fn_GetContactName(Mst.numrecowner) || ', ' ||  fn_GetContactName(Mst.numassignedto) AS OrderRecOwner,
						 fn_GetContactName(DM.numRecOwner) || ', ' ||  fn_GetContactName(DM.numAssignedTo) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 coalesce(BT.bitEnabled,false) AS bitEnabled,
						 coalesce(BT.txtBizDocTemplate,'') AS txtBizDocTemplate,
						 coalesce(BT.txtCSS,'') AS txtCSS,
						 fn_GetContactName(Mst.numassignedto) AS AssigneeName,
						 coalesce((SELECT vcEmail FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneeEmail,
						 coalesce((SELECT numPhone FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneePhone,
						 fn_GetComapnyName(Mst.numDivisionId) AS OrganizationName,
						 fn_GetContactName(Mst.numContactId)  AS OrgContactName,
						 case when ACI.numPhone <> '' then ACI.numPhone || case when ACI.numPhoneExtension <> '' then ' - ' || ACI.numPhoneExtension else '' end  else '' END AS OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 coalesce(ACI.vcEmail,'') AS OrgContactEmail,
 						 fn_GetContactName(Mst.numrecowner) AS OnlyOrderRecOwner,
 						 0 AS bitAuthoritativeBizDocs,
						0 AS tintDeferred,0 AS monCreditAmount,
						 false as bitRentalBizDoc,coalesce(BT.numBizDocTempID,0) as numBizDocTempID,coalesce(BT.vcTemplateName,'') as vcTemplateName,
						 GetDealAmount(Mst.numOppId,TIMEZONE('UTC',now()),0::NUMERIC) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 coalesce(CMP.txtComments,'') as vcOrganizationComments,
						 coalesce((SELECT SUBSTR((SELECT '$^$' || coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = Mst.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = OBD.numShipVia  AND vcFieldName = 'Tracking URL')),'') || '#^#' || RTRIM(LTRIM(vcTrackingNo))  FROM OpportunityBizDocs OBD WHERE OBD.numoppid = Mst.numOppId),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,coalesce(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,coalesce(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 coalesce(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,FormatedDateFromDate(Mst.dtReleaseDate,v_numDomainId) AS vcReleaseDate
						 ,FormatedDateFromDate(Mst.intPEstimatedCloseDate,v_numDomainId) AS vcRequiredDate
						 ,(CASE WHEN coalesce(Mst.numPartner,0) > 0 THEN coalesce((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 coalesce("vcCustomerPO#",'') AS "vcCustomerPO#",
						 coalesce(Mst.txtComments,'') AS vcSOComments,
						 coalesce(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CAST(Mst.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS OrderCreatedDate,
						 '' AS vcVendorInvoice
						 ,coalesce(CheckOrderInventoryStatus(Mst.numOppId,Mst.numDomainId,2::SMALLINT),'') AS vcInventoryStatus
						 ,CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
      WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
      WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
      WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
      ELSE '-'
      END AS vcDropShip
      FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainId = Mst.numDomainId
      LEFT OUTER JOIN Currency C ON C.numCurrencyID = Mst.numCurrencyID
      LEFT JOIN DivisionMaster DM ON DM.numDivisionID = Mst.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = Mst.numContactId
      LEFT JOIN BizDocTemplate BT ON BT.numDomainID = v_numDomainId AND BT.numBizDocID = v_numBizDocType
      AND ((BT.numBizDocTempID = coalesce(Mst.numOppBizDocTempID,0)) OR (coalesce(BT.bitDefault,false) = true AND coalesce(BT.bitEnabled,false) = true))
      LEFT JOIN BillingTerms BTR ON BTR.numTermsID = coalesce(Mst.intBillingDays,0)
      JOIN DivisionMaster div1 ON D.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo com1 ON com1.numCompanyId = div1.numCompanyID
      WHERE  Mst.numOppId = v_numReferenceID AND Mst.numDomainId = v_numDomainId;
      SELECT * INTO SWV_RefCur2,SWV_RefCur3,SWV_RefCur4,SWV_RefCur5,SWV_RefCur6,SWV_RefCur7 FROM USP_OPPGetOppAddressDetails(v_numReferenceID,v_numDomainId,0,SWV_RCur,SWV_RCur2,SWV_RCur3,SWV_RCur4,SWV_RCur5,SWV_RCur6);
   ELSEIF v_numReferenceType = 5 OR v_numReferenceType = 6 OR v_numReferenceType = 7 OR v_numReferenceType = 8
   OR v_numReferenceType = 9 OR v_numReferenceType = 10
   then
      select   numDivisionId, (CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 OR v_numReferenceType = 9 OR v_numReferenceType = 10 THEN numRMATempID ELSE numBizdocTempID END) INTO v_numDivisionID,v_numBizDocTempID FROM    ReturnHeader RH WHERE   RH.numReturnHeaderID = v_numReferenceID;
                                
    			---------------------
               
      open SWV_RefCur for
      SELECT  RH.vcBizDocName AS vcBizDocID,
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,v_numBizDocType AS BizDoc,coalesce(RH.numCreatedBy,0) AS Owner,
								   CMP.vcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        coalesce(RDA.monAmount+RDA.monTotalTax -RDA.monTotalDiscount,0) AS monAmountPaid,
                                        coalesce(RH.vcComments,'') AS vcComments,
                                        CAST(RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS dtCreatedDate,
                                        fn_GetContactName(RH.numCreatedBy) AS numCreatedby,
                                        fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS numBillingDaysName,
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        v_numBizDocType AS numBizDocId,
                                        fn_GetContactName(RH.numCreatedBy) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        coalesce(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        coalesce(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionId,
                                         coalesce(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        coalesce(RH.monTotalDiscount,0) AS fltDiscount,
                                        false AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '' AS ShipVia,
										'' AS vcShippingService,
										'' AS vcPackingSlip,
                                        coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        false AS bitPartialFulfilment,
                                        vcBizDocName,
                                        fn_GetContactName(RH.numModifiedBy)
      || ', '
      || CAST(RH.dtModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
                                        fn_GetContactName(RH.numCreatedBy) AS OrderRecOwner,
                                        fn_GetContactName(DM.numRecOwner)
      || ', '
      || fn_GetContactName(DM.numAssignedTo) AS AccountRecOwner,
                                        RH.dtCreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        coalesce(BT.bitEnabled,false) AS bitEnabled,
                                        coalesce(BT.txtBizDocTemplate,'') AS txtBizDocTemplate,
                                        coalesce(BT.txtCSS,'') AS txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        fn_GetComapnyName(RH.numDivisionId) AS OrganizationName,
                                        fn_GetContactName(RH.numContactID) AS OrgContactName,
                                        getCompanyAddress(RH.numDivisionId,1::SMALLINT,RH.numDomainID) AS CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
      THEN ACI.numPhone
         || CASE WHEN ACI.numPhoneExtension <> ''
         THEN ' - ' || ACI.numPhoneExtension
         ELSE ''
         END
      ELSE ''
      END AS OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        coalesce(ACI.vcEmail,'') AS OrgContactEmail,
                                        fn_GetContactName(RH.numCreatedBy) AS OnlyOrderRecOwner,
                                        1 AS bitAuthoritativeBizDocs,
                                        0 AS tintDeferred,
                                        0 AS monCreditAmount,
                                        false AS bitRentalBizDoc,
                                        coalesce(BT.numBizDocTempID,0) AS numBizDocTempID,
                                        coalesce(BT.vcTemplateName,'') AS vcTemplateName,
                                        0 AS monPAmount,
                                        coalesce(CMP.txtComments,'') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
										,'' AS vcReleaseDate
										,'' AS vcRequiredDate
										,'' AS vcPartner
										,'' AS "vcCustomerPO#"
										,'' AS vcSOComments
										,'' AS vcShippersAccountNo
										,'' AS OrderCreatedDate
										,'' AS vcVendorInvoice
										,'' AS vcInventoryStatus
										,CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
      WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
      WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
      WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
      ELSE '-'
      END AS vcDropShip
      FROM    ReturnHeader RH
      JOIN Domain D ON D.numDomainId = RH.numDomainID
      JOIN DivisionMaster DM ON DM.numDivisionID = RH.numDivisionId
      LEFT OUTER JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId
      AND CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = RH.numContactID
      LEFT JOIN BizDocTemplate BT ON BT.numDomainID = v_numDomainId
      AND BT.numBizDocID = v_numBizDocType
      AND ((BT.numBizDocTempID = coalesce(v_numBizDocTempID,0)) OR (coalesce(BT.bitDefault,false) = true AND coalesce(BT.bitEnabled,false) = true))
      JOIN DivisionMaster div1 ON D.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo com1 ON com1.numCompanyId = div1.numCompanyID,
                                                                         GetReturnDealAmount(v_numReferenceID,v_numReferenceType) RDA
      WHERE   RH.numReturnHeaderID = v_numReferenceID;
                                
                                	--************Customer/Vendor Billing Address************
      open SWV_RefCur2 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											coalesce(AD.vcStreet,'') AS vcStreet,
											coalesce(AD.vcCity,'') AS vcCity,
											coalesce(fn_GetState(AD.numState),'') AS vcState,
											coalesce(AD.vcPostalCode,'') AS vcPostalCode,
											coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											coalesce(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
      WHERE AD.numDomainID = v_numDomainId AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;

                                        
                                	--************Customer/Vendor Shipping Address************
      open SWV_RefCur3 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											coalesce(AD.vcStreet,'') AS vcStreet,
											coalesce(AD.vcCity,'') AS vcCity,
											coalesce(fn_GetState(AD.numState),'') AS vcState,
											coalesce(AD.vcPostalCode,'') AS vcPostalCode,
											coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											coalesce(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
      WHERE AD.numDomainID = v_numDomainId AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;

                                --************Employer Shipping Address************ 
      open SWV_RefCur4 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											coalesce(AD.vcStreet,'') AS vcStreet,
											coalesce(AD.vcCity,'') AS vcCity,
											coalesce(fn_GetState(AD.numState),'') AS vcState,
											coalesce(AD.vcPostalCode,'') AS vcPostalCode,
											coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE  D1.numDomainId = v_numDomainId
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;


								--************Employer Shipping Address************ 
      open SWV_RefCur5 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										coalesce(AD.vcStreet,'') AS vcStreet,
										coalesce(AD.vcCity,'') AS vcCity,
										coalesce(fn_GetState(AD.numState),'') AS vcState,
										coalesce(AD.vcPostalCode,'') AS vcPostalCode,
										coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE AD.numDomainID = v_numDomainId
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;

								 --************Customer/Vendor Billing Address************
      open SWV_RefCur6 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											coalesce(AD.vcStreet,'') AS vcStreet,
											coalesce(AD.vcCity,'') AS vcCity,
											coalesce(fn_GetState(AD.numState),'') AS vcState,
											coalesce(AD.vcPostalCode,'') AS vcPostalCode,
											coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											coalesce(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
      WHERE AD.numDomainID = v_numDomainId AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;

                                        
                                	--************Customer/Vendor Shipping Address************
      open SWV_RefCur7 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											coalesce(AD.vcStreet,'') AS vcStreet,
											coalesce(AD.vcCity,'') AS vcCity,
											coalesce(fn_GetState(AD.numState),'') AS vcState,
											coalesce(AD.vcPostalCode,'') AS vcPostalCode,
											coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											coalesce(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
      WHERE AD.numDomainID = v_numDomainId AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;
   ELSEIF v_numReferenceType = 11
   then
		
		      
			---------------------------------------------------------------------------------------
      
      open SWV_RefCur for
      SELECT Mst.vcpOppName AS vcBizDocID,
						 Mst.vcpOppName AS OppName,OBD.vcBizDocID AS BizDcocName,v_numBizDocType AS BizDoc,coalesce(Mst.numrecowner,0) AS Owner,
						 CMP.vcCompanyName AS CompName,Mst.numContactId,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(coalesce(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numoppid = Mst.numOppId) AS monAmountPaid,
						 coalesce(Mst.txtComments,'') AS vcComments,
						 CAST(Mst.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS dtCreatedDate,
						 fn_GetContactName(Mst.numCreatedBy) AS numCreatedby,
						 fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 coalesce(Mst.bitBillingTerms,false) AS tintBillingTerms,
						 coalesce(Mst.intBillingDays,0) AS numBillingDays,
						 coalesce(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 coalesce(Mst.bitInterestType,false) AS tintInterestType,
						 coalesce(Mst.fltInterest,0) AS fltInterest,
						 tintopptype,
						 v_numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL AS bintShippedDate,
						 coalesce(Mst.monShipCost,0) AS monShipCost,
						 coalesce(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 coalesce(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
						 Mst.numDivisionId,
						  coalesce(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 coalesce(Mst.fltDiscount,0) AS fltDiscount,
						 coalesce(Mst.bitDiscountType,false) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE
      WHEN coalesce(OBD.numShipVia,0) = 0 THEN(CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE fn_GetListItemName(Mst.intUsedShippingCompany::NUMERIC) END)
      WHEN OBD.numShipVia = -1 THEN 'Will-call'
      ELSE fn_GetListItemName(OBD.numShipVia)
      END AS ShipVia,
						 coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainId OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
						 0 AS ShippingReportCount,
						 false AS bitPartialFulfilment,
						 vcpOppName as vcBizDocName,
						 fn_GetContactName(Mst.numModifiedBy)  || ', '
      || CAST(Mst.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
						 fn_GetContactName(Mst.numrecowner) || ', ' ||  fn_GetContactName(Mst.numassignedto) AS OrderRecOwner,
						 fn_GetContactName(DM.numRecOwner) || ', ' ||  fn_GetContactName(DM.numAssignedTo) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 coalesce(BT.bitEnabled,false) AS bitEnabled,
						 coalesce(BT.txtBizDocTemplate,'') AS txtBizDocTemplate,
						 coalesce(BT.txtCSS,'') AS txtCSS,
						 fn_GetContactName(Mst.numassignedto) AS AssigneeName,
						 coalesce((SELECT vcEmail FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneeEmail,
						 coalesce((SELECT numPhone FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneePhone,
						 fn_GetComapnyName(Mst.numDivisionId) AS OrganizationName,
						 fn_GetContactName(Mst.numContactId)  AS OrgContactName,
						 case when ACI.numPhone <> '' then ACI.numPhone || case when ACI.numPhoneExtension <> '' then ' - ' || ACI.numPhoneExtension else '' end  else '' END AS OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 coalesce(ACI.vcEmail,'') AS OrgContactEmail,
 						 fn_GetContactName(Mst.numrecowner) AS OnlyOrderRecOwner,
 						 0 AS bitAuthoritativeBizDocs,
						0 AS tintDeferred,0 AS monCreditAmount,
						 false as bitRentalBizDoc,coalesce(BT.numBizDocTempID,0) as numBizDocTempID,coalesce(BT.vcTemplateName,'') as vcTemplateName,
						 GetDealAmount(Mst.numOppId,TIMEZONE('UTC',now()),0::NUMERIC) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 coalesce(CMP.txtComments,'') as vcOrganizationComments,
						 coalesce((SELECT SUBSTR((SELECT '$^$' || coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = Mst.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = OBD.numShipVia  AND vcFieldName = 'Tracking URL')),'') || '#^#' || RTRIM(LTRIM(vcTrackingNo))  FROM OpportunityBizDocs OBD WHERE OBD.numoppid = Mst.numOppId),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,coalesce(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,coalesce(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 coalesce(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
						 ,FormatedDateFromDate(Mst.dtReleaseDate,v_numDomainId) AS vcReleaseDate
						 ,FormatedDateFromDate(Mst.intPEstimatedCloseDate,v_numDomainId) AS vcRequiredDate
						 ,(CASE WHEN coalesce(Mst.numPartner,0) > 0 THEN coalesce((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 coalesce(Mst."vcCustomerPO#",'') AS "vcCustomerPO#",
						 coalesce(Mst.txtComments,'') AS vcSOComments,
						 coalesce(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CAST(Mst.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS OrderCreatedDate,
						 (CASE WHEN coalesce(OBD.numSourceBizDocId,0) > 0 THEN coalesce((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = OBD.numSourceBizDocId),'') ELSE coalesce(vcVendorInvoice,'') END) AS vcVendorInvoice,
						 (CASE
      WHEN coalesce(OBD.numSourceBizDocId,0) > 0
      THEN coalesce((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = OBD.numSourceBizDocId),'')
      ELSE ''
      END) AS vcPackingSlip
						,coalesce(CheckOrderInventoryStatus(Mst.numOppId,Mst.numDomainId,2::SMALLINT),'') AS vcInventoryStatus,
			CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
      WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
      WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
      WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
      ELSE '-'
      END AS vcDropShip
      FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainId = Mst.numDomainId
      LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppId = OBD.numoppid
      LEFT OUTER JOIN Currency C ON C.numCurrencyID = Mst.numCurrencyID
      LEFT JOIN DivisionMaster DM ON DM.numDivisionID = Mst.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = Mst.numContactId
      LEFT JOIN BizDocTemplate BT ON BT.numDomainID = v_numDomainId AND BT.numBizDocID = v_numBizDocType
      AND ((BT.numBizDocTempID = coalesce(Mst.numOppBizDocTempID,0)) OR (coalesce(BT.bitDefault,false) = true AND coalesce(BT.bitEnabled,false) = true))
      LEFT JOIN BillingTerms BTR ON BTR.numTermsID = coalesce(Mst.intBillingDays,0)
      JOIN DivisionMaster div1 ON D.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo com1 ON com1.numCompanyId = div1.numCompanyID
      WHERE  OBD.numOppBizDocsId = v_numReferenceID AND  Mst.numDomainId = v_numDomainId;
      v_numReferenceType := 1;
      SELECT  numoppid INTO v_numReferenceID FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numReferenceID     LIMIT 1;
      SELECT * INTO SWV_RefCur2,SWV_RefCur3,SWV_RefCur4,SWV_RefCur5,SWV_RefCur6,SWV_RefCur7 FROM USP_OPPGetOppAddressDetails(v_numReferenceID,v_numDomainId,0,SWV_RCur,SWV_RCur2,SWV_RCur3,SWV_RCur4,SWV_RCur5,SWV_RCur6);
   end if;
   RETURN;
END; $$;



