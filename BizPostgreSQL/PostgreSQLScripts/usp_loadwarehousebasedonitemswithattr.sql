-- Stored procedure definition script USP_LoadWarehouseBasedOnItemsWithAttr for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LoadWarehouseBasedOnItemsWithAttr(v_numItemCode NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitSerialize  BOOLEAN;  
   v_bitKitParent  BOOLEAN;
BEGIN
   select   bitSerialized, (CASE WHEN coalesce(bitKitParent,false) = true
   AND coalesce(bitAssembly,false) = true THEN false
   WHEN coalesce(bitKitParent,false) = true THEN true
   ELSE false
   END) INTO v_bitSerialize,v_bitKitParent from Item where numItemCode = v_numItemCode;   
   
	DROP TABLE IF EXISTS tt_TEMP CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP 
	( 
		numItemKitID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numQtyItemsReq DOUBLE PRECISION
		,numCalculatedQty DOUBLE PRECISION
	);

	IF coalesce(v_bitKitParent,false) = true THEN
		WITH RECURSIVE CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS
		(
			SELECT 
				CAST(0 AS NUMERIC),
				numItemCode,
				DTL.numQtyItemsReq,
				CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty                                
			FROM 
				Item
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=v_numItemCode
			UNION ALL
			SELECT 
				Dtl.numItemKitID,
				i.numItemCode,
				DTL.numQtyItemsReq,
				CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
			FROM 
				Item i                               
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				Dtl.numChildItemID=i.numItemCode
			INNER JOIN 
				CTE c 
			ON 
				Dtl.numItemKitID = c.numItemCode 
			WHERE 
				Dtl.numChildItemID <> v_numItemCode
		)

		INSERT INTO tt_TEMP SELECT numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty FROM CTE;
	END IF;

   open SWV_RefCur for select distinct cast((WareHouseItems.numWareHouseItemID) as VARCHAR(255)),vcWareHouse || ' --- ' || USP_GetAttributes(numWareHouseItemId,v_bitSerialize) as vcWareHouse,
  Case when v_bitKitParent = true then COALESCE((SELECT
		CAST(MIN(FLOOR(numOnHand)) AS DOUBLE PRECISION) 
	FROM
	(
		SELECT 
			(CASE WHEN COALESCE(SUM(numOnHand),0)=0 THEN 0 WHEN COALESCE(SUM(numOnHand),0)>=numQtyItemsReq AND numQtyItemsReq>0 THEN COALESCE(SUM(numOnHand),0)/numQtyItemsReq ELSE 0 end) AS numOnHand
		FROM 
			tt_TEMP c  
		LEFT JOIN
		(
			SELECT 
				numItemID
				,numWareHouseID
				,COALESCE(numOnHand,0) numOnHand
			FROM
				WareHouseItems
		) WI
		ON
			WI.numItemID = c.numItemCode
			AND WI.numWareHouseID = WareHouseItems.numWareHouseID 
		GROUP BY
			c.numItemCode
			,c.numQtyItemsReq
	) TEMP),0) else coalesce(numOnHand,0) END AS numOnHand,
Case when v_bitKitParent = true then 0 else coalesce(numOnOrder,0) END  AS numOnOrder,
Case when v_bitKitParent = true then 0 else coalesce(numReorder,0) END  AS numReorder,
Case when v_bitKitParent = true then 0 else coalesce(numAllocation,0) END  AS numAllocation,
Case when v_bitKitParent = true then 0 else coalesce(numBackOrder,0) END  AS numBackOrder
   from WareHouseItems
   join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   where numItemID = v_numItemCode;

END; $$;












