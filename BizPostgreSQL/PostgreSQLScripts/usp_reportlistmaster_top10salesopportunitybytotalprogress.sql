-- Stored procedure definition script USP_ReportListMaster_Top10SalesOpportunityByTotalProgress for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10SalesOpportunityByTotalProgress(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   OM.vcpOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,coalesce(monTotAmount,0) AS monTotAmount
		,cast(coalesce(OM.numPercentageComplete,0) as NUMERIC(9,0)) AS numPercentageComplete
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 50
   AND (coalesce(LD.constFlag,false) = true OR LD.numDomainid = v_numDomainID)
   AND LD.numListItemID = OM.numPercentageComplete
   WHERE
   OM.numDomainId = v_numDomainID
   AND coalesce(OM.tintopptype,0) = 1
   AND coalesce(OM.tintoppstatus,0) = 0
   ORDER BY
   coalesce(OM.numPercentageComplete,0) DESC LIMIT 10;
END; $$;












