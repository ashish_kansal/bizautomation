-- Stored procedure definition script USP_DeleteBreadCrumb for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteBreadCrumb(v_numBreadCrumbID NUMERIC(9,0),
	v_numSiteID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM SiteBreadCrumb
   WHERE numBreadCrumbID = v_numBreadCrumbID AND numSiteID = v_numSiteID;
   RETURN;
END; $$;


