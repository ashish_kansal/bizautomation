-- Function definition script GetItemInventoryAssetAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemInventoryAssetAccount(v_numItemCode NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAssetChartAcntId  NUMERIC(9,0);
BEGIN
   select   coalesce(numAssetChartAcntId,'0') INTO v_numAssetChartAcntId from Item where numItemCode = v_numItemCode;       
    
    
   return coalesce(v_numAssetChartAcntId,cast('0' as NUMERIC));
END; $$;

