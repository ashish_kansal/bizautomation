CREATE OR REPLACE FUNCTION USP_ActItemAlert(v_numDomainID NUMERIC(9,0) DEFAULT null,                    
v_numUserCntID NUMERIC(9,0) DEFAULT null,              
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor DEFAULT null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_GetDate  TIMESTAMP;
BEGIN
   v_GetDate := TIMEZONE('UTC',now());                  
                  
   open SWV_RefCur for SELECT  com.numCommId AS Id,
  AddC.numContactId,
  cast(Comp.numCompanyId as VARCHAR(255)),
  Div.numDivisionID
   FROM  Communication com
   JOIN  AdditionalContactsInformation AddC
   ON com.numContactId = AddC.numContactId
   JOIN  DivisionMaster Div
   ON AddC.numDivisionId = Div.numDivisionID
   JOIN   CompanyInfo Comp
   ON Div.numCompanyID = Comp.numCompanyId
   where com.bitClosedFlag = false
   and intSnoozeMins <> 0
   and com.numAssign = v_numUserCntID
   and tintSnoozeStatus = 1 and dtEndTime+CAST(intSnoozeMins || 'minute' as interval)  <= v_GetDate
   union
   SELECT  com.numCommId AS Id,
  AddC.numContactId,
  cast(Comp.numCompanyId as VARCHAR(255)),
  Div.numDivisionID
   FROM  Communication com
   JOIN  AdditionalContactsInformation AddC
   ON com.numContactId = AddC.numContactId
   JOIN  DivisionMaster Div
   ON AddC.numDivisionId = Div.numDivisionID
   JOIN   CompanyInfo Comp
   ON Div.numCompanyID = Comp.numCompanyId
   where com.bitClosedFlag = false
   and intRemainderMins <> 0
   and com.numAssign = v_numUserCntID
   and tintRemStatus = 1 and dtStartTime+CAST(-intRemainderMins || 'minute' as interval)  <= v_GetDate;
END; $$;






