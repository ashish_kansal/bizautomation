-- Stored procedure definition script usp_GetCampaignForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCampaignForReport(v_numDomainId NUMERIC,  
v_numUserCntID NUMERIC,  
v_tintType SMALLINT,  
v_tintRights SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;  
  
  
--if @tintRights=1  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy=@numUserCntID  
--  
--  
--if @tintRights=2  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy in(select distinct(numUserCntID) from userTeams where numTeam in(select F.numTeam from ForReportsByTeam F           
--where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType))  
--  
--  
--if @tintRights=3  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy in(select distinct(numUserCntID) from userterritory where numTerritoryID in(select F.numTerritory from ForReportsByTerritory F           
--     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType))



