-- Stored procedure definition script USP_GetOpenOrdersAddBill for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpenOrdersAddBill(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numOppID,0) > 0 then
	
      open SWV_RefCur for
      SELECT
      numOppId
			,vcpOppName
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND numOppId = v_numOppID;
   ELSE
      open SWV_RefCur for
      SELECT
      numOppId
			,vcpOppName
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND numDivisionId = v_numDivisionID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND coalesce(tintshipped,0) = 0
      AND EXISTS(SELECT OI.numoppitemtCode FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE OI.numOppId = OpportunityMaster.numOppId AND coalesce(I.bitExpenseItem,false) = true)
      ORDER BY
      numOppId;
   end if;
   RETURN;
END; $$;


