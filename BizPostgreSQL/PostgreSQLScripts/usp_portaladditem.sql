-- Stored procedure definition script USP_PortalAddItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PortalAddItem(v_numDivID NUMERIC(9,0),                  
INOUT v_numOppID NUMERIC(9,0) DEFAULT null  ,                  
v_numItemCode NUMERIC(9,0) DEFAULT NULL,                  
INOUT v_vcPOppName VARCHAR(200) DEFAULT '' ,                  
v_numContactId NUMERIC(9,0) DEFAULT NULL,                  
v_numDomainId NUMERIC(9,0) DEFAULT NULL,                  
v_numUnits NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CRMType  INTEGER;     
   v_numRecOwner  INTEGER;                  
   v_monPrice  DECIMAL(20,5);             
   v_TotalmonPrice  DECIMAL(20,5);
   v_TotalAmount  INTEGER;                     
   v_intOppTcode  NUMERIC(9,0);
BEGIN
   if v_numOppID =  0 then
      select   tintCRMType, numRecOwner INTO v_CRMType,v_numRecOwner from DivisionMaster where numDivisionID = v_numDivID;
      if v_CRMType = 0 then

         UPDATE DivisionMaster
         SET tintCRMType = 1
         WHERE numDivisionID = v_numDivID;
      end if;
      select   max(numOppId) INTO v_intOppTcode from OpportunityMaster;
      v_intOppTcode := coalesce(v_intOppTcode,0)+1;
      v_vcPOppName :=(select  vcCompanyName from CompanyInfo Com
      join DivisionMaster Div
      on Div.numCompanyID = Com.numCompanyId
      where Div.numDivisionID = v_numDivID) || '-' || coalesce(v_vcPOppName,'');
      v_vcPOppName := coalesce(v_vcPOppName,'') || '-' || CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS VARCHAR(4))  || '-A' || SUBSTR(CAST(v_intOppTcode AS VARCHAR(10)),1,10);
      insert into OpportunityMaster(numContactId,
  numDivisionId,
  txtComments,
  numCampainID,
  bitPublicFlag,
  tintSource,
  vcpOppName,
  monPAmount,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numDomainId,
  tintopptype,
  intPEstimatedCloseDate,
  numrecowner,
  bitOrder)
 Values(v_numContactId,
  v_numDivID,
  '',
  0,
  false,
  0,
  v_vcPOppName,
  0,
  v_numContactId,
  TIMEZONE('UTC',now()),
  v_numContactId,
  TIMEZONE('UTC',now()),
  v_numDomainId,
  3,
  TIMEZONE('UTC',now()),
  v_numRecOwner,
  true);

      v_numOppID := CURRVAL('OpportunityMaster_seq');
   end if;                   
                  
   v_monPrice := CAST(GetPriceBasedonOrgDisc(v_numItemCode,1,v_numDivID) AS DECIMAL(20,5));   
   v_TotalmonPrice := v_monPrice*v_numUnits;                       
                 

   insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount)
values(v_numOppID,v_numItemCode,v_numUnits,coalesce(v_monPrice, 0),coalesce(v_TotalmonPrice, 0));                  
                   
                  

   select   sum(monTotAmount) INTO v_TotalAmount from OpportunityItems where numOppId = v_numOppID;                  
   update OpportunityMaster set  monPAmount = v_TotalAmount
   where numOppId = v_numOppID;                 
                
   select  vcpOppName INTO v_vcPOppName from OpportunityMaster where numOppId = v_numOppID;
   RETURN;
END; $$;


