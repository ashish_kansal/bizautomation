-- Stored procedure definition script USP_SaveOperationBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveOperationBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                     
v_numBudgetId NUMERIC(9,0) DEFAULT 0,                    
v_strRow TEXT DEFAULT '',              
v_numDepartmentId NUMERIC(9,0) DEFAULT 0,                   
v_numDivisionId NUMERIC(9,0) DEFAULT 0,                                
v_numCostCenterId NUMERIC(9,0) DEFAULT 0,              
v_intFiscalYear NUMERIC(9,0) DEFAULT 0,           
v_Id NUMERIC(9,0) DEFAULT 0,         
v_tinyByteMode SMALLINT DEFAULT 0,  
v_intType INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Date  TIMESTAMP;                            
   v_dtFiscalStDate  TIMESTAMP;                                    
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN                  
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);         
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);                                 
   v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';                           
   RAISE NOTICE '%',v_dtFiscalStDate;                 
   RAISE NOTICE '%',v_dtFiscalEndDate;              
--For Deleting Record                 
   If v_tinyByteMode = 0 then
 
      DELETE FROM OperationBudgetDetails OBD using OperationBudgetMaster OBM where OBD.numBudgetID = OBM.numBudgetId AND OBD.numBudgetID = v_numBudgetId And OBM.numDivisionId = v_numDivisionId And OBM.numDepartmentId = v_numDepartmentId And OBM.numCostCenterId = v_numCostCenterId      
      And ((OBD.tintMonth between EXTRACT(Month FROM v_dtFiscalStDate) and 12 and OBD.intYear = EXTRACT(Year FROM v_dtFiscalStDate)) Or (OBD.tintMonth between 1 and EXTRACT(month FROM v_dtFiscalEndDate)  and OBD.intYear = EXTRACT(Year FROM v_dtFiscalEndDate)));
   Else
      DELETE FROM OperationBudgetDetails OBD using OperationBudgetMaster OBM where OBM.numBudgetId = OBD.numBudgetID AND OBD.numBudgetID = v_numBudgetId And OBD.numChartAcntId = v_Id  And OBM.numDomainId = v_numDomainId
      And ((OBD.tintMonth between EXTRACT(month FROM v_Date) and 12 and OBD.intYear = EXTRACT(year FROM v_Date)) Or (OBD.tintMonth between 1 and 12 and OBD.intYear = EXTRACT(Year FROM v_Date)+1));
   end if;             
                                          
	Insert Into OperationBudgetDetails
	(
		numBudgetID,numChartAcntId,numParentAcntId,txtChartAcntname,tintMonth,intYear,monAmount,vcComments
	)
	Select 
		numBudgetId,numChartAcntId,numParentAcntId,vcCategoryName,tintMonth,intYear,monAmount,Comments
	from
	XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_strRow AS XML)
		COLUMNS
			id FOR ORDINALITY,
			BudgetDetId NUMERIC(18,0) PATH 'BudgetDetId',
			numBudgetId NUMERIC(9,0) PATH 'numBudgetId',
			numChartAcntId NUMERIC(9,0) PATH 'numChartAcntId',
			numParentAcntId NUMERIC(9,0) PATH 'numParentAcntId',
			vcCategoryName TEXT PATH 'vcCategoryName',
			tintMonth SMALLINT PATH 'tintMonth',
			intYear INTEGER PATH 'intYear',
			monAmount DECIMAL(20,5) PATH 'monAmount',
			Comments VARCHAR(100) PATH 'Comments'
	) AS X
	WHERE
		COALESCE(BudgetDetId,0) = 0;
RETURN;
END; $$;


