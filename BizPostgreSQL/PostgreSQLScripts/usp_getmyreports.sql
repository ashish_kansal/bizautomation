-- Stored procedure definition script USP_GetMyReports for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMyReports(v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(rptID as VARCHAR(255)),cast(vcFileName as VARCHAR(255)),cast(RPtHeading as VARCHAR(255)),cast(RptDesc as VARCHAR(255)), RPtHeading || ' - ' || RptDesc as Report,cast(tintRptSequence as VARCHAR(255)),cast(tintType as VARCHAR(255)) from PageMaster
   join ReportList on
   numPageID = NumID
   join MyReportList
   on rptID = numRPTId
   where numModuleID = 8 and numUserCntID = v_numUserCntID and tintType = 0 AND bitEnable = true
   union
   select cast(numRPTId as VARCHAR(255)),'frmCustomReport.aspx?reportId=' || CAST(numRptId AS VARCHAR(10)) as vcFileName ,
vcReportName as RPtHeading,
vcReportDescription as RptDesc,
vcReportName || ' - ' || vcReportDescription as Report,cast(tintRptSequence as VARCHAR(255)),cast(tintType as VARCHAR(255))
   from CustomReport
   join MyReportList
   on numCustomReportId = numRPTId
   where  MyReportList.numUserCntID = v_numUserCntID and tintType = 1
   order by 6;
END; $$;












