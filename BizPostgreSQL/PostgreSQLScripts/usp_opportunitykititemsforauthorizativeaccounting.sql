-- Stored procedure definition script USP_OpportunityKitItemsForAuthorizativeAccounting for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityKitItemsForAuthorizativeAccounting(v_numOppId NUMERIC(9,0) DEFAULT null,                                                                                                                                                
 v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

     --OpportunityKitItems
   open SWV_RefCur for SELECT
		I.vcItemName as Item,
		I.charItemType as type,
		'' as "desc",
		(coalesce(OBI.numUnitHour,0)*(OKI.numQtyItemsReq_Orig*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,I.numDomainID,I.numBaseUnit),1))) as Unit,
		0 as Price,
		0 as Amount,0 AS ItemTotalAmount,
		0 as listPrice,
		I.numItemCode as ItemCode,
		numOppChildItemID AS numoppitemtCode,
		L.vcData as vcItemClassification,
		CASE WHEN I.bitTaxable = false THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		coalesce(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		coalesce(I.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		coalesce(I.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN '0' ELSE coalesce(OKI.monAvgCost,'0') END) AS AverageCost,
		coalesce(OBI.bitDropShip,false) AS bitDropShip,
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) AS numProjectID,
		NULLIF(Opp.numClassID,0) AS numClassID
   FROM
   OpportunityItems Opp
   JOIN
   OpportunityBizDocItems OBI
   ON
   OBI.numOppItemID = Opp.numoppitemtCode
   JOIN
   OpportunityKitItems OKI
   ON
   OKI.numOppItemID = Opp.numoppitemtCode
   AND Opp.numOppId = OKI.numOppId
   LEFT JOIN
   Item iMain
   ON
   Opp.numItemCode = iMain.numItemCode
   LEFT JOIN
   Item I
   ON
   OKI.numChildItemID = I.numItemCode
   LEFT JOIN
   Listdetails L
   ON
   I.numItemClassification = L.numListItemID
   WHERE
   Opp.numOppId = v_numOppId
   AND OBI.numOppBizDocID = v_numOppBizDocsId
   AND coalesce(I.bitKitParent,false) = false
   AND coalesce(iMain.bitAssembly,false) = false
   UNION ALL
   SELECT
		I.vcItemName as Item,
		charitemType as type,
		'' as "desc",
		(coalesce(OBI.numUnitHour,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,I.numDomainID,I.numBaseUnit),1))) as Unit,
		0 as Price,
		0 as Amount,0 AS ItemTotalAmount,
		0 as listPrice,
		I.numItemCode as ItemCode,
		OKI.numOppChildItemID AS numoppitemtCode,
		L.vcData as vcItemClassification,
		CASE WHEN bitTaxable = false THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		coalesce(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		coalesce(I.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		coalesce(I.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN '0' ELSE coalesce(OKCI.monAvgCost,'0') END) AS AverageCost,
		coalesce(OBI.bitDropShip,false) AS bitDropShip,
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) AS numProjectID,
		NULLIF(Opp.numClassID,0) AS numClassID
   FROM
   OpportunityKitChildItems OKCI
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKCI.numOppChildItemID = OKI.numOppChildItemID
   INNER JOIN
   OpportunityItems Opp
   ON
   OKCI.numOppItemID = Opp.numoppitemtCode
   AND OKCI.numOppId = v_numOppId
   INNER JOIN
   OpportunityBizDocItems OBI
   ON
   OBI.numOppItemID = Opp.numoppitemtCode
   LEFT JOIN
   Item I
   ON
   OKCI.numItemID = I.numItemCode
   LEFT JOIN
   Listdetails L
   ON
   I.numItemClassification = L.numListItemID
   WHERE
   OKCI.numOppId = v_numOppId
   AND OBI.numOppBizDocID = v_numOppBizDocsId;
   RETURN;
END; $$;













