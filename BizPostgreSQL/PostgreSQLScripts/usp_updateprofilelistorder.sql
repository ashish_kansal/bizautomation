-- Stored procedure definition script USP_updateProfileListOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_updateProfileListOrder(v_xmlStr TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TBLTEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TBLTEMP
   (
      numRelProID NUMERIC(9,0),
      tintOrder NUMERIC(9,0)
   );    
                           
	insert into tt_TBLTEMP
	(
      numRelProID,
      tintOrder
	)   
	select
		X.numRelProID
		,X.tintOrder
	from
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_xmlStr AS XML)
		COLUMNS
			numRelProID NUMERIC(9,0) PATH 'numRelProID',
			tintOrder NUMERIC(9,0) PATH 'tintOrder'
	) AS X;                              
                                
                
      
--inserting data from temp table to main table(listdetails)    
   UPDATE RelProfile dtl SET numOrder = t.tintOrder FROM  tt_TBLTEMP t
   WHERE dtl.numRelProID = t.numRelProID;
   RETURN;
END; $$;


