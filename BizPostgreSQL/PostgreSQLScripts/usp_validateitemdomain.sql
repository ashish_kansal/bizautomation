-- Stored procedure definition script USP_ValidateItemDomain for PostgreSQL
Create or replace FUNCTION USP_ValidateItemDomain(v_numItemCode NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   v_numDomainID
		,v_numItemCode
		,(CASE WHEN EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode) THEN true ELSE false END) AS IsItemInDomain;
END; $$;












