-- Stored procedure definition script USP_CompanyTaxTypes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CompanyTaxTypes(v_numDivisionID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select TaxItems.numTaxItemID,vcTaxName,coalesce(bitApplicable,false) as bitApplicable from TaxItems
   left join DivisionTaxTypes
   on DivisionTaxTypes.numTaxItemID = TaxItems.numTaxItemID and numDivisionID = v_numDivisionID
   where  numDomainID = v_numDomainID order by TaxItems.numTaxItemID;
END; $$;














