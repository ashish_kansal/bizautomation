-- Stored procedure definition script USP_DeleteHelpCategories for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteHelpCategories(v_numHelpCategoryID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   DELETE FROM HelpCategories
   WHERE numHelpCategoryID = v_numHelpCategoryID;
   RETURN;
END; $$;




