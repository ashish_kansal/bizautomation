-- Stored procedure definition script USP_DeleteAlertConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DeleteAlertConfig(v_numAlertConfigId NUMERIC(18,0),
v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE
   FROM   AlertConfig
   WHERE  numAlertConfigId = v_numAlertConfigId AND numDomainID = v_numDomainID;
   RETURN;
END; $$; 


