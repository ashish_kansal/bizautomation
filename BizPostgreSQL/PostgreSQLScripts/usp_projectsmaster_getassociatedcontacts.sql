-- Stored procedure definition script USP_ProjectsMaster_GetAssociatedContacts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProjectsMaster_GetAssociatedContacts(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPCONTACTS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCONTACTS
   (
      numContactID NUMERIC(18,0),
      vcContact VARCHAR(200),
      vcCompanyname VARCHAR(300),
      tinUserType SMALLINT,
      ActivityID NUMERIC(18,0),
      Email VARCHAR(150),
      vcStatus VARCHAR(200),
      vcPosition VARCHAR(200),
      vcEmail VARCHAR(200),
      numPhone VARCHAR(200),
      numPhoneExtension VARCHAR(200),
      bitDefault BOOLEAN,
      vcUserName VARCHAR(200),
      numRights SMALLINT,
      numProjectRole NUMERIC(18,0)
   );
   INSERT INTO
   tt_TEMPCONTACTS
   SELECT
   AC.numContactId
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,CAST(AC.vcEmail AS VARCHAR(150)) as Email
		,CAST('' AS VARCHAR(200)) AS vcStatus
		,CAST(GetListIemName(AC.vcPosition) AS VARCHAR(200)) AS vcPosition
		,CAST(AC.vcEmail AS VARCHAR(200))
		,CAST(AC.numPhone AS VARCHAR(200))
		,CAST(AC.numPhoneExtension AS VARCHAR(200))
		,CAST(1 AS BOOLEAN)
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,''))
		,1
		,0
   FROM
   ProjectsMaster PM
   INNER JOIN
   AdditionalContactsInformation AC
   ON
   PM.numIntPrjMgr = AC.numContactId
   LEFT JOIN
   DivisionMaster DM
   ON
   DM.numDivisionID = AC.numDivisionId
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   UserMaster UM
   ON
   UM.numUserDetailId = AC.numContactId
   WHERE
   PM.numdomainId = v_numDomainID
   AND PM.numProId = v_numProId;

   INSERT INTO
   tt_TEMPCONTACTS
   SELECT
   AC.numContactId
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,CAST(AC.vcEmail AS VARCHAR(150)) as Email
		,CAST('' AS VARCHAR(200)) AS vcStatus
		,CAST(GetListIemName(AC.vcPosition) AS VARCHAR(200)) AS vcPosition
		,CAST(AC.vcEmail AS VARCHAR(200))
		,CAST(AC.numPhone AS VARCHAR(200))
		,CAST(AC.numPhoneExtension AS VARCHAR(200))
		,CAST(1 AS BOOLEAN)
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,''))
		,1
		,0
   FROM
   ProjectsMaster PM
   INNER JOIN
   AdditionalContactsInformation AC
   ON
   PM.numCustPrjMgr = AC.numContactId
   LEFT JOIN
   DivisionMaster DM
   ON
   DM.numDivisionID = AC.numDivisionId
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   UserMaster UM
   ON
   UM.numUserDetailId = AC.numContactId
   WHERE
   PM.numdomainId = v_numDomainID
   AND PM.numProId = v_numProId
   AND AC.numContactId NOT IN(SELECT TC.numContactID FROM tt_TEMPCONTACTS TC);

   INSERT INTO
   tt_TEMPCONTACTS
   SELECT DISTINCT
   AC.numContactId
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,CAST(AC.vcEmail AS VARCHAR(150)) as Email
		,CAST('' AS VARCHAR(200)) AS vcStatus
		,CAST(GetListIemName(AC.vcPosition) AS VARCHAR(200)) AS vcPosition
		,CAST(AC.vcEmail AS VARCHAR(200))
		,CAST(AC.numPhone AS VARCHAR(200))
		,CAST(AC.numPhoneExtension AS VARCHAR(200))
		,CAST(1 AS BOOLEAN)
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,''))
		,1
		,0
   FROM
   ProjectsMaster PM
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   PM.numProId = SPDT.numProjectId
   INNER JOIN
   AdditionalContactsInformation AC
   ON
   SPDT.numAssignTo = AC.numContactId
   LEFT JOIN
   DivisionMaster DM
   ON
   DM.numDivisionID = AC.numDivisionId
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   UserMaster UM
   ON
   UM.numUserDetailId = AC.numContactId
   WHERE
   PM.numdomainId = v_numDomainID
   AND PM.numProId = v_numProId
   AND AC.numContactId NOT IN(SELECT TC.numContactID FROM tt_TEMPCONTACTS TC);

   INSERT INTO
   tt_TEMPCONTACTS
   SELECT DISTINCT
   PC.numContactID
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,CAST(AC.vcEmail AS VARCHAR(150)) as Email
		,CAST('' AS VARCHAR(200)) AS vcStatus
		,CAST(GetListIemName(AC.vcPosition) AS VARCHAR(200)) AS vcPosition
		,CAST(AC.vcEmail AS VARCHAR(200))
		,CAST(AC.numPhone AS VARCHAR(200))
		,CAST(AC.numPhoneExtension AS VARCHAR(200))
		,CAST(0 AS BOOLEAN)
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,''))
		,1
		,0
   FROM
   ProjectsMaster PM
   INNER JOIN
   ProjectsContacts PC
   ON
   PM.numProId = PC.numProId
   INNER JOIN
   AdditionalContactsInformation AC
   ON
   PC.numContactID = AC.numContactId
   LEFT JOIN
   DivisionMaster DM
   ON
   DM.numDivisionID = AC.numDivisionId
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   UserMaster UM
   ON
   UM.numUserDetailId = AC.numContactId
   WHERE
   PM.numdomainId = v_numDomainID
   AND PM.numProId = v_numProId
   AND AC.numContactId NOT IN(SELECT TC.numContactID FROM tt_TEMPCONTACTS TC);

	
open SWV_RefCur for SELECT * FROM tt_TEMPCONTACTS ORDER BY vcContact;
END; $$;












