-- Stored procedure definition script USP_CreateExtUsersPortal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateExtUsersPortal(v_numCompanyID NUMERIC(9,0) DEFAULT 0,
	v_numDivisionID NUMERIC(9,0) DEFAULT 0,
	v_numContactID NUMERIC(9,0) DEFAULT 0,
	v_vcPassword VARCHAR(100) DEFAULT '',
	v_numDomainID NUMERIC DEFAULT NULL,
	INOUT v_vcEmail VARCHAR(100) DEFAULT '')
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intNoofBusinessPortalUsers  INTEGER;
   v_numBusinessPortalUsers  INTEGER;
   v_Identity  NUMERIC(18,0);
   v_numGroupID  NUMERIC;
BEGIN
   select   coalesce(intNoofBusinessPortalUsers,0) INTO v_intNoofBusinessPortalUsers FROM
   Subscribers WHERE
   numTargetDomainID = v_numDomainID;

   v_numBusinessPortalUsers := coalesce((SELECT COUNT(numExtranetDtlID) FROM ExtranetAccountsDtl WHERE numDomainID = v_numDomainID AND coalesce(bitPartnerAccess,false) = true),0);

   IF(v_numBusinessPortalUsers::bigint+1) > v_intNoofBusinessPortalUsers then
	
      RAISE EXCEPTION 'BUSINESS_PORTAL_USERS_EXCEED';
      RETURN;
   end if;

   IF coalesce(v_numCompanyID,0) = 0 then
	
      select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
   end if;
  
   IF NOT EXISTS(SELECT * FROM ExtarnetAccounts WHERE numCompanyID = v_numCompanyID and numDivisionID = v_numDivisionID) then
      select   numGroupID INTO v_numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID AND tintGroupType = 2    LIMIT 1;
      IF v_numGroupID IS NULL then 
         v_numGroupID := 0;
      end if;
      INSERT INTO ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)
		VALUES(v_numCompanyID,v_numDivisionID,v_numGroupID,v_numDomainID);
		
      v_Identity := CURRVAL('ExtarnetAccounts_seq');
   ELSE
      select   numExtranetID INTO v_Identity FROM ExtarnetAccounts WHERE numCompanyID = v_numCompanyID AND numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
   end if;
	

   IF NOT EXISTS(select * from ExtranetAccountsDtl where numContactID = v_numContactID::VARCHAR) then
	
      INSERT INTO ExtranetAccountsDtl(numExtranetID,numContactID,vcPassword,bitPartnerAccess,numDomainID)
		VALUES(v_Identity,v_numContactID,v_vcPassword,true,v_numDomainID);
   end if;

   select   coalesce(vcEmail,'') INTO v_vcEmail FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
END; $$;


