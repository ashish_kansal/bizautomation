-- Stored procedure definition script USP_ScheduledReportsGroup_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_Delete(v_numDomainID NUMERIC(18,0)
	,v_vcRecords VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_vcRecords,'') <> '' then
	
      DELETE FROM ScheduledReportsGroupReports SRGR using ScheduledReportsGroup SRG where SRG.ID = SRGR.numSRGID AND SRG.numDomainID = v_numDomainID AND SRG.ID IN(SELECT Id FROM SplitIDs(v_vcRecords,','));
      DELETE FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID AND ID IN(SELECT Id FROM SplitIDs(v_vcRecords,','));
   end if;
   RETURN;
END; $$;


