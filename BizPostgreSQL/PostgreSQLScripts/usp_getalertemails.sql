-- Stored procedure definition script USP_GetAlertEmails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertEmails(v_numAlertDTLID NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numAlertEmailID as VARCHAR(255)),cast(Alt.numAlertDTLid as VARCHAR(255)),cast(vcEmailID as VARCHAR(255)) from  AlertEmailDTL Alt
   where Alt.numAlertDTLid = v_numAlertDTLID  and numDomainID = v_numDomainID;
END; $$;












