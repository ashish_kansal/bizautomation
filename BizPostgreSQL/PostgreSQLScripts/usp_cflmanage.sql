-- Stored procedure definition script USP_cflManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_cflManage(v_locid SMALLINT DEFAULT 0,          
v_fldtype VARCHAR(30) DEFAULT '',          
v_fldlbl VARCHAR(50) DEFAULT '',          
v_FieldOdr SMALLINT DEFAULT 0,          
v_userId NUMERIC(9,0) DEFAULT 0,          
v_TabId NUMERIC(9,0) DEFAULT 0,          
INOUT v_fldId NUMERIC(9,0)  DEFAULT NULL,      
v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_vcURL VARCHAR(1000) DEFAULT '',
v_vcToolTip VARCHAR(1000) DEFAULT '',         
INOUT v_ListId NUMERIC(9,0)  DEFAULT NULL,
v_bitAutocomplete BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitElasticSearch  BOOLEAN;
   v_ddlName  VARCHAR(100);
   v_UpdateCustomField  VARCHAR(100);
BEGIN
   v_bitElasticSearch := coalesce((SELECT  bitElasticSearch FROM
   eCommerceDTL WHERE numDomainID = v_numDomainID AND bitElasticSearch = true LIMIT 1),false);


   if v_fldId = 0 then
          
  --declare @ListId as numeric(9)          
            
            
      if v_fldtype = 'SelectBox' OR v_fldtype = 'CheckBoxList' then
  
         INSERT INTO listmaster(vcListName,
   numCreatedBy,
   bintCreatedDate,
   bitDeleted,
   bitFixed ,
   numModifiedBy,
   bintModifiedDate,
   numDomainID,
   bitFlag,numModuleId)
   values(v_fldlbl,
   1,
   TIMEZONE('UTC',now()),
   false,
   false,
   1,
   TIMEZONE('UTC',now()),
   v_numDomainID,
   false,8);
  
         v_ListId := CURRVAL('ListMaster_seq');
      end if;
      Insert into CFW_Fld_Master(fld_type,
   FLd_label,
   Fld_tab_ord,
   Grp_id,
   subgrp,
   numlistid,
   numDomainID,
	   vcURL,vcToolTip,bitAutocomplete)
   values(v_fldtype,
   v_fldlbl,
   v_FieldOdr,
   v_locid,
   v_TabId,
   v_ListId,
   v_numDomainID,
   v_vcURL,v_vcToolTip, v_bitAutocomplete);
  
      v_fldId := CURRVAL('cfw_fld_master_seq'); --(SELECT MAX(fld_id) FROM dbo.CFW_Fld_Master WHERE numDomainID = @numDomainID)
   
      IF(v_bitElasticSearch = true) then
   
         IF (EXISTS(SELECT 1 FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id IN(5,9))) then
		
            INSERT INTO ElasticSearchBizCart(numDomainID,vcValue,vcAction,vcModule)
            SELECT v_numDomainID,
			CASE
            WHEN Grp_id = 5 THEN REPLACE(FLd_label,' ','') || '_C'
            WHEN Grp_id = 9 THEN REPLACE(FLd_label,' ','') || '_' || CAST(Fld_id AS VARCHAR(30))
            END
			,'Insert'
			,'CustomField'
            FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id IN(5,9);
         end if;
      end if;
   else
      if v_fldtype = 'SelectBox' OR v_fldtype = 'CheckBoxList' then
         select   FLd_label INTO v_ddlName from CFW_Fld_Master where Fld_id = v_fldId  and numDomainID = v_numDomainID;
         update  listmaster set  vcListName = v_fldlbl where vcListName = v_ddlName  and    numDomainID = v_numDomainID;
      end if;
      IF(v_bitElasticSearch = true) then
   
         IF (EXISTS(SELECT 1 FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id IN(5,9))) then
		
            IF(v_locid NOT IN(5,9)) then
			
               INSERT INTO ElasticSearchBizCart(numDomainID,vcValue,vcAction,vcModule)
               SELECT v_numDomainID,
				CASE
               WHEN Grp_id = 5 THEN REPLACE(FLd_label,' ','') || '_C'
               WHEN Grp_id = 9 THEN REPLACE(FLd_label,' ','') || '_' || CAST(Fld_id AS VARCHAR(30))
               END
				,'Delete'
				,'CustomField'
               FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id IN(5,9);
            ELSE
               IF(NOT EXISTS(SELECT 1 FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id = v_locid AND FLd_label = v_fldlbl)) then
                  v_UpdateCustomField := CASE
                  WHEN v_locid = 5 THEN REPLACE(v_fldlbl,' ','') || '_C'
                  WHEN v_locid = 9 THEN REPLACE(v_fldlbl,' ','') || '_' || SUBSTR(CAST(v_fldId AS VARCHAR(30)),1,30)
                  END;
                  INSERT INTO ElasticSearchBizCart(numDomainID,vcValue,vcAction,vcModule)
                  SELECT v_numDomainID,
					CASE
                  WHEN Grp_id = 5 THEN REPLACE(FLd_label,' ','') || '_C'
                  WHEN Grp_id = 9 THEN REPLACE(FLd_label,' ','') || '_' || CAST(Fld_id AS VARCHAR(30))
                  END || '||' || coalesce(v_UpdateCustomField,'')
					,'Update'
					,'CustomField'
                  FROM CFW_Fld_Master where Fld_id = v_fldId AND Grp_id IN(5,9);
               end if;
            end if;
         ELSEIF (v_locid IN(5,9))
         then
		
            INSERT INTO ElasticSearchBizCart(numDomainID,vcValue,vcAction,vcModule)
            SELECT v_numDomainID,
			CASE
            WHEN v_locid = 5 THEN REPLACE(v_fldlbl,' ','') || '_C'
            WHEN v_locid = 9 THEN REPLACE(v_fldlbl,' ','') || '_' || CAST(v_fldId AS VARCHAR(30))
            END
			,'Insert'
			,'CustomField';
         end if;
      end if;
      update CFW_Fld_Master set
      FLd_label = v_fldlbl,Grp_id = v_locid,subgrp = v_TabId,vcURL = v_vcURL,vcToolTip = v_vcToolTip, 
      bitAutocomplete = v_bitAutocomplete
      where Fld_id = v_fldId and   numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


