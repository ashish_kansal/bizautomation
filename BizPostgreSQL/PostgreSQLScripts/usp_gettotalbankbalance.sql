-- Stored procedure definition script USP_GetTotalBankBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTotalBankBalance(v_numDomainId NUMERIC(9,0),
INOUT v_monTotalBankBal DECIMAL(20,5))
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monDebitBankAmt  DECIMAL(20,5);        
   v_monCreditBankAmt  DECIMAL(20,5);
BEGIN
   select   sum(coalesce(GJD.numDebitAmt,0)), sum(coalesce(GJD.numCreditAmt,0)) INTO v_monDebitBankAmt,v_monCreditBankAmt From General_Journal_Header GJH
   inner join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
   inner join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId Where GJH.numDomainId = v_numDomainId And CA.numAcntTypeId = 813 And DATE_PART('day',GJH.datEntry_Date -TIMEZONE('UTC',now()):: timestamp) <= 0 And EXTRACT(Year FROM GJH.datEntry_Date) = EXTRACT(Year FROM TIMEZONE('UTC',now())); 
   v_monTotalBankBal := coalesce(v_monDebitBankAmt,0) -coalesce(v_monCreditBankAmt,0);
   RETURN;
END; $$;


