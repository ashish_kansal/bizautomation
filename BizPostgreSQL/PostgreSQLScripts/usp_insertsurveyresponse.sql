-- Stored procedure definition script usp_InsertSurveyResponse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertSurveyResponse(v_numSurID NUMERIC(9,0),               
 v_numRespondantID NUMERIC(9,0),             
 v_numSurRating NUMERIC(9,0),               
 v_numSurveyRespondentContactId NUMERIC(9,0),                
 v_vcSurveyResponseInformation TEXT,
 v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql              
--This will insert response for the surveys.              
   AS $$
   DECLARE
   v_iDoc  INTEGER;
   v_bitRegisteredRespondant  BOOLEAN;
BEGIN
   RAISE NOTICE '%',v_numSurID;    
   RAISE NOTICE '%',v_numRespondantID;    
   RAISE NOTICE '%',v_numSurveyRespondentContactId;                             
                  
   IF v_numRespondantID = 0 then
      IF v_numSurveyRespondentContactId > 0 then
         v_bitRegisteredRespondant := true;
      ELSE
         v_bitRegisteredRespondant := false;
      end if;
      INSERT INTO SurveyRespondentsMaster(numSurID, numSurRating,
         dtDateofResponse, bitRegisteredRespondant, numRegisteredRespondentContactId, numDomainId)
         VALUES(v_numSurID, v_numSurRating, TIMEZONE('UTC',now()), v_bitRegisteredRespondant, v_numSurveyRespondentContactId, v_numDomainId) RETURNING numRespondantID INTO v_numRespondantID;
   end if;            
    
   IF v_numSurRating > 0 then
    
      UPDATE SurveyRespondentsMaster SET numSurRating = v_numSurRating
      WHERE numRespondantID = v_numRespondantID;
   end if;
    
   IF v_numSurveyRespondentContactId > 0 then
   
      UPDATE SurveyRespondentsMaster SET numSurRating = v_numSurRating,numRegisteredRespondentContactId = v_numSurveyRespondentContactId
      WHERE numRespondantID = v_numRespondantID;
   end if;    
                
   INSERT INTO SurveyResponse(numRespondantID,
        numSurID,
        numParentSurID,
        numQuestionID,
        numAnsID,
        vcAnsText,numMatrixID)
	SELECT 
		v_numRespondantID AS numRespondantID
		,numSurID
		,numParentSurID
		,numQuestionID
		,numAnsID
		,vcAnsText
		,numMatrixID 
	FROM 
	XMLTABLE
	(
		'NewDataSet/SurveyResponse'
		PASSING 
			CAST(v_vcSurveyResponseInformation AS XML)
		COLUMNS
			numSurID NUMERIC PATH 'numSurID',
			numParentSurID NUMERIC PATH 'numParentSurID',
			numQuestionID NUMERIC PATH 'numQuestionID',
			numAnsID NUMERIC PATH 'numAnsID',
			vcAnsText VARCHAR(500) PATH 'vcAnsText',
			numMatrixID NUMERIC PATH 'numMatrixID'
	) AS X;             
 
   RETURN;
END; $$;


