-- Stored procedure definition script AddAuthBizDocTE for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
DROP TYPE IF EXISTS AddAuthBizDocTE_rs CASCADE;
CREATE TYPE AddAuthBizDocTE_rs AS(SWC_Col1 VARCHAR(255));
CREATE OR REPLACE FUNCTION AddAuthBizDocTE(v_numOppId NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
    v_numOppBizDocsId NUMERIC(9,0))
RETURNS SETOF AddAuthBizDocTE_rs LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocId  NUMERIC;
   v_vcBizDocID  VARCHAR(100);                  
   v_numBizMax  NUMERIC(9,0);
BEGIN
   select   numAuthoritativeSales INTO v_numBizDocId FROM    AuthoritativeBizDocs WHERE   numDomainId =(SELECT    numDomainId
      FROM      OpportunityMaster
      WHERE     numOppId = v_numOppId);

   IF(SELECT COUNT(*) FROM   OpportunityBizDocs WHERE  numoppid = v_numOppId AND numBizDocId = v_numBizDocId AND bitAuthoritativeBizDocs = true) = 0 then
      select   vcpOppName INTO v_vcBizDocID FROM    OpportunityMaster WHERE   numOppId = v_numOppId;
      select   COUNT(*) INTO v_numBizMax FROM    OpportunityBizDocs;
      IF v_numBizMax > 0 then
                
         select   MAX(numOppBizDocsId) INTO v_numBizMax FROM    OpportunityBizDocs;
      end if;
      v_numBizMax := v_numBizMax+1;
      v_vcBizDocID := coalesce(v_vcBizDocID,'') || '-BD-' || SUBSTR(CAST(v_numBizMax AS VARCHAR(10)),1,10);
      RAISE NOTICE '%',v_vcBizDocID;
      INSERT  INTO OpportunityBizDocs(numoppid,
                      numBizDocId,
                      vcBizDocID,
                      numCreatedBy,
                      dtCreatedDate,
                      numModifiedBy,
                      dtModifiedDate ,
                      dtFromDate,
					  bitAuthoritativeBizDocs)
            VALUES(v_numOppId,
                      v_numBizDocId,
                      v_vcBizDocID,
                      v_numUserCntID,
                      TIMEZONE('UTC',now()),
                      v_numUserCntID,
                      TIMEZONE('UTC',now()),
                      TIMEZONE('UTC',now()),
                      true);
            
return query SELECT  cast(CURRVAL('OpportunityBizDocs_seq') as VARCHAR(255));
   end if;
END; $$;



