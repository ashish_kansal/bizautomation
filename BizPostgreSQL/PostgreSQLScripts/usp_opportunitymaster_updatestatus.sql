-- Stored procedure definition script USP_OpportunityMaster_UpdateStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 25thjul2014>
-- Description:	<Description,, Update Order Status>
-- Used      IN: Purchase FullFillment
-- =============================================
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_UpdateStatus(v_numOppID NUMERIC(18,0), 
		v_numDomainId NUMERIC(9,0),
		v_numStatus NUMERIC(9,0),
		v_numModifiedBy NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here		
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   Update
   OpportunityMaster
   SET
   numStatus = v_numStatus,numModifiedBy = v_numModifiedBy,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE
   numOppId = v_numOppID
   and numDomainId = v_numDomainId; 

   IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId = v_numOppID AND tintopptype = 1 AND tintoppstatus = 1) then
	
      PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainId,v_numModifiedBy,v_numOppID,v_numStatus);
   end if;
   RETURN;
END; $$;


