-- Stored procedure definition script USP_LoadAttributes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LoadAttributes(v_numItemCode NUMERIC(9,0) DEFAULT 0,    
v_numListID NUMERIC(9,0) DEFAULT 0,    
v_strAtrr VARCHAR(1000) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitSerialize  BOOLEAN;    
   v_ItemGroup  NUMERIC(9,0);    
   v_strSQL  VARCHAR(1000);
   v_Cnt  INTEGER;
   v_SplitOn  CHAR(1);
BEGIN
   select   bitSerialized, numItemGroup INTO v_bitSerialize,v_ItemGroup from Item where numItemCode = v_numItemCode;    
    
    
   if v_strAtrr != '' then
      v_Cnt := 1;
      v_SplitOn := ',';
      v_strSQL := 'SELECT recid FROM CFW_Fld_Values_Serialized_Items where  bitSerialized =' || SUBSTR(CAST(v_bitSerialize AS VARCHAR(1)),1,1);
      While (POSITION(v_SplitOn IN v_strAtrr) > 0) LOOP
         if  v_Cnt = 1 then 
            v_strSQL := coalesce(v_strSQL,'') || ' and fld_value = ''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         else 
            v_strSQL := coalesce(v_strSQL,'') || ' or fld_value = ''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         end if;
         v_strAtrr := SUBSTR(v_strAtrr,POSITION(v_SplitOn IN v_strAtrr)+1,LENGTH(v_strAtrr));
         v_Cnt := v_Cnt::bigint+1;
      END LOOP;
      if v_Cnt = 1 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and fld_value = ''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
      else 
         v_strSQL := coalesce(v_strSQL,'') || ' or fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
      end if;
   end if;    
    
    
   if v_bitSerialize = false and v_numItemCode > 0 then

      if v_strAtrr = '' then
         open SWV_RefCur for
         select numListItemID,vcData from Listdetails where numListItemID in(select distinct(fld_value) from WareHouseItems W
            join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID = CSI.RecId
            join CFW_Fld_Master M on CSI.Fld_ID = M.Fld_id
            join ItemGroupsDTL on CSI.Fld_ID = ItemGroupsDTL.numOppAccAttrID
            where  M.numlistid = v_numListID and tintType = 2 and CSI.bitSerialized = false and numItemID = v_numItemCode);
      else
         v_strSQL := 'select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID=' || SUBSTR(CAST(v_numListID AS VARCHAR(20)),1,20) || ' and tintType=2 and CSI.bitSerialized=false and numItemID=' || SUBSTR(CAST(v_numItemCode AS VARCHAR(20)),1,20) || ' and fld_value!=''0'' and fld_value!=''''    
  and numWareHouseItemID in (' || coalesce(v_strSQL,'') || '))';
         OPEN SWV_RefCur FOR EXECUTE v_strSQL;
      end if;
   ELSEIF v_bitSerialize = true and v_numItemCode > 0
   then

      if v_strAtrr = '' then
         open SWV_RefCur for
         select numListItemID,vcData from Listdetails where numListItemID in(select distinct(fld_value) from WareHouseItems W
            join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID = W.numWareHouseItemID
            join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID = CSI.RecId
            join CFW_Fld_Master M on CSI.Fld_ID = M.Fld_id
            join ItemGroupsDTL on CSI.Fld_ID = ItemGroupsDTL.numOppAccAttrID
            where  M.numlistid = v_numListID and tintType = 2 and coalesce(WDTL.numQty,0) > 0 and CSI.bitSerialized = true and numItemID = v_numItemCode
            and fld_value != '0' and fld_value != '');
      else
         v_strSQL := 'select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID    
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID=' || SUBSTR(CAST(v_numListID AS VARCHAR(20)),1,20) || ' and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=true and numItemID=' || SUBSTR(CAST(v_numItemCode AS VARCHAR(20)),1,20) || '     
  and fld_value!=''0'' and fld_value!='''' and WDTL.numWareHouseItmsDTLID in     
  (' || coalesce(v_strSQL,'') || '))';
         OPEN SWV_RefCur FOR EXECUTE v_strSQL;
      end if;
   else 
      open SWV_RefCur for
      select 0;
   end if;
   RETURN;
END; $$;


