-- Stored procedure definition script usp_GetDynamicLeadBoxEntryForLeadGeneration for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDynamicLeadBoxEntryForLeadGeneration(v_numLeadBoxId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(vcDbColumnName as VARCHAR(255)),
 Case When boolAOIField = 1 Then
      Case When vcDbColumnValue = 1 Then
         vcFieldName
      Else
         ''
      End
   When vcDbColumnValue = 0 Then
      vcDbColumnValueText
   Else
      vcDbColumnValue
   End
   as vcDbColumnValue, cast(vcDbColumnValueText as VARCHAR(255)), cast(vcAssociatedControlType as VARCHAR(255))
   from DynamicLeadBoxFormDataDetails
   WHERE numLeadBoxId = v_numLeadBoxId;
END; $$;












