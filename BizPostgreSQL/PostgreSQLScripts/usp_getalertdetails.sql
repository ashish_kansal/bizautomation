-- Stored procedure definition script USP_GetAlertdetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertdetails(v_numAlertID NUMERIC(9,0) DEFAULT 0  ,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   cast(AD.numAlertDTLid as VARCHAR(255)),
	cast(AD.numAlertID as VARCHAR(255)),
	cast(AD.tintCCManager as VARCHAR(255)),
	cast(ADDT.numEmailTemplate as VARCHAR(255)),
	cast(ADDT.numDaysAfterDue as VARCHAR(255)),
	cast(ADDT.monAmount as VARCHAR(255)) ,
	cast(ADDT.numBizDocs as VARCHAR(255)),
	cast(ADDT.numDepartment as VARCHAR(255)),
	cast(ADDT.numNoOfItems as VARCHAR(255)),
	cast(ADDT.numAge as VARCHAR(255)) ,
	cast(ADDT.tintAlertOn as VARCHAR(255)) ,
	cast(AD.vcDesc as VARCHAR(255)) ,
	cast(ADDT.numEmailCampaignId as VARCHAR(255))
   from AlertDTL AD
   join AlertDomainDtl ADDT on ADDT.numAlertDTLid = AD.numAlertDTLid
   where numAlertID = v_numAlertID  and ADDT.numDomainID = v_numDomainID;
END; $$;












