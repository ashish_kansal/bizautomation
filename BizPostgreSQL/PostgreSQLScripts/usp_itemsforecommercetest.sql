-- Stored procedure definition script USP_ItemsForECommerceTest for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemsForECommerceTest(v_numSiteID NUMERIC(9,0) DEFAULT 0,
    v_numCategoryID NUMERIC(18,0) DEFAULT 0,
    v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
    v_SortBy TEXT DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER DEFAULT 0 ,
    v_SearchText TEXT DEFAULT '',--Comma seperated item ids
    v_FilterQuery TEXT DEFAULT '',----Comma seperated Attributes Ids
    v_FilterRegularCondition TEXT DEFAULT NULL,
    v_FilterCustomFields TEXT DEFAULT NULL,
    v_FilterCustomWhere TEXT DEFAULT NULL,
	v_numDivisionID NUMERIC(18,0) DEFAULT 0,
	v_numManufacturerID NUMERIC(18,0) DEFAULT 0,
	v_FilterItemAttributes TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(9,0);
   v_numDefaultRelationship  NUMERIC(18,0);
   v_numDefaultProfile  NUMERIC(18,0);
   v_tintPreLoginPriceLevel  SMALLINT;
   v_tintPriceLevel  SMALLINT;
   v_tintDisplayCategory  SMALLINT;
		
   v_strSQL  TEXT;
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_Where  TEXT;
   v_SortString  TEXT;
   v_searchPositionColumn  TEXT;
   v_searchPositionColumnGroupBy  TEXT;
   v_row  INTEGER;
   v_data  VARCHAR(100);
   v_dynamicFilterQuery  TEXT;
   v_checkFldType  VARCHAR(100);
   v_count  NUMERIC(9,0);
   v_whereNumItemCode  TEXT;
   v_filterByNumItemCode  VARCHAR(100);
        
   v_bitAutoSelectWarehouse  BOOLEAN;
   v_vcWarehouseIDs  VARCHAR(1000);
   v_Join  TEXT;
   v_SortFields  TEXT;
   v_vcSort  TEXT;

   v_DefaultSort  TEXT;
   v_DefaultSortFields  TEXT;
   v_DefaultSortJoin  TEXT;
   v_intCnt  INTEGER;
   v_intTotalCnt  INTEGER;	
   v_strColumnName  VARCHAR(100);
   v_bitCustom  BOOLEAN;
   v_FieldID  VARCHAR(100);				

   v_fldList  TEXT;
   v_fldList1  TEXT;
   v_tmpSQL  TEXT;
   v_tintOrder  SMALLINT;                                                  
   v_vcFormFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(1);                                             
   v_vcAssociatedControlType  VARCHAR(10);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_WhereCondition  TEXT;                       
   v_numFormFieldId  NUMERIC;  
		--DECLARE @vcFieldType CHAR(1)
   v_vcFieldType  VARCHAR(15);
   v_FilterCustomFieldIds  TEXT;
   v_strCustomSql  TEXT;
		
   v_fldID  NUMERIC(18,0);
   v_RowNumber  TEXT;
   v_bitSortPriceMode  BOOLEAN;
   v_fldID1  NUMERIC(18,0);
   v_RowNumber1  TEXT;
   v_vcSort1  VARCHAR(10);
   v_vcSortBy  VARCHAR(1000);
BEGIN
   select   numDomainID INTO v_numDomainID FROM    Sites WHERE   numSiteID = v_numSiteID;
        
   IF coalesce(v_numDivisionID,0) > 0 then
		
      select   coalesce(tintCRMType,0), coalesce(vcProfile,0), coalesce(tintPriceLevel,0) INTO v_numDefaultRelationship,v_numDefaultProfile,v_tintPriceLevel FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = v_numDivisionID AND DivisionMaster.numDomainID = v_numDomainID;
   end if;

   select   coalesce(bitDisplayCategory,0), numRelationshipId, numProfileId, (CASE WHEN coalesce(bitShowPriceUsingPriceLevel,0) = 1 THEN(CASE WHEN v_numDivisionID > 0 THEN(CASE WHEN coalesce(bitShowPriceUsingPriceLevel,0) = 1 THEN coalesce(v_tintPriceLevel,0) ELSE 0 END) ELSE coalesce(tintPreLoginProceLevel,0) END) ELSE 0 END) INTO v_tintDisplayCategory,v_numDefaultRelationship,v_numDefaultProfile,v_tintPreLoginPriceLevel FROM
   eCommerceDTL WHERE
   numSiteId = v_numSiteID;

   DROP TABLE IF EXISTS tt_TEMPITEMSTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMSTABLE AS
      SELECT
      IC.numItemID
			,C.numCategoryID
			,vcCategoryName
		
      FROM
      SiteCategories SC
      INNER JOIN
      ItemCategory IC
      ON
      SC.numCategoryID = IC.numCategoryID
      INNER JOIN
      Category C
      ON
      SC.numCategoryID = C.numCategoryID
      WHERE
      SC.numSiteID = v_numSiteID
      AND (IC.numCategoryID = v_numCategoryID OR coalesce(v_numCategoryID,0) = 0);
		
   DROP TABLE IF EXISTS tt_TMPITEMCAT CASCADE;
   CREATE TEMPORARY TABLE tt_TMPITEMCAT
   (
      numCategoryID NUMERIC(18,0)
   );
   DROP TABLE IF EXISTS tt_TMPITEMCODE CASCADE;
   CREATE TEMPORARY TABLE tt_TMPITEMCODE
   (
      numItemCode NUMERIC(18,0)
   );
   IF LENGTH(v_FilterCustomFields) > 0 AND LENGTH(v_FilterCustomWhere) > 0 then
      v_FilterCustomFieldIds := Replace(v_FilterCustomFields,'[','');
      v_FilterCustomFieldIds := Replace(v_FilterCustomFieldIds,']','');
      v_strCustomSql := 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' || coalesce(v_FilterCustomFieldIds,'') || ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' || coalesce(v_FilterCustomFields,'') || ')) AS pvt 
															) T  WHERE 1=1 AND ' || coalesce(v_FilterCustomWhere,'');
      RAISE NOTICE '%',v_strCustomSql;
      EXECUTE v_strCustomSql;
   end if;
		
   v_SortString := '';
   v_searchPositionColumn := '';
   v_searchPositionColumnGroupBy := '';
   v_strSQL := '';
   v_whereNumItemCode := '';
   v_dynamicFilterQuery := '';
		 
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
                
   select   coalesce(ECD.bitAutoSelectWarehouse,0) INTO v_bitAutoSelectWarehouse FROM eCommerceDTL AS ECD WHERE ECD.numSiteId = v_numSiteID
   AND ECD.numDomainID = v_numDomainID;

   IF (v_numWareHouseID = 0 OR v_bitAutoSelectWarehouse = true) then
            
      IF v_bitAutoSelectWarehouse = true then
					
         v_vcWarehouseIDs := coalesce(OVERLAY((SELECT DISTINCT ',' || CAST(numWareHouseID AS VARCHAR(10)) FROM Warehouses WHERE Warehouses.numDomainID = v_numDomainID) placing '' from 1 for 1),'');
      ELSE
         select   coalesce(numDefaultWareHouseID,0) INTO v_numWareHouseID FROM    eCommerceDTL WHERE   numDomainID = v_numDomainID;
         v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
      end if;
   ELSE
      v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
   end if;

   RAISE NOTICE '%',v_vcWarehouseIDs;

		--PRIAMRY SORTING USING CART DROPDOWN
   v_Join := '';
   v_SortFields := '';
   v_vcSort := '';

   IF v_SortBy = '' then
			
      v_SortString := ' vcItemName Asc ';
   ELSEIF POSITION('~0' IN v_SortBy) > 0 OR POSITION('~1' IN v_SortBy) > 0
   then
      DROP TABLE IF EXISTS tt_FLDVALUES CASCADE;
      CREATE TEMPORARY TABLE tt_FLDVALUES AS
         SELECT * FROM SplitIDsWithPosition(v_SortBy,'~');
      select   RowNumber, id INTO v_RowNumber,v_fldID FROM tt_FLDVALUES WHERE RowNumber = 1;
      select   CASE WHEN id = 0 THEN 'ASC' ELSE 'DESC' END INTO v_vcSort FROM tt_FLDVALUES WHERE RowNumber = 2;
      v_Join := coalesce(v_Join,'') || ' LEFT JOIN CFW_FLD_Values_Item CFVI' || coalesce(v_RowNumber,'') || ' ON I.numItemCode = CFVI' || coalesce(v_RowNumber,'') || '.RecID AND CFVI' || coalesce(v_RowNumber,'') || '.fld_Id = ' || SUBSTR(CAST(v_fldID AS VARCHAR(10)),1,10);
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
      v_SortString := '[fld_Value' || coalesce(v_RowNumber,'') || '] '; --+ @vcSort
      v_SortFields := ', CFVI' || coalesce(v_RowNumber,'') || '.fld_Value [fld_Value' || coalesce(v_RowNumber,'') || ']';
   ELSE
				--SET @SortString = @SortBy	
				
      IF POSITION('monListPrice' IN v_SortBy) > 0 then
         select   coalesce(bitSortPriceMode,0) INTO v_bitSortPriceMode FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
         RAISE NOTICE '%',v_bitSortPriceMode;
         IF v_bitSortPriceMode = true then
					
            v_SortString := ' ';
         ELSE
            v_SortString := v_SortBy;
         end if;
      ELSE
         v_SortString := v_SortBy;
      end if;
   end if;	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
   DROP TABLE IF EXISTS tt_TEMPSORT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSORT AS
      SELECT ROW_NUMBER() OVER(ORDER BY Custom,vcFieldName) AS RowID,* FROM(SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName
      FROM View_DynamicColumns
      WHERE numFormId = 84
      AND coalesce(bitSettingField,0) = 1
      AND coalesce(bitDeleted,0) = 0
      AND coalesce(bitCustom,0) = 0
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
      FROM View_DynamicCustomColumns
      WHERE numFormId = 84
      AND numDomainID = v_numDomainID
      AND Grp_id IN(5,9)
      AND vcAssociatedControlType = 'TextBox'
      AND coalesce(bitCustom,0) = 1
      AND tintPageType = 1) TABLE1 ORDER BY Custom,vcFieldName;
		
		--SELECT * FROM #tempSort
   v_DefaultSort := '';
   v_DefaultSortFields := '';
   v_DefaultSortJoin := '';		
   v_intCnt := 0;
   SELECT COUNT(*) INTO v_intTotalCnt FROM tt_TEMPSORT;
   v_strColumnName := '';
		
   IF(SELECT coalesce(bitEnableSecSorting,0) FROM eCommerceDTL WHERE numSiteId = v_numSiteID) = 1 then
		
      DROP TABLE IF EXISTS tt_FLDDEFAULTVALUES CASCADE;
      CREATE TEMPORARY TABLE tt_FLDDEFAULTVALUES
      (
         RowNumber INTEGER,
         Id VARCHAR(1000)
      );
      WHILE(v_intCnt < v_intTotalCnt) LOOP
         v_intCnt := v_intCnt::bigint+1;
         select   numFieldID, vcDbColumnName, Custom INTO v_FieldID,v_strColumnName,v_bitCustom FROM tt_TEMPSORT WHERE RowID = v_intCnt;
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

         IF v_bitCustom = false then
					
            v_DefaultSort :=  coalesce(v_strColumnName,'') || ',' || coalesce(v_DefaultSort,'');
         ELSE
            v_vcSortBy := SUBSTR(CAST(v_FieldID AS VARCHAR(100)),1,100) || '~' || '0';
						--PRINT @vcSortBy

            INSERT INTO tt_FLDDEFAULTVALUES(RowNumber,Id) SELECT RowNumber, id FROM SplitIDsWithPosition(v_vcSortBy,'~');
            select   RowNumber::bigint+100+v_intCnt::bigint, Id INTO v_RowNumber1,v_fldID1 FROM tt_FLDDEFAULTVALUES WHERE RowNumber = 1;
            v_vcSort1 := 'ASC';
            v_DefaultSortJoin := coalesce(v_DefaultSortJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFVI' || coalesce(v_RowNumber1,'') || ' ON I.numItemCode = CFVI' || coalesce(v_RowNumber1,'') || '.RecID AND CFVI' || coalesce(v_RowNumber1,'') || '.fld_Id = ' || SUBSTR(CAST(v_fldID1 AS VARCHAR(10)),1,10);
            v_DefaultSort := '[fld_Value' || coalesce(v_RowNumber1,'') || '] ' || ',' || coalesce(v_DefaultSort,'');
            v_DefaultSortFields := coalesce(v_DefaultSortFields,'') || ', CFVI' || coalesce(v_RowNumber1,'') || '.fld_Value [fld_Value' || coalesce(v_RowNumber1,'') || ']';
         end if;
      END LOOP;
      IF LENGTH(v_DefaultSort) > 0 then
			
         v_DefaultSort := SUBSTR(v_DefaultSort,1,LENGTH(v_DefaultSort) -1);
         v_SortString := coalesce(v_SortString,'') || ',';
      end if;
   end if;
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
   IF LENGTH(v_SearchText) > 0 then
			
      RAISE NOTICE '%',1;
      IF POSITION('SearchOrder' IN v_SortString) > 0 then
				
         v_searchPositionColumn := ' ,SearchItems.RowNumber AS SearchOrder ';
         v_searchPositionColumnGroupBy := ' ,SearchItems.RowNumber ';
      end if;
      v_Where := ' INNER JOIN dbo.SplitIDsWithPosition(''' || coalesce(v_SearchText,'') || ''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode AND WHERE ISNULL(IsArchieve,0) = 0';
   ELSEIF v_SearchText = ''
   then
      v_Where := ' WHERE 1=2 '
      || ' AND ISNULL(IsArchieve,0) = 0';
   ELSE
      RAISE NOTICE '%',2;
      v_Where := ' WHERE 1=1 '
      || ' AND ISNULL(IsArchieve,0) = 0';
      RAISE NOTICE '%',v_tintDisplayCategory;
      RAISE NOTICE '%',v_Where;
      IF v_tintDisplayCategory = 1 then
				
         INSERT INTO tt_TMPITEMCAT(numCategoryID) with recursive CTE AS(SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel = 1 THEN 1 ELSE 0 END AS bitChild
         FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID = C.numCategoryID WHERE SC.numSiteID = v_numSiteID AND (C.numCategoryID = v_numCategoryID OR coalesce(v_numCategoryID,0) = 0) AND numDomainID = v_numDomainID
         UNION ALL
         SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON
         C.numDepCategory = CTE.numCategoryID AND CTE.bitChild = 1 AND numDomainID = v_numDomainID) SELECT DISTINCT C.numCategoryID FROM CTE C JOIN ItemCategory IC ON C.numCategoryID = IC.numCategoryID
         WHERE numItemID IN(SELECT numItemID FROM WareHouseItems WI
            INNER JOIN Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = v_numDomainID
            GROUP BY numItemID
            HAVING SUM(WI.numOnHand) > 0
            UNION ALL
            SELECT numItemID FROM WareHouseItems WI
            INNER JOIN Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = v_numDomainID
            GROUP BY numItemID
            HAVING SUM(WI.numAllocation) > 0);
      end if;
   end if;

   v_strSQL := coalesce(v_strSQL,'') || CONCAT(' WITH Items AS 
												( 
													 SELECT
														I.numDomainID
														,I.numItemCode
														,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
														,I.bitMatrix
														,I.bitKitParent
														,I.numItemGroup
														,vcItemName
														,I.bintCreatedDate
														,vcManufacturer
														,ISNULL(txtItemDesc,'''') txtItemDesc
														,vcSKU
														,fltHeight
														,fltWidth
														,fltLength
														,vcModelID
														,fltWeight
														,numBaseUnit
														,numSaleUnit
														,numPurchaseUnit
														,bitFreeShipping
														,C.vcCategoryName
														,C.vcDescription as CategoryDesc
														,charItemType
														,monListPrice
														,bitAllowBackOrder
														,bitShowInStock
														,numVendorID
														,numItemClassification',v_searchPositionColumn,v_SortFields,
   v_DefaultSortFields,'
														,SUM(numOnHand) numOnHand
														,SUM(numAllocation) numAllocation
													 FROM      
														#TEMPItemsTable	T1
													 INNER JOIN Item AS I ON T1.numItemID = I.numItemCode 
													 INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',v_numSiteID,'
													 LEFT JOIN WareHouseItems WI ON WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' || coalesce(v_vcWarehouseIDs,'') || ''','',''))
													  ',
   v_Join,' ',v_DefaultSortJoin);

   IF LENGTH(coalesce(v_FilterItemAttributes,'')) > 0 then
			
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_FilterItemAttributes,'');
   end if;

   IF LENGTH(v_FilterCustomFields) > 0 AND LENGTH(v_FilterCustomWhere) > 0 then
			
      v_strSQL := coalesce(v_strSQL,'') || ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ';
   end if;
			
   IF(SELECT COUNT(*) FROM tt_TMPITEMCAT) > 0 then
			
      v_strSQL := coalesce(v_strSQL,'') || ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = T1.numCategoryID ';
   ELSEIF v_numManufacturerID > 0
   then
			
      v_Where := REPLACE(v_Where,'WHERE',' WHERE I.numManufacturer = ' || SUBSTR(CAST(v_numManufacturerID AS VARCHAR(10)),1,10) || ' AND ');
   end if; 
			
   IF(SELECT COUNT(*) FROM EcommerceRelationshipProfile WHERE numSiteID = v_numSiteID AND numRelationship = v_numDefaultRelationship AND numProfile = v_numDefaultProfile) > 0 then
			
      v_Where := coalesce(v_Where,'') || CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',v_numSiteID,' AND ERP.numRelationship=',v_numDefaultRelationship,
      ' AND ERP.numProfile=',v_numDefaultProfile,')');
   end if;

			--PRINT @Where
   v_strSQL := coalesce(v_strSQL,'') || coalesce(v_Where,'');
   RAISE NOTICE '%',v_strSQL;
   IF LENGTH(v_FilterRegularCondition) > 0 then
			
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_FilterRegularCondition,'');
   end if;                                         

   IF LENGTH(v_whereNumItemCode) > 0 then
                                        
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_filterByNumItemCode,'');
   end if;
   v_strSQL := CONCAT(v_strSQL,' GROUP BY
												I.numDomainID
												,I.numItemCode
												,I.bitMatrix
												,I.bitKitParent
												,I.numItemGroup
												,vcItemName
												,I.bintCreatedDate
												,vcManufacturer
												,txtItemDesc
												,vcSKU
												,fltHeight
												,fltWidth
												,fltLength
												,vcModelID
												,fltWeight
												,numBaseUnit
												,numSaleUnit
												,numPurchaseUnit
												,bitFreeShipping
												,C.vcCategoryName
												,C.vcDescription
												,charItemType
												,monListPrice
												,bitAllowBackOrder
												,bitShowInStock
												,numVendorID
												,numItemClassification',v_searchPositionColumnGroupBy,v_SortFields,
   v_DefaultSortFields,(CASE WHEN coalesce(v_tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), 
   ') SELECT * INTO #TEMPItems FROM Items;');
                                        
   IF v_numDomainID = 204 then
			
      v_strSQL := coalesce(v_strSQL,'') || 'DELETE FROM 
											#TEMPItems
										WHERE 
											numItemCode IN (
																SELECT 
																	F.numItemCode
																FROM 
																	#TEMPItems AS F
																WHERE 
																	ISNULL(F.bitMatrix,0) = 1 
																	AND EXISTS (
																				SELECT 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																				FROM 
																					#TEMPItems t1
																				WHERE 
																					t1.vcItemName = F.vcItemName
																					AND t1.bitMatrix = 1
																					AND t1.numItemGroup = F.numItemGroup
																				GROUP BY 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																				HAVING 
																					Count(t1.numItemCode) > 1
																			)
															)
											AND numItemCode NOT IN (
																					SELECT 
																						Min(numItemCode)
																					FROM 
																						#TEMPItems AS F
																					WHERE
																						ISNULL(F.bitMatrix,0) = 1 
																						AND Exists (
																									SELECT 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																									FROM 
																										#TEMPItems t2
																									WHERE 
																										t2.vcItemName = F.vcItemName
																									   AND t2.bitMatrix = 1
																									   AND t2.numItemGroup = F.numItemGroup
																									GROUP BY 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																									HAVING 
																										Count(t2.numItemCode) > 1
																								)
																					GROUP BY 
																						vcItemName, bitMatrix, numItemGroup
																				);';
   end if;
									  
									  
   v_strSQL := coalesce(v_strSQL,'') || ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' || coalesce(v_SortString,'') || coalesce(v_DefaultSort,'') || ' ' || coalesce(v_vcSort,'') || ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)';



   select   COALESCE(coalesce(v_fldList,'') || ',','') || concat('"',REPLACE(FLd_label,' ','') || '_C','"') INTO v_fldList FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 5;
   select   COALESCE(coalesce(v_fldList1,'') || ',','') || concat('"',REPLACE(FLd_label,' ','') || '_' || SUBSTR(CAST(Fld_id AS VARCHAR(30)),1,30),'"') INTO v_fldList1 FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 9;
            
   v_strSQL := coalesce(v_strSQL,'') || 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || '
                                      AND   Rownumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '
                                      order by Rownumber ';
                                                                  
   IF LENGTH(coalesce(v_fldList,'')) > 0 OR LENGTH(coalesce(v_fldList1,'')) > 0 then
            
      v_strSQL := coalesce(v_strSQL,'') || 'SELECT  
											Rownumber
											,I.numItemCode
											,vcCategoryName
											,CategoryDesc
											,vcItemName
											,txtItemDesc
											,vcManufacturer
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,bitFreeShipping
											,' ||(CASE
      WHEN v_tintPreLoginPriceLevel > 0
      THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))'
      ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)'
      END)
      || ' AS monListPrice,
											   ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP
											   ' ||(CASE WHEN v_numDomainID = 172 THEN ',ISNULL(CASE 
														WHEN tintRuleType = 1 AND tintDiscountType = 1
														THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
														WHEN tintRuleType = 1 AND tintDiscountType = 2
														THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
														WHEN tintRuleType = 2 AND tintDiscountType = 1
														THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
														WHEN tintRuleType = 2 AND tintDiscountType = 2
														THEN ISNULL(Vendor.monCost,0) + decDiscount
														WHEN tintRuleType = 3
														THEN decDiscount
													END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
													WHEN tintRuleType = 1 AND tintDiscountType = 1 
													THEN decDiscount 
													WHEN tintRuleType = 1 AND tintDiscountType = 2
													THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
													WHEN tintRuleType = 2 AND tintDiscountType = 1
													THEN decDiscount
													WHEN tintRuleType = 2 AND tintDiscountType = 2
													THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
													WHEN tintRuleType = 3
													THEN 0
												END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END) || '  
											,vcPathForImage
											,vcpathForTImage
											,UOM AS UOMConversionFactor
											,UOMPurchase AS UOMPurchaseConversionFactor
											,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN (CASE 
																WHEN bitAllowBackOrder = 1 THEN 1
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
																ELSE 1
															END)
													   ELSE 1
											END) AS bitInStock
											,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN (CASE 
																			WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																			WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																			WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																			ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																		END)
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock
											, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
											,(SELECT 
													COUNT(numProId) 
												FROM 
													PromotionOffer PO
												WHERE 
													numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
													AND ISNULL(bitEnabled,0)=1
													AND ISNULL(bitAppliesToSite,0)=1 
													AND ISNULL(bitRequireCouponCode,0)=0
													AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
													AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
													AND (1 = (CASE 
																WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																ELSE 0
															END)
														OR
														1 = (CASE 
																WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																ELSE 0
															END)
														)
												)  IsOnSale,' || coalesce(v_fldList,'') ||(CASE WHEN LENGTH(coalesce(v_fldList1,'')) > 0 THEN CONCAT(',',v_fldList1) ELSE '' END) || '
                                        FROM 
											#tmpPagedItems I
										LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1' ||(CASE
      WHEN v_tintPreLoginPriceLevel > 0
      THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' || CAST(v_tintPreLoginPriceLevel -1 AS VARCHAR(30)) || ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption '
      ELSE ''
      END) ||(CASE
      WHEN v_numDomainID = 172
      THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
      ELSE ''
      END) || ' 
										OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' || coalesce(v_vcWarehouseIDs,'') || ''','',''))) AS W1 			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase';
      IF LENGTH(coalesce(v_fldList,'')) > 0 then
				
         v_strSQL := coalesce(v_strSQL,'') || ' left join (
														SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ' AND t1.fld_type <> ''Link''
														AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' || coalesce(v_fldList,'') || ')) AS pvt on I.numItemCode=pvt.RecId';
      end if;
      IF LENGTH(coalesce(v_fldList1,'')) > 0 then
				
         v_strSQL := coalesce(v_strSQL,'') || ' left join (
														SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ' AND t1.fld_type <> ''Link''
														AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' || coalesce(v_fldList1,'') || ')) AS pvt1 on I.numItemCode=pvt1.RecId';
      ELSE
         v_strSQL := REPLACE(v_strSQL,',##FLDLIST1##','');
      end if;
      v_strSQL := coalesce(v_strSQL,'') || ' order by Rownumber';
   ELSE
      RAISE NOTICE 'flds2';
      v_strSQL := coalesce(v_strSQL,'') || ' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;';
   end if;               
                                        
            --print('@strSQL=')
            --PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
   v_strSQL := coalesce(v_strSQL,'') || ' SELECT :1 = COUNT(*) FROM #TEMPItems WHERE RowID=1 ';                            

   RAISE NOTICE '%',LENGTH(v_strSQL);
   v_tmpSQL := v_strSQL;
			 --PRINT  @tmpSQL
   RAISE NOTICE '%',CAST(v_tmpSQL AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
             
             
   v_tintOrder := 0;                                                  
   v_WhereCondition := '';                 
                   
              
   DROP TABLE IF EXISTS tt_TEMPAVAILABLEFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAVAILABLEFIELDS
   (
      numFormFieldId  NUMERIC(9,0),
      vcFormFieldName VARCHAR(50),
		--vcFieldType CHAR(1),
      vcFieldType VARCHAR(15),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcListItemType CHAR(1),
      intRowNum INTEGER,
      vcItemValue VARCHAR(3000)
   );
		   
   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)
   SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CAST(CASE GRP_ID WHEN 4 then 'C'
   WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
   WHEN 9 THEN CAST(Fld_id AS VARCHAR(15))
   ELSE 'C' END AS VARCHAR(15)) AS vcFieldType,
				fld_type as vcAssociatedControlType,
				coalesce(C.numlistid,0) AS numListID,
				CASE WHEN C.numlistid > 0 THEN 'L'
   ELSE ''
   END AS vcListItemType,ROW_NUMBER() OVER(ORDER BY Fld_id ASC) AS intRowNum
   FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldId = C.Fld_id
   WHERE   C.numDomainID = v_numDomainID
   AND GRP_ID IN(5,9);

		  
   open SWV_RefCur for
   SELECT * FROM tt_TEMPAVAILABLEFIELDS;
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		
		
   DROP TABLE IF EXISTS tt_TMPITEMCODE CASCADE;
		
   DROP TABLE IF EXISTS tt_TMPITEMCAT CASCADE;
		
   DROP TABLE IF EXISTS tt_FLDVALUES CASCADE;
		
   DROP TABLE IF EXISTS tt_TEMPSORT CASCADE;
		
   DROP TABLE IF EXISTS tt_FLDDEFAULTVALUES CASCADE;

   DROP TABLE IF EXISTS tt_TMPPAGEDITEMS CASCADE;


   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   RETURN;
END; $$;




