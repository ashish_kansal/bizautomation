-- Stored procedure definition script USP_GetDivisionCreditBalanceHistory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivisionCreditBalanceHistory(v_numDomainId   NUMERIC(9,0)  DEFAULT 0,
v_numDivisionID NUMERIC(9,0) DEFAULT 0,
v_tintOppType SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT opp.numOppId,r.numReturnID,om.vcpOppName,I.vcItemName,opp.numItemCode,
		sum(CBH.monAmount) as monAmount,0 as numOppBizDocsId,null as vcBizDocID,CASE om.tintopptype WHEN 1 THEN 'Sales' WHEN 2 THEN 'Purchase' END AS type,om.tintopptype
   FROM   OpportunityItems AS opp INNER JOIN Returns AS r ON opp.numOppId = r.numOppID and opp.numItemCode = r.numitemcode
   INNER JOIN OpportunityMaster om ON om.numOppId = opp.numOppId
   INNER JOIN Item I ON I.numItemCode = opp.numItemCode
   INNER JOIN DivisionMaster div on div.numDivisionID = om.numDivisionId
   INNER JOIN CreditBalanceHistory CBH on CBH.numDivisionID = div.numDivisionID
   WHERE  om.numDomainId = v_numDomainId and om.numDivisionId = v_numDivisionID and CBH.numReturnID = r.numReturnID and om.tintopptype = v_tintOppType
   group by opp.numOppId,r.numReturnID,om.vcpOppName,I.vcItemName,opp.numItemCode,
   om.tintopptype
   Union
   SELECT om.numOppId,null as numreturnid,om.vcpOppName,null as vcItemName,null as numitemcode,
			CBH.monAmount,CBH.numOppBizDocsId,oppBD.vcBizDocID,CASE om.tintopptype WHEN 1 THEN 'Sales Return' WHEN 2 THEN 'Purchase Return' END AS type,om.tintopptype
   FROM CreditBalanceHistory CBH join OpportunityBizDocs oppBD on  CBH.numOppBizDocsId = oppBD.numOppBizDocsId
   INNER JOIN OpportunityMaster om ON om.numOppId = oppBD.numoppid
   INNER JOIN DivisionMaster div on div.numDivisionID = om.numDivisionId
   WHERE  om.numDomainId = v_numDomainId and om.numDivisionId = v_numDivisionID
   and CBH.numDomainID = v_numDomainId and CBH.numDivisionID = v_numDivisionID and om.tintopptype = v_tintOppType
   UNION
   SELECT null AS numoppid,null as numreturnid,vcMemo,null as vcItemName,null as numitemcode,
			CBH.monAmount,null AS numOppBizDocsId,vcReference AS vcBizDocID,'Over Payment Credit' AS type, 0 AS tintOppType
   FROM CreditBalanceHistory CBH WHERE numDomainID = v_numDomainId AND numDivisionID = v_numDivisionID;
END; $$;
--created by anoop jayaraj                                          












