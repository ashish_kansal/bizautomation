CREATE OR REPLACE FUNCTION USP_GetManagerEmail(v_byteMode SMALLINT,
v_numUserID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then --- If Userid Is Available

      open SWV_RefCur for
      select coalesce(vcEmail,'') from AdditionalContactsInformation where
      numContactId =(select numManagerID from AdditionalContactsInformation Addc
         join UserMaster U
         on U.numUserDetailId = Addc.numContactId
         where numUserID = v_numUserID);
   end if;
   if v_byteMode = 1 then  --- If Conatct ID is Available

      open SWV_RefCur for
      select coalesce(vcEmail,'') from AdditionalContactsInformation where
      numContactId =(select numManagerID from AdditionalContactsInformation where numContactId = v_numContactID);
   end if;
   RETURN;
END; $$;


