DROP FUNCTION IF EXISTS USP_GetContactList1;

CREATE OR REPLACE FUNCTION USP_GetContactList1(                                                                             
--                                                                                    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                    
v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                                                    
v_tintUserRightType SMALLINT DEFAULT 0,                                                                                    
v_tintSortOrder SMALLINT DEFAULT 4,                                                                                    
v_SortChar CHAR(1) DEFAULT '0',                                                                                   
v_FirstName VARCHAR(100) DEFAULT '',                                                                                  
v_LastName VARCHAR(100) DEFAULT '',                                                                                  
v_CustName VARCHAR(100) DEFAULT '',                                                                                  
v_CurrentPage INTEGER DEFAULT NULL,                                                                                  
v_PageSize INTEGER DEFAULT NULL,                                                                                  
v_columnName VARCHAR(50) DEFAULT NULL,                                                                                  
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                                                  
v_numDivisionID NUMERIC(9,0) DEFAULT 0,                                              
v_bitPartner BOOLEAN DEFAULT NULL ,                                                   
v_inttype NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
 v_vcRegularSearchCriteria TEXT DEFAULT '',
 v_vcCustomSearchCriteria TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_join  VARCHAR(400);                  
   v_firstRec  INTEGER;                                                                                  
   v_lastRec  INTEGER;                                                                                  
   v_strSql TEXT;                                                                            
   v_strExternalUser  TEXT DEFAULT '';
   v_tintOrder  SMALLINT;                                                        
   v_vcFieldName  VARCHAR(50);                                                        
   v_vcListItemType  VARCHAR(3);                                                   
   v_vcListItemType1  VARCHAR(1);                                                       
   v_vcAssociatedControlType  VARCHAR(20);                                                        
   v_numListID  NUMERIC(9,0);                                                        
   v_vcDbColumnName  VARCHAR(200);                            
   v_WhereCondition  VARCHAR(2000);                             
   v_vcLookBackTableName  VARCHAR(2000);                      
   v_bitCustom  BOOLEAN;  
   v_bitAllowEdit  BOOLEAN;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;                   
   v_vcColumnName  VARCHAR(500);                            
   v_Nocolumns  SMALLINT;                      
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_ListRelID  NUMERIC(9,0); 
  
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_join := '';                 
                        
   if v_columnName ilike 'CFW.Cust%' then

      v_join := ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= ' || replace(v_columnName,'CFW.Cust','') || ' ';
      v_columnName := 'CFW.Fld_Value';
   end if;                                         
   if v_columnName ilike 'DCust%' then

      v_join := ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= ' || replace(v_columnName,'DCust','');
      v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  ';
      v_columnName := 'LstCF.vcData';
   end if;                                                                              
                                                                                  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                                   
                                                                                  
                                                                                  
   v_strSql := 'with tblcontacts as (Select ';                                                                                                                         
                                     
   v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || CASE when v_columnName = 'ADC.numAge' then 'ADC.bintDOB' ELSE v_columnName end || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   ADC.numContactId                          
  FROM AdditionalContactsInformation ADC                                
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE  ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId '  || coalesce(v_join,'') ||  '                                                                  
  left join ListDetails LD on LD.numListItemID=cmp.numCompanyRating                                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus 
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=true ';                                           
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=true and CA.bitDeleted=false                                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;                                                                   
                                                                    
   if v_tintSortOrder = 7 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactid=ADC.numContactID ';
   end if;                                                                     
                                                       
   v_strSql := coalesce(v_strSql,'') || '                                                                
        where 
 --DM.tintCRMType<>0     AND 
 cmp.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                             
    and DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                             
   if v_inttype <> 0 and v_inttype <> 101 then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numContactType  = ' || SUBSTR(CAST(v_inttype AS VARCHAR(10)),1,10);
   end if;                            
   if v_inttype = 101 then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.bitPrimaryContact  =true ';
   end if;                            
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                                             
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                                              
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and cmp.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                                                  
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And ADC.vcFirstName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                                                           
	
   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,' AND EA.numDivisionID=DM.numDivisionID))');

   IF v_tintUserRightType = 1 then
      v_strSql := CONCAT(v_strSql,' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' OR ',v_strExternalUser,')',(CASE WHEN v_bitPartner = true THEN ' OR (CA.bitShareportal=true AND CA.bitDeleted=false)' ELSE '' END));
   ELSEIF v_tintUserRightType = 2
   then
      v_strSql := CONCAT(v_strSql,' AND DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',v_numUserCntID,')');
   end if;
                
   if v_numDivisionID <> 0 then  
      v_strSql := coalesce(v_strSql,'') || ' And DM.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10);
   end if;                      
                                                                                         
   if v_tintSortOrder = 2 then  
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || '  AND ADC.numContactType=92  ';
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND ADC.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 5
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                                                                             
                    
   if (v_tintSortOrder = 5 and v_columnName != 'ADC.bintcreateddate') then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF (v_tintSortOrder = 6 and v_columnName != 'ADC.bintcreateddate')
   then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and cType=''C'' ';
   end if;                      
    
   IF POSITION('ADC.vcLastFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcLastFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=true))');
   end if;
   IF POSITION('ADC.vcNextFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=false))');
   end if;  

   IF v_vcRegularSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if; 


   IF v_vcCustomSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;

   v_strSql := coalesce(v_strSql,'') || ')';                      
                                                                                 
   v_tintOrder := 0;                                                        
   v_WhereCondition := '';                       
                         
   v_Nocolumns := 0;                      
  
   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = 10 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_inttype
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 10 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_inttype) TotalRows;

                               
                     
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );

--Check if Master Form Configuration is created by administrator if NoColumns=0
   IF v_Nocolumns = 0 then

      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 10 AND numRelCntType = v_inttype AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
	
         v_IsMasterConfAvailable := true;
      end if;
   end if;


--If MasterConfiguration is available then load it otherwise load default columns
   IF v_IsMasterConfAvailable = true then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      10,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_inttype,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 10 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_inttype AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      10,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_inttype,1,true,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 10 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_inttype AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 10 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_inttype AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 10 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_inttype AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      if v_Nocolumns = 0 then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         select 10,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,v_inttype,1,false,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = 10 and bitDefault = false AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM View_DynamicColumns
      where numFormId = 10 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_inttype
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      from View_DynamicCustomColumns
      where numFormId = 10 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_inttype
      AND coalesce(bitCustom,false) = true
      order by tintOrder asc;
   end if;
   
   v_strSql := coalesce(v_strSql,'') || ' select COALESCE(ADC.numContactId,0) as numContactId            
       ,COALESCE(ADC.numECampaignID,0) as numECampaignID
	   ,COALESCE( (SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactId AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true),0) numConEmailCampID
	   ,COALESCE(VIE.Total,0) as TotalEmail
	   ,COALESCE(VOA.OpenActionItemCount,0) as TotalActionItem
       ,COALESCE(DM.numDivisionID,0) numDivisionID, COALESCE(DM.numTerID,0)numTerID,COALESCE( ADC.numRecOwner,0) numRecOwner,COALESCE( DM.tintCRMType,0) tintCRMType ,RowNumber,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail         ';  
	   
---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 COALESCE( (CASE WHEN 
				COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) 
				= COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),''0'')



	ELSE '''' 
	END)) as FollowupFlag ';

 
	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------                    
                                                    
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            

                   
   while v_tintOrder > 0 LOOP
      if v_bitCustom = false then
         if LOWER(v_vcLookBackTableName) = LOWER('AdditionalContactsInformation') then
            v_Prefix := 'ADC.';
         end if;
         if LOWER(v_vcLookBackTableName) = LOWER('DivisionMaster') then
            v_Prefix := 'DM.';
         end if;

         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';

			----- Added by Priya For Followups(14Jan2018)---- 

                         
			----- Added by Priya For Followups(14Jan2018)----  
         if v_vcDbColumnName = 'vcLastFollowup' then

            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcNextFollowup'
         then

            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcPassword'
         then
 
            v_strSql := coalesce(v_strSql,'') || ',COALESCE((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=CAST(ADC.numContactID AS VARCHAR)),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then

            if v_vcListItemType = 'LI' then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
    
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
 
            v_strSql := coalesce(v_strSql,'') || ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -v_ClientTimeZoneOffset || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -v_ClientTimeZoneOffset || ') AS DATE)= CAST(now()::TIMESTAMP + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -v_ClientTimeZoneOffset || ') AS DATE)= CAST(now()::TIMESTAMP + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -v_ClientTimeZoneOffset || ') AS DATE),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'Label'
         then

            IF v_vcDbColumnName = 'vcCompactContactDetails' then
	
               v_strSql := coalesce(v_strSql,'') || ' ,'''' AS vcCompactContactDetails';
			ELSEIF v_vcDbColumnName = 'vcTaxID' then
				v_strSql := coalesce(v_strSql,'') || ', ADC.vcTaxID "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
               when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else coalesce(v_vcDbColumnName,'') end || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         else
            if v_vcDbColumnName = 'vcCampaignAudit' then
               v_strSql := coalesce(v_strSql,'') || ', GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) "' || coalesce(v_vcColumnName,'') || '"';
            else
               v_strSql := coalesce(v_strSql,'') || ', ' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         end if;
      ELSEIF v_bitCustom = true
      then

         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master where    CFW_Fld_Master.Fld_id = v_numFieldId;
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then 0 when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then 1 end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSE
            v_strSql := coalesce(v_strSql,'') || CONCAT(',GetCustFldValueContact(',v_numFieldId,',ADC.numContactID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                             
                          
       
-------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   drop table IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID AND DFCS.numFormID = DFFM.numFormID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 10 AND DFCS.numFormID = 10 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
---------------------------- 
 
   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      v_strSql := coalesce(v_strSql,'') || ',tCS.vcColorScheme';
   end if;                         
                                  
   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) AS varchar)

			|| ''/'' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)AS varchar) || '')''
			|| ''<a onclick=openDrip('' || (cast(COALESCE((SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactID AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true AND ADC.bitPrimaryContact = true),0)AS varchar)) || '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr ';                         
                                  
   v_strSql := coalesce(v_strSql,'') || ' ,TotalRowCount FROM AdditionalContactsInformation ADC join tblcontacts T on T.numContactId=ADC.numContactId                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                      
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE  ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=true
   ' || coalesce(v_WhereCondition,'');                     

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      if v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CI.';
      end if;
      if v_vcCSLookBackTableName = 'AddressDetails' then
         v_Prefix := 'AD.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
	v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
	
         v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
      end if;
   end if;
    

   v_strSql := coalesce(v_strSql,'') || ' WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID   
  and RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' order by RowNumber';

                                                     
   RAISE NOTICE '%',v_strSql;                              
                      
   OPEN SWV_RefCur FOR EXECUTE v_strSql;


   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;

 
   RETURN;
END; $$;


