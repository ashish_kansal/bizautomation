-- Stored procedure definition script USP_CalculateCommissionSalesReturn for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CalculateCommissionSalesReturn(v_numComPayPeriodID NUMERIC(18,0),
	 v_numDomainID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE

   v_dtPayStart  DATE;
   v_dtPayEnd  DATE;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPCOMMISSIONSALESRETURN CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONSALESRETURN
   (
      numComPayPeriodID NUMERIC(18,0),
      numComRuleID NUMERIC(18,0),
      numUserCntID NUMERIC(18,0),
      tintAssignTo SMALLINT,
      numReturnHeaderID NUMERIC(18,0),
      numReturnItemID NUMERIC(18,0),
      tintComType SMALLINT,
      tintComBasedOn SMALLINT,
      decCommission DOUBLE PRECISION,
      monCommission DECIMAL(20,5)
   );
   select   dtStart, dtEnd INTO v_dtPayStart,v_dtPayEnd FROM
   CommissionPayPeriod WHERE
   numComPayPeriodID = v_numComPayPeriodID;

   DELETE FROM
   SalesReturnCommission
   WHERE
   numDomainId = v_numDomainID
   AND coalesce(bitCommissionReversed,false) = false
   AND numReturnItemID IN(SELECT
   ReturnHeader.numReturnHeaderID
   FROM
   ReturnHeader
   INNER JOIN
   ReturnItems
   ON
   ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
   CROSS JOIN LATERAL(SELECT
      MAX(datEntry_Date) AS dtDepositDate
      FROM
      General_Journal_Header
      WHERE
      numDomainId = v_numDomainID
      AND numReturnID = ReturnHeader.numReturnHeaderID) TempDepositMaster
   WHERE
   ReturnHeader.numDomainID = v_numDomainID
   AND ReturnHeader.numReturnStatus = 303
   AND CAST(TempDepositMaster.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);

   INSERT INTO
   tt_TEMPCOMMISSIONSALESRETURN
   SELECT
   v_numComPayPeriodID
		,BizDocComission.numComRuleID
		,BizDocComission.numUserCntID
		,BizDocComission.tintAssignTo
		,ReturnHeader.numReturnHeaderID
		,ReturnItems.numReturnItemID
		,BizDocComission.tintComType
		,BizDocComission.tintComBasedOn
		,BizDocComission.decCommission
		,CASE tintComType
   WHEN 1 --PERCENT
   THEN
      CASE
      WHEN coalesce(bitDomainCommissionBasedOn,false) = true
      THEN
         CASE tintDomainCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
         WHEN 1 THEN(coalesce(ReturnItems.monTotAmount,0) -coalesce(OpportunityItems.monVendorCost,0))*(BizDocComission.decCommission/CAST(100 AS DOUBLE PRECISION))
								--ITEM GROSS PROFIT (AVERAGE COST)
         WHEN 2 THEN(coalesce(ReturnItems.monTotAmount,0) -coalesce(OpportunityItems.monAvgCost,0))*(BizDocComission.decCommission/CAST(100 AS DOUBLE PRECISION))
         END
      ELSE
         coalesce(ReturnItems.monTotAmount,0)*(BizDocComission.decCommission/CAST(100 AS DOUBLE PRECISION))
      END
   ELSE  --FLAT
      BizDocComission.decCommission
   END AS monCommissionReversed
   FROM
   ReturnHeader
   INNER JOIN
   ReturnItems
   ON
   ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
   INNER JOIN
   BizDocComission
   ON
   ReturnItems.numOppItemID = BizDocComission.numOppItemID
   INNER JOIN
   OpportunityItems
   ON
   ReturnItems.numOppItemID = OpportunityItems.numoppitemtCode
   CROSS JOIN LATERAL(SELECT
      MAX(datEntry_Date) AS dtDepositDate
      FROM
      General_Journal_Header
      WHERE
      numDomainId = v_numDomainID
      AND numReturnID = ReturnHeader.numReturnHeaderID) TempDepositMaster
   WHERE
   ReturnHeader.numDomainID = v_numDomainID
   AND ReturnHeader.numReturnStatus = 303
   AND CAST(TempDepositMaster.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd;
	
	-- UPDATE FLAT COMMMISSION AMOUNT 
   UPDATE
   tt_TEMPCOMMISSIONSALESRETURN T1
   SET
   monCommission = decCommission/(SELECT COUNT(*) FROM tt_TEMPCOMMISSIONSALESRETURN Tinner WHERE Tinner.numUserCntID = T1.numUserCntID AND Tinner.tintComType = 2)
	
   WHERE
   tintComType = 2;

   INSERT INTO SalesReturnCommission(numDomainId
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommissionReversed
		,bitCommissionReversed)
   SELECT
   v_numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommission
		,false
   FROM
   tt_TEMPCOMMISSIONSALESRETURN T1
   WHERE(SELECT
      COUNT(*)
      FROM
      SalesReturnCommission SRC
      WHERE
      SRC.numUserCntID = T1.numUserCntID
      AND SRC.numReturnHeaderID = T1.numReturnHeaderID
      AND SRC.numReturnItemID = T1.numReturnItemID
      AND SRC.tintAssignTo = T1.tintAssignTo) = 0;
RETURN;
END; $$;


