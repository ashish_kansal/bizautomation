CREATE OR REPLACE FUNCTION USP_AccountsPerformance(v_numdivisionid NUMERIC(9,0)
, INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_year INTEGER;    
   v_Month  INTEGER;   
   v_day  INTEGER;  
   
   v_startDate TIMESTAMP;    
   v_EndDate TIMESTAMP;    
   v_count  SMALLINT;    
   v_SalesAmount  DOUBLE PRECISION;
   v_PurchaseAmount  DOUBLE PRECISION;
BEGIN
   v_year := CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS Integer);     
   v_Month := CAST(EXTRACT(month FROM TIMEZONE('UTC',now())) AS Integer);    
   if NULLIF(v_Month,0) < 10 then 
      v_Month := coalesce(v_Month,0);
   end if;    

   v_count := 1;   

   drop table IF EXISTS tt_ACCOUNTINGPERFORMANCE CASCADE;
   CREATE TEMPORARY TABLE tt_ACCOUNTINGPERFORMANCE
   (
      vcMonth VARCHAR(15),
      SalesAmount DOUBLE PRECISION,
      PurchaseAmount DOUBLE PRECISION  
   );



-- Do some stuff with the table



   RAISE NOTICE '%',v_count;
   while  v_count <> 13 LOOP
      v_day := CAST(CASE WHEN v_Month = 1
      THEN 31
      WHEN v_Month = 2
      THEN CASE WHEN (MOD(v_year:: NUMERIC,4) = 0 AND MOD(v_year:: NUMERIC,100) <> 0) OR
         MOD(v_year:: NUMERIC,400) = 0
         THEN 29
         ELSE 28
         END
      WHEN v_Month = 3
      THEN 31
      WHEN v_Month = 4
      THEN 30
      WHEN v_Month = 5
      THEN 31
      WHEN v_Month = 6
      THEN 30
      WHEN v_Month = 7
      THEN 31
      WHEN v_Month = 8
      THEN 31
      WHEN v_Month = 9
      THEN 30
      WHEN v_Month = 10
      THEN 31
      WHEN v_Month = 11
      THEN 30
      WHEN v_Month = 12
      THEN 31 END AS VARCHAR(5));
      v_startDate := coalesce(v_Month,0) || '/01/' || coalesce(v_year,0);
      v_EndDate := coalesce(v_Month,0) || '/' || coalesce(v_day,0) || '/' || coalesce(v_year,0);
      RAISE NOTICE '%',v_startDate;
      RAISE NOTICE '%',v_EndDate;
      RAISE NOTICE '%',v_count;
      select   coalesce(sum(monPAmount),0) INTO v_SalesAmount from OpportunityMaster Opp Inner Join OpportunityBizDocs OppBizDocs On OppBizDocs.numoppid = Opp.numOppId WHERE  numDivisionId = v_numdivisionid and tintopptype = 1 and /*tintOppStatus=1 and */CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) >= CAST(v_startDate AS timestamp) and CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) <= CAST(v_EndDate AS timestamp)
      AND (OppBizDocs.numBizDocId IN(SELECT    numAuthoritativePurchase FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.numDomainId) OR OppBizDocs.numBizDocId IN(SELECT    numAuthoritativeSales FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.numDomainId));
      select   coalesce(sum(monPAmount),0) INTO v_PurchaseAmount from OpportunityMaster Opp Inner Join OpportunityBizDocs OppBizDocs On OppBizDocs.numoppid = Opp.numOppId where numDivisionId = v_numdivisionid and tintopptype = 2 and /*tintOppStatus=1 and */CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) >= CAST(v_startDate AS timestamp) and CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) <= CAST(v_EndDate AS timestamp)
      AND (OppBizDocs.numBizDocId IN(SELECT    numAuthoritativePurchase FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.numDomainId) OR OppBizDocs.numBizDocId IN(SELECT    numAuthoritativeSales FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.numDomainId));
      INSERT INTO tt_ACCOUNTINGPERFORMANCE(vcMonth, SalesAmount,PurchaseAmount)
      SELECT 	CAST(TO_CHAR('1900-01-01':: date+CAST(v_Month -1 || 'month' as interval),'Month') AS VARCHAR(15)),v_SalesAmount,v_PurchaseAmount;
      v_Month := CAST(v_Month -1 AS VARCHAR(5));
      if NULLIF(v_Month,0) < 10 then 
         v_Month := coalesce(v_Month,0);
      end if;
      if v_Month = 0 then 
         v_year := CAST(v_year -1 AS VARCHAR(5));
      end if;
      if v_Month = 0 then 
         v_Month := CAST(12 AS VARCHAR(5));
      end if;
      v_count := v_count+1;
   END LOOP;    
    
   open SWV_RefCur for SELECT SUBSTR(vcMonth,0,4) AS vcMonth,SalesAmount AS Invoice,PurchaseAmount AS "Purchase Orders"
   FROM tt_ACCOUNTINGPERFORMANCE;
   

   RETURN;
END; $$;






