-- Stored procedure definition script USP_ManageEmbeddedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEmbeddedCost(v_numOppBizDocID NUMERIC,
    v_numCostCatID NUMERIC,
    v_numDomainID NUMERIC,
    v_numUserCntID NUMERIC,
    v_strItems TEXT,
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalEmbeddedCost  DECIMAL(20,5); 
   v_ItemCount  INTEGER;
   v_EmbeddedCostForEachItem  DECIMAL(20,5);
   v_OldEmbeddedCostForEachItem  DECIMAL(20,5);
   v_bitAppliedEmbeddedCost  BOOLEAN;
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 0 then
		
      IF v_numCostCatID > 0
      AND v_numOppBizDocID > 0 then

         IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
                    
            SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
            INSERT  INTO EmbeddedCost(numOppBizDocID,
                                  numCostCatID,
                                  numCostCenterID,
                                  numAccountID,
                                  numDivisionID,
                                  numPaymentMethod,
                                  monCost,
                                  monActualCost,
                                  vcMemo,
                                  dtDueDate,
                                  numDomainID,
                                  numCreatedBy,
                                  dtCreatedDate,
                                  numModifiedBy,
                                  dtModifiedDate)
            SELECT  v_numOppBizDocID,
                                        v_numCostCatID,
                                        X.numCostCenterID,
                                        X.numAccountID,
                                        X.numDivisionID,
                                        X.numPaymentMethod,
                                        0,--X.monCost,
                                        X.monActualCost,
                                        X.vcMemo,
                                        X.dtDueDate,
                                        v_numDomainID,
                                        v_numUserCntID,
                                        TIMEZONE('UTC',now()),
                                        v_numUserCntID,
                                        TIMEZONE('UTC',now())
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numEmbeddedCostID NUMERIC PATH 'numEmbeddedCostID',
					numCostCenterID NUMERIC PATH 'numCostCenterID',
					numAccountID NUMERIC PATH 'numAccountID',
					numDivisionID NUMERIC PATH 'numDivisionID',
					numPaymentMethod NUMERIC PATH 'numPaymentMethod',
					monCost DECIMAL(20,5) PATH 'monCost',
					monActualCost DECIMAL(20,5) PATH 'monActualCost',
					vcMemo TEXT PATH 'vcMemo',
					dtDueDate TIMESTAMP PATH 'dtDueDate'
			) AS X 
			WHERE
				COALESCE(numEmbeddedCostID,0)=0;
                 
            UPDATE EmbeddedCost
            SET	numCostCenterID = X.numCostCenterID,numAccountID = X.numAccountID,numDivisionID = X.numDivisionID,
            numPaymentMethod = X.numPaymentMethod,
--                              [monCost]=X.monCost,
                              monActualCost = X.monActualCost,
            vcMemo = X.vcMemo,dtDueDate = X.dtDueDate
            FROM
			(
				SELECT
					Y.* 
				FROM  
				XMLTABLE
				(
					'NewDataSet/Table1'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numEmbeddedCostID NUMERIC PATH 'numEmbeddedCostID',
						numCostCenterID NUMERIC PATH 'numCostCenterID',
						numAccountID NUMERIC PATH 'numAccountID',
						numDivisionID NUMERIC PATH 'numDivisionID',
						numPaymentMethod NUMERIC PATH 'numPaymentMethod',
						monActualCost DECIMAL(20,5) PATH 'monActualCost',
						vcMemo TEXT PATH 'vcMemo',
						dtDueDate TIMESTAMP PATH 'dtDueDate'
				) AS Y 
            INNER JOIN EmbeddedCost EC ON EC.numEmbeddedCostID = Y.numEmbeddedCostID LEFT OUTER JOIN
            OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId
            where COALESCE(numEmbeddedCostID,0) > 0 AND coalesce(PD.bitIntegrated,false) = false) X
            WHERE EmbeddedCost.numEmbeddedCostID = X.numEmbeddedCostID;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
            CREATE TEMPORARY TABLE tt_TEMP
            (
               numEmbeddedCostID NUMERIC,
               monActualCost DECIMAL(20,5),
               numDivisionID NUMERIC
            );
            INSERT INTO tt_TEMP
            SELECT    numEmbeddedCostID,monActualCost,numDivisionID
            FROM 
			XMLTABLE
				(
					'NewDataSet/Table1'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numEmbeddedCostID NUMERIC PATH 'numEmbeddedCostID',
						monActualCost DECIMAL(20,5) PATH 'monActualCost',
						numDivisionID NUMERIC PATH 'numDivisionID'
				) AS Y 
			WHERE
				COALESCE(numEmbeddedCostID,0) > 0;

							--Remove Bills record which are already paid to account, they can't be updated
            DELETE FROM tt_TEMP WHERE numEmbeddedCostID IN(SELECT t.numEmbeddedCostID FROM tt_TEMP t INNER JOIN EmbeddedCost EC ON EC.numEmbeddedCostID = t.numEmbeddedCostID INNER JOIN OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE PD.bitIntegrated = true);
                                        
							 --Update Bill value
            UPDATE OpportunityBizDocsDetails SET monAmount = X.monActualCost,numDivisionID = X.numDivisionID
            FROM(SELECT EC.numBizDocsPaymentDetId,t.monActualCost,t.numDivisionID FROM tt_TEMP t INNER JOIN EmbeddedCost EC ON EC.numEmbeddedCostID = t.numEmbeddedCostID) X
            WHERE OpportunityBizDocsDetails.numBizDocsPaymentDetId = X.numBizDocsPaymentDetId;
							 --Update Bill value
            UPDATE OpportunityBizDocsPaymentDetails SET monAmount = X.monActualCost
            FROM(SELECT EC.numBizDocsPaymentDetId,t.monActualCost FROM tt_TEMP t INNER JOIN EmbeddedCost EC ON EC.numEmbeddedCostID = t.numEmbeddedCostID) X
            WHERE OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId = X.numBizDocsPaymentDetId;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;

         end if;
      end if;
   end if;
		
		--Distibute Total cost among bizdoc items
   IF v_tintMode = 1 then
		
      IF EXISTS(SELECT * FROM EmbeddedCost WHERE   numOppBizDocID = v_numOppBizDocID AND numDomainID = v_numDomainID AND coalesce(monCost,0) > 0) then
                    
                    -- remove old cost per item
         UPDATE OpportunityBizDocItems
         SET
         monPrice = coalesce(monPrice,0)  -(monEmbeddedCost/numUnitHour),monTotAmount =(coalesce(monPrice,0)*numUnitHour) -monEmbeddedCost,monTotAmtBefDiscount =(coalesce(monPrice,0)*numUnitHour) -monEmbeddedCost,
         bitEmbeddedCost = false,monEmbeddedCost = 0
         WHERE   numOppBizDocID = v_numOppBizDocID
         AND bitEmbeddedCost = false;		
                        
					--Embed new cost in each item
         UPDATE OpportunityBizDocItems
         SET
         bitEmbeddedCost = false,monEmbeddedCost = X.monAmount,monPrice = coalesce(monPrice,0)+(X.monAmount/OpportunityBizDocItems.numUnitHour),monTotAmount =(coalesce(monPrice,0)*numUnitHour)+X.monAmount,
         monTotAmtBefDiscount =(coalesce(monPrice,0)*numUnitHour)+X.monAmount
         FROM(SELECT
         numOppBizDocItemID,
									SUM(EmbeddedCostItems.monAmount) AS monAmount
         FROM    EmbeddedCost
         INNER JOIN EmbeddedCostItems ON EmbeddedCost.numEmbeddedCostID = EmbeddedCostItems.numEmbeddedCostID
         WHERE   numOppBizDocID = v_numOppBizDocID
         GROUP BY numOppBizDocItemID) X
         WHERE(X.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID AND OpportunityBizDocItems.numUnitHour > 0
         AND OpportunityBizDocItems.bitEmbeddedCost = false);
                        
									
					--Integrate bizdoc with Embedded  cost
         UPDATE   OpportunityBizDocs
         SET      bitAppliedEmbeddedCost = true,monEmbeddedCostPerItem =(SELECT coalesce(SUM(monCost),0) FROM EmbeddedCost WHERE numOppBizDocID = v_numOppBizDocID)
         WHERE    numOppBizDocsId = v_numOppBizDocID;
      ELSE
         RAISE NOTICE 'hi';
         UPDATE OpportunityBizDocItems
         SET
         monPrice = coalesce(monPrice,0)  -(monEmbeddedCost/numUnitHour),monTotAmount =(coalesce(monPrice,0)*numUnitHour) -monEmbeddedCost,monTotAmtBefDiscount =(coalesce(monPrice,0)*numUnitHour) -monEmbeddedCost,
         bitEmbeddedCost = false,monEmbeddedCost = 0
         WHERE   numOppBizDocID = v_numOppBizDocID
         AND bitEmbeddedCost = true;
         UPDATE   OpportunityBizDocs
         SET bitAppliedEmbeddedCost = false,monEmbeddedCostPerItem = 0
         WHERE    numOppBizDocsId = v_numOppBizDocID;
      end if;
   end if;
   RETURN;
END; $$;
 
              
      


