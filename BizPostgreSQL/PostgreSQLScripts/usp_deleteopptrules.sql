CREATE OR REPLACE FUNCTION usp_deleteOpptRules
(
	v_numRuleId NUMERIC(18,0),
	v_numDetsDel NUMERIC DEFAULT 0
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDetsDel = 0 then  -- If the value is 1 then it means that the details have to be
      DELETE FROM OpptRuleDetails WHERE numruleid = v_numRuleId;
      DELETE FROM OpptRuleMaster WHERE numruleid = v_numRuleId;
   ELSE
      DELETE FROM OpptRuleDetails WHERE numruleid = v_numRuleId;
   end if;

   RETURN;
END; $$;


