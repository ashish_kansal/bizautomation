-- Stored procedure definition script USP_GetItemAttributesEcommerce for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemAttributesEcommerce(v_numDomainID NUMERIC(9,0),
	v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
    v_FilterQuery VARCHAR(2000) DEFAULT '',
    v_numCategoryID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
		CFW.Fld_id
		,CFW.fld_type
		,CFW.FLd_label
		,CFW.numlistid
		,CFW.bitAutocomplete
   FROM
   CFW_Fld_Master CFW
   JOIN
   ItemGroupsDTL
   ON
   numOppAccAttrID = Fld_id
   WHERE
   numDomainID = v_numDomainID
   AND Grp_id = 9
   AND tintType = 2
   AND numItemGroupID IN(SELECT DISTINCT numItemGroup FROM ItemCategory INNER JOIN Item ON ItemCategory.numItemID = Item.numItemCode  WHERE numCategoryID = v_numCategoryID)
   ORDER BY
   bitAutoComplete;
END; $$;












