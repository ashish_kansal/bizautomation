-- Stored procedure definition script USP_PromotionOffer_ClearCouponCodeItemPromotion for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PromotionOffer_ClearCouponCodeItemPromotion(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_numPromotionID NUMERIC(18,0),
	v_vcItems TEXT,
	v_numCurrencyID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnits DOUBLE PRECISION,
      numWarehouseItemID NUMERIC(18,0),
      monUnitPrice DECIMAL(20,5),
      fltDiscount DECIMAL(20,5),
      bitDiscountType BOOLEAN,
      monTotalAmount DECIMAL(20,5),
      numPromotionID NUMERIC(18,0),
      bitPromotionTriggered BOOLEAN,
      bitPromotionDiscount BOOLEAN,
      vcPromotionDescription TEXT,
      vcKitChildItems TEXT,
      vcInclusionDetail TEXT,
      bitChanged BOOLEAN
   );
   IF coalesce(v_vcItems,'') <> '' then
      INSERT INTO tt_TEMP(numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,bitChanged)
      SELECT
      numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,CAST(0 AS BOOLEAN)
      FROM
	  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_vcItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numOppItemID NUMERIC(18,0) PATH 'numOppItemID',
					numItemCode NUMERIC(18,0) PATH 'numItemCode',
					numUnits DOUBLE PRECISION PATH 'numUnits',
					numWarehouseItemID NUMERIC(18,0) PATH 'numWarehouseItemID',
					monUnitPrice DECIMAL(20,5) PATH 'monUnitPrice',
					fltDiscount DECIMAL(20,5) PATH 'fltDiscount',
					bitDiscountType BOOLEAN PATH 'bitDiscountType',
					monTotalAmount DECIMAL(20,5) PATH 'monTotalAmount',
					numPromotionID NUMERIC(18,0) PATH 'numPromotionID',
					bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered',
					bitPromotionDiscount BOOLEAN PATH 'bitPromotionDiscount',
					vcKitChildItems TEXT PATH 'vcKitChildItems',
					vcInclusionDetail TEXT PATH 'vcInclusionDetail',
					vcPromotionDescription TEXT PATH 'vcPromotionDescription'
			) AS X;

      UPDATE
      tt_TEMP T1
      SET
      numPromotionID = 0,bitPromotionTriggered = false,vcPromotionDescription = '',
      bitChanged = true,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0),fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0),
      bitDiscountType = coalesce((select (CASE WHEN tintDisountType = 1 THEN true else false END) from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),false),
      bitPromotionDiscount = false
      WHERE
      coalesce(numPromotionID,0) = v_numPromotionID;
   end if;

   open SWV_RefCur for SELECT * FROM tt_TEMP WHERE bitChanged = true;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/













