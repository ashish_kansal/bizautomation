-- Stored procedure definition script USP_OpportunityBizDocs_RevertFulFillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_RevertFulFillment(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

   v_vcItemType  CHAR(1);
   v_bitDropShip  BOOLEAN;
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitKit  BOOLEAN;
   v_numOppItemID  INTEGER;
   v_numWarehouseItemID  INTEGER;
   v_numQty  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_vcDescription  VARCHAR(300);

   v_j  INTEGER DEFAULT 0;
   v_ChildCount  INTEGER;
   v_vcChildDescription  VARCHAR(300);
   v_numOppChildItemID  INTEGER;
   v_numChildItemCode  INTEGER;
   v_bitChildIsKit  BOOLEAN;
   v_numChildWarehouseItemID  INTEGER;
   v_numChildQty  DOUBLE PRECISION;
   v_numChildQtyShipped  DOUBLE PRECISION;

   v_k  INTEGER DEFAULT 1;
   v_CountKitChildItems  INTEGER;
   v_vcKitChildDescription  VARCHAR(300);
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_numKitChildWarehouseItemID  NUMERIC(18,0);
   v_numKitChildQty  DOUBLE PRECISION;
   v_numKitChildQtyShipped  DOUBLE PRECISION;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_RCur REFCURSOR;
   v_l  INTEGER DEFAULT 1;
   v_lCount  INTEGER;
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_vcSerialLot  VARCHAR(100);
   v_vcTempDescription  VARCHAR(500);
   v_numTempQty  DOUBLE PRECISION;
BEGIN
   BEGIN
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppId;
      DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
      (
         ID INTEGER,
         numWarehouseItemID NUMERIC(18,0),
         vcSerialLot VARCHAR(100),
         numQty DOUBLE PRECISION
      );
      DROP TABLE IF EXISTS tt_TEMPKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITSUBITEMS
      (
         ID INTEGER,
         numOppChildItemID NUMERIC(18,0),
         numChildItemCode NUMERIC(18,0),
         bitChildIsKit BOOLEAN,
         numChildWarehouseItemID NUMERIC(18,0),
         numChildQty DOUBLE PRECISION,
         numChildQtyShipped DOUBLE PRECISION
      );
      DROP TABLE IF EXISTS tt_TEMPKITCHILDKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITCHILDKITSUBITEMS
      (
         ID INTEGER,
         numOppKitChildItemID NUMERIC(18,0),
         numKitChildItemCode NUMERIC(18,0),
         numKitChildWarehouseItemID NUMERIC(18,0),
         numKitChildQty DOUBLE PRECISION,
         numKitChildQtyShipped DOUBLE PRECISION
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TempFinalTable_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPFINALTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINALTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numItemCode NUMERIC(18,0),
         vcItemType CHAR(1),
         bitSerial BOOLEAN,
         bitLot BOOLEAN,
         bitAssembly BOOLEAN,
         bitKit BOOLEAN,
         numOppItemID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numQtyShipped DOUBLE PRECISION,
         numQty DOUBLE PRECISION,
         bitDropShip BOOLEAN
      );
      INSERT INTO tt_TEMPFINALTABLE(numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip)
      SELECT
      OpportunityBizDocItems.numItemCode,
		coalesce(Item.charItemType,''),
		coalesce(Item.bitSerialized,false),
		coalesce(Item.bitLotNo,false),
		coalesce(Item.bitAssembly,false),
		coalesce(Item.bitKitParent,false),
		coalesce(OpportunityItems.numoppitemtCode,0),
		coalesce(OpportunityItems.numWarehouseItmsID,0),
		coalesce(OpportunityItems.numQtyShipped,0),
		coalesce(OpportunityBizDocItems.numUnitHour,0),
		coalesce(OpportunityItems.bitDropShip,false)
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityBizDocItems
      ON
      OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
      INNER JOIN
      OpportunityItems
      ON
      OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocsId = v_numOppBizDocID;
      -- BEGIN TRANSACTION
select   COUNT(*) INTO v_COUNT FROM tt_TEMPFINALTABLE;
      WHILE v_i <= v_COUNT LOOP
         select   vcItemType, bitSerial, bitLot, bitAssembly, bitKit, bitDropShip, numOppItemID, numQty, numQtyShipped, numWarehouseItemID INTO v_vcItemType,v_bitSerial,v_bitLot,v_bitAssembly,v_bitKit,v_bitDropShip,
         v_numOppItemID,v_numQty,v_numQtyShipped,v_numWarehouseItemID FROM
         tt_TEMPFINALTABLE WHERE
         ID = v_i;
         IF v_numQty > v_numQtyShipped then
			
            RAISE NOTICE 'INVALID_SHIPPED_QTY';
         end if;

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
         IF (UPPER(v_vcItemType) = 'P' AND v_bitDropShip = false) then
			
            v_vcDescription := CONCAT('SO Qty Shipped Return (Qty:',v_numQty,' Shipped:',v_numQtyShipped,')');
            IF v_bitKit = true then
				
					-- CLEAR DATA OF PREVIOUS ITERATION
               DELETE FROM tt_TEMPKITSUBITEMS;

					-- GET KIT SUB ITEMS DETAIL
               INSERT INTO tt_TEMPKITSUBITEMS(ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped)
               SELECT
               ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						coalesce(numOppChildItemID,0),
						coalesce(I.numItemCode,0),
						coalesce(I.bitKitParent,false),
						coalesce(OKI.numWareHouseItemId,0),
						((coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numQty),
						coalesce(OKI.numQtyShipped,0)
               FROM
               OpportunityKitItems OKI
               JOIN
               Item I
               ON
               OKI.numChildItemID = I.numItemCode
               WHERE
               charitemtype = 'P'
               AND coalesce(numWareHouseItemId,0) > 0
               AND OKI.numOppId = v_numOppId
               AND OKI.numOppItemID = v_numOppItemID;
               v_j := 1;
               select   COUNT(*) INTO v_ChildCount FROM tt_TEMPKITSUBITEMS;

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
               WHILE v_j <= v_ChildCount LOOP
                  select   numOppChildItemID, numChildItemCode, bitChildIsKit, numChildWarehouseItemID, numChildQty, numChildQtyShipped INTO v_numOppChildItemID,v_numChildItemCode,v_bitChildIsKit,v_numChildWarehouseItemID,
                  v_numChildQty,v_numChildQtyShipped FROM
                  tt_TEMPKITSUBITEMS WHERE
                  ID = v_j;
                  IF v_numChildQty > v_numChildQtyShipped then
						
                     RAISE NOTICE 'INVALID_KIT_SUB_ITEM_SHIPPED_QTY';
                  end if;
                  IF v_bitChildIsKit = true then
						
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
                     IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppId = v_numOppId AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID) then
							
								-- CLEAR DATA OF PREVIOUS ITERATION
                        DELETE FROM tt_TEMPKITCHILDKITSUBITEMS;

								-- GET SUB KIT SUB ITEMS DETAIL
                        INSERT INTO tt_TEMPKITCHILDKITSUBITEMS(ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped)
                        SELECT
                        ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									coalesce(numOppKitChildItemID,0),
									coalesce(OKCI.numItemID,0),
									coalesce(OKCI.numWareHouseItemId,0),
									((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQty),
									coalesce(numQtyShipped,0)
                        FROM
                        OpportunityKitChildItems OKCI
                        JOIN
                        Item I
                        ON
                        OKCI.numItemID = I.numItemCode
                        WHERE
                        charitemtype = 'P'
                        AND coalesce(numWareHouseItemId,0) > 0
                        AND OKCI.numOppId = v_numOppId
                        AND OKCI.numOppItemID = v_numOppItemID
                        AND OKCI.numOppChildItemID = v_numOppChildItemID;
                        v_k := 1;
                        select   COUNT(*) INTO v_CountKitChildItems FROM tt_TEMPKITCHILDKITSUBITEMS;


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
                        WHILE v_k <= v_CountKitChildItems LOOP
                           select   numOppKitChildItemID, numKitChildItemCode, numKitChildWarehouseItemID, numKitChildQty, numKitChildQtyShipped INTO v_numOppKitChildItemID,v_numKitChildItemCode,v_numKitChildWarehouseItemID,
                           v_numKitChildQty,v_numKitChildQtyShipped FROM
                           tt_TEMPKITCHILDKITSUBITEMS WHERE
                           ID = v_k;
                           IF v_numKitChildQty > v_numKitChildQtyShipped then
									
                              RAISE NOTICE 'INVALID_KIT_SUB_ITEM_SHIPPED_QTY';
                           end if;
                           IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
															
										--PLACE QTY BACK ON ALLOCATION
                              UPDATE
                              WareHouseItems
                              SET
                              numAllocation = coalesce(numAllocation,0)+v_numKitChildQty,dtModified = LOCALTIMESTAMP
                              WHERE
                              numWareHouseItemID = v_numKitChildWarehouseItemID;
                           ELSE
										--PLACE QTY BACK ON ONHAND
                              UPDATE
                              WareHouseItems
                              SET
                              numOnHand = coalesce(numOnHand,0)+v_numKitChildQty,dtModified = LOCALTIMESTAMP
                              WHERE
                              numWareHouseItemID = v_numKitChildWarehouseItemID;
                           end if;

									--DECREASE QTY SHIPPED OF ORDER ITEM	
                           UPDATE
                           OpportunityKitChildItems
                           SET
                           numQtyShipped = coalesce(numQtyShipped,0) -coalesce(v_numKitChildQty,0)
                           WHERE
                           numOppKitChildItemID = v_numOppKitChildItemID
                           AND numOppChildItemID = v_numOppChildItemID
                           AND numOppId = v_numOppId
                           AND numOppItemID = v_numOppItemID;
                           v_vcKitChildDescription := CONCAT('SO CHILD KIT Qty Shipped Return (Qty:',v_numKitChildQty,' Shipped:',v_numKitChildQtyShipped,
                           ')');

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
                           PERFORM FROM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numKitChildWarehouseItemID,v_numReferenceID := v_numOppId,
                           v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcKitChildDescription,
                           v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                           SWV_RefCur := null);
                           v_k := v_k::bigint+1;
                        END LOOP;
                     end if;

							--DECREASE QTY SHIPPED OF ORDER ITEM	
                     UPDATE
                     OpportunityKitItems
                     SET
                     numQtyShipped = coalesce(numQtyShipped,0) -coalesce(v_numChildQty,0)
                     WHERE
                     numOppChildItemID = v_numOppChildItemID
                     AND numOppId = v_numOppId
                     AND numOppItemID = v_numOppItemID;
                     v_vcChildDescription := CONCAT('SO KIT Qty Shipped Return (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
                     ')');

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppId,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                  ELSE
                     IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
							
								--PLACE QTY BACK ON ALLOCATION
                        UPDATE
                        WareHouseItems
                        SET
                        numAllocation = coalesce(numAllocation,0)+v_numChildQty,dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numChildWarehouseItemID;
                     ELSE
								--PLACE QTY BACK ON ONHAND
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand = coalesce(numOnHand,0)+v_numChildQty,dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numChildWarehouseItemID;
                     end if;
							      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
                     UPDATE
                     OpportunityKitItems
                     SET
                     numQtyShipped = coalesce(numQtyShipped,0) -coalesce(v_numChildQty,0)
                     WHERE
                     numOppChildItemID = v_numOppChildItemID
                     AND numOppId = v_numOppId
                     AND numOppItemID = v_numOppItemID;
                     v_vcChildDescription := CONCAT('SO KIT Qty Shipped Return (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
                     ')');

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppId,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;
            ELSE
               IF v_bitSerial = true OR v_bitLot = true then
                  IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
						
                     UPDATE
                     WareHouseItems
                     SET
                     numAllocation = coalesce(numAllocation,0)+v_numQty
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  ELSE
							-- ADD QUANTITY BACK ON ONHAND
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand = coalesce(numOnHand,0)+v_numQty,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  end if;

						-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcDescription,
                  v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
                  DELETE FROM tt_TEMPSERIALLOT;
                  INSERT INTO tt_TEMPSERIALLOT(ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,coalesce(WID.vcSerialNo,'')
							,coalesce(OWSI.numQty,0)
                  FROM
                  OppWarehouseSerializedItem OWSI
                  INNER JOIN
                  WareHouseItmsDTL WID
                  ON
                  OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
                  INNER JOIN
                  WareHouseItems WI
                  ON
                  OWSI.numWarehouseItmsID = WI.numWareHouseItemID
                  WHERE
                  numOppID = v_numOppId
                  AND numOppItemID = v_numOppItemID
                  AND coalesce(numOppBizDocsId,0) = v_numOppBizDocID;
                  select   COUNT(*) INTO v_lCount FROM tt_TEMPSERIALLOT;
                  WHILE v_l <= v_lCount LOOP
                     select   numWarehouseItemID, vcSerialLot, numQty INTO v_numTempWarehouseItemID,v_vcSerialLot,v_numTempQty FROM
                     tt_TEMPSERIALLOT WHERE
                     ID = v_l;
                     IF v_numWarehouseItemID <> v_numTempWarehouseItemID then
							
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand = coalesce(numOnHand,0)+v_numTempQty
                        WHERE
                        numWareHouseItemID = v_numTempWarehouseItemID;
								 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
                        v_vcTempDescription := CONCAT('Serial/Lot(',v_vcSerialLot,') Shipped Return Qty(',v_numTempQty,')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppId,
                        v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcTempDescription,
                        v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                        SWV_RefCur := null);
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand =(CASE WHEN coalesce(numOnHand,0) >= v_numTempQty THEN(coalesce(numOnHand,0) -v_numTempQty) ELSE 0 END),numAllocation = coalesce(numAllocation,0) -(CASE WHEN coalesce(numOnHand,0) >= v_numTempQty THEN 0 ELSE(CASE WHEN coalesce(numAllocation,0) > coalesce(numOnHand,0) THEN coalesce(numOnHand,0) ELSE 0 END) END),numBackOrder = coalesce(numBackOrder,0)+(CASE WHEN coalesce(numOnHand,0) >= v_numTempQty THEN 0 ELSE(CASE WHEN coalesce(numAllocation,0) > coalesce(numOnHand,0) THEN coalesce(numOnHand,0) ELSE 0 END) END)
                        WHERE
                        numWareHouseItemID = v_numWarehouseItemID;
								 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
                        v_vcTempDescription := CONCAT('Serial/Lot(',v_vcSerialLot,') Shipped Return Qty(',v_numTempQty,')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
                        v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcTempDescription,
                        v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
                     end if;
                     v_l := v_l::bigint+1;
                  END LOOP;
                  UPDATE OppWarehouseSerializedItem SET numOppBizDocsId = NULL WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND numOppBizDocsId = v_numOppBizDocID;
               ELSE
                  IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
						
							--PLACE QTY BACK ON ALLOCATION
                     UPDATE
                     WareHouseItems
                     SET
                     numAllocation = coalesce(numAllocation,0)+v_numQty,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  ELSE
							-- ADD QUANTITY BACK ON ONHAND
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand = coalesce(numOnHand,0)+v_numQty,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  end if;
               end if;
            end if;
            IF NOT (v_bitSerial = true OR v_bitLot = true) then
				
					-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
               v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcDescription,
               v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
            end if;
         end if;

			--DECREASE QTY SHIPPED OF ORDER ITEM	
         UPDATE
         OpportunityItems
         SET
         numQtyShipped =(coalesce(numQtyShipped,0) -v_numQty)
         WHERE
         numoppitemtCode = v_numOppItemID
         AND numOppId = v_numOppId;
         v_i := v_i::bigint+1;
      END LOOP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;

