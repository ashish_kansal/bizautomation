DROP FUNCTION IF EXISTS fn_GetAttributes;

CREATE OR REPLACE FUNCTION fn_GetAttributes(v_numRecID NUMERIC,v_bitSerialized  BOOLEAN)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strAttribute TEXT;
   v_numCusFldID  NUMERIC(9,0);
   v_numCusFldValue  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_numCusFldID := 0;
   v_strAttribute := '';
   select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue from CFW_Fld_Values_Serialized_Items where RecId = v_numRecID and Fld_Value != '0' and Fld_Value != ''   order by Fld_ID LIMIT 1;
   while v_numCusFldID > 0 LOOP
      if SWF_IsNumeric(v_numCusFldValue) = true then
	
		--set @strAttribute=@strAttribute+@numCusFldValue
         select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute from  CFW_Fld_Master where Fld_id = v_numCusFldID AND Grp_id = 9;
         IF LENGTH(v_strAttribute) > 0 then
        
            select   coalesce(v_strAttribute,'') || ':' || REPLACE(vcData,'''','''''') || ',' INTO v_strAttribute from Listdetails where numListItemID = CAST(v_numCusFldValue AS NUMERIC);
         end if;
      else
         select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute from  CFW_Fld_Master where Fld_id = v_numCusFldID AND Grp_id = 9;
         IF LENGTH(v_strAttribute) > 0 then
        
            v_strAttribute := coalesce(v_strAttribute,'') || ':-,';
         end if;
      end if;
      select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue from CFW_Fld_Values_Serialized_Items where RecId = v_numRecID and Fld_Value != '0' and Fld_Value != '' and Fld_ID > v_numCusFldID   order by Fld_ID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numCusFldID := 0;
      end if;
   END LOOP;

   If LENGTH(v_strAttribute) > 0 then

      v_strAttribute := SUBSTR(v_strAttribute,1,LENGTH(v_strAttribute) -1);
   end if;

   return COALESCE(v_strAttribute,'');
END; $$;

