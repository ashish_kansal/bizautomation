-- Function definition script fn_GetProjectStageName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetProjectStageName(v_numProId NUMERIC,v_numStageDetailsId NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcStageName  VARCHAR(100);
BEGIN
   select   coalesce(vcStageName,'') INTO v_vcStageName from StagePercentageDetails where numProjectid = v_numProId
   and numStageDetailsId = v_numStageDetailsId;

   return coalesce(v_vcStageName,'-');
END; $$;

