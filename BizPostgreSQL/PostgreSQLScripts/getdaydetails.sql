-- Function definition script GetDaydetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDaydetails(v_numUserCntID NUMERIC(9,0),
	v_strDate DATE,
	v_numCategoryType NUMERIC(9,0),
	v_ClientTimeZoneOffset INTEGER)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Time  INTEGER DEFAULT 0;
   v_Expense  DOUBLE PRECISION DEFAULT 0;          
   v_Leave  VARCHAR(10);              
   v_rtDtl  VARCHAR(30);
BEGIN
   IF v_numCategoryType <> 0 then
	
      select   coalesce(SUM((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate))),
      0) INTO v_Time FROM
      timeandexpense WHERE
      (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::DATE  = v_strDate
      AND numCategory = 1
      AND numUserCntID = v_numUserCntID
      AND numtype = v_numCategoryType;
   ELSE
      select   coalesce(SUM((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate))),
      0) INTO v_Time FROM
      timeandexpense WHERE
      (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::DATE = v_strDate
      AND numCategory = 1
      AND numUserCntID = v_numUserCntID;
   end if;
              
   IF v_numCategoryType <> 0 then
	
      select   v_Expense + coalesce(ROUND(CAST(SUM(monAmount) AS NUMERIC),2),0) INTO v_Expense FROM
      timeandexpense WHERE
      v_strDate = dtFromDate::DATE
      AND numCategory = 2
      AND numUserCntID = v_numUserCntID
      AND numtype = v_numCategoryType;
   ELSE
      select   v_Expense + coalesce(round(CAST(sum(monAmount) AS NUMERIC),2),0) INTO v_Expense FROM
      timeandexpense WHERE
      v_strDate = dtFromDate::DATE
      AND numCategory = 2
      AND numUserCntID = v_numUserCntID;
   end if;
              
   IF v_numCategoryType <> 0 then
      select(CASE
      WHEN (dtFromDate::DATE = v_strDate AND bitFromFullDay = false) THEN  'HDL'
      WHEN (dtFromDate::DATE = v_strDate AND bitFromFullDay = true) THEN  'FDL'
      WHEN (dtToDate::DATE = v_strDate AND bittofullday = false) THEN  'HDL'
      WHEN (dtToDate::DATE = v_strDate AND bittofullday = true) THEN  'FDL'
      WHEN ((v_strDate BETWEEN dtFromDate::DATE AND dtToDate::DATE) AND dtFromDate::DATE <> v_strDate AND dtToDate::DATE <> v_strDate) THEN 'FDL'
      ELSE '-'
      END) INTO v_Leave FROM
      timeandexpense WHERE
      v_strDate BETWEEN dtFromDate::DATE AND dtToDate::DATE
      AND numCategory = 3
      AND numUserCntID = v_numUserCntID
      AND numtype = v_numCategoryType;
   ELSE
      select(CASE
      WHEN (dtFromDate::DATE = v_strDate and bitFromFullDay = false) THEN 'HDL'
      WHEN (dtFromDate::DATE = v_strDate and bitFromFullDay = true) THEN  'FDL'
      WHEN (dtToDate::DATE = v_strDate and bittofullday = false) THEN  'HDL'
      WHEN (dtToDate::DATE = v_strDate and bittofullday = true) THEN  'FDL'
      WHEN ((v_strDate between dtFromDate::DATE and dtToDate::DATE) and dtFromDate::DATE <> v_strDate and dtToDate::DATE <> v_strDate) THEN  'FDL'
      ELSE '-'
      END) INTO v_Leave FROM
      timeandexpense WHERE
      v_strDate BETWEEN dtFromDate::DATE AND dtToDate::DATE AND numCategory = 3 AND numUserCntID = v_numUserCntID;
   end if;              
            
   IF ((v_Time IS NULL OR v_Time = 0) AND (v_Expense IS NULL OR v_Expense = 0) AND v_Leave IS NULL) then
      v_rtDtl := '-';
   ELSEIF (v_Time IS NOT NULL OR v_Expense IS NOT NULL OR v_Leave IS NOT NULL)
   then
	
      IF v_Time IS NOT NULL AND v_Time <> 0 then
         v_rtDtl := SUBSTR(TO_CHAR('1900-01-01':: date+CAST(v_Time || 'minute' as interval),'hh24:mi:ss'),1,5);
      ELSE
         v_rtDtl := '';
      end if;
      IF v_Expense IS NOT NULL AND v_Expense <> 0 then
		
         IF v_rtDtl = '' then
            v_rtDtl := 'E';
         ELSE 
            v_rtDtl := coalesce(v_rtDtl,'') || ',E';
         end if;
      end if;
      IF v_Leave IS NOT NULL then
		
         IF v_rtDtl = '' then
            v_rtDtl := 'L';
         ELSE
            v_rtDtl := coalesce(v_rtDtl,'') || ',L';
         end if;
      end if;
   end if;              
            
   IF v_rtDtl = '' then 
      v_rtDtl := '0';
   end if;
   IF v_rtDtl = '-' then 
      v_rtDtl := ' ';
   end if;              
   RETURN v_rtDtl;
END; $$;

