-- Function definition script OppClosedPipeline for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION OppClosedPipeline(v_OppStatus SMALLINT,v_numDomainId NUMERIC(9,0),v_numCreatedby NUMERIC,v_month INTEGER,v_year INTEGER)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retVal  NUMERIC;
BEGIN
   v_retVal := 0; 
   select   sum(monPAmount) INTO v_retVal from OpportunityMaster where tintopptype = 1 and tintoppstatus = v_OppStatus and
   OpportunityMaster.numDomainId = v_numDomainId and numrecowner = v_numCreatedby
   and EXTRACT(month FROM bintCreatedDate) = v_month and EXTRACT(year FROM bintCreatedDate) = v_year;
   return v_retVal;
END; $$;

