-- Function definition script GetDFHistoricalSalesOppData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDFHistoricalSalesOppData(v_numDFID NUMERIC(18,0),
      v_numDomainID NUMERIC(9,0),
	  v_dtFromDate TIMESTAMP,
	  v_dtToDate TIMESTAMP)
RETURNS TABLE 
(
   numItemCode NUMERIC(18,0), 
   numWarehouseItmsID NUMERIC(18,0),
   numQtySold INTEGER
) LANGUAGE plpgsql
   AS $$
BEGIN


		--GET Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0) Created Between Dates
   DROP TABLE IF EXISTS tt_GETDFHISTORICALSALESOPPDATA_TEMP;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETDFHISTORICALSALESOPPDATA_TEMP
   (
      numItemCode NUMERIC(18,0),
      numWarehouseItmsID NUMERIC(18,0),
      numQtySold INTEGER
   );
   DROP TABLE IF EXISTS tt_TEMPOPP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPP
   (
      numOppID NUMERIC(18,0),
      numPercentageComplete NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseItmsID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      charItemType CHAR(1),
      bitKitParent BOOLEAN,
      bitAssembly BOOLEAN
   );
   DROP TABLE IF EXISTS tt_ITEMTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_ITEMTABLE
   (
      numItemCode NUMERIC(18,0),
      numWarehouseItmsID NUMERIC(18,0),
      numQtySold DOUBLE PRECISION
   );
   INSERT INTO
   tt_TEMPOPP
   SELECT
   OpportunityMaster.numOppId,
			(coalesce((SELECT CAST(coalesce(vcData,'0') AS DECIMAL) FROM Listdetails WHERE numListItemID = OpportunityMaster.numPercentageComplete),0)/100),
			Item.numItemCode,
			OpportunityItems.numWarehouseItmsID,
			OpportunityItems.numUnitHour,
			Item.charItemType,
			Item.bitKitParent,
			Item.bitAssembly
   FROM
   OpportunityMaster
   LEFT JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   LEFT JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID AND
   Item.charItemType = 'P' AND
   CAST(OpportunityMaster.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE) AND
   OpportunityMaster.tintopptype = 1 AND
   OpportunityMaster.tintoppstatus = 0 AND
   coalesce(OpportunityMaster.numPercentageComplete,0) > 0 AND
			((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WareHouseItems.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID));
				
		--Get total quantity of each inventory or assembly item sold in Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0)
   INSERT INTO
   tt_ITEMTABLE
   SELECT
   TEMP.numItemCode,
			TEMP.numWarehouseItmsID,
			SUM(TEMP.QuantitySold) AS numQtySold
   FROM(SELECT
      t1.numOppId,
				t1.numItemCode,
				t1.numWarehouseItmsID,
				CEIL(SUM(t1.numUnitHour)*coalesce(t1.numPercentageComplete,0)) AS QuantitySold
      FROM
      tt_TEMPOPP AS t1
      WHERE
				(t1.bitKitParent = false OR (t1.bitKitParent = true AND t1.bitAssembly = true))
      GROUP BY
      t1.numOppId,t1.numItemCode,t1.numWarehouseItmsID,t1.numPercentageComplete) AS TEMP
   GROUP BY
   TEMP.numItemCode,TEMP.numWarehouseItmsID;

   INSERT INTO
   tt_GETDFHISTORICALSALESOPPDATA_TEMP
   SELECT
   T.numItemCode,
			T.numWarehouseItmsID,
			SUM(T.numQtySold)
   FROM
   tt_ITEMTABLE T
   GROUP BY
   T.numItemCode,T.numWarehouseItmsID;

		RETURN QUERY (SELECT * FROM tt_GETDFHISTORICALSALESOPPDATA_TEMP);
END; $$;

