CREATE OR REPLACE FUNCTION Usp_DeleteTrackingVisitorsHDR(v_numTrackingID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	DELETE FROM TrackingVisitorsHDR
    WHERE numDomainID = v_numDomainID
    AND numTrackingID = v_numTrackingID;

   RETURN;
END; $$;


