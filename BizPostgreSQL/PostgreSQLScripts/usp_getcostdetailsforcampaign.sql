-- Stored procedure definition script Usp_GetCostDetailsForCampaign for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetCostDetailsForCampaign(v_numCampaignID NUMERIC(18,0),
      v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT(SELECT    vcAccountName
      FROM      Chart_Of_Accounts
      WHERE     numAccountId = BD.numExpenseAccountID) AS vcAccountName,
                cast(BH.numBillID as VARCHAR(255)),
                cast(BD.numClassID as VARCHAR(255)),
                cast(LD.vcData as VARCHAR(255)) AS vcClassName,
                cast(BD.numCampaignID as VARCHAR(255)),
                cast(BD.vcDescription as VARCHAR(255)),
                cast(BD.monAmount as VARCHAR(255))
   FROM    BillDetails BD
   JOIN BillHeader BH ON BD.numBillID = BH.numBillID
   LEFT JOIN Listdetails LD ON BD.numClassID = LD.numListItemID
   AND numListID = 381
   WHERE   BH.numDomainId = v_numDomainID
   AND BD.numCampaignID = v_numCampaignID;
END; $$;

--Created by Anoop Jayaraj












