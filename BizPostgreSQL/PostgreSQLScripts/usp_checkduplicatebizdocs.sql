-- Stored procedure definition script USP_CheckDuplicateBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckDuplicateBizDocs(v_numDomainId NUMERIC(18,0) ,
 v_numBizDocId NUMERIC(18,0),
 v_tintOppType INTEGER,
 v_numBizDocTemplateID NUMERIC(18,0),
 v_numOppId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   open SWV_RefCur for SELECT CAST('col1' AS CHAR(4)) FROM OpportunityBizDocs as od INNER JOIN OpportunityMaster o on od.numoppid = o.numOppId
   WHERE o.numDomainId = v_numDomainId AND
   od.numBizDocId = v_numBizDocId AND
   o.tintopptype = v_tintOppType AND
   od.numBizDocTempID = v_numBizDocTemplateID
   and o.numOppId = v_numOppId;
END; $$;


	












