-- Function definition script GetNthRecurringDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
/*
Returns Nth occurance date for given recurring template
@startdate = date from which recurring is going to start 
@intRecurrenceNumber = no of recurrance from start date
*/
-- SELECT [dbo].[GetNthRecurringDate](GETDATE(),2,10)
CREATE OR REPLACE FUNCTION GetNthRecurringDate(v_StartDate TIMESTAMP,
                                              v_intRecurrenceNumber INTEGER,
                                              v_numRecurringID NUMERIC)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtRetValue  TIMESTAMP; 
 	
   v_chrIntervalType  CHAR(1);                              
   v_tintIntervalDays  SMALLINT;                              
   v_tintMonthlyType  SMALLINT;                              
   v_tintFirstDet  SMALLINT;                              
   v_tintWeekDays  SMALLINT;                              
   v_tintMonths  SMALLINT;                              
   v_dtStartDate  TIMESTAMP;                              
   v_dtEndDate  TIMESTAMP;          
   v_numNoTransactionsRecurring  NUMERIC(9,0);                              
   v_bitEndType  BOOLEAN;             
   v_bitEndTransactionType  BOOLEAN;     
   v_Day  SMALLINT;                          
   v_Month  SMALLINT;
   v_Year  INTEGER;
BEGIN
   select   chrIntervalType, tintIntervalDays, tintMonthlyType, tintFirstDet, tintWeekDays, tintMonths, dtStartDate, dtEndDate, bitEndType, bitEndTransactionType INTO v_chrIntervalType,v_tintIntervalDays,v_tintMonthlyType,v_tintFirstDet,
   v_tintWeekDays,v_tintMonths,v_dtStartDate,v_dtEndDate,v_bitEndType,v_bitEndTransactionType FROM    RecurringTemplate WHERE   numRecurringId = v_numRecurringID;
 	
   IF v_chrIntervalType = 'D' then
 	
      v_dtRetValue := v_StartDate+CAST(v_tintIntervalDays*v_intRecurrenceNumber::bigint || 'day' as interval);
   end if;
   IF v_chrIntervalType = 'M' then
 	
      v_dtRetValue :=  v_StartDate+CAST(v_tintIntervalDays*v_intRecurrenceNumber::bigint || 'month' as interval);
      v_Day := EXTRACT(DAY FROM v_dtRetValue);
      v_Month := EXTRACT(MONTH FROM v_dtRetValue);
      v_Year := EXTRACT(YEAR FROM v_dtRetValue);
      IF v_tintWeekDays > 0 then
 		
 		--Every Nth Weekday of month
--			SET @dtRetValue = Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)
----			IF @tintFirstDet = 5  
----			SET @tintFirstDet = -1 -- a positive number means from the beginning of month and a negative number means from the end of month
			
         v_dtRetValue := fn_GetNthWeekdayOfMonth(v_dtRetValue,v_tintWeekDays,CASE v_tintFirstDet WHEN 5 THEN -1 ELSE v_tintFirstDet END::SMALLINT);
      ELSE
			--Every Nth Day of Month
	 		
         v_Day := EXTRACT(DAY FROM v_dtRetValue);
         v_Month := EXTRACT(MONTH FROM v_dtRetValue);
         v_Year := EXTRACT(YEAR FROM v_dtRetValue);
         v_dtRetValue := '1900-01-01':: date + make_interval(days => v_tintFirstDet-1) + CAST(((v_Year::bigint -1900)*12)+v_Month -1 || 'month' as interval);
      end if;
   end if; 
 	
   IF v_chrIntervalType = 'Y' then
 	
      v_dtStartDate := v_dtStartDate+CAST(v_intRecurrenceNumber || 'year' as interval);
      v_Year := EXTRACT(YEAR FROM v_dtStartDate);
      v_dtRetValue := '1900-01-01':: date + make_interval(days => v_tintFirstDet-1) + CAST(((v_Year::bigint -1900)*12)+v_tintMonths -1 || 'month' as interval);
   end if;
 	
 	
   RETURN v_dtRetValue;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[GetOppAmount]    Script Date: 07/26/2008 18:13:05 ******/

