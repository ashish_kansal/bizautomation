-- Stored procedure definition script usp_DeleteOpportunityStageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteOpportunityStageDetails(v_numOppId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OpportunitySubStageDetails WHERE numOppId = v_numOppId;
   DELETE FROM OpptStageComments WHERE numoppid = v_numOppId;
   delete from stageOpportunity where numOppID = v_numOppId;
   DELETE FROM OpportunityStageDetails WHERE numoppid = v_numOppId;
   RETURN;
END; $$;


