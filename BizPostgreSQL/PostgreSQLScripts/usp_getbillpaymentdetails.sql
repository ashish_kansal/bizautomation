DROP FUNCTION IF EXISTS USP_GetBillPaymentDetails;

CREATE OR REPLACE FUNCTION USP_GetBillPaymentDetails
(
	v_numBillPaymentID NUMERIC(18,0) ,
    v_numDomainID NUMERIC ,
    v_ClientTimeZoneOffset INTEGER,
    v_numDivisionID NUMERIC DEFAULT 0,
    v_numCurrencyID NUMERIC DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0,
	v_SortColumn VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_strCondition TEXT DEFAULT '',
	INOUT SWV_RefCur refcursor DEFAULT NULL,
	INOUT SWV_RefCur2 refcursor DEFAULT NULL)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_numBaseCurrencyID  NUMERIC;
	v_BaseCurrencySymbol  VARCHAR(3);
	v_firstRec  INTEGER;
	v_lastRec  INTEGER;
	v_strSQLTable1  TEXT;
	v_strSQLTable2  TEXT;
	v_strFinal  TEXT;
BEGIN
	select 
		coalesce(numCurrencyID,0) INTO v_numBaseCurrencyID 
	FROM 
		Domain 
	WHERE 
		numDomainId = v_numDomainID;
   
	select 
		coalesce(varCurrSymbol,'') INTO v_BaseCurrencySymbol 
	FROM 
		Currency 
	WHERE 
		numCurrencyID = v_numBaseCurrencyID;
    
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
				  
	IF v_numBillPaymentID > 0 then
		select numDivisionId INTO v_numDivisionID FROM BillPaymentHeader BPH WHERE BPH.numBillPaymentID = v_numBillPaymentID;
	end if;

	IF LENGTH(v_strCondition) = 0 then
		v_strCondition := '1=1';
	end if;

	v_strSQLTable1 := CONCAT(' SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
						,coalesce(BPH.monPaymentAmount,0) AS monPaymentAmount,coalesce(BPH.monAppliedAmount,0) AS monAppliedAmount,
					 coalesce(CH.numCheckHeaderID, 0) numCheckHeaderID,
					 coalesce(CH.numCheckNo,0) AS numCheckNo,coalesce(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
					 BPH.numCurrencyID,coalesce(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
					,coalesce(C.varCurrSymbol,'''') varCurrSymbol
					,''',coalesce(v_BaseCurrencySymbol,''),''' AS BaseCurrencySymbol					
					,coalesce(monRefundAmount,0) AS monRefundAmount,coalesce(BPH.numAccountClass,0) AS numAccountClass
					  FROM  BillPaymentHeader BPH
							LEFT JOIN CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
							LEFT JOIN Currency C ON C.numCurrencyID = BPH.numCurrencyID
							WHERE BPH.numBillPaymentID =',coalesce(v_numBillPaymentID,0),';');
		
	IF LENGTH(COALESCE(v_SortColumn,'')) = 0 then
		v_SortColumn := 'dtDueDate';
	end if; 
	IF LENGTH(COALESCE(v_columnSortOrder,'')) = 0 then
		v_columnSortOrder := 'ASC';
	END IF;

	v_strSQLTable2 := ' (
						SELECT  Case When coalesce(BH.bitLandedCost,false)=true then  coalesce(BH.numOppId,0) else OBD.numOppId end AS numoppid,
							  BPH.numDivisionID,BPD.numOppBizDocsID,BPD.numBillID,
							  CASE WHEN(BPD.numBillID>0) THEN  Case When coalesce(BH.bitLandedCost,false)=true THEN ''Laned Cost-'' ELSE ''Bill-'' END || COALESCE(BH.numBillID,0)
							  else OBD.vcbizdocid END AS Name,
							  CASE BPD.tintBillType WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
							  CASE BPD.tintBillType WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.monAmount) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.monAmount END monAmountDue,
							  CASE BPD.tintBillType WHEN 1 THEN '''' WHEN 2 THEN BH.vcReference END Reference,
							  CASE BPD.tintBillType WHEN 1 THEN fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
							  BPD.tintBillType,
							  CASE BPD.tintBillType
								  WHEN 1
								  THEN CASE coalesce(OM.bitBillingTerms, false)
										 WHEN true
										 THEN FormatedDateFromDate(OBD.dtFromDate + (coalesce((SELECT CAST(numNetDueInDays AS INTEGER) FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) || '' days'')::interval,' || v_numDomainID || ')
										 WHEN false
										 THEN FormatedDateFromDate(OBD.dtFromDate, ' || v_numDomainID || ')
									   END
								  WHEN 2
								  THEN FormatedDateFromDate(BH.dtDueDate, ' || v_numDomainID || ') END AS DueDate,
								  BPD.monAmount AS monAmountPaid,true bitIsPaid
								  ,coalesce(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
								  ,BH.dtDueDate 
						FROM    BillPaymentHeader BPH
								INNER JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
								LEFT JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
								LEFT JOIN OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								LEFT JOIN BillHeader BH ON BH.numBillID = BPD.numBillID
								LEFT JOIN CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
																AND CH.tintReferenceType = 8
						                                                 
						WHERE   BPH.numBillPaymentID = ' || COALESCE(v_numBillPaymentID,0) || '
						AND BPH.numDomainID = ' || v_numDomainID || '
						AND (OM.numCurrencyID = ' ||  COALESCE(v_numCurrencyID,0) || ' OR ' || COALESCE(v_numCurrencyID,0) || ' = 0 OR coalesce(BPD.numOppBizDocsID,0) =0)
						AND (coalesce(BPH.numAccountClass,0)=' || COALESCE(v_numAccountClass,0) || ' OR ' || COALESCE(v_numAccountClass,0) || '=0)
	            
						UNION
				
						SELECT  opp.numoppid AS numoppid ,
								opp.numdivisionid AS numDivisionID ,
								OBD.numOppBizDocsId AS numOppBizDocsId ,
								0 AS numBillID ,
								OBD.vcbizdocid AS name ,
								OBD.monDealAmount AS monOriginalAmount ,
								( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
			
								OBD.vcRefOrderNo AS reference ,
			
								c.vccompanyname AS vccompanyname ,
								1 AS tintBillType , 
								CASE coalesce(Opp.bitBillingTerms, false)
								  WHEN true
								  THEN FormatedDateFromDate(OBD.dtFromDate + (coalesce((SELECT CAST(numNetDueInDays AS INTEGER) FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) || '' days'')::interval,' || v_numDomainID || ')
								  WHEN false
								  THEN FormatedDateFromDate(OBD.dtFromDate, ' || v_numDomainID || ')
								END AS DueDate ,
								0 AS monAmountPaid,
								false bitIsPaid
								,coalesce(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
								,OBD.dtFromDate AS dtDueDate
						FROM    OpportunityBizDocs OBD
								INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
								INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
								INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
																 AND ADC.numDivisionId = Div.numDivisionID
								INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
						WHERE   Opp.numDomainId = ' || v_numDomainID || '
								AND OBD.bitAuthoritativeBizDocs = 1
								AND Opp.tintOppType = 2
								AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
								AND (Opp.numDivisionId = ' || COALESCE(v_numDivisionID,0) || ' OR coalesce(' || COALESCE(v_numDivisionID,0) || ',0)=0)
								AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = ' || COALESCE(v_numBillPaymentID,0) || ')
								AND (Opp.numCurrencyID = ' || COALESCE(v_numCurrencyID,0) || ' OR ' || COALESCE(v_numCurrencyID,0) || ' = 0)
								AND (coalesce(Opp.numAccountClass,0)=' || COALESCE(v_numAccountClass,0) || ' OR ' || COALESCE(v_numAccountClass,0) || '=0)
						 UNION       
						
						SELECT  Case When coalesce(BH.bitLandedCost,false)=true then  coalesce(BH.numOppId,0) else 0 end AS numoppid ,
								numDivisionID AS numDivisionID ,
								0 AS numOppBizDocsId ,
								BH.numBillID ,
								Case When coalesce(BH.bitLandedCost,false)=true THEN ''Laned Cost-'' ELSE ''Bill-'' END || COALESCE(BH.numBillID,0) AS name ,
								BH.monAmountDue AS monOriginalAmount ,
								(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
			
								BH.vcReference AS reference ,
			
								fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
								2 AS tintBillType ,
								FormatedDateFromDate(BH.dtDueDate,' || v_numDomainID || ') AS DueDate ,
								0 AS monAmountPaid,
								false bitIsPaid
								,coalesce(BH.fltExchangeRate,1) fltExchangeRateOfOrder
								,BH.dtDueDate
						FROM    BillHeader BH
						WHERE   BH.numDomainId = ' || v_numDomainID || '
								AND BH.bitIsPaid = false
								AND (BH.numDivisionId = ' || COALESCE(v_numDivisionID,0) || ' OR coalesce(' || COALESCE(v_numDivisionID,0) || ',0)=0) 
								AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = ' || SUBSTR(CAST(v_numBillPaymentID AS VARCHAR(10)),1,10) || ')      
								AND (coalesce(BH.numCurrencyID,' || COALESCE(v_numBaseCurrencyID,0) || ') = ' || COALESCE(v_numCurrencyID,0) || ' OR ' || COALESCE(v_numCurrencyID,0) || ' = 0)
								AND (coalesce((SELECT numClassID FROM BillDetails BD WHERE BD.numBillID=BH.numBillID LIMIT 1),0)=' || COALESCE(v_numAccountClass,0) || ' OR ' || COALESCE(v_numAccountClass,0) || '=0)
					) BillPayment  WHERE ' || CAST(v_strCondition AS TEXT); 

	RAISE NOTICE '%',v_strSQLTable1;
	
	OPEN SWV_RefCur FOR EXECUTE v_strSQLTable1;
	
	v_strFinal := CONCAT('DROP TABLE IF EXISTS tt_BillPayment CASCADE; CREATE TEMPORARY TABLE tt_BillPayment AS SELECT Row_number() OVER ( ORDER BY ',v_SortColumn,' ',v_columnSortOrder,') AS row,* FROM ',v_strSQLTable2,';');   

	EXECUTE v_strFinal;
	
	SELECT COUNT(*) INTO v_TotRecs FROM tt_BillPayment;

	OPEN SWV_RefCur2 FOR SELECT * FROM tt_BillPayment  WHERE row > v_firstRec and row < v_lastRec;

	RETURN;
END; $$;




