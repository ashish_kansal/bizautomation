-- Function definition script GetListIemName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetListIemName(v_vcItemID NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(100);
BEGIN
   v_listName := '';
   if (v_vcItemID is not null and v_vcItemID <> 0) then
    
      select   coalesce(vcData,'-') INTO v_listName from Listdetails where numListItemID = v_vcItemID;
   else 
      v_listName := '-';
   end if;


   return v_listName;
END; $$;

