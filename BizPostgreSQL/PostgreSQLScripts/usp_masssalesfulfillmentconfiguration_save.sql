DROP FUNCTION IF EXISTS USP_MassSalesFulfillmentConfiguration_Save;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentConfiguration_Save(v_numDomainID NUMERIC(18,0)
	,v_bitGroupByOrderForPick BOOLEAN
	,v_bitGroupByOrderForShip BOOLEAN
	,v_bitGroupByOrderForInvoice BOOLEAN
	,v_bitGroupByOrderForPay BOOLEAN
	,v_bitGroupByOrderForClose BOOLEAN
	,v_tintInvoicingType SMALLINT
	,v_tintScanValue SMALLINT
	,v_tintPackingMode SMALLINT
	,v_tintPendingCloseFilter SMALLINT
	,v_bitGeneratePickListByOrder BOOLEAN
	,v_numOrderStatusPicked NUMERIC(18,0)
	,v_numOrderStatusPacked1 NUMERIC(18,0)
	,v_numOrderStatusPacked2 NUMERIC(18,0)
	,v_numOrderStatusPacked3 NUMERIC(18,0)
	,v_numOrderStatusPacked4 NUMERIC(18,0)
	,v_numOrderStatusInvoiced NUMERIC(18,0)
	,v_numOrderStatusPaid NUMERIC(18,0)
	,v_numOrderStatusClosed NUMERIC(18,0)
	,v_bitPickWithoutInventoryCheck BOOLEAN
	,v_bitEnablePickListMapping BOOLEAN
	,v_bitEnableFulfillmentBizDocMapping BOOLEAN
	,v_bitAutoWarehouseSelection BOOLEAN
	,v_bitBOOrderStatus BOOLEAN
	,v_tintPackShipButtons SMALLINT
	,v_bitAutoGeneratePackingSlip BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) then
	
      UPDATE
      MassSalesFulfillmentConfiguration
      SET
      bitGroupByOrderForPick = v_bitGroupByOrderForPick,bitGroupByOrderForShip = v_bitGroupByOrderForShip,
      bitGroupByOrderForInvoice = v_bitGroupByOrderForInvoice,
      bitGroupByOrderForPay = v_bitGroupByOrderForPay,bitGroupByOrderForClose = v_bitGroupByOrderForClose,
      tintInvoicingType = v_tintInvoicingType,
      tintScanValue = v_tintScanValue,tintPackingMode = v_tintPackingMode,tintPendingCloseFilter = v_tintPendingCloseFilter,
      bitGeneratePickListByOrder = v_bitGeneratePickListByOrder,
      numOrderStatusPicked = v_numOrderStatusPicked,
      numOrderStatusPacked1 = v_numOrderStatusPacked1,numOrderStatusPacked2 = v_numOrderStatusPacked2,
      numOrderStatusPacked3 = v_numOrderStatusPacked3,
      numOrderStatusPacked4 = v_numOrderStatusPacked4,numOrderStatusInvoiced = v_numOrderStatusInvoiced,
      numOrderStatusPaid = v_numOrderStatusPaid,numOrderStatusClosed = v_numOrderStatusClosed,
      bitPickWithoutInventoryCheck = v_bitPickWithoutInventoryCheck,
      bitEnablePickListMapping = v_bitEnablePickListMapping,
      bitEnableFulfillmentBizDocMapping = v_bitEnableFulfillmentBizDocMapping,
	  bitAutoWarehouseSelection = v_bitAutoWarehouseSelection,
	  bitBOOrderStatus = v_bitBOOrderStatus,
	  tintPackShipButtons=v_tintPackShipButtons,
	  bitAutoGeneratePackingSlip = v_bitAutoGeneratePackingSlip
      WHERE
      numDomainID = v_numDomainID;
   ELSE
      INSERT INTO MassSalesFulfillmentConfiguration(numDomainID
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
			,bitGeneratePickListByOrder
			,numOrderStatusPicked
			,numOrderStatusPacked1
			,numOrderStatusPacked2
			,numOrderStatusPacked3
			,numOrderStatusPacked4
			,numOrderStatusInvoiced
			,numOrderStatusPaid
			,numOrderStatusClosed
			,bitPickWithoutInventoryCheck
			,bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping
			,bitAutoWarehouseSelection
			,bitBOOrderStatus
			,tintPackShipButtons
			,bitAutoGeneratePackingSlip)
		VALUES(v_numDomainID
			,v_bitGroupByOrderForPick
			,v_bitGroupByOrderForShip
			,v_bitGroupByOrderForInvoice
			,v_bitGroupByOrderForPay
			,v_bitGroupByOrderForClose
			,v_tintInvoicingType
			,v_tintScanValue
			,v_tintPackingMode
			,v_tintPendingCloseFilter
			,v_bitGeneratePickListByOrder
			,v_numOrderStatusPicked
			,v_numOrderStatusPacked1
			,v_numOrderStatusPacked2
			,v_numOrderStatusPacked3
			,v_numOrderStatusPacked4
			,v_numOrderStatusInvoiced
			,v_numOrderStatusPaid
			,v_numOrderStatusClosed
			,v_bitPickWithoutInventoryCheck
			,v_bitEnablePickListMapping
			,v_bitEnableFulfillmentBizDocMapping
			,v_bitAutoWarehouseSelection
			,v_bitBOOrderStatus
			,v_tintPackShipButtons
			,v_bitAutoGeneratePackingSlip);
   end if;
   RETURN;
END; $$;


