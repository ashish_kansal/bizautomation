-- Stored procedure definition script usp_GetItemDetailsFor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetItemDetailsFor(v_numUserCntID NUMERIC(9,0) DEFAULT 0,        
 v_numDomainID NUMERIC(9,0) DEFAULT 0,      
v_numItemCode NUMERIC(9,0) DEFAULT NULL,
v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if   v_numUserCntID > 0 then

      open SWV_RefCur for
      SELECT
      intPEstimatedCloseDate,
	    numUnitHour,
	  tintoppstatus,
	  bintAccountClosingDate,
	  (select  numStagePercentage  from OpportunityStageDetails  where OpportunityStageDetails.numoppid = mst.numOppId and numStagePercentage != 100 and bitstagecompleted = true order by numStagePercentage,numOppStageId desc LIMIT 1) as Percentage
      FROM
      OpportunityMaster  mst
      join OpportunityItems item
      on item.numOppId = mst.numOppId
      WHERE tintoppstatus <> 2 and  mst.numrecowner = v_numUserCntID
      and mst.numDomainId = v_numDomainID
      and numItemCode = v_numItemCode;
   end if;     

   if   v_numDivisionID > 0 then

      open SWV_RefCur for
      SELECT
      intPEstimatedCloseDate,
	    numUnitHour,
	  tintoppstatus,
	  bintAccountClosingDate,
	  (select  numStagePercentage  from OpportunityStageDetails  where OpportunityStageDetails.numoppid = mst.numOppId and numStagePercentage != 100 and bitstagecompleted = true order by numStagePercentage,numOppStageId desc LIMIT 1) as Percentage
      FROM
      OpportunityMaster  mst
      join OpportunityItems item
      on item.numOppId = mst.numOppId
      WHERE tintoppstatus <> 2 and  mst.numrecowner =(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID)
      and mst.numDomainId = v_numDomainID
      and numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$;


