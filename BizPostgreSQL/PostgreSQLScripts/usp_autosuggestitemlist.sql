CREATE OR REPLACE FUNCTION USP_AutoSuggestItemList(v_numDomainID NUMERIC(9,0),
v_numSiteId NUMERIC(9,0),
v_vcSubChar VARCHAR(400),
INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSqlQuery  TEXT DEFAULT '';
BEGIN
	open SWV_RefCur for 
	SELECT distinct 
		I.numItemCode AS "numItemCode"
		,I.vcItemName AS "vcItemName"
		,coalesce(CASE WHEN I.charItemType = 'P'
						   THEN coalesce(fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)),0)*W.monWListPrice
						   ELSE coalesce(fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)),0)*I.monListPrice
						   END,0) AS "monListPrice"
		,I.vcModelID AS "vcModelID"
		,I.vcSKU AS "vcSKU"
		,IM.vcPathForImage AS "vcPathForImage"
		,C.vcCategoryName AS "vcCategoryName"
   FROM
   Item AS I
   LEFT JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
   LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitDefault = true AND IM.bitIsImage = true
   LEFT JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
   LEFT JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
   LEFT JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID
   LEFT JOIN (SELECT numDomainID,numItemID,numWareHouseItemID,monWListPrice FROM WareHouseItems LIMIT 1) AS W ON W.numDomainID = v_numDomainID AND W.numItemID = I.numItemCode 
   WHERE I.numDomainID = v_numDomainID  AND E.numSiteId = v_numSiteId
   AND (I.numItemCode::VARCHAR ilike '%' || coalesce(v_vcSubChar,'') || '%' OR I.numItemCode::VARCHAR ilike '%' || coalesce(v_vcSubChar,'') || '%'
   OR I.vcItemName ilike '%' || coalesce(v_vcSubChar,'') || '%' OR I.vcModelID ilike '%' || coalesce(v_vcSubChar,'') || '%' OR I.vcSKU ilike '%' || coalesce(v_vcSubChar,'') || '%');
   RETURN;
END; $$;



