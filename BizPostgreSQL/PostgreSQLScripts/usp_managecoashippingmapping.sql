-- Stored procedure definition script USP_ManageCOAShippingMapping for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCOAShippingMapping(INOUT v_numShippingMappingID NUMERIC(9,0) DEFAULT 0 ,
	v_numShippingMethodID NUMERIC(9,0) DEFAULT NULL,
	v_numIncomeAccountID NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_numShippingMappingID = 0 then 
      INSERT INTO COAShippingMapping(numShippingMethodID,
		numIncomeAccountID,
		numDomainID)
	VALUES(v_numShippingMethodID,
		v_numIncomeAccountID,
		v_numDomainID);
	
      v_numShippingMappingID := CURRVAL('COAShippingMapping_seq');
   ELSE
      UPDATE COAShippingMapping SET
      numShippingMethodID = v_numShippingMethodID,numIncomeAccountID = v_numIncomeAccountID,
      numDomainID = v_numDomainID
      WHERE numShippingMappingID = v_numShippingMappingID
      AND numDomainID = v_numDomainID;
   end if;

   RETURN;
END; $$;


