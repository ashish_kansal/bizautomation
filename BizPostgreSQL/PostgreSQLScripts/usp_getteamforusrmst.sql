-- Stored procedure definition script USP_GetTeamForUsrMst for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTeamForUsrMst(v_numUserCntID NUMERIC(9,0),    
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select distinct numlistitemid,vcdata from UserTeams
   join Listdetails
   on numlistitemid = numteam
   where numUserCntID = v_numUserCntID and numListID = 35
   and UserTeams.numDomainID = v_numDomainID;
END; $$;












