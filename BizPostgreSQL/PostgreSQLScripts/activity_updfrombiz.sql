CREATE OR REPLACE FUNCTION Activity_UpdFromBiz
(
	v_DataKey INTEGER,		
	v_Priority NUMERIC(18,0),	
	v_Activity NUMERIC(18,0),		
	v_FollowUpStatus	NUMERIC(18,0),	
	v_Comments	TEXT,
	v_numContactID NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	v_numCompanyId  NUMERIC(9,0);          
	v_numDivisionId  NUMERIC(9,0);  
	v_numDomainID  NUMERIC(9,0);
BEGIN
	SELECT numDivisionId INTO v_numDivisionId FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
	SELECT numCompanyID, numDomainID INTO v_numCompanyId,v_numDomainID FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;

	UPDATE
		Activity
	SET
		Priority = v_Priority,Activity = v_Activity,FollowUpStatus = v_FollowUpStatus,
		Comments = v_Comments
	WHERE
		Activity.ActivityID = v_DataKey;

	UPDATE
		DivisionMaster
	SET
		numFollowUpStatus = v_FollowUpStatus
	WHERE 
		numDivisionID = v_numDivisionId
		AND numCompanyID = v_numCompanyId;
                
	INSERT INTO FollowUpHistory
	(
		numFollowUpstatus,
		numDivisionID,
		bintAddedDate,
		numDomainID
	)
	VALUES
	( 
		v_FollowUpStatus,
		v_numDivisionId,
		TIMEZONE('UTC',now()),
		v_numDomainID
	);

	RETURN;
END; $$;






