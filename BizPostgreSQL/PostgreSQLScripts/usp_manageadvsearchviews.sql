-- Stored procedure definition script USP_ManageAdvSearchViews for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAdvSearchViews(v_strAdvSerView TEXT DEFAULT '',  
v_numDomainID NUMERIC(18,0) DEFAULT NULL,  
v_FormId NUMERIC(18,0) DEFAULT NULL,
v_numUserCntID NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM AdvSerViewConf WHERE numDomainID = v_numDomainID and numUserCntID = v_numUserCntID and numFormID = v_FormId;
                             
                                  
   INSERT INTO AdvSerViewConf(numFormFieldID
		,tintOrder
		,numDomainID
		,numUserCntID
		,numFormID
		,bitCustom)
	SELECT
		X.numFormFieldID
		,X.tintOrder
		,v_numDomainID
		,v_numUserCntID
		,v_FormId
		,X.bitCustom
	FROM
	XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strAdvSerView AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFormFieldID NUMERIC(9,0) PATH 'numFormFieldID',
				tintOrder SMALLINT PATH 'tintOrder',
				bitCustom BOOLEAN PATH 'bitCustom'
		) AS X;

   RETURN;
END; $$;


