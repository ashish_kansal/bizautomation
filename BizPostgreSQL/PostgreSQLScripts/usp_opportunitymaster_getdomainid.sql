-- Stored procedure definition script USP_OpportunityMaster_GetDomainID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetDomainID(v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numDomainId
   FROM
   OpportunityMaster
   WHERE
   numOppId = v_numOppID;
END; $$;












