-- Function definition script fn_GetExpenseDtlsbySalesStgID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetExpenseDtlsbySalesStgID(v_numOppID NUMERIC, v_numOppStageID NUMERIC,v_tintType SMALLINT)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$    
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.    
   DECLARE
   v_vcRetValue  DECIMAL(20,5);    
     
    
-- select @vcRetValue=monAmount    
-- from  OpportunityExpense where numOppID=@numOppID and numOppStageID=@numOppStageID    
    
BEGIN
   select   CAST(case when numtype = 1 then  monAmount else 0 end AS VARCHAR(100)) INTO v_vcRetValue from  timeandexpense where numOppId = v_numOppID and numStageId = v_numOppStageID
   and numCategory = 2 and tintTEType = 1; 
   
   RETURN v_vcRetValue;
END; $$;

