-- Stored procedure definition script USP_ItemUOMConversion_GetAllByItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemUOMConversion_GetAllByItem(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT numBaseUnit FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;

   open SWV_RefCur2 for
   SELECT
   ItemUOMConversion.numItemUOMConvID,
		ItemUOMConversion.numDomainID,
		ItemUOMConversion.numItemCode,
		ItemUOMConversion.numSourceUOM,
		SourceUOM.vcUnitName AS SourceUOM,
		ItemUOMConversion.numTargetUOM,
		TargetUOM.vcUnitName AS TargetUOM,
		ItemUOMConversion.numTargetUnit
   FROM
   ItemUOMConversion
   INNER JOIN
   UOM AS SourceUOM
   ON
   ItemUOMConversion.numSourceUOM = SourceUOM.numUOMId
   INNER JOIN
   UOM AS TargetUOM
   ON
   ItemUOMConversion.numTargetUOM = TargetUOM.numUOMId
   WHERE
   ItemUOMConversion.numDomainID = v_numDomainID
   AND ItemUOMConversion.numItemCode = v_numItemCode;
   RETURN;
END; $$;

