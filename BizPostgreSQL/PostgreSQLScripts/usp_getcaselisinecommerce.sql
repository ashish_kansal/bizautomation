-- Stored procedure definition script USP_GetCaseLisInEcommerce for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCaseLisInEcommerce(v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,
 v_CurrentPage INTEGER DEFAULT NULL,
 v_PageSize INTEGER DEFAULT NULL,
 v_UserId NUMERIC(18,0) DEFAULT NULL,
 v_CaseStatus NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER DEFAULT 0;
   v_lastRec  INTEGER DEFAULT 0;
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   open SWV_RefCur for
   SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY numCaseId) AS Rownumber,* FROM(select
         C.numCaseId,
		C.vcCaseNumber,
		FormatedDateFromDate(C.bintCreatedDate,C.numDomainID) AS bintCreatedDate,
		C.textSubject,
		(select  vcUserName from UserMaster where numUserDetailId = C.numAssignedTo LIMIT 1) AS AssignTo,
		(select  vcData from Listdetails WHERE numListItemID = C.numStatus LIMIT 1) AS Status,
		C.textDesc
         from
         Cases AS C
         WHERE
         1 =(CASE WHEN v_CaseStatus = 0 AND C.numStatus <> 136 THEN 1 WHEN C.numStatus = v_CaseStatus THEN 1 ELSE 0 END) AND
         C.numDivisionID = v_numDivisionID AND
         C.numContactId = v_UserId) AS K) AS T
   WHERE T.Rownumber > v_firstRec AND T.Rownumber < v_lastRec;

   open SWV_RefCur2 for
   select
   COUNT(C.numCaseId)
   from
   Cases AS C
   WHERE
   1 =(CASE WHEN v_CaseStatus = 0 AND C.numStatus <> 136 THEN 1 WHEN C.numStatus = v_CaseStatus THEN 1 ELSE 0 END) AND
   C.numDivisionID = v_numDivisionID AND
   C.numContactId = v_UserId;
   RETURN;
END; $$;




--created by Prasanta

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0


