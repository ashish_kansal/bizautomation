CREATE OR REPLACE PROCEDURE USP_DeleteFinancialYear(v_numFinYearId NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0))
LANGUAGE plpgsql
   AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF(SELECT bitCurrentYear FROM FinancialYear WHERE numFinYearId = v_numFinYearId AND numDomainId = v_numDomainID) = true then
      RAISE EXCEPTION 'IS_CURRENT_YEAR';
      RETURN;
   end if;
   BEGIN
		DELETE FROM ChartAccountOpening WHERE numDomainID = v_numDomainID AND numFinYearId = v_numFinYearId;
		DELETE FROM FinancialYear
		WHERE numFinYearId = v_numFinYearId
		AND numDomainId = v_numDomainID;
EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;


