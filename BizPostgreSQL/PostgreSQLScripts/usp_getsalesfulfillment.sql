-- Stored procedure definition script USP_GetSalesFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSalesFulfillment(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                              
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                              
 v_tintSortOrder SMALLINT DEFAULT 0,                                                            
 v_SortChar CHAR(1) DEFAULT '0',                                                                                                                           
 v_CustName VARCHAR(100) DEFAULT '' ,                                                                    
 v_CurrentPage INTEGER DEFAULT NULL,                                                              
 v_PageSize INTEGER DEFAULT NULL,                                                              
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                              
 v_columnName VARCHAR(50) DEFAULT NULL,                                                              
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                                                     
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
 v_numOrderStatus NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                              
   v_firstRec  INTEGER;                                                
   v_lastRec  INTEGER;                                                              
   v_tintOrder  SMALLINT;                                                          
   v_vcFieldName  VARCHAR(50);             
   v_vcListItemType  VARCHAR(3);                                                     
   v_vcListItemType1  VARCHAR(1);                                                         
   v_vcAssociatedControlType  VARCHAR(10);                                                          
   v_numListID  NUMERIC(9,0);                                                          
   v_vcDbColumnName  VARCHAR(40);                              
   v_WhereCondition  VARCHAR(2000);                               
   v_vcLookBackTableName  VARCHAR(2000);                        
   v_bitCustom  BOOLEAN;                
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;   
   v_bitAllowEdit  BOOLEAN;
   v_ListRelID  NUMERIC(9,0); 

   v_Nocolumns  SMALLINT;                        
   v_DefaultNocolumns  SMALLINT;                     
   v_strColumns  VARCHAR(2000);
   v_strOperator  VARCHAR(5);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numOppID NUMERIC(9,0),
      numoppitemtCode NUMERIC(9,0),
      Fulfilled NUMERIC(9,0),
      QtyToFulfill NUMERIC(9,0),
      bintCreatedDate TIMESTAMP,
      Allocation DOUBLE PRECISION,
      OnHand DOUBLE PRECISION,
      numWareHouseItemID NUMERIC(9,0),
      AmtPaid DECIMAL(20,5),
      bitSerialized BOOLEAN,
      bitLotNo BOOLEAN,
      numShipVia NUMERIC(9,0),
      QtyShipped NUMERIC(9,0),
      bitKitParent BOOLEAN
--    numOppBizDocsId numeric(9)
   );                                              
                                              
            
      
                                                             
   v_strSql := 'Select numOppID,numoppitemtCode,Fulfilled,numUnitHour -Fulfilled as QtyToFulfill,bintCreatedDate,numAllocation as Allocation,numOnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo,numShipVia,QtyShipped,bitKitParent from(SELECT  Opp.numOppId,OppItems.numoppitemtCode,Opp.bintCreatedDate,coalesce(sum(OppBizItems.numUnitHour),0) as Fulfilled,coalesce(SUM(OppBiz.monAmountPaid),0) AS AmtPaid,
  OppItems.numUnitHour  ,numAllocation ,numOnHand,numWareHouseItemID,coalesce(I.bitSerialized,false) as bitSerialized,coalesce(I.bitLotNo,false) as bitLotNo,OppBiz.numShipVia,
  Case when Opp.tintshipped = 1 then coalesce(sum(OppItems.numUnitHour),0) else coalesce(sum(OppItems.numQtyShipped),0) end as QtyShipped,
  CASE WHEN coalesce(bitKitParent,false) = true
                               AND coalesce(bitAssembly,false) = true THEN false
                          WHEN coalesce(bitKitParent,false) = true THEN true
                          ELSE false
                     END as bitKitParent
  FROM OpportunityMaster Opp 
  INNER JOIN AdditionalContactsInformation ADC                                                               
  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster Div                                                               
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                               
  INNER JOIN CompanyInfo C                                                               
  ON Div.numCompanyID = C.numCompanyId                                  
  left join AdditionalContactsInformation ADC1 
  on ADC1.numContactId = Opp.numRecOwner
   left join OpportunityBizDocs OppBiz 
  on Opp.numOppId = OppBiz.numOppId and OppBiz.numBizDocId =(select numAuthoritativeSales from AuthoritativeBizDocs where numDomainID = Div.numDomainID) 
  Join  OpportunityItems OppItems
  on   OppItems.numOppID = Opp.numOppId
  left Join  OpportunityBizDocItems OppBizItems
  on  OppBizItems.numOppItemID = OppItems.numoppitemtCode and OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID                                                   
  left join WareHouseItems WI
  on WI.numWareHouseItemID = OppItems.numWarehouseItmsID 
  LEFT JOIN Item I ON I.numItemCode = OppItems.numItemCode
 WHERE Opp.tintOppStatus = 1 and  Opp.tintOppType = 1 and (WI.numWareHouseID in(select numWarehouseID from WareHouseForSalesFulfillment where numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') or WI.numWareHouseID is null) 
  AND I.charItemType = ''P'' and (OppItems.bitDropShip = false or OppItems.bitDropShip is null)
  and Div.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                       
  
   if v_tintSortOrder != 4 then
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintshipped = 0 ';
   end if;

                                  
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and C.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;
   if v_numOrderStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || 'and Opp.numStatus =' || SUBSTR(CAST(v_numOrderStatus AS VARCHAR(10)),1,10);
   end if;
                                                             
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                      


    
--if @columnName = 'OppBiz.dtCreatedDate'                                                                               
-- set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                                                                             
   v_strSql := coalesce(v_strSql,'') || ' group by  numWareHouseItemID,numAllocation,numOnHand,Opp.bintCreatedDate,OppItems.numUnitHour,    Opp.numOppId,OppItems.numoppitemtCode,I.bitSerialized,I.bitLotNo,    OppBiz.numShipVia,OppItems.numQtyShipped,Opp.tintshipped,I.bitKitParent,    I.bitAssembly) X where ';

   if v_tintSortOrder = 3 then

      v_strSql := coalesce(v_strSql,'') || '  X.QtyShipped = 0';
   ELSEIF v_tintSortOrder = 4
   then

      v_strSql := coalesce(v_strSql,'') || '  X.QtyShipped > 0';
   else
      if v_tintSortOrder = 1 then
         v_strOperator := '=';
      ELSE
         v_strOperator := '>';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' X.numUnitHour-X.Fulfilled' || coalesce(v_strOperator,'') || '0';
   end if;

-- OLD filters
-- if @tintSortOrder=1  set @strSql=@strSql + '  AND COALESCE(numAllocation,0)>=numUnitHour- Fulfilled'        
-- if @tintSortOrder=2  set @strSql=@strSql + '  AND numWareHouseItemID>0' 
-- if @tintSortOrder=3  set @strSql=@strSql + '  AND COALESCE(numAllocation,0)<numUnitHour- Fulfilled'        
-- if @tintSortOrder=4  set @strSql=@strSql + '  AND ( (COALESCE(numUnitHour,0)< COALESCE(numAllocation,0)) or (COALESCE(numUnitHour,0)< COALESCE(numOnHand,0)) ) '

   if v_tintSortOrder = 2 then  
      v_strSql := coalesce(v_strSql,'') || '  AND X.Fulfilled >0';
   end if; 
   if v_tintSortOrder = 4 or v_tintSortOrder = 3 then  
      v_strSql := coalesce(v_strSql,'') || '  AND (SELECT COUNT(*) FROM OpportunityBizDocItems WHERE numOppItemID=X.numoppitemtCode)=0';
   end if;
--Sort Newest to oldest
	--set @strSql=@strSql + ' Order by bintCreatedDate desc'
       
   RAISE NOTICE '%',v_strSql;   
                                                         
/*,numOppBizDocsId*/
   EXECUTE 'insert into tt_TEMPTABLE(numOppId,numoppitemtCode,Fulfilled ,QtyToFulfill,bintCreatedDate,Allocation,OnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo/*,numOppBizDocsId*/,numShipVia,QtyShipped,bitKitParent)
' || v_strSql;                                                               
                    
                  
                  
                                                            
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                           
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                
                    
   v_tintOrder := 0;                                                          
   v_WhereCondition := '';                         
                           
   v_Nocolumns := 0;               

   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = 23 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 23 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1) TotalRows;

   v_DefaultNocolumns :=  v_Nocolumns;                       
   v_strColumns := '';    

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType CHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0)
   );
                     
   while v_DefaultNocolumns > 0 LOOP
      v_strColumns := coalesce(v_strColumns,'') || ',null';
      v_DefaultNocolumns := v_DefaultNocolumns -1;
   END LOOP;                  
   v_strSql := '';                    
   v_strSql := ' select ADC.numContactId,DM.numDivisionID,coalesce(DM.numTerID,0) AS numTerID,opp.numRecOwner,DM.tintCRMType,opp.numOppId,OppItems.numoppitemtCode,Case when Opp.tintShipped = 1 then coalesce(OppItems.numUnitHour,0) else coalesce(OppItems.numQtyShipped,0) end as numQtyShipped,Fulfilled,coalesce(WI.numWareHouseItemID,0) AS numWareHouseItemID,T.bitSerialized,T.bitLotNo,Item.numItemCode,cast(OppItems.numUnitHour as NUMERIC(9,0)) as QtyOrdered,T.bitKitParent';
     
   if v_Nocolumns > 0 then

      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID
      FROM View_DynamicColumns
      where numFormId = 23 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
      from View_DynamicCustomColumns
      where numFormId = 23 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      order by tintOrder asc;
   else
      INSERT INTO tt_TEMPFORM
      select tintOrder,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID
      FROM View_DynamicDefaultColumns
      where numFormId = 23 and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
      order by tintOrder asc;
   end if;                                                    
   v_DefaultNocolumns :=  v_Nocolumns;                      
        

   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            
                                              
   while v_tintOrder > 0 LOOP
      RAISE NOTICE '%',v_tintOrder;
      if v_bitCustom = false then
         if v_vcLookBackTableName = 'AdditionalContactsInformation' then  
            v_Prefix := 'ADC.';
         end if;
         if v_vcLookBackTableName = 'DivisionMaster' then  
            v_Prefix := 'DM.';
         end if;
         if v_vcLookBackTableName = 'OpportunityMaster' then 
            v_Prefix := 'Opp.';
         end if;
         if v_vcLookBackTableName = 'Warehouses' then 
            v_Prefix := 'W.';
         end if;
         if v_vcAssociatedControlType = 'SelectBox' then
        
            if v_vcListItemType = 'LI' then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
    
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => -1)  AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => 1)  AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || '),'|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
            
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',' || case
            when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
            when v_vcDbColumnName = 'monPAmount' then 'COALESCE((SELECT varCurrSymbol FROM Currency WHERE numCurrencyID = Opp.numCurrencyID),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
            when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
            WHEN v_vcDbColumnName = 'OrderAmt' then 'monPAmount'
            WHEN v_vcDbColumnName = 'AmtPaid' then 'AmtPaid'
            WHEN v_vcDbColumnName = 'vcItemName' then 'OppItems.vcItemName'
            WHEN v_vcDbColumnName = 'vcModelID' then 'OppItems.vcModelID'
            WHEN v_vcDbColumnName = 'vcItemDesc' then 'OppItems.vcItemDesc'
            WHEN v_vcDbColumnName = 'vcPOppName' then 'Opp.vcPOppName + '' ('' + fn_GetListItemName(COALESCE(Opp.numStatus,0)) + '')'' '
            WHEN v_vcDbColumnName = 'numInTransit' then 'fn_GetItemTransitCount(Item.numItemCode,Opp.numDomainID)'
            WHEN v_vcDbColumnName = 'numQtyShipped' then 'Case when Opp.tintShipped=1 then COALESCE(OppItems.numUnitHour,0) else COALESCE(OppItems.numQtyShipped,0) end'
            WHEN v_vcDbColumnName = 'numOnHand' then 'Case when T.bitKitParent=true then fn_GetKitInventory(OppItems.numItemCode,WI.numWarehouseID) else COALESCE(WI.numOnHand,0) END '
            WHEN v_vcDbColumnName = 'numOnOrder' then 'Case when T.bitKitParent=true then 0 else COALESCE(WI.numOnOrder,0) END '
            WHEN v_vcDbColumnName = 'numAllocation' then 'Case when T.bitKitParent=true then 0 else COALESCE(WI.numAllocation,0) END '
            WHEN v_vcDbColumnName = 'numBackOrder' then 'Case when T.bitKitParent=true then 0 else COALESCE(WI.numBackOrder,0) END '
            else v_vcDbColumnName end || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
   
            v_strSql := coalesce(v_strSql,'') || ', '''' ' || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~' || coalesce(v_bitAllowSorting,false) || '"';
         end if;
      Else
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master where  CFW_Fld_Master.Fld_id = v_numFieldId;
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~0"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~0"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~0"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid    ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~0"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid     ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                               
                          
   IF v_columnName = 'bintCreatedDate' then
      v_columnName := 'opp.bintCreatedDate';
   end if;                        
                  
                                            
   v_strSql := coalesce(v_strSql,'') || '                                                            
  FROM OpportunityMaster Opp     
  INNER JOIN AdditionalContactsInformation ADC  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster DM  ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
   join tt_TEMPTABLE T on T.numOppId=Opp.numOppId                                                          
  INNER JOIN CompanyInfo C                                                               
  ON DM.numCompanyID = C.numCompanyId ' || coalesce(v_WhereCondition,'') ||  '                       
  Join OpportunityItems OppItems
  on   OppItems.numoppitemtCode=T.numoppitemtCode
  left join WareHouseItems WI
  on WI.numWareHouseItemID=OppItems.numWarehouseItmsID 
  left join Warehouses W on W.numWareHouseID=WI.numWareHouseID
  Join Item
  on Item.numItemCode= OppItems.numItemCode                                       
  WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' and opp.numOppId not in (SELECT numOppId FROM WorkOrder WHERE numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND numWOStatus!=23184 AND numOppId IS NOT null) ';
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY  ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                   
                    
   RAISE NOTICE '%',v_strSql;                  
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                                            
                          
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
   RETURN;
END; $$;


