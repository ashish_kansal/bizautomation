-- Stored procedure definition script USP_GetUserBankAccounts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUserBankAccounts(v_numDomainID NUMERIC(18,0),
	v_numUserContactID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT DISTINCT coalesce(BD.numBankDetailID,0) AS numBankDetailID,BD.numAccountID, BD.vcAccountNumber, BM.vcFIName,  BD.vcAccountType,
coalesce((SELECT  monAvailableBalance FROM BankStatementHeader A WHERE A.numBankDetailID = BD.numBankDetailID
      AND dtAvailableBalanceDate =(SELECT  dtLedgerBalanceDate FROM BankStatementHeader
         WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC LIMIT 1) ORDER BY dtCreatedDate DESC LIMIT 1),
   0) AS monAvailableBalance,
FormatedDateTimeFromDate((SELECT  dtAvailableBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC LIMIT 1),v_numDomainID) AS dtAvailableBalanceDate,
coalesce((SELECT  monLedgerBalance FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID
      AND dtLedgerBalanceDate =(SELECT  dtLedgerBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC LIMIT 1) ORDER BY dtCreatedDate DESC LIMIT 1),0) AS monLedgerBalance,
FormatedDateTimeFromDate((SELECT  dtLedgerBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC LIMIT 1),v_numDomainID) AS dtLedgerBalanceDate
   FROM BankDetails BD
   INNER JOIN Chart_Of_Accounts COA ON BD.numBankDetailID = COA.numBankDetailID
   INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID
   LEFT JOIN BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
   WHERE numUserContactID = v_numUserContactID AND BD.numDomainID = v_numDomainID AND BD.bitIsActive = true
   AND COA.IsConnected = true  AND COA.bitActive = true
   GROUP BY BD.vcAccountType,BM.vcFIName,BD.vcAccountNumber,BSH.dtAvailableBalanceDate,
   BSH.monAvailableBalance,BSH.dtLedgerBalanceDate,BSH.monLedgerBalance,
   BD.numBankDetailID,BD.numAccountID  ORDER BY BM.vcFIName,BD.vcAccountType DESC;
END; $$;
	