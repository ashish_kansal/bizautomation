-- FUNCTION: public.usp_getapidetails(numeric, refcursor)

-- DROP FUNCTION public.usp_getapidetails(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getapidetails(
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for SELECT numDomainId,
           bitEnableAPI,
           vcFirstFldValue,
           vcSecondFldValue,
           vcThirdFldValue,
           vcFourthFldValue,
           vcFifthFldValue,
           vcSixthFldValue,
           bitEnableAPI
   FROM   WebAPI W
   LEFT JOIN WebAPIDetail WD
   ON W.WebApiId = WD.WebApiId
   WHERE  WD.bitEnableAPI = true
   AND (numDomainId = v_numDomainID
   OR v_numDomainID = 0);
END;
$BODY$;

ALTER FUNCTION public.usp_getapidetails(numeric, refcursor)
    OWNER TO postgres;
