-- Stored procedure definition script USP_GetShippingRuleStateList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingRuleStateList(v_numDomainID NUMERIC,
    v_numRuleID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT  cast(numStateID as VARCHAR(255)),
            cast(numCountryID as VARCHAR(255)),
            fn_GetListItemName(numCountryID) AS vcCountry,
            fn_GetState(numStateID) AS vcState,
            cast(numRuleID as VARCHAR(255)),
            cast(numDomainID as VARCHAR(255)),
			OVERLAY((SELECT  ', ' || SS.vcZipPostal
      FROM ShippingRuleStateList SS
      WHERE SS.numStateID = SL.numStateID	AND numRuleID = 20079) placing '' from 1 for 2) AS vcZipPostalRange
   FROM    ShippingRuleStateList SL
   WHERE   numRuleID = v_numRuleID
   AND numDomainID = v_numDomainID
   GROUP BY numStateID,numRuleStateID,numCountryID,numRuleID,numDomainID,vcZipPostal;
END; $$;












