-- Stored procedure definition script usp_GetCompanyInfo for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyInfo(v_numDivisionID NUMERIC,        
 v_numDomainID NUMERIC(9,0)   ,        
v_ClientTimeZoneOffset  INTEGER           
    --                                                         
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CMP.numCompanyId, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,
  DM.vcDivisionName,
  coalesce(AD1.numAddressID,0) as numBillAddressID,
  coalesce(AD1.vcStreet,'') as vcBillStreet,
  coalesce(AD1.vcCity,'') as vcBillCity,
  coalesce(fn_GetState(AD1.numState),'') as vcBilState,
  coalesce(AD1.vcPostalCode,'') as vcBillPostCode,
  coalesce(fn_GetListName(AD1.numCountry,false),'')  as vcBillCountry,
  coalesce(AD2.numAddressID,0) as numShipAddressID,
  coalesce(AD2.vcStreet,'') as vcShipStreet,
  coalesce(AD2.vcCity,'') as vcShipCity,
  coalesce(fn_GetState(AD2.numState),'')  as vcShipState,
  coalesce(AD2.vcPostalCode,'') as vcShipPostCode,
  AD2.numCountry AS vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,
   DM.bitPublicFlag, coalesce(DM.numTerID,0) AS numTerID,
  CMP.numCompanyIndustry, CMP.numCompanyCredit,
  coalesce(CMP.txtComments,'') as txtComments, coalesce(CMP.vcWebSite,'') AS vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesId, fn_GetListItemName(CMP.numNoOfEmployeesId) as NoofEmp,
  CMP.vcProfile, DM.numCreatedBy, coalesce(DM.numRecOwner,0) AS numRecOwner, fn_GetContactName(DM.numRecOwner) as RecOwner,
fn_GetContactName(DM.numCreatedBy) || ' ' || CAST(DM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcCreatedBy,
 fn_GetContactName(DM.numModifiedBy) || ' ' || CAST(DM.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcModifiedBy,vcComPhone,vcComFax,
  DM.numGrpId, CMP.vcHow,
  DM.tintCRMType,coalesce(DM.bitNoTax,false) AS bitNoTax ,
   numCampaignID,numAssignedBy,numAssignedTo,
(select  count(*) from GenericDocuments where numRecID = v_numDivisionID and  vcDocumentSection = 'A' AND tintDocumentType = 2) as DocumentCount ,
(SELECT count(*) from CompanyAssociations where numDivisionID = v_numDivisionID and bitDeleted = false) as AssociateCountFrom,
(SELECT count(*) from CompanyAssociations where numAssociateFromDivisionID = v_numDivisionID and bitDeleted = false) as AssociateCountTo,
 coalesce(DM.numCurrencyID,0) AS numCurrencyID,coalesce(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,coalesce(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,coalesce(DM.bitOnCreditHold,false) AS bitOnCreditHold,
 coalesce(vcShippersAccountNo,'') AS vcShippersAccountNo,
 coalesce(intShippingCompany,0) AS intShippingCompany,coalesce(DM.bitEmailToCase,false) AS bitEmailToCase,
 coalesce(numDefaultExpenseAccountID,0) AS numDefaultExpenseAccountID,
 coalesce(numDefaultShippingServiceID,0) AS numDefaultShippingServiceID,
 coalesce(numAccountClassID,0) As numAccountClassID,
 coalesce(tintPriceLevel,0) AS tintPriceLevel,DM.vcPartnerCode as vcPartnerCode,
 bitSaveCreditCardInfo,coalesce(bitShippingLabelRequired,false) AS bitShippingLabelRequired,
 coalesce(DM.tintInbound850PickItem,0) AS tintInbound850PickItem,
 coalesce(bitAllocateInventoryOnPickList,false) AS bitAllocateInventoryOnPickList,
 coalesce(DM.bitAutoCheckCustomerPart,false) AS  bitAutoCheckCustomerPart
   FROM  CompanyInfo CMP
   join DivisionMaster DM
   on DM.numCompanyID = CMP.numCompanyId
   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
   LEFT JOIN AddressDetails AD2 ON AD1.numDomainID = DM.numDomainID
   AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
   left join Domain on Domain.numDomainId = DM.numDomainID
   where
   DM.numDivisionID = v_numDivisionID   and DM.numDomainID = v_numDomainID;
END; $$;












