-- Stored procedure definition script USP_CustomQueryReport_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_Delete(v_numReportID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CustomQueryReport WHERE numReportID = v_numReportID;
   RETURN;
END; $$;

