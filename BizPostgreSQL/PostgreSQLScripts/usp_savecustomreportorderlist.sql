-- Stored procedure definition script usp_SaveCustomReportOrderList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveCustomReportOrderList(v_ReportId NUMERIC(9,0) DEFAULT 0,
v_strOrderList TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete FROM CustReportOrderlist where numCustomReportId = v_ReportId;
        
   insert into CustReportOrderlist(numCustomReportId,vcFieldText,vcValue,numOrder)
   select v_ReportId,
     X.vcText,X.vcValue,X.numOrder
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strOrderList AS XML)
		COLUMNS
			id FOR ORDINALITY,
			vcText VARCHAR(200) PATH 'vcText',
			vcValue VARCHAR(200) PATH 'vcValue',
			numOrder NUMERIC(18,0) PATH 'numOrder'
	) AS X; 
	 
   RETURN;
END; $$;


