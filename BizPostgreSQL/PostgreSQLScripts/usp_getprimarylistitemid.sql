-- Stored procedure definition script USP_GetPrimaryListItemID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--exec USP_GetPrimaryListItemID 1,1,1593,5,7074 
CREATE OR REPLACE FUNCTION USP_GetPrimaryListItemID(v_numDomainID NUMERIC,
v_tintMode SMALLINT DEFAULT 0,
v_numFormFieldId NUMERIC DEFAULT NULL,
v_numPrimaryListID NUMERIC DEFAULT NULL,
v_numRecordID NUMERIC DEFAULT NULL,
v_bitCustomField BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fld_id  NUMERIC; 
   v_numFormID  NUMERIC;
   v_vcDbColumnName  VARCHAR(100);
   v_vcLookBackTableName  VARCHAR(100);
   v_numCompanyID  NUMERIC;
   v_strSQL  VARCHAR(4000);
   v_strTemp  VARCHAR(100);
BEGIN
   IF v_tintMode = 1 OR v_tintMode = 5 then -- 1 for organization, 5 for contact
	
	
		-- when primary list is custom field and secondary is regular(non-custom) field
      IF(SELECT coalesce(numModuleId,0) FROM listmaster WHERE numListID = v_numPrimaryListID) = 8 then
         IF v_tintMode = 1 then 
            v_strTemp := 'CFW_FLD_Values';
         ELSEIF v_tintMode = 5
         then 
            v_strTemp := 'CFW_FLD_Values_Cont';
         end if;
         select   Fld_id INTO v_fld_id FROM CFW_Fld_Master WHERE numlistid = v_numPrimaryListID AND numDomainID = v_numDomainID;
         v_strSQL := 'SELECT top 1 Fld_Value FROM dbo.' || coalesce(v_strTemp,'') || ' WHERE RecId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15) || ' and fld_id = ' || SUBSTR(CAST(v_fld_id AS VARCHAR(15)),1,15) || ' LIMIT 1 ';
      ELSE
         IF v_numPrimaryListID = 834 then -- 834: Shipping Zone
				
            open SWV_RefCur for
            SELECT numShippingZone FROM AddressDetails AD2 INNER JOIN State ON AD2.numState = State.numStateID WHERE AD2.numDomainID = v_numDomainID AND AD2.numRecordID = v_numRecordID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true;
         ELSE
            select   numFormId INTO v_numFormID FROM View_DynamicDefaultColumns WHERE numFieldID = v_numFormFieldId AND numDomainID = v_numDomainID;
            select   vcDbColumnName, vcLookBackTableName INTO v_vcDbColumnName,v_vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
            numFormId = v_numFormID AND numListID = v_numPrimaryListID AND numDomainID = v_numDomainID    LIMIT 1;
            IF v_vcLookBackTableName = 'CompanyInfo' then
					
               select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numRecordID AND numDomainID = v_numDomainID;
               v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM CompanyInfo WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numCompanyId = ' || SUBSTR(CAST(v_numCompanyID AS VARCHAR(15)),1,15);
            ELSEIF v_vcLookBackTableName = 'DivisionMaster'
            then
					
               v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM DivisionMaster WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numDivisionID = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15);
            ELSEIF v_vcLookBackTableName = 'AdditionalContactsInformation'
            then
					
               v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM AdditionalContactsInformation WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numContactId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15);
            ELSE
                v_strSQL := 'SELECT 0';
            end if;
         end if;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;		
   IF v_tintMode = 2 then -- Project
	
			-- when primary list is custom field and secondary is regular(non-custom) field
      IF(SELECT coalesce(numModuleId,0) FROM listmaster WHERE numListID = v_numPrimaryListID) = 8 then
			
         select   Fld_id INTO v_fld_id FROM CFW_Fld_Master WHERE numlistid = v_numPrimaryListID AND numDomainID = v_numDomainID;
         v_strSQL := 'SELECT  Fld_Value FROM CFW_FLD_Values_Pro WHERE RecId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15) || ' and fld_id = ' || SUBSTR(CAST(v_fld_id AS VARCHAR(15)),1,15) || ' LIMIT 1 ';
      ELSE
         select   vcDbColumnName, vcLookBackTableName INTO v_vcDbColumnName,v_vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
         numFormId = 13 AND numListID = v_numPrimaryListID AND numDomainID = v_numDomainID    LIMIT 1;
         v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM ProjectsMaster WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numProId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15);
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
   IF v_tintMode = 3 then -- Opportunity
	
		-- when primary list is custom field and secondary is regular(non-custom) field
      IF(SELECT coalesce(numModuleId,0) FROM listmaster WHERE numListID = v_numPrimaryListID) = 8 then
			
         select   Fld_id INTO v_fld_id FROM CFW_Fld_Master WHERE numlistid = v_numPrimaryListID AND numDomainID = v_numDomainID;
         v_strSQL := 'SELECT  Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15) || ' and fld_id = ' || SUBSTR(CAST(v_fld_id AS VARCHAR(15)),1,15) || ' LIMIT 1 ';
      ELSE
         select   numFormId INTO v_numFormID FROM View_DynamicDefaultColumns WHERE numFieldID = v_numFormFieldId AND numDomainID = v_numDomainID;
         select   vcDbColumnName, vcLookBackTableName INTO v_vcDbColumnName,v_vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
         numFormId = v_numFormID AND numListID = v_numPrimaryListID AND numDomainID = v_numDomainID    LIMIT 1;
         v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM OpportunityMaster WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numOppID = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15);
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
   IF v_tintMode = 4 then -- Cases
	
			-- when primary list is custom field and secondary is regular(non-custom) field
      IF(SELECT coalesce(numModuleId,0) FROM listmaster WHERE numListID = v_numPrimaryListID) = 8 then
			
         select   Fld_id INTO v_fld_id FROM CFW_Fld_Master WHERE numlistid = v_numPrimaryListID AND numDomainID = v_numDomainID;
         v_strSQL := 'SELECT  Fld_Value FROM CFW_FLD_Values_Case WHERE RecId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15) || ' and fld_id = ' || SUBSTR(CAST(v_fld_id AS VARCHAR(15)),1,15) || ' LIMIT 1 ';
      ELSE
         select   vcDbColumnName, vcLookBackTableName INTO v_vcDbColumnName,v_vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
         numFormId = 12 AND numListID = v_numPrimaryListID AND numDomainID = v_numDomainID    LIMIT 1;
         v_strSQL := 'SELECT ' || coalesce(v_vcDbColumnName,'') ||  '  FROM Cases WHERE numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numCaseId = ' || SUBSTR(CAST(v_numRecordID AS VARCHAR(15)),1,15);
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
   RETURN;
END; $$;



