CREATE OR REPLACE FUNCTION USP_KeyWordsUsed
(
	v_From TIMESTAMP,
	v_To TIMESTAMP,
	v_CurrentPage INTEGER,            
	v_PageSize INTEGER,        
	INOUT v_TotRecs INTEGER,
	v_columnName VARCHAR(50),           
	v_columnSortOrder VARCHAR(10),    
	v_numDomainID NUMERIC(9,0),  
	v_ClientOffSetTime INTEGER,
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql              
AS $$
	DECLARE
	v_strSql TEXT;              
	v_firstRec INTEGER;              
	v_lastRec INTEGER;
BEGIN
	BEGIN
		CREATE TEMP SEQUENCE tt_tempTable_seq;
		EXCEPTION WHEN OTHERS THEN
			NULL;
	END;
   
	drop table IF EXISTS tt_TEMPTABLE CASCADE;
	Create TEMPORARY TABLE tt_TEMPTABLE
	( 
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
		vcSearchTerm VARCHAR(100),
		Count  VARCHAR(15)
	);              
   
	v_strSql := 'select 
					distinct(vcSearchTerm),       
					Count(*) as Count
				from 
					TrackingVisitorsHDR HDR      
				where 
					numDomainId =' || COALESCE(v_numDomainID,0) || ' 
					and dtCreated between CAST(''' || v_From || ''' AS TIMESTAMP) - (' || COALESCE(v_ClientOffSetTime,0) || ' || '' MINUTE'')::INTERVAL and CAST(''' || v_To || ''' AS TIMESTAMP) - (' || COALESCE(v_ClientOffSetTime,0) || ' || '' MINUTE'')::INTERVAL 
					and NULLIF(vcSearchTerm,'''') is not null';         
	
	v_strSql := coalesce(v_strSql,'') || ' Group By vcSearchTerm '; 
	v_strSql := coalesce(v_strSql,'') || ' ORDER BY vcSearchTerm ASC ';            
   
	RAISE NOTICE '%',v_strSql;        
   
	EXECUTE 'insert into tt_TEMPTABLE (vcSearchTerm,Count) ' || v_strSql;              

	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;              
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);              
   
	open SWV_RefCur for
	select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;              
   
	select count(*) INTO v_TotRecs from tt_TEMPTABLE;              
   RETURN;
END; $$;


