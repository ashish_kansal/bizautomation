CREATE OR REPLACE FUNCTION USP_DELETE_IMPORT_FILE(v_numImportFileID			BIGINT,
  v_numDomainID				NUMERIC,
  v_numUserCntID				NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM Import_File_Field_Mapping WHERE intImportFileID = v_numImportFileID;
   DELETE FROM Import_History WHERE intImportFileID = v_numImportFileID;
	
   DELETE FROM Import_File_Master
   WHERE intImportFileID = v_numImportFileID
   AND numDomainID = v_numDomainID;
   RETURN;    
END; $$;




