-- Stored procedure definition script USP_UpdateCurrencyDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCurrencyDetails(v_numDomanID NUMERIC(9,0),
v_fltExchangeRate DOUBLE PRECISION,
v_varCurrSymbol VARCHAR(3),
v_numCurrencyId NUMERIC(9,0),
v_bitEnabled BOOLEAN,
v_vcCurrencyDesc VARCHAR(100),
v_numCountryID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Currency set vcCurrencyDesc = v_vcCurrencyDesc,varCurrSymbol = v_varCurrSymbol,fltExchangeRate = v_fltExchangeRate,
   bitEnabled = v_bitEnabled,numCountryId = v_numCountryID
   where numCurrencyID = v_numCurrencyId;
   RETURN;
END; $$;



