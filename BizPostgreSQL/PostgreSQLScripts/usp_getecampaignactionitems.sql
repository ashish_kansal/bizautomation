-- Stored procedure definition script USP_GetECampaignActionItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetECampaignActionItems(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LastDateTime  TIMESTAMP;
   v_CurrentDateTime  TIMESTAMP;
BEGIN
   select   dtLastDateTimeECampaign INTO v_LastDateTime FROM WindowsServiceHistory;
   v_CurrentDateTime := GetUTCDateWithoutTime()+CAST(EXTRACT(HOUR FROM TIMEZONE('UTC',now())) || 'hour' as interval)+CAST(EXTRACT(MINUTE FROM TIMEZONE('UTC',now())) || 'minute' as interval);
   RAISE NOTICE '%',v_LastDateTime;
   RAISE NOTICE '%',v_CurrentDateTime;


   open SWV_RefCur for SELECT   cast(C.numContactID as VARCHAR(255)),
             cast(C.numRecOwner as VARCHAR(255)),
             cast(intStartDate as VARCHAR(255)),
             cast(E.numActionItemTemplate as VARCHAR(255)),
             A.numDomainID,
             A.numDivisionId,
             cast(DTL.numConECampDTLID as VARCHAR(255))
   FROM     ConECampaign C
   JOIN ConECampaignDTL DTL
   ON numConEmailCampID = numConECampID
   JOIN ECampaignDTLs E
   ON DTL.numECampDTLID = E.numECampDTLId
   JOIN ECampaign EC ON E.numECampID = EC.numECampaignID
   JOIN AdditionalContactsInformation A
   ON A.numContactId = C.numContactID
   WHERE    bitSend IS NULL
   AND bitEngaged = true
   AND numActionItemTemplate > 0
   AND (DTL.dtExecutionDate BETWEEN v_LastDateTime+INTERVAL '-2 hour' AND v_CurrentDateTime OR(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND coalesce(bitSend,false) = true) = 0);
END; $$;












