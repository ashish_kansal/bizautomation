-- Stored procedure definition script USP_GetRecurringDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRecurringDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	Select 
		numRecurringId as "numRecurringId"
		,varRecurringTemplateName as "varRecurringTemplateName"
	From 
		RecurringTemplate 
	Where 
		numDomainID = v_numDomainId;
END; $$;












