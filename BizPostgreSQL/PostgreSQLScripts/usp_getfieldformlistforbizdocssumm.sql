-- Stored procedure definition script USP_GetFieldFormListForBizDocsSumm for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetFieldFormListForBizDocsSumm(v_numDomainID NUMERIC(9,0),
v_BizDocID NUMERIC(9,0),
v_numFormID NUMERIC(9,0),
v_numBizDocTempID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select DFM.numFieldID as numFormFieldId,coalesce(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,DFM.vcDbColumnName
   from DycFormField_Mapping DFFM
   JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
   where numFormID = 16 and (DFFM.numDomainID is null or DFFM.numDomainID = v_numDomainID)
   AND coalesce(DFM.bitDeleted,false) = false;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numFormID NUMERIC(18,0) NOT NULL,
      numBizDocID NUMERIC(18,0) NOT NULL,
      numFieldID NUMERIC(18,0) NOT NULL,
      numDomainID NUMERIC(18,0) NOT NULL,
      numBizDocTempID NUMERIC(18,0) 
   ); 

   insert into tt_TEMP
   (
		numFormID,
		numBizDocID,
		numFieldID,
		numDomainID,
		numBizDocTempID
   )
   select * from DycFrmConfigBizDocsSumm Ds where numBizDocID = v_BizDocID and Ds.numFormID = v_numFormID
   and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID;


   open SWV_RefCur2 for
   select DFM.numFieldID as numFormFieldId,coalesce(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,DFM.vcDbColumnName
   from DycFormField_Mapping DFFM
   JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
   join tt_TEMP Ds on Ds.numFieldID = DFFM.numFieldID
   where numBizDocID = v_BizDocID and Ds.numFormID = v_numFormID and (DFFM.numFormID = v_numFormID or DFFM.numDomainID is null)
   and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID order by Ds.ID;
   RETURN;
END; $$;


