-- Stored procedure definition script USP_ManageCorrespondense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCorrespondense(INOUT v_numCorrespondenseID NUMERIC DEFAULT 0 ,
	v_numCommID NUMERIC DEFAULT 0,
	v_numEmailHistoryID NUMERIC DEFAULT 0,
	v_tintCorrType SMALLINT DEFAULT NULL,
	v_numDomainID NUMERIC DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF v_numCommID = 0 then 
      v_numCommID := NULL;
   end if;
   IF v_numEmailHistoryID = 0 then 
      v_numEmailHistoryID := NULL;
   end if;
   IF v_numCorrespondenseID = 0 then

      INSERT INTO Correspondense(numCommID,
		numEmailHistoryID,
		tintCorrType,
		numDomainID)
	VALUES(v_numCommID,
		v_numEmailHistoryID,
		v_tintCorrType,
		v_numDomainID);
	
      v_numCorrespondenseID := CURRVAL('Correspondense_seq');
   ELSE
      UPDATE Correspondense SET
      numCommID = v_numCommID,numEmailHistoryID = v_numEmailHistoryID,tintCorrType = v_tintCorrType,
      numDomainID = v_numDomainID
      WHERE numCorrespondenseID = v_numCorrespondenseID;
   end if;
   RETURN;
END; $$;


