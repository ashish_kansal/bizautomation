-- Stored procedure definition script USP_GetReportAsItsType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReportAsItsType(v_numUserCntid NUMERIC(9,0),        
v_numDomainId NUMERIC(9,0),        
v_tintType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select AIT.numListItemID,LD.vcData from ForReportsByAsItsType AIT
   JOIN Listdetails LD
   ON LD.numListItemID = AIT.numListItemID
   where AIT.numUserCntid = v_numUserCntid
   and AIT.numDomainID = v_numDomainId
   and AIT.tintType = v_tintType;
END; $$;












