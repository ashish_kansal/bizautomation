-- Stored procedure definition script USP_PromotionOffer_GetPromotionApplicableToItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PromotionOffer_GetPromotionApplicableToItem(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_numItemClassification  NUMERIC(18,0);
BEGIN
   select   coalesce(numItemClassification,0) INTO v_numItemClassification FROM
   Item WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode;

   IF coalesce(v_numDivisionID,0) > 0 then
	
      select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile FROM
      DivisionMaster D
      JOIN
      CompanyInfo C
      ON
      C.numCompanyId = D.numCompanyID WHERE
      numDivisionID = v_numDivisionID;
   ELSEIF coalesce(v_numSiteID,0) > 0
   then
	
      select   coalesce(numRelationshipId,0), coalesce(numProfileId,0) INTO v_numRelationship,v_numProfile FROM
      eCommerceDTL WHERE
      numSiteId = v_numSiteID;
   end if;

   open SWV_RefCur for SELECT
		PO.numProId
		,PO.vcProName
		,coalesce(PO.numOrderPromotionID,0) numOrderPromotionID
		,CONCAT(coalesce(PO.vcShortDesc,'-'),(CASE WHEN coalesce(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcShortDesc
		,CONCAT(coalesce(PO.vcLongDesc,'-'),(CASE WHEN coalesce(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcLongDesc
   FROM
   PromotionOffer PO
   LEFT JOIN
   PromotionOffer POOrder
   ON
   PO.numOrderPromotionID = POOrder.numProId
   WHERE
   PO.numDomainId = v_numDomainID
   AND coalesce(PO.bitEnabled,false) = true
   AND coalesce(PO.IsOrderBasedPromotion,false) = false
   AND 1 =(CASE
   WHEN coalesce(PO.numOrderPromotionID,0) > 0
   THEN(CASE WHEN POOrder.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN 1 ELSE 0 END) END)
   ELSE(CASE WHEN PO.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN 1 ELSE 0 END) END)
   END)
   AND 1 =(CASE
   WHEN coalesce(PO.numOrderPromotionID,0) > 0
   THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
   ELSE(CASE PO.tintCustomersBasedOn
      WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionID) > 0 THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
      WHEN 3 THEN 1
      ELSE 0
      END)
   END)
   AND 1 =(CASE
   WHEN PO.tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
   WHEN PO.tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
   WHEN PO.tintOfferBasedOn = 4 THEN 1
   ELSE 0
   END)
   ORDER BY
   CASE
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
   END;
END; $$;












