-- Stored procedure definition script USP_GeBizDocType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GeBizDocType(v_BizDocType SMALLINT,                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then

      PERFORM USP_GetMasterListItems(27,v_numDomainID);
   ELSE
      open SWV_RefCur for SELECT Ld.numListItemID, coalesce(vcRenamedListName,vcData) as vcData
      FROM BizDocFilter BDF INNER JOIN Listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
      left join listorder LO on Ld.numListItemID = LO.numListItemID AND LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = 27 AND BDF.tintBizocType = v_BizDocType AND BDF.numDomainID = v_numDomainID
      AND (constFlag = true or Ld.numDomainid = v_numDomainID);
   end if;
END; $$;












