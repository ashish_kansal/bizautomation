DROP FUNCTION IF EXISTS USP_GetDepositDetailsWithClass;

CREATE OR REPLACE FUNCTION USP_GetDepositDetailsWithClass
(
	v_numDepositId NUMERIC(9,0) DEFAULT 0 ,
    v_numDomainId NUMERIC(9,0) DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0, -- 1= Get Undeposited Payments 
    v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0,
	v_SortColumn VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_strCondition TEXT DEFAULT '',
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_numBaseCurrencyID NUMERIC;
	v_BaseCurrencySymbol VARCHAR(3);
	v_firstRec INTEGER;
	v_lastRec INTEGER;
	v_strSQLTintMode0First TEXT;
	v_strSQLTintMode0Second TEXT;
	v_strSQLTintMode1First TEXT;
	v_strSQLTintMode1Second TEXT;
	v_strFinal  TEXT;
BEGIN
	SELECT COALESCE(numCurrencyID,0) INTO v_numBaseCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
	SELECT COALESCE(varCurrSymbol,'') INTO v_BaseCurrencySymbol FROM Currency WHERE numCurrencyID = v_numBaseCurrencyID;
      
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

	IF LENGTH(COALESCE(v_SortColumn,'')) = 0 then
      v_SortColumn := 'DepositDate';
	END IF;

	IF LENGTH(COALESCE(v_columnSortOrder,'')) = 0 then
      v_columnSortOrder := 'ASC';
	end if;

	IF v_tintMode = 0 THEN
		v_strSQLTintMode0First := 'SELECT 
								numDepositId ,
								fn_GetComapnyName(numDivisionID) vcCompanyName ,
								numDivisionID ,
								numChartAcntId ,
								FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
								dtDepositDate AS dtOrigDepositDate,
								monDepositAmount ,
								numPaymentMethod ,
								vcReference ,
								vcMemo ,
								tintDepositeToType,tintDepositePage,bitDepositedToAcnt
								,DM.numCurrencyID
								,COALESCE(C.varCurrSymbol,'''') varCurrSymbol
								,COALESCE(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
								,''' || COALESCE(v_BaseCurrencySymbol,'') || ''' BaseCurrencySymbol
								,COALESCE(monRefundAmount,0) AS monRefundAmount
								,COALESCE(DM.numAccountClass,0) AS numAccountClass
							FROM 
								DepositMaster DM
							LEFT 
								JOIN Currency C 
							ON 
								C.numCurrencyID = DM.numCurrencyID
							WHERE 
								numDepositId = ' || COALESCE(v_numDepositId,0) || '
								AND DM.numDomainId = ' || COALESCE(v_numDomainId,0);
        
		v_strSQLTintMode0Second := 'SELECT  
									DD.numDepositeDetailID ,
									DD.numDepositID ,
									DD.numOppBizDocsID ,
									DD.numOppID ,
									DD.monAmountPaid ,
									DD.numChildDepositID ,
									DD.numPaymentMethod ,
									DD.vcMemo ,
									DD.vcReference ,
									DD.numClassID ,
									DD.numProjectID ,
									DD.numReceivedFrom ,
									DD.numAccountID,
									GJD.numTransactionId AS numTransactionID,
									GJD1.numTransactionId AS numTransactionIDHeader
									,COALESCE(DM.numCurrencyID,0) numCurrencyID
									,COALESCE(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
								FROM   DepositeDetails DD
								LEFT JOIN DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
								LEFT JOIN General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7 
								LEFT JOIN General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6 
								WHERE   DD.numDepositID = ' || COALESCE(v_numDepositId,0);

		OPEN SWV_RefCur FOR EXECUTE v_strSQLTintMode0First;
		OPEN SWV_RefCur2 FOR EXECUTE v_strSQLTintMode0Second;
	END IF;

	IF v_tintMode = 1 THEN
		v_strSQLTintMode1First := 'DROP TABLE IF EXISTS tt_Deposits CASCADE; 
									CREATE TEMPORARY TABLE tt_Deposits AS  
									SELECT 
										Row_number() OVER ( ORDER BY ' || COALESCE(v_SortColumn,'') || ' ' || COALESCE(v_columnSortOrder,'') || ') AS row
										,* 
									FROM 
									(
									--Get Saved deposit entries for edit mode
										SELECT 										
											DD.numDepositeDetailID ,
											DM.numDepositId ,
											DM.numChartAcntId ,
											FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
											DD.monAmountPaid AS monDepositAmount,
											DD.numPaymentMethod ,
											DD.vcReference ,
											DD.vcMemo ,
											DD.numReceivedFrom AS numDivisionID,
											fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
											bitDepositedToAcnt,
												(SELECT 
											CASE tintSourceType 
											WHEN 1 THEN GetListIemName(tintSource) 
											WHEN 2 THEN (SELECT vcSiteName FROM Sites WHERE numSiteID = tintSource LIMIT 1) 
											WHEN 3 THEN (SELECT vcProviderName FROM WebAPI WHERE WebApiId = tintSource LIMIT 1)  
											ELSE '''' END 
											FROM OpportunityMaster WHERE numOppId IN ( SELECT numOppID FROM DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0 LIMIT 1)) AS vcSource,
											CASE WHEN DM.numTransHistoryID >0 THEN GetListIemName(COALESCE(TH.numCardType,0)) ELSE '''' END  AS vcCardType
											,DM.numCurrencyID
											,COALESCE(C.varCurrSymbol,'''') varCurrSymbol
											,COALESCE(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
											,''' || COALESCE(v_BaseCurrencySymbol,'') || ''' BaseCurrencySymbol
											,dtDepositDate AS DepositDate
										FROM  DepositeDetails DD
										INNER JOIN DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
										LEFT JOIN TransactionHistory TH ON TH.numTransHistoryID= DM.numTransHistoryID
										LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
										WHERE DD.numDepositID = ' || COALESCE(v_numDepositId,0) || ' AND (COALESCE(DM.numAccountClass,0)=' || COALESCE(v_numAccountClass,0) || ' OR ' || COALESCE(v_numAccountClass,0) || '=0)
								UNION
								SELECT
										0 numDepositeDetailID ,
										numDepositId ,
										numChartAcntId ,
										FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
										monDepositAmount ,
										numPaymentMethod ,
										vcReference ,
										vcMemo ,
										DM.numDivisionID,
										fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
										bitDepositedToAcnt,
										(SELECT 
										CASE tintSourceType 
										WHEN 1 THEN GetListIemName(tintSource) 
										WHEN 2 THEN (SELECT vcSiteName FROM Sites WHERE numSiteID = tintSource LIMIT 1) 
										WHEN 3 THEN (SELECT vcProviderName FROM WebAPI WHERE WebApiId = tintSource LIMIT 1)  
										ELSE '''' END 
										FROM OpportunityMaster WHERE numOppId IN ( SELECT numOppID FROM DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0 LIMIT 1) ) AS vcSource,
										CASE WHEN DM.numTransHistoryID >0 THEN GetListIemName(COALESCE(TH.numCardType,0)) ELSE '''' END  AS vcCardType
										,DM.numCurrencyID
										,COALESCE(C.varCurrSymbol,'''') varCurrSymbol
										,COALESCE(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
										,''' || COALESCE(v_BaseCurrencySymbol,'') || ''' BaseCurrencySymbol
										,dtDepositDate AS DepositDate
								FROM   DepositMaster DM
										INNER JOIN Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
										LEFT JOIN TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
										LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
								WHERE   DM.numDomainId = ' ||COALESCE(v_numDomainId,0) || '
										AND tintDepositeToType = 2
										AND COALESCE(bitDepositedToAcnt,false) = false 
										AND (COALESCE(DM.numCurrencyID,' || COALESCE(v_numBaseCurrencyID,0) || ') = ' || COALESCE(v_numCurrencyID,0) || ' OR ' || COALESCE(v_numCurrencyID,0) || ' =0)
										AND (COALESCE(DM.numAccountClass,0)=' || COALESCE(v_numAccountClass,0) || '  OR ' || COALESCE(v_numAccountClass,0) || '=0)
							)  Deposits ' || (CASE WHEN LENGTH(COALESCE(v_strCondition,'')) > 0 THEN 'WHERE ' || v_strCondition ELSE '' END) || ';'; 

		RAISE NOTICE '%',v_strSQLTintMode1First;
		EXECUTE v_strSQLTintMode1First;
		OPEN SWV_RefCur FOR EXECUTE 'SELECT * FROM tt_Deposits  WHERE row > ' || v_firstRec || ' and row <' || v_lastRec || ';';
		EXECUTE 'SELECT COUNT(*) FROM tt_Deposits' INTO v_TotRecs;

		v_strSQLTintMode1Second := 'SELECT 
										DD.numDepositeDetailID ,
										DM.numDepositId ,
										FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
										DD.monAmountPaid ,
										DD.numPaymentMethod ,
										DD.vcReference ,
										DD.vcMemo ,
										DD.numAccountID,
			--							 COA.vcAccountCode numAcntType,
										DD.numClassID,
										DD.numProjectID,
										DD.numReceivedFrom,
										fn_GetComapnyName(DD.numReceivedFrom) ReceivedFrom,
										bitDepositedToAcnt
									FROM   DepositeDetails DD
									INNER JOIN DepositMaster DM ON DD.numDepositID = DM.numDepositId
									WHERE DD.numDepositID = ' || COALESCE(v_numDepositId,0) || ' AND COALESCE(numChildDepositID,0) = 0
									AND (COALESCE(DM.numAccountClass,0)=' || COALESCE(v_numAccountClass,0) || '  OR ' || COALESCE(v_numAccountClass,0) || '=0)';

		OPEN SWV_RefCur2 FOR EXECUTE v_strSQLTintMode1Second;
	END IF;

   RETURN;
END; $$;
    


