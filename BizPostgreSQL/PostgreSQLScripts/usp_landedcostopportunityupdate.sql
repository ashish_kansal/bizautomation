-- Stored procedure definition script USP_LandedCostOpportunityUpdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LandedCostOpportunityUpdate(v_numDomainId NUMERIC(18,0),
    v_numOppId NUMERIC(18,0),
	v_vcLanedCost VARCHAR(50))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monLandedCostTotal  DECIMAL(20,5);
   v_Total  DECIMAL(18,2);
BEGIN
   select   SUM(BH.monAmountDue) INTO v_monLandedCostTotal FROM BillHeader AS BH WHERE BH.numDomainId = v_numDomainId AND BH.numOppId = v_numOppId AND BH.bitLandedCost = true;

		--Update Total Landed Cost on Opp
   UPDATE OpportunityMaster SET monLandedCostTotal = v_monLandedCostTotal,vcLanedCost = v_vcLanedCost
   WHERE OpportunityMaster.numDomainId = v_numDomainId AND OpportunityMaster.numOppId = v_numOppId;

		--Calculate landed cost for each item and Update Landed Cost on Item
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      SELECT OI.numoppitemtCode,
		CASE v_vcLanedCost WHEN 'Amount' THEN OI.monTotAmount
      WHEN 'Cubic Size' THEN coalesce(I.fltWidth,0)*coalesce(I.fltHeight,0)*coalesce(I.fltLength,0)
      WHEN 'Weight' THEN coalesce(I.fltWeight,0) END AS Total
		
      FROM OpportunityItems AS OI JOIN Item AS I ON OI.numItemCode = I.numItemCode
      WHERE OI.numOppId = v_numOppId;

   select   SUM(Total) INTO v_Total FROM tt_TEMP AS T;

   UPDATE OpportunityItems AS OI SET monLandedCost = CAST(coalesce((v_monLandedCostTotal*T.Total)/NULLIF(v_Total,0),0) AS DECIMAL(18,2)) FROM tt_TEMP T WHERE OI.numoppitemtCode = T.numoppitemtCode AND OI.numOppId = v_numOppId;


		

		--Update Item Average Cost
		UPDATE 
			Item AS I  
		SET 
			monAverageCost = CASE 
								WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 
								ELSE 
									CASE WHEN coalesce(TotalOnHand.Qty,0) <= 0
									THEN 
										I.monAverageCost
									ELSE								
										((coalesce(I.monAverageCost,0) * coalesce(TotalOnHand.Qty,0)) + coalesce(OI.monLandedCost,0) - coalesce(OI.monAvgLandedCost,0))/NULLIF(coalesce(TotalOnHand.Qty,0),0)
									END
								END
		FROM 
			OpportunityItems AS OI 
		LEFT JOIN LATERAL
		(
			SELECT
				coalesce((SUM(numOnHand) + SUM(numAllocation)),0) AS Qty
			FROM
				WarehouseItems
			WHERE 
				numItemID = OI.numItemCode
		) AS TotalOnHand
		ON true
		WHERE OI.numOppId = v_numOppId
			AND OI.numItemCode = I.numItemCode;

		--Update monAvgLandedCost for tracking Purpose
   UPDATE OpportunityItems AS OI SET monAvgLandedCost = OI.monLandedCost  WHERE OI.numOppId = v_numOppId;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   RETURN;
END; $$;



