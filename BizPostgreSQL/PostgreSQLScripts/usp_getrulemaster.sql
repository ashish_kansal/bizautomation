-- Stored procedure definition script USP_GetRuleMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRuleMaster(v_numRuleID NUMERIC(9,0) DEFAULT 0,
    v_byteMode SMALLINT DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_tintModuleType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
        
      open SWV_RefCur for
      SELECT  numRuleID,tintModuleType,vcRuleName,vcRuleDescription,
                    tintBasedOn,numFormFieldIdChange,tintStatus
      FROM    RuleMaster RM
      WHERE   numRuleID = v_numRuleID and numDomainID = v_numDomainID;

      open SWV_RefCur2 for
      select  numRuleConditionID,vcCondition,vcJSON,tintConditionOrder FROM RuleCondition where numRuleID = v_numRuleID and numDomainID = v_numDomainID
      ORDER BY tintConditionOrder;
   ELSEIF v_byteMode = 1
   then
            
      open SWV_RefCur for
      SELECT  RM.numRuleID,RM.tintModuleType,DFM.vcFormName as vcModuleType,RM.vcRuleName,RM.vcRuleDescription,
                    RM.tintBasedOn,RM.numFormFieldIdChange,RM.tintStatus,
					Case tintStatus When 0 then 'Active' else 'InActive' end as vcStatus
      FROM    RuleMaster RM join dynamicFormMaster as DFM on RM.tintModuleType = DFM.numFormID
      WHERE    numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 2
   then
		
      open SWV_RefCur for
      select tintOrder+1 as tintOrder,vcDbColumnName,vcFieldName as vcFormFieldName,
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,numFieldID as numFormFieldId,vcFieldDataType
      FROM View_DynamicDefaultColumns
      where numFormId = v_tintModuleType and coalesce(bitWorkFlowField,false) = true and numDomainID = v_numDomainID
      order by tintOrder asc;
   ELSEIF v_byteMode = 3
   then
            
      open SWV_RefCur for
      SELECT  RM.numRuleID,RM.tintModuleType,DFM.vcFormName as vcModuleType,RM.vcRuleName,RM.vcRuleDescription,
                    RM.tintBasedOn,RM.numFormFieldIdChange
      FROM    RuleMaster RM join dynamicFormMaster as DFM on RM.tintModuleType = DFM.numFormID
      WHERE    numDomainID = v_numDomainID and  tintModuleType = v_tintModuleType and tintStatus = 0;
   ELSEIF v_byteMode = 4
   then
            
      open SWV_RefCur for
      select  numRuleConditionID,vcCondition,vcJSON,tintConditionOrder FROM RuleCondition where numRuleID = v_numRuleID and numDomainID = v_numDomainID
      ORDER BY tintConditionOrder;
   end if;
   RETURN;
END; $$;


