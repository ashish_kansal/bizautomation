-- Stored procedure definition script USP_SaveReorderForSerialItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveReorderForSerialItems(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	INOUT v_numItemCode NUMERIC(18,0) ,
	v_numReorder DOUBLE PRECISION)
LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   WareHouseItems
   SET
   numReorder = v_numReorder,dtModified = LOCALTIMESTAMP
   WHERE
   numItemID = v_numItemCode
   AND numDomainID = v_numDomainID;

   UPDATE Item SET bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
   RETURN;
END; $$;


