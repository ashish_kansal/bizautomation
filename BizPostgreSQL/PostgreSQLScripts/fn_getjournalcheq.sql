-- Function definition script fn_GetJournalCheq for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetJournalCheq(v_numCheckId NUMERIC(8,0),
 v_numDomainId INTEGER)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Amount  VARCHAR(50);
   v_numCheckNo  VARCHAR(100);
   v_vcMemo TEXT;
   v_vcDescription VARCHAR(2000);


   BizDoc_Cursor CURSOR FOR
	
   SELECT cast(SUM(CheckDetails.monAmount) as VARCHAR(50)) as Amount,
	coalesce(CAST(CheckHeader.numCheckNo AS VARCHAR(50)),'No-Cheq') as numCheckNo,
	coalesce(CheckHeader.vcMemo,'No-Memo') as vcMemo FROM CheckHeader INNER JOIN CheckDetails ON CheckHeader.numCheckHeaderID=CheckDetails.numCheckHeaderID
   WHERE CheckHeader.numCheckHeaderID = v_numCheckId AND 
   CheckHeader.numDomainId = v_numDomainId
   GROUP BY CheckHeader.numCheckNo,CheckHeader.vcMemo;
BEGIN
   v_vcDescription := '';

   OPEN BizDoc_Cursor;

   FETCH NEXT FROM BizDoc_Cursor into v_Amount,v_numCheckNo,v_vcMemo;
   WHILE FOUND LOOP
      v_vcDescription := coalesce(v_vcDescription,'') || ',' ||
      ' Memo: ' || coalesce(v_vcMemo,'') ||
      ' Chek No: ' || coalesce(v_numCheckNo,'') ||
      ' Amount: ' || coalesce(v_Amount,'');
      FETCH NEXT FROM BizDoc_Cursor into v_Amount,v_numCheckNo,v_vcMemo;
   END LOOP;

   CLOSE BizDoc_Cursor;


   RETURN SUBSTR(v_vcDescription,2,LENGTH(v_vcDescription));
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_GetLatestEntityEditedDateAndUser]    Script Date: 07/26/2008 18:12:38 ******/

