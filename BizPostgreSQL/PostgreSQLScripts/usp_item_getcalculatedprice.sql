-- Stored procedure definition script USP_Item_GetCalculatedPrice for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetCalculatedPrice(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numQty NUMERIC
	,v_vcChildKitSelectedItem TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWarehouseID  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_tintKitAssemblyPriceBasedOn  SMALLINT;
BEGIN
   select   coalesce(numDefaultWareHouseID,0) INTO v_numWarehouseID FROM eCommerceDTL WHERE  numSiteId = v_numSiteID;
	

   IF coalesce(v_numWarehouseID,0) > 0 AND EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID) then
	
      select   numWareHouseItemID INTO v_numWarehouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID   ORDER BY numWareHouseItemID ASC LIMIT 1;
   ELSE
      select   numWareHouseItemID INTO v_numWarehouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode   ORDER BY numWareHouseItemID ASC LIMIT 1;
   end if;


   select   coalesce(tintKitAssemblyPriceBasedOn,1) INTO v_tintKitAssemblyPriceBasedOn FROM Item WHERE numItemCode = v_numItemCode;

   open SWV_RefCur for SELECT
   coalesce(monPrice,0) AS "monPrice"
		,coalesce(monMSRPPrice,0) AS "monMSRP"
   FROM
   fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,v_numWarehouseItemID,
   v_tintKitAssemblyPriceBasedOn::SMALLINT,0,0,v_vcChildKitSelectedItem,0,1);
END; $$;












