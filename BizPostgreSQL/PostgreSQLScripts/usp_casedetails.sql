-- Stored procedure definition script USP_CaseDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CaseDetails(v_numCaseId NUMERIC(9,0) ,    
v_numDomainID NUMERIC(9,0),    
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select C.numContactId,vcCaseNumber,
 intTargetResolveDate,textSubject,Div.numDivisionID,
 numStatus,GetListIemName(numStatus) AS vcCaseStatus, numPriority,textDesc,tintCRMType,
 textInternalComments,numReason,C.numContractID,
 coalesce((SELECT cm.vcContractName FROM ContractManagement cm WHERE cm.numContractId = C.numContractID),'') AS vcContractName,
 numOrigin,numType,
 fn_GetContactName(C.numCreatedby) || ' ' || C.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as CreatedBy,
 fn_GetContactName(C.numModifiedBy) || ' ' || C.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as ModifiedBy,
 fn_GetContactName(C.numRecOwner) as RecOwner,
 C.numRecOwner,
 tintSupportKeyType,
 C.numAssignedTo,C.numAssignedBy,coalesce(Addc.vcFirstName,'') || ' ' || coalesce(Addc.vcLastname,'') as vcName,
 coalesce(vcEmail,'') as vcEmail,coalesce(numPhone,'') || case when NULLIF(numPhoneExtension,'') is null then '' when numPhoneExtension = '' then '' else ', ' || numPhoneExtension end  as Phone,
         coalesce(numPhone,'') AS numPhone,
         coalesce(numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT  coalesce(vcData,'') FROM Listdetails WHERE numListID = 5 AND numListItemID = com.numCompanyType AND numDomainid = v_numDomainID LIMIT 1) AS vcCompanyType,
 com.numCompanyId,com.vcCompanyName,(select count(*) from Comments where numCaseID = v_numCaseId)  as NoofCases,
 (SELECT COUNT(*) FROM   GenericDocuments WHERE  numRecID = v_numCaseId AND vcDocumentSection = 'CS') AS DocumentCount, Div.numTerID,
 coalesce((SELECT (coalesce(C.numIncidents,0)  -coalesce(C.numIncidentsUsed,0)) FROM Contracts AS C WHERE C.numDivisonId = Div.numDivisionID AND numDomainId = v_numDomainID AND intType = 3),0) AS numIncidentLeft,
  coalesce(((SELECT  CASE WHEN  coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainID AND CAST(I.numItemClassification AS VARCHAR) = C.vcItemClassification AND intType = 2),0) > 0 THEN coalesce((SELECT vcNotes FROM Contracts AS C WHERE  numDomainId = v_numDomainID AND CAST(I.numItemClassification AS VARCHAR) = C.vcItemClassification AND intType = 2),'-') ELSE '-' END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item I ON OI.numItemCode = I.numItemCode WHERE O.numDivisionId = Addc.numDivisionId AND O.numDomainId = v_numDomainID AND
         1 =(CASE WHEN OI.numoppitemtCode IN(SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId = C.numCaseId) THEN 1
         ELSE 0 END)
         AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1)),'') AS vcContractNotes,
 coalesce((SELECT  CASE WHEN coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainID AND CAST(I.numItemClassification AS VARCHAR) = C.vcItemClassification  AND intType = 2),0) > 0 THEN coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainID AND CAST(I.numItemClassification AS VARCHAR) = C.vcItemClassification AND intType = 2),0) -coalesce(DATE_PART('day',LOCALTIMESTAMP  -O.bintClosedDate),0) ELSE 0 END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item I ON OI.numItemCode = I.numItemCode WHERE O.numDivisionId = Addc.numDivisionId AND O.numDomainId = v_numDomainID AND
      1 =(CASE WHEN OI.numoppitemtCode IN(SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId = C.numCaseId) THEN 1
      ELSE 0 END)
      AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1),0) AS numWarrantyDaysLeft,
  coalesce((SELECT  I.vcItemName FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item I ON OI.numItemCode = I.numItemCode WHERE O.numDivisionId = Addc.numDivisionId AND O.numDomainId = v_numDomainID AND
      1 =(CASE WHEN OI.numoppitemtCode IN(SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId = C.numCaseId) THEN 1
      ELSE 0 END)
      AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1),'-') AS vcWarrantyItemName,
  coalesce((SELECT  I.numItemCode FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item I ON OI.numItemCode = I.numItemCode WHERE O.numDivisionId = Addc.numDivisionId AND O.numDomainId = v_numDomainID AND
      1 =(CASE WHEN OI.numoppitemtCode IN(SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId = C.numCaseId) THEN 1
      ELSE 0 END)
      AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1),0) AS numWarrantyItemCode
   from Cases C
   left join AdditionalContactsInformation Addc
   on Addc.numContactId = C.numContactId
   join DivisionMaster Div
   on Div.numDivisionID = Addc.numDivisionId
   join CompanyInfo com
   on com.numCompanyId = Div.numCompanyID
   where numCaseId = v_numCaseId and C.numDomainID = v_numDomainID;
END; $$;













