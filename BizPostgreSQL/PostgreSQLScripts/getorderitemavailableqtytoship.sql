-- Function definition script GetOrderItemAvailableQtyToShip for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOrderItemAvailableQtyToShip(v_numOppItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_bitKitParent BOOLEAN,
	v_tintCommitAllocation SMALLINT,
	v_bitAllocateInventoryOnPickList BOOLEAN)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numQuantity  DOUBLE PRECISION DEFAULT 0;
   v_numAvailableQtyToShip  DOUBLE PRECISION DEFAULT 0;
   v_numOnHand  DOUBLE PRECISION;
   v_numAllocation  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION DEFAULT 0;
   v_numItemCode  NUMERIC(18,0);
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   select   coalesce(numUnitHour,0), coalesce(numQtyShipped,0) INTO v_numQuantity,v_numQtyShipped FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemCode;
   select   numItemID, numWareHouseID INTO v_numItemCode,v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;

   IF v_bitKitParent = true then
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         numItemCode NUMERIC(18,0),
         numParentItemID NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         vcItemName VARCHAR(300),
         bitKitParent BOOLEAN,
         numWarehouseItmsID NUMERIC(18,0),
         numQtyItemsReq DOUBLE PRECISION,
         numQtyItemReq_Orig DOUBLE PRECISION,
         numOnHand DOUBLE PRECISION,
         numAllocation DOUBLE PRECISION,
         numAvailableQtyToShip DOUBLE PRECISION,
         numWOID NUMERIC(18,0)
      );
      INSERT INTO
      tt_TEMPTABLE
      SELECT
      OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig*(coalesce(OI.numUnitHour,0) -coalesce(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			coalesce(WI.numOnHand,0),
			coalesce(WI.numAllocation,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),
      0),
			0,
			coalesce((SELECT numWOId FROM WorkOrder WHERE numOppId = OKI.numOppId AND numOppItemID = OKI.numOppItemID AND numOppChildItemID = OKI.numOppChildItemID AND coalesce(numOppKitChildItemID,0) = 0 AND coalesce(numParentWOID,0) = 0),0)
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      OpportunityItems OI
      ON
      OKI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      WHERE
      OI.numoppitemtCode = v_numOppItemCode;
      INSERT INTO
      tt_TEMPTABLE
      SELECT
      OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig*t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			coalesce(WI.numOnHand,0),
			coalesce(WI.numAllocation,0)+coalesce((SELECT
         SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numItemID = WI.numItemID
         AND WIInner.numWareHouseID = WI.numWareHouseID
         AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),
      0),
			0,
			coalesce((SELECT numWOId FROM WorkOrder WHERE numOppId = OKCI.numOppId AND numOppItemID = OKCI.numOppItemID AND numOppChildItemID = OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND coalesce(numParentWOID,0) = 0),0)
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      tt_TEMPTABLE t1
      ON
      OKCI.numOppChildItemID = t1.numOppChildItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode;
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
      IF v_tintCommitAllocation = 2 AND coalesce(v_bitAllocateInventoryOnPickList,true) = false then
		
         UPDATE
         tt_TEMPTABLE
         SET
         numAvailableQtyToShip =(CASE
         WHEN coalesce(numWOID,0) > 0
         THEN(CASE
            WHEN coalesce(numOnHand,0) = 0 THEN 0
            WHEN  GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq) >= numQtyItemsReq THEN(numQtyItemsReq/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            ELSE FLOOR(GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            END)
         ELSE(CASE
            WHEN coalesce(numOnHand,0) = 0 THEN 0
            WHEN  numOnHand >= numQtyItemsReq THEN(numQtyItemsReq/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            ELSE FLOOR(coalesce(numOnHand,0)/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            END)
         END)
         WHERE
         bitKitParent = false;
      ELSE
         UPDATE
         tt_TEMPTABLE
         SET
         numAvailableQtyToShip =(CASE
         WHEN coalesce(numWOID,0) > 0
         THEN(CASE
            WHEN coalesce(numOnHand,0) = 0 THEN 0
            WHEN  GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq) >= numQtyItemsReq THEN(numQtyItemsReq/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            ELSE FLOOR(GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            END)
         ELSE(CASE
            WHEN coalesce(numAllocation,0) = 0 THEN 0
            WHEN  numAllocation >= numQtyItemsReq THEN(numQtyItemsReq/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            ELSE FLOOR(coalesce(numAllocation,0)/(CASE WHEN coalesce(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
            END)
         END)
         WHERE
         bitKitParent = false;
      end if;

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
      UPDATE
      tt_TEMPTABLE t2
      SET
      numAvailableQtyToShip = CEIL((SELECT MAX(numAvailableQtyToShip) FROM tt_TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode)/(CASE WHEN coalesce(t2.numQtyItemReq_Orig,0) > 0 THEN t2.numQtyItemReq_Orig ELSE 1 END))
		
      WHERE
      bitKitParent = true;
      IF(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE bitKitParent = false) > 0 then
		
         select   MIN(numAvailableQtyToShip) INTO v_numAvailableQtyToShip FROM tt_TEMPTABLE WHERE coalesce(numParentItemID,0) = 0;
      ELSE
			-- KIT WITH ALL CHILD ITEMS OF TYPE NON INVENTORY OR SERVICE ITEMS
         v_numAvailableQtyToShip := v_numQuantity;
      end if;
   ELSE
      IF v_tintCommitAllocation = 2 AND coalesce(v_bitAllocateInventoryOnPickList,true) = false then
		
         select   SUM(numOnHand) INTO v_numOnHand FROM
         WareHouseItems WHERE
         numItemID = v_numItemCode
         AND numWareHouseID = v_numWarehouseID;
         IF(v_numQuantity -v_numQtyShipped) >= v_numOnHand then
            v_numAvailableQtyToShip := v_numOnHand;
         ELSE
            v_numAvailableQtyToShip :=(v_numQuantity -v_numQtyShipped);
         end if;
      ELSE
         select   numAllocation INTO v_numAllocation FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWarehouseItemID;
         v_numAllocation := coalesce(v_numAllocation,0)+coalesce((SELECT
         SUM(coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0))
         FROM
         WareHouseItems
         WHERE
         numItemID = v_numItemCode
         AND numWareHouseID = v_numWarehouseID
         AND numWareHouseItemID <> v_numWarehouseItemID),0);
         IF(v_numQuantity -v_numQtyShipped) >= v_numAllocation then
            v_numAvailableQtyToShip := v_numAllocation;
         ELSE
            v_numAvailableQtyToShip :=(v_numQuantity -v_numQtyShipped);
         end if;
      end if;
   end if;

   RETURN v_numAvailableQtyToShip;
END; $$;

