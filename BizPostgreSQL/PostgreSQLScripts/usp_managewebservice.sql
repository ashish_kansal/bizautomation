-- Stored procedure definition script usp_ManageWebService for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageWebService(v_numWebServiceId NUMERIC(9,0) DEFAULT 0 ,
	v_vcToken VARCHAR(500) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_numUserCntId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM WebService WHERE numDomainID = v_numDomainID) = 0 then
	
      INSERT INTO WebService(vcToken,
			numDomainID,
			dtCreatedDate,
			numCreatedby,
			dtModifiedDate,
			numModifiedBy)
		VALUES(v_vcToken,
			v_numDomainID,
			TIMEZONE('UTC',now()),
			v_numUserCntId,
			TIMEZONE('UTC',now()),
			v_numUserCntId);
   ELSE
      UPDATE WebService SET
      vcToken = v_vcToken,numDomainID = v_numDomainID,dtModifiedDate = TIMEZONE('UTC',now()),
      numModifiedBy = v_numUserCntId
      WHERE numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;




