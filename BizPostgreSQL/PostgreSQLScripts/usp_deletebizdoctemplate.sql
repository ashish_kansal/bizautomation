-- Stored procedure definition script USP_DeleteBizDocTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_DeleteBizDocTemplate(v_numDomainID NUMERIC,
v_numBizDocTempID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM BizDocTemplate WHERE numDomainID = v_numDomainID AND numBizDocTempID = v_numBizDocTempID;
   RETURN;
END; $$;

