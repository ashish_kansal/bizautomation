-- Stored procedure definition script usp_InsertEditSurvey for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertEditSurvey(v_numSurID NUMERIC,                  
 v_numDomainID NUMERIC,                 
 v_numUserID NUMERIC,                  
 v_vcSurveyInformation TEXT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql                  
--This procedure will Save Survey based on input parameters.                  
   AS $$
   DECLARE
   v_iDoc  INTEGER;
BEGIN
   IF v_numSurID = 0 then
      INSERT INTO SurveyMaster(vcSurName,
   vcRedirectURL,
   numDomainID,
   numCreatedBy,
   bintCreatedOn,
   numModifiedBy,
   bintModifiedOn,
   bitSkipRegistration,
   bitRandomAnswers,
   bitSinglePageSurvey,
   bitPostRegistration,
   bitRelationshipProfile,
   bitSurveyRedirect,
   bitEmailTemplateContact,
   bitEmailTemplateRecordOwner,numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage,vcLandingPageYes,vcLandingPageNo,vcSurveyRedirect,tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId)
      SELECT vcSurName, vcRedirectURL, v_numDomainID, v_numUserID,
   TIMEZONE('UTC',now()), v_numUserID, TIMEZONE('UTC',now()), bitSkipRegistration, bitRandomAnswers, bitSinglePageSurvey,
   bitPostRegistration, bitRelationshipProfile,bitSurveyRedirect,bitEmailTemplateContact,bitEmailTemplateRecordOwner,
   numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage ,vcLandingPageYes,vcLandingPageNo,vcSurveyRedirect  ,tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId
      FROM 
	  XMLTABLE
		(
			'NewDataSet/SurveyMaster'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcSurName VARCHAR(100) PATH 'vcSurName',
				vcRedirectURL VARCHAR(225) PATH 'vcRedirectURL',
				bitSkipRegistration BOOLEAN PATH 'bitSkipRegistration',
				bitRandomAnswers BOOLEAN PATH 'bitRandomAnswers',
				bitSinglePageSurvey BOOLEAN PATH 'bitSinglePageSurvey',
				bitPostRegistration BOOLEAN PATH 'bitPostRegistration',
				bitRelationshipProfile BOOLEAN PATH 'bitRelationshipProfile',
				bitSurveyRedirect BOOLEAN PATH 'bitSurveyRedirect',
				bitEmailTemplateContact BOOLEAN PATH 'bitEmailTemplateContact',
				bitEmailTemplateRecordOwner BOOLEAN PATH 'bitEmailTemplateRecordOwner',
				numRelationShipId NUMERIC PATH 'numRelationShipId',
				numEmailTemplate1Id NUMERIC PATH 'numEmailTemplate1Id',
				numEmailTemplate2Id NUMERIC PATH 'numEmailTemplate2Id',
				bitLandingPage BOOLEAN PATH 'bitLandingPage',
				vcLandingPageYes VARCHAR PATH 'vcLandingPageYes',
				vcLandingPageNo VARCHAR PATH 'vcLandingPageNo',
				vcSurveyRedirect VARCHAR PATH 'vcSurveyRedirect',
				tIntCRMType SMALLINT PATH 'tIntCRMType',
				numGrpId NUMERIC PATH 'numGrpId',
				numRecOwner NUMERIC PATH 'numRecOwner',
				bitCreateRecord BOOLEAN PATH 'bitCreateRecord',
				numProfileId NUMERIC PATH 'numProfileId'
		) AS X RETURNING numSurID INTO v_numSurID;

      INSERT INTO SurveyQuestionMaster(numQID,
   numSurID,
   vcQuestion,
   tIntAnsType,
   intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn)
      SELECT numQID, v_numSurID as numSurID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn  FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyQuestionMaster','numQID |vcQuestion |tIntAnsType |intNumOfAns |boolDeleted |boolMatrixColumn |intNumOfMatrixColumn') SWA_OpenXml(numQID NUMERIC,vcQuestion VARCHAR(250),tIntAnsType SMALLINT,intNumOfAns INTEGER,
      boolDeleted BOOLEAN,boolMatrixColumn BOOLEAN,intNumOfMatrixColumn INTEGER) WHERE boolDeleted = false;
      INSERT INTO SurveyAnsMaster(numQID,
   numAnsID,
   numSurID,
   vcAnsLabel,
   boolSurveyRuleAttached)
      SELECT numQID, numAnsID, v_numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyAnsMaster','numQID |numAnsID |vcAnsLabel |boolSurveyRuleAttached |boolDeleted') SWA_OpenXml(numQID NUMERIC,numAnsID NUMERIC,vcAnsLabel VARCHAR(100),boolSurveyRuleAttached BOOLEAN,
      boolDeleted BOOLEAN) WHERE boolDeleted = false;
      INSERT INTO SurveyWorkflowRules(numAnsID,
   numRuleID,
   numQID,
   numSurID,
   boolActivation,
   numEventOrder,
   vcQuestionList,
   numLinkedSurID,
   numResponseRating,
   vcPopUpURL,
   numGrpId,
   numCompanyType,
   tIntCRMType,
   numRecOwner  ,
tIntRuleFourRadio,
tIntCreateNewID,
vcRuleFourSelectFields,
boolPopulateSalesOpportunity,
vcRuleFiveSelectFields,numMatrixID,
vcItem)
      SELECT numAnsID, numRuleID, numQID, v_numSurID as numSurID, boolActivation,
   numEventOrder, vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL,
   numGrpId, numCompanyType, tIntCRMType, numRecOwner , tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,
boolPopulateSalesOpportunity, vcRuleFiveSelectFields,numMatrixID,vcItem FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyWorkflowRules','numAnsID |numRuleID |numQID |numSurID |boolActivation |numEventOrder |vcQuestionList |numLinkedSurID |numResponseRating |vcPopUpURL |numGrpId |numCompanyType |tIntCRMType |numRecOwner |tIntRuleFourRadio |tIntCreateNewID |vcRuleFourSelectFields |boolPopulateSalesOpportunity |vcRuleFiveSelectFields |vcItem |numMatrixID') SWA_OpenXml(numAnsID NUMERIC,numRuleID NUMERIC,numQID NUMERIC,numSurID NUMERIC,boolActivation BOOLEAN,
      numEventOrder INTEGER,vcQuestionList VARCHAR(50),numLinkedSurID DOUBLE PRECISION,
      numResponseRating SMALLINT,vcPopUpURL VARCHAR(100),
      numGrpId NUMERIC,numCompanyType NUMERIC,tIntCRMType SMALLINT,
      numRecOwner NUMERIC,tIntRuleFourRadio SMALLINT,tIntCreateNewID SMALLINT,
      vcRuleFourSelectFields VARCHAR(500),boolPopulateSalesOpportunity BOOLEAN,
      vcRuleFiveSelectFields VARCHAR(500),vcItem VARCHAR(500),numMatrixID NUMERIC) WHERE boolActivation = true;
      INSERT INTO SurveyMatrixAnsMaster(numQID,
   numMatrixID,
   numSurID,
   vcAnsLabel,
   boolSurveyRuleAttached)
      SELECT numQID, numMatrixID, v_numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyMatrixMaster','numQID |numMatrixID |vcAnsLabel |boolSurveyRuleAttached |boolDeleted') SWA_OpenXml(numQID NUMERIC,numMatrixID NUMERIC,vcAnsLabel VARCHAR(100),boolSurveyRuleAttached BOOLEAN,
      boolDeleted BOOLEAN) WHERE boolDeleted = false;
      INSERT INTO SurveyCreateRecord(numSurID,
   numCreateRecordId, numRatingMin ,numRatingMax,
   numRelationShipId,  numGrpId     ,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault)
      SELECT v_numSurID as numSurID,numCreateRecordId,numRatingMin,numRatingMax,numRelationShipId,
    numGrpId,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault
      FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyCreateRecord','numCreateRecordId |numRatingMin |numRatingMax |numRelationShipId |numGrpId |numRecOwner |tIntCRMType |numProfileId |numFollowUpStatus |bitDefault') SWA_OpenXml(numCreateRecordId NUMERIC,numRatingMin NUMERIC,numRatingMax NUMERIC,numRelationShipId NUMERIC,
      numGrpId NUMERIC,numRecOwner NUMERIC,tIntCRMType SMALLINT,
      numProfileId NUMERIC,numFollowUpStatus NUMERIC,bitDefault BOOLEAN);
      PERFORM SWF_Xml_RemoveDocument(v_iDoc);                  
   --Return the New ID.                  
      open SWV_RefCur for
      SELECT v_numSurID;
   Else
      SELECT * INTO v_iDoc FROM SWF_Xml_PrepareDocument(v_vcSurveyInformation);
      UPDATE SurveyMaster
      SET vcSurName = XMLTABLE.vcSurName,vcRedirectURL = XMLTABLE.vcRedirectURL,
      numModifiedBy = v_numUserID,bintModifiedOn =  TIMEZONE('UTC',now()),bitSkipRegistration = XMLTABLE.bitSkipRegistration,
      bitRandomAnswers = XMLTABLE.bitRandomAnswers,
      bitSinglePageSurvey = XMLTABLE.bitSinglePageSurvey,
      bitPostRegistration = XMLTABLE.bitPostRegistration,bitRelationshipProfile = XMLTABLE.bitRelationshipProfile,
      bitSurveyRedirect = XMLTABLE.bitSurveyRedirect,
      bitEmailTemplateContact = XMLTABLE.bitEmailTemplateContact,
      bitEmailTemplateRecordOwner = XMLTABLE.bitEmailTemplateRecordOwner,numRelationShipId = XMLTABLE.numRelationShipId,
      numEmailTemplate1Id = XMLTABLE.numEmailTemplate1Id,
      numEmailTemplate2Id = XMLTABLE.numEmailTemplate2Id,
      bitLandingPage = XMLTABLE.bitLandingPage,vcLandingPageYes = XMLTABLE.vcLandingPageYes,
      vcLandingPageNo = XMLTABLE.vcLandingPageNo,vcSurveyRedirect = XMLTABLE.vcSurveyRedirect,
      tIntCRMType = XMLTABLE.tIntCRMType,numGrpId = XMLTABLE.numGrpId,
      numRecOwner = XMLTABLE.numRecOwner,bitCreateRecord = XMLTABLE.bitCreateRecord,
      numProfileId = XMLTABLE.numProfileId
      FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyMaster','SurveyMaster') SWA_OpenXml(SurveyMaster XMLTABLE)
      WHERE SurveyMaster.numSurID = v_numSurID;
      DELETE FROM SurveyQuestionMaster Where numSurID = v_numSurID;
      INSERT INTO SurveyQuestionMaster(numQID,
   numSurID,
   vcQuestion,
   tIntAnsType,
   intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn)
      SELECT numQID, v_numSurID as numSurID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn  FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyQuestionMaster','numQID |vcQuestion |tIntAnsType |intNumOfAns |boolDeleted |boolMatrixColumn |intNumOfMatrixColumn') SWA_OpenXml(numQID NUMERIC,vcQuestion VARCHAR(250),tIntAnsType SMALLINT,intNumOfAns INTEGER,
      boolDeleted BOOLEAN,boolMatrixColumn BOOLEAN,intNumOfMatrixColumn INTEGER) WHERE boolDeleted = false;
      DELETE FROM SurveyAnsMaster Where numSurID = v_numSurID;
      INSERT INTO SurveyAnsMaster(numQID,
   numAnsID,
   numSurID,
   vcAnsLabel,
   boolSurveyRuleAttached)
      SELECT numQID, numAnsID, v_numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyAnsMaster','numQID |numAnsID |vcAnsLabel |boolSurveyRuleAttached |boolDeleted') SWA_OpenXml(numQID NUMERIC,numAnsID NUMERIC,vcAnsLabel VARCHAR(100),boolSurveyRuleAttached BOOLEAN,
      boolDeleted BOOLEAN) WHERE boolDeleted = false;
      DELETE FROM SurveyWorkflowRules WHERE numSurID = v_numSurID;
      INSERT INTO SurveyWorkflowRules(numAnsID,
   numRuleID,
   numQID,
   numSurID,
   boolActivation,
   numEventOrder,
   vcQuestionList,
   numLinkedSurID,
   numResponseRating,
   vcPopUpURL,
   numGrpId,
   numCompanyType,
   tIntCRMType,
   numRecOwner,
tIntRuleFourRadio,
tIntCreateNewID,
vcRuleFourSelectFields,
boolPopulateSalesOpportunity,
vcRuleFiveSelectFields,numMatrixID,
vcItem)
      SELECT numAnsID, numRuleID, numQID, v_numSurID as numSurID, boolActivation,
   numEventOrder, vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL,
   numGrpId, numCompanyType, tIntCRMType, numRecOwner, tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,
boolPopulateSalesOpportunity, vcRuleFiveSelectFields,numMatrixID,vcItem FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyWorkflowRules','numAnsID |numRuleID |numQID |numSurID |boolActivation |numEventOrder |vcQuestionList |numLinkedSurID |numResponseRating |vcPopUpURL |numGrpId |numCompanyType |tIntCRMType |numRecOwner |tIntRuleFourRadio |tIntCreateNewID |vcRuleFourSelectFields |boolPopulateSalesOpportunity |vcRuleFiveSelectFields |vcItem |numMatrixID') SWA_OpenXml(numAnsID NUMERIC,numRuleID NUMERIC,numQID NUMERIC,numSurID NUMERIC,boolActivation BOOLEAN,
      numEventOrder INTEGER,vcQuestionList VARCHAR(50),numLinkedSurID DOUBLE PRECISION,
      numResponseRating SMALLINT,vcPopUpURL VARCHAR(100),
      numGrpId NUMERIC,numCompanyType NUMERIC,tIntCRMType SMALLINT,
      numRecOwner NUMERIC,tIntRuleFourRadio SMALLINT,tIntCreateNewID SMALLINT,
      vcRuleFourSelectFields VARCHAR(500),boolPopulateSalesOpportunity BOOLEAN,
      vcRuleFiveSelectFields VARCHAR(500),vcItem VARCHAR(500),numMatrixID NUMERIC) WHERE boolActivation = true;
      DELETE FROM SurveyMatrixAnsMaster Where numSurID = v_numSurID;
      INSERT INTO SurveyMatrixAnsMaster(numQID,
   numMatrixID,
   numSurID,
   vcAnsLabel,
   boolSurveyRuleAttached)
      SELECT numQID, numMatrixID, v_numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyMatrixMaster','numQID |numMatrixID |vcAnsLabel |boolSurveyRuleAttached |boolDeleted') SWA_OpenXml(numQID NUMERIC,numMatrixID NUMERIC,vcAnsLabel VARCHAR(100),boolSurveyRuleAttached BOOLEAN,
      boolDeleted BOOLEAN) WHERE boolDeleted = false;
      DELETE FROM SurveyCreateRecord Where numSurID = v_numSurID;
      INSERT INTO SurveyCreateRecord(numSurID,
   numCreateRecordId, numRatingMin ,numRatingMax,
   numRelationShipId,  numGrpId     ,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus ,bitDefault)
      SELECT v_numSurID as numSurID,numCreateRecordId,numRatingMin,numRatingMax,numRelationShipId,
    numGrpId,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault
      FROM SWF_OpenXml(v_iDoc,'/NewDataSet/SurveyCreateRecord','numCreateRecordId |numRatingMin |numRatingMax |numRelationShipId |numGrpId |numRecOwner |tIntCRMType |numProfileId |numFollowUpStatus |bitDefault') SWA_OpenXml(numCreateRecordId NUMERIC,numRatingMin NUMERIC,numRatingMax NUMERIC,numRelationShipId NUMERIC,
      numGrpId NUMERIC,numRecOwner NUMERIC,tIntCRMType SMALLINT,
      numProfileId NUMERIC,numFollowUpStatus NUMERIC,bitDefault BOOLEAN);
      PERFORM SWF_Xml_RemoveDocument(v_iDoc);                  
   --Return the New ID.                  
      open SWV_RefCur for
      SELECT v_numSurID;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_InsertEmailAddCCAndBCC]    Script Date: 07/26/2008 16:19:06 ******/



