CREATE OR REPLACE FUNCTION USP_DeleteBankStatementTransactions(v_numTransactionID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   DELETE FROM BankStatementTransactions
   WHERE
   numTransactionID = v_numTransactionID
   AND numReferenceID IS NULL OR numReferenceID = 0;
   RETURN;
END; $$;



