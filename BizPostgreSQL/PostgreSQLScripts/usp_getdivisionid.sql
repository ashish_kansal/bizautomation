-- Stored procedure definition script USP_GetDivisionId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivisionId(v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_numOppId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numDivisionId,0) as NUMERIC(18,0)) From OpportunityMaster Where numDomainId = v_numDomainId  And numOppId = v_numOppId;
END; $$;












