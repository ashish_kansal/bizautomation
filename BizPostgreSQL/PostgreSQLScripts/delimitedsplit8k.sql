CREATE OR REPLACE FUNCTION DelimitedSplit8K
(
	v_pString VARCHAR(7999)
	,v_pDelimiter VARCHAR(3)
)
RETURNS TABLE
(
   ItemNumber VARCHAR(255),
   Item TEXT
) LANGUAGE plpgsql
AS $$
BEGIN
RETURN
	query WITH E1(N) AS
	( --=== Create Ten 1's
		SELECT 1 AS N UNION ALL SELECT 1 AS N UNION ALL
		SELECT 1 AS N UNION ALL SELECT 1 AS N UNION ALL
		SELECT 1 AS N UNION ALL SELECT 1 AS N UNION ALL
		SELECT 1 AS N UNION ALL SELECT 1 AS N UNION ALL
		SELECT 1 AS N UNION ALL SELECT 1 AS N --10
    ),E2(N) AS
	(
		SELECT 1 AS N FROM E1 a LEFT JOIN LATERAL(select * from E1 b) AS TabAl on TRUE
	),   --100
    E4(N) AS
	(
		SELECT 1 AS N FROM E2 a LEFT JOIN LATERAL(select * from E2 b) AS TabAl on TRUE
	),   --10,000
	cteTally(N) AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY(SELECT N)) AS N FROM E4
	)  
	--===== Do the split
	SELECT 
		CAST(ROW_NUMBER() OVER(ORDER BY N) AS VARCHAR) AS ItemNumber
		,SUBSTR(v_pString,CAST(N AS INT),(CASE 
											WHEN POSITION(v_pDelimiter IN SUBSTR(coalesce(v_pString,'') || coalesce(v_pDelimiter,''),CAST(N AS INT))) > 0 
											THEN POSITION(v_pDelimiter IN SUBSTR(coalesce(v_pString,'') || coalesce(v_pDelimiter,''),CAST(N AS INT))) + CAST(N AS INT) -1 
											ELSE CAST(0 AS INT)
											END) - CAST(N AS INT)) AS Item
	FROM 
		cteTally
	WHERE 
		N < LENGTH(v_pString) + 2
		AND SUBSTR(coalesce(v_pDelimiter,'') || coalesce(v_pString,''),CAST(N AS INT),LENGTH(v_pDelimiter)) = v_pDelimiter;
END; $$;

