-- Stored procedure definition script usp_caseTime1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_caseTime1(v_byteMode  SMALLINT,                
v_numCaseId NUMERIC(9,0),     
v_numCaseTimeId NUMERIC,    
v_numRate DECIMAL(20,5),                
v_vcDesc VARCHAR(1000),            
v_bitBillable BOOLEAN,     
v_numcontractId NUMERIC(9,0),       
v_dtStartTime TIMESTAMP,      
v_dtEndTime TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetCaseTimeId  INTEGER;
BEGIN
   if v_byteMode = 0 then
      delete from CaseTime where numCaseId = v_numCaseId and numCaseTimeId = v_numCaseTimeId;
      insert into CaseTime(numCaseId,
numRate,
vcDesc,
bitBill,
numContractId,
dtStartTime,
dtEndTime)
values(v_numCaseId,
v_numRate,
v_vcDesc,
v_bitBillable,
v_numcontractId ,
v_dtStartTime,
v_dtEndTime);
  
      v_numCaseTimeId := CURRVAL('CaseTime_seq');
      v_RetCaseTimeId := CURRVAL('CaseTime_seq');
      open SWV_RefCur for
      Select  v_numCaseTimeId;
   end if;
              
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numCaseId,
numCaseTimeId,
numRate,
vcDesc,
bitBill,
numContractId ,
dtStartTime,
dtEndTime
      from CaseTime
      where numCaseId = v_numCaseId and numCaseTimeId = v_numCaseTimeId;
   end if;            
        
   if v_byteMode = 2 then

      delete from CaseTime where numCaseId = v_numCaseId and numCaseTimeId = v_numCaseTimeId;
   end if;
   RETURN;
END; $$;


