DROP FUNCTION IF EXISTS USP_WFDateFieldsDataSync;

CREATE OR REPLACE FUNCTION USP_WFDateFieldsDataSync()
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 3) AS DATE) <> CAST(LOCALTIMESTAMP  AS DATE) AND EXTRACT(HOUR FROM LOCALTIMESTAMP) > 1 then
	
      delete from AdditionalContactsInformation_TempDateFields;
      delete from Cases_TempDateFields;
      delete from Communication_TempDateFields;
      delete from DivisionMaster_TempDateFields;
      delete from OpportunityMaster_TempDateFields;
      delete from ProjectsMaster_TempDateFields;
      delete from StagePercentageDetails_TempDateFields;
      INSERT INTO AdditionalContactsInformation_TempDateFields(numContactId, numDomainID, bintCreatedDate, bintDOB)
      SELECT numContactId,numDomainID, bintCreatedDate,bintDOB
      FROM AdditionalContactsInformation
      WHERE (
				(bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(bintDOB >= LOCALTIMESTAMP+INTERVAL '-30 day' AND bintDOB <= LOCALTIMESTAMP+INTERVAL '90 day'));

		---------------------------------------------------------------------------------
      INSERT INTO Cases_TempDateFields(numCaseId, numDomainID, bintCreatedDate, intTargetResolveDate)
      SELECT numCaseId,numDomainID, bintCreatedDate, intTargetResolveDate
      FROM Cases
      WHERE (
				(bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(intTargetResolveDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND intTargetResolveDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'));

		------------------------------------------------------------------------------------

      INSERT INTO Communication_TempDateFields(numCommId, numDomainID, dtStartTime)
      SELECT numCommId,numDomainID, dtStartTime
      FROM Communication
      WHERE (
				(dtStartTime >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND dtStartTime <= TIMEZONE('UTC',now())+INTERVAL '90 day'));

		----------------------------------------------------------------------------------------------

      INSERT INTO DivisionMaster_TempDateFields(numDivisionID, numDomainID, bintCreatedDate)
      SELECT numDivisionID,numDomainID, bintCreatedDate
      FROM DivisionMaster
      WHERE (
				(bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'));
		------------------------------------------------------------------------------------------------
      INSERT INTO OpportunityMaster_TempDateFields(numOppId, numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate)
      SELECT numOppId,numDomainId, bintClosedDate, bintCreatedDate,intPEstimatedCloseDate,dtReleaseDate
      FROM OpportunityMaster
      WHERE (
				(bintClosedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintClosedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(intPEstimatedCloseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND intPEstimatedCloseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(dtReleaseDate >= LOCALTIMESTAMP+INTERVAL '-30 day' AND dtReleaseDate <= LOCALTIMESTAMP+INTERVAL '90 day'));			 

		--------------------------------------------------------------------------------------------------------

      INSERT INTO ProjectsMaster_TempDateFields(numProId, numDomainID, bintCreatedDate, bintProClosingDate,intDueDate)
      SELECT numProId,numdomainId, bintCreatedDate, bintProClosingDate, intDueDate
      FROM ProjectsMaster
      WHERE (
				(bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(bintProClosingDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND bintProClosingDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(intDueDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND intDueDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'));

		--------------------------------------------------------------------------------------------------------
      INSERT INTO StagePercentageDetails_TempDateFields(numStageDetailsId, numDomainID, dtEndDate, dtStartDate)
      SELECT numStageDetailsId,numdomainid, dtEndDate, dtStartDate
      FROM StagePercentageDetails
      WHERE (
				(dtEndDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND dtEndDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(dtStartDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND dtStartDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'));

		-------------------------------------------------------------------------------------------------------

      UPDATE ElasticSearchLastReindex SET LastReindexDate = LOCALTIMESTAMP  WHERE ID = 3;
   end if;
   RETURN;
END; $$;




