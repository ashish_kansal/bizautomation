CREATE OR REPLACE FUNCTION public.usp_getcartitem(
	v_numusercntid numeric,
	v_numdomainid numeric,
	v_vccookieid character varying,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF v_numUserCntId <> 0 then
	
      open SWV_RefCur for
      SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   CartItems.numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   CartItems.numItemCode,
			   (SELECT  numCategoryID  FROM ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId = v_numDomainId LIMIT 1) AS numCategoryId,
			   (SELECT vcCategoryName FROM Category WHERE Category.numCategoryID  =(SELECT  numCategoryID  FROM ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId = v_numDomainId LIMIT 1)) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS monListPrice,
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   CartItems.numWarehouseId,
			   CartItems.vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainId AND bitDefault = true AND numItemCode = I.numItemCode LIMIT 1),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST(dtDeliveryDate AS VARCHAR(30)) AS dtDeliveryDate,monTotAmount,
			   coalesce(monTotAmtBefDiscount,0) -coalesce(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM Item WHERE Item.numItemCode =  CartItems.numItemCode) AS vcModelID
			   ,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId = CartItems.numDomainId AND II.bitDefault = true LIMIT 1) As vcPathForTImage
			   ,coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc,coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell, coalesce(vcUpSellDesc,'') AS vcUpSellDesc
			   ,coalesce(I.vcSKU,'') AS vcSKU
			   ,I.vcManufacturer
			   ,coalesce(PromotionID,0) AS PromotionID
			   ,CAST(coalesce(PromotionDesc,'') AS VARCHAR(30)) AS PromotionDesc
			   ,coalesce((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = CartItems.PromotionID),False) AS IsOrderBasedPromotion
			   ,I.txtItemDesc AS txtItemDesc,coalesce(vcChildKitItemSelection,'') AS vcChildKitItemSelection,
			   (CASE WHEN coalesce(bitKitParent,false) = true THEN GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) AS vcInclusionDetails,
			   I.numItemClassification
      FROM CartItems
      LEFT JOIN SimilarItems AS SI ON SI.numItemCode = CartItems.numItemCode AND SI.numParentItemCode = CartItems.numParentItem
      LEFT JOIN Item AS I ON I.numItemCode = CartItems.numItemCode
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
      WHERE CartItems.numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId;
   ELSE
      open SWV_RefCur for
      SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   CartItems.numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   CartItems.numItemCode,
			   (SELECT  numCategoryID  FROM ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId = v_numDomainId LIMIT 1) AS numCategoryId,
			   (SELECT vcCategoryName FROM Category WHERE Category.numCategoryID  =(SELECT  numCategoryID  FROM ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId = v_numDomainId LIMIT 1)) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS monListPrice,
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   CartItems.numWarehouseId,
			   CartItems.vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainId AND bitDefault = true AND numItemCode = I.numItemCode LIMIT 1),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST(dtDeliveryDate AS VARCHAR(30)) AS dtDeliveryDate,
			   monTotAmount,
			   coalesce(monTotAmtBefDiscount,0) -coalesce(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM Item WHERE Item.numItemCode =  CartItems.numItemCode) AS vcModelID
			   ,(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId = CartItems.numDomainId AND II.bitDefault = true LIMIT 1) As vcPathForTImage
			   , coalesce(vcUpSellDesc,'') AS txtUpSellItemDesc,coalesce(bitPreUpSell,false) AS bitPreUpSell, coalesce(bitPostUpSell,false) AS bitPostUpSell, coalesce(vcUpSellDesc,'') AS vcUpSellDesc
			   ,coalesce(I.vcSKU,'') AS vcSKU
			   ,I.vcManufacturer
			   ,coalesce(PromotionID,0) AS PromotionID
			   ,CAST(coalesce(PromotionDesc,'') AS VARCHAR(30)) AS PromotionDesc
			   ,coalesce((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = CartItems.PromotionID),False) AS IsOrderBasedPromotion
			   ,coalesce(vcChildKitItemSelection,'') AS vcChildKitItemSelection
			   ,(CASE WHEN coalesce(bitKitParent,false) = true THEN GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) AS vcInclusionDetails,
			   I.numItemClassification
      FROM CartItems
      LEFT JOIN SimilarItems AS SI ON SI.numItemCode = CartItems.numItemCode AND SI.numParentItemCode = CartItems.numParentItem
      LEFT JOIN Item AS I ON I.numItemCode = CartItems.numItemCode
      WHERE CartItems.numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId AND vcCookieId = v_vcCookieId;
   end if;
	
   RETURN;
END;
$BODY$;

