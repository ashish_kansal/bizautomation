-- Stored procedure definition script usp_InsertStageOpport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertStageOpport(v_numOppID  NUMERIC(9,0) DEFAULT 0,
	v_numstageID NUMERIC(9,0) DEFAULT 0,
	v_slp_id  INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	--Insertion into Table  
             --delete from stageOpportunity where numOppID=@numOppID
	
   INSERT INTO stageOpportunity(numOppID,numstageID,slp_id)
	VALUES(v_numOppID,v_numstageID,v_slp_id);
RETURN;
END; $$;


