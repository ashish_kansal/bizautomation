-- Stored procedure definition script USP_InLineEditPropertyName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InLineEditPropertyName(v_numDomainID NUMERIC(9,0),
 v_numUserCntID NUMERIC(9,0),
 v_numFormFieldId NUMERIC(9,0),
 v_bitCustom BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitCustom = false then
	
      open SWV_RefCur for
      SELECT
      vcDbColumnName
			,vcOrigDbCOlumnName
			,coalesce(vcPropertyName,'') AS vcPropertyName
			,numListID
			,CASE
      WHEN vcAssociatedControlType = 'SelectBox'
      THEN coalesce((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = numListID and numDomainID = v_numDomainID),0)
      ELSE 0
      END AS ListRelID
			,vcListItemType
      FROM
      DycFieldMaster
      WHERE
      numFieldID = v_numFormFieldId;
   ELSE
      open SWV_RefCur for
      SELECT
      '' AS vcDbColumnName
			,'' AS vcOrigDbColumnName
			,'' AS vcPropertyName
			,numlistid
			,CASE
      WHEN fld_type = 'SelectBox'
      THEN coalesce((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = numListID AND numDomainID = v_numDomainID),0)
      ELSE 0
      END AS ListRelID
      FROM
      CFW_Fld_Master
      WHERE
      Fld_id = v_numFormFieldId;
   end if;
   RETURN;
END; $$;


