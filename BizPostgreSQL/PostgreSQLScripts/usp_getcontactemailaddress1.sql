-- Stored procedure definition script USP_GetContactEmailAddress1 for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactEmailAddress1(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                      
 v_numDomainID NUMERIC(9,0) DEFAULT 0,              
 v_keyWord VARCHAR(100) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select  ADC.vcEmail
   FROM AdditionalContactsInformation ADC INNER JOIN
   DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID INNER JOIN
   CompanyInfo C ON DM.numCompanyID = C.numCompanyId
   where vcEmail <> ''
   AND ADC.numDomainID = cast(NULLIF(CAST(v_numDomainID AS VARCHAR(10)),'') as NUMERIC(18,0))
   and (ADC.vcFirstName  ilike '' || coalesce(v_keyWord,'') || '%'
   or ADC.vcLastname ilike '' || coalesce(v_keyWord,'') || '%'
   or C.vcCompanyName ilike '' || coalesce(v_keyWord,'') || '%');
END; $$;












