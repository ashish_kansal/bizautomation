-- Stored procedure definition script USP_ManageInventoryPickList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageInventoryPickList(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemCode  NUMERIC(18,0);
   v_numOppItemID  NUMERIC(18,0);
   v_bitWorkorder  BOOLEAN;
   v_numUnitHour  DOUBLE PRECISION;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_onHand  DOUBLE PRECISION;                                    
   v_onOrder  DOUBLE PRECISION; 
   v_onBackOrder  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onReOrder  DOUBLE PRECISION;
   v_bitKitParent  BOOLEAN;
   v_numOrigUnits  NUMERIC;		
   v_description  VARCHAR(100);
   v_bitAsset  BOOLEAN;
   v_dtItemReceivedDate  TIMESTAMP DEFAULT NULL;

   v_j  INTEGER DEFAULT 0;
   v_ChildCount  INTEGER;
   v_vcChildDescription  VARCHAR(300);
   v_numChildAllocation  DOUBLE PRECISION;
   v_numChildBackOrder  DOUBLE PRECISION;
   v_numChildOnHand  DOUBLE PRECISION;
   v_numOppChildItemID  INTEGER;
   v_numChildItemCode  INTEGER;
   v_bitChildIsKit  BOOLEAN;
   v_numChildWarehouseItemID  INTEGER;
   v_numChildQty  DOUBLE PRECISION;
   v_numChildQtyShipped  DOUBLE PRECISION;

   v_k  INTEGER DEFAULT 1;
   v_CountKitChildItems  INTEGER;
   v_vcKitChildDescription  VARCHAR(300);
   v_numKitChildAllocation  DOUBLE PRECISION;
   v_numKitChildBackOrder  DOUBLE PRECISION;
   v_numKitChildOnHand  DOUBLE PRECISION;
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_numKitChildWarehouseItemID  NUMERIC(18,0);
   v_numKitChildQty  DOUBLE PRECISION;
   v_numKitChildQtyShipped  DOUBLE PRECISION;

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
BEGIN
   IF coalesce(v_numUserCntID,0) = 0 then
	
      RAISE EXCEPTION 'INVALID_USERID';
      RETURN;
   end if;

   DROP TABLE IF EXISTS tt_TEMPKITSUBITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPKITSUBITEMS
   (
      ID INTEGER,
      numOppChildItemID NUMERIC(18,0),
      numChildItemCode NUMERIC(18,0),
      bitChildIsKit BOOLEAN,
      numChildWarehouseItemID NUMERIC(18,0),
      numChildQty DOUBLE PRECISION,
      numChildQtyShipped DOUBLE PRECISION
   );

   DROP TABLE IF EXISTS tt_TEMPKITCHILDKITSUBITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPKITCHILDKITSUBITEMS
   (
      ID INTEGER,
      numOppKitChildItemID NUMERIC(18,0),
      numKitChildItemCode NUMERIC(18,0),
      numKitChildWarehouseItemID NUMERIC(18,0),
      numKitChildQty DOUBLE PRECISION,
      numKitChildQtyShipped DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempPickListItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPPICKLISTITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPICKLISTITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      numWarehouseItemID NUMERIC(18,0),
      bitWorkorder BOOLEAN,
      bitAsset BOOLEAN,
      bitKitParent BOOLEAN
   );

   INSERT INTO tt_TEMPPICKLISTITEMS(numOppItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItemID
		,bitWorkorder
		,bitAsset
		,bitKitParent)
   SELECT
   OI.numoppitemtCode
		,I.numItemCode
		,OBDI.numUnitHour
		,OI.numWarehouseItmsID
		,coalesce(OI.bitWorkOrder,false)
		,coalesce(I.bitAsset,false)
		,coalesce(I.bitKitParent,false)
   FROM
   OpportunityBizDocItems OBDI
   INNER JOIN
   OpportunityItems OI
   ON
   OBDI.numOppItemID = OI.numoppitemtCode
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   charitemtype = 'P'
   AND OBDI.numOppBizDocID = v_numOppBizDocID
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OBDI.numUnitHour,0) > 0
   ORDER BY
   OI.numoppitemtCode;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPPICKLISTITEMS;

   WHILE v_i <= v_iCount LOOP
      select   numOppItemID, numItemCode, numUnitHour, numWarehouseItemID, bitKitParent, bitWorkorder, bitAsset INTO v_numOppItemID,v_numItemCode,v_numUnitHour,v_numWareHouseItemID,v_bitKitParent,
      v_bitWorkorder,v_bitAsset FROM
      tt_TEMPPICKLISTITEMS WHERE
      ID = v_i;
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0), coalesce(numReorder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder,v_onReOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;

		/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
      v_description := CONCAT('SO Pick List insert/edit (Qty:',v_numUnitHour,' Shipped:0)');
      IF v_bitKitParent = true then
		
			-- CLEAR DATA OF PREVIOUS ITERATION
         DELETE FROM tt_TEMPKITSUBITEMS;

			-- GET KIT SUB ITEMS DETAIL
         INSERT INTO tt_TEMPKITSUBITEMS(ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped)
         SELECT
         ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				coalesce(numOppChildItemID,0),
				coalesce(I.numItemCode,0),
				coalesce(I.bitKitParent,false),
				coalesce(numWareHouseItemId,0),
				((coalesce(numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numUnitHour),
				coalesce(numQtyShipped,0)
         FROM
         OpportunityKitItems OKI
         JOIN
         Item I
         ON
         OKI.numChildItemID = I.numItemCode
         WHERE
         charitemtype = 'P'
         AND coalesce(numWareHouseItemId,0) > 0
         AND OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = v_numOppItemID;
         v_j := 1;
         select   COUNT(*) INTO v_ChildCount FROM tt_TEMPKITSUBITEMS;

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
         WHILE v_j <= v_ChildCount LOOP
            select   numOppChildItemID, numChildItemCode, bitChildIsKit, numChildWarehouseItemID, numChildQty, numChildQtyShipped INTO v_numOppChildItemID,v_numChildItemCode,v_bitChildIsKit,v_numChildWarehouseItemID,
            v_numChildQty,v_numChildQtyShipped FROM
            tt_TEMPKITSUBITEMS WHERE
            ID = v_j;
            select   coalesce(numOnHand,0) INTO v_numChildOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numChildWarehouseItemID;

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
            IF v_bitChildIsKit = true then
				
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
               IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID) then
					
						-- CLEAR DATA OF PREVIOUS ITERATION
                  DELETE FROM tt_TEMPKITCHILDKITSUBITEMS;

						-- GET SUB KIT SUB ITEMS DETAIL
                  INSERT INTO tt_TEMPKITCHILDKITSUBITEMS(ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							coalesce(numOppKitChildItemID,0),
							coalesce(OKCI.numItemID,0),
							coalesce(OKCI.numWareHouseItemId,0),
							((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQty),
							coalesce(numQtyShipped,0)
                  FROM
                  OpportunityKitChildItems OKCI
                  JOIN
                  Item I
                  ON
                  OKCI.numItemID = I.numItemCode
                  WHERE
                  charitemtype = 'P'
                  AND coalesce(numWareHouseItemId,0) > 0
                  AND OKCI.numOppId = v_numOppID
                  AND OKCI.numOppItemID = v_numOppItemID
                  AND OKCI.numOppChildItemID = v_numOppChildItemID;
                  v_k := 1;
                  select   COUNT(*) INTO v_CountKitChildItems FROM tt_TEMPKITCHILDKITSUBITEMS;
                  WHILE v_k <= v_CountKitChildItems LOOP
                     select   numOppKitChildItemID, numKitChildItemCode, numKitChildWarehouseItemID, numKitChildQty, numKitChildQtyShipped INTO v_numOppKitChildItemID,v_numKitChildItemCode,v_numKitChildWarehouseItemID,
                     v_numKitChildQty,v_numKitChildQtyShipped FROM
                     tt_TEMPKITCHILDKITSUBITEMS WHERE
                     ID = v_k;
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand =(CASE WHEN coalesce(numOnHand,0) >= v_numKitChildQty THEN coalesce(numOnHand,0) -v_numKitChildQty ELSE 0 END),numAllocation =(CASE WHEN coalesce(numOnHand,0) >= v_numKitChildQty THEN coalesce(numAllocation,0)+v_numKitChildQty ELSE coalesce(numAllocation,0)+coalesce(numOnHand,0) END),
                     numBackOrder =(CASE WHEN coalesce(numOnHand,0) >= v_numKitChildQty THEN coalesce(numBackOrder,0) ELSE coalesce(numBackOrder,0)+(v_numKitChildQty -coalesce(numOnHand,0)) END),
                     dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numKitChildWarehouseItemID;
                     v_vcKitChildDescription := CONCAT('SO Pick List Child Kit insert/edit (Qty:',v_numKitChildQty,' Shipped:0)');
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numKitChildWarehouseItemID,v_numReferenceID := v_numOppID,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcKitChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                     v_k := v_k::bigint+1;
                  END LOOP;
               end if;
					 --  numeric(9, 0)
						 --  numeric(9, 0)
						 --  tinyint
						 --  varchar(100)
               v_description := CONCAT('SO Pick List Kit insert/edit (Qty:',v_numChildQty,' Shipped:0)');
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppID,
               v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,
               v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
               v_numDomainID := v_numDomainID,SWV_RefCur := null);
            ELSE
               UPDATE
               WareHouseItems
               SET
               numOnHand =(CASE WHEN coalesce(numOnHand,0) >= v_numChildQty THEN coalesce(numOnHand,0) -v_numChildQty ELSE 0 END),numAllocation =(CASE WHEN coalesce(numOnHand,0) >= v_numChildQty THEN coalesce(numAllocation,0)+v_numChildQty ELSE coalesce(numAllocation,0)+coalesce(numOnHand,0) END),
               numBackOrder =(CASE WHEN coalesce(numOnHand,0) >= v_numChildQty THEN coalesce(numBackOrder,0) ELSE coalesce(numBackOrder,0)+(v_numChildQty -coalesce(numOnHand,0)) END),dtModified = LOCALTIMESTAMP
               WHERE
               numWareHouseItemID = v_numChildWarehouseItemID;
               v_vcChildDescription := CONCAT('SO Pick List Kit insert/edit (Qty:',v_numChildQty,' Shipped:0)');
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppID,
               v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
               v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
               SWV_RefCur := null);
            end if;
            v_j := v_j::bigint+1;
         END LOOP;
      ELSE
         IF v_onHand >= v_numUnitHour then
			
            v_onHand := v_onHand -v_numUnitHour;
            v_onAllocation := v_onAllocation+v_numUnitHour;
         ELSEIF v_onHand < v_numUnitHour
         then
			
            v_onAllocation := v_onAllocation+v_onHand;
            v_onBackOrder := v_onBackOrder+v_numUnitHour -v_onHand;
            v_onHand := 0;
         end if;
         IF v_bitAsset = false then--Not Asset
			
            UPDATE
            WareHouseItems
            SET
            numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
            dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         end if;
      end if;
      IF v_numWareHouseItemID > 0 AND v_description <> 'DO NOT ADD WAREHOUSE TRACKING' then
		
			 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
         PERFORM FROM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
         v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
         v_dtRecordDate := v_dtItemReceivedDate,
         v_numDomainID := v_numDomainID,SWV_RefCur := null);
      end if;
      v_i := v_i::bigint+1;
   END LOOP;
END; $$;


