-- Stored procedure definition script USP_UpdateOrgCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOrgCampaign(v_vcContactIDList VARCHAR(8000),
    v_numCampaignID NUMERIC,
    v_numUserCntID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  DivisionMaster
   SET     numCampaignID = v_numCampaignID,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE   numDomainID = v_numDomainID
   AND numDivisionID IN(SELECT DISTINCT
   numDivisionId
   FROM    AdditionalContactsInformation
   WHERE   numContactId IN(SELECT  id
      FROM    SplitIDs(v_vcContactIDList,',')));
   RETURN;
END; $$;
--Created by Anoop Jayaraj



