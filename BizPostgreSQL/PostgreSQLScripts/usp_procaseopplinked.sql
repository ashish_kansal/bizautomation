-- Stored procedure definition script usp_ProCaseOppLinked for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ProCaseOppLinked(v_byteMode INTEGER,
v_numProId NUMERIC(9,0),
v_numCaseId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then
	
      open SWV_RefCur for
      select PO.numOppId,vcpOppName from ProjectsOpportunities PO
      join OpportunityMaster OM on PO.numOppId = OM.numOppId where numproid = v_numProId;
   else
      open SWV_RefCur for
      select CO.numoppid,vcpOppName from CaseOpportunities CO
      join OpportunityMaster OM on CO.numoppid = OM.numOppId where numCaseid = v_numCaseId;
   end if;
   RETURN;
END; $$;


