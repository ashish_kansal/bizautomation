DROP FUNCTION IF EXISTS USP_GetScalerValue;

CREATE OR REPLACE FUNCTION USP_GetScalerValue(v_numDomainID NUMERIC,--Will receive SiteID when mode =2,12,9
v_tintMode SMALLINT,
v_vcInput VARCHAR(1000),
v_numUserCntId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtEmailStartDate  TIMESTAMP;
   v_dtEmailEndDate TIMESTAMP;
   v_intNoOfEmail  NUMERIC;  
   v_numItemCode  NUMERIC(18,0);
   v_numItemID  NUMERIC(18,0);
   v_vcSKU  VARCHAR(50);
BEGIN
   IF v_tintMode = 1 then
	
      open SWV_RefCur for
      SELECT numJOurnal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainID and numBizDocsPaymentDetId::VARCHAR = v_vcInput;
   end if;
   IF v_tintMode = 2 then
	
      open SWV_RefCur for
      SELECT COUNT(*) FROM Sites WHERE vcHostName = v_vcInput AND numSiteID <> v_numDomainID;
   end if;
   IF v_tintMode = 3 then
	
      open SWV_RefCur for
      SELECT COUNT(*) FROM Domain WHERE vcPortalName = v_vcInput AND numDomainId <> v_numDomainID;
   end if;
   IF v_tintMode = 4 then --Item Name
	
      open SWV_RefCur for
      SELECT GetItemName(CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END END);
   end if;
   IF v_tintMode = 5 then --Company Name
	
      open SWV_RefCur for
      SELECT fn_GetComapnyName(CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END);
   end if;
   IF v_tintMode = 6 then --Vendor Cost
	
      open SWV_RefCur for
      SELECT fn_GetVendorCost(CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END);
   end if;
   IF v_tintMode = 7 then -- tintRuleFor
	
      open SWV_RefCur for
      SELECT tintRuleFor FROM PriceBookRules WHERE numPricRuleID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 8 then -- monSCreditBalance
	
      open SWV_RefCur for
      SELECT coalesce(monSCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0)) AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 9 then -- Check Site Page Exists
	
      if exists(SELECT vcPageName FROM   SitePages WHERE  numSiteID = v_numDomainID AND LOWER(vcPageURL) = LOWER(v_vcInput)) then
         open SWV_RefCur for
         SELECT 1 as SitePageExists;
      else
         open SWV_RefCur for
         SELECT 0 as SitePageExists;
      end if;
   end if;
   IF v_tintMode = 10 then -- Category Name
	
      open SWV_RefCur for
      SELECT coalesce(vcCategoryName,'NA') FROM Category WHERE numCategoryID::VARCHAR = v_vcInput AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 11 then -- monPCreditBalance
	
      open SWV_RefCur for
      SELECT coalesce(monPCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0)) AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 12 then -- Get Style ID from filename
	
      open SWV_RefCur for
      SELECT  coalesce(numCssID,0) FROM  StyleSheets WHERE  numSiteID = v_numDomainID AND LOWER(styleFileName) = LOWER(v_vcInput) LIMIT 1;
   end if;
   IF v_tintMode = 13 then -- numItemClassification
	
      open SWV_RefCur for
      SELECT coalesce(numItemClassification,0) as numItemClassification FROM  Item WHERE  numDomainID = v_numDomainID AND numItemCode = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END;
   end if;
   IF v_tintMode = 14 then -- numAssignedTo
	
      open SWV_RefCur for
      SELECT coalesce(numAssignedTo,0) as numAssignedTo FROM  DivisionMaster WHERE  numDomainID = v_numDomainID AND numDivisionID = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0));
   end if;
   IF v_tintMode = 15 then -- Get Currency ID 
	
      open SWV_RefCur for
      SELECT numCurrencyID FROM Currency WHERE numDomainId = v_numDomainID AND chrCurrency = v_vcInput;
   end if;
   IF v_tintMode = 16 then -- Total Order of Items where Warehouse exists
	
      open SWV_RefCur for
      SELECT count(*) as TotalRecords FROM OpportunityMaster OM join OpportunityItems OI on OM.numOppId = OI.numOppId
      WHERE OI.numItemCode = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND OM.numDomainId = v_numDomainID and OI.numWarehouseItmsID > 0;
   end if;
   IF v_tintMode = 17 then -- Get numBizDocID
	
      open SWV_RefCur for
      SELECT numBizDocTempID FROM BizDocTemplate
      WHERE numDomainID = v_numDomainID
      AND tintTemplateType = 0
      AND bitDefault = true
      AND bitEnabled = true
      AND numBizDocID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END;
   end if;
   IF v_tintMode = 18 then -- Get Domain DivisionID
	
      open SWV_RefCur for
      SELECT numDivisionId FROM Domain WHERE numDomainId = v_numDomainID;
   end if;
   IF v_tintMode = 19 then
	
      open SWV_RefCur for
      SELECT numItemID FROM ItemAPI WHERE numDomainId = v_numDomainID AND  vcAPIItemID = v_vcInput;
   end if;
   IF v_tintMode = 20 then
	
      open SWV_RefCur for
      SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND  vcSKU = v_vcInput;
   end if;
   IF v_tintMode = 21 then
	
      open SWV_RefCur for
      SELECT numStateId FROM State S inner JOIN webApiStateMapping SM
      ON S.vcState =  SM.vcStateName
      AND S.numDomainID = v_numDomainID
      AND (LOWER(SM.vcAmazonStateCode) = LOWER(v_vcInput) OR LOWER(SM.vcStateName) = LOWER(v_vcInput));
   end if;
   IF v_tintMode = 22 then
	
      open SWV_RefCur for
      SELECT LD.numListItemID FROM Listdetails LD INNER Join WebApiCountryMapping CM ON
      LD.vcData = CM.vcCountryName WHERE LD.numListID = 40 AND LD.numDomainid = v_numDomainID AND
      CM.vcAmazonCountryCode = v_vcInput;
   end if;
   IF v_tintMode = 23 then
   
      open SWV_RefCur for
      select coalesce(txtSignature,'') from UserMaster where numUserDetailId = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END and numDomainID = v_numDomainID;
   end if;
   
   IF v_tintMode = 24 then
      select dtEmailStartDate, dtEmailEndDate, intNoOfEmail INTO v_dtEmailStartDate,v_dtEmailEndDate,v_intNoOfEmail from Subscribers where numTargetDomainID = v_numDomainID;
      IF  LOCALTIMESTAMP >=  v_dtEmailStartDate AND LOCALTIMESTAMP <=  v_dtEmailEndDate then
            
         open SWV_RefCur for
         select(CAST(count(*) AS VARCHAR(10)) || '-' ||(select  CAST(intNoOfEmail AS VARCHAR(10)) from Subscribers where numTargetDomainID = v_numDomainID LIMIT 1)) as EmailData
         from BroadCastDtls BD inner join Broadcast B on B.numBroadCastId = BD.numBroadcastID
         where B.numDomainID = v_numDomainID AND bintBroadCastDate between v_dtEmailStartDate and v_dtEmailEndDate and bitBroadcasted = true and tintSucessfull = 1;
      end if;
   end if;  
   IF v_tintMode = 25 then
	
      open SWV_RefCur for
      SELECT coalesce(vcMsg,'') AS vcMsg FROM fn_GetEmailAlert(v_numDomainID,CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END,v_numUserCntId);
   end if;
   IF v_tintMode = 26 then
	
      open SWV_RefCur for
      SELECT COUNT(*) AS Total FROM Communication WHERE numDomainID = v_numDomainID and numContactId = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0));
   end if; 
   IF v_tintMode = 27 then -- Check Order Closed
	
      open SWV_RefCur for
      SELECT coalesce(tintshipped,0) AS tintshipped FROM OpportunityMaster
      WHERE numOppId = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND numDomainId = v_numDomainID;
   end if;
   IF v_tintMode = 28 then -- General Journal Header for Bank Recon. Adjustments
	
      open SWV_RefCur for
      SELECT coalesce(numJOurnal_Id,0) AS numJournal_Id FROM General_Journal_Header
      WHERE numReconcileID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND numDomainId = v_numDomainID AND coalesce(bitReconcileInterest,false) = false;
   end if;
   IF v_tintMode = 29 then -- General Journal Header for Write Check
	
      open SWV_RefCur for
      SELECT coalesce(numJOurnal_Id,0) AS numJournal_Id FROM General_Journal_Header
      WHERE numCheckHeaderID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND numDomainId = v_numDomainID;
   end if;
	
   IF v_tintMode = 30 then -- Get Units for sample item to be created
	
      open SWV_RefCur for
      SELECT coalesce(u.numUOMId,0) FROM
      UOM u INNER JOIN Domain d ON u.numDomainID = d.numDomainId
      WHERE u.numDomainID = v_numDomainID
      AND d.numDomainId = v_numDomainID
      AND u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END)
      AND u.vcUnitName = 'Units';
   end if;
	
   IF v_tintMode = 31 then -- Get Magento Attribute Set for Current Domain
	
      open SWV_RefCur for
      SELECT vcEighthFldValue FROM WebAPIDetail WHERE numDomainID = v_numDomainID  AND WebApiId = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END;
   end if;
	
   IF v_tintMode = 32 then -- Get WareHouseItemID for ItemID is existis for Current Domain
	
      open SWV_RefCur for
      SELECT coalesce((SELECT  numWareHouseItemID FROM  WareHouseItems WHERE numDomainID = v_numDomainID  AND numItemID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END LIMIT 1),0) AS  numWareHouseItemID;
   end if;
	
   IF v_tintMode = 33 then
	
      open SWV_RefCur for
      SELECT coalesce(MAX(CAST(coalesce(NULLIF(numSequenceId,''),'0') AS BIGINT)),
      0)+1 FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
      WHERE OM.numDomainId = v_numDomainID AND OBD.numBizDocId::VARCHAR = v_vcInput;
   end if;	
	
   IF v_tintMode = 34 then
	
      open SWV_RefCur for
      SELECT coalesce(vcOppRefOrderNo,'') FROM OpportunityMaster OM
      WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0));
   end if;
	
   IF v_tintMode = 35 then
	
      open SWV_RefCur for
      SELECT coalesce(bitAutolinkUnappliedPayment,false) FROM Domain  WHERE numDomainId = v_numDomainID;
   end if;		 
	
   IF v_tintMode = 36 then
	
      open SWV_RefCur for
      SELECT coalesce(numJournalId,0) FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numChartAcntId = cast(NULLIF(v_vcInput,'') as NUMERIC(18,0)) AND chBizDocItems = 'OE';
   end if;	
	
   IF v_tintMode = 37 then
	
      open SWV_RefCur for
      SELECT coalesce(numShippingServiceItemID,0) FROM Domain WHERE numDomainId = v_numDomainID;
   end if;	
   IF v_tintMode = 38 then
	
		
		--FIRST CHECK FOR ITEM LEVEL SKU
      select   numItemCode INTO v_numItemCode FROM Item WHERE numDomainID = v_numDomainID AND  vcSKU = v_vcInput    LIMIT 1;

		--IF ITEM NOT AVAILABLE WITH SKU CHECK FOR MATRIX ITEM (WAREHOUSE LEVEL SKU)
      IF coalesce(v_numItemCode,0) = 0 then
		
         select   numItemID INTO v_numItemCode FROM WareHouseItems WHERE numDomainID = v_numDomainID AND vcWHSKU = v_vcInput    LIMIT 1;
      end if;
      open SWV_RefCur for
      SELECT CAST(numItemCode AS VARCHAR(50)) || '~' || charItemType as numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
   end if;	
   IF v_tintMode = 39 then -- Get WareHouseItemID for SKU is exists for Current Domain
	
      IF POSITION('~' IN v_vcInput) > 0 then
         select   strItem INTO v_numItemID FROM SplitWordWithPosition(v_vcInput,'~') WHERE RowNumber = 1;
         select   strItem INTO v_vcSKU FROM SplitWordWithPosition(v_vcInput,'~') WHERE RowNumber = 2;
         open SWV_RefCur for
         SELECT   coalesce((SELECT 
            numWareHouseItemID
            FROM   WareHouseItems
            WHERE  numDomainID = v_numDomainID
            AND vcWHSKU = v_vcSKU
            AND numItemID = CAST(v_numItemID AS NUMERIC) LIMIT 1),0) AS numWareHouseItemID;
      ELSE
         open SWV_RefCur for
         SELECT   coalesce((SELECT 
            numWareHouseItemID
            FROM   WareHouseItems
            WHERE  numDomainID = v_numDomainID
            AND numItemID = CAST(v_numItemID AS NUMERIC) LIMIT 1),0) AS numWareHouseItemID;
      end if;
   end if;
	
   IF v_tintMode = 40 then -- General Journal Header for Bank Recon Interest Service Charge.
	
      open SWV_RefCur for
      SELECT coalesce(numJOurnal_Id,0) AS numJournal_Id FROM General_Journal_Header
      WHERE numReconcileID = CASE WHEN COALESCE(v_vcInput,'') = '' THEN 0 ELSE CAST(v_vcInput AS numeric) END AND numDomainId = v_numDomainID AND coalesce(bitReconcileInterest,false) = true;
   end if; 
   IF v_tintMode = 41 then -- Item On Hand
	
      open SWV_RefCur for
      SELECT SUM(coalesce(numOnHand,0)+coalesce(numAllocation,0)) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = CAST(v_numItemID AS NUMERIC);
   end if;
   RETURN;
END; $$;


