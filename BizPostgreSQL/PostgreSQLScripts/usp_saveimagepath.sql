-- Stored procedure definition script USP_SaveImagePath for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveImagePath(v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_vcImagePath VARCHAR(100) DEFAULT '',
v_sintbyteMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If v_sintbyteMode = 0 then
      Update Domain Set vcBizDocImagePath = v_vcImagePath Where numDomainId = v_numDomainId;
   ELSEIF v_sintbyteMode = 1
   then
      Update Domain Set vcCompanyImagePath = v_vcImagePath Where numDomainId = v_numDomainId;
   ELSEIF v_sintbyteMode = 2
   then
      Update Domain Set vcBankImagePath = v_vcImagePath Where numDomainId = v_numDomainId;
   end if;
   RETURN;
END; $$;


