-- Function definition script GetOppName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppName(v_numOppid NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppName  VARCHAR(100);
BEGIN
   select   vcpOppName INTO v_OppName from OpportunityMaster where numOppId = v_numOppid;
   RETURN v_OppName;
END; $$;

