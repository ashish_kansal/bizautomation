-- Stored procedure definition script USP_Domain_GetDateFormat for PostgreSQL
Create or replace FUNCTION USP_Domain_GetDateFormat(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN
   open SWV_RefCur for Select vcDateFormat,tintDecimalPoints from Domain where numDomainId = v_numDomainId;
END; $$;













