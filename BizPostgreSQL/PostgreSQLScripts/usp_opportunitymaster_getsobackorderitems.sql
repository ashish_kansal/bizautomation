-- Stored procedure definition script USP_OpportunityMaster_GetSOBackOrderItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetSOBackOrderItems(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintDefaultCost  NUMERIC(18,0);
   v_bitReOrderPoint  BOOLEAN;
   v_numReOrderPointOrderStatus  NUMERIC(18,0);
   v_tintOppStautsForAutoPOBackOrder  SMALLINT;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT;
   v_bitIncludeRequisitions  BOOLEAN;
BEGIN
   IF NOT EXISTS(SELECT * FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID) then
	
      RAISE EXCEPTION 'INVALID_OPPID';
      RETURN;
   end if; 

   select   coalesce(numCost,0), coalesce(bitReOrderPoint,false), coalesce(numReOrderPointOrderStatus,0), coalesce(tintOppStautsForAutoPOBackOrder,0), coalesce(tintUnitsRecommendationForAutoPOBackOrder,1), coalesce(bitIncludeRequisitions,false) INTO v_tintDefaultCost,v_bitReOrderPoint,v_numReOrderPointOrderStatus,v_tintOppStautsForAutoPOBackOrder,
   v_tintUnitsRecommendationForAutoPOBackOrder,
   v_bitIncludeRequisitions FROM
   Domain WHERE
   numDomainId = v_numDomainID;


   open SWV_RefCur for
   SELECT
   v_numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,v_tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,v_tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder;

   IF v_tintOppStautsForAutoPOBackOrder IN(0,1) then
	
      open SWV_RefCur2 for
      SELECT
      OI.numItemCode
			,CONCAT(I.vcItemName,CASE WHEN fn_GetAttributes(OI.numWarehouseItmsID,0::BOOLEAN) <> '' THEN CONCAT(' (',fn_GetAttributes(OI.numWarehouseItmsID,0::BOOLEAN),')') ELSE '' END) AS vcItemName
			,I.vcModelID
			,(CASE WHEN coalesce(I.numItemGroup,0) > 0 AND bitMatrix = false THEN coalesce(WI.vcWHSKU,coalesce(I.vcSKU,''))  ELSE coalesce(I.vcSKU,'') END) AS vcSKU
			,V.vcNotes
			,I.charItemType
			,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),
      '') AS vcPathForTImage
			,coalesce(OI.numWarehouseItmsID,0) AS numWarehouseItemID
			,(CASE
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
      THEN(CASE WHEN coalesce(I.fltReorderQty,0) > coalesce(V.intMinQty,0) THEN coalesce(I.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)+coalesce(WI.numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OI.numItemCode
               AND OpportunityItems.numWarehouseItmsID = OI.numWarehouseItmsID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
      THEN(CASE
         WHEN coalesce(I.fltReorderQty,0) >= coalesce(V.intMinQty,0) AND coalesce(I.fltReorderQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(I.fltReorderQty,0)
         WHEN coalesce(V.intMinQty,0) >= coalesce(I.fltReorderQty,0) AND coalesce(V.intMinQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(V.intMinQty,0)
         WHEN coalesce(WI.numBackOrder,0) >= coalesce(I.fltReorderQty,0) AND coalesce(WI.numBackOrder,0) >= coalesce(V.intMinQty,0) THEN coalesce(WI.numBackOrder,0)
         ELSE coalesce(I.fltReorderQty,0)
         END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OI.numItemCode
               AND OpportunityItems.numWarehouseItmsID = OI.numWarehouseItmsID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
      THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OI.numItemCode
               AND OpportunityItems.numWarehouseItmsID = OI.numWarehouseItmsID),0) ELSE 0 END)) -coalesce(WI.numBackOrder,0)+(CASE
         WHEN coalesce(I.fltReorderQty,0) >= coalesce(V.intMinQty,0) THEN coalesce(I.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)
      ELSE coalesce(WI.numBackOrder,0)+coalesce(V.intMinQty,0)
      END) AS numUnitHour
			,coalesce(WI.numBackOrder,0) AS numBackOrder
			,coalesce(I.numBaseUnit,0) AS numUOMID
			,fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor
			,coalesce(I.numVendorID,0) AS numVendorID
			,CASE WHEN v_tintDefaultCost = 3 THEN(coalesce(V.monCost,0)/fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE coalesce(I.monAverageCost,0) END AS monCost
			,coalesce(V.intMinQty,0) AS intMinQty
			,coalesce(v_tintDefaultCost,0) AS tintDefaultCost
			,coalesce(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
      FROM
      OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      INNER JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
      LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
      WHERE
      OI.numOppId = v_numOppID
      AND OM.numDomainId = v_numDomainID
      AND coalesce(v_bitReOrderPoint,false) = true
      AND coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID = WI.numWareHouseID),0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(I.bitAssembly,false) = false
      AND coalesce(I.bitKitParent,false) = false
      AND coalesce(OI.bitDropShip,false) = false
      AND coalesce(WI.numBackOrder,0) > 0
      UNION
      SELECT
      IChild.numItemCode
			,CONCAT(IChild.vcItemName,CASE WHEN fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) <> '' THEN CONCAT(' (',fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN coalesce(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = false THEN coalesce(WI.vcWHSKU,coalesce(IChild.vcSKU,''))  ELSE coalesce(IChild.vcSKU,'') END) AS vcSKU
			,V.vcNotes
			,IChild.charItemType
			,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND numItemCode = IChild.numItemCode AND bitDefault = true LIMIT 1),
      '') AS vcPathForTImage
			,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
      THEN(CASE WHEN coalesce(IChild.fltReorderQty,0) > coalesce(V.intMinQty,0) THEN coalesce(IChild.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)+coalesce(WI.numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = IChild.numItemCode
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
      THEN(CASE
         WHEN coalesce(IChild.fltReorderQty,0) >= coalesce(V.intMinQty,0) AND coalesce(IChild.fltReorderQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(IChild.fltReorderQty,0)
         WHEN coalesce(V.intMinQty,0) >= coalesce(IChild.fltReorderQty,0) AND coalesce(V.intMinQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(V.intMinQty,0)
         WHEN coalesce(WI.numBackOrder,0) >= coalesce(IChild.fltReorderQty,0) AND coalesce(WI.numBackOrder,0) >= coalesce(V.intMinQty,0) THEN coalesce(WI.numBackOrder,0)
         ELSE coalesce(IChild.fltReorderQty,0)
         END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = IChild.numItemCode
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
      THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = IChild.numItemCode
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END)) -coalesce(WI.numBackOrder,0)+(CASE
         WHEN coalesce(IChild.fltReorderQty,0) >= coalesce(V.intMinQty,0) THEN coalesce(IChild.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)
      ELSE coalesce(WI.numBackOrder,0)+coalesce(V.intMinQty,0)
      END) AS numUnitHour
			,coalesce(WI.numBackOrder,0) AS numBackOrder
			,coalesce(IChild.numBaseUnit,0) AS numUOMID
			,fn_UOMConversion(OKI.numUOMId,IChild.numItemCode,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor
			,coalesce(IChild.numVendorID,0) AS numVendorID
			,CASE WHEN v_tintDefaultCost = 3 OR v_tintDefaultCost = 2 THEN(coalesce(V.monCost,0)/fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE coalesce(IChild.monAverageCost,0) END AS monCost
			,coalesce(V.intMinQty,0) AS intMinQty
			,coalesce(v_tintDefaultCost,0) AS tintDefaultCost
			,coalesce(fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      Item IChild
      ON
      OKI.numChildItemID = IChild.numItemCode
      AND coalesce(IChild.bitKitParent,false) = false
      INNER JOIN
      Vendor V
      ON
      IChild.numVendorID = V.numVendorID
      AND IChild.numItemCode = V.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OI.numOppId = v_numOppID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(bitDropShip,false) = false
      AND coalesce(bitWorkOrder,false) = false
      AND coalesce(I.bitKitParent,false) = true
      AND coalesce(WI.numBackOrder,0) > 0
      AND coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID = WI.numWareHouseID),0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
      UNION
      SELECT
      IChild.numItemCode
			,CONCAT(IChild.vcItemName,CASE WHEN fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) <> '' THEN CONCAT(' (',fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN coalesce(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = false THEN coalesce(WI.vcWHSKU,coalesce(IChild.vcSKU,''))  ELSE coalesce(IChild.vcSKU,'') END) AS vcSKU
			,V.vcNotes
			,IChild.charItemType
			,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND numItemCode = IChild.numItemCode AND bitDefault = true LIMIT 1),
      '') AS vcPathForTImage
			,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
      THEN(CASE WHEN coalesce(IChild.fltReorderQty,0) > coalesce(V.intMinQty,0) THEN coalesce(IChild.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)+coalesce(WI.numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OKCI.numItemID
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
      THEN(CASE
         WHEN coalesce(IChild.fltReorderQty,0) >= coalesce(V.intMinQty,0) AND coalesce(IChild.fltReorderQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(IChild.fltReorderQty,0)
         WHEN coalesce(V.intMinQty,0) >= coalesce(IChild.fltReorderQty,0) AND coalesce(V.intMinQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(V.intMinQty,0)
         WHEN coalesce(WI.numBackOrder,0) >= coalesce(IChild.fltReorderQty,0) AND coalesce(WI.numBackOrder,0) >= coalesce(V.intMinQty,0) THEN coalesce(WI.numBackOrder,0)
         ELSE coalesce(IChild.fltReorderQty,0)
         END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OKCI.numItemID
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END))
      WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
      THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
               SUM(numUnitHour)
               FROM
               OpportunityItems
               INNER JOIN
               OpportunityMaster
               ON
               OpportunityItems.numOppId = OpportunityMaster.numOppId
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               AND OpportunityMaster.tintopptype = 2
               AND OpportunityMaster.tintoppstatus = 0
               AND OpportunityItems.numItemCode = OKCI.numItemID
               AND OpportunityItems.numWarehouseItmsID = WI.numWareHouseItemID),0) ELSE 0 END)) -coalesce(WI.numBackOrder,0)+(CASE
         WHEN coalesce(IChild.fltReorderQty,0) >= coalesce(V.intMinQty,0) THEN coalesce(IChild.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)
      ELSE coalesce(WI.numBackOrder,0)+coalesce(V.intMinQty,0)
      END) AS numUnitHour
			,coalesce(WI.numBackOrder,0) AS numBackOrder
			,coalesce(IChild.numBaseUnit,0) AS numUOMID
			,fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor
			,coalesce(IChild.numVendorID,0) AS numVendorID
			,CASE WHEN (v_tintDefaultCost = 3 OR v_tintDefaultCost = 2) THEN(coalesce(V.monCost,0)/fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE coalesce(IChild.monAverageCost,0) END AS monCost
			,coalesce(V.intMinQty,0) AS intMinQty
			,coalesce(v_tintDefaultCost,0) AS tintDefaultCost
			,coalesce(fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OKCI.numOppChildItemID = OKI.numOppChildItemID
      INNER JOIN
      Item IChild
      ON
      OKCI.numItemID = IChild.numItemCode
      AND coalesce(IChild.bitKitParent,false) = false
      INNER JOIN
      Vendor V
      ON
      IChild.numVendorID = V.numVendorID
      AND IChild.numItemCode = V.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OI.numOppId = v_numOppID
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(bitDropShip,false) = false
      AND coalesce(bitWorkOrder,false) = false
      AND coalesce(I.bitKitParent,false) = true
      AND coalesce(WI.numBackOrder,0) > 0
      AND coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID = WI.numWareHouseID),0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0);
   end if;
END; $$;


