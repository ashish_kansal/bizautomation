-- Stored procedure definition script USP_GetDocApprovers for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDocApprovers(v_numDocID NUMERIC(9,0) DEFAULT 0,    
v_cDocType VARCHAR(10) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numDocID as VARCHAR(255)),cast(D.numContactID as VARCHAR(255)),cast(D.vcComment as VARCHAR(255)),
case  when tintApprove = 0 then '-' when tintApprove = 1 then 'Approved' when tintApprove = 2 then 'Declined' end AS Status,
cast(dtApprovedOn as VARCHAR(255)),
cast(cDocType as VARCHAR(255)),vcFirstName || ' ' || vcLastname as Name,
cast(vcCompanyName as VARCHAR(255)) from DocumentWorkflow D
   join AdditionalContactsInformation A
   on A.numContactId = D.numContactID
   join DivisionMaster DM
   on DM.numDivisionID = A.numDivisionId
   join CompanyInfo C
   on C.numCompanyId = DM.numCompanyID
   where numDocID = v_numDocID and cDocType = v_cDocType;
END; $$;












