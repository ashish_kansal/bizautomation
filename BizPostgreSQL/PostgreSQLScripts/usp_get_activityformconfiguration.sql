-- Stored procedure definition script USP_Get_ActivityFormConfiguration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Get_ActivityFormConfiguration(v_numDomainID NUMERIC(18,0),
	v_numGroupId NUMERIC(18,0),
	v_numUserContId NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ErrorMessage  VARCHAR(4000);
   v_ErrorNumber  TEXT;
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_ErrorLine  INTEGER;
   v_ErrorProcedure  VARCHAR(200);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
open SWV_RefCur for
      SELECT
      numActivityAuthenticationId,
		numDomainID,
		numGroupId,
		bitFollowupStatus,
		bitPriority,
		bitActivity,
		bitCustomField,
		bitFollowupAnytime,
		bitAttendee
      FROM
      ActivityFormAuthentication
      WHERE
      numDomainID = v_numDomainID AND
      numGroupId = v_numGroupId;

      open SWV_RefCur2 for
      SELECT
      numDisplayId,
		numDomainID,
		numUserCntId,
		bitTitle,
		bitLocation,
		bitDescription
      FROM
      ActivityDisplayConfiguration
      WHERE
      numDomainID = v_numDomainID AND
      numUserCntId = v_numUserContId;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_ErrorMessage := SQLERRM;
         v_ErrorNumber := sqlstate;
         v_ErrorSeverity := ERROR_SEVERITY();
         v_ErrorState := ERROR_STATE();
         v_ErrorLine := ERROR_LINE();
         v_ErrorProcedure := coalesce(ERROR_PROCEDURE(),'-');
         RAISE NOTICE '% % % % % % ',v_ErrorMessage,v_ErrorNumber,v_ErrorSeverity,v_ErrorState,v_ErrorProcedure,
         v_ErrorLine;
   END;
   RETURN;
END; $$;



