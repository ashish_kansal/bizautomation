CREATE OR REPLACE FUNCTION USP_SaveJavascriptDisplayOrder(v_strItems TEXT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
	IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
		UPDATE 
			StyleSheets 
		SET 
			intDisplayOrder = X.intDisplayOrder
		FROM  
		XMLTABLE
		(
			'NewDataSet/CssStyles'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numCssID NUMERIC(9,0) PATH 'numCssID',
				intDisplayOrder INTEGER PATH 'intDisplayOrder'
		) AS X
		WHERE 
			StyleSheets.numCssID = X.numCssID AND tintType = 1;
	end if;
   
	RETURN;
END; $$;




