-- Stored procedure definition script USP_ManageContracts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageContracts(v_numContractId NUMERIC(18,0),
v_intType INTEGER,
v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_numDivisonId NUMERIC(18,0),
v_numIncidents NUMERIC(18,0),
v_numIncidentLeft NUMERIC(18,0),
v_numHours NUMERIC(18,0) DEFAULT 0,
v_numMinutes NUMERIC(18,0) DEFAULT 0,
v_vcItemClassification TEXT DEFAULT NULL,
v_numWarrantyDays NUMERIC(18,0) DEFAULT NULL,
v_vcNotes TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_timeLeft  NUMERIC(18,0) DEFAULT 0;
   v_timeUsed  NUMERIC(18,0) DEFAULT 0;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   -- BEGIN TRAN
IF(v_numContractId > 0) then
	
      IF(v_intType = 1) then
		
         SELECT  timeUsed INTO v_timeUsed FROM Contracts WHERE numContractId = v_numContractId     LIMIT 1;
         v_timeLeft :=((v_numHours*60*60)+(v_numMinutes*60)) -v_timeUsed;
      end if;
      UPDATE
      Contracts
      SET
      numIncidents = v_numIncidents,numHours = v_numHours,numMinutes = v_numMinutes,
      vcItemClassification = v_vcItemClassification,numWarrantyDays = v_numWarrantyDays,
      vcNotes = v_vcNotes,timeUsed = v_timeUsed,timeLeft = v_timeLeft
      WHERE
      numContractId = v_numContractId;
   ELSE
      IF((v_intType = 1 OR v_intType = 3) AND(SELECT COUNT(*) FROM Contracts WHERE numDivisonId = v_numDivisonId AND intType = v_intType) > 0) then
		
         RAISE EXCEPTION 'Organization contract is already exist';
         RETURN;
      ELSE
         v_timeLeft :=((v_numHours*60*60)+(v_numMinutes*60));
         INSERT INTO Contracts(intType,
				numDomainId,
				numDivisonId,
				numIncidents,
				numIncidentLeft,
				numHours,
				numMinutes,
				timeLeft,
				vcItemClassification,
				numWarrantyDays,
				vcNotes,
				numCreatedBy,
				dtmCreatedOn,
				timeUsed)VALUES(v_intType,
				v_numDomainID,
				v_numDivisonId,
				v_numIncidents,
				v_numIncidents,
				v_numHours,
				v_numMinutes,
				v_timeLeft,
				v_vcItemClassification,
				v_numWarrantyDays,
				v_vcNotes,
				v_numUserCntID,
				LOCALTIMESTAMP ,
				v_timeUsed);
			
         v_numContractId := CURRVAL('Contracts_seq');
         IF(v_intType = 1) then
			
            PERFORM USP_UpdateNameTemplateValueForContracts(4::SMALLINT,v_numDomainID,v_numContractId);
         end if;
         IF(v_intType = 2) then
			
            PERFORM USP_UpdateNameTemplateValueForContracts(5::SMALLINT,v_numDomainID,v_numContractId);
         end if;
         IF(v_intType = 3) then
			
            PERFORM USP_UpdateNameTemplateValueForContracts(6::SMALLINT,v_numDomainID,v_numContractId);
         end if;
      end if;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


