-- Stored procedure definition script USP_UpdateBreadCrumLevel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateBreadCrumLevel(v_strItems XML DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
      UPDATE SiteBreadCrumb SET tintLevel = X.tintLevel
      FROM  XMLTABLE
	(
		'NewDataSet/Item'
        PASSING 
			v_strItems 
		COLUMNS
			id FOR ORDINALITY,
			numBreadCrumbID NUMERIC PATH 'numBreadCrumbID',
			tintLevel INTEGER PATH 'tintLevel'
	) AS X
      WHERE SiteBreadCrumb.numBreadCrumbID = X.numBreadCrumbID;

   end if;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/



