-- Function definition script fn_CheckContactAOI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CheckContactAOI(v_numCntID NUMERIC, v_vcAOIs VARCHAR(550))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetVal  NUMERIC;
BEGIN
	SELECT numContactID INTO v_RetVal FROM AOIContactlink WHERE numAOIID IN (SELECT Id FROM SplitIDs(v_vcAOIs,',')) AND numContactID=v_numCntID;

	RETURN v_RetVal;
END; $$;

