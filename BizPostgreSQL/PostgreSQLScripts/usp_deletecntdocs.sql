-- Stored procedure definition script usp_DeleteCntDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteCntDocs(v_numDocID NUMERIC(9,0) DEFAULT 0   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM  Cntdocuments WHERE numDocID = v_numDocID;
   RETURN;
END; $$;


