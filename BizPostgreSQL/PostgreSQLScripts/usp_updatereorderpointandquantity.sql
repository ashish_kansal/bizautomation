DROP FUNCTION IF EXISTS USP_UpdateReorderPointAndQuantity;

CREATE OR REPLACE FUNCTION USP_UpdateReorderPointAndQuantity()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_numDomainID  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_numVendorID  NUMERIC(18,0);
   v_fltUnitSold  DOUBLE PRECISION;
   v_numLeadTimeDays  NUMERIC(18,0);
   v_tintReorderPointBasedOn  SMALLINT;
   v_intReorderPointDays  INTEGER;
   v_intReorderPointPercent  INTEGER;
BEGIN
   IF TO_CHAR(LOCALTIMESTAMP,'Day') = 'Sunday' AND (EXTRACT(HOUR FROM LOCALTIMESTAMP) >= 0 AND EXTRACT(HOUR FROM LOCALTIMESTAMP) <= 7) then -- BETWEEN 0 AM TO 7 AM
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPUpdateReorderPoint_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPUpdateReorderPoint CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPUpdateReorderPoint
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numDomainID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numVendorID NUMERIC(18,0),
         numLeadTimeDays NUMERIC(18,0),
         tintReorderPointBasedOn SMALLINT,
         intReorderPointDays INTEGER,
         intReorderPointPercent INTEGER
      );
      INSERT INTO tt_TEMPUpdateReorderPoint(numDomainID
			,numItemCode
			,numVendorID
			,tintReorderPointBasedOn
			,intReorderPointDays
			,intReorderPointPercent)
      SELECT 
      Item.numDomainID
			,Item.numItemCode
			,Item.numVendorID
			,coalesce(tintReorderPointBasedOn,1)
			,coalesce(intReorderPointDays,30)
			,coalesce(intReorderPointPercent,0)
      FROM
      Item
      INNER JOIN
      Domain
      ON
      Item.numDomainID = Domain.numDomainId
      WHERE
      coalesce(bitAutomateReorderPoint,false) = true
      AND (dtLastAutomateReorderPoint IS NULL OR dtLastAutomateReorderPoint <> CAST(LOCALTIMESTAMP  AS DATE)) LIMIT 500;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPUpdateReorderPoint;
      WHILE v_i <= v_iCount LOOP
         select   numDomainID, numItemCode, numVendorID, tintReorderPointBasedOn, intReorderPointDays, intReorderPointPercent INTO v_numDomainID,v_numItemCode,v_numVendorID,v_tintReorderPointBasedOn,v_intReorderPointDays,
         v_intReorderPointPercent FROM
         tt_TEMPUpdateReorderPoint WHERE
         ID = v_i;
         v_numLeadTimeDays := coalesce((SELECT 
         numListValue
         FROM
         VendorShipmentMethod
         INNER JOIN
         AddressDetails
         ON
         AddressDetails.numRecordID = VendorShipmentMethod.numVendorID
         AND tintAddressOf = 2
         AND tintAddressType = 2
         AND coalesce(bitIsPrimary,false) = true
         WHERE
         VendorShipmentMethod.numVendorID = v_numVendorID ORDER BY bitPreferredMethod DESC LIMIT 1),
         0);
         v_fltUnitSold := coalesce((SELECT
         SUM(OI.numUnitHour)
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.tintopptype = 1
         AND OM.tintoppstatus = 1
         AND OI.numItemCode = v_numItemCode
         AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST((CASE WHEN v_tintReorderPointBasedOn = 2 THEN LOCALTIMESTAMP || INTERVAL '-1 year' ELSE LOCALTIMESTAMP  END) || CAST(-v_intReorderPointDays || 'day' as interval) AS DATE) AND CAST((CASE WHEN v_tintReorderPointBasedOn = 2 THEN LOCALTIMESTAMP || INTERVAL '-1 year' ELSE LOCALTIMESTAMP  END) AS DATE)),0);
         RAISE NOTICE '%',CAST((CASE WHEN v_tintReorderPointBasedOn = 2 THEN LOCALTIMESTAMP+INTERVAL '-1 year' ELSE LOCALTIMESTAMP  END)+CAST(-v_intReorderPointDays || 'day' as interval) AS DATE);
         RAISE NOTICE '%',v_fltUnitSold;
         UPDATE WareHouseItems SET numReorder = CEIL(coalesce((v_fltUnitSold/v_intReorderPointDays::bigint)*(CASE WHEN coalesce(v_numLeadTimeDays,0) = 0 THEN 1 ELSE v_numLeadTimeDays END),0)) WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode;
         UPDATE
         Item
         SET
         fltReorderQty = CEIL(coalesce((v_fltUnitSold/v_intReorderPointDays::bigint)*(CASE WHEN coalesce(v_numLeadTimeDays,0) = 0 THEN 1 ELSE v_numLeadTimeDays END),0)+(coalesce((v_fltUnitSold/v_intReorderPointDays::bigint)*(CASE WHEN coalesce(v_numLeadTimeDays,0) = 0 THEN 1 ELSE v_numLeadTimeDays END),0)*v_intReorderPointPercent::bigint/100)),dtLastAutomateReorderPoint = LOCALTIMESTAMP
         WHERE
         numItemCode = v_numItemCode;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
   RETURN;
END; $$;


