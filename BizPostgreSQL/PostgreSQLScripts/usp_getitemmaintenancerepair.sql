-- Stored procedure definition script USP_GetItemMaintenanceRepair for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemMaintenanceRepair(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemCode NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT comm.numCommId,comm.numContactId,comm.numDivisionID, comm.textDetails,
comm.numActivity,GetListIemName(comm.numActivity) as vcActivity,
case when CAST(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
   when CAST(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
   when CAST(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
   else FormatedDateFromDate(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),1::NUMERIC) end  AS dtStartTime,
cast(coalesce(C.numCorrespondenceID,0) as VARCHAR(255)) AS numCorrespondenceID,
cast(coalesce(C.numOpenRecordID,0) as VARCHAR(255)) AS numOpenRecordID,
cast(coalesce(C.tintCorrType,0) as VARCHAR(255)) AS tintCorrType,
cast(com.vcCompanyName as VARCHAR(255)) as vcVendor,cast(C.monMRItemAmount as VARCHAR(255)),
case when CAST(dtEventClosedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
   when CAST(dtEventClosedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
   when CAST(dtEventClosedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
   else FormatedDateFromDate(dtEventClosedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),1::NUMERIC) end  AS dtEventClosedDate,
GetListIemName(comm.bitTask) AS Task
   FROM Communication comm JOIN Correspondence C ON C.numCommID = comm.numCommId
   join Item I on I.numItemCode = C.numOpenRecordID
   join DivisionMaster div  on div.numDivisionID = comm.numDivisionID
   join CompanyInfo com on com.numCompanyId = div.numCompanyID
   WHERE  C.tintCorrType = 7 and C.numDomainID = v_numDomainID
   and comm.numDomainID = v_numDomainID
   and I.numDomainID = v_numDomainID
   and C.numOpenRecordID = v_numItemCode
   and I.numItemCode = v_numItemCode;
END; $$;













