DROP FUNCTION IF EXISTS USP_GetDomainDetails;

CREATE OR REPLACE FUNCTION USP_GetDomainDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listIds  TEXT;
   v_canChangeDiferredIncomeSelection  BOOLEAN DEFAULT 1;
BEGIN
   select   COALESCE(coalesce(v_listIds,'') || ',','') || numUserID INTO v_listIds FROM UnitPriceApprover WHERE numDomainID = v_numDomainID;
   
   IF EXISTS(SELECT
   OB.numOppBizDocsId
   FROM
   OpportunityBizDocs OB
   INNER JOIN
   OpportunityMaster OM
   ON
   OB.numoppid = OM.numOppId
   WHERE
   numBizDocId = 304
   AND OM.numDomainId = v_numDomainID) then

      v_canChangeDiferredIncomeSelection := false;
   end if;   
   
                               
   open SWV_RefCur for select
   vcDomainName,
coalesce(vcDomainDesc,'') AS vcDomainDesc,
coalesce(bitExchangeIntegration,false) AS bitExchangeIntegration,
coalesce(bitAccessExchange,false) AS bitAccessExchange,
coalesce(vcExchUserName,'') AS vcExchUserName,
coalesce(vcExchPassword,'') AS vcExchPassword,
coalesce(vcExchPath,'') AS vcExchPath,
coalesce(vcExchDomain,'') AS vcExchDomain,
tintCustomPagingRows,
vcDateFormat,
numDefCountry,
tintComposeWindow,
sintStartDate,
sintNoofDaysInterval,
tintAssignToCriteria,
bitIntmedPage,
coalesce(D.numCost,0) as numCost,
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
coalesce(tintPayPeriod,0) as tintPayPeriod,
coalesce(numCurrencyID,0) as numCurrencyID,
coalesce(vcCurrency,'') as vcCurrency,
coalesce(charUnitSystem,'') as charUnitSystem ,
coalesce(vcPortalLogo,'') as vcPortalLogo,
coalesce(tintChrForComSearch,2) as tintChrForComSearch,
coalesce(tintChrForItemSearch,2) as tintChrForItemSearch,
coalesce(intPaymentGateWay,0) as intPaymentGateWay  ,
 case when bitPSMTPServer = true then vcPSMTPServer else '' end as vcPSMTPServer  ,
 case when bitPSMTPServer = true then  coalesce(numPSMTPPort,0)  else 0 end as numPSMTPPort
,coalesce(bitPSMTPAuth,false) AS bitPSMTPAuth
,coalesce(vcPSmtpPassword,'') AS vcPSmtpPassword
,coalesce(bitPSMTPSSL,false) AS bitPSMTPSSL
,coalesce(bitPSMTPServer,false) AS bitPSMTPServer  ,coalesce(vcPSMTPUserName,'') AS vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
coalesce(numShipCompany,0)   AS numShipCompany,
coalesce(bitAutoPopulateAddress,false) AS bitAutoPopulateAddress,
coalesce(tintPoulateAddressTo,0) AS tintPoulateAddressTo,
coalesce(bitMultiCompany,false) AS bitMultiCompany,
coalesce(bitMultiCurrency,false) AS bitMultiCurrency,
coalesce(bitCreateInvoice,0) AS bitCreateInvoice,
coalesce(numDefaultSalesBizDocId,0) AS numDefaultSalesBizDocId,
coalesce(bitSaveCreditCardInfo,false) AS bitSaveCreditCardInfo,
coalesce(intLastViewedRecord,10) AS intLastViewedRecord,
coalesce(tintCommissionType,1) AS tintCommissionType,
coalesce(tintBaseTax,2) AS tintBaseTax,
coalesce(numDefaultPurchaseBizDocId,0) AS numDefaultPurchaseBizDocId,
coalesce(bitEmbeddedCost,false) AS bitEmbeddedCost,
coalesce(numDefaultSalesOppBizDocId,0) AS numDefaultSalesOppBizDocId,
coalesce(tintSessionTimeOut,1) AS tintSessionTimeOut,
coalesce(tintDecimalPoints,2) AS tintDecimalPoints,
coalesce(bitCustomizePortal,false) AS bitCustomizePortal,
coalesce(tintBillToForPO,0) AS tintBillToForPO,
coalesce(tintShipToForPO,0) AS tintShipToForPO,
coalesce(tintOrganizationSearchCriteria,1) AS tintOrganizationSearchCriteria,
coalesce(tintLogin,0) AS tintLogin,
lower(coalesce(vcPortalName,'')) AS vcPortalName,
coalesce(bitDocumentRepositary,false) AS bitDocumentRepositary,
D.numDomainId,
coalesce(vcPSMTPDisplayName,'') AS vcPSMTPDisplayName,
coalesce(bitGtoBContact,false) AS bitGtoBContact,
coalesce(bitBtoGContact,false) AS bitBtoGContact,
coalesce(bitGtoBCalendar,false) AS bitGtoBCalendar,
coalesce(bitBtoGCalendar,false) AS bitBtoGCalendar,
coalesce(bitExpenseNonInventoryItem,false) AS bitExpenseNonInventoryItem,
coalesce(bitInlineEdit,false) AS bitInlineEdit,
coalesce(bitRemoveVendorPOValidation,false) AS bitRemoveVendorPOValidation,
coalesce(D.tintBaseTaxOnArea,0) AS tintBaseTaxOnArea,
coalesce(D.bitAmountPastDue,false) AS bitAmountPastDue,
coalesce(D.monAmountPastDue,0) AS monAmountPastDue,
coalesce(bitSearchOrderCustomerHistory,false) AS bitSearchOrderCustomerHistory,
coalesce(D.bitRentalItem,false) as bitRentalItem,
coalesce(D.numRentalItemClass,0) as numRentalItemClass,
coalesce(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
coalesce(D.numRentalDailyUOM,0) as numRentalDailyUOM,
coalesce(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
coalesce(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
coalesce(D.tintDefaultClassType,0) as tintDefaultClassType,
coalesce(D.tintPriceBookDiscount,0) AS tintPriceBookDiscount,
coalesce(D.bitAutoSerialNoAssign,false) as bitAutoSerialNoAssign,
coalesce(D.bitIsShowBalance,false) AS bitIsShowBalance,
coalesce(numIncomeAccID,0) AS numIncomeAccID,
coalesce(numCOGSAccID,0) AS numCOGSAccID,
coalesce(numAssetAccID,0) AS numAssetAccID,
coalesce(IsEnableProjectTracking,false) AS IsEnableProjectTracking,
coalesce(IsEnableCampaignTracking,false) AS IsEnableCampaignTracking,
coalesce(IsEnableResourceScheduling,false) AS IsEnableResourceScheduling,
coalesce(numShippingServiceItemID,0) AS numShippingServiceItemID,
coalesce(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
coalesce(D.bitAutolinkUnappliedPayment,false) AS bitAutolinkUnappliedPayment,
coalesce(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
coalesce(D.vcShipToPhoneNo,'') AS vcShipToPhoneNo,
coalesce(D.vcGAUserEMail,'') AS vcGAUserEMail,
coalesce(D.vcGAUserPassword,'') AS vcGAUserPassword,
coalesce(D.vcGAUserProfileId,'') AS vcGAUserProfileId,
coalesce(D.bitDiscountOnUnitPrice,false) AS bitDiscountOnUnitPrice,
coalesce(D.intShippingImageWidth,0) AS intShippingImageWidth ,
coalesce(D.intShippingImageHeight,0) AS intShippingImageHeight,
coalesce(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
coalesce(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
coalesce(D.vcHideTabs,'') AS vcHideTabs,
coalesce(D.bitUseBizdocAmount,false) AS bitUseBizdocAmount,
coalesce(D.bitDefaultRateType,false) AS bitDefaultRateType,
coalesce(D.bitIncludeTaxAndShippingInCommission,false) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
coalesce(D.bitPurchaseTaxCredit,false) AS bitPurchaseTaxCredit,
coalesce(D.bitLandedCost,false) AS bitLandedCost,
coalesce(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
coalesce(D.bitMinUnitPriceRule,false) AS bitMinUnitPriceRule,
coalesce(AP.numAbovePercent,0) AS numAbovePercent,
coalesce(AP.numBelowPercent,0) AS numBelowPercent,
coalesce(AP.numBelowPriceField,0) AS numBelowPriceField,
v_listIds AS vcUnitPriceApprover,
coalesce(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
coalesce(D.bitReOrderPoint,false) AS bitReOrderPoint,
coalesce(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
coalesce(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
coalesce(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
coalesce(D.IsEnableDeferredIncome,false) AS IsEnableDeferredIncome,
coalesce(D.bitApprovalforTImeExpense,false) AS bitApprovalforTImeExpense,
coalesce(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
coalesce(D.bitApprovalforOpportunity,false) AS bitApprovalforOpportunity,
coalesce(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
v_canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
coalesce(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
coalesce(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
coalesce(D.bitchkOverRideAssignto,false) AS bitchkOverRideAssignto,
coalesce(D.vcPrinterIPAddress,'') AS vcPrinterIPAddress,
coalesce(D.vcPrinterPort,'') AS vcPrinterPort,
coalesce(AP.bitCostApproval,false) AS bitCostApproval
,coalesce(AP.bitListPriceApproval,false) AS bitListPriceApproval
,coalesce(AP.bitMarginPriceViolated,false) AS bitMarginPriceViolated
,coalesce(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,coalesce(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,coalesce(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,coalesce(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,coalesce(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,coalesce(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,coalesce(bitSalesOrderTabs,false) AS bitSalesOrderTabs
,coalesce(bitSalesQuotesTabs,false) AS bitSalesQuotesTabs
,coalesce(bitItemPurchaseHistoryTabs,false) AS bitItemPurchaseHistoryTabs
,coalesce(bitItemsFrequentlyPurchasedTabs,false) AS bitItemsFrequentlyPurchasedTabs
,coalesce(bitOpenCasesTabs,false) AS bitOpenCasesTabs
,coalesce(bitOpenRMATabs,false) AS bitOpenRMATabs
,coalesce(bitSupportTabs,false) AS bitSupportTabs
,coalesce(vcSupportTabs,'Support') AS vcSupportTabs
,coalesce(D.numDefaultSiteID,0) AS numDefaultSiteID
,coalesce(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,coalesce(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,coalesce(bitRemoveGlobalLocation,false) AS bitRemoveGlobalLocation
,coalesce(numAuthorizePercentage,0) AS numAuthorizePercentage
,coalesce(bitEDI,false) AS bitEDI
,coalesce(bit3PL,false) AS bit3PL
,coalesce(numListItemID,0) AS numListItemID
,coalesce(bitAllowDuplicateLineItems,false) AS bitAllowDuplicateLineItems
,coalesce(tintCommitAllocation,1) AS tintCommitAllocation
,coalesce(tintInvoicing,1) AS tintInvoicing
,coalesce(tintMarkupDiscountOption,1) AS tintMarkupDiscountOption
,coalesce(tintMarkupDiscountValue,1) AS tintMarkupDiscountValue
,coalesce(bitIncludeRequisitions,false) AS bitIncludeRequisitions
,coalesce(bitCommissionBasedOn,false) AS bitCommissionBasedOn
,coalesce(tintCommissionBasedOn,0) AS tintCommissionBasedOn
,coalesce(bitDoNotShowDropshipPOWindow,false) AS bitDoNotShowDropshipPOWindow
,coalesce(tintReceivePaymentTo,2) AS tintReceivePaymentTo
,coalesce(numReceivePaymentBankAccount,0) AS numReceivePaymentBankAccount
,coalesce(numOverheadServiceItemID,0) AS numOverheadServiceItemID,
coalesce(D.bitDisplayCustomField,false) AS bitDisplayCustomField,
coalesce(D.bitFollowupAnytime,false) AS bitFollowupAnytime,
coalesce(D.bitpartycalendarTitle,false) AS bitpartycalendarTitle,
coalesce(D.bitpartycalendarLocation,false) AS bitpartycalendarLocation,
coalesce(D.bitpartycalendarDescription,false) AS bitpartycalendarDescription,
coalesce(D.bitREQPOApproval,false) AS bitREQPOApproval,
coalesce(D.bitARInvoiceDue,false) AS bitARInvoiceDue,
coalesce(D.bitAPBillsDue,false) AS bitAPBillsDue,
coalesce(D.bitItemsToPickPackShip,false) AS bitItemsToPickPackShip,
coalesce(D.bitItemsToInvoice,false) AS bitItemsToInvoice,
coalesce(D.bitSalesOrderToClose,false) AS bitSalesOrderToClose,
coalesce(D.bitItemsToPutAway,false) AS bitItemsToPutAway,
coalesce(D.bitItemsToBill,false) AS bitItemsToBill,
coalesce(D.bitPosToClose,false) AS bitPosToClose,
coalesce(D.bitPOToClose,false) AS bitPOToClose,
coalesce(D.bitBOMSToPick,false) AS bitBOMSToPick,
coalesce(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
coalesce(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
coalesce(D.vchAPBillsDue,'') AS vchAPBillsDue,
coalesce(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
coalesce(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
coalesce(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
coalesce(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
coalesce(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
coalesce(D.vchItemsToBill,'') AS vchItemsToBill,
coalesce(D.vchPosToClose,'') AS vchPosToClose,
coalesce(D.vchPOToClose,'') AS vchPOToClose,
coalesce(D.vchBOMSToPick,'') AS vchBOMSToPick,
coalesce(D.decReqPOMinValue,0) AS decReqPOMinValue,
coalesce(D.bitUseOnlyActionItems,false) AS bitUseOnlyActionItems,
coalesce(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
coalesce(tintMailProvider,3) AS tintMailProvider,
coalesce(D.bitDisplayContractElement,false) AS bitDisplayContractElement,
coalesce(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
coalesce(numARContactPosition,0) AS numARContactPosition,
coalesce(bitShowCardConnectLink,false) AS bitShowCardConnectLink,
coalesce(vcBluePayFormName,'') AS vcBluePayFormName,
coalesce(vcBluePaySuccessURL,'') AS vcBluePaySuccessURL,
coalesce(vcBluePayDeclineURL,'') AS vcBluePayDeclineURL,
coalesce(bitUseDeluxeCheckStock,false) AS bitUseDeluxeCheckStock,
coalesce(D.bitEnableSmartyStreets,false) AS bitEnableSmartyStreets,
coalesce(D.vcSmartyStreetsAPIKeys,'') AS vcSmartyStreetsAPIKeys,
coalesce(bitReceiveOrderWithNonMappedItem,false) AS bitReceiveOrderWithNonMappedItem,
coalesce(numItemToUseForNonMappedItem,0) AS numItemToUseForNonMappedItem,
coalesce(bitUsePredefinedCustomer,false) AS bitUsePredefinedCustomer,
coalesce(bitUsePreviousEmailBizDoc,false) AS bitUsePreviousEmailBizDoc,
CASE WHEN coalesce((SELECT COUNT(*) FROM ImapUserDetails WHERE
      numUserCntID = -1
      AND numDomainID = v_numDomainID),0) > 0 THEN true ELSE false END AS bitImapConfigured ,
coalesce(bitInventoryInvoicing,false) AS bitInventoryInvoicing,
coalesce(tintCommissionVendorCostType,1) tintCommissionVendorCostType
   from Domain D
   LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID = D.numDomainId
   LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainId
   where (D.numDomainId = v_numDomainID OR v_numDomainID = -1);


   RETURN;
END; $$;












