-- Stored procedure definition script usp_GetSpecficDocDtl for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSpecficDocDtl(v_numRecId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select
		numGenericDocID as "numGenericDocID",
		VcFileName as "VcFileName",
		VcDocName as "VcDocName",
		numDocCategory as "numDocCategory"
   from GenericDocuments
   where numGenericDocID = v_numRecId;
END; $$;












