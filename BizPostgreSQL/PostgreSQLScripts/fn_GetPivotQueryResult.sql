CREATE OR REPLACE FUNCTION fn_GetPivotQueryResult(p_vcQuery Text, p_numRecordID NUMERIC)
RETURNS SETOF RECORD
LANGUAGE plpgsql
   AS $$
BEGIN
   p_vcQuery:= REPLACE(p_vcQuery,'##RecordID##',CAST(p_numRecordID AS TEXT));
   RETURN QUERY EXECUTE p_vcQuery;
END; $$;

