DROP FUNCTION IF EXISTS USP_ManagePricingTable;

CREATE OR REPLACE FUNCTION USP_ManagePricingTable(v_numPriceRuleID NUMERIC(18,0),
    v_numItemCode NUMERIC(18,0),
	v_numCurrencyID NUMERIC(18,0),
    v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
   v_DecDiscount  DECIMAL(18,4);
BEGIN
   DELETE FROM PricingTable WHERE coalesce(numPriceRuleID,0) = v_numPriceRuleID AND coalesce(numCurrencyID,0) = v_numCurrencyID AND coalesce(numItemCode,0) = v_numItemCode;
		
	IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' THEN
		INSERT  INTO PricingTable
		(
			numPriceRuleID,
            intFromQty,
            intToQty,
            tintRuleType,
			tintVendorCostType,
            tintDiscountType,
            decDiscount,
			numItemCode,
			numCurrencyID
		)
		SELECT
			X.numPriceRuleID,
            X.intFromQty,
            X.intToQty,
            X.tintRuleType,
			X.tintVendorCostType,
            CASE coalesce(X.tintDiscountType,0) WHEN 0 THEN 1 ELSE coalesce(X.tintDiscountType,0) END,
			CAST(X.decDiscount AS DECIMAL(20,5)) AS decDiscount,
			X.numItemCode,
			v_numCurrencyID
		FROM
		XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numPriceRuleID NUMERIC PATH 'numPriceRuleID',
				intFromQty INTEGER PATH 'intFromQty',
				intToQty INTEGER PATH 'intToQty',
				tintRuleType SMALLINT PATH 'tintRuleType',
				tintVendorCostType SMALLINT PATH 'tintVendorCostType',
				tintDiscountType SMALLINT PATH 'tintDiscountType',
				decDiscount DECIMAL(20,5) PATH 'decDiscount',
				numItemCode NUMERIC PATH 'numItemCode',
				vcName VARCHAR(300) PATH 'vcName'
		) X;
      
		SELECT 
			CAST(X.decDiscount AS DECIMAL(18,4)) INTO v_DecDiscount 
		FROM
		XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numPriceRuleID NUMERIC PATH 'numPriceRuleID',
				intFromQty INTEGER PATH 'intFromQty',
				intToQty INTEGER PATH 'intToQty',
				tintRuleType SMALLINT PATH 'tintRuleType',
				tintVendorCostType SMALLINT PATH 'tintVendorCostType',
				tintDiscountType SMALLINT PATH 'tintDiscountType',
				decDiscount DECIMAL(20,5) PATH 'decDiscount',
				numItemCode NUMERIC PATH 'numItemCode',
				vcName VARCHAR(300) PATH 'vcName'
		) X;

      IF EXISTS(SELECT * FROM SalesTemplateItems WHERE numItemCode = v_numItemCode) then
		
         UPDATE SalesTemplateItems
         SET monPrice = v_DecDiscount,monTotAmount = v_DecDiscount,monTotAmtBefDiscount = v_DecDiscount
         WHERE numItemCode = v_numItemCode;
      end if;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/



