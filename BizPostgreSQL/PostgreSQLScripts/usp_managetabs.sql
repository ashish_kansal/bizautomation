-- Stored procedure definition script USP_ManageTabs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTabs(v_numGroupID NUMERIC(9,0) DEFAULT 0,        
v_strTabs TEXT DEFAULT '',    
v_numRelationships NUMERIC(9,0) DEFAULT 0,
v_numProfileID NUMERIC(9,0) DEFAULT 0,
v_tintType SMALLINT DEFAULT 0,-- if 1 then tabId is subtabid e.g. [CFw_Grp_Master].Grp_id
v_Loc_Id NUMERIC(9,0) DEFAULT 0-- will be passed when subtab
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
   v_tintGroupType  SMALLINT;
BEGIN
   IF v_tintType = 1 then --subtab

      DELETE FROM GroupTabDetails
      WHERE       numTabDetailID IN(SELECT numTabDetailID
      FROM   GroupTabDetails GTD
      INNER JOIN CFw_Grp_Master G
      ON GTD.numTabId = G.Grp_id
      WHERE  GTD.numGroupID = v_numGroupID
      AND GTD.tintType = v_tintType
      AND G.loc_Id = v_Loc_Id);
   ELSE -- main tab
      select   tintGroupType INTO v_tintGroupType from AuthenticationGroupMaster where numGroupID = v_numGroupID;
      if v_tintGroupType = 2 then
         delete from GroupTabDetails where numGroupID = v_numGroupID  and coalesce(numRelationShip,0) = v_numRelationships AND coalesce(numProfileID,0) = v_numProfileID AND coalesce(tintType,0) <> 1;
      else
         delete from GroupTabDetails where numGroupID = v_numGroupID
         AND coalesce(tintType,0) <> 1;
      end if;
   end if;

   insert into GroupTabDetails(numGroupID,numTabId,bitallowed,numOrder,numRelationShip,tintType,numProfileID)
   select v_numGroupID,X.TabId,X.bitallowed,X.numOrder,v_numRelationships,v_tintType,v_numProfileID  
   from
   XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strTabs AS XML)
		COLUMNS
			id FOR ORDINALITY,
			TabId NUMERIC(9,0) PATH 'TabId',
			bitallowed BOOLEAN PATH 'bitallowed',
			numOrder NUMERIC PATH 'numOrder'
	) AS X;

/****** Object:  StoredProcedure [dbo].[usp_ManageTaxDetails]    Script Date: 07/26/2008 16:20:03 ******/
   RETURN;
END; $$;


