-- Stored procedure definition script USP_DeleteEmailHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteEmailHistory(v_vcItemId VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM EmailHStrToBCCAndCC where numEmailHstrID in(select numEmailHstrID from EmailHistory where vcItemId = v_vcItemId);

   delete FROM EmailHistory  where vcItemId = v_vcItemId;
   RETURN;
END; $$;


