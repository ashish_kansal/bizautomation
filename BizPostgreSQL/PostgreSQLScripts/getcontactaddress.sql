-- Function definition script getContactAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION getContactAddress(v_numContactId NUMERIC)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_street  VARCHAR(100);
   v_City  VARCHAR(100);
   v_State  VARCHAR(100);
   v_PostCode  VARCHAR(100);
   v_vcCountry  VARCHAR(100);
   v_address1  VARCHAR(1000);
   v_add  VARCHAR(100);
BEGIN
   v_add := ',';
   v_address1 := '';

   select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(fn_GetState(numState),''), coalesce(vcPostalCode,''), coalesce(fn_GetListName(numCountry,0::BOOLEAN),'') INTO v_street,v_City,v_State,v_PostCode,v_vcCountry FROM
   AddressDetails AD where AD.numRecordID = v_numContactId AND tintAddressOf = 1 AND tintAddressType = 0
   AND AD.bitIsPrimary = true    LIMIT 1;




   v_address1 := coalesce(v_address1,'') || coalesce(v_street,'');
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ',';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_City <> '' then coalesce(v_add,'') || coalesce(v_City,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ',';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_State <> '' then coalesce(v_add,'') || coalesce(v_State,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ',';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_PostCode <> '' then coalesce(v_add,'') || coalesce(v_PostCode,'') else '' end;
   if  LENGTH(v_address1) = 0 then
      v_add := '';
   else
      v_add := ',';
   end if;

   v_address1 := coalesce(v_address1,'') || case when v_vcCountry <> '' then coalesce(v_add,'') || coalesce(v_vcCountry,'') else '' end;
--print @address1
   return v_address1;
END; $$;

