-- Stored procedure definition script USP_ManageErrorDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageErrorDetail(v_numDomainId NUMERIC(18,0),
v_numSiteId NUMERIC(18,0),
v_numCultureId NUMERIC(18,0),
v_tintApplication INTEGER,
v_strErrorMessages  TEXT  DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	IF SUBSTR(CAST(v_strErrorMessages AS VARCHAR(500)),1,500) <> '' then
      UPDATE ErrorDetail   SET vcErrorDesc = z.vcErrorDesc
      FROM
	  XMLTABLE
		(
			'NewDataSet/ErrorMessages'
			PASSING 
				CAST(v_strErrorMessages AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numErrorId NUMERIC PATH 'numErrorId',
				vcErrorDesc TEXT PATH 'vcErrorDesc',
				numErrorDetailId NUMERIC PATH 'numErrorDetailId'
		) AS Z
		WHERE
			COALESCE(Z.numErrorDetailId,0) > 0
			AND ErrorDetail.numErrorDetailId = z.numErrorDetailId;

      INSERT INTO ErrorDetail(numErrorID ,
                   vcErrorDesc ,
                   numDomainID ,
                   numSiteId ,
                   numCultureId)
      SELECT      X.numErrorId,
                   X.vcErrorDesc ,
                   v_numDomainId,
                   v_numSiteId ,
                   v_numCultureId
      FROM
	   XMLTABLE
		(
			'NewDataSet/ErrorMessages'
			PASSING 
				CAST(v_strErrorMessages AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numErrorId NUMERIC PATH 'numErrorId',
				vcErrorDesc TEXT PATH 'vcErrorDesc',
				numErrorDetailId NUMERIC PATH 'numErrorDetailId'
		) AS X
		WHERE
			COALESCE(numErrorDetailId,0) = 0;
   end if;
	
   RETURN;
END; $$;
--exec USP_ManageErrorDetail @numDomainId=1,@numSiteId=65,@numCultureId=0,@tintApplication=3,@strErrorMessages='<NewDataSet> <ErrorMessages><numErrorDetailId>1032</numErrorDetailId><vcErrorDesc>kkkk</vcErrorDesc><numErrorId>3</numErrorId></ErrorMessages></NewDataSet>'


