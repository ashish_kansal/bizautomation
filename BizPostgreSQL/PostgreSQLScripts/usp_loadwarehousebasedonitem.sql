-- Stored procedure definition script USP_LoadWarehouseBasedOnItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LoadWarehouseBasedOnItem(v_numItemCode NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numBaseUnit  NUMERIC(18,0);
   v_vcUnitName  VARCHAR(100); 
   v_numSaleUnit  NUMERIC(18,0);
   v_vcSaleUnitName  VARCHAR(100); 
   v_numPurchaseUnit  NUMERIC(18,0);
   v_vcPurchaseUnitName  VARCHAR(100); 
   v_bitSerialize  BOOLEAN;     
   v_bitKitParent  BOOLEAN;
   v_numSaleUOMFactor  DECIMAL(28,14);
   v_numPurchaseUOMFactor  DECIMAL(28,14);
BEGIN
   select   numDomainID, coalesce(numBaseUnit,0), coalesce(numSaleUnit,0), coalesce(numPurchaseUnit,0), bitSerialized, (CASE WHEN coalesce(bitKitParent,false) = true
   AND coalesce(bitAssembly,false) = true THEN 0
   WHEN coalesce(bitKitParent,false) = true THEN 1
   ELSE 0
   END) INTO v_numDomainID,v_numBaseUnit,v_numSaleUnit,v_numPurchaseUnit,v_bitSerialize,
   v_bitKitParent FROM
   Item WHERE
   numItemCode = v_numItemCode;    


   select   coalesce(vcUnitName,'-') INTO v_vcUnitName FROM UOM u WHERE u.numUOMId = v_numBaseUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcSaleUnitName FROM UOM u WHERE u.numUOMId = v_numSaleUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcPurchaseUnitName FROM UOM u WHERE u.numUOMId = v_numPurchaseUnit;

   v_numSaleUOMFactor := fn_UOMConversion(v_numSaleUnit,v_numItemCode,v_numDomainID,v_numBaseUnit);
   v_numPurchaseUOMFactor := fn_UOMConversion(v_numPurchaseUnit,v_numItemCode,v_numDomainID,v_numBaseUnit);

   open SWV_RefCur for SELECT
   DISTINCT cast((WareHouseItems.numWareHouseItemID) as VARCHAR(255)),
	cast(coalesce(WareHouseItems.numWLocationID,0) as VARCHAR(255)) AS numWLocationID,
	cast(CONCAT(vcWareHouse,(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ' (Global)' ELSE(CASE WHEN coalesce(WarehouseLocation.numWLocationID,0) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END) END)) as VARCHAR(255)) AS vcWareHouse,
	fn_GetAttributes(WareHouseItems.numWareHouseItemID,v_bitSerialize) as Attr,
	cast(WareHouseItems.monWListPrice as VARCHAR(255)),
	CASE WHEN v_bitKitParent = true THEN fn_GetKitInventory(v_numItemCode,WareHouseItems.numWareHouseID) else coalesce(numOnHand,0) END AS numOnHand,
	CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numOnOrder,0) END AS numOnOrder,
	CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numReorder,0) END AS numReorder,
	CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numAllocation,0) END AS numAllocation,
	CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numBackOrder,0) END AS numBackOrder,
	cast(Warehouses.numWareHouseID as VARCHAR(255)),
	v_vcUnitName AS vcUnitName,
	v_vcSaleUnitName AS vcSaleUnitName,
	v_vcPurchaseUnitName AS vcPurchaseUnitName,
	CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN CAST(CAST(fn_GetKitInventory(v_numItemCode,WareHouseItems.numWareHouseID)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) else CAST(CAST(coalesce(numOnHand,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numOnHandUOM,
	CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numOnOrder,0)/v_numPurchaseUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numOnOrderUOM,
	CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numReorder,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numReorderUOM,
	CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numAllocation,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numAllocationUOM,
	CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numBackOrder,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numBackOrderUOM
   FROM
   WareHouseItems
   JOIN
   Warehouses
   ON
   Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   WareHouseItems.numDomainID = v_numDomainID
   AND numItemID = v_numItemCode
   ORDER BY
   numWareHouseItemId ASC;
END; $$;












