
CREATE OR REPLACE FUNCTION usp_GetListDetailsFor(   
v_strListMasterName VARCHAR(50),
	v_numDomainID NUMERIC,
	v_bitSortbyID BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitSortbyID = false then
	
      open SWV_RefCur for
      SELECT LD.numListItemID AS "numListItemID", LD.vcData AS "vcData"
      FROM Listdetails LD INNER JOIN listmaster LM ON LD.numListID = LM.numListID
      WHERE LM.vcListName = v_strListMasterName
      AND LD.numDomainid = v_numDomainID
      ORDER BY LD.vcData;
   ELSE
      open SWV_RefCur for
      SELECT LD.numListItemID AS "numListItemID", LD.vcData AS "vcData" 
      FROM Listdetails LD INNER JOIN listmaster LM ON LD.numListID = LM.numListID
      WHERE LM.vcListName = v_strListMasterName
      AND LD.numDomainid = v_numDomainID
      ORDER BY LD.numListItemID;
   end if;
   RETURN;
END; $$;


