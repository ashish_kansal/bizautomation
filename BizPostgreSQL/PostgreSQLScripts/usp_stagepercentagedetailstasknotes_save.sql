-- Stored procedure definition script USP_StagePercentageDetailsTaskNotes_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTaskNotes_Save(v_numTaskID NUMERIC(18,0)
	,v_vcNotes TEXT
	,v_bitDone BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskNotes WHERE numTaskID = v_numTaskID) then
	
      UPDATE StagePercentageDetailsTaskNotes SET vcNotes = v_vcNotes,bitDone = v_bitDone WHERE numTaskID = v_numTaskID;
   ELSEIF EXISTS(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskID)
   then
	
      INSERT INTO StagePercentageDetailsTaskNotes(numTaskID
			,vcNotes
			,bitDone)
		VALUES(v_numTaskID
			,v_vcNotes
			,v_bitDone);
   end if;
   RETURN;
END; $$;


