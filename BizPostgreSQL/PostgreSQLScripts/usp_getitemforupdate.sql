CREATE OR REPLACE FUNCTION USP_GetItemForUpdate
(
	v_numDomainID NUMERIC(9,0) DEFAULT 0
	,INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_i  INTEGER DEFAULT 1;
	v_strSql  TEXT;
	v_numMaxFieldId  NUMERIC(9,0);
	v_from  TEXT;
	v_Where  TEXT;
	v_OrderBy  TEXT;
	v_GroupBy  TEXT;
	v_SelectFields  TEXT;
	v_InneJoinOn  TEXT;
	v_tintOrder  SMALLINT;
	v_vcFieldName  TEXT;
	v_vcListItemType TEXT;
	v_vcListItemType1 TEXT;
	v_vcAssociatedControlType TEXT;
	v_numListID  NUMERIC(9,0);
	v_vcDbColumnName TEXT;
	v_vcLookBackTableName TEXT;
	v_WCondition  TEXT;
	v_bitCustom  BOOLEAN;
	v_fld_id  NUMERIC(9,0);
	SWV_RowCount INTEGER;
BEGIN
	DROp TABLE IF EXISTS tt_TEMPIDS CASCADE;
	CREATE TEMPORARY TABLE  tt_TEMPIDS
	(
		ID INTEGER
	);

	WHILE v_i <= 20 LOOP
		INSERT INTO tt_TEMPIDS(ID) VALUES(v_i);
			
		v_i := v_i::bigint+1;
	END LOOP;
   
------------Declaration---------------------  
	v_InneJoinOn := '';
	v_OrderBy := ' Order by I.numItemCode ';
	v_Where := '';
	v_from := ' FROM item I  LEFT JOIN LATERAL (SELECT
		vcPriceLevel1Name,
		intPriceLevel1FromQty,
		intPriceLevel1ToQty,
		monPriceLevel1,
		vcPriceLevel2Name,
		intPriceLevel2FromQty,
		intPriceLevel2ToQty,
		monPriceLevel2,
		vcPriceLevel3Name,
		intPriceLevel3FromQty,
		intPriceLevel3ToQty,
		monPriceLevel3,
		vcPriceLevel4Name,
		intPriceLevel4FromQty,
		intPriceLevel4ToQty,
		monPriceLevel4,
		vcPriceLevel5Name,
		intPriceLevel5FromQty,
		intPriceLevel5ToQty,
		monPriceLevel5,
		vcPriceLevel6Name,
		intPriceLevel6FromQty,
		intPriceLevel6ToQty,
		monPriceLevel6,
		vcPriceLevel7Name,
		intPriceLevel7FromQty,
		intPriceLevel7ToQty,
		monPriceLevel7,
		vcPriceLevel8Name,
		intPriceLevel8FromQty,
		intPriceLevel8ToQty,
		monPriceLevel8,
		vcPriceLevel9Name,
		intPriceLevel9FromQty,
		intPriceLevel9ToQty,
		monPriceLevel9,
		vcPriceLevel10Name,
		intPriceLevel10FromQty,
		intPriceLevel10ToQty,
		monPriceLevel10,
		vcPriceLevel11Name,
		intPriceLevel11FromQty,
		intPriceLevel11ToQty,
		monPriceLevel11,
		vcPriceLevel12Name,
		intPriceLevel12FromQty,
		intPriceLevel12ToQty,
		monPriceLevel12,
		vcPriceLevel13Name,
		intPriceLevel13FromQty,
		intPriceLevel13ToQty,
		monPriceLevel13,
		vcPriceLevel14Name,
		intPriceLevel14FromQty,
		intPriceLevel14ToQty,
		monPriceLevel14,
		vcPriceLevel15Name,
		intPriceLevel15FromQty,
		intPriceLevel15ToQty,
		monPriceLevel15,
		vcPriceLevel16Name,
		intPriceLevel16FromQty,
		intPriceLevel16ToQty,
		monPriceLevel16,
		vcPriceLevel17Name,
		intPriceLevel17FromQty,
		intPriceLevel17ToQty,
		monPriceLevel17,
		vcPriceLevel18Name,
		intPriceLevel18FromQty,
		intPriceLevel18ToQty,
		monPriceLevel18,
		vcPriceLevel19Name,
		intPriceLevel19FromQty,
		intPriceLevel19ToQty,
		monPriceLevel19,
		vcPriceLevel20Name,
		intPriceLevel20FromQty,
		intPriceLevel20ToQty,
		monPriceLevel20
	FROM 
		fn_GetPivotQueryResult(''SELECT 
		row_id, monPriceLevel1, monPriceLevel2, monPriceLevel3, monPriceLevel4, monPriceLevel5, monPriceLevel6, monPriceLevel7, monPriceLevel8, monPriceLevel9, monPriceLevel10, monPriceLevel11, monPriceLevel12, monPriceLevel13, monPriceLevel14, monPriceLevel15, monPriceLevel16, monPriceLevel17, monPriceLevel18, monPriceLevel19, monPriceLevel20, intPriceLevel1FromQty, intPriceLevel2FromQty, intPriceLevel3FromQty, intPriceLevel4FromQty, intPriceLevel5FromQty, intPriceLevel6FromQty, intPriceLevel7FromQty, intPriceLevel8FromQty, intPriceLevel9FromQty, intPriceLevel10FromQty, intPriceLevel11FromQty, intPriceLevel12FromQty, intPriceLevel13FromQty, intPriceLevel14FromQty, intPriceLevel15FromQty, intPriceLevel16FromQty, intPriceLevel17FromQty, intPriceLevel18FromQty, intPriceLevel19FromQty, intPriceLevel20FromQty, intPriceLevel1ToQty, intPriceLevel2ToQty, intPriceLevel3ToQty, intPriceLevel4ToQty, intPriceLevel5ToQty, intPriceLevel6ToQty, intPriceLevel7ToQty, intPriceLevel8ToQty, intPriceLevel9ToQty, intPriceLevel10ToQty, intPriceLevel11ToQty, intPriceLevel12ToQty, intPriceLevel13ToQty, intPriceLevel14ToQty, intPriceLevel15ToQty, intPriceLevel16ToQty, intPriceLevel17ToQty, intPriceLevel18ToQty, intPriceLevel19ToQty, intPriceLevel20ToQty, vcPriceLevel1Name, vcPriceLevel2Name, vcPriceLevel3Name, vcPriceLevel4Name, vcPriceLevel5Name, vcPriceLevel6Name, vcPriceLevel7Name, vcPriceLevel8Name, vcPriceLevel9Name, vcPriceLevel10Name, vcPriceLevel11Name, vcPriceLevel12Name, vcPriceLevel13Name, vcPriceLevel14Name, vcPriceLevel15Name, vcPriceLevel16Name, vcPriceLevel17Name, vcPriceLevel18Name, vcPriceLevel19Name, vcPriceLevel20Name
							   FROM crosstab (
							   ''''Select 
				CAST(TEMP1.ID AS TEXT) AS row_id,
				''''''''monPriceLevel'''''''' || Cast(TEMP1.ID as TEXT) As PriceLevel,
				CAST(decDiscount AS TEXT) AS decDiscount
			From 
		    (
				SELECT
					T2.numItemCode AS ID
					,COALESCE(CAST(T2.decDiscount AS VARCHAR),'''''''''''''''') decDiscount
				FROM
					tt_TEMPIDS T1
				LEFT JOIN
				(
					SELECT
						ROW_NUMBER() OVER(ORDER BY numPricingID) ID
						,numItemCode
						,decDiscount
					FROM
						PricingTable
					WHERE
						numItemCode =  ##RecordID##
						AND COALESCE(numCurrencyID,0) = 0
						AND COALESCE(numPriceRuleID,0) = 0
				) AS T2
				ON
					T1.ID = T2.ID
			) AS TEMP1 order by 1'''') AS t1(row_id TEXT, monPriceLevel1 TEXT, monPriceLevel2 TEXT, monPriceLevel3 TEXT, monPriceLevel4 TEXT, monPriceLevel5 TEXT, monPriceLevel6 TEXT, monPriceLevel7 TEXT, monPriceLevel8 TEXT, monPriceLevel9 TEXT, monPriceLevel10 TEXT, monPriceLevel11 TEXT, monPriceLevel12 TEXT, monPriceLevel13 TEXT, monPriceLevel14 TEXT, monPriceLevel15 TEXT, monPriceLevel16 TEXT, monPriceLevel17 TEXT, monPriceLevel18 TEXT, monPriceLevel19 TEXT, monPriceLevel20 TEXT)
		   JOIN   crosstab (
							   ''''Select 
				CAST(TEMP1.ID AS TEXT) AS row_id,
				''''''''intPriceLevel'''''''' || Cast(TEMP1.ID as TEXT) || ''''''''FromQty'''''''' As FromQty,
				CAST(intFromQty AS TEXT) AS intFromQty
			From 
		    (
				SELECT
					T2.numItemCode AS ID
					,COALESCE(CAST(T2.intFromQty AS VARCHAR),'''''''''''''''') intFromQty
				FROM
					tt_TEMPIDS T1
				LEFT JOIN
				(
					SELECT
						ROW_NUMBER() OVER(ORDER BY numPricingID) ID
						,numItemCode
						,intFromQty
					FROM
						PricingTable
					WHERE
						numItemCode =  ##RecordID##
						AND COALESCE(numCurrencyID,0) = 0
						AND COALESCE(numPriceRuleID,0) = 0
				) AS T2
				ON
					T1.ID = T2.ID
			) AS TEMP1 order by 1'''') AS t2(row_id TEXT, intPriceLevel1FromQty TEXT, intPriceLevel2FromQty TEXT, intPriceLevel3FromQty TEXT, intPriceLevel4FromQty TEXT, intPriceLevel5FromQty TEXT, intPriceLevel6FromQty TEXT, intPriceLevel7FromQty TEXT, intPriceLevel8FromQty TEXT, intPriceLevel9FromQty TEXT, intPriceLevel10FromQty TEXT, intPriceLevel11FromQty TEXT, intPriceLevel12FromQty TEXT, intPriceLevel13FromQty TEXT, intPriceLevel14FromQty TEXT, intPriceLevel15FromQty TEXT, intPriceLevel16FromQty TEXT, intPriceLevel17FromQty TEXT, intPriceLevel18FromQty TEXT, intPriceLevel19FromQty TEXT, intPriceLevel20FromQty TEXT) USING (row_id)
			JOIN   crosstab (
							   ''''Select 
				CAST(TEMP1.ID AS TEXT) AS row_id,
				''''''''intPriceLevel'''''''' || Cast(TEMP1.ID as TEXT) || ''''''''ToQty'''''''' As ToQty,
				CAST(intToQty AS TEXT) AS intToQty
			From 
		    (
				SELECT
					T2.numItemCode AS ID
					,COALESCE(CAST(T2.intToQty AS VARCHAR),'''''''''''''''') intToQty
				FROM
					tt_TEMPIDS T1
				LEFT JOIN
				(
					SELECT
						ROW_NUMBER() OVER(ORDER BY numPricingID) ID
						,numItemCode
						,intToQty
					FROM
						PricingTable
					WHERE
						numItemCode =  ##RecordID##
						AND COALESCE(numCurrencyID,0) = 0
						AND COALESCE(numPriceRuleID,0) = 0
				) AS T2
				ON
					T1.ID = T2.ID
			) AS TEMP1 order by 1'''') AS t3(row_id TEXT, intPriceLevel1ToQty TEXT, intPriceLevel2ToQty TEXT, intPriceLevel3ToQty TEXT, intPriceLevel4ToQty TEXT, intPriceLevel5ToQty TEXT, intPriceLevel6ToQty TEXT, intPriceLevel7ToQty TEXT, intPriceLevel8ToQty TEXT, intPriceLevel9ToQty TEXT, intPriceLevel10ToQty TEXT, intPriceLevel11ToQty TEXT, intPriceLevel12ToQty TEXT, intPriceLevel13ToQty TEXT, intPriceLevel14ToQty TEXT, intPriceLevel15ToQty TEXT, intPriceLevel16ToQty TEXT, intPriceLevel17ToQty TEXT, intPriceLevel18ToQty TEXT, intPriceLevel19ToQty TEXT, intPriceLevel20ToQty TEXT) USING (row_id)
			JOIN   crosstab (
							   ''''Select 
				CAST(TEMP1.ID AS TEXT) AS row_id,
				''''''''vcPriceLevel'''''''' || Cast(TEMP1.ID as TEXT) || ''''''''Name'''''''' As PriceLevelName,
				CAST(vcName AS TEXT) AS vcName
			From 
		    (
				SELECT
					T2.numItemCode AS ID
					,COALESCE(T2.vcName,'''''''''''''''') vcName
				FROM
					tt_TEMPIDS T1
				LEFT JOIN
				(
					SELECT
						ROW_NUMBER() OVER(ORDER BY numPricingID) ID
						,numItemCode
						,vcName
					FROM
						PricingTable
					WHERE
						numItemCode = ##RecordID##
						AND COALESCE(numCurrencyID,0) = 0
						AND COALESCE(numPriceRuleID,0) = 0
				) AS T2
				ON
					T1.ID = T2.ID
			) AS TEMP1 order by 1'''') AS t4(row_id TEXT, vcPriceLevel1Name TEXT, vcPriceLevel2Name TEXT, vcPriceLevel3Name TEXT, vcPriceLevel4Name TEXT, vcPriceLevel5Name TEXT, vcPriceLevel6Name TEXT, vcPriceLevel7Name TEXT, vcPriceLevel8Name TEXT, vcPriceLevel9Name TEXT, vcPriceLevel10Name TEXT, vcPriceLevel11Name TEXT, vcPriceLevel12Name TEXT, vcPriceLevel13Name TEXT, vcPriceLevel14Name TEXT, vcPriceLevel15Name TEXT, vcPriceLevel16Name TEXT, vcPriceLevel17Name TEXT, vcPriceLevel18Name TEXT, vcPriceLevel19Name TEXT, vcPriceLevel20Name TEXT) USING (row_id)'',I.numItemCode) 
			AS t(row_id TEXT, monPriceLevel1 TEXT, monPriceLevel2 TEXT, monPriceLevel3 TEXT, monPriceLevel4 TEXT, monPriceLevel5 TEXT, monPriceLevel6 TEXT, monPriceLevel7 TEXT, monPriceLevel8 TEXT, monPriceLevel9 TEXT, monPriceLevel10 TEXT, monPriceLevel11 TEXT, monPriceLevel12 TEXT, monPriceLevel13 TEXT, monPriceLevel14 TEXT, monPriceLevel15 TEXT, monPriceLevel16 TEXT, monPriceLevel17 TEXT, monPriceLevel18 TEXT, monPriceLevel19 TEXT, monPriceLevel20 TEXT, intPriceLevel1FromQty TEXT, intPriceLevel2FromQty TEXT, intPriceLevel3FromQty TEXT, intPriceLevel4FromQty TEXT, intPriceLevel5FromQty TEXT, intPriceLevel6FromQty TEXT, intPriceLevel7FromQty TEXT, intPriceLevel8FromQty TEXT, intPriceLevel9FromQty TEXT, intPriceLevel10FromQty TEXT, intPriceLevel11FromQty TEXT, intPriceLevel12FromQty TEXT, intPriceLevel13FromQty TEXT, intPriceLevel14FromQty TEXT, intPriceLevel15FromQty TEXT, intPriceLevel16FromQty TEXT, intPriceLevel17FromQty TEXT, intPriceLevel18FromQty TEXT, intPriceLevel19FromQty TEXT, intPriceLevel20FromQty TEXT, intPriceLevel1ToQty TEXT, intPriceLevel2ToQty TEXT, intPriceLevel3ToQty TEXT, intPriceLevel4ToQty TEXT, intPriceLevel5ToQty TEXT, intPriceLevel6ToQty TEXT, intPriceLevel7ToQty TEXT, intPriceLevel8ToQty TEXT, intPriceLevel9ToQty TEXT, intPriceLevel10ToQty TEXT, intPriceLevel11ToQty TEXT, intPriceLevel12ToQty TEXT, intPriceLevel13ToQty TEXT, intPriceLevel14ToQty TEXT, intPriceLevel15ToQty TEXT, intPriceLevel16ToQty TEXT, intPriceLevel17ToQty TEXT, intPriceLevel18ToQty TEXT, intPriceLevel19ToQty TEXT, intPriceLevel20ToQty TEXT, vcPriceLevel1Name TEXT, vcPriceLevel2Name TEXT, vcPriceLevel3Name TEXT, vcPriceLevel4Name TEXT, vcPriceLevel5Name TEXT, vcPriceLevel6Name TEXT, vcPriceLevel7Name TEXT, vcPriceLevel8Name TEXT, vcPriceLevel9Name TEXT, vcPriceLevel10Name TEXT, vcPriceLevel11Name TEXT, vcPriceLevel12Name TEXT, vcPriceLevel13Name TEXT, vcPriceLevel14Name TEXT, vcPriceLevel15Name TEXT, vcPriceLevel16Name TEXT, vcPriceLevel17Name TEXT, vcPriceLevel18Name TEXT, vcPriceLevel19Name TEXT, vcPriceLevel20Name TEXT)) AS TempPriceTable ON true';
	   
	v_GroupBy := '';
	v_SelectFields := '';  
	v_WCondition := '';

	v_Where := coalesce(v_Where,'') || ' WHERE I.charItemType <>''A'' and I.numDomainID=' || COALESCE(v_numDomainID,0);

	DROP TABLE IF EXISTS tt_FIELLIST CASCADE;
	CREATE TEMPORARY TABLE tt_FIELLIST AS
    SELECT 
		X.*
		,ROW_NUMBER() OVER(ORDER BY numFieldID ASC) AS tintColumn        
    FROM
	(
		SELECT 
			vcDbColumnName,
            vcFieldName,
            vcAssociatedControlType,
            vcListItemType,
            numListID,
            vcLookBackTableName,
            bitCustom,
            numFieldID
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId = 20
			AND numDomainID = v_numDomainID
			AND bitImport = true
			AND numFieldID NOT IN(SELECT numFieldID FROM DycFieldMaster WHERE vcDbColumnName = 'numItemCode' AND vcLookBackTableName = 'Item')
      UNION ALL
      SELECT 
		vcDbColumnName,
        vcFieldName,
        vcAssociatedControlType,
        '' AS vcListItemType,
        numListID,
        '' AS vcLookBackTableName,
        bitCustom,
        numFieldId
      FROM 
		View_DynamicCustomColumns
      WHERE 
		numDomainID = v_numDomainID
		AND Grp_id = 5
		AND numFormId = 20
	) X
    ORDER BY 
		tintColumn ASC;

	v_tintOrder := 0;

	v_strSql := 'SELECT DISTINCT I.numItemCode AS "Item ID" ';

	SELECT 
		tintColumn+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numFieldId 
	INTO 
		v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,v_fld_id 
	FROM 
		tt_FIELLIST 
	ORDER BY 
		tintColumn ASC 
	LIMIT 1;

	WHILE v_tintOrder > 0 LOOP
		RAISE NOTICE '%',v_bitCustom;
		RAISE NOTICE '%',v_vcAssociatedControlType;
		RAISE NOTICE '%',v_vcLookBackTableName;
		RAISE NOTICE '%',v_vcDbColumnName;
		RAISE NOTICE '%',v_vcListItemType;

		IF coalesce(v_bitCustom,false) = false then       
			IF v_vcAssociatedControlType = 'SelectBox' then          
				IF v_vcLookBackTableName = 'Warehouses' then            
					v_SelectFields := coalesce(v_SelectFields,'') || ' ,W.vcWarehouse "' || coalesce(v_vcFieldName,'') || '"';

					IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID ';
					end if;

					IF POSITION('WareHouseItmsDTL' IN v_InneJoinOn) = 0 then		
						IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then			
							v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID ';
						end if;
				  
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || '  Left JOIN WareHouseItmsDTL WID ON WID.numWareHouseItemID = WI.numWareHouseItemID  ';
					end if;

					IF POSITION('WareHouses W' IN v_InneJoinOn) = 0 then
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouses W ON W.numWareHouseID = WI.numWareHouseID  ';
					end if;
				ELSEIF v_vcLookBackTableName = 'WareHouseItems' then	
					IF v_vcDbColumnName = 'vcLocation' then
						v_SelectFields := coalesce(v_SelectFields,'') || ' ,WL.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';
						
						IF POSITION('WarehouseLocation' IN v_InneJoinOn) = 0 then
							IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then							
								v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID ';
							end if;
						end if;
				  
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID and WI.numDomainID = WL.numDomainID ';
					end if;
				ELSEIF v_vcLookBackTableName = 'CompanyAssets' then
					v_SelectFields := coalesce(v_SelectFields,'') || ' ,fn_GetComapnyName(CA.numDivId) "'|| coalesce(v_vcFieldName,'') || '"';

					IF POSITION('CompanyAssets CA' IN v_InneJoinOn) = 0 then
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || 'LEFT JOIN  CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId ';
					end if;
				ELSEIF v_vcLookBackTableName = 'Category' then
					v_SelectFields := coalesce(v_SelectFields,'')  || ' ,C.vcCategoryName "' || coalesce(v_vcFieldName,'') || '"'; 
					
					IF POSITION('Category C' IN v_InneJoinOn) = 0 then
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN ItemCategory IC on IC.numItemID = I.numItemCode LEFT JOIN Category C ON C.numCategoryID = IC.numCategoryID ';
					end if;
				ELSEIF v_vcLookBackTableName = 'chart_of_accounts' then            
					IF v_vcDbColumnName = 'numCOGsChartAcntId' then
						v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numCOGsChartAcntId) as "' || coalesce(v_vcFieldName,'') || '"';
					ELSEIF v_vcDbColumnName = 'numAssetChartAcntId' then
						v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numAssetChartAcntId) as  "' || coalesce(v_vcFieldName,'') || '"';
					ELSEIF v_vcDbColumnName = 'numIncomeChartAcntId' then
						v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcAccountName from chart_of_Accounts COA where COA.numAccountId=numIncomeChartAcntId) as  "' || coalesce(v_vcFieldName,'') || '"';
					end if;
				ELSEIF v_vcLookBackTableName = 'DivisionMaster' then                   
					v_SelectFields := coalesce(v_SelectFields,'') || ' ,fn_GetComapnyName(V.numVendorID) "' || coalesce(v_vcFieldName,'') || '"';

					IF POSITION('Vendor' IN v_InneJoinOn) = 0 then
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID ';
					end if;
				ELSEIF v_vcLookBackTableName = 'Vendor' then
				   v_SelectFields := coalesce(v_SelectFields,'') || ' ,fn_GetComapnyName(V.numVendorID) "' || coalesce(v_vcFieldName,'') || '"';
				   
				   IF POSITION('Vendor' IN v_InneJoinOn) = 0 then
					  v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID ';
				   end if;
				ELSEIF v_vcLookBackTableName = 'PricingTable' then                        
					IF v_vcFieldName = 'Price Rule Type' then										
						v_SelectFields := coalesce(v_SelectFields,'') || '  , (CASE WHEN (SELECT DISTINCT tintRuleType FROM PricingTable WHERE numItemCode = I.numItemCode AND COALESCE(numCurrencyID,0) = 0) = 1
																												  THEN ''Deduct From List Price''
																												  WHEN (SELECT DISTINCT tintRuleType FROM PricingTable WHERE numItemCode = I.numItemCode AND COALESCE(numCurrencyID,0) = 0) = 2 
																												  THEN ''Add to Primary Vendor Cost''
																												  WHEN (SELECT DISTINCT tintRuleType FROM PricingTable WHERE numItemCode = I.numItemCode AND COALESCE(numCurrencyID,0) = 0) = 3 
																												  THEN ''Named Price''
																												  ELSE ''''
																										      END) AS "Price Rule Type" ';
					end if;
					IF v_vcFieldName = 'Price Level Discount Type' then									
						v_SelectFields := coalesce(v_SelectFields,'') || '  ,(CASE WHEN (SELECT DISTINCT tintDiscountType FROM PricingTable WHERE numItemCode = I.numItemCode AND COALESCE(numCurrencyID,0) = 0) = 1
																												  THEN ''Percentage''
																												  WHEN (SELECT DISTINCT tintDiscountType FROM PricingTable WHERE numItemCode = I.numItemCode AND COALESCE(numCurrencyID,0) = 0) = 2 
																												  THEN ''Flat Amount''
																												  ELSE ''''
																										      END) AS "Price Level Discount Type" ';
					end if;

					v_SelectFields := REPLACE(v_SelectFields,',I.vcPriceLevelDetail [Price Level]','');
				ELSEIF v_vcListItemType = 'IG' then --Item group                            
					v_SelectFields := coalesce(v_SelectFields,'') || ',IG.vcItemGroup "' || coalesce(v_vcFieldName,'') || '"';
					v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID ';
				ELSEIF v_vcListItemType = 'PP' then--Item Type                                      
				   v_SelectFields := coalesce(v_SelectFields,'') || ', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   "' || coalesce(v_vcFieldName,'') || '"';
				ELSEIF v_vcListItemType = 'LI' then--Any List Item                                             
				   v_SelectFields := coalesce(v_SelectFields,'') || ',L' || v_tintOrder || '.vcData' || ' "' || coalesce(v_vcFieldName,'') || '"';
				   v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join ListDetails L' || v_tintOrder || ' on L' || v_tintOrder || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
				ELSEIF v_vcListItemType = 'UOM' then                                      
				   IF v_vcDbColumnName = 'numBaseUnit' then
					  v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcUnitName from UOM where numUOMId=COALESCE(I.numBaseUnit,0)) as  "' || coalesce(v_vcFieldName,'') || '"';
				   ELSEIF v_vcDbColumnName = 'numPurchaseUnit' then
					  v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcUnitName from UOM where numUOMId=COALESCE(I.numPurchaseUnit,0)) as  "' || coalesce(v_vcFieldName,'') || '"';
				   ELSEIF v_vcDbColumnName = 'numSaleUnit' then
					  v_SelectFields := coalesce(v_SelectFields,'') || ' ,(select vcUnitName from UOM where numUOMId=COALESCE(I.numSaleUnit,0)) as  "' || coalesce(v_vcFieldName,'') || '"';
				   end if;
				end if;
			ELSEIF v_vcLookBackTableName = 'WareHouseItmsDTL' then
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,WID.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';

				IF POSITION('WareHouseItmsDTL' IN v_InneJoinOn) = 0 then			
					IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then					
						v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID ';
					end if;

					v_InneJoinOn := coalesce(v_InneJoinOn,'') || '  Left JOIN WareHouseItmsDTL WID ON WI.numWarehouseItemID = WI.numWarehouseItemID';
				end if;
			ELSEIF v_vcLookBackTableName = 'WareHouseItems' then
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,WI.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';

				IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then
					v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' Left JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID';
				end if;
			ELSEIF v_vcLookBackTableName = 'CompanyAssets' then          
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,CA.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';

				IF POSITION('CompanyAssets CA' IN v_InneJoinOn) = 0 then
					v_InneJoinOn := coalesce(v_InneJoinOn,'')
					|| 'LEFT JOIN  CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId ';
				end if;
			ELSEIF v_vcLookBackTableName = 'Vendor' then   
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,V.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';
				
				IF POSITION('Vendor' IN v_InneJoinOn) = 0 then
					v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID ';
				end if;
			ELSEIF v_vcDbColumnName = 'vcSerialNo' then
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,0 "' || coalesce(v_vcFieldName,'') || '"';

				IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then
					v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode ';
				end if;
			ELSEIF v_vcDbColumnName = 'vcPartNo' then   
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,0 "' || coalesce(v_vcFieldName,'') || '"';
			ELSEIF v_vcDbColumnName = 'charItemType' then   
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,case when i.charItemType=''P'' then ''Inventory Item'' when i.charItemType=''S'' then ''Service'' when i.charItemType=''A'' then ''Asset Item'' when i.charItemType=''N'' then ''Non-Inventory Item'' ELSE ''Non-Inventory Item'' END  "' || coalesce(v_vcFieldName,'') || '"';
			ELSEIF v_vcDbColumnName = 'txtDesc' then            
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,IED.txtDesc "' || coalesce(v_vcFieldName,'') || '"';
				v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left JOIN ItemExtendedDetails IED ON IED.numItemCode = I.numItemCode ';
			ELSEIF v_vcDbColumnName = 'txtShortDesc' then       
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,IED1.txtShortDesc "' || coalesce(v_vcFieldName,'') || '"';
				v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left JOIN ItemExtendedDetails IED1 ON IED1.numItemCode = I.numItemCode ';
			ELSEIF v_vcDbColumnName = 'monPriceLevel1' then					
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel1 AS "monPriceLevel1"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel2' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel2 AS "monPriceLevel2"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel3' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel3 AS "monPriceLevel3"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel4' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel4 AS "monPriceLevel4"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel5' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel5 AS "monPriceLevel5"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel6' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel6 AS "monPriceLevel6"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel7' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel7 AS "monPriceLevel7"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel8' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel8 AS "monPriceLevel8"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel9' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel9 AS "monPriceLevel9"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel10' then															
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel10 AS "monPriceLevel10"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel11' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel11 AS "monPriceLevel11"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel12' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel12 AS "monPriceLevel12"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel13' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel13 AS "monPriceLevel13"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel14' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel14 AS "monPriceLevel14"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel15' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel15 AS "monPriceLevel15"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel16' then					
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel16 AS "monPriceLevel16"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel17' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel17 AS "monPriceLevel17"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel18' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel18 AS "monPriceLevel18"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel19' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel19 AS "monPriceLevel19"';
			ELSEIF v_vcDbColumnName = 'monPriceLevel20' then	
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.monPriceLevel20 AS "monPriceLevel20"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel1FromQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel1FromQty AS "intPriceLevel1FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel1ToQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel1ToQty AS "intPriceLevel1ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel2FromQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel2FromQty AS "intPriceLevel2FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel2ToQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel2ToQty AS "intPriceLevel2ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel3FromQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel3FromQty AS "intPriceLevel3FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel3ToQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel3ToQty AS "intPriceLevel3ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel4FromQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel4FromQty AS "intPriceLevel4FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel4ToQty' then
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel4ToQty AS "intPriceLevel4ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel5FromQty' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel5FromQty AS "intPriceLevel5FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel5ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel5ToQty AS "intPriceLevel5ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel6FromQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel6FromQty AS "intPriceLevel6FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel6ToQty' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel6ToQty AS "intPriceLevel6ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel7FromQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel7FromQty AS "intPriceLevel7FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel7ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel7ToQty AS "intPriceLevel7ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel8FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel8FromQty AS "intPriceLevel8FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel8ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel8ToQty AS "intPriceLevel8ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel9FromQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel9FromQty AS "intPriceLevel9FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel9ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel9ToQty AS "intPriceLevel9ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel10FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel10FromQty AS "intPriceLevel10FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel10ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel10ToQty AS "intPriceLevel10ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel11FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel11FromQty AS "intPriceLevel11FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel11ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel11ToQty AS "intPriceLevel11ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel12FromQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel12FromQty AS "intPriceLevel12FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel12ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel12ToQty AS "intPriceLevel12ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel13FromQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel13FromQty AS "intPriceLevel13FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel13ToQty' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel13ToQty AS "intPriceLevel13ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel14FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel14FromQty AS "intPriceLevel14FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel14ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel14ToQty AS "intPriceLevel14ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel15FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel15FromQty AS "intPriceLevel15FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel15ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel15ToQty AS "intPriceLevel15ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel16FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel16FromQty AS "intPriceLevel16FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel16ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel16ToQty AS "intPriceLevel16ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel17FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel17FromQty AS "intPriceLevel17FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel17ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel17ToQty AS "intPriceLevel17ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel18FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel18FromQty AS "intPriceLevel18FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel18ToQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel18ToQty AS "intPriceLevel18ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel19FromQty' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel19FromQty AS "intPriceLevel19FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel19ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel19ToQty AS "intPriceLevel19ToQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel20FromQty' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel20FromQty AS "intPriceLevel20FromQty"';
			ELSEIF v_vcDbColumnName = 'intPriceLevel20ToQty' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.intPriceLevel20ToQty AS "intPriceLevel20ToQty"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel1Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel1Name AS "vcPriceLevel1Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel2Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel2Name AS "vcPriceLevel2Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel3Name' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel3Name AS "vcPriceLevel3Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel4Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel4Name AS "vcPriceLevel4Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel5Name' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel5Name AS "vcPriceLevel5Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel6Name' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel6Name AS "vcPriceLevel6Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel7Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel7Name AS "vcPriceLevel7Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel8Name' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel8Name AS "vcPriceLevel8Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel9Name' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel9Name AS "vcPriceLevel9Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel10Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel10Name AS "vcPriceLevel10Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel11Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel11Name AS "vcPriceLevel11Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel12Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel12Name AS "vcPriceLevel12Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel13Name' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel13Name AS "vcPriceLevel13Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel14Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel14Name AS "vcPriceLevel14Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel15Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel15Name AS "vcPriceLevel15Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel16Name' then								
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel16Name AS "vcPriceLevel16Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel17Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel17Name AS "vcPriceLevel17Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel18Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel18Name AS "vcPriceLevel18Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel19Name' then										
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel19Name AS "vcPriceLevel19Name"';
			ELSEIF v_vcDbColumnName = 'vcPriceLevel20Name' then									
				v_SelectFields := coalesce(v_SelectFields,'') || '  ,TempPriceTable.vcPriceLevel20Name AS "vcPriceLevel20Name"';
			ELSEIF v_vcDbColumnName = 'bitAllowBackOrder' OR v_vcDbColumnName = 'bitTaxable' OR v_vcDbColumnName = 'IsArchieve' then                                       
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,Case  I.' || coalesce(v_vcDbColumnName,'') || ' when true then ''TRUE'' ELSE ''FALSE'' END "' || coalesce(v_vcFieldName,'') || '"';
			ELSE
				v_SelectFields := coalesce(v_SelectFields,'') || ' ,I.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFieldName,'') || '"';
			end if;
		ELSEIF v_bitCustom = true then              
		/*--------------Custom field logic-----------*/
		
			RAISE NOTICE '%',v_tintOrder;
			RAISE NOTICE '%',v_vcAssociatedControlType;
			RAISE NOTICE '%',v_vcFieldName;

			IF v_vcAssociatedControlType = 'Drop Down' then        
				v_SelectFields := coalesce(v_SelectFields,'') || ',L' || v_tintOrder || '.vcData "' || coalesce(v_vcFieldName,'') || '"';
				v_InneJoinOn := coalesce(v_InneJoinOn,'') 
				|| ' left join CFW_FLD_Values_Item CFV' 
				|| v_tintOrder 
				|| ' ON CFV' 
				|| v_tintOrder 
				|| '.RecId = I.numItemCode and CFV'
				|| v_tintOrder
				|| '.Fld_Id = '
				|| v_fld_id
				|| ' left Join ListDetails L'
				|| v_tintOrder
				|| ' on L'
				|| v_tintOrder
				|| '.numListItemID = (CASE WHEN ISNUMERIC(CFV'
				|| v_tintOrder
				|| '.Fld_Value)=1 THEN CAST(CFV'
				|| v_tintOrder
				|| '.Fld_Value AS NUMERIC(9)) ELSE 0 END)';
			ELSE
				v_SelectFields := coalesce(v_SelectFields,'') || ',CFV' || v_tintOrder || '.Fld_Value "' || coalesce(v_vcFieldName,'') || '"';

				v_InneJoinOn := coalesce(v_InneJoinOn,'')
				|| ' left join CFW_FLD_Values_Item CFV'
				|| v_tintOrder
				|| ' ON CFV'
				|| v_tintOrder
				|| '.RecId = I.numItemCode and CFV'
				|| v_tintOrder
				|| '.Fld_Id = '
				|| v_fld_id;
			 end if;
		end if;

		IF POSITION('WareHouseItmsDTL' IN v_Where) > 0 then
			IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then
				v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode ';
			end if;
		end if;

		SELECT 
			tintColumn+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numFieldId 
		INTO 
			v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,v_fld_id 
		FROM 
			tt_FIELLIST 
		WHERE  
			tintColumn > v_tintOrder -1
		ORDER BY 
			tintColumn ASC 
		LIMIT 1;
     
		GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;

		IF SWV_RowCount = 0 then
			v_tintOrder := 0;
		end if;
	END LOOP;                              
	
	IF POSITION('V.bitIsPrimaryVendor' IN v_SelectFields) > 0 then
		v_SelectFields := REPLACE(v_SelectFields,'V.bitIsPrimaryVendor',' CASE WHEN V.numVendorID = I.numVendorID THEN 1 ELSE 0 END ');
	end if;

	v_strSql := coalesce(v_strSql,'') || coalesce(v_SelectFields,'') || coalesce(v_from,'') || coalesce(v_InneJoinOn,'') || coalesce(v_Where,'') || coalesce(v_OrderBy,'');

	v_strSql := coalesce(v_strSql,'') || coalesce(v_GroupBy,''); /*+ ' order by vcItemName; '*/
   
	RAISE NOTICE '%',v_strSql;
	OPEN SWV_RefCur FOR EXECUTE v_strSql;
   
	RETURN;
-------------------------------------------------	
END; $$;




