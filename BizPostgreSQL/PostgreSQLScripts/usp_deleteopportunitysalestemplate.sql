-- Stored procedure definition script USP_DeleteOpportunitySalesTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteOpportunitySalesTemplate(v_numSalesTemplateID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM SalesTemplateItems WHERE numSalesTemplateID = v_numSalesTemplateID AND numDomainID = v_numDomainID;

   DELETE FROM OpportunitySalesTemplate
   WHERE numSalesTemplateID = v_numSalesTemplateID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;

--modified by anoop jayaraj              


