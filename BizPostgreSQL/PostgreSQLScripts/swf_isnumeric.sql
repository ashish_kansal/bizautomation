-- Function definition script dbo.SWF_IsNumeric for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE or REPLACE FUNCTION SWF_isnumeric(text) RETURNS boolean AS '
SELECT $1 ~ ''^[0-9.]+$''
' LANGUAGE 'sql'

