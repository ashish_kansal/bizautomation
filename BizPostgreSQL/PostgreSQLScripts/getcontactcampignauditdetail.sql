-- Function definition script GetContactCampignAuditDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetContactCampignAuditDetail(v_numECampaignID NUMERIC(18,0), v_numContactID NUMERIC(18,0))
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCampaignAudit  VARCHAR(500) DEFAULT '';
   v_CampaignName  VARCHAR(200);
   v_NextDrip  VARCHAR(200);
   v_CompletedStages  VARCHAR(200);
   v_LastDrip  VARCHAR(200);

   v_numDomainID  NUMERIC(18,0);
   v_ConECampaignID NUMERIC;
   v_bitCompleted  BOOLEAN DEFAULT 0;
BEGIN
   IF(SELECT COUNT(*) FROM ECampaign WHERE numECampaignID = v_numECampaignID) > 0 then
	
      select   vcECampName, numDomainID INTO v_vcCampaignAudit,v_numDomainID FROM ECampaign WHERE numECampaignID = v_numECampaignID;
      select   numConEmailCampID INTO v_ConECampaignID FROM ConECampaign WHERE numECampaignID = v_numECampaignID AND numContactID = v_numContactID   ORDER BY numConEmailCampID DESC LIMIT 1;
      select   CONCAT(EXTRACT(MONTH FROM dtExecutionDate),'-',EXTRACT(DAY FROM dtExecutionDate)) INTO v_NextDrip FROM ConECampaignDTL WHERE numConECampID = v_ConECampaignID AND coalesce(bitSend,false) = false ORDER BY numConECampDTLID ASC LIMIT 1;
      select   CONCAT(EXTRACT(MONTH FROM bintSentON),'-',EXTRACT(DAY FROM bintSentON)) INTO v_LastDrip FROM ConECampaignDTL WHERE numConECampID = v_ConECampaignID AND coalesce(bitSend,false) = true   ORDER BY numConECampDTLID DESC LIMIT 1;
      select   COUNT(*) INTO v_CompletedStages FROM ConECampaignDTL WHERE numConECampID = v_ConECampaignID AND coalesce(bitSend,false) = true;
      IF(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = v_ConECampaignID AND coalesce(bitSend,false) = false) = 0 then
		
         v_bitCompleted := true;
      end if;
      v_vcCampaignAudit := coalesce(v_vcCampaignAudit,'') || ' ' || coalesce(v_NextDrip,'') || ' / Audit (' || coalesce(v_CompletedStages,cast(0 as TEXT)) || ') ' ||  coalesce(v_LastDrip,'') || ' ' ||(CASE WHEN v_bitCompleted = true THEN '#Completed#' ELSE '#Running#' END);
   end if;

   RETURN v_vcCampaignAudit;
END; $$;

