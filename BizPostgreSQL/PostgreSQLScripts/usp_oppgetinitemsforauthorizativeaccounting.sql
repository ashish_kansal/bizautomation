DROP FUNCTION IF EXISTS USP_OPPGetINItemsForAuthorizativeAccounting;

CREATE OR REPLACE FUNCTION USP_OPPGetINItemsForAuthorizativeAccounting(v_numOppId NUMERIC(18,0) DEFAULT null,                                                                                                                                                
	v_numOppBizDocsId NUMERIC(18,0) DEFAULT null,                                                                
	v_numDomainID NUMERIC(18,0) DEFAULT 0 ,                                                      
	v_numUserCntID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCurrencyID  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;

   v_fltCurrentExchangeRate  DOUBLE PRECISION;
   v_tintType  SMALLINT;
   v_vcPOppName  VARCHAR(100);
   v_bitPPVariance  BOOLEAN;
   v_numDivisionID  NUMERIC(18,0);
   v_vcBaseCurrency  VARCHAR(100) DEFAULT '';
   v_bitAutolinkUnappliedPayment  BOOLEAN; 

   v_vcForeignCurrency  VARCHAR(100) DEFAULT '';
   v_fltExchangeRateBizDoc  DOUBLE PRECISION; 
   v_numBizDocId  NUMERIC(18,0);                                                           
   v_vcBizDocID  VARCHAR(100);
BEGIN
   select   numCurrencyID, fltExchangeRate, tintopptype, vcpOppName, coalesce(bitPPVariance,false), numDivisionId INTO v_numCurrencyID,v_fltExchangeRate,v_tintType,v_vcPOppName,v_bitPPVariance,
   v_numDivisionID FROM
   OpportunityMaster WHERE
   numOppId = v_numOppId; 

   select   coalesce(C.varCurrSymbol,''), coalesce(bitAutolinkUnappliedPayment,false) INTO v_vcBaseCurrency,v_bitAutolinkUnappliedPayment FROM
   Domain D
   LEFT JOIN
   Currency C
   ON
   D.numCurrencyID = C.numCurrencyID WHERE
   D.numDomainId = v_numDomainID;

   v_vcForeignCurrency := COALESCE((select C.varCurrSymbol FROM
   Currency C WHERE
   numCurrencyID = v_numCurrencyID),'');

   select   numBizDocId, fltExchangeRateBizDoc, vcBizDocID INTO v_numBizDocId,v_fltExchangeRateBizDoc,v_vcBizDocID FROM
   OpportunityBizDocs WHERE
   numOppBizDocsId = v_numOppBizDocsId;                                                         

   IF v_numCurrencyID > 0 then
      v_fltCurrentExchangeRate := GetExchangeRate(v_numDomainID,v_numCurrencyID);
   ELSE
      v_fltCurrentExchangeRate := v_fltExchangeRate;
   end if;                                                                                                                                          

   IF v_numBizDocId = 296 OR v_numBizDocId = 287 then --FULFILLMENT BIZDOC OR INVOICE
	
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
      UPDATE
      OpportunityBizDocItems OBDI
      SET
      monAverageCost =(CASE
      WHEN coalesce(I.bitKitParent,false) = true
      THEN coalesce((SELECT SUM(coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(IInner.monAverageCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID = IInner.numItemCode WHERE OKI.numOppId = OI.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)+coalesce((SELECT SUM(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(IInner.monAverageCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID = IInner.numItemCode WHERE OKI.numOppId = OI.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)
      ELSE coalesce(I.monAverageCost,0)
      END)
      FROM
      OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode
      WHERE
      OBDI.numOppItemID = OI.numoppitemtCode AND(OBDI.numOppBizDocID = v_numOppBizDocsId
      AND OBDI.monAverageCost IS NULL);
   end if;
                                                                                                                     
   open SWV_RefCur for
   SELECT
   I.vcItemName as Item
		,charitemType as type
		,OBI.vcItemDesc as "desc"
		,OBI.numUnitHour as Unit
		,coalesce(OBI.monPrice,0) as Price
		,coalesce(OBI.monTotAmount,0) as Amount
		,coalesce((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
		,coalesce(monListPrice,0) as listPrice
		,CAST(I.numItemCode AS VARCHAR(30)) as ItemCode
		,numoppitemtCode
		,L.vcData as vcItemClassification
		,case when bitTaxable = false then 'No' else 'Yes' end as Taxable
		,coalesce(I.numIncomeChartAcntId,0) as itemIncomeAccount
		,coalesce(I.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType
		,coalesce(I.numCOGsChartAcntId,0) as itemCoGs
		,coalesce(I.bitExpenseItem,false) as bitExpenseItem
		,coalesce(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(opp.monAvgCost,0) END) as AverageCost
		,coalesce(OBI.bitDropShip,false) as bitDropShip
		,(OBI.monTotAmtBefDiscount -OBI.monTotAmount) as DiscAmt
		,NULLIF(opp.numProjectID,0) AS numProjectID
		,NULLIF(opp.numClassID,0) AS numClassID
		,coalesce(I.bitKitParent,false) AS bitKitParent
		,coalesce(I.bitAssembly,false) AS bitAssembly
		,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(OBI.monAverageCost,0) END) AS ShippedAverageCost
   FROM
   OpportunityItems opp
   INNER JOIN
   OpportunityBizDocItems OBI
   ON
   OBI.numOppItemID = opp.numoppitemtCode
   LEFT JOIN
   Item I
   ON
   opp.numItemCode = I.numItemCode
   LEFT JOIN
   Listdetails L
   ON
   I.numItemClassification = L.numListItemID
   WHERE
   opp.numOppId = v_numOppId
   AND OBI.numOppBizDocID = v_numOppBizDocsId;
                                                               
   open SWV_RefCur2 for
   SELECT
   coalesce(v_numCurrencyID,0) as numCurrencyID
		,coalesce(v_fltExchangeRate,1) as fltExchangeRate
		,coalesce(v_fltCurrentExchangeRate,1) as CurrfltExchangeRate
		,coalesce(v_fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc
		,v_vcBaseCurrency AS vcBaseCurrency
		,v_vcForeignCurrency AS vcForeignCurrency
		,coalesce(v_vcPOppName,'') AS vcPOppName
		,coalesce(v_vcBizDocID,'') AS vcBizDocID
		,coalesce(v_bitPPVariance,false) AS bitPPVariance
		,v_numDivisionID AS numDivisionID
		,coalesce(v_bitAutolinkUnappliedPayment,false) AS bitAutolinkUnappliedPayment;
   RETURN;
END; $$;


