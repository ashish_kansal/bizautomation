-- Stored procedure definition script USP_CustomQueryReport_GetByID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_GetByID(v_numReportID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		vcEmailTo,
		tintEmailFrequency,
		vcQuery,
		vcCSS
   FROM
   CustomQueryReport
   WHERE
   numReportID = v_numReportID;
END; $$;











