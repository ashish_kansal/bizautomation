-- Stored procedure definition script USP_ManagaeAlertsConAndCom for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagaeAlertsConAndCom(v_numAlerDTLId NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numAlertConID NUMERIC(9,0) DEFAULT 0,
v_byteMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      insert into AlertContactsAndCompany(numAlerDTLId,numContactID)
	values(v_numAlerDTLId,v_numContactID);
   end if;
   if v_byteMode = 1 then

      delete from AlertContactsAndCompany where numAlertConID = v_numAlertConID;
   end if;
   RETURN;
END; $$;


