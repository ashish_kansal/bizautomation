-- Function definition script fn_GetChart_Of_AccountsName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetChart_Of_AccountsName(v_numAccountId NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAccountName  VARCHAR(100);
BEGIN
   select   coalesce(vcAccountName,'') INTO v_vcAccountName from Chart_Of_Accounts where numAccountId = v_numAccountId;

   return coalesce(v_vcAccountName,'-');
END; $$;

