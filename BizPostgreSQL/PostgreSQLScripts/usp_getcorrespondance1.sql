-- Stored procedure definition script Usp_getCorrespondance1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_getCorrespondance1(v_FromDate TIMESTAMP,  
v_toDate TIMESTAMP,  
v_numContactId NUMERIC,  
v_vcMessageFrom VARCHAR(200) DEFAULT '' ,
v_tintSortOrder NUMERIC DEFAULT NULL,
v_SeachKeyword VARCHAR(100) DEFAULT NULL,
v_CurrentPage NUMERIC DEFAULT NULL,
v_PageSize NUMERIC DEFAULT NULL,
INOUT v_TotRecs NUMERIC  DEFAULT NULL,
v_columnName VARCHAR(100) DEFAULT NULL,
v_columnSortOrder VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(3000);   
   v_firstRec  INTEGER;                                              
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numEmailHstrID VARCHAR(15),
      Date VARCHAR(100),
      notes  VARCHAR(1000),
      type VARCHAR(100),
      Phone  VARCHAR(200),
      assignedto  VARCHAR(100),
      caseid  VARCHAR(50),
      vcCasenumber  VARCHAR(50),
      caseTimeid  VARCHAR(100),
      caseExpid  VARCHAR(100),
      tinttype  VARCHAR(100)
   );                                                 
   v_strSql := '
select HDR.numEmailHstrID,  
bintCreatedOn as date,vcFromEmail || '' to '' || GetEmaillAdd(HDR.numEmailHstrID,1) || '' , '' || vcsubject as notes,  
CAST(''Email'' AS VARCHAR(16)) as type ,  
CAST('''' AS VARCHAR(50)) as phone,CAST('''' AS VARCHAR(50)) as assignedto ,  
CAST(''0'' AS NUMERIC(18,0)) as caseid,     
CAST(null as VARCHAR(255)) as vcCasenumber,         
CAST(''0'' AS NUMERIC(18,0)) as caseTimeid,        
CAST(''0'' AS NUMERIC(18,0)) as caseExpid ,  
HDR.tintType   
from EmailHistory HDR            
  left join EmailHStrToBCCAndCC DTL            
  on DTL.numEmailHstrID = HDR.numEmailHstrID      ';                       
   v_strSql := coalesce(v_strSql,'') || 'where 
(HDR.tintType = 1 or HDR.tintType = 2) ';
   v_strSql := coalesce(v_strSql,'') || ' and bintCreatedOn between ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''':: BYTEA and ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30) || ''':: BYTEA   
and                                      
 (vcFromEmail = ''' || SUBSTR(CAST(v_vcMessageFrom AS VARCHAR(100)),1,100) || ''' or vcEmail = ''' || SUBSTR(CAST(v_vcMessageFrom AS VARCHAR(100)),1,100) || ''')  
union 
 
select  c.numCommId,  /** \n **--** \n **/case when c.bitTask = 4 then dtCreatedDate else dtEndtime end AS CreatedDate,                                           
CAST(textdetails AS VARCHAR(1000)) as notes,   
                                                                        
case when c.bitTask = 1 then ''Communication'' when c.bitTask = 3 then ''Task'' when c.bitTask = 4 then ''Notes'' when c.bitTask = 5 then ''Follow up Status'' end AS type,                                           
A1.vcFirstName || '' '' ||  
A1.vcLastName || '' '' ||                
case when A1.numPhone <> '''' then A1.numPhone || case when A1.numPhoneExtension <> '''' then '' - '' || A1.numPhoneExtension else '''' end  else '''' end as phone,  
                                            
  A2.vcFirstName ||                                                     
A2.vcLastName  as assignedto,   
c.CaseId,     
(select  vcCaseNumber from Cases where Cases.numcaseid = c.CaseId LIMIT 1) as vcCasenumber,         
coalesce(caseTimeid,0) as caseTimeid,        
coalesce(caseExpid,0) as caseExpid  ,  
CAST(1 AS SMALLINT) as tinttype                                              
from  Communication c                      
JOIN AdditionalContactsInformation  A1                   
ON c.numContactId = A1.numContactId                                                     
left join AdditionalContactsInformation A2 on A2.numContactId = c.numAssign                                                    
left join ListDetails on numActivity = numListID   
 where  c.numContactId = ' || SUBSTR(CAST(v_numContactId    AS VARCHAR(15)),1,15) || '
and dtStartTime  >= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''' and dtStartTime  <= ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30)  || ''' order by 2 desc      ';
   RAISE NOTICE '%',(v_strSql);
   EXECUTE 'insert into tt_TEMPTABLE           
 ' || v_strSql;   

   v_firstRec :=(v_CurrentPage -1)*v_PageSize;                                              
   v_lastRec :=(v_CurrentPage*v_PageSize+1);   

   open SWV_RefCur for
   select * from tt_TEMPTABLE T
   where ID > v_firstRec and ID < v_lastRec order by  numEmailHstrID  Desc;                                                          
                                            
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;
   RETURN;
END; $$;


