-- Stored procedure definition script USP_GetOrderProAuditTrailDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOrderProAuditTrailDetails(v_numProId NUMERIC(9,0) DEFAULT 0,   
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numItemCode NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(DC.vcDiscountCode as VARCHAR(255)), cast(DC.numDiscountId as VARCHAR(255)), vcpOppName, OM.numOppId, OM.numDivisionId,
   (SELECT
      CASE WHEN COUNT(OBDI.numOppBizDocID) > 0 THEN 'Yes'
      ELSE 'No'
      END
      FROM OpportunityMaster OM
      INNER JOIN OpportunityItems OI
      ON OM.numOppId = OI.numOppId
      INNER JOIN OpportunityBizDocItems OBDI
      ON OI.numItemCode = OBDI.numItemCode
      INNER JOIN OpportunityBizDocs OBD
      ON OBDI.numOppBizDocID = OBD.numBizDocId
      WHERE OBDI.numItemCode = v_numItemCode) As HasBizDoc
   FROM OpportunityMaster OM
   INNER JOIN DiscountCodes DC
   ON OM.numDiscountID = DC.numDiscountId
   WHERE OM.numDomainId = v_numDomainID
   AND DC.numPromotionID = v_numProId;
END; $$;      
  












