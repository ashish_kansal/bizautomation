-- Stored procedure definition script USP_OppShippingorReceiving for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppShippingorReceiving(v_OppID NUMERIC(9,0),
v_numUserCntID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_status  SMALLINT;            
   v_OppType  SMALLINT;   
   v_bitStockTransfer  BOOLEAN;
   v_fltExchangeRate  DOUBLE PRECISION; 
   v_numDomain  NUMERIC(18,0);
   v_numoppitemtCode  NUMERIC;
   v_numUnits  DOUBLE PRECISION;
   v_numWarehouseItemID  NUMERIC;       
   v_numToWarehouseItemID  NUMERIC(9,0); --to be used with stock transfer
   v_itemcode  NUMERIC;        
   v_QtyShipped  DOUBLE PRECISION;
   v_QtyReceived  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5);
   v_Kit  BOOLEAN;
   v_bitWorkOrder  BOOLEAN;
   v_bitSerialized  BOOLEAN;
   v_bitLotNo  BOOLEAN;
    
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
      select   OM.numDomainId, tintoppstatus, tintopptype, coalesce(bitStockTransfer,false), (CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END) INTO v_numDomain,v_status,v_OppType,v_bitStockTransfer,v_fltExchangeRate FROM OpportunityMaster AS OM WHERE OM.numOppId = v_OppID;
	
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
      IF(SELECT coalesce(tintshipped,0) FROM OpportunityMaster WHERE OpportunityMaster.numOppId = v_OppID AND OpportunityMaster.numDomainId = v_numDomain) = 1 then
	
         RAISE EXCEPTION 'ORDER_CLOSED';
      end if;
      IF v_OppType = 2 AND v_status = 1 AND coalesce(v_bitStockTransfer,false) = false AND(SELECT
      COUNT(*)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      OM.numDomainId = v_numDomain
      AND OM.numOppId = v_OppID
      AND WI.numWLocationID = -1
      AND OI.numUnitHour <> OI.numUnitHourReceived) > 0 then
	
         RAISE EXCEPTION 'GLOCAL_LOCATION_PENDING_FULFILLMENT';
      end if;
      -- BEGIN TRANSACTION
update OpportunityMaster set tintshipped = 1,bintAccountClosingDate = TIMEZONE('UTC',now()),bintClosedDate = TIMEZONE('UTC',now()) where  numOppId = v_OppID;
      UPDATE OpportunityBizDocs SET dtShippedDate = TIMEZONE('UTC',now()) WHERE numoppid = v_OppID AND numBizDocId <> 296;
      UPDATE OpportunityBizDocs SET dtShippedDate = TIMEZONE('UTC',now()) WHERE numoppid = v_OppID AND numBizDocId = 296 AND dtShippedDate IS NULL;
			
						
			
       
			
			-- Archive item based on Item's individual setting
      IF v_OppType = 1 then
			
         UPDATE Item I SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode)
         THEN true
         ELSE false
         END
         FROM OpportunityItems OI
         WHERE I.numItemCode = OI.numItemCode AND(numOppId = v_OppID
         AND I.numDomainID = v_numDomain
         AND coalesce(I.bitArchiveItem,false) = true);
      end if;
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
      select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), numToWarehouseItemID, coalesce(monPrice,0)*v_fltExchangeRate, (case when bitKitParent = true and bitAssembly = true then 0 when bitKitParent = true then 1 else 0 end), coalesce(bitWorkOrder,false), coalesce(bitSerialized,false), coalesce(bitLotNo,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWarehouseItemID,
      v_numToWarehouseItemID,v_monPrice,v_Kit,v_bitWorkOrder,
      v_bitSerialized,v_bitLotNo from OpportunityItems OI join Item I
      on OI.numItemCode = I.numItemCode and numOppId = v_OppID and (bitDropShip = false or bitDropShip is null) where (charitemtype = 'P' OR 1 =(CASE WHEN v_OppType = 1 THEN
         CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
         WHEN coalesce(I.bitKitParent,false) = true THEN 1
         ELSE 0 END
      ELSE 0 END))
      AND I.numDomainID = v_numDomain   order by OI.numoppitemtCode LIMIT 1;
      while v_numoppitemtCode > 0 LOOP   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
         IF v_bitStockTransfer = true then
				
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
            PERFORM usp_ManageInventory(v_itemcode,v_numWarehouseItemID,0,1::SMALLINT,v_numUnits,v_QtyShipped,
            v_QtyReceived,v_monPrice,2::SMALLINT,v_OppID,v_numoppitemtCode,v_numUserCntID,
            v_bitWorkOrder);
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
            PERFORM usp_ManageInventory(v_itemcode,v_numToWarehouseItemID,0,2::SMALLINT,v_numUnits,v_QtyShipped,
            v_QtyReceived,v_monPrice,2::SMALLINT,v_OppID,v_numoppitemtCode,v_numUserCntID,
            v_bitWorkOrder);
            If v_bitSerialized = true OR v_bitLotNo = true then
					
						--Transfer Serial/Lot Numbers
               PERFORM USP_WareHouseItmsDTL_TransferSerialLotNo(v_OppID,v_numoppitemtCode,v_numWarehouseItemID,v_numToWarehouseItemID,
               v_bitSerialized,v_bitLotNo);
            end if;
         ELSE
            PERFORM usp_ManageInventory(v_itemcode,v_numWarehouseItemID,0,v_OppType::SMALLINT,v_numUnits,v_QtyShipped,
            v_QtyReceived,v_monPrice,2::SMALLINT,v_OppID,v_numoppitemtCode,
            v_numUserCntID,v_bitWorkOrder);
         end if;
         select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), numToWarehouseItemID, coalesce(monPrice,0)*v_fltExchangeRate, (case when bitKitParent = true and bitAssembly = true then 0 when bitKitParent = true then 1 else 0 end), coalesce(bitWorkOrder,false), coalesce(bitSerialized,false), coalesce(bitLotNo,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWarehouseItemID,
         v_numToWarehouseItemID,v_monPrice,v_Kit,v_bitWorkOrder,
         v_bitSerialized,v_bitLotNo from OpportunityItems OI
         join Item I
         on OI.numItemCode = I.numItemCode and numOppId = v_OppID where (charitemtype = 'P' OR 1 =(CASE WHEN v_OppType = 1 THEN
            CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
            WHEN coalesce(I.bitKitParent,false) = true THEN 1
            ELSE 0 END
         ELSE 0 END)) and OI.numoppitemtCode > v_numoppitemtCode and (bitDropShip = false or bitDropShip is null)
         AND I.numDomainID = v_numDomain   ORDER by OI.numoppitemtCode LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numoppitemtCode := 0;
         end if;
      END LOOP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



