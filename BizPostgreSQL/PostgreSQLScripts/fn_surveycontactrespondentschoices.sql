-- Function definition script fn_SurveyContactRespondentsChoices for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


CREATE OR REPLACE FUNCTION fn_SurveyContactRespondentsChoices(v_numSurId NUMERIC,
      v_numRespondantID NUMERIC,
      v_numQuestionID NUMERIC)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listStr  TEXT;
BEGIN
   select STRING_AGG(vcAnsLabel,',<br />') INTO v_listStr FROM(SELECT DISTINCT
      CASE WHEN sqm.tIntAnsType = 2 THEN sr.vcAnsText
      ELSE CASE WHEN sr.numMatrixID = 0
         THEN sam.vcAnsLabel
         ELSE sam.vcAnsLabel || '('
            || fn_SurveyMatrixAns(v_numSurId,v_numQuestionID,v_numRespondantID,sam.numAnsID)
            || ')'
         END
      END AS vcAnsLabel
      FROM      SurveyQuestionMaster sqm,
                        SurveyAnsMaster sam,
                        SurveyResponse sr
      WHERE     sqm.numSurID = sam.numSurID
      AND sr.numParentSurID = sam.numSurID
      AND sr.numSurID = v_numSurId
      AND sr.numQuestionID = sqm.numQID
      AND sr.numQuestionID = sam.numQID
      AND sr.numAnsID = sam.numAnsID
      AND sr.numRespondantID = v_numRespondantID
      AND sam.numQID = v_numQuestionID) temptable;
      
   RETURN v_listStr;
END; $$;

