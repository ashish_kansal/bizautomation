CREATE OR REPLACE FUNCTION usp_GetCustomMergeFieldsForEmailTemplate(v_numModuleID NUMERIC,
	  v_numDomainID NUMERIC,
	  v_vcRecordIDs VARCHAR(8000) DEFAULT '',
	  v_numOppId NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionId  VARCHAR(250);
BEGIN
   IF v_numModuleID = 45 OR v_numModuleID = 1 then
      v_numDivisionId := '';
      select   coalesce(v_numDivisionId,'') || SUBSTR(cast(DivisionMaster.numDivisionID AS VARCHAR(10)),1,10) || ', ' INTO v_numDivisionId FROM
      DivisionMaster
      INNER JOIN
      AdditionalContactsInformation
      ON
      DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId WHERE
      AdditionalContactsInformation.numContactId  IN(SELECT Id FROM SplitIDs(v_vcRecordIDs,','));
      open SWV_RefCur for
      SELECT
      CFW_FLD_Values.RecId
			,CFW_Fld_Master.Fld_id,CFW_Fld_Master.FLd_label
			,'#' || REPLACE(CFW_Fld_Master.FLd_label,' ','') || '#'  AS CustomField_Name
			,(CASE
      WHEN Grp_id = 1 THEN fn_GetCustFldStringValue(CFW_FLD_Values.Fld_ID,CFW_FLD_Values.RecId,CFW_FLD_Values.Fld_Value)
      WHEN Grp_id = 4 THEN fn_GetCustFldStringValue(CFW_FLD_Values_Cont.fld_id,CFW_FLD_Values_Cont.RecId,CFW_FLD_Values_Cont.Fld_Value)
      ELSE ''
      END)
      AS CustomField_Value
      FROM
      CFW_Fld_Master
      LEFT JOIN
      CFW_FLD_Values
      ON
      CFW_FLD_Values.Fld_ID = CFW_Fld_Master.Fld_id
      AND
      CFW_FLD_Values.RecId IN(SELECT Id FROM SplitIDs(v_numDivisionId,','))
      LEFT JOIN
      CFW_FLD_Values_Cont
      ON
      CFW_FLD_Values_Cont.fld_id = CFW_Fld_Master.Fld_id
      AND
      CFW_FLD_Values_Cont.RecId IN(SELECT Id FROM SplitIDs(v_vcRecordIDs,','))
      WHERE
			(grp_id = 1 or grp_id = 4) AND CFW_Fld_Master.numDomainID = v_numDomainID;
   ELSEIF (v_numModuleID = 2 or v_numModuleID = 8)
   then
	
      open SWV_RefCur for
      SELECT  CFW_Fld_Values_Opp.RecId,
				CFW_Fld_Master.Fld_id,CFW_Fld_Master.FLd_label,
				'#' || REPLACE(CFW_Fld_Master.FLd_label,' ','') || '#'  AS CustomField_Name,
				(CASE
      WHEN (Grp_id = 2 or Grp_id = 6) THEN fn_GetCustFldStringValue(CFW_Fld_Values_Opp.Fld_ID,CFW_Fld_Values_Opp.RecId,CFW_Fld_Values_Opp.Fld_Value)
						
				--WHEN Grp_id = 4 THEN dbo.fn_GetCustFldStringValue(CFW_FLD_Values_Cont.Fld_ID, CFW_FLD_Values_Cont.RecId, CFW_FLD_Values_Cont.Fld_Value)
      ELSE ''
      END)
      AS CustomField_Value
      FROM CFW_Fld_Master
      LEFT JOIN CFW_Fld_Values_Opp
      ON CFW_Fld_Values_Opp.Fld_ID = CFW_Fld_Master.Fld_id
      AND CFW_Fld_Values_Opp.RecId = v_numOppId
      WHERE
					(grp_id = 2 or grp_id = 6) AND CFW_Fld_Master.numDomainID = 72;
   end if;
   RETURN;
END; $$;



