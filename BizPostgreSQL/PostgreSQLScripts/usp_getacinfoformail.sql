-- Stored procedure definition script usp_GetACInfoForMail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetACInfoForMail(v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
		
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   vcEmail
   FROM
   AdditionalContactsInformation
   WHERE
   numCreatedBy = v_numCreatedBy
   and numDomainID = v_numDomainID;
END; $$;












