-- Stored procedure definition script USP_ManageTerritory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTerritory(v_numTerID NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,       
v_vcTerName VARCHAR(100) DEFAULT '',      
v_numUserID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numTerID = 0 then

      insert into TerritoryMaster(vcTerName,numDomainid,numCreatedBy,numModifiedBy,bintCreatedDate,bintModifiedDate)
values(v_vcTerName,v_numDomainID,v_numUserID,v_numUserID,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()));
   else
      update TerritoryMaster set vcTerName = v_vcTerName,numDomainid = v_numDomainID,numModifiedBy = v_numUserID,
      bintModifiedDate = TIMEZONE('UTC',now())
      where numTerID = v_numTerID;
   end if;
   RETURN;
END; $$;


