-- Stored procedure definition script USP_ManageShortCutUsrConf for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShortCutUsrConf(v_numGroupID NUMERIC(9,0) DEFAULT 0,            
v_numDomainId NUMERIC(9,0) DEFAULT 0,            
v_numContactId NUMERIC(9,0) DEFAULT 0,  
v_strFav TEXT DEFAULT '',        
v_strRec TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql            
        
  
------------------------------------------------------------------------------------------  
--For Links  
------------------------------------------------------------------------------------------            
   AS $$
   DECLARE
   v_hDoc  INTEGER;            
               
   v_hDoc1  INTEGER;
BEGIN
   IF(select count(*) from ShortCutUsrconf where bittype = true and numDomainId = v_numDomainId and numGroupID = v_numGroupID and numcontactId = v_numContactId) > 0 then
 
      delete FROM ShortCutUsrconf where bittype = true and numDomainId = v_numDomainId and numGroupID = v_numGroupID  and numcontactId = v_numContactId;
   end if;  
      
   insert into ShortCutUsrconf(numGroupID,numDomainId,numcontactId,bittype,numOrder,numLinkId)
   select v_numGroupID,v_numDomainId,v_numContactId,1,
     X.numOrder,X.numLinkId from
	 XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFav AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numOrder NUMERIC PATH 'numOrder',
			numLinkId NUMERIC PATH 'numLinkId'
	) X;        
                
      
------------------------------------------------------------------------------------------  
--For New Items  
------------------------------------------------------------------------------------------            
            
   IF(select count(*) from ShortCutUsrconf where bittype = false and numDomainId = v_numDomainId and numGroupID = v_numGroupID and numcontactId = v_numContactId) > 0 then
 
      delete FROM ShortCutUsrconf where bittype = false and numDomainId = v_numDomainId and numGroupID = v_numGroupID  and numcontactId = v_numContactId;
   end if;  
  
          
   insert into ShortCutUsrconf(numGroupID,numDomainId,numcontactId,bittype,numOrder,numLinkId)
   select v_numGroupID,v_numDomainId,v_numContactId,0,
     X.numOrder,X.numLinkId
	 from
	  XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strRec AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numOrder NUMERIC PATH 'numOrder',
			numLinkId NUMERIC PATH 'numLinkId'
	) X;            
 
   RETURN;
END; $$;


