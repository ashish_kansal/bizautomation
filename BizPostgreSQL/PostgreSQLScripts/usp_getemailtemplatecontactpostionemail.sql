-- Stored procedure definition script usp_GetEmailTemplateContactPostionEmail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetEmailTemplateContactPostionEmail(v_GenDocId NUMERIC(9,0) DEFAULT null,                
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_numDivID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcContactPosition  VARCHAR(200);
BEGIN
   select   coalesce(vcContactPosition,'') INTO v_vcContactPosition FROM GenericDocuments where numGenericDocID = v_GenDocId and numDomainId = v_numDomainID;
	
   open SWV_RefCur for SELECT A.vcEmail,A.numContactId,A.vcFirstName,A.vcLastname FROM DivisionMaster AS DM JOIN AdditionalContactsInformation A ON A.numDivisionId = DM.numDivisionID
   WHERE DM.numDomainID = v_numDomainID AND DM.numDivisionID = v_numDivID AND A.vcPosition IN(SELECT Id FROM SplitIds(v_vcContactPosition,','))
   AND coalesce(A.vcEmail,'') != '' AND coalesce(A.vcFirstName,'') != '' AND coalesce(A.vcLastname,'') != '';
END; $$;













