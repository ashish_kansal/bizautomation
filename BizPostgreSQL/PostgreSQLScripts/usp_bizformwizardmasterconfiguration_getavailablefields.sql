DROP FUNCTION IF EXISTS USP_BizFormWizardMasterConfiguration_GetAvailableFields;

CREATE OR REPLACE FUNCTION USP_BizFormWizardMasterConfiguration_GetAvailableFields(v_numDomainID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_numGroupID NUMERIC(18,0),
	v_numRelCntType NUMERIC(18,0),
	v_tintPageType SMALLINT,
	v_pageID NUMERIC(18,0),
	v_bitGridConfiguration BOOLEAN DEFAULT false,
	v_numFormFieldGroupId NUMERIC(18,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF v_bitGridConfiguration = true then -- Retrive Available Fields For Grids Columns Configuration
	
      IF v_pageID = 1 OR v_pageID = 4 then
		
         open SWV_RefCur for
         SELECT
         vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CAST(numFieldID AS VARCHAR(15)) || 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				false AS boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
				0 AS boolAOIField,
				numFormId,
				vcLookBackTableName,
				CAST(numFieldID AS VARCHAR(15)) || '~' || CAST(coalesce(bitCustom,0) AS VARCHAR(15)) || '~' || CAST(coalesce(tintOrder,0) AS VARCHAR(15)) AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM    View_DynamicDefaultColumns
         WHERE   numFormId = v_numFormID
         AND coalesce(bitSettingField,false) = true
         AND coalesce(bitDeleted,false) = false
         AND numDomainID = v_numDomainID
         AND numFieldID NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            bitCustom = false AND
            bitGridConfiguration = true)
         UNION
         SELECT
         fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CAST(fld_id AS VARCHAR(15)) || 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				coalesce(CFW_Fld_Master.numlistid,0) AS numListID,
				Fld_label AS vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numlistid > 0 THEN 'LI' ELSE '' END AS vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				false as boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
				0 AS boolAOIField,
				v_numFormID AS numFormID,
				'' AS vcLookBackTableName,
				CAST(Fld_id AS VARCHAR(15)) || '~1' || '~0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM    CFW_Fld_Master
         LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
         AND numRelation =(CASE
         WHEN v_numRelCntType IN(1,2,3)
         THEN numRelation
         ELSE v_numRelCntType
         END)
         LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id::VARCHAR
         WHERE   CFW_Fld_Master.Grp_id = v_pageID
         AND CFW_Fld_Master.numDomainID = v_numDomainID
         AND Fld_type <> 'Link'
         AND fld_id NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            bitCustom = true AND
            bitGridConfiguration = true)
         ORDER BY
         1;
      ELSE
         open SWV_RefCur for
         SELECT
         vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CAST(numFieldID AS VARCHAR(15)) || 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				0 AS boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
				0 AS boolAOIField,
				numFormId,
				vcLookBackTableName,
				CAST(numFieldID AS VARCHAR(15)) || '~' || CAST(coalesce(bitCustom,0) AS VARCHAR(15)) || '~' || CAST(coalesce(tintOrder,0) AS VARCHAR(15)) AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM    View_DynamicDefaultColumns
         WHERE   numFormId = v_numFormID
         AND coalesce(bitSettingField,false) = true
         AND coalesce(bitDeleted,false) = false
         AND numDomainID = v_numDomainID
         AND numFieldID NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            bitCustom = false AND
            bitGridConfiguration = true)
         UNION
         SELECT
         fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CAST(fld_id AS VARCHAR(15)) || 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				coalesce(CFW_Fld_Master.numlistid,0) AS numListID,
				Fld_label AS vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numlistid > 0 THEN 'LI' ELSE '' END AS vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				0 as boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
				0 AS boolAOIField,
				v_numFormID AS numFormID,
				'' AS vcLookBackTableName,
				CAST(Fld_id AS VARCHAR(15)) || '~1' || '~0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM    CFW_Fld_Master
         LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id::VARCHAR
         WHERE   CFW_Fld_Master.Grp_id = v_pageID
         AND CFW_Fld_Master.numDomainID = v_numDomainID
         AND Fld_type <> 'Link'
         AND fld_id NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            bitCustom = true AND
            bitGridConfiguration = true)
         ORDER BY
         1;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT
      vcFieldName AS vcNewFormFieldName,
			vcFieldName AS vcFormFieldName,
			CAST(numFieldID AS VARCHAR(15)) || 'R' AS numFormFieldId,
			'R' as vcFieldType,
			vcAssociatedControlType,
			numListID,
			vcDbColumnName,
			vcListItemType,
			0 AS intColumnNum,
			0 AS intRowNum,
			0 AS boolRequired,
			0 AS numAuthGroupID,
			CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
			0 AS boolAOIField,
			numFormId,
			vcLookBackTableName,
			CAST(numFieldID AS VARCHAR(15)) || '~' || CAST(coalesce(bitCustom,0) AS VARCHAR(15)) || '~' || CAST(coalesce(tintOrder,0) AS VARCHAR(15)) AS vcFieldAndType,
			false AS bitDefaultMandatory,
			coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_numFormID AND
      numDomainID = v_numDomainID  AND
      1 =(CASE
      WHEN v_tintPageType = 2 THEN
         CASE
         WHEN coalesce(bitAddField,false) = true THEN 1
         ELSE 0
         END
      WHEN v_tintPageType = 3 THEN
         CASE
         WHEN coalesce(bitDetailField,false) = true THEN 1
         ELSE 0
         END
      ELSE
         0
      END) AND
      numFieldID NOT IN(SELECT
         numFieldID
         FROM
         BizFormWizardMasterConfiguration
         WHERE
         numFormID = v_numFormID AND
         numDomainID = v_numDomainID AND
         numGroupID = v_numGroupID AND
         numRelCntType = v_numRelCntType AND
         tintPageType = v_tintPageType AND
         bitCustom = false AND
         bitGridConfiguration = false)
      ORDER BY
      vcNewFormFieldName;
      IF v_pageID = 4 OR v_pageID = 1  OR v_pageID = 12 OR v_pageID = 13  OR v_pageID = 14 then
		
         open SWV_RefCur2 for
         SELECT
         fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CAST(fld_id AS VARCHAR(15)) || 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				coalesce(CFM.numlistid,0) AS numListID,
				Fld_label AS vcDbColumnName,
				CASE WHEN CFM.numlistid > 0 THEN 'LI' ELSE '' END AS vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				0 as boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN CFM.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
				0 AS boolAOIField,
				v_numFormID AS numFormID,
				'' AS vcLookBackTableName,
				CAST(Fld_id AS VARCHAR(15)) || '~1' || '~0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM
         CFW_Fld_Master CFM
         LEFT JOIN
         CFw_Grp_Master
         ON
         subgrp = CAST(CFM.Grp_id AS VARCHAR)
         WHERE
         CFM.numDomainID = v_numDomainID
         AND CFM.Grp_id = v_pageID
         AND subgrp = '0'
         AND fld_id NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            tintPageType = v_tintPageType AND
            bitCustom = true AND
            bitGridConfiguration = false)
         ORDER BY
         vcNewFormFieldName,subgrp;
      end if;
      IF v_pageID = 2 or  v_pageID = 3 or v_pageID = 5 or v_pageID = 6 or v_pageID = 7 or v_pageID = 8   or v_pageID = 11 then
		
         open SWV_RefCur2 for
         SELECT
         fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CAST(fld_id AS VARCHAR(15)) || 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				coalesce(CFM.numlistid,0) AS numListID,
				Fld_label AS vcDbColumnName,
				CASE WHEN CFM.numlistid > 0 THEN 'LI' ELSE '' END AS vcListItemType,
				0 AS intColumnNum,
				0 AS intRowNum,
				0 as boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN CFM.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
				0 AS boolAOIField,
				v_numFormID AS numFormID,
				'' AS vcLookBackTableName,
				CAST(Fld_id AS VARCHAR(15)) || '~1' || '~0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(v_numFormFieldGroupId,0) AS numFormGroupId
         FROM
         CFW_Fld_Master CFM
         LEFT JOIN
         CFw_Grp_Master
         ON
         subgrp = CAST(CFM.Grp_id AS VARCHAR)
         WHERE
         CFM.numDomainID = v_numDomainID
         AND CFM.Grp_id = v_pageID
         AND subgrp = '0'
         AND fld_id NOT IN(SELECT
            numFieldID
            FROM
            BizFormWizardMasterConfiguration
            WHERE
            numFormID = v_numFormID AND
            numDomainID = v_numDomainID AND
            numGroupID = v_numGroupID AND
            numRelCntType = v_numRelCntType AND
            tintPageType = v_tintPageType AND
            bitCustom = true AND
            bitGridConfiguration = false)
         ORDER BY
         vcNewFormFieldName,subgrp;
      end if;
   end if;
   RETURN;
END; $$;


