-- Stored procedure definition script USP_GetEmployer for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmployer(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   DM.numDivisionID,
		cast(CMP.vcCompanyName as VARCHAR(255))
   FROM
   CompanyInfo CMP
   INNER JOIN
   DivisionMaster DM
   ON
   CMP.numCompanyId = DM.numCompanyID
   INNER JOIN
   Domain D
   ON
   DM.numDivisionID = D.numDivisionId
   WHERE
   D.numDomainId = v_numDomainID;
END; $$;












