-- Stored procedure definition script USP_UpdateOppItemLabel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOppItemLabel(v_numOppID NUMERIC,
	v_numoppitemtCode NUMERIC(9,0),
    v_vcItemName VARCHAR(300),
    v_vcModelID VARCHAR(200),
    v_vcItemDesc VARCHAR(2000),
	v_vcManufacturer VARCHAR(250),
	v_numProjectID NUMERIC(9,0),
	v_numClassID NUMERIC(9,0),
	v_vcNotes VARCHAR(1000),
	v_numCost DECIMAL(30,16))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  OpportunityItems
   SET     vcItemName = v_vcItemName,vcModelID = v_vcModelID,vcItemDesc = v_vcItemDesc,
   vcManufacturer = v_vcManufacturer,numProjectID = v_numProjectID,
   numClassID = v_numClassID,vcNotes = v_vcNotes,numCost = v_numCost,numProjectStageID =(Case when numProjectID <> v_numProjectID then 0 else numProjectStageID end)
   WHERE   numOppId = v_numOppID and numoppitemtCode = v_numoppitemtCode;
   RETURN;
END; $$;

