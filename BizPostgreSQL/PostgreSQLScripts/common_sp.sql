-- Stored procedure definition script common_sp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
create or replace PROCEDURE common_sp(v_numAcntTypeId NUMERIC(9,0) DEFAULT 0,
    v_numParntAcntTypeID NUMERIC(9,0) DEFAULT 0,
    v_vcAccountName VARCHAR(100) DEFAULT NULL,
    v_vcAccountDescription VARCHAR(100) DEFAULT NULL,
    v_monOriginalOpeningBal NUMERIC(19,4) DEFAULT NULL,
    v_monOpeningBal NUMERIC(19,4) DEFAULT NULL,
    v_dtOpeningDate TIMESTAMP DEFAULT NULL,
    v_bitActive BOOLEAN DEFAULT NULL,
    INOUT v_numAccountId NUMERIC(9,0) DEFAULT 0 ,
    v_bitFixed BOOLEAN DEFAULT NULL,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numListItemID NUMERIC(9,0) DEFAULT 0,
    v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_bitProfitLoss BOOLEAN DEFAULT NULL,
    v_bitDepreciation BOOLEAN DEFAULT NULL,
    v_dtDepreciationCostDate TIMESTAMP DEFAULT NULL,
    v_monDepreciationCost NUMERIC(19,4) DEFAULT NULL,
    v_vcNumber VARCHAR(50) DEFAULT NULL,
    v_bitIsSubAccount BOOLEAN DEFAULT NULL,
    v_numParentAccId NUMERIC(18,0) DEFAULT NULL,
    v_IsBankAccount			BOOLEAN	DEFAULT false,
	v_vcStartingCheckNumber	VARCHAR(50) DEFAULT NULL ,
	v_vcBankRountingNumber	VARCHAR(50) DEFAULT NULL ,
	v_bitIsConnected				BOOLEAN DEFAULT false,
	v_numBankDetailID			NUMERIC(18,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   SWV_RCur REFCURSOR;
   v_usp_InsertNewChartAccountDetails_numAccountId NUMERIC(9,0);
BEGIN
   IF  NOT EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcAccountName = v_vcAccountName AND numDomainId = 72) then
      v_usp_InsertNewChartAccountDetails_numAccountId := 0;
      SELECT * INTO SWV_RCur FROM usp_InsertNewChartAccountDetails(0,0,v_vcAccountName,v_vcAccountDescription,v_monOriginalOpeningBal,v_monOpeningBal,
      v_dtOpeningDate,v_bitActive,v_usp_InsertNewChartAccountDetails_numAccountId,
      v_bitFixed,72,0,0,v_bitProfitLoss,v_bitDepreciation,
      v_dtDepreciationCostDate,v_monDepreciationCost,v_vcNumber,v_bitIsSubAccount,
      v_numParentAccId,false,NULL,NULL,false,NULL,SWV_RefCur := 'SWV_RCur');
   else
      RAISE NOTICE 'Your Account Already Exist';
      RETURN;
   end if;
END; $$;


