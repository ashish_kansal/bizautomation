DROP FUNCTION IF EXISTS GetAlertDetail;

CREATE OR REPLACE FUNCTION GetAlertDetail(v_numEmailHstrID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_numECampaignID NUMERIC(18,0),
	v_numContactID NUMERIC(18,0),
	v_bitOpenActionItem BOOLEAN,
	v_bitOpencases BOOLEAN,
	v_bitOpenProject BOOLEAN,
	v_bitOpenSalesOpp BOOLEAN,
	v_bitBalancedue BOOLEAN,
	v_bitUnreadEmail BOOLEAN,
	v_bitCampaign BOOLEAN,
	v_TotalBalanceDue NUMERIC(18,0),
	v_OpenSalesOppCount NUMERIC(18,0),
	v_OpenCaseCount NUMERIC(18,0),
	v_OpenProjectCount NUMERIC(18,0),
	v_UnreadEmailCount NUMERIC(18,0),
	v_OpenActionItemCount NUMERIC(18,0),
	v_CampaignDTLCount NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAlert  TEXT DEFAULT '';
	
	--CHECK AR BALANCE
BEGIN
   IF v_bitBalancedue = true AND  v_TotalBalanceDue > 0 then
    
      v_vcAlert := '<a href="#" onclick="OpenAlertDetail(1,' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',0)"><img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if; 

	--CHECK OPEN SALES ORDER COUNT
   IF v_bitOpenSalesOpp = true AND v_OpenSalesOppCount > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<a href="#" onclick="OpenAlertDetail(2,' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',0)"><img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if; 

	--CHECK OPEN CASES COUNT
   IF v_bitOpencases = true AND v_OpenCaseCount > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<a href="#" onclick="OpenAlertDetail(3,' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',0)"><img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if; 

	--CHECK OPEN PROJECT COUNT
   IF v_bitOpenProject = true AND v_OpenProjectCount > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<a href="#" onclick="OpenAlertDetail(4,' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',0)"><img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if; 

	--CHECK UNREAD MAIL COUNT
	--IF @bitUnreadEmail = 1 AND @UnreadEmailCount > 0 
 --   BEGIN 
 --      SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST(@UnreadEmailCount AS VARCHAR(10)) + ')</a>&nbsp;'
 --   END
	
	--CHECK ACTIVE CAMPAIGN COUNT
   IF v_bitCampaign = true AND coalesce(v_numECampaignID,0) > 0 then
	
      IF v_CampaignDTLCount = 0 then
		
         v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
      ELSE
         v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/GreenFlag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
      end if;
   end if; 

	--CHECK COUNT OF ACTION ITEM
   IF v_bitOpenActionItem = true AND v_OpenActionItemCount > 0 then
	
      v_vcAlert := coalesce(v_vcAlert,'') || '<a href="#" onclick="OpenAlertDetail(5,' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',' || SUBSTR(CAST(v_numContactID AS VARCHAR(18)),1,18) || ')"><img alt="" src="../images/MasterList-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if;
   IF(v_numContactID > 0) then
	
      v_vcAlert := coalesce(v_vcAlert,'') || '<a href="#" onclick="OpenWindow(' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ',0,0)"><img alt="" src="../images/MasterList_Organization.png" style="height: 20px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;';
   end if;
   RETURN v_vcAlert;
END; $$;

