-- Stored procedure definition script USP_DisableImap for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DisableImap(v_numUserCntId NUMERIC(9,0),
    v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintRetryLeft  SMALLINT;

		--give 10 retry
BEGIN
   select   coalesce(tintRetryLeft,10) INTO v_tintRetryLeft FROM    ImapUserDetails WHERE   numUserCntID = v_numUserCntId
   AND numDomainID = v_numDomainId;
	
   IF v_tintRetryLeft = 0 then -- then disable imap account
            
      UPDATE  ImapUserDetails
      SET     bitImap = false,tintRetryLeft = 0,bitNotified = false
      WHERE   numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainId;
   ELSE
      IF v_tintRetryLeft > 0 then
         UPDATE  ImapUserDetails
         SET     tintRetryLeft = v_tintRetryLeft -1
         WHERE   numUserCntID = v_numUserCntId
         AND numDomainID = v_numDomainId;
      end if;
   end if;
   RETURN;
END; $$;



