-- Stored procedure definition script USP_ListDetails_Search for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ListDetails_Search(v_numDomainID NUMERIC(18,0)
	,v_numListID NUMERIC(18,0)
	,v_vcSearchText VARCHAR(300)
	,v_intOffset INTEGER
	,v_intPageSize INTEGER
	,INOUT v_intTotalRecords INTEGER , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   numListItemID
		,vcData
   FROM
   Listdetails
   WHERE
   numListID = v_numListID
   AND (numDomainid = v_numDomainID OR coalesce(constFlag,false) = true)
   AND vcData ilike CONCAT('%',v_vcSearchText,'%')
   ORDER BY
   vcData;

   SELECT COUNT(*) INTO v_intTotalRecords FROM Listdetails WHERE numListID = v_numListID AND (numDomainid = v_numDomainID OR coalesce(constFlag,false) = true) AND vcData ilike CONCAT(v_vcSearchText,'%');
   RETURN;
END; $$;


