-- Stored procedure definition script USP_ReportListMaster_Top10ItemByProfitMargin for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ItemByProfitMargin(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM Domain WHERE numDomainId = v_numDomainID;


	open SWV_RefCur for 
	SELECT 
		numItemCode
		,vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',numItemCode,'&frm=All Items') AS URL
		,(SUM(Profit)/SUM(monTotAmount))*100 AS BlendedProfit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,coalesce(monTotAmount,0) AS monTotAmount
			,CAST(coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) as DECIMAL(20,5)) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(monTotAmount,0) <> 0
      AND coalesce(numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP
   GROUP BY
   numItemCode,vcItemName
   HAVING(SUM(Profit)/SUM(monTotAmount))*100 > 0
   ORDER BY(SUM(Profit)/SUM(monTotAmount))*100 DESC LIMIT 10;
END; $$;












