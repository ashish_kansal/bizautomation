-- Function definition script GetBirthDayFromDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetBirthDayFromDate(v_CustomerBirthDate TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ResultVar  INTEGER;
    
   v_currentDateTime  TIMESTAMP;
BEGIN
   v_currentDateTime := LOCALTIMESTAMP;
	

   v_ResultVar := DATE_PART('day',v_currentDateTime -v_CustomerBirthDate);
	/* write  your code here*/
   RETURN v_ResultVar;
END; $$;

