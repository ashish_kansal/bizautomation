-- Stored procedure definition script usp_DeleteActionItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteActionItem(v_numQBCalID NUMERIC(9,0)   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


--DELETE FROM QBActionItem where numQBCalID=@numQBCalID 
   UPDATE QBActionItem set bitStatus = true where numQBCalID = v_numQBCalID;
   RETURN;
END; $$;


