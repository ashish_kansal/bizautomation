-- Stored procedure definition script USP_FixedAssetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_FixedAssetDetails(v_numAssetClass NUMERIC(9,0) DEFAULT 0,
 v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
	OPEN SWV_RefCur FOR 
	SELECT
		numFixedAssetId
		,'' as numAssetClass
		,FA.vcAssetName as vcAssetName
		,FA.vcAssetDescription as vcAssetDescription
		,FA.vcSerialNo as vcSerialNo
		,FA.monCost as Cost
		,FA.monCurrentValue as monCurrentValue
		,FA.monFiscalDepOrAppAmt as monFiscalDepOrAppAmt 
	from 
		FixedAssetDetails FA     
	Left outer join 
		Chart_Of_Accounts as CA 
	on 
		FA.numAssetClass = CA.numAccountId 
	Where 
		FA.numDomainId = v_numDomainId 
		AND (COALESCE(v_numAssetClass,0) = 0 OR FA.numAssetClass = v_numAssetClass);

   RETURN;               
END; $$;


