-- Stored procedure definition script USP_RecurringTemplateList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RecurringTemplateList(v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_bitBizDocBreakup BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numRecurringId as "numRecurringId",varRecurringTemplateName as "varRecurringTemplateName", case when chrIntervalType = 'D' then 'Daily' when chrIntervalType = 'M' then 'Monthly' When chrIntervalType = 'Y' then 'Yearly' end as "IntervalType"
   From RecurringTemplate Where numDomainID = v_numDomainId
   AND (bitBizDocBreakup = v_bitBizDocBreakup OR v_bitBizDocBreakup = false);
END; $$;












