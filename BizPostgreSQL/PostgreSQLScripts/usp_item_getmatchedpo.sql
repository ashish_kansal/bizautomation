-- Stored procedure definition script USP_Item_GetMatchedPO for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetMatchedPO(v_numDomainId NUMERIC(18,0)
	,v_vcItems TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE

   v_hDoc  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      Index INTEGER,
      ItemCode NUMERIC(18,0),
      Item VARCHAR(300),
      Attributes VARCHAR(1000),
      AttributeValues VARCHAR(500),
      UnitHour DOUBLE PRECISION,
      POs TEXT
   );

   INSERT INTO
   tt_TEMP
   SELECT
   Index
		,ItemCode
        ,Item
        ,Attributes
        ,AttributeValues
        ,UnitHour
		,''
   FROM
   XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					Index INTEGER PATH 'Index',
					ItemCode NUMERIC(18,0) PATH 'ItemCode',
					Item VARCHAR(300) PATH 'Item',
					Attributes VARCHAR(1000) PATH 'Attributes',
					AttributeValues VARCHAR(500) PATH 'AttributeValues',
					UnitHour DOUBLE PRECISION PATH 'UnitHour',
					POs TEXT PATH 'POs'
			) AS X;


   UPDATE
   tt_TEMP TP
   SET
   POs = CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"numOppID":',OM.numOppId,', "numOppItemID":',OI.numoppitemtCode,', "POName":"',
   coalesce(OM.vcpOppName,''),'", "Vendor":"',coalesce(CI.vcCompanyName,''),
   '","Price":',coalesce(OI.monPrice,0),'}'),',')
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   WHERE
   OM.numDomainId = v_numDomainId
   AND OM.tintopptype = 2
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND OI.numItemCode = TP.ItemCode
   AND OI.numUnitHour = TP.UnitHour
   AND coalesce(OI.vcAttrValues,'') = coalesce(TP.AttributeValues,'')),''),']');


   open SWV_RefCur for SELECT Index AS "Index",
      ItemCode As "ItemCode",
      Item  As "Item",
      Attributes As "Attributes",
      AttributeValues As "AttributeValues",
      UnitHour As "UnitHour",
      POs As "POs" FROM tt_TEMP;
END; $$;












