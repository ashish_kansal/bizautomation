CREATE OR REPLACE FUNCTION USP_GetMasterListItemsForBizDocs(v_ListID NUMERIC(9,0) DEFAULT 0,                
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numOppId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAuthoritativeId  NUMERIC(9,0);            
   v_tintOppType  SMALLINT;            
   v_tintOppStatus  SMALLINT;      
   v_tintshipped  SMALLINT;
BEGIN
   v_numAuthoritativeId := 0;    
 
   select   tintopptype, tintoppstatus, tintshipped INTO v_tintOppType,v_tintOppStatus,v_tintshipped From OpportunityMaster Where numOppId = v_numOppId And  numDomainId = v_numDomainID;         
             
   If v_tintOppType = 1 then
      select   numAuthoritativeSales INTO v_numAuthoritativeId from AuthoritativeBizDocs Where numDomainId = v_numDomainID;
   ELSEIF v_tintOppType = 2
   then
      select   numAuthoritativePurchase INTO v_numAuthoritativeId from AuthoritativeBizDocs Where numDomainId = v_numDomainID;
   end if;            
 

   if v_tintOppStatus = 1 then

      IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then

	 --SELECT numListItemID,case when @numAuthoritativeId=numListItemID then  vcData +' - Authoritative' Else vcData   End as  vcData 
         open SWV_RefCur for
         SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN
            CASE WHEN v_numAuthoritativeId = LDN.numListItemID
            THEN  vcName || ' - Authoritative'
            ELSE vcName
            END
         ELSE CASE WHEN v_numAuthoritativeId = Ld.numListItemID
            THEN vcData || ' - Authoritative'
            ELSE vcData
            END
         END AS vcData
         FROM Listdetails Ld
         LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = Ld.numListItemID AND LDN.numDomainID = v_numDomainID
         WHERE Ld.numListID = v_ListID
         and (constFlag = true or Ld.numDomainid = v_numDomainID);
      ELSE
  --SELECT Ld.numListItemID,case when @numAuthoritativeId=Ld.numListItemID then  isnull(vcRenamedListName,vcData) +' - Authoritative' Else isnull(vcRenamedListName,vcData)   End as  vcData 
         open SWV_RefCur for
         SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN
            CASE WHEN v_numAuthoritativeId = LDN.numListItemID
            THEN  coalesce(vcRenamedListName,vcName) || ' - Authoritative'
            ELSE coalesce(vcRenamedListName,vcName)
            END
         ELSE CASE WHEN v_numAuthoritativeId = Ld.numListItemID
            THEN coalesce(vcRenamedListName,vcData) || ' - Authoritative'
            ELSE coalesce(vcRenamedListName,vcData)
            END
         END AS vcData ,
						(SELECT COUNT(noOfRecord) FROM(SELECT(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) As noOfRecord FROM
               OpportunityItems OI
               LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
               LEFT JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
               AND OB.numBizDocId = BDF.numBizDoc
               LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
               AND OBI.numOppItemID = OI.numoppitemtCode
               WHERE OM.numOppId = v_numOppId AND numBizDocId = BDF.numBizDoc
               GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
               HAVING(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
         FROM BizDocFilter BDF
         INNER JOIN Listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
         left join listorder LO on Ld.numListItemID = LO.numListItemID AND LO.numDomainId = v_numDomainID
         LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = Ld.numListItemID AND LDN.numDomainID = v_numDomainID
         WHERE Ld.numListID = v_ListID AND BDF.tintBizocType = v_tintOppType AND BDF.numDomainID = v_numDomainID
         AND (constFlag = true or Ld.numDomainid = v_numDomainID) ORDER BY totalPendingInvoice desc,LO.intSortOrder;
      end if;
   Else
      IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then

 --SELECT numListItemID,vcData as  vcData 
         open SWV_RefCur for
         SELECT LD.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN LDN.vcName
         ELSE LD.vcData
         END AS vcData
         FROM Listdetails LD
         LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID  AND LDN.numDomainID = v_numDomainID
         WHERE LD.numListID = v_ListID and (constFlag = true or LD.numDomainid = v_numDomainID)
         And LD.numListItemID <> v_numAuthoritativeId;
      ELSE
  --SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
         open SWV_RefCur for
         SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN coalesce(vcRenamedListName,LDN.vcName)
         ELSE coalesce(vcRenamedListName,vcData)
         END AS vcData,
													(SELECT COUNT(noOfRecord) FROM(SELECT(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) As noOfRecord FROM
               OpportunityItems OI
               LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
               LEFT JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
               AND OB.numBizDocId = BDF.numBizDoc
               LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
               AND OBI.numOppItemID = OI.numoppitemtCode
               WHERE OM.numOppId = v_numOppId AND numBizDocId = BDF.numBizDoc
               GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
               HAVING(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
         FROM BizDocFilter BDF
         INNER JOIN Listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = Ld.numListItemID AND LDN.numDomainID = v_numDomainID
         WHERE Ld.numListID = v_ListID
         AND BDF.tintBizocType = v_tintOppType
         AND BDF.numDomainID = v_numDomainID
         AND (constFlag = true OR Ld.numDomainid = v_numDomainID)
         AND Ld.numListItemID <> v_numAuthoritativeId
         AND Ld.numListItemID NOT IN(296)  ORDER BY totalPendingInvoice desc,LO.intSortOrder;
      end if;
   end if;
   RETURN;
END; $$;


