DROP FUNCTION IF EXISTS USP_GetGeneralLedger_Virtual;

CREATE OR REPLACE FUNCTION USP_GetGeneralLedger_Virtual(v_numDomainID INTEGER,
      v_vcAccountId VARCHAR(4000),
      v_dtFromDate TIMESTAMP,
      v_dtToDate TIMESTAMP,
      v_vcTranType VARCHAR(50) DEFAULT '',
      v_varDescription VARCHAR(50) DEFAULT '',
      v_CompanyName VARCHAR(50) DEFAULT '',
      v_vcBizPayment VARCHAR(50) DEFAULT '',
      v_vcCheqNo VARCHAR(50) DEFAULT '',
      v_vcBizDocID VARCHAR(50) DEFAULT '',
      v_vcTranRef VARCHAR(50) DEFAULT '',
      v_vcTranDesc VARCHAR(50) DEFAULT '',
	  v_numDivisionID NUMERIC(9,0) DEFAULT 0,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Chintan to enable calculation of date according to client machine
      v_charReconFilter CHAR(1) DEFAULT '',
	  v_tintMode SMALLINT DEFAULT 0,
      v_numItemID NUMERIC(9,0) DEFAULT 0,
      v_CurrentPage INTEGER DEFAULT 0,
	  v_PageSize INTEGER DEFAULT 0,
	  v_numAccountClass NUMERIC(18,0) DEFAULT 0,
	  v_tintFilterTransactionType SMALLINT DEFAULT 0,
	  v_vcFiterName VARCHAR(300) DEFAULT '',
	  v_vcFiterDescription VARCHAR(300) DEFAULT '',
	  v_vcFiterSplit VARCHAR(300) DEFAULT '',
	  v_vcFiterAmount VARCHAR(300) DEFAULT '',
	  v_vcFiterNarration VARCHAR(300) DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
   AS $$
   DECLARE
   v_dtFinFromDate  TIMESTAMP;
   v_dtFinYearFromJournal  TIMESTAMP;
   v_dtFinYearFrom  TIMESTAMP;
   v_numMaxFinYearID  NUMERIC;
   v_Month  INTEGER;
   v_year  INTEGER;
   v_startRow  INTEGER;
   v_CurrRecord  INTEGER;
   v_sql TEXT;
BEGIN
   SELECT   dtPeriodFrom INTO v_dtFinFromDate FROM     FinancialYear WHERE    dtPeriodFrom <= v_dtFromDate
   AND dtPeriodTo >= v_dtFromDate
   AND numDomainId = v_numDomainID;

   RAISE NOTICE '%',v_dtFromDate;
   RAISE NOTICE '%',v_dtToDate;

   select   MIN(datEntry_Date) INTO v_dtFinYearFromJournal FROM General_Journal_Header WHERE numDomainId = v_numDomainID; 
   SELECT   dtPeriodFrom INTO v_dtFinYearFrom FROM     FinancialYear WHERE    dtPeriodFrom <= v_dtFromDate
   AND dtPeriodTo >= v_dtFromDate
   AND numDomainId = v_numDomainID;

--SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

   select   MAX(numFinYearId) INTO v_numMaxFinYearID FROM FinancialYear WHERE numDomainId = v_numDomainID;
   select   EXTRACT(MONTH FROM dtPeriodFrom), EXTRACT(YEAR FROM dtPeriodFrom) INTO v_Month,v_year FROM FinancialYear WHERE numDomainId = v_numDomainID AND numFinYearId = v_numMaxFinYearID;
   RAISE NOTICE '%',v_Month;
   RAISE NOTICE '%',v_year;
   RAISE NOTICE 'Mode: %',v_tintMode;
		 
   if v_tintMode = 0 then

      DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP1 AS
         SELECT  COA1.numAccountId  /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
         FROM    Chart_Of_Accounts COA1
         WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcAccountId,','))
         AND COA1.numDomainId = v_numDomainID;
      v_vcAccountId := coalesce((SELECT string_agg(numAccountID::VARCHAR, ',') FROM tt_TEMP1),''); 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
      open SWV_RefCur for
      SELECT
      v_numDomainID AS "numDomainID"
			,numAccountId AS "numAccountId"
			,vcAccountName AS "vcAccountName"
			,numParntAcntTypeID AS "numParntAcntTypeID"
			,vcAccountDescription AS "vcAccountDescription"
			,vcAccountCode AS "vcAccountCode"
			,dtAsOnDate AS "dtAsOnDate"
			,CASE
      WHEN (Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%') AND(v_year::bigint+v_Month::bigint) =(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN 0
      ELSE coalesce(mnOpeningBalance,0)
      END  AS "mnOpeningBalance"
      FROM(SELECT
         COA.numAccountId
				,vcAccountName
				,numParntAcntTypeID
				,vcAccountDescription
				,vcAccountCode
				,v_dtFromDate AS dtAsOnDate
				,CASE
         WHEN(v_year::bigint+v_Month::bigint) >(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate))
         THEN(SELECT
               coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
               FROM
               General_Journal_Header GJH
               INNER JOIN
               General_Journal_Details GJD
               ON
               GJH.numJOurnal_Id = GJD.numJournalId
               WHERE
               GJH.numDomainId = v_numDomainID
               AND GJH.datEntry_Date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%'
               THEN v_dtFinYearFrom
               ELSE v_dtFinYearFromJournal
               END) AND  v_dtFromDate+INTERVAL '-1 minute'
               AND GJD.numChartAcntId = COA.numAccountId
               AND (GJD.numClassID = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0))
         WHEN(v_year::bigint+v_Month::bigint) <(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate))
         THEN(SELECT
               coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
               FROM
               General_Journal_Header GJH
               INNER JOIN
               General_Journal_Details GJD
               ON
               GJH.numJOurnal_Id = GJD.numJournalId
               WHERE
               GJH.numDomainId = v_numDomainID
               AND GJH.datEntry_Date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%'
               THEN v_dtFinYearFrom
               ELSE v_dtFinYearFromJournal
               END) AND  v_dtFromDate+INTERVAL '1 minute'
               AND GJD.numChartAcntId = COA.numAccountId
               AND (GJD.numClassID = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0))
         ELSE(SELECT
               coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
               FROM
               General_Journal_Header GJH
               INNER JOIN
               General_Journal_Details GJD
               ON
               GJH.numJOurnal_Id = GJD.numJournalId
               WHERE
               GJH.numDomainId = v_numDomainID
               AND GJH.datEntry_Date <= v_dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
               AND GJD.numChartAcntId = COA.numAccountId
               AND (GJD.numClassID = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0))
         END AS mnOpeningBalance
         FROM
         Chart_Of_Accounts COA
         WHERE
         COA.numDomainId = v_numDomainID
         AND COA.numAccountId in(select CAST(ID AS NUMERIC(9,0)) from SplitIDs(v_vcAccountId,','))) Table1;
		

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

		
		OPEN SWV_RefCur2 FOR SELECT 
			General_Journal_Details.numDomainId AS "numDomainId",
			Chart_Of_Accounts.numAccountId AS "numAccountId" ,
			General_Journal_Header.numJOurnal_Id AS "numJOurnal_Id",
			FormatedDateFromDate(General_Journal_Header.datEntry_Date,General_Journal_Details.numDomainId) AS "Date",
			General_Journal_Header.varDescription AS "varDescription",
			CompanyInfo.vcCompanyName AS "CompanyName",
			General_Journal_Details.numTransactionId AS "numTransactionId",
			coalesce(coalesce(coalesce(fn_getpaymentbizdoc(General_Journal_Header.numBizDocsPaymentDetId,v_numDomainID),General_Journal_Header.varDescription)) || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
			ELSE VIEW_JOURNALCHEQ.Narration 
			END),'') as "Narration",
			coalesce(General_Journal_Details.vcReference) AS "TranRef",
			coalesce(General_Journal_Details.varDescription) AS "TranDesc",
			coalesce(General_Journal_Details.numDebitAmt,0)::VARCHAR AS "numDebitAmt",
			coalesce(General_Journal_Details.numCreditAmt,0)::VARCHAR AS "numCreditAmt",
			Chart_Of_Accounts.vcAccountName AS "vcAccountName",
			'' as "balance",
			CASE
				  WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 'Checks'
				  WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
				  WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
				  WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
				  OpportunityMaster.tintopptype = 1 THEN coalesce(OpportunityBizDocs.vcBizDocID,'BizDocs Invoice')
				  WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
				  OpportunityMaster.tintopptype = 2 THEN coalesce(OpportunityBizDocs.vcBizDocID,'BizDocs Purchase')
				  WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
				  WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
				  WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN  CONCAT('Bill-',General_Journal_Header.numBillID)
				  WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 'Pay Bill'
				  WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN
					 CASE
					 WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
					 WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
					 WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
					 WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
					 WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund Against Credit Memo'
					 END
				  WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
			WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 'Journal'
			END AS "TransactionType",
			coalesce(General_Journal_Details.bitCleared,false) AS "bitCleared",
			coalesce(General_Journal_Details.bitReconcile,false) AS "bitReconcile",
			coalesce(General_Journal_Details.numItemID,0) AS "numItemID",
			Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS "numLandedCostOppId",
			COALESCE((SELECT string_agg(CAST(COA.vcAccountName AS VARCHAR),', ')
         FROM General_Journal_Details AS GJD JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
         WHERE GJD.numJournalId = General_Journal_Header.numJOurnal_Id
         AND COA.numDomainId = General_Journal_Header.numDomainId
         AND COA.numAccountId <> Chart_Of_Accounts.numAccountId)) AS "vcSplitAccountName"
      FROM General_Journal_Header INNER JOIN
      General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId INNER JOIN
      Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
      timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID
      LEFT OUTER JOIN DivisionMaster ON General_Journal_Details.numCustomerId = DivisionMaster.numDivisionID and
      General_Journal_Details.numDomainId = DivisionMaster.numDomainID
      LEFT OUTER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      LEFT OUTER JOIN OpportunityMaster ON General_Journal_Header.numOppId = OpportunityMaster.numOppId LEFT OUTER JOIN
      OpportunityBizDocs ON General_Journal_Header.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId 
	  LEFT JOIN LATERAL (SELECT * FROM VIEW_JOURNALCHEQ WHERE 1 = (CASE 
																		WHEN General_Journal_Header.numCheckHeaderID > 0
																		THEN (CASE WHEN VIEW_JOURNALCHEQ.numCheckHeaderID = General_Journal_Header.numCheckHeaderID THEN 1 ELSE 0 END)
																		WHEN General_Journal_Header.numBillPaymentID > 0
																		THEN (CASE WHEN VIEW_JOURNALCHEQ.numReferenceID = General_Journal_Header.numBillPaymentID and VIEW_JOURNALCHEQ.tintReferenceType = 8 THEN 1 ELSE 0 END)
																		ELSE 0 
																		END)) VIEW_JOURNALCHEQ ON TRUE
	 LEFT OUTER JOIN
      DepositMaster DM ON DM.numDepositId = General_Journal_Header.numDepositId  LEFT OUTER JOIN
      ReturnHeader RH ON RH.numReturnHeaderID = General_Journal_Header.numReturnID LEFT OUTER JOIN
      CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID AND CH.tintReferenceType = 10
      LEFT OUTER JOIN BillHeader BH  ON coalesce(General_Journal_Header.numBillID,0) = BH.numBillID
      WHERE   General_Journal_Header.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate -- Removed Tmezone bug id 1029 
      AND General_Journal_Header.numDomainId = v_numDomainID
      AND (DivisionMaster.numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
      AND (bitCleared =(CASE v_charReconFilter WHEN 'C' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND (bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND Chart_Of_Accounts.numAccountId IN(SELECT numAccountID FROM tt_TEMP1)
      AND (v_tintFilterTransactionType = 0 OR 1 =(CASE v_tintFilterTransactionType
      WHEN 1 THEN(CASE WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 1 THEN 1 ELSE 0 END)
      WHEN 3 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 2 THEN 1 ELSE 0 END)
      WHEN 4 THEN(CASE WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 5 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 6 THEN(CASE WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 1 ELSE 0 END)
      WHEN 7 THEN(CASE WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 1 ELSE 0 END)
      WHEN 8 THEN(CASE WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true THEN 1 ELSE 0 END)
      WHEN 9 THEN(CASE WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 10 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 1 ELSE 0 END)
      WHEN 11 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 12 THEN(CASE WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 1 ELSE 0 END)
      WHEN 13 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 1 ELSE 0 END)
      WHEN 14 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 15 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 1 ELSE 0 END)
      WHEN 16 THEN(CASE WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 1 ELSE 0 END)
      ELSE 0
      END))
      AND (coalesce(v_vcFiterName) = '' OR  CompanyInfo.vcCompanyName ilike CONCAT('%',v_vcFiterName,'%'))
      AND (coalesce(v_vcFiterDescription) = '' OR  General_Journal_Details.varDescription ilike CONCAT('%',v_vcFiterDescription,'%'))
      AND (coalesce(v_vcFiterSplit) = '' OR  COALESCE((SELECT string_agg(COA.vcAccountName,', ')
         FROM General_Journal_Details AS GJD JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
         WHERE GJD.numJournalId = General_Journal_Header.numJOurnal_Id
         AND COA.numDomainId = General_Journal_Header.numDomainId
         AND COA.numAccountId <> Chart_Of_Accounts.numAccountId)) ilike CONCAT('%',v_vcFiterSplit,'%'))
      AND (coalesce(v_vcFiterAmount) = '' OR  CAST(coalesce(General_Journal_Details.numDebitAmt,0) AS VARCHAR(20)) = v_vcFiterAmount)
      AND (coalesce(v_vcFiterNarration) = '' OR  (coalesce(coalesce(coalesce(fn_getpaymentbizdoc(General_Journal_Header.numBizDocsPaymentDetId,v_numDomainID),General_Journal_Header.varDescription)) || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
      ELSE  VIEW_JOURNALCHEQ.Narration
      END)) ilike CONCAT('%',v_vcFiterNarration,'%') OR General_Journal_Details.vcReference ilike CONCAT('%',v_vcFiterNarration,'%'))) LIMIT 1;
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      SELECT
      v_numDomainID AS "numDomainID"
		,numAccountId AS "numAccountId"
		,vcAccountName AS "vcAccountName"
		,numParntAcntTypeID AS "numParntAcntTypeID"
		,vcAccountDescription AS "vcAccountDescription"
		,vcAccountCode AS "vcAccountCode"
		,dtAsOnDate AS "dtAsOnDate"
		,CASE WHEN (Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%') AND(v_year::bigint+v_Month::bigint) =(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN 0
      WHEN(v_year::bigint+v_Month::bigint) >(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t1.OPENING,0)
      WHEN(v_year::bigint+v_Month::bigint) <(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t2.OPENING,0)
      ELSE coalesce(mnOpeningBalance,0)
      END  AS "mnOpeningBalance"
		,(SELECT
         coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
         FROM
         General_Journal_Details GJD
         JOIN
         Domain D
         ON
         D.numDomainId = GJD.numDomainId AND D.numDomainId = v_numDomainID
         INNER JOIN
         General_Journal_Header GJH
         ON
         GJD.numJournalId = GJH.numJOurnal_Id
         AND GJD.numDomainId = v_numDomainID
         AND GJD.numChartAcntId = Table1.numAccountId
         AND GJH.numDomainId = D.numDomainId
         AND GJH.datEntry_Date <= v_dtFinFromDate)
      FROM
      fn_GetOpeningBalance(v_vcAccountId,v_numDomainID,v_dtFromDate,v_ClientTimeZoneOffset) Table1
      LEFT JOIN LATERAL(SELECT
         SUM(Debit -Credit) AS OPENING
         FROM
         VIEW_JOURNAL VJ
         WHERE
         VJ.numDomainID = v_numDomainID
         AND VJ.numAccountId = Table1.numAccountID
         AND (numAccountClass = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0)
         AND datEntry_Date BETWEEN(CASE
         WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '-1 minute') AS t1
      LEFT JOIN LATERAL(SELECT
         SUM(Debit -Credit) AS OPENING
         FROM
         VIEW_JOURNAL VJ
         WHERE
         VJ.numDomainID = v_numDomainID
         AND VJ.numAccountId = Table1.numAccountID
         AND (numAccountClass = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0)
         AND datEntry_Date BETWEEN(CASE
         WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '1 minute') AS t2 on TRUE on TRUE;

      open SWV_RefCur2 for
      SELECT
      General_Journal_Details.numDomainId AS "numDomainId"
		,Chart_Of_Accounts.numAccountId AS "numAccountId"
		,General_Journal_Header.numJOurnal_Id AS "numJOurnal_Id"
		,FormatedDateFromDate(General_Journal_Header.datEntry_Date,General_Journal_Details.numDomainId) AS "Date"
		,General_Journal_Header.varDescription AS "varDescription"
		,CompanyInfo.vcCompanyName AS "CompanyName"
		,General_Journal_Details.numTransactionId AS "numTransactionId"
		,coalesce(coalesce(coalesce(fn_getpaymentbizdoc(General_Journal_Header.numBizDocsPaymentDetId,v_numDomainID),General_Journal_Header.varDescription),'') || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
      ELSE  VIEW_JOURNALCHEQ.Narration
      END),'') as "Narration"
		,coalesce(General_Journal_Details.vcReference,'') AS "TranRef"
		,coalesce(General_Journal_Details.varDescription,'') AS "TranDesc"
		,coalesce(General_Journal_Details.numDebitAmt,0)::VARCHAR AS "numDebitAmt"
		,coalesce(General_Journal_Details.numCreditAmt,0)::VARCHAR AS "numCreditAmt"
		,Chart_Of_Accounts.vcAccountName AS "vcAccountName" 
		,'' as "balance"
		,CASE
      WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 'Checks'
      WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
      WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
      WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 1 THEN coalesce(OpportunityBizDocs.vcBizDocID,'BizDocs Invoice')
      WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 2 THEN coalesce(OpportunityBizDocs.vcBizDocID,'BizDocs Purchase')
      WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
      WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
      WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN CONCAT('Bill-',General_Journal_Header.numBillID)
      WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 'Pay Bill'
      WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN
         CASE
         WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
         WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
         WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
         WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
         WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund Against Credit Memo'
         END
      WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
      WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 'Journal'
      END AS "TransactionType",
		coalesce(General_Journal_Details.bitCleared,false) AS "bitCleared",
		coalesce(General_Journal_Details.bitReconcile,false) AS "bitReconcile",
		coalesce(General_Journal_Details.numItemID,0) AS "numItemID",
		General_Journal_Details.numEntryDateSortOrder1 AS "numEntryDateSortOrder",
		Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS "numLandedCostOppId",
		COALESCE((SELECT string_agg(CAST(COA.vcAccountName AS VARCHAR),', ')
         FROM General_Journal_Details AS GJD JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
         WHERE GJD.numJournalId = General_Journal_Header.numJOurnal_Id
         AND COA.numDomainId = General_Journal_Header.numDomainId
         AND COA.numAccountId <> Chart_Of_Accounts.numAccountId),'') AS "vcSplitAccountName"
      FROM General_Journal_Header INNER JOIN
      General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId INNER JOIN
      Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
      timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID
      LEFT OUTER JOIN DivisionMaster ON General_Journal_Details.numCustomerId = DivisionMaster.numDivisionID and
      General_Journal_Details.numDomainId = DivisionMaster.numDomainID
      LEFT OUTER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      LEFT OUTER JOIN OpportunityMaster ON General_Journal_Header.numOppId = OpportunityMaster.numOppId LEFT OUTER JOIN
      OpportunityBizDocs ON General_Journal_Header.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId 
	 LEFT JOIN LATERAL (SELECT * FROM VIEW_JOURNALCHEQ WHERE 1 = (CASE 
																		WHEN General_Journal_Header.numCheckHeaderID > 0
																		THEN (CASE WHEN VIEW_JOURNALCHEQ.numCheckHeaderID = General_Journal_Header.numCheckHeaderID THEN 1 ELSE 0 END)
																		WHEN General_Journal_Header.numBillPaymentID > 0
																		THEN (CASE WHEN VIEW_JOURNALCHEQ.numReferenceID = General_Journal_Header.numBillPaymentID and VIEW_JOURNALCHEQ.tintReferenceType = 8 THEN 1 ELSE 0 END)
																		ELSE 0 
																		END)) VIEW_JOURNALCHEQ ON TRUE
		LEFT OUTER JOIN
      DepositMaster DM ON DM.numDepositId = General_Journal_Header.numDepositId  LEFT OUTER JOIN
      ReturnHeader RH ON RH.numReturnHeaderID = General_Journal_Header.numReturnID LEFT OUTER JOIN
      CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID AND CH.tintReferenceType = 10
      LEFT OUTER JOIN BillHeader BH  ON coalesce(General_Journal_Header.numBillID,0) = BH.numBillID
      WHERE   General_Journal_Header.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate -- Removed Tmezone bug id 1029 
      AND General_Journal_Header.numDomainId = v_numDomainID
      AND (DivisionMaster.numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
      AND (bitCleared =(CASE v_charReconFilter WHEN 'C' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND (bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND Chart_Of_Accounts.numAccountId IN  (SELECT  ID::NUMERIC FROM SplitIDs(v_vcAccountId,','))
      AND (General_Journal_Details.numItemID = v_numItemID OR v_numItemID = 0)
      AND (v_tintFilterTransactionType = 0 OR 1 =(CASE v_tintFilterTransactionType
      WHEN 1 THEN(CASE WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 1 THEN 1 ELSE 0 END)
      WHEN 3 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 2 THEN 1 ELSE 0 END)
      WHEN 4 THEN(CASE WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 5 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 6 THEN(CASE WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 1 ELSE 0 END)
      WHEN 7 THEN(CASE WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 1 ELSE 0 END)
      WHEN 8 THEN(CASE WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true THEN 1 ELSE 0 END)
      WHEN 9 THEN(CASE WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 1 ELSE 0 END)
      WHEN 10 THEN(CASE WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 1 ELSE 0 END)
      WHEN 11 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 12 THEN(CASE WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 1 ELSE 0 END)
      WHEN 13 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 1 ELSE 0 END)
      WHEN 14 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 1 ELSE 0 END)
      WHEN 15 THEN(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 AND RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 1 ELSE 0 END)
      WHEN 16 THEN(CASE WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 1 ELSE 0 END)
      ELSE 0
      END))
      AND (coalesce(v_vcFiterName,'') = '' OR  CompanyInfo.vcCompanyName ilike CONCAT('%',v_vcFiterName,'%'))
      AND (coalesce(v_vcFiterDescription,'') = '' OR  General_Journal_Details.varDescription ilike CONCAT('%',v_vcFiterDescription,'%'))
      AND (coalesce(v_vcFiterSplit,'') = '' OR  COALESCE((SELECT string_agg(COA.vcAccountName,', ')
         FROM General_Journal_Details AS GJD JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
         WHERE GJD.numJournalId = General_Journal_Header.numJOurnal_Id
         AND COA.numDomainId = General_Journal_Header.numDomainId
         AND COA.numAccountId <> Chart_Of_Accounts.numAccountId),'') ilike CONCAT('%',v_vcFiterSplit,'%'))
      AND (coalesce(v_vcFiterAmount,'') = '' OR  CAST(coalesce(General_Journal_Details.numDebitAmt,0) AS VARCHAR(20)) = v_vcFiterAmount)
      AND (coalesce(v_vcFiterNarration,'') = '' OR  (coalesce(coalesce(coalesce(fn_getpaymentbizdoc(General_Journal_Header.numBizDocsPaymentDetId,v_numDomainID),General_Journal_Header.varDescription),'') || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
      ELSE  VIEW_JOURNALCHEQ.Narration
      END),'') ilike CONCAT('%',v_vcFiterNarration,'%') OR General_Journal_Details.vcReference ilike CONCAT('%',v_vcFiterNarration,'%')))
      ORDER BY General_Journal_Header.datEntry_Date asc;
   end if;
END; $$;



