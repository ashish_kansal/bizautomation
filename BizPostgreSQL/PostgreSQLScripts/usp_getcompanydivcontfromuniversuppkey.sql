-- Stored procedure definition script usp_GetCompanyDivContFromUniverSuppKey for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyDivContFromUniverSuppKey(v_numUSuppKey NUMERIC
	--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivID  NUMERIC;
BEGIN
   select   numDivisionID INTO v_numDivID FROM UniversalSupportKeyMaster WHERE numUniversalSupportKey = v_numUSuppKey;
	
   open SWV_RefCur for SELECT DM.numDivisionID, DM.vcDivisionName, cast(CM.numCompanyId as VARCHAR(255)), cast(CM.vcCompanyName as VARCHAR(255)),
	ACI.numContactId, ACI.vcFirstName, ACI.vcLastname
   FROM DivisionMaster DM INNER JOIN CompanyInfo CM ON DM.numCompanyID = CM.numCompanyId INNER JOIN AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
   WHERE DM.numDivisionID = v_numDivID
   ORDER BY ACI.vcFirstName,ACI.vcLastname;
END; $$;












