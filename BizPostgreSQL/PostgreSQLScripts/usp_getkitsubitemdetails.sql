-- Stored procedure definition script USP_GetKitSubItemDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetKitSubItemDetails(v_numItemDetailID		BIGINT,
	v_numDomainID			NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  coalesce(ID.numItemDetailID,0) AS "numItemDetailID",
			coalesce(ID.numItemKitID,0) AS "numItemKitID",
			coalesce(ID.numChildItemID,0) AS "numChildItemID",
			coalesce(ID.numQtyItemsReq,0) AS "numQtyItemsReq",
			coalesce(ID.numUOMID,0) AS "numUOMId",
			coalesce(ID.vcItemDesc,'') AS "vcItemDesc",
			coalesce(ID.sintOrder,0) AS "sintOrder"
   FROM  ItemDetails ID
   JOIN Item I ON ID.numItemKitID = I.numItemCode
   WHERE ID.numItemDetailID = v_numItemDetailID
   AND I.numDomainID = v_numDomainID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetailsDynamicForm]    Script Date: 07/26/2008 16:17:49 ******/













