-- Stored procedure definition script USP_GetAuthoritativeBizDocsDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAuthoritativeBizDocsDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from AuthoritativeBizDocs where  numDomainId = v_numDomainID;
END; $$;












