-- Stored procedure definition script USP_CheckMasterlistItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckMasterlistItem(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_ListID NUMERIC(9,0) DEFAULT 0,        
v_ListItemID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COUNT(*) as IsExists FROM Listdetails LD WHERE  LD.numListItemID = v_ListItemID AND LD.numListID = v_ListID  AND (LD.numDomainid = v_numDomainID Or LD.constFlag = true);
END; $$;













