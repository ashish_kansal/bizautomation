-- Stored procedure definition script USP_GetTimeAndExpense_BudgetTotal for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeAndExpense_BudgetTotal(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numProId NUMERIC(9,0) DEFAULT 0,
    v_numStageId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalTime  DECIMAL(10,2);
   v_TotalExpense  DECIMAL(10,2);
   v_TimeBudget  DECIMAL(10,2);
   v_ExpenseBudget  DECIMAL(10,2);
   v_bitTimeBudget  BOOLEAN;
   v_bitExpenseBudget  BOOLEAN;
BEGIN
   v_TotalTime := 0;
   v_TotalExpense := 0;

   v_TimeBudget := 0;
   v_ExpenseBudget := 0;

   select   coalesce(CAST(CAST(SUM(CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))/60 AS DECIMAL(10,2))) AS DECIMAL(10,2)) AS DECIMAL(10,2)),0) INTO v_TotalTime from  timeandexpense where numProId = v_numProId and numStageId = v_numStageId AND numCategory = 1 and tintTEType = 2
   AND numtype = 1 GROUP BY numtype;

   v_TotalExpense := CAST(coalesce(fn_GetExpenseDtlsbyProStgID(v_numProId,v_numStageId,1::SMALLINT),
   cast(0 as TEXT)) AS DECIMAL(10,2));

   select   coalesce(monTimeBudget,0), coalesce(monExpenseBudget,0), coalesce(bitTimeBudget,false), coalesce(bitExpenseBudget,false) INTO v_TimeBudget,v_ExpenseBudget,v_bitTimeBudget,v_bitExpenseBudget from StagePercentageDetails where numProjectid = v_numProId and numStageDetailsId = v_numStageId;

   if v_bitTimeBudget = true then
      v_TotalTime := v_TimeBudget -v_TotalTime;
   end if;

   if v_bitExpenseBudget = true then
      v_TotalExpense := v_ExpenseBudget -v_TotalExpense;
   end if;


   open SWV_RefCur for select cast(coalesce(v_TotalTime,0) as DECIMAL(10,2)) AS TotalTime,cast(coalesce(v_TotalExpense,0) as DECIMAL(10,2)) AS TotalExpense,cast(coalesce(v_bitTimeBudget,false) as BOOLEAN) AS bitTimeBudget,cast(coalesce(v_bitExpenseBudget,false) as BOOLEAN) AS bitExpenseBudget;

 

   RETURN;
END; $$;












