-- Function definition script CheckOrderInvoicingStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckOrderInvoicingStatus(v_numOppID NUMERIC(9,0),
      v_numDomainID NUMERIC(9,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintInvoicing  TEXT;
   v_numUnits  INTEGER;
BEGIN
   v_tintInvoicing := '<font color="#000000">All</font>';

	/********************************************************Fully Invoiced - 1********************************************************/
	--EXEC USP_CheckOrderedAndInvoicedOrBilledQty 23749



	--IF EXISTS(select 1)
	--BEGIN
	--	IF(SELECT COUNT(*) FROM Opportunity WHERE Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
 --                     			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
 --                     			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
 --                     			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
 --                     			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
 --                     			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour]))

	--	SET @tintInvoicing='<font color="#008000">Fully Invoiced</font>';
	--END

	/********************************************************UN-INVOICED - 2********************************************************/
   IF(SELECT COUNT(*)
   FROM OpportunityBizDocs
   WHERE numoppid = v_numOppID
   AND numBizDocId IN(SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID)) = 0 then
	
      v_tintInvoicing := '<font color="#800080">Un-Invoiced</font>';
   end if;

	/********************************************************PARTIALLY INVOICED (UNITS) - 3********************************************************/
   select(SUM(coalesce(OrderedQty,0)) -SUM(coalesce(InvoicedQty,0))) INTO v_numUnits FROM(SELECT OI.numoppitemtCode, coalesce(OI.numUnitHour,0) AS OrderedQty, coalesce(TempInvoice.InvoicedQty,0) AS InvoicedQty
      FROM OpportunityItems OI
      INNER JOIN Item I ON OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
         FROM OpportunityBizDocs
         INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE OpportunityBizDocs.numoppid = v_numOppID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND numBizDocId IN(SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID)) AS TempInvoice on TRUE
      WHERE
      OI.numOppId = v_numOppID) X;

   IF v_numUnits > 0 then
	
      v_tintInvoicing := '<font color="#800080">Partially Invoiced(Units)</font>';
   end if;
	/********************************************************PARTIALLY INVOICED (--) - 4********************************************************/

	--SELECT monDealAmount,* FROM OpportunityMaster WHERE numOppId = 717
	--SELECT SUM(monDealAmount) FROM OpportunityBizDocs WHERE numOppId=717 AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=72)

   IF(SELECT SUM(coalesce(monDealAmount,0))
   FROM OpportunityBizDocs
   WHERE numoppid = v_numOppID AND numBizDocId IN(SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID)) > 0 then
	
      v_tintInvoicing := '<font color="#800080">Partially Invoiced(%)</font>';
   end if;
    
	/********************************************************Deferred Income Invoices - 5********************************************************/


   RETURN coalesce(v_tintInvoicing,'');
END; $$;

