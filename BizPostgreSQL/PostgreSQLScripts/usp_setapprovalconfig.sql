-- Stored procedure definition script USP_SetApprovalConfig for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SetApprovalConfig(v_chrAction CHAR(10) DEFAULT NULL,
  v_numDomainId NUMERIC(18,2) DEFAULT 0,
  v_numUserId NUMERIC(18,2) DEFAULT 0,
  v_numModuleId NUMERIC(18,2) DEFAULT 0,
  v_numConfigId NUMERIC(18,2) DEFAULT 0,
  v_vchXML XML DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_COUNT  INTEGER DEFAULT 0;
   v_INT_COUNTER  INTEGER DEFAULT 0;
   v_numLevel1Authority  NUMERIC;
   v_numLevel2Authority  NUMERIC;
   v_numLevel3Authority  NUMERIC;
   v_numLevel4Authority  NUMERIC;
   v_numLevel5Authority  NUMERIC;
BEGIN
   IF(v_chrAction = 'A') then
      DROP TABLE IF EXISTS tt_TMP_APPROVAL CASCADE;
      CREATE TEMPORARY TABLE tt_TMP_APPROVAL AS
         SELECT
			numUserId,
			numLevel1Authority,
			numLevel2Authority,
			numLevel3Authority,
			numLevel4Authority,
			numLevel5Authority
         FROM
         XMLTABLE('NewDataSet/ApprovalConfig'
         PASSING v_vchXML
         COLUMNS
			id FOR ORDINALITY,
            numUserId NUMERIC PATH 'numUserId',
            numLevel1Authority NUMERIC PATH 'numLevel1Authority',	
            numLevel2Authority NUMERIC PATH 'numLevel2Authority',
            numLevel3Authority NUMERIC PATH 'numLevel3Authority',
			numLevel4Authority NUMERIC PATH 'numLevel4Authority',
			numLevel5Authority NUMERIC PATH 'numLevel5Authority') AS Tabx;

      ALTER TABLE tt_TMP_APPROVAL add ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1);
      open SWV_RefCur for SELECT * FROM  tt_TMP_APPROVAL;
      v_INT_COUNTER := 1;
      select   COUNT(*) INTO v_COUNT FROM tt_TMP_APPROVAL;
      WHILE v_INT_COUNTER <= v_COUNT LOOP
         IF(v_numDomainId > 0) then
			
            select   numUserId, numLevel1Authority, numLevel2Authority, numLevel3Authority, numLevel4Authority, numLevel5Authority INTO v_numUserId,v_numLevel1Authority,v_numLevel2Authority,v_numLevel3Authority,
            v_numLevel4Authority,v_numLevel5Authority FROM
            tt_TMP_APPROVAL WHERE
            ID = v_INT_COUNTER;
            IF((SELECT COUNT(*) FROM ApprovalConfig WHERE numUserId = v_numUserId AND numModule = v_numModuleId AND numDomainID = v_numDomainId) > 0) then
				
               SELECT  numConfigId INTO v_numConfigId FROM ApprovalConfig WHERE numUserId = v_numUserId AND numModule = v_numModuleId AND numDomainID = v_numDomainId     LIMIT 1;
               UPDATE
               ApprovalConfig
               SET
               numLevel1Authority = v_numLevel1Authority,numLevel2Authority = v_numLevel2Authority,
               numLevel3Authority = v_numLevel3Authority,numLevel4Authority = v_numLevel4Authority,
               numLevel5Authority = v_numLevel5Authority
               WHERE
               numUserId = v_numUserId AND numModule = v_numModuleId AND numDomainID = v_numDomainId;
            ELSE
               INSERT INTO
               ApprovalConfig(numDomainID,numModule,numUserId,numLevel1Authority,
							numLevel2Authority,numLevel3Authority,numLevel4Authority,numLevel5Authority)
					VALUES(v_numDomainId,v_numModuleId,v_numUserId,v_numLevel1Authority,
							v_numLevel2Authority,v_numLevel3Authority,v_numLevel4Authority,v_numLevel5Authority);
            end if;
            v_INT_COUNTER := v_INT_COUNTER::bigint+1;
         end if;
      END LOOP;
   end if;
   RETURN;
END; $$;













