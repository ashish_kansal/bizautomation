-- Stored procedure definition script USP_UpdateBizDocNameTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--SELECT * FROM dbo.ReturnHeader
--EXEC USP_UpdateBizDocNameTemplate  6,1,18
CREATE OR REPLACE FUNCTION USP_UpdateBizDocNameTemplate(v_tintType SMALLINT DEFAULT 0,
v_numDomainID NUMERIC DEFAULT NULL,
v_RecordID NUMERIC DEFAULT NULL-- pass Orderid ,BizDocID,ProjectID, or ReturnHeaderID
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Tempate  VARCHAR(200);
   v_OrgName  VARCHAR(100);
   v_numBizDocId  NUMERIC;
   v_OrderID  NUMERIC; 
   v_numSequenceId  NUMERIC(9,0);
   v_numMinLength  NUMERIC(9,0);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF coalesce(v_tintType,0) > 2 then
      BEGIN
         -- BEGIN TRANSACTION
v_numSequenceId := 0;
         IF v_tintType = 3 then --Sales Credit Memo
            SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND constFlag = true AND vcData = 'Sales Credit Memo';
         ELSEIF v_tintType = 4
         then --Purchase Credit Memo
            SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND constFlag = true AND vcData = 'Purchase Credit Memo';
         ELSEIF v_tintType = 5
         then --Refund Receipt
            SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND constFlag = true AND vcData = 'Refund Receipt';
         ELSEIF v_tintType = 6
         then --Credit Memo
            SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND constFlag = true AND vcData = 'Credit Memo';
         ELSEIF v_tintType = 7
         then --Sales RMA (Mirror BizDoc)
            SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND constFlag = true AND vcData = 'RMA';
         end if;
         IF coalesce(v_numBizDocId,0) = 0 then
			
            RAISE EXCEPTION 'BizDoc Type Not found!!';
            RETURN;
         end if;
         IF NOT EXISTS(SELECT * FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2) then
			
            INSERT INTO NameTemplate(numDomainID,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
            SELECT v_numDomainID,2,v_numBizDocId,CASE WHEN v_numBizDocId = 290 THEN 'PROI' WHEN v_tintType = 3 THEN 'SCM' WHEN v_tintType = 4 THEN 'PCM' WHEN v_tintType = 5 THEN 'RFND' WHEN v_tintType = 6 THEN 'CM' WHEN v_tintType = 7 THEN 'RMA' ELSE UPPER(SUBSTR(GetListIemName(v_numBizDocId),0,4)) END || '-',1,4;
         end if;
         select   coalesce(vcNameTemplate,''), coalesce(numSequenceId,1), coalesce(numMinLength,4) INTO v_Tempate,v_numSequenceId,v_numMinLength FROM
         NameTemplate WHERE
         numDomainID = v_numDomainID
         AND numRecordID = v_numBizDocId
         AND tintModuleID = 2;			
			
			--Replace Other Values
         v_Tempate := REPLACE(v_Tempate,'$YEAR$',TO_CHAR(TIMEZONE('UTC',now()),'YYYY'));
         v_Tempate := REPLACE(v_Tempate,'$MONTH$',TO_CHAR(TIMEZONE('UTC',now()),'Month'));
         v_Tempate := REPLACE(v_Tempate,'$DAY$',TO_CHAR(TIMEZONE('UTC',now()),'DD'));
         v_Tempate := REPLACE(v_Tempate,'$QUARTER$',EXTRACT(quarter FROM TIMEZONE('UTC',now()))::VARCHAR);
         IF POSITION(lower(v_Tempate) IN '$organizationname$') >= 0 then
			
            select   coalesce(C.vcCompanyName,'') INTO v_OrgName FROM ReturnHeader RH INNER JOIN DivisionMaster DM ON RH.numDivisionId = DM.numDivisionID
            INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE RH.numReturnHeaderID = v_RecordID;
            v_Tempate := REPLACE(v_Tempate,'$OrganizationName$',v_OrgName);
         end if;
         v_Tempate := coalesce(v_Tempate,'') || coalesce(REPEAT('0',(v_numMinLength -LENGTH(v_numSequenceId::VARCHAR))::INT),'') || SUBSTR(CAST(v_numSequenceId AS VARCHAR(18)),1,18);
         RAISE NOTICE '%',v_Tempate;
         IF EXISTS(SELECT numReturnHeaderID FROM ReturnHeader WHERE numDomainID = v_numDomainID AND numReturnHeaderID <> v_RecordID AND vcRMA = v_Tempate) then
			
            RAISE EXCEPTION 'Duplicate return name!!';
            RETURN;
         end if;
         IF v_tintType = 3 OR v_tintType = 4 then
            UPDATE ReturnHeader SET vcBizDocName = v_Tempate WHERE numReturnHeaderID = v_RecordID;
         ELSEIF v_tintType = 5 OR v_tintType = 6
         then
            UPDATE ReturnHeader SET vcBizDocName = v_Tempate,vcRMA =(CASE WHEN tintReturnType = 1 THEN vcRMA ELSE v_Tempate END) WHERE numReturnHeaderID = v_RecordID;
         ELSEIF v_tintType = 7
         then --RMA
            UPDATE ReturnHeader SET vcRMA = v_Tempate WHERE numReturnHeaderID = v_RecordID;
         end if;
         UPDATE NameTemplate SET numSequenceId = numSequenceId+1  WHERE numDomainID = v_numDomainID
         AND numRecordID = v_numBizDocId AND tintModuleID = 2;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
           GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   ELSE
      select   coalesce(numBizDocId,0), coalesce(numoppid,0) INTO v_numBizDocId,v_OrderID FROM OpportunityBizDocs WHERE numOppBizDocsId = v_RecordID;
      v_numSequenceId := 0;
      IF NOT EXISTS(SELECT * FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2) then
		
         INSERT INTO NameTemplate(numDomainID,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
         SELECT v_numDomainID,2,v_numBizDocId,CASE WHEN v_numBizDocId = 290 THEN 'PROI' ELSE UPPER(SUBSTR(GetListIemName(v_numBizDocId),0,4)) END || '-',1,4;
      end if;
		
		--Get Custom Template If enabled
		/*,@numSequenceId=ISNULL(numSequenceId,1),@numMinLength=ISNULL(numMinLength,4)*/ 
      select   coalesce(vcNameTemplate,'') INTO v_Tempate FROM NameTemplate WHERE numDomainID = v_numDomainID
      AND numRecordID = v_numBizDocId AND tintModuleID = 2;
		
		--Replace Other Values
      v_Tempate := REPLACE(v_Tempate,'$YEAR$',TO_CHAR(TIMEZONE('UTC',now()),'YYYY'));
      v_Tempate := REPLACE(v_Tempate,'$MONTH$',TO_CHAR(TIMEZONE('UTC',now()),'Month'));
      v_Tempate := REPLACE(v_Tempate,'$DAY$',TO_CHAR(TIMEZONE('UTC',now()),'DD'));
      v_Tempate := REPLACE(v_Tempate,'$QUARTER$',EXTRACT(quarter FROM TIMEZONE('UTC',now()))::VARCHAR);
      IF POSITION(lower(v_Tempate) IN '$organizationname$') >= 0 then
			
         select   coalesce(C.vcCompanyName,'') INTO v_OrgName FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
         INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE numOppId = v_OrderID;
         v_Tempate := REPLACE(v_Tempate,'$OrganizationName$',v_OrgName);
      end if;
      v_Tempate := REPLACE(v_Tempate,'$OrderID$',v_OrderID:: TEXT);
      v_Tempate := REPLACE(v_Tempate,'$BizDocID$',v_RecordID:: TEXT );
      v_Tempate := v_Tempate; --+ ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
			
      RAISE NOTICE '%',v_Tempate;
      select   coalesce(MAX(case when numSequenceId = '' then 0 else numSequenceId:: BIGINT end),1) INTO v_numSequenceId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid WHERE OM.numDomainId = v_numDomainID AND OBD.numBizDocId = v_numBizDocId;
      UPDATE OpportunityBizDocs SET vcBizDocName = v_Tempate/*,numSequenceId=ISNULL(@numSequenceId,0) + 1*/  WHERE numOppBizDocsId = v_RecordID;
      IF v_numBizDocId <> 297 AND v_numBizDocId <> 299 then
			
         UPDATE
         NameTemplate
         SET
         numSequenceId = coalesce(v_numSequenceId,0)+1
         WHERE
         numDomainID = v_numDomainID
         AND numRecordID = v_numBizDocId AND tintModuleID = 2;
      end if;
   end if;
END; $$;

