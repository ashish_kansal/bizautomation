-- Stored procedure definition script USP_UpdateOppLinkingDTls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOppLinkingDTls(v_numOppID NUMERIC(9,0) DEFAULT 0,            
v_tintOppType SMALLINT DEFAULT NULL,        
v_tintBillToType SMALLINT DEFAULT NULL,        
v_tintShipToType SMALLINT DEFAULT NULL,        
v_vcBillStreet VARCHAR(50) DEFAULT NULL,                  
v_vcBillCity VARCHAR(50) DEFAULT NULL,                  
v_numBillState NUMERIC(9,0) DEFAULT NULL,                  
v_vcBillPostCode VARCHAR(15) DEFAULT NULL,                  
v_numBillCountry NUMERIC(9,0) DEFAULT NULL,                          
v_vcShipStreet VARCHAR(50) DEFAULT NULL,                  
v_vcShipCity VARCHAR(50) DEFAULT NULL,                  
v_numShipState NUMERIC(9,0) DEFAULT NULL,                  
v_vcShipPostCode VARCHAR(15) DEFAULT NULL,                  
v_numShipCountry NUMERIC(9,0) DEFAULT NULL,    
v_vcBillCompanyName VARCHAR(100) DEFAULT NULL,    
v_vcShipCompanyName VARCHAR(100) DEFAULT NULL,
v_numParentOppID NUMERIC(9,0) DEFAULT NULL,
v_numParentOppBizDocID NUMERIC(9,0) DEFAULT NULL,
v_numBillToAddressID NUMERIC(18,0) DEFAULT 0,
v_numShipToAddressID NUMERIC(18,0) DEFAULT 0,
v_numBillingContact NUMERIC(18,0) DEFAULT 0,
v_bitAltBillingContact BOOLEAN DEFAULT false,
v_vcAltBillingContact VARCHAR(200) DEFAULT '',
v_numShippingContact NUMERIC(18,0) DEFAULT 0,
v_bitAltShippingContact BOOLEAN DEFAULT false,
v_vcAltShippingContact VARCHAR(200) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numBillCompany  NUMERIC(18,0);
   v_numShipCompany  NUMERIC(18,0);
BEGIN
   IF coalesce(v_numBillToAddressID,0) > 0 then
      select(CASE WHEN tintAddressOf = 2 THEN AddressDetails.numRecordID ELSE 0 END), (CASE WHEN tintAddressOf = 2 THEN coalesce((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = AddressDetails.numRecordID),'') ELSE '' END), vcStreet, vcCity, numState, vcPostalCode, numCountry INTO v_numBillCompany,v_vcBillCompanyName,v_vcBillStreet,v_vcBillCity,v_numBillState,
      v_vcBillPostCode,v_numBillCountry FROM
      AddressDetails WHERE
      numAddressID = v_numBillToAddressID;
      UPDATE
      OpportunityMaster
      SET
      tintBillToType = 2
      WHERE
      numOppId = v_numOppID;
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) = 0 then
		
         INSERT INTO OpportunityAddress(numOppID) VALUES(v_numOppID);
      end if;
      UPDATE
      OpportunityAddress
      SET
      numBillCompanyId = v_numBillCompany,vcBillCompanyName = v_vcBillCompanyName,
      vcBillStreet = v_vcBillStreet,vcBillCity = v_vcBillCity,numBillState = v_numBillState,
      vcBillPostCode = v_vcBillPostCode,numBillCountry = v_numBillCountry,
      numBillingContact = v_numBillingContact,bitAltBillingContact = v_bitAltBillingContact,
      vcAltBillingContact = v_vcAltBillingContact
      WHERE
      numOppID = v_numOppID;
   ELSE
      UPDATE
      OpportunityMaster
      SET
      tintBillToType = v_tintBillToType,numBillToAddressID = v_numBillToAddressID
      WHERE
      numOppId = v_numOppID;
   end if;

   IF coalesce(v_numShipToAddressID,0) > 0 then
      select(CASE WHEN tintAddressOf = 2 THEN AddressDetails.numRecordID ELSE 0 END), (CASE WHEN tintAddressOf = 2 THEN coalesce((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = AddressDetails.numRecordID),'') ELSE '' END), vcStreet, vcCity, numState, vcPostalCode, numCountry INTO v_numShipCompany,v_vcShipCompanyName,v_vcShipStreet,v_vcShipCity,v_numShipState,
      v_vcShipPostCode,v_numShipCountry FROM
      AddressDetails WHERE
      numAddressID = v_numShipToAddressID;
      UPDATE
      OpportunityMaster
      SET
      tintShipToType = 2
      WHERE
      numOppId = v_numOppID;
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) = 0 then
		
         INSERT INTO OpportunityAddress(numOppID) VALUES(v_numOppID);
      end if;
      UPDATE
      OpportunityAddress
      SET
      numShipCompanyId = v_numShipCompany,vcShipCompanyName = v_vcShipCompanyName,
      vcShipStreet = v_vcShipStreet,vcShipCity = v_vcShipCity,numShipState = v_numShipState,
      vcShipPostCode = v_vcShipPostCode,numShipCountry = v_numShipCountry,
      numShippingContact = v_numShippingContact,bitAltShippingContact = v_bitAltShippingContact,
      vcAltShippingContact = v_vcAltShippingContact
      WHERE
      numOppID = v_numOppID;
   ELSE
      UPDATE
      OpportunityMaster
      SET
      tintShipToType = v_tintShipToType,numShipToAddressID = v_numShipToAddressID
      WHERE
      numOppId = v_numOppID;
   end if;           
	        
   IF (v_tintBillToType = 2 AND coalesce(v_numBillToAddressID,0) = 0) OR (v_tintShipToType = 2  AND coalesce(v_numShipToAddressID,0) = 0) then
	
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) = 0 then
		
         INSERT  INTO OpportunityAddress(numOppID)
			VALUES(v_numOppID);
      end if;
   end if;

   IF v_tintBillToType = 2 AND coalesce(v_numBillToAddressID,0) = 0 then
	
      UPDATE  OpportunityAddress
      SET     vcBillCompanyName = v_vcBillCompanyName,vcBillStreet = v_vcBillStreet,
      vcBillCity = v_vcBillCity,numBillState = v_numBillState,vcBillPostCode = v_vcBillPostCode,
      numBillCountry = v_numBillCountry,bitAltBillingContact = v_bitAltBillingContact,
      vcAltBillingContact = v_vcAltBillingContact
      WHERE   numOppID = v_numOppID;
   end if;
    
   IF v_tintShipToType = 2 AND coalesce(v_numShipToAddressID,0) = 0 then
	
      RAISE NOTICE 'hi';
      UPDATE  OpportunityAddress
      SET     vcShipCompanyName = v_vcShipCompanyName,vcShipStreet = v_vcShipStreet,
      vcShipCity = v_vcShipCity,numShipState = v_numShipState,vcShipPostCode = v_vcShipPostCode,
      numShipCountry = v_numShipCountry,bitAltShippingContact = v_bitAltShippingContact,
      vcAltShippingContact = v_vcAltShippingContact
      WHERE   numOppID = v_numOppID;
   end if;

   select   numDomainId, numDivisionId INTO v_numDomainID,v_numDivisionID FROM
   OpportunityMaster WHERE
   numOppId = v_numOppID;

   DELETE FROM OpportunityMasterTaxItems WHERE numOppId = v_numOppID;

	--INSERT TAX FOR DIVISION   
   IF v_tintOppType = 1 OR (v_tintOppType = 2 and(select coalesce(bitPurchaseTaxCredit,false) from Domain where numDomainId = v_numDomainID) = true) then
	
      INSERT INTO OpportunityMasterTaxItems(numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID)
      SELECT
      v_numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
      FROM
      TaxItems TI
      JOIN
      DivisionTaxTypes DTT
      ON
      TI.numTaxItemID = DTT.numTaxItemID
      CROSS JOIN LATERAL(SELECT
         decTaxValue,
				tintTaxType
         FROM
         fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,TI.numTaxItemID,v_numOppID,false,NULL)) AS TEMPTax
      WHERE
      DTT.numDivisionID = v_numDivisionID
      AND DTT.bitApplicable = true
      UNION
      SELECT
      v_numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
      FROM
      DivisionMaster
      CROSS JOIN LATERAL(SELECT
         decTaxValue,
				tintTaxType
         FROM
         fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,0,v_numOppID,true,NULL)) AS TEMPTax
      WHERE
      bitNoTax = false
      AND numDivisionID = v_numDivisionID
      UNION
      SELECT
      v_numOppID
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
      FROM
      fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,1,v_numOppID,true,NULL);
   end if;	


   if v_numParentOppID > 0 then
	
      IF v_numParentOppBizDocID = 0 then
         v_numParentOppBizDocID := NULL;
      end if;
      insert into OpportunityLinking(numParentOppID,numChildOppID,numParentOppBizDocID) values(v_numParentOppID,v_numOppID,v_numParentOppBizDocID);
		
      If (v_tintOppType = 2 OR v_tintOppType = 1) then
		
         UPDATE
         OpportunityMaster
         SET
         "vcCustomerPO#" = coalesce((SELECT vcOppRefOrderNo FROM OpportunityMaster WHERE numOppId = v_numParentOppID),'')
         WHERE
         numOppId = v_numOppID;
         IF EXISTS(SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID = v_numDomainID AND numParentFieldID = 100 AND tintParentModule = 2 AND numChildFieldID = 100 AND (tintChildModule = 6 OR tintChildModule = 2)) then
			
            UPDATE
            OpportunityMaster
            SET
            numassignedto = coalesce((SELECT numassignedto FROM OpportunityMaster WHERE numOppId = v_numParentOppID),0)
            WHERE
            numOppId = v_numOppID;
         end if;
         IF EXISTS(SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID = v_numDomainID AND numParentFieldID = 122 AND tintParentModule = 2 AND numChildFieldID = 122 AND (tintChildModule = 6 OR tintChildModule = 2)) then
			
            UPDATE
            OpportunityMaster
            SET
            txtComments = coalesce((SELECT txtComments FROM OpportunityMaster WHERE numOppId = v_numParentOppID),'')
            WHERE
            numOppId = v_numOppID;
         end if;
         IF EXISTS(SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID = v_numDomainID AND numParentFieldID = 771 AND tintParentModule = 2 AND numChildFieldID = 771 AND (tintChildModule = 6 OR tintChildModule = 2)) then
			
            UPDATE
            OpportunityMaster
            SET
            intUsedShippingCompany = coalesce((SELECT intUsedShippingCompany FROM OpportunityMaster WHERE numOppId = v_numParentOppID),0),numShipmentMethod = coalesce((SELECT numShipmentMethod FROM OpportunityMaster WHERE numOppId = v_numParentOppID),0),
            numShippingService = coalesce((SELECT numShippingService FROM OpportunityMaster WHERE numOppId = v_numParentOppID),0)
            WHERE
            numOppId = v_numOppID;
         end if;
         If (v_tintOppType = 2) then
			
				 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
            PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numOppID,v_numParentRecId := v_numParentOppID,
            v_tintPageID := 6::SMALLINT); --  tinyin

            PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numOppID,v_numParentRecId := v_numDivisionID,
            v_tintPageID := 6::SMALLINT);
         ELSEIF (v_tintOppType = 1)
         then
			
				 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
							 --  numeric(18, 0)
            PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numOppID,v_numParentRecId := v_numParentOppID,
            v_tintPageID := 2::SMALLINT); --  tinyin

            PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numOppID,v_numParentRecId := v_numDivisionID,
            v_tintPageID := 2::SMALLINT);
         end if;
      end if;
   end if;
   RETURN;
END; $$;


