-- Stored procedure definition script USP_ManageAuthorizationGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAuthorizationGroup(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numGroupID NUMERIC(9,0) DEFAULT 0,
v_vcGroupName VARCHAR(100) DEFAULT '',
v_tinTGroupType SMALLINT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numGroupID = 0 then

      INSERT INTO AuthenticationGroupMaster(vcGroupName,
		numDomainID,
		tintGroupType)
	VALUES(v_vcGroupName,
		v_numDomainID,
		v_tinTGroupType);
	
      v_numGroupID := CURRVAL('AuthenticationGroupMaster_seq');

	--MainTab From TabMaster 
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,1,0,true,1,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,36,0,true,2,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,7,0,true,3,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,80,0,true,4,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,3,0,true,5,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,45,0,true,6,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,4,0,true,7,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,44,0,true,8,0);
	
      INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupID,6,0,true,9,0);
	
      IF(SELECT COUNT(*) FROM TreeNavigationAuthorization WHERE numDomainID = v_numDomainID AND numGroupID = v_numGroupID) = 0 then
	
         INSERT  INTO TreeNavigationAuthorization(numGroupID,
			numTabID,
			numPageNavID,
			bitVisible,
			numDomainID,
			tintType)
         SELECT
         v_numGroupID as numGroupID,
			PND.numTabID,
			numPageNavID,
			true,
			v_numDomainID AS numDomainID,
			v_tinTGroupType AS tintType
         FROM
         PageNavigationDTL PND
         WHERE
         PND.numTabID IS NOT NULL
         UNION ALL
         SELECT
         numGroupID AS numGroupID,
			1 AS numTabID,
			coalesce(LD.numListItemID,0) AS numPageNavID,
			true AS bitVisible,
			v_numDomainID AS numDomainID,
			v_tinTGroupType AS tintType
         FROM
         Listdetails LD
         CROSS JOIN
         AuthenticationGroupMaster AG
         WHERE
         numListID = 27
         AND coalesce(constFlag,false) = true
         AND AG.numDomainID = v_numDomainID
         AND AG.numGroupID = v_numGroupID
         AND numListItemID NOT IN(293,294,295,297,298,299);
      end if;
   ELSE
      UPDATE
      AuthenticationGroupMaster
      SET
      vcGroupName = v_vcGroupName,numDomainID = v_numDomainID,tintGroupType = v_tinTGroupType
      WHERE
      numGroupID = v_numGroupID;
   end if;
   RETURN;
END; $$;


