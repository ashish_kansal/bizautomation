-- Stored procedure definition script USP_CompanyList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CompanyList(v_numRelationType NUMERIC(9,0),                                   
v_numUserCntID NUMERIC(9,0),                                      
v_tintUserRightType SMALLINT,                                          
v_SortChar CHAR(1) DEFAULT '0',                                     
v_FirstName VARCHAR(100) DEFAULT '',                                      
v_LastName VARCHAR(100) DEFAULT '',                                      
v_CustName VARCHAR(100) DEFAULT '',                                    
v_CurrentPage INTEGER DEFAULT NULL,                                    
v_PageSize INTEGER DEFAULT NULL,                                    
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                    
v_columnName VARCHAR(50) DEFAULT NULL,                                    
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                            
v_tintSortOrder NUMERIC DEFAULT 0,                             
v_numProfile NUMERIC(9,0) DEFAULT 0,                    
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_tintCRMType SMALLINT DEFAULT NULL,            
v_bitPartner BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                    
                                    
--Create a Temporary table to hold data                                    
   AS $$
   DECLARE
   v_strSql  VARCHAR(5000);                                    
   v_firstRec  INTEGER;                                    
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numDivisionID VARCHAR(15)
   );                                    
                                    
                                    
   v_strSql := 'SELECT                                        
     DM.numDivisionID                          
    FROM  CompanyInfo CMP                                    
    join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId';                            
                            
   if v_tintSortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactID = DM.numDivisionID ';
   end if;                            
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID = DM.numDivisionID and bitShareportal = true and bitDeleted = false           
and CA.numDivisionID =(select numDivisionID from AdditionalContactsInformation where numContactID =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;          
   v_strSql := coalesce(v_strSql,'') || ' join AdditionalContactsInformation ADC on ADC.numDivisionID = DM.numDivisionID                                    
    left join ListDetails LD on LD.numListItemID = CMP.numCompanyRating                                     
    left join ListDetails LD1 on LD1.numListItemID = DM.numFollowUpStatus                                     
  WHERE coalesce(ADC.bitPrimaryContact,false) = true                                     
    AND (DM.bitPublicFlag = false OR DM.numRecOwner =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')                                          
    AND DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                                    
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                     
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''';
   end if;                                    
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;               
   if v_numRelationType <> cast(NULLIF('0','') as NUMERIC(9,0)) then 
      v_strSql := coalesce(v_strSql,'') || ' AND CMP.numCompanyType = ' || SUBSTR(CAST(v_numRelationType AS VARCHAR(15)),1,15) || '';
   end if;                                    
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CMP.vcCompanyName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                     
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND ((DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( CA.bitShareportal=true and CA.bitDeleted=false))' else ')' end;
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;     
   if v_tintSortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and cType=''O''';
   end if;                            
   if v_numProfile > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  vcProfile=' || SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
   end if;                            
   if v_tintCRMType > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  DM.tintCRMType=' || SUBSTR(CAST(v_tintCRMType AS VARCHAR(1)),1,1);
   end if;                              
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                                    
   RAISE NOTICE '%',v_strSql;                                        
   EXECUTE 'insert into tt_TEMPTABLE (                                   
   numDivisionID)                                    
' || v_strSql;                                    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                    
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                              
                                   
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                            
                          
                          
                          
   open SWV_RefCur for
   SELECT  CMP.vcCompanyName || ' - <I>' || DM.vcDivisionName || '</I>' as CompanyName,
     DM.numTerID,
     ADC.vcFirstName  || ' ' || ADC.vcLastname as PrimaryContact,
     case when ADC.numPhone <> '' then ADC.numPhone || case when ADC.numPhoneExtension <> '' then ' - ' || ADC.numPhoneExtension else '' end  else '' end as Phone,
     ADC.vcEmail As vcEmail,
     CMP.numCompanyId AS numCompanyID,
     DM.numDivisionID As numDivisionID,
     ADC.numContactId AS numContactID,
     CMP.numCompanyRating AS numCompanyRating,
     LD.vcData as vcRating,
     DM.numStatusID AS numStatusID,
     LD1.vcData as Follow,
     DM.numCreatedBy AS numCreatedby, DM.numRecOwner , DM.tintCRMType,
     case when DM.tintCRMType = 1 then 'Prospects' when DM.tintCRMType = 2 then 'Accounts' end as AccountType
   FROM  CompanyInfo CMP
   join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId
   join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID
   left join Listdetails LD on LD.numListItemID = CMP.numCompanyRating
   left join Listdetails LD1 on LD1.numListItemID = DM.numFollowUpStatus
   join tt_TEMPTABLE T on cast(NULLIF(T.numDivisionID,'') as NUMERIC(18,0)) = DM.numDivisionID
   WHERE coalesce(ADC.bitPrimaryContact,false) = true and ID > v_firstRec and ID < v_lastRec order by ID;                                 
   RETURN;
END; $$;


