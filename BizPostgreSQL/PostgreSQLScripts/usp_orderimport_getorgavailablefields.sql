-- Stored procedure definition script USP_OrderImport_GetOrgAvailableFields for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OrderImport_GetOrgAvailableFields(v_numDomainID NUMERIC(18,0)
	,v_tintImportType NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numFieldID
		,vcFieldName
		,intSectionID
		,false AS Custom
   FROM
   View_DynamicDefaultColumns
   WHERE
   numFormId = v_tintImportType
   AND coalesce(bitDeleted,false) = false
   AND numDomainID = v_numDomainID
   AND intSectionID = 1 -- Organization Fields
   ORDER BY
   vcFieldName;
END; $$;












