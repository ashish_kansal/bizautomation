CREATE OR REPLACE FUNCTION usp_getDocumentsContCmpOpp(v_numContactId NUMERIC DEFAULT 0,
v_numDivisionId NUMERIC DEFAULT 0,
v_numDocCategory NUMERIC DEFAULT 0,
v_byte NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byte = 0 then

      open SWV_RefCur for
      select distinct numGenericDocID,
  VcFileName ,
  VcDocName
      from GenericDocuments
      where numRecID = v_numContactId or numRecID = v_numDivisionId;
   end if;
   if v_byte = 1 then

      open SWV_RefCur for
      select distinct numGenericDocID,
  VcFileName ,
  VcDocName
      from GenericDocuments
      where numRecID in(select numOppId from OpportunityMaster where numContactId = v_numContactId)
      and numDocCategory = v_numDocCategory;
   end if;
   RETURN;
END; $$;


