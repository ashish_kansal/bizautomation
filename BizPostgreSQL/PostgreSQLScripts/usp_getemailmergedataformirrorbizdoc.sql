-- FUNCTION: public.usp_getemailmergedataformirrorbizdoc(numeric, smallint, numeric, numeric, integer, refcursor)

-- DROP FUNCTION public.usp_getemailmergedataformirrorbizdoc(numeric, smallint, numeric, numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getemailmergedataformirrorbizdoc(
	v_numreferenceid numeric DEFAULT 0,
	v_numreferencetype smallint DEFAULT NULL::smallint,
	v_numdomainid numeric DEFAULT NULL::numeric,
	v_numusercntid numeric DEFAULT NULL::numeric,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_numBizDocType  NUMERIC;
   v_numBizDocTempID  NUMERIC(9,0);
   v_numDivisionID  NUMERIC(9,0);
BEGIN
-- This function was converted on Sat Apr 10 16:45:45 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF v_numReferenceType = 1 then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Order' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 2
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Order' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 3
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Opportunity' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 4
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Opportunity' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 5
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'RMA' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 6
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'RMA' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 7
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Sales Credit Memo' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 8
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Purchase Credit Memo' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 9
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Refund Receipt' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 10
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Credit Memo' AND constFlag = TRUE;
   ELSEIF v_numReferenceType = 11
   then
      select   numListItemID INTO v_numBizDocType FROM Listdetails WHERE vcData = 'Invoice' AND constFlag = TRUE;
   end if;

   IF v_numReferenceType = 1 OR v_numReferenceType = 2 OR v_numReferenceType = 3 OR v_numReferenceType = 4 then
	
      open SWV_RefCur for
      SELECT ACI.vcFirstName AS ContactFirstName,
                ACI.vcLastname AS ContactLastName,
				CMP.vcCompanyName AS OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
      FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainId = Mst.numDomainId
      LEFT JOIN DivisionMaster DM ON DM.numDivisionID = Mst.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = Mst.numContactId
      WHERE  Mst.numOppId = v_numReferenceID AND Mst.numDomainId = v_numDomainId;
   ELSEIF v_numReferenceType = 5 OR v_numReferenceType = 6 OR v_numReferenceType = 7 OR v_numReferenceType = 8
   OR v_numReferenceType = 9 OR v_numReferenceType = 10
   then
      select   numDivisionId, (CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 OR v_numReferenceType = 9 OR v_numReferenceType = 10 THEN numRMATempID ELSE numBizdocTempID END) INTO v_numDivisionID,v_numBizDocTempID FROM    ReturnHeader RH WHERE   RH.numReturnHeaderID = v_numReferenceID;
      open SWV_RefCur for
      SELECT	ACI.vcFirstName AS ContactFirstName,
                ACI.vcLastname AS ContactLastName,
				CMP.vcCompanyName AS OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
      FROM    ReturnHeader RH
      JOIN Domain D ON D.numDomainId = RH.numDomainID
      JOIN DivisionMaster DM ON DM.numDivisionID = RH.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId
      AND CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = RH.numContactID
      WHERE   RH.numReturnHeaderID = v_numReferenceID;
   ELSEIF v_numReferenceType = 11
   then
	
      open SWV_RefCur for
      SELECT	ACI.vcFirstName AS ContactFirstName,
				ACI.vcLastname AS ContactLastName,
				CMP.vcCompanyName AS OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
      FROM OpportunityMaster Mst JOIN Domain D ON D.numDomainId = Mst.numDomainId
      LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppId = OBD.numoppid
      LEFT JOIN DivisionMaster DM ON DM.numDivisionID = Mst.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = Mst.numContactId
      WHERE  OBD.numOppBizDocsId = v_numReferenceID AND  Mst.numDomainId = v_numDomainId;
      v_numReferenceType := 1;
      SELECT  numoppid INTO v_numReferenceID FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numReferenceID     LIMIT 1;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getemailmergedataformirrorbizdoc(numeric, smallint, numeric, numeric, integer, refcursor)
    OWNER TO postgres;
