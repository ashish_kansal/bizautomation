-- Stored procedure definition script USP_GetMultiPL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMultiPL(v_numParentDomainID NUMERIC(9,0),                        
v_dtFromDate TIMESTAMP,                      
v_dtToDate TIMESTAMP,
v_numSubscriberId INTEGER ,
v_Rollup INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFinYear  INTEGER;
   v_dtFinYearFrom  TIMESTAMP;

--set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numParentDomainID);
--
--set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numParentDomainID);

BEGIN
   drop table IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numDomainId NUMERIC(9,0),
      vcDomainCode VARCHAR(50),
      vcDomainName VARCHAR(150), 
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );


   drop table IF EXISTS tt_TLGROUP CASCADE;
   CREATE TEMPORARY TABLE tt_TLGROUP 
   (
      numDomainId NUMERIC(9,0),
      vcDomainCode VARCHAR(50),
      vcDomainName VARCHAR(150), 
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );


   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numDomainId,dn.vcDomainCode,dn.vcDomainName,COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND numDomainId = COA.numDomainId) and
      numDomainID = v_numParentDomainID and
      CAO.numAccountId = COA.numAccountId),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = COA.numDomainId AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.numAccountId = COA.numAccountId AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = COA.numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = COA.numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM Chart_Of_Accounts COA,
	Domain dn,
	(SELECT * FROM Domain DNN WHERE numSubscriberID = v_numSubscriberId) DNN
   WHERE COA.numDomainId = DNN.numDomainId AND
   DNN.numDomainId = dn.numDomainId AND
   dn.numDomainId = v_numParentDomainID AND
   dn.numSubscriberID = v_numSubscriberId
   union
   SELECT COA.numDomainId,dn.vcDomainCode,dn.vcDomainName,COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND numDomainId = COA.numDomainId) and
      numDomainID = v_numParentDomainID and
      CAO.numAccountId = COA.numAccountId),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = COA.numDomainId AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.numAccountId = COA.numAccountId AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = COA.numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = COA.numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM Chart_Of_Accounts COA,
	Domain dn,
	(SELECT * FROM Domain DNN WHERE DNN.numSubscriberID = v_numSubscriberId) DNN
   WHERE COA.numDomainId = DNN.numDomainId AND
   DNN.vcDomainCode ilike dn.vcDomainCode || '%'  AND
   dn.numParentDomainID = v_numParentDomainID  and
   dn.numSubscriberID = v_numSubscriberId;

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------


   INSERT INTO  tt_TLGROUP
   SELECT AD.numDomainID,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeID,CAST(AD.vcAccountType AS VARCHAR(250)),numParentID,CAST('' AS VARCHAR(250)),AD.vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO,Chart_Of_Accounts COA WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND FinancialYear.numDomainId = AD.numDomainID)
      AND CAO.numDomainID = AD.numDomainID AND
      CAO.numAccountId = COA.numAccountId  AND
      COA.vcAccountCode ilike AD.vcAccountCode || '%'),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = AD.numDomainID AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),
   0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode  || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM AccountTypeDetail AD,
	Domain dn,
	(select * from Domain DNN where DNN.numSubscriberID = v_numSubscriberId) DNN
   WHERE AD.numDomainID = DNN.numDomainId AND
   DNN.numDomainId = dn.numDomainId AND
   dn.numDomainId = v_numParentDomainID AND
   dn.numSubscriberID = v_numSubscriberId
   UNION
   SELECT AD.numDomainID,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeID,CAST(AD.vcAccountType AS VARCHAR(250)),numParentID,'',AD.vcAccountCode,
coalesce((SELECT sum(coalesce(monOpening,0)) from ChartAccountOpening CAO,Chart_Of_Accounts COA WHERE
      numFinYearId in(SELECT numFinYearId FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
         dtPeriodTo >= v_dtFromDate AND FinancialYear.numDomainId = AD.numDomainID)
      AND CAO.numDomainID = AD.numDomainID AND
      CAO.numAccountId = COA.numAccountId  AND
      COA.vcAccountCode ilike AD.vcAccountCode || '%'),0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ,FinancialYear FN
      WHERE VJ.numDomainID = AD.numDomainID AND
      FN.numDomainId = VJ.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      FN.dtPeriodFrom <= v_dtFromDate AND
      FN.dtPeriodTo >= v_dtFromDate and
      datEntry_Date BETWEEN FN.dtPeriodFrom  AND  v_dtFromDate+INTERVAL '-1 day'),
   0) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = AD.numDomainID AND
      VJ.COAvcAccountCode ilike AD.vcAccountCode || '%' AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT
   FROM AccountTypeDetail AD,
	Domain dn,
	(select * from Domain DNN where DNN.numSubscriberID = v_numSubscriberId) DNN
   WHERE AD.numDomainID = DNN.numDomainId AND
   DNN.vcDomainCode ilike dn.vcDomainCode || '%'  AND
   dn.numParentDomainID = v_numParentDomainID  AND
   dn.numSubscriberID = v_numSubscriberId;







--select * from #PLSummary;
--
--select * from #TLGroup


   if v_Rollup = 0 then
	
      open SWV_RefCur for
      select vcDomainCode, vcDomainName from tt_TLGROUP
      where (vcAccountCode ilike '0103%'  or vcAccountCode ilike '0104%')
      group by vcDomainCode,vcDomainName;
   ELSEIF v_Rollup = 1
   then
	
      open SWV_RefCur for
      Select '00' AS vcDomainCode,'Roll-Up' AS vcDomainName;
   end if;

   open SWV_RefCur2 for
   select
   CASE
   WHEN LENGTH(vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(vcAccountCode) -4) || vcAccountCode
   ELSE '<b>' || vcAccountCode ||  '</b>'
   END AS vcAccountCode,
 CASE
   WHEN LENGTH(vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(vcAccountCode) -4) || vcAccountName
   ELSE '<b>' || vcAccountName || '</b>'
   END  as vcAccountName,
vcAccountName as vcAccountNamePK,
vcAccountCode as vcAccountCodePK
   from tt_TLGROUP
   where LENGTH(vcAccountCode) > 2 AND
(vcAccountCode ilike '0103%'  or vcAccountCode ilike '0104%')
   group by vcAccountCode,vcAccountName;
   if v_Rollup = 0 then
	
      open SWV_RefCur3 for
      SELECT vcDomainCode,vcDomainName,vcAccountName,vcAccountCode,sum(Opening)+sum(Debit) -Sum(Credit) as Balance
      FROM tt_TLGROUP where LENGTH(vcAccountCode) > 2 AND
		(vcAccountCode ilike '0103%'  or vcAccountCode ilike '0104%')
      GROUP BY vcDomainCode,vcDomainName,vcAccountName,vcAccountCode;
   ELSEIF v_Rollup = 1
   then
	
      open SWV_RefCur3 for
      SELECT '00' as vcDomainCode,'Roll-Up' as vcDomainName ,vcAccountName,vcAccountCode,sum(Opening)+sum(Debit) -Sum(Credit) as Balance
      FROM tt_TLGROUP where LENGTH(vcAccountCode) > 2 AND
		(vcAccountCode ilike '0103%'  or vcAccountCode ilike '0104%')
      GROUP BY vcAccountName,vcAccountCode;
   end if;
   drop table IF EXISTS tt_PLSUMMARY CASCADE;
   RETURN;
END; $$;


-- Created by Sojan        
-- [USP_GetChartAcntDetails] @numParentDomainID=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            


