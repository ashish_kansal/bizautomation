CREATE OR REPLACE FUNCTION usp_GetTop10DealsInPipeline(     
v_numDomainID NUMERIC,                
 v_dtDateFrom TIMESTAMP,                
 v_dtDateTo TIMESTAMP,                
 v_numTerID NUMERIC DEFAULT 0,                
 v_numUserCntID NUMERIC DEFAULT 0,                
 v_tintRights SMALLINT DEFAULT 3,                
 v_intType NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 3 then --All Records                
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
    GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId ,fn_GetContactName(OM.numrecowner) as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         ORDER BY Amount DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
     GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId    ,ACI1.vcFirstName || ' ' || ACI1.vcLastname as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         JOIN  AdditionalContactsInformation ACI1
         ON ACI1.numContactId = OM.numrecowner
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND ACI1.numTeam is not null
         AND ACI1.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         ORDER BY Amount DESC LIMIT 10;
      end if;
   end if;                
                
   IF v_tintRights = 2 then --Territory Records                
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
    GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId      ,fn_GetContactName(OM.numrecowner) as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         ORDER BY Amount DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
     GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId,ACI1.vcFirstName || ' ' || ACI1.vcLastname as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         JOIN  AdditionalContactsInformation ACI1
         ON ACI1.numContactId = OM.numrecowner
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         ORDER BY Amount DESC LIMIT 10;
      end if;
   end if;                
                
   IF v_tintRights = 1 then --Owner Records                
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
    GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId,fn_GetContactName(OM.numrecowner) as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         ORDER BY Amount DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    ACI.vcFirstName || ' ' || ACI.vcLastname As Contact,
     GetOppLstMileStonePercentage(OM.numOppId) as Mile, OM.monPAmount As Amount,
    OM.numCreatedBy, OM.numPClosingPercent, OM.numOppId  ,ACI1.vcFirstName || ' ' || ACI1.vcLastname as vcUserName
         FROM OpportunityMaster OM
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CI
         ON CI.numCompanyId = DM.numCompanyID
         JOIN  AdditionalContactsInformation ACI
         ON ACI.numContactId = OM.numContactId
         JOIN  AdditionalContactsInformation ACI1
         ON ACI1.numContactId = OM.numrecowner
         WHERE OM.tintoppstatus = 0  and OM.tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         AND ACI1.numTeam is not null
         AND ACI1.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         ORDER BY Amount DESC LIMIT 10;
      end if;
   end if;
   RETURN;
END; $$;


