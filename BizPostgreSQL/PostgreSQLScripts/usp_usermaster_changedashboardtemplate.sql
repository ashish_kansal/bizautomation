-- Stored procedure definition script USP_UserMaster_ChangeDashboardTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UserMaster_ChangeDashboardTemplate(v_numDomainID NUMERIC(18,0)
	,v_numUserID NUMERIC(18,0)
	,v_numTemplateID NUMERIC(18,0)
	,v_vcDashboardTemplateIDs VARCHAR(250) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcDashboardTemplateIDs IS NULL OR v_vcDashboardTemplateIDs = '' then
	
      UPDATE
      UserMaster
      SET
      numDashboardTemplateID = v_numTemplateID
      WHERE
      numUserId = v_numUserID;
   ELSE
      UPDATE
      UserMaster
      SET
      numDashboardTemplateID = v_numTemplateID,vcDashboardTemplateIDs = v_vcDashboardTemplateIDs
      WHERE
      numUserId = v_numUserID;
   end if;
	
   IF coalesce(v_numTemplateID,0) > 0 and (v_vcDashboardTemplateIDs IS NOT NULL OR v_vcDashboardTemplateIDs <> '') then
	
		-- DELETE OLD DASHBOARD CONFIGURATION FOR USER BECAUSE IT MAY BE CHANGED FROM LAST ASSIGNMENT 
      DELETE FROM ReportDashboard WHERE numDomainID = v_numDomainID AND numDashboardTemplateID = v_numTemplateID AND numUserCntID = coalesce((SELECT numUserDetailId FROM UserMaster WHERE numUserId = v_numUserID),
      0);

		-- INSERT LATEST CONFIGURATION OF DASHBOARD TEMPLATE
      INSERT INTO ReportDashboard(numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID)
      SELECT
      DashboardTemplateReports.numDomainID
			,DashboardTemplateReports.numReportID
			,coalesce((SELECT numUserDetailId FROM UserMaster WHERE numUserId = v_numUserID),
      0)
			,DashboardTemplateReports.tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
      FROM
      DashboardTemplateReports
      INNER JOIN
      ReportListMaster
      ON
      DashboardTemplateReports.numReportID = ReportListMaster.numReportID
      AND (ReportListMaster.numDomainID = v_numDomainID OR bitDefault = true)
      WHERE
      DashboardTemplateReports.numDomainID = v_numDomainID
      AND numDashboardTemplateID = v_numTemplateID;
   end if;
   RETURN;
END; $$;


