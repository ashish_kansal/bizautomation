-- Stored procedure definition script USP_OPPDetailsDTLPL for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPDetailsDTLPL(v_OpportunityId        NUMERIC(9,0)  DEFAULT NULL,
               v_numDomainID          NUMERIC(9,0) DEFAULT NULL,
               v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintDecimalPoints  SMALLINT;
BEGIN
   select   coalesce(tintDecimalPoints,0) INTO v_tintDecimalPoints FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   open SWV_RefCur for SELECT Opp.numOppId,numCampainID,
		 CAMP.vcCampaignName AS numCampainIDName,
         fn_GetContactName(Opp.numContactId) AS numContactIdName,
         coalesce(ADC.vcEmail,'') AS vcEmail,
         coalesce(ADC.numPhone,'') AS Phone,
         coalesce(ADC.numPhoneExtension,'') AS PhoneExtension,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionId,
         tintopptype,
         bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS bintAccountClosingDate,
         fn_GetOpportunitySourceValue(coalesce(Opp.tintSource,0),coalesce(Opp.tintSourceType,0)::SMALLINT,Opp.numDomainId) AS tintSourceName,
          tintSource,
         Opp.vcpOppName,
         intPEstimatedCloseDate,
		 FormatedDateFromDate(Opp.bintOppToOrder+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS bintOppToOrder,
         GetDealAmount(v_OpportunityId,TIMEZONE('UTC',now()),0::NUMERIC) AS CalAmount,
         --monPAmount,
         CAST(coalesce(monPAmount,0) AS DECIMAL(10,2)) AS monPAmount,
		lngPConclAnalysis,
         cast((SELECT vcData
      FROM   Listdetails
      WHERE  numListItemID = lngPConclAnalysis) as VARCHAR(255)) AS lngPConclAnalysisName,
         cast((SELECT vcData
      FROM   Listdetails
      WHERE  numListItemID = numSalesOrPurType) as VARCHAR(255)) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         cast(C2.vcCompanyName as VARCHAR(255)),
         D2.tintCRMType,
         Opp.tintActive,
         fn_GetContactName(Opp.numCreatedBy)
   || ' '
   || CAST(Opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy,
         fn_GetContactName(Opp.numModifiedBy)
   || ' '
   || CAST(Opp.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
         fn_GetContactName(Opp.numrecowner) AS RecordOwner,
         Opp.numrecowner,
         cast(coalesce(tintshipped,0) as SMALLINT) AS tintshipped,
         cast(coalesce(tintoppstatus,0) as SMALLINT) AS tintOppStatus,
         GetOppStatus(Opp.numOppId) AS vcDealStatus,
         OpportunityLinkedItems(v_OpportunityId)+(SELECT COUNT(*) FROM  Communication Comm JOIN Correspondence Corr ON Comm.numCommId = Corr.numCommID
      WHERE Comm.numDomainID = Opp.numDomainId AND Corr.numDomainID = Comm.numDomainID AND Corr.tintCorrType IN(1,2,3,4) AND Corr.numOpenRecordID = Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
      || ' '
      || A.vcLastname
      FROM   AdditionalContactsInformation A
      WHERE  A.numContactId = Opp.numassignedto) AS numAssignedToName,
		  Opp.numassignedto,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
      FROM   GenericDocuments
      WHERE  numRecID = v_OpportunityId
      AND vcDocumentSection = 'O') AS DocumentCount,
         CASE
   WHEN tintoppstatus = 1 THEN 100
   ELSE coalesce((SELECT cast(SUM(tintPercentage) as NUMERIC(18,0))
         FROM   OpportunityStageDetails
         WHERE  numoppid = v_OpportunityId
         AND bitstagecompleted = true),
      0)
   END AS TProgress,
         (SELECT  coalesce(varRecurringTemplateName,'')
      FROM   RecurringTemplate
      WHERE  numRecurringId = OPR.numRecurringId LIMIT 1) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
          coalesce(Opp.fltExchangeRate,0) AS fltExchangeRate,
          coalesce(C.chrCurrency,'')  AS chrCurrency,
         coalesce(C.numCurrencyID,0) AS numCurrencyID,
         (SELECT COUNT(*) FROM OpportunityLinking OL WHERE OL.numParentOppID = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM RecurringTransactionReport RTR WHERE RTR.numRecTranSeedId = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM ShippingReport WHERE numOppBizDocId IN(SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numoppid = v_OpportunityId)) AS ShippingReportCount,
         coalesce(Opp.numStatus,0) AS numStatus,
         cast((SELECT vcData
      FROM   Listdetails
      WHERE  numListItemID = Opp.numStatus) as VARCHAR(255)) AS numStatusName,
          (SELECT SUBSTR((SELECT ',' ||  A.vcFirstName || ' ' || A.vcLastname || CASE WHEN coalesce(SR.numContactType,0) > 0 THEN '(' || fn_GetListItemName(SR.numContactType) || ')' ELSE '' END
         FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
         JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
         WHERE SR.numDomainID = Opp.numDomainId AND SR.numModuleID = 3 AND SR.numRecordID = Opp.numOppId
         AND UM.numDomainID = Opp.numDomainId and UM.numDomainID = A.numDomainID),2,200000)) AS numShareWith,
		coalesce((SELECT SUM(coalesce(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = v_OpportunityId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) AS monDealAmount,
		coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = v_OpportunityId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) AS monAmountPaid,
		coalesce(Opp.tintSourceType,0)  AS tintSourceType,
		coalesce(Opp.tintTaxOperator,0) AS tintTaxOperator,
             coalesce(Opp.intBillingDays,0) AS numBillingDays,coalesce(Opp.bitInterestType,false) AS bitInterestType,
             coalesce(Opp.fltInterest,0) AS fltInterest,  coalesce(Opp.fltDiscount,0) AS fltDiscount,
             coalesce(Opp.bitDiscountType,false) AS bitDiscountType,coalesce(Opp.bitBillingTerms,false) AS bitBillingTerms,
             coalesce(Opp.numDiscountAcntType,0) AS numDiscountAcntType,
              fn_getOPPAddress(Opp.numOppId,v_numDomainID,1::SMALLINT) AS BillingAddress,
            fn_getOPPAddress(Opp.numOppId,v_numDomainID,2::SMALLINT) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 COALESCE((SELECT  string_agg((SELECT vcShipFieldValue FROM ShippingFieldValues
            WHERE numDomainID = Opp.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = OBD.numShipVia  AND vcFieldName = 'Tracking URL')) || '#^#' || RTRIM(LTRIM(vcTrackingNo)),'$^$') 
         FROM OpportunityBizDocs OBD WHERE OBD.numoppid = Opp.numOppId and coalesce(OBD.numShipVia,0) <> 0),'') AS  vcTrackingNo,
			 COALESCE((SELECT
      string_agg(OpportunityBizDocs.vcBizDocID || ': ' || vcTrackingDetail,'<br/>') 
      FROM
      ShippingReport
      INNER JOIN
      OpportunityBizDocs
      ON
      ShippingReport.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
      WHERE
      ShippingReport.numOppID = Opp.numOppId
      AND coalesce(ShippingReport.vcTrackingDetail,'') <> ''),'') AS vcTrackingDetail,
            coalesce(numPercentageComplete,0) AS numPercentageComplete,
            GetListIemName(coalesce(numPercentageComplete,0)) AS PercentageCompleteName,
            coalesce(Opp.numBusinessProcessID,0) AS numBusinessProcessID,
            CASE WHEN Opp.tintopptype = 1 THEN CheckOrderInventoryStatus(Opp.numOppId,Opp.numDomainId,1::SMALLINT) ELSE '' END AS vcInventoryStatus
            ,coalesce(bitUseShippersAccountNo,false) AS bitUseShippersAccountNo
			, coalesce((SELECT DMSA.vcAccountNumber FROm DivisionMasterShippingAccount DMSA WHERE DMSA.numDivisionID = D2.numDivisionID AND DMSA.numShipViaID = Opp.intUsedShippingCompany),'') AS vcShippersAccountNo ,
			cast((SELECT SUM((coalesce(monTotAmtBefDiscount,coalesce(monTotAmount,0)) -coalesce(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = v_OpportunityId AND bitAuthoritativeBizDocs = 1)) as VARCHAR(255)) AS numTotalDiscount,
			coalesce(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			coalesce(Opp.bitRecurred,false) AS bitRecurred,
			(CASE
   WHEN Opp.tintopptype = 1 AND Opp.tintoppstatus = 1
   THEN
      GetOrderShipReceiveStatus(Opp.numOppId,1::NUMERIC(18,0),Opp.tintshipped,v_tintDecimalPoints)
   ELSE
      ''
   END) AS vcOrderedShipped,
			(CASE
   WHEN Opp.tintopptype = 2 AND Opp.tintoppstatus = 1
   THEN
      GetOrderShipReceiveStatus(Opp.numOppId,2::NUMERIC(18,0),Opp.tintshipped,v_tintDecimalPoints)
   ELSE
      ''
   END) AS vcOrderedReceived,
		coalesce(C2.numCompanyType,0) AS numCompanyType,
		coalesce(C2.vcProfile,0) AS vcProfile,
		coalesce(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode || '-' || C3.vcCompanyName as numPartner,
		coalesce(Opp.numPartner,0) AS numPartnerId,
		coalesce(Opp.bitIsInitalSalesOrder,false) AS bitIsInitalSalesOrder,
		coalesce(Opp.dtReleaseDate,'1900-01-01 00:00:00.000') AS dtReleaseDate,
		coalesce(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) AS numReleaseStatusName,
		coalesce(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		CASE WHEN coalesce(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE coalesce((SELECT vcData FROM Listdetails WHERE numListID = 82 AND numListItemID = coalesce(Opp.intUsedShippingCompany,0)),
      '') END AS vcShippingCompany,
		coalesce(Opp.numShipmentMethod,0) AS numShipmentMethod
		,CASE WHEN coalesce(Opp.numShipmentMethod,0) = 0 THEN '' ELSE coalesce((SELECT vcData FROM Listdetails WHERE numListID = 338 AND numListItemID = coalesce(Opp.numShipmentMethod,0)),
      '') END AS vcShipmentMethod
		,coalesce(Opp."vcCustomerPO#",'') AS "vcCustomerPO#",
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
   WHEN 2 THEN '850 Acknowledged'
   WHEN 3 THEN '940 Sent'
   WHEN 4 THEN '940 Acknowledged'
   WHEN 5 THEN '856 Received'
   WHEN 6 THEN '856 Acknowledged'
   WHEN 7 THEN '856 Sent'
   WHEN 8 THEN 'Send 940 Failed'
   WHEN 9 THEN 'Send 856 & 810 Failed'
   WHEN 11 THEN '850 Partially Created'
   WHEN 12 THEN '850 SO Created'
   ELSE ''
   END,'  ','<a onclick="return Open3PLEDIHistory(',
   Opp.numOppId,')"><img src="../images/GLReport.png" border="0"></a>') AS tintEDIStatusName,
		coalesce(Opp.numShippingService,0) AS numShippingService,
		coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE
   WHEN vcSignatureType = '0' THEN 'Service Default'
   WHEN vcSignatureType = '1' THEN 'Adult Signature Required'
   WHEN vcSignatureType = '2' THEN 'Direct Signature'
   WHEN vcSignatureType = '3' THEN 'InDirect Signature'
   WHEN vcSignatureType = '4' THEN 'No Signature Required'
   ELSE ''
   END AS vcSignatureType
		,fn_getOPPState(Opp.numOppId,Opp.numDomainId,2::SMALLINT) AS vcShipState
		,Opp.dtExpectedDate
		,coalesce(Opp.numProjectID,0) AS numProjectID
		,coalesce(PM.vcProjectName,'') AS numProjectIDName
		,coalesce(Opp.intReqPOApproved,0) AS intReqPOApproved
		,CASE WHEN coalesce(Opp.intReqPOApproved,0) = 0 THEN 'Pending Approval' WHEN coalesce(Opp.intReqPOApproved,0) = 1 THEN 'Approved'  WHEN coalesce(Opp.intReqPOApproved,0) = 2 THEN 'Rejected' ELSE '' END AS vcReqPOApproved,
		coalesce(D2.intdropship,0) as intDropShip,
CASE WHEN coalesce(D2.intdropship,0) = 1 THEN 'Not Available'
   WHEN coalesce(D2.intdropship,0) = 2 THEN 'Blank Available'
   WHEN coalesce(D2.intdropship,0) = 3 THEN 'Vendor Label'
   WHEN coalesce(D2.intdropship,0) = 4 THEN 'Private Label'
   ELSE 'Select One'
   END AS vcDropShip
   FROM   OpportunityMaster Opp
   JOIN DivisionMaster D2 ON Opp.numDivisionId = D2.numDivisionID
   JOIN CompanyInfo C2 ON C2.numCompanyId = D2.numCompanyID
   LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
   LEFT JOIN DivisionMaster D3 ON Opp.numPartner = D3.numDivisionID
   LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
   LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
   LEFT JOIN CompanyInfo C3 ON C3.numCompanyId = D3.numCompanyID
   LEFT JOIN ProjectsMaster PM ON Opp.numProjectID = PM.numProId
   LEFT JOIN(SELECT  vcpOppName,
                                 numOppId,
                                 numChildOppID,
                                 vcSource,numParentOppID ,OpportunityMaster.numDivisionId AS numParentDivisionID
      FROM   OpportunityLinking
      left JOIN OpportunityMaster
      ON numOppId = numParentOppID
      WHERE  numChildOppID = v_OpportunityId LIMIT 1) Link
   ON Link.numChildOppID = Opp.numOppId
   LEFT OUTER JOIN Currency C
   ON C.numCurrencyID = Opp.numCurrencyID
   LEFT OUTER JOIN OpportunityRecurring OPR
   ON OPR.numOppId = Opp.numOppId
   LEFT JOIN AddressDetails AD --Billing add
   ON AD.numDomainID = D2.numDomainID AND AD.numRecordID = D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true
   LEFT JOIN AddressDetails AD2--Shipping address
   ON AD2.numDomainID = D2.numDomainID AND AD2.numRecordID = D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
   LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID
   WHERE  Opp.numOppId = v_OpportunityId
   AND Opp.numDomainId = v_numDomainID;
END; $$;
