-- Stored procedure definition script USP_GetAlertIDs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertIDs(v_numAlertID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from AlertDTL where numAlertID = v_numAlertID and numAlertDTLid In(50,51);
END; $$;













