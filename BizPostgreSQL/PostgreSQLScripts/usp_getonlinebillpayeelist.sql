-- Stored procedure definition script USP_GetOnlineBillPayeeList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOnlineBillPayeeList(v_numDomainID NUMERIC(18,0),
	v_numBankDetailId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT
   numPayeeDetailId,
	vcPayeeFIId,
	vcPayeeFIListID,
	vcPayeeName,
	vcAccount,
	vcAddress1,
	vcAddress2,
	vcAddress3,
	vcCity,
	vcState,
	vcPostalCode,
	vcPhone,
	vcCountry
   FROM
   OnlineBillPayeeDetails OBP
   INNER JOIN BankDetails BD ON BD.numBankDetailID = OBP.numBankDetailId
   WHERE
   BD.numDomainID = v_numDomainID AND BD.numBankDetailID = v_numBankDetailId
   AND BD.bitIsActive = true;
END; $$; 
