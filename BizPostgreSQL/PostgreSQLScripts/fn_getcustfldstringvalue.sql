DROP FUNCTION IF EXISTS fn_GetCustFldStringValue;

CREATE OR REPLACE FUNCTION fn_GetCustFldStringValue(v_numFldId NUMERIC(18,0),
	v_numRecordID NUMERIC(18,0),
	v_vcFldValue TEXT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT;  

   v_fld_type  VARCHAR(20);
BEGIN
   select   fld_type INTO v_fld_type FROM CFW_Fld_Master WHERE Fld_id = v_numFldId;

   IF v_fld_type = 'SelectBox' AND SWF_isnumeric(v_vcFldValue) = true then
	
      SELECT vcData INTO v_vcValue FROM Listdetails WHERE numListItemID = CAST(v_vcFldValue AS NUMERIC);
   ELSEIF v_fld_type = 'CheckBoxList' AND COALESCE(v_vcFldValue,'') <> ''
   then
	
      v_vcValue := coalesce((SELECT string_agg(vcData,',')
      FROM
      Listdetails
      WHERE
      numListItemID IN (SELECT (CASE WHEN SWF_isnumeric(Id) = true THEN coalesce(CAST(Id AS NUMERIC),0)  ELSE -1 END) FROM regexp_split_to_table(v_vcFldValue,',') AS Id)),'');
   ELSEIF v_fld_type = 'CheckBox'
   then
	
      v_vcValue :=(CASE WHEN cast(NULLIF(coalesce(v_vcFldValue,cast(0 as TEXT)),'') as INTEGER) = 1 THEN 'Yes' ELSE 'No' END);
   ELSE
      v_vcValue := v_vcFldValue;
   end if;

   IF v_vcValue IS NULL then
      v_vcValue := '';
   end if;  

   RETURN coalesce(v_vcValue,'');
END; $$;

