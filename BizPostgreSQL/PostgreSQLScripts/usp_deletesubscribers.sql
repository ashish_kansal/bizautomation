-- Stored procedure definition script USP_DeleteSubscribers for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSubscribers(v_numSubscriberID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Subscribers set bitDeleted = true where numSubscriberID = v_numSubscriberID;
   RETURN;
END; $$;


