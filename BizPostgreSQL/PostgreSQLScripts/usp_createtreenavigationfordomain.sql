-- Stored procedure definition script USP_CreateTreeNavigationForDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CreateTreeNavigationForDomain(v_numDomainID NUMERIC,
v_numPageNavID NUMERIC DEFAULT 0 -- Suppy this parameter when you want to give tree node permissoin to all domain and all user groups by default.
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN
      IF coalesce(v_numPageNavID,0) = 0 then
    
         DELETE FROM TreeNavigationAuthorization WHERE numDomainID = v_numDomainID;
         DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDATA AS
            SELECT * FROM(SELECT    T.numTabId,
                            CASE WHEN bitFixed = true
            THEN CASE WHEN EXISTS(SELECT numTabName
                  FROM   TabDefault
                  WHERE  numTabId = T.numTabId
                  AND tintTabType = T.tintTabType AND numDomainID = v_numDomainID)
               THEN(SELECT 
                     numTabName
                     FROM      TabDefault
                     WHERE     numTabId = T.numTabId
                     AND tintTabType = T.tintTabType AND numDomainID = v_numDomainID LIMIT 1)
               ELSE T.numTabName
               END
            ELSE T.numTabName
            END AS numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainId,
                            A.numGroupID
            FROM      TabMaster T
            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
            LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
            WHERE     bitFixed = true
            AND D.numDomainId <> -255
            AND T.tintTabType = 1
            AND D.numDomainId = v_numDomainID
            UNION ALL
            SELECT    -1 AS numTabID,
                            'Administration' AS numTabname,
                            '' AS vcURL,
                            true AS bitFixed,
                            1 AS tintType,
                            '' AS vcImage,
                            D.numDomainId,
                            A.numGroupID
            FROM      Domain D
            JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
            WHERE D.numDomainId = v_numDomainID
            UNION ALL
            SELECT    -3 AS numTabID,
							'Advanced Search' AS numTabname,
							'' AS vcURL,
							true AS bitFixed,
							1 AS tintType,
							'' AS vcImage,
							D.numDomainId,
							A.numGroupID
            FROM      Domain D
            JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId) X;
         INSERT  INTO TreeNavigationAuthorization(numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType)
         SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
         FROM    PageNavigationDTL PND
         JOIN tt_TEMPDATA TD ON TD.numTabID = PND.numTabID
         WHERE TD.numDomainID = v_numDomainID
         UNION ALL
         SELECT  numGroupID AS numGroupID,
						1 AS numTabID,
						coalesce(LD.numListItemID,0) AS numPageNavID,
						true AS bitVisible,
						v_numDomainID AS numDomainID,
						1 AS tintType
         FROM    Listdetails LD
         CROSS JOIN AuthenticationGroupMaster AG
         WHERE   numListID = 27
         AND coalesce(constFlag,false) = true
         AND AG.numDomainID = v_numDomainID
         AND numListItemID NOT IN(293,294,295,297,298,299);
--SELECT * FROM #tempData WHERE numGroupID = 1 ORDER BY numDomainID
         DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
      end if;
	
	------------------------------------------
      IF coalesce(v_numPageNavID,0) > 0 then
	

		
		--Delete all existing tree permission from all domains for selected page
         DELETE FROM TreeNavigationAuthorization WHERE numPageNavID = v_numPageNavID;
         DROP TABLE IF EXISTS tt_TEMPDATA1 CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDATA1 AS
            SELECT    T.numTabId,
                            CASE WHEN bitFixed = true
            THEN CASE WHEN EXISTS(SELECT numTabName
               FROM   TabDefault
               WHERE  numTabId = T.numTabId
               AND tintTabType = T.tintTabType AND numDomainID = D.numDomainId)
               THEN(SELECT 
                  numTabName
                  FROM      TabDefault
                  WHERE     numTabId = T.numTabId
                  AND tintTabType = T.tintTabType AND numDomainID = D.numDomainId LIMIT 1)
               ELSE T.numTabName
               END
            ELSE T.numTabName
            END AS numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainId,
                            A.numGroupID
				  
            FROM      TabMaster T
            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
            LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
            WHERE     bitFixed = true
            AND D.numDomainId > 0
            AND T.tintTabType = 1;
                            
                            

		
--			SELECT * FROM #tempData
		
                            
         INSERT  INTO TreeNavigationAuthorization(numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType)
         SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
         FROM    PageNavigationDTL PND
         JOIN tt_TEMPDATA1 TD ON TD.numTabID = PND.numTabID
         WHERE PND.numPageNavID = v_numPageNavID;
         DROP TABLE IF EXISTS tt_TEMPDATA1 CASCADE;
         open SWV_RefCur for SELECT * FROM TreeNavigationAuthorization WHERE numPageNavID = v_numPageNavID;
      end if;
   END;
   RETURN;
END; $$;    
   

 












