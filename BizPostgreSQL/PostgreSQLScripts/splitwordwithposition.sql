-- Function definition script SplitWordWithPosition for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SplitWordWithPosition(v_IdList VARCHAR(8000),
	v_Delimiter CHAR(1) DEFAULT '/')
RETURNS table
(
   RowNumber INTEGER,
   strItem VARCHAR(1000)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Id  VARCHAR(1000);
   v_Pos  INTEGER;
   v_rownumber  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_SPLITWORDWITHPOSITION_PARSEDLIST CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_SPLITWORDWITHPOSITION_PARSEDLIST
   (
      RowNumber INTEGER,
      strItem VARCHAR(1000)
   );
   v_IdList := LTRIM(RTRIM(v_IdList)) || coalesce(v_Delimiter,'');
   v_Pos := POSITION(v_Delimiter IN v_IdList);
   v_rownumber := 0;
   IF REPLACE(v_IdList,v_Delimiter,'') <> '' then
	
      WHILE v_Pos > 0 LOOP
         v_Id := LTRIM(RTRIM(SUBSTR(v_IdList,1,v_Pos -1)));
         IF v_Id <> '' then
			
            v_rownumber := v_rownumber::bigint+1;
            INSERT INTO tt_SPLITWORDWITHPOSITION_PARSEDLIST(RowNumber,strItem)
				VALUES(v_rownumber ,v_Id);
         end if;
         v_IdList := SUBSTR(v_IdList,length(v_IdList) -(LENGTH(v_IdList) -v_Pos)+1);
         v_Pos := POSITION(v_Delimiter IN v_IdList);
      END LOOP;
   end if;	
   RETURN QUERY (SELECT * FROM tt_SPLITWORDWITHPOSITION_PARSEDLIST);
END; $$;

