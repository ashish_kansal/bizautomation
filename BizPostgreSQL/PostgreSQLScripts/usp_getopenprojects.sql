-- Stored procedure definition script USP_GetOpenProjects for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpenProjects(v_numDomainID NUMERIC(9,0),
    v_numProId NUMERIC(9,0),
    v_tintMode SMALLINT DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
    
      IF v_numProId > 0 then
            
         open SWV_RefCur for
         SELECT  numAccountID AS "numAccountID"
         FROM    ProjectsMaster
         WHERE   numProId = v_numProId
         AND numdomainId = v_numDomainID;
      ELSE
         open SWV_RefCur for
         SELECT  numProId AS "numProId",
                    vcProjectName AS "vcProjectName"
         FROM    ProjectsMaster
         WHERE   numdomainId = v_numDomainID
         AND tintProStatus <> 1
         AND (numDivisionId = v_numDivisionID OR v_numDivisionID = 0)
         AND coalesce(numProjectStatus,0) <> 27492
         ORDER BY vcProjectName,numProId desc;
      end if;
   ELSEIF v_tintMode = 1
   then
	
      open SWV_RefCur for
      SELECT  numProId AS "numProId",
                    vcProjectName AS "vcProjectName"
      FROM    ProjectsMaster
      WHERE   numdomainId = v_numDomainID
      ORDER BY numProId desc;
   end if;
   RETURN;
END; $$; 


