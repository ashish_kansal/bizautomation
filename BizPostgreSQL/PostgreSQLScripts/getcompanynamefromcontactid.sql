DROP FUNCTION IF EXISTS GetCompanyNameFromContactID;

CREATE OR REPLACE FUNCTION GetCompanyNameFromContactID(v_numContactId NUMERIC(9,0),
	v_numDomainId NUMERIC(9,0))
RETURNS VARCHAR(300) LANGUAGE plpgsql
	-- Add the parameters for the function here
	
	-- Declare the return variable here
   AS $$
   DECLARE
   v_Result  VARCHAR(300);

	-- Add the T-SQL statements to compute the return value here

BEGIN
   select   CompanyInfo.vcCompanyName INTO v_Result FROM         DivisionMaster INNER JOIN
   CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId INNER JOIN
   AdditionalContactsInformation ON DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId WHERE     AdditionalContactsInformation.numContactId = v_numContactId and AdditionalContactsInformation.numDomainID = v_numDomainId;
	-- Return the result of the function
   RETURN v_Result;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[getContactAddress]    Script Date: 07/26/2008 18:12:57 ******/

