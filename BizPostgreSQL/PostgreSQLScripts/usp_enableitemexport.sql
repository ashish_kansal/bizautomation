-- Stored procedure definition script USP_EnableItemExport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EnableItemExport(v_numDomainId NUMERIC(9,0),
v_numItemCode NUMERIC(9,0),
v_bitExportToAPI BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Item SET bitExportToAPI = v_bitExportToAPI,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainId;
   RETURN;
END; $$; 
-- USP_ExportDataBackUp 132,1


