CREATE OR REPLACE FUNCTION GetAutoRoutRulesData(v_numRoutID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintEqualTo  SMALLINT;
BEGIN
   select   tintEqualTo INTO v_tintEqualTo from RoutingLeads where numRoutID = v_numRoutID;      
      
      
   open SWV_RefCur for
   select * from RoutingLeads where numRoutID = v_numRoutID;      
      
   open SWV_RefCur2 for
   select numContactId,vcFirstName || ' ' || vcLastname as Name from RoutingLeadDetails
   join AdditionalContactsInformation
   on  numContactId = numEmpID
   where numRoutID = v_numRoutID order by intAssignOrder;      
      
   if v_tintEqualTo = 1 then

      open SWV_RefCur3 for
      select numStateID,vcState from  State
      where numStateID in(select vcValue from  RoutinLeadsValues where numRoutID = v_numRoutID and SWF_IsNumeric(vcValue) = true);
   ELSEIF v_tintEqualTo = 2
   then

      open SWV_RefCur3 for
      select vcValue from RoutinLeadsValues where numRoutID = v_numRoutID;
   ELSEIF v_tintEqualTo = 3
   then

      open SWV_RefCur3 for
      select vcValue,vcData as vcAOIName from RoutinLeadsValues
      join Listdetails
      on numListItemID = vcValue
      where numRoutID = v_numRoutID;
   end if;
   RETURN;
END; $$;
