CREATE OR REPLACE FUNCTION USP_WorkOrder_InsertRecursive(v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numOppChildItemID NUMERIC(18,0),
	v_numOppKitChildItemID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_numWarehouseItemID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0) DEFAULT 0,
	v_numQtyShipped DOUBLE PRECISION DEFAULT NULL,
	v_numParentWOID NUMERIC(18,0) DEFAULT NULL,
	v_bitFromWorkOrderScreen BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWOID  NUMERIC(18,0);
   v_vcInstruction  VARCHAR(2000);
   v_numAssignedTo  NUMERIC(18,0);
   v_bintCompletionDate  TIMESTAMP;
   v_numWarehouseID  NUMERIC(18,0);
   v_numBuildProcessId  NUMERIC(18,0);
   v_dtmStartDate  TIMESTAMP;
   v_dtmEndDate  TIMESTAMP;
   v_Description  VARCHAR(1000);
   v_numNewProcessId  NUMERIC(18,0) DEFAULT 0;

   SWV_RCur REFCURSOR;
BEGIN
   IF NOT EXISTS(SELECT
   numWOId
   FROM
   WorkOrder
   WHERE
   numDomainId = v_numDomainID
   AND numItemCode = v_numItemCode
   AND numParentWOID = v_numParentWOID) then
      select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
      IF(SELECT
      COUNT(*)
      FROM
      Item
      INNER JOIN
      ItemDetails Dtl
      ON
      numChildItemID = numItemCode
      WHERE
      numItemKitID = v_numItemCode
      AND Item.charItemType = 'P'
      AND NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = Dtl.numChildItemID AND numWareHouseID = v_numWarehouseID)) > 0 then
		
         RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         RETURN;
      end if;
      select   coalesce(Item.numBusinessProcessId,0), coalesce(Sales_process_List_Master.numBuildManager,0) INTO v_numBuildProcessId,v_numAssignedTo FROM
      Item
      INNER JOIN
      Sales_process_List_Master
      ON
      Item.numBusinessProcessId = Sales_process_List_Master.Slp_Id WHERE
      numItemCode = v_numItemCode;
      select   vcInstruction, bintCompliationDate, dtmStartDate, dtmEndDate INTO v_vcInstruction,v_bintCompletionDate,v_dtmStartDate,v_dtmEndDate FROM
      WorkOrder WHERE
      numWOId = v_numParentWOID;
      INSERT INTO WorkOrder(numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainId,numWOStatus,numOppId,numOppItemID,numOppChildItemID,numOppKitChildItemID,numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate,numBuildProcessId,dtmStartDate,dtmEndDate)
		VALUES(v_numItemCode,v_numQty,v_numWarehouseItemID,v_numUserCntID,TIMEZONE('UTC',now()),v_numDomainID,0,v_numOppID,v_numOppItemID,v_numOppChildItemID,v_numOppKitChildItemID,v_numParentWOID,v_vcInstruction,v_numAssignedTo,v_bintCompletionDate,v_numBuildProcessId,v_dtmStartDate,v_dtmEndDate);
		
      v_numWOID := CURRVAL('WorkOrder_seq');
      PERFORM USP_UpdateNameTemplateValueForWorkOrder(3::SMALLINT,v_numDomainID,v_numWOID);
      IF(v_numBuildProcessId > 0) then
         INSERT  INTO Sales_process_List_Master(Slp_Name,
													 numDomainID,
													 Pro_Type,
													 numCreatedby,
													 dtCreatedon,
													 numModifedby,
													 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
													 numProjectId,numWorkOrderId)
         SELECT Slp_Name,
					numDomainID,
					Pro_Type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,v_numWOID FROM Sales_process_List_Master WHERE Slp_Id = v_numBuildProcessId;
         v_numNewProcessId := CURRVAL('Sales_process_List_Master_seq');
         UPDATE WorkOrder SET numBuildProcessId = v_numNewProcessId WHERE numWOId = v_numWOID;
         INSERT INTO StagePercentageDetails(numStagePercentageId,
				tintConfiguration,
				vcStageName,
				numdomainid,
				numCreatedBy,
				bintCreatedDate,
				numModifiedBy,
				bintModifiedDate,
				slp_id,
				numAssignTo,
				vcMileStoneName,
				tintPercentage,
				tinProgressPercentage,
				dtStartDate,
				numParentStageID,
				intDueDays,
				numProjectid,
				numOppid,
				vcDescription,
				bitIsDueDaysUsed,
				numTeamId,
				bitRunningDynamicMode,
				numStageOrder,
				numWorkOrderId)
         SELECT
         numStagePercentageId,
				tintConfiguration,
				vcStageName,
				numdomainid,
				v_numUserCntID,
				LOCALTIMESTAMP,
				v_numUserCntID,
				LOCALTIMESTAMP,
				v_numNewProcessId,
				numAssignTo,
				vcMileStoneName,
				tintPercentage,
				tinProgressPercentage,
				LOCALTIMESTAMP,
				numParentStageID,
				intDueDays,
				0,
				0,
				vcDescription,
				bitIsDueDaysUsed,
				numTeamId,
				bitRunningDynamicMode,
				numStageOrder,
				v_numWOID
         FROM
         StagePercentageDetails
         WHERE
         slp_id = v_numBuildProcessId;
         INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
				vcTaskName,
				numHours,
				numMinutes,
				numAssignTo,
				numDomainID,
				numCreatedBy,
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitSavedTask,
				numOrder,
				numReferenceTaskId,
				numWorkOrderId)
         SELECT(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numWorkOrderId = v_numWOID LIMIT 1),
				vcTaskName,
				numHours,
				numMinutes,
				ST.numAssignTo,
				v_numDomainID,
				v_numUserCntID,
				LOCALTIMESTAMP,
				0,
				0,
				0,
				1,
				CASE WHEN SP.bitRunningDynamicMode = true THEN false ELSE true END,
				numOrder,
				numTaskId,
				v_numWOID
         FROM
         StagePercentageDetailsTask AS ST
         LEFT JOIN
         StagePercentageDetails As SP
         ON
         ST.numStageDetailsId = SP.numStageDetailsId
         WHERE
         coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numProjectId,0) = 0 AND
         ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
            StagePercentageDetails As ST
            WHERE
            ST.slp_id = v_numBuildProcessId)
         ORDER BY ST.numOrder;
      end if;
      INSERT INTO WorkOrderDetails(numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWarehouseItemID,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId)
      SELECT
      v_numWOID,numItemKitID,numItemCode,
			CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))*v_numQty) AS DOUBLE PRECISION),
			coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID ORDER BY coalesce(numWLocationID,0),numWareHouseItemID LIMIT 1),0),
			coalesce(DTL.vcItemDesc,txtItemDesc),
			coalesce(sintOrder,0),
			DTL.numQtyItemsReq,
			DTL.numUOMID
      FROM
      Item
      INNER JOIN
      ItemDetails DTL
      ON
      numChildItemID = numItemCode
      WHERE
      numItemKitID = v_numItemCode;
		

		--UPDATE ON ORDER OF ASSEMBLY
      UPDATE
      WareHouseItems
      SET
      numonOrder = coalesce(numonOrder,0)+v_numQty,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;

      v_Description := CONCAT('Work Order Created (Qty:',v_numQty,')');

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numWOID,
      v_tintRefType := 2::SMALLINT,v_vcDescription := v_Description,v_numModifiedBy := v_numUserCntID,
      v_numDomainID := v_numDomainID,SWV_RefCur := null);
      PERFORM USP_ManageInventoryWorkOrder(v_numWOID,v_numDomainID,v_numUserCntID);
   end if;
END; $$;


