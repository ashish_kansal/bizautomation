-- Stored procedure definition script USP_GetProjectExpenseReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProjectExpenseReports(v_numDomainID NUMERIC(18,0),
    v_numProId NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCost  SMALLINT;
   v_numCurrencyID  NUMERIC(18,0);
   v_varCurrSymbol  VARCHAR(10);
BEGIN
   select   coalesce(numCost,1), coalesce(numCurrencyID,0) INTO v_numCost,v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainID;

   select   coalesce(varCurrSymbol,'$') INTO v_varCurrSymbol FROM Currency WHERE numCurrencyID = v_numCurrencyID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      vcItem TEXT,
      vcUnits TEXT,
      vcIncomeSource TEXT,
      vcExpenseSource TEXT,
      vcIncomeExpense TEXT,
      vcNetIncome TEXT,
      monIncome DECIMAL(20,5),
      monExpense DECIMAL(20,5),
      monNetIncome DECIMAL(20,5),
      bitExpensePending BOOLEAN
   );

	-- Sales Orders
   INSERT INTO tt_TEMP(vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending)
   SELECT
   CONCAT(coalesce(OI.vcItemName,I.vcItemName),' <i style="color:#afabab">(',(CASE
   WHEN I.charItemType = 'P'
   THEN CONCAT('Inventory',(CASE WHEN coalesce(OI.bitDropShip,false) = true THEN ' Drop ship' ELSE '' END))
   WHEN I.charItemType = 'N' THEN 'Non-Inventory'
   WHEN I.charItemType = 'S' THEN 'Service'
   WHEN I.charItemType = 'A' THEN 'Accessory'
   END),
   ')</i> ',OI.vcItemDesc)
		,CONCAT(fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(OI.numUOMId,0))*OI.numUnitHour,' (',coalesce(UOM.vcUnitName,'-'),
   ')')
		,CONCAT('<a href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'" target="_blank">',
   vcpOppName,'</a>')
		,(CASE
   WHEN I.charItemType = 'S' AND coalesce(I.bitExpenseItem,false) = true
   THEN(CASE
      WHEN BH.numBillID IS NULL
      THEN '<span class="text-orange">Pending</span>'
      ELSE CONCAT('<a href="javascript:void(0)" onclick="OpenBizInvoice(0,0,',BH.numBillID,
         ',2);">',((CASE WHEN coalesce(BH.bitLandedCost,false) = true THEN 'Laned Cost-' ELSE 'Bill-' END) || CAST(BH.numBillID AS VARCHAR(10))),'</a>')
      END)
   ELSE(CASE v_numCost
      WHEN 3 THEN 'Primary Vendor Cost'
      WHEN 2 THEN 'Products & Services Cost'
      ELSE(CASE WHEN I.charItemType = 'P' THEN 'Average Cost' ELSE 'Primary Vendor Cost' END)
      END)
   END)
		,OI.monTotAmount
		,(CASE
   WHEN I.charItemType = 'S' AND coalesce(I.bitExpenseItem,false) = true
   THEN(CASE
      WHEN BH.numBillID IS NULL
      THEN 0
      ELSE coalesce(BD.monAmount,0)
      END)
   ELSE(CASE v_numCost
      WHEN 3 THEN coalesce(OI.numUnitHour,0)*coalesce(OI.monVendorCost,0)
      WHEN 2 THEN coalesce(OI.numUnitHour,0)*coalesce(OI.numCost,0)
      ELSE(CASE
         WHEN I.charItemType = 'P'
         THEN((coalesce(OI.numUnitHour,0) -coalesce(TEMPFulfilledItems.numUnitHour,0))*coalesce(OI.monAvgCost,0))+coalesce(TEMPFulfilledItems.monCost,0)
         ELSE coalesce(OI.numUnitHour,0)*coalesce(OI.monVendorCost,0)
         END)
      END)
   END)
		,CAST((CASE
   WHEN I.charItemType = 'S' AND coalesce(I.bitExpenseItem,false) = true
   THEN(CASE
      WHEN BH.numBillID IS NULL
      THEN 1
      ELSE 0
      END)
   ELSE 0
   END) AS BOOLEAN)
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   UOM
   ON
   OI.numUOMId = UOM.numUOMId
   LEFT JOIN
   BillDetails BD
   ON
   OI.numoppitemtCode = BD.numOppItemID
   LEFT JOIN
   BillHeader BH
   ON
   BD.numBillID = BH.numBillID
   LEFT JOIN LATERAL(SELECT
      SUM(numUnitHour) AS numUnitHour
			,SUM(coalesce(OBDI.numUnitHour,0)*coalesce(OBDI.monAverageCost,0)) AS monCost
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      WHERE
      OBD.numoppid = OM.numOppID
      AND OBD.numBizDocId = 296
      AND OBDI.numOppItemID = OI.numoppitemtCode) TEMPFulfilledItems on TRUE
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OI.numProjectID,OM.numProjectID) = v_numProId;
		

	-- Bills
   INSERT INTO tt_TEMP(vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending)
   SELECT
   coalesce(BD.vcDescription,'')
		,''
		,''
		,(CASE WHEN(BD.numBillID > 0) THEN  Case When coalesce(BH.bitLandedCost,false) = true THEN 'Laned Cost-' ELSE 'Bill-' END || CAST(BH.numBillID AS VARCHAR(10)) END)
		,0
		,coalesce(monAmount,0)
		,CAST(0 AS BOOLEAN)
   FROM
   BillHeader BH
   INNER JOIN
   BillDetails BD
   ON
   BH.numBillID = BD.numBillID
   WHERE
   BH.numDomainId = v_numDomainID
   AND BD.numProjectID = v_numProId
   AND coalesce(BD.numOppItemID,0) = 0;

   UPDATE
   tt_TEMP
   SET
   vcNetIncome =(CASE
   WHEN coalesce(bitExpensePending,false) = true THEN CONCAT('<span class="text-orange">',v_varCurrSymbol,' ',TO_CHAR(coalesce(monIncome,0) -coalesce(monExpense,0),'#,##0.00###'),'</span>')
   WHEN coalesce(monIncome,0) -coalesce(monExpense,0) >= 0
   THEN CONCAT('<span class="text-green">',v_varCurrSymbol,' ',TO_CHAR(coalesce(monIncome,0) -coalesce(monExpense,0),'#,##0.00###'),'</span>')
   ELSE CONCAT('<span class="text-red">',v_varCurrSymbol,' ',TO_CHAR(coalesce(monIncome,0) -coalesce(monExpense,0),'#,##0.00###'),'</span>')
   END),monNetIncome = coalesce(monIncome,0) -coalesce(monExpense,0),
   vcIncomeExpense = CONCAT(TO_CHAR(coalesce(monIncome,0),'#,##0.00###'),(CASE WHEN coalesce(bitExpensePending,false) = true THEN '' ELSE CONCAT(' (',TO_CHAR(coalesce(monExpense,0),'#,##0.00###'),')') END));
			
   open SWV_RefCur for
   SELECT * FROM tt_TEMP;

   open SWV_RefCur2 for
   with CTE AS(SELECT
   coalesce((SELECT SUM(monIncome) FROM tt_TEMP),0) AS monIncomeAR
		,coalesce((SELECT SUM(monExpense) FROM tt_TEMP),0) AS monNonLaborExpense
		,coalesce((SELECT SUM(monNetIncome) FROM tt_TEMP),0) AS monTotalIncome
		,0 AS monExpenseAP
		,coalesce((SELECT
      SUM(GetTimeSpendOnTaskInMinutes(v_numDomainID,SPDT.numTaskId)::bigint*(coalesce(SPDT.monHourlyRate,UM.monHourlyRate)/60))
      FROM
      Sales_process_List_Master SPLM
      INNER JOIN
      StagePercentageDetails SPD
      ON
      SPLM.Slp_Id = SPD.slp_id
      INNER JOIN
      StagePercentageDetailsTask SPDT
      ON
      SPD.numStageDetailsId = SPDT.numStageDetailsId
      INNER JOIN
      UserMaster UM
      ON
      SPDT.numAssignTo = UM.numUserDetailId
      WHERE
      SPLM.numDomainID = v_numDomainID
      AND SPLM.numProjectId = v_numProId
      AND EXISTS(SELECT SPDTTL.ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)),0) AS monLaborExpense
		,coalesce((SELECT SUM(monIncome) FROM tt_TEMP WHERE coalesce(bitExpensePending,false) = true),0) AS monIncomePendingExpense)
   SELECT *,coalesce(monTotalIncome,0) -coalesce(monLaborExpense,0) AS monGrossProfit FROM CTE;
   RETURN;
	   
END; $$;        
        




