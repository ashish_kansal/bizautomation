CREATE OR REPLACE FUNCTION Item_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
	v_action  CHAR(1) DEFAULT 'I';
	v_bitElasticSearch BOOLEAN;
BEGIN
	

	v_bitElasticSearch := COALESCE((SELECT ECD.bitElasticSearch FROM (SELECT OLD.numDomainID UNION SELECT NEW.numDomainID) T INNER JOIN eCommerceDTL ECD ON (ECD.numDomainId = T.numDomainID AND ECD.bitElasticSearch = true) LIMIT 1),false);

	IF (TG_OP = 'DELETE') THEN
		
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Item', OLD.numItemCode, 'Delete', OLD.numDomainID);
		
		IF v_bitElasticSearch THEN
			INSERT INTO ElasticSearchBizCart
			(
				vcModule
				,vcValue
				,vcAction
				,numDomainID
			) 
			VALUES
			(
				'Item'
				,NEW.numItemCode
				,'Delete'
				,NEW.numDomainID
			);
		END IF;
	ELSIF (TG_OP = 'UPDATE') THEN
		IF NOT EXISTS (SELECT FROM ElasticSearchModifiedRecords WHERE vcModule='Item' AND vcAction='Update' AND numRecordID=NEW.numItemCode) THEN
			INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Item', NEW.numItemCode, 'Update', NEW.numDomainID);
		END IF;

		IF v_bitElasticSearch AND NOT EXISTS (SELECT FROM ElasticSearchBizCart WHERE vcModule='Item' AND vcAction='Update' AND vcValue=NEW.numItemCode::VARCHAR) THEN
			INSERT INTO ElasticSearchBizCart
			(
				vcModule
				,vcValue
				,vcAction
				,numDomainID
			)  
			VALUES
			(
				'Item'
				,NEW.numItemCode
				,'Update'
				,NEW.numDomainID
			);
		END IF;
	ELSIF (TG_OP = 'INSERT') THEN
		IF NOT EXISTS (SELECT FROM ElasticSearchModifiedRecords WHERE vcModule='Item' AND vcAction='Insert' AND numRecordID=NEW.numItemCode) THEN
			INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES ('Item', NEW.numItemCode, 'Insert', NEW.numDomainID);
		END IF;
		
		IF v_bitElasticSearch AND NOT EXISTS (SELECT FROM ElasticSearchBizCart WHERE vcModule='Item' AND vcAction='Insert' AND vcValue=NEW.numItemCode::VARCHAR) THEN
			INSERT INTO ElasticSearchBizCart
			(
				vcModule
				,vcValue
				,vcAction
				,numDomainID
			) 
			VALUES 
			(
				'Item'
				,NEW.numItemCode
				,'Insert'
				,NEW.numDomainID
			);
		END IF;
	END IF;

	RETURN NULL;
END; $$;
CREATE TRIGGER Item_IUD AFTER INSERT OR UPDATE OR DELETE ON Item FOR EACH ROW WHEN (pg_trigger_depth() < 1) EXECUTE PROCEDURE Item_IUD_TrFunc();

