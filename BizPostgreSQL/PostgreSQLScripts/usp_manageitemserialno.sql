-- Stored procedure definition script USP_ManageItemSerialNo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemSerialNo(v_numDomainID NUMERIC(9,0),
    v_strFieldList TEXT,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
   v_minROWNUMBER  INTEGER;
   v_maxROWNUMBER  INTEGER;
   v_Diff  INTEGER;
   v_numNewQty  INTEGER;
   v_dtExpirationDate  DATE;
   v_Diff1  INTEGER;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numWarehouseItmDTLID  NUMERIC(18,0);
   v_vcSerialNo  VARCHAR(100);

   v_vcDescription  VARCHAR(100);
   v_numItemCode  NUMERIC(18,0);
   v_numDomain  NUMERIC(18,0);	           
     
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT
      X.*,
		ROW_NUMBER() OVER(order by X.NewQty) AS ROWNUMBER
	
      FROM(SELECT
      numWareHouseItmsDTLID,
				numWareHouseItemID,
				vcSerialNo,
				NewQty,
				OldQty,
				(CASE WHEN CAST(dExpirationDate AS DATE) = cast('1753-1-1' as DATE) THEN NULL ELSE dExpirationDate END) AS dExpirationDate
      FROM
	  XMLTABLE
		(
			'NewDataSet/SerializedItems'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
				numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
				vcSerialNo VARCHAR(100) PATH 'vcSerialNo',
				NewQty NUMERIC(9,0) PATH 'NewQty',
				OldQty NUMERIC(9,0) PATH 'OldQty',
				dExpirationDate TIMESTAMP PATH 'dExpirationDate'
		) AS Y) AS X;

   select   MIN(ROWNUMBER), MAX(ROWNUMBER) INTO v_minROWNUMBER,v_maxROWNUMBER FROM tt_TEMPTABLE;

   WHILE  v_minROWNUMBER <= v_maxROWNUMBER LOOP
      select   numWarehouseItmsDTLID, numWareHouseItemID, NewQty, vcSerialNo, dExpirationDate, NewQty -OldQty INTO v_numWarehouseItmDTLID,v_numWareHouseItemID,v_numNewQty,v_vcSerialNo,v_dtExpirationDate,
      v_Diff FROM
      tt_TEMPTABLE WHERE
      ROWNUMBER = v_minROWNUMBER;
      v_numItemCode := 0;
      select   numItemID INTO v_numItemCode FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID
      AND numDomainID = v_numDomainID;
      IF coalesce((SELECT bitSerialized FROM Item WHERE numItemCode = v_numItemCode),
      false) = true then
		
         IF EXISTS(SELECT
         numWareHouseItmsDTLID
         FROM
         WareHouseItmsDTL
         WHERE
         numWareHouseItemID IN(SELECT coalesce(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = v_numItemCode)
         AND WareHouseItmsDTL.numWareHouseItmsDTLID <> v_numWarehouseItmDTLID
         AND WareHouseItmsDTL.vcSerialNo = v_vcSerialNo) then
			
            RAISE EXCEPTION 'DUPLICATE_SERIAL_NUMBER';
            RETURN;
         end if;
      end if;
      UPDATE
      WareHouseItmsDTL
      SET
      numQty = v_numNewQty,vcSerialNo = v_vcSerialNo,dExpirationDate = v_dtExpirationDate
      WHERE
      numWareHouseItmsDTLID = v_numWarehouseItmDTLID;
      v_vcDescription := 'UPDATE Lot/Serial#';
      UPDATE
      WareHouseItems
      SET
      numOnHand = coalesce(numOnHand,0)+v_Diff,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWareHouseItemID
      AND(numOnHand+v_Diff) >= 0;
      v_numDomain := v_numDomainID;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
      v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
      select   MIN(ROWNUMBER) INTO v_minROWNUMBER FROM  tt_TEMPTABLE WHERE  ROWNUMBER > v_minROWNUMBER;
   END LOOP;	           
     
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
END; $$;
                      

