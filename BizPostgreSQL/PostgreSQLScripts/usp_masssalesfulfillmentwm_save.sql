-- Stored procedure definition script USP_MassSalesFulfillmentWM_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWM_Save(v_numDomainID NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0)
	,v_tintSourceType SMALLINT
	,v_numCountryID NUMERIC(18,0)
	,v_vcStateIDs TEXT
	,v_vcWarehousePriorities TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ID  NUMERIC(18,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numStateID NUMERIC(18,0)
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP(numStateID)
   SELECT
   Id
   FROM
   SplitIDs(v_vcStateIDs,',');

   INSERT INTO tt_TEMPWAREHOUSE(numWarehouseID)
   SELECT
   Id
   FROM
   SplitIDs(v_vcWarehousePriorities,',');


   IF EXISTS(SELECT
   MassSalesFulfillmentWMState.ID
   FROM
   MassSalesFulfillmentWM
   INNER JOIN
   MassSalesFulfillmentWMState
   ON
   MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
   WHERE
   numDomainID = v_numDomainID
   AND numOrderSource = v_numOrderSource
   AND numStateID IN(SELECT numStateID FROM tt_TEMP)) then
	
      RAISE EXCEPTION 'DUPLICATE_STATE';
      RETURN;
   ELSE
	
      INSERT INTO MassSalesFulfillmentWM(numDomainID
			,numOrderSource
			,numCountryID
			,tintSourceType)
		VALUES(v_numDomainID
			,v_numOrderSource
			,v_numCountryID
			,v_tintSourceType);
		
      v_ID := CURRVAL('MassSalesFulfillmentWM_seq');
      INSERT INTO MassSalesFulfillmentWMState(numMSFWMID
			,numStateID)
      SELECT
      v_ID
			,numStateID
      FROM
      tt_TEMP;
      INSERT INTO MassSalesFulfillmentWP(numMSFWMID
			,numWarehouseID
			,intOrder)
      SELECT
      v_ID
			,numWarehouseID
			,ID
      FROM
      tt_TEMPWAREHOUSE;
   end if;
END; $$;


