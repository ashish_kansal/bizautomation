-- Stored procedure definition script USP_GetReportKPIGroupListMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReportKPIGroupListMaster(v_numDomainID NUMERIC(18,0),
	v_numReportKPIGroupID NUMERIC(18,0),
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintKPIGroupReportType  SMALLINT;
BEGIN
   IF v_tintMode = 0 then --Select KPI Group Detail

      IF v_numReportKPIGroupID = 0 then
	
         open SWV_RefCur for
         SELECT *,CASE tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType FROM ReportKPIGroupListMaster WHERE numDomainID = v_numDomainID;
      ELSE
         open SWV_RefCur for
         SELECT * FROM ReportKPIGroupListMaster WHERE numDomainID = v_numDomainID AND numReportKPIGroupID = v_numReportKPIGroupID;
      end if;
   ELSEIF v_tintMode = 1
   then  --Select KPI Group Reports Detail

      open SWV_RefCur for
      SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName
      from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID = RMM.numReportModuleID
      JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID = RMGM.numReportModuleGroupID
      join ReportKPIGroupDetailList KPI on RLM.numReportID = KPI.numReportID
      where RLM.numDomainID = v_numDomainID AND KPI.numReportKPIGroupID = v_numReportKPIGroupID
      order by RLM.numReportID;
   ELSEIF v_tintMode = 2
   then --Select Available KPI Reports
      select   tintKPIGroupReportType INTO v_tintKPIGroupReportType FROM ReportKPIGroupListMaster WHERE numDomainID = v_numDomainID AND numReportKPIGroupID = v_numReportKPIGroupID;
      open SWV_RefCur for
      SELECT numReportID,vcReportName FROM ReportListMaster WHERE numDomainID = v_numDomainID AND tintReportType = v_tintKPIGroupReportType
      AND numReportID NOT IN(SELECT numReportID FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID = v_numReportKPIGroupID);
   end if;
   RETURN;
END; $$;
	


