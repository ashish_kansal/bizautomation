-- Stored procedure definition script USP_GetParentCategory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetParentCategory(v_numDomainId NUMERIC(9,0),
    v_vcInitialCode VARCHAR(50) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
    

--if @vcInitialCode<>'010101'
--	  begin
   open SWV_RefCur for SELECT
   coalesce(C.vcAccountName,'') || ' (' || ATD.vcAccountType || ')' AS vcAccountName1,
                ATD.vcAccountCode AS vcAccountTypeCode,
                ATD.numAccountTypeID,
				ATD.vcAccountType,
				ATD.numParentID,
				ATD.numDomainID,
				ATD.dtCreateDate,
				ATD.dtModifiedDate,
				C.numAccountId,
				C.numParntAcntTypeID,
				C.numOriginalOpeningBal,
				C.numOpeningBal,
				C.dtOpeningDate,
				C.bitActive,
				C.bitFixed,
				C.numDomainId,
				C.numListItemID,
				C.bitDepreciation,
				C.bitOpeningBalanceEquity,
				C.monEndingOpeningBal,
				C.monEndingBal,
				C.dtEndStatementDate,
				C.chBizDocItems,
				C.numAcntTypeId,
				C.vcAccountCode,
				cast(coalesce(C.vcAccountName,'') as VARCHAR(100)) AS vcAccountName,
				C.vcAccountDescription,
				C.bitProfitLoss,cast(coalesce(C.vcStartingCheckNumber,'') as VARCHAR(50)) AS vcStartingCheckNumber
   FROM    Chart_Of_Accounts C
   INNER JOIN AccountTypeDetail ATD
   ON C.numParntAcntTypeID = ATD.numAccountTypeID
   WHERE   C.numDomainId = v_numDomainId AND coalesce(ATD.bitActive,false) = true
   AND (C.vcAccountCode ilike coalesce(v_vcInitialCode,'') || '%' OR LENGTH(v_vcInitialCode) = 0) AND C.bitActive = true
   ORDER BY ATD.vcAccountCode,C.vcAccountCode;
--     Declare @RootAccountId as numeric(9)  
--    Set @RootAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = @numDomainId) --and numAccountId = 1   
--      
--         
-- Select * From Chart_Of_Accounts COA inner join ListDetails LD on COA.numAcntType= LD.numListItemID Where COA.numParntAcntId =@RootAccountId and COA.numDomainId=@numDomainId and COA.numAccountId <>@RootAccountId    
--         
--
--	end
-- else
--	begin
--		SELECT  
--                ISNULL(C.[vcAccountName],'') + ' (' + ATD.[vcAccountType] + ')' AS vcAccountName1,
--                ATD.[vcAccountCode] AS vcAccountTypeCode,
--                ATD.[numAccountTypeID],
--				ATD.[vcAccountCode],
--				ATD.[vcAccountType],
--				ATD.[numParentID],
--				ATD.[numDomainID],
--				ATD.[dtCreateDate],
--				ATD.[dtModifiedDate],
--				C.[numAccountId],
--				C.[numParntAcntTypeId],
--				C.[numOriginalOpeningBal],
--				C.[numOpeningBal],
--				C.[dtOpeningDate],
--				C.[bitActive],
--				C.[bitFixed],
--				C.[numDomainId],
--				C.[numListItemID],
--				C.[bitDepreciation],
--				C.[bitOpeningBalanceEquity],
--				C.[monEndingOpeningBal],
--				C.[monEndingBal],
--				C.[dtEndStatementDate],
--				C.[chBizDocItems],
--				C.[numAcntTypeId],
--				C.[vcAccountCode],
--				ISNULL(C.[vcAccountName],'') vcAccountName,
--				C.[vcAccountDescription],
--				C.[bitProfitLoss],ISNULL(C.vcStartingCheckNumber,'') AS vcStartingCheckNumber
--        FROM    [Chart_Of_Accounts] C
--                INNER JOIN [AccountTypeDetail] ATD 
--                ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
--        WHERE   C.[numDomainId] = @numDomainId AND C.bitActive = 1
--  AND (C.[vcAccountCode] LIKE @vcInitialCode + '--' OR  C.[vcAccountCode] LIKE '010201--' OR  LEN(@vcInitialCode)=0)
--  ORDER BY C.[vcAccountCode]
--	end
END; $$;
--Created By                                                          












