DROP FUNCTION IF EXISTS Usp_CreateCloneItem;

CREATE OR REPLACE FUNCTION Usp_CreateCloneItem(INOUT v_numItemCode		BIGINT ,
	v_numDomainID		NUMERIC(18,0))
LANGUAGE plpgsql
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
   AS $$
   DECLARE
   v_bitCloneLocationWithItem  BOOLEAN;
   v_numItemGroup  NUMERIC(18,0);
   v_bitMatrix BOOLEAN;
   v_vcItemName  VARCHAR(300);

   v_numNewItemCode  BIGINT;
	
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_vcProductVariation  VARCHAR(300);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   coalesce(bitCloneLocationWithItem,false) INTO v_bitCloneLocationWithItem FROM Domain WHERE numDomainId = v_numDomainID;
      select   numItemGroup, bitMatrix, vcItemName INTO v_numItemGroup,v_bitMatrix,v_vcItemName FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
      INSERT INTO Item(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId)
      SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,LOCALTIMESTAMP,
	LOCALTIMESTAMP,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId
      FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID RETURNING numItemCode INTO v_numNewItemCode;
       
      IF coalesce(v_bitMatrix,false) = true then
         with recursive data(ProductId,ProductOptionGroupId,ProductOptionId) AS(SELECT
         v_numItemCode AS ProductId
				,CFW_Fld_Master.Fld_id AS ProductOptionGroupId
				,Listdetails.numListItemID AS ProductOptionId
         FROM
         ItemGroupsDTL
         INNER JOIN
         CFW_Fld_Master
         ON
         ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
         INNER JOIN
         Listdetails
         ON
         CFW_Fld_Master.numlistid = Listdetails.numListID
         WHERE
         numItemGroupID = v_numItemGroup),ranked AS(
		/* ranking the group IDs */
			SELECT
         ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				DENSE_RANK() OVER(PARTITION BY ProductId ORDER BY ProductOptionGroupId) AS GroupRank
         FROM
         data),crossjoined AS(
			/* obtaining all possible combinations */
			SELECT
         ProductId,
				ProductOptionGroupId,
				GroupRank,
				CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS VARCHAR(250)) AS ProductVariant
         FROM
         ranked
         WHERE
         GroupRank = 1
         UNION ALL
         SELECT
         r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250)) AS ProductVariant
         FROM
         ranked r
         INNER JOIN
         crossjoined c
         ON
         r.ProductId = c.ProductId
         AND
         r.GroupRank = c.GroupRank+1),
         maxranks AS(
			/* getting the maximum group rank value for every product */
			SELECT
         ProductId,
				MAX(GroupRank) AS MaxRank
         FROM
         ranked
         GROUP BY
         ProductId) select   c.ProductVariant INTO v_vcProductVariation FROM
         crossjoined c
         INNER JOIN
         maxranks m
         ON
         c.ProductId = m.ProductId
         AND c.GroupRank = m.MaxRank
         AND c.ProductVariant NOT IN(select string_agg(CONCAT(FLD_ID,':',FLD_Value),',') FROM ItemAttributes)     LIMIT 1;
         IF LENGTH(coalesce(v_vcProductVariation,'')) = 0 then
		
            RAISE EXCEPTION 'ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD';
         ELSE
            INSERT INTO ItemAttributes(numDomainID
				,numItemCode
				,Fld_ID
				,Fld_Value)
            SELECT
            v_numDomainID
				,v_numNewItemCode
				,SUBSTR(OutParam,0,POSITION(':' IN OutParam))::NUMERIC
				,SUBSTR(OutParam,POSITION(':' IN OutParam)+1,LENGTH(OutParam))::NUMERIC
            FROM
            SplitString(v_vcProductVariation,',');
         end if;
      end if;
      INSERT INTO ItemAPI(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
      SELECT WebApiId,numDomainId,v_numNewItemCode,vcAPIItemID,numCreatedby,LOCALTIMESTAMP,numModifiedby,LOCALTIMESTAMP
      FROM ItemAPI WHERE numItemID = v_numItemCode AND numDomainId = v_numDomainID;
      INSERT INTO ItemCategory(numItemID,numCategoryID)
      SELECT v_numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = v_numItemCode;
      INSERT INTO ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,numUOMID,vcItemDesc,sintOrder)
      SELECT v_numNewItemCode,numChildItemID,numQtyItemsReq,numUOMID,vcItemDesc,sintOrder FROM ItemDetails WHERE numItemKitID = v_numItemCode;
      INSERT INTO ItemImages(numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
      SELECT
      v_numNewItemCode
		,CONCAT(SUBSTR(vcPathForImage,0,POSITION('.' IN vcPathForImage)),'_',v_numNewItemCode,
      SUBSTR(vcPathForImage,POSITION('.' IN vcPathForImage),LENGTH(vcPathForImage)))
		,CONCAT(SUBSTR(vcPathForTImage,0,POSITION('.' IN vcPathForTImage)),'_',v_numNewItemCode,
      SUBSTR(vcPathForTImage,POSITION('.' IN vcPathForTImage),LENGTH(vcPathForTImage)))
		,bitDefault
		,intDisplayOrder
		,numDomainId
		,bitIsImage
      FROM ItemImages WHERE numItemCode = v_numItemCode  AND numDomainId = v_numDomainID;
      INSERT INTO ItemTax(numItemCode,numTaxItemID,bitApplicable)
      SELECT v_numNewItemCode,numTaxItemID,bitApplicable FROM ItemTax WHERE numItemCode = v_numItemCode;
      INSERT INTO Vendor(numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
      SELECT numVendorID,vcPartNo,numDomainID,monCost,v_numNewItemCode,intMinQty
      FROM Vendor WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
      INSERT INTO WareHouseItems(numItemID,numWareHouseID,numOnHand,numonOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID)
      SELECT v_numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,LOCALTIMESTAMP,numWLocationID
      FROM WareHouseItems  WHERE numItemID = v_numItemCode AND numDomainID = v_numDomainID AND 1 =(CASE WHEN v_bitCloneLocationWithItem = true THEN 1 ELSE(CASE WHEN coalesce(numWLocationID,0) = 0 THEN 1 ELSE 0 END) END);
      IF coalesce(v_bitMatrix,false) = true AND(SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = v_numNewItemCode) > 0 then
	
		-- INSERT NEW WAREHOUSE ATTRIBUTES
         INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized)
         SELECT
         Fld_ID,
			coalesce(CAST(Fld_Value AS VARCHAR(30)),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			false
         FROM
         ItemAttributes
         INNER JOIN (SELECT
            numWareHouseItemID
            FROM
            WareHouseItems
            WHERE
            numDomainID = v_numDomainID
            AND numItemID = v_numNewItemCode) AS TEMPWarehouse on TRUE
         WHERE
         numDomainID = v_numDomainID
         AND numItemCode = v_numNewItemCode;
      end if;
      INSERT INTO CFW_FLD_Values_Item(Fld_ID,Fld_Value,RecId)
      SELECT Fld_ID,Fld_Value,v_numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = v_numItemCode;
      v_numItemCode := v_numNewItemCode;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


