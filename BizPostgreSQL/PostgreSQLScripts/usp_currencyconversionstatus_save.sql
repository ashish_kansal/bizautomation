-- Stored procedure definition script USP_CurrencyConversionStatus_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CurrencyConversionStatus_Save(v_dtLastExecuted DATE,
	v_bitSucceed BOOLEAN,
	v_intNoOfTimesTried INTEGER,
	v_bitFailureNotificationSent BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   CurrencyConversionStatus
   SET
   dtLastExecuted = v_dtLastExecuted,bitSucceed = v_bitSucceed,intNoOfTimesTried = v_intNoOfTimesTried,
   bitFailureNotificationSent = v_bitFailureNotificationSent
   WHERE
   Id = 1;
   RETURN; --WE ARE PASSING FIX ID BECUASE TABLE WILL ALWAYS HAVE 1 ROW AND WE WILL UPDATE DATA TO SAME ROW
END; $$;

