CREATE OR REPLACE FUNCTION USP_GetCourses(v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numCountry NUMERIC(9,0) DEFAULT 0,            
v_numState NUMERIC(9,0) DEFAULT 0,            
v_SearchText VARCHAR(100) DEFAULT '',            
v_FromDate TIMESTAMP DEFAULT null,            
v_ToDate TIMESTAMP DEFAULT null,            
v_Miles INTEGER DEFAULT 0,            
v_ZipCode VARCHAR(15) DEFAULT '',          
v_numCategory NUMERIC(9,0) DEFAULT 0,      
v_byteMode SMALLINT DEFAULT NULL,      
v_ItemID NUMERIC(9,0) DEFAULT 0 ,      
v_ModelID VARCHAR(200) DEFAULT NULL,      
v_strWarehouse VARCHAR(100) DEFAULT NULL,      
v_strState VARCHAR(2) DEFAULT NULL,      
v_chapter VARCHAR(50) DEFAULT NULL,      
v_subchapter VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);
BEGIN
   If v_FromDate = '1753-01-01 00:00:00:000' then 
      v_FromDate := null;
   end if;          
   If v_ToDate = '1753-01-01 00:00:00:000' then 
      v_ToDate := null;
   end if;          
          
   If v_byteMode = 0 then

      v_strSQL := '      
    with recursive HierarchyTable(numCategoryID,ParentID,name) as       
 (select numCategoryID AS numCategoryID, numDepCategory AS ParentID, vcCategoryName AS name      
 from Category      
 where numCategoryID = ' || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20) || '      
 union all       
 select Category.numCategoryID AS numCategoryID, Category.numDepCategory AS ParentID ,Category.vcCategoryName AS name      
 from Category       
 inner join HierarchyTable       
 on Category.numDepCategory = HierarchyTable.numCategoryID)      
    select distinct(vcModelID) as CourseCode,vcItemName as Title,          
 monWListPrice  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID = I.numItemCode            
 Join Warehouses W            
 on W.numWareHouseID = WI.numWareHouseID      
    join   RemarketDTL R      
    on  R.numItemID = I.numItemCode and R.numWareHouseItemID = WI.numWareHouseItemID      
 where I.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20);
      if v_numCountry > 0 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and W.numWCountry =' || SUBSTR(CAST(v_numCountry AS VARCHAR(20)),1,20);
      end if;
      if  v_numState > 0 then  
         v_strSQL := coalesce(v_strSQL,'') || ' and W.numWState =' || SUBSTR(CAST(v_numState AS VARCHAR(20)),1,20);
      end if;
      if  v_FromDate is not null and v_ToDate is null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select RecId from CFW_FLD_Values_Item where Fld_ID = 150 and Fld_Value <= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select RecId from CFW_FLD_Values_Item where Fld_ID = 151 and Fld_Value >= ''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is not null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select RecId from CFW_FLD_Values_Item where ((Fld_ID = 151 and Fld_Value >= ''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''') or (Fld_ID = 150 and Fld_Value <= ''' ||
         SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || ''')) group by RecId having count(*) > 1)';
      end if;
      if  v_SearchText <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and (vcSKU ilike ''%' || coalesce(v_SearchText,'') || '%'' or vcModelID ilike ''%' || coalesce(v_SearchText,'') || '%'' or vcItemName ilike ''%' || coalesce(v_SearchText,'') || '%'' or txtItemDesc ilike ''%' || coalesce(v_SearchText,'') || '%'')';
      end if;
      If v_Miles <> 0 And v_ZipCode <> '' then
  
         If SWF_IsNumeric(v_ZipCode) = true then  
            v_strSQL := coalesce(v_strSQL,'') || ' and vcWPinCode in(SELECT * FROM funZips(' || coalesce(v_ZipCode,'') || ',' || SUBSTR(CAST(v_Miles AS VARCHAR(20)),1,20) || '))';
         end if;
      end if;
      if v_numCategory > 0 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select numItemID from ItemCategory where numCategoryID in(select numCategoryID from HierarchyTable))';
      end if;
      v_strSQL := coalesce(v_strSQL,'') || ' order by vcModelID';
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   ELSEIF v_byteMode = 1
   then

      v_strSQL := 'Select * from(select numItemCode,WI.numWareHouseItemID,vcSKU as vcClassNum,vcModelID as CourseCode,vcItemName as Title,vcWarehouse,          
 convert(varchar(11),cast(dbo.GetCustFldValue(150,5,numItemCode) as datetime),100)+'' - ''+ convert(varchar(11),cast(dbo.GetCustFldValue(151,5,numItemCode) as datetime),100) as CourseDate,          
 dbo.GetListIemName(dbo.GetCustFldValue(153,5,numItemCode)) as CLASSAT,vcWCity as CITY,dbo.fn_GetState(numWState) as STATE,monWListPrice,vcWareHouse as FACNAME  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID=I.numItemCode            
 Join Warehouses W       
 on W.numWareHouseID=WI.numWareHouseID       
     join   RemarketDTL R      
    on  R.numItemID=I.numItemCode   and R.numWareHouseItemID=WI.numWareHouseItemID       
 where I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) ||
      'and I.vcModelID=''' || coalesce(v_ModelID,'') || '''';
      if v_numCountry > 0 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and W.numWCountry=' || SUBSTR(CAST(v_numCountry AS VARCHAR(20)),1,20);
      end if;
      if  v_numState > 0 then  
         v_strSQL := coalesce(v_strSQL,'') || ' and W.numWState=' || SUBSTR(CAST(v_numState AS VARCHAR(20)),1,20);
      end if;
      if  v_FromDate is not null and v_ToDate is null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=150 and Fld_Value <=''' || SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=151 and Fld_Value >=''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is not null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select RecId from CFW_FLD_Values_Item where ((Fld_ID=151 and Fld_Value >=''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''') or (Fld_ID=150 and Fld_Value <=''' ||
         SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || '''))  group by RecId having count(*)>1 )';
      end if;
      if  v_SearchText <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and ( vcSKU like ''%' || coalesce(v_SearchText,'') || '%'' or vcModelID like ''%' || coalesce(v_SearchText,'') || '%'' or vcItemName like ''%' || coalesce(v_SearchText,'') || '%'' or txtItemDesc like ''%' || coalesce(v_SearchText,'') || '%'')';
      end if;
      If v_Miles <> 0 And v_ZipCode <> '' then
  
         If SWF_IsNumeric(v_ZipCode) = true then  
            v_strSQL := coalesce(v_strSQL,'') || ' and vcWPinCode in (SELECT * FROM dbo.funZips(' || coalesce(v_ZipCode,'') || ',' || SUBSTR(CAST(v_Miles AS VARCHAR(20)),1,20) || '))';
         end if;
      end if;
      if v_numCategory > 0 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select numItemID from ItemCategory where numCategoryID=' || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20) || ')';
      end if;
      v_strSQL := coalesce(v_strSQL,'') || ' )X order by X.CourseCode,cast(dbo.GetCustFldValue(151,5,X.numItemCode) as datetime),X.CITY Asc';
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   ELSEIF v_byteMode = 2
   then

      v_strSQL := 'Select *,X."Start Date" || '' - '' || X."End Date" as CourseDate from(select numItemCode,WI.numWareHouseItemID,vcSKU as vcClassNum,vcModelID as CourseCode,vcItemName as Title,vcWarehouse,          
 SUBSTR(TO_CHAR(cast(GetCustFldValue(150,5,numItemCode) as TIMESTAMP),''Mon dd yyyy hh:miAM''),1,11) as "Start Date",SUBSTR(TO_CHAR(cast(GetCustFldValue(151,5,numItemCode) as TIMESTAMP),''Mon dd yyyy hh:miAM''),1,11) as "End Date",          
 GetListIemName(GetCustFldValue(153,5,numItemCode)) as CLASSAT,vcWCity as CITY,fn_GetState(numWState) as STATE,monWListPrice,vcWarehouse as FACNAME  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID = I.numItemCode            
 Join Warehouses W            
 on W.numWareHouseID = WI.numWareHouseID       
    join   RemarketDTL R      
    on  R.numItemID = I.numItemCode    and R.numWareHouseItemID = WI.numWareHouseItemID      
 where I.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20);
      if  v_ModelID <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and I.vcModelID=''' || coalesce(v_ModelID,'') || '''';
      end if;
      if  v_chapter <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and GetCustFldValue(133,5,numItemCode) in(select numListItemID from ListDetails where  numListId = 118 and vcData ilike ''%' || SUBSTR(CAST(v_chapter AS VARCHAR(100)),1,100) || '%'' and numDomainID =' ||
         SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
      end if;
      if  v_subchapter <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and dbo.GetCustFldValue(134,5,numItemCode) in (select numListItemID from ListDetails where numListId=119 and  vcData like ''%' || SUBSTR(CAST(v_subchapter AS VARCHAR(100)),1,100) || '%'' and numDomainID=' ||
         SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
      end if;
      if  v_strState <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and W.numWState in(select numStateID from State where vcState = ''' || SUBSTR(CAST(v_strState AS VARCHAR(2)),1,2) || ''' and numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
      end if;
      if  v_strWarehouse <> '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and W.vcWarehouse like  ''%' || SUBSTR(CAST(v_strWarehouse AS VARCHAR(100)),1,100) || '%''';
      end if;
      if  v_FromDate is not null and v_ToDate is null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select RecId from CFW_FLD_Values_Item where Fld_ID = 150 and Fld_Value <= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=151 and Fld_Value >=''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      if  v_FromDate is not null and v_ToDate is not  null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in(select RecId from CFW_FLD_Values_Item where ((Fld_ID = 151 and Fld_Value >= ''' || SUBSTR(CAST(v_ToDate AS VARCHAR(20)),1,20) || ''') or (Fld_ID = 150 and Fld_Value <= ''' ||
         SUBSTR(CAST(v_FromDate AS VARCHAR(20)),1,20) || '''))  group by RecId having count(*) > 1))';
      end if;
      if v_numCategory > 0 then 
         v_strSQL := coalesce(v_strSQL,'') || ' and numItemCode in (select numItemID from ItemCategory where numCategoryID=' || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20) || ')';
      end if;
      v_strSQL := coalesce(v_strSQL,'') || ' X order by X.CourseCode,X."Start Date",X.CITY Asc';
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;  
/****** Object:  StoredProcedure [dbo].[USP_GetCreditStatusOfCompany]    Script Date: 07/26/2008 16:17:03 ******/
   RETURN;
END; $$;


