-- Stored procedure definition script usp_GetDocumentsForOpp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDocumentsForOpp(   
--
v_numOppID NUMERIC(9,0) DEFAULT 0,
v_numDocID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numDocID = 0 then
	
      open SWV_RefCur for
      select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID = DM.numDivisionID
      where OD.numOppID = v_numOppID
      Order By OD.vcDisplayName;
   else
      open SWV_RefCur for
      select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID = DM.numDivisionID
      where OD.numDocID = v_numDocID;
   end if;
   RETURN;
END; $$;


