-- Stored procedure definition script USP_ItemUOMConversion_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemUOMConversion_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numSourceUOM NUMERIC(18,0),
	v_numTargetUOM NUMERIC(18,0),
	v_numTargetUnit DOUBLE PRECISION)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO ItemUOMConversion(numDomainID,
		numItemCode,
		numSourceUOM,
		numTargetUOM,
		numTargetUnit,
		numCreatedBy,
		dtCreated)
	VALUES(v_numDomainID,
		v_numItemCode,
		v_numSourceUOM,
		v_numTargetUOM,
		v_numTargetUnit,
		v_numUserCntID,
		LOCALTIMESTAMP);
RETURN;
END; $$;

