-- Function definition script fn_GetTotalHrsWorked for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetTotalHrsWorked(v_dtStartDate TIMESTAMP,v_dtEndDate TIMESTAMP,v_numUserCntID NUMERIC,v_numDomainId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_decTotalHrsWorked  DECIMAL(10,2);
BEGIN
   select   sum(x.Hrs) INTO v_decTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
      from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1
      And (dtFromDate between v_dtStartDate And v_dtEndDate Or dtToDate between v_dtStartDate And v_dtEndDate) And
      numUserCntID = v_numUserCntID  And numDomainID = v_numDomainId) x;        
   Return v_decTotalHrsWorked;
END; $$;

