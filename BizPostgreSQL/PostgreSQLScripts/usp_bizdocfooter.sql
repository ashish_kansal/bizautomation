-- Stored procedure definition script USP_BizDocFooter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDocFooter(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_byteMode SMALLINT DEFAULT NULL,
v_vcBizDocFooter VARCHAR(100) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      update Domain set vcBizDocFooter = v_vcBizDocFooter where numDomainId = v_numDomainID;
   ELSEIF v_byteMode = 2
   then

      update Domain set vcBizDocFooter = null where numDomainId = v_numDomainID;
   ELSEIF v_byteMode = 3
   then

      update Domain set vcPurBizDocFooter = v_vcBizDocFooter where numDomainId = v_numDomainID;
   ELSEIF v_byteMode = 4
   then

      update Domain set vcPurBizDocFooter = null where numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


