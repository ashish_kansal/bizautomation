-- Stored procedure definition script usp_GetSurveyAnswersAndNosOfResponses for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetSurveyAnswersAndNosOfResponses(v_numParentSurID NUMERIC,
    v_numSurID NUMERIC,
    v_numQID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_boolMatrixColumn  BOOLEAN;
BEGIN
   SELECT    boolMatrixColumn INTO v_boolMatrixColumn FROM      SurveyQuestionMaster WHERE     numQID = v_numQID
   AND numSurID = v_numSurID;
                 
   IF v_boolMatrixColumn = false then
            
      open SWV_RefCur for
      SELECT  sam.numSurID,
                        sam.numQID,
                        sam.numAnsID,
                        sam.vcAnsLabel,
                        COUNT(numRespondantID) AS numResponses,
                        1 AS boolActivation,
                        CASE WHEN v_numParentSurID = v_numSurID
      THEN sam.boolSurveyRuleAttached
      ELSE false
      END AS boolSurveyRuleAttached,
                        0 AS boolDeleted
      FROM    SurveyAnsMaster sam INNER JOIN SurveyResponse sr ON sam.numSurID = sr.numParentSurID AND sr.numQuestionID = sam.numQID AND sr.numAnsID = sam.numAnsID
      WHERE   sr.numParentSurID = v_numParentSurID
      AND sr.numSurID = v_numSurID
      AND sam.numSurID = v_numParentSurID
      AND sr.numQuestionID = v_numQID
      AND sam.numQID = v_numQID
      GROUP BY sam.numSurID,sam.numQID,sam.numAnsID,sam.vcAnsLabel,sam.boolSurveyRuleAttached
      ORDER BY sam.numAnsID;
   ELSEIF v_boolMatrixColumn = true
   then
            
      open SWV_RefCur for
      SELECT  sam.numSurID,
                        sam.numQID,
                        sam.numMatrixID AS numAnsID,
                        sam.vcAnsLabel,
                        COUNT(numRespondantID) AS numResponses,
                        1 AS boolActivation,
                        CASE WHEN v_numParentSurID = v_numSurID
      THEN sam.boolSurveyRuleAttached
      ELSE false
      END AS boolSurveyRuleAttached,
                        0 AS boolDeleted
      FROM    SurveyMatrixAnsMaster sam INNER JOIN SurveyResponse sr ON sam.numSurID = sr.numParentSurID AND sr.numQuestionID = sam.numQID AND sr.numMatrixID = sam.numMatrixID
      WHERE   sr.numParentSurID = v_numParentSurID
      AND sr.numSurID = v_numSurID
      AND sam.numSurID = v_numParentSurID
      AND sr.numQuestionID = v_numQID
      AND sam.numQID = v_numQID
      GROUP BY sam.numSurID,sam.numQID,sam.numMatrixID,sam.vcAnsLabel,sam.boolSurveyRuleAttached
      ORDER BY sam.numMatrixID;
   end if;
   RETURN;
END; $$;


