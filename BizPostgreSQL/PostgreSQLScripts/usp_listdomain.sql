-- Stored procedure definition script USP_ListDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ListDomain(v_numType INTEGER,
v_numDomainId INTEGER,
v_numSubscriberID INTEGER,
v_vcDomainCode VARCHAR(50) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numType = 1 then
	
      open SWV_RefCur for
      select vcDomainName ,numDomainId from Domain where numParentDomainID is null and numSubscriberID is  null;
   ELSEIF v_numType = 2
   then
	
      open SWV_RefCur for
      select vcCompanyName || '-Subscriber' as vcDomainName ,a.numSubscriberID as numDomainID  from Subscribers a inner join DivisionMaster b on a.numDivisionid = b.numDivisionID
      inner join CompanyInfo C ON C.numCompanyId = b.numCompanyID AND a.numSubscriberID = v_numSubscriberID
      UNION
      select vcDomainName || '-Domain' as vcDomainName ,numDomainId from Domain
      where numDomainId not in(v_numDomainId) and
      numDomainId in(select DN.numDomainId from Domain DN
         LEFT JOIN LATERAL(SELECT * FROM Domain) DNA on TRUE where DN.vcDomainCode ilike DNA.vcDomainCode || '%'
         and  DNA.numSubscriberID = v_numSubscriberID) and
      numSubscriberID is not null and numParentDomainID is not null;
   ELSEIF v_numType = 3
   then
	
      open SWV_RefCur for
      select vcDomainName ,numDomainId,vcDomainCode from Domain where numSubscriberID = v_numSubscriberID and numParentDomainID = 0;
   ELSEIF v_numType = 4
   then
	
      open SWV_RefCur for
      select DN.vcDomainName ,DN.numDomainId,DN.vcDomainCode,DNA.vcDomainName as ParentDomainName,DNA.numDomainID as numParentDomainID from Domain DN
      LEFT JOIN LATERAL(SELECT * FROM Domain) DNA on TRUE
      where  DNA.numDomainID = DN.numParentDomainID and DNA.numSubscriberID = v_numSubscriberID and DN.numSubscriberID = v_numSubscriberID and DN.vcDomainCode ilike coalesce(v_vcDomainCode,'') || '%'
      order by DN.vcDomainCode;
   end if;
   RETURN;
END; $$;




