-- Function definition script GetLastTemplateSentON for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetLastTemplateSentON(v_numConECampID NUMERIC(9,0),
                v_numDomainId       NUMERIC(9,0))
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcStartDate  VARCHAR(50);
BEGIN
   v_vcStartDate := '';
   select   FormatedDateFromDate(DTL.bintSentON,v_numDomainId) INTO v_vcStartDate FROM   ConECampaignDTL DTL WHERE  numConECampID = v_numConECampID AND bitSend=true ORDER BY DTL.bintSentON DESC LIMIT 1;
    
   RETURN v_vcStartDate;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[GetListIemName]    Script Date: 07/26/2008 18:13:04 ******/

