-- Stored procedure definition script customerCURD for PostgreSQL
CREATE OR REPLACE FUNCTION customerCURD(v_action CHAR(1),
	v_cusromerId INTEGER DEFAULT null,
	v_CompanyName VARCHAR(40) DEFAULT null,
	v_ContactName VARCHAR(30) DEFAULT null,
	v_newsletter BOOLEAN DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


-- select

   IF v_action = 'S' then
	
      open SWV_RefCur for SELECT Customers.CustomerID,
			Customers.CompanyName,
			Customers.ContactName,
			Customers.NewsLetters
      FROM
      Customers;
   end if;
END; $$;












