-- Stored procedure definition script USP_BankReconcileFileData_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BankReconcileFileData_Get(v_numReconcileID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM BankReconcileFileData WHERE numReconcileID = v_numReconcileID AND bitReconcile = false;
END; $$;












