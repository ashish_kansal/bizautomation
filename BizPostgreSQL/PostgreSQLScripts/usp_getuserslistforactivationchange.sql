-- Stored procedure definition script usp_GetUsersListForActivationChange for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetUsersListForActivationChange(v_DomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     U.numUserId,U.vcUserName,vcMailNickName, coalesce(U.vcEmailID,'') AS Email, U.vcUserDesc AS vcUserDesc,coalesce(U.vcEmailID,'') as vcEmailID,
                      U.bitactivateflag AS Active, 0 as activeflag
   FROM         UserMaster U INNER JOIN
   Domain D ON U.numDomainID = D.numDomainId where D.numDomainId = v_DomainID
   and U.bitactivateflag = true
   order by U.bitactivateflag desc;
END; $$;
