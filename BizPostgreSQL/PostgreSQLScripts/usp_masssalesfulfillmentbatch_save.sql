-- Stored procedure definition script USP_MassSalesFulfillmentBatch_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentBatch_Save(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_numBatchID NUMERIC(18,0)
	,v_vcBatchName TEXT
	,v_vcBatchRecords TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then -- ADD TO EXISTING BATCH
	
      IF NOT EXISTS(SELECT ID FROM MassSalesFulfillmentBatch WHERE numDomainID = v_numDomainID AND ID = v_numBatchID) then
		
         RAISE EXCEPTION 'INVALID_BATCH';
         RETURN;
      end if;
   ELSEIF v_tintMode = 2
   then -- CREATE NEW BATCH AND ADD
	
      IF(SELECT COUNT(*) FROM MassSalesFulfillmentBatch WHERE numDomainID = v_numDomainID AND coalesce(bitEnabled,false) = true) > 20 then
		
         RAISE EXCEPTION 'MAX_ACTIVE_BATCH_LIMIT_EXCEED';
         RETURN;
      ELSEIF(SELECT COUNT(*) FROM MassSalesFulfillmentBatch WHERE numDomainID = v_numDomainID AND vcName = v_vcBatchName) > 0
      then
		
         RAISE EXCEPTION 'BATCH_WITH_SAME_NAME_EXISTS';
         RETURN;
      ELSE
         INSERT INTO MassSalesFulfillmentBatch(numDomainID
				,numCreatedBy
				,dtCreatedDate
				,vcName)
			VALUES(v_numDomainID
				,v_numUserCntID
				,TIMEZONE('UTC',now())
				,v_vcBatchName);
      end if;
      v_numBatchID := CURRVAL('Table_seq');
   end if;

   IF v_tintMode = 4 then
	
      UPDATE MassSalesFulfillmentBatch SET bitEnabled = false WHERE ID = v_numBatchID;
   end if;
   IF v_tintMode = 3 then
	
      DELETE FROM MassSalesFulfillmentBatchOrders MSFBO using(SELECT
      CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC(18,0)) AS numOppID
				,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS NUMERIC(18,0)) AS numOppItemID
      FROM
      SplitString(v_vcBatchRecords,',')) TEMP where MSFBO.numOppID = TEMP.numOppID
      AND (coalesce(MSFBO.numOppItemID,0) = coalesce(TEMP.numOppItemID,0) OR coalesce(TEMP.numOppItemID,0) = 0) AND MSFBO.numBatchID = v_numBatchID;
   ELSE
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numOppID NUMERIC(18,0),
         numOppItemID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID)
      SELECT
      CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC(18,0)) AS numOppID
			,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS NUMERIC(18,0)) AS numOppItemID
      FROM
      SplitString(v_vcBatchRecords,',');
      DELETE FROM MassSalesFulfillmentBatchOrders MSFBO using tt_TEMP T1 where MSFBO.numOppID = T1.numOppID
      AND ((coalesce(MSFBO.numOppItemID,0) = 0 AND coalesce(T1.numOppItemID,0) > 0) OR (coalesce(MSFBO.numOppItemID,0) > 0 AND coalesce(T1.numOppItemID,0) = 0)) AND MSFBO.numBatchID = v_numBatchID;
      INSERT INTO MassSalesFulfillmentBatchOrders(numBatchID
			,numOppID
			,numOppItemID)
      SELECT
      v_numBatchID
			,numOppID
			,numOppItemID
      FROM
      tt_TEMP;
   end if;
END; $$;


