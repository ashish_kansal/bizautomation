-- Stored procedure definition script USP_ChartofChildAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE OR REPLACE FUNCTION USP_ChartofChildAccounts(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_strSortOn VARCHAR(11) DEFAULT NULL,
    v_strSortDirection VARCHAR(10) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtFinYearFrom  TIMESTAMP;
   v_dtFinYearTo  TIMESTAMP;
   v_CURRENTPL  DECIMAL(20,5);
   v_PLOPENING  DECIMAL(20,5);
   v_PLCHARTID  NUMERIC(8,0);
   v_TotalIncome  DECIMAL(20,5);
   v_TotalExpense  DECIMAL(20,5);
   v_TotalCOGS  DECIMAL(20,5);
   v_CurrentPL_COA  DECIMAL(20,5);
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SELECT   dtPeriodFrom INTO v_dtFinYearFrom FROM     FinancialYear WHERE    dtPeriodFrom <= LOCALTIMESTAMP
   AND dtPeriodTo >= LOCALTIMESTAMP
   AND numDomainId = v_numDomainId;
   select   MAX(datEntry_Date) INTO v_dtFinYearTo FROM General_Journal_Header WHERE numDomainId = v_numDomainId;


   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numAccountID NUMERIC(18,0),
      bitIsSubAccount BOOLEAN,
      bitActive BOOLEAN,
      vcAccountCode VARCHAR(300),
      numParentAccontID NUMERIC(18,0),
      OPENING DECIMAL(20,5),
      numDebitAmt DECIMAL(20,5),
      numCreditAmt DECIMAL(20,5)
   );

   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1
   (
      numAccountID NUMERIC(18,0),
      bitIsSubAccount BOOLEAN,
      bitActive BOOLEAN,
      vcAccountCode VARCHAR(300),
      numParentAccontID NUMERIC(18,0),
      OPENING DECIMAL(20,5),
      numDebitAmt DECIMAL(20,5),
      numCreditAmt DECIMAL(20,5)
   );

   INSERT INTO tt_TEMP(numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,OPENING,numDebitAmt,numCreditAmt) with recursive CTE(numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID) AS(SELECT
   numAccountId AS numAccountID
			,bitIsSubAccount AS bitIsSubAccount
			,vcAccountCode AS vcAccountCode
			,numParentAccId AS numParentAccontID
   FROM
   Chart_Of_Accounts
   WHERE
   numDomainId = v_numDomainId
   AND coalesce(numParentAccId,0) = 0
   UNION ALL
   SELECT
   COA.numAccountId AS numAccountID
			,COA.bitIsSubAccount AS bitIsSubAccount
			,COA.vcAccountCode AS vcAccountCode
			,COA.numParentAccId AS numParentAccontID
   FROM
   Chart_Of_Accounts COA
   INNER JOIN
   CTE C
   ON
   COA.numParentAccId = C.numAccountID) SELECT
   numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,0,0,0
   FROM
   CTE;

   WITH RECURSIVE CTE(numAccountID,numDebitAmt,numCreditAmt) AS(SELECT
   GJD.numChartAcntId AS numAccountID
			,coalesce(SUM(GJD.numDebitAmt),0) AS numDebitAmt
			,coalesce(SUM(GJD.numCreditAmt),0) AS numCreditAmt
   FROM
   General_Journal_Header GJH
   INNER JOIN
   General_Journal_Details GJD
   ON
   GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN
   tt_TEMP T
   ON
   GJD.numChartAcntId = T.numAccountID
   WHERE
   GJH.numDomainId = v_numDomainId
   AND datEntry_Date BETWEEN  v_dtFinYearFrom AND v_dtFinYearTo
   GROUP BY
   GJD.numChartAcntId)
   UPDATE
   tt_TEMP COA
   SET
   numDebitAmt = coalesce(C.numDebitAmt,0),numCreditAmt = coalesce(C.numCreditAmt,0)
   FROM
   CTE C WHERE COA.numAccountID = C.numAccountID;

   INSERT INTO tt_TEMP1  SELECT * FROM tt_TEMP;

   WITH RECURSIVE CTE(numAccountId,numDebitAmt,numCreditAmt,RootID) as(SELECT
   COA.numAccountID AS numAccountId,
			COA.numDebitAmt AS numDebitAmt,
			COA.numCreditAmt AS numCreditAmt,
			COA.numAccountID AS RootID
   FROM
   tt_TEMP COA
   UNION ALL
   SELECT
   COA.numAccountID AS numAccountId,
			COA.numDebitAmt AS numDebitAmt,
			COA.numCreditAmt AS numCreditAmt,
			C.RootID AS RootID
   FROM
   tt_TEMP COA
   INNER JOIN
   CTE C
   ON
   COA.numParentAccontID = C.numAccountID)
   UPDATE
   tt_TEMP COA
   SET
   numDebitAmt = coalesce(numDebitAmtIncludingChildren,0),numCreditAmt = coalesce(numCreditAmtIncludingChildren,0)
   FROM(select RootID,
                sum(numDebitAmt) as numDebitAmtIncludingChildren,
				sum(numCreditAmt) as numCreditAmtIncludingChildren
   from CTE C
   group by RootID) as S
   WHERE COA.numAccountID = S.RootID; 

   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      bitIsSubAccount BOOLEAN
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,CAST(COA.vcAccountName AS VARCHAR(250)),COA.numParntAcntTypeID,CAST(COA.vcAccountDescription AS VARCHAR(250)),COA.vcAccountCode,
	0 AS OPENING,
	coalesce(SUM(numDebitAmt),0) as DEBIT,
	coalesce(SUM(numCreditAmt),0) as CREDIT,
	coalesce(COA.bitIsSubAccount,false)
   FROM
   Chart_Of_Accounts COA
   LEFT JOIN
   tt_TEMP VJ
   ON
   COA.numAccountId = VJ.numAccountID
   WHERE
   COA.numDomainId = v_numDomainId
   AND COA.bitActive = true
   AND (COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%')
   GROUP BY
   COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,
   COA.vcAccountCode,COA.bitIsSubAccount;


   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
		coalesce(SUM(Opening),0) as Opening,
	coalesce(Sum(Debit),0) as Debit,coalesce(Sum(Credit),0) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
	(ATD.vcAccountCode ilike '0103%' OR
   ATD.vcAccountCode ilike '0104%' OR
   ATD.vcAccountCode ilike '0106%')
   WHERE
   PL.bitIsSubAccount = false
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


   v_CURRENTPL := 0;	
   v_PLOPENING := 0;

   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;

   select   coalesce(sum(numCreditAmt),0) -coalesce(sum(numDebitAmt),0) INTO v_CurrentPL_COA FROM tt_TEMP VJ WHERE numAccountID = v_PLCHARTID;

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalIncome FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0103');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalExpense FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0104');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalCOGS FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0106');

   RAISE NOTICE 'CurrentPL_COA = %',SUBSTR(CAST(v_CurrentPL_COA AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalIncome = %',SUBSTR(CAST(v_TotalIncome AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalExpense = %',SUBSTR(CAST(v_TotalExpense AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalCOGS = %',SUBSTR(CAST(v_TotalCOGS AS VARCHAR(20)),1,20);

   v_CURRENTPL := v_CurrentPL_COA+v_TotalIncome+v_TotalExpense+v_TotalCOGS; 
   RAISE NOTICE 'Current Profit/Loss = (Income - expense - cogs)= %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);

   v_CURRENTPL := v_CURRENTPL*(-1);

   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM VIEW_JOURNAL VJ WHERE numDomainID = v_numDomainId
   AND (vcAccountCode  ilike '0103%' or  vcAccountCode  ilike '0104%' or  vcAccountCode  ilike '0106%')
   AND datEntry_Date <=  v_dtFinYearFrom+INTERVAL '0 minute';

   select   coalesce(v_PLOPENING,0)+coalesce(sum(numCreditAmt),0) -coalesce(sum(numDebitAmt),0) INTO v_PLOPENING FROM General_Journal_Header GJH INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId WHERE GJH.numDomainId = v_numDomainId
   AND GJD.numChartAcntId = v_PLCHARTID AND datEntry_Date <=  v_dtFinYearFrom+INTERVAL '0 minute';

   v_CURRENTPL := v_CURRENTPL*(-1);

   RAISE NOTICE '@PLOPENING = %',SUBSTR(CAST(v_PLOPENING AS VARCHAR(20)),1,20);
   RAISE NOTICE '@CURRENTPL = %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);
   RAISE NOTICE '-(@PLOPENING + @CURRENTPL) = %',CAST((v_PLOPENING+v_CURRENTPL) AS VARCHAR(20));

   IF (v_strSortOn = 'ID' AND v_strSortDirection = 'ASCENDING') then
    
      open SWV_RefCur for
      SELECT  c.numAccountId,
                numAccountTypeID,
                c.numParntAcntTypeID,
                c.vcAccountCode,
                coalesce(c.vcAccountCode,'') || ' ~ '
      || c.vcAccountName AS vcAccountName,
                c.vcAccountName AS vcAccountName1,
                AT.vcAccountType,
                CASE WHEN (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%') THEN coalesce(t1.OPENING,0)
      WHEN c.vcAccountCode = '0105010101' THEN -(v_PLOPENING+v_CURRENTPL)
      ELSE(SELECT  SUM(COA.numOpeningBal) FROM Chart_Of_Accounts COA
            WHERE COA.numDomainId = v_numDomainId
            AND (COA.vcAccountCode ilike c.vcAccountCode OR (COA.vcAccountCode ilike c.vcAccountCode || '%' AND COA.numParentAccId > 0)))
      END AS numOpeningBal,
                coalesce(c.numParentAccId,0) AS numParentAccId,
                COA.vcAccountName AS vcParentAccountName,
                (SELECT MAX(intLevel) FROM Chart_Of_Accounts) AS intMaxLevel,
                coalesce(c.intLevel,1) AS intLevel
				,coalesce(c.bitActive,false) AS bitActive
      FROM    Chart_Of_Accounts c
      LEFT OUTER JOIN AccountTypeDetail AT ON AT.numAccountTypeID = c.numParntAcntTypeID
      LEFT OUTER JOIN Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
      LEFT JOIN LATERAL(SELECT   SUM(numDebitAmt -numCreditAmt) AS OPENING
         FROM     tt_TEMP1 VJ
         WHERE
         VJ.numAccountID = c.numAccountID
         AND (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%')) AS t1 on TRUE
      WHERE   c.numDomainId = v_numDomainId
        --AND c.bitActive = 0
      ORDER BY numAccountID ASC;
      open SWV_RefCur2 for
      SELECT  c.numAccountId,
				numAccountTypeID,
				c.numParntAcntTypeID,
				c.vcAccountCode,
				coalesce(c.vcAccountCode,'') || ' ~ ' || c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.vcAccountType,
				CASE WHEN c.numParntAcntTypeID IS NULL THEN NULL
      ELSE CASE WHEN (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%') THEN coalesce(t1.OPENING,0)
         WHEN c.vcAccountCode = '0105010101' THEN -(v_PLOPENING+v_CURRENTPL)
         ELSE(SELECT  SUM(COA.numOpeningBal) FROM Chart_Of_Accounts COA
               WHERE COA.numDomainId = v_numDomainId
               AND (COA.vcAccountCode ilike c.vcAccountCode OR (COA.vcAccountCode ilike c.vcAccountCode || '%' AND COA.numParentAccId > 0)))
         END
      END AS numOpeningBal,
				coalesce(c.numParentAccId,0) AS numParentAccId,
				COA.vcAccountName AS vcParentAccountName,
				(SELECT MAX(intLevel) FROM Chart_Of_Accounts) AS intMaxLevel,
				coalesce(c.intLevel,1) AS intLevel,
				coalesce(c.IsConnected,false) AS IsConnected
				,coalesce(c.numBankDetailID,0) AS numBankDetailID
				,coalesce(c.bitActive,false) AS bitActive
      FROM    Chart_Of_Accounts c
      LEFT OUTER JOIN AccountTypeDetail AT ON AT.numAccountTypeID = c.numParntAcntTypeID
      LEFT OUTER JOIN Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
      LEFT JOIN LATERAL(SELECT   SUM(numDebitAmt -numCreditAmt) AS OPENING
         FROM     tt_TEMP1 VJ
         WHERE VJ.numAccountID = c.numAccountID
         AND (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%')) AS t1 on TRUE
      WHERE   c.numDomainId = v_numDomainId
        --AND c.bitActive = 0
      ORDER BY c.numAccountId ASC;
   end if;  
  
   IF (v_strSortOn = 'DESCRIPTION' AND v_strSortDirection = 'ASCENDING') then
    
      open SWV_RefCur for
      SELECT  c.numAccountId,
				numAccountTypeID,
				c.numParntAcntTypeID,
				c.vcAccountCode,
				coalesce(c.vcAccountCode,'') || ' ~ ' || c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.vcAccountType,
				CASE WHEN c.numParntAcntTypeID IS NULL THEN NULL
      ELSE CASE WHEN (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%') THEN coalesce(t1.OPENING,0)
         WHEN c.vcAccountCode = '0105010101' THEN -(v_PLOPENING+v_CURRENTPL)
         ELSE(SELECT SUM(COA.numOpeningBal) FROM Chart_Of_Accounts COA
               WHERE COA.numDomainId = v_numDomainId
               AND (COA.vcAccountCode ilike c.vcAccountCode OR (COA.vcAccountCode ilike c.vcAccountCode || '%' AND COA.numParentAccId > 0)))
         END
      END AS numOpeningBal,
				coalesce(c.numParentAccId,0) AS numParentAccId,
				COA.vcAccountName AS vcParentAccountName,
				(SELECT MAX(intLevel) FROM Chart_Of_Accounts) AS intMaxLevel,
				coalesce(c.intLevel,1) AS intLevel,
				coalesce(c.IsConnected,false) AS IsConnected
				,coalesce(c.numBankDetailID,0) AS numBankDetailID
				,coalesce(c.bitActive,false) AS bitActive
      FROM    Chart_Of_Accounts c
      LEFT OUTER JOIN AccountTypeDetail AT ON AT.numAccountTypeID = c.numParntAcntTypeID
      LEFT OUTER JOIN Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
      LEFT JOIN LATERAL(SELECT   SUM(numDebitAmt -numCreditAmt) AS OPENING
         FROM  tt_TEMP1 VJ
         WHERE VJ.numAccountID = c.numAccountID
         AND (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%')) AS t1 on TRUE
      WHERE   c.numDomainId = v_numDomainId
      AND c.numParntAcntTypeID IS NULL
            --AND c.bitActive = 0
      ORDER BY AT.vcAccountCode,case SWF_IsNumeric(c.vcNumber) when true then REPEAT('0',50 -LENGTH(c.vcNumber)) || c.vcNumber else REPEAT('9',50) end  ASC;
      open SWV_RefCur2 for
      SELECT  c.numAccountId,
				numAccountTypeID,
				c.numParntAcntTypeID,
				c.vcAccountCode,
				coalesce(c.vcAccountCode,'') || ' ~ ' || c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.vcAccountType,
				CASE WHEN c.numParntAcntTypeID IS NULL THEN NULL
      ELSE  CASE WHEN (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%') THEN coalesce(t1.OPENING,0)
         WHEN c.vcAccountCode = '0105010101' THEN -(v_PLOPENING+v_CURRENTPL)
         ELSE(SELECT  SUM(COA.numOpeningBal) FROM Chart_Of_Accounts COA WHERE COA.numDomainId = v_numDomainId AND (COA.vcAccountCode ilike c.vcAccountCode OR (COA.vcAccountCode ilike c.vcAccountCode || '%' AND COA.numParentAccId > 0))) END
      END AS numOpeningBal,
				coalesce(c.numParentAccId,0) AS numParentAccId,
				COA.vcAccountName AS vcParentAccountName,
				(SELECT MAX(intLevel) FROM Chart_Of_Accounts) AS intMaxLevel,
				coalesce(c.intLevel,1) AS intLevel,
				coalesce(c.IsConnected,false) AS IsConnected
				,coalesce(c.numBankDetailID,0) AS numBankDetailID
				,coalesce(c.bitActive,false) AS bitActive
      FROM    Chart_Of_Accounts c
      LEFT OUTER JOIN AccountTypeDetail AT ON AT.numAccountTypeID = c.numParntAcntTypeID
      LEFT OUTER JOIN Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
      LEFT JOIN LATERAL(SELECT   SUM(numDebitAmt -numCreditAmt) AS OPENING
         FROM  tt_TEMP1 VJ
         WHERE   VJ.numAccountID = c.numAccountID
         AND (c.vcAccountCode ilike '0103%' OR c.vcAccountCode ilike '0104%' OR c.vcAccountCode ilike '0106%')) AS t1 on TRUE
      WHERE   c.numDomainId = v_numDomainId
      AND c.numParntAcntTypeID IS NOT NULL
                --AND c.bitActive = 0
      ORDER BY AT.vcAccountCode,case SWF_IsNumeric(c.vcNumber) when true then REPEAT('0',50 -LENGTH(c.vcNumber)) || c.vcNumber else REPEAT('9',50) end  ASC;
   end if;
   RETURN;
END; $$;


