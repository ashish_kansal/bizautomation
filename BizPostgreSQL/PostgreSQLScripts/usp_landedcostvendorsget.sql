-- Stored procedure definition script USP_LandedCostVendorsGet for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LandedCostVendorsGet(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numLCVendorId as "numLCVendorId", LCV.numDomainID as "numDomainID", numVendorId as "numVendorId", CI.vcCompanyName as "vcCompanyName"
   FROM   LandedCostVendors LCV JOIN DivisionMaster AS DM
   ON LCV.numVendorId = DM.numDivisionID
   JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
   WHERE LCV.numDomainID = v_numDomainId;
END; $$;














