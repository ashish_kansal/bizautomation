-- Stored procedure definition script USP_GetMetaTags for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMetaTags(v_numMetaID      NUMERIC(9,0)  DEFAULT 0,
                v_tintMetatagFor SMALLINT DEFAULT NULL, -- 1 for Page, 2 for Items,
                v_numReferenceID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numMetaID <> 0 then
    
      open SWV_RefCur for
      SELECT
      vcPageTitle,
			vcMetaKeywords,
			vcMetaDescription,
			numReferenceID
      FROM
      MetaTags
      WHERE
      tintMetatagFor = v_tintMetatagFor;
   ELSE
      open SWV_RefCur for
      SELECT
      vcPageTitle,
			vcMetaKeywords,
            vcMetaDescription,
            numMetaID
      FROM
      MetaTags
      WHERE
      numReferenceID = v_numReferenceID
      AND tintMetatagFor =  v_tintMetatagFor;
   end if;
   RETURN;
END; $$;



