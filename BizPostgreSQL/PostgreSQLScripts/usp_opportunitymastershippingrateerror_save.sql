-- Stored procedure definition script USP_OpportunityMasterShippingRateError_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMasterShippingRateError_Save(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_vcErrorMessage TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM OpportunityMasterShippingRateError WHERE numDomainID = v_numDomainID AND numOppID = v_numOppID) then
	
      UPDATE
      OpportunityMasterShippingRateError
      SET
      intNoOfTimeFailed = coalesce(intNoOfTimeFailed,0)+1,dtLastExecutedDate = TIMEZONE('UTC',now())
      WHERE
      numDomainID = v_numDomainID
      AND numOppID = v_numOppID;
   ELSE
      INSERT INTO OpportunityMasterShippingRateError(numDomainID
			,numOppID
			,intNoOfTimeFailed
			,vcErrorMessage
			,dtLastExecutedDate)
		VALUES(v_numDomainID
			,v_numOppID
			,1
			,v_vcErrorMessage
			,TIMEZONE('UTC',now()));
   end if;
   RETURN;
END; $$;


