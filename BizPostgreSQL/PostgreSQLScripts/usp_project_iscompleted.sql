-- Stored procedure definition script USP_Project_IsCompleted for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Project_IsCompleted(v_numDomainID NUMERIC(18,0)                            
	,v_numProId NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numProId FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = v_numProId) then
	
      open SWV_RefCur for
      SELECT 1;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;





