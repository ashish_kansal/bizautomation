-- FUNCTION: public.usp_getinventoryitemstatus(numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getinventoryitemstatus(numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getinventoryitemstatus(
	v_numdomainid numeric DEFAULT 0,
	v_numwarehouseid numeric DEFAULT NULL::numeric,
	v_numitemcode numeric DEFAULT NULL::numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_bitShowInStock  BOOLEAN;
   v_bitShowQuantity  BOOLEAN;
BEGIN

  --DECLARE  @tintColumns  AS TINYINT
   v_bitShowInStock := false;
   v_bitShowQuantity := false;
  --SET @tintColumns = 1
  
         --@tintColumns = Isnull(tintItemColumns,1)
   select   coalesce(bitShowInStock,false), coalesce(bitShowQOnHand,false) INTO v_bitShowInStock,v_bitShowQuantity FROM   eCommerceDTL WHERE  numDomainID = v_numDomainID;
  
  
   open SWV_RefCur for SELECT(CASE
   WHEN charItemType <> 'S' THEN(CASE
      WHEN v_bitShowInStock = true THEN(CASE
         WHEN numWareHouseID IS NULL THEN 'Out Of Stock'
         WHEN bitAllowBackOrder = true THEN 'In Stock'
            ||(CASE
            WHEN v_bitShowQuantity = true THEN '('
               || CAST(X.OnHand AS VARCHAR(20))
               || ')'
            ELSE ''
            END)
         ELSE(CASE
            WHEN cast(X.OnHand As Numeric) > 0 THEN 'In Stock'
               ||(CASE
               WHEN v_bitShowQuantity = true THEN '('
                  || CAST(X.OnHand AS VARCHAR(20))
                  || ')'
               ELSE ''
               END)
            ELSE 'Out Of Stock'
            END)
         END)
      ELSE ''
      END)
   ELSE ''
   END) AS InStock,
       cast(X.OnHand as VARCHAR(255)),
       cast(numWareHouseID as VARCHAR(255))
   FROM(SELECT   cast(numItemCode as TEXT),
                 cast(coalesce(SUM(numOnHand),0) as VARCHAR(255)) AS OnHand,
                 cast(charItemType as VARCHAR(255)),
                 bitAllowBackOrder,
                 numWareHouseID
      FROM     Item
      LEFT JOIN WareHouseItems W
      ON W.numItemID = numItemCode
      WHERE    Item.numDomainID = v_numDomainID
      AND W.numDomainID = v_numDomainID
      AND numItemCode = v_numItemCode
      AND (numWareHouseID = v_numWareHouseID
      OR v_numWareHouseID IS NULL)
      GROUP BY numItemCode,charItemType,bitAllowBackOrder,numWareHouseID) X
   ORDER BY X.OnHand DESC;
		
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getinventoryitemstatus(numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
