-- Stored procedure definition script USP_SaveUserStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SaveUserStatus(v_numUserID NUMERIC(9,0),    
 v_Active SMALLINT,    
 v_noOfLicences NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_Active = 1 then

      if v_noOfLicences >(select count(*) from UserMaster where bitactivateflag = true) then
 
         update UserMaster set
         bitactivateflag = v_Active
         where  numUserId = v_numUserID;
      end if;
   else
      update UserMaster set
      bitactivateflag = v_Active
      where  numUserId = v_numUserID;
   end if;  
   open SWV_RefCur for select count(*) from UserMaster where bitactivateflag = true;
END; $$;












