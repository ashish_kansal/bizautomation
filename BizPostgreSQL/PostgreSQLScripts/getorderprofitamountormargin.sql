-- Function definition script GetOrderProfitAmountOrMargin for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOrderProfitAmountOrMargin(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_tintMode SMALLINT --1: Profit Amount, 2:Profit Margin
)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Value  DECIMAL(20,5);
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_ProfitCost  INTEGER;
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID, numCost INTO v_numShippingItemID,v_numDiscountItemID,v_ProfitCost FROM Domain WHERE numDomainId = v_numDomainID;

   If v_tintMode = 1 then
	
      select   SUM(coalesce(monTotAmount,0) -(coalesce(numUnitHour,0)*(CASE
      WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
      THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
      ELSE(CASE v_ProfitCost
         WHEN 3 THEN coalesce(V.monCost,0)*fn_UOMConversion(numBaseUnit,I.numItemCode,v_numDomainID,numPurchaseUnit)
         ELSE coalesce(OI.monAvgCost,0)
         END)
      END))) INTO v_Value FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode 
		 AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 LIMIT 1) TEMPMatchedPO ON TRUE
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OI.numItemCode
         AND OIInner.vcAttrValues = OI.vcAttrValues LIMIT 1) TEMPPO on TRUE
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numOppId = v_numOppID
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID);
   ELSEIF v_tintMode = 2
   then
	
      select(SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END)*100 INTO v_Value FROM(SELECT
         coalesce(monTotAmount,0) AS monTotAmount
				,coalesce(monTotAmount,0) -(coalesce(numUnitHour,0)*(CASE
         WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
         THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
         ELSE(CASE v_ProfitCost
            WHEN 3 THEN coalesce(V.monCost,0)*fn_UOMConversion(numBaseUnit,I.numItemCode,v_numDomainID,numPurchaseUnit)
            ELSE coalesce(OI.monAvgCost,0)
            END)
         END)) AS Profit
         FROM
         OpportunityItems OI
         INNER JOIN
         OpportunityMaster OM
         ON
         OI.numOppId = OM.numOppId
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         INNER JOIN
         CompanyInfo CI
         ON
         DM.numCompanyID = CI.numCompanyId
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         LEFT JOIN LATERAL(SELECT 
            OMInner.numOppId
					,OMInner.vcpOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
            FROM
            SalesOrderLineItemsPOLinking SOLIPL
            INNER JOIN
            OpportunityMaster OMInner
            ON
            SOLIPL.numPurchaseOrderID = OMInner.numOppId
            INNER JOIN
            OpportunityItems OIInner
            ON
            OMInner.numOppId = OIInner.numOppId
            AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
            WHERE
            SOLIPL.numSalesOrderID = OM.numOppId
            AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode 
			AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 LIMIT 1) TEMPMatchedPO on TRUE
         LEFT JOIN LATERAL(SELECT 
            OMInner.numOppId
					,OMInner.vcpOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
            FROM
            OpportunityLinking OL
            INNER JOIN
            OpportunityMaster OMInner
            ON
            OL.numChildOppID = OMInner.numOppId
            INNER JOIN
            OpportunityItems OIInner
            ON
            OMInner.numOppId = OIInner.numOppId
            WHERE
            OL.numParentOppID = OM.numOppId
            AND OIInner.numItemCode = OI.numItemCode
            AND OIInner.vcAttrValues = OI.vcAttrValues LIMIT 1) TEMPPO  on TRUE
         Left JOIN
         Vendor V
         ON
         V.numVendorID = I.numVendorID
         AND V.numItemCode = I.numItemCode
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.numOppId = v_numOppID
         AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;
   end if;

   RETURN v_Value; -- Set Accuracy of Two precision
END; $$;

