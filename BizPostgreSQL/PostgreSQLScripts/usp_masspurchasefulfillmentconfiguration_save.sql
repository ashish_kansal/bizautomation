-- Stored procedure definition script USP_MassPurchaseFulfillmentConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillmentConfiguration_Save(v_numDomainID NUMERIC(18,0)
    ,v_numUserCntID NUMERIC(18,0)
	,v_bitGroupByOrderForReceive BOOLEAN
	,v_bitGroupByOrderForPutAway BOOLEAN
	,v_bitGroupByOrderForBill BOOLEAN
	,v_bitGroupByOrderForClose BOOLEAN
	,v_bitShowOnlyFullyReceived BOOLEAN
	,v_bitReceiveBillOnClose BOOLEAN
	,v_tintScanValue SMALLINT
	,v_tintBillType SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
	
      UPDATE
      MassPurchaseFulfillmentConfiguration
      SET
      bitGroupByOrderForReceive = v_bitGroupByOrderForReceive,bitGroupByOrderForPutAway = v_bitGroupByOrderForPutAway,
      bitGroupByOrderForBill = v_bitGroupByOrderForBill,
      bitGroupByOrderForClose = v_bitGroupByOrderForClose,bitShowOnlyFullyReceived = v_bitShowOnlyFullyReceived,
      bitReceiveBillOnClose = v_bitReceiveBillOnClose,
      tintScanValue = v_tintScanValue,tintBillType = v_tintBillType
      WHERE
      numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntID;
   ELSE
      INSERT INTO MassPurchaseFulfillmentConfiguration(numDomainID
			,numUserCntID
			,bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway
			,bitGroupByOrderForBill
			,bitGroupByOrderForClose
			,bitShowOnlyFullyReceived
			,bitReceiveBillOnClose
			,tintScanValue
			,tintBillType)
		VALUES(v_numDomainID
			,v_numUserCntID
			,v_bitGroupByOrderForReceive
			,v_bitGroupByOrderForPutAway
			,v_bitGroupByOrderForBill
			,v_bitGroupByOrderForClose
			,v_bitShowOnlyFullyReceived
			,v_bitReceiveBillOnClose
			,v_tintScanValue
			,v_tintBillType);
   end if;
   RETURN;
END; $$;


