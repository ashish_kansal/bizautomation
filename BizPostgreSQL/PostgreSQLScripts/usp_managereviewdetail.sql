-- Stored procedure definition script USP_ManageReviewDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageReviewDetail(v_numReviewId NUMERIC(9,0) DEFAULT 0,    
v_numContactId NUMERIC(18,0) DEFAULT NULL ,
v_bitAbuse BOOLEAN DEFAULT NULL ,
v_bitHelpful BOOLEAN DEFAULT NULL ,
v_Mode INTEGER DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM ReviewDetail WHERE numReviewId = v_numReviewId AND numContactId = v_numContactId) then
			
      INSERT INTO ReviewDetail (numReviewId,bitAbuse,bitHelpful,numContactId)
					 VALUES(v_numReviewId ,
					  v_bitAbuse ,
					  v_bitHelpful ,
					  v_numContactId);
   ELSE
      IF v_Mode = 0 then
				   
         UPDATE ReviewDetail SET  bitAbuse = v_bitAbuse WHERE numContactId = v_numContactId AND numReviewId = v_numReviewId;
      ELSEIF v_Mode = 1
      then
				   
         UPDATE ReviewDetail SET  bitHelpful = v_bitHelpful WHERE numContactId = v_numContactId AND numReviewId = v_numReviewId;
      end if;
   end if;
   RETURN;
END; $$;

	
--SELECT AVG(CONVERT(NUMERIC (9,0) , intRatingCount)) FROM Ratings
--UPDATE Ratings SET intRatingCount = 4 WHERE numRatingId = 1
--SELECT * FROM ReviewDetail

--INSERT INTO Ratings VALUES(2,'19017',1,20,'255.124.13.1',92,1 ,2012-10-03)


