-- Stored procedure definition script USP_UpdateInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateInventory(v_numDomainID NUMERIC(9,0),
v_numItemCode NUMERIC(9,0),
v_numOnHand DOUBLE PRECISION,
v_numWarehouseID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Update WareHouseItems W set numOnHand = v_numOnHand,dtModified = LOCALTIMESTAMP
   from Item I
   where W.numItemID = I.numItemCode AND(I.numDomainID = v_numDomainID AND W.numDomainID = v_numDomainID and numWareHouseID = v_numWarehouseID
   and I.numItemCode = v_numItemCode);
   RETURN;
END; $$;

