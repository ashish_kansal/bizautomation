-- Stored procedure definition script USP_GetCaseListForPortal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCaseListForPortal(v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_tintSortOrder NUMERIC DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintUserRightType SMALLINT DEFAULT NULL,
    v_SortChar CHAR(1) DEFAULT '0',
    v_CustName VARCHAR(100) DEFAULT '',
    v_FirstName VARCHAR(100) DEFAULT '',
    v_LastName VARCHAR(100) DEFAULT '',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,
    v_bitPartner BOOLEAN DEFAULT false,
    v_vcRegularSearchCriteria VARCHAR(1000) DEFAULT '',
    v_vcCustomSearchCriteria VARCHAR(1000) DEFAULT '',
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_numStatus BIGINT DEFAULT 0,
	v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                            
   v_lastRec  INTEGER;                                                            
   v_strSql  TEXT;
   v_column  VARCHAR(50);
   v_join  VARCHAR(400);          
   v_lookbckTable  VARCHAR(50);              
   v_strShareRedordWith  VARCHAR(300);
   v_tintOrder  SMALLINT;                                                  
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcListItemType1  VARCHAR(1);                                                 
   v_vcAssociatedControlType  VARCHAR(20);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_vcDbColumnName  VARCHAR(20);                      
   v_WhereCondition  VARCHAR(2000);                       
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;          
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  CHAR(1);   
   v_bitAllowEdit  CHAR(1);                 
   v_vcColumnName  VARCHAR(500);                  
               
   v_Nocolumns  SMALLINT;                
   v_ListRelID  NUMERIC(9,0);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   IF v_tintMode = 0 then


    --DROP TABLE #tempColorScheme
      v_column := v_columnName;
      v_join := '';
      v_lookbckTable := '';
      IF v_column ilike 'CFW.Cust%' then
        
         v_column := 'CFW.Fld_Value';
         v_join := ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','');
      end if;
      IF v_column ilike 'DCust%' then
        
         v_column := 'LstCF.vcData';
         v_join := ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= ' || REPLACE(v_columnName,'DCust','');
         v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value';
      end if;
      v_strShareRedordWith := ' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' AND SR.numModuleID = 7 AND SR.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ') ';
      v_strSql := 'WITH tblcases AS (SELECT  ';
      IF v_tintSortOrder = 3
      OR v_tintSortOrder = 4 then
         v_strSql := coalesce(v_strSql,'') || ' TOP 20 ';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || CASE WHEN coalesce(POSITION(substring(v_column from 'TEXT') IN v_column),0) > 0
      THEN ' CONVERT(VARCHAR(MAX),' || coalesce(v_column,'') || ')'
      ELSE v_column
      END || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,Cs.numCaseID,
							  COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount                                                                             
							  FROM                
							  AdditionalContactsInformation ADC                                                             
							  JOIN Cases Cs ON ADC.numContactId = Cs.numContactId                                                             
							  JOIN DivisionMaster Div ON ADC.numDivisionId = Div.numDivisionID ';
      IF v_bitPartner = true then
         v_strSql := coalesce(v_strSql,'')
         || ' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '';
      end if;
      v_strSql := coalesce(v_strSql,'')
      || ' JOIN CompanyInfo CMP ON Div.numCompanyID = CMP.numCompanyId                                                             
			LEFT JOIN listdetails lst ON lst.numListItemID = cs.numPriority                       
			LEFT JOIN listdetails lst1 ON lst1.numListItemID = cs.numStatus                                                           
			LEFT JOIN AdditionalContactsInformation ADC1 ON Cs.numAssignedTo = ADC1.numContactId ' || coalesce(v_join,'') || '                                                            
			WHERE cs.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';
      IF v_numStatus <> 0 then
        
         v_strSql := coalesce(v_strSql,'') || ' AND cs.numStatus = ' || SUBSTR(CAST(v_numStatus AS VARCHAR(10)),1,10);
      ELSE
         v_strSql := coalesce(v_strSql,'') || ' AND cs.numStatus <> 136 ';
      end if;
      IF v_FirstName <> '' then
         v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
      end if;
      IF v_LastName <> '' then
         v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
      end if;
      IF v_CustName <> '' then
         v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
      end if;
      IF v_numDivisionID <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And div.numDivisionID = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
      end if;
      IF v_SortChar <> '0' then
         v_strSql := coalesce(v_strSql,'') || ' And (ADC.vcFirstName like ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      IF v_SortChar <> '0' then
         v_strSql := coalesce(v_strSql,'') || ' or ADC.vcLastName like ''' || coalesce(v_SortChar,'') || '%'')';
      end if;
      IF v_tintUserRightType = 1 then
         v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15)
         || ' or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15)
         || CASE WHEN v_bitPartner = true
         THEN ' or (CCont.bitPartner = true AND CCont.numContactId = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')'
         ELSE ''
         END || ' OR ' || coalesce(v_strShareRedordWith,'') || ')';
      ELSEIF v_tintUserRightType = 2
      then
         v_strSql := coalesce(v_strSql,'')
         || ' AND (Div.numTerID IN (select numTerritoryID from  UserTerritory where numUserCntID = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15)
         || ' ) OR Div.numTerID = 0 or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
      IF v_numDivisionID = 0 then
        
         IF v_tintSortOrder = 0 then
            v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15)
            || ' or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
         end if;
      end if;
      IF v_tintSortOrder = 2 then
         v_strSql := coalesce(v_strSql,'') || 'And cs.numStatus <> 136  ' || ' AND cs.bintCreatedDate> ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20))
         || '''';
      ELSEIF v_tintSortOrder = 3
      then
         v_strSql := coalesce(v_strSql,'') || 'And cs.numStatus <> 136  ' || ' and cs.numCreatedBy = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
      ELSEIF v_tintSortOrder = 4
      then
         v_strSql := coalesce(v_strSql,'') || 'And cs.numStatus <> 136  ' || ' and cs.numModifiedBy = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
      ELSEIF v_tintSortOrder = 5
      then
         v_strSql := coalesce(v_strSql,'') || 'And cs.numStatus = 136  ' || ' and cs.numModifiedBy = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
      end if;
      IF v_vcRegularSearchCriteria <> '' then
         v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
      end if;
      IF v_vcCustomSearchCriteria <> '' then
         v_strSql := coalesce(v_strSql,'') || ' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' || coalesce(v_vcCustomSearchCriteria,'') || ')';
      end if;
      v_strSql := coalesce(v_strSql,'') || ')';
      v_tintOrder := 0;
      v_WhereCondition := '';
      v_Nocolumns := 0;
    
                        --AND tintPageType = 1
      select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT    COUNT(*) AS TotalRow
         FROM      View_DynamicColumns
         WHERE     numFormId = 80
         AND numUserCntID = v_numUserCntId
         AND numDomainID = v_numDomainID
                        --AND tintPageType = 1
         UNION
         SELECT    COUNT(*) AS TotalRow
         FROM      View_DynamicCustomColumns
         WHERE     numFormId = 80
         AND numUserCntID = v_numUserCntId
         AND numDomainID = v_numDomainID) TotalRows;
      DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFORM
      (
         tintOrder SMALLINT,
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldName VARCHAR(50),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC(9,0),
         vcLookBackTableName VARCHAR(50),
         bitCustomField BOOLEAN,
         numFieldId NUMERIC,
         bitAllowSorting BOOLEAN,
         bitAllowEdit BOOLEAN,
         bitIsRequired BOOLEAN,
         bitIsEmail BOOLEAN,
         bitIsAlphaNumeric BOOLEAN,
         bitIsNumeric BOOLEAN,
         bitIsLengthValidation BOOLEAN,
         intMaxLength INTEGER,
         intMinLength INTEGER,
         bitFieldMessage BOOLEAN,
         vcFieldMessage VARCHAR(500),
         ListRelID NUMERIC(9,0),
         intColumnWidth INTEGER,
         bitAllowFiltering BOOLEAN,
         vcFieldDataType CHAR(1)
      );
      v_strSql := coalesce(v_strSql,'')
      || ' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,DM.tintCRMType,cs.numCaseID,RowNumber';
      IF v_Nocolumns = 0 then
        
         INSERT  INTO DycFormConfigurationDetails(numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth)
         SELECT  80,
                            numFieldId,
                            0,
                            Row_number() OVER(ORDER BY tintRow DESC),
                            v_numDomainID,
                            v_numUserCntId,
                            NULL,
                            1,
                            0,
                            intColumnWidth
         FROM    View_DynamicDefaultColumns
         WHERE   numFormId = 80
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID
         ORDER BY tintOrder ASC;
      end if;
      INSERT  INTO tt_TEMPFORM
      SELECT  tintRow+1 AS tintOrder,
                    vcDbColumnName,
                    vcOrigDbColumnName,
                    coalesce(vcCultureFieldName,vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitInlineEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage AS vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    bitAllowFiltering,
                    vcFieldDataType
      FROM    View_DynamicColumns
      WHERE   numFormId = 80
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      UNION
      SELECT  tintRow+1 AS tintOrder,
                    vcDbColumnName,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    false,
                    ''
      FROM    View_DynamicCustomColumns
      WHERE   numFormId = 80
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      ORDER BY tintOrder ASC;  
           
--set @DefaultNocolumns=  @Nocolumns              
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM    ORDER BY tintOrder ASC LIMIT 1;
      WHILE v_tintOrder > 0 LOOP  
			--PRINT @tintOrder
			--PRINT @vcDbColumnName    
			--PRINT @vcAssociatedControlType                                           
         IF v_bitCustom = false then
            IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
               v_Prefix := 'ADC.';
            end if;
            IF v_vcLookBackTableName = 'DivisionMaster' then
               v_Prefix := 'DM.';
            end if;
            IF v_vcLookBackTableName = 'Cases' then
               v_Prefix := 'Cs.';
            end if;      
--					IF @vcLookBackTableName = 'OpportunityMaster'
--						SET @PreFix = 'Opp.'  
    
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
            RAISE NOTICE '%',v_vcColumnName;
            IF v_vcAssociatedControlType = 'SelectBox' then
                        
               IF v_vcListItemType = 'LI' then
                                
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'S'
               then
                                    
                  v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'T'
               then
                                        
                  v_strSql := coalesce(v_strSql,'') || ',L'
                  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
                  || '.vcData' || ' ['
                  || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join ListDetails L'
                  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
                  || ' on L'
                  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
                  || '.numListItemID='
                  || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'U'
               then
                                            
                  v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcColumnName,'') || ']';
               end if;
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
                            
               IF v_vcDbColumnName = 'intTargetResolveDate' then
                                    
                  v_strSql := coalesce(v_strSql,'')
                  || ',case when convert(varchar(11),'
                  || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
                  v_strSql := coalesce(v_strSql,'')
                  || 'when convert(varchar(11),'
                  || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strSql := coalesce(v_strSql,'')
                  || 'when convert(varchar(11),'
                  || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strSql := coalesce(v_strSql,'')
                  || 'else dbo.FormatedDateFromDate('
                  || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ','
                  || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
                  || ') end  [' || coalesce(v_vcColumnName,'') || ']';
               ELSE
                  v_strSql := coalesce(v_strSql,'')
                  || ',case when convert(varchar(11),DateAdd(minute, '
                  || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
                  || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
                  v_strSql := coalesce(v_strSql,'')
                  || 'when convert(varchar(11),DateAdd(minute, '
                  || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
                  || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strSql := coalesce(v_strSql,'')
                  || 'when convert(varchar(11),DateAdd(minute, '
                  || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
                  || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strSql := coalesce(v_strSql,'')
                  || 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                  || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
                  || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
                  || '),'
                  || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
                  || ') end  [' || coalesce(v_vcColumnName,'') || ']';
               end if;
            ELSEIF v_vcAssociatedControlType = 'TextBox'
            then
                                
               v_strSql := coalesce(v_strSql,'') || ','
               || CASE WHEN v_vcDbColumnName = 'numAge'
               THEN 'year(getutcdate())-year(bintDOB)'
               WHEN v_vcDbColumnName = 'numDivisionID'
               THEN 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'textSubject'
               THEN 'convert(varchar(max),textSubject)'
               WHEN v_vcDbColumnName = 'textDesc'
               THEN 'convert(varchar(max),textDesc)'
               WHEN v_vcDbColumnName = 'textInternalComments'
               THEN 'convert(varchar(max),textInternalComments)'
               ELSE v_vcDbColumnName
               END || ' [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcAssociatedControlType = 'TextArea'
            then
                                    
               v_strSql := coalesce(v_strSql,'') || ', '
               || coalesce(v_vcDbColumnName,'') || ' ['
               || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcAssociatedControlType = 'Popup'
            AND v_vcDbColumnName = 'numShareWith'
            then
                                        
               v_strSql := coalesce(v_strSql,'')
               || ',(SELECT SUBSTRING(
													(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
													FROM ShareRecord SR 
													JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID 
													WHERE SR.numDomainID = Cs.numDomainID 
													AND SR.numModuleID = 7 
													AND SR.numRecordID = Cs.numCaseID
													AND UM.numDomainID = Cs.numDomainID 
													AND UM.numDomainID = A.numDomainID FOR XML PATH('''')),2,200000)) ['
               || coalesce(v_vcColumnName,'') || ']';
            end if;
         ELSE        
       
--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                         
--   where  CFW_Fld_Master.Fld_Id = @numFieldId                                    
       
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
            IF v_vcAssociatedControlType = 'TextBox'
            OR v_vcAssociatedControlType = 'TextArea' then
                        
               v_strSql := coalesce(v_strSql,'') || ',CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value  [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Case CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '         
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=cs.numCaseId   ';
            ELSEIF v_vcAssociatedControlType = 'CheckBox'
            then
                            
               v_strSql := coalesce(v_strSql,'')
               || ',case when isnull(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,0)=1 then ''Yes'' end   ['
               || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Case CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '           
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=cs.numCaseId   ';
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
                                
               v_strSql := coalesce(v_strSql,'')
               || ',dbo.FormatedDateFromDate(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,'
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ')  [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Case CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '         
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=cs.numCaseId   ';
            ELSEIF v_vcAssociatedControlType = 'SelectBox'
            then
                                    
               v_vcDbColumnName := 'DCust'
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
               v_strSql := coalesce(v_strSql,'') || ',L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.vcData' || ' [' || coalesce(v_vcColumnName,'')
               || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Case CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '         
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=cs.numCaseId   ';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join ListDetails L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numListItemID=CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value';
            end if;
         end if;
         RAISE NOTICE '%',v_tintOrder -1;                                    
			---SELECT TOP 1 * FROM  #tempForm  WHERE   tintOrder > @tintOrder - 1 ORDER BY tintOrder ASC      
			
         select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
         v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
         v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM WHERE   tintOrder > v_tintOrder -1   ORDER BY tintOrder ASC LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_tintOrder := 0;
         end if;
      END LOOP;                       
                                                                   
      v_strSql := coalesce(v_strSql,'')
      || ' ,TotalRowCount FROM                                                               
   AdditionalContactsInformation ADC                                                             
 JOIN Cases  Cs                                                              
   ON ADC.numContactId =Cs.numContactId                                                             
 JOIN DivisionMaster DM                                                              
  ON ADC.numDivisionId = DM.numDivisionID                                                             
  JOIN CompanyInfo CMP                                                             
 ON DM.numCompanyID = CMP.numCompanyId                                                             
 left join listdetails lst                                                            
 on lst.numListItemID= cs.numPriority                                                  
     ' || coalesce(v_WhereCondition,'') || '                              
 join tblcases T on T.numCaseID=Cs.numCaseID';                                                            
             
      v_strSql := coalesce(v_strSql,'') || ' WHERE RowNumber > '
      || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <'
      || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' order by RowNumber';
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
      open SWV_RefCur for
      SELECT * FROM    tt_TEMPFORM;
    
   ELSE
      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      drop table IF EXISTS tt_TEMPTABLE CASCADE;
      Create TEMPORARY TABLE tt_TEMPTABLE 
      ( 
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         numCaseID VARCHAR(15)
      );                                            
	                                             
	--declare @strSql as varchar(5000)                                            
      v_strSql := 'SELECT  ';
      if v_tintSortOrder = 3 or v_tintSortOrder = 4 then  
         v_strSql := coalesce(v_strSql,'') || ' top 20 ';
      end if;
      v_strSql := coalesce(v_strSql,'') || '                                             
	   Cs.numCaseId                                                             
	 FROM                                               
	   AdditionalContactsInformation ADC                                             
	 JOIN Cases  Cs                                              
	   ON ADC.numContactId = Cs.numContactId                                             
	 JOIN DivisionMaster Div                                               
	  ON ADC.numDivisionId = Div.numDivisionID';
      if v_bitPartner = true then 
         v_strSql := coalesce(v_strSql,'') || ' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' JOIN CompanyInfo CMP                                             
	 ON Div.numCompanyID = CMP.numCompanyId                                             
	 left join ListDetails lst                                            
	 on lst.numListItemID = Cs.numPriority       
	 left join ListDetails lst1                                            
	 on lst1.numListItemID = Cs.numStatus                                                                
	 left join AdditionalContactsInformation ADC1                                            
	 on Cs.numAssignedTo = ADC1.numContactId                                            
	 Where                                            
	 Cs.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';
      IF v_numStatus > 0 then 
--	 set @strSql=@strSql+ 'and Cs.numStatus = '+ CONVERT(VARCHAR(15),@numStatus) 
--	ELSE
         v_strSql := coalesce(v_strSql,'') || 'and Cs.numStatus<>136 ';
      end if;
      if v_FirstName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
      end if;
      if v_LastName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
      end if;
      if v_CustName <> '' then 
         v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
      end if;
      if v_numDivisionID <> 0 then 
         v_strSql := coalesce(v_strSql,'') || ' And div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
      end if;
      if v_SortChar <> '0' then 
         v_strSql := coalesce(v_strSql,'') || ' And (ADC.vcFirstName like ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      if v_SortChar <> '0' then 
         v_strSql := coalesce(v_strSql,'') || ' or ADC.vcLastName like ''' || coalesce(v_SortChar,'') || '%'')';
      end if;
      if v_tintUserRightType = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) ||
         case when v_bitPartner = true then ' or (CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')' else '' end  || ')';
      ELSEIF v_tintUserRightType = 2
      then 
         v_strSql := coalesce(v_strSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' ) or Div.numTerID=0 or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '
	) ';
      end if;
      if v_numDivisionID = 0 then
	
         if v_tintSortOrder = 0 then  
            v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')';
         end if;
      end if;
      if v_tintSortOrder = 2 then  
         v_strSql := coalesce(v_strSql,'') || ' AND cs.bintCreatedDate> ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
      ELSEIF v_tintSortOrder = 3
      then  
         v_strSql := coalesce(v_strSql,'') || ' and cs.numCreatedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' and cs.numModifiedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
      end if;
      if v_tintSortOrder = 0 then 
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 1
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 2
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 3
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY cs.bintCreatedDate ';
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY cs.bintModifiedDate ';
      end if;
      RAISE NOTICE '%',v_strSql;
      EXECUTE 'insert into tt_TEMPTABLE (numCaseID)                                            
	' || v_strSql;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      open SWV_RefCur for
      SELECT   ADC.vcFirstName || ' ' || ADC.vcLastname as Name,
	   CMP.vcCompanyName,
	   Cs.numCaseId,
	   Cs.vcCaseNumber,
	   Cs.intTargetResolveDate,
	   Cs.numCreatedby,
	   Cs.textSubject,
	   lst.vcData as Priority,
	   CMP.numCompanyId,
	   Div.numDivisionID,
	   Div.numTerID,
	   Div.tintCRMType,
	   ADC.numContactId as ActCntID,
	   ADC1.vcFirstName || ' ' || ADC1.vcLastname || '/ ' || fn_GetContactName(Cs.numAssignedBy)  as AssignedToBy,
	   fn_GetListItemName(Cs.numStatus) as Status,
	   Cs.numRecOwner
      FROM
      AdditionalContactsInformation ADC
      JOIN Cases  Cs
      ON ADC.numContactId = Cs.numContactId
      JOIN DivisionMaster Div
      ON ADC.numDivisionId = Div.numDivisionID
      JOIN CompanyInfo CMP
      ON Div.numCompanyID = CMP.numCompanyId
      left join Listdetails lst
      on lst.numListItemID = Cs.numPriority
      left join AdditionalContactsInformation ADC1
      on Cs.numAssignedTo = ADC1.numContactId
      join tt_TEMPTABLE T on T.numCaseID = Cs.numCaseId
      where ID > v_firstRec and ID < v_lastRec;
      select count(*) INTO v_TotRecs from tt_TEMPTABLE;
   end if;
   RETURN;
END; $$;



