-- Stored procedure definition script USP_DiscountCodes_Validate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DiscountCodes_Validate(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numDivisionId NUMERIC(18,0)
	,v_txtCouponCode VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC(18,0) DEFAULT 0;
   v_numProfile  NUMERIC(18,0) DEFAULT 0;
   v_numPromotionID  NUMERIC(18,0);
   v_bitOrderBasedPromotion  BOOLEAN DEFAULT false;
   v_bitPromotionNeverExpires  BOOLEAN;
   v_dtPromotionStartDate  TIMESTAMP;
   v_dtPromotionEndDate  TIMESTAMP;
   v_intUsageLimit  INTEGER;
   v_numDisocuntID  NUMERIC(18,0);
   v_bitUseForCouponManagement  BOOLEAN;
BEGIN
   IF coalesce(v_numDivisionId,0) > 0 then
	
      select   coalesce(numCompanyType,0), coalesce(vcProfile,0) INTO v_numRelationship,v_numProfile FROM
      DivisionMaster
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
      numDivisionID = v_numDivisionId;
   ELSEIF coalesce(v_numSiteID,0) > 0
   then
	
      select   coalesce(numRelationshipId,0), coalesce(numProfileId,0) INTO v_numRelationship,v_numProfile FROM
      eCommerceDTL WHERE
      numSiteId = v_numSiteID;
   end if;

	

	-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
   select   PO.numProId, coalesce(DC.CodeUsageLimit,0), coalesce(bitNeverExpires,false), PO.dtValidFrom, PO.dtValidTo, numDiscountId, coalesce(PO.IsOrderBasedPromotion,false), coalesce(bitUseForCouponManagement,false) INTO v_numPromotionID,v_intUsageLimit,v_bitPromotionNeverExpires,v_dtPromotionStartDate,
   v_dtPromotionEndDate,v_numDisocuntID,v_bitOrderBasedPromotion,
   v_bitUseForCouponManagement FROM
   PromotionOffer PO
   INNER JOIN
   PromotionOfferOrganizations PORG
   ON
   PO.numProId = PORG.numProId
   INNER JOIN
   DiscountCodes DC
   ON
   PO.numProId = DC.numPromotionID WHERE
   PO.numDomainId = v_numDomainID
   AND 1 =(CASE WHEN numRelationship = v_numRelationship AND numProfile = v_numProfile THEN 1 ELSE 0 END)
   AND DC.vcDiscountCode = v_txtCouponCode;

   IF coalesce(v_numPromotionID,0) > 0 then
	
		-- NOW CHECK IF PROMOTION EXPIRES OR NOT
      IF coalesce(v_bitPromotionNeverExpires,false) = false then
		
         IF (v_dtPromotionStartDate <= TIMEZONE('UTC',now()) AND v_dtPromotionEndDate >= TIMEZONE('UTC',now())) then
			
            IF coalesce(v_intUsageLimit,0) <> 0 then
				
               IF coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId = v_numDisocuntID AND numDivisionId = v_numDivisionId),0) >= v_intUsageLimit then
					
                  RAISE EXCEPTION 'COUPON_USAGE_LIMIT_EXCEEDED';
                  RETURN;
               end if;
            end if;
         ELSE
            RAISE EXCEPTION 'COUPON_CODE_EXPIRED';
            RETURN;
         end if;
      end if;
   ELSE
      RAISE EXCEPTION 'INVALID_COUPON_CODE';
      RETURN;
   end if;

   open SWV_RefCur for SELECT
   v_numPromotionID AS numPromotionID
		,v_numDisocuntID AS numDisocuntID
		,v_txtCouponCode AS vcCouponCode
		,v_bitOrderBasedPromotion AS bitOrderBasedPromotion
		,v_bitUseForCouponManagement AS bitUseForCouponManagement;
END; $$;












