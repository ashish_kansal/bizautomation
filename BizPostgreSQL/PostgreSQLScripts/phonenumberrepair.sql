-- Function definition script PhoneNumberRepair for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION PhoneNumberRepair(v_FieldValue VARCHAR(25))
RETURNS VARCHAR(25) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StrippedText  VARCHAR(25);
   v_LenFieldValue  INTEGER;
   v_Looper  INTEGER;
   v_CharCnt  INTEGER;
   v_NoOfTimes  INTEGER;
   v_CurrentCharacter  VARCHAR(1);
   v_AreaCode  VARCHAR(3);
   v_Prefix  VARCHAR(3);
   v_Suffix  VARCHAR(4);
BEGIN
   v_LenFieldValue := LENGTH(v_FieldValue);
   v_Looper := 1;
   v_StrippedText := '';
   v_CharCnt := 0;
   v_NoOfTimes := 0;

   While v_Looper <= v_LenFieldValue LOOP
      v_CurrentCharacter := SUBSTR(v_FieldValue,v_Looper,1);
      If SWF_IsNumeric(v_CurrentCharacter) = true and not v_CurrentCharacter = '-' then
                        
         If v_StrippedText = '' then
				
            v_StrippedText := v_CurrentCharacter;
         Else
            If v_CharCnt >= 3 and v_NoOfTimes < 2 then
					
               v_StrippedText := coalesce(v_StrippedText,'') || '-' || coalesce(v_CurrentCharacter,'');
               v_CharCnt := 0;
               v_NoOfTimes := v_NoOfTimes::bigint+1;
            Else
               v_StrippedText := coalesce(v_StrippedText,'') || coalesce(v_CurrentCharacter,'');
            end if;
         end if;
      end if;
      v_Looper := v_Looper::bigint+1;
      v_CharCnt := v_CharCnt::bigint+1;
   END LOOP;

   Return v_StrippedText;
END; $$;

