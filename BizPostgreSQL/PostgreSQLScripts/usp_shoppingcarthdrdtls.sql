-- Stored procedure definition script USP_ShoppingcartHDRDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShoppingcartHDRDtls(v_numItemCode NUMERIC(9,0) DEFAULT 0,    
v_byteMode SMALLINT DEFAULT 0 ,
v_txtDesc   TEXT DEFAULT NULL,
v_numUserCntID NUMERIC DEFAULT NULL,
v_txtShortDesc TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      SELECT coalesce(txtDesc,'') AS txtDesc from ItemExtendedDetails where numItemCode = v_numItemCode;
   ELSEIF v_byteMode = 1
   then

      if not exists(select * from ItemExtendedDetails  where numItemCode = v_numItemCode) then
         insert into ItemExtendedDetails
	 values(v_numItemCode,v_txtDesc,v_txtShortDesc);
      else
         update ItemExtendedDetails set txtDesc = v_txtDesc,txtShortDesc = v_txtShortDesc where numItemCode = v_numItemCode;
      end if;
      UPDATE Item SET bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode;
   ELSEIF v_byteMode = 2
   then

      open SWV_RefCur for
      SELECT coalesce(txtDesc,'') AS txtDesc,coalesce(txtShortDesc,'') AS txtShortDesc  from ItemExtendedDetails where numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$;


