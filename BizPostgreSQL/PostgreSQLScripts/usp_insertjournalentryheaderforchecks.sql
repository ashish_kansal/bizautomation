-- Stored procedure definition script USP_InsertJournalEntryHeaderForChecks for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryHeaderForChecks(v_datEntry_Date TIMESTAMP,                                
v_numAmount DECIMAL(20,5),                              
v_numJournal_Id NUMERIC(9,0) DEFAULT 0,                          
v_numDomainId NUMERIC(9,0) DEFAULT 0,                  
v_numCheckId NUMERIC(9,0) DEFAULT 0,                
v_numRecurringId NUMERIC(9,0) DEFAULT 0,              
v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numRecurringId = 0 then 
      v_numRecurringId := null;
   end if;                                
   If v_numJournal_Id = 0 then
  
      Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numCheckId,numRecurringId,numCreatedBy,datCreatedDate)
   Values(v_datEntry_Date,v_numAmount,v_numDomainId,v_numCheckId,v_numRecurringId,v_numUserCntID,TIMEZONE('UTC',now()));
   
      v_numJournal_Id := CURRVAL('General_Journal_Header_seq');
      open SWV_RefCur for Select v_numJournal_Id;
   end if;
END; $$;












