-- Stored procedure definition script USP_AccountDiagnosis for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================  
-- Author:  <Manish Anjara>  
-- Create date: <Wednesday, 3rd Sep, 2014>  
-- Description: <This procedure will be used for complete accounting diagnosis>  
-- =============================================  
-- EXEC USP_AccountDiagnosis 169
CREATE OR REPLACE FUNCTION USP_AccountDiagnosis(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor , INOUT SWV_RefCur6 refcursor , INOUT SWV_RefCur7 refcursor , INOUT SWV_RefCur8 refcursor , INOUT SWV_RefCur9 refcursor , INOUT SWV_RefCur10 refcursor , INOUT SWV_RefCur11 refcursor , INOUT SWV_RefCur12 refcursor , INOUT SWV_RefCur13 refcursor , INOUT SWV_RefCur14 refcursor , INOUT SWV_RefCur15 refcursor , INOUT SWV_RefCur16 refcursor , INOUT SWV_RefCur17 refcursor , INOUT SWV_RefCur18 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcARAccountID  TEXT;
   v_vcARAccountCode  TEXT;
   v_vcAPAccountID  TEXT;
   v_vcAPAccountCode  TEXT;
   v_vcDiff  TEXT;
   v_ARJournalTotal  NUMERIC(19,4);
   v_ARTransactionTotal  NUMERIC(19,4);

-- JOURNAL ENTRIES TOTAL
   v_AuthoritativeSalesBizDocId  INTEGER;
   v_InvoiceTotal  NUMERIC(19,4);
   v_OtherAREntriesTotal  NUMERIC(19,4);
   v_InvoicedDepositTotal  NUMERIC(19,4);
   v_DirectDepositTotal  NUMERIC(19,4);
   v_RefundReturnTotal  NUMERIC(19,4);

   v_InvoiceTotalAsPerTransaction  NUMERIC(19,4);
   v_InvoiceTotalAsPerGL  NUMERIC(19,4);

   v_OtherAREntriesTotalAsPerTransaction  NUMERIC(19,4);
   v_OtherAREntriesTotalAsPerGL  NUMERIC(19,4);

   v_InvoicedDepositTotalAsPerTransaction  NUMERIC(19,4);
   v_InvoicedDepositTotalAsPerGL  NUMERIC(19,4);

   v_RefundReturnTotalAsPerTransaction  NUMERIC(19,4);
   v_RefundReturnTotalAsPerGL  NUMERIC(19,4);

   v_DirectDepositTotalAsPerTransaction  NUMERIC(19,4);
   v_DirectDepositTotalAsPerGL  NUMERIC(19,4);

   v_ARDifference  TEXT;
   v_APJournalTotal  NUMERIC(19,4);
   v_APTransactionTotal  NUMERIC(19,4);
   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_BillPaymentTotal  NUMERIC(19,4);
   v_POAuthoritativeTotal  NUMERIC(19,4);
   v_BillTotal  NUMERIC(19,4);
   v_CheckTotal  NUMERIC(19,4);
   v_OtherAPEntriesTotal  NUMERIC(19,4);
   v_CommissionTotal  NUMERIC(19,4);
   v_RefundReturnForAPTotal  NUMERIC(19,4);

   v_POAuthoritativeTotalAsPerTransaction  NUMERIC(19,4);
   v_BillTotalAsPerTransaction  NUMERIC(19,4);
   v_CheckTotalAsPerTransaction  NUMERIC(19,4);
   v_OtherAPEntriesTotalAsPerTransaction  NUMERIC(19,4);
   v_CommissionTotalAsPerTransaction  NUMERIC(19,4);
   v_RefundReturnForAPTotalAsPerTransaction  NUMERIC(19,4);
   v_BillPaymentTotalAsPerTransaction  NUMERIC(19,4);

   v_POAuthoritativeTotalAsPerGL  NUMERIC(19,4);
   v_BillTotalAsPerGL  NUMERIC(19,4);
   v_CheckTotalAsPerGL  NUMERIC(19,4);
   v_OtherAPEntriesTotalAsPerGL  NUMERIC(19,4);
   v_CommissionTotalAsPerGL  NUMERIC(19,4);
   v_RefundReturnForAPTotalAsPerGL  NUMERIC(19,4);
   v_BillPaymentTotalAsPerGL  NUMERIC(19,4);

   v_APDifference  TEXT;
BEGIN
   v_vcARAccountCode := '01010105';

   v_vcAPAccountCode := '01020102';

   v_vcDiff := '';

--DECLARE @numDomainID NUMERIC(18, 0)
--SET @numDomainID = 143
   open SWV_RefCur for
   SELECT 'General' AS Title,'General' AS TableName,'1. General details for all tables.' AS vcDesc
   UNION ALL
   SELECT 'GL Details' AS Title,'GLDetails' AS TableName,'1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). Given table will get records from General Header table which records have not General Detail records.' AS vcDesc
   UNION ALL
   SELECT 'Missed Transaction Details' AS Title,'TransactionDetails' AS TableName,'2. Check details of every transactions whether they are in respective tables or not.'
   UNION ALL
   SELECT 'Not Matching Credit' AS Title,'NotMatching_Credit' AS TableName,'4. List entries if General header amount is not matching Total Credit value of General Details table'
   UNION ALL
   SELECT 'Not Matching Debit' AS Title,'NotMatching_Debit' AS TableName,'5. List entries if General header amount is not matching Total Debit value of General Details table'
   UNION ALL
   SELECT 'AR Aging Summary' AS Title,'ARAgingSummary' AS TableName,'6. AR Aging Summary'
   UNION ALL
   SELECT 'AR Aging Details' AS Title,'ARAgingDetails' AS TableName,'7. AR Aging Details'
   UNION ALL
   SELECT 'Invoice To GL Mapping' AS Title,'InvoiceToGLMapping' AS TableName,'8. Check whether any Invoiced transaction are in general entries or not (1)'
   UNION ALL
   SELECT 'GL To Invoice Mapping' AS Title,'GLToInvoiceMapping' AS TableName,'9. Check whether any general entries are in Invoiced transactions or not (2).'
   UNION ALL
   SELECT 'AP Aging Summary' AS Title,'APAgingSummary' AS TableName,'10. AP Aging Summary'
   UNION ALL
   SELECT 'AP Aging Detail' AS Title,'APAgingDetail' AS TableName,'11. AP Aging Details'
   UNION ALL
   SELECT 'Differences' AS Title,'Differences' AS TableName,'12. Differences in result'
   UNION ALL
   SELECT 'Null AR GLs' AS Title,'Null_ARGLs' AS TableName,'13. Check whether any of the Accounts Receivable accounts have null CustomerID in GL or not'
   UNION ALL
   SELECT 'Null AP GLs' AS Title,'Null_APGLs' AS TableName,'14. Check whether any of the Accounts Payable accounts have null CustomerID in GL or not'
   UNION ALL
   SELECT 'SameAccount AR GLs' AS Title,'SameAccount_ARGLs' AS TableName,'15. AR - Check whether accounting entries are not in same account types. (E.g single order entry may not credit and debit from same account type)'
   UNION ALL
   SELECT 'SameAccount AP GLs' AS Title,'SameAccount_APGLs' AS TableName,'16. AP - Check whether accounting entries are not in same account types. (E.g single order entry may not credit and debit from same account type)'
   UNION ALL
   SELECT 'Compare GL To Transactions By DivisionID For AR' AS Title,'CompareGLToTransactionsByDivisionIDAR' AS TableName,'17. Compare GL and Transactions group by DivisionID for AR'
   UNION ALL
   SELECT 'Compare GL To Transactions By DivisionID For AP' AS Title,'CompareGLToTransactionsByDivisionIDAP' AS TableName,'18. Compare GL and Transactions group by DivisionID for AP';


-- BASICS TO STICK TO :
-- 1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). 
--    Given query will get records from General Header table which records have not General Detail records

   RAISE NOTICE '1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). ';
   RAISE NOTICE 'Given query will get records from General Header table which records have not General Detail records';

   open SWV_RefCur2 for
   SELECT * FROM General_Journal_Header AS GJH
   WHERE GJH.numDomainId = v_numDomainID
   AND GJH.numJOurnal_Id NOT IN(SELECT GJD.numJournalId FROM General_Journal_Details AS GJD)
   ORDER BY GJH.datEntry_Date DESC;

-- 2. Check details of every transactions whether they are in respective tables or not.
   RAISE NOTICE '2. Check details of every transactions whether they are in respective tables or not.';

   DROP TABLE IF EXISTS tt_MISSEDTRANSACTIONDETAILS CASCADE;
   CREATE TEMPORARY TABLE tt_MISSEDTRANSACTIONDETAILS
   (	
      TransactionType  VARCHAR(50),
      numJournalId  NUMERIC(18,0),
      numOppId  NUMERIC(18,0),
      numOppBizDocsId  NUMERIC(18,0),
      numBillID  NUMERIC(18,0),
      vcComments VARCHAR(100),
      numBillPaymentID  NUMERIC(18,0),
      numBizDocsPaymentDetId  NUMERIC(18,0),
      numCheckHeaderID  NUMERIC(18,0),
      numDepositId  NUMERIC(18,0),
      numReturnID  NUMERIC(18,0)
   );

   INSERT INTO tt_MISSEDTRANSACTIONDETAILS 

   SELECT CAST('Bills' AS VARCHAR(50)) AS TransactionType,  GJH.numJOurnal_Id,GJH.numOppId,GJH.numOppBizDocsId,GJH.numBillID,CAST('GL records not in Bills' AS VARCHAR(100)) AS Comments, GJH.numBillPaymentID,GJH.numBizDocsPaymentDetId,GJH.numCheckHeaderID, GJH.numDepositId,GJH.numReturnID
   FROM General_Journal_Header AS GJH
   WHERE GJH.numDomainId = v_numDomainID
   AND coalesce(GJH.numBillID,0) > 0
   AND GJH.numBillID NOT IN(SELECT BH.numBillID FROM BillHeader AS BH WHERE BH.numDomainId = v_numDomainID AND coalesce(BH.numOppId,0) = 0)
   UNION ALL
   SELECT CAST('Bills' AS VARCHAR(50)) AS TransactionType, CAST(0 AS NUMERIC(18,0)) AS numJournal_Id,CAST(0 AS NUMERIC(18,0)) AS numOppId,CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,CAST(0 AS NUMERIC(18,0)) AS numBillID,CAST('Bills not in GL' AS VARCHAR(100)) AS Comments, CAST(0 AS NUMERIC(18,0)) AS numBillPaymentID,CAST(0 AS NUMERIC(18,0)) AS numBizDocsPaymentDetId,CAST(0 AS NUMERIC(18,0)) AS numCheckHeaderID, CAST(0 AS NUMERIC(18,0)) AS numDepositId,CAST(0 AS NUMERIC(18,0)) AS numReturnID
   FROM BillHeader AS BH
   WHERE BH.numDomainId = v_numDomainID
   AND coalesce(BH.numOppId,0) = 0
   AND BH.numBillID NOT IN(SELECT  GJH.numBillID FROM General_Journal_Header AS GJH WHERE GJH.numDomainId = v_numDomainID AND coalesce(GJH.numBillID,0) > 0)
   UNION ALL
   SELECT CAST('Deposits' AS VARCHAR(50)) AS TransactionType,  GJH.numJOurnal_Id,GJH.numOppId,GJH.numOppBizDocsId,GJH.numBillID,CAST('GL records not in Deposits' AS VARCHAR(100)) AS Comments, GJH.numBillPaymentID,GJH.numBizDocsPaymentDetId,GJH.numCheckHeaderID, GJH.numDepositId,GJH.numReturnID
   FROM General_Journal_Header AS GJH
   JOIN DepositMaster AS DM2 ON DM2.numDepositId = GJH.numDepositId AND DM2.tintDepositePage IN(2,3) AND GJH.numDomainId = DM2.numDomainId
   WHERE GJH.numDomainId = v_numDomainID
   AND coalesce(GJH.numDepositId,0) > 0
   AND DM2.tintDepositePage IN(2,3)
   AND GJH.numDepositId NOT IN(SELECT numDepositId FROM DepositMaster AS DM WHERE DM.numDomainId = v_numDomainID AND DM.tintDepositePage IN(2,3))
   UNION ALL
   SELECT CAST('Deposits' AS VARCHAR(50)) AS TransactionType, CAST(0 AS NUMERIC(18,0)) AS numJournal_Id,CAST(0 AS NUMERIC(18,0)) AS numOppId,CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,CAST(0 AS NUMERIC(18,0)) AS numBillID,CAST('Deposits not in GL' AS VARCHAR(100)) AS Comments, CAST(0 AS NUMERIC(18,0)) AS numBillPaymentID,CAST(0 AS NUMERIC(18,0)) AS numBizDocsPaymentDetId, CAST(0 AS NUMERIC(18,0)) AS numCheckHeaderID, DM.numDepositId AS numDepositId,CAST(0 AS NUMERIC(18,0)) AS numReturnID
   FROM DepositMaster AS DM WHERE DM.numDomainId = v_numDomainID AND DM.tintDepositePage IN(2,3)
   AND DM.numDepositId NOT IN(SELECT  GJH.numDepositId FROM General_Journal_Header AS GJH WHERE GJH.numDomainId = v_numDomainID AND coalesce(GJH.numDepositId,0) > 0)
   AND DM.tintDepositePage IN(2,3)
   UNION ALL
   SELECT CAST('Returns' AS VARCHAR(50)) AS TransactionType,  GJH.numJOurnal_Id,GJH.numOppId,GJH.numOppBizDocsId,GJH.numBillID,CAST('GL records not in Returns' AS VARCHAR(100)) AS Comments, GJH.numBillPaymentID,GJH.numBizDocsPaymentDetId,GJH.numCheckHeaderID, GJH.numDepositId,GJH.numReturnID FROM General_Journal_Header AS GJH
   WHERE GJH.numDomainId = v_numDomainID
   AND coalesce(GJH.numReturnID,0) > 0
   AND GJH.numReturnID NOT IN(SELECT RH.numReturnHeaderID FROM ReturnHeader AS RH WHERE RH.numDomainID = v_numDomainID)
   UNION ALL
   SELECT CAST('Returns' AS VARCHAR(50)) AS TransactionType, CAST(0 AS NUMERIC(18,0)) AS numJournal_Id,CAST(0 AS NUMERIC(18,0)) AS numOppId,CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,CAST(0 AS NUMERIC(18,0)) AS numBillID,CAST('Returns not in GL' AS VARCHAR(100)) AS Comments, CAST(0 AS NUMERIC(18,0)) AS numBillPaymentID,CAST(0 AS NUMERIC(18,0)) AS numBizDocsPaymentDetId, CAST(0 AS NUMERIC(18,0)) AS numCheckHeaderID, CAST(0 AS NUMERIC(18,0)) AS numDepositId, RH.numReturnHeaderID AS numReturnID
   FROM ReturnHeader AS RH WHERE RH.numDomainID = v_numDomainID
   AND RH.numReturnHeaderID NOT IN(SELECT  GJH.numReturnID FROM General_Journal_Header AS GJH WHERE GJH.numDomainId = v_numDomainID AND coalesce(GJH.numReturnID,0) > 0);

-- Execute Select Statement For [#MissedTransactionDetails]
   open SWV_RefCur3 for
   SELECT * FROM tt_MISSEDTRANSACTIONDETAILS;


--- List entries if General header amount is not matching Total Credit value of General Details table
   RAISE NOTICE '--- List entries if General header amount is not matching Total Credit value of General Details table--';
   open SWV_RefCur4 for
   SELECT  GJH.numJOurnal_Id,GJH.numAmount, SUM(GJD.numCreditAmt) AS TotalCredit, GJH.numAmount -SUM(GJD.numCreditAmt) AS Diff
   FROM    General_Journal_Header AS GJH
   LEFT JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId
   WHERE GJH.numDomainId = v_numDomainID
   GROUP BY GJH.numJOurnal_Id,GJH.numDomainId,GJH.numAmount
   HAVING(GJH.numAmount -SUM(GJD.numCreditAmt)) <> 0
   ORDER BY(GJH.numAmount -SUM(GJD.numCreditAmt)) DESC;

--- List entries if General header amount is not matching Total Debit value of General Details table
   RAISE NOTICE 'List entries if General header amount is not matching Total Debit value of General Details table';
   open SWV_RefCur5 for
   SELECT  GJH.numJOurnal_Id,GJH.numAmount, SUM(GJD.numDebitAmt) AS TotalDebit, GJH.numAmount -SUM(GJD.numDebitAmt) AS Diff
   FROM    General_Journal_Header AS GJH
   LEFT JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId
   WHERE GJH.numDomainId = v_numDomainID
   GROUP BY GJH.numJOurnal_Id,GJH.numDomainId,GJH.numAmount
   HAVING(GJH.numAmount -SUM(GJD.numDebitAmt)) <> 0
   ORDER BY(GJH.numAmount -SUM(GJD.numDebitAmt)) DESC;


---- Accounts Receivable Diagnosis
--==========================================================================================
   RAISE NOTICE '---- Accounts Receivable Diagnosis';
   RAISE NOTICE '-- JOURNAL ENTRIES TOTAL';
   select   SUM(coalesce(GJD.numDebitAmt,0)) -SUM(coalesce(GJD.numCreditAmt,0)) INTO v_ARJournalTotal FROM    General_Journal_Header AS GJH
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId WHERE   1 = 1
   AND GJH.numDomainId = v_numDomainID
   AND GJD.numChartAcntId IN(SELECT  numAccountId
      FROM    Chart_Of_Accounts AS COA
      WHERE   COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
      AND COA.numDomainId = GJH.numDomainId);

   IF coalesce(v_ARJournalTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_ARJournalTotal := CAST(0 AS NUMERIC(19,4));
   end if;
   RAISE NOTICE '@ARJournalTotal : %',SUBSTR(CAST(v_ARJournalTotal AS VARCHAR(18)),1,18);

-- TRANSACTIONS TOTAL
   RAISE NOTICE '-- TRANSACTIONS TOTAL';
   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM    AuthoritativeBizDocs WHERE   numDomainId = v_numDomainID;

   IF coalesce(v_AuthoritativeSalesBizDocId,0) = 0 then
      v_AuthoritativeSalesBizDocId := 0;
   end if;
   RAISE NOTICE '@AuthoritativeSalesBizDocId : %',SUBSTR(CAST(v_AuthoritativeSalesBizDocId AS VARCHAR(18)),1,18);

   select   SUM(TABLE1.DealAmount) INTO v_ARTransactionTotal FROM(SELECT  coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount
      FROM    OpportunityMaster Opp
      JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
      LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
      JOIN DivisionMaster AS DIV ON DIV.numDivisionID = Opp.numDivisionId
      WHERE   tintopptype = 1
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND numBizDocId = v_AuthoritativeSalesBizDocId
      AND OB.bitAuthoritativeBizDocs = 1
      AND coalesce(OB.tintDeferred,0) <> 1
		--AND Opp.[numDivisionId] = 217158

      UNION ALL --Show Impact of AR journal entries in report as well 

      SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt
      ELSE -1*GJD.numCreditAmt
      END
      FROM    General_Journal_Header GJH
      INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      JOIN DivisionMaster AS DIV ON DIV.numDivisionID = GJD.numCustomerId
      WHERE   GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
      AND coalesce(numOppId,0) = 0
      AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJH.numDepositId,0) = 0
      AND coalesce(GJH.numReturnID,0) = 0
      UNION ALL -- Deposits

      SELECT  CAST((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/ )*-1 AS DOUBLE PRECISION)
      FROM    DepositMaster DM
      JOIN DivisionMaster AS DIV ON DM.numDivisionID = DIV.numDivisionID
      WHERE   DM.numDomainId = v_numDomainID
      AND tintDepositePage IN(2,3)
      AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 
      UNION ALL --Standalone Refund against Account Receivable

      SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END
      FROM    ReturnHeader RH
      JOIN DivisionMaster AS DIV ON DIV.numDivisionID = RH.numDivisionId
      JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
      AND  monBizDocAmount > 0 
      AND coalesce(RH.numBillPaymentIDRef,0) = 0) TABLE1;

   IF coalesce(v_ARTransactionTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_ARTransactionTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   RAISE NOTICE '@ARTransactionTotal : %',SUBSTR(CAST(v_ARTransactionTotal AS VARCHAR(18)),1,18);

-- AR Aging Summary
   RAISE NOTICE '-- AR Aging Summary';
   open SWV_RefCur6 for
   SELECT v_ARJournalTotal AS ARJournalTotal,
	   v_ARTransactionTotal AS ARTransactionTotal,
	   v_ARJournalTotal -v_ARTransactionTotal AS Difference,
	   (CASE WHEN(v_ARJournalTotal -v_ARTransactionTotal) = 0 THEN 'Pass' ELSE 'Failed' END) AS Result;

   IF(v_ARJournalTotal -v_ARTransactionTotal) <> 0 then

      v_vcDiff := coalesce(v_vcDiff,'') || '@ARJournalTotal - @ARTransactionTotal : ' || CAST((v_ARJournalTotal -v_ARTransactionTotal) AS VARCHAR(100));
   end if;


-- AR Aging Details
   RAISE NOTICE '-- AR Aging Details';
   SELECT  SUM(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) INTO v_InvoiceTotal FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = Opp.numDivisionId
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numDomainID
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1;
   SELECT  SUM((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/ )*-1) INTO v_InvoicedDepositTotal FROM    DepositMaster DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID
   JOIN OpportunityMaster AS OM ON OM.numOppId = DD.numOppID AND OM.numDomainId = DM.numDomainId
   JOIN OpportunityBizDocs AS OBD ON OBD.numoppid = OM.numOppId AND OBD.numOppBizDocsId = DD.numOppBizDocsID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = DM.numDivisionID
   WHERE   DM.numDomainId = v_numDomainID
   AND tintDepositePage IN(2,3)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;
   SELECT  SUM((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))*-1) INTO v_DirectDepositTotal FROM    DepositMaster DM
   JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = DM.numDivisionID
   WHERE   DM.numDomainId = v_numDomainID
   AND tintDepositePage IN(1)
   AND coalesce(DD.numOppID,0) = 0 AND coalesce(DD.numOppBizDocsID,0) = 0
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;
   SELECT  SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END) INTO v_OtherAREntriesTotal FROM    General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = GJD.numCustomerId
   WHERE   GJH.numDomainId = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
   AND coalesce(numOppId,0) = 0
   AND coalesce(numOppBizDocsId,0) = 0
   AND coalesce(GJH.numDepositId,0) = 0
   AND coalesce(GJH.numReturnID,0) = 0;
   SELECT  SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END) INTO v_RefundReturnTotal FROM    ReturnHeader RH
   JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
   JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = RH.numDivisionId
   WHERE   RH.tintReturnType = 4
   AND RH.numDomainID = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
   AND  monBizDocAmount > 0 
   AND coalesce(RH.numBillPaymentIDRef,0) = 0;

   IF coalesce(v_InvoiceTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoiceTotal := CAST(0 AS NUMERIC(19,4));
   end if;
   IF coalesce(v_InvoicedDepositTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoicedDepositTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_DirectDepositTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_DirectDepositTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_OtherAREntriesTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_OtherAREntriesTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   DROP TABLE IF EXISTS tt_ARAGINTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_ARAGINTABLE ON COMMIT DROP AS
      SELECT * FROM(SELECT 'AR : Invoice' AS TransactionType,v_InvoiceTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 1 AS "Order"
      UNION
      SELECT 'AR : Manual Journal Entries' AS TransactionType,v_OtherAREntriesTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 5 AS "Order"
      UNION
      SELECT 'AR : Invoiced Deposit' AS TransactionType,v_InvoicedDepositTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS Diff, 2 AS "Order"
      UNION
      SELECT 'AR : Make Deposit' AS TransactionType,v_DirectDepositTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS Diff, 3 AS "Order"
      UNION
      SELECT 'AR : Refunds' AS TransactionType,v_RefundReturnTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 4 AS "Order") AS ARAginTable;

   select   SUM(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)*(-1)) INTO v_InvoicedDepositTotalAsPerTransaction FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID AND tintDepositePage IN(2,3)
   JOIN OpportunityMaster AS OM ON OM.numOppId = DD.numOppID AND DM.numDomainId = OM.numDomainId
   JOIN OpportunityBizDocs AS OBD ON OM.numOppId = OBD.numoppid AND DD.numOppBizDocsID = OBD.numOppBizDocsId
   LEFT JOIN General_Journal_Header AS GJH ON DM.numDepositId = GJH.numDepositId AND DM.numDomainId = GJH.numDomainId WHERE DM.numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OBD.bitAuthoritativeBizDocs = 1
   AND coalesce(OBD.tintDeferred,0) <> 1
   AND tintDepositePage IN(2,3)
   AND coalesce(DM.numReturnHeaderID,0) = 0
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;

   select   SUM(GJD.numCreditAmt) -SUM(coalesce(monAppliedAmount,0)*(-1)) INTO v_InvoicedDepositTotalAsPerGL FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID AND tintDepositePage IN(2,3)
   JOIN General_Journal_Header AS GJH ON DM.numDepositId = GJH.numDepositId AND DM.numDomainId = GJH.numDomainId
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId WHERE DM.numDomainId = v_numDomainID
   AND tintDepositePage IN(2,3)
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;

   select   SUM(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)*(-1)) INTO v_DirectDepositTotalAsPerTransaction FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID  AND tintDepositePage IN(1) WHERE DM.numDomainId = v_numDomainID
   AND tintDepositePage IN(1)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;

   select   SUM(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)*(-1)) INTO v_DirectDepositTotalAsPerGL FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID  AND tintDepositePage IN(1)
   JOIN General_Journal_Header AS GJH ON DM.numDepositId = GJH.numDepositId AND DM.numDomainId = GJH.numDomainId WHERE DM.numDomainId = v_numDomainID
   AND tintDepositePage IN(1)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0;

   select   SUM(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) INTO v_InvoiceTotalAsPerTransaction FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   JOIN General_Journal_Header AS GJH ON Opp.numOppId = GJH.numOppId AND OB.numOppBizDocsId = GJH.numOppBizDocsId AND GJH.numDomainId = Opp.numDomainId WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numDomainID
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND coalesce(GJH.numReturnID,0) = 0;

   select   SUM(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0)) -SUM(coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) INTO v_InvoiceTotalAsPerGL FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   JOIN General_Journal_Header AS GJH ON Opp.numOppId = GJH.numOppId AND OB.numOppBizDocsId = GJH.numOppBizDocsId AND Opp.numDomainId = GJH.numDomainId
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numDomainID
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%';
		--AND ISNULL([GJH].[numReturnID],0) = 0

   v_OtherAREntriesTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));

   select   SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END) INTO v_OtherAREntriesTotalAsPerGL FROM    General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId WHERE   GJH.numDomainId = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
   AND coalesce(numOppId,0) = 0
   AND coalesce(numOppBizDocsId,0) = 0
   AND coalesce(GJH.numDepositId,0) = 0
   AND coalesce(GJH.numReturnID,0) = 0;

   select   SUM(RH.monBizDocAmount) INTO v_RefundReturnTotalAsPerTransaction FROM    ReturnHeader RH WHERE   RH.tintReturnType = 4
   AND RH.numDomainID = v_numDomainID
   AND  monBizDocAmount > 0 
   AND coalesce(RH.numBillPaymentIDRef,0) = 0;

   select   SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END) INTO v_RefundReturnTotalAsPerGL FROM    ReturnHeader RH
   JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
   JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId WHERE   RH.tintReturnType = 4
   AND RH.numDomainID = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
   AND  monBizDocAmount > 0 
   AND coalesce(RH.numBillPaymentIDRef,0) = 0;

   IF coalesce(v_InvoicedDepositTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoicedDepositTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_InvoicedDepositTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoicedDepositTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_DirectDepositTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_DirectDepositTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_DirectDepositTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_DirectDepositTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_InvoiceTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoiceTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_InvoiceTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_InvoiceTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_OtherAREntriesTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_OtherAREntriesTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_OtherAREntriesTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_OtherAREntriesTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   RAISE NOTICE '@InvoicedDepositTotalAsPerTransaction : %',SUBSTR(CAST(v_InvoicedDepositTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@InvoicedDepositTotalAsPerGL : %',SUBSTR(CAST(v_InvoicedDepositTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@DirectDepositTotalAsPerTransaction : %',SUBSTR(CAST(v_DirectDepositTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@DirectDepositTotalAsPerGL : %',SUBSTR(CAST(v_DirectDepositTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@InvoiceTotalAsPerTransaction : %',SUBSTR(CAST(v_InvoiceTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@InvoiceTotalAsPerGL : %',SUBSTR(CAST(v_InvoiceTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@OtherAREntriesTotalAsPerTransaction : %',SUBSTR(CAST(v_OtherAREntriesTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@OtherAREntriesTotalAsPerGL : %',SUBSTR(CAST(v_OtherAREntriesTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@RefundReturnTotalAsPerTransaction : %',SUBSTR(CAST(v_RefundReturnTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@RefundReturnTotalAsPerGL : %',SUBSTR(CAST(v_RefundReturnTotalAsPerGL AS VARCHAR(18)),1,18);

   UPDATE tt_ARAGINTABLE SET AsPerTransaction = v_InvoicedDepositTotalAsPerTransaction,AsPerGL = v_InvoicedDepositTotalAsPerGL, 
   Diff = v_InvoicedDepositTotalAsPerTransaction -v_InvoicedDepositTotalAsPerGL WHERE tt_ARAGINTABLE.TransactionType = 'AR : Invoiced Deposit';
   UPDATE tt_ARAGINTABLE SET AsPerTransaction = v_DirectDepositTotalAsPerTransaction,AsPerGL = v_DirectDepositTotalAsPerGL, 
   Diff = v_DirectDepositTotalAsPerTransaction -v_DirectDepositTotalAsPerGL WHERE tt_ARAGINTABLE.TransactionType = 'AR : Make Deposit';
   UPDATE tt_ARAGINTABLE SET AsPerTransaction = v_InvoiceTotalAsPerTransaction,AsPerGL = v_InvoiceTotalAsPerGL, 
   Diff = v_InvoiceTotalAsPerTransaction -v_InvoiceTotalAsPerGL  WHERE tt_ARAGINTABLE.TransactionType = 'AR : Invoice';
   UPDATE tt_ARAGINTABLE SET AsPerTransaction = v_OtherAREntriesTotalAsPerTransaction,AsPerGL = v_OtherAREntriesTotalAsPerGL, 
   Diff = v_OtherAREntriesTotalAsPerTransaction -v_OtherAREntriesTotalAsPerGL  WHERE tt_ARAGINTABLE.TransactionType = 'AR : Manual Journal Entries';
   UPDATE tt_ARAGINTABLE SET AsPerTransaction = v_RefundReturnTotalAsPerTransaction,AsPerGL = v_RefundReturnTotalAsPerGL, 
   Diff = v_RefundReturnTotalAsPerTransaction -v_RefundReturnTotalAsPerGL  WHERE tt_ARAGINTABLE.TransactionType = 'AR : Refunds';

   RAISE NOTICE '--- Execute AR Aging Detail table';
   open SWV_RefCur7 for
   SELECT AR.TransactionType,AR.Total, AR.AsPerTransaction, AR.AsPerGL, AR.Diff, CASE WHEN AR.Diff = 0 THEN 'Pass' ELSE 'Failed' END AS Result
   FROM tt_ARAGINTABLE AS AR
   ORDER BY "Order";

   select   CASE WHEN AR.Diff = 0 THEN '' ELSE COALESCE(coalesce(v_ARDifference,'') || ', ','') || TransactionType || ' : ' || SUBSTR(CAST(AR.Diff AS VARCHAR(10)),1,10) END INTO v_ARDifference FROM tt_ARAGINTABLE AS AR;

--SET @ARDifference = RIGHT(@ARDifference,LEN(@ARDifference) - 1)
   v_vcDiff := coalesce(v_vcDiff,'') || coalesce(v_ARDifference,'') || ', ';



--- Transaction v/s GL Entries mapping details.
   RAISE NOTICE '--- Transaction v/s GL Entries mapping details.';

-- Check whether any Invoiced transaction are in general entries or not.
   RAISE NOTICE '-- Check whether any Invoiced transaction are in general entries or not.';
   open SWV_RefCur8 for
   SELECT OM.vcpOppName,OBD.vcBizDocID,DD.* FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID AND tintDepositePage IN(2,3)
   JOIN OpportunityMaster AS OM ON OM.numOppId = DD.numOppID AND DM.numDomainId = OM.numDomainId
   JOIN OpportunityBizDocs AS OBD ON OM.numOppId = OBD.numoppid AND DD.numOppBizDocsID = OBD.numOppBizDocsId
   LEFT JOIN General_Journal_Header AS GJH ON DM.numDepositId = GJH.numDepositId AND DM.numDomainId = GJH.numDomainId
   WHERE DM.numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OBD.bitAuthoritativeBizDocs = 1
   AND coalesce(OBD.tintDeferred,0) <> 1
   AND tintDepositePage IN(2,3)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 
   AND coalesce(DM.numReturnHeaderID,0) = 0
   AND OBD.numOppBizDocsId NOT IN(SELECT DD.numOppBizDocsID FROM DepositMaster AS DM
      JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID AND tintDepositePage IN(2,3)
      JOIN General_Journal_Header AS GJH ON DM.numDepositId = GJH.numDepositId AND DM.numDomainId = GJH.numDomainId
      WHERE DM.numDomainId = v_numDomainID
      AND tintDepositePage IN(2,3)
      AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0);

   open SWV_RefCur9 for
   SELECT OM.vcpOppName,OBD.numOppBizDocsId, OBD.vcBizDocID ,GJD.numJournalId,OBD.monDealAmount,
SUM(coalesce(GJD.numCreditAmt,0)) AS Credit, OBD.monDealAmount -SUM(coalesce(GJD.numCreditAmt,0))  AS Diff
   FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID AND tintDepositePage IN(2,3)
   LEFT JOIN OpportunityMaster AS OM ON OM.numOppId = DD.numOppID AND DM.numDomainId = OM.numDomainId
   LEFT JOIN OpportunityBizDocs AS OBD ON OM.numOppId = OBD.numoppid AND DD.numOppBizDocsID = OBD.numOppBizDocsId
   LEFT JOIN General_Journal_Header AS GJH ON DD.numOppID = GJH.numOppId AND DD.numOppBizDocsID = GJH.numOppBizDocsId
   LEFT JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   WHERE DM.numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OBD.bitAuthoritativeBizDocs = 1
   AND coalesce(OBD.tintDeferred,0) <> 1
   AND tintDepositePage IN(2,3)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 
   GROUP BY OM.vcpOppName,OBD.numOppBizDocsId,OBD.vcBizDocID,GJD.numJournalId,OBD.monDealAmount
   ORDER BY OBD.numOppBizDocsId,OBD.monDealAmount -SUM(coalesce(GJD.numCreditAmt,0));

--==========================================================================================


---- Accounts Payable Diagnosis
   RAISE NOTICE '---- Accounts Payable Diagnosis';
--==========================================================================================
   select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID;

-- SELECT  isnull(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID ;
-- JOURNAL ENTRIES TOTAL
   RAISE NOTICE '-- JOURNAL ENTRIES TOTAL';
   select   SUM(GJD.numDebitAmt) -SUM(GJD.numCreditAmt) INTO v_APJournalTotal FROM    General_Journal_Header AS GJH
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId WHERE   1 = 1
   AND GJH.numDomainId = v_numDomainID
   AND GJD.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts AS COA
      WHERE COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND COA.numDomainId = v_numDomainID);

   IF coalesce(v_APJournalTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_APJournalTotal := CAST(0 AS NUMERIC(19,4));
   end if; 

   RAISE NOTICE '@APJournalTotal : %',SUBSTR(CAST(v_APJournalTotal AS VARCHAR(18)),1,18);

-- TRANSACTIONS TOTAL
   RAISE NOTICE '-- TRANSACTIONS TOTAL';
   select   SUM(APTable.DealAmount) INTO v_APTransactionTotal FROM(SELECT  coalesce(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0),
      0) AS DealAmount
      FROM    OpportunityMaster Opp
      JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
      LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
      WHERE   tintopptype = 2
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND OB.bitAuthoritativeBizDocs = 1
      AND coalesce(OB.tintDeferred,0) <> 1
      AND numBizDocId = v_AuthoritativePurchaseBizDocId
      GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
      OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
      OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate,OB.monAmountPaid
      HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) > 0
      UNION ALL 

--Add Bill Amount
      SELECT  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0)  AS DealAmount
      FROM    BillHeader BH
      WHERE   BH.numDomainId = v_numDomainID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
      AND coalesce(BH.numOppId,0) = 0
      GROUP BY BH.numDivisionId,BH.dtDueDate
      HAVING  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0) > 0
      UNION ALL
--Add Write Check entry (As Amount Paid)
      SELECT  CAST(0 AS DOUBLE PRECISION)  AS DealAmount
      FROM    CheckHeader CH
      JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
      WHERE   CH.numDomainID = v_numDomainID
      AND CH.tintReferenceType = 1
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      GROUP BY CD.numCustomerId,CH.dtCheckDate

--Show Impact of AP journal entries in report as well 
      UNION ALL
      SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1*GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS DealAmount
      FROM    General_Journal_Header GJH
      INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND coalesce(numOppId,0) = 0
      AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJH.numBillID,0) = 0
      AND coalesce(GJH.numBillPaymentID,0) = 0
      AND coalesce(GJH.numCheckHeaderID,0) = 0
      AND coalesce(GJH.numReturnID,0) = 0
      AND coalesce(GJH.numPayrollDetailID,0) = 0
      UNION ALL
--Add Commission Amount
      SELECT  CAST(coalesce(SUM(BDC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) AS DOUBLE PRECISION) AS DealAmount
      FROM    BizDocComission BDC
      JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
      JOIN OpportunityMaster Opp ON Opp.numOppId = OBD.numoppid
      JOIN Domain D ON D.numDomainId = BDC.numDomainId
      LEFT JOIN PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
      WHERE   Opp.numDomainId = v_numDomainID
      AND BDC.numDomainId = v_numDomainID
      AND OBD.bitAuthoritativeBizDocs = 1
      GROUP BY D.numDivisionId,OBD.numoppid,BDC.numOppBizDocId,Opp.numCurrencyID,BDC.numComissionAmount,
      OBD.dtCreatedDate
      HAVING  coalesce(BDC.numComissionAmount,0) > 0
      UNION ALL 
--Standalone Refund against Account Receivable
      SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt*-1 ELSE GJD.numCreditAmt END AS DealAmount
      FROM    ReturnHeader RH
      JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = RH.numAccountID
      WHERE   RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%') AS APTable;

   IF coalesce(v_APTransactionTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_APTransactionTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   RAISE NOTICE '@APTransactionTotal : %',SUBSTR(CAST(v_APTransactionTotal AS VARCHAR(18)),1,18);

-- Bill Payment Details (UnApplied Amount)
   RAISE NOTICE '-- Bill Payment Details (UnApplied Amount)';
   select   SUM(coalesce(PaymentTable.DealAmount,0)) INTO v_BillPaymentTotal FROM(SELECT SUM(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0))  AS DealAmount
      FROM BillPaymentHeader BPH
      WHERE BPH.numDomainId = v_numDomainID
      AND(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) > 0
      GROUP BY BPH.numDivisionId) AS PaymentTable;

   IF coalesce(v_BillPaymentTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillPaymentTotal := CAST(0 AS NUMERIC(19,4));
   end if;

-- AR Aging Summary
   RAISE NOTICE '-- AR Aging Summary';
   open SWV_RefCur10 for
   SELECT v_APJournalTotal AS APJournalTotal,
	   v_APTransactionTotal -v_BillPaymentTotal AS APTransactionTotal,
	   v_APJournalTotal+v_APTransactionTotal -v_BillPaymentTotal AS Difference,
	   (CASE WHEN(v_APJournalTotal+v_APTransactionTotal -v_BillPaymentTotal) = 0 THEN 'Pass' ELSE 'Failed' END) AS Result;

   IF(v_APJournalTotal+v_APTransactionTotal -v_BillPaymentTotal) <> 0 then

      v_vcDiff := coalesce(v_vcDiff,'') || '@APJournalTotal + @APTransactionTotal - @BillPaymentTotal : ' || CAST((v_APJournalTotal+v_APTransactionTotal -v_BillPaymentTotal) AS VARCHAR(100));
   end if;

-- AP Aging Details
   RAISE NOTICE '-- AP Aging Details';
   select   SUM(POAuthTable.DealAmount) INTO v_POAuthoritativeTotal FROM(SELECT coalesce(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0),
      0) AS DealAmount
      FROM    OpportunityMaster Opp
      JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
      LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
      WHERE   tintopptype = 2
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND OB.bitAuthoritativeBizDocs = 1
      AND coalesce(OB.tintDeferred,0) <> 1
      AND numBizDocId = v_AuthoritativePurchaseBizDocId
      GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
      OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
      OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate,OB.monAmountPaid
      HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) > 0) AS POAuthTable;

--Add Bill Amount
   select   SUM(BillTable.DealAmount) INTO v_BillTotal FROM(SELECT  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0)  AS DealAmount
      FROM    BillHeader BH
      WHERE   BH.numDomainId = v_numDomainID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
      GROUP BY BH.numDivisionId,BH.dtDueDate
      HAVING  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0) > 0) AS BillTable;

--Add Write Check entry (As Amount Paid)
   select   SUM(coalesce(CheckTable.DealAmount,0)) INTO v_CheckTotal FROM(SELECT  0  AS DealAmount
      FROM    CheckHeader CH
      JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
      WHERE   CH.numDomainID = v_numDomainID
      AND CH.tintReferenceType = 1
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      GROUP BY CD.numCustomerId,CH.dtCheckDate) AS CheckTable;

--Show Impact of AP journal entries in report as well 
   select   SUM(coalesce(OtherTable.DealAmount,0)) INTO v_OtherAPEntriesTotal FROM(SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1*GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS DealAmount
      FROM    General_Journal_Header GJH
      INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND coalesce(numOppId,0) = 0
      AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJH.numBillID,0) = 0
      AND coalesce(GJH.numBillPaymentID,0) = 0
      AND coalesce(GJH.numCheckHeaderID,0) = 0
      AND coalesce(GJH.numReturnID,0) = 0
      AND coalesce(GJH.numPayrollDetailID,0) = 0) AS OtherTable;        

--Add Commission Amount
   select   SUM(coalesce(TabCommission.DealAmount,0)) INTO v_CommissionTotal FROM(SELECT  coalesce(SUM(BDC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) AS DealAmount
      FROM    BizDocComission BDC
      JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
      JOIN OpportunityMaster Opp ON Opp.numOppId = OBD.numoppid
      JOIN Domain D ON D.numDomainId = BDC.numDomainId
      LEFT JOIN PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
      WHERE   Opp.numDomainId = v_numDomainID
      AND BDC.numDomainId = v_numDomainID
      AND OBD.bitAuthoritativeBizDocs = 1
      GROUP BY D.numDivisionId,OBD.numoppid,BDC.numOppBizDocId,Opp.numCurrencyID,BDC.numComissionAmount,
      OBD.dtCreatedDate
      HAVING  coalesce(BDC.numComissionAmount,0) > 0) AS TabCommission;

--Standalone Refund against Account Receivable
   select   SUM(coalesce(RefundTable.DealAmount,0)) INTO v_RefundReturnForAPTotal FROM(SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt*-1 ELSE GJD.numCreditAmt END AS DealAmount
      FROM    ReturnHeader RH
      JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%') AS RefundTable;

   IF coalesce(v_POAuthoritativeTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_POAuthoritativeTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_BillTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_CheckTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_CheckTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_OtherAPEntriesTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_OtherAPEntriesTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_CommissionTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_CommissionTotal := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnForAPTotal,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnForAPTotal := CAST(0 AS NUMERIC(19,4));
   end if;


   RAISE NOTICE '@POAuthoritativeTotal : %',SUBSTR(CAST(v_POAuthoritativeTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillTotal : %',SUBSTR(CAST(v_BillTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CheckTotal : %',SUBSTR(CAST(v_CheckTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@OtherAPEntriesTotal : %',SUBSTR(CAST(v_OtherAPEntriesTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CommissionTotal : %',SUBSTR(CAST(v_CommissionTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@RefundReturnForAPTotal : %',SUBSTR(CAST(v_RefundReturnForAPTotal AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillPaymentTotal : %',SUBSTR(CAST(v_BillPaymentTotal AS VARCHAR(18)),1,18);


	--UNION 
	--SELECT 'Bill Payment Total' AS [TransactionType],@BillPaymentTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 7 AS [Order]
   DROP TABLE IF EXISTS tt_APAGINTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_APAGINTABLE ON COMMIT DROP AS
      SELECT * FROM(SELECT 'AP : PO Authoritative' AS TransactionType,v_POAuthoritativeTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 1 AS "Order"
      UNION
      SELECT 'AP : Stand Alone Bills' AS TransactionType,v_BillTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 2 AS "Order"
      UNION
      SELECT 'AP : Checks' AS TransactionType,v_CheckTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS Diff, 3 AS "Order"
      UNION
      SELECT 'AP : Other AP Entries' AS TransactionType,v_OtherAPEntriesTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS Diff, 4 AS "Order"
      UNION
      SELECT 'AP : Commission' AS TransactionType,v_CommissionTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 5 AS "Order"
      UNION
      SELECT 'AP : Refunds' AS TransactionType,v_RefundReturnForAPTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS Diff, 6 AS "Order") AS APAginTable;


   select   SUM(POAuthTable.DealAmount) INTO v_POAuthoritativeTotalAsPerTransaction FROM(SELECT coalesce(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0),
      0) AS DealAmount
      FROM    OpportunityMaster Opp
      JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
      WHERE   tintopptype = 2
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND OB.bitAuthoritativeBizDocs = 1
      AND coalesce(OB.tintDeferred,0) <> 1
      AND numBizDocId = v_AuthoritativePurchaseBizDocId
      GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
      OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
      OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate,OB.monAmountPaid
      HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) > 0) AS POAuthTable;

   select   SUM(GJD.numCreditAmt) -SUM(coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) INTO v_POAuthoritativeTotalAsPerGL FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   JOIN General_Journal_Header AS GJH ON Opp.numOppId = GJH.numOppId AND OB.numOppBizDocsId = GJH.numOppBizDocsId AND Opp.numDomainId = GJH.numDomainId
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId WHERE   tintopptype = 2
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numDomainID
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND numBizDocId = v_AuthoritativePurchaseBizDocId
   AND GJD.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts AS COA
      WHERE COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND COA.numDomainId = v_numDomainID);

--Add Bill Amount
   select   SUM(BillTable.DealAmount) INTO v_BillTotalAsPerTransaction FROM(SELECT  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0)  AS DealAmount
      FROM    BillHeader BH
      WHERE   BH.numDomainId = v_numDomainID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
      GROUP BY BH.numDivisionId,BH.dtDueDate
      HAVING  coalesce(SUM(BH.monAmountDue),0) -coalesce(SUM(BH.monAmtPaid),0) > 0) AS BillTable;

   select   SUM(GJD.numCreditAmt) -coalesce(SUM(BH.monAmtPaid),0) INTO v_BillTotalAsPerGL FROM    BillHeader BH
   JOIN General_Journal_Header AS GJH ON BH.numBillID = GJH.numBillID
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId WHERE   BH.numDomainId = v_numDomainID
   AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
   AND GJD.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts AS COA
      WHERE COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND COA.numDomainId = v_numDomainID);

--Add Write Check entry (As Amount Paid)
   v_CheckTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   v_CheckTotalAsPerGL := CAST(0 AS NUMERIC(19,4));

--Show Impact of AP journal entries in report as well 
   v_OtherAPEntriesTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   select   SUM(coalesce(OtherTable.DealAmount,0)) INTO v_OtherAPEntriesTotalAsPerGL FROM(SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1*GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS DealAmount
      FROM    General_Journal_Header GJH
      INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
      AND coalesce(numOppId,0) = 0
      AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJH.numBillID,0) = 0
      AND coalesce(GJH.numBillPaymentID,0) = 0
      AND coalesce(GJH.numCheckHeaderID,0) = 0
      AND coalesce(GJH.numReturnID,0) = 0
      AND coalesce(GJH.numPayrollDetailID,0) = 0) AS OtherTable;     

----Add Commission Amount
   select   SUM(coalesce(TabCommission.DealAmount,0)) INTO v_CommissionTotalAsPerTransaction FROM(SELECT  coalesce(SUM(BDC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) AS DealAmount
      FROM    BizDocComission BDC
      JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
      JOIN OpportunityMaster Opp ON Opp.numOppId = OBD.numoppid
      JOIN Domain D ON D.numDomainId = BDC.numDomainId
      LEFT JOIN PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
      WHERE   Opp.numDomainId = v_numDomainID
      AND BDC.numDomainId = v_numDomainID
      AND OBD.bitAuthoritativeBizDocs = 1
      GROUP BY D.numDivisionId,OBD.numoppid,BDC.numOppBizDocId,Opp.numCurrencyID,BDC.numComissionAmount,
      OBD.dtCreatedDate
      HAVING  coalesce(BDC.numComissionAmount,0) > 0) AS TabCommission;

   v_CommissionTotalAsPerGL := CAST(0 AS NUMERIC(19,4));

----Standalone Refund against Account Receivable
   select   SUM(coalesce(RefundTable.DealAmount,0)) INTO v_RefundReturnForAPTotalAsPerTransaction FROM(SELECT  SUM(RH.monBizDocAmount)  AS DealAmount
      FROM    ReturnHeader RH
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = RH.numAccountID
      WHERE   RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%') AS RefundTable;

   select   SUM(coalesce(RefundTable.DealAmount,0)) INTO v_RefundReturnForAPTotalAsPerGL FROM(SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt*-1 ELSE GJD.numCreditAmt END AS DealAmount
      FROM    ReturnHeader RH
      JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      WHERE   RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%') AS RefundTable;

   select   SUM(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) INTO v_BillPaymentTotalAsPerTransaction FROM BillPaymentHeader BPH WHERE BPH.numDomainId = v_numDomainID
   AND(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) > 0;

--SET @BillPaymentTotalAsPerGL = 0
   select   SUM(coalesce(monPaymentAmount,0)) -SUM(coalesce(monAppliedAmount,0)) INTO v_BillPaymentTotalAsPerGL FROM BillPaymentHeader BPH
   JOIN General_Journal_Header AS GJH ON GJH.numBillPaymentID = BPH.numBillPaymentID AND GJH.numDomainId = BPH.numDomainId
   JOIN General_Journal_Details AS GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId WHERE BPH.numDomainId = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%';

   IF coalesce(v_POAuthoritativeTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_POAuthoritativeTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_POAuthoritativeTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_POAuthoritativeTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_BillTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_BillTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;
   
   IF coalesce(v_OtherAPEntriesTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_OtherAPEntriesTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_CommissionTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_CommissionTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnForAPTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnForAPTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_RefundReturnForAPTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_RefundReturnForAPTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_BillPaymentTotalAsPerTransaction,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillPaymentTotalAsPerTransaction := CAST(0 AS NUMERIC(19,4));
   end if;

   IF coalesce(v_BillPaymentTotalAsPerGL,cast(0 as NUMERIC(19,4))) = 0 then
      v_BillPaymentTotalAsPerGL := CAST(0 AS NUMERIC(19,4));
   end if;

   RAISE NOTICE '@POAuthoritativeTotalAsPerTransaction : %',SUBSTR(CAST(v_POAuthoritativeTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@POAuthoritativeTotalAsPerGL : %',SUBSTR(CAST(v_POAuthoritativeTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillTotalAsPerTransaction : %',SUBSTR(CAST(v_BillTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillTotalAsPerGL : %',SUBSTR(CAST(v_BillTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CheckTotalAsPerTransaction : %',SUBSTR(CAST(v_CheckTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CheckTotalAsPerGL : %',SUBSTR(CAST(v_CheckTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@OtherAPEntriesTotalAsPerTransaction : %',SUBSTR(CAST(v_OtherAPEntriesTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@OtherAPEntriesTotalAsPerGL : %',SUBSTR(CAST(v_OtherAPEntriesTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CommissionTotalAsPerTransaction : %',SUBSTR(CAST(v_CommissionTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@CommissionTotalAsPerGL : %',SUBSTR(CAST(v_CommissionTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@RefundReturnForAPTotalAsPerTransaction : %',SUBSTR(CAST(v_RefundReturnForAPTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@RefundReturnForAPTotalAsPerGL : %',SUBSTR(CAST(v_RefundReturnForAPTotalAsPerGL AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillPaymentTotalAsPerTransaction : %',SUBSTR(CAST(v_BillPaymentTotalAsPerTransaction AS VARCHAR(18)),1,18);
   RAISE NOTICE '@BillPaymentTotalAsPerGL : %',SUBSTR(CAST(v_BillPaymentTotalAsPerGL AS VARCHAR(18)),1,18);

   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_POAuthoritativeTotalAsPerTransaction,AsPerGL = v_POAuthoritativeTotalAsPerGL, 
   Diff = v_POAuthoritativeTotalAsPerTransaction -v_POAuthoritativeTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'AP : PO Authoritative';
   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_BillTotalAsPerTransaction,AsPerGL = v_BillTotalAsPerGL, 
   Diff = v_BillTotalAsPerTransaction -v_BillTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'AP : Stand Alone Bills';
   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_CheckTotalAsPerTransaction,AsPerGL = v_CheckTotalAsPerGL, 
   Diff = v_CheckTotalAsPerTransaction -v_CheckTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'Check Total';
   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_OtherAPEntriesTotalAsPerTransaction,AsPerGL = v_OtherAPEntriesTotalAsPerGL, 
   Diff = v_OtherAPEntriesTotalAsPerTransaction -v_OtherAPEntriesTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'AP : Other AP Entries';
   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_CommissionTotalAsPerTransaction,AsPerGL = v_CommissionTotalAsPerGL, 
   Diff = v_CommissionTotalAsPerTransaction -v_CommissionTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'AP : Commission';
   UPDATE tt_APAGINTABLE SET AsPerTransaction = v_RefundReturnForAPTotalAsPerTransaction,AsPerGL = v_RefundReturnForAPTotalAsPerGL, 
   Diff = v_RefundReturnForAPTotalAsPerTransaction -v_RefundReturnForAPTotalAsPerGL
   WHERE tt_APAGINTABLE.TransactionType = 'AP : Refunds';
--UPDATE #APAginTable SET [AsPerTransaction] = @BillPaymentTotalAsPerTransaction ,[AsPerGL] = @BillPaymentTotalAsPerGL, [Diff] = @BillPaymentTotalAsPerTransaction - @BillPaymentTotalAsPerGL  
--WHERE #APAginTable.[TransactionType] = 'Bill Payment Total'

--SELECT @POAuthoritativeTotalAsPerGL + @BillTotalAsPerGL + @CheckTotalAsPerGL + @OtherAPEntriesTotalAsPerGL + @CommissionTotalAsPerGL + @RefundReturnForAPTotalAsPerGL

   RAISE NOTICE 'Execute AP Aging Details';
   open SWV_RefCur11 for
   SELECT AP.TransactionType,AP.Total, AP.AsPerTransaction, AP.AsPerGL, AP.Diff, CASE WHEN AP.Diff = 0 THEN 'Pass' ELSE 'Failed' END AS Result
   FROM tt_APAGINTABLE AS AP
   ORDER BY "Order";

   select   CASE WHEN AP.Diff = 0 THEN '' ELSE COALESCE(coalesce(v_APDifference,'') || ', ','')  || TransactionType || ' : ' || SUBSTR(CAST(AP.Diff AS VARCHAR(10)),1,10) END INTO v_APDifference FROM tt_APAGINTABLE AS AP;

--SET @APDifference = RIGHT(@APDifference,LEN(@APDifference) - 1) 
   v_vcDiff := coalesce(v_vcDiff,'') || coalesce(v_APDifference,'');



   v_vcDiff := LTRIM(v_vcDiff);
   v_vcDiff := RTRIM(v_vcDiff);
   IF LENGTH(v_vcDiff) <= 5 then
      v_vcDiff := '';
   end if;

   open SWV_RefCur12 for
   SELECT coalesce(v_vcDiff,cast(0 as TEXT)) AS Difference;

--- Check whether any of the Accounts Receivable accounts have null CustomerID in GL or not
   open SWV_RefCur13 for
   SELECT * FROM General_Journal_Header AS GJH WHERE GJH.numJOurnal_Id IN(SELECT GJD.numJournalId FROM General_Journal_Details AS GJD
      JOIN General_Journal_Details AS GJD1 ON GJD.numJournalId = GJD1.numJournalId
      WHERE 1 = 1
      AND GJD.numChartAcntId IN(SELECT COA.numAccountId FROM Chart_Of_Accounts AS COA WHERE COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%' AND COA.numDomainId = v_numDomainID)
      AND coalesce(GJD.numCustomerId,0) = 0
      GROUP BY GJD.numJournalId);

--- Check whether any of the Accounts Payable accounts have null CustomerID in GL or not
   open SWV_RefCur14 for
   SELECT * FROM General_Journal_Header AS GJH WHERE GJH.numJOurnal_Id IN(SELECT GJD.numJournalId FROM General_Journal_Details AS GJD
      JOIN General_Journal_Details AS GJD1 ON GJD.numJournalId = GJD1.numJournalId
      WHERE 1 = 1
      AND GJD.numChartAcntId IN(SELECT COA.numAccountId FROM Chart_Of_Accounts AS COA WHERE COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%' AND COA.numDomainId = v_numDomainID)
      AND coalesce(GJD.numCustomerId,0) = 0
      GROUP BY GJD.numJournalId);

---- Check whether accounting entries are not in same account types. (E.g single order entry may not credit and debit from same account type)
-- For Accounts Receivable
   open SWV_RefCur15 for
   SELECT GJD.numJournalId,COUNT(GJD.numJournalId) FROM General_Journal_Details AS GJD
   JOIN General_Journal_Details AS GJD1 ON GJD.numJournalId = GJD1.numJournalId
   WHERE 1 = 1
   AND GJD.numChartAcntId IN(SELECT COA.numAccountId FROM Chart_Of_Accounts AS COA WHERE COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%' AND COA.numDomainId = v_numDomainID)
   GROUP BY GJD.numJournalId
   HAVING COUNT(GJD.numJournalId) > 1
   ORDER BY GJD.numJournalId;

-- For Accounts Payable
   open SWV_RefCur16 for
   SELECT GJD.numJournalId,COUNT(GJD.numJournalId) FROM General_Journal_Details AS GJD
   JOIN General_Journal_Details AS GJD1 ON GJD.numTransactionId = GJD1.numTransactionId AND GJD.numJournalId = GJD1.numJournalId
   WHERE 1 = 1
   AND GJD.numChartAcntId IN(SELECT COA.numAccountId FROM Chart_Of_Accounts AS COA WHERE COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%' AND COA.numDomainId = v_numDomainID)
   GROUP BY GJD.numJournalId
   HAVING COUNT(GJD.numJournalId) > 1
   ORDER BY GJD.numJournalId;


-- Compare GL and Transactions group by DivisionID for AR
   v_vcARAccountID := coalesce((SELECT string_agg(CAST(numAccountId AS VARCHAR(10)),',')
   FROM      Chart_Of_Accounts AS COA
   WHERE     COA.numDomainId = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'),''); 

--ORDER BY ci.vcCompanyName 
   open SWV_RefCur17 for
   SELECT GLTable.numDomainId,GLTable.numDivisionID,GLTable.numAccountId,GLTable.CompanyName,GLTable.Opening,GLTable.TotalDebit,GLTable.TotalCredit,
	   GLTable.Closing,TABLE2.DealAmount,
	   CASE WHEN coalesce(GLTable.Closing,0) > 0 AND coalesce(TABLE2.DealAmount,0) > 0 THEN(coalesce(TABLE2.DealAmount,0) -coalesce(GLTable.Closing,0))
   WHEN coalesce(GLTable.Closing,0) > 0 AND coalesce(TABLE2.DealAmount,0) < 0 THEN(coalesce(TABLE2.DealAmount,0)+coalesce(GLTable.Closing,0))
   WHEN coalesce(GLTable.Closing,0) < 0 AND coalesce(TABLE2.DealAmount,0) > 0 THEN(coalesce(TABLE2.DealAmount,0)+coalesce(GLTable.Closing,0))
   ELSE(coalesce(GLTable.Closing,0) -coalesce(TABLE2.DealAmount,0))
   END AS Diff
   FROM(SELECT  h.numDomainId ,
        coa.numAccountId ,
        ci.vcCompanyName AS CompanyName ,
        dm.numDivisionID ,
        0 AS Opening ,
        SUM(coalesce(d.numDebitAmt,0)) AS TotalDebit ,
        SUM(coalesce(d.numCreditAmt,0)) AS TotalCredit ,
        (SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0))) AS Closing ,
        coa.numParntAcntTypeID
      FROM    General_Journal_Header h
      INNER JOIN General_Journal_Details d ON h.numJOurnal_Id = d.numJournalId
      LEFT JOIN Chart_Of_Accounts coa ON d.numChartAcntId = coa.numAccountId
      JOIN DivisionMaster dm
      JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId ON d.numCustomerId = dm.numDivisionID
      WHERE   h.numDomainId = v_numDomainID
      AND coalesce(dm.numDivisionID,0) > 0
      AND coa.numAccountId IN(SELECT  CAST(SS.OutParam AS NUMERIC)
         FROM    SplitString(v_vcARAccountID,',') AS SS)
      GROUP BY h.numDomainId,coa.numAccountId,ci.vcCompanyName,dm.numDivisionID,coa.numParntAcntTypeID,
      h.numDomainId,coa.numAccountId
      HAVING(SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0))) <> 0) GLTable
   JOIN(SELECT numDomainID,vcCompanyName, numDivisionID, Opening, DealAmount FROM(SELECT  Opp.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) AS DealAmount
         FROM    OpportunityMaster Opp
         JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
         LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
         LEFT JOIN DivisionMaster dm ON Opp.numDivisionId = dm.numDivisionID
         LEFT JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   tintopptype = 1
         AND tintoppstatus = 1
         AND Opp.numDomainId = v_numDomainID
         AND numBizDocId = v_AuthoritativeSalesBizDocId
         AND OB.bitAuthoritativeBizDocs = 1
         AND coalesce(OB.tintDeferred,0) <> 1
         GROUP BY Opp.numDomainId,ci.vcCompanyName,dm.numDivisionID 

			--AND Opp.[numDivisionId] = 217158
         UNION ALL --Show Impact of AR journal entries in report as well 
         SELECT	GJH.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)
         FROM    General_Journal_Header GJH
         INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         AND GJH.numDomainId = GJD.numDomainId
         INNER JOIN Chart_Of_Accounts coa ON coa.numAccountId = GJD.numChartAcntId
         LEFT JOIN DivisionMaster dm ON GJD.numCustomerId = dm.numDivisionID
         LEFT JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   GJH.numDomainId = v_numDomainID
         AND coa.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
         AND coalesce(numOppId,0) = 0
         AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(GJH.numDepositId,0) = 0
         AND coalesce(GJH.numReturnID,0) = 0
         GROUP BY GJH.numDomainId,ci.vcCompanyName,dm.numDivisionID
         UNION ALL -- Deposits
         SELECT  dm.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			CAST(SUM((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/ )*-1) AS DOUBLE PRECISION)
         FROM    DepositMaster dm
         LEFT JOIN DivisionMaster DIV ON dm.numDivisionID = DIV.numDivisionID
         LEFT JOIN CompanyInfo ci ON DIV.numCompanyID = ci.numCompanyId
         WHERE   dm.numDomainId = v_numDomainID
         AND tintDepositePage IN(2,3)
         AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 
         GROUP BY dm.numDomainId,ci.vcCompanyName,dm.numDivisionID
         UNION ALL --Standalone Refund against Account Receivable
         SELECT  RH.numDomainID ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)
         FROM    ReturnHeader RH
         JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
         JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         INNER JOIN Chart_Of_Accounts coa ON coa.numAccountId = GJD.numChartAcntId
         LEFT JOIN DivisionMaster dm ON RH.numDivisionId = dm.numDivisionID
         LEFT JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   RH.tintReturnType = 4
         AND RH.numDomainID = v_numDomainID
         AND coa.vcAccountCode ilike coalesce(v_vcARAccountCode,'') || '%'
         AND  monBizDocAmount > 0 
         AND coalesce(RH.numBillPaymentIDRef,0) = 0
         GROUP BY RH.numDomainID,ci.vcCompanyName,dm.numDivisionID) TABLE1
      WHERE coalesce(TABLE1.numDivisionID,0) > 0
      AND coalesce(TABLE1.DealAmount,0) > 0) TABLE2 ON GLTable.numDomainId = TABLE2.numDomainId AND GLTable.numDivisionID = TABLE2.numDivisionID
   AND(coalesce(GLTable.Closing,0) -coalesce(TABLE2.DealAmount,0)) <> 0
   AND coalesce(TABLE2.numDomainId,0) > 0
   AND coalesce(GLTable.numDomainId,0) > 0
   ORDER BY GLTable.CompanyName;


-- Compare GL and Transactions group by DivisionID for AP
   v_vcAPAccountID := coalesce((SELECT string_agg(CAST(numAccountId AS VARCHAR(10)),',')
   FROM      Chart_Of_Accounts AS COA
   WHERE     COA.numDomainId = v_numDomainID
   AND COA.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'),''); 

--ORDER BY ci.vcCompanyName 
   open SWV_RefCur18 for
   SELECT GLTable.numDomainId,GLTable.numDivisionID,GLTable.numAccountId,GLTable.CompanyName,GLTable.Opening,GLTable.TotalDebit,GLTable.TotalCredit,
	   GLTable.Closing,TABLE2.DealAmount,
	   CASE WHEN coalesce(GLTable.Closing,0) > 0 AND coalesce(TABLE2.DealAmount,0) > 0 THEN(coalesce(TABLE2.DealAmount,0) -coalesce(GLTable.Closing,0))
   WHEN coalesce(GLTable.Closing,0) > 0 AND coalesce(TABLE2.DealAmount,0) < 0 THEN(coalesce(TABLE2.DealAmount,0)+coalesce(GLTable.Closing,0))
   WHEN coalesce(GLTable.Closing,0) < 0 AND coalesce(TABLE2.DealAmount,0) > 0 THEN(coalesce(TABLE2.DealAmount,0)+coalesce(GLTable.Closing,0))
   ELSE(coalesce(GLTable.Closing,0) -coalesce(TABLE2.DealAmount,0))
   END AS Diff
   FROM(SELECT  h.numDomainId ,
        coa.numAccountId ,
        ci.vcCompanyName AS CompanyName ,
        dm.numDivisionID ,
        0 AS Opening ,
        SUM(coalesce(d.numDebitAmt,0)) AS TotalDebit ,
        SUM(coalesce(d.numCreditAmt,0)) AS TotalCredit ,
        (SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0))) AS Closing ,
        coa.numParntAcntTypeID
      FROM    General_Journal_Header h
      INNER JOIN General_Journal_Details d ON h.numJOurnal_Id = d.numJournalId
      LEFT JOIN Chart_Of_Accounts coa ON d.numChartAcntId = coa.numAccountId
      JOIN DivisionMaster dm
      JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId ON d.numCustomerId = dm.numDivisionID
      WHERE   h.numDomainId = v_numDomainID
      AND coalesce(dm.numDivisionID,0) > 0
      AND coa.numAccountId IN(SELECT  CAST(SS.OutParam AS NUMERIC)
         FROM    SplitString(v_vcARAccountID,',') AS SS)
      GROUP BY h.numDomainId,coa.numAccountId,ci.vcCompanyName,dm.numDivisionID,coa.numParntAcntTypeID,
      h.numDomainId,coa.numAccountId
      HAVING(SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0))) <> 0) GLTable
   JOIN(SELECT numDomainID,vcCompanyName, numDivisionID, Opening, DealAmount FROM(SELECT  Opp.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) AS DealAmount
         FROM    OpportunityMaster Opp
         JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
         LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
         JOIN DivisionMaster dm ON Opp.numDivisionId = dm.numDivisionID
         JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   tintopptype = 2
         AND tintoppstatus = 1
         AND Opp.numDomainId = v_numDomainID
         AND numBizDocId = v_AuthoritativePurchaseBizDocId
         AND OB.bitAuthoritativeBizDocs = 1
         AND coalesce(OB.tintDeferred,0) <> 1
         GROUP BY Opp.numDomainId,ci.vcCompanyName,dm.numDivisionID 

			--AND Opp.[numDivisionId] = 217158
         UNION ALL --Show Impact of AR journal entries in report as well 
         SELECT	GJH.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)
         FROM    General_Journal_Header GJH
         INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         AND GJH.numDomainId = GJD.numDomainId
         INNER JOIN Chart_Of_Accounts coa ON coa.numAccountId = GJD.numChartAcntId
         LEFT JOIN DivisionMaster dm ON GJD.numCustomerId = dm.numDivisionID
         LEFT JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   GJH.numDomainId = v_numDomainID
         AND coa.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
         AND coalesce(numOppId,0) = 0
         AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(GJH.numDepositId,0) = 0
         AND coalesce(GJH.numReturnID,0) = 0
         GROUP BY GJH.numDomainId,ci.vcCompanyName,dm.numDivisionID
         UNION ALL -- Deposits
         SELECT  dm.numDomainId ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			CAST(SUM((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/ )*-1) AS DOUBLE PRECISION)
         FROM    DepositMaster dm
         LEFT JOIN DivisionMaster DIV ON dm.numDivisionID = DIV.numDivisionID
         LEFT JOIN CompanyInfo ci ON DIV.numCompanyID = ci.numCompanyId
         WHERE   dm.numDomainId = v_numDomainID
         AND tintDepositePage IN(2,3)
         AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 
         GROUP BY dm.numDomainId,ci.vcCompanyName,dm.numDivisionID
         UNION ALL --Standalone Refund against Account Receivable
         SELECT  RH.numDomainID ,
			ci.vcCompanyName ,
			dm.numDivisionID ,
			0 AS Opening ,
			SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)
         FROM    ReturnHeader RH
         JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
         JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         INNER JOIN Chart_Of_Accounts coa ON coa.numAccountId = GJD.numChartAcntId
         LEFT JOIN DivisionMaster dm ON RH.numDivisionId = dm.numDivisionID
         LEFT JOIN CompanyInfo ci ON dm.numCompanyID = ci.numCompanyId
         WHERE   RH.tintReturnType = 4
         AND RH.numDomainID = v_numDomainID
         AND coa.vcAccountCode ilike coalesce(v_vcAPAccountCode,'') || '%'
         AND  monBizDocAmount > 0 
         AND coalesce(RH.numBillPaymentIDRef,0) = 0
         GROUP BY RH.numDomainID,ci.vcCompanyName,dm.numDivisionID) TABLE1
      WHERE coalesce(TABLE1.numDivisionID,0) > 0
      AND coalesce(TABLE1.DealAmount,0) > 0) TABLE2 ON GLTable.numDomainId = TABLE2.numDomainId AND GLTable.numDivisionID = TABLE2.numDivisionID
   AND(coalesce(GLTable.Closing,0) -coalesce(TABLE2.DealAmount,0)) <> 0
   AND coalesce(TABLE2.numDomainId,0) > 0
   AND coalesce(GLTable.numDomainId,0) > 0
   ORDER BY GLTable.CompanyName;
   RETURN;
END; $$;


