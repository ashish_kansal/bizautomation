-- Stored procedure definition script USP_ManageRatings for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRatings(v_numRatingId NUMERIC(9,0) DEFAULT 0,    
v_tintTypeId INTEGER DEFAULT 0,       
v_vcReferenceId	 VARCHAR(100) DEFAULT '', 
v_intRatingCount INTEGER DEFAULT NULL , 
v_vcIpAddress VARCHAR(100) DEFAULT NULL ,
v_numContactId NUMERIC(18,0) DEFAULT NULL ,
v_numSiteId NUMERIC(18,0) DEFAULT NULL ,
v_numDomainId NUMERIC(18,0) DEFAULT NULL ,
v_vcCreatedDate VARCHAR(100) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numRatingId = 0 then
      
      IF NOT EXISTS(SELECT * FROM  Ratings  WHERE numContactId = v_numContactId AND vcReferenceId = v_vcReferenceId AND numSiteID = v_numSiteId AND numDomainID = v_numDomainId) then
			
         INSERT INTO Ratings(tintTypeId ,
					vcReferenceId,
					intRatingCount,
					numContactId,
					vcIpAddress,
					numSiteID ,
					numDomainID ,
					dtCreatedDate)
			values(v_tintTypeId ,
					v_vcReferenceId ,
					v_intRatingCount ,
					v_numContactId ,
					v_vcIpAddress,
					v_numSiteId ,
					v_numDomainId ,
					CAST(v_vcCreatedDate AS TIMESTAMP));
      ELSE
         UPDATE Ratings set
         intRatingCount = v_intRatingCount
         where  numContactId = v_numContactId AND vcReferenceId = v_vcReferenceId AND numSiteID = v_numSiteId AND numDomainID = v_numDomainId;
      end if;
   ELSE
      UPDATE Ratings set
      intRatingCount = v_intRatingCount
      where numRatingId = v_numRatingId;
   end if;
   RETURN;
END; $$;


--SELECT * FROM Ratings



