-- Stored procedure definition script USP_OpportunityMaster_SetDoNotMerge for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_SetDoNotMerge(v_numDomainID NUMERIC(18,0),
	v_vcPOID VARCHAR(2000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   OpportunityMaster
   SET
   bitStopMerge = true
   WHERE
   numDomainId = v_numDomainID
   AND numOppId IN (SELECT Id FROM SplitIds(v_vcPOID,','));
   RETURN;
END; $$;


