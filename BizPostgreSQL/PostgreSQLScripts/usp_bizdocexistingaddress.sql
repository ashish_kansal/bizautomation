CREATE OR REPLACE FUNCTION USP_BizDocExistingAddress(v_numDomainId NUMERIC(9,0),        
v_numUserId NUMERIC(9,0),        
v_numDivisionId NUMERIC(9,0) ,
v_vcAddressType VARCHAR(15),
v_numPrimaryContactCheck VARCHAR(10), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyId  NUMERIC(9,0);
   v_vcCompanyName  VARCHAR(200);

   v_numContactId  NUMERIC(18,0);
BEGIN
   BEGIN
      select   numCompanyID INTO v_numCompanyId from DivisionMaster where numDomainID = v_numDomainId and numDivisionID = v_numDivisionId;
      select   vcCompanyName INTO v_vcCompanyName from CompanyInfo where numCompanyId = v_numCompanyId;
      if (v_numPrimaryContactCheck = 'Yes') then
	
         select   numContactId INTO v_numContactId from AdditionalContactsInformation where numDomainID = v_numDomainId and numDivisionId = v_numDivisionId;
         open SWV_RefCur for
         SELECT v_numCompanyId as numCompanyId,v_vcCompanyName as vcCompanyName,
		vcStreet as Street, vcCity as City, vcPostalCode as PostCode ,
		numState as State, numCountry  as Country,vcAddressName,coalesce(numContact,0) AS numContact, coalesce(bitAltContact,false) AS bitAltContact, coalesce(vcAltContact,'') AS vcAltContact
         FROM AddressDetails WHERE numDomainID = v_numDomainId AND numRecordID = v_numContactId AND tintAddressOf = 1 AND tintAddressType = 0 AND bitIsPrimary = true;
      end if;
      if (v_numPrimaryContactCheck = 'No') then
	
         if (v_vcAddressType = 'Bill') then
		
            open SWV_RefCur for
            select   v_numCompanyId as numCompanyId,
					v_vcCompanyName as vcCompanyName,
					vcStreet as Street,
					vcCity as City,
					numState as State,
					fn_GetState(numState) AS vcState,
					numCountry as Country,
					fn_GetListItemName(numCountry) AS vcCountry,
					vcPostalCode as PostCode,vcAddressName
					,coalesce(numContact,0) AS numContact
					, coalesce(bitAltContact,false) AS bitAltContact
					, coalesce(vcAltContact,'') AS vcAltContact
            FROM     AddressDetails
            WHERE    numDomainID = v_numDomainId
            AND numRecordID = v_numDivisionId
            AND tintAddressOf = 2
            AND tintAddressType = 1
            AND bitIsPrimary = true;
         end if;
         if (v_vcAddressType = 'Ship') then
		
            open SWV_RefCur for
            select   v_numCompanyId as numCompanyId,
					v_vcCompanyName as vcCompanyName,
					vcStreet as Street,
					vcCity as City,
					numState as State,
					fn_GetState(numState) AS vcState,
					numCountry as Country,
					fn_GetListItemName(numCountry) AS vcCountry,
					vcPostalCode as PostCode,vcAddressName
					,coalesce(numContact,0) AS numContact
					, coalesce(bitAltContact,false) AS bitAltContact
					, coalesce(vcAltContact,'') AS vcAltContact
            FROM     AddressDetails
            WHERE    numDomainID = v_numDomainId
            AND numRecordID = v_numDivisionId
            AND tintAddressOf = 2
            AND tintAddressType = 2
            AND bitIsPrimary = true;
         end if;
      end if;
   END;
   RETURN;
END; $$; 


