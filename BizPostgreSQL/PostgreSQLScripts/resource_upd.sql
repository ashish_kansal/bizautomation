-- Stored procedure definition script Resource_Upd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Resource_Upd(v_ResourceID		INTEGER,	-- the key of the Resource to be updated
	v_ResourceName		VARCHAR(64),	-- changed name of the Resource
	v_ResourceDesc		TEXT,		-- changed description of the Resource
	v_ResourceEmail		VARCHAR(64),	-- changed e-mail contact information of the Resource
	v_EnableEmailReminders	INTEGER		-- resource preference for e-mail reminders
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ResourcePreferenceID  INTEGER;
BEGIN
   -- BEGIN TRANSACTION
UPDATE
   Resource
   SET
   ResourceName = v_ResourceName,ResourceDescription = v_ResourceDesc,EmailAddress = v_ResourceEmail
   WHERE
   ResourceID = v_ResourceID;

   select   ResourcePreference.ResourceID INTO v_ResourcePreferenceID FROM
   ResourcePreference WHERE
   ResourcePreference.ResourceID = v_ResourceID;

   IF (v_ResourcePreferenceID IS NOT NULL) then
	
      UPDATE
      ResourcePreference
      SET
      EnableEmailReminders = v_EnableEmailReminders
      WHERE
      ResourceID = v_ResourcePreferenceID;
   ELSEIF (v_EnableEmailReminders <> 0)
   then
	
      INSERT INTO ResourcePreference(ResourceID,
			EnableEmailReminders) VALUES(v_ResourceID,
			v_EnableEmailReminders);
   end if;

   -- COMMIT
RETURN;
END; $$;


