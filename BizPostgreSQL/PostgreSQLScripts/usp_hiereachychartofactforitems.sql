-- Stored procedure definition script USP_HiereachyChartOfActForItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_HiereachyChartOfActForItems(v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
--@numParentAcntID as numeric(9)=0,          
   AS $$
BEGIN
                
--Begin      
--Declare @strSQL as varchar(8000)      
--  SELECT * FROM [AccountTypeDetail] WHERE numdomainid=72
--        WITH    RecursionCTE ( RecordID, ParentRecordID, vcAccountName, TOC, T, numChartOfAcntID, numAcntTypeId )
--                  AS ( SELECT   numAccountId,
--                                numParntAcntTypeId,
--                                vcAccountName,
--                                CONVERT(VARCHAR(1000), '') TOC,
--                                CONVERT(VARCHAR(1000), '') T,
--                                CONVERT(VARCHAR(10), numAccountId) + '~'
--                                + CONVERT(VARCHAR(10), numAcntTypeId) AS numChartOfAcntID,
--                                numAcntTypeId
--                       FROM     Chart_Of_Accounts
--                       WHERE    numParntAcntTypeId IS NULL
--                                AND numDomainID = @numDomainID
--                       UNION ALL
--                       SELECT   R1.numAccountId,
--                                R1.numParntAcntTypeId,
--                                R1.vcAccountName,
--                                CASE WHEN DATALENGTH(R2.TOC) > 0
--                                     THEN CONVERT(VARCHAR(1000), CASE WHEN CHARINDEX('.', R2.TOC) > 0 THEN R2.TOC
--                                                                      ELSE R2.TOC + '.'
--                                                                 END
--                                          + CAST(R1.numAccountId AS VARCHAR(10)))
--                                     ELSE CONVERT(VARCHAR(1000), CAST(R1.numAccountId AS VARCHAR(10)))
--                                END AS TOC,
--                                CASE WHEN DATALENGTH(R2.TOC) > 0
--                                     THEN CONVERT(VARCHAR(1000), '..' + R2.T)
--                                     ELSE CONVERT(VARCHAR(1000), '')
--                                END AS T,
--                                CONVERT(VARCHAR(10), R1.numAccountId) + '~'
--                                + CONVERT(VARCHAR(10), R1.numAcntTypeId) AS numChartOfAcntID,
--                                R1.numAcntTypeId
--                       FROM     Chart_Of_Accounts AS R1
--                                JOIN RecursionCTE AS R2 ON R1.numParntAcntTypeId = R2.RecordID
--                                                           AND R1.numDomainID = @numDomainID
--                     )
--            SELECT  RecordID,
--                    T + vcAccountName AS vcCategoryName,
--                    TOC,
--                    numChartOfAcntID,
--                    numAcntTypeId,
--                    ISNULL(vcData, '') vcAcntType
--            FROM    RecursionCTE
--                    LEFT OUTER JOIN ListDetails ON numAcntTypeId = numLIstItemId
--            WHERE   ParentRecordID IS NOT NULL
--            ORDER BY numAcntTypeId,
--                    TOC ASC
----Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcCatgyName,TOC,T,numChartOfAcntID,numAcntType)                    
---- as                    
---- (                    
---- select numAccountId,numParntAcntId,vcCatgyName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T,  
----Convert(varchar(10),numAccountId)  as numChartOfAcntID,numAcntType
----    from Chart_Of_Accounts                    
----    where numParntAcntId is null and numDomainID='+Convert(varchar(10),@numDomainId)                 
----    +' union all                    
----   select R1.numAccountId,                    
----          R1.numParntAcntId,                    
----    R1.vcCatgyName,                    
----          case when DataLength(R2.TOC) > 0                    
----                    then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                    
----                                 + cast(R1.numAccountId as varchar(10)))                     
----       else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                     
----                    end as TOC,case when DataLength(R2.TOC) > 0                    
----                    then  convert(varchar(1000),''..''+ R2.T)                    
----else convert(varchar(1000),'''')                    
----                    end as T,Convert(varchar(10),R1.numAccountId)as numChartOfAcntID,R1.numAcntType
----      from Chart_Of_Accounts as R1                          
----      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID='+Convert(varchar(10),@numDomainId) +'                     
----  )                     
----select RecordID ,T+vcCatgyName as vcCategoryName,TOC,numChartOfAcntID,numAcntType,isnull(vcData,'''') vcAcntType  from RecursionCTE left outer join ListDetails on numAcntType=numLIstItemId Where ParentRecordID is not null order by numAcntType,TOC asc'  
----  
---- Exec (@strSQL)   
--End
/****** Object:  StoredProcedure [dbo].[USP_HowOftenPagesVisited]    Script Date: 07/26/2008 16:18:53 ******/
   RETURN;
END; $$;


