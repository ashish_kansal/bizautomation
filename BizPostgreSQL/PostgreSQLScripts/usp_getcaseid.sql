-- Stored procedure definition script USP_GetCaseId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCaseId(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from Cases LIMIT 1) then
      open SWV_RefCur for
      select max(numCaseId)+1 from Cases;
   else
      open SWV_RefCur for
      select 1;
   end if;
   RETURN;
END; $$;


