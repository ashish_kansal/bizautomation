-- Stored procedure definition script usp_RemoveStagePercentageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_RemoveStagePercentageDetails(v_numDomainid NUMERIC DEFAULT 0,
    v_numProjectID NUMERIC DEFAULT 0,
	v_tintModeType INTEGER DEFAULT -1)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF EXISTS(SELECT * FROM    ProjectsOpportunities
   WHERE   numDomainId = v_numDomainid
   AND v_numProjectID =(CASE v_tintModeType
   WHEN 0 THEN numOppId
   WHEN 1 THEN numProId
   END)) then
    
      RAISE EXCEPTION 'DEPENDANT';
      RETURN;
   end if;			


   BEGIN
      -- BEGIN TRAN
DELETE    FROM StageAccessDetail
      WHERE     v_numProjectID =(CASE v_tintModeType
      WHEN 0 THEN numOppID
      WHEN 1 THEN numProjectID
      END);
      DELETE FROM ProjectsOpportunities
      WHERE numDomainId = v_numDomainid AND v_numProjectID =(CASE v_tintModeType
      WHEN 0 THEN numOppId
      WHEN 1 THEN numProId
      END);
      DELETE FROM ProjectTeamRights
      WHERE v_numProjectID =(CASE v_tintModeType
      WHEN 0 THEN numOppId
      WHEN 1 THEN numProId
      END);
      DELETE FROM DocumentWorkflow WHERE numDocID IN(SELECT numGenericDocID FROM GenericDocuments WHERE vcDocumentSection = 'PS'
      AND numRecID IN(SELECT numStageDetailsId FROM StagePercentageDetails
         WHERE v_numProjectID =(case v_tintModeType when 0 then numOppid
         when 1 then numProjectid end) AND numdomainid = v_numDomainid) AND numDomainId = v_numDomainid);
      DELETE FROM GenericDocuments WHERE vcDocumentSection = 'PS' AND
      numRecID IN(SELECT numStageDetailsId FROM StagePercentageDetails
      WHERE v_numProjectID =(case v_tintModeType when 0 then numOppid
      when 1 then numProjectid end) AND numdomainid = v_numDomainid) AND numDomainId = v_numDomainid;
      delete from StageDependency where numStageDetailId in(select numStageDetailsId from StagePercentageDetails where
      v_numProjectID =(case v_tintModeType when 0 then numOppid
      when 1 then numProjectid end) and numdomainid = v_numDomainid) or numDependantOnID in(select numStageDetailsId from StagePercentageDetails where
      v_numProjectID =(case v_tintModeType when 0 then numOppid
      when 1 then numProjectid end) and numdomainid = v_numDomainid);
      delete from Comments where
      numStageID in(select numStageDetailsId from StagePercentageDetails where
      v_numProjectID =(case when v_tintModeType = 0 then numOppid
      when v_tintModeType = 1 then numProjectid end) and numdomainid = v_numDomainid);
      delete from StagePercentageDetails where
      v_numProjectID =(case when v_tintModeType = 0 then numOppid
      when v_tintModeType = 1 then numProjectid end) and numdomainid = v_numDomainid;
      delete from ProjectProgress WHERE  v_numProjectID =(case v_tintModeType when 0 then numOppId
      when 1 then numProId end)  AND numDomainID = v_numDomainid;
      IF v_tintModeType = 0 then
         UPDATE OpportunityMaster SET numBusinessProcessID = NULL WHERE numOppId = v_numProjectID and numDomainId = v_numDomainid;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
         RETURN;
   END;
END; $$;	



