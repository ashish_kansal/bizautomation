CREATE OR REPLACE FUNCTION USP_GET_IMPORT_MAPPED_DATA
(
	v_intImportFileID	BIGINT,
	v_numDomainID NUMERIC(18,0),
	v_numFormID	NUMERIC(18,0),
	INOUT SWV_RefCur refcursor DEFAULT NULL,
	INOUT SWV_RefCur2 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSQL  TEXT;
	v_strWHERE  TEXT;
BEGIN
	v_strWHERE := ' WHERE 1 = 1 ';
	
	IF coalesce(v_intImportFileID,0) <> 0 then
		v_strWHERE := coalesce(v_strWHERE,'') || ' AND PARENT.intImportFileID = ' || v_intImportFileID;
	end if;
		
	v_strSQL := 'SELECT   PARENT.intImportFileID AS ImportFileID,
			   					CONCAT((CASE PARENT.tintImportType WHEN 1 THEN ''Update'' WHEN 2 THEN ''Import'' ELSE ''Import/Update'' END),'' ('',IMPORT.vcFormName,'')'') AS ImportType,
								PARENT.vcImportFileName AS ImportFileName,
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''Pending ''
									WHEN PARENT.tintStatus = 1 THEN ''Completed''
									WHEN PARENT.tintStatus = 2 THEN ''Pending''	
									WHEN PARENT.tintStatus = 3 THEN ''Incomplete''	
								END) AS Status,								
								FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS CreatedDate,
								fn_GetContactName(PARENT.numUserContactID) AS CreatedBy ,
								coalesce(CASE WHEN coalesce(tintStatus,0) = 1
											 THEN CONCAT((CASE WHEN coalesce(numRecordAdded,0) <> 0 THEN CAST(numRecordAdded AS VARCHAR(30)) || '' records added. '' ELSE '''' END),
												  (CASE WHEN coalesce(numRecordUpdated,0) <> 0 THEN CAST(numRecordUpdated AS VARCHAR(30)) || '' records updated. '' ELSE '''' END),
												      (CASE WHEN coalesce(numErrors,0) <> 0 THEN ''Error occured in '' || CAST(numErrors AS VARCHAR(30)) || '' records. '' ELSE '''' END))
												 
											 ELSE	''''
												  
											END,''-'') AS Records,
								PARENT.tintStatus AS intStatus,
								PARENT.numMasterID AS MasterID,
								coalesce(HISTORY.vcHistory_Added_Value,'''') AS ItemCodes,
								coalesce(numRecordAdded,0) AS numRecordAdded,
								coalesce(numRecordUpdated,0) AS numRecordUpdated,
								coalesce(CASE WHEN (coalesce(tintStatus,0) = 1 OR coalesce(tintStatus,0) = 3)
											THEN 
											     CASE WHEN coalesce(numRecordAdded,0) > 0 AND coalesce(numRecordUpdated,0) = 0 THEN 1 
											          ELSE 0
												 END
									   END,0)	AS IsForRollback
									   ,coalesce(PARENT.tintItemLinkingID,0) AS tintItemLinkingID,tintImportType
						FROM Import_File_Master PARENT 
						INNER JOIN DynamicFormMaster IMPORT ON PARENT.numMasterID = IMPORT.numFormId
						LEFT JOIN Import_History HISTORY ON HISTORY.intImportFileID = PARENT.intImportFileID ' || coalesce(v_strWHERE,'') || ' AND PARENT.numDomainId = ' || v_numDomainID ||
						' ORDER BY PARENT.intImportFileID DESC';
   
	RAISE NOTICE '%',v_strSQL;
   
	OPEN SWV_RefCur FOR EXECUTE v_strSQL;
		
	open SWV_RefCur2 for 
	SELECT 
		* 
	FROM
	(
		SELECT DISTINCT 
			PARENT.numFieldID AS FormFieldID,
			IFFM.intMapColumnNo AS ImportFieldID,
			IFFM.intImportFileID,
			IFFM.intImportTransactionID,
			CASE 
				WHEN coalesce(bitCustomField,false) = true
				THEN CFW.FLd_label
				ELSE PARENT.vcDbColumnName
			END AS dbFieldName,
			CASE 
				WHEN coalesce(bitCustomField,false) = true
				THEN CFW.FLd_label
				ELSE PARENT.vcFieldName
			END AS FormFieldName,
			CASE 
				WHEN CFW.fld_type = 'Drop Down List Box'
				THEN 'SelectBox'
				ELSE coalesce(PARENT.vcAssociatedControlType,'')
			END AS vcAssociatedControlType,
			coalesce(PARENT.bitDefault,false) AS bitDefault,
			coalesce(PARENT.vcFieldType,'') AS vcFieldType,
			coalesce(PARENT.vcListItemType,'') AS vcListItemType,
			coalesce(PARENT.vcLookBackTableName,'') AS vcLookBackTableName,
			CASE 
				WHEN coalesce(bitCustomField,false) = true
				THEN CFW.FLd_label
				ELSE PARENT.vcOrigDbColumnName
			END AS vcOrigDbColumnName,
			coalesce(PARENT.vcPropertyName,'') AS vcPropertyName,
			coalesce(bitCustomField,false) AS bitCustomField,
			PARENT.numDomainID AS DomainID
		FROM 
			View_DynamicDefaultColumns PARENT
		LEFT JOIN Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID
		LEFT JOIN CFW_Fld_Master CFW ON CFW.numDomainID = PARENT.numDomainID
		WHERE 
			IFFM.intImportFileID = v_intImportFileID
			AND PARENT.numDomainID = v_numDomainID
			AND numFormID = v_numFormID
		UNION ALL
		SELECT DISTINCT 
			IFFM.numFormFieldID AS FormFieldID,
			IFFM.intMapColumnNo AS ImportFieldID,
			IFFM.intImportFileID,
			IFFM.intImportTransactionID,
			CFW.FLd_label AS dbFieldName,
			CFW.FLd_label AS FormFieldName,
			CASE 
				WHEN CFW.fld_type = 'Drop Down List Box'
				THEN 'SelectBox'
				ELSE CFW.fld_type
			END	AS vcAssociatedControlType,
			false AS bitDefault,
			'R' AS vcFieldType,
			'' AS vcListItemType,
			'' AS  vcLookBackTableName,
			CFW.FLd_label AS vcOrigDbColumnName,
			CAST('' AS CHAR(1)) AS vcPropertyName,
			true AS bitCustomField,
			0
		FROM Import_File_Field_Mapping IFFM
		INNER JOIN CFW_Fld_Master CFW ON IFFM.numFormFieldID = CFW.Fld_id
		INNER JOIN CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id
		INNER JOIN DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE 
			IFFM.intImportFileID = v_intImportFileID
			AND (DFSD.numFormID = v_numFormID OR DFSD.numFormID =(CASE WHEN v_numFormID = 20 THEN 48 ELSE v_numFormID END))
			AND Grp_id = DFSD.Loc_id) TABLE1
		ORDER BY intImportFileID DESC; 
END; $$;		











