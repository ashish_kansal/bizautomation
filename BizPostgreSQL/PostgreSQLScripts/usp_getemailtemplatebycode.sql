-- Stored procedure definition script USP_GetEmailTemplateByCode for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmailTemplateByCode(v_vcTemplateCode VARCHAR(50),
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numGenericDocID as VARCHAR(255)),
            cast(VcFileName as VARCHAR(255)),
            cast(VcDocName as VARCHAR(255)),
            cast(numDocCategory as VARCHAR(255)),
            cast(numDocStatus as VARCHAR(255)),
            cast(vcDocdesc as VARCHAR(255)),
            cast(vcSubject as VARCHAR(255)),
            cast(tintDocumentType as VARCHAR(255)),
            cast(numModuleId as VARCHAR(255))
   FROM    GenericDocuments
   WHERE   numDomainId = v_numDomainID
   AND numDocCategory = 369
   AND VcFileName = v_vcTemplateCode;
END; $$;
-- exec USP_GetEmbeddedCost @numEmbeddedCostID=0,@numOppBizDocID=11945,@numCostCatID=0,@numDomainID=110












