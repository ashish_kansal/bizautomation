-- Stored procedure definition script USP_GetItemTransitforSO for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemTransitforSO(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_vcItemcode VARCHAR(900) DEFAULT 0,
v_ClientTimeZoneOffset      INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select opp.numOppId,opp.vcpOppName,
case when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>' when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>' when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>' else FormatedDateFromDate(opp.bintCreatedDate,v_numDomainID) end  AS bintCreatedDate,
FormatedDateFromDate(opp.intPEstimatedCloseDate,v_numDomainID) AS DueDate,
fn_GetContactName(opp.numContactId) AS vcOrderedBy,
I.vcItemName || CASE WHEN I.vcSKU <> '' THEN '(' || I.vcSKU || ')' ELSE '' END AS ItemName,
CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OppI.numItemCode,opp.numDomainId,coalesce(OppI.numUOMId,0))*(coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0))) as NUMERIC(18,0)) as numUnitHour
,cast(coalesce(u.vcUnitName,'') as VARCHAR(255)) AS vcUOMName,cast(OppI.vcManufacturer as VARCHAR(255)),cast(coalesce(OppI.vcItemDesc,'') as VARCHAR(255)) as vcItemDesc,cast(OppI.vcModelID as VARCHAR(255)),
fn_GetListItemName(opp.intUsedShippingCompany::NUMERIC) AS vcShipmentMethod,
CASE WHEN coalesce(VSM.numListValue,0) > 0 THEN
      case when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
      when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
      when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
      else FormatedDateFromDate(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval),v_numDomainID) end
   ELSE '' end AS bintExpectedDelivery,
	 cast(OPA.vcShipCity as VARCHAR(255)) As vcDeliverTo,
	 CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
   WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
   WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
   WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
   ELSE '-'
   END AS vcDropShip,
 (select
      distinct
      OVERLAY((select ' ' || LD.vcData || '(<span class="text-primary">' || CAST(coalesce(VM.numListValue,0) AS VARCHAR(100)) || ' Days</span>)'
         from Listdetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID = LD.numListItemID
         AND VM.numVendorID = opp.numDivisionId AND VM.numAddressID = opp.numVendorAddressID AND VM.numDomainId = v_numDomainID
         WHERE LD.numListID = 338 AND (LD.numDomainid = v_numDomainID or LD.constFlag = true) AND coalesce(VM.numListValue,0) > 0
         order by  LD.vcData) placing '' from 1 for 1) as userlist
      from Listdetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID = LD.numListItemID
      AND VM.numVendorID = opp.numDivisionId AND VM.numAddressID = opp.numVendorAddressID AND VM.numDomainId = v_numDomainID AND coalesce(VM.numWarehouseID,0) = WI.numWareHouseID
      WHERE LD.numListID = 338 AND (LD.numDomainid = v_numDomainID or LD.constFlag = true) AND coalesce(VM.numListValue,0) > 0
      group by LD.vcData) AS vendorShipmentDays,
cast(CMP.vcCompanyName as VARCHAR(255))
   FROM OpportunityMaster opp INNER JOIN OpportunityItems OppI ON opp.numOppId = OppI.numOppId LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT join AdditionalContactsInformation ADC
   on ADC.numContactId = opp.numContactId
   LEFT join DivisionMaster DM
   on DM.numDivisionID = ADC.numDivisionId
   LEFT JOIN CompanyInfo CMP
   on DM.numCompanyID = CMP.numCompanyId
   INNER JOIN Item I on OppI.numItemCode = I.numItemCode
   LEFT JOIN OpportunityAddress AS OPA ON OPA.numOppID = OppI.numOppId
   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId
   LEFT JOIN LATERAL(SELECT * FROM VendorShipmentMethod WHERE numAddressID = opp.numVendorAddressID AND numVendorID = opp.numDivisionId ORDER BY coalesce(bitPrimary,false) DESC,coalesce(bitPreferredMethod,false) DESC LIMIT 1) VSM on TRUE
   WHERE opp.tintopptype = 2 and opp.tintoppstatus = 1 and opp.tintshipped = 0
   and opp.numDomainId = v_numDomainID AND OppI.numItemCode IN(SELECT CAST(Items AS NUMERIC) FROM Split(v_vcItemcode,',') WHERE Items <> '')
   and coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0) > 0
   ORDER BY OppI.vcItemName,opp.intPEstimatedCloseDate;
END; $$;











