-- Stored procedure definition script USP_ItemCategory_MassUpdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemCategory_MassUpdate(v_numDomainID NUMERIC(9,0)
	,v_tintType SMALLINT
	,v_numCategoryProfileID NUMERIC(18,0)
	,v_vcItemCodes TEXT
	,v_vcCategories TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_vcCategories,'') <> '' AND coalesce(v_vcItemCodes,'') <> '' then
	
      IF v_tintType = 1 then -- ADD
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            numItemCode NUMERIC(18,0),
            numCategoryID NUMERIC(18,0)
         );
         INSERT INTO
         tt_TEMP
         SELECT
         t1.ID
				,t2.ID
         FROM(SELECT coalesce(ID,0) AS ID FROM SplitIDs(v_vcItemCodes,',')) t1
         CROSS JOIN LATERAL(SELECT coalesce(ID,0) AS ID FROM SplitIDs(v_vcCategories,',')) t2;
         INSERT INTO ItemCategory(numCategoryID
				,numItemID)
         SELECT
         t1.numCategoryID
				,t1.numItemCode
         FROM
         tt_TEMP t1
         LEFT JOIN
         ItemCategory
         ON
         t1.numCategoryID = ItemCategory.numCategoryID
         AND t1.numItemCode = ItemCategory.numItemID
         WHERE
         t1.numCategoryID IN(SELECT coalesce(numCategoryID,0) FROM Category WHERE numDomainID = v_numDomainID AND numCategoryProfileID = v_numCategoryProfileID)
         AND ItemCategory.numCategoryID IS NULL;
      ELSEIF v_tintType = 2
      then -- REMOVE
		
         DELETE FROM ItemCategory IC using Category where IC.numCategoryID = Category.numCategoryID AND numItemID IN(SELECT coalesce(ID,0) from SplitIDs(v_vcItemCodes,','))
         AND IC.numCategoryID IN(SELECT coalesce(ID,0) from SplitIDs(v_vcCategories,','))
         AND numCategoryProfileID = v_numCategoryProfileID;
      end if;
   end if;
   RETURN;
END; $$;
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90


