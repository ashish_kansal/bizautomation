-- Stored procedure definition script USP_TerritoryName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TerritoryName(v_numTerritoryID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcTerName as VARCHAR(255)) from TerritoryMaster where numTerID = v_numTerritoryID;
END; $$;












