-- Stored procedure definition script USP_SaveMarketBudgetHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveMarketBudgetHeader(v_intFiscalYear NUMERIC(9,0) DEFAULT 0,                    
v_bitMarketCampaign  BOOLEAN DEFAULT NULL,                    
v_numDomainId NUMERIC(9,0) DEFAULT 0,                    
v_numUserCntId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMarketBudgetId  NUMERIC(9,0);
BEGIN
   Select coalesce(numMarketBudgetId,0) INTO v_numMarketBudgetId From MarketBudgetMaster Where  numDomainID = v_numDomainId;         
                 
   If v_numMarketBudgetId is null then
 
      Insert into MarketBudgetMaster(bitMarketCampaign,numDomainID,numCreatedBy,dtCreationDate)
  Values(v_bitMarketCampaign,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
  
      v_numMarketBudgetId := CURRVAL('MarketBudgetMaster_seq');
      open SWV_RefCur for
      Select v_numMarketBudgetId;
   Else
      Update MarketBudgetMaster Set bitMarketCampaign = v_bitMarketCampaign,numModifiedBy = v_numUserCntId,dtModifiedDate = TIMEZONE('UTC',now())
      Where numMarketBudgetId = v_numMarketBudgetId And numDomainID = v_numDomainId;
      open SWV_RefCur for
      Select v_numMarketBudgetId;
   end if;
   RETURN;
END; $$;


