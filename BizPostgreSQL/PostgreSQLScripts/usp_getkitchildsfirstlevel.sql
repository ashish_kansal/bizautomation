CREATE OR REPLACE FUNCTION USP_GetKitChildsFirstLevel
(
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_strItem VARCHAR(1000) DEFAULT NULL,
	v_numItemCode NUMERIC(18,0) DEFAULT 0,
	v_numPageIndex INTEGER DEFAULT NULL,
	v_numPageSize INTEGER DEFAULT NULL,    
	INOUT v_TotalCount INTEGER  DEFAULT NULL,
	INOUT SWV_RefCur refcursor DEFAULT null,
	INOUT SWV_RefCur2 refcursor DEFAULT null
)
LANGUAGE plpgsql
AS $$
   DECLARE
   v_strSQL  TEXT DEFAULT '';
   v_strSQL2  TEXT DEFAULT '';
   v_vcWareHouseSearch  VARCHAR(1000) DEFAULT '';
   v_vcVendorSearch  VARCHAR(1000) DEFAULT '';
   TotalCount INTEGER  DEFAULT 0;
   v_tintDecimalPoints  SMALLINT;
   v_tintOrder  INTEGER;
   v_Fld_id  VARCHAR(20);
   v_Fld_Name  VARCHAR(20);
        
   v_strSearch  TEXT;
   v_CustomSearch  TEXT;
   v_numFieldId  NUMERIC(18,0);

   v_strSQLCount  TEXT DEFAULT '';
   SWV_RowCount INTEGER;
BEGIN
   
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      numItemCode NUMERIC(18,0)
   );
   
   SELECT COALESCE(tintDecimalPoints, 4) 
   INTO v_tintDecimalPoints 
   FROM Domain 
   WHERE numDomainId = v_numDomainID;
               
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   
   CREATE TEMPORARY TABLE tt_TEMP1 AS
   SELECT * FROM (
	   			  SELECT numFieldID, vcDbColumnName
					  , coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
					  , tintRow AS tintOrder, 0 AS Custom
				  FROM View_DynamicColumns
				  WHERE numFormId = 22
				  AND numDomainID = v_numDomainID
				  AND tintPageType = 1
				  AND bitCustom = false
				  AND coalesce(bitSettingField,false) = true
				  AND numRelCntType = 0
				  UNION
				  SELECT numFieldId, vcFieldName, vcFieldName, tintRow AS tintOrder, 1 AS Custom
				  FROM View_DynamicCustomColumns
				  WHERE Grp_id = 5
				  AND numFormId = 22
				  AND numDomainID = v_numDomainID
				  AND tintPageType = 1
				  AND bitCustom = true
				  AND numRelCntType = 0
		) X; 

   IF NOT EXISTS(SELECT * FROM tt_TEMP1) THEN
      INSERT INTO tt_TEMP1
      SELECT numFieldID
			, vcDbColumnName
			, coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
			, tintOrder
			, 0
      FROM View_DynamicDefaultColumns
      WHERE numFormId = 22
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID;
   END IF;

   SELECT tintOrder + 1, numFieldId, vcFieldName 
   INTO v_tintOrder, v_Fld_id, v_Fld_Name 
   FROM tt_TEMP1 
   WHERE Custom = 1   
   ORDER BY tintOrder 
   LIMIT 1;

   WHILE v_tintOrder > 0 LOOP
      v_strSQL := COALESCE(v_strSQL,'') || ', GetCustFldValueItem(' || COALESCE(v_Fld_id,'') || ', I.numItemCode) AS "' || COALESCE(v_Fld_Name,'') || '"';
      
	  SELECT tintOrder + 1, numFieldId, vcFieldName 
	  INTO v_tintOrder, v_Fld_id, v_Fld_Name 
	  FROM tt_TEMP1 
	  WHERE Custom = 1
      AND tintOrder >= v_tintOrder   
	  ORDER BY tintOrder LIMIT 1;
      
	  GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      
	  IF SWV_RowCount = 0 THEN
         v_tintOrder := 0;
      END IF;
	  
   END LOOP;

	--Temp table for Item Search Configuration

   DROP TABLE IF EXISTS tt_TEMPSEARCH CASCADE;
   
   CREATE TEMPORARY TABLE tt_TEMPSEARCH AS
      SELECT * FROM
	  (
		  SELECT numFieldID,
				vcDbColumnName ,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
				tintRow AS tintOrder ,
				0 AS Custom
		  FROM View_DynamicColumns
		  WHERE numFormId = 22
		  AND numDomainID = v_numDomainID
		  AND tintPageType = 1
		  AND bitCustom = false
		  AND coalesce(bitSettingField,false) = true
		  AND numRelCntType = 1
		  UNION
		  SELECT numFieldId,
				vcFieldName ,
				vcFieldName ,
				tintRow AS tintOrder ,
				1 AS Custom
		  FROM View_DynamicCustomColumns
		  WHERE Grp_id = 5
		  AND numFormId = 22
		  AND numDomainID = v_numDomainID
		  AND tintPageType = 1
		  AND bitCustom = true
		  AND numRelCntType = 1
	  ) X; 
  
   IF NOT EXISTS(SELECT * FROM tt_TEMPSEARCH) THEN
      INSERT INTO tt_TEMPSEARCH
      SELECT numFieldID,
            vcDbColumnName,
            coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
            tintOrder,
            0
      FROM View_DynamicDefaultColumns
      WHERE numFormId = 22
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID;
   END IF;

   v_strSearch := '';
        
   SELECT tintOrder + 1, vcDbColumnName, numFieldId 
   INTO v_tintOrder, v_Fld_Name, v_numFieldId 
   FROM tt_TEMPSEARCH 
   WHERE Custom = 0   
   ORDER BY tintOrder 
   LIMIT 1;
        
   WHILE v_tintOrder > -1 LOOP
      IF v_Fld_Name = 'vcPartNo' THEN
		
         INSERT INTO tt_TEMPITEMS
         SELECT	DISTINCT Item.numItemCode
         FROM	Vendor
         JOIN	Item ON Item.numItemCode = Vendor.numItemCode
         WHERE	Item.numDomainID = v_numDomainID
         AND Vendor.numDomainID = v_numDomainID
         AND Vendor.vcPartNo IS NOT NULL
         AND LENGTH(Vendor.vcPartNo) > 0
         AND vcPartNo ILIKE '%' || COALESCE(v_strItem,'') || '%';
				
         v_vcVendorSearch := 'Vendor.vcPartNo ILIKE ''%' || COALESCE(v_strItem,'') || '%''';
		 
      ELSEIF v_Fld_Name = 'vcBarCode' THEN
         INSERT INTO tt_TEMPITEMS
         SELECT DISTINCT Item.numItemCode
         FROM Item
         JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
         AND Item.numItemCode = WI.numItemID
         JOIN Warehouses W ON W.numDomainID = v_numDomainID
         AND W.numWareHouseID = WI.numWareHouseID
         WHERE Item.numDomainID = v_numDomainID
         AND coalesce(Item.numItemGroup,0) > 0
         AND WI.vcBarCode IS NOT NULL
         AND LENGTH(WI.vcBarCode) > 0
         AND WI.vcBarCode ILIKE '%' || COALESCE(v_strItem,'') || '%';
		 
		 v_vcWareHouseSearch := 'WareHouseItems.vcBarCode ILIKE ''%' || COALESCE(v_strItem,'') || '%''';
      
	  ELSEIF v_Fld_Name = 'vcWHSKU' THEN
         INSERT INTO tt_TEMPITEMS
         SELECT DISTINCT Item.numItemCode
         FROM Item
         JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
         AND Item.numItemCode = WI.numItemID
         JOIN Warehouses W ON W.numDomainID = v_numDomainID
         AND W.numWareHouseID = WI.numWareHouseID
         WHERE Item.numDomainID = v_numDomainID
         AND COALESCE(Item.numItemGroup,0) > 0
         AND WI.vcWHSKU IS NOT NULL
         AND LENGTH(WI.vcWHSKU) > 0
         AND WI.vcWHSKU ILIKE '%' || coalesce(v_strItem,'') || '%';
         
		 IF LENGTH(v_vcWareHouseSearch) > 0 THEN
            v_vcWareHouseSearch := COALESCE(v_vcWareHouseSearch,'') || ' OR WareHouseItems.vcWHSKU ILIKE ''%' || COALESCE(v_strItem,'') || '%''';
         ELSE
            v_vcWareHouseSearch := 'WareHouseItems.vcWHSKU ILIKE ''%' || coalesce(v_strItem,'') || '%''';
         END IF;
		 
      ELSE
         v_strSearch := COALESCE(v_strSearch,'') ||(CASE v_Fld_Name WHEN 'numItemCode' THEN ' I."' || COALESCE(v_Fld_Name,'') ELSE ' I.' || COALESCE(v_Fld_Name,'') END)  || ' ILIKE ''%' || COALESCE(v_strItem,'') || '%''';
      
	  END IF;
      
	  select   tintOrder+1, vcDbColumnName, numFieldId 
	  INTO v_tintOrder,v_Fld_Name,v_numFieldId 
	  FROM tt_TEMPSEARCH 
	  WHERE tintOrder >= v_tintOrder
      AND Custom = 0   
	  ORDER BY tintOrder 
	  LIMIT 1;
      
	  GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      
	  IF SWV_RowCount = 0 THEN
         v_tintOrder := -1;
      ELSE
         IF v_Fld_Name != 'vcPartNo' AND v_Fld_Name != 'vcBarCode' AND v_Fld_Name != 'vcWHSKU' THEN
            v_strSearch := COALESCE(v_strSearch,'') || ' or ';
         END IF;
      END IF;
   
   END LOOP;

   IF(SELECT COUNT(*) FROM tt_TEMPITEMS) > 0 THEN
      --v_strSearch := COALESCE(v_strSearch,'') || ' OR  I.numItemCode IN (SELECT numItemCode FROM tt_tempItemCode)';
	  --I think above tt_tempItemCode table was not created in this function so it's wrong to check it, 
	  --We might need to compare with a relevant input parameter which was not being used anywhere else
	  v_strSearch := COALESCE(v_strSearch,'') || ' OR  I.numItemCode = ' || v_numItemCode ;
   END IF;

	--Custom Search
   v_CustomSearch := '';
   
   SELECT tintOrder + 1, vcDbColumnName, numFieldId 
   INTO v_tintOrder, v_Fld_Name, v_numFieldId 
   FROM tt_TEMPSEARCH 
   WHERE Custom = 1   
   ORDER BY tintOrder 
   LIMIT 1;

   WHILE v_tintOrder > -1 LOOP
      v_CustomSearch := COALESCE(v_CustomSearch, '') || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)), 1, 10);
      
	  SELECT tintOrder+1, vcDbColumnName, numFieldId 
	  INTO v_tintOrder,v_Fld_Name,v_numFieldId 
	  FROM tt_TEMPSEARCH 
	  WHERE tintOrder >= v_tintOrder
      AND Custom = 1   
	  ORDER BY tintOrder 
	  LIMIT 1;
      
	  GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      
	  IF SWV_RowCount = 0 THEN    
         v_tintOrder := -1;
      ELSE
         v_CustomSearch := COALESCE(v_CustomSearch, '') || ' , ';
      END IF;
	  
   END LOOP;
	
   IF LENGTH(v_CustomSearch) > 0 THEN    
      v_CustomSearch := ' I.numItemCode IN (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID IN ('
      || COALESCE(v_CustomSearch, '') || ') AND GetCustFldValueItem(Fld_ID, RecId) ILIKE ''%'
      || COALESCE(v_strItem, '') || '%'')';
      
	  IF LENGTH(v_strSearch) > 0 THEN
         v_strSearch := COALESCE(v_strSearch, '') || ' OR ' || COALESCE(v_CustomSearch, '');
      ELSE
         v_strSearch := v_CustomSearch;
      END IF;
   END IF;

   v_strSQLCount := '
   SELECT COALESCE((SELECT 
										COUNT(I.numItemCode) 
									FROM 
										Item  I
									LEFT JOIN 
										DivisionMaster D              
									ON 
										I.numVendorID=D.numDivisionID  
									LEFT JOIN 
										CompanyInfo C  
									ON 
										C.numCompanyID=D.numCompanyID
									LEFT JOIN 
										CustomerPartNumber  CPN  
									ON 
										I.numItemCode=CPN.numItemCode 
										AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' ||
   CASE LENGTH(v_vcWareHouseSearch)
   WHEN 0
   THEN
      'SELECT 
																				numWareHouseItemID 
																			FROM 
																				WareHouseItems 
																			WHERE 
																				WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																				AND WareHouseItems.numItemID = I.numItemCode 
																				LIMIT 1'
   ELSE
      'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (' || coalesce(v_vcWareHouseSearch,'') || ')) > 0 
																				THEN
																					(SELECT 
																						numWareHouseItemID 
																					FROM 
																						WareHouseItems 
																					WHERE 
																						WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																						AND WareHouseItems.numItemID = I.numItemCode 
																						AND (' || coalesce(v_vcWareHouseSearch,'') || ')
																						LIMIT 1)
																				ELSE
																					(SELECT 
																						numWareHouseItemID 
																					FROM 
																						WareHouseItems 
																					WHERE 
																						WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																						AND WareHouseItems.numItemID = I.numItemCode
																						LIMIT 1
																					)
																				END'
   END
   ||
   ') 
									WHERE ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
   || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20)
   || ') > 0)) AND COALESCE(I.bitKitParent,false) = false AND COALESCE(I.bitAssembly,false) = false AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN (''A'') AND  I.numDomainID='
   || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || CASE WHEN coalesce(v_strSearch,'') <> '' THEN ' AND ' || coalesce(v_strSearch,'') ELSE '' END;  
						    
   v_strSQLCount := COALESCE(v_strSQLCount,'') || '),0);';
   
   RAISE NOTICE '%', v_strSQLCount;
   
   v_strSQLCount := REPLACE(v_strSQLCount, 'I.vcCompanyName', 'C.vcCompanyName');
   v_strSQLCount := REPLACE(v_strSQLCount, 'I.monListPrice', 'CAST(I.monListPrice AS VARCHAR)');                
   
   EXECUTE (v_strSQLCount) INTO v_TotalCount;
   
   v_strSQL := ' 
   DROP TABLE IF EXISTS tt_TEMPItems2 CASCADE;
   
   CREATE TEMPORARY TABLE tt_TEMPItems2
   AS
   SELECT I.numItemCode AS "numItemCode", COALESCE(CPN.CustomerPartNo,'''')  AS "CustomerPartNo",
									(SELECT vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForImage",
									(SELECT vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForTImage",
									COALESCE(I.vcItemName,'''') AS "vcItemName",
									CAST(CAST(CASE 
									WHEN I.charItemType=''P''
									THEN COALESCE((SELECT monWListPrice FROM WareHouseItems WHERE numItemID=I.numItemCode LIMIT 1),0) 
									ELSE 
									monListPrice 
									END AS DECIMAL(20,' || SUBSTR(CAST(COALESCE(v_tintDecimalPoints, 0) AS VARCHAR(30)),1,30) || ')) AS VARCHAR) AS "monListPrice",
									COALESCE(vcSKU,'''') AS "vcSKU",
									COALESCE(numBarCodeId, '''') AS "numBarCodeId",
									COALESCE(vcModelID,'''') AS "vcModelID",
									COALESCE(txtItemDesc,'''') as "txtItemDesc",
									COALESCE(C.vcCompanyName,'''') as "vcCompanyName",
									D.numDivisionID AS "numDivisionID",
									WHT.numWareHouseItemID AS "numWareHouseItemID",
									fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS "vcAttributes",
									(CASE 
										WHEN COALESCE(I.bitKitParent,false) = true
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) AS "bitHasKitAsChild"
									,COALESCE(I.bitMatrix,false) AS "bitMatrix"
									,COALESCE(I.numItemGroup,0) AS "numItemGroup"
									,I.charItemType AS "charItemType"
									,COALESCE(I.bitKitParent,false) AS "bitKitParent"
									,COALESCE(I.bitAssembly,false) AS "bitAssembly" '
   || COALESCE(v_strSQL,'')
   || ' 
   FROM Item  I
									Left join DivisionMaster D              
									on I.numVendorID=D.numDivisionID  
									left join CompanyInfo C  
									on C.numCompanyID=D.numCompanyID
									LEFT JOIN CustomerPartNumber  CPN  
									ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' ||
   CASE LENGTH(v_vcWareHouseSearch)
   WHEN 0
   THEN
      'SELECT 
																				numWareHouseItemID 
																			FROM 
																				WareHouseItems 
																			WHERE 
																				WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																				AND WareHouseItems.numItemID = I.numItemCode 
																				LIMIT 1'
   ELSE
      'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (' || COALESCE(v_vcWareHouseSearch,'') || ')) > 0 
																				THEN
																					(SELECT 
																						numWareHouseItemID 
																					FROM 
																						WareHouseItems 
																					WHERE 
																						WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																						AND WareHouseItems.numItemID = I.numItemCode 
																						AND (' || COALESCE(v_vcWareHouseSearch,'') || ')
																						LIMIT 1)
																				ELSE
																					(SELECT 
																						numWareHouseItemID 
																					FROM 
																						WareHouseItems 
																					WHERE 
																						WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																						AND WareHouseItems.numItemID = I.numItemCode 
																					LIMIT 1
																					)
																				END'
   END
   ||
   ') 
									where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
   || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ') > 0)) AND COALESCE(I.bitKitParent,false) = false AND COALESCE(I.bitAssembly,false) = false AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID='
   || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || CASE WHEN coalesce(v_strSearch,'') <> '' THEN ' AND ' || coalesce(v_strSearch,'') ELSE '' END;   
						     
   IF LENGTH(v_strSQL) > 0 THEN
   v_strSQL := COALESCE(v_strSQL,'') || CONCAT(' ORDER BY I.vcItemName OFFSET ',(v_numPageIndex::bigint -1)*v_numPageSize::bigint,
   ' ROWS FETCH NEXT ',v_numPageSize,' ROWS ONLY; ');
   END IF;
   
   v_strSQL := REPLACE(v_strSQL, 'I.vcCompanyName', 'C.vcCompanyName');
   v_strSQL := REPLACE(v_strSQL, 'I.monListPrice', 'CAST(I.monListPrice AS VARCHAR)');                
   
   EXECUTE (v_strSQL);
   
   v_strSQL2 := '
   DROP TABLE IF EXISTS tt_TEMPItemsFinal CASCADE; 
   
   CREATE TEMPORARY TABLE tt_TEMPItemsFinal
   AS
   SELECT ROW_NUMBER() OVER( ORDER BY vcItemName) AS "SRNO", (CASE WHEN (vcSKU=''' || COALESCE(v_strItem,'') || ''' OR ' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR(30)) || '=209) THEN 1 ELSE 0 END) AS "bitSKUMatch"
   , * 
   FROM tt_TEMPItems2;
   ';

   EXECUTE (v_strSQL2);
  
   OPEN SWV_RefCur FOR 
   SELECT * FROM tt_TEMPItemsFinal ORDER BY SRNO; 

   OPEN SWV_RefCur2 FOR
   SELECT * FROM tt_TEMP1 WHERE vcDbColumnName NOT IN ('vcPartNo','vcBarCode','vcWHSKU')  ORDER BY tintOrder; 

   RETURN;
END; 
$$;