-- Stored procedure definition script USP_GetReportListMasterDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReportListMasterDetail(v_numDomainID NUMERIC(18,0),
	v_numReportID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor , INOUT SWV_RefCur6 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT RLM.*,RMM.vcModuleName,RMGM.vcGroupName FROM ReportListMaster RLM
   JOIN ReportModuleMaster RMM ON RLM.numReportModuleID = RMM.numReportModuleID
   JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID = RMGM.numReportModuleGroupID
   WHERE RLM.numDomainID = v_numDomainID AND RLM.numReportID = v_numReportID;

   open SWV_RefCur2 for
   SELECT ReportFieldsList.*,coalesce(vcFieldDataType,'') AS vcFieldDataType FROM ReportFieldsList LEFT JOIN ReportFieldGroupMappingMaster ON ReportFieldsList.numFieldID = ReportFieldGroupMappingMaster.numFieldId AND ReportFieldsList.numReportFieldGroupID = ReportFieldGroupMappingMaster.numReportFieldGroupID WHERE numReportID = v_numReportID ORDER BY numReportFieldID;

   open SWV_RefCur3 for
   SELECT * FROM ReportFilterList WHERE numReportID = v_numReportID ORDER BY numReportFilterID;

   open SWV_RefCur4 for
   SELECT * FROM ReportSummaryGroupList WHERE numReportID = v_numReportID ORDER BY numReportGroupID;

--SELECT * FROM ReportMatrixBreakList WHERE numReportID=@numReportID ORDER BY numReportMatrixID

   open SWV_RefCur5 for
   SELECT * FROM ReportMatrixAggregateList WHERE numReportID = v_numReportID ORDER BY numReportAggregateID;

   open SWV_RefCur6 for
   SELECT * FROM ReportKPIThresold WHERE numReportID = v_numReportID ORDER BY numReportThresoldID;
/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
   RETURN;
END; $$;


