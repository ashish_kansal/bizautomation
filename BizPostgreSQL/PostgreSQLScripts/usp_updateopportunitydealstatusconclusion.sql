-- Stored procedure definition script USP_UpdateOpportunityDealStatusConclusion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOpportunityDealStatusConclusion(v_numDomainID NUMERIC(9,0),
 v_numOppId NUMERIC(9,0),
 v_tintOppStatus INTEGER DEFAULT 0,
 v_lngPConclAnalysis NUMERIC(18,0) DEFAULT 0,
 v_numUserCntID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitViolatePrice  BOOLEAN;
   v_intPendingApprovePending  INTEGER;
   v_intExecuteDiv  INTEGER DEFAULT 0;
   v_numDivisionID  NUMERIC DEFAULT 0;
   v_tintCommitAllocation  SMALLINT;
   v_tintOldOppStatus  SMALLINT;
   v_tintCRMType  SMALLINT;
BEGIN
   IF(v_tintOppStatus > 0) then
	
		--UPDATE 
		--	OpportunityMaster
		--SET
		--	tintOppStatus=@tintOppStatus
		--WHERE
		--	numDomainId=@numDomainID AND
		--	numOppId=@numOppId
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      IF EXISTS(SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID = v_numDomainID AND coalesce(bitMarginPriceViolated,false) = true) then
		
         v_bitViolatePrice := true;
      end if;
      SELECT COUNT(*) INTO v_intPendingApprovePending FROM OpportunityItems WHERE numOppId = v_numOppId AND coalesce(bitItemPriceApprovalRequired,false) = true;
      IF(v_intPendingApprovePending > 0 AND v_bitViolatePrice = true AND v_tintOppStatus = cast(NULLIF('1','') as INTEGER)) then
		
         v_intExecuteDiv := 1;
      end if;
      IF coalesce(v_intExecuteDiv,0) = 0 then
         select   tintoppstatus INTO v_tintOldOppStatus FROM OpportunityMaster WHERE numOppId = v_numOppId;
         IF v_tintOldOppStatus = 0 AND v_tintOppStatus = cast(NULLIF('1','') as INTEGER) then --Open to Won
            select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numOppId = v_numOppId;
            select   tintCRMType INTO v_tintCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID; 

				-- Promote Lead to Account when Sales/Purchase Order is created against it
            IF v_tintCRMType = 0 then --Lead & Order
				
               UPDATE
               DivisionMaster
               SET
               tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
               bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
               WHERE
               numDivisionID = v_numDivisionID;
				--Promote Prospect to Account
            ELSEIF v_tintCRMType = 1
            then
				
               UPDATE
               DivisionMaster
               SET
               tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
               WHERE
               numDivisionID = v_numDivisionID;
            end if;
            IF v_tintCommitAllocation = 1 then
				
               PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppId,0,v_numUserCntID);
            end if;
            UPDATE OpportunityMaster SET bintOppToOrder = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppId and numDomainId = v_numDomainID;
            UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = NULL WHERE numOppId = v_numOppId;
            PERFORM USP_OpportunityItems_CheckWarehouseMapping(v_numDomainID,v_numOppId);
         ELSEIF (v_tintOppStatus = cast(NULLIF('2','') as INTEGER))
         then -- Win to Lost
			
            UPDATE OpportunityMaster SET dtDealLost = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppId and numDomainId = v_numDomainID;
         end if;
         IF ((v_tintOldOppStatus = 1 and v_tintOppStatus = cast(NULLIF('2','') as INTEGER)) or (v_tintOldOppStatus = 1 and v_tintOppStatus = cast(NULLIF('0','') as INTEGER))) AND v_tintCommitAllocation = 1 then --Won to Lost or Won to Open
            PERFORM USP_RevertDetailsOpp(v_numOppId,0,0::SMALLINT,v_numUserCntID);
         end if;
         UPDATE OpportunityMaster SET tintoppstatus = v_tintOppStatus WHERE numOppId = v_numOppId;
      end if;
   end if;
   IF(v_lngPConclAnalysis > 0) then
	
      UPDATE
      OpportunityMaster
      SET
      lngPConclAnalysis = v_lngPConclAnalysis
      WHERE
      numDomainId = v_numDomainID AND
      numOppId = v_numOppId;
   end if;
   RETURN;
END; $$;



