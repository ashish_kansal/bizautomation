-- Stored procedure definition script USP_OpportunityMaster_GetOrderDetailForBizDoc for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetOrderDetailForBizDoc(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   OpportunityMaster.numOppId
		,OpportunityMaster.tintopptype
		,OpportunityMaster.numContactId
		,DivisionMaster.numCurrencyID AS numBaseCurrencyID
		,coalesce(OpportunityMaster.numCurrencyID,0) AS numCurrencyID
		,DivisionMaster.numDivisionID
		,CompanyInfo.numCompanyType
		,CompanyInfo.vcProfile
		,DivisionMaster.numDivisionID
		,CompanyInfo.vcCompanyName
		,coalesce(OpportunityMaster.fltExchangeRate,1) AS fltExchangeRate
		,coalesce(OpportunityMaster.intUsedShippingCompany,DivisionMaster.intShippingCompany) AS intUsedShippingCompany
   FROM
   OpportunityMaster
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND numOppId = v_numOppID;
END; $$;












