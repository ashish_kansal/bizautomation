CREATE OR REPLACE FUNCTION USP_WorkOrder_GetWorkOrderDetailByID(v_numDomainID NUMERIC(18,0),
	v_numWOID NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for
	SELECT
		WO.numWOId AS "numWOId",
		Opp.numDivisionId AS "numDivisionId",
		coalesce(WO.numAssignedTo,0) AS "numRecOwner",
		CONCAT(',',COALESCE((SELECT
				string_agg(CAST(SPDT.numAssignTo AS VARCHAR),',')
			  FROM
				StagePercentageDetailsTask SPDT
			  WHERE
				SPDT.numDomainID = v_numDomainID
				AND SPDT.numWorkOrderId = WO.numWOId),''),',') AS "vcTasksAssignee",
		coalesce(WO.monAverageCost,0) AS "monAverageCost",
		I.numItemCode AS "numItemCode",
		coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') AS "vcPathForTImage",
		coalesce(WO.numOppId,0) AS "numOppId",
		I.vcItemName AS "vcItemName",
		I.numAssetChartAcntId AS "numAssetChartAcntId",
		WO.numQtyItemsReq AS "numQtyItemsReq",
		WO.vcWorkOrderName AS "vcWorkOrderName",
		WO.numBuildProcessId AS "numBuildProcessId",
		coalesce(Slp_Name,'') AS "vcProcessName",
		WO.numWOStatus AS "numWOStatus",
		GetTotalProgress(v_numDomainID,WO.numWOId,1::SMALLINT,1::SMALLINT,'',0) AS "numTotalProgress",
		(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = WO.numWOId)) THEN true ELSE false END) AS "bitTaskStarted",
		CONCAT(fn_GetContactName(WO.numCreatedBy),' ',FormatedDateTimeFromDate(WO.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)) AS "CreatedBy",
        CONCAT(fn_GetContactName(WO.numModifiedBy),' ',FormatedDateTimeFromDate(WO.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)) AS "ModifiedBy",
		CONCAT(fn_GetContactName(WO.numCompletedBy),' ',FormatedDateTimeFromDate(WO.bintCompletedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)) AS "CompletedBy",
		coalesce(A.vcFirstName,'') || ' ' || COALESCE(A.vcLastname,'') AS "vcContactName",
        coalesce(A.vcEmail,'') AS "vcEmail",
        coalesce(A.numPhone,'') AS "Phone",
        coalesce(A.numPhoneExtension,'') AS "PhoneExtension",
        CONCAT(C2.vcCompanyName,Case when coalesce(D2.numCompanyDiff,0) > 0 then  '  ' || fn_GetListItemName(D2.numCompanyDiff) || ':' || coalesce(D2.vcCompanyDiff,'') else '' end) as "vcCompanyname",
		coalesce((SELECT  coalesce(vcData,'') FROM Listdetails WHERE numListID = 5 AND numListItemID = C2.numCompanyType AND numDomainid = v_numDomainID LIMIT 1),'') AS "vcCompanyType",
		CONCAT(fn_GetContactName(WO.numCompletedBy),' ',FormatedDateTimeFromDate(WO.bintCompletedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)) AS "CompletedBy",
		CONCAT(coalesce(WO.numQtyBuilt,0),' (',WO.numQtyItemsReq,')') AS "vcBuiltQty",
		(CASE
			WHEN EXISTS(SELECT
							WorkOrderDetails.numWODetailId
						FROM
							WorkOrderDetails
						LEFT JOIN LATERAL(SELECT
											SUM(numPickedQty) AS numPickedQty
										FROM
											WorkOrderPickedItems
										WHERE
											WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId) TEMP on TRUE
						WHERE
							WorkOrderDetails.numWOId = v_numWOID
							AND coalesce(numWareHouseItemId,0) > 0
							AND coalesce(WorkOrderDetails.numQtyItemsReq,0) <> coalesce(TEMP.numPickedQty,0))
		THEN false
		ELSE true
		END) AS "bitBOMPicked"
	FROM
		WorkOrder WO
	LEFT JOIN 
		OpportunityMaster Opp
	ON
		WO.numOppId = Opp.numOppId
	LEFT JOIN 
		DivisionMaster D2
	ON 
		Opp.numDivisionId = D2.numDivisionID
	LEFT JOIN 
		CompanyInfo C2 
	ON 
		C2.numCompanyId = D2.numCompanyID
	LEFT JOIN 
		AdditionalContactsInformation A 
	ON 
		Opp.numContactId = A.numContactId
	LEFT JOIN
		Sales_process_List_Master SPLFM
	ON
		WO.numBuildProcessId = SPLFM.Slp_Id
	INNER JOIN
		Item I
	ON
		WO.numItemCode = I.numItemCode
	WHERE
		WO.numDomainId = v_numDomainID
		AND WO.numWOId = v_numWOID;

	open SWV_RefCur2 for
	SELECT
		WOD.numQtyItemsReq AS "RequiredQty",
		WOD.monAverageCost AS "monAverageCost",
		I.numItemCode AS "numItemCode",
		I.vcItemName AS "vcItemName",
		I.numAssetChartAcntId AS "numAssetChartAcntId"
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item I
	ON
		WOD.numChildItemID = I.numItemCode
	WHERE
		WOD.numWOId = v_numWOID
		AND I.charItemType = 'P';

	RETURN;
END; $$;



