-- Stored procedure definition script usp_GetSurveyRespondentForBusinessCreation for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyRespondentForBusinessCreation(v_numSurId NUMERIC,  
 v_numRespondantID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all survey Respondent information for the selected survey    
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(vcDbColumnName as VARCHAR(255)),
  cast(Case When vcDbColumnValue = '' Then
      vcDbColumnValueText
   Else
      vcDbColumnValue
   End as VARCHAR(255))
   as vcDbColumnValue, cast(vcDbColumnValueText as VARCHAR(255))
   FROM SurveyRespondentsChild
   WHERE numRespondantID = v_numRespondantID
   AND numSurID = v_numSurId;
END; $$;












