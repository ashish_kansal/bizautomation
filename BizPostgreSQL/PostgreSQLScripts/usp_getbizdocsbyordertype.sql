-- Stored procedure definition script USP_GetBizDocsByOrderType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBizDocsByOrderType(v_ListID NUMERIC(9,0) DEFAULT 0,                
	v_numDomainID NUMERIC(9,0) DEFAULT 0, 
	v_tintOppType SMALLINT DEFAULT NULL,
	v_tintOppStatus SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAuthoritativeId  NUMERIC(9,0);
BEGIN


   If v_tintOppType = 1 then
      select   numAuthoritativeSales INTO v_numAuthoritativeId FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID;
   ELSEIF v_tintOppType = 2
   then
      select   numAuthoritativePurchase INTO v_numAuthoritativeId FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID;
   end if;            
 

   IF v_tintOppStatus = 1 then
	
      IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then
		
         open SWV_RefCur for
         SELECT
         numListItemID,
				case when v_numAuthoritativeId = numListItemID then  vcData || ' - Authoritative' Else vcData   End as  vcData
         FROM
         Listdetails
         WHERE
         numListID = v_ListID AND
				(constFlag = true OR numDomainid = v_numDomainID);
      ELSE
         open SWV_RefCur for
         SELECT
         Ld.numListItemID,
				case when v_numAuthoritativeId = Ld.numListItemID then  coalesce(vcRenamedListName,vcData) || ' - Authoritative' Else coalesce(vcRenamedListName,vcData)   End as  vcData
         FROM
         BizDocFilter BDF
         INNER JOIN
         Listdetails Ld
         ON
         BDF.numBizDoc = Ld.numListItemID
         LEFT JOIN
         listorder LO
         ON
         Ld.numListItemID = LO.numListItemID AND
         LO.numDomainId = v_numDomainID
         WHERE
         Ld.numListID = v_ListID AND
         BDF.tintBizocType = v_tintOppType AND
         BDF.numDomainID = v_numDomainID AND
				(constFlag = true OR Ld.numDomainid = v_numDomainID);
      end if;
   Else
      IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID = v_numDomainID) = 0) then
		
         open SWV_RefCur for
         SELECT
         numListItemID,
				vcData as  vcData
         FROM
         Listdetails
         WHERE
         numListID = v_ListID and
				(constFlag = true or numDomainid = v_numDomainID) And
         numListItemID <> v_numAuthoritativeId;
      ELSE
         open SWV_RefCur for
         SELECT
         Ld.numListItemID,
				coalesce(vcRenamedListName,vcData) as vcData
         FROM
         BizDocFilter BDF
         INNER JOIN
         Listdetails Ld
         ON
         BDF.numBizDoc = Ld.numListItemID
         LEFT JOIN
         listorder LO
         ON
         Ld.numListItemID = LO.numListItemID AND
         LO.numDomainId = v_numDomainID
         WHERE
         Ld.numListID = v_ListID AND
         BDF.tintBizocType = v_tintOppType AND
         BDF.numDomainID = v_numDomainID AND
				(constFlag = true or Ld.numDomainid = v_numDomainID) AND
         Ld.numListItemID <> v_numAuthoritativeId AND
         Ld.numListItemID NOT IN(296);
      end if;
   end if;
   RETURN;
END; $$;









