DROP FUNCTION IF EXISTS USP_GetMirrorBizDocItems;

CREATE OR REPLACE FUNCTION USP_GetMirrorBizDocItems
(
	v_numReferenceID NUMERIC(9,0) DEFAULT NULL,
    v_numReferenceType SMALLINT DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0, 
	INOUT SWV_RefCur refcursor default null, 
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_DivisionID  NUMERIC(9,0);                                                                                                                                                                                                                                                                                                                                            
	v_tintReturnType  NUMERIC(9,0);
	v_DecimalPoint  SMALLINT;
	v_strSQL  TEXT;                                                                                          
	v_strSQL1  VARCHAR(8000);                                      
	v_strSQLCusFields  VARCHAR(1000);                  
	v_strSQLEmpFlds  VARCHAR(500);                                                                      
	v_tintType  SMALLINT;
	v_intRowNum  INTEGER;                                             
	v_numFldID  VARCHAR(15);                                     
	v_vcFldname  VARCHAR(50);                                          
	v_numBizDocId  NUMERIC(9,0);
	v_numBizDocTempID  NUMERIC(9,0);
	v_strSQLUpdate  VARCHAR(2000);
	v_vcTaxName  VARCHAR(100);
	v_numTaxItemID  NUMERIC(9,0);
	v_vcDbColumnName  VARCHAR(50);
	SWV_ExecDyn  VARCHAR(5000);
	SWV_RCur REFCURSOR;
	SWV_RCur2 REFCURSOR;
	SWV_RowCount INTEGER;
BEGIN
	IF v_numReferenceType = 1 OR v_numReferenceType = 2 OR v_numReferenceType = 3 OR v_numReferenceType = 4 then
		SELECT * INTO SWV_RefCur,SWV_RefCur2 FROM USP_MirrorOPPBizDocItems(v_numReferenceID,v_numDomainID,'SWV_RCur','SWV_RCur2');
	ELSE
		SELECT 
			numDivisionId,tintReturnType INTO v_DivisionID,v_tintReturnType 
		FROM 
			ReturnHeader 
		WHERE 
			numReturnHeaderID = v_numReferenceID;                                                                                                                                                                          
	
		--Decimal points to show in bizdoc 
		SELECT 
			tintDecimalPoints INTO v_DecimalPoint 
		FROM 
			Domain 
		WHERE 
			numDomainId = v_numDomainID;
		
		v_strSQLCusFields := '';
		v_strSQLEmpFlds := '';
		v_numBizDocTempID := 0;
      
		IF v_numReferenceType = 5 OR v_numReferenceType = 6 then
			SELECT COALESCE(numRMATempID,0) INTO v_numBizDocTempID FROM ReturnHeader WHERE numReturnHeaderID = v_numReferenceID;
		ELSEIF v_numReferenceType = 7 OR v_numReferenceType = 8  OR v_numReferenceType = 9  OR v_numReferenceType = 10 then
			SELECT COALESCE(numBizdocTempID,0) INTO v_numBizDocTempID FROM    ReturnHeader WHERE   numReturnHeaderID = v_numReferenceID;
		end if;

		SELECT numBizDocID INTO v_numBizDocId FROM BizDocTemplate WHERE numBizDocTempID = v_numBizDocTempID AND numDomainID = v_numDomainID;
      
		IF v_numReferenceType = 5 then
			SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'RMA' AND constFlag = true;
			v_tintType := 7;
		ELSEIF v_numReferenceType = 6 then
			SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'RMA' AND constFlag = true;
			v_tintType := 8;
		ELSEIF v_numReferenceType = 7 then
			select numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'Sales Credit Memo' AND constFlag = true;
			v_tintType := 7;
		ELSEIF v_numReferenceType = 8 then
			select numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'Purchase Credit Memo' AND constFlag = true;
			v_tintType := 8;
		ELSEIF v_numReferenceType = 9 then
			select numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'Refund Receipt' AND constFlag = true;
			v_tintType := 7;
		ELSEIF v_numReferenceType = 10 then
			select numListItemID INTO v_numBizDocId FROM Listdetails WHERE vcData = 'Credit Memo' AND constFlag = true;
			v_tintType := 7;
		end if;

		DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
		CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS         
		SELECT 
			* 
		FROM
		(
			SELECT 
				numReturnItemID
				,I.vcItemName AS vcItemName
				,0 AS OppBizDocItemID
				,CASE 
					WHEN charitemType = 'P' THEN 'Product'
					 WHEN charitemType = 'S' THEN 'Service'
					 END AS charitemType
				,RI.vcItemDesc AS txtItemDesc
				,fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(RI.numUOMId,0))*(CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) AS numUnitHourUOM
				,CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour
				,RI.monPrice
				,(CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CAST(coalesce(RI.monTotAmount,0)/RI.numUnitHour AS DECIMAL(18,4)) END)  As monUnitSalePrice
				,(CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END)*(CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CAST(coalesce(RI.monTotAmount,0)/RI.numUnitHour AS DECIMAL(18,4)) END) AS  Amount
				,(CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END)*(CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CAST(coalesce(RI.monTotAmount,0)/RI.numUnitHour AS DECIMAL(18,4)) END) AS monTotAmount
				,RH.monTotalTax AS Tax
				,coalesce(CAST(I.monListPrice AS VARCHAR(30)),cast(0 as TEXT)) AS monListPrice
				,I.numItemCode AS ItemCode,I.numItemCode
				,RI.numItemCode AS numoppitemtCode
				,L.vcData AS numItemClassification
				,CASE WHEN bitTaxable = false THEN 'No' ELSE 'Yes' END AS Taxable
				,numIncomeChartAcntId AS itemIncomeAccount
				,'NI' AS ItemType
				,0 AS numJournalId
				,0 AS numTransactionId
				,coalesce(I.vcModelID,'') AS vcModelID
				,USP_GetAttributes(RI.numWareHouseItemID,bitSerialized) AS vcAttributes
				,'' AS vcPartNo
				,(CASE WHEN coalesce(RH.monTotalDiscount,0) > 0 THEN (coalesce(RH.monTotalDiscount,0) -RI.monTotAmount) ELSE ((CASE WHEN v_numReferenceType = 5 OR v_numReferenceType = 6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END)*RI.monPrice) -RI.monTotAmount END) AS DiscAmt
				,'' AS vcNotes
				,'' AS vcTrackingNo
				,'' AS vcShippingMethod
				,0 AS monShipCost
				,FormatedDateFromDate(RH.dtCreatedDate,v_numDomainID) AS dtDeliveryDate
				,RI.numWareHouseItemID
				,coalesce(u.vcUnitName,'') AS numUOMId
				,coalesce(RI.vcManufacturer,'') AS vcManufacturer
				,coalesce(V.monCost,0) AS VendorCost
				,'' AS vcPathForTImage
				,I.fltLength
                ,I.fltWidth
                ,I.fltHeight
                ,I.fltWeight
                ,coalesce(I.numVendorID,0) AS numVendorID
                ,0 AS DropShip
                ,coalesce(RI.numUOMId,0) AS numUOM
                ,coalesce(u.vcUnitName,'') AS vcUOMName
                ,fn_UOMConversion(RI.numUOMId,RI.numItemCode,v_numDomainID,NULL::NUMERIC) AS UOMConversionFactor
                ,coalesce(W.vcWareHouse,'') AS Warehouse
                ,CASE WHEN charitemType = 'P' THEN WI.vcWHSKU ELSE I.vcSKU END AS vcSKU
				,CASE WHEN charitemType = 'P' THEN WI.vcBarCode ELSE I.numBarCodeId END AS vcBarcode
				,I.numShipClass
                ,FormatedDateTimeFromDate(RH.dtCreatedDate,v_numDomainID) AS dtRentalStartDate
                ,FormatedDateTimeFromDate(RH.dtCreatedDate,v_numDomainID) AS dtRentalReturnDate
                ,COALESCE((SELECT string_agg(vcSerialNo || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')' ELSE '' end,',' ORDER BY vcSerialNo)
				FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID = RH.numReturnHeaderID and oppI.numReturnItemID = RI.numReturnItemID
				),'') AS SerialLotNo
			FROM      ReturnHeader RH
			JOIN ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
			LEFT JOIN Item I ON RI.numItemCode = I.numItemCode
			LEFT JOIN Listdetails L ON I.numItemClassification = L.numListItemID
			LEFT JOIN Vendor V ON V.numVendorID = v_DivisionID
			AND V.numItemCode = RI.numItemCode
			LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
			LEFT JOIN WareHouseItems WI ON RI.numWareHouseItemID = WI.numWareHouseItemID
			AND WI.numDomainID = v_numDomainID
			LEFT JOIN Warehouses W ON WI.numDomainID = W.numDomainID
			AND WI.numWareHouseID = W.numWareHouseID
			WHERE RH.numReturnHeaderID = v_numReferenceID
		) X;

      IF v_DecimalPoint = 1 then
         UPDATE  tt_TEMP1
         SET     monPrice = CAST(monPrice AS DECIMAL(18,1)),Amount = CAST(Amount AS DECIMAL(18,1));
      end if;
      IF v_DecimalPoint = 2 then
         UPDATE  tt_TEMP1
         SET     monPrice = CAST(monPrice AS DECIMAL(18,2)),Amount = CAST(Amount AS DECIMAL(18,2));
      end if;
      IF v_DecimalPoint = 3 then
         UPDATE  tt_TEMP1
         SET     monPrice = CAST(monPrice AS DECIMAL(18,3)),Amount = CAST(Amount AS DECIMAL(18,3));
      end if;
      IF v_DecimalPoint = 4 then
         UPDATE  tt_TEMP1
         SET     monPrice = CAST(monPrice AS DECIMAL(18,4)),Amount = CAST(Amount AS DECIMAL(18,4));
      end if;

      v_numTaxItemID := 0;
      v_strSQLUpdate := '';
      select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID    LIMIT 1;
      EXECUTE 'alter table tt_TEMP1 add TotalTax DECIMAL(20,5)';
      EXECUTE 'alter table tt_TEMP1 add CRV DECIMAL(20,5)';
      IF v_tintReturnType = 1 then
	
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
         || ',TotalTax = fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
         || SUBSTR(CAST(v_numReferenceID AS VARCHAR(20)),1,20)
         || ',numReturnItemID,CAST(2 AS SMALLINT),Amount,numUnitHourUOM)';
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
         || ',CRV = fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
         || SUBSTR(CAST(v_numReferenceID AS VARCHAR(20)),1,20)
         || ',numReturnItemID,CAST(2 AS SMALLINT),Amount,numUnitHourUOM)';
      ELSEIF v_tintReturnType = 1
      then
		
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',TotalTax = 0';
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
      ELSEIF v_tintReturnType = 1
      then
			
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
         || ',TotalTax = fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
         || SUBSTR(CAST(v_numReferenceID AS VARCHAR(20)),1,20)
         || ',numReturnItemID,(2 AS SMALLINT),Amount,numUnitHourUOM)';
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
         || ', CRV = fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
         || SUBSTR(CAST(v_numReferenceID AS VARCHAR(20)),1,20)
         || ',numReturnItemID,CAST(2 AS SMALLINT),Amount,numUnitHourUOM)';
      ELSE
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',TotalTax = 0';
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
      end if;
      WHILE v_numTaxItemID > 0 LOOP
         SWV_ExecDyn := 'alter table tt_TEMP1 add "' || coalesce(v_vcTaxName,'') || '" DECIMAL(20,5)';
         EXECUTE SWV_ExecDyn;
         IF v_tintReturnType = 1 then
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',"' || coalesce(v_vcTaxName,'')
            || '" = fn_CalBizDocTaxAmt('
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numTaxItemID AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numReferenceID AS VARCHAR(20)),1,20) || ',numReturnItemID,CAST(2 AS SMALLINT),Amount,numUnitHourUOM)';
         ELSE
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',"' || coalesce(v_vcTaxName,'') || '" = 0';
         end if;
         select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID
         AND numTaxItemID > v_numTaxItemID    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numTaxItemID := 0;
         end if;
      END LOOP;
      RAISE NOTICE '%','QUERY  :' || coalesce(v_strSQLUpdate,'');
      IF v_strSQLUpdate <> '' then
        
         v_strSQLUpdate := 'update tt_TEMP1 set ItemCode = ItemCode'
         || coalesce(v_strSQLUpdate,'') || ' where ItemCode > 0';
         RAISE NOTICE '%',v_strSQLUpdate;
         EXECUTE v_strSQLUpdate;
      end if;
      select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;
      WHILE v_intRowNum > 0 LOOP
         RAISE NOTICE '%',v_vcDbColumnName;
         SWV_ExecDyn := 'alter table tt_TEMP1 add "' || coalesce(v_vcDbColumnName,'') || '" VARCHAR(100)';
         EXECUTE SWV_ExecDyn;
         v_strSQLUpdate := 'update tt_TEMP1 set ItemCode = ItemCode,"'
         || coalesce(v_vcDbColumnName,'') || '" = GetCustFldValueItem(' || coalesce(v_numFldID,'')
         || ',ItemCode) where ItemCode > 0';
         EXECUTE v_strSQLUpdate;
         select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
         AND Grp_id = 5
         AND numDomainID = v_numDomainID
         AND numAuthGroupID = v_numBizDocId
         AND bitCustom = true
         AND tintRow >= v_intRowNum
         AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_intRowNum := 0;
         end if;
      END LOOP;
      open SWV_RefCur for
      SELECT  Row_NUmber() OVER(ORDER BY numoppitemtCode) AS RowNumber,
            * FROM    tt_TEMP1
      ORDER BY numoppitemtCode ASC;
    
      IF(SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormId = v_tintType AND numDomainID = v_numDomainID
      AND numAuthGroupId = v_numBizDocId AND vcFieldType = 'R' AND coalesce(numRelCntType,0) = v_numBizDocTempID) = 0 then

         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
         select numFormId,numFieldID,1,tintRow,v_numDomainID,v_numBizDocId,v_numBizDocTempID
         FROM View_DynamicDefaultColumns
         where numFormId = v_tintType and bitDefault = true and numDomainID = v_numDomainID order by tintRow ASC;
         IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID = v_numBizDocId and Ds.numFormID = v_tintType
         and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID) = 0 then
	
            INSERT INTO DycFrmConfigBizDocsSumm(numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
            SELECT v_tintType,v_numBizDocId,DFM.numFieldID,v_numDomainID,v_numBizDocTempID FROM
            DycFormField_Mapping DFFM
            JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
            where numFormID = 16 and DFFM.numDomainID is null;
         end if;
      end if;
      open SWV_RefCur2 for
      SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
      FROM    View_DynamicColumns
      WHERE   numFormId = v_tintType
      AND numDomainID = v_numDomainID
      AND numAuthGroupId = v_numBizDocId
      AND vcFieldType = 'R'
      AND coalesce(numRelCntType,0) = v_numBizDocTempID
      UNION
      SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' AS vcFieldDataType,
            tintRow AS intRowNum
      FROM    View_DynamicCustomColumns
      WHERE   numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND coalesce(numRelCntType,0) = v_numBizDocTempID
      ORDER BY 4;
   end if;
   RETURN;
END; $$;


