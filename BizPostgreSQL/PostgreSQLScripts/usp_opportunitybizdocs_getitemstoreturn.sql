-- Stored procedure definition script USP_OpportunityBizDocs_GetItemsToReturn for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetItemsToReturn(v_numDomainID NUMERIC(18,0),                                                                                                                                     
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   OBDI.numOppBizDocItemID,
		OI.numoppitemtCode,
        OI.numOppId,
		OI.vcNotes as txtNotes,
        OI.numItemCode,
		OI.bitItemPriceApprovalRequired,
		OBDI.numUnitHour AS numOrigUnitHour,
        (OBDI.numUnitHour -coalesce((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID = OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND
      numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
         WHERE numOppId = v_numOppID) AND numOppItemID = OI.numoppitemtCode),0))
   AS numUnitHour,
        OI.monPrice,
        OI.monTotAmount,
        OI.numSourceID,
        OI.vcItemDesc,
		OI.numCost,
        OI.numWarehouseItmsID,
        OI.vcType AS ItemType,
        OI.vcAttributes AS Attributes,
        OI.bitDropShip AS DropShip,
        OI.numUnitHourReceived,
        OI.bitDiscountType,
        OI.fltDiscount,
		OI.bitMarkupDiscount,
        OI.monTotAmtBefDiscount,
        OI.numQtyShipped,
        OI.vcItemName,
        OI.vcModelID,
        OI.vcPathForTImage,
        OI.vcManufacturer,
        OI.monVendorCost,
        coalesce(OI.numUOMId,0) AS numUOM,
        coalesce(U.vcUnitName,'') AS vcUOMName,
        coalesce(fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,NULL::NUMERIC),1) AS UOMConversionFactor,
        OI.bitWorkOrder,
        coalesce(OI.numVendorWareHouse,0) as numVendorWareHouse,
        OI.numShipmentMethod,
        OI.numSOVendorId,
        OI.numProjectID,
        OI.numClassID,
        OI.numProjectStageID,
        OI.numToWarehouseItemID,
        UPPER(I.charItemType) AS charItemType,
        WItems.numWareHouseID,
        W.vcWareHouse AS Warehouse,
        0 as Op_Flag,
		CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,OM.numDomainId,coalesce(OI.numUOMId,0))*OBDI.numUnitHour) AS NUMERIC(18,2)) AS numUnitHour,
		coalesce(OI.numClassID,0) as numClassID,coalesce(OI.numProjectID,0) as numProjectID,
		CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
      AND (OB.bitAuthoritativeBizDocs = 1 OR OB.numBizDocId = 304) AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numoppid = OM.numOppId) > 0 THEN true ELSE false END  AS bitIsAuthBizDoc,
		coalesce(OI.numUnitHourReceived,0) AS numUnitHourReceived,coalesce(numQtyShipped,0) AS numQtyShipped,coalesce(fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,coalesce(I.fltWeight,0) AS fltWeight,coalesce(I.bitFreeShipping,false) AS bitFreeShipping,
		coalesce(I.fltLength,0) AS fltLength,
		coalesce(I.fltWidth,0) AS fltWidth,
		coalesce(I.fltHeight,0) AS fltHeight,
		coalesce(WItems.vcWHSKU,coalesce(I.vcSKU,'')) AS vcSKU,
		(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) AS monAverageCost,
		coalesce(I.bitFreeShipping,false) AS bitFreeShipping,
		coalesce(I.txtItemDesc,'') AS txtItemDesc,
		coalesce(I.vcModelID,'') AS vcModelID,
		coalesce(I.vcManufacturer,'') AS vcManufacturer,
		coalesce(I.numBarCodeId,'') AS numBarCodeId,
		coalesce(I.bitSerialized,false) AS bitSerialized,
		coalesce(I.bitKitParent,false) AS bitKitParent,
		coalesce(I.bitAssembly,false) AS bitAssembly,
		coalesce(I.IsArchieve,false) AS IsArchieve,
		coalesce(I.bitLotNo,false) AS bitLotNo,
		coalesce(WItems.monWListPrice,0) AS monWListPrice,
		coalesce(I.bitTaxable,false) AS bitTaxable,
		CASE WHEN I.charItemType = 'P' THEN(SELECT CAST(coalesce(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
		CASE WHEN I.charItemType = 'P' THEN(SELECT CAST(coalesce(SUM(numonOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
		CASE WHEN I.charItemType = 'P' THEN(SELECT CAST(coalesce(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
		CASE WHEN I.charItemType = 'P' THEN(SELECT CAST(coalesce(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
		case when coalesce(I.bitAssembly,false) = true then fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
		coalesce((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numOppItemID = OI.numoppitemtCode),0) AS numSerialNoAssigned,
		(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID WHERE OB.numoppid = OI.numOppId AND OBI.numOppItemID = OI.numoppitemtCode AND OB.numBizDocId = 296) > 0 THEN true ELSE false END) AS bitAddedFulFillmentBizDoc,
		coalesce(OI.vcAttrValues,'') AS AttributeIDs
		,(CASE WHEN OM.tintopptype = 2 THEN coalesce(OI.vcNotes,'') ELSE '' END) AS vcVendorNotes
   FROM
   OpportunityBizDocItems OBDI
   INNER JOIN
   OpportunityItems OI
   ON
   OBDI.numOppItemID = OI.numoppitemtCode
   INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
   INNER JOIN Item I ON I.numItemCode = OI.numItemCode
   LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
   LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
   WHERE
   OI.numOppId = v_numOppID
   AND OBDI.numOppBizDocID = v_numOppBizDocID
   AND OM.numDomainId = v_numDomainID
   AND(OBDI.numUnitHour -coalesce((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID = OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId = v_numOppID) AND numOppItemID = OI.numoppitemtCode),0)) > 0;
                    
   open SWV_RefCur2 for
   SELECT  vcSerialNo,
			vcComments AS Comments,
			numOppItemID AS numoppitemtCode,
			W.numWareHouseItmsDTLID,
			numWarehouseItmsID AS numWItmsID,
			fn_GetAttributes(W.numWareHouseItmsDTLID,1::BOOLEAN) AS Attributes
   FROM    WareHouseItmsDTL W
   JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
   WHERE
   numOppID = v_numOppID
   AND numOppItemID IN(SELECT
      OI.numoppitemtCode
      FROM
      OpportunityBizDocItems OBDI
      INNER JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      WHERE
      OI.numOppId = v_numOppID
      AND OBDI.numOppBizDocID = v_numOppBizDocID
      AND(OBDI.numUnitHour -coalesce((SELECT
         SUM(numUnitHour)
         FROM
         ReturnItems
         WHERE
         numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId = v_numOppID)
         AND numOppItemID = OI.numoppitemtCode),
      0)) > 0);
   RETURN;
END; $$;

