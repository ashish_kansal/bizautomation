DROP FUNCTION IF EXISTS usp_vendorleadtimes_getbyvendor;

CREATE OR REPLACE FUNCTION usp_vendorleadtimes_getbyvendor
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numdivisionid NUMERIC(18,0)
	,INOUT p_swvrefcur REFCURSOR
)
LANGUAGE plpgsql
   AS $$
BEGIN
	OPEN p_swvrefcur FOR
	SELECT 
		JSON_AGG(vcdata) vcdata
	FROM
	(
		SELECT 
			JSON_BUILD_OBJECT
			(
				'VendorLeadTimesID',numvendorleadtimesid
				,'FromQuantity', (VLT.vcdata->'FromQuantity')::INT
				,'ToQuantity', (VLT.vcdata->'ToQuantity')::INT
				,'ItemGroups', array_agg(DISTINCT LDItemGroup.vcData)
				,'ShipVia', LDShipVia.vcData
				,'ShippingServices', array_agg(DISTINCT ShippingService.vcShipmentService)
				,'ShipFromLocations', array_agg(DISTINCT AddressDetails.vcAddressName)
				,'ShipToCountry', LDCountry.vcData
				,'ShipToStates', (SELECT array_agg(state) from unnest(array_agg(DISTINCT State.vcState)) state where state is not null)
				,'DaysToArrive', (VLT.vcdata->'DaysToArrive')::INT
			) AS vcdata
		FROM   
			VendorLeadTimes VLT
		INNER JOIN DivisionMaster DM ON VLT.numdivisionid = DM.numdivisionid
		INNER JOIN jsonb_array_elements(VLT.vcdata->'ItemGroups') AS TEMPItemGroups ON TRUE
		INNER JOIN ListDetails LDItemGroup ON LDItemGroup.numListID = 36 AND TEMPItemGroups.value::numeric = LDItemGroup.numListItemID
		INNER JOIN ListDetails LDShipVia ON LDShipVia.numListID = 82 AND (VLT.vcdata->'ShipVia')::INT::numeric = LDShipVia.numListItemID
		INNER JOIN jsonb_array_elements(VLT.vcdata->'ShippingServices') AS TEMPShippingServices ON TRUE
		INNER JOIN ShippingService ON ShippingService.numShipViaID = (VLT.vcdata->'ShipVia')::INT::numeric AND TEMPShippingServices.value::numeric = ShippingService.numShippingServiceID
		INNER JOIN jsonb_array_elements(VLT.vcdata->'ShipFromLocations') AS TEMPShipFromLocations ON TRUE
		INNER JOIN AddressDetails ON TEMPShipFromLocations.value::numeric = AddressDetails.numAddressID
		INNER JOIN ListDetails LDCountry ON LDCountry.numListID = 40 AND (VLT.vcdata->'ShipToCountry')::INT::numeric = LDCountry.numListItemID
		LEFT JOIN jsonb_array_elements(VLT.vcdata->'ShipToStates') AS TEMPShipToStates ON TRUE
		LEFT JOIN State ON TEMPShipToStates.value::numeric = State.numStateID
		WHERE
			VLT.numdivisionid = p_numdivisionid
			AND DM.numdomainid = p_numdomainid
		GROUP BY
			VLT.numvendorleadtimesid
			,VLT.vcdata
			,LDShipVia.vcData
			,LDCountry.vcData
	) TEMP;
END; $$;