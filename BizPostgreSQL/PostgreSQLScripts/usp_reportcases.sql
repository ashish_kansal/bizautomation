-- Stored procedure definition script USP_ReportCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportCases(v_numDomainID NUMERIC,                                        
        v_dtFromDate TIMESTAMP,                                        
        v_dtToDate TIMESTAMP,                                        
        v_numUserCntID NUMERIC DEFAULT 0,                                             
        v_tintType SMALLINT DEFAULT NULL,                                        
        v_tintRights INTEGER DEFAULT 0,                                  
        v_status NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQl  VARCHAR(2000);
BEGIN
   If v_tintRights = 1 then

      If v_status = 0 then

         v_strSQl := 'select A.numContactId as vcAssignTo ,vcFirstname || '' '' || vcLastname  || ''('' ||                                  
 CAST((select count(*) from Cases C where C.numAssignedTo = A.numContactId and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''') AS VARCHAR(15)) || '')'' as Nam                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactId =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and A.numContactId in(select numAssignedTo from Cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      If v_status <> 0 then

         v_strSQl := '
select A.numContactId as vcAssignTo ,vcFirstname || '' '' || vcLastname  || ''('' ||                                  
 CAST((select count(*) from Cases C where C.numAssignedTo = A.numContactId and C.numStatus =' || SUBSTR(CAST(v_status AS VARCHAR(15)),1,15) || ' and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''') AS VARCHAR(15)) || '')'' as Nam                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactId =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and A.numContactId in(select numAssignedTo from Cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      RAISE NOTICE '%',v_strSQl;
      OPEN SWV_RefCur FOR EXECUTE v_strSQl;
   end if;                                  
   If v_tintRights = 2 then
                        
                    
--on A.numContactID= C1.numAssignedTo              
      If  v_status = 0 then

         v_strSQl := 'select A.numContactID as vcAssignTo ,vcFirstname||'' ''||vcLastname  ||''(''||                                  
 (select count(*) from cases C where C.numAssignedTo=A.numContactID and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''  ) as "Nam"                             
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')' || ' And A.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                   
 where F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ')';
      end if;
      If v_status <> 0 then

         v_strSQl := 'select A.numContactID as vcAssignTo ,vcFirstname||'' ''||vcLastname  ||''(''||                                  
 (select count(*) from cases C where C.numAssignedTo=A.numContactID  and C.numStatus =' || SUBSTR(CAST(v_status AS VARCHAR(15)),1,15) || ' and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''  ) as "Nam"                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')' || ' And A.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                   
 where F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and A.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ')';
      end if;
      RAISE NOTICE '%',v_strSQl;
      OPEN SWV_RefCur FOR EXECUTE v_strSQl;
   end if;                                  
                                  
   If v_tintRights = 3 then

      If v_status = 0 then

         v_strSQl := 'select A.numContactID as vcAssignTo ,vcFirstname||'' ''||vcLastname  ||''(''||                                  
 (select count(*) from cases C
 join DivisionMaster DM on DM.numDivisionID = C.numDivisionID 
 where   DM.numTerId   in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                                     
 where F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ') and C.numAssignedTo=A.numContactID and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''  ) as "Nam"                              
 from  AdditionalContactsInformation A                                                               
 where  A.numContactID in (select numAssignedTo from cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      If v_status <> 0 then

         v_strSQl := 'select A.numContactID as vcAssignTo ,vcFirstname||'' ''||vcLastname  ||''(''||                                  
 (select count(*) from cases C
 join DivisionMaster DM on DM.numDivisionID = C.numDivisionID 
 where C.numStatus =' || SUBSTR(CAST(v_status AS VARCHAR(15)),1,15) || ' and DM.numTerId   in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                                     
 where F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ') and C.numAssignedTo=A.numContactID and C.bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''  ) as "Nam"                            
 from  AdditionalContactsInformation A                                                               
 where  A.numContactID in (select numAssignedTo from cases where bintCreatedDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''')';
      end if;
      RAISE NOTICE '%',v_strSQl;
      OPEN SWV_RefCur FOR EXECUTE v_strSQl;
   end if;
   RETURN;
END; $$;


