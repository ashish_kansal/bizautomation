-- Stored procedure definition script Get_Max_Template_ID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Get_Max_Template_ID(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_maxValue  INTEGER;
BEGIN
   select   Max(RowID) INTO v_maxValue From tblActionItemData; 
   open SWV_RefCur for
      SELECT v_maxValue::bigint+1;
   RETURN;
END; $$;


