-- Function definition script fn_GetModuleName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE OR REPLACE FUNCTION fn_GetModuleName(v_numModuleId BIGINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
	-- Add the parameters for the function here
	
	-- Declare the return variable here
   AS $$
   DECLARE
   v_ModuleName  VARCHAR(100);

	-- Add the T-SQL statements to compute the return value here
BEGIN
   v_ModuleName := '';
   select   vcModuleName INTO v_ModuleName from ModuleMaster where numModuleID = v_numModuleId;

	-- Return the result of the function
   RETURN v_ModuleName;
END; $$;

