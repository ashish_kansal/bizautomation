-- Stored procedure definition script USP_GetOrderRuleDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOrderRuleDetails(v_numDomainID NUMERIC,
v_numRuleID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPITEMTYPE CASCADE;
   create TEMPORARY TABLE tt_TEMPITEMTYPE
   (
      numTypeID NUMERIC,
      vcType VARCHAR(50)
   );
	
   insert into tt_TEMPITEMTYPE
   select 21,'-Select One--'
   union
   select 1,'Services'
   union
   select 2,'Inventory Items'
   union
   select 3,'Non-Inventory Items'
   union
   select 4,'Serialized Items'
   union
   select 5,'Kits'
   union
   select 6,'Assemblies'
   union
   select 7,'Matrix Items';

	
open SWV_RefCur for select TIT.vcType as "Item Type", ITC.vcData as "Item Classification",(case numBillToID when 0 then 'Employer' when 1 then 'Customer' end) as "Bill To",
	(case numShipToID when 0 then 'Employer' when 1 then 'Customer' end) as "Ship To",
	(case btFullPaid when false then 'No' when true then 'Yes' end) as "Amount Paid Full",
	(case numBizDocStatus when 0 then 'No' else ITB.vcData end) AS "Biz Doc Status",
	OAR.numItemTypeID,
	OAR.numItemClassID,
	numBillToID,
	numShipToID,
	btFullPaid,
	numBizDocStatus
   from  OrderAutoRuleDetails OAR INNER JOIN tt_TEMPITEMTYPE TIT ON TIT.numTypeID = OAR.numItemTypeID
   INNER JOIN(select LD.vcData,LD.numListItemID from Listdetails LD,listmaster LM  where LM.numListID = 36
      and LD.numListID = LM.numListID AND LD.numDomainid = v_numDomainID) ITC ON
   ITC.numListItemID = OAR.numItemClassID
   LEFT OUTER JOIN(select LD.vcData,LD.numListItemID from Listdetails LD,listmaster LM  where LM.numListID = 11
      and LD.numListID = LM.numListID AND LD.numDomainid = v_numDomainID)	ITB
   ON ITB.numListItemID = numBizDocStatus
   where numRuleID = v_numRuleID;

/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
   RETURN;
END; $$;












