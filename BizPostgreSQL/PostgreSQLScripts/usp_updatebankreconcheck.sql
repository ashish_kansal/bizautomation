-- Stored procedure definition script USP_UpdateBankReconCheck for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateBankReconCheck(v_numDomainId NUMERIC(18,0),
    v_numCheckNo VARCHAR(50),
    v_bitReconcile BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  General_Journal_Details
   SET     bitReconcile = v_bitReconcile
   WHERE   numDomainId = v_numDomainId
   AND numJournalId IN(SELECT  GH.numJOurnal_Id
   FROM    General_Journal_Header GH
   INNER JOIN CheckDetails CD ON GH.numCheckId = CD.numCheckId
   WHERE   CAST(CD.numCheckNo AS VARCHAR(50)) = v_numCheckNo
   AND GH.numDomainId = v_numDomainId
   AND CD.numDomainId = v_numDomainId);
   RETURN;
END; $$;



