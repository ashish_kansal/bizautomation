-- Stored procedure definition script usp_ManageBankStatementHeader for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ManageBankStatementHeader(v_numStatementID NUMERIC(18,0),
	v_numBankDetailID NUMERIC(18,0),
	v_vcFIStatementID VARCHAR(50),
	v_monAvailableBalance DECIMAL(20,5),
	v_dtAvailableBalanceDate TIMESTAMP,
	v_numCurrencyID NUMERIC(18,0),
	v_monLedgerBalance DECIMAL(20,5),
	v_dtLedgerBalanceDate TIMESTAMP,
	v_dtStartDate TIMESTAMP,
	v_dtEndDate TIMESTAMP,
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT numStatementID FROM BankStatementHeader WHERE numStatementID = v_numStatementID) then

      UPDATE BankStatementHeader SET
      numBankDetailID = v_numBankDetailID,vcFIStatementID = v_vcFIStatementID,
      monAvailableBalance = v_monAvailableBalance,dtAvailableBalanceDate = v_dtAvailableBalanceDate,
      numCurrencyID = v_numCurrencyID,monLedgerBalance = v_monLedgerBalance,
      dtLedgerBalanceDate = v_dtLedgerBalanceDate,
      dtStartDate = v_dtStartDate,dtEndDate = v_dtEndDate,numDomainID = v_numDomainID,
      dtModifiedDate = LOCALTIMESTAMP
      WHERE
      numStatementID = v_numStatementID;
   ELSE
      INSERT INTO BankStatementHeader(numBankDetailID,
		vcFIStatementID,
		monAvailableBalance,
		dtAvailableBalanceDate,
		numCurrencyID,
		monLedgerBalance,
		dtLedgerBalanceDate,
		dtStartDate,
		dtEndDate,
		numDomainID,
		dtCreatedDate) VALUES(v_numBankDetailID,
		v_vcFIStatementID,
		v_monAvailableBalance,
		v_dtAvailableBalanceDate,
		v_numCurrencyID,
		v_monLedgerBalance,
		v_dtLedgerBalanceDate,
		v_dtStartDate,
		v_dtEndDate,
		v_numDomainID,
		LOCALTIMESTAMP);
   
      v_numStatementID := CURRVAL('BankStatementHeader_seq');
   end if;

   open SWV_RefCur for SELECT v_numStatementID;
END; $$;












