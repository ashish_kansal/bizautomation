-- Stored procedure definition script USP_UpdateStartDateProcesstoOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateStartDateProcesstoOpportunity(v_numStageDetailsId NUMERIC(18,0) DEFAULT 0,   
v_numOppId NUMERIC(18,0) DEFAULT 0,   
v_numProjectId NUMERIC(18,0) DEFAULT 0,
v_dtmStartDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   StagePercentageDetails
   SET
   dtStartDate = v_dtmStartDate
   WHERE
   numOppid = v_numOppId AND numStageDetailsId = v_numStageDetailsId;
   RETURN;
END; $$; 


