-- Stored procedure definition script USP_AssignCaseRecOwner for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AssignCaseRecOwner(v_numRecOwner NUMERIC(9,0) DEFAULT 0,
v_numAssignedTo NUMERIC(9,0) DEFAULT 0,  
v_numCaseId NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Cases set numRecOwner = v_numRecOwner,numAssignedBy = v_numRecOwner,numAssignedTo = v_numAssignedTo where numCaseId = v_numCaseId  and numDomainID = v_numDomainID;
   RETURN;
END; $$;


