-- Stored procedure definition script USP_FollowUpHstr for PostgreSQL
CREATE OR REPLACE FUNCTION USP_FollowUpHstr(v_numDivisionID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numFollowUpStatusID,numFollowUpstatus,vcdata as "vcdata",numDivisionID,bintAddedDate from FollowUpHistory
   join Listdetails on numlistitemid = numFollowUpstatus
   where numDivisionID = v_numDivisionID  and FollowUpHistory.numDomainID = v_numDomainID;
END; $$;












