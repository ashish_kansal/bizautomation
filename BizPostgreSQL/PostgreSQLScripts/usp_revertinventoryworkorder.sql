DROP FUNCTION IF EXISTS USP_RevertInventoryWorkOrder;

CREATE OR REPLACE FUNCTION USP_RevertInventoryWorkOrder(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numWOID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
	--REVERT INVENTORY OF WORK ORDER ITEMS
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;
   v_Description  VARCHAR(2000);
   v_onHand  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onBackOrder  DOUBLE PRECISION;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPRevertInventoryWorkOrder_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPRevertInventoryWorkOrder CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRevertInventoryWorkOrder
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseItemID NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION
   );
   INSERT INTO tt_TEMPRevertInventoryWorkOrder(numWarehouseItemID, numQtyItemsReq)  SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId = v_numWOID AND coalesce(numWareHouseItemId,0) > 0;


   select   COUNT(*) INTO v_COUNT FROM tt_TEMPRevertInventoryWorkOrder;

   WHILE v_i <= v_COUNT LOOP
      select   coalesce(numWarehouseItemID,0), coalesce(numQtyItemsReq,0) INTO v_numWareHouseItemID,v_numQtyItemsReq FROM tt_TEMPRevertInventoryWorkOrder WHERE ID = v_i;
      v_Description := CONCAT('Items Allocation Reverted For Work Order',' (Qty:',v_numQtyItemsReq,')');

		--GET CURRENT INEVENTORY STATUS
      select   coalesce(numOnHand,0), coalesce(numonOrder,0), coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_onHand,v_onOrder,v_onAllocation,v_onBackOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;

		--CHANGE INVENTORY
      IF v_numQtyItemsReq >= v_onBackOrder then
		
         v_numQtyItemsReq := v_numQtyItemsReq -v_onBackOrder;
         v_onBackOrder := 0;
         IF (v_onAllocation -v_numQtyItemsReq >= 0) then
            v_onAllocation := v_onAllocation -v_numQtyItemsReq;
         end if;
         v_onHand := v_onHand+v_numQtyItemsReq;
      ELSEIF v_numQtyItemsReq < v_onBackOrder
      then
		
         v_onBackOrder := v_onBackOrder -v_numQtyItemsReq;
      end if;

		--UPDATE INVENTORY AND WAREHOUSE TRACKING
      PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_onHand,v_numOnAllocation := v_onAllocation,v_numOnBackOrder := v_onBackOrder,
      v_numOnOrder := v_onOrder,v_numWareHouseItemID := v_numWareHouseItemID,
      v_numReferenceID := v_numWOID,v_tintRefType := 2::SMALLINT,
      v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
      v_Description := v_Description);
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


