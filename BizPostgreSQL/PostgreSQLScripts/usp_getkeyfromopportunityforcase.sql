-- Stored procedure definition script usp_GetKeyFromOpportunityForCase for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetKeyFromOpportunityForCase(     
--  
v_numDomainID NUMERIC(9,0) DEFAULT 0,  
 v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDivisionID > 0 then

      open SWV_RefCur for
      SELECT numOppId
      FROM OpportunityMaster
      WHERE numContactId IN(SELECT numContactId
         FROM AdditionalContactsInformation
         WHERE numDomainID = v_numDomainID)
      AND numDivisionId = v_numDivisionID
      ORDER BY OpportunityMaster.numOppId;
   ELSE
      open SWV_RefCur for
      SELECT numOppId
      FROM OpportunityMaster
      WHERE numContactId IN(SELECT numContactId
         FROM AdditionalContactsInformation
         WHERE numDomainID = 1)
      ORDER BY OpportunityMaster.numOppId;
   end if;
   RETURN;
END; $$;  
  
     
   /*(select numlistitemid from stagelistitemdetails   
where numstagedetailsid in(select numstagedetailsid from stagepercentagedetails where numstagepercentageid=12)  
AND numOppID IS NOT NULL)*/  
--Modified in tintPStage 19-apr-03


