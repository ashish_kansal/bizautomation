-- Stored procedure definition script USP_SaveMarketBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveMarketBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                           
v_numMarketId NUMERIC(9,0) DEFAULT 0,                          
v_strRow TEXT DEFAULT '',                  
v_intYear INTEGER DEFAULT 0,              
v_sintByte SMALLINT DEFAULT 0,            
v_intType INTEGER DEFAULT 0,      
v_numListItemID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Date  TIMESTAMP;                      
   v_dtFiscalStDate  TIMESTAMP;                                
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN             
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);                 
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);                             
   v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';                       
   RAISE NOTICE '%',v_dtFiscalStDate;             
   RAISE NOTICE '%',v_dtFiscalEndDate;                   
   if v_sintByte = 0 then
              
    --To Delete the Records                    
      DELETE FROM MarketBudgetDetails MBD using MarketBudgetMaster MBM where MBM.numMarketBudgetId = MBD.numMarketId AND MBD.numMarketId = v_numMarketId And MBM.numDomainID = v_numDomainId And (MBD.tintMonth between EXTRACT(Month FROM v_dtFiscalStDate) and 12 and MBD.intYear = EXTRACT(Year FROM v_dtFiscalStDate)) Or (MBD.tintMonth between 1 and EXTRACT(month FROM v_dtFiscalEndDate)  and MBD.intYear = EXTRACT(Year FROM v_dtFiscalEndDate));
		Insert Into MarketBudgetDetails
		(
			numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments
		)
		Select 
			numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments 
		from
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numMarketId NUMERIC(9,0) PATH 'numMarketId',
				numListItemID NUMERIC(9,0) PATH 'numListItemID',
				tintMonth SMALLINT PATH 'tintMonth',
				intYear INTEGER PATH 'intYear',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				vcComments VARCHAR(100) PATH 'vcComments'
		) AS X;
	Else            
          
		--To Delete Record            
		DELETE FROM MarketBudgetDetails MBD using MarketBudgetMaster MBM where MBM.numMarketBudgetId = MBD.numMarketId AND MBD.numMarketId = v_numMarketId And MBD.numListItemID = v_numListItemID               
		And ((MBD.tintMonth between EXTRACT(month FROM TIMEZONE('UTC',now())) and 12 and MBD.intYear = EXTRACT(year FROM v_Date)) Or (MBD.tintMonth between 1 and 12 and MBD.intYear = EXTRACT(Year FROM v_Date)+1));
      
		Insert Into MarketBudgetDetails
		(
			numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments
		)
		Select 
			numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments 
		from
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numMarketId NUMERIC(9,0) PATH 'numMarketId',
				numListItemID NUMERIC(9,0) PATH 'numListItemID',
				tintMonth SMALLINT PATH 'tintMonth',
				intYear INTEGER PATH 'intYear',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				vcComments VARCHAR(100) PATH 'vcComments'
		) AS X;
	end if;
	RETURN;
END; $$;


