-- Stored procedure definition script USP_GetPerformanceEvaluationReportData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPerformanceEvaluationReportData(v_numDomainID NUMERIC(18,0)                            
	,v_tintProcessType SMALLINT
	,v_numProcessID NUMERIC(18,0)
	,v_vcMilestone VARCHAR(300)
	,v_numStageDetailsId NUMERIC(18,0)
	,v_vcTaskIDs VARCHAR(200)
	,v_vcTeams VARCHAR(200)
	,v_vcEmployees VARCHAR(200)
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor , INOUT SWV_RefCur6 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numStageDetailsId NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      vcTaskName VARCHAR(300),
      numTaskTimeInMinutes NUMERIC(18,0)
   );
   INSERT INTO
   tt_TEMP
   SELECT
   SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0)
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   SPD.numStageDetailsId = SPDT.numStageDetailsId
   WHERE
   SPD.numdomainid = v_numDomainID
   AND SPD.slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR SPD.vcMileStoneName = v_vcMilestone)
   AND (coalesce(v_numStageDetailsId,0) = 0 OR SPD.numStageDetailsId = v_numStageDetailsId)
   AND (coalesce(v_vcTaskIDs,'') = '' OR SPDT.numTaskId IN(SELECT ID FROM SplitIDs(v_vcTaskIDs,',')));

   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      numRecordID NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      numReferenceTaskId NUMERIC(18,0),
      fltBuildQty DOUBLE PRECISION,
      numContactId NUMERIC(18,0),
      numTeam NUMERIC(18,0),
      numEstimatedTaskMinutes NUMERIC(18,0),
      numActualTaskMinutes NUMERIC(18,0),
      monEstimatedCost DECIMAL(20,5),
      monActualCost DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPTASKS(numRecordID
		,numTaskID
		,numReferenceTaskId
		,fltBuildQty
		,numContactId
		,numTeam
		,numEstimatedTaskMinutes
		,numActualTaskMinutes
		,monEstimatedCost
		,monActualCost)
   SELECT(CASE WHEN v_tintProcessType = 3 THEN WO.numWOId ELSE PM.numProId END)
		,SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,(CASE WHEN v_tintProcessType = 3 THEN coalesce(WO.numQtyItemsReq,0) ELSE 1 END) AS numBuildQty
		,ACI.numContactId
		,ACI.numTeam
		,(((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(CASE WHEN v_tintProcessType = 3 THEN coalesce(WO.numQtyItemsReq,0) ELSE 1 END)) AS numEstimatedTaskMinutes
		,coalesce(TEMPTime.numTotalMinutes,0) AS numActualTaskMinutes
		,(((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(CASE WHEN v_tintProcessType = 3 THEN coalesce(WO.numQtyItemsReq,0) ELSE 1 END))*(coalesce(SPDT.monHourlyRate,coalesce(UM.monHourlyRate,0))/60) AS monEstimatedCost
		,coalesce(TEMPTime.numTotalMinutes,0)*(coalesce(SPDT.monHourlyRate,coalesce(UM.monHourlyRate,0))/60) AS monActualCost
   FROM
   tt_TEMP T
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   T.numTaskID = SPDT.numReferenceTaskId
   INNER JOIN
   UserMaster UM
   ON
   SPDT.numAssignTo = UM.numUserDetailId
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   UM.numUserDetailId = ACI.numContactId
   LEFT JOIN
   WorkOrder WO
   ON
   SPDT.numWorkOrderId = WO.numWOId
   LEFT JOIN
   ProjectsMaster PM
   ON
   SPDT.numProjectId = PM.numProId
   CROSS JOIN LATERAL(SELECT
      SUM(T.numTotalMinutes) AS numTotalMinutes
      FROM(SELECT(EXTRACT(DAY FROM MAX(dtActionTime) -MIN(dtActionTime))*60*24+EXTRACT(HOUR FROM MAX(dtActionTime) -MIN(dtActionTime))*60+EXTRACT(MINUTE FROM MAX(dtActionTime) -MIN(dtActionTime))) AS numTotalMinutes
         FROM
         StagePercentageDetailsTaskTimeLog SPDTTL
         WHERE
         SPDTTL.numTaskId = SPDT.numTaskId
         AND dtActionTime BETWEEN v_dtFromDate AND v_dtToDate
         GROUP BY
         CAST(dtActionTime AS DATE)) T) TEMPTime
   WHERE
		(coalesce(v_vcTeams,'') = '' OR ACI.numTeam IN(SELECT ID FROM SplitIDs(v_vcTeams,',')))
   AND (coalesce(v_vcEmployees,'') = '' OR ACI.numContactId IN(SELECT ID FROM SplitIDs(v_vcEmployees,',')))
   AND(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4) > 0;

   DROP TABLE IF EXISTS tt_TEMPRESULT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRESULT
   (
      numTotalRecords NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      numBuildUnits DOUBLE PRECISION,
      numAssignedTo NUMERIC(18,0),
      numTeam NUMERIC(18,0),
      vcEmployee VARCHAR(200),
      vcTeam VARCHAR(200),
      numOnTimeOrEarly NUMERIC(18,0),
      monBudgetImpact DECIMAL(20,5),
      numAvailability NUMERIC(18,0),
      numProductivity NUMERIC(18,0),
      fltUnits DOUBLE PRECISION,
      monLabourPerUnit DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPRESULT(numTotalRecords
		,numTaskID
		,numBuildUnits
		,numAssignedTo
		,numTeam
		,vcEmployee
		,vcTeam
		,numOnTimeOrEarly
		,monBudgetImpact
		,fltUnits
		,monLabourPerUnit)
   SELECT
   COUNT(DISTINCT TT.numRecordID)
		,TT.numReferenceTaskId
		,coalesce((SELECT SUM(fltBuildQty) FROM tt_TEMPTASKS TTInner WHERE TTInner.numReferenceTaskId = TT.numReferenceTaskId),0)
		,TT.numContactId
		,TT.numTeam
		,CAST(fn_GetContactName(TT.numContactId) AS VARCHAR(200))
		,CAST(fn_GetListItemName(TT.numTeam) AS VARCHAR(200))
		,((SELECT
      COUNT(*)
      FROM
      tt_TEMPTASKS TTInner
      WHERE
      TTInner.numReferenceTaskId = TT.numReferenceTaskId
      AND TTInner.numContactId = TT.numContactId
      AND TTInner.numTeam = TT.numTeam
      AND numActualTaskMinutes <= numEstimatedTaskMinutes)*100/(SELECT
      COUNT(*)
      FROM
      tt_TEMPTASKS TTInner
      WHERE
      TTInner.numReferenceTaskId = TT.numReferenceTaskId
      AND TTInner.numContactId = TT.numContactId
      AND TTInner.numTeam = TT.numTeam))
		,SUM(coalesce(monActualCost,0) -coalesce(monEstimatedCost,0))
		,SUM(fltBuildQty)
		,AVG(coalesce(monActualCost,0)/fltBuildQty)
   FROM
   tt_TEMPTASKS TT
   GROUP BY
   TT.numReferenceTaskId,TT.numContactId,TT.numTeam;

   open SWV_RefCur for
   SELECT
   vcMileStoneName AS "vcMileStoneName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR vcMileStoneName = v_vcMilestone)
   GROUP BY
   vcMileStoneName;

   open SWV_RefCur2 for
   SELECT
   numStageDetailsId AS "numStageDetailsId"
		,vcMileStoneName AS "vcMileStoneName"
		,vcStageName AS "vcStageName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   AND (coalesce(v_vcMilestone,'') = '' OR vcMileStoneName = v_vcMilestone)
   AND (coalesce(v_numStageDetailsId,0) = 0 OR numStageDetailsId = v_numStageDetailsId);

   open SWV_RefCur3 for
   SELECT  numStageDetailsId AS "numStageDetailsId",
      numTaskID AS "numTaskID",
      vcTaskName AS "vcTaskName",
      numTaskTimeInMinutes AS "numTaskTimeInMinutes" FROM tt_TEMP;

   open SWV_RefCur4 for
   SELECT  numTotalRecords AS "numTotalRecords",
      numTaskID AS "numTaskID",
      numBuildUnits AS "numBuildUnits",
      numAssignedTo AS "numAssignedTo",
      numTeam AS "numTeam",
      vcEmployee AS "vcEmployee",
      vcTeam AS "vcTeam",
      numOnTimeOrEarly AS "numOnTimeOrEarly",
      monBudgetImpact AS "monBudgetImpact",
      numAvailability AS "numAvailability",
      numProductivity AS "numProductivity",
      fltUnits AS "fltUnits",
      monLabourPerUnit AS "monLabourPerUnit" FROM tt_TEMPRESULT ORDER BY numTaskID,vcEmployee;

   open SWV_RefCur5 for
   SELECT
   ACI.numContactId
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastname) AS "vcContctName"
		,coalesce(TEMP.numAvailabilityPercent,0) AS "numAvailabilityPercent"
		,coalesce(TEMP.numProductivityPercent,0) AS "numProductivityPercent"
   FROM
   UserMaster UM
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   UM.numUserDetailId = ACI.numContactId
   LEFT JOIN LATERAL(SELECT
      numTotalAvailableMinutes
			,numAvailabilityPercent
			,numProductiveMinutes
			,numProductivityPercent
      FROM
      GetEmployeeAvailabilityPercent(v_numDomainID,ACI.numContactId,v_dtFromDate,v_dtToDate,v_ClientTimeZoneOffset) T) TEMP on TRUE
   WHERE
   UM.numDomainID = v_numDomainID
   AND coalesce(UM.bitactivateflag,false) = true;

   open SWV_RefCur6 for
   SELECT coalesce((SELECT SUM(numTotalRecords) FROM tt_TEMPRESULT),0) AS "numTotalWorkOrders";
   RETURN;
END; $$;


