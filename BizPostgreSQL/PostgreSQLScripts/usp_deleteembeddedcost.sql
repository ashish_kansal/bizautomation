-- Stored procedure definition script USP_DeleteEmbeddedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteEmbeddedCost(v_numOppBizDocID NUMERIC,
    v_numCostCatID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF exists(SELECT * FROM OpportunityBizDocsPaymentDetails WHERE bitIntegrated = true AND numBizDocsPaymentDetId IN(SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE numBizDocsPaymentDetId > 0 AND numOppBizDocID = v_numOppBizDocID)) then
    
      RAISE EXCEPTION 'PAID';
      RETURN;
   end if;
	
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId IN(SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE numBizDocsPaymentDetId > 0 AND numOppBizDocID = v_numOppBizDocID);
   DELETE FROM EmbeddedCostItems WHERE numEmbeddedCostID IN(SELECT numEmbeddedCostID FROM EmbeddedCost WHERE numOppBizDocID = v_numOppBizDocID AND (numCostCatID = v_numCostCatID OR v_numCostCatID = 0) AND numDomainID = v_numDomainID);
	
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numBizDocsPaymentDetId NUMERIC
   );
   INSERT INTO tt_TEMP  SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE numBizDocsPaymentDetId > 0 AND numOppBizDocID = v_numOppBizDocID;
	
   DELETE  FROM EmbeddedCost
   WHERE   numOppBizDocID = v_numOppBizDocID
   AND (numCostCatID = v_numCostCatID OR v_numCostCatID = 0)
   AND numDomainID = v_numDomainID;
   DELETE FROM OpportunityBizDocsDetails WHERE numBizDocsPaymentDetId IN(SELECT numBizDocsPaymentDetId FROM tt_TEMP);
	
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
END; $$; 


