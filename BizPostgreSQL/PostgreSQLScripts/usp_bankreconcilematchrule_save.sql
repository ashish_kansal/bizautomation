-- Stored procedure definition script USP_BankReconcileMatchRule_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BankReconcileMatchRule_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_vcName VARCHAR(200),
    v_vcBankAccounts TEXT,
    v_bitMatchAllConditions BOOLEAN,
	v_vcConditions TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  INTEGER;
   v_numRuleID  NUMERIC(18,0);
   v_hDocItem  INTEGER;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
v_tintOrder := coalesce((SELECT MAX(tintOrder) FROM BankReconcileMatchRule),0)+1;
      INSERT INTO BankReconcileMatchRule(numDomainID
		,vcName
		,vcBankAccounts
		,bitMatchAllConditions
		,numCreatedBy
		,dtCreatedDate
		,tintOrder)
	VALUES(v_numDomainID
		,coalesce(v_vcName, '')
		,coalesce(v_vcBankAccounts, '')
		,coalesce(v_bitMatchAllConditions, false)
		,v_numUserCntID
		,TIMEZONE('UTC',now())
		,v_tintOrder);
	
      v_numRuleID := CURRVAL('BankReconcileMatchRule_seq');

      INSERT INTO BankReconcileMatchRuleCondition(numRuleID
		,tintColumn
		,tintConditionOperator
		,vcTextToMatch
		,numDivisionID)
      SELECT
      v_numRuleID
		,tintColumn
		,tintConditionOperator
		,vcTextToMatch
		,numDivisionID
      FROM
	  XMLTABLE
		(
			'NewDataSet/Condition'
			PASSING 
				CAST(v_vcConditions AS XML)
			COLUMNS
				id FOR ORDINALITY,
				tintColumn SMALLINT PATH 'tintColumn',
				tintConditionOperator SMALLINT PATH 'tintConditionOperator',
				vcTextToMatch TEXT PATH 'vcTextToMatch',
				numDivisionID NUMERIC(18,0) PATH 'numDivisionID'
		) AS X;

EXCEPTION WHEN OTHERS THEN

   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;

