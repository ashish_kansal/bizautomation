-- Stored procedure definition script usp_GetCampaignSalesPersonel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCampaignSalesPersonel(    
  
--    
v_numDomainID NUMERIC,  
        v_numUserCntID NUMERIC,    
        v_tintRights NUMERIC DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 1 then   
        -- ALL RECORDS RIGHTS    
        
      open SWV_RefCur for
      Select AI.vcFirstName || ' ' || AI.vcLastname as SalesPeronel,
  12 as InPipeline, 10 as Closed
      from AdditionalContactsInformation AI
      JOIN  DivisionMaster DM
      ON DM.numDivisionID = AI.numDivisionId
      WHERE DM.numDomainID = v_numDomainID;
   end if;    
    
   IF v_tintRights = 2 then  
        -- OWNER RECORDS RIGHTS    
        
      open SWV_RefCur for
      Select AI.vcFirstName || ' ' || AI.vcLastname as SalesPersonel,
  12 as InPipeline, 10 as Closed
      from AdditionalContactsInformation AI
      JOIN  DivisionMaster DM
      ON DM.numDivisionID = AI.numDivisionId
      WHERE DM.numDomainID = v_numDomainID  and DM.numRecOwner = v_numUserCntID;
   end if;    
    
   IF v_tintRights = 3 then    
        -- TERRITORY RECORDS RIGHTS    
        
      open SWV_RefCur for
      Select AI.vcFirstName || ' ' || AI.vcLastname as SalesPersonel,
  12 as InPipeline, 10 as Closed
      from AdditionalContactsInformation AI
      JOIN  DivisionMaster DM
      ON DM.numDivisionID = AI.numDivisionId
      WHERE DM.numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


