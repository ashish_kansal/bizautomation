-- Stored procedure definition script Get_Email_Template_By_Marketing_Dept for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Get_Email_Template_By_Marketing_Dept( -- 0 false , 1 true  
v_currentUserID INTEGER ,  
v_currentDomainID INTEGER  ,  
v_isForMarketingDept INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_isForMarketingDept = 1 then

      open SWV_RefCur for
      select VcDocName , numGenericDocID from GenericDocuments
      where  numDocCategory = 369
      And
      numCreatedBy = v_currentUserID
      And
      numDomainId = v_currentDomainID
      And
      vcDocumentSection = 'M';
   Else
      open SWV_RefCur for
      select VcDocName , numGenericDocID from GenericDocuments
      where  numDocCategory = 369
      And
      numCreatedBy = v_currentUserID
      And
      numDomainId = v_currentDomainID;
   end if;
   RETURN;
END; $$;


