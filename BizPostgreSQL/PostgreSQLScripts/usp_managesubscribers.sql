-- Stored procedure definition script USP_ManageSubscribers for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSubscribers(INOUT v_numSubscriberID NUMERIC(9,0) ,
    v_numDivisionID NUMERIC(9,0),
    v_intNoofUsersSubscribed INTEGER,
    v_dtSubStartDate TIMESTAMP,
    v_dtSubEndDate TIMESTAMP,
    v_bitTrial BOOLEAN,
    v_numAdminContactID NUMERIC(9,0),
    v_bitActive SMALLINT,
    v_vcSuspendedReason VARCHAR(1000),
    INOUT v_numTargetDomainID NUMERIC(9,0) ,
    v_numDomainID NUMERIC(9,0),
    v_numUserContactID NUMERIC(9,0),
    v_strUsers TEXT DEFAULT '',
    INOUT v_bitExists BOOLEAN DEFAULT false ,
    INOUT v_TargetGroupId NUMERIC(9,0) DEFAULT 0 ,
	v_bitEnabledAccountingAudit BOOLEAN DEFAULT NULL,
	v_bitEnabledNotifyAdmin BOOLEAN DEFAULT NULL,
	v_intNoofLimitedAccessUsers INTEGER DEFAULT NULL,
	v_intNoofBusinessPortalUsers INTEGER DEFAULT NULL,
	v_monFullUserCost DECIMAL(20,5) DEFAULT NULL,
	v_monLimitedAccessUserCost DECIMAL(20,5) DEFAULT NULL,
	v_monBusinessPortalUserCost DECIMAL(20,5) DEFAULT NULL,
	v_monSiteCost DECIMAL(20,5) DEFAULT NULL,
	v_intFullUserConcurrency INTEGER  DEFAULT 0, INOUT SWV_RefCur refcursor default null,
												
    v_bitAllowToEditHelpFile BOOLEAN DEFAULT FALSE)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitOldStatus  BOOLEAN;
   v_hDoc1  INTEGER;
   v_vcCompanyName  VARCHAR(100);                             
   v_numCompanyID  NUMERIC(9,0);                                   
                    
   v_numNewCompanyID  NUMERIC(9,0);                                   
   v_numNewDivisionID  NUMERIC(9,0);                                    
   v_numNewContactID  NUMERIC(9,0);                                    
                                      
   v_vcFirstname  VARCHAR(50);                            
   v_vcEmail  VARCHAR(100);                            
   v_numListItemID  NUMERIC(9,0);                    
   v_numNewListItemID  NUMERIC(9,0);                    
   v_numCountryID  NUMERIC(18,0);
   v_numCurrencyID  NUMERIC(18,0);
   v_numDefCountry  NUMERIC(18,0);
   v_numGroupId1  NUMERIC(9,0);
   v_numGroupId2  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   IF v_numSubscriberID = 0 then
    
      IF NOT EXISTS(SELECT * FROM Subscribers WHERE numDivisionid = v_numDivisionID AND numDomainID = v_numDomainID) then
         v_bitExists := false;
         select   vcCompanyName, C.numCompanyId INTO v_vcCompanyName,v_numCompanyID FROM
         CompanyInfo C
         INNER JOIN
         DivisionMaster D
         ON
         D.numCompanyID = C.numCompanyId WHERE
         D.numDivisionID = v_numDivisionID;
         INSERT INTO Domain(vcDomainName,
                vcDomainDesc,
                numDivisionId,
                numAdminID,
				tintDecimalPoints,
				tintChrForComSearch,
				tintChrForItemSearch,
				bitIsShowBalance,
				tintComAppliesTo,
				tintCommissionType,
				charUnitSystem,
				bitExpenseNonInventoryItem,
						   bitAllowToEditHelpFile)
			VALUES(v_vcCompanyName,
                v_vcCompanyName,
                v_numDivisionID,
                v_numAdminContactID,
                2,
				1,
				1,
				false,
				3,
				1,
				'E',
				true,
				  false);
            
         v_numTargetDomainID := CURRVAL('domain_seq');
         INSERT  INTO CompanyInfo(vcCompanyName,
                numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID)
         SELECT
         vcCompanyName,
                93,--i.e employer --numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID
         FROM
         CompanyInfo
         WHERE
         numCompanyId = v_numCompanyID;
         v_numNewCompanyID := CURRVAL('CompanyInfo_seq');
         INSERT INTO DivisionMaster(numCompanyID,
                vcDivisionName,
                numGrpId,
                bitPublicFlag,
                tintCRMType,
                numStatusID,
                tintBillingTerms,
                numBillingDays,
                tintInterestType,
                fltInterest,
                vcComPhone,
                vcComFax,
                numDomainID)
         SELECT
         v_numNewCompanyID,
				vcDivisionName,
				numGrpId,
				bitPublicFlag,
				2,
				numStatusID,
				tintBillingTerms,
				numBillingDays,
				tintInterestType,
				fltInterest,
				vcComPhone,
				vcComFax,
				numDomainID
         FROM
         DivisionMaster
         WHERE
         numDivisionID = v_numDivisionID;
         v_numNewDivisionID := CURRVAL('DivisionMaster_seq');
         INSERT INTO AddressDetails(vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID)
         SELECT
         vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				v_numNewDivisionID,
				numDomainID
         FROM
         AddressDetails
         WHERE
         numRecordID = v_numDivisionID
         AND tintAddressOf = 2;
         INSERT  INTO AdditionalContactsInformation(vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastname,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact)
         SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastname,
                                    v_numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    true
         FROM    AdditionalContactsInformation
         WHERE   numContactId = v_numAdminContactID;
         v_numNewContactID := CURRVAL('AdditionalContactsInformation_seq');
         UPDATE  CompanyInfo
         SET     numCreatedBy = v_numNewContactID,bintCreatedDate = TIMEZONE('UTC',now()),
         numModifiedBy = v_numNewContactID,bintModifiedDate = TIMEZONE('UTC',now()),
         numDomainID = v_numTargetDomainID
         WHERE   numCompanyId = v_numNewCompanyID;
         UPDATE  DivisionMaster
         SET     numCompanyID = v_numNewCompanyID,numCreatedBy = v_numNewContactID,bintCreatedDate = TIMEZONE('UTC',now()),
         numModifiedBy = v_numNewContactID,
         bintModifiedDate = TIMEZONE('UTC',now()),numDomainID = v_numTargetDomainID,
         tintCRMType = 2,numRecOwner = v_numNewContactID
         WHERE   numDivisionID = v_numNewDivisionID;
         UPDATE  AdditionalContactsInformation
         SET     numDivisionId = v_numNewDivisionID,numCreatedBy = v_numNewContactID,bintCreatedDate = TIMEZONE('UTC',now()),
         numModifiedBy = v_numNewContactID,
         bintModifiedDate = TIMEZONE('UTC',now()),numDomainID = v_numTargetDomainID,
         numRecOwner = v_numNewContactID
         WHERE   numContactId = v_numNewContactID;
         select   vcFirstName, vcEmail INTO v_vcFirstname,v_vcEmail FROM    AdditionalContactsInformation WHERE   numContactId = v_numNewContactID;
         PERFORM Resource_Add(v_vcFirstname,'',v_vcEmail,1,v_numTargetDomainID,v_numNewContactID);   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
         PERFORM USP_ManageNewItemRequiredFields(v_numDomainID := v_numTargetDomainID);
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 6::SMALLINT,v_LocID := 0,v_TabName := '',v_TabID := 0,v_numDomainID := v_numTargetDomainID,
         v_vcURL := '');
         INSERT  INTO RoutingLeads(numDomainID,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority)
                    VALUES(v_numTargetDomainID,
                              'Default',
                              0,
                              v_numNewContactID,
                              v_numNewContactID,
                              TIMEZONE('UTC',now()),
                              v_numNewContactID,
                              TIMEZONE('UTC',now()),
                              true,
                              0);
                    
         INSERT  INTO RoutingLeadDetails(numRoutID,
                              numEmpId,
                              intAssignOrder)
                    VALUES(CURRVAL('RoutingLeads_seq'),
                              v_numNewContactID,
                              1);
                    
         IF NOT EXISTS(SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numTargetDomainID AND vcGroupName = 'System Administrator') then
            INSERT INTO AuthenticationGroupMaster(vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag)
                        VALUES('System Administrator',
                            v_numTargetDomainID,
                            1,
                            1);
						
            v_numGroupId1 := CURRVAL('AuthenticationGroupMaster_seq');
            v_TargetGroupId := v_numGroupId1;
            INSERT INTO GroupAuthorization(numGroupID
							,numModuleID
							,numPageID
							,intExportAllowed
							,intPrintAllowed
							,intViewAllowed
							,intAddAllowed
							,intUpdateAllowed
							,intDeleteAllowed
							,numDomainID)
            SELECT
            v_numGroupId1
							,numModuleID
							,numPageID
							,bitIsExportApplicable
							,0
							,bitIsViewApplicable
							,bitIsAddApplicable
							,bitIsUpdateApplicable
							,bitIsDeleteApplicable
							,v_numTargetDomainID
            FROM
            PageMaster;
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,1,0,true,1,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,36,0,true,2,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,7,0,true,3,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,80,0,true,4,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,3,0,true,5,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,45,0,true,6,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,4,0,true,7,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,44,0,true,8,0);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES(v_numGroupId1,6,0,true,9,0);

						--default Sub tab From cfw_grp_master
						
            INSERT  INTO GroupTabDetails(numGroupID,
                            numTabId,
                            numRelationShip,
                            bitallowed,
                            numOrder,tintType)
            SELECT
            v_numGroupId1,
                            x.Grp_id,
                            0,
                            true,
                            x.numOrder
							,1
            FROM(SELECT *
								 ,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
               FROM
               CFw_Grp_Master
               WHERE
               tintType = 2
               AND numDomainID = v_numTargetDomainID) x; 


						--Added by Anoop for Dashboard +++++++++++++++++++
            IF NOT EXISTS(SELECT numCustomReportId FROM CustomReport WHERE numdomainId = v_numTargetDomainID) then
                        
               PERFORM CreateCustomReportsForNewDomain(v_numTargetDomainID,v_numNewContactID);
            end if;
            INSERT  INTO AuthenticationGroupMaster(vcGroupName,
							numDomainID,
							tintGroupType,
							bitConsFlag)
						VALUES('Executive',
							v_numTargetDomainID,
							1,
							false);
						
            v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq');
            PERFORM USP_ManageSubscribers_GorupPermission(v_numTargetDomainID,v_numGroupId2,258);
            INSERT INTO AuthenticationGroupMaster(vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag)
                        VALUES('Sales Staff',
                            v_numTargetDomainID,
                            1,
                            false);
                        
            v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq');
            PERFORM USP_ManageSubscribers_GorupPermission(v_numTargetDomainID,v_numGroupId2,259);
            INSERT  INTO AuthenticationGroupMaster(vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag)
                        VALUES('Warehouse Staff',
                            v_numTargetDomainID,
                            1,
                            false);
						
            v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq');
            PERFORM USP_ManageSubscribers_GorupPermission(v_numTargetDomainID,v_numGroupId2,260);
            INSERT INTO AuthenticationGroupMaster(vcGroupName,numDomainID,tintGroupType,bitConsFlag) VALUES('Limited Access Users',v_numTargetDomainID,4,true);
						
            v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq');
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,135,0,true,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = v_numGroupId2;
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,5,0,true,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = v_numGroupId2;
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,36,0,true,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = v_numGroupId2;
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,132,0,true,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = v_numGroupId2;
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
						VALUES(v_numTargetDomainID,v_numGroupId2,5,279,1,1),(v_numTargetDomainID,v_numGroupId2,5,280,true,1),
            (v_numTargetDomainID,v_numGroupId2,135,274,1,1),
            (v_numTargetDomainID,v_numGroupId2,135,275,1,1),(v_numTargetDomainID,v_numGroupId2,132,84,1,1),
            (v_numTargetDomainID,v_numGroupId2,132,271,1,1),
            (v_numTargetDomainID,v_numGroupId2,36,250,1,1);
						
            INSERT INTO PageMasterGroupSetting(numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable)
            SELECT 152,16,numGroupID,true,true,false,true,false,false,false,false,false,false,false,false,false,false,false,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 153,16,numGroupID,true,true,false,true,false,false,false,false,false,false,false,false,false,false,false,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 154,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 155,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 156,16,numGroupID,true,true,false,true,false,false,false,false,false,false,false,false,false,false,false,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 157,16,numGroupID,false,false,false,false,false,false,false,false,true,true,false,false,false,false,false,false,false,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 158,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 159,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 160,16,numGroupID,true,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 161,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 162,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 163,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 164,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT 165,16,numGroupID,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2;
            INSERT INTO GroupAuthorization(numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed)
            SELECT numDomainID,numGroupID,152,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,153,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,154,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,155,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,156,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,157,16,1,0,1,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,158,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,159,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,160,16,3,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,161,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,162,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,163,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,164,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2
            UNION
            SELECT numDomainID,numGroupID,165,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2;
            INSERT INTO PageMasterGroupSetting(numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable)
            SELECT 117,40,numGroupID,true,true,false,false,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2;
            INSERT INTO GroupAuthorization(numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed)
            SELECT numDomainID,numGroupID,117,40,1,1,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId2;
            INSERT INTO PageMasterGroupSetting(numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable)
            SELECT
            PageMaster.numPageID,PageMaster.numModuleID,v_numGroupId2,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
            FROM
            PageMaster
            WHERE
            numModuleID = 35
            AND numPageID <> 87
            UNION
            SELECT
            PageMaster.numPageID,PageMaster.numModuleID,v_numGroupId2,true,true,false,false,false,false,false,false,true,true,false,false,false,false,false,false,true,true,false,false
            FROM
            PageMaster
            WHERE
            numModuleID = 35
            AND numPageID = 87;
            INSERT INTO GroupAuthorization(numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed)
            SELECT
            v_numTargetDomainID,v_numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,0,0,0,0,0,0
            FROM
            PageMaster
            WHERE
            numModuleID = 35
            AND numPageID <> 87
            UNION
            SELECT
            v_numTargetDomainID,v_numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,1,0,1,0,1,0
            FROM
            PageMaster
            WHERE
            numModuleID = 35
            AND numPageID = 87;
            INSERT INTO PageMasterGroupSetting(numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable)
						VALUES(1,1,v_numGroupId2,true,true,false,false,false,false,false,false,true,true,false,false,true,true,false,false,false,false,false,false),(5,1,v_numGroupId2,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false),
            (6,1,v_numGroupId2,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false),
            (10,1,v_numGroupId2,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false);
						
            INSERT INTO GroupAuthorization(numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed)
						VALUES(v_numTargetDomainID,v_numGroupId2,1,1,1,0,1,1,0,0),(v_numTargetDomainID,v_numGroupId2,5,1,0,0,0,0,0,0),
            (v_numTargetDomainID,v_numGroupId2,6,1,0,0,0,0,0,0),
            (v_numTargetDomainID,v_numGroupId2,10,1,0,0,0,0,0,0);
         end if;
         IF NOT EXISTS(SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numTargetDomainID AND vcGroupName = 'Business Portal Users') then
                    
            INSERT INTO AuthenticationGroupMaster(vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag)
                        VALUES('Business Portal Users',
                            v_numTargetDomainID,
                            2,
                            true);
                        
            v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq');
            INSERT INTO PageMasterGroupSetting(numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable)
            SELECT
            numPageID,numModuleID,v_numGroupId2,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
            FROM
            PageMaster
            WHERE
            numModuleID IN(32,2,3,4,11,37,10,46,12,7,16);
            INSERT INTO GroupAuthorization(numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
            SELECT
            numPageID,numModuleID,v_numGroupId2,0,0,1,0,0,0,v_numTargetDomainID
            FROM
            PageMaster
            WHERE
            numModuleID IN(32,2,3,4,11,37,10,46,12,7,16)
            AND NOT EXISTS(SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID = PageMaster.numPageID AND GA.numModuleID = PageMaster.numModuleID AND GA.numGroupID = v_numGroupId2);
            INSERT INTO GroupAuthorization(numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
            SELECT
            numListItemID,32,v_numGroupId2,0,0,1,0,0,0,v_numTargetDomainID
            FROM(SELECT
               numListItemID
               FROM
               Listdetails
               WHERE
               numListID = 5
               AND ((constFlag = 1 AND numListItemID <> 46) OR constFlag = 0)) PageMaster
            WHERE
            NOT EXISTS(SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID = PageMaster.numListItemID AND GA.numModuleID = 32 AND GA.numGroupID = v_numGroupId2);
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 4 AND numPageID IN(12,13,16,17);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 4 AND numPageID IN(12,13,16,17) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 11 AND numPageID IN(1,134);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 11 AND numPageID IN(1,134) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 37 AND numPageID IN(34,130,132,149);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 37 AND numPageID IN(34,130,132,149) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 2 AND numPageID IN(7,9,10,11,12);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 2 AND numPageID IN(7,9,10,11,12) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 16 AND numPageID IN(154,155,158,159,161,162,163,164,165);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 16 AND numPageID IN(154,155,158,159,161,162,163,164,165) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 10 AND numPageID IN(15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 10 AND numPageID IN(15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 12 AND numPageID IN(5,6,7,8,13,14,16,17);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 12 AND numPageID IN(5,6,7,8,13,14,16,17) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 3 AND numPageID IN(10,11,13,14,15);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 3 AND numPageID IN(10,11,13,14,15) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 32 AND numPageID IN(2);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 32 AND numPageID IN(2) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsViewApplicable = false,bitViewPermission1Applicable = false WHERE numModuleID = 7 AND numPageID IN(1,12,13);
            UPDATE GroupAuthorization SET intViewAllowed = 0 WHERE numModuleID = 7 AND numPageID IN(1,12,13) AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsAddApplicable = true,bitAddPermission1Applicable = true WHERE numModuleID = 7 AND numPageID = 1;
            UPDATE GroupAuthorization SET intAddAllowed = 1 WHERE numModuleID = 7 AND numPageID = 1 AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsAddApplicable = true,bitAddPermission1Applicable = true,bitIsUpdateApplicable = true,
            bitUpdatePermission1Applicable = true,bitIsDeleteApplicable = true,bitDeletePermission1Applicable = true WHERE numModuleID = 10 AND numPageID = 5;
            UPDATE GroupAuthorization SET intAddAllowed = 1,intUpdateAllowed = 1,intDeleteAllowed = 1 WHERE numModuleID = 10 AND numPageID = 5 AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsAddApplicable = true,bitAddPermission1Applicable = true WHERE numModuleID = 10 AND numPageID = 28;
            UPDATE GroupAuthorization SET intAddAllowed = 1 WHERE numModuleID = 10 AND numPageID = 28 AND numGroupID = v_numGroupId2;
            UPDATE PageMasterGroupSetting SET bitIsAddApplicable = true,bitAddPermission1Applicable = true WHERE numModuleID = 10 AND numPageID = 1;
            UPDATE GroupAuthorization SET intAddAllowed = 1 WHERE numModuleID = 10 AND numPageID = 1 AND numGroupID = v_numGroupId2;
            DELETE FROM GroupTabDetails WHERE numTabId IN(1,4,5,7,80,112,132,133,134,135) AND numGroupID IN(SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType = 2);
            DROP TABLE IF EXISTS tt_TEMPTABS CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPTABS
            (
               numTabID INTEGER,
               numOrder INTEGER
            );
            INSERT INTO tt_TEMPTABS(numTabID,numOrder) VALUES(7,1),(80,2),(1,3),(134,4),(133,5),(135,6),(5,7),(4,8),(112,9),(132,10);
						
            INSERT INTO GroupTabDetails(numGroupID,numTabId,numRelationShip,bitallowed,numOrder)
            SELECT
            v_numGroupId2,numTabID,0,true,numOrder
            FROM
            tt_TEMPTABS;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
            CREATE TEMPORARY TABLE tt_TEMP
            (
               numPageNavID INTEGER
            );
            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 1 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(122),(123),(125),(149);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,1,numPageNavID,true,1
            FROM
            tt_TEMP;

						-----------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 4 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(246),(247),(248);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,4,numPageNavID,true,1
            FROM
            tt_TEMP;

						------------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 5 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(279),(280);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,5,numPageNavID,true,1
            FROM
            tt_TEMP;

						-----------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 7 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(6),(7),(11),(12);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,7,numPageNavID,true,1
            FROM
            tt_TEMP;

						------------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 80 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(62),(1),(2),(3),(4),(71),(72),(76),(171),(240);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,80,numPageNavID,true,1
            FROM
            tt_TEMP;

						-----------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 132 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(270),(84);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,132,numPageNavID,true,1
            FROM
            tt_TEMP;

						------------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 133 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(273),(124),(126),(207);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,133,numPageNavID,true,1
            FROM
            tt_TEMP;

						-----------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 134 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(13);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,134,numPageNavID,true,1
            FROM
            tt_TEMP;

						-----------------------------------------

            DELETE FROM tt_TEMP;
            DELETE FROM TreeNavigationAuthorization WHERE numTabID = 135 AND numGroupID = v_numGroupId2;
            INSERT INTO tt_TEMP(numPageNavID) VALUES(274),(275);
						
            INSERT INTO TreeNavigationAuthorization(numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType)
            SELECT
            v_numTargetDomainID,v_numGroupId2,135,numPageNavID,true,1
            FROM
            tt_TEMP;
         end if;                      
----                   
         PERFORM USP_ChartAcntDefaultValues(v_numDomainID := v_numTargetDomainID,v_numUserCntId := v_numNewContactID);      
    
    
-- Dashboard Size    
         INSERT  INTO DashBoardSize
         SELECT  1,
                                    1,
                                    v_numGroupId1,
                                    1
         UNION
         SELECT  2,
                                    1,
                                    v_numGroupId1,
                                    1
         UNION
         SELECT  3,
                                    2,
                                    v_numGroupId1,
                                    1;     
    
----inserting all the Custome Reports for the newly created group    
    
         INSERT  INTO DashboardAllowedReports
         SELECT  numCustomReportId,
                                    v_numGroupId1
         FROM    CustomReport
         WHERE   numdomainId = v_numTargetDomainID;    
    
    
           
 --inserting Default BizDocs for new domain
         INSERT INTO AuthoritativeBizDocs(numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId)
         SELECT
         coalesce((SELECT numListItemID  FROM Listdetails WHERE numListID = 27 AND constFlag = 1 AND vcData = 'bill'),0),
coalesce((SELECT numListItemID  FROM Listdetails WHERE numListID = 27 AND constFlag = 1 AND vcData = 'invoice'),0),
v_numTargetDomainID;
         IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID = v_numTargetDomainID) then

	--delete from PortalBizDocs WHERE numDomainID=176
            INSERT INTO PortalBizDocs(numBizDocID, numDomainID)
            SELECT numListItemID,v_numTargetDomainID FROM Listdetails WHERE numListID = 27 AND constFlag = 1;
         end if;



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
         INSERT INTO BizDocFilter(numBizDoc ,tintBizocType ,numDomainID)
         SELECT numListItemID,1,v_numTargetDomainID FROM Listdetails WHERE numListID = 27 AND constFlag = 1 AND vcData IN('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order',
         'Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List');
         INSERT INTO BizDocFilter(numBizDoc ,tintBizocType ,numDomainID)
         SELECT numListItemID,2,v_numTargetDomainID FROM Listdetails WHERE numListID = 27 AND constFlag = 1 AND vcData IN('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo',
         'RMA');



--- Give permission of all tree node to all Groups
         PERFORM USP_CreateTreeNavigationForDomain(v_numTargetDomainID,0,Nothing);

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
         PERFORM USP_CreateBizDocTemplateByDefault(v_numDomainID := v_numTargetDomainID,v_txtBizDocTemplate := '',v_txtCss := '',
         v_tintMode := 2::SMALLINT,v_txtPackingSlipBizDocTemplate := '');
 

          
 --inserting the details of BizForms Wizard          
                     -- numeric
     -- text
     -- text
         INSERT  INTO DycFormConfigurationDetails(numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainID,numSurId)
         SELECT  numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,v_numGroupId1,
									v_numTargetDomainID,0
         FROM    DycFormConfigurationDetails
         WHERE   numDomainID = 0
         AND numFormID IN(1,2,6);
         INSERT  INTO DycFormConfigurationDetails(numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainID,numSurId)
         SELECT  numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									v_numTargetDomainID,0
         FROM    DycFormConfigurationDetails
         WHERE   numDomainID = 0
         AND numFormID IN(3,4,5);
         INSERT  INTO DycFormConfigurationDetails(numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainID,numSurId)
         SELECT  numFormID,numFieldID,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									v_numTargetDomainID,0
         FROM    DycFormConfigurationDetails
         WHERE   numDomainID = 0
         AND numFormID IN(7,8);          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,
								numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom)
         SELECT  numFormID,numFieldID,intColumnNum,intRowNum,v_numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
         FROM DycFormConfigurationDetails
         WHERE numDomainID = 0 AND numFormID IN(34,35,36) AND tintPageType = 2;
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
         SELECT numFormId,numFormFieldId,v_numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
         FROM DynamicFormField_Validation
         WHERE numDomainID = 0 AND numFormId IN(34,35,36);
         select   numListItemID INTO v_numListItemID FROM    Listdetails WHERE   numDomainid = 1
         AND numListID = 40    LIMIT 1;
         WHILE v_numListItemID > 0 LOOP
            INSERT  INTO Listdetails(numListID,
                                      vcData,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainid,
                                      constFlag,
                                      sintOrder)
            SELECT  numListID,
                                            vcData,
                                            v_numNewContactID,
                                            TIMEZONE('UTC',now()),
                                            v_numNewContactID,
                                            TIMEZONE('UTC',now()),
                                            bitDelete,
                                            v_numTargetDomainID,
                                            constFlag,
                                            sintOrder
            FROM    Listdetails
            WHERE   numListItemID = v_numListItemID;
            v_numNewListItemID := CURRVAL('ListDetails_seq');
            INSERT  INTO State(numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      ConstFlag,
									  numShippingZone,
									  vcAbbreviations)
            SELECT  v_numNewListItemID,
                                            vcState,
                                            v_numNewContactID,
                                            TIMEZONE('UTC',now()),
                                            v_numNewContactID,
                                            TIMEZONE('UTC',now()),
                                            v_numTargetDomainID,
                                            ConstFlag,
											numShippingZone,
											vcAbbreviations
            FROM    State
            WHERE   numCountryID = v_numListItemID;
            select   numListItemID INTO v_numListItemID FROM    Listdetails WHERE   numDomainid = 1
            AND numListID = 40
            AND numListItemID > v_numListItemID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numListItemID := 0;
            end if;
         END LOOP;                    
                    
                    --INsert Net Terms
         INSERT INTO Listdetails(numListID,vcData,numCreatedBy,bitDelete,numDomainid,constFlag,sintOrder)
         SELECT 296,'0',1,false,v_numTargetDomainID,false,1
         UNION ALL SELECT 296,'7',1,false,v_numTargetDomainID,false,2
         UNION ALL SELECT 296,'15',1,false,v_numTargetDomainID,false,3
         UNION ALL SELECT 296,'30',1,false,v_numTargetDomainID,false,4
         UNION ALL SELECT 296,'45',1,false,v_numTargetDomainID,false,5
         UNION ALL SELECT 296,'60',1,false,v_numTargetDomainID,false,6;
                    --Insert Default email templates
         INSERT INTO GenericDocuments(VcFileName,
						VcDocName,
						numDocCategory,
						cUrlType,
						vcfiletype,
						numDocStatus,
						vcDocdesc,
						numDomainId,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleId)
         SELECT  VcFileName,
							VcDocName,
							numDocCategory,
							cUrlType,
							vcfiletype,
							numDocStatus,
							vcDocdesc,
							v_numTargetDomainID,
							v_numNewContactID,
							TIMEZONE('UTC',now()),
							v_numNewContactID,
							TIMEZONE('UTC',now()),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleId
         FROM    GenericDocuments
         WHERE   numDomainId = 0;
         INSERT  INTO UserMaster(vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitactivateflag,
                              numUserDetailId,
                              vcEmailID)
         SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    v_numGroupId1,
                                    v_numTargetDomainID,
                                    v_numUserContactID,
                                    TIMEZONE('UTC',now()),
                                    v_numUserContactID,
                                    TIMEZONE('UTC',now()),
                                    false,
                                    v_numNewContactID,
                                    vcEmail
         FROM    AdditionalContactsInformation
         WHERE   numContactId = v_numAdminContactID;
         INSERT INTO UOM(numDomainID,vcUnitName,tintUnitType,bitEnabled)
         SELECT v_numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainID = 0;
         if not exists(select * from Currency where numDomainId = v_numTargetDomainID) then
            insert into Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainId,fltExchangeRate,bitEnabled)
            select vcCurrencyDesc, chrCurrency,varCurrSymbol, v_numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainId = 0;
         end if;
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
         select   numListItemID INTO v_numCountryID FROM Listdetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainid = v_numTargetDomainID;

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
         IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainId = v_numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar') then
					
            RAISE NOTICE '%',1;
            INSERT  INTO Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainId,fltExchangeRate,bitEnabled,numCountryId)(SELECT   vcCurrencyDesc,chrCurrency,varCurrSymbol,v_numTargetDomainID,coalesce(fltExchangeRate,1),1 AS bitEnabled,v_numCountryID FROM Currency
            WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainId = 0 LIMIT 1)
            UNION
            SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,v_numTargetDomainID,fltExchangeRate,0 AS bitEnabled,CAST(null as INTEGER) FROM Currency
            WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainId = 0;
         ELSE
            RAISE NOTICE '%',2;
            UPDATE Currency SET numCountryId = v_numCountryID,bitEnabled = true,fltExchangeRate = 1
            WHERE numDomainId = v_numTargetDomainID
            AND vcCurrencyDesc = 'USD-U.S. Dollar';
            RAISE NOTICE '%',3;
         end if;
						
					--Set Default Currency
         SELECT  numCurrencyID INTO v_numCurrencyID FROM Currency WHERE (bitEnabled = true or chrCurrency = 'USD') AND numDomainId = v_numTargetDomainID     LIMIT 1;
                   
					--Set Default Country
         SELECT  numListItemID INTO v_numDefCountry FROM Listdetails where numListID = 40 AND numDomainid = v_numTargetDomainID and vcData ilike '%United States%'     LIMIT 1;
         UPDATE  Domain
         SET     numDivisionId = v_numNewDivisionID,numAdminID = v_numNewContactID,numCurrencyID = v_numCurrencyID,
         numDefCountry = coalesce(v_numDefCountry,0)
         WHERE   numDomainId = v_numTargetDomainID;                                      
                         	
					------------------


                         
         INSERT  INTO Subscribers(numDivisionid,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin)
                    VALUES(v_numDivisionID,
                              v_numNewContactID,
                              v_numTargetDomainID,
                              v_numDomainID,
                              v_numUserContactID,
                              TIMEZONE('UTC',now()),
                              v_numUserContactID,
                              TIMEZONE('UTC',now()),
                              GetUTCDateWithoutTime(),
                              GetUTCDateWithoutTime(),
							  v_bitEnabledAccountingAudit,
							  v_bitEnabledNotifyAdmin);
                    
         v_numSubscriberID := CURRVAL('Subscribers_seq');
      ELSE
         select   numSubscriberID INTO v_numSubscriberID FROM    Subscribers WHERE   numDivisionid = v_numDivisionID
         AND numDomainID = v_numDomainID;
         UPDATE  Subscribers
         SET     bitDeleted = false
         WHERE   numSubscriberID = v_numSubscriberID;
      end if;
   ELSE
      v_bitExists := true;
      select   bitActive, numTargetDomainID INTO v_bitOldStatus,v_numTargetDomainID FROM
      Subscribers WHERE
      numSubscriberID = v_numSubscriberID;
      IF (v_bitOldStatus = true AND v_bitActive = 0) then
         UPDATE Subscribers SET dtSuspendedDate = TIMEZONE('UTC',now()) WHERE numSubscriberID = v_numSubscriberID;
      end if;
      UPDATE
      Subscribers
      SET
      intNoofUsersSubscribed = v_intNoofUsersSubscribed,dtSubStartDate = v_dtSubStartDate,
      dtSubEndDate = v_dtSubEndDate,bitTrial = v_bitTrial,numAdminContactID = v_numAdminContactID,
      bitActive = v_bitActive,vcSuspendedReason = v_vcSuspendedReason,
      numModifiedBy = v_numUserContactID,dtModifiedDate = TIMEZONE('UTC',now()),
      bitEnabledAccountingAudit = v_bitEnabledAccountingAudit,
      bitEnabledNotifyAdmin = v_bitEnabledNotifyAdmin,intNoofLimitedAccessUsers = v_intNoofLimitedAccessUsers,
      intNoofBusinessPortalUsers = v_intNoofBusinessPortalUsers,
      monFullUserCost = v_monFullUserCost,
      monLimitedAccessUserCost = v_monLimitedAccessUserCost,monBusinessPortalUserCost = v_monBusinessPortalUserCost,
      monSiteCost = v_monSiteCost,
      intFullUserConcurrency = v_intFullUserConcurrency
      WHERE
      numSubscriberID = v_numSubscriberID;
      UPDATE
      Domain
      SET
      numAdminID = v_numAdminContactID,
	  bitAllowToEditHelpFile = v_bitAllowToEditHelpFile
      WHERE
      numDomainId = v_numTargetDomainID;

      INSERT INTO UserMaster(vcUserName,
            vcMailNickName,
            vcUserDesc,
            numGroupID,
            numDomainID,
            numCreatedBy,
            bintCreatedDate,
            numModifiedBy,
            bintModifiedDate,
            bitactivateflag,
            numUserDetailId,
            vcEmailID,
            vcPassword)
      SELECT  UserName,
                UserName,
                UserName,
                0,
                v_numTargetDomainID,
                v_numUserContactID,
                TIMEZONE('UTC',now()),
                v_numUserContactID,
                TIMEZONE('UTC',now()),
                (CASE WHEN Active = 1 THEN true ELSE false END),
                numContactID,
                Email,
                vcPassword
      FROM
	  XMLTABLE
		(
			'NewDataSet/Users'
			PASSING 
				CAST(v_strUsers AS XML)
			COLUMNS
				numContactID NUMERIC(9,0) PATH 'numContactID',
				numUserID NUMERIC(9,0) PATH 'numUserID',
				Email VARCHAR(100) PATH 'Email',
				vcPassword VARCHAR(100) PATH 'vcPassword',
				UserName VARCHAR(100) PATH 'UserName',
				Active SMALLINT PATH 'Active'
		) AS X WHERE numUserID=0;

      UPDATE  UserMaster
      SET     vcUserName = X.UserName,numModifiedBy = v_numUserContactID,bintModifiedDate = TIMEZONE('UTC',now()),
      vcEmailID = X.Email,vcPassword = X.vcPassword,
      bitactivateflag = (CASE WHEN Active = 1 THEN true ELSE false END)
      FROM(SELECT    numUserId AS UserID,
                            numContactID,
                            Email,
                            vcPassword,
                            UserName,
                            Active
      FROM  
	  XMLTABLE
		(
			'NewDataSet/Users'
			PASSING 
				CAST(v_strUsers AS XML)
			COLUMNS
				numContactID NUMERIC(9,0) PATH 'numContactID',
				numUserID NUMERIC(9,0) PATH 'numUserID',
				Email VARCHAR(100) PATH 'Email',
				vcPassword VARCHAR(100) PATH 'vcPassword',
				UserName VARCHAR(100) PATH 'UserName',
				Active SMALLINT PATH 'Active'
		) AS X WHERE numUserID > 0) X
      WHERE   UserMaster.numUserID = X.UserID;
    
   end if;
	                           
   open SWV_RefCur for
   SELECT  v_numSubscriberID;
   RETURN;
END; $$;


