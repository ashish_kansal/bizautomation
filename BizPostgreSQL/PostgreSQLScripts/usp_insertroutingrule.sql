-- Stored procedure definition script usp_InsertRoutingRule for PostgreSQL
CREATE OR REPLACE FUNCTION usp_InsertRoutingRule(v_numDomainID NUMERIC,                    
 v_vcRoutName VARCHAR(50),  
 v_tintEqualTo SMALLINT DEFAULT 0,                                    
 v_numValue NUMERIC(9,0) DEFAULT 0,                                               
 v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                       
 v_tintPriority SMALLINT DEFAULT NULL,  
 v_strValues TEXT DEFAULT NULL,  
 v_strDistribitionList TEXT DEFAULT NULL,
 v_vcDBCoulmnName VARCHAR(100) DEFAULT '',
 v_vcSelectedColumn VARCHAR(200) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRoutID  NUMERIC(9,0);
BEGIN
   select   coalesce(max(tintPriority),0) INTO v_tintPriority from RoutingLeads where numDomainID = v_numDomainID;    
   v_tintPriority := v_tintPriority+1;              
         
  
          
   Insert into RoutingLeads(numDomainID, vcRoutName,tintEqualTo, numValue,
 numCreatedBy, dtCreatedDate ,numModifiedBy, dtModifiedDate,bitDefault,tintPriority,vcDbColumnName,vcSelectedCoulmn)
   values(v_numDomainID, v_vcRoutName, v_tintEqualTo, v_numValue,
    v_numUserCntID, TIMEZONE('UTC',now()),v_numUserCntID, TIMEZONE('UTC',now()),false,v_tintPriority,v_vcDBCoulmnName,v_vcSelectedColumn);    
  
       
    
   v_numRoutID := CURRVAL('RoutingLeads_seq');    
   if SUBSTR(CAST(v_strValues AS VARCHAR(2)),1,2) <> '' then
      insert into RoutinLeadsValues(numRoutID,vcValue) VALUES (v_numRoutID,unnest(xpath('//vcValue/text()', v_strValues::xml))::text::VARCHAR);
   end if;  
   if SUBSTR(CAST(v_strDistribitionList AS VARCHAR(2)),1,2) <> '' then
      insert into RoutingLeadDetails(numRoutID,numEmpId,intAssignOrder)
	  VALUES
      (v_numRoutID,unnest(xpath('//numEmpId/text()', v_strDistribitionList::xml))::text::NUMERIC,unnest(xpath('//intAssignOrder/text()', v_strDistribitionList::xml))::text::INTEGER);
   end if;         
        
   open SWV_RefCur for select v_numRoutID;
END; $$;












