-- Stored procedure definition script USP_ManagePageNavigationAuthorization for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManagePageNavigationAuthorization(v_numGroupID NUMERIC(18,0),
	v_numTabID		NUMERIC(18,0),
    v_numDomainID	NUMERIC(18,0),
    v_strItems		TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   -- BEGIN TRAN
DELETE  FROM TreeNavigationAuthorization WHERE numGroupID = v_numGroupID
   AND numDomainID = v_numDomainID
   AND numTabID = v_numTabID
   AND numPageNavID NOT IN(SELECT coalesce(numPageNavID,0) FROM PageNavigationDTL WHERE bitVisible = false);
   IF CAST(v_strItems AS TEXT) <> '' then
    
      IF v_numTabID = 7 then --Relationships
				
         DELETE FROM TreeNodeOrder WHERE numGroupID = v_numGroupID AND numDomainID = v_numDomainID AND numTabID = v_numTabID;
         DROP TABLE IF EXISTS tt_TMEP CASCADE;
         CREATE TEMPORARY TABLE tt_TMEP AS
            SELECT
            v_numGroupID AS numGroupID,
						v_numTabID AS numTabID,
						X.numPageNavID AS numPageNavID,
						X.numListItemID As numListItemID,
						X.bitVisible AS bitVisible,
						v_numDomainID AS numDomainID,
						X.numParentID AS numParentID,
						X.tintType AS tintType,
						X.numOrder AS numOrder
					
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numPageNavID NUMERIC(9,0) PATH 'numPageNavID',
					bitVisible BOOLEAN PATH 'bitVisible',
					tintType SMALLINT PATH 'tintType',
					numListItemID NUMERIC(18,0) PATH 'numListItemID',
					numParentID NUMERIC(18,0) PATH 'numParentID',
					numOrder INTEGER PATH 'numOrder'
			) AS X;

         INSERT  INTO TreeNavigationAuthorization(numGroupID
							  ,numTabID
							  ,numPageNavID
							  ,bitVisible
							  ,numDomainID
							  ,tintType)
         SELECT
         numGroupID,
						numTabID,
						numPageNavID,
						bitVisible,
						numDomainID,
						tintType
         FROM
         tt_TMEP
         WHERE
         coalesce(numPageNavID,0) <> 0;
         INSERT  INTO TreeNodeOrder(numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder)
         SELECT
         numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentId,
						bitVisible,
						tintType,
						numOrder
         FROM
         tt_TMEP;
      ELSE
         INSERT  INTO TreeNavigationAuthorization(numGroupID
						  ,numTabID
						  ,numPageNavID
						  ,bitVisible
						  ,numDomainID
						  ,tintType)
         SELECT  v_numGroupID,
							    v_numTabID,
							    X.numPageNavID,
							    X.bitVisible,
							    v_numDomainID,
							    X.tintType
         FROM
		 XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numPageNavID NUMERIC(9,0) PATH 'numPageNavID',
					bitVisible BOOLEAN PATH 'bitVisible',
					tintType SMALLINT PATH 'tintType'
			) AS X;
					-- SHOW PARENT RECURSIVELY
         WITH RECURSIVE CTE(numPageNavID,numParentID) AS(SELECT
         PND.numPageNavID AS numPageNavID
							,PND.numParentID AS numParentID
         FROM
         TreeNavigationAuthorization TNA
         INNER JOIN
         PageNavigationDTL PND
         ON
         TNA.numPageNavID = PND.numPageNavID
         WHERE
         TNA.bitVisible = true
         AND TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numPageNavID NOT IN(26)
         AND TNA.numTabID = v_numTabID
         UNION ALL
         SELECT
         PND.numPageNavID AS numPageNavID
							,PND.numParentID AS numParentID
         FROM
         TreeNavigationAuthorization TNA
         INNER JOIN
         PageNavigationDTL PND
         ON
         TNA.numPageNavID = PND.numPageNavID
         JOIN
         CTE c
         ON
         TNA.numPageNavID = c.numParentID
         WHERE
         TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numTabID = v_numTabID)
         UPDATE
         TreeNavigationAuthorization
         SET
         bitVisible = true
         WHERE
         numDomainID = v_numDomainID
         AND numGroupID = v_numGroupID
         AND numTabID = v_numTabID
         AND bitVisible = false
         AND numPageNavID IN(SELECT numPageNavID FROM CTE);


					--HIDE CHILD RECURSIVELY
         WITH RECURSIVE CTE(numPageAuthID,numDomainID,numGroupID,numTabID,numPageNavID) AS(SELECT
         numPageAuthID AS numPageAuthID,
							numDomainID AS numDomainID,
							numGroupID AS numGroupID,
							numTabID AS numTabID,
							numPageNavID AS numPageNavID
         FROM
         TreeNavigationAuthorization TNA
         WHERE
         TNA.bitVisible = false
         AND TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numPageNavID NOT IN(26)
         AND TNA.numTabID = v_numTabID
         UNION ALL
         SELECT
         TNA.numPageAuthID AS numPageAuthID,
							TNA.numDomainID AS numDomainID,
							TNA.numGroupID AS numGroupID,
							TNA.numTabID AS numTabID,
							TNA.numPageNavID AS numPageNavID
         FROM
         TreeNavigationAuthorization TNA
         JOIN
         PageNavigationDTL PND
         ON
         TNA.numPageNavID = PND.numPageNavID
         JOIN
         CTE c
         ON
         PND.numParentID = c.numPageNavID
         WHERE
         TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numTabID = v_numTabID)
         UPDATE TreeNavigationAuthorization SET bitVisible = false WHERE bitVisible = true AND numPageAuthID IN(SELECT numPageAuthID FROM CTE);
      end if;
     
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      RETURN;
END; $$;



