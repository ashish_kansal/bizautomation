-- Stored procedure definition script USP_BizAPIThrottlePolicy_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizAPIThrottlePolicy_Get(v_BizAPIPublicKey VARCHAR(100),
	v_IpAddress VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
   open SWV_RefCur for SELECT * FROM BizAPIThrottlePolicy WHERE RateLimitKey = v_BizAPIPublicKey OR RateLimitKey = v_IpAddress;
END; $$;













