-- Stored procedure definition script usp_GetBankDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
/****** Added By : Joseph ******/
/****** Gets Bank Account Details with Account Balance and LastUpdated Details from Table BankDetails******/


CREATE OR REPLACE FUNCTION usp_GetBankDetail(v_tintMode NUMERIC(9,0) DEFAULT 0,  
v_numBankDetailID NUMERIC(18,0) DEFAULT 0,
v_numAccountID NUMERIC(18,0) DEFAULT 0,
v_numDomainID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN
      IF v_tintMode = 0 then
		
         open SWV_RefCur for
         SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl, BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,BD.numUserContactID,BSH.dtCreatedDate
         FROM BankDetails BD INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
         LEFT JOIN BankStatementHeader BSH ON  BD.numBankDetailID = BSH.numBankDetailID
         WHERE BD.numBankDetailID = v_numBankDetailID AND  BD.bitIsActive = true AND BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =(SELECT MAX(dtCreatedDate) FROM BankStatementHeader WHERE numBankDetailID = v_numBankDetailID);
      end if;
      IF v_tintMode = 1 then
		
         open SWV_RefCur for
         SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl,
			BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,BD.numUserContactID,BSH.dtCreatedDate
         FROM BankDetails BD INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
         LEFT JOIN BankStatementHeader BSH ON  BD.numBankDetailID = BSH.numBankDetailID
         WHERE  BD.vcAccountNumber IS NOT NULL AND BD.vcAccountType IS NOT NULL AND BD.vcUserName IS NOT NULL AND BD.vcPassword IS NOT NULL AND  BD.bitIsActive = true AND BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =(SELECT MAX(dtCreatedDate) FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID);
      end if;
      IF v_tintMode = 2 then
		
         open SWV_RefCur for
         SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl, BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,BD.numUserContactID FROM BankDetails BD
         INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
         INNER JOIN Chart_Of_Accounts COA ON BD.numAccountID = COA.numAccountId
         WHERE COA.numDomainId = v_numDomainID AND COA.numAccountId = v_numAccountID AND BD.bitIsActive = true;
      end if;
   END;
   RETURN;
END; $$;
--/****** Object:  StoredProcedure [dbo].[USP_GetBankDetails]    Script Date: 07/26/2008 16:16:21 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
----Created By Siva  
--GO
--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbankdetails')
--DROP PROCEDURE usp_getbankdetails
--GO
--CREATE PROCEDURE [dbo].[USP_GetBankDetails]  
--@numDomainId as numeric(9)=0  
--As  
--Begin  
--	Select * From Domain Where numDomainId=@numDomainId  
--End
--GO



