-- Stored procedure definition script USP_DeleteDiscProfile for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteDiscProfile(v_numDiscProfileID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from ItemDiscountProfile where numDiscProfileID = v_numDiscProfileID;
   RETURN;
END; $$;


