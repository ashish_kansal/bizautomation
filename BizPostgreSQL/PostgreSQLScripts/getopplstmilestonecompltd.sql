-- Function definition script GetOppLstMileStoneCompltd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppLstMileStoneCompltd(v_numoppid NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPercentage  VARCHAR(100);
BEGIN
   select  numStagePercentage INTO v_numPercentage from OpportunityStageDetails where numoppid = v_numoppid
   and numStagePercentage not  in(select numStagePercentage from OpportunityStageDetails where numoppid = v_numoppid  and bitstagecompleted = false) and  numStagePercentage != 100 and bitstagecompleted = true   order by numStagePercentage desc  LIMIT 1;
   if v_numPercentage != '' then

      v_numPercentage := 'Milestone -' || coalesce(v_numPercentage,'') || ' %';
   end if;



   return coalesce(v_numPercentage,'-');
END; $$;

