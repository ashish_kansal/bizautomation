-- Stored procedure definition script USP_ManageDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDomain(v_byteMode SMALLINT DEFAULT 0,  
v_vcDomainName VARCHAR(50) DEFAULT '',  
v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);
BEGIN
   if v_byteMode = 0 then
      insert into Domain(vcDomainName)
values(v_vcDomainName);

      v_numDomainId := CURRVAL('domain_seq');
      insert into CompanyInfo(vcCompanyName,bintCreatedDate,numDomainID)
values(v_vcDomainName,TIMEZONE('UTC',now()),v_numDomainId);

      insert into DivisionMaster(numCompanyID,vcDivisionName,bintCreatedDate,tintCRMType,numDomainID,numStatusID,bitLeadBoxFlg,numGrpId)
values(CURRVAL('CompanyInfo_seq'),'-',TIMEZONE('UTC',now()),2,v_numDomainId,2,false,0);

      v_numDivisionID := CURRVAL('DivisionMaster_seq');
      insert into AdditionalContactsInformation(vcGivenName,vcFirstName,vcLastname,numDivisionId,numContactType,bintCreatedDate,numEmpStatus,numDomainID,bitPrimaryContact)
values('Administrator','Administrator','-',v_numDivisionID,0,TIMEZONE('UTC',now()),658,v_numDomainId,true);

      update Domain set numDivisionId = v_numDivisionID where numDomainId = v_numDomainId;
      open SWV_RefCur for
      select v_numDomainId;
   end if;  
  
   if v_byteMode = 1 then

      update Domain set vcDomainName = v_vcDomainName
      where numDomainId = v_numDomainId;
      open SWV_RefCur for
      select v_numDomainId;
   end if;  
  
   if v_byteMode = 2 then

      open SWV_RefCur for
      select numDomainId,vcDomainName from Domain;
   end if;
   RETURN;
END; $$;


