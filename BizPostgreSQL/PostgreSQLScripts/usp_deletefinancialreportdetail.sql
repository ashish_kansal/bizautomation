-- Stored procedure definition script USP_DeleteFinancialReportDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteFinancialReportDetail(v_numFRDtlID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE  FROM FinancialReportDetail
   WHERE   numFRDtlID = v_numFRDtlID;
   RETURN;
END; $$; 


