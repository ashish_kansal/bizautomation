-- Stored procedure definition script usp_CountAlertPanel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CountAlertPanel(v_chrAction CHAR(10) DEFAULT NULL,
  v_numDomainId NUMERIC(18,2) DEFAULT 0,
  v_numUserId NUMERIC(18,2) DEFAULT 0,
  v_numModuleId NUMERIC(18,2) DEFAULT 0,
  v_numRecordId NUMERIC(18,2) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcStartDate  VARCHAR(30) DEFAULT '2/4/2016';

--All Count
BEGIN
   IF(v_chrAction = 'ALLC') then
	
      open SWV_RefCur for
      SELECT(SELECT COUNT(D.numDivisionID)
         FROM
         View_CompanyAlert as C
         LEFT JOIN
         DivisionMaster as D
         ON
         C.numCompanyId = D.numCompanyID
         LEFT JOIN
         UserMaster as U
         ON
         C.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         ShareRecord as SH
         ON
         D.numDivisionID = SH.numRecordID AND SH.numModuleID = 1
         WHERE
				--(C.bintCreatedDate>=Cast(@vcStartDate as date) OR D.bintModifiedDate>=Cast(@vcStartDate as date)) AND 
         C.numDomainID = v_numDomainId AND D.numDivisionID != 0 AND
         C.numCreatedBy != v_numUserId AND (D.numAssignedTo = v_numUserId OR D.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND D.numDivisionID NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId = 1 AND numDomainID = v_numDomainId)) as company,
		(SELECT COUNT(C.numCaseId)
         FROM
         View_CaseAlert as C
         LEFT JOIN
         DivisionMaster as D
         ON
         C.numDivisionID = D.numDivisionID
         LEFT JOIN
         CompanyInfo as CO
         ON
         D.numCompanyID = CO.numCompanyId
         LEFT JOIN
         UserMaster as U
         ON
         C.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         AdditionalContactsInformation as AC
         ON
         C.numContactId = AC.numContactId
         LEFT JOIN
         ShareRecord as SH
         ON
         C.numCaseId = SH.numRecordID AND SH.numModuleID = 7
         WHERE
				--C.bintModifiedDate>=Cast(@vcStartDate as date) AND 
         C.numDomainID = v_numDomainId AND C.numCaseId != 0 AND
         C.numCreatedBy != v_numUserId AND (C.numAssignedTo = v_numUserId OR C.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND C.numCaseId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId = 2 AND numDomainID = v_numDomainId)) as "case",
		(SELECT COUNT(O.numOppId)
         FROM
         View_OpportunityAlert as O
         LEFT JOIN
         DivisionMaster as D
         ON
         O.numDivisionID = D.numDivisionID
         LEFT JOIN
         CompanyInfo as CO
         ON
         D.numCompanyID = CO.numCompanyId
         LEFT JOIN
         UserMaster as U
         ON
         O.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         AdditionalContactsInformation as AC
         ON
         O.numContactId = AC.numContactId
         LEFT JOIN
         ShareRecord as SH
         ON
         SH.numRecordID = O.numOppId AND SH.numModuleID = 3
         WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
         O.numDomainID = v_numDomainId AND O.numOppId != 0 AND O.tintOppStatus = 0 AND
         O.numCreatedBy != v_numUserId AND (O.numAssignedTo = v_numUserId OR O.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND O.numOppId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(3,4,5,6) AND numDomainID = v_numDomainId)) as opp,
	(SELECT COUNT(O.numOppId)
         FROM
         View_OpportunityAlert as O
         LEFT JOIN
         DivisionMaster as D
         ON
         O.numDivisionID = D.numDivisionID
         LEFT JOIN
         CompanyInfo as CO
         ON
         D.numCompanyID = CO.numCompanyId
         LEFT JOIN
         UserMaster as U
         ON
         O.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         AdditionalContactsInformation as AC
         ON
         O.numContactId = AC.numContactId
         LEFT JOIN
         ShareRecord as SH
         ON
         SH.numRecordID = O.numOppId AND SH.numModuleID = 3
         WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
         O.numDomainID = v_numDomainId AND O.numOppId != 0 AND O.tintOppStatus = 1 AND
         O.numCreatedBy != v_numUserId AND (O.numAssignedTo = v_numUserId OR O.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND O.numOppId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(3,4,5,6) AND numDomainID = v_numDomainId)) as "order",
	(SELECT  COUNT(P.numProId)
         FROM
         View_ProjectsAlert as P
         LEFT JOIN
         DivisionMaster as D
         ON
         P.numDivisionID = D.numDivisionID
         LEFT JOIN
         CompanyInfo as CO
         ON
         D.numCompanyID = CO.numCompanyId
         LEFT JOIN
         UserMaster as U
         ON
         P.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         ShareRecord as SH
         ON
         SH.numRecordID = P.numProId AND SH.numModuleID = 5
         WHERE
				--P.bintModifiedDate>=Cast(@vcStartDate as date) AND 
         P.numDomainID = v_numDomainId AND P.numProId != 0 AND
         P.numCreatedBy != v_numUserId AND (P.numAssignedTo = v_numUserId OR P.numRecOwner = v_numUserId OR P.numIntPrjMgr = v_numUserId OR SH.numAssignedTo = v_numUserId) AND P.numProId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(7) AND numDomainID = v_numDomainId)) as project,
	(SELECT COUNT(H.numEmailHstrID)
         FROM
         View_EmailAlert as H
         LEFT JOIN
         UserMaster as U
         ON
         H.numUserCntId = U.numUserDetailId
         LEFT JOIN
         EmailMaster as T
         ON
         H.vcTo = CONCAT(T.numEmailId,'$^$',T.vcName,'$^$',T.vcEmailId)
         LEFT JOIN
         UserMaster as UT
         ON
         T.numContactID = UT.numUserDetailId
         WHERE
				--H.bintCreatedOn>=Cast(@vcStartDate as date) AND 
         H.numDomainID = v_numDomainId AND H.numEmailHstrID != 0 AND
         H.numUserCntId != v_numUserId AND T.numContactID = v_numUserId AND H.numEmailHstrID NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(8) AND numDomainID = v_numDomainId)) as Email,
	(SELECT COUNT(C.numCommId)
         FROM
         View_TicklerAlert as C
         LEFT JOIN
         DivisionMaster as D
         ON
         C.numDivisionID = D.numDivisionID
         LEFT JOIN
         CompanyInfo as CO
         ON
         D.numCompanyID = CO.numCompanyId
         LEFT JOIN
         UserMaster as U
         ON
         C.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         AdditionalContactsInformation as AD
         ON
         C.numContactId = AD.numContactId
         WHERE
				--C.dtModifiedDate>=Cast(@vcStartDate as date) AND 
         C.numDomainID = v_numDomainId AND C.numCommId != 0 AND
         C.numCreatedBy != v_numUserId AND C.numAssignedBy != v_numUserId AND C.numAssign = v_numUserId  AND C.numCommId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(9) AND numDomainID = v_numDomainId)) as tickler,
	(SELECT COUNT(S.numStageDetailsId)
         FROM
         View_ProcessAlert as S
         LEFT JOIN
         UserMaster as U
         ON
         S.numCreatedBy = U.numUserDetailId
         LEFT JOIN
         OpportunityMaster as O
         ON
         O.numOppId = S.numOppID
         LEFT JOIN
         ProjectsMaster as P
         ON
         P.numProId = S.numProjectID
         WHERE
				--S.bintModifiedDate>=Cast(@vcStartDate as date) AND 
         S.numDomainID = v_numDomainId AND S.numStageDetailsId != 0 AND
				(O.numassignedto = v_numUserId OR O.numrecowner = v_numUserId OR P.numAssignedTo = v_numUserId OR P.numRecOwner = v_numUserId OR P.numIntPrjMgr = v_numUserId)
         AND (O.numAssignedBy != v_numUserId OR P.numAssignedby != v_numUserId OR O.numCreatedBy != v_numUserId OR P.numCreatedBy != v_numUserId) AND
         S.numCreatedBy != v_numUserId AND  S.numStageDetailsId NOT IN(SELECT numRecordId
            FROM
            TrackNotification
            WHERE
            numUserId = v_numUserId and numModuleId IN(10) AND numDomainID = v_numDomainId)) as process;
   end if; 



--Organization Alert
   IF(v_chrAction = 'V') then
	
      open SWV_RefCur for
      SELECT COUNT(D.numDivisionID)
      FROM
      View_CompanyAlert as C
      LEFT JOIN
      DivisionMaster as D
      ON
      C.numCompanyId = D.numCompanyID
      LEFT JOIN
      UserMaster as U
      ON
      C.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      ShareRecord as SH
      ON
      D.numDivisionID = SH.numRecordID AND SH.numModuleID = 1
      WHERE
				--(C.bintCreatedDate>=Cast(@vcStartDate as date) OR D.bintModifiedDate>=Cast(@vcStartDate as date)) AND 
      C.numDomainID = v_numDomainId AND D.numDivisionID != 0 AND
      C.numCreatedBy != v_numUserId AND (D.numAssignedTo = v_numUserId OR D.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND D.numDivisionID NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId = v_numModuleId AND numDomainID = v_numDomainId);
   end if;

--Case Module Alert
   IF(v_chrAction = 'CV') then
	
      open SWV_RefCur for
      SELECT COUNT(C.numCaseId)
      FROM
      View_CaseAlert as C
      LEFT JOIN
      DivisionMaster as D
      ON
      C.numDivisionID = D.numDivisionID
      LEFT JOIN
      CompanyInfo as CO
      ON
      D.numCompanyID = CO.numCompanyId
      LEFT JOIN
      UserMaster as U
      ON
      C.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      AdditionalContactsInformation as AC
      ON
      C.numContactId = AC.numContactId
      LEFT JOIN
      ShareRecord as SH
      ON
      C.numCaseId = SH.numRecordID AND SH.numModuleID = 7
      WHERE
				--C.bintModifiedDate>=Cast(@vcStartDate as date) AND 
      C.numDomainID = v_numDomainId AND C.numCaseId != 0 AND
      C.numCreatedBy != v_numUserId AND (C.numAssignedTo = v_numUserId OR C.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND C.numCaseId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId = v_numModuleId AND numDomainID = v_numDomainId);
   end if;

--Puchase & Sales Oppourtunity Module Alert
   IF(v_chrAction = 'PO') then
	
      open SWV_RefCur for
      SELECT COUNT(O.numOppId)
      FROM
      View_OpportunityAlert as O
      LEFT JOIN
      DivisionMaster as D
      ON
      O.numDivisionID = D.numDivisionID
      LEFT JOIN
      CompanyInfo as CO
      ON
      D.numCompanyID = CO.numCompanyId
      LEFT JOIN
      UserMaster as U
      ON
      O.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      AdditionalContactsInformation as AC
      ON
      O.numContactId = AC.numContactId
      LEFT JOIN
      ShareRecord as SH
      ON
      SH.numRecordID = O.numOppId AND SH.numModuleID = 3
      WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
      O.numDomainID = v_numDomainId AND O.numOppId != 0 AND O.tintOppStatus = 0 AND
      O.numCreatedBy != v_numUserId AND (O.numAssignedTo = v_numUserId OR O.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND O.numOppId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(3,4,5,6) AND numDomainID = v_numDomainId);
   end if;

   IF(v_chrAction = 'SO') then
	
      open SWV_RefCur for
      SELECT COUNT(O.numOppId)
      FROM
      View_OpportunityAlert as O
      LEFT JOIN
      DivisionMaster as D
      ON
      O.numDivisionID = D.numDivisionID
      LEFT JOIN
      CompanyInfo as CO
      ON
      D.numCompanyID = CO.numCompanyId
      LEFT JOIN
      UserMaster as U
      ON
      O.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      AdditionalContactsInformation as AC
      ON
      O.numContactId = AC.numContactId
      LEFT JOIN
      ShareRecord as SH
      ON
      SH.numRecordID = O.numOppId AND SH.numModuleID = 3
      WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
      O.numDomainID = v_numDomainId AND O.numOppId != 0 AND O.tintOppStatus = 1 AND
      O.numCreatedBy != v_numUserId AND (O.numAssignedTo = v_numUserId OR O.numRecOwner = v_numUserId OR SH.numAssignedTo = v_numUserId) AND O.numOppId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(3,4,5,6) AND numDomainID = v_numDomainId);
   end if;

--Projects Module Alert
   IF(v_chrAction = 'PV') then
	
      open SWV_RefCur for
      SELECT  COUNT(P.numProId)
      FROM
      View_ProjectsAlert as P
      LEFT JOIN
      DivisionMaster as D
      ON
      P.numDivisionID = D.numDivisionID
      LEFT JOIN
      CompanyInfo as CO
      ON
      D.numCompanyID = CO.numCompanyId
      LEFT JOIN
      UserMaster as U
      ON
      P.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      ShareRecord as SH
      ON
      SH.numRecordID = P.numProId AND SH.numModuleID = 5
      WHERE
				--P.bintModifiedDate>=Cast(@vcStartDate as date) AND 
      P.numDomainID = v_numDomainId AND P.numProId != 0 AND
      P.numCreatedBy != v_numUserId AND (P.numAssignedTo = v_numUserId OR P.numRecOwner = v_numUserId OR P.numIntPrjMgr = v_numUserId OR SH.numAssignedTo = v_numUserId) AND P.numProId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(7) AND numDomainID = v_numDomainId);
   end if;

--Email Module Alert
   IF(v_chrAction = 'EV') then
	
      open SWV_RefCur for
      SELECT COUNT(H.numEmailHstrID)
      FROM
      View_EmailAlert as H
      LEFT JOIN
      UserMaster as U
      ON
      H.numUserCntId = U.numUserDetailId
      LEFT JOIN
      EmailMaster as T
      ON(SELECT  Id from  SplitIds(H.vcTo,'$^$') LIMIT 1) = T.numEmailId
      LEFT JOIN
      UserMaster as UT
      ON
      T.numContactID = UT.numUserDetailId
      WHERE
				--H.bintCreatedOn>=Cast(@vcStartDate as date) AND 
      H.numDomainID = v_numDomainId AND H.numEmailHstrID != 0 AND
      H.numUserCntId != v_numUserId AND T.numContactID = v_numUserId AND H.numEmailHstrID NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(8) AND numDomainID = v_numDomainId);
   end if;
	--Tickler Module Alert
   IF(v_chrAction = 'TV') then
	
      open SWV_RefCur for
      SELECT COUNT(C.numCommId)
      FROM
      View_TicklerAlert as C
      LEFT JOIN
      DivisionMaster as D
      ON
      C.numDivisionID = D.numDivisionID
      LEFT JOIN
      CompanyInfo as CO
      ON
      D.numCompanyID = CO.numCompanyId
      LEFT JOIN
      UserMaster as U
      ON
      C.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      AdditionalContactsInformation as AD
      ON
      C.numContactId = AD.numContactId
      WHERE
				--C.dtModifiedDate>=Cast(@vcStartDate as date) AND 
      C.numDomainID = v_numDomainId AND C.numCommId != 0 AND
      C.numCreatedBy != v_numUserId AND C.numAssignedBy != v_numUserId AND C.numAssign = v_numUserId  AND C.numCommId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(9) AND numDomainID = v_numDomainId);
   end if;
	--Process Module Alert 
   IF(v_chrAction = 'PPV') then
	
      open SWV_RefCur for
      SELECT COUNT(S.numStageDetailsId)
      FROM
      View_ProcessAlert as S
      LEFT JOIN
      UserMaster as U
      ON
      S.numCreatedBy = U.numUserDetailId
      LEFT JOIN
      OpportunityMaster as O
      ON
      O.numOppId = S.numOppID
      LEFT JOIN
      ProjectsMaster as P
      ON
      P.numProId = S.numProjectID
      WHERE
				--S.bintModifiedDate>=Cast(@vcStartDate as date) AND 
      S.numDomainID = v_numDomainId AND S.numStageDetailsId != 0 AND
				(O.numassignedto = v_numUserId OR O.numrecowner = v_numUserId OR P.numAssignedTo = v_numUserId OR P.numRecOwner = v_numUserId OR P.numIntPrjMgr = v_numUserId)
      AND (O.numAssignedBy != v_numUserId OR P.numAssignedby != v_numUserId OR O.numCreatedBy != v_numUserId OR P.numCreatedBy != v_numUserId) AND
      S.numCreatedBy != v_numUserId AND  S.numStageDetailsId NOT IN(SELECT numRecordId
         FROM
         TrackNotification
         WHERE
         numUserId = v_numUserId and numModuleId IN(10) AND numDomainID = v_numDomainId);
   end if;
   RETURN;
END; $$;





