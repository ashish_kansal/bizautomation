DROP FUNCTION IF EXISTS USP_CompanyList1;

CREATE OR REPLACE FUNCTION USP_CompanyList1(v_numRelationType NUMERIC(9,0),                                                   
	v_numUserCntID NUMERIC(9,0),                                                      
	v_tintUserRightType SMALLINT,                                                          
	v_SortChar CHAR(1) DEFAULT '0',                                                     
	v_FirstName VARCHAR(100) DEFAULT '',                                                      
	v_LastName VARCHAR(100) DEFAULT '',                                                      
	v_CustName VARCHAR(100) DEFAULT '',                                                    
	v_CurrentPage INTEGER DEFAULT NULL,                                                    
	v_PageSize INTEGER DEFAULT NULL,                                                    
	v_columnName VARCHAR(50) DEFAULT NULL,                                                    
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                            
	v_tintSortOrder NUMERIC DEFAULT 0,                                             
	v_numProfile NUMERIC(9,0) DEFAULT 0,                                    
	v_numDomainID NUMERIC(9,0) DEFAULT 0,                            
	v_tintCRMType SMALLINT DEFAULT NULL,                            
	v_bitPartner BOOLEAN DEFAULT NULL,
	v_numFormID NUMERIC(9,0) DEFAULT NULL,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null
	, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                                
   v_lastRec  INTEGER;                                                                
   v_column  VARCHAR(50);                  
   v_lookbckTable  VARCHAR(50);                  
   v_strSql  TEXT;                                                  
   v_strExternalUser  TEXT DEFAULT '';
   v_tintOrder  SMALLINT;                                                        
   v_vcFieldName  VARCHAR(50);                                                        
   v_vcListItemType  VARCHAR(3);           
   v_numListID  NUMERIC(9,0);                                        
   v_vcAssociatedControlType  VARCHAR(20);                                                        
   v_vcDbColumnName  VARCHAR(200);                            
   v_WhereCondition  TEXT;                             
   v_vcLookBackTableName  VARCHAR(2000);                      
   v_bitCustom  BOOLEAN;  
   v_bitAllowEdit BOOLEAN;             
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;            
   v_vcColumnName  VARCHAR(500);                          
   v_Nocolumns  SMALLINT;                 
   v_DefaultNocolumns  SMALLINT;                      
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_ListRelID  NUMERIC(9,0);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'^LIKE?','ilike','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'^LIKE?','ilike','gi');

   IF POSITION('AD.numBillCountry' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry');
   ELSEIF POSITION('AD.numShipCountry' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry');
   ELSEIF POSITION('AD.vcBillCity' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity');
   ELSEIF POSITION('AD.vcShipCity' IN v_vcRegularSearchCriteria) > 0
   then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity');
   end if;
   IF POSITION('ADC.vcLastFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcLastFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=true))');
   end if;
   IF POSITION('ADC.vcNextFollowup' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=false))');
   end if;     

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                                
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                          
   v_column := v_columnName;                  
   v_lookbckTable := '';                                                               
                                                    
   v_strSql := 'with tblCompany as (SELECT  ';          
   if v_tintSortOrder = 7 or v_tintSortOrder = 8 then  
      v_strSql := coalesce(v_strSql,'') || ' ';
   end if;             
   if v_tintSortOrder = 5 then  
      v_strSql := coalesce(v_strSql,'') || 'ROW_NUMBER() OVER ( order by numCompanyRating desc) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, ';
   else
      v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_column,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, ';
   end if;                                                       
   v_strSql := coalesce(v_strSql,'') || '  DM.numDivisionID                                          
    FROM  CompanyInfo CMP                                 
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID 
	left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND COALESCE(ADC.bitPrimaryContact,false)=true
	left join VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE   ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId ';                                            
                               
   if v_tintSortOrder = 9 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactid=DM.numDivisionID ';
   end if;                                            
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=true and bitDeleted=false 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;                          
   v_strSql := coalesce(v_strSql,'') || ' ##JOIN##                                                     
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus 
left Join AddressDetails AD on AD.numRecordId= DM.numDivisionID and AD.tintAddressOf=2 and AD.tintAddressType=1 AND AD.bitIsPrimary=true and AD.numDomainID= DM.numDomainID                                                   
  WHERE CMP.numDomainID=  DM.numDomainID                                                    
    AND DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                                                    
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                                     
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                                                    
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                               
   IF v_numRelationType <> cast(NULLIF('0','') as NUMERIC(9,0)) then
	
      IF v_numRelationType = 47 then
		
         v_strSql := coalesce(v_strSql,'') || ' AND (CMP.numCompanyType = 47 OR (CMP.numCompanyType=46 AND EXISTS (SELECT numOppID FROM OpportunityMaster OM WHERE OM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 2 AND OM.tintOppStatus = 1)))';
      ELSE
         v_strSql := coalesce(v_strSql,'') || ' AND CMP.numCompanyType = ' || SUBSTR(CAST(v_numRelationType AS VARCHAR(15)),1,15) || '';
      end if;
   end if;

                                                 
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CMP.vcCompanyName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;

   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,'::VARCHAR AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))');
                                           
   IF v_tintUserRightType = 1 then
      v_strSql := CONCAT(v_strSql,' AND ((DM.numRecOwner = ',v_numUserCntID,')',' OR ',v_strExternalUser,
      (CASE WHEN v_bitPartner = true THEN ' OR ( CA.bitShareportal=true and CA.bitDeleted=false))' ELSE ')' END));
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
   end if;                     
                                                                       
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                                                                       
                    
--+ ' ORDER BY DM.bintCreateddate desc '                                                              
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numStatusID=2 ';
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND DM.numStatusID=3 ';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND DM.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and DM.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 8
   then  
      v_strSql := coalesce(v_strSql,'') || ' and DM.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;          
           
--if (@tintSortOrder=7 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X   '                                                     
--else if (@tintSortOrder=8 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X  '                                                               
--else 
   if v_tintSortOrder = 9 then  
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and cType=''O''';
   end if;              
                                       
   if v_numProfile > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  vcProfile=' || SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
   end if;                                            
   if v_tintCRMType > 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  DM.tintCRMType=' || SUBSTR(CAST(v_tintCRMType AS VARCHAR(1)),1,1);
   end if;                                              
                                                  
   IF v_vcRegularSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if; 
   IF v_vcCustomSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;
                                                  
                                                    
   v_strSql := coalesce(v_strSql,'') || ')';                      
   
                                                                                 
   v_tintOrder := 0;                                                        
   v_WhereCondition := '';                       
                         
   v_Nocolumns := 0;  

   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_numRelationType
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_numRelationType) TotalRows;

                    
   v_DefaultNocolumns :=  v_Nocolumns;  
                       
   while v_DefaultNocolumns > 0 LOOP
      v_DefaultNocolumns := v_DefaultNocolumns -1;
   END LOOP;              
   

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );
    


   v_strSql := coalesce(v_strSql,'') || ' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber,ADC.vcFirstName||'' ''||ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail';                       

--Check if Master Form Configuration is created by administrator if NoColumns=0
   IF v_Nocolumns = 0 then

      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND numRelCntType = v_numRelationType AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
	
         v_IsMasterConfAvailable := true;
      end if;
   end if;


--If MasterConfiguration is available then load it otherwise load default columns
   IF v_IsMasterConfAvailable = true then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      v_numFormID,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numRelationType,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormID AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numRelationType AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      v_numFormID,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numRelationType,1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormID AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numRelationType AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormID AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numRelationType AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormID AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numRelationType AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      if v_Nocolumns = 0 then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         select v_numFormID,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,v_numRelationType,1,false,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = v_numFormID and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM View_DynamicColumns
      where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_numRelationType
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      from View_DynamicCustomColumns
      where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = v_numRelationType
      AND coalesce(bitCustom,false) = true
      order by tintOrder asc;
   end if;                                                  
                        
--    set @DefaultNocolumns=  @Nocolumns  
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            

                                                    
   while v_tintOrder > 0 LOOP
      RAISE NOTICE '%',v_vcFieldName;
      if v_bitCustom = false then
         if v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         ELSEIF v_vcLookBackTableName = 'DivisionMaster'
         then
            v_Prefix := 'DM.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         if v_vcDbColumnName = 'vcLastFollowup' then

            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcNextFollowup'
         then

            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN ADC.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'Label' and v_vcDbColumnName = 'vcLastSalesOrderDate'
         then

            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON TRUE ';
            v_strSql := coalesce(v_strSql,'') || ',' || CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate,',v_numDomainID,
            '),'''')') || ' "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
        
            IF v_vcDbColumnName = 'numDefaultShippingServiceID' then
	
               v_strSql := coalesce(v_strSql,'') || ' ,COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR COALESCE(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numPartenerSource'
            then
	
               v_strSql := coalesce(v_strSql,'') || ' ,(DM.numPartenerSource) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strSql := coalesce(v_strSql,'') || ' ,(SELECT D.vcPartnerCode || ''-'' || C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource';
            ELSEIF v_vcListItemType = 'LI'
            then
     
               IF v_numListID = 40 then
		 
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF v_vcDbColumnName = 'numShipCountry' then
			
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry ';
                  ELSE
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry ';
                  end if;
               ELSEIF v_numListID = 5
               then
		
                  v_strSql := coalesce(v_strSql,'') || ',(CASE WHEN CMP.numCompanyType=46 THEN CONCAT(L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData END)' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'C'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcCampaignName' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CampaignMaster C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numCampaignId=DM.' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
     
               IF v_vcDbColumnName = 'numShipState' then
		
                  v_strSql := coalesce(v_strSql,'') || ',ShipState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=true and AD3.numDomainID= DM.numDomainID '
                  || ' left Join State ShipState on ShipState.numStateID=AD3.numState ';
               ELSEIF v_vcDbColumnName = 'numBillState'
               then
		
                  v_strSql := coalesce(v_strSql,'') || ',BillState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=true and AD4.numDomainID= DM.numDomainID '
                  || ' left Join State BillState on BillState.numStateID=AD4.numState ';
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
    
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'SYS'
            then
	
               v_strSql := coalesce(v_strSql,'') || ',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
 
            v_strSql := coalesce(v_strSql,'') || ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE)= CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE)= CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'TextBox' AND v_vcDbColumnName = 'vcBillCity'
         then

            v_strSql := coalesce(v_strSql,'') || ',AD5.vcCity' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=true and AD5.numDomainID= DM.numDomainID ';
         ELSEIF v_vcAssociatedControlType = 'TextBox' AND   v_vcDbColumnName = 'vcCompactContactDetails'
         then

            v_strSql := coalesce(v_strSql,'') || ' ,'''' AS vcCompactContactDetails';
         ELSEIF v_vcAssociatedControlType = 'TextBox' AND  v_vcDbColumnName = 'vcShipCity'
         then

            v_strSql := coalesce(v_strSql,'') || ',AD6.vcCity' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=true and AD6.numDomainID= DM.numDomainID ';
         ELSEIF v_vcAssociatedControlType = 'TextBox'  OR v_vcAssociatedControlType = 'Label'
         then

            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when
            v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
         else
            v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then

         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master where  CFW_Fld_Master.Fld_id = v_numFieldId;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Dm.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
                 
                 
-- OLD    set @strSql= @strSql+',case when COALESCE(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when COALESCE(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   '+ @vcFieldName+'~'+ @vcDbColumnName+''               
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then 0 when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then 1 end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Dm.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Dm.numDivisionId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=(CASE WHEN isnumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value ELSE ''0'' END)::NUMERIC ';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
	
            v_strSql := coalesce(v_strSql,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'',''))),'''')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=DM.numDivisionId ';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                             

---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 COALESCE( (CASE WHEN 
				COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) 
				= COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),''0'')



	ELSE '''' 
	END)) as FollowupFlag ';

   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) AS varchar)

			|| ''/'' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)AS varchar) || '')''
			|| ''<a onclick=openDrip('' || (cast(COALESCE((SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactID AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true AND ADC.bitPrimaryContact = true ),0)AS varchar)) || '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr ';

	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
   v_strSql := coalesce(v_strSql,'') || ' ,TotalRowCount,
	COALESCE(VIE.Total,0) as TotalEmail,
	COALESCE(VOA.OpenActionItemCount,0) as TotalActionItem
  From CompanyInfo CMP                                                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                                      
    left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
    left join VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE   ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId
   ' || coalesce(v_WhereCondition,'') ||
   ' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                                          
  WHERE (COALESCE(ADC.bitPrimaryContact,false)=true OR ADC.numContactID IS NULL) and 
  CMP.numDomainID=  DM.numDomainID and DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || '
  and RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);     
  
  

   v_strSql := REPLACE(v_strSql,'##JOIN##',v_WhereCondition);              

   v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_column,'') || ' ' || coalesce(v_columnSortOrder,'') || ' ';                                
   RAISE NOTICE '%',v_strSql;                              
                      
   OPEN SWV_RefCur FOR EXECUTE v_strSql; 


   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;


   RETURN;
END; $$;


