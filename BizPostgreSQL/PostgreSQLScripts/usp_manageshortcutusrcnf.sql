-- Stored procedure definition script USP_ManageShortCutUsrCnf for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShortCutUsrCnf(v_numGroupID NUMERIC(9,0) DEFAULT 0,              
v_numDomainId NUMERIC(9,0) DEFAULT 0,              
v_numContactId NUMERIC(9,0) DEFAULT 0,    
v_strFav TEXT DEFAULT '',  
v_numTabId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql              
   
------------------------------------------------------------------------------------------    
--For Links    
------------------------------------------------------------------------------------------              
--IF (select count(*) from ShortCutUsrCnf where numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID and numcontactId= @numContactId ) > 0    
    
 --begin     
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete FROM ShortCutUsrCnf where numTabId = v_numTabId and numDomainID = v_numDomainId and numGroupID = v_numGroupID  and numContactId = v_numContactId;  
           
   insert into ShortCutUsrCnf(numGroupID,numDomainID,numContactId,numTabId,numOrder,numLinkid,bitInitialPage,tintLinkType,bitFavourite)
   select v_numGroupID,v_numDomainId,v_numContactId,v_numTabId,
    X.numOrder,X.numLinkid,X.bitInitialPage,X.tintLinkType,X.bitFavourite
	 from
	  XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFav AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numOrder NUMERIC PATH 'numOrder',
			numLinkId NUMERIC PATH 'numLinkId',
			bitInitialPage BOOLEAN PATH 'bitInitialPage',
			tintLinkType SMALLINT PATH 'tintLinkType',
			bitFavourite BOOLEAN PATH 'bitFavourite'
	) X;         
                 

   RETURN;
END; $$;              



