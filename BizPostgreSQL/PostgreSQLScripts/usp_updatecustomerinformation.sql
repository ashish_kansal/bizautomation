-- Stored procedure definition script USP_UpdateCustomerInformation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
/* TODO: Use USP_SelectiveUpdate for following */
CREATE OR REPLACE FUNCTION USP_UpdateCustomerInformation(v_numDomainID NUMERIC,
    v_numContactID NUMERIC,
    v_vcFirstName VARCHAR(50),
    v_vcLastName VARCHAR(50),
    v_vcEmail VARCHAR(50),
    v_numPhone VARCHAR(15))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  AdditionalContactsInformation
   SET     vcFirstName = v_vcFirstName,vcLastname = v_vcLastName,vcEmail = v_vcEmail,
   numPhone = v_numPhone
   WHERE   numContactId = v_numContactID
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;



