-- Stored procedure definition script USP_Journal_CompanyName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Journal_CompanyName(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numDivisionId  NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select CI.vcCompanyName as "vcCompanyName" From CompanyInfo CI
   join DivisionMaster DM on  CI.numCompanyId = DM.numCompanyID
   Where CI.numDomainID = v_numDomainID And DM.numDivisionID = v_numDivisionId;
END; $$;












