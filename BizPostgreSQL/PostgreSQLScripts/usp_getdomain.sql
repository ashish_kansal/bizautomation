CREATE OR REPLACE FUNCTION USP_GetDomain(v_numDomainID INTEGER,
      v_numType INTEGER,
      v_vcDomainCode VARCHAR(50) DEFAULT '',
      v_numSubscriberID INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numType = 1 then
            
      open SWV_RefCur for
      SELECT  numDomainId,
                        vcDomainName,
                        numParentDomainID,
                        vcDomainCode
      FROM    Domain
      WHERE   numDomainId = v_numDomainID
      AND numSubscriberID = v_numSubscriberID;
   ELSEIF v_numType = 2
   then
                
      open SWV_RefCur for
      SELECT  DN.numDomainId,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                            DP.vcDomainName AS ParentDomainName
      FROM    Domain DN INNER JOIN Domain DP ON DP.numDomainId = DN.numParentDomainID
      WHERE
      DN.vcDomainCode ilike coalesce(v_vcDomainCode,'') || '%'
      AND DN.numSubscriberID = v_numSubscriberID
      AND DP.numSubscriberID = v_numSubscriberID
      ORDER BY DN.vcDomainCode;
   ELSEIF v_numType = 3
   then
                
      open SWV_RefCur for
      SELECT  DN.numDomainId,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                           '' AS ParentDomainName
      FROM    Domain DN;
   end if;
   RETURN;
END; $$;



