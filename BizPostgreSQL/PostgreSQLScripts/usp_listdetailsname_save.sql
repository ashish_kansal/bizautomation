-- Stored procedure definition script USP_ListDetailsName_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ListDetailsName_Save(v_numDomainID NUMERIC(18,0),
v_numListID NUMERIC(18,0),
v_numListItemID NUMERIC(18,0),
v_vcName VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM ListDetailsName WHERE numDomainID = v_numDomainID AND numListID = v_numListID AND numListItemID = v_numListItemID) then
	
      UPDATE
      ListDetailsName
      SET
      vcName = coalesce(v_vcName,'')
      WHERE
      numDomainID = v_numDomainID
      AND numListID = v_numListID
      AND numListItemID = v_numListItemID;
   ELSE
      INSERT INTO ListDetailsName(numDomainID
			,numListID
			,numListItemID
			,vcName)
		VALUES(v_numDomainID
			,v_numListID
			,v_numListItemID
			,coalesce(v_vcName, ''));
   end if;
   RETURN;
END; $$;


