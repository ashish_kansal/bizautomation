-- Stored procedure definition script USP_CompanyInfo_SearchWithClass for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 26 October 2015
-- Description:	Gets company info based on class
-- =============================================
CREATE OR REPLACE FUNCTION USP_CompanyInfo_SearchWithClass(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_isStartWithSearch BOOLEAN DEFAULT false,
	v_searchText TEXT DEFAULT NULL,
	v_numAccountClass NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_tintDefaultClassType  SMALLINT; 
   v_CustomerSelectFields  TEXT;
   v_CustomerSelectCustomFields  TEXT;
   v_ProspectsRights  SMALLINT;                    
   v_accountsRights  SMALLINT;                   
   v_leadsRights  SMALLINT; 

   v_strSQL  TEXT;    

   v_searchWhereCondition  TEXT;
   v_singleValueSearchFields  TEXT;   
   v_multiValueSearchFields  TEXT; 
   v_customSearchFields  TEXT;
   v_finalSearchText  TEXT;
BEGIN


   select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainID;


   v_searchText := REPLACE(v_searchText,'''','''''');

   v_CustomerSelectFields := '';
   v_CustomerSelectCustomFields := '';
               
   SELECT 
   MAX(GA.intViewAllowed) AS intViewAllowed INTO v_ProspectsRights FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE
   PM.numPageID = 2
   AND PM.numModuleID = 3
   AND UM.numUserId =(SELECT numUserId
      FROM   UserMaster
      WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                        
                    
   SELECT 
   MAX(GA.intViewAllowed) AS intViewAllowed INTO v_accountsRights FROM  UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE   PM.numPageID = 2
   AND PM.numModuleID = 4
   AND UM.numUserId =(SELECT numUserId
      FROM   UserMaster
      WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                
              
   SELECT 
   MAX(GA.intViewAllowed) AS intViewAllowed INTO v_leadsRights FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
   AND PM.numModuleID = 2
   AND UM.numUserId =(SELECT numUserId
      FROM   UserMaster
      WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                   
                    
-- START: Get organization display fields
   DROP TABLE IF EXISTS tt_TEMPORGDISPLAYFIELDS;
   CREATE TEMPORARY TABLE tt_TEMPORGDISPLAYFIELDS AS
      SELECT * FROM(SELECT
      numFieldID,
		vcDbColumnName,
		coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 96 AND numDomainID = v_numDomainID AND
      tintPageType = 1 AND bitCustom = false  AND coalesce(bitSettingField,false) = true AND numRelCntType = 0 AND vcDbColumnName <> 'numCompanyID'
      UNION
      SELECT
      numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
      FROM
      View_DynamicCustomColumns
      WHERE
      Grp_id = 1 AND numFormId = 96 AND numDomainID = v_numDomainID AND
      tintPageType = 1 AND bitCustom = true AND numRelCntType = 0) X;

   IF EXISTS(SELECT * FROM tt_TEMPORGDISPLAYFIELDS) then

      select   COALESCE(v_CustomerSelectFields,'') || SUBSTR(CAST(vcDbColumnName AS VARCHAR(100)),1,100) || ',' INTO v_CustomerSelectFields FROM
      tt_TEMPORGDISPLAYFIELDS WHERE
      Custom = 0;
		
	-- Removes last , from string
      IF OCTET_LENGTH(v_CustomerSelectFields) > 0 then
         v_CustomerSelectFields := SUBSTR(v_CustomerSelectFields,1,LENGTH(v_CustomerSelectFields) -1);
      end if;
      IF EXISTS(SELECT * FROM tt_TEMPORGDISPLAYFIELDS WHERE Custom = 1) then
	
         select   COALESCE(v_CustomerSelectCustomFields,'') || 'fn_GetCustFldOrganization(' || SUBSTR(CAST(numFieldId AS VARCHAR(100)),1,100) || ',1::SMALLINT,TEMP1.numCompanyID) AS [' || SUBSTR(CAST(vcDbColumnName AS VARCHAR(100)),1,100) || '],' INTO v_CustomerSelectCustomFields FROM
         tt_TEMPORGDISPLAYFIELDS WHERE
         Custom = 1;
      end if;

	-- Removes last , from string
      IF OCTET_LENGTH(v_CustomerSelectCustomFields) > 0 then
         v_CustomerSelectCustomFields := SUBSTR(v_CustomerSelectCustomFields,1,LENGTH(v_CustomerSelectCustomFields) -1);
      end if;
   end if;

-- END: Get organization display fields

-- START: Generate default select statement 
                    
   v_strSQL := 'SELECT numDivisionID, numCompanyID, vcCompanyName' ||
   CASE v_CustomerSelectFields
   WHEN '' THEN ''
   ELSE ',' || coalesce(v_CustomerSelectFields,'')
   END
   ||
   CASE v_CustomerSelectCustomFields
   WHEN '' THEN ''
   ELSE ',' || coalesce(v_CustomerSelectCustomFields,'')
   END
   || ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = true) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	COALESCE(CMP.txtComments,'''') as txtComments, 
	COALESCE(CMP.vcWebSite,'''') vcWebSite, 
	COALESCE(AD1.vcStreet,'''') as vcBillStreet, 
	COALESCE(AD1.vcCity,'''') as vcBillCity, 
	COALESCE(fn_GetState(AD1.numState),'''') as numBillState,
	COALESCE(AD1.vcPostalCode,'''') as vcBillPostCode,
	COALESCE(fn_GetListName(AD1.numCountry,false),'''') as numBillCountry,
	COALESCE(AD2.vcStreet,'''') as vcShipStreet,
	COALESCE(AD2.vcCity,'''') as vcShipCity,
	COALESCE(fn_GetState(AD2.numState),'''')  as numShipState,
	COALESCE(AD2.vcPostalCode,'''') as vcShipPostCode, 
	COALESCE(fn_GetListName(AD2.numCountry,false),'''') as numShipCountry,
	COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	CompanyInfo CMP
INNER JOIN
	DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true
WHERE 
	CMP.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) ||
   ' AND 1 = (CASE WHEN ' || CAST(coalesce(v_tintDefaultClassType,0) AS VARCHAR(20)) || ' = 2 THEN (CASE WHEN COALESCE(d.numAccountClassID,0) = ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(20)),1,20) || ' THEN 1 ELSE 0 END) ELSE 1 END) '  ||
   ' AND d.tintCRMType<>0 and d.tintCRMType=1' ||
   CASE v_ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END ||
   CASE v_ProspectsRights WHEN 1 THEN ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ELSE '' END ||
   CASE v_ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)' ELSE '' END ||
   '
UNION
SELECT
	CMP.numCompanyId
FROM
	CompanyInfo CMP
INNER JOIN
	DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true
WHERE 
	CMP.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
   || ' AND 1 = (CASE WHEN ' || CAST(coalesce(v_tintDefaultClassType,0) AS VARCHAR(20)) || ' = 2 THEN (CASE WHEN COALESCE(d.numAccountClassID,0) = ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(20)),1,20) || ' THEN 1 ELSE 0 END) ELSE 1 END) '  ||
   ' AND d.tintCRMType<>0 and d.tintCRMType=2' ||
   CASE v_accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END ||
   CASE v_accountsRights WHEN 1 THEN ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ELSE '' END ||
   CASE v_accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)' ELSE '' END ||
   '
UNION
SELECT
	CMP.numCompanyId
FROM
	CompanyInfo CMP
INNER JOIN
	DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true
WHERE 
	CMP.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
   || ' AND 1 = (CASE WHEN ' || CAST(coalesce(v_tintDefaultClassType,0) AS VARCHAR(20)) || ' = 2 THEN (CASE WHEN COALESCE(d.numAccountClassID,0) = ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(20)),1,20) || ' THEN 1 ELSE 0 END) ELSE 1 END) '  ||
   ' AND d.tintCRMType=0' ||
   CASE v_leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END ||
   CASE v_leadsRights WHEN 1 THEN ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ELSE '' END ||
   CASE v_leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)' ELSE '' END ||
   ') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = true
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = true
						)) AS TEMP1';
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

   v_searchWhereCondition := '';
   v_singleValueSearchFields := '';   
   v_multiValueSearchFields := '';
   v_customSearchFields := '';
                

   DROP TABLE IF EXISTS tt_TEMPORGSEARCH;
   CREATE TEMPORARY TABLE tt_TEMPORGSEARCH AS
      SELECT * FROM(SELECT
      numFieldID,
		vcDbColumnName,
		coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 97 AND numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND
      tintPageType = 1 AND bitCustom = false  AND coalesce(bitSettingField,false) = true AND numRelCntType = 0
      UNION
      SELECT
      numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
      FROM
      View_DynamicCustomColumns
      WHERE
      Grp_id = 1 AND numFormId = 97 AND numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID AND
      tintPageType = 1 AND bitCustom = true AND numRelCntType = 0) Y;

   IF v_searchText != '' then

      IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH) > 0 then
	
		-- START: Remove white spaces from search values
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE
         (
            vcValue TEXT
         );
         WHILE LENGTH(v_searchText) > 0 LOOP
            v_finalSearchText := SUBSTR(v_searchText,1,coalesce(NULLIF(POSITION(',' IN v_searchText) -1,-1),LENGTH(v_searchText)));
            v_searchText := SUBSTR(v_searchText,coalesce(NULLIF(POSITION(',' IN v_searchText),0),LENGTH(v_searchText))+1,LENGTH(v_searchText));
            INSERT INTO tt_TEMPTABLE(vcValue) VALUES(v_finalSearchText);
         END LOOP;
		
		--Removes white spaces
         UPDATE
         tt_TEMPTABLE
         SET
         vcValue = LTRIM(RTRIM(vcValue));
	
		--Converting table to comma seperated string
         select   COALESCE(v_searchText,'') ||  CAST(vcValue AS TEXT) || ',' INTO v_searchText FROM tt_TEMPTABLE;
	
		--Remove last comma from final search string
         IF OCTET_LENGTH(v_searchText) > 0 then
            v_searchText := SUBSTR(v_searchText,1,LENGTH(v_searchText) -1);
         end if;
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
         select   COALESCE(v_singleValueSearchFields,'') || '(' || SUBSTR(CAST(vcDbColumnName AS VARCHAR(50)),1,50) || ' ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END || REPLACE(v_searchText,',','%'' OR ' || vcDbColumnName || ' ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%'')' || ' OR ' INTO v_singleValueSearchFields FROM
         tt_TEMPORGSEARCH WHERE
         numFieldId NOT IN(51,52,96,148,127) AND
         Custom = 0;
			
		
		-- Removes last OR from string
         IF OCTET_LENGTH(v_singleValueSearchFields) > 0 then
            v_singleValueSearchFields := SUBSTR(v_singleValueSearchFields,1,LENGTH(v_singleValueSearchFields) -3);
         end if;
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH WHERE numFieldId = 51) > 0 then
            v_multiValueSearchFields := 'EXISTS (SELECT ACI.numContactID FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR ACI.vcFirstName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
         end if;	
		
		-- 2. CONTACT LAST NAME
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH WHERE numFieldId = 52) > 0 then
		
            IF v_multiValueSearchFields = '' then
               v_multiValueSearchFields := 'EXISTS (SELECT ACI.numContactID FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR ACI.vcLastName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            ELSE
               v_multiValueSearchFields := coalesce(v_multiValueSearchFields,'') || ' OR EXISTS (SELECT numContactID FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR ACI.vcLastName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            end if;
         end if;	
		
		-- 3. ORDER NAME
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH WHERE numFieldId = 96) > 0 then
		
            IF v_multiValueSearchFields = '' then
               v_multiValueSearchFields := 'EXISTS (SELECT OM.numOppID FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR OM.vcPOppName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            ELSE
               v_multiValueSearchFields := coalesce(v_multiValueSearchFields,'') || ' OR EXISTS (SELECT OM.numOppID FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR OM.vcPOppName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            end if;
         end if;	
		
		-- 4.PROJECT NAME
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH WHERE numFieldId = 148) > 0 then
		
            IF v_multiValueSearchFields = '' then
               v_multiValueSearchFields := 'EXISTS (SELECT PM.numProId FROM ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR PM.vcProjectName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            ELSE
               v_multiValueSearchFields := coalesce(v_multiValueSearchFields,'') || ' OR EXISTS (SELECT PM.numProId FROM ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR PM.vcProjectName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%''))';
            end if;
         end if;	
		
		-- 5. CASE NAME
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH WHERE numFieldId = 127) > 0 then
		
            IF v_multiValueSearchFields = '' then
               v_multiValueSearchFields := 'EXISTS (SELECT C.numCaseId FROM Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR C.textSubject ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%'')) > 0';
            ELSE
               v_multiValueSearchFields := coalesce(v_multiValueSearchFields,'') || ' OR EXISTS (SELECT C.numCaseId FROM Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR C.textSubject ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%'')) > 0';
            end if;
         end if;	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
         IF EXISTS(SELECT * FROM tt_TEMPORGSEARCH WHERE Custom = 1) then
		
            select   COALESCE(v_customSearchFields,'') || '( fn_GetCustFldOrganization(' || SUBSTR(CAST(numFieldId AS VARCHAR(20)),1,20) || ',1::SMALLINT,TEMP1.numCompanyID) ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END || REPLACE(v_searchText,',','%'' OR fn_GetCustFldOrganization(' || SUBSTR(CAST(numFieldId AS VARCHAR(20)),1,20) || ',1,TEMP1.numCompanyID) ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%'')' || ' OR ' INTO v_customSearchFields FROM
            tt_TEMPORGSEARCH WHERE
            Custom = 1;
         end if;
		
		-- Removes last OR from string
         IF OCTET_LENGTH(v_customSearchFields) > 0 then
            v_customSearchFields := SUBSTR(v_customSearchFields,1,LENGTH(v_customSearchFields) -3);
         end if;
		
		--END: Get custom search fields
		
         IF OCTET_LENGTH(v_singleValueSearchFields) > 0 then
            v_searchWhereCondition := v_singleValueSearchFields;
         end if;
         IF OCTET_LENGTH(v_multiValueSearchFields) > 0 then
            IF OCTET_LENGTH(v_searchWhereCondition) > 0 then
               v_searchWhereCondition := coalesce(v_searchWhereCondition,'') || ' OR ' || coalesce(v_multiValueSearchFields,'');
            ELSE
               v_searchWhereCondition := v_multiValueSearchFields;
            end if;
         end if;
         IF OCTET_LENGTH(v_customSearchFields) > 0 then
            IF 	OCTET_LENGTH(v_searchWhereCondition) = 0 then
               v_searchWhereCondition := v_customSearchFields;
            ELSE
               v_searchWhereCondition := coalesce(v_searchWhereCondition,'') || ' OR ' || coalesce(v_customSearchFields,'');
            end if;
         end if;
      ELSE
         v_searchWhereCondition := '(vcCompanyName ILIKE ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END ||  REPLACE(v_searchText,',','%'' OR vcCompanyName ILIKE  ' || CASE v_isStartWithSearch WHEN true THEN '''' ELSE '''%' END) || '%'')';
      end if;
   end if;

-- END: Generate user defined search fields condition

   IF OCTET_LENGTH(v_searchWhereCondition) > 0 then
      v_strSQL := coalesce(v_strSQL,'') || ' WHERE ' || coalesce(v_searchWhereCondition,'');
   end if; 
    
 
   v_strSQL := coalesce(v_strSQL,'') || ' ORDER BY vcCompanyname';    

--SELECT @strSQL
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


