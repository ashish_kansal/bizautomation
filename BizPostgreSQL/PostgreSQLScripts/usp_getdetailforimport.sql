CREATE OR REPLACE FUNCTION USP_GetDetailForImport
(
	v_numDomainID NUMERIC(18,0),
    v_numMasterID NUMERIC(9,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSQL  TEXT;
	v_vcColumns  TEXT;
BEGIN
	IF coalesce(v_numMasterID,0) = 31 then  
		v_vcColumns := COALESCE((SELECT	
									string_agg((vcLookBackTableName || '.' || vcDbColumnName || ' AS "' || MAPPING.vcFieldName || '"'),',' ORDER BY MAPPING."order")
								FROM
									DycFormField_Mapping MAPPING
								JOIN
									DycFieldMaster MAST 
								ON 
									MAPPING.numFieldID = MAST.numFieldID 
								WHERE 
									numFormID = coalesce(v_numMasterID,0)),'');
		
		v_vcColumns := REPLACE(v_vcColumns,'ItemDetails.numItemDetailID AS "Item Detail ID",','');
		v_vcColumns := REPLACE(v_vcColumns,'ItemDetails.numItemKitID AS "Item Name",','');
		v_vcColumns := REPLACE(v_vcColumns,'ItemDetails.numChildItemID AS "Child Item Name",','');
		v_vcColumns := REPLACE(v_vcColumns,'ItemDetails.numWareHouseItemID','');
		v_vcColumns := REPLACE(v_vcColumns,'Warehouses.numWareHouseID AS "Warehouse Name",','');
		v_vcColumns := REPLACE(v_vcColumns,'ItemDetails.numUOMId','(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId)');

		v_strSQL := ' SELECT 
							numItemDetailID AS "Item Detail ID"
							,Item.numItemCode AS "Item Code"
							,Item.vcItemName AS "Item Name"
							,Item1.vcItemName AS "Child Item Name"
							,' || coalesce(v_vcColumns,'') || ' 
						FROM 
							ItemDetails 
						LEFT JOIN Item ON ItemDetails.numItemKitID = Item.numItemCode
						LEFT JOIN Item Item1 ON ItemDetails.numChildItemID = Item1.numItemCode
						WHERE 
							Item.numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
							AND coalesce(Item.bitAssembly,false) = true 
							AND coalesce(Item.bitKitParent,false) = true 
						ORDER BY 
							Item.numItemCode ';

		RAISE NOTICE '%',v_strSQL;
		OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
	
	IF coalesce(v_numMasterID,0) = 54 then 
		v_vcColumns := COALESCE((SELECT	
									string_agg((vcLookBackTableName || '.' || vcDbColumnName || ' AS "' || MAPPING.vcFieldName || '"'),',' ORDER BY MAPPING."order")
								FROM
									DycFormField_Mapping MAPPING
								JOIN
									DycFieldMaster MAST 
								ON 
									MAPPING.numFieldID = MAST.numFieldID 
								WHERE 
									numFormID = coalesce(v_numMasterID,0)),'');   
				
		v_strSQL := 'SELECT 
						Item.vcItemName AS "Item Name"
						,' || coalesce(v_vcColumns,'') || ' 
					FROM 
						Item 
					LEFT JOIN 
						ItemImages 
					ON 
						Item.numItemCode = ItemImages.numItemCode AND Item.numDomainID = ItemImages.numDomainID
					WHERE Item.numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						ORDER BY Item.numItemCode';
		
		RAISE NOTICE '%',v_strSQL;
		
		OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
   RETURN;
END; $$;


