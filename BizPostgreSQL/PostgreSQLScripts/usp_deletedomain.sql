-- Stored procedure definition script USP_DeleteDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteDomain(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

	SWV_Label_value varchar(30);
	SWV_RowCount INTEGER;
BEGIN
   IF coalesce(v_numDomainID,0) = 0 then
	
      RAISE EXCEPTION 'Domain id required';
      RETURN;
   ELSEIF v_numDomainID = -255 OR v_numDomainID = -1
   then
	
      RAISE EXCEPTION 'Verry funny.. can''t delete system account';
      RETURN;
   ELSEIF v_numDomainID = 1
   then
	
      RAISE EXCEPTION 'Verry funny.. can''t delete yourself';
      RETURN;
   ELSEIF v_numDomainID = 72
   then
	
      RAISE EXCEPTION 'Verry funny.. can''t delete developers test account';
      RETURN;
   end if;

	/*Allow deletion only for suspended subscription*/
   IF exists(SELECT * FROM Subscribers WHERE bitActive = 1 AND numTargetDomainID = v_numDomainID) then
	
      RAISE EXCEPTION 'Can''t delete active subscription';
      RETURN;
   end if;

   BEGIN
	--BEGIN TRANSACTION
		
	--COMMIT
      SWV_Label_value := 'SWL_Start_Label';
      << SWL_Label >>
      LOOP
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         CASE
         WHEN SWV_Label_value = 'SWL_Start_Label' THEN
            DELETE FROM General_Journal_Details WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM General_Journal_Header WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityRecurring WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmbeddedCostItems WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmbeddedCost WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CaseOpportunities WHERE coalesce(numDomainid,0) = v_numDomainID;
            DELETE FROM CreditBalanceHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            
			DELETE FROM 
				ShippingReportItems SRI 
			using 
				ShippingBox SB  
			INNER JOIN 
				ShippingReport SR 
			ON 
				SB.numShippingReportID = SR.numShippingReportID 
			INNER JOIN
				OpportunityBizDocs OBD
			ON
				SR.numOppBizDocId = OBD.numOppBizDocsId
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppID = OM.numOppID
			WHERE 
				SRI.numBoxID = SB.numBoxID
				AND coalesce(OM.numDomainID,0) = v_numDomainID;

			DELETE FROM
				ShippingBox SB 
			using 
				ShippingReport SR 
			INNER JOIN
				OpportunityBizDocs OBD
			ON
				SR.numOppBizDocId = OBD.numOppBizDocsId
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppID = OM.numOppID
			WHERE 
				SB.numShippingReportID = SR.numShippingReportID 
				AND coalesce(OM.numDomainID,0) = v_numDomainID;

			DELETE FROM
				OpportunityBizDocItems OBDI 
			USING 
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppID = OM.numOppID
			WHERE 
				OBDI.numOppBizDocID = OBD.numOppBizDocsId
				AND coalesce(OM.numDomainID,0) = v_numDomainID;

            DELETE FROM OpportunityItemsReceievedLocationReturn OIRLR using ReturnItems RI INNER JOIN ReturnHeader RH ON RI.numReturnHeaderID=RH.numReturnHeaderID where OIRLR.numReturnItemID = RI.numReturnItemID AND RH.numDomainID = v_numDomainID;
            DELETE FROM OpportunityItemsReceievedLocationReturn OIRLR using ReturnHeader RH where OIRLR.numReturnID = RH.numReturnHeaderID AND RH.numDomainID = v_numDomainID;
            DELETE FROM OpportunityKitChildItems OKCI using OpportunityMaster OM where OKCI.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            
			DELETE FROM
				RecurrenceTransaction RT  
			USING 
				OpportunityBizDocs OBD 
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppID = OM.numOppID
			WHERE 
				RT.numRecurrOppBizDocID=OBD.numOppBizDocsId 
				AND coalesce(OM.numDomainID,0) = v_numDomainID;

			DELETE FROM
				OpportunityBizDocTaxItems OBTI  
			USING
				OpportunityBizDocs OBD 
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppID = OM.numOppID
			WHERE 
				OBTI.numOppBizDocID=OBD.numOppBizDocsId 
				AND coalesce(OM.numDomainID,0)=v_numDomainID;
			
			DELETE FROM RecurrenceTransaction RT using OpportunityBizDocs OBD where RT.numRecurrOppBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocTaxItems OBTI using OpportunityBizDocs OBD where OBTI.numOppBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM StageAccessDetail SAD using AdditionalContactsInformation ACI where SAD.numContactId = ACI.numContactId AND coalesce(ACI.numDomainID,0) = v_numDomainID;
            DELETE FROM RecurringTransactionReport RTR using OpportunityBizDocs OBD where RTR.numRecTranBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM RecurringTransactionReport RTR using OpportunityMaster OM where RTR.numRecTranOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM RecurrenceTransactionHistory RTH using OpportunityBizDocs OBD where RTH.numRecurrOppBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM RecurrenceTransactionHistory RTH using OpportunityMaster OM where RTH.numRecurrOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM ProjectTeam PT using AdditionalContactsInformation ACI where PT.numContactId = ACI.numContactId AND coalesce(ACI.numDomainID,0) = v_numDomainID;
            DELETE FROM ProjectTeamRights PTR using ProjectsMaster PM where PTR.numProId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ProjectProcessStageDetails PPSD using ProjectsMaster PM where PPSD.numProjectId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ProjectProcessStageDetails PPSD using OpportunityMaster OM where PPSD.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM PricingTable PT using PriceBookRules PBR where PT.numPriceRuleID = PBR.numPricRuleID AND coalesce(PBR.numDomainID,0) = v_numDomainID;
            DELETE FROM PricingTable PT using Item I where PT.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM PriceBookRuleDTL PBRD using PriceBookRules PBR where PBRD.numRuleID = PBR.numPricRuleID AND coalesce(PBR.numDomainID,0) = v_numDomainID;
            DELETE FROM PageMasterGroupSetting PMGS using AuthenticationGroupMaster AGM where PMGS.numGroupID = AGM.numGroupID AND coalesce(AGM.numDomainID,0) = v_numDomainID;
            DELETE FROM OrgOppAddressModificationHistory OA using UserMaster UM where OA.numModifiedBy = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityMasterTaxItems OMTI using OpportunityMaster OM where OMTI.numOppId = OM.numOppId AND coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocsPaymentDetails OBDPD using OpportunityBizDocsDetails OBD where OBDPD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId AND coalesce(OBD.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityItemLinking OIL using OpportunityMaster OM where OIL.numNewOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityItemsTaxItems OITI using OpportunityMaster OM where OITI.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityItemsTaxItems OITI using ReturnHeader RH where OITI.numReturnHeaderID = RH.numReturnHeaderID AND coalesce(RH.numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocKitChildItems OBDKCI using OpportunityBizDocs OBD where OBDKCI.numOppBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocKitItems OBDKI using OpportunityBizDocs OBD where OBDKI.numOppBizDocID = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM MassSalesOrderLog MSOL using OpportunityMaster OM where MSOL.numCreatedOrderID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM MassSalesOrderQueue MSOL using OpportunityMaster OM where MSOL.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM EmailHstrAttchDtls EHAD using EmailHistory EH where EHAD.numEmailHstrID = EH.numEmailHstrID AND coalesce(EH.numDomainID,0) = v_numDomainID;
            DELETE FROM EmailHStrToBCCAndCC EHCB using EmailHistory EH where EHCB.numEmailHstrID = EH.numEmailHstrID AND coalesce(EH.numDomainID,0) = v_numDomainID;
            DELETE FROM DocumentWorkflow DW using AdditionalContactsInformation ACI where DW.numContactID = ACI.numContactId AND coalesce(ACI.numDomainID,0) = v_numDomainID;
            DELETE FROM DivisionTaxTypes DTT using DivisionMaster DM where DTT.numDivisionID = DM.numDivisionID AND coalesce(DM.numDomainID,0) = v_numDomainID;
            DELETE FROM DivisionMasterShippingAccount DMSA using DivisionMaster DM where DMSA.numDivisionID = DM.numDivisionID AND coalesce(DM.numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocComissionPaymentDifference BCPD using BizDocComission BDC where BCPD.numComissionID = BDC.numComissionID AND coalesce(BDC.numDomainId,0) = v_numDomainID;
            DELETE FROM RecurrenceTransaction RT using OpportunityMaster OM where RT.numRecurrOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM BizActionDetails BAD using OpportunityBizDocs OBD where BAD.numOppBizDocsId = OBD.numOppBizDocsId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityAddress OA using OpportunityMaster OM where OA.numOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocs OBD using OpportunityMaster OM where OBD.numoppid = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityContact OC using OpportunityMaster OM where OC.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityDependency OD using OpportunityMaster OM where OD.numOpportunityId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityKitItems OKI using OpportunityMaster OM where OKI.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityLinking OL using OpportunityMaster OM where OL.numChildOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityLinking OL using OpportunityMaster OM where OL.numParentOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunitySubStageDetails OSSD using OpportunityMaster OM where OSSD.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM OppWarehouseSerializedItem OWSI using OpportunityMaster OM where OWSI.numOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM Returns R using OpportunityMaster OM where R.numOppID = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM ActivityAttendees AA using ActivityResource AR where AA.ActivityID = AR.ActivityID AND coalesce(R.numDomainId,0) = v_numDomainID;
            DELETE FROM ActivityResource AR using Resource R where AR.ResourceID = R.ResourceID AND coalesce(R.numDomainId,0) = v_numDomainID;
            DELETE FROM BroadcastErrorLog BEL using BroadCastDtls BD where BEL.numBroadcastDtlID = BD.numBroadCastDtlId AND coalesce(B.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Dtl CFD using CFW_Fld_Master CFM where CFD.numFieldId = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_FLD_Values CFV using CFW_Fld_Master CFM where CFV.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Attributes CFVA using CFW_Fld_Master CFM where CFVA.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_FLD_Values_Case CFVC using CFW_Fld_Master CFM where CFVC.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_FLD_Values_Cont CFVC using CFW_Fld_Master CFM where CFVC.fld_id = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_FLD_Values_Item CFVI using CFW_Fld_Master CFM where CFVI.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_FLD_Values_Item_EDI CFVIE using CFW_Fld_Master CFM where CFVIE.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Opp CFVO using CFW_Fld_Master CFM where CFVO.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Opp_EDI CFVOE using CFW_Fld_Master CFM where CFVOE.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_OppItems CFVOI using CFW_Fld_Master CFM where CFVOI.fld_id = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_OppItems_EDI CFVOIE using CFW_Fld_Master CFM where CFVOIE.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Product CFVP using CFW_Fld_Master CFM where CFVP.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Pro CFVP using CFW_Fld_Master CFM where CFVP.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Values_Serialized_Items CFVSI using CFW_Fld_Master CFM where CFVSI.Fld_ID = CFM.Fld_id AND coalesce(CFM.numDomainID,0) = v_numDomainID;
            DELETE FROM ChecksRecurringDetails CRD using CheckHeader CH where CRD.numCheckId = CH.numCheckHeaderID AND coalesce(CH.numDomainID,0) = v_numDomainID;
            DELETE FROM Comments C using StagePercentageDetails SPD where C.numStageID = SPD.numStageDetailsId AND coalesce(SPD.numdomainid,0) = v_numDomainID;
            DELETE FROM Comments C using Cases SPD where C.numCaseID = SPD.numCaseId AND coalesce(SPD.numDomainID,0) = v_numDomainID;
            DELETE FROM ConECampaignDTL CED using ECampaignDTLs ED where CED.numECampDTLID = ED.numECampDTLId AND coalesce(E.numDomainID,0) = v_numDomainID;
            DELETE FROM DepositeDetails DD using DepositMaster DM where DD.numDepositID = DM.numDepositId AND coalesce(DM.numDomainId,0) = v_numDomainID;
            DELETE FROM ECommerceItemClassification EIC using EcommerceRelationshipProfile ERP where EIC.numECommRelatiionshipProfileID = ERP.ID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityItems OI using OpportunityMaster OM where OI.numOppId = OM.numOppId AND coalesce(OM.numDomainId,0) = v_numDomainID;
            DELETE FROM ProjectsContacts PC using ProjectsMaster PM where PC.numProId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ProjectsDependency PD using ProjectsMaster PM where PD.numProId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ProjectsOpportunities PO using ProjectsMaster PM where PO.numProId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ProjectsSubStageDetails PSSD using ProjectsMaster PM where PSSD.numProId = PM.numProId AND coalesce(PM.numdomainId,0) = v_numDomainID;
            DELETE FROM ReturnItems RI using ReturnHeader RH where RI.numReturnHeaderID = RH.numReturnHeaderID AND coalesce(RH.numDomainID,0) = v_numDomainID;
            DELETE FROM StageDependency SD using StagePercentageDetails SPD where SD.numStageDetailId = SPD.numStageDetailsId AND coalesce(SPD.numdomainid,0) = v_numDomainID;
            DELETE FROM StageDependency SD using StagePercentageDetails SPD where SD.numDependantOnID = SPD.numStageDetailsId AND coalesce(SPD.numdomainid,0) = v_numDomainID;
            DELETE FROM StyleSheetDetails SSD using SitePages SP where SSD.numPageID = SP.numPageID AND coalesce(SP.numDomainID,0) = v_numDomainID;
            DELETE FROM StyleSheetDetails SSD using StyleSheets SS where SSD.numCssID = SS.numCssID AND coalesce(SS.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyRespondentsChild SRC using SurveyRespondentsMaster SRM where SRC.numRespondantID = SRM.numRespondantID AND coalesce(SRM.numDomainId,0) = v_numDomainID;
            DELETE FROM WareHouseItmsDTL WID using WareHouseItems WI where WID.numWareHouseItemID = WI.numWareHouseItemID AND coalesce(WI.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowActionUpdateFields WFAUF using WorkFlowActionList WFASL where WFAUF.numWFActionID = WFASL.numWFActionID AND coalesce(WFM.numDomainID,0) = v_numDomainID;
            DELETE FROM Activity A using ActivityResource AR where A.ActivityID = AR.ActivityID AND coalesce(R.numDomainId,0) = v_numDomainID;
            DELETE FROM AssembledItemChilds AIC using Item I where AIC.numAssembledItemID = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM AssembledItemChilds AIC using Item I where AIC.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM BankReconcileMatchRuleCondition BRMRC using BankReconcileMatchRule BRMR where BRMRC.numRuleID = BRMR.ID AND coalesce(BRMR.numDomainID,0) = v_numDomainID;
            DELETE FROM BillDetails BD using BillHeader BH where BD.numBillID = BH.numBillID AND coalesce(BH.numDomainId,0) = v_numDomainID;
            DELETE FROM BroadCastDtls BCD using Broadcast B where BCD.numBroadcastID = B.numBroadCastId AND coalesce(B.numDomainID,0) = v_numDomainID;
            DELETE FROM CaseContacts CC using Cases C where CC.numCaseID = C.numCaseId AND coalesce(C.numDomainID,0) = v_numDomainID;
            DELETE FROM CaseSolutions CS using Cases C where CS.numCaseId = C.numCaseId AND coalesce(C.numDomainID,0) = v_numDomainID;
            DELETE FROM CategoryProfileSites CPS using Sites S where CPS.numSiteID = S.numSiteID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionRuleDtl CRD using CommissionRules CR where CRD.numComRuleID = CR.numComRuleID AND coalesce(CR.numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionRuleItems CRI using CommissionRules CR where CRI.numComRuleID = CR.numComRuleID AND coalesce(CR.numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionRuleOrganization CRO using CommissionRules CR where CRO.numComRuleID = CR.numComRuleID AND coalesce(CR.numDomainID,0) = v_numDomainID;
            DELETE FROM CommunicationLinkedOrganization CLO using Communication C where CLO.numCommID = C.numCommId AND coalesce(C.numDomainID,0) = v_numDomainID;
            DELETE FROM ConECampaign CEC using ECampaign EC where CEC.numECampaignID = EC.numECampaignID AND coalesce(EC.numDomainID,0) = v_numDomainID;
            DELETE FROM CustomerCreditCardInfo CCCI using AdditionalContactsInformation ACI where CCCI.numContactId = ACI.numContactId AND coalesce(ACI.numDomainID,0) = v_numDomainID;
            DELETE FROM CustomQueryReportEmail CQRE using ReportListMaster RLM where CQRE.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecastItemClassification DFIC using DemandForecast DF where DFIC.numDFID = DF.numDFID AND coalesce(DF.numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecastItemGroup DFIG using DemandForecast DF where DFIG.numDFID = DF.numDFID AND coalesce(DF.numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecastWarehouse DFW using DemandForecast DF where DFW.numDFID = DF.numDFID AND coalesce(DF.numDomainID,0) = v_numDomainID;
            DELETE FROM ECampaignDTLs ECD using ECampaign EC where ECD.numECampID = EC.numECampaignID AND coalesce(EC.numDomainID,0) = v_numDomainID;
            DELETE FROM EcommerceRelationshipProfile ERP using Sites S where ERP.numSiteID = S.numSiteID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM FieldRelationshipDTL FRD using FieldRelationship FR where FRD.numFieldRelID = FR.numFieldRelID AND coalesce(FR.numDomainID,0) = v_numDomainID;
            DELETE FROM FinancialReportDetail FRD using FinancialReport FR where FRD.numFRID = FR.numFRID AND coalesce(FR.numDomainID,0) = v_numDomainID;
            DELETE FROM Import_File_Report IFR using Import_File_Master IFM where IFR.intImportFileID = IFM.intImportFileID AND coalesce(IFM.numDomainID,0) = v_numDomainID;
            DELETE FROM MarketBudgetDetails MBD using MarketBudgetMaster MBM where MBD.numMarketId = MBM.numMarketBudgetId AND coalesce(MBM.numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentWMState MSFWMS using MassSalesFulfillmentWM MSFWM where MSFWMS.numMSFWMID = MSFWM.ID AND coalesce(MSFWM.numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentWP MSFWP using MassSalesFulfillmentWM MSFWM where MSFWP.numMSFWMID = MSFWM.ID AND coalesce(MSFWM.numDomainID,0) = v_numDomainID;
            DELETE FROM OperationBudgetDetails OBD using OperationBudgetMaster OBM where OBD.numBudgetID = OBM.numBudgetId AND coalesce(OBM.numDomainId,0) = v_numDomainID;
            DELETE FROM OrderAutoRuleDetails OARD using OrderAutoRule OAR where OARD.numRuleID = OAR.numRuleID AND coalesce(OAR.numDomainID,0) = v_numDomainID;
            DELETE FROM PriceBookRuleItems PBRI using PriceBookRules PBR where PBRI.numRuleID = PBR.numPricRuleID AND coalesce(PBR.numDomainID,0) = v_numDomainID;
            DELETE FROM ProfileEGroupDTL PGD using ProfileEmailGroup PEG where PGD.numEmailGroupID = PEG.numEmailGroupID AND coalesce(PEG.numDomainID,0) = v_numDomainID;
            DELETE FROM PromotionOfferOrganizations POO using PromotionOffer PO where POO.numProId = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM PromotionOfferSites POS using PromotionOffer PO where POS.numPromotionID = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM ResourcePreference RP using Resource R where RP.ResourceID = R.ResourceID AND coalesce(R.numDomainId,0) = v_numDomainID;
            DELETE FROM ReviewDetail RD using Review R where RD.numReviewId = R.numReviewId AND coalesce(R.numDomainID,0) = v_numDomainID;
            DELETE FROM RoutingLeadDetails RLD using RoutingLeads RL where RLD.numRoutID = RL.numRoutID AND coalesce(RL.numDomainID,0) = v_numDomainID;
            DELETE FROM RoutinLeadsValues RLV using RoutingLeads RL where RLV.numRoutID = RL.numRoutID AND coalesce(RL.numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingLabelRuleChild SLRC using ShippingLabelRuleMaster SLRM where SLRC.numShippingRuleID = SLRM.numShippingRuleID AND coalesce(SLRM.numDomainID,0) = v_numDomainID;
            DELETE FROM SiteCategories SC using Sites S where SC.numSiteID = S.numSiteID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM SiteSubscriberDetails SSD using Sites S where SSD.numSiteID = S.numSiteID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM SiteWiseCategories SWC using Sites S where SWC.numSiteID = S.numSiteID AND coalesce(S.numDomainID,0) = v_numDomainID;
            DELETE FROM StagePercentageDetailsTaskNotes SPDTN using StagePercentageDetailsTask SPDT where SPDTN.numTaskID = SPDT.numTaskId AND coalesce(SPDT.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyAnsMaster SAM using SurveyMaster SM where SAM.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyQuestionMaster SQM using SurveyMaster SM where SQM.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyResponse SR using SurveyMaster SM where SR.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyWorkflowRules SWR using SurveyMaster SM where SWR.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM TrackingVisitorsDTL TVD using TrackingVisitorsHDR TVH where TVD.numTracVisitorsHDRID = TVH.numTrackingID AND coalesce(TVH.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowActionList WAL using WorkFlowMaster WFM where WAL.numWFID = WFM.numWFID AND coalesce(WFM.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowConditionList WCL using WorkFlowMaster WFM where WCL.numWFID = WFM.numWFID AND coalesce(WFM.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowTriggerFieldList WTFL using WorkFlowMaster WFM where WTFL.numWFID = WFM.numWFID AND coalesce(WFM.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkOrderPickedItems WOPI using WorkOrderDetails WOD where WOPI.numWODetailId = WOD.numWODetailId AND coalesce(WO.numDomainId,0) = v_numDomainID;
            DELETE FROM WorkScheduleDaysOff WSDO using WorkSchedule WS where WSDO.numWorkScheduleID = WS.ID AND coalesce(WS.numDomainID,0) = v_numDomainID;
            DELETE FROM AOIContactLink ACL using AdditionalContactsInformation ACI where ACL.numContactID = ACI.numContactId AND coalesce(ACI.numDomainID,0) = v_numDomainID;
            DELETE FROM BillPaymentDetails BPD using BillPaymentHeader BPH where BPD.numBillPaymentID = BPH.numBillPaymentID AND coalesce(BPH.numDomainId,0) = v_numDomainID;
            DELETE FROM BroadcastingLink BL using Broadcast B where BL.numBroadcastID = B.numBroadCastId AND coalesce(B.numDomainID,0) = v_numDomainID;
            DELETE FROM CampaignDetails CD using CampaignMaster CM where CD.numCampaignId = CM.numCampaignID AND coalesce(CM.numDomainID,0) = v_numDomainID;
            DELETE FROM CheckDetails CD using CheckHeader CH where CD.numCheckHeaderID = CH.numCheckHeaderID AND coalesce(CH.numDomainID,0) = v_numDomainID;
            DELETE FROM ChildKitsRelation CKR using Item I where CKR.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionRuleContacts CRC using CommissionRules CR where CRC.numComRuleId = CR.numComRuleID AND coalesce(CR.numDomainID,0) = v_numDomainID;
            DELETE FROM CommunicationAttendees CA using Communication C where CA.numCommId = C.numCommId AND coalesce(C.numDomainID,0) = v_numDomainID;
            DELETE FROM ContractsLog CL using Contracts C where CL.numContractId = C.numContractId AND coalesce(C.numDomainId,0) = v_numDomainID;
            DELETE FROM CustReportFields CRF using CustomReport CR where CRF.numCustomReportId = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM CustReportFilterlist CRFL using CustomReport CR where CRFL.numCustomReportId = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM CustReportKPI CRK using CustomReport CR where CRK.numCustomReportId = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM CustReportOrderlist CROL using CustomReport CR where CROL.numCustomReportId = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM CustReportSummationlist CRSL using CustomReport CR where CRSL.numCustomReportId = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM Dashboard D using CustomReport CR where D.numReportID = CR.numCustomReportId AND coalesce(CR.numdomainId,0) = v_numDomainID;
            DELETE FROM DashboardAllowedReports DAR using AuthenticationGroupMaster AGM where DAR.numGrpId = AGM.numGroupID AND coalesce(AGM.numDomainID,0) = v_numDomainID;
            DELETE FROM DashBoardSize DS using UserMaster UM where DS.numGroupUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM DiscountCodeUsage DCU using DiscountCodes DC where DCU.numDiscountId = DC.numDiscountId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM DiscountCodes DC using PromotionOffer PO where DC.numPromotionID = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM Favorites F using UserMaster UM where F.numUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM ForReportsByCampaign FRC using UserMaster UM where FRC.numUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM GroupTabDetails GTD using AuthenticationGroupMaster AGM where GTD.numGroupID = AGM.numGroupID AND coalesce(AGM.numDomainID,0) = v_numDomainID;
            DELETE FROM Import_File_Field_Mapping IFFM using Import_File_Master IFM where IFFM.intImportFileID = IFM.intImportFileID AND coalesce(IFM.numDomainID,0) = v_numDomainID;
            DELETE FROM Import_History IH using Import_File_Master IFM where IH.intImportFileID = IFM.intImportFileID AND coalesce(IFM.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemCategory IC using Category C where IC.numCategoryID = C.numCategoryID AND coalesce(C.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemCategory IC using Item I where IC.numItemID = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemDetails ID using Item I where ID.numChildItemID = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemDetails ID using Item I where ID.numItemKitID = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemExtendedDetails IED using Item I where IED.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemGroupsDTL IGD using ItemGroups IG where IGD.numItemGroupID = IG.numItemGroupID AND coalesce(IG.numDomainID,0) = v_numDomainID;
            DELETE FROM ItemTax IT using Item I where IT.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentBatchOrders MSFBO using MassSalesFulfillmentBatch MSFB where MSFBO.numBatchID = MSFB.ID AND coalesce(MSFB.numDomainID,0) = v_numDomainID;
            DELETE FROM MassStockTransfer MST using Item I where MST.numItemCode = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM MyReportList MRL using UserMaster UM where MRL.numUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityAutomationQueueExecution OAQE using OpportunityAutomationQueue OAQ where OAQE.numOppQueueID = OAQ.numOppQueueID AND coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProcurementBudgetDetails PBD using ProcurementBudgetMaster PBM where PBD.numProcurementId = PBM.numProcurementBudgetId AND coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PromotionOfferContacts POC using PromotionOffer PO where POC.numProId = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM PromotionOfferItems POI using PromotionOffer PO where POI.numProId = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM PromotionOfferOrderBased POOB using PromotionOffer PO where POOB.numProID = PO.numProId AND coalesce(PO.numDomainId,0) = v_numDomainID;
            DELETE FROM RecentItems RI using UserMaster UM where RI.numUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM RemarketDTL RD using Item I where RD.numItemID = I.numItemCode AND coalesce(I.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportSummaryGroupList RSGL using ReportListMaster RLM where RSGL.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportMatrixAggregateList RMAL using ReportListMaster RLM where RMAL.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportKPIThresold RKT using ReportListMaster RLM where RKT.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportFieldsList RFL using ReportListMaster RLM where RFL.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportFilterList RFL using ReportListMaster RLM where RFL.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ReportKPIGroupDetailList RKGDL using ReportListMaster RLM where RKGDL.numReportID = RLM.numReportID AND coalesce(RLM.numDomainID,0) = v_numDomainID;
            DELETE FROM ScheduledReportsGroupLog SRGL using ScheduledReportsGroup SRG where SRGL.numSRGID = SRG.ID AND coalesce(SRG.numDomainID,0) = v_numDomainID;
            DELETE FROM ScheduledReportsGroupReports SRGR using ScheduledReportsGroup SRG where SRGR.numSRGID = SRG.ID AND coalesce(SRG.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyMatrixAnsMaster SMAM using SurveyMaster SM where SMAM.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyCreateRecord SCR using SurveyMaster SM where SCR.numSurID = SM.numSurID AND coalesce(SM.numDomainID,0) = v_numDomainID;
            DELETE FROM UserRoles UR using UserMaster UM where UR.numUserCntID = UM.numUserDetailId AND coalesce(UM.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowQueueExecution WFQE using WorkFlowMaster WFM where WFQE.numWFID = WFM.numWFID AND coalesce(WFM.numDomainID,0) = v_numDomainID;
            DELETE FROM WorkOrderDetails WOD using WorkOrder WO where WOD.numWOId = WO.numWOId AND coalesce(WO.numDomainId,0) = v_numDomainID;
            DELETE FROM SalesTemplateItems WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingReport WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EDIQueue WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ExtranetAccountsDtl WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityItemsReceievedLocation WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityItemsReleaseDates WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunitySalesTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityStageDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProjectProgress WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AlertEmailDTL WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionContacts WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CustomerPartNumber WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DivisionMasterPromotionHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmbeddedCostDefaults WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ExtarnetAccounts EA using DivisionMaster DM where EA.numDivisionID = DM.numDivisionID AND coalesce(DM.numDomainID,0) = v_numDomainID;
            DELETE FROM ExtarnetAccounts EA using CompanyInfo CI where EA.numCompanyID = CI.numCompanyId AND coalesce(CI.numDomainID,0) = v_numDomainID;
            DELETE FROM ExtarnetAccounts WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ImportActionItemReference WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ImportOrganizationContactReference WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PageElementDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProjectsStageDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SiteBreadCrumb WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SiteMenu WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Vendor WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM VendorShipmentMethod WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM AccountingCharges WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AccountTypeDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AlertConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ApprovalProcessItemsClassification WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BankReconcileFileData WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocAlerts WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CashCreditCardDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CFW_Fld_Master WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ChartAccountOpening WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CheckDetails_Old WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM COAShippingMapping WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionPayPeriod WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CompanyAssociations WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ContactTypeMapping WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Correspondence WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CustomReport WHERE coalesce(numdomainId,0) = v_numDomainID;
            DELETE FROM DashboardTemplateReports WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DepositDetails_Old WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DepositMaster WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ErrorDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ExportDataSettings WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM GroupAuthorization WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ImapUserDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemUOMConversion WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ListDetailsName WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PortalDashboardDtl WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProjectsMaster WHERE coalesce(numdomainId,0) = v_numDomainID;
            DELETE FROM PurchaseOrderFulfillmentSettings WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecordHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReturnHeader WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingServiceAbbreviations WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SitePages WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SiteTemplates WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM StagePercentageDetails WHERE coalesce(numdomainid,0) = v_numDomainID;
            DELETE FROM StagePercentageDetailsTaskTimeLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM StyleSheets WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SubscriberHstr WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyRespondentsMaster WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM TaxCountryConfi WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TrackingCampaignData WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AccountingIntegration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AccountTypeDetail_delete WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ActivityDisplayConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ActivityFormAuthentication WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AdditionalContactsInformation WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AdditionalContactsInformation_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AddressDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AdvSearchCriteria WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AdvSerViewConf WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AlertDomainDtl WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AlertHDR WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Approval_transaction_log WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ApprovalConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AssembledItem WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Audit WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AuthenticationGroupBackOrder WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AuthenticationGroupMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM AuthoritativeBizDocs WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM BankDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BankReconcileMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BankReconcileMatchRule WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BankStatementHeader WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BankStatementTransactions WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BillHeader WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM BillingTerms WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BillPaymentHeader WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM BizAPIErrorLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocAction WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocApprovalRule WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocAttachments WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocComission WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM BizDocFilter WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocStatusApprove WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizDocTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM BizFormWizardMasterConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Broadcast WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CampaignMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CartItems WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Cases WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Cases_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Category WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CategoryProfile WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CFw_Grp_Master WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CheckCompanyDetails WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM CheckHeader WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ClassDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM COACreditCardCharge WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM COARelationships WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionReports WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CommissionRules WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Communication WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Communication_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CommunicationBackUP WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CompanyAssets WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Competitor WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ContractManagement WHERE coalesce(numdomainId,0) = v_numDomainID;
            DELETE FROM Contracts WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ContractsContact WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Currency WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM CustomQueryReport WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CustomReport26032011 WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM CustRptScheduler WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DashboardTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DefaultTabs WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DeleteThis WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecast WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecastAnalysisPattern WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DemandForecastDays WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DivisionMaster_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DivisionMasterShippingConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DomainMarketplace WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DomainSFTPDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycCartFilters WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycField_Globalization WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycField_Validation WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFieldColorScheme WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFieldMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFieldMasterSynonym WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFormConfigurationDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFormField_Mapping WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycFrmConfigBizDocsSumm WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DycModuleMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFormAOIConf WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFormConfigurationDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFormField_Validation WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFormFieldMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFormMasterParam WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM DynamicFrmConfigBizDocsSumm WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ECampaign WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ECampaignAssignee WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ECommerceCreditCardTransactionLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM eCommerceDTL WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM eCommercePaymentConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EDIHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EDIQueueLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ElasticSearchBizCart WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ElasticSearchModifiedRecords WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmailBroadcastConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmailBroadcastConfiguration_old WHERE coalesce(numDomainID,0) = v_numDomainID;
            SWV_Label_value := 'deleteMoreEmailHistory';
         WHEN SWV_Label_value = 'deleteMoreEmailHistory' THEN
            DELETE FROM EmailHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreEmailHistory';
               CONTINUE SWL_Label;
            end if;
            DELETE FROM EmailMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM EmbeddedCostConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ExceptionLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM FieldRelationship WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM FinancialReport WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM FinancialYear WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM FixedAssetDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM FollowUpHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Forecast WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM FormFieldGroupConfigurarion WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ForReportsByAsItsType WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ForReportsByTeam WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ForReportsByTerritory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ForReportsByUser WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM GenealEntryAudit WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM GenericDocuments WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM GoogleContactGroups WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM HTMLFormURL WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Import_File_Master WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ImportApiOrder WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM InboxTree WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM InboxTreeSort WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM InitialListColumnConf WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM InventroyReportDTL WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemAPI WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ItemAttributes WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemCurrencyPrice WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemGroups WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemImages WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ItemMarketplaceMapping WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ItemOptionAccAttr WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM LandedCostExpenseAccount WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM LandedCostVendors WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM LeadsubForms WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Listdetails WHERE coalesce(numDomainid,0) = v_numDomainID AND coalesce(constFlag,false) = false;
            DELETE FROM listmaster WHERE coalesce(numDomainID,0) = v_numDomainID AND coalesce(bitFixed,false) = false;
            DELETE FROM listorder WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM LuceneItemsIndex WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MarketBudgetMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MassPurchaseFulfillmentConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentBatch WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentShippingConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MassSalesFulfillmentWM WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM MessageMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM NameTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OperationBudgetMaster WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OppFulfillmentBizDocsStatusHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityAutomationQueue WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityAutomationRules WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityBizDocsDetails WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityMaster_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityMasterAPI WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM OpportunityMasterShippingRateError WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OpportunityOrderStatus WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OrderAutoRule WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OrganizationRatingRule WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM OverrideAssignTo WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM PackagingRules WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PageLayoutDTL WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ParentChildCustomFieldMap WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PaymentGatewayDTLID WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PayrollDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PayrollHeader WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PayrollTracking WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PortalBizDocs WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PortalWorkflow WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PriceBookRules WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PricingNamesTable WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProcurementBudgetMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProfileEmailGroup WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ProjectsMaster_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PromotionOffer WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM PromotionOfferOrder WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM PurchaseIncentives WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Ratings WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecImportConfg WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecordOrganizationChangeHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecurrenceConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecurrenceErrorLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RecurringTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RedirectConfig WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RelationsDefaultFilter WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReportDashboard WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReportDashboardAllowedReports WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReportDashBoardSize WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReportKPIGroupListMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ReportListMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Resource WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Review WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RoutingLeads WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ruleAction WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RuleCondition WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM RuleMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Sales_process_List_Master WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesFulfillmentConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesFulfillmentLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesFulfillmentQueue WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesOrderConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesOrderLineItemsPOLinking WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesOrderRule WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SalesReturnCommission WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM SavedSearch WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ScheduledReportsGroup WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShareRecord WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingExceptions WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingFieldValues WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingLabelRuleMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingPromotions WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingProviderForEComerce WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingRules WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ShippingRuleStateList WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingService WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShippingServiceTypes WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShortCutBar WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM ShortCutGrpConf WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM ShortCutUsrCnf WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SignatureDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SimilarItems WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Sites WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SMTPUserDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SolutionMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SpecificDocuments WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM StagePercentageDetails_TempDateFields WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM StagePercentageDetailsTask WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM StagePercentageDetailsTaskCommission WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM State WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM SurveyTemplate WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TabDefault WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TabMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TaxDetails WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM TaxItems WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM tblActionItemData WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM tblStageGradeDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TicklerListFilterConfiguration WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM timeandexpense WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TimeAndExpenseCommission WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TOPICMASTER WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TOPICMESSAGEATTACHMENTS WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TrackingVisitorsHDR WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TrackNotification WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TransactionHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TreeNavigationAuthorization WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TreeNodeOrder WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM TrueCommerceLog WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UnitPriceApprover WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UniversalSMTPIMAP WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UOM WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UOMConversion WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UserAccessedDTL WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM UserReportList WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UserTeams WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UserTerritory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Vendortemp WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WareHouseDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WareHouseForSalesFulfillment WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WareHouseItems_Tracking WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WebAPIDetail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WebApiOppItemDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WebApiOrderDetails WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WebApiOrderReports WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WebService WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowAlertList WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowARAgingExecutionHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkflowAutomation WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowDateFieldExecutionHistory WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowQueue WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkFlowMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WorkOrder WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM WorkSchedule WHERE coalesce(numDomainID,0) = v_numDomainID;
            SWV_Label_value := 'deleteMoreOpportunityMaster';
         WHEN SWV_Label_value = 'deleteMoreOpportunityMaster' THEN
            DELETE FROM OpportunityMaster WHERE coalesce(numDomainId,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreOpportunityMaster';
               CONTINUE SWL_Label;
            end if;
            SWV_Label_value := 'deleteMoreWareHouseItems';
         WHEN SWV_Label_value = 'deleteMoreWareHouseItems' THEN
            DELETE FROM WareHouseItems WHERE coalesce(numDomainID,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreWareHouseItems';
               CONTINUE SWL_Label;
            end if;
            SWV_Label_value := 'deleteMoreItem';
         WHEN SWV_Label_value = 'deleteMoreItem' THEN
            DELETE FROM Item WHERE coalesce(numDomainID,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreItem';
               CONTINUE SWL_Label;
            end if;
            DELETE FROM Warehouses WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM WarehouseLocation WHERE coalesce(numDomainID,0) = v_numDomainID;
            SWV_Label_value := 'deleteMoreDivisionMaster';
         WHEN SWV_Label_value = 'deleteMoreDivisionMaster' THEN
            DELETE FROM DivisionMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreDivisionMaster';
               CONTINUE SWL_Label;
            end if;
            SWV_Label_value := 'deleteMoreCompanyInfo';
         WHEN SWV_Label_value = 'deleteMoreCompanyInfo' THEN
            DELETE FROM CompanyInfo WHERE coalesce(numDomainID,0) = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount != 0 then
               SWV_Label_value := 'deleteMoreCompanyInfo';
               CONTINUE SWL_Label;
            end if;
            DELETE FROM Chart_Of_Accounts WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM DivisionMasterBizDocEmail WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM UserMaster WHERE coalesce(numDomainID,0) = v_numDomainID;
            DELETE FROM Domain WHERE coalesce(numDomainId,0) = v_numDomainID;
            DELETE FROM Subscribers WHERE numTargetDomainID = v_numDomainID;
            EXIT SWL_Label;
         END CASE;
      END LOOP;
      EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;


