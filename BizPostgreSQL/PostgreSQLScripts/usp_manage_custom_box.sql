-- Stored procedure definition script USP_MANAGE_CUSTOM_BOX for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_MANAGE_CUSTOM_BOX(INOUT v_numCustomPackageID		NUMERIC(18,0) ,
	v_vcPackageName			VARCHAR(1000),
	v_fltWidth				NUMERIC(18,2),
	v_fltHeight				NUMERIC(18,2),
	v_fltLength				NUMERIC(18,2),
	v_fltTotalWeight			NUMERIC(18,2))
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPackageTypeID  NUMERIC(18,2);
BEGIN
   IF EXISTS(SELECT * FROM CustomPackages WHERE vcPackageName = v_vcPackageName AND numCustomPackageID <> v_numCustomPackageID) then
	
	     -- Message text.
				    -- Severity.
				    -- State.
      RAISE EXCEPTION 'ERROR: Custom Box Name already exists. Please provide another Box Name.';
      RETURN;
   end if;

   IF EXISTS(SELECT numCustomPackageID FROM CustomPackages WHERE numCustomPackageID = v_numCustomPackageID) then
	
      UPDATE CustomPackages SET vcPackageName = v_vcPackageName,fltHeight = v_fltHeight,fltLength = v_fltLength,
      fltTotalWeight = v_fltTotalWeight,fltWidth = v_fltWidth
      WHERE numCustomPackageID = v_numCustomPackageID;
   ELSE
      v_numPackageTypeID := 100;
      v_numPackageTypeID :=  v_numPackageTypeID+(SELECT MAX(numPackageTypeID) FROM CustomPackages);
      INSERT INTO CustomPackages(numPackageTypeID,vcPackageName,fltWidth,fltHeight,fltLength,fltTotalWeight)
								VALUES(v_numPackageTypeID,v_vcPackageName,v_fltWidth,v_fltHeight,v_fltLength,v_fltTotalWeight);
		
      v_numCustomPackageID := CURRVAL('CustomPackages_seq');
   end if;
END; $$;



