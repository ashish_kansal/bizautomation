-- Stored procedure definition script sendMail_With_CDOMessage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION sendMail_With_CDOMessage(v_to VARCHAR(64),   
    v_subject VARCHAR(1000) DEFAULT '',   
    v_body VARCHAR(8000) DEFAULT '',  
    v_CC VARCHAR(1000) DEFAULT '',
    v_From VARCHAR(64) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_handle  INTEGER;
   v_return  INTEGER;
   v_s  VARCHAR(64);
   v_sc  VARCHAR(64);
   v_up  CHAR(27);
   v_server  VARCHAR(255);
   v_filename  VARCHAR(255);
BEGIN
    
   
   v_s := '"http://schemas.microsoft.com/cdo/configuration/';    
       

   if v_From IS NULL then 
      v_From := '';
   end if;
   if v_From = '' then 
      v_From := 'administrator@bizautomation.com';
   end if;
       
        -- or IP address, e.g. '127.0.0.1'   
       
        -- if you want an attachment:    
   v_s := 'Configuration.Fields(' || coalesce(v_s,'');
   v_up := 'Configuration.Fields.Update';
   v_server := '127.0.0.1';
   v_filename := E'C:\\folder\\file.ext';    
       
       
   SELECT * INTO v_return FROM sp_OACreate('CDO.Message',v_handle);    
   v_sc := coalesce(v_s,'') || 'sendusing").Value';    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,v_sc,'2');    
   v_sc := coalesce(v_s,'') || 'smtpserver").Value';    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,v_sc,v_server);    
   SELECT * INTO v_return FROM sp_OAMethod(v_handle,v_up,NULL);    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,'To',v_to);    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,'From',v_From);    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,'Subject',v_subject);    
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,'htmlbody',v_body);  
   SELECT * INTO v_return FROM sp_OASetProperty(v_handle,'CC',v_CC);  
       
   IF v_filename IS NOT NULL then
      SELECT * INTO v_return FROM sp_OAMethod(v_handle,'AddAttachment',NULL,v_filename);
   end if;    
   
   SELECT * INTO v_return FROM sp_OAMethod(v_handle,'Send',NULL);  

   IF v_return <> 0 then
    
      RAISE NOTICE 'Mail failed.';
      IF v_From IS NULL then
         RAISE NOTICE 'From address undefined.';
      ELSE
         RAISE NOTICE 'Check that server is valid.';
      end if;
      SELECT * INTO v_return FROM sp_OADestroy(v_handle);
   end if;
   RETURN;
END; $$;


