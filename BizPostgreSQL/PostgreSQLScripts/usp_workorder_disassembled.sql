CREATE OR REPLACE FUNCTION USP_WorkOrder_Disassembled(v_numDomainID NUMERIC(18,0),
	v_numWOID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	BEGIN
		UPDATE WorkOrder SET numParentWOID = 0 WHERE numParentWOID = v_numWOID;
		DELETE FROM WorkOrderDetails WHERE numWOId = v_numWOID;
		DELETE FROM WorkOrder WHERE numWOId = v_numWOID;
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
		my_ex_state   = RETURNED_SQLSTATE,
		my_ex_message = MESSAGE_TEXT,
		my_ex_detail  = PG_EXCEPTION_DETAIL,
		my_ex_hint    = PG_EXCEPTION_HINT,
		my_ex_ctx     = PG_EXCEPTION_CONTEXT;

		raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
	END;

	RETURN;
END; $$;  



