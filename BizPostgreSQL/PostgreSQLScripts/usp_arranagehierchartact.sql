-- Stored procedure definition script USP_ArranageHierChartAct for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ArranageHierChartAct(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,          
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountId  NUMERIC(9,0);
   v_vcCatgyName  VARCHAR(100);
   v_numAcntType  NUMERIC(9,0);
BEGIN
   SELECT vcCatgyName INTO v_vcCatgyName FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId; 
   SELECT coalesce(numAcntType,0) INTO v_numAcntType FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId;         
   insert into HierChartOfAct(numChartOfAcntID,numDomainID,vcCategoryName,numAcntType)
 values(v_numParntAcntId,v_numDomainID,REPEAT('.', (v_NESTLEVEL -3)*4) || coalesce(v_vcCatgyName, ''),v_numAcntType);        
 
   SELECT MIN(numAccountId) INTO v_numAccountId FROM Chart_Of_Accounts WHERE numParntAcntId = v_numParntAcntId;          
          
   WHILE v_numAccountId IS NOT NULL LOOP
      PERFORM USP_ArranageHierChartAct(v_numAccountId,v_numDomainID);
      SELECT MIN(numAccountId) INTO v_numAccountId FROM Chart_Of_Accounts WHERE numParntAcntId = v_numParntAcntId AND numAccountId > v_numAccountId;
   END LOOP;
   RETURN;
END; $$;


