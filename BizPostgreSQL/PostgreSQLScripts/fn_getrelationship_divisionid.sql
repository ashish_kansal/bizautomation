-- Function definition script fn_GetRelationship_DivisionId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetRelationship_DivisionId(v_ListID NUMERIC,v_numDomainID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRelationValue  VARCHAR(250);
   v_vcDivisionId  VARCHAR(250);
BEGIN
   v_vcRelationValue := '';
   v_vcDivisionId := '';
	
        

   select   vcData INTO v_vcRelationValue FROM Listdetails WHERE numListItemID = v_ListID and (constFlag = true or numDomainid = v_numDomainID); 
   RETURN v_vcRelationValue;
END; $$;

