-- Stored procedure definition script USP_Project_GetMilestones for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Project_GetMilestones(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   ROW_NUMBER() OVER(ORDER BY vcMileStoneName ASC)  AS numMileStoneID
		,vcMileStoneName
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',coalesce(stagepercentagemaster.numstagepercentage,0),
   '% of total)') AS vcMileStone
		,GetTotalProgress(v_numDomainID,v_numProId,2::SMALLINT,2::SMALLINT,vcMileStoneName,0) AS numTotalProgress
		,(SELECT intTotalProgress FROM ProjectProgress where numProId = v_numProId AND numDomainID = v_numDomainID LIMIT 1) AS numTotalCompletedProgress
   FROM
   ProjectsMaster
   INNER JOIN
   StagePercentageDetails
   ON
   ProjectsMaster.numBusinessProcessID = StagePercentageDetails.slp_id
   LEFT JOIN
   stagepercentagemaster
   ON
   StagePercentageDetails.numStagePercentageId = stagepercentagemaster.numstagepercentageid
   WHERE
   ProjectsMaster.numdomainId = v_numDomainID
   AND ProjectsMaster.numProId = v_numProId
   GROUP BY
   vcMileStoneName,stagepercentagemaster.numstagepercentage;
END; $$;












