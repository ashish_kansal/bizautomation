-- Stored procedure definition script USP_GetKitParentsForChildItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetKitParentsForChildItems(v_numChildItemID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numItemKitID as VARCHAR(255)),cast(numChildItemID as VARCHAR(255)),cast(ItemDetails.numQtyItemsReq as VARCHAR(255)),cast(vcItemName as VARCHAR(255)) as KitParent
   from ItemDetails
   join Item
   on numItemCode = numItemKitID
   where numChildItemID = v_numChildItemID;
END; $$;












