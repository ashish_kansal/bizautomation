-- Function definition script GetAge for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetAge(v_DOB TIMESTAMP, v_Today TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Age  INTEGER;
BEGIN
   v_Age := EXTRACT(Year FROM v_Today) -EXTRACT(Year FROM v_DOB);
   If EXTRACT(Month FROM v_Today) < EXTRACT(Month FROM v_DOB) then
      v_Age := v_Age::bigint -1;
   end if;
   If EXTRACT(Month FROM v_Today) = EXTRACT(Month FROM v_DOB) and EXTRACT(Day FROM v_Today) < EXTRACT(Day FROM v_DOB) then
      v_Age := v_Age::bigint -1;
   end if;
   Return v_Age;
END; $$;

