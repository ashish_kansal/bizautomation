-- Function definition script fn_GetAOINames for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetAOINames(v_vcAOIList VARCHAR(250))
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
--This function will take the AOI IDs in comma separated string and return the AOI names with <BR> separating each AOI.
   DECLARE
   v_intFirstComma  INTEGER;
   v_intNextComma  INTEGER;
   v_bintValue  BIGINT;
   v_vcRetVal  VARCHAR(1000);
   v_vcTempVal  VARCHAR(255);
BEGIN
   v_vcRetVal := '';

   IF v_vcAOIList = '' then
	
      RETURN '&nbsp;';
   end if;
   v_intFirstComma := POSITION(',' IN v_vcAOIList);
   v_intNextComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intFirstComma+1)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   IF v_intNextComma = 0 then
	
      v_bintValue := CAST(SUBSTR(v_vcAOIList,v_intFirstComma+1,LENGTH(v_vcAOIList) -v_intFirstComma) AS BIGINT);
      IF CAST(v_bintValue AS NUMERIC) > 0 then
		
			--Get the AOI Name.
         select   vcAOIName INTO v_vcTempVal FROM AOIMaster WHERE numAOIID = v_bintValue;
         v_vcRetVal := coalesce(v_vcTempVal,'') || '<BR>';
      end if;
      v_intFirstComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intNextComma)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
   end if;
   WHILE v_intFirstComma > 0 LOOP
      IF v_intNextComma = 0 then
		
         v_bintValue := CAST(SUBSTR(v_vcAOIList,v_intFirstComma+1,LENGTH(v_vcAOIList) -v_intFirstComma) AS BIGINT);
         IF CAST(v_bintValue AS NUMERIC) > 0 then
			
				--Get the AOI Name.
            select   vcAOIName INTO v_vcTempVal FROM AOIMaster WHERE numAOIID = v_bintValue;
            v_vcRetVal := coalesce(v_vcRetVal,'') || coalesce(v_vcTempVal,'') || '<BR>';
         end if;
         EXIT;
      ELSE
         v_bintValue := CAST(SUBSTR(v_vcAOIList,v_intFirstComma+1,v_intNextComma -v_intFirstComma -1) AS BIGINT);
         IF CAST(v_bintValue AS NUMERIC) > 0 then
			
				--Get the AOI Name.
            select   vcAOIName INTO v_vcTempVal FROM AOIMaster WHERE numAOIID = v_bintValue;
            v_vcRetVal := coalesce(v_vcRetVal,'') || coalesce(v_vcTempVal,'') || '<BR>';
         end if;
      end if;
      v_intFirstComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intNextComma)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
      v_intNextComma := CASE WHEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intFirstComma+1)) > 0 THEN POSITION(',' IN SUBSTR(v_vcAOIList,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   END LOOP;

   RETURN v_vcRetVal;
END; $$;

