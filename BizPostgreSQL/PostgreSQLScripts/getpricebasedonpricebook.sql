DROP FUNCTION IF EXISTS GetPriceBasedOnPriceBook;

CREATE OR REPLACE FUNCTION GetPriceBasedOnPriceBook(v_numRuleID NUMERIC,
    v_monPrice DECIMAL(20,5),
    v_units DOUBLE PRECISION,
    v_ItemCode NUMERIC)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
--Below function applies Price Rule on given Price an Qty and returns calculated value
   AS $$
   DECLARE
   v_PriceBookPrice  DECIMAL(20,5) DEFAULT 0;
   v_tintRuleType  SMALLINT;
   v_tintVendorCostType SMALLINT;
   v_tintPricingMethod  SMALLINT;
   v_tintDiscountType  SMALLINT;
   v_decDiscount  DOUBLE PRECISION;
   v_intQntyItems  INTEGER;
   v_decMaxDedPerAmt  DOUBLE PRECISION;
   v_bitRoundTo BOOLEAN;
   v_tintRoundTo SMALLINT;
   v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
   SWV_RowCount INTEGER;
BEGIN
	SELECT 
		PB.tintRuleType,PB.tintVendorCostType,PB.tintPricingMethod,PB.tintDiscountType,PB.decDiscount,PB.intQntyItems,PB.decMaxDedPerAmt,PB.bitRoundTo,PB.tintRoundTo   
	INTO 
		v_tintRuleType,v_tintVendorCostType,v_tintPricingMethod,v_tintDiscountType,v_decDiscount,v_intQntyItems,v_decMaxDedPerAmt,v_bitRoundTo,v_tintRoundTo  
	FROM
		PriceBookRules PB
	LEFT OUTER JOIN
		PriceBookRuleDTL PD
	ON
		PB.numPricRuleID = PD.numRuleID 
	WHERE
		numPricRuleID = v_numRuleID;
	

    
   IF (v_tintPricingMethod = 1) then -- Price table
    
      v_intQntyItems := 1;
      select   tintRuleType, tintDiscountType, decDiscount INTO v_tintRuleType,v_tintDiscountType,v_decDiscount FROM
      PricingTable WHERE
      numPriceRuleID = v_numRuleID
      AND coalesce(numCurrencyID,0) = 0
      AND v_units BETWEEN intFromQty:: DOUBLE PRECISION AND intToQty:: DOUBLE PRECISION    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF (SWV_RowCount = 0) then
         RETURN v_monPrice;
      end if;
      IF v_tintRuleType = 2 then
         v_monPrice := fn_GetVendorCost(v_ItemCode);

		 IF EXISTS (SELECT 
						VendorCostTable.numvendorcosttableid
					FROM 
						Item
					INNER JOIN
						Vendor 
					ON
						Item.numVendorID = Vendor.numVendorID
					INNER JOIN 
						VendorCostTable 
					ON 
						Vendor.numvendortcode=VendorCostTable.numvendortcode 
					WHERE
						Item.numItemCode = v_ItemCode
						AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
						AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit)) THEN
			SELECT 
				COALESCE(VendorCostTable.monDynamicCost,v_monPrice)
				,COALESCE(VendorCostTable.monStaticCost,v_monPrice)
			INTO 
				v_monVendorDynamicCost
				,v_monVendorStaticCost
			FROM 
				Item
			INNER JOIN
				Vendor 
			ON
				Item.numVendorID = Vendor.numVendorID
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Item.numItemCode = v_ItemCode
				AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
				AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit);
		ELSE
			v_monVendorDynamicCost := v_monPrice;
			v_monVendorStaticCost := v_monPrice;
		END IF;
	
		v_monPrice := (CASE v_tintVendorCostType WHEN 1 THEN v_monVendorDynamicCost WHEN 2 THEN v_monVendorStaticCost ELSE v_monPrice END);
      end if;
	
		-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
      IF (v_tintDiscountType = 1) then -- Percentage 
        
         v_decDiscount := v_monPrice*(v_decDiscount/100);
         IF (v_tintRuleType = 1) then
            v_PriceBookPrice :=(v_monPrice -v_decDiscount);
         end if;
         IF (v_tintRuleType = 2) then
            v_PriceBookPrice := v_monPrice+v_decDiscount;
         end if;
      end if;
      IF (v_tintDiscountType = 2) then -- Flat discount 
		
         IF (v_tintRuleType = 1) then
            v_PriceBookPrice :=(v_monPrice -v_decDiscount);
         end if;
         IF (v_tintRuleType = 2) then
            v_PriceBookPrice :=(v_monPrice+v_decDiscount);
         end if;
      end if;
      IF (v_tintDiscountType = 2) then -- Named Price
		
         v_PriceBookPrice :=(v_monPrice -v_decDiscount);
      end if;
   end if;
        
   IF (v_tintPricingMethod = 2) then -- Pricing Formula 
    
      IF v_tintRuleType = 2 then
		
         v_monPrice := fn_GetVendorCost(v_ItemCode);

		 IF EXISTS (SELECT 
						VendorCostTable.numvendorcosttableid
					FROM 
						Item
					INNER JOIN
						Vendor 
					ON
						Item.numVendorID = Vendor.numVendorID
					INNER JOIN 
						VendorCostTable 
					ON 
						Vendor.numvendortcode=VendorCostTable.numvendortcode 
					WHERE
						Item.numItemCode = v_ItemCode
						AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
						AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit)) THEN
			SELECT 
				COALESCE(VendorCostTable.monDynamicCost,v_monPrice)
				,COALESCE(VendorCostTable.monStaticCost,v_monPrice)
			INTO 
				v_monVendorDynamicCost
				,v_monVendorStaticCost
			FROM 
				Item
			INNER JOIN
				Vendor 
			ON
				Item.numVendorID = Vendor.numVendorID
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Item.numItemCode = v_ItemCode
				AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
				AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit);
		ELSE
			v_monVendorDynamicCost := v_monPrice;
			v_monVendorStaticCost := v_monPrice;
		END IF;
	
		v_monPrice := (CASE v_tintVendorCostType WHEN 1 THEN v_monVendorDynamicCost WHEN 2 THEN v_monVendorStaticCost ELSE v_monPrice END);
      end if;
      IF (v_tintDiscountType = 1) then -- Percentage 
        
         v_decDiscount := v_decDiscount*(v_units/v_intQntyItems::bigint);
         v_decDiscount := v_decDiscount*v_monPrice/100;
         v_decMaxDedPerAmt := v_decMaxDedPerAmt*v_monPrice/100;
         IF (v_decDiscount > v_decMaxDedPerAmt) then
            v_decDiscount := v_decMaxDedPerAmt;
         end if;
         IF (v_tintRuleType = 1) then
            v_PriceBookPrice := v_monPrice -v_decDiscount;
         end if;
         IF (v_tintRuleType = 2) then
            v_PriceBookPrice := v_monPrice+v_decDiscount;
         end if;
      end if;
      IF (v_tintDiscountType = 2) then -- Flat discount 
        
         v_decDiscount := v_decDiscount*(v_units/v_intQntyItems::bigint);
         IF (v_decDiscount > v_decMaxDedPerAmt) then
            v_decDiscount := v_decMaxDedPerAmt;
         end if;
         IF (v_tintRuleType = 1) then
            v_PriceBookPrice :=(v_monPrice -v_decDiscount);
         end if;
         IF (v_tintRuleType = 2) then
            v_PriceBookPrice :=(v_monPrice+v_decDiscount);
         end if;
      end if;
   end if;


   RETURN (CASE 
				WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
				THEN ROUND(coalesce(v_PriceBookPrice,0)) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
				ELSE coalesce(v_PriceBookPrice,0) 
			END);
END; $$;
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)

