-- Stored procedure definition script USP_Cases_ChangeOrganization for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Cases_ChangeOrganization(v_numDomainID NUMERIC(18,0),    
	v_numUserCntID NUMERIC(18,0),
	v_numCaseId NUMERIC(18,0),
	v_numNewDivisionID NUMERIC(18,0),
	v_numNewContactID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldDivisionID  NUMERIC(18,0);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   select   numDivisionID INTO v_numOldDivisionID FROM Cases WHERE numDomainID = v_numDomainID AND numCaseId = v_numCaseId;

   BEGIN
      -- BEGIN TRANSACTION
UPDATE
      Cases
      SET
      numDivisionID = v_numNewDivisionID,numContactId = v_numNewContactID
      WHERE
      numDomainID = v_numDomainID
      AND numCaseId = v_numCaseId;
      INSERT INTO RecordOrganizationChangeHistory(numDomainID,numCaseID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified)
		VALUES(v_numDomainID,v_numCaseId,v_numOldDivisionID,v_numNewDivisionID,v_numUserCntID,TIMEZONE('UTC',now()));
	
      -- COMMIT

EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


