-- Stored procedure definition script USP_GetImportantItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetImportantItems(v_PageSize INTEGER ,                      
v_CurrentPage INTEGER,                      
v_srchSubjectBody VARCHAR(100) DEFAULT '',                      
--@srchEmail as varchar(100) ='',                      
v_srchAttachmenttype VARCHAR(100) DEFAULT '',                             
INOUT v_TotRecs INTEGER  DEFAULT NULL        ,              
v_ToEmail VARCHAR(100) DEFAULT NULL  ,    
v_columnName VARCHAR(50) DEFAULT NULL ,    
v_columnSortOrder VARCHAR(4) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                
   v_lastRec  INTEGER;                                                
   v_strSql  VARCHAR(8000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                
                                               
               
   if v_columnName = 'FromName' then
 
      v_columnName :=  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)';
   end if;    
   if v_columnName = 'FromEmail' then
 
      v_columnName := 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)';
   end if;           
            
   v_strSql := 'With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,      
ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS  RowNumber       
from emailHistory                          
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID               
join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID               
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId               
where emailHistory.tinttype=4 ';      
           
   if v_srchSubjectBody <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and (vcSubject  like ''%' || coalesce(v_srchSubjectBody,'') || '%'' or vcBodyText like ''%' || coalesce(v_srchSubjectBody,'') || '%''     
or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where       
(numemailid in (select numEmailid from emailmaster where vcemailid like ''%' || coalesce(v_srchSubjectBody,'') || '%'')) or  vcName like ''%' || coalesce(v_srchSubjectBody,'') || '%'')) ';
   end if;          
   if v_ToEmail <> '' then 
      v_strSql := coalesce(v_strSql,'') || '         
      and (EmailMaster.vcEmailId like ''' || coalesce(v_ToEmail,'') || ''' and EmailHStrToBCCAndCC.tintType<>4 )  ';
   end if;      
   if v_srchAttachmenttype <> '' then 
      v_strSql := coalesce(v_strSql,'') || '        
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls          
  where EmailHstrAttchDtls.vcAttachmentType like ''%' || coalesce(v_srchAttachmenttype,'') || '%'')';
   end if;      
      
      
           
   v_strSql := coalesce(v_strSql,'') || ')               
 select RowNumber,      
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,            
 dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                       
--isnull(vcFromEmail,'''') as FromEmail,                                
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                           
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                          
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                               
emailHistory.numEmailHstrID,                      
isnull(vcSubject,'''') as vcSubject,                                
convert(varchar(max),vcBody) as Body,                        
dbo.FormatedDateFromDate(bintCreatedOn,numDomainId) as created,                        
isnull(vcItemId,'''') as ItemId,                        
isnull(vcChangeKey,'''') as ChangeKey,                        
isnull(bitIsRead,false) as IsRead,                        
isnull(vcSize,0) as vcSize,                        
isnull(bitHasAttachments,false) as HasAttachments,                       
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                      
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                       
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,                        
isnull(emailHistory.tintType,1) as type,                        
isnull(chrSource,''B'') as chrSource,                    
isnull(vcCategory,''white'') as vcCategory             
 from tblSubscriber T            
 join emailHistory             
 on emailHistory.numEmailHstrID=T.numEmailHstrID       
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID       
where emailHistory.tinttype=4  and          
  RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '        
union           
 select 0 as RowNumber,null,null,null,null,null,count(*),null,null,      
null,null,null,null,null,null,null,null,null,      
null,1,null,null from tblSubscriber  order by RowNumber';      
   RAISE NOTICE '%',v_strSql;       
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


