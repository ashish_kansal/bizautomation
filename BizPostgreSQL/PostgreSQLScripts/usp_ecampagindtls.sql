-- Stored procedure definition script USP_EcampaginDTls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EcampaginDTls(v_numECampaignID NUMERIC(9,0) DEFAULT 0,  
v_byteMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select vcECampName,txtDesc,coalesce(dtStartTime,LOCALTIMESTAMP) AS dtStartTime,coalesce(fltTimeZone,0) AS fltTimeZone,coalesce(tintTimeZoneIndex,0) AS tintTimeZoneIndex, coalesce(tintFromField,1) AS tintFromField,coalesce(numFromContactID,0) AS numFromContactID from ECampaign
      where numECampaignID = v_numECampaignID;
      open SWV_RefCur2 for
      select numECampDTLId,numEmailTemplate,numActionItemTemplate, tintDays, tintWaitPeriod, coalesce(numFollowUpID,0) AS numFollowUpID from ECampaignDTLs
      where numECampID = v_numECampaignID;
   end if;  
   if v_byteMode = 1 then

      UPDATE ECampaign SET bitDeleted = true WHERE numECampaignID = v_numECampaignID;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_ECampaignHstr]    Script Date: 07/26/2008 16:15:47 ******/
   RETURN;
END; $$;


