-- Stored procedure definition script USP_GetItemsForCreatingOpp for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemsForCreatingOpp(v_numOppID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   OppOld.numoppitemtCode,
 OppOld.numItemCode,
 i.vcItemName,
 OppOld.vcItemDesc as "Desc",
 OppOld.vcType AS ItemType,
 OppOld.numUnitHour,
 OppOld.monPrice,
 OppOld.monTotAmount,
 (SELECT   vcPathForImage FROM ItemImages
      WHERE  numItemCode  = i.numItemCode
      AND bitDefault = true AND bitIsImage = true LIMIT 1)
   AS vcPathForImage,
 OppOld.vcItemDesc,
 WItems.numWareHouseID,
 OppOld.numWarehouseItmsID ,
 0 as Op_Flag,
 vcWarehouse as Warehouse,
 i.bitSerialized,
 fn_GetAttributes(OppOld.numWarehouseItmsID,i.bitSerialized) as Attributes,
    vcpOppName,
    vcCompanyName,
 coalesce(OppNewMst.numOppId,0) as numoppIDNew,
    OppNew.monPrice as NewPrice
   from OpportunityItems OppOld
   join Item i
   on OppOld.numItemCode = i.numItemCode
   left join  WareHouseItems  WItems
   on   WItems.numWareHouseItemID = OppOld.numWarehouseItmsID
   left join  Warehouses  W
   on   W.numWareHouseID = WItems.numWareHouseID
   left join OpportunityItemLinking OppLink
   on numOppOldItemID = OppOld.numoppitemtCode
   left join OpportunityItems OppNew
   on numOppNewItemID = OppNew.numoppitemtCode
   left join  OpportunityMaster OppNewMst
   on OppNewMst.numOppId = OppNew.numOppId
   left join DivisionMaster Div
   on Div.numDivisionID = OppNewMst.numDivisionId
   left join CompanyInfo Com
   on Com.numCompanyId = Div.numCompanyID
   where OppOld.numOppId = v_numOppID;
END; $$;

