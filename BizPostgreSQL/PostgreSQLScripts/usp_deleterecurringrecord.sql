-- Stored procedure definition script USP_DeleteRecurringRecord for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteRecurringRecord(v_numOppRecID NUMERIC,
v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OpportunityRecurring WHERE numOppRecID = v_numOppRecID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetCompSpecificValues]    Script Date: 07/26/2008 16:16:46 ******/



