CREATE OR REPLACE FUNCTION USP_GetReceivePaymentDetailsForBizDocs(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
 v_numOppId NUMERIC(9,0) DEFAULT 0,  
 v_OppBizDocID NUMERIC(9,0) DEFAULT 0,
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
 v_dtFromDate TIMESTAMP DEFAULT NULL,
 v_dtToDate TIMESTAMP DEFAULT NULL,
 v_tintType SMALLINT DEFAULT 0,
 v_numDivisionId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_tintType = 1 OR v_tintType = 2 then     -- 1 : SO Receivable,2 : PO Payable
      IF v_tintType = 1 then --Sales

         open SWV_RefCur for
         Select DD.numDepositID AS numReferenceID,DD.numDepositeDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,
   DD.numOppBizDocsID as numBizDocsId, Opp.vcpOppName,DD.monAmountPaid As Amount,OppBizDocs.vcBizDocID,
	fn_GetListItemName(DM.numPaymentMethod)  as PaymentMethod,
   fn_GetContactName(DM.numCreatedBy) || ' - ' || FormatedDateTimeFromDate(DM.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId)  As CreatedBy,
   DD.vcMemo as Memo,coalesce(DD.vcReference,'') || ' ' || OppBizDocs.vcRefOrderNo as Reference,Opp.tintopptype,C.vcCompanyName,FormatedDateFromDate(DM.dtDepositDate,v_numDomainId) AS dtPaymentDate
         FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
         Inner Join OpportunityBizDocs OppBizDocs on DD.numOppBizDocsID = OppBizDocs.numOppBizDocsId
         Inner Join OpportunityMaster Opp On OppBizDocs.numoppid = Opp.numOppId
         Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId
         Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
         Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
         WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  DM.numDomainId = v_numDomainId
         AND DM.tintDepositePage = 2
         and DM.dtDepositDate <= v_dtToDate AND  DM.dtDepositDate >= v_dtFromDate
         AND Opp.tintopptype = 1 AND (Opp.numOppId = v_numOppId OR v_numOppId = 0) and (OppBizDocs.numOppBizDocsId = v_OppBizDocID OR v_OppBizDocID = 0)
         AND (Opp.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL);
      ELSEIF v_tintType = 2
      then --Purchase

         open SWV_RefCur for
         Select BPH.numBillPaymentID AS numReferenceID,BPD.numBillPaymentDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,
   BPD.numOppBizDocsID as numBizDocsId, Opp.vcpOppName,BPD.monAmount As Amount,OppBizDocs.vcBizDocID,
	fn_GetListItemName(BPH.numPaymentMethod)  as PaymentMethod,
   fn_GetContactName(BPH.numCreatedBy) || ' - ' || FormatedDateTimeFromDate(BPH.dtCreateDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId)  As CreatedBy,
   '' as Memo,coalesce(OppBizDocs.vcRefOrderNo,'') as Reference,Opp.tintopptype,C.vcCompanyName,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainId) AS dtPaymentDate   FROM
         BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
         LEFT OUTER JOIN CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.numReferenceID = 8
         Inner Join OpportunityBizDocs OppBizDocs on BPD.numOppBizDocsID = OppBizDocs.numOppBizDocsId
         Inner Join OpportunityMaster Opp On OppBizDocs.numoppid = Opp.numOppId
         Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId
         Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
         Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
         WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  BPH.numDomainId = v_numDomainId
         and BPH.dtPaymentDate <= v_dtToDate AND  BPH.dtPaymentDate >= v_dtFromDate
         AND Opp.tintopptype = 2 AND (Opp.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL);
      end if;
   end if;
   RETURN;
END; $$;


