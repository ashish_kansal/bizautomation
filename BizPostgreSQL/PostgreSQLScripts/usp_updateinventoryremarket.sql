-- Stored procedure definition script USP_UpdateInventoryRemarket for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateInventoryRemarket(v_numDomainID NUMERIC(9,0),        
v_FilePath VARCHAR(500),        
v_numUserCntID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_varSQL  VARCHAR(500);        
        
   v_numClassID  NUMERIC;      
   v_vcClassNum  VARCHAR(100);      
--declare @ModelID as varchar(100)      
   v_Warehouse  VARCHAR(200);      
   v_numWareHouseID  NUMERIC(9,0);      
   v_numWareHouseItemID  NUMERIC(9,0);      
   v_numItemID  NUMERIC(9,0);      
   v_Price  DECIMAL(20,5);      
   v_Chapter  NUMERIC;      
   v_SubChapter  NUMERIC;      
   v_ChapterItemID  NUMERIC;    
   v_ClassStatusItemID  NUMERIC;   
   v_SubChapterItemID  NUMERIC;      
   v_vcChapter  VARCHAR(100);      
   v_vcSubChapter  VARCHAR(100); 
   v_vcClassStatus  VARCHAR(100);   
   v_numCategoryID  NUMERIC(9,0);     
   v_numSubCategoryID  NUMERIC(9,0); 
   v_dtStartDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP;
   v_ClassStatusID  NUMERIC;

     
      
   v_CusChapterItemID  NUMERIC;      
   v_CusSubChapterItemID  NUMERIC;  
   v_CusStartDate  NUMERIC;      
   v_CusEndDate  NUMERIC; 
   v_CusClassNum  NUMERIC;
   v_CusClassStatus  NUMERIC;
   SWV_RowCount INTEGER;
BEGIN
   truncate table remarket;        
   truncate table RemarketDTL;       
   v_varSQL := 'COPY Remarket from ''' || coalesce(v_FilePath,'') || ''' with(DELIMITER ''~'')';        
   EXECUTE v_varSQL;       
        
        
   insert into  Item(vcSKU,vcModelID,vcItemName, txtItemDesc, charItemType, monListPrice, --dtDateEntered, 
numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate, numModifiedBy,numCOGsChartAcntId,
numAssetChartAcntId,numIncomeChartAcntId)
   select CLASSNUM,COURSECODE,COURSETITLE,CAST(DESCR AS VARCHAR(1000)),'P',PRICE,
v_numDomainID,v_numUserCntID,LOCALTIMESTAMP,LOCALTIMESTAMP,v_numUserCntID,957,994,932 from remarket
   where CLASSNUM not in(select vcSKU from Item where numDomainID = v_numDomainID);        
        
   update Item Tab1 set vcItemName = COURSETITLE,txtItemDesc = DESCR,monListPrice = PRICE,bintModifiedDate = LOCALTIMESTAMP,
   numModifiedBy = v_numUserCntID
   from(select CLASSNUM as CLASSNUM,COURSETITLE,CAST(DESCR AS VARCHAR(1000)) as DESCR,PRICE from remarket) Tab2
   where Tab1.vcSKU = Tab2.CLASSNUM AND numDomainID = v_numDomainID;       
      
      
   insert into Warehouses(vcWareHouse,vcWCity,numWState,numDomainID,vcWStreet,numWCountry,vcWPinCode)
   select distinct(FACNAME),CITY,coalesce((select  numStateID from State where vcState = R.STATE and numDomainID = v_numDomainID LIMIT 1),0),v_numDomainID,'',0,''
   from remarket R where FACNAME not in(select vcWareHouse from Warehouses where numDomainID = v_numDomainID);      
      
   update Warehouses Tab1 set vcWCity = CITY,numWState = numStateID,numWCountry = 11708
   from(select distinct(FACNAME),CITY,coalesce((select  numStateID from State where vcState = R.STATE and numDomainID = v_numDomainID LIMIT 1),0) as numStateID
   from remarket R) Tab2
   where Tab1.vcWareHouse = Tab2.FACNAME AND numDomainID = v_numDomainID;      
      
      
   insert  into RemarketDTL(vcClassNum)
   select CLASSNUM from remarket;      
      
      
   select   Fld_id INTO v_CusChapterItemID from CFW_Fld_Master where FLd_label = 'Chapter' and numDomainID = v_numDomainID;      
   select   Fld_id INTO v_CusSubChapterItemID from CFW_Fld_Master where FLd_label = 'SubChapter' and numDomainID = v_numDomainID;  
   select   Fld_id INTO v_CusStartDate from CFW_Fld_Master where FLd_label = 'Start Date' and numDomainID = v_numDomainID; 
   select   Fld_id INTO v_CusEndDate from CFW_Fld_Master where FLd_label = 'End Date' and numDomainID = v_numDomainID; 
   select   Fld_id INTO v_CusClassNum from CFW_Fld_Master where FLd_label = 'ClassNum' and numDomainID = v_numDomainID; 
   select   Fld_id INTO v_CusClassStatus from CFW_Fld_Master where FLd_label = 'Class Status' and numDomainID = v_numDomainID;    
      
      
   select   numListID INTO v_Chapter from listmaster where vcListName = 'Chapter' and numDomainID = v_numDomainID;      
      
   select   numListID INTO v_SubChapter from listmaster where vcListName = 'SubChapter' and numDomainID = v_numDomainID;   

   select   numListID INTO v_ClassStatusID from listmaster where vcListName = 'Class Status' and numDomainID = v_numDomainID;   
      
      
      
      
   if v_Chapter > 0 then

      drop table IF EXISTS tt_TEMP2 CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP2 AS
         select distinct(Chapter)  from remarket;
      insert into Listdetails(vcData,numListID, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate,numDomainid, constFlag, sintOrder)
      select CAST(Chapter AS VARCHAR(50)),v_Chapter,v_numUserCntID,LOCALTIMESTAMP,v_numUserCntID,LOCALTIMESTAMP,v_numDomainID,0,0
      from tt_TEMP2 where CAST(Chapter AS VARCHAR(50)) not in(select vcData from Listdetails where numDomainid = v_numDomainID and numListID = v_Chapter);
      drop table IF EXISTS tt_TEMP2 CASCADE;
   end if;      
   if v_SubChapter > 0 then

      drop table IF EXISTS tt_TEMP1 CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP1 AS
         select distinct(SubChapter)  from remarket;
      insert into Listdetails(vcData,numListID, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate,numDomainid, constFlag, sintOrder)
      select CAST(SubChapter AS VARCHAR(50)),v_SubChapter,v_numUserCntID,LOCALTIMESTAMP,v_numUserCntID,LOCALTIMESTAMP,v_numDomainID,0,0
      from tt_TEMP1 where CAST(SubChapter AS VARCHAR(50)) not in(select vcData from Listdetails where numDomainid = v_numDomainID and numListID = v_SubChapter);
      drop table IF EXISTS tt_TEMP1 CASCADE;
   end if;      
      
   v_numClassID := 0;      
   select   numClassID, vcClassNum INTO v_numClassID,v_vcClassNum from RemarketDTL     LIMIT 1;       
   while v_numClassID > 0 LOOP
      select   FACNAME, PRICE, Chapter, SubChapter, CLASSAT, StartDate, EndDate INTO v_Warehouse,v_Price,v_vcChapter,v_vcSubChapter,v_vcClassStatus,v_dtStartDate,
      v_dtEndDate from  remarket where CLASSNUM = v_vcClassNum;
      select   numItemCode INTO v_numItemID from Item where vcSKU = v_vcClassNum and numDomainID = v_numDomainID;
      delete from CFW_FLD_Values_Item where RecId = v_numItemID and Fld_ID in(v_CusChapterItemID,v_CusSubChapterItemID,v_CusStartDate,v_CusEndDate,v_CusClassNum,
      v_CusClassStatus);
      select   numListItemID INTO v_ChapterItemID from Listdetails where vcData = v_vcChapter and numDomainid = v_numDomainID and numListID = v_Chapter    LIMIT 1;
      select   numListItemID INTO v_SubChapterItemID from Listdetails where vcData = v_vcSubChapter and numDomainid = v_numDomainID and numListID = v_SubChapter    LIMIT 1;
      select   numListItemID INTO v_ClassStatusItemID from Listdetails where vcData = v_vcClassStatus and numDomainid = v_numDomainID and numListID = v_ClassStatusID    LIMIT 1;
      insert into CFW_FLD_Values_Item  values(v_CusChapterItemID,v_ChapterItemID,v_numItemID);
    
      insert into CFW_FLD_Values_Item  values(v_CusSubChapterItemID,v_SubChapterItemID,v_numItemID);
	
      insert into CFW_FLD_Values_Item  values(v_CusStartDate,v_dtStartDate,v_numItemID);
	
      insert into CFW_FLD_Values_Item  values(v_CusEndDate,v_dtEndDate,v_numItemID);
	
      insert into CFW_FLD_Values_Item  values(v_CusClassNum,v_vcClassNum,v_numItemID);
	
      insert into CFW_FLD_Values_Item  values(v_CusClassStatus,v_ClassStatusItemID,v_numItemID);
 
      v_numSubCategoryID := 0;
      select   numCategoryID INTO v_numSubCategoryID from Category where vcCategoryName = SUBSTR(CAST(v_vcSubChapter AS VARCHAR(50)),1,50) and numDomainID = v_numDomainID;
      v_numCategoryID := 0;
      select   numCategoryID INTO v_numCategoryID from Category where vcCategoryName = v_vcChapter and numDomainID = v_numDomainID;
      if v_vcSubChapter <> 'No Value' then
    
         if not exists(select * from ItemCategory where numItemID = v_numItemID and numCategoryID = v_numSubCategoryID) then
            insert into ItemCategory  values(v_numItemID,v_numSubCategoryID);
         end if;
      else
         if not exists(select * from ItemCategory where numItemID = v_numItemID and numCategoryID = v_numCategoryID) then
            insert into ItemCategory  values(v_numItemID,v_numCategoryID);
         end if;
      end if;    
        
    
--    if @numCategoryID>0    
--    begin    
--  if not exists(select * from ItemCategory where numItemID=@numItemID and numCategoryID=@numCategoryID)    
--        insert into ItemCategory values(@numItemID,@numCategoryID)    
--    
-- end    
       
    
    
    
      select   numWareHouseID INTO v_numWareHouseID from Warehouses where vcWareHouse = v_Warehouse and numDomainID = v_numDomainID;
      if exists(select * from WareHouseItems where numItemID = v_numItemID and numWareHouseID = v_numWareHouseID) then
   
         select   numWareHouseItemID INTO v_numWareHouseItemID from WareHouseItems where numItemID = v_numItemID and numWareHouseID = v_numWareHouseID;
         update WareHouseItems set   monWListPrice = v_Price,dtModified = LOCALTIMESTAMP  where numWareHouseItemID = v_numWareHouseItemID;
      else
         insert into WareHouseItems(numItemID, numWareHouseID, monWListPrice,dtModified)
      values(v_numItemID,v_numWareHouseID,v_Price,LOCALTIMESTAMP);
     
         v_numWareHouseItemID := CURRVAL('WareHouseItems_seq');
      end if;
      update RemarketDTL set numItemID = v_numItemID,numWareHouseItemID = v_numWareHouseItemID where numClassID = v_numClassID;
      select   numClassID, vcClassNum INTO v_numClassID,v_vcClassNum from RemarketDTL where numClassID > v_numClassID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numClassID := 0;
      end if;
   END LOOP; 
/****** Object:  StoredProcedure [dbo].[USP_UpdateItem]    Script Date: 07/26/2008 16:21:42 ******/
   RETURN;
END; $$;


