-- Stored procedure definition script USP_GetUserReportList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUserReportList(v_numDomainID NUMERIC(9,0) DEFAULT 0,         
v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select URL.numReportID,vcFileName,RPtHeading,RptDesc,tintReportType from PageMaster
   join ReportList on numPageID = NumID
   join UserReportList URL on RptID = URL.numReportID
   where numModuleID = 8 AND URL.numDomainID = v_numDomainID AND URL.numUserCntID = v_numUserCntID and URL.tintReportType = 0 AND bitEnable = true
   UNION
   select URL.numReportID,'frmCustomReportRun.aspx?ReptID=' || URL.numReportID as vcFileName ,
vcReportName as RPtHeading,vcReportDescription as RptDesc,URL.tintReportType
   from ReportListMaster RLM
   join UserReportList URL on RLM.numReportID = URL.numReportID
   WHERE URL.numDomainID = v_numDomainID AND URL.numUserCntID = v_numUserCntID and URL.tintReportType = 1;
END; $$;
