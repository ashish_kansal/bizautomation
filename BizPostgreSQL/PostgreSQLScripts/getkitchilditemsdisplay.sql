-- Function definition script GetKitChildItemsDisplay for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetKitChildItemsDisplay(v_numItemKitID NUMERIC(18,0)
	,v_numChildKitItemID  NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDisplayText  TEXT;
   v_vcItemName  VARCHAR(300);
   v_vcSKU  VARCHAR(100);
   v_vcModelID  VARCHAR(100);
   v_tintView  SMALLINT;
   v_vcFields  VARCHAR(1000);
BEGIN
   select   numItemCode, coalesce(vcItemName,''), coalesce(vcSKU,''), coalesce(vcModelID,'') INTO v_numItemCode,v_vcItemName,v_vcSKU,v_vcModelID FROM Item WHERE numItemCode = v_numItemCode;

   select   tintView, coalesce(vcFields,'') INTO v_tintView,v_vcFields FROM ItemDetails WHERE numItemKitID = v_numItemKitID AND numChildItemID = v_numChildKitItemID;

   IF LENGTH(v_vcFields) > 0 then
	
      IF v_tintView = 2 then
		
         v_vcDisplayText := '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">';
         IF POSITION(',0~1,' IN CONCAT(',',v_vcFields,',')) <> 0 then
			
            v_vcDisplayText := CONCAT(v_vcDisplayText,'<li><img class="ul-child-kit-item-image" src="',(CASE
            WHEN coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') = ''
            THEN '../Images/DefaultProduct.png'
            ELSE CONCAT('##URL##/',(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1))
            END),
            '" alt="',v_vcItemName,
            '"/></li><li>');
         end if;
      ELSE
         v_vcDisplayText := '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">';
      end if;
      IF POSITION(',0~2,' IN CONCAT(',',v_vcFields,',')) <> 0 AND LENGTH(v_vcItemName) > 0 then
		
         v_vcDisplayText := CONCAT(v_vcDisplayText,'<li class=" ul-child-kit-item-name">',v_vcItemName,'</li>');
      end if;
      IF POSITION(',0~3,' IN CONCAT(',',v_vcFields,',')) <> 0 AND LENGTH(v_vcSKU) > 0 then
		
         v_vcDisplayText := CONCAT(v_vcDisplayText,'<li>',v_vcSKU,'</li>');
      end if;
      IF POSITION(',0~4' IN CONCAT(',',v_vcFields,',')) <> 0 AND LENGTH(v_vcModelID) > 0 then
		
         v_vcDisplayText := CONCAT(v_vcDisplayText,'<li>',v_vcModelID,'</li>');
      end if;
      select   CONCAT(v_vcDisplayText,'<li>',GetCustFldValueItem(CAST(REPLACE(OutParam,'1~','') AS NUMERIC),v_numItemCode),'</li>') INTO v_vcDisplayText FROM
      SplitString(v_vcFields,',') WHERE
      POSITION('1~' IN OutParam) <> 0;
      IF POSITION(',0~1,' IN CONCAT(',',v_vcFields,',')) <> 0 AND v_tintView = 1 then
		
         v_vcDisplayText := CONCAT('<ul class="list-inline ul-child-kit-item"><li><img class="ul-child-kit-item-image" src="',(CASE
         WHEN coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') = ''
         THEN '../Images/DefaultProduct.png'
         ELSE CONCAT('##URL##/',(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1))
         END),'" alt="',v_vcItemName,'"/></li><li>',
         v_vcDisplayText,'</ul></li></ul>');
      ELSE
         v_vcDisplayText := CONCAT(v_vcDisplayText,'</ul>');
      end if;
   ELSE
      v_vcDisplayText := CONCAT(v_vcItemName,' (Item Code:',v_numItemCode,(CASE WHEN LENGTH(v_vcSKU) > 0 THEN CONCAT(', SKU:',v_vcSKU) ELSE '' END),')');
   end if;

   RETURN v_vcDisplayText;
END; $$;

