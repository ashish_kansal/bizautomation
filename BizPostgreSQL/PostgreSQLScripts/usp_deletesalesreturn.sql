-- Stored procedure definition script Usp_DeleteSalesReturn for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_DeleteSalesReturn(v_numReturnHeaderID NUMERIC(9,0),
    v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBillPaymentIDRef  NUMERIC(18,0);
   v_numDepositIDRef  NUMERIC(18,0);
   v_monAmount  DECIMAL(20,5);
BEGIN
   IF EXISTS(SELECT  numReturnHeaderID
   FROM    ReturnHeader
   WHERE   numReturnHeaderID = v_numReturnHeaderID
   AND coalesce(tintReceiveType,0) <> 0) then
            
      RAISE EXCEPTION 'CreditMemo_Refund';
      RETURN;
   end if;

   IF EXISTS(SELECT numDepositId FROM DepositMaster WHERE coalesce(numReturnHeaderID,0) = v_numReturnHeaderID
   AND numDomainId = v_numDomainId
   And coalesce(monRefundAmount,0) > 0) then
            
      RAISE EXCEPTION 'Deposit_Refund_Payment';
      RETURN;
   end if;
            
   IF EXISTS(SELECT numBillPaymentID FROM BillPaymentHeader WHERE coalesce(numReturnHeaderID,0) = v_numReturnHeaderID
   AND numDomainId = v_numDomainId
   And coalesce(monRefundAmount,0) > 0) then
            
      RAISE EXCEPTION 'BillPayment_Refund_Payment';
      RETURN;
   end if;

   DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID = v_numReturnHeaderID;
		
   DELETE  FROM ReturnItems
   WHERE   numReturnHeaderID = v_numReturnHeaderID;
        
        /*Start : If Refund against Deposit or Bill Payment*/
   v_numBillPaymentIDRef := 0;
   v_numDepositIDRef := 0;
   v_monAmount := 0;
        
   select   coalesce(numBillPaymentIDRef,0), coalesce(numDepositIDRef,0), coalesce(monAmount,0) INTO v_numBillPaymentIDRef,v_numDepositIDRef,v_monAmount FROM ReturnHeader WHERE numReturnHeaderID = v_numReturnHeaderID;
        
   IF v_numBillPaymentIDRef > 0 then
        
      UPDATE BillPaymentHeader SET monRefundAmount = coalesce(monRefundAmount,0) -v_monAmount
      WHERE numDomainId = v_numDomainId AND numBillPaymentID = v_numBillPaymentIDRef;
   end if;

   IF v_numDepositIDRef > 0 then
        
      UPDATE DepositMaster SET monRefundAmount = coalesce(monRefundAmount,0) -v_monAmount
      WHERE numDomainId = v_numDomainId AND numDepositId = v_numDepositIDRef;
   end if;

   DELETE  FROM ReturnItems
   WHERE   ReturnItems.numReturnHeaderID = v_numReturnHeaderID; 
   DELETE  FROM ReturnHeader
   WHERE   numReturnHeaderID = v_numReturnHeaderID
   AND numDomainID = v_numDomainId; 

   DELETE FROM OpportunityMasterTaxItems WHERE numReturnHeaderID = v_numReturnHeaderID;
   DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID = v_numReturnHeaderID;
END; $$;




