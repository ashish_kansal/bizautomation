-- Stored procedure definition script usp_GetStageItemDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetStageItemDetails(
--              
v_numDomainid NUMERIC DEFAULT 0,
    v_slpid NUMERIC DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0,
	v_numOppId NUMERIC DEFAULT 0,
	v_numProjectId NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
--    @numStageNo NUMERIC = 0,
   AS $$
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      SELECT  numStageDetailsId AS "numStageDetailsId",
                numStagePercentageId AS "numStagePercentageId",
                vcStageName AS "vcStageName",
                sp.numdomainid AS "numdomainid",
                sp.numCreatedBy AS "numCreatedBy",
                sp.bintCreatedDate AS "bintCreatedDate",
                sp.numModifiedBy AS "numModifiedBy",
                sp.bintModifiedDate AS "bintModifiedDate",
                slp_id AS "slp_id",
                sp.numAssignTo AS "numAssignTo",
                vcMilestoneName AS "vcMilestoneName",
				coalesce((select string_agg(ST.vcTaskName || '(' || coalesce((SELECT(AU.vcFirstName || ' ' || AU.vcLastname) FROM AdditionalContactsInformation AS AU WHERE AU.numContactId = ST.numAssignTo LIMIT 1),
            '-') || ')',', ')
            from StagePercentageDetailsTask ST
            where ST.numStageDetailsId =sp.numStageDetailsId AND coalesce(ST.numOppId,0) = 0 AND  coalesce(ST.numProjectId,0) = 0
            GROUP BY ST.numStageDetailsId),'-') As "TaskName",
				(SELECT coalesce(SUM(numHours),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId = sp.numStageDetailsId) As "numHours",
				(SELECT coalesce(SUM(numMinutes),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId = sp.numStageDetailsId)  As "numMinutes",
                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
      ELSE A.vcFirstName || ' ' || A.vcLastname
      END AS "vcAssignTo",
                coalesce(tintPercentage,0) AS "tintPercentage",
                coalesce(bitClose,false) AS "bitClose",
                tintConfiguration AS "tintConfiguration",
                coalesce(vcDescription,'') AS "vcDescription",
                coalesce(numParentStageID,0) AS "numParentStageID",
				coalesce(bitIsDueDaysUsed,false) AS "bitIsDueDaysUsed",
				coalesce(numTeamId,0) AS "numTeamId",
				coalesce(bitRunningDynamicMode,false) AS "bitRunningDynamicMode",
                coalesce(intDueDays,0) AS "intDueDays",(select count(*) from StageDependency where numStageDetailId = sp.numStageDetailsId) AS "numDependencyCount",
				(TO_CHAR(sp.dtStartDate,'mm/dd/yyyy') || SUBSTR(TO_CHAR(sp.dtStartDate,'Mon dd yyyy hh:miAM'),length(TO_CHAR(sp.dtStartDate,'Mon dd yyyy hh:miAM')) -8+1)) AS "StageStartDate",
				(select fn_calculateTotalHoursMinutesByStageDetails(sp.numStageDetailsId,v_numOppId,v_numProjectId)) as "dayshoursTaskDuration",
				coalesce(sp.numStageOrder,0) As "numStageOrder",
				CASE WHEN v_numProjectId > 0 THEN
         coalesce((SELECT coalesce(intTotalProgress,0) FROM ProjectProgress WHERE numDomainID = v_numDomainid AND numProId = v_numProjectId),0)
      WHEN v_numOppId > 0 THEN
         coalesce((SELECT coalesce(intTotalProgress,0) FROM ProjectProgress WHERE numDomainID = v_numDomainid AND numOppId = v_numOppId),0) ELSE 0 END AS "intTotalProgress"
      FROM    StagePercentageDetails sp
      LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactId
      WHERE
      sp.numdomainid = v_numDomainid
      AND slp_id = v_slpid
      AND coalesce(sp.numProjectid,0) = v_numProjectId
      AND coalesce(sp.numOppid,0) = v_numOppId
      ORDER BY numstagepercentageid,numStageOrder ASC;
   ELSEIF v_tintMode = 1
   then -- Fill dropdown
      open SWV_RefCur for
      SELECT numStageDetailsId AS "numStageDetailsId",vcStageName AS "vcStageName" FROM StagePercentageDetails WHERE slp_id = v_slpid AND numdomainid = v_numDomainid
      AND LENGTH(vcStageName) > 1;
   ELSEIF v_tintMode = 2
   then
      open SWV_RefCur for
      SELECT numStageDetailsId AS "numStageDetailsId"
	  ,numStagePercentageId AS "numStagePercentageId"
	  ,tintConfiguration AS "tintConfiguration"
      ,vcStageName AS "vcStageName"
	  ,numdomainid AS "numdomainid"
	  ,numCreatedBy AS "numCreatedBy"
	  ,(Case bitFromTemplate when false then coalesce(fn_GetContactName(numCreatedBy),'-') else 'Template' end) as vcCreatedBy,FormatedDateFromDate(coalesce(bintCreatedDate,LOCALTIMESTAMP),v_numDomainid) AS "bintCreatedDate"
      ,numModifiedBy AS "numModifiedBy"
	  ,coalesce(fn_GetContactName(numModifiedBy),'-') as "vcModifiedBy"
      ,FormatedDateTimeFromDate(coalesce(bintModifiedDate,LOCALTIMESTAMP),v_numDomainid) AS "bintModifiedDate"
	  ,slp_id AS "slp_id"
      ,numAssignTo AS "numAssignTo"
	  ,coalesce(fn_GetContactName(numAssignTo),'-') as "vcAssignTo"
      ,vcMileStoneName AS "vcMileStoneName"
	  ,tintPercentage AS "tintPercentage"
	  ,coalesce(tinProgressPercentage,0) AS "tinProgressPercentage"
	  ,bitClose AS "bitClose"
      ,FormatedDateFromDate(coalesce(dtStartDate,LOCALTIMESTAMP),v_numDomainid) AS "dtStartDate"
	  ,FormatedDateFromDate(coalesce(dtEndDate,LOCALTIMESTAMP),v_numDomainid) AS "dtEndDate"
	  ,numParentStageID AS "numParentStageID"
	  ,intDueDays AS "intDueDays"
      ,numProjectid AS "numProjectid"
	  ,numOppid AS "numOppid"
	  ,vcDescription AS "vcDescription"
	  ,coalesce(fn_GetExpenseDtlsbyProStgID(numProjectid,numStageDetailsId,0::SMALLINT),cast(0 as TEXT)) as "numExpense"
	  ,coalesce(fn_GeTimeDtlsbyProStgID_BT_NBT(numProjectid,numStageDetailsId,1),'Billable Time (0)  Non Billable Time (0)') as "numTime"
	  ,(select count(*) from GenericDocuments where numRecID = numStageDetailsId and vcDocumentSection = 'PS') AS "numDocuments"
	  ,(select count(*) from StagePercentageDetails where numParentStageID = p.numStageDetailsId and numProjectid = p.numProjectid) as "numChildCount"
	  ,coalesce(bitTimeBudget,false) as "bitTimeBudget"
	  ,coalesce(bitExpenseBudget,false) as "bitExpenseBudget"
	  ,coalesce(monTimeBudget,0) as "monTimeBudget"
	  ,coalesce(monExpenseBudget,0) as "monExpenseBudget"
      FROM StagePercentageDetails p where numStageDetailsId = v_slpid;
   ELSEIF v_tintMode = 3
   then
      open SWV_RefCur for
      SELECT count(*) AS "Total"
      FROM StagePercentageDetails p where numProjectid = v_slpid and numdomainid = v_numDomainid;
   ELSEIF v_tintMode = 4
   then
      open SWV_RefCur for
      SELECT count(*) AS "Total"
      FROM StagePercentageDetails p where numOppid = v_slpid and numdomainid = v_numDomainid;
   ELSE
      open SWV_RefCur for
      SELECT numStageDetailsId AS "numStageDetailsId",vcStageName AS "vcStageName" FROM StagePercentageDetails WHERE slp_id = v_slpid AND numdomainid = v_numDomainid
      AND LENGTH(vcStageName) > 1;
   end if;
   RETURN;
END; $$;


