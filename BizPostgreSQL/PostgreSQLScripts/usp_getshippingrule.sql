DROP FUNCTION IF EXISTS USP_GetShippingRule;
CREATE OR REPLACE FUNCTION USP_GetShippingRule(v_numRuleID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_byteMode SMALLINT,
	v_numRelProfileID VARCHAR(50), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC;
   v_numProfile  NUMERIC;
BEGIN
   IF v_byteMode = 0 then
        
      open SWV_RefCur for
      SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
      ELSE tintFixedShippingQuotesMode END
      AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
					,numSiteID
					,bitItemClassification
      FROM    ShippingRules SR
      WHERE   (numRuleID = v_numRuleID
      OR v_numRuleID = 0)
      AND numDomainId = v_numDomainID;
   ELSEIF v_byteMode = 1
   then
      v_numRelationship := CAST((CASE WHEN split_part(replace(v_numRelProfileID,'-','.'),'.',1) = '' THEN '0' ELSE split_part(replace(v_numRelProfileID,'-','.'),'.',1) END) AS NUMERIC);
      v_numProfile := CAST((CASE WHEN split_part(replace(v_numRelProfileID,'-','.'),'.',2) = '' THEN '0' ELSE split_part(replace(v_numRelProfileID,'-','.'),'.',2) END) AS NUMERIC);
      open SWV_RefCur for
      SELECT minShippingCost,bitEnableStaticShippingRule,bitEnableShippingExceptions FROM Domain WHERE numDomainId = v_numDomainID;

      IF v_numRelationship > 0 AND v_numProfile > 0 then
		
         open SWV_RefCur2 for
         SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
         WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
         WHEN tintTaxMode = 3 THEN 'Always Tax'
         WHEN tintTaxMode = 0 THEN 'Do Not Tax'
         ELSE 'Tax Mode Not Selected'
         END AS vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode = 1 THEN 'Flat rate per item'
         WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
         WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
         WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
         WHEN tintFixedShippingQuotesMode = 0 THEN 'Flat rate per item'
         ELSE 'Fixed rate shipping quotes not selected'
         END  AS vcFixedShippingQuotesMode
				,COALESCE((SELECT string_agg(W.vcWareHouse,', ')
               FROM   Warehouses W
               WHERE   W.numWareHouseID  in(SELECT  Id
                  FROM    SplitIDs(SR.numWareHouseID,','))),'') AS vcShipFrom
                , COALESCE((SELECT string_agg(fn_GetState(numStateID) || ' ,' || SS.vcZipPostal,', ')
               FROM ShippingRuleStateList SS
               join ShippingRules S on SS.numRuleID = S.numRuleID
               WHERE SR.numRuleID = SS.numRuleID),'') AS vcShipTo
					,(CASE WHEN coalesce(SR.bitFreeShipping,false) = true
         THEN CONCAT(COALESCE((SELECT string_agg(CONCAT(intfrom,' - ',intTo,' pays $',CAST(monRate AS DECIMAL(20,2))),', ')
                     FROM ShippingServiceTypes SS
                     join ShippingRules S on SS.numRuleID = S.numRuleID
                     WHERE SR.numRuleID = SS.numRuleID),''),' , Free shipping when order amount reaches $',SR.FreeShippingOrderAmt)
         ELSE COALESCE((SELECT string_agg(CONCAT(intfrom,' - ',intTo,' pays $',CAST(monRate AS DECIMAL(20,2))),', ')
                  FROM ShippingServiceTypes SS
                  join ShippingRules S on SS.numRuleID = S.numRuleID
                  WHERE SR.numRuleID = SS.numRuleID),'')
         END) AS vcShippingCharges
				, (SELECT CONCAT((SELECT string_agg(vcData,', ') from Listdetails LD
               JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship
               WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),', ',(SELECT string_agg(vcData,', ') from Listdetails LD
               JOIN  ShippingRules S ON LD.numListItemID = S.numProfile
               WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo
         FROM    ShippingRules SR
         WHERE
         numDomainId = v_numDomainID AND numRelationship = v_numRelationship AND numProfile = v_numProfile;
      ELSE
         open SWV_RefCur2 for
         SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
         WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
         WHEN tintTaxMode = 3 THEN 'Always Tax'
         WHEN tintTaxMode = 0 THEN 'Do Not Tax'
         ELSE 'Tax Mode Not Selected'
         END AS vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode = 1 THEN 'Flat rate per item'
         WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
         WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
         WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
         WHEN tintFixedShippingQuotesMode = 0 THEN 'Flat rate per item'
         ELSE 'Fixed rate shipping quotes not selected'
         END  AS vcFixedShippingQuotesMode
				,COALESCE((SELECT string_agg(W.vcWareHouse,', ')
               FROM   Warehouses W
               WHERE   W.numWareHouseID  in(SELECT  Id
                  FROM    SplitIDs(SR.numWareHouseID,','))),'') AS vcShipFrom
                ,
					COALESCE((SELECT  string_agg(fn_GetListName(numCountryID,0::BOOLEAN) || ' - ' || COALESCE((SELECT string_agg(vcData,', ') FROM(SELECT string_agg(coalesce(fn_GetState(numStateID),'All States') || (CASE WHEN LENGTH(COALESCE((SELECT string_agg(s.vcZipPostal, ',')  From ShippingRuleStateList s
                  WHERE SR.numRuleID = s.numRuleID AND s.numCountryID = SRSL.numCountryID and s.numStateID = SRSL.numStateID),'')) > 0
               THEN CONCAT(' (',COALESCE((SELECT string_agg(s.vcZipPostal, ',')  From ShippingRuleStateList s
                  WHERE SR.numRuleID = s.numRuleID AND s.numCountryID = SRSL.numCountryID and s.numStateID = SRSL.numStateID),''),') ')
               ELSE ''
               END),', ') as vcData
               From ShippingRuleStateList SRSL
               WHERE SR.numRuleID = SRSL.numRuleID AND SRSL.numCountryID = SS.numCountryID GROUP BY SRSL.numStateID,numCountryID) X1),''),', ')
            
            FROM ShippingRuleStateList SS
            WHERE SR.numRuleID = SS.numRuleID
            GROUP BY numCountryID),'') AS vcShipTo
				,(CASE WHEN coalesce(SR.bitFreeShipping,false) = true
         THEN CONCAT((COALESCE((SELECT string_agg(CONCAT(intfrom,' - ',intTo,' pays $',CAST(monRate AS DECIMAL(20,2))),', ')
                     FROM ShippingServiceTypes SS
                     join ShippingRules s on SS.numRuleID = s.numRuleID
                     WHERE SR.numRuleID = SS.numRuleID),''),' , Free shipping when order amount reaches $',SR.FreeShippingOrderAmt))
         ELSE COALESCE((SELECT string_agg(CONCAT(intfrom,' - ',intTo,' pays $',CAST(monRate AS DECIMAL(20,2))),', ')
                  FROM ShippingServiceTypes SS
                  join ShippingRules s on SS.numRuleID = s.numRuleID
                  WHERE SR.numRuleID = SS.numRuleID),'')
         END) AS vcShippingCharges
				,(SELECT CONCAT(coalesce((SELECT string_agg(vcData,', ') from Listdetails LD
               JOIN  ShippingRules s ON LD.numListItemID = s.numRelationship
               WHERE LD.numListItemID = s.numRelationship AND s.numRuleID = SR.numRuleID),'All Relationships'),', ',coalesce((SELECT string_agg(vcData,', ') from Listdetails LD
               JOIN  ShippingRules s ON LD.numListItemID = s.numProfile
               WHERE LD.numListItemID = s.numProfile  AND s.numRuleID = SR.numRuleID),'All Profiles'))) AS vcAppliesTo
         FROM    ShippingRules SR
         WHERE
         numDomainId = v_numDomainID;
      end if;
   ELSEIF v_byteMode = 2
   then --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            
      open SWV_RefCur for
      SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = v_numRuleID AND numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


