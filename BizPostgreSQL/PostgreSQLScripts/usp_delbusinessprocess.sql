-- Stored procedure definition script usp_DelBusinessProcess for PostgreSQL
CREATE OR REPLACE FUNCTION usp_DelBusinessProcess(v_slpid NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE from StagePercentageDetailsTask where numStageDetailsId IN(SELECT numStageDetailsId from StagePercentageDetails where slp_id = v_slpid);
   DELETE from StagePercentageDetails where slp_id = v_slpid;
   delete from Sales_process_List_Master where Slp_Id = v_slpid;
	
   open SWV_RefCur for SELECT CAST('1' AS CHAR(1));
END; $$;














