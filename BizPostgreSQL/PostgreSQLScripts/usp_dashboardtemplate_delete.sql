-- Stored procedure definition script USP_DashboardTemplate_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DashboardTemplate_Delete(v_numDomainID NUMERIC(18,0)
	,v_numTemplateID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
DELETE FROM ReportDashboard WHERE numDomainID = v_numDomainID AND coalesce(numDashboardTemplateID,0) = v_numTemplateID AND numReportID NOT IN(SELECT numReportID FROM ReportListMaster WHERE coalesce(bitDefault,false) = true);
      UPDATE UserMaster SET numDashboardTemplateID = 0 WHERE numDomainID = v_numDomainID AND numDashboardTemplateID = v_numTemplateID;
      DELETE FROM DashboardTemplate WHERE numDomainID = v_numDomainID AND numTemplateID = v_numTemplateID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


