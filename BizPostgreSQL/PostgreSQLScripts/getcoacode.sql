-- Function definition script GetCOACode for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCOACode(v_numDomainID NUMERIC(9,0),v_numParentID NUMERIC(9,0))
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountTypeID  NUMERIC(9,0);
   v_vcNewAccountCode  VARCHAR(50);
   v_vcParentAccountCode  VARCHAR(50);
BEGIN
   select   numAccountTypeID, vcAccountCode INTO v_numAccountTypeID,v_vcParentAccountCode FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND numAccountTypeID = v_numParentID;

   RETURN coalesce(v_vcNewAccountCode,'');
END; $$;

