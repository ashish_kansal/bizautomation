-- Function definition script GetTimeSpendOnTask for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTimeSpendOnTask(v_numDomainID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_tintReturnType SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcTotalTimeSpendOnTask  VARCHAR(50) DEFAULT '';
   v_numTotalMinutes  INTEGER DEFAULT 0;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_tintLastAction  SMALLINT;
   v_dtLastActionTime  TIMESTAMP;

   v_tintAction  SMALLINT;
   v_dtActionTime  TIMESTAMP;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMTIMELong_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMTIMELONG CASCADE;
   CREATE TEMPORARY TABLE tt_TEMTIMELONG
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      tintAction SMALLINT,
      dtActionTime TIMESTAMP
   );
   INSERT INTO tt_TEMTIMELONG(tintAction
		,dtActionTime)
   SELECT
   tintAction
		,dtActionTime
   FROM
   StagePercentageDetailsTaskTimeLog SPDTTL
   WHERE
   SPDTTL.numDomainID = v_numDomainID
   AND SPDTTL.numTaskId = v_numTaskID
   ORDER BY
   dtActionTime,tintAction;

   select   COUNT(*) INTO v_iCount FROM tt_TEMTIMELONG;

   WHILE v_i <= v_iCount LOOP
      select   tintAction, dtActionTime INTO v_tintAction,v_dtActionTime FROM
      tt_TEMTIMELONG WHERE
      ID = v_i;
      IF v_i = 1 then
		
         IF v_tintAction <> 1 then
			
            v_vcTotalTimeSpendOnTask := 'Invalid time sequence';
            EXIT;
         ELSE
            v_tintLastAction := v_tintAction;
            v_dtLastActionTime := v_dtActionTime;
         end if;
      ELSE
         IF v_tintLastAction = 1 AND v_tintAction NOT IN(2,4) then
			
            v_vcTotalTimeSpendOnTask := 'Invalid time sequence';
            EXIT;
			--ELSE IF @tintLastAction = 2 AND @tintAction <> 3
         ELSEIF v_tintLastAction = 2 AND v_tintAction NOT IN(3,4)
         then
			
            v_vcTotalTimeSpendOnTask := 'Invalid time sequence';
            EXIT;
         ELSEIF v_tintLastAction = 3 AND v_tintAction NOT IN(2,4)
         then
			
            v_vcTotalTimeSpendOnTask := 'Invalid time sequence';
            EXIT;
         ELSEIF v_tintLastAction = 4
         then
			
            v_vcTotalTimeSpendOnTask := 'Invalid time sequence';
            EXIT;
         ELSE
            IF v_tintAction IN(2,4) then
				
               v_numTotalMinutes := v_numTotalMinutes::bigint+(EXTRACT(DAY FROM v_dtActionTime -v_dtLastActionTime)*60*24+EXTRACT(HOUR FROM v_dtActionTime -v_dtLastActionTime)*60+EXTRACT(MINUTE FROM v_dtActionTime -v_dtLastActionTime));
            end if;
            v_tintLastAction := v_tintAction;
            v_dtLastActionTime := v_dtActionTime;
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   IF v_vcTotalTimeSpendOnTask <> 'Invalid time sequence' then
	
      IF coalesce(v_tintReturnType,0) = 1 then
		
         RETURN CONCAT('<ul class="list-inline"><li style="padding-right:0px">',(CASE WHEN(CAST(v_numTotalMinutes::bigint AS decimal)/60) > 0 THEN CONCAT('<label style="color:#3366ff">',(v_numTotalMinutes::bigint/60),
            (CASE WHEN(CAST(v_numTotalMinutes::bigint AS decimal)/60) > 1 THEN ' Hours' ELSE ' Hour' END),'</label>') ELSE '' END),
         '</li><li>',(CASE WHEN(MOD(v_numTotalMinutes,60)) > 0 THEN CONCAT('<label>',MOD(v_numTotalMinutes,60),(CASE WHEN(MOD(v_numTotalMinutes,60)) > 1 THEN ' Minutes' ELSE ' Minute' END),'</label>') ELSE '' END),
         '</li></ul>');
      ELSEIF coalesce(v_tintReturnType,0) = 2
      then
		
         RETURN v_numTotalMinutes;
      ELSE
         RETURN TO_CHAR(CAST(v_numTotalMinutes::bigint AS decimal)/60,'00') || ':' || TO_CHAR(MOD(v_numTotalMinutes,60),'00');
      end if;
   ELSEIF coalesce(v_tintReturnType,0) = 2
   then
	
      v_vcTotalTimeSpendOnTask := '0';
   end if;
	
   RETURN v_vcTotalTimeSpendOnTask;
END; $$;

