-- Stored procedure definition script USP_GetOppItemsForAccounting for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC USP_GetOppItemsForAccounting 111925,169,1
CREATE OR REPLACE FUNCTION USP_GetOppItemsForAccounting(v_numOppId NUMERIC(9,0) DEFAULT NULL ,
      v_numDomainID NUMERIC(9,0) DEFAULT 0 ,
      v_numUserCntID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCurrencyID  NUMERIC(9,0);
   v_fltExchangeRate  DOUBLE PRECISION;

   v_fltCurrentExchangeRate  DOUBLE PRECISION;
   v_tintType  SMALLINT;                                                          
   v_vcPOppName  VARCHAR(100);                                                          
   v_bitPPVariance  BOOLEAN;
   v_numDivisionID  NUMERIC(9,0);
   v_vcBaseCurrency  VARCHAR(100);
   v_bitAutolinkUnappliedPayment  BOOLEAN; 
   v_vcForeignCurrency  VARCHAR(100);
   v_fltExchangeRateBizDoc  DOUBLE PRECISION; 
   v_strSQL  VARCHAR(8000);                                                          
   v_strSQLCusFields  VARCHAR(1000);                                                          
   v_strSQLEmpFlds  VARCHAR(500);                                                          
   v_intRowNum  INTEGER;                                                          
   v_numFldID  VARCHAR(15);                                                          
   v_vcFldname  VARCHAR(50);
   SWV_RowCount INTEGER;
BEGIN
   select   numCurrencyID, fltExchangeRate, tintopptype, vcpOppName, coalesce(bitPPVariance,false), numDivisionId INTO v_numCurrencyID,v_fltExchangeRate,v_tintType,v_vcPOppName,v_bitPPVariance,
   v_numDivisionID FROM    OpportunityMaster WHERE   numOppId = v_numOppId; 

   v_vcBaseCurrency := '';
   select   coalesce(C.varCurrSymbol,''), coalesce(bitAutolinkUnappliedPayment,false) INTO v_vcBaseCurrency,v_bitAutolinkUnappliedPayment FROM    Domain D
   LEFT JOIN Currency C ON D.numCurrencyID = C.numCurrencyID WHERE   D.numDomainId = v_numDomainID;

   v_vcForeignCurrency := '';
   select   coalesce(C.varCurrSymbol,'') INTO v_vcForeignCurrency FROM    Currency C WHERE   numCurrencyID = v_numCurrencyID;

   IF v_numCurrencyID > 0 then
      v_fltCurrentExchangeRate := GetExchangeRate(v_numDomainID,v_numCurrencyID);
   ELSE
      v_fltCurrentExchangeRate := v_fltExchangeRate;
   end if;                                                                                                                                          

   v_strSQLCusFields := '';                                                          
   v_strSQLEmpFlds := '';                                                          
   IF v_tintType = 1 then
      v_tintType := 7;
   ELSE
      v_tintType := 8;
   end if;                                                                                                                      
   select   numFieldId, vcFieldName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_intRowNum FROM    View_DynamicCustomColumns WHERE   Grp_id = v_tintType
   AND numDomainID = v_numDomainID
                --AND numAuthGroupID = @numBizDocId
   AND bitCustom = true   ORDER BY tintRow LIMIT 1;                                                          
   WHILE v_intRowNum > 0 LOOP
      v_strSQLCusFields := coalesce(v_strSQLCusFields,'')
      || ',  GetCustFldValueBizdoc(' || coalesce(v_numFldID,'') || ','
      || SUBSTR(CAST(v_tintType AS VARCHAR(2)),1,2)
      || ',opp.numoppitemtCode) as [' || coalesce(v_vcFldname,'') || ']';
      v_strSQLEmpFlds := coalesce(v_strSQLEmpFlds,'') || ',''-'' as "'
      || coalesce(v_vcFldname,'') || '"';
      select   numFieldId, vcFieldName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_intRowNum FROM    View_DynamicCustomColumns WHERE   Grp_id = v_tintType
      AND numDomainID = v_numDomainID
                        --AND numAuthGroupID = @numBizDocId
      AND bitCustom = true
      AND tintRow > v_intRowNum   ORDER BY tintRow LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_intRowNum := 0;
      end if;
   END LOOP;                                                           
                                                       
       
         
   open SWV_RefCur for
   SELECT  I.vcItemName AS Item ,
                charitemType AS type ,
                opp.vcItemDesc AS "desc" ,
                opp.numUnitHour AS Unit ,
                opp.monPrice AS Price ,
                opp.monTotAmount AS Amount ,
                (opp.monTotAmount/opp.numUnitHour)*opp.numUnitHour AS ItemTotalAmount ,       
--case when isnull(bitTaxable,0) =0 then 0 when bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0)  end as Tax,       
                coalesce(monListPrice,0) AS listPrice ,
                coalesce(I.numItemCode,0) AS ItemCode ,
                numoppitemtCode ,
                L.vcData AS vcItemClassification ,
                CASE WHEN bitTaxable = false THEN 'No'
   ELSE 'Yes'
   END AS Taxable ,
                monListPrice || ' ' || coalesce(v_strSQLCusFields,'') ,
                coalesce(I.numIncomeChartAcntId,0) AS itemIncomeAccount ,
                coalesce(I.numAssetChartAcntId,0) AS itemInventoryAsset ,
                'NI' AS ItemType ,
                coalesce(I.numCOGsChartAcntId,0) AS itemCoGs ,
                coalesce(I.monAverageCost,'0') AS AverageCost ,
                coalesce(opp.bitDropShip,false) AS bitDropShip ,
                (opp.monTotAmtBefDiscount -opp.monTotAmount) AS DiscAmt ,
                NULLIF(opp.numProjectID,0) AS numProjectID ,
                NULLIF(opp.numClassID,0) AS numClassID ,
                coalesce(I.bitKitParent,false) AS bitKitParent ,
                coalesce(I.bitAssembly,false) AS bitAssembly,
				coalesce(opp.numUnitHour,0) AS numUnitHour,
				coalesce(opp.numUnitHourReceived,0) AS numUnitHourReceived,
				coalesce(opp.numUnitHourReceived,0) AS numUnitReceived,
				coalesce(opp.monPrice,0) AS monPrice,
				coalesce(OM.dtItemReceivedDate,LOCALTIMESTAMP) AS datEntry_Date,
				OM.vcpOppName,
				coalesce(OM.fltExchangeRate,0) AS fltExchangeRate,I.charItemType,
				coalesce(opp.bitDropShip,false) AS DropShip,
				coalesce(I.numAssetChartAcntId,0) AS itemInventoryAsset,
				coalesce(I.numIncomeChartAcntId,0) AS itemInventoryAsset,
				coalesce(I.numCOGsChartAcntId,0) AS itemCoGs,
				I.vcItemName AS vcItemName,
				coalesce(OM.numDivisionId,0) AS numDivisionId,
				coalesce(opp.numoppitemtCode,0) AS numoppitemtCode,
				coalesce(OM.numCurrencyID,0) AS numCurrencyID,
				coalesce(opp.numClassID,0) AS numClassID,
				coalesce(opp.numProjectID,0) AS numProjectID,
				I.numItemCode
   FROM    OpportunityItems opp
   JOIN OpportunityMaster AS OM ON opp.numOppId = OM.numOppId
                --JOIN OpportunityBizDocItems OBI ON OBI.numOppItemID = Opp.numoppitemtCode
   LEFT JOIN Item I ON opp.numItemCode = I.numItemCode
   LEFT JOIN Listdetails L ON I.numItemClassification = L.numListItemID
   WHERE   opp.numOppId = v_numOppId;
                --AND OBI.numOppBizDocID = @numOppBizDocsId                                                                                                                  
                                                         
   open SWV_RefCur2 for
   SELECT  coalesce(v_numCurrencyID,0) AS numCurrencyID ,
                coalesce(v_fltExchangeRate,1) AS fltExchangeRate ,
                coalesce(v_fltCurrentExchangeRate,1) AS CurrfltExchangeRate ,
                coalesce(v_fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc ,
                v_vcBaseCurrency AS vcBaseCurrency ,
                v_vcForeignCurrency AS vcForeignCurrency ,
                coalesce(v_vcPOppName,'') AS vcPOppName ,
                --ISNULL(@vcBizDocID, '') AS vcBizDocID ,
                coalesce(v_bitPPVariance,false) AS bitPPVariance ,
                v_numDivisionID AS numDivisionID ,
                v_bitAutolinkUnappliedPayment AS bitAutolinkUnappliedPayment;
   RETURN;
END; $$;


