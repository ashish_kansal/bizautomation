-- Stored procedure definition script USP_GetChartAcntDetailsForBalanceSheetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForBalanceSheetDetails(v_numDomainId NUMERIC(9,0),                                                        
v_dtFromDate TIMESTAMP,                                                      
v_dtToDate TIMESTAMP,                    
v_tintByteMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);                                                      
   v_i  INTEGER;
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   v_strSQL := '';                                                     
   select   count(*) INTO v_i From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                             
   GJH.numDomainId = v_numDomainId And GJH.datEntry_Date <= CAST('' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '' AS timestamp);                       
                  
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1                 
                   
              
   If v_tintByteMode = 1 then
  
      If v_i = 0 then
    
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,             
      COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID                                     
      Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5)
         || ' And (COA.numAcntType = 813 Or COA.numAcntType = 814 Or COA.numAcntType = 817 Or COA.numAcntType = 818 Or COA.numAcntType = 819)';
         v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''' And  COA.dtOpeningDate<=''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '''';
      Else
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,             
       COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID                                  
     Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) ||
         ' And(COA.numAcntType = 813 Or COA.numAcntType = 814 Or COA.numAcntType = 817 Or COA.numAcntType = 818 Or COA.numAcntType = 819)';
      end if;
   end if;                    
   if v_tintByteMode = 2 then
   
      if v_i = 0 then
     
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,             
       COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID                                    
       Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5)
         || ' And  (COA.numAcntType = 815 Or COA.numAcntType = 816 Or COA.numAcntType = 820 Or COA.numAcntType = 827)';
         v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate >= ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''' And  COA.dtOpeningDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '''';
      Else
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,             
       COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID                                   
      Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5)
         || ' And(COA.numAcntType = 815 Or COA.numAcntType = 816 Or COA.numAcntType = 820 Or COA.numAcntType = 827)';
      end if;
   end if;                    
                    
   if v_tintByteMode = 3 then
   
      if v_i = 0 then
     
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,             
     COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID             
         Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5)
         || ' And COA.numAcntType = 821';
         v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''' And  COA.dtOpeningDate<=''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '''';
      Else
         v_strSQL := ' Select COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as AcntTypeDescription,                                      
      fn_GetCurrentOpeningBalanceForTrailBalance(COA.numAccountId,''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''',''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || ''',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(300)),1,300) || ') As Amount,            
      COA.numAcntType as numAcntType  From Chart_Of_Accounts COA              
      Left outer join ListDetails LD on COA.numAcntType = LD.numListItemID                                  
      Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || ' And COA.numAcntType = 821';
      end if;
   end if;                    
   RAISE NOTICE '%',v_strSQL;                                   
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


