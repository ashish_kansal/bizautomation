-- Stored procedure definition script USP_InsertScheduler for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertScheduler(v_numScheduleId NUMERIC(9,0) DEFAULT 0,        
v_varScheduleName VARCHAR(50) DEFAULT NULL,          
v_chrIntervalType CHAR(1) DEFAULT NULL,          
v_tintIntervalDays SMALLINT DEFAULT NULL,          
v_tintMonthlyType SMALLINT DEFAULT NULL,          
v_tintFirstDet SMALLINT DEFAULT NULL,          
v_tintWeekDays SMALLINT DEFAULT NULL,          
v_tintMonths SMALLINT DEFAULT NULL,          
v_dtStartDate TIMESTAMP DEFAULT NULL,          
v_dtEndDate TIMESTAMP DEFAULT NULL,          
v_bitEndType BOOLEAN DEFAULT NULL,      
v_bitEndTransactionType BOOLEAN DEFAULT NULL,      
v_numNoTransaction NUMERIC(9,0) DEFAULT NULL,    
v_numDomainId NUMERIC(9,0) DEFAULT NULL ,  
v_numReportId NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_dtEndDate = 'Jan  1 1753 12:00AM' then 
      v_dtEndDate := null;
   end if;         
        
--Insert        
   if v_numScheduleId = 0 then

      Insert into CustRptScheduler(varScheduleName,chrIntervalType,tintIntervalDays,tintMonthlyType,tintFirstDet,tintWeekDays,tintMonths,dtStartDate,dtEndDate,bitEndType,bitEndTransactionType,numNoTransaction,numDomainID,numReportId)
Values(v_varScheduleName,v_chrIntervalType,v_tintIntervalDays,v_tintMonthlyType,v_tintFirstDet,v_tintWeekDays,v_tintMonths,v_dtStartDate,v_dtEndDate,v_bitEndType,v_bitEndTransactionType,v_numNoTransaction,v_numDomainId,v_numReportId) RETURNING numScheduleId INTO v_numScheduleId;
 
   end if;        
        
---Update        
   if v_numScheduleId <> 0 then

      Update CustRptScheduler Set varScheduleName = v_varScheduleName,chrIntervalType = v_chrIntervalType,tintIntervalDays = v_tintIntervalDays,
      tintMonthlyType = v_tintMonthlyType,tintFirstDet = v_tintFirstDet,
      tintWeekDays = v_tintWeekDays,tintMonths = v_tintMonths,
      dtStartDate = v_dtStartDate,dtEndDate = v_dtEndDate,bitEndType = v_bitEndType,
      bitEndTransactionType = v_bitEndTransactionType,numNoTransaction = v_numNoTransaction,
      numDomainID = v_numDomainId,numReportId = v_numReportId Where numScheduleid = v_numScheduleId;
   end if;        
   open SWV_RefCur for SELECT
   v_numScheduleId;
END; $$;












