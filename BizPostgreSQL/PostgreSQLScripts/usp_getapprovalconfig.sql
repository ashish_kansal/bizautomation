CREATE OR REPLACE FUNCTION USP_GetApprovalConfig(v_chrAction CHAR(10) DEFAULT NULL,
  v_numDomainId NUMERIC(18,2) DEFAULT 0,
  v_numUserId NUMERIC(18,2) DEFAULT 0,
  v_numModuleId NUMERIC(18,2) DEFAULT 0,
  v_numConfigId NUMERIC(18,2) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_chrAction = 'VU' then
	
      open SWV_RefCur for
      SELECT
      U.numUserDetailId AS numUserId
			,CONCAT(coalesce(vcFirstName,'-'),' ',coalesce(vcLastname,'-')) AS vcUserName
      FROM
      UserMaster AS U
      INNER JOIN
      AdditionalContactsInformation ACI
      ON
      U.numUserDetailId = ACI.numContactId
      WHERE
      U.numDomainID = v_numDomainId AND U.bitactivateflag = true;
   ELSEIF v_chrAction = 'V'
   then
	
      open SWV_RefCur for
      SELECT
      U.numUserDetailId AS numUserId,CONCAT(coalesce(vcFirstName,'-'),' ',coalesce(vcLastname,'-')) AS vcUserName,coalesce(AP.numLevel1Authority,0) AS numLevel1Authority,coalesce(AP.numLevel2Authority,0) AS numLevel2Authority,
			coalesce(AP.numLevel3Authority,0) AS numLevel3Authority,coalesce(AP.numLevel4Authority,0) AS numLevel4Authority,
			coalesce(AP.numLevel5Authority,0) AS numLevel5Authority,AP.numConfigId
      FROM
      UserMaster AS U
      INNER JOIN
      AdditionalContactsInformation ACI
      ON
      U.numUserDetailId = ACI.numContactId
      LEFT JOIN
      ApprovalConfig AS AP
      ON
      U.numUserDetailId = AP.numUserId AND AP.numModule = v_numModuleId
      WHERE
      U.numDomainID = v_numDomainId AND U.bitactivateflag = true;
   ELSE
      open SWV_RefCur for
      SELECT * FROM
      ApprovalConfig
      WHERE
      numDomainID = v_numDomainId
      AND numModule = v_numModuleId
      AND (numUserId = v_numUserId OR coalesce(v_numUserId,0) = 0)
      AND (numConfigId = v_numConfigId OR coalesce(v_numConfigId,0) = 0);
   end if;
   RETURN;
END; $$;


