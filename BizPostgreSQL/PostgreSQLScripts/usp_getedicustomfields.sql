CREATE OR REPLACE FUNCTION USP_GetEDICustomFields(v_numDomainID NUMERIC(18,0)
	,v_Grp_id NUMERIC(5,0)
	,v_RecID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_Grp_id = 19 then  -- Opp/Order 
	  
      open SWV_RefCur for
      SELECT CFM.Fld_id
			  ,CFM.FLd_label
			  ,CFM.fld_type
			  ,CFM.Grp_id
			  ,OPPEDI.FldDTLID
			  ,OPPEDI.Fld_Value
      FROM CFW_Fld_Master CFM
      LEFT JOIN CFW_Fld_Values_Opp_EDI OPPEDI
      ON CFM.Fld_id = OPPEDI.Fld_ID AND OPPEDI.RecId = v_RecID
      WHERE  CFM.Grp_id = v_Grp_id AND CFM.numDomainID = v_numDomainID;
   end if;

   IF v_Grp_id = 20 then  --- Item
	  
      open SWV_RefCur for
      SELECT CFM.Fld_id
			  ,CFM.FLd_label
			  ,CFM.fld_type
			  ,CFM.Grp_id
			  ,ITEMEDI.FldDTLID
			  ,ITEMEDI.Fld_Value
      FROM CFW_Fld_Master CFM
      LEFT JOIN  CFW_FLD_Values_Item_EDI ITEMEDI
      ON CFM.Fld_id = ITEMEDI.Fld_ID AND ITEMEDI.RecId = v_RecID
      WHERE  CFM.Grp_id = v_Grp_id AND CFM.numDomainID = v_numDomainID;
   end if;

   IF v_Grp_id = 21 then  --- Opp Item
	  
      open SWV_RefCur for
      SELECT	   CFM.Fld_id
				  ,CFM.FLd_label
				  ,CFM.fld_type
				  ,'21' AS  Grp_id
				  ,OPPITEMEDI.FldDTLID
				  ,CASE WHEN(coalesce(OPPITEMEDI.Fld_Value,'') = '') THEN  ITEMEDI.Fld_Value 
							--(SELECT ITEMEDI.Fld_Value FROM CFW_FLD_Values_Item_EDI ITEMEDI 
							--	LEFT JOIN CFW_Fld_Master CFM ON CFM.Fld_ID = ITEMEDI.Fld_id	 
							--	WHERE CFM.Grp_id=20            -- If Fld Value is not set in 'order's item' then show it from Item
							--)
      ELSE OPPITEMEDI.Fld_Value
      END AS Fld_Value
      FROM CFW_Fld_Master CFM
      LEFT JOIN  CFW_Fld_Values_OppItems_EDI OPPITEMEDI
      ON CFM.Fld_id = OPPITEMEDI.Fld_ID AND OPPITEMEDI.RecId = v_RecID
      LEFT JOIN CFW_FLD_Values_Item_EDI ITEMEDI
      ON CFM.Fld_id = ITEMEDI.Fld_ID
      WHERE  CFM.Grp_id = 20 AND CFM.numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


