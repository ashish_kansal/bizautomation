-- Stored procedure definition script USP_UpdateSitePageStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateSitePageStatus(v_numPageID NUMERIC,
    v_numSiteID NUMERIC,
    v_numDomainId NUMERIC,
    v_numUserCntID NUMERIC,
    v_bitIsActive BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  SitePages
   SET     bitIsActive = v_bitIsActive,numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
   WHERE   numPageID = v_numPageID
   AND numDomainID = v_numDomainId
   AND numSiteID = v_numSiteID;
   RETURN;
END; $$;


/****** Object:  View [dbo].[VIEW_GENERALLEDGER]    Script Date: 10/05/2009 14:25:37 ******/



