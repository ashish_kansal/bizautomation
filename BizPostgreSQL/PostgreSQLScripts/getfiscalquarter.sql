-- Function definition script GetFiscalQuarter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetFiscalQuarter(v_date TIMESTAMP,v_numDomainId NUMERIC(4,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_year  NUMERIC(4,0);
   v_SQ1  TIMESTAMP; 
   v_SQ2  TIMESTAMP; 
   v_SQ3  TIMESTAMP; 
   v_SQ4  TIMESTAMP; 
   v_EQ1  TIMESTAMP; 
   v_EQ2  TIMESTAMP; 
   v_EQ3  TIMESTAMP; 
   v_EQ4  TIMESTAMP; 
   v_Quat  NUMERIC(2,0);
BEGIN
   v_year := GetFiscalyear(v_date,v_numDomainId);




--print @date
--print @year

--print '----quarter1------------------'


   v_SQ1 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '0 month'; 
   v_EQ1 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-3 MILLISECONDS ';
--print @sq1
--print @eq1
--print '----quarter2------------------'
   v_SQ2 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '3 month'; 
   v_EQ2 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-3 MILLISECONDS ';
--print @sq2
--print @eq2
--print '----quarter3------------------'
   v_SQ3 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '6 month'; 
   v_EQ3 :=  GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-3 MILLISECONDS ';
--print @sq3
--print @eq3
--print '----quarter4------------------'
   v_SQ4 := GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '9 month'; 
   v_EQ4 :=  GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-3 MILLISECONDS ';
--print @sq4
--print @eq4
--print '------------------------------'

   if (v_SQ1 <= v_date and v_EQ1 >= v_date) then
 
--print '1'
      v_Quat := 1;
   ELSEIF v_SQ2 <= v_date  and v_EQ2 >= v_date
   then
 
--print '2'
      v_Quat := 2;
   ELSEIF v_SQ3 <= v_date and v_EQ3 >= v_date
   then
 
--print '3'
      v_Quat := 3;
   ELSEIF v_SQ4 <= v_date and v_EQ4 >= v_date
   then
 
--print '4'
      v_Quat := 4;
   end if;
--print @Quat
   return v_Quat;
END; $$;

