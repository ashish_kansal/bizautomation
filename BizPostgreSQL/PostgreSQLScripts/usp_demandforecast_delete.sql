-- Stored procedure definition script USP_DemandForecast_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_Delete(v_numDomainID NUMERIC(18,0),
	v_vcSelectedIDs VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  TEXT;
BEGIN
   DELETE FROM DemandForecast WHERE numDomainID = v_numDomainID AND numDFID IN (SELECT Id FROM SplitIDs(coalesce(v_vcSelectedIDs,''),','));
   RETURN;
END; $$;



