-- FUNCTION: public.usp_getcreditstatusofcompany(numeric, refcursor)

-- DROP FUNCTION public.usp_getcreditstatusofcompany(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcreditstatusofcompany(
	v_numdivisionid numeric,
	INOUT swv_refcur refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_numDomainID  NUMERIC(9,0);
   v_bitCheckCreditStatus  BOOLEAN;
BEGIN
   select   numDomainID INTO v_numDomainID from DivisionMaster where numDivisionID = v_numDivisionID;
   select   coalesce(bitCheckCreditStatus,false) INTO v_bitCheckCreditStatus from eCommerceDTL where numDomainID = v_numDomainID;

   if v_bitCheckCreditStatus = true then

      open SWV_RefCur for
      SELECT
      coalesce(vcData,'0')
      FROM
      CompanyInfo Com
      JOIN
      DivisionMaster Div
      ON
      Com.numCompanyId = Div.numCompanyID
      LEFT JOIN
      Listdetails
      ON
      numListID = 3 AND numListItemID = numCompanyCredit
      WHERE
      Div.numDivisionID = v_numDivisionID;
   else
      open SWV_RefCur for
      SELECT 10000000;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getcreditstatusofcompany(numeric, refcursor)
    OWNER TO postgres;
