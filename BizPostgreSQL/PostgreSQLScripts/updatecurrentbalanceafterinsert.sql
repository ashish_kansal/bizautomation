CREATE OR REPLACE FUNCTION UpdateCurrentBalanceAfterInsert_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
		v_numChartAccountId  NUMERIC(18,0);
		v_DebitAmount  DECIMAL(20,5);
		v_CreditAmount  DECIMAL(20,5);
		v_numDomainId  NUMERIC(18,0);
		v_bitOpeningBalance  BOOLEAN;

	BizDoc_Cursor CURSOR FOR SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from new_table;
BEGIN
	v_DebitAmount := 0;
	v_CreditAmount := 0;

    SELECT 
		COALESCE(bitOpeningBalance,false) INTO v_bitOpeningBalance 
	FROM 
		General_Journal_Header 
	INNER JOIN
		new_table
	ON
		General_Journal_Header.numJOurnal_Id = new_table.numJournalId
	LIMIT 1;

    IF v_bitOpeningBalance = false then
        OPEN BizDoc_Cursor;
        FETCH NEXT FROM BizDoc_Cursor INTO v_numChartAccountId,v_DebitAmount,v_CreditAmount,v_numDomainId;

        WHILE FOUND LOOP
			UPDATE 
				Chart_Of_Accounts 
			SET 
				numOpeningBal = coalesce(numOpeningBal,0) + v_DebitAmount - v_CreditAmount
            WHERE 
				numAccountId = v_numChartAccountId
				AND numDomainId = v_numDomainId;
            
			FETCH NEXT FROM BizDoc_Cursor INTO v_numChartAccountId,v_DebitAmount,v_CreditAmount,v_numDomainId;
        END LOOP;
        
		CLOSE BizDoc_Cursor;
    END IF;
  
	RETURN NULL;
END; $$;

CREATE TRIGGER UpdateCurrentBalanceAfterInsert AFTER INSERT ON General_Journal_Details REFERENCING NEW TABLE AS new_table FOR EACH ROW EXECUTE PROCEDURE UpdateCurrentBalanceAfterInsert_TrFunc();


