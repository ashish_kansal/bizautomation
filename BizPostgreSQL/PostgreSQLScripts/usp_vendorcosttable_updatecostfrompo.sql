DROP FUNCTION IF EXISTS usp_vendorcosttable_updatecostfrompo;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_updatecostfrompo
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numoppid NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		VendorCostTable VCT
	SET
		numDynamicCostModifiedBy = (CASE WHEN monDynamicCost <> OI.monPrice THEN p_numoppid ELSE numDynamicCostModifiedBy END)
		,dtDynamicCostModifiedDate = (CASE WHEN monDynamicCost <> OI.monPrice THEN timezone('utc', now()) ELSE dtDynamicCostModifiedDate END)
		,bitIsDynamicCostByOrder = (CASE WHEN monDynamicCost <> OI.monPrice THEN true ELSE bitIsDynamicCostByOrder END)
		,monDynamicCost = OI.monPrice
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppID = OI.numOppID
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		Vendor V
	ON
		OM.numDivisionID = V.numVendorID
		AND OI.numItemCode = V.numItemCode
	WHERE
		OM.numDomainID = p_numdomainid
		AND OM.numOppID = p_numoppid
		AND V.numVendorTcode = VCT.numVendorTcode
		AND VCT.numCurrencyID = OM.numCurrencyID
		AND OI.numUnitHour BETWEEN COALESCE(VCT.intFromQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AND COALESCE(VCT.intToQty,0) * COALESCE(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1);
END; $$;