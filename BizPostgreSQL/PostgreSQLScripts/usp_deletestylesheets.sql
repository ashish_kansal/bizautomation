-- Stored procedure definition script USP_DeleteStyleSheets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteStyleSheets(v_numCssID NUMERIC(9,0),
	v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(select COUNT(*) FROM StyleSheetDetails WHERE numCssID = v_numCssID) = 0 then
      DELETE FROM StyleSheets
      WHERE numCssID = v_numCssID AND numDomainID = v_numDomainId;
   ELSE
      RAISE EXCEPTION 'Dependant';
   end if;
   RETURN;
END; $$;


