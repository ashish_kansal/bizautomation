CREATE OR REPLACE FUNCTION USP_GetBankDetailsList(v_tintMode SMALLINT DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numUserContactID NUMERIC(18,0) DEFAULT NULL,
	v_numBankMasterID NUMERIC(18,0) DEFAULT NULL,
	v_vcUserName VARCHAR(50) DEFAULT '',
	v_vcPassword VARCHAR(50) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
IF v_tintMode = 0 then
	
      open SWV_RefCur for
      SELECT
      numBankDetailID,
		numBankMasterID,
		numDomainID,
		numUserContactID,
		numAccountID,
		vcBankID,
		vcAccountNumber,
		vcAccountType,
		vcUserName,
		vcPassword,
		dtCreatedDate,
		dtModifiedDate
      FROM
      BankDetails
      WHERE numDomainID = v_numDomainID AND numUserContactID = v_numUserContactID AND bitIsActive = true;
   end if;
	
   IF v_tintMode = 1 then
	
      open SWV_RefCur for
      SELECT
      BD.numBankDetailID,
		numBankMasterID,
		BD.numDomainID,
		BD.numUserContactID,
		BD.numAccountID,
		vcBankID,
		vcAccountNumber,
		vcAccountType,
		vcUserName,
		vcPassword,
		dtCreatedDate,
		dtModifiedDate,
		COA.IsConnected
      FROM
      BankDetails BD LEFT JOIN Chart_Of_Accounts COA ON BD.numDomainID = COA.numDomainId AND BD.numAccountID = COA.numAccountId AND BD.numBankDetailID = COA.numBankDetailID
      WHERE BD.numDomainID = v_numDomainID AND
      BD.numUserContactID = v_numUserContactID AND
      numBankMasterID = v_numBankMasterID AND
      vcUserName = v_vcUserName AND
      vcPassword = v_vcPassword AND
      BD.bitIsActive = true;
   end if;
   RETURN;
END; $$;


