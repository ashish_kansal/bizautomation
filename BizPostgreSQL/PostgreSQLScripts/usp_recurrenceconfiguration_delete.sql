Create or replace FUNCTION USP_RecurrenceConfiguration_Delete(v_numRecConfigID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numType  SMALLINT DEFAULT 0;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   UPDATE RecurrenceConfiguration SET bitMarkForDelete = true WHERE numRecConfigID = v_numRecConfigID;
   select   numType INTO v_numType FROM RecurrenceConfiguration WHERE numRecConfigID = v_numRecConfigID;
   IF v_numType > 0 then
	
      IF v_numType = 1 then  --Sales Order
		
         IF(SELECT COUNT(*) FROM RecurrenceTransaction WHERE numRecConfigID = v_numRecConfigID AND coalesce(numRecurrOppID,0) > 0) > 0 then
			
            RAISE EXCEPTION 'RECURRENCE_TRANSACTIOS_EXISTS';
            RETURN;
         end if;
      ELSEIF v_numType = 2
      then --BizDoc
		
         IF(SELECT COUNT(*) FROM RecurrenceTransaction WHERE numRecConfigID = v_numRecConfigID AND coalesce(numRecurrOppBizDocID,0) > 0) > 0 then
			
            RAISE EXCEPTION 'RECURRENCE_TRANSACTIOS_EXISTS';
            RETURN;
         end if;
      end if;
   end if;
   UPDATE OpportunityMaster SET vcRecurrenceType = NULL WHERE numOppId IN(SELECT numOppID FROM RecurrenceConfiguration WHERE numRecConfigID = v_numRecConfigID);
   DELETE FROM RecurrenceErrorLog WHERE numRecConfigID = v_numRecConfigID;
   DELETE FROM RecurrenceTransaction WHERE numRecConfigID = v_numRecConfigID;
   DELETE FROM RecurrenceConfiguration WHERE numRecConfigID = v_numRecConfigID;
   EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;  


