-- Stored procedure definition script usp_SaveWeblinkLabels for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveWeblinkLabels(v_numDivisionID NUMERIC DEFAULT 0,    
 v_vcWebLable1 VARCHAR(100) DEFAULT NULL,    
 v_vcWebLable2 VARCHAR(100) DEFAULT NULL,    
 v_vcWebLable3 VARCHAR(100) DEFAULT NULL,    
 v_vcWebLable4 VARCHAR(100) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE CompanyInfo
   SET vcWebLabel1 = v_vcWebLable1,vcWebLabel2 = v_vcWebLable2,vcWebLabel3 = v_vcWebLable3, 
   vcWeblabel4 = v_vcWebLable4
   WHERE numCompanyId =(select numCompanyID from DivisionMaster where numDivisionID = v_numDivisionID);
   RETURN;
END; $$;


