-- Stored procedure definition script USP_ExtranetLogin for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtranetLogin(v_Email VARCHAR(100),
v_Password VARCHAR(100),
v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numContactId  NUMERIC(9,0);                                              
   v_numDivisionId  NUMERIC(9,0);                                              
   v_numcompanyID  NUMERIC(9,0);                                              
   v_numGroupID  NUMERIC(9,0);                                              
   v_tintCRMType  NUMERIC(9,0);                                                           
   v_vcCompanyName  VARCHAR(100);                                          
   v_AccOwner  NUMERIC(9,0);                                        
   v_AccOwnerName  VARCHAR(100);                                       
   v_numExtranetDtlID  VARCHAR(15);                                             
   v_numNoOfTimes NUMERIC;                        
   v_numPartnerGroup  NUMERIC(9,0);                        
   v_bitPartnerAccess  BOOLEAN;                        
   v_numRelationShip  NUMERIC(9,0);                        
   v_numProfileID  NUMERIC(9,0);
   v_numContactType  NUMERIC(9,0);                      
--declare @vcHomePage as varchar(1000)                     
   v_UserEmail  VARCHAR(100);                   
   v_ContactName  VARCHAR(100);         
   v_vcFirstName  VARCHAR(100);
   v_vcLastName  VARCHAR(100);                  
   v_count  SMALLINT;
BEGIN
   select   count(*) INTO v_count from AdditionalContactsInformation A
   join ExtranetAccountsDtl E
   on CAST(A.numContactId AS VARCHAR) = E.numContactID where (LOWER(E.vcUserName) = LOWER(v_Email) OR LOWER(vcEmail) = LOWER(v_Email)) and (LOWER(vcPassword) = LOWER(v_Password) OR v_Password = 'EXISTINGUSERGUESTLOGIN') and tintAccessAllowed = 1 and A.numDomainID = v_numDomainID;
   RAISE NOTICE '%',v_count;                                 
   if v_count = 1 then             ---checking whether an user exists with the username and password.Multiple results will be rejected                                 

      select   A.numContactId, vcFirstName || ' ' || vcLastname, numDivisionId, numContactType, coalesce(vcEmail,''), coalesce(vcFirstName,''), coalesce(vcLastname,'') INTO v_numContactId,v_ContactName,v_numDivisionId,v_numContactType,v_UserEmail,
      v_vcFirstName,v_vcLastName from AdditionalContactsInformation A
      join ExtranetAccountsDtl E
      on CAST(A.numContactId AS VARCHAR) = E.numContactID where (LOWER(E.vcUserName) = LOWER(v_Email) OR LOWER(vcEmail) = LOWER(v_Email)) and (LOWER(vcPassword) = LOWER(v_Password) OR v_Password = 'EXISTINGUSERGUESTLOGIN') AND E.numDomainID = v_numDomainID AND E.tintAccessAllowed = 1;
      select   numCompanyID, tintCRMType, numRecOwner INTO v_numcompanyID,v_tintCRMType,v_AccOwner from DivisionMaster where numDivisionID = v_numDivisionId;
      select   numDomainID, vcCompanyName, numCompanyType, vcProfile INTO v_numDomainID,v_vcCompanyName,v_numRelationShip,v_numProfileID from CompanyInfo where numCompanyId = v_numcompanyID;
      select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') INTO v_AccOwnerName from AdditionalContactsInformation where numContactId = v_AccOwner;                       
                      
--select @vcHomePage=vcHomePage from HomePage where numDomainID=@numDomainID and numRelationship=@numRelationShip and numContactType=@numContactType                                         
      select   numNoOfTimes, bitPartnerAccess, numPartnerGroupID, numExtranetDtlID, numGroupID INTO v_numNoOfTimes,v_bitPartnerAccess,v_numPartnerGroup,v_numExtranetDtlID,
      v_numGroupID from ExtarnetAccounts hdr
      join ExtranetAccountsDtl Dtl
      on hdr.numExtranetID = Dtl.numExtranetID where numDivisionId = v_numDivisionId and tintAccessAllowed = 1 and numContactID = v_numContactId::VARCHAR and LOWER(vcPassword) = LOWER(v_Password);
      if v_bitPartnerAccess = false then 
         v_numPartnerGroup := 0;
      end if;
      open SWV_RefCur for
      select 1 ,v_numContactId,v_numDivisionId,v_numcompanyID,v_numGroupID,v_tintCRMType,v_numDomainID,v_vcCompanyName,v_AccOwner,v_AccOwnerName,coalesce(v_numPartnerGroup,0),coalesce(v_numRelationShip,0) ,/* isnull(@vcHomePage,'')*/ '',
v_UserEmail ,v_ContactName AS ContactName,coalesce(v_bitPartnerAccess,false),
tintCustomPagingRows,
vcDateFormat,
numDefCountry,
tintAssignToCriteria,
bitIntmedPage,
coalesce(vcPortalLogo,'') AS vcPortalLogo,
tintFiscalStartMonth,
 TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval) as StartDate,
 TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval)+CAST(sintNoofDaysInterval || 'day' as interval) as EndDate,
case when bitPSMTPServer = true then vcPSMTPServer else '' end as vcSMTPServer ,
case when bitPSMTPServer = true then coalesce(numPSMTPPort,0) else 0 end as numSMTPPort
,coalesce(bitPSMTPAuth,false) AS bitSMTPAuth
,coalesce(vcPSmtpPassword,'') AS vcSmtpPassword
,coalesce(bitPSMTPSSL,false) AS bitSMTPSSL   ,coalesce(bitPSMTPServer,false) AS bitSMTPServer  ,coalesce(vcPSMTPUserName,'') AS vcSMTPUserName ,
coalesce(numCurrencyID,0) AS numCurrencyID, coalesce(vcCurrency,'') AS vcCurrency,vcDomainName,
coalesce(v_numProfileID,0) AS numProfileID,
coalesce(bitCustomizePortal,false) AS bitCustomizePortal,
coalesce(vcPortalName,'') AS vcPortalName,
v_vcFirstName as vcFirstName,
v_vcLastName as vcLastName
      from Domain
      where numDomainId = v_numDomainID;
      IF v_Password <> 'EXISTINGUSERGUESTLOGIN' then

         v_numNoOfTimes := coalesce(v_numNoOfTimes,0) + 1;
         update ExtranetAccountsDtl set bintLastLoggedIn = TIMEZONE('UTC',now()),numNoOfTimes = v_numNoOfTimes  where numExtranetDtlID::VARCHAR = v_numExtranetDtlID;
      end if;
   end if;                                           
   open SWV_RefCur2 for
   select v_count;
   RETURN;
END; $$;


