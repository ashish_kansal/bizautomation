CREATE OR REPLACE FUNCTION USP_TicklerActItemsV2TopCount
(
	v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                                        
	v_numDomainID NUMERIC(9,0) DEFAULT null,                                                                        
	v_startDate TIMESTAMP DEFAULT NULL,                                                                        
	v_endDate TIMESTAMP DEFAULT NULL,                                                                    
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numViewID NUMERIC(18,0) DEFAULT NULL,
	INOUT v_numTotalRecords NUMERIC(18,0) DEFAULT NULL
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_bitGroupByOrder  BOOLEAN DEFAULT 0;
	v_numBatchID  NUMERIC(18,0) DEFAULT 0;
	v_bitIncludeSearch  BOOLEAN DEFAULT 1;
	v_tintPrintBizDocViewMode  INTEGER DEFAULT 1;
	v_tintPendingCloseFilter  INTEGER DEFAULT 1;
	v_numShippingZone  NUMERIC(18,0) DEFAULT 0;
	v_vcOrderStauts  TEXT DEFAULT ''; 
	v_tintFilterBy  SMALLINT DEFAULT 0;
	v_vcFilterValue  TEXT DEFAULT '';
	v_numPageIndex  INTEGER DEFAULT 1;
	v_vcSortColumn  VARCHAR(100) DEFAULT '';
	v_vcSortOrder  VARCHAR(4) DEFAULT '';
	v_numWarehouseID  VARCHAR(100);
	v_bitRemoveBO  BOOLEAN DEFAULT 0;
	v_tintPackingViewMode  INTEGER DEFAULT 1;
	v_tintPackingMode  INTEGER;
	v_tintInvoicingType  INTEGER; 
	v_tintCommitAllocation  SMALLINT;
	v_numShippingServiceItemID  NUMERIC(18,0);
	v_numDefaultSalesShippingDoc  NUMERIC(18,0);
	v_vcCustomSearchValue  TEXT DEFAULT ''; 
	v_vcSQL  TEXT;
	v_vcSQLFinal  TEXT;
	v_WarehouseCount  INTEGER DEFAULT 0;
	v_I  INTEGER DEFAULT 1;
BEGIN
	IF v_numViewID = 4 OR v_numViewID = 5 OR v_numViewID = 6 then
		v_bitGroupByOrder := true;
	end if;
   
	select 
		tintPackingMode,
		tintInvoicingType 
	INTO 
		v_tintPackingMode,v_tintInvoicingType 
	FROM
		MassSalesFulfillmentConfiguration 
	WHERE
		numDomainID = v_numDomainID; 

	select 
		coalesce(tintCommitAllocation,1), coalesce(numShippingServiceItemID,0), coalesce(numDefaultSalesShippingDoc,0) 
	INTO 
		v_tintCommitAllocation,v_numShippingServiceItemID,v_numDefaultSalesShippingDoc 
	FROM
		Domain 
	WHERE
		numDomainId = v_numDomainID; 

	v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC LIMIT 1)');

	DROP TABLE IF EXISTS tt_TEMPMSRECORDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPMSRECORDS
	(
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0)
	);
	BEGIN
		CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
		EXCEPTION WHEN OTHERS THEN
			NULL;
	END;
	DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
		numWarehouseID NUMERIC(18,0)
	);

	INSERT INTO tt_TEMPWAREHOUSE
	(
		numWarehouseID
	)
	SELECT
		numWareHouseID
	FROM 
		Warehouses W
	LEFT JOIN
		AddressDetails AD
	ON
		W.numAddressID = AD.numAddressID
	WHERE
		W.numDomainID = v_numDomainID;

	SELECT COUNT(*) INTO v_WarehouseCount FROM tt_TEMPWAREHOUSE;

	WHILE(v_I <= v_WarehouseCount) LOOP
		SELECT numWarehouseID INTO v_numWarehouseID FROM tt_TEMPWAREHOUSE WHERE ID = v_I;

		v_vcSQL := CONCAT('INSERT INTO tt_TEMPMSRECORDS
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN v_bitGroupByOrder = true THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN v_numViewID = 4 OR v_numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN COALESCE(v_numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR COALESCE(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN v_numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN v_numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE v_tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND COALESCE(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND COALESCE(@bitRemoveBO,false) = true
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND COALESCE(OpportunityItems.bitDropShip, false) = false AND COALESCE(Item.bitAsset,false)=false)
												THEN 
													(CASE WHEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN COALESCE(@tintPackingMode,1) = 2 THEN COALESCE(OpportunityItems.numQtyPicked,0) - COALESCE(OpportunityItems.numQtyShipped,0) ELSE COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN COALESCE(@tintPackingMode,1) = 2 THEN COALESCE(OpportunityItems.numQtyPicked,0) ELSE COALESCE(OpportunityItems.numUnitHour,0) END) - COALESCE((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN COALESCE(Item.bitKitParent,false) = true THEN true ELSE false END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LENGTH(COALESCE(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN COALESCE(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LENGTH(COALESCE(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN COALESCE(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN COALESCE((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=COALESCE(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN COALESCE(v_tintPackingMode,1) = 2 THEN 'COALESCE(OpportunityItems.numQtyPicked,0)' ELSE 'COALESCE(OpportunityItems.numUnitHour,0)' END),' - COALESCE(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN COALESCE(v_tintPackingMode,1) = 2 THEN 'COALESCE(OpportunityItems.numQtyPicked,0)' ELSE 'COALESCE(OpportunityItems.numUnitHour,0)' END),' - COALESCE((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN COALESCE(@tintInvoicingType,0)=1 
														THEN COALESCE(OpportunityItems.numQtyShipped,0) 
														ELSE COALESCE(OpportunityItems.numUnitHour,0) 
													END) - COALESCE((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN COALESCE(WareHouseItems.numWareHouseItemID,0) > 0 AND COALESCE(OpportunityItems.bitDropship,false) = false THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN COALESCE(WareHouseItems.numWareHouseItemID,0) > 0 AND COALESCE(OpportunityItems.bitDropship,false) = false THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN v_numViewID = 5 AND COALESCE(v_tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													COALESCE(OI.numUnitHour,0) AS OrderedQty,
													COALESCE(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND COALESCE(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND COALESCE(OI.numUnitHour,0) > 0
													AND COALESCE(OI.bitDropShip,false) = false
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													COALESCE(OI.numUnitHour,0) AS OrderedQty,
													COALESCE(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND COALESCE(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN v_numViewID = 4 THEN 'AND 1 = (CASE WHEN COALESCE(OpportunityBizDocs.monDealAmount,0) - COALESCE(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN COALESCE(Item.bitContainer,false)=true THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN COALESCE(@numWarehouseID,'''') <>'''' AND COALESCE(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT CAST(Items AS NUMERIC) FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN COALESCE(v_numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2) LIMIT 1),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2) LIMIT 1) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								v_vcCustomSearchValue,(CASE WHEN v_bitGroupByOrder = true THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN v_numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN v_numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));
	   
		v_vcSQL := REPLACE(v_vcSQL,'@numDomainID',CAST(COALESCE(v_numDomainID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@vcOrderStauts',CONCAT('''',COALESCE(v_vcOrderStauts,''),''''));
			v_vcSQL := REPLACE(v_vcSQL,'@vcFilterValue',CONCAT('''',COALESCE(v_vcFilterValue,''),''''));
			v_vcSQL := REPLACE(v_vcSQL,'@tintFilterBy',CAST(COALESCE(v_tintFilterBy,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numViewID',CAST(COALESCE(v_numViewID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintPackingViewMode',CAST(COALESCE(v_tintPackingViewMode,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numShippingServiceItemID',CAST(COALESCE(v_numShippingServiceItemID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numShippingZone',CAST(COALESCE(v_numShippingZone,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numDefaultSalesShippingDoc',CAST(COALESCE(v_numDefaultSalesShippingDoc,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintInvoicingType',CAST(COALESCE(v_tintInvoicingType,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintPackingMode',CAST(COALESCE(v_tintPackingMode,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@bitRemoveBO',CAST(COALESCE(v_bitRemoveBO,false) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numWarehouseID',CONCAT('''',COALESCE(v_numWarehouseID,''),''''));
		EXECUTE v_vcSQL;
	   
		If v_numViewID = 2 AND v_tintPackingViewMode = 3 then
	 
			v_vcSQL := CONCAT('INSERT INTO tt_TEMPMSRECORDS
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN v_bitGroupByOrder = true THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN COALESCE(v_numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR COALESCE(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND COALESCE(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LENGTH(COALESCE(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN COALESCE(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LENGTH(COALESCE(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN COALESCE(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN COALESCE((SELECT COUNT(*) FROM ShippingBox WHERE COALESCE(vcShippingLabelImage,'''') <> '''' AND COALESCE(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN COALESCE(WareHouseItems.numWareHouseItemID,0) > 0 AND COALESCE(OpportunityItems.bitDropship,false) = false THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN COALESCE(Item.bitContainer,false)=true THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN COALESCE(@numWarehouseID,'''') <>'''' AND COALESCE(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT CAST(Items AS NUMERIC) FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN COALESCE(v_numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2) LIMIT 1),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2) LIMIT 1) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								v_vcCustomSearchValue,(CASE WHEN v_bitGroupByOrder = true THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

			v_vcSQL := REPLACE(v_vcSQL,'@numDomainID',CAST(COALESCE(v_numDomainID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@vcOrderStauts',CONCAT('''',COALESCE(v_vcOrderStauts,''),''''));
			v_vcSQL := REPLACE(v_vcSQL,'@vcFilterValue',CONCAT('''',COALESCE(v_vcFilterValue,''),''''));
			v_vcSQL := REPLACE(v_vcSQL,'@tintFilterBy',CAST(COALESCE(v_tintFilterBy,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numViewID',CAST(COALESCE(v_numViewID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintPackingViewMode',CAST(COALESCE(v_tintPackingViewMode,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numShippingServiceItemID',CAST(COALESCE(v_numShippingServiceItemID,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numShippingZone',CAST(COALESCE(v_numShippingZone,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numDefaultSalesShippingDoc',CAST(COALESCE(v_numDefaultSalesShippingDoc,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintInvoicingType',CAST(COALESCE(v_tintInvoicingType,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@tintPackingMode',CAST(COALESCE(v_tintPackingMode,0) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@bitRemoveBO',CAST(COALESCE(v_bitRemoveBO,false) AS VARCHAR));
			v_vcSQL := REPLACE(v_vcSQL,'@numWarehouseID',CONCAT('''',COALESCE(v_numWarehouseID,''),''''));

			EXECUTE v_vcSQL;
		end if;
	
	 
		v_I := v_I::bigint+1;
	END LOOP;
	
	SELECT COUNT(*) INTO v_numTotalRecords FROM tt_TEMPMSRECORDS;

	RETURN;
END; $$;



