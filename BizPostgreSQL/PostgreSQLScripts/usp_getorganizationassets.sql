-- Stored procedure definition script usp_GetOrganizationAssets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION usp_GetOrganizationAssets(v_numDivId NUMERIC(18,0),
v_numDomainId NUMERIC(18,0),
v_CurrentPage NUMERIC(18,0),
INOUT v_TotRecs INTEGER ,
v_numItemClassification NUMERIC(18,0) DEFAULT 0,
v_vcKeyWord VARCHAR(50) DEFAULT '',
v_vcSortColumn VARCHAR(50) DEFAULT '',
v_vcSort VARCHAR(10) DEFAULT '',
v_vcSortChar VARCHAR(1) DEFAULT '0',
v_PageSize INTEGER DEFAULT 20, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(2000);
--Sachin
   v_firstRec  INTEGER;                     
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_Temp_CompAsset_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP_COMPASSET CASCADE;
   create TEMPORARY TABLE tt_TEMP_COMPASSET
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(200),
      vcModelId VARCHAR(100),
      txtItemDesc VARCHAR(2000),
      vcSKU VARCHAR(100),
      numBarCodeId VARCHAR(50),
--Department varchar(100),
--dtPurchase datetime,
--dtWarrentyTill datetime,
      vcPathForTImage VARCHAR(250),
      Vendor VARCHAR(250),
      Cost NUMERIC(19,4)
   );


   v_strSql :=
   'SELECT 
IT.numItemCode,
IT.vcItemName,
COALESCE(vcModelId,''''),
COALESCE(txtItemDesc,'''') as txtItemDesc, 
COALESCE(vcSKU,'''') as vcSKU,
COALESCE( cast(numBarCodeId as varchar(50)),'''') as numBarCodeId,
--COALESCE(fn_GetListItemName(numDeptId),'''') as Department,
--dtPurchase as dtPurchase ,
--dtWarrentyTill as dtWarrentyTill,	
COALESCE(
  (SELECT II.vcPathForTImage 
    FROM ItemImages II 
        WHERE II.numItemCode = IT.numItemCode 
    AND II.bitDefault = true 
    AND II.numDomainID =' || SUBSTR(cast(v_numDomainId as VARCHAR(50)),1,50) || ' LIMIT 1),'''')   as vcPathForTImage,
fn_GetComapnyName(IT.numVendorID) as Vendor,
COALESCE(V.monCost,0) as Cost
FROM  Item IT

left outer join DivisionMaster DV on dv.numDivisionId=numVendorID 
left outer join CompanyInfo CI on CI.numCompanyId=DV.numCompanyId
left outer join Vendor V  on V.numDomainId = ' || SUBSTR(cast(v_numDomainId as VARCHAR(50)),1,50) || ' and v.numVendorID=IT.numVendorID and V.numItemCode=IT.numItemCode 
 where 
-- (numOppId is null or numOppId =0) and 
It.numDomainId=' || SUBSTR(cast(v_numDomainId as VARCHAR(50)),1,50) || '
AND IT.bitAsset=true or IT.bitRental=true';
--if @numDivId<>0 set @strSql=@strSql + ' and DV.numDivId=' + cast(@numDivId as varchar(50))
   if v_numItemClassification <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and IT.numItemClassification=' || SUBSTR(cast(v_numItemClassification as VARCHAR(50)),1,50);
   end if;
   v_strSql := coalesce(v_strSql,'') || ' and It.numDomainID=' || SUBSTR(cast(v_numDomainId as VARCHAR(50)),1,50);
   if v_vcSortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' and vcItemName iLike ''' || coalesce(v_vcSortChar,'') || '%''';
   end if;
   if v_vcKeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcKeyWord,'');
   end if;
   if v_vcSortColumn <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_vcSortColumn,'');
   end if;
   if v_vcSort <> '' and v_vcSortColumn <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' ' || coalesce(v_vcSort,'');
   end if;
--SELECT * FROM #Temp_CompAsset 

   RAISE NOTICE '%',v_strSql;

--select * from Companyinfo

   EXECUTE 'insert into tt_TEMP_COMPASSET (
      numItemCode,
      vcItemName,
      vcModelId,
      txtItemDesc,
      vcSKU,
      numBarCodeId,
      vcPathForTImage,
      Vendor,
      Cost
   )' || v_strSql;


   v_firstRec :=(v_CurrentPage -1)*v_PageSize::bigint;                    
   v_lastRec :=(v_CurrentPage*v_PageSize::bigint+1);       
   

   open SWV_RefCur for
   select * from tt_TEMP_COMPASSET where ID > v_firstRec and ID < v_lastRec;                    

   open SWV_RefCur2 for
   SeLECT numAssetItemID,coalesce(vcSerialNo,'') as vcSerialNo, coalesce(vcModelId,'') as vcModelId,
coalesce(CAST(numBarCodeId AS VARCHAR(100)),'') as vcBarCodeId, dtPurchase,dtWarrante,vcLocation
   from CompanyAssetSerial where numAssetItemID in(select numItemCode from tt_TEMP_COMPASSET where ID > v_firstRec and ID < v_lastRec);

   select count(*) INTO v_TotRecs from tt_TEMP_COMPASSET; 

   RETURN;
END; $$;

