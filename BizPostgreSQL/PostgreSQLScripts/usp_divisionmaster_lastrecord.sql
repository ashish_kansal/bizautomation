-- Stored procedure definition script Usp_DivisionMaster_LastRecord for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_DivisionMaster_LastRecord(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
 -- Add the parameters for the stored procedure here  
   
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   AS $$
   DECLARE
   v_DivisionId  INTEGER;
BEGIN
  
  SELECT last_value INTO v_DivisionId FROM divisionmaster_seq;

   open SWV_RefCur for select v_DivisionId::bigint+1 as LastDivision;
   RETURN;
END; $$;           
                    
                    
                      
   


/****** Object:  StoredProcedure [dbo].[USP_GetCaseListForPortal]    Script Date: 15/10/2013 ******/













