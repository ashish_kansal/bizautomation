-- Stored procedure definition script USP_GetCustomerCredits for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCustomerCredits(v_numDomainId NUMERIC(18,0),
    v_numDivisionId NUMERIC(18,0),
    v_numReferenceId NUMERIC(18,0),
    v_tintMode SMALLINT,
    v_numCurrencyID NUMERIC(18,0) DEFAULT 0,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseCurrencyID  NUMERIC;
   v_BaseCurrencySymbol  VARCHAR(3);
   v_tintDepositePage  SMALLINT;
   v_numReturnHeaderID  NUMERIC(18,0);
BEGIN
   v_BaseCurrencySymbol := '$';
	    
   select   coalesce(numCurrencyID,0) INTO v_numBaseCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
   select   coalesce(varCurrSymbol,'$') INTO v_BaseCurrencySymbol FROM Currency WHERE numCurrencyID = v_numBaseCurrencyID;
    
    
   IF v_tintMode = 1 then
		
	
--UNION 
--
--SELECT 2 AS tintRefType,numReturnHeaderID AS numReferenceID,monAmount,monAmountUsed AS monAppliedAmount FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintReturnType IN (1,4)
      v_tintDepositePage := 0;
      IF v_numReferenceId > 0 then
	
         select   tintDepositePage INTO v_tintDepositePage FROM DepositMaster WHERE numDepositId = v_numReferenceId;
      end if;
      RAISE NOTICE '%',v_tintDepositePage;
      IF v_tintDepositePage = 3 then
	
         open SWV_RefCur for
         SELECT tintDepositePage AS tintRefType,
		CASE WHEN tintDepositePage = 3 THEN 'Credit Memo #' || CAST((SELECT  ReturnHeader.vcRMA FROM ReturnHeader WHERE ReturnHeader.numReturnHeaderID = DM.numReturnHeaderID LIMIT 1) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		coalesce(CAST(monDepositAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		coalesce(numReturnHeaderID,0) AS numReturnHeaderID,true AS bitIsPaid,coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0) AS monAmountPaid,
		DM.numCurrencyID,coalesce(C.varCurrSymbol,'') AS varCurrSymbol,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
		,v_BaseCurrencySymbol AS BaseCurrencySymbol,CASE WHEN tintDepositePage = 3 THEN '' ELSE coalesce(DM.vcReference,'') END  AS vcDepositReference
         FROM DepositMaster DM LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
         WHERE DM.numDomainId = v_numDomainId AND numDivisionID = v_numDivisionId AND tintDepositePage IN(3)
         AND numDepositId = v_numReferenceId AND (coalesce(DM.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
         UNION
         SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage = 3 THEN 'Credit Memo #' || CAST((SELECT  ReturnHeader.vcRMA FROM ReturnHeader WHERE ReturnHeader.numReturnHeaderID = DM.numReturnHeaderID LIMIT 1) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		coalesce(CAST(monDepositAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0)  /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		coalesce(numReturnHeaderID,0) AS numReturnHeaderID,false AS bitIsPaid,CAST(0 AS DECIMAL(19,2)) AS monAmountPaid,DM.numCurrencyID,coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
		coalesce(NULLIF(DM.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment,v_BaseCurrencySymbol AS BaseCurrencySymbol,
		CASE WHEN tintDepositePage = 3 THEN '' ELSE coalesce(DM.vcReference,'') END AS vcDepositReference
         FROM DepositMaster DM LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
         WHERE DM.numDomainId = v_numDomainId AND numDivisionID = v_numDivisionId AND tintDepositePage IN(2,3)
         AND(coalesce(CAST(monDepositAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId != v_numReferenceId
         AND (coalesce(DM.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0);
      ELSE
         open SWV_RefCur for
         SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage = 3 THEN 'Credit Memo #' || CAST((SELECT  ReturnHeader.vcRMA FROM ReturnHeader WHERE ReturnHeader.numReturnHeaderID = DM.numReturnHeaderID LIMIT 1) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		coalesce(CAST(monDepositAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0) /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		coalesce(numReturnHeaderID,0) AS numReturnHeaderID,false AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
		,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment,v_BaseCurrencySymbol AS BaseCurrencySymbol,CASE WHEN tintDepositePage = 3 THEN '' ELSE coalesce(DM.vcReference,'') END  AS vcDepositReference
         FROM DepositMaster DM LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
         WHERE DM.numDomainId = v_numDomainId AND numDivisionID = v_numDivisionId AND tintDepositePage IN(2,3)
         AND(coalesce(CAST(monDepositAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId != v_numReferenceId
         AND (coalesce(DM.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
         ORDER BY numDepositId DESC;
      end if;
   ELSEIF v_tintMode = 2
   then
		
	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,numReturnHeaderID AS numReferenceID,monBizDocAmount AS monAmount, ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  
--		FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID
--		AND numReturnHeaderID NOT IN (SELECT numReturnHeaderID FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceId AND tintReferenceType=2)
--		AND (ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0)) >0 AND tintReturnType=2 AND tintReceiveType=2
--		
--	UNION
--	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,RH.numReturnHeaderID AS numReferenceID,RH.monBizDocAmount, ISNULL(RH.monBizDocAmount,0) - ISNULL(RH.monBizDocUsedAmount,0) + ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monAmountPaid  
--		FROM ReturnHeader RH JOIN ReturnPaymentHistory RPH ON RH.numReturnHeaderID=RPH.numReturnHeaderID  
--		WHERE RH.numDomainId=@numDomainId AND RH.numDivisionID=@numDivisionID 
--		AND RPH.numReferenceID=@numReferenceId AND RPH.tintReferenceType=2 AND tintReturnType=2 AND tintReceiveType=2
--		GROUP BY RH.numReturnHeaderID,RH.monBizDocAmount,RH.monBizDocUsedAmount,RH.tintReturnType
--	
	
      v_numReturnHeaderID := 0;
      IF v_numReferenceId > 0 then
	
         select   numReturnHeaderID INTO v_numReturnHeaderID FROM BillPaymentHeader WHERE numBillPaymentID = v_numReferenceId;
      end if;
      IF v_numReturnHeaderID > 0 then
	
         open SWV_RefCur for
         SELECT CAST(1 AS INTEGER) AS tintRefType,RH.vcRMA AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,coalesce(monPaymentAmount,0) -coalesce(monRefundAmount,0) AS monNonAppliedAmount,true AS bitIsPaid,coalesce(monAppliedAmount,0) AS monAmountPaid,coalesce(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
						,coalesce(NULLIF(BPH.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
						,v_BaseCurrencySymbol AS BaseCurrencySymbol
         FROM BillPaymentHeader BPH LEFT JOIN Currency C ON C.numCurrencyID = BPH.numCurrencyID
         LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID = coalesce(BPH.numReturnHeaderID,0)
         WHERE BPH.numDomainId = v_numDomainId AND BPH.numDivisionId = v_numDivisionId
         AND numBillPaymentID = v_numReferenceId AND (coalesce(BPH.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(BPH.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
         UNION
         SELECT CASE WHEN coalesce(BPH.numReturnHeaderID,0) > 0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN coalesce(BPH.numReturnHeaderID,0) > 0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0) -coalesce(monRefundAmount,0) AS monNonAppliedAmount,false AS bitIsPaid,0 AS monAmountPaid,coalesce(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
						,coalesce(NULLIF(BPH.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
						,v_BaseCurrencySymbol AS BaseCurrencySymbol
         FROM BillPaymentHeader BPH LEFT JOIN Currency C ON C.numCurrencyID = BPH.numCurrencyID
         LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID = coalesce(BPH.numReturnHeaderID,0)
         WHERE BPH.numDomainId = v_numDomainId AND BPH.numDivisionId = v_numDivisionId
         AND(coalesce(CAST(monPaymentAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numBillPaymentID != v_numReferenceId
         AND (coalesce(BPH.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(BPH.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0);
      ELSE
         open SWV_RefCur for
         SELECT CASE WHEN coalesce(BPH.numReturnHeaderID,0) > 0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN coalesce(BPH.numReturnHeaderID,0) > 0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0) -coalesce(monRefundAmount,0) AS monNonAppliedAmount,false AS bitIsPaid,0 AS monAmountPaid  ,coalesce(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
						,coalesce(NULLIF(BPH.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
						,v_BaseCurrencySymbol AS BaseCurrencySymbol
         FROM BillPaymentHeader BPH LEFT JOIN Currency C ON C.numCurrencyID = BPH.numCurrencyID
         LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID = coalesce(BPH.numReturnHeaderID,0)
         WHERE BPH.numDomainId = v_numDomainId AND BPH.numDivisionId = v_numDivisionId
         AND(coalesce(CAST(monPaymentAmount AS DECIMAL(19,2)),0) -coalesce(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0.01 AND numBillPaymentID != v_numReferenceId
         AND (coalesce(BPH.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(BPH.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0);
      end if;
   end if;
   RETURN;
END; $$;


