-- Stored procedure definition script USP_ManageSiteCategories for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSiteCategories(v_numSiteID NUMERIC(9,0),
          v_strItems  TEXT  DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   DELETE FROM SiteCategories
   WHERE       numSiteID = v_numSiteID;
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      INSERT INTO SiteCategories(numSiteID,
                  numCategoryID)
      SELECT X.numSiteID,
             X.numCategoryID
      FROM
	  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numSiteID NUMERIC PATH 'numSiteID',
					numCategoryID NUMERIC PATH 'numCategoryID'
			) AS X;

   end if;
   RETURN;
END; $$;



