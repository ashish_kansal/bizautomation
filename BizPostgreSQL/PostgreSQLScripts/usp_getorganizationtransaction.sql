-- Stored procedure definition script USP_GetOrganizationTransaction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOrganizationTransaction(v_numDivisionID NUMERIC(9,0) DEFAULT 0,
      v_numDomainID NUMERIC DEFAULT NULL,
      v_CurrentPage INTEGER DEFAULT 0,
	  v_PageSize INTEGER DEFAULT 0,
      INOUT v_TotRecs INTEGER DEFAULT 0  ,
      v_vcTransactionType VARCHAR(500) DEFAULT NULL,
      v_dtFromDate TIMESTAMP DEFAULT NULL,
      v_dtToDate TIMESTAMP DEFAULT NULL,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	  v_SortCreatedDate SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      Items INTEGER
   );
   INSERT INTO tt_TEMP  SELECT Id FROM SplitIds(v_vcTransactionType,',');

   DROP TABLE IF EXISTS tt_TEMPTRANSACTION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTRANSACTION
   (
      intTransactionType INTEGER,
      vcTransactionType VARCHAR(30),
      dtCreatedDate TIMESTAMP,
      numRecordID NUMERIC,
      vcRecordName VARCHAR(100),
      monTotalAmount DECIMAL(20,5),
      monPaidAmount DECIMAL(20,5),
      dtDueDate TIMESTAMP,
      vcMemo VARCHAR(1000),
      vcDescription VARCHAR(500),
      vcStatus VARCHAR(100),
      numRecordID2 NUMERIC,
      numStatus NUMERIC, 
      vcBizDocId VARCHAR(1000)
   );
		 
		 --Sales Order
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 1) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 1,CAST('Sales Order' AS VARCHAR(30)),OM.bintCreatedDate,OM.numOppId,
		 	OM.vcpOppName,
		 	OM.monDealAmount,
		 	coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(numStatus),0,numStatus,
		 	CAST(coalesce(OM.vcOppRefOrderNo,'') AS VARCHAR(1000))
      FROM OpportunityMaster OM where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 1  AND OM.tintoppstatus =  1
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Sales Order Invoices(BizDocs)
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 2) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 2,CAST('SO Invoice' AS VARCHAR(30)),OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	coalesce(OBD.monAmountPaid,0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	CAST(coalesce(vcRefOrderNo,'') AS VARCHAR(1000))
      FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId
      where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 1  AND OM.tintoppstatus =  1 AND OBD.bitAuthoritativeBizDocs = 1
      AND (OBD.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Sales Opportunity
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 3) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 3,CAST('Sales Opportunity' AS VARCHAR(30)),OM.bintCreatedDate,OM.numOppId,
		 	OM.vcpOppName,OM.monDealAmount,
		 	coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(numStatus),0,numStatus,CAST('' AS VARCHAR(1000))
      FROM OpportunityMaster OM where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 1  AND OM.tintoppstatus =  0
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Purchase Order
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 4) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 4,CAST('Purchase Order' AS VARCHAR(30)),OM.bintCreatedDate,OM.numOppId,
		 	OM.vcpOppName,
		 	OM.monDealAmount,
		 	coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(numStatus),0,numStatus,
		 	CAST(coalesce(OM.vcOppRefOrderNo,'') AS VARCHAR(1000))
      FROM OpportunityMaster OM where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 2  AND OM.tintoppstatus =  1
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Purchase Order Bill(BizDocs)
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 5) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 5,CAST('PO Bill' AS VARCHAR(30)),OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	coalesce(OBD.monAmountPaid,0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	CAST(coalesce(vcRefOrderNo,'') AS VARCHAR(1000))
      FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId
      where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 2  AND OM.tintoppstatus =  1 AND OBD.bitAuthoritativeBizDocs = 1
      AND (OBD.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Purchase Opportunity
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 6) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 6,CAST('Purchase Opportunity' AS VARCHAR(30)),OM.bintCreatedDate,OM.numOppId,
		 	OM.vcpOppName,OM.monDealAmount,
		 	coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(numStatus),0,numStatus,CAST('' AS VARCHAR(1000))
      FROM OpportunityMaster OM where OM.numDomainId = v_numDomainID AND OM.numDivisionId = v_numDivisionID
      AND OM.tintopptype = 2  AND OM.tintoppstatus =  0
      AND (OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Sales Return
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 7) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 7,CAST('Sales Return' AS VARCHAR(30)),RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,coalesce(RH.monAmount,0),
		 	CASE coalesce(RH.tintReceiveType,0) WHEN 1 THEN coalesce((SELECT coalesce(SUM(CH.monAmount),0) FROM CheckHeader CH WHERE CH.numDomainID = 1 AND CH.tintReferenceType = 10 AND CH.numReferenceID = RH.numReturnHeaderID AND coalesce(CH.bitIsPrint,false) = true),0)
      WHEN 2 THEN coalesce((SELECT SUM(coalesce(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainId = RH.numDomainID AND DM.numReturnHeaderID = RH.numReturnHeaderID),0)
      ELSE 0 END,
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,CAST('' AS VARCHAR(1000))
      FROM ReturnHeader RH where RH.numDomainID = v_numDomainID AND RH.numDivisionId = v_numDivisionID
      AND RH.tintReturnType = 1
      AND (RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Purchase Return
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 8) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 8,CAST('Purchase Return' AS VARCHAR(30)),RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,coalesce(RH.monAmount,0),
		 	coalesce((SELECT SUM(coalesce(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainId = RH.numDomainID AND BPH.numReturnHeaderID = RH.numReturnHeaderID),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,CAST('' AS VARCHAR(1000))
      FROM ReturnHeader RH where RH.numDomainID = v_numDomainID AND RH.numDivisionId = v_numDivisionID
      AND RH.tintReturnType = 2
      AND (RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Credit Memo
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 9) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 9,CAST('Credit Memo' AS VARCHAR(30)),RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,coalesce(RH.monAmount,0),
		 	coalesce((SELECT SUM(coalesce(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainId = RH.numDomainID AND DM.numReturnHeaderID = RH.numReturnHeaderID),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,CAST('' AS VARCHAR(1000))
      FROM ReturnHeader RH where RH.numDomainID = v_numDomainID AND RH.numDivisionId = v_numDivisionID
      AND RH.tintReturnType = 3
      AND (RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Refund Receipt
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 10) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 10,CAST('Refund Receipt' AS VARCHAR(30)),RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,coalesce(RH.monAmount,0),
		 	coalesce((SELECT coalesce(SUM(CH.monAmount),0) FROM CheckHeader CH WHERE CH.numDomainID = 1 AND CH.tintReferenceType = 10 AND CH.numReferenceID = RH.numReturnHeaderID AND coalesce(CH.bitIsPrint,false) = true),0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,CAST('' AS VARCHAR(1000))
      FROM ReturnHeader RH where RH.numDomainID = v_numDomainID AND RH.numDivisionId = v_numDivisionID
      AND RH.tintReturnType = 4
      AND (RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Bills
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 11) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 11,CAST('Bills' AS VARCHAR(30)),BH.dtCreatedDate,BH.numBillID,
		 	CAST('Bill-' || CAST(BH.numBillID AS VARCHAR(18)) AS VARCHAR(100)),coalesce(BH.monAmountDue,0),
		 	coalesce(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,CAST('' AS VARCHAR(100)),0,0,CAST('' AS VARCHAR(1000))
      FROM BillHeader BH where BH.numDomainId = v_numDomainID AND BH.numDivisionId = v_numDivisionID
      AND (BH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)
      AND coalesce(BH.bitLandedCost,false) = false;
   end if;
		 
		  --Bills Landed Cost
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 16) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 16,CAST('Landed Cost' AS VARCHAR(30)),BH.dtCreatedDate,BH.numOppId,
		 	CAST('Landed Cost Bill-' || CAST(BH.numBillID AS VARCHAR(18)) AS VARCHAR(100)),coalesce(BH.monAmountDue,0),
		 	coalesce(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,CAST('' AS VARCHAR(100)),0,0,CAST('' AS VARCHAR(1000))
      FROM BillHeader BH where BH.numDomainId = v_numDomainID AND BH.numDivisionId = v_numDivisionID
      AND (BH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)
      AND coalesce(BH.bitLandedCost,false) = true;
   end if;

		 --UnApplied Bill Payments
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 12) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 12,CAST('Unapplied Bill Payments' AS VARCHAR(30)),BPH.dtCreateDate,BPH.numBillPaymentID,
		 	CAST(CASE WHEN coalesce(numReturnHeaderID,0) > 0 THEN 'Return Credit #' || CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END AS VARCHAR(100))
			,coalesce(BPH.monPaymentAmount,0),
		 	coalesce(BPH.monAppliedAmount,0),
		 	NULL,CAST('' AS VARCHAR(1000)),CAST('' AS VARCHAR(500)),CAST('' AS VARCHAR(100)),coalesce(numReturnHeaderID,0),0,CAST('' AS VARCHAR(1000))
      FROM BillPaymentHeader BPH where BPH.numDomainId = v_numDomainID AND BPH.numDivisionId = v_numDivisionID
      AND (BPH.dtCreateDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)
      AND(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) > 0;
   end if;
		 
		 --Checks
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 13) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 13,CAST('Checks' AS VARCHAR(30)),CH.dtCreatedDate,CH.numCheckHeaderID,
		 	CAST(CASE CH.tintReferenceType WHEN 1 THEN 'Checks'
      WHEN 8 THEN 'Bill Payment'
      WHEN 10 THEN 'RMA'
      WHEN 11 THEN 'Payroll'
      ELSE 'Check' END || CASE WHEN coalesce(CH.numCheckNo,0) > 0 THEN ' - #' || CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END AS VARCHAR(100))
			,coalesce(CH.monAmount,0),
		 	CASE WHEN coalesce(bitIsPrint,false) = true THEN coalesce(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,CAST('' AS VARCHAR(500)),CAST('' AS VARCHAR(100)),0,0,CAST('' AS VARCHAR(1000))
      FROM CheckHeader CH where CH.numDomainID = v_numDomainID AND CH.numDivisionID = v_numDivisionID
		 				--AND ch.tintReferenceType=1
      AND (CH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate);
   end if;
		 
		 --Deposits
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 14) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 14,CAST('Deposits' AS VARCHAR(30)),DM.dtCreationDate,DM.numDepositId,
		 	CAST('Deposit' AS VARCHAR(100)),
		 	coalesce(DM.monDepositAmount,0),
		 	coalesce(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,CAST('' AS VARCHAR(100)),0,tintDepositePage,CAST('' AS VARCHAR(1000))
      FROM DepositMaster DM where DM.numDomainId = v_numDomainID AND DM.numDivisionID = v_numDivisionID
      AND (DM.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)
      AND tintDepositePage IN(1,2);
   end if;
		 
		 --Unapplied Payments
   IF EXISTS(SELECT 1 FROM tt_TEMP WHERE Items = 15) then
		 
      INSERT INTO tt_TEMPTRANSACTION
      SELECT 15,CAST('Unapplied Payments' AS VARCHAR(30)),DM.dtCreationDate,DM.numDepositId,
		 	CAST(CASE WHEN tintDepositePage = 3 THEN 'Credit Memo #' || CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS VARCHAR(100)),
		 	coalesce(DM.monDepositAmount,0),
		 	coalesce(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,CAST('' AS VARCHAR(100)),coalesce(numReturnHeaderID,0),tintDepositePage,CAST('' AS VARCHAR(1000))
      FROM DepositMaster DM where DM.numDomainId = v_numDomainID AND DM.numDivisionID = v_numDivisionID
      AND (DM.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)
      AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3);
   end if;
		 
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTRANSACTION;
         
   IF v_SortCreatedDate = 1 then -- Ascending 
		 
      open SWV_RefCur for
      SELECT * FROM(SELECT intTransactionType ,vcTransactionType, dtCreatedDate AS dtActualCreatedDate,FormatedDateFromDate(dtCreatedDate,v_numDomainID) AS dtCreatedDate,numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate AS E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate ASC) AS RowNumber FROM tt_TEMPTRANSACTION) a
      WHERE RowNumber > v_firstRec and RowNumber < v_lastRec order BY  intTransactionType,CAST(dtActualCreatedDate AS DATE) ASC;
   ELSE
      open SWV_RefCur for
      SELECT * FROM(SELECT intTransactionType ,vcTransactionType, dtCreatedDate AS dtActualCreatedDate, FormatedDateFromDate(dtCreatedDate,v_numDomainID) AS dtCreatedDate,numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate AS E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate DESC) AS RowNumber FROM tt_TEMPTRANSACTION) a
      WHERE RowNumber > v_firstRec and RowNumber < v_lastRec order BY  intTransactionType,CAST(dtActualCreatedDate AS DATE) DESC;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetPartNo]    Script Date: 07/26/2008 16:18:11 ******/



