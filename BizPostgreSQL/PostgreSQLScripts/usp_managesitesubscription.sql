-- Stored procedure definition script USP_ManageSiteSubscription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSiteSubscription(v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_numSiteID NUMERIC(18,0) DEFAULT 0,
	v_numContactId NUMERIC(18,0) DEFAULT 0,
	v_vcEmail VARCHAR(50) DEFAULT null,
	v_bitToSubscribe BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numSubscriberID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numCompanyID  NUMERIC(18,0);
BEGIN


   IF (v_bitToSubscribe = true AND (NOT EXISTS(SELECT numSubscriberID
   FROM SiteSubscriberDetails
   WHERE numContactId = v_numContactId
   AND numSiteID = v_numSiteID))) then
	
      INSERT INTO SiteSubscriberDetails(numContactId,
				numSiteID
			   ,bitIsSubscribed
			   ,dtSubscribedDate)
		 VALUES(v_numContactId
			   ,v_numSiteID
			   ,v_bitToSubscribe
			   ,LOCALTIMESTAMP);
		
      open SWV_RefCur for
      SELECT CURRVAL('SiteSubscriberDetails_seq');
   ELSEIF (v_bitToSubscribe = false AND (EXISTS(SELECT SSD.numSubscriberID FROM SiteSubscriberDetails SSD
   INNER JOIN AdditionalContactsInformation ACI
   ON SSD.numContactId = ACI.numContactId
   WHERE ACI.vcEmail = v_vcEmail
   AND ACI.numDomainID = v_numDomainID
   AND SSD.numSiteID = v_numSiteID)))
   then
	
      select   SSD.numSubscriberID INTO v_numSubscriberID FROM SiteSubscriberDetails SSD
      INNER JOIN AdditionalContactsInformation ACI
      ON SSD.numContactId = ACI.numContactId WHERE ACI.vcEmail = v_vcEmail
      AND ACI.numDomainID = v_numDomainID
      AND SSD.numSiteID = v_numSiteID;
      UPDATE SiteSubscriberDetails
      SET bitIsSubscribed = v_bitToSubscribe,dtUnsubscribedDate = LOCALTIMESTAMP
      WHERE numSubscriberID = v_numSubscriberID;
      RETURN;
   ELSE
      RETURN;
   end if;
END; $$;



