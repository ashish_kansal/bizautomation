CREATE OR REPLACE FUNCTION USP_Ecommerce_PageNavigation(v_numModuleID NUMERIC,      
		v_numDomainID NUMERIC,
		v_numGroupID NUMERIC,
		v_numParentID NUMERIC,
		v_Mode INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   with recursive CTE(ID,ParentID,PageNavName,NavURL,ImageURL,SortOrder)
   AS(SELECT
			PND.numPageNavID  AS ID,
			numParentID AS ParentID,
			vcPageNavName AS PageNavName,
			coalesce(vcNavURL,'') AS NavURL,
			vcImageURL AS ImageURL,
			intSortOrder AS SortOrder
   FROM
   PageNavigationDTL PND
   JOIN
   TreeNavigationAuthorization TNA
   ON
   PND.numPageNavID = TNA.numPageNavID
   WHERE
   numModuleID = v_numModuleID
   AND TNA.numDomainID = v_numDomainID
   AND numGroupID = v_numGroupID
   AND PND.numParentID in(v_numParentID)
   AND PND.numPageNavID <> 119
   AND TNA.numTabID = -1
   UNION ALL
   SELECT
	PND.numPageNavID AS ID,
	numParentID AS ParentID,
	vcPageNavName  AS PageNavName,
	coalesce(vcNavURL,'') AS NavURL,
	vcImageURL AS ImageURL,
	intSortOrder AS SortOrder
   FROM
   PageNavigationDTL PND
   JOIN
   TreeNavigationAuthorization TNA
   ON
   PND.numPageNavID = TNA.numPageNavID
   JOIN
   CTE
   ON
   PND.numParentID = CTE.ID
   WHERE
   numModuleID = v_numModuleID
   AND TNA.numDomainID = v_numDomainID
   AND numGroupID = v_numGroupID
   AND PND.numPageNavID != CTE.ID
   AND TNA.numTabID = -1)
   SELECT
	ID AS numPageNavID,
	(CASE ParentID WHEN 73 THEN NULL ELSE ParentID END) AS numParentID ,
	PageNavName AS vcPageNavName,
	NavURL AS vcNavURL,
	ImageURL AS vcImageURL,
	SortOrder AS intSortOrder
   FROM
   CTE
   ORDER BY
   SortOrder;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/













