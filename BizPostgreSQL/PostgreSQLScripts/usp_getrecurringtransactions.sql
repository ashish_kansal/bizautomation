-- Stored procedure definition script USP_GetRecurringTransactions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRecurringTransactions(v_tintRecurringType SMALLINT,
v_dtStartDate TIMESTAMP,
v_dtEndDate TIMESTAMP,
v_numOppRecID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRecurringType = 1 then
	
      open SWV_RefCur for
      SELECT * FROM OpportunityRecurring WHERE tintRecurringType = tintRecurringType;
   end if;
	--Authoritative BizDocs
   IF v_tintRecurringType = 2 then
	
      open SWV_RefCur for
      SELECT    numOppRecID,
				OPR.numOppId,
				OPR.numOppBizDocID,
				OPR.numRecurringId,
				OPR.dtRecurringDate,
				OPR.bitBillingTerms,
				OPR.fltBreakupPercentage,
				OPR.numDomainID,
				CASE OM.tintopptype WHEN 1 THEN(SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = OPR.numDomainID)
      WHEN 2 THEN(SELECT numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE numDomainId = OPR.numDomainID)
      END AS AuthBizDocID,
			   OM.tintopptype,
			   OM.numrecowner,
			   OM.numDivisionId,
			   CASE OPR.bitBillingTerms
      WHEN true THEN(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = OM.intBillingDays)--dbo.GetListIemName(OPR.numBillingDays)
      ELSE 0
      END AS intBillingDays,
			   OPR.numBillingDays, OPR.tintRecurringType/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
			   
      FROM      OpportunityRecurring OPR
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OPR.numOppId
      JOIN Domain D ON D.numDomainId = OM.numDomainId
      WHERE
      OPR.tintRecurringType = tintRecurringType
      AND (OPR.dtRecurringDate BETWEEN v_dtStartDate
      AND     v_dtEndDate
      OR
      v_numOppRecID > 0)
      AND (coalesce(OPR.numOppBizDocID,0) = 0 or (OPR.tintRecurringType = 4 and(select coalesce(tintDeferred,0) from  OpportunityBizDocs where numOppBizDocsId = coalesce(OPR.numOppBizDocID,0) and tintDeferred = 1) = 1))
      AND
				(OPR.numOppRecID = v_numOppRecID OR v_numOppRecID = 0);
   end if;
   RETURN;
END; $$;


