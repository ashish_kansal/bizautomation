-- Function definition script fn_CalItemTaxAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CalItemTaxAmt(v_numDomainID NUMERIC(9,0),
	v_numDivisionID NUMERIC(9,0),
	v_numItemCode NUMERIC(9,0),
	v_numTaxItemID NUMERIC(9,0),
	v_numOppId NUMERIC(9,0),
	v_bitAlwaysTaxApply BOOLEAN,
	v_numReturnHeaderID NUMERIC(9,0))
RETURNS TABLE
(
   decTaxValue DOUBLE PRECISION, 
   tintTaxType SMALLINT,
   numTaxID NUMERIC(18,0)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTaxID  NUMERIC(18,0);
   v_tintBaseTaxCalcOn  SMALLINT;
   v_tintBaseTaxOnArea  SMALLINT;

   v_TaxPercentage  DOUBLE PRECISION;
   v_tintTaxType  SMALLINT DEFAULT 1; -- 1 = PERCENTAGE, 2 = FLAT AMOUNT
   v_bitTaxApplicable  BOOLEAN;
   v_numCountry  NUMERIC(9,0);
   v_numState  NUMERIC(9,0);
   v_vcCity  VARCHAR(100);
   v_vcZipPostal  VARCHAR(20);

   v_tintOppType  SMALLINT;
   v_tintBillType  SMALLINT;
   v_tintShipType  SMALLINT;
   v_bitDropShipAddress  BOOLEAN;
BEGIN
   DROP TABLE IF EXISTS tt_FN_CALITEMTAXAMT_TEMP CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_CALITEMTAXAMT_TEMP
   (
      decTaxValue DOUBLE PRECISION,
      tintTaxType SMALLINT,
      numTaxID NUMERIC(18,0)
   );
   v_numOppId := NULLIF(v_numOppId,0);
   v_numReturnHeaderID := NULLIF(v_numReturnHeaderID,0);

   select   tintBaseTax, tintBaseTaxOnArea INTO v_tintBaseTaxCalcOn,v_tintBaseTaxOnArea FROM Domain WHERE numDomainId = v_numDomainID;

   v_bitTaxApplicable := false; ---Check Tax is applied for the Item

   IF v_numTaxItemID = 0 AND v_numItemCode <> 0 then
	
      select   bitTaxable INTO v_bitTaxApplicable FROM
      Item WHERE
      numItemCode = v_numItemCode;
   ELSEIF v_numItemCode <> 0
   then
	
      select   bitApplicable, numTaxID INTO v_bitTaxApplicable,v_numTaxID FROM
      ItemTax WHERE
      numItemCode = v_numItemCode
      AND numTaxItemID = v_numTaxItemID;
   ELSEIF v_numItemCode = 0
   then
	
      v_bitTaxApplicable := true;
   end if;


	---Check Tax is applied for Company
   if v_bitTaxApplicable = true AND v_bitAlwaysTaxApply = false then
	
      v_bitTaxApplicable := false;
      if v_numTaxItemID = 0 then
         select(case when bitNoTax = true then 0 else 1 end) INTO v_bitTaxApplicable from DivisionMaster where numDivisionID = v_numDivisionID;
      else
         select   bitApplicable INTO v_bitTaxApplicable from DivisionTaxTypes where numDivisionID = v_numDivisionID and numTaxItemID = v_numTaxItemID;
      end if;
   end if;


   if v_bitTaxApplicable = true then
      IF v_numOppId > 0 then
         select   tintopptype, tintBillToType, tintShipToType, coalesce(bitDropShipAddress,false) INTO v_tintOppType,v_tintBillType,v_tintShipType,v_bitDropShipAddress FROM   OpportunityMaster WHERE  numOppId = v_numOppId;
         IF v_tintBaseTaxCalcOn = 1 then --Billing Address
			
            IF v_tintBillType IS NULL or (v_tintBillType = 1 AND v_tintOppType = 1) or (v_tintBillType = 1 AND v_tintOppType = 2) then
				
               select   coalesce(AD.numState,0), coalesce(AD.numCountry,0), coalesce(AD.vcCity,''), coalesce(AD.vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal from AddressDetails AD where AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;
            ELSEIF v_tintBillType = 0
            then
				
               select   coalesce(AD1.numState,0), coalesce(AD1.numCountry,0), coalesce(AD1.vcCity,''), coalesce(AD1.vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM   CompanyInfo Com1 JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
               JOIN Domain D1  ON D1.numDivisionId = div1.numDivisionID
               JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true WHERE  D1.numDomainId = v_numDomainID;
            ELSEIF v_tintBillType = 2 or v_tintBillType = 3
            then
				
               select   coalesce(numBillState,0), coalesce(numBillCountry,0), coalesce(vcBillCity,''), coalesce(vcBillPostCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM   OpportunityAddress WHERE  numOppID = v_numOppId;
            end if;
         ELSE
            IF v_bitDropShipAddress = true then
				
               select   coalesce(numShipState,0), coalesce(numShipCountry,0), coalesce(vcShipCity,''), coalesce(vcShipPostCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM  OpportunityAddress WHERE  numOppID = v_numOppId;
            ELSEIF v_tintShipType IS NULL or (v_tintShipType = 1 AND v_tintOppType = 1) or (v_tintShipType = 1 AND v_tintOppType = 2)
            then
				
               select   coalesce(AD2.numState,0), coalesce(AD2.numCountry,0), coalesce(AD2.vcCity,''), coalesce(AD2.vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal from AddressDetails AD2 where AD2.numDomainID = v_numDomainID AND AD2.numRecordID = v_numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true;
            ELSEIF v_tintShipType = 0
            then
				
               select   coalesce(AD1.numState,0), coalesce(AD1.numCountry,0), coalesce(AD1.vcCity,''), coalesce(AD1.vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM CompanyInfo Com1 JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
               JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
               JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true WHERE  D1.numDomainId = v_numDomainID;
            ELSEIF v_tintShipType = 2 or v_tintShipType = 3
            then
				
               select   coalesce(numShipState,0), coalesce(numShipCountry,0), coalesce(vcShipCity,''), coalesce(vcShipPostCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM  OpportunityAddress WHERE  numOppID = v_numOppId;
            end if;
         end if;
      ELSEIF v_numReturnHeaderID > 0
      then
		
         IF v_tintBaseTaxCalcOn = 1 then --Billing Address
			
            select   coalesce(numBillState,0), coalesce(numBillCountry,0), coalesce(vcBillCity,''), coalesce(vcBillPostCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM   OpportunityAddress WHERE  numReturnHeaderID = v_numReturnHeaderID;
         ELSE
            select   coalesce(numShipState,0), coalesce(numShipCountry,0), coalesce(vcShipCity,''), coalesce(vcShipPostCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM  OpportunityAddress WHERE  numReturnHeaderID = v_numReturnHeaderID;
         end if;
      ELSE
         IF v_tintBaseTaxCalcOn = 2 then --Shipping Address(Default)
            select   coalesce(numState,0), coalesce(numCountry,0), coalesce(vcCity,''), coalesce(vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM     AddressDetails WHERE    numDomainID = v_numDomainID
            AND numRecordID = v_numDivisionID
            AND tintAddressOf = 2
            AND tintAddressType = 2
            AND bitIsPrimary = true;
         ELSE --Billing Address
            select   coalesce(numState,0), coalesce(numCountry,0), coalesce(vcCity,''), coalesce(vcPostalCode,'') INTO v_numState,v_numCountry,v_vcCity,v_vcZipPostal FROM     AddressDetails WHERE    numDomainID = v_numDomainID
            AND numRecordID = v_numDivisionID
            AND tintAddressOf = 2
            AND tintAddressType = 1
            AND bitIsPrimary = true;
         end if;
      end if;
      If EXISTS(SELECT tintBaseTaxOnArea FROM TaxCountryConfi where numDomainID = v_numDomainID and numCountry = v_numCountry) then
		
			--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
         select   tintBaseTaxOnArea INTO v_tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID = v_numDomainID AND numCountry = v_numCountry;
      end if;
      v_TaxPercentage := 0;
      IF v_numCountry > 0 and v_numState > 0 then
		
         IF v_numState > 0 then
			
            IF EXISTS(SELECT * FROM
            TaxDetails
            WHERE
            numCountryID = v_numCountry
            AND numStateID = v_numState
            AND numDomainId = v_numDomainID
            AND numTaxItemID = v_numTaxItemID
            AND 1 =(Case
            when v_tintBaseTaxOnArea = 0 then 1 --State
            when v_tintBaseTaxOnArea = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
            when v_tintBaseTaxOnArea = 2 then Case When vcZipPostal = v_vcZipPostal then 1 else 0 end --Zip/Postal
            else 0 end)) then
				
               IF v_numTaxItemID = 1 then
					
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = v_numState
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID
                  AND 1 =(Case
                  when v_tintBaseTaxOnArea = 0 then 1 --State
                  when v_tintBaseTaxOnArea = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
                  when v_tintBaseTaxOnArea = 2 then Case When vcZipPostal = v_vcZipPostal then 1 else 0 end --Zip/Postal
                  else 0 end);
               ELSE
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT 
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = v_numState
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID
                  AND 1 =(Case
                  when v_tintBaseTaxOnArea = 0 then 1 --State
                  when v_tintBaseTaxOnArea = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
                  when v_tintBaseTaxOnArea = 2 then Case When vcZipPostal = v_vcZipPostal then 1 else 0 end --Zip/Postal
                  else 0 end) LIMIT 1;
               end if;
            ELSEIF EXISTS(SELECT * FROM TaxDetails where numCountryID = v_numCountry and numStateID = v_numState AND numDomainId = v_numDomainID and numTaxItemID = v_numTaxItemID AND coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = '')
            then
				
               IF v_numTaxItemID = 1 then
					
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = v_numState
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID
                  AND coalesce(vcCity,'') = ''
                  AND coalesce(vcZipPostal,'') = '';
               ELSE
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT 
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = v_numState
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID
                  AND coalesce(vcCity,'') = ''
                  AND coalesce(vcZipPostal,'') = '' LIMIT 1;
               end if;
            ELSE
               IF v_numTaxItemID = 1 then
					
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = 0
                  AND coalesce(vcCity,'') = ''
                  AND coalesce(vcZipPostal,'') = ''
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID;
               ELSE
                  INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
							tintTaxType,
							numTaxID)
                  SELECT 
                  decTaxPercentage
							,TaxDetails.tintTaxType
							,TaxDetails.numTaxID
                  FROM
                  TaxDetails
                  WHERE
                  numCountryID = v_numCountry
                  AND numStateID = 0
                  AND coalesce(vcCity,'') = ''
                  AND coalesce(vcZipPostal,'') = ''
                  AND numDomainId = v_numDomainID
                  AND numTaxItemID = v_numTaxItemID LIMIT 1;
               end if;
            end if;
         end if;
      ELSEIF v_numCountry > 0
      then
		
         IF v_numTaxItemID = 1 then
			
            INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
					tintTaxType,
					numTaxID)
            SELECT
            decTaxPercentage
					,TaxDetails.tintTaxType
					,TaxDetails.numTaxID
            FROM
            TaxDetails
            WHERE
            numCountryID = v_numCountry
            AND numStateID = 0
            AND coalesce(vcCity,'') = ''
            AND coalesce(vcZipPostal,'') = ''
            AND numDomainId = v_numDomainID
            AND numTaxItemID = v_numTaxItemID;
         ELSE
            INSERT INTO tt_FN_CALITEMTAXAMT_TEMP(decTaxValue,
					tintTaxType,
					numTaxID)
            SELECT 
            decTaxPercentage
					,TaxDetails.tintTaxType
					,TaxDetails.numTaxID
            FROM
            TaxDetails
            WHERE
            numCountryID = v_numCountry
            AND numStateID = 0
            AND coalesce(vcCity,'') = ''
            AND coalesce(vcZipPostal,'') = ''
            AND numDomainId = v_numDomainID
            AND numTaxItemID = v_numTaxItemID LIMIT 1;
         end if;
      end if;
   end if;

   RETURN QUERY (SELECT * FROM tt_FN_CALITEMTAXAMT_TEMP);
END; $$;

