-- Stored procedure definition script USP_GetContactDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactDetails(v_numDomainId NUMERIC(9,0),        
v_numDivisionId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numDivisionId, (vcFirstName || ' ' || vcLastname) as contactName,numContactId,cast(coalesce(bitPrimaryContact,false) as BOOLEAN) AS bitPrimaryContact from AdditionalContactsInformation
   where numDivisionId = v_numDivisionId and numDomainID = v_numDomainId;
   RETURN;
END; $$;  
















