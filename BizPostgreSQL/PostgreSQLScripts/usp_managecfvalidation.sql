-- Stored procedure definition script USP_ManageCFValidation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCFValidation(v_strFieldList TEXT DEFAULT '',v_numFieldID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM CFW_Validation WHERE numFieldId = v_numFieldID) then

      UPDATE  CFW_Validation
      SET     bitIsRequired = X.bitIsRequired,bitIsNumeric = X.bitIsNumeric,bitIsAlphaNumeric = X.bitIsAlphaNumeric,
      bitIsEmail = X.bitIsEmail,bitIsLengthValidation = X.bitIsLengthValidation,
      intMaxLength = X.intMaxLength,intMinLength = X.intMinLength,
      bitFieldMessage = X.bitFieldMessage,vcFieldMessage = X.vcFieldMessage
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFieldId NUMERIC(9,0) PATH 'numFieldId',
				bitIsRequired BOOLEAN PATH 'bitIsRequired',
				bitIsNumeric BOOLEAN PATH 'bitIsNumeric',
				bitIsAlphaNumeric BOOLEAN PATH 'bitIsAlphaNumeric',
				bitIsEmail BOOLEAN PATH 'bitIsEmail',
				bitIsLengthValidation BOOLEAN PATH 'bitIsLengthValidation',
				intMaxLength INTEGER PATH 'intMaxLength',
				intMinLength INTEGER PATH 'intMinLength',
				bitFieldMessage BOOLEAN PATH 'bitFieldMessage',
				vcFieldMessage VARCHAR(500) PATH 'vcFieldMessage'
		) AS X
      WHERE   CFW_Validation.numFieldId = X.numFieldId;
   ELSE
      INSERT INTO CFW_Validation(numFieldId,
		bitIsRequired,
		bitIsNumeric,
		bitIsAlphaNumeric,
		bitIsEmail,
		bitIsLengthValidation,
		intMaxLength,
		intMinLength,
		bitFieldMessage,
		vcFieldMessage)
      SELECT
      X.numFieldId,
		X.bitIsRequired,
		X.bitIsNumeric,
		X.bitIsAlphaNumeric,
		X.bitIsEmail,
		X.bitIsLengthValidation,
		X.intMaxLength,
		X.intMinLength,
		X.bitFieldMessage,
		X.vcFieldMessage
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFieldId NUMERIC(9,0) PATH 'numFieldId',
				bitIsRequired BOOLEAN PATH 'bitIsRequired',
				bitIsNumeric BOOLEAN PATH 'bitIsNumeric',
				bitIsAlphaNumeric BOOLEAN PATH 'bitIsAlphaNumeric',
				bitIsEmail BOOLEAN PATH 'bitIsEmail',
				bitIsLengthValidation BOOLEAN PATH 'bitIsLengthValidation',
				intMaxLength INTEGER PATH 'intMaxLength',
				intMinLength INTEGER PATH 'intMinLength',
				bitFieldMessage BOOLEAN PATH 'bitFieldMessage',
				vcFieldMessage VARCHAR(500) PATH 'vcFieldMessage'
		) AS X;
	  
   end if;

   RETURN;
END; $$;


