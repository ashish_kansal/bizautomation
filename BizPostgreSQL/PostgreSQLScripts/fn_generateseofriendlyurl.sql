-- Function definition script fn_GenerateSEOFriendlyURL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION fn_GenerateSEOFriendlyURL(v_CategoryName VARCHAR(1000))
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CName  VARCHAR(1000);
BEGIN
   v_CName := '';
   if v_CategoryName <> '' then

      v_CName := LOWER(v_CategoryName);
      v_CName := LTRIM(v_CName);
      v_CName := RTRIM(v_CName);
      v_CName := REGEXP_REPLACE(v_CName,'/&/w+;','');
      v_CName := REGEXP_REPLACE(v_CName,'[^a-z0-9\\-\\s()]','');
      v_CName := REGEXP_REPLACE(v_CName,'[\\s-]+','-');
      v_CName := REPLACE(v_CName,' ','-');
      v_CName := REGEXP_REPLACE(v_CName,'-{2,}','-');
      v_CName := SUBSTR(v_CName,length(v_CName) -(LENGTH(v_CName)+1 -coalesce(POSITION(substring(v_CName from '[^-]') IN v_CName),0))+1);
      v_CName := SUBSTR(v_CName,1,LENGTH(v_CName)+1 -coalesce(POSITION(substring(v_CName from '[^-]') IN v_CName),0));
      v_CName := REPLACE(v_CName,'-',' ');
      IF(LENGTH(v_CName) > 80) then

         v_CName := SUBSTR(v_CName,0,80);
      end if;
   end if;

   Return(v_CName);
END; $$;

