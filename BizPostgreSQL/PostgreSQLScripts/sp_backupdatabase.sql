-- Stored procedure definition script sp_BackupDatabase for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


-- ============================================= 
-- Author: Edgewood Solutions 
-- Create date: 2007-02-07 
-- Description: Backup Database 
-- Parameter1: databaseName 
-- Parameter2: backupType F=full, D=differential, L=log
-- ============================================= 
CREATE OR REPLACE FUNCTION sp_BackupDatabase(v_databaseName VARCHAR(128), v_backupType CHAR(1))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sqlCommand  VARCHAR(1000); 
   v_dateTime  VARCHAR(20);
BEGIN
 

   v_dateTime := REPLACE(TO_CHAR(LOCALTIMESTAMP,'yyyy/mm/dd'),'/','') ||
   REPLACE(TO_CHAR(LOCALTIMESTAMP,'hh24:mi:ss'),':','');  

   IF v_backupType = 'F' then
      v_sqlCommand := 'BACKUP DATABASE ' || coalesce(v_databaseName,'') ||
      E' TO DISK = ''E:\\Database BKP\\' || coalesce(v_databaseName,'') || '_Full_' || coalesce(v_dateTime,'') || '.BAK''';
   end if; 
        
   IF v_backupType = 'D' then
      v_sqlCommand := 'BACKUP DATABASE ' || coalesce(v_databaseName,'') ||
      E' TO DISK = ''E:\\Database BKP\\' || coalesce(v_databaseName,'') || '_Diff_' || coalesce(v_dateTime,'') || '.BAK'' WITH DIFFERENTIAL';
   end if; 
        
   IF v_backupType = 'L' then
      v_sqlCommand := 'BACKUP LOG ' || coalesce(v_databaseName,'') ||
      E' TO DISK = ''E:\\Database BKP\\' || coalesce(v_databaseName,'') || '_Log_' || coalesce(v_dateTime,'') || '.TRN''';
   end if; 
        
   EXECUTE v_sqlCommand;
   RETURN;
END; $$;
/*
sp_BackupDatabase 'TotalBiz2009', 'F'
GO
sp_BackupDatabase 'model', 'F'
GO
sp_BackupDatabase 'msdb', 'F'
GO
*/




