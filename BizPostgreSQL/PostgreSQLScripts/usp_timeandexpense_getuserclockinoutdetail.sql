-- Stored procedure definition script USP_TimeAndExpense_GetUserClockInOutDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TimeAndExpense_GetUserClockInOutDetail(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_dtFromDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   FormatedDateFromDate(v_dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS "vcDate"
		,TO_CHAR(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'h:mm tt') AS "vcCheckedIn"
		,TO_CHAR(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'h:mm tt') AS "vcCheckedOut"
		,CONCAT(TO_CHAR(FLOOR(CAST((EXTRACT(DAY FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))*60*24+EXTRACT(HOUR FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))*60+EXTRACT(MINUTE FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))) AS decimal)/60),'00'),':',TO_CHAR(MOD(CAST((EXTRACT(DAY FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))*60*24+EXTRACT(HOUR FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))*60+EXTRACT(MINUTE FROM dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) -dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))) AS INTEGER),60),'00')) AS "vcTime"
		,(EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS "numTimeInMinutes"
   FROM
   timeandexpense
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   AND numCategory = 1
   AND numtype = 7
   AND CAST(dtFromDate AS DATE) = CAST(v_dtFromDate AS DATE);
END; $$;












