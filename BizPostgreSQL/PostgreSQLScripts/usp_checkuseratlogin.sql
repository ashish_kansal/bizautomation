DROP FUNCTION IF EXISTS USP_CheckUserAtLogin;

CREATE OR REPLACE FUNCTION USP_CheckUserAtLogin(v_UserName VARCHAR(100) DEFAULT '',                                                                        
	v_vcPassword VARCHAR(100) DEFAULT '',
	v_vcLinkedinId VARCHAR(300) DEFAULT '',
	v_numDomainID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $BODY$
   DECLARE
   v_listIds  TEXT;
   v_bitIsBusinessPortalUser  BOOLEAN DEFAULT 0;
   v_numUserCount  INTEGER DEFAULT 0;
BEGIN

    -- This procedure was converted on Wed Apr 07 20:42:42 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   select  string_agg(UDTL.numUserId::VARCHAR,',') INTO v_listIds FROM
   UnitPriceApprover U
   Join
   Domain D
   on
   D.numDomainId = U.numDomainID
   Join
   Subscribers S
   on
   S.numTargetDomainID = D.numDomainId
   LEFT JOIN
   UserMaster As UDTL
   ON
   U.numUserID = UDTL.numUserDetailId WHERE
   1 =(CASE
   WHEN v_vcLinkedinId = 'N' THEN(CASE WHEN LOWER(UDTL.vcEmailID) = LOWER(v_UserName) and LOWER(UDTL.vcPassword) = LOWER(v_vcPassword) THEN 1 ELSE 0 END)
   ELSE(CASE WHEN UDTL.vcLinkedinId = v_vcLinkedinId THEN 1 ELSE 0 END)
   END);
       

	 

	-- If request came from domain url so first check whether external user with vcusername and password exists
   IF coalesce(v_numDomainID,0) > 0 then
	
      v_numUserCount := coalesce((SELECT
      COUNT(*)
      FROM
      AdditionalContactsInformation A
      INNER JOIN
      ExtranetAccountsDtl E
      ON
      A.numContactId = cast(NULLIF(E.numContactID,'') as NUMERIC(18,0))
      INNER JOIN
      ExtarnetAccounts EA
      ON
      E.numExtranetID = EA.numExtranetID
      WHERE
      A.numDomainID = v_numDomainID
      AND EA.numDomainID = v_numDomainID
      AND (LOWER(E.vcUserName) = LOWER(v_UserName) OR LOWER(vcEmail) = LOWER(v_UserName))
      AND LOWER(vcPassword) = LOWER(v_vcPassword)
      AND bitPartnerAccess = true),0);

		/*check subscription validity */
      IF EXISTS(SELECT * FROM
      AdditionalContactsInformation A
      INNER JOIN
      ExtranetAccountsDtl E
      ON
      A.numContactId = cast(NULLIF(E.numContactID,'') as NUMERIC(18,0))
      INNER JOIN
      ExtarnetAccounts EA
      ON
      E.numExtranetID = EA.numExtranetID
      INNER JOIN
      Domain D
      ON
      EA.numDomainID = D.numDomainId
      INNER JOIN
      Subscribers S
      ON
      S.numTargetDomainID = D.numDomainId
      WHERE
      A.numDomainID = v_numDomainID
      AND EA.numDomainID = v_numDomainID
      AND (LOWER(E.vcUserName) = LOWER(v_UserName) OR LOWER(vcEmail) = LOWER(v_UserName))
      AND LOWER(vcPassword) = LOWER(v_vcPassword)
      AND bitPartnerAccess = true
      AND bitActive = 1
      AND TIMEZONE('UTC',now()) > dtSubEndDate) then
		
         RAISE EXCEPTION 'SUB_RENEW';
         RETURN;
      end if;
      v_bitIsBusinessPortalUser := true;
   end if;

	-- If no external user found then check for regular biz users
   IF coalesce(v_numUserCount,0) = 0 then
	
      v_numUserCount := coalesce((SELECT
      COUNT(*)
      FROM
      UserMaster U
      JOIN
      Domain D
      ON
      D.numDomainId = U.numDomainID
      JOIN
      Subscribers S
      ON
      S.numTargetDomainID = D.numDomainId
      WHERE
      1 =(CASE
      WHEN v_vcLinkedinId = 'N'
      THEN(CASE WHEN LOWER(vcEmailID) = LOWER(v_UserName) and LOWER(vcPassword) = LOWER(v_vcPassword) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN vcLinkedinId = v_vcLinkedinId THEN 1 ELSE 0 END)
      END)
      AND bitActive = 1),0);
      v_bitIsBusinessPortalUser := false;

		/*check subscription validity */
      IF EXISTS(SELECT * FROM
      UserMaster U
      JOIN
      Domain D
      ON
      D.numDomainId = U.numDomainID
      JOIN
      Subscribers S
      ON
      S.numTargetDomainID = D.numDomainId
      WHERE
      1 =(CASE
      WHEN v_vcLinkedinId = 'N' THEN(CASE WHEN LOWER(vcEmailID) = LOWER(v_UserName) and LOWER(vcPassword) = LOWER(v_vcPassword) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN vcLinkedinId = v_vcLinkedinId THEN 1 ELSE 0 END)
      END)
      AND bitActive = 1
      AND TIMEZONE('UTC',now()) > dtSubEndDate) then
		
         RAISE EXCEPTION 'SUB_RENEW';
         RETURN;
      end if;
   end if;

   IF v_numUserCount > 1 then
	
      RAISE EXCEPTION 'MULTIPLE_RECORDS_WITH_SAME_EMAIL';
      RETURN;
   end if;   

   IF v_bitIsBusinessPortalUser = true then
	
      open SWV_RefCur for
      SELECT 
      0 AS numUserID
			,EAD.numExtranetDtlID
			,A.numContactId AS numUserDetailId
			,D.numCost
			,coalesce(A.vcEmail,'') AS vcEmailID
			,'' AS vcEmailAlias
			,'' AS vcEmailAliasPassword
			,coalesce(numGroupId,0) AS numGroupID
			,true AS bitActivateFlag
			,'' AS vcMailNickName
			,'' AS txtSignature
			,coalesce(EAD.numDomainID,0) AS numDomainID
			,coalesce(EA.numDivisionID,0) AS numDivisionID
			,coalesce(vcCompanyName,'') AS vcCompanyName
			,false AS bitExchangeIntegration
			,false AS bitAccessExchange
			,'' AS vcExchPath
			,'' AS vcExchDomain
			,'' AS vcExchUserName
			,coalesce(vcFirstName,'') || ' ' || coalesce(vcLastName,'') as ContactName
			,'' as vcExchPassword
			,tintCustomPagingRows
			,vcDateFormat
			,numDefCountry
			,tintComposeWindow
			,TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval) as StartDate
			,TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval)+CAST(sintNoofDaysInterval || 'day' as interval) as EndDate
			,tintAssignToCriteria
			,bitIntmedPage
			,tintFiscalStartMonth
			,numAdminID
			,coalesce(A.numTeam,0) AS numTeam
			,coalesce(D.vcCurrency,'') as vcCurrency
			,coalesce(D.numCurrencyID,0) as numCurrencyID,
			coalesce(D.bitMultiCurrency,false) as bitMultiCurrency,
			bitTrial,coalesce(tintChrForComSearch,0) as tintChrForComSearch,
			coalesce(tintChrForItemSearch,0) as tintChrForItemSearch,
			'' as vcSMTPServer  ,
			0 as numSMTPPort
			,false AS bitSMTPAuth
			,'' AS vcSmtpPassword
			,false AS bitSMTPSSL
			,false AS bitSMTPServer,
			false AS bitImapIntegration, coalesce(charUnitSystem,'E')   AS UnitSystem, DATE_PART('day',dtSubEndDate -TIMEZONE('UTC',now()):: timestamp) as NoOfDaysLeft,
			coalesce(bitCreateInvoice,0) AS bitCreateInvoice,
			coalesce(numDefaultSalesBizDocId,0) AS numDefaultSalesBizDocId,
			coalesce(numDefaultPurchaseBizDocId,0) AS numDefaultPurchaseBizDocId,
			vcDomainName,
			S.numSubscriberID,
			D.tintBaseTax,
			'' AS ProfilePic,
			coalesce(D.numShipCompany,0) AS numShipCompany,
			coalesce(D.bitEmbeddedCost,false) AS bitEmbeddedCost,
			(Select coalesce(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId = D.numDomainId LIMIT 1) AS numAuthoritativeSales,
			(Select coalesce(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId = D.numDomainId LIMIT 1) AS numAuthoritativePurchase,
			coalesce(D.numDefaultSalesOppBizDocId,0) AS numDefaultSalesOppBizDocId,
			coalesce(tintDecimalPoints,2) AS tintDecimalPoints,
			coalesce(D.tintBillToForPO,0) AS tintBillToForPO,
			coalesce(D.tintShipToForPO,0) AS tintShipToForPO,
			coalesce(D.tintOrganizationSearchCriteria,0) AS tintOrganizationSearchCriteria,
			coalesce(D.tintLogin,0) AS tintLogin,
			coalesce(S.intFullUserConcurrency,0) AS intFullUserConcurrency,
			coalesce(bitDocumentRepositary,false) AS bitDocumentRepositary,
			coalesce(D.vcPortalName,'') AS vcPortalName,
			(SELECT COUNT(*) FROM Subscribers WHERE TIMEZONE('UTC',now()) between dtSubStartDate and dtSubEndDate and bitActive = 1),
			coalesce(D.bitGtoBContact,false) AS bitGtoBContact,
			coalesce(D.bitBtoGContact,false) AS bitBtoGContact,
			coalesce(D.bitGtoBCalendar,false) AS bitGtoBCalendar,
			coalesce(D.bitBtoGCalendar,false) AS bitBtoGCalendar,
			coalesce(bitExpenseNonInventoryItem,false) AS bitExpenseNonInventoryItem,
			coalesce(D.bitInlineEdit,false) AS bitInlineEdit,
			0 AS numDefaultClass,
			coalesce(D.bitRemoveVendorPOValidation,false) AS bitRemoveVendorPOValidation,
			coalesce(D.bitTrakDirtyForm,true) AS bitTrakDirtyForm,
			coalesce(D.tintBaseTaxOnArea,0) AS tintBaseTaxOnArea,
			coalesce(D.bitAmountPastDue,false) AS bitAmountPastDue,
			CAST(coalesce(D.monAmountPastDue,0) AS DECIMAL(18,2)) AS monAmountPastDue,
			coalesce(D.bitSearchOrderCustomerHistory,false) AS bitSearchOrderCustomerHistory,
			0 as tintTabEvent,
			coalesce(D.bitRentalItem,false) as bitRentalItem,
			coalesce(D.numRentalItemClass,0) as numRentalItemClass,
			coalesce(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
			coalesce(D.numRentalDailyUOM,0) as numRentalDailyUOM,
			coalesce(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
			coalesce(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
			coalesce(D.tintDefaultClassType,0) as tintDefaultClassType,
			0 AS numDefaultWarehouse,
			coalesce(D.tintPriceBookDiscount,0) AS tintPriceBookDiscount,
			coalesce(D.bitAutoSerialNoAssign,false) AS bitAutoSerialNoAssign,
			coalesce(IsEnableProjectTracking,false) AS IsEnableProjectTracking,
			coalesce(IsEnableCampaignTracking,false) AS IsEnableCampaignTracking,
			coalesce(IsEnableResourceScheduling,false) AS IsEnableResourceScheduling,
			coalesce(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
			coalesce(D.bitAutolinkUnappliedPayment,false) AS bitAutolinkUnappliedPayment,
			coalesce(numShippingServiceItemID,0) AS numShippingServiceItemID,
			coalesce(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
			coalesce(D.tintCommissionType,1) AS tintCommissionType,
			coalesce(D.bitDiscountOnUnitPrice,false) AS bitDiscountOnUnitPrice,
			coalesce(D.intShippingImageWidth,0) AS intShippingImageWidth ,
			coalesce(D.intShippingImageHeight,0) AS intShippingImageHeight,
			coalesce(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
			coalesce(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
			coalesce(D.bitUseBizdocAmount,true) AS bitUseBizdocAmount,
			coalesce(D.bitDefaultRateType,true) AS bitDefaultRateType,
			coalesce(D.bitIncludeTaxAndShippingInCommission,true) AS bitIncludeTaxAndShippingInCommission,
			coalesce(D.bitLandedCost,false) AS bitLandedCost,
			coalesce(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
			coalesce(D.bitMinUnitPriceRule,false) AS bitMinUnitPriceRule,
			coalesce(v_listIds,'') AS vcUnitPriceApprover,
			coalesce(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
			coalesce(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
			coalesce(D.IsEnableDeferredIncome,false) AS IsEnableDeferredIncome,
			coalesce(D.bitEnableItemLevelUOM,false) AS bitEnableItemLevelUOM,
			coalesce(D.bitPurchaseTaxCredit,false) AS bitPurchaseTaxCredit,
			NULL AS bintCreatedDate,
			(CASE WHEN LENGTH(coalesce(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN coalesce(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE coalesce(TempDefaultPage.vcDefaultNavURL,'') END) AS vcDefaultNavURL,
			coalesce(D.bitApprovalforTImeExpense,false) AS bitApprovalforTImeExpense,
			coalesce(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,coalesce(D.bitApprovalforOpportunity,false) AS bitApprovalforOpportunity,
			coalesce(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
			coalesce(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
			coalesce(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
			coalesce((SELECT  bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID = D.numDomainId AND coalesce(bitMarginPriceViolated,false) = true LIMIT 1),false) AS bitMarginPriceViolated,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 2 AND numPageID = 2 LIMIT 1),0) AS tintLeadRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 3 AND numPageID = 2 LIMIT 1),0) AS tintProspectRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 4 AND numPageID = 2 LIMIT 1),0) AS tintAccountRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 10 AND numPageID = 10 LIMIT 1),0) AS tintSalesOrderListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 10 AND numPageID = 11 LIMIT 1),0) AS tintPurchaseOrderListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 10 AND numPageID = 2 LIMIT 1),0) AS tintSalesOpportunityListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 10 AND numPageID = 8 LIMIT 1),0) AS tintPurchaseOpportunityListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 11 AND numPageID = 2 LIMIT 1),0) AS tintContactListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 7 AND numPageID = 2 LIMIT 1),0) AS tintCaseListRights,
			coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = EA.numGroupID AND numModuleID = 12 AND numPageID = 2 LIMIT 1),0) AS tintProjectListRights,
			coalesce((SELECT  numCriteria FROM TicklerListFilterConfiguration WHERE POSITION(CONCAT(',',A.numContactId,',') IN CONCAT(',',vcSelectedEmployee,',')) > 0 LIMIT 1),0) AS tintActionItemRights,
			coalesce(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
			coalesce(D.bitAllowDuplicateLineItems,false) AS bitAllowDuplicateLineItems,
			coalesce(D.bitLogoAppliedToBizTheme,false) AS bitLogoAppliedToBizTheme,
			coalesce(D.vcLogoForBizTheme,'') AS vcLogoForBizTheme,
			coalesce(D.bitLogoAppliedToLoginBizTheme,false) AS bitLogoAppliedToLoginBizTheme,
			coalesce(D.vcLogoForLoginBizTheme,'') AS vcLogoForLoginBizTheme,
			coalesce(D.vcThemeClass,'1473b4') AS vcThemeClass,
			coalesce(D.vcLoginURL,'') AS vcLoginURL,
			coalesce(D.bitDisplayCustomField,false) AS bitDisplayCustomField,
			coalesce(D.bitFollowupAnytime,false) AS bitFollowupAnytime,
			coalesce(D.bitpartycalendarTitle,false) AS bitpartycalendarTitle,
			coalesce(D.bitpartycalendarLocation,false) AS bitpartycalendarLocation,
			coalesce(D.bitpartycalendarDescription,false) AS bitpartycalendarDescription,
			coalesce(D.bitREQPOApproval,false) AS bitREQPOApproval,
			coalesce(D.bitARInvoiceDue,false) AS bitARInvoiceDue,
			coalesce(D.bitAPBillsDue,false) AS bitAPBillsDue,
			coalesce(D.bitItemsToPickPackShip,false) AS bitItemsToPickPackShip,
			coalesce(D.bitItemsToInvoice,false) AS bitItemsToInvoice,
			coalesce(D.bitSalesOrderToClose,false) AS bitSalesOrderToClose,
			coalesce(D.bitItemsToPutAway,false) AS bitItemsToPutAway,
			coalesce(D.bitItemsToBill,false) AS bitItemsToBill,
			coalesce(D.bitPosToClose,false) AS bitPosToClose,
			coalesce(D.bitPOToClose,false) AS bitPOToClose,
			coalesce(D.bitBOMSToPick,false) AS bitBOMSToPick,
			coalesce(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
			coalesce(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
			coalesce(D.vchAPBillsDue,'') AS vchAPBillsDue,
			coalesce(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
			coalesce(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
			coalesce((select string_agg(UPC.numUserID::VARCHAR,',' order by UPC.numUserID)
         from UnitPriceApprover UPC
         where UPC.numDomainID = D.numDomainId),'') AS vchPriceMarginApproval,
			coalesce(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
			coalesce(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
			coalesce(D.vchItemsToBill,'') AS vchItemsToBill,
			coalesce(D.vchPosToClose,'') AS vchPosToClose,
			coalesce(D.vchPOToClose,'') AS vchPOToClose,
			coalesce(D.vchBOMSToPick,'') AS vchBOMSToPick,
			coalesce(D.decReqPOMinValue,0) AS decReqPOMinValue,
			0 AS tintPayrollType,
			(CASE WHEN EXISTS(SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID = A.numContactId AND numCategory = 1 AND numType = 7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) AS bitUserClockedIn,
			coalesce(D.bitUseOnlyActionItems,false) AS bitUseOnlyActionItems,
			coalesce(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
			coalesce(D.bitDisplayContractElement,false) AS bitDisplayContractElement,
			coalesce(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
			coalesce(D.bitEnableSmartyStreets,false) AS bitEnableSmartyStreets,
			coalesce(D.vcSmartyStreetsAPIKeys,'') AS vcSmartyStreetsAPIKeys
      FROM
      ExtranetAccountsDtl EAD
      INNER JOIN
      ExtarnetAccounts EA
      ON
      EAD.numExtranetID = EA.numExtranetID
      INNER JOIN
      Domain D
      ON
      D.numDomainId = EAD.numDomainID
      INNER JOIN
      AdditionalContactsInformation A
      ON
      A.numContactId = cast(NULLIF(EAD.numContactID,'') as NUMERIC(18,0))
      INNER JOIN
      Subscribers S
      ON
      S.numTargetDomainID = D.numDomainId
      LEFT JOIN
      DivisionMaster Div
      ON
      EA.numDivisionID = Div.numDivisionID
      LEFT JOIN
      CompanyInfo C
      ON
      C.numCompanyId = Div.numCompanyID
      LEFT JOIN LATERAL(SELECT
         REPLACE(SCB.Link,'../','') AS vcDefaultNavURL
         FROM
         TabMaster T
         JOIN
         GroupTabDetails G ON G.numTabId = T.numTabId
         LEFT JOIN
         ShortCutGrpConf SCUC ON G.numTabId = SCUC.numTabId AND SCUC.numDomainId = EAD.numDomainID AND SCUC.numGroupId = EA.numGroupID AND bitDefaultTab = true
         LEFT JOIN
         ShortCutBar SCB
         ON
         SCB.Id = SCUC.numLinkId
         WHERE
				(T.numDomainID = EAD.numDomainID OR bitFixed = true)
         AND G.numGroupId = EA.numGroupID
         AND coalesce(G.tintType,0) <> 1
         AND tintTabType = 1
         AND T.numTabId NOT IN(2,68)
         ORDER BY
         SCUC.bitInitialPage DESC LIMIT 1)  TempDefaultPage
      LEFT JOIN LATERAL(SELECT
         REPLACE(T.vcURL,'../','') AS vcDefaultNavURL
         FROM
         TabMaster T
         JOIN
         GroupTabDetails G
         ON
         G.numTabId = T.numTabId
         WHERE
				(T.numDomainID = EAD.numDomainID OR bitFixed = true)
         AND G.numGroupId = EA.numGroupID
         AND coalesce(G.tintType,0) <> 1
         AND tintTabType = 1
         AND T.numTabId NOT IN(2,68)
         AND coalesce(G.bitInitialTab,false) = true
         AND coalesce(bitallowed,false) = true
         ORDER BY
         G.bitInitialTab DESC LIMIT 1) TEMPDefaultTab on TRUE on TRUE
      WHERE
			(LOWER(EAD.vcUserName) = LOWER(v_UserName) OR LOWER(A.vcEmail) = LOWER(v_UserName))
      AND LOWER(vcPassword) = LOWER(v_vcPassword)
      AND bitPartnerAccess = true
      AND TIMEZONE('UTC',now()) BETWEEN dtSubStartDate and dtSubEndDate LIMIT 1;
   ELSE
      open SWV_RefCur for
      SELECT 
      U.numUserId
		,numUserDetailId
		,D.numCost
		,coalesce(vcEmailID,'') AS vcEmailID
		,coalesce(vcEmailAlias,'') AS vcEmailAlias
		,coalesce(vcEmailAliasPassword,'') AS vcEmailAliasPassword
		,coalesce(numGroupId,0) AS numGroupID
		,bitActivateFlag
		,coalesce(vcMailNickName,'') AS vcMailNickName
		,coalesce(txtSignature,'') AS txtSignature
		,coalesce(U.numDomainID,0) AS numDomainID
		,coalesce(Div.numDivisionID,0) AS numDivisionID
		,coalesce(vcCompanyName,'') AS vcCompanyName
		,coalesce(E.bitExchangeIntegration,false) AS bitExchangeIntegration
		,coalesce(E.bitAccessExchange,false) AS bitAccessExchange
		,coalesce(E.vcExchPath,'') AS vcExchPath
		,coalesce(E.vcExchDomain,'') AS vcExchDomain
		,coalesce(D.vcExchUserName,'') AS vcExchUserName
		,coalesce(vcFirstName,'') || ' ' || coalesce(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange = false then  E.vcExchPassword when E.bitAccessExchange = true then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval) as StartDate
		,TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval)+CAST(sintNoofDaysInterval || 'day' as interval) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,coalesce(A.numTeam,0) AS numTeam
		,coalesce(D.vcCurrency,'') as vcCurrency
		,coalesce(D.numCurrencyID,0) as numCurrencyID,
		coalesce(D.bitMultiCurrency,false) as bitMultiCurrency,
		bitTrial,coalesce(tintChrForComSearch,0) as tintChrForComSearch,
		coalesce(tintChrForItemSearch,0) as tintChrForItemSearch,
		case when bitSMTPServer = true then vcSMTPServer else '' end as vcSMTPServer  ,
		case when bitSMTPServer = true then  coalesce(numSMTPPort,0)  else 0 end as numSMTPPort
		,coalesce(bitSMTPAuth,false) AS bitSMTPAuth
		,coalesce(vcSmtpPassword,'') AS vcSmtpPassword
		,coalesce(bitSMTPSSL,false) AS bitSMTPSSL   ,coalesce(bitSMTPServer,false) AS bitSMTPServer,
		coalesce(IM.bitImap,false) AS bitImapIntegration, coalesce(charUnitSystem,'E')   AS UnitSystem, DATE_PART('day',dtSubEndDate -TIMEZONE('UTC',now()):: timestamp) as NoOfDaysLeft,
		coalesce(bitCreateInvoice,0) AS bitCreateInvoice,
		coalesce(numDefaultSalesBizDocId,0) AS numDefaultSalesBizDocId,
		coalesce(numDefaultPurchaseBizDocId,0) AS numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.tintBaseTax,
		U.ProfilePic,
		coalesce(D.numShipCompany,0) AS numShipCompany,
		coalesce(D.bitEmbeddedCost,false) AS bitEmbeddedCost,
		(Select coalesce(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId = D.numDomainId LIMIT 1) AS numAuthoritativeSales,
		(Select coalesce(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId = D.numDomainId LIMIT 1) AS numAuthoritativePurchase,
		coalesce(D.numDefaultSalesOppBizDocId,0) AS numDefaultSalesOppBizDocId,
		coalesce(tintDecimalPoints,2) AS tintDecimalPoints,
		coalesce(D.tintBillToForPO,0) AS tintBillToForPO,
		coalesce(D.tintShipToForPO,0) AS tintShipToForPO,
		coalesce(D.tintOrganizationSearchCriteria,0) AS tintOrganizationSearchCriteria,
		coalesce(D.tintLogin,0) AS tintLogin,
			coalesce(S.intFullUserConcurrency,0) AS intFullUserConcurrency,
		coalesce(bitDocumentRepositary,false) AS bitDocumentRepositary,
		coalesce(D.vcPortalName,'') AS vcPortalName,
		(SELECT COUNT(*) FROM Subscribers WHERE TIMEZONE('UTC',now()) between dtSubStartDate and dtSubEndDate and bitActive = 1),
		coalesce(D.bitGtoBContact,false) AS bitGtoBContact,
		coalesce(D.bitBtoGContact,false) AS bitBtoGContact,
		coalesce(D.bitGtoBCalendar,false) AS bitGtoBCalendar,
		coalesce(D.bitBtoGCalendar,false) AS bitBtoGCalendar,
		coalesce(bitExpenseNonInventoryItem,false) AS bitExpenseNonInventoryItem,
		coalesce(D.bitInlineEdit,false) AS bitInlineEdit,
		coalesce(U.numDefaultClass,0) AS numDefaultClass,
		coalesce(D.bitRemoveVendorPOValidation,false) AS bitRemoveVendorPOValidation,
		coalesce(D.bitTrakDirtyForm,true) AS bitTrakDirtyForm,
		coalesce(D.tintBaseTaxOnArea,0) AS tintBaseTaxOnArea,
		coalesce(D.bitAmountPastDue,false) AS bitAmountPastDue,
		CAST(coalesce(D.monAmountPastDue,0) AS DECIMAL(18,2)) AS monAmountPastDue,
		coalesce(D.bitSearchOrderCustomerHistory,false) AS bitSearchOrderCustomerHistory,
		coalesce(U.tintTabEvent,0) as tintTabEvent,
		coalesce(D.bitRentalItem,false) as bitRentalItem,
		coalesce(D.numRentalItemClass,0) as numRentalItemClass,
		coalesce(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		coalesce(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		coalesce(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		coalesce(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		coalesce(D.tintDefaultClassType,0) as tintDefaultClassType,
		coalesce(U.numDefaultWarehouse,0) AS numDefaultWarehouse,
		coalesce(D.tintPriceBookDiscount,0) AS tintPriceBookDiscount,
		coalesce(D.bitAutoSerialNoAssign,false) AS bitAutoSerialNoAssign,
		coalesce(IsEnableProjectTracking,false) AS IsEnableProjectTracking,
		coalesce(IsEnableCampaignTracking,false) AS IsEnableCampaignTracking,
		coalesce(IsEnableResourceScheduling,false) AS IsEnableResourceScheduling,
		coalesce(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		coalesce(D.bitAutolinkUnappliedPayment,false) AS bitAutolinkUnappliedPayment,
		coalesce(numShippingServiceItemID,0) AS numShippingServiceItemID,
		coalesce(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		coalesce(D.tintCommissionType,1) AS tintCommissionType,
		coalesce(D.bitDiscountOnUnitPrice,false) AS bitDiscountOnUnitPrice,
		coalesce(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		coalesce(D.intShippingImageHeight,0) AS intShippingImageHeight,
		coalesce(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		coalesce(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		coalesce(D.bitUseBizdocAmount,true) AS bitUseBizdocAmount,
		coalesce(D.bitDefaultRateType,true) AS bitDefaultRateType,
		coalesce(D.bitIncludeTaxAndShippingInCommission,true) AS bitIncludeTaxAndShippingInCommission,
		coalesce(D.bitLandedCost,false) AS bitLandedCost,
		coalesce(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		coalesce(D.bitMinUnitPriceRule,false) AS bitMinUnitPriceRule,
		coalesce(v_listIds,'') AS vcUnitPriceApprover,
		coalesce(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		coalesce(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		coalesce(D.IsEnableDeferredIncome,false) AS IsEnableDeferredIncome,
		coalesce(D.bitEnableItemLevelUOM,false) AS bitEnableItemLevelUOM,
		coalesce(D.bitPurchaseTaxCredit,false) AS bitPurchaseTaxCredit,
		coalesce(U.bintCreatedDate,cast('1900-01-01 00:00:00.000' as TIMESTAMP)) AS bintCreatedDate,
		(CASE WHEN LENGTH(coalesce(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN coalesce(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE coalesce(TempDefaultPage.vcDefaultNavURL,'') END) AS vcDefaultNavURL,
		coalesce(D.bitApprovalforTImeExpense,false) AS bitApprovalforTImeExpense,
		coalesce(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,coalesce(D.bitApprovalforOpportunity,false) AS bitApprovalforOpportunity,
		coalesce(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		coalesce(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		coalesce(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		coalesce((SELECT  bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID = D.numDomainId AND coalesce(bitMarginPriceViolated,false) = true LIMIT 1),false) AS bitMarginPriceViolated,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 2 AND numPageID = 2 LIMIT 1),0) AS tintLeadRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 3 AND numPageID = 2 LIMIT 1),0) AS tintProspectRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 4 AND numPageID = 2 LIMIT 1),0) AS tintAccountRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 10 AND numPageID = 10 LIMIT 1),0) AS tintSalesOrderListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 10 AND numPageID = 11 LIMIT 1),0) AS tintPurchaseOrderListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 10 AND numPageID = 2 LIMIT 1),0) AS tintSalesOpportunityListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 10 AND numPageID = 8 LIMIT 1),0) AS tintPurchaseOpportunityListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 11 AND numPageID = 2 LIMIT 1),0) AS tintContactListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 7 AND numPageID = 2 LIMIT 1),0) AS tintCaseListRights,
		coalesce((SELECT  intViewAllowed FROM GroupAuthorization WHERE numGroupID = U.numGroupID AND numModuleID = 12 AND numPageID = 2 LIMIT 1),0) AS tintProjectListRights,
		coalesce((SELECT  numCriteria FROM TicklerListFilterConfiguration WHERE POSITION(CONCAT(',',U.numUserDetailId,',') IN CONCAT(',',vcSelectedEmployee,',')) > 0 LIMIT 1),0) AS tintActionItemRights,
		coalesce(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		coalesce(D.bitAllowDuplicateLineItems,false) AS bitAllowDuplicateLineItems,
		coalesce(D.bitLogoAppliedToBizTheme,false) AS bitLogoAppliedToBizTheme,
		coalesce(D.vcLogoForBizTheme,'') AS vcLogoForBizTheme,
		coalesce(D.bitLogoAppliedToLoginBizTheme,false) AS bitLogoAppliedToLoginBizTheme,
		coalesce(D.vcLogoForLoginBizTheme,'') AS vcLogoForLoginBizTheme,
		coalesce(D.vcThemeClass,'1473b4') AS vcThemeClass,
		coalesce(D.vcLoginURL,'') AS vcLoginURL,
		coalesce(D.bitDisplayCustomField,false) AS bitDisplayCustomField,
		coalesce(D.bitFollowupAnytime,false) AS bitFollowupAnytime,
		coalesce(D.bitpartycalendarTitle,false) AS bitpartycalendarTitle,
		coalesce(D.bitpartycalendarLocation,false) AS bitpartycalendarLocation,
		coalesce(D.bitpartycalendarDescription,false) AS bitpartycalendarDescription,
		coalesce(D.bitREQPOApproval,false) AS bitREQPOApproval,
		coalesce(D.bitARInvoiceDue,false) AS bitARInvoiceDue,
		coalesce(D.bitAPBillsDue,false) AS bitAPBillsDue,
		coalesce(D.bitItemsToPickPackShip,false) AS bitItemsToPickPackShip,
		coalesce(D.bitItemsToInvoice,false) AS bitItemsToInvoice,
		coalesce(D.bitSalesOrderToClose,false) AS bitSalesOrderToClose,
		coalesce(D.bitItemsToPutAway,false) AS bitItemsToPutAway,
		coalesce(D.bitItemsToBill,false) AS bitItemsToBill,
		coalesce(D.bitPosToClose,false) AS bitPosToClose,
		coalesce(D.bitPOToClose,false) AS bitPOToClose,
		coalesce(D.bitBOMSToPick,false) AS bitBOMSToPick,
		coalesce(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		coalesce(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		coalesce(D.vchAPBillsDue,'') AS vchAPBillsDue,
		coalesce(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		coalesce(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		coalesce((select string_agg(UPC.numUserID::VARCHAR,',' order by UPC.numUserID)
         from UnitPriceApprover UPC
         where UPC.numDomainID = D.numDomainId
         ),'') AS vchPriceMarginApproval,
		coalesce(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		coalesce(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		coalesce(D.vchItemsToBill,'') AS vchItemsToBill,
		coalesce(D.vchPosToClose,'') AS vchPosToClose,
		coalesce(D.vchPOToClose,'') AS vchPOToClose,
		coalesce(D.vchBOMSToPick,'') AS vchBOMSToPick,
		coalesce(D.decReqPOMinValue,0) AS decReqPOMinValue,
		coalesce(U.tintPayrollType,1) AS tintPayrollType,
		(CASE WHEN EXISTS(SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID = U.numUserDetailId AND numCategory = 1 AND numType = 7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) AS bitUserClockedIn,
		coalesce(D.bitUseOnlyActionItems,false) AS bitUseOnlyActionItems,
		coalesce(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
		coalesce(D.bitDisplayContractElement,false) AS bitDisplayContractElement,
		coalesce(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
		coalesce(D.bitEnableSmartyStreets,false) AS bitEnableSmartyStreets,
		coalesce(D.vcSmartyStreetsAPIKeys,'') AS vcSmartyStreetsAPIKeys,
		coalesce(D.bitAllowToEditHelpFile,false) AS bitAllowToEditHelpFile
      FROM
      UserMaster U
      left join ExchangeUserDetails E
      on E.numUserID = U.numUserId
      left join ImapUserDetails IM
      on IM.numUserCntId = U.numUserDetailId
      Join Domain D
      on D.numDomainId = U.numDomainID
      left join DivisionMaster Div
      on D.numDivisionID = Div.numDivisionID
      left join CompanyInfo C
      on C.numCompanyId = Div.numCompanyID
      left join AdditionalContactsInformation A
      on  A.numContactId = U.numUserDetailId
      Join  Subscribers S
      on S.numTargetDomainID = D.numDomainId
      LEFT JOIN LATERAL(SELECT
         REPLACE(SCB.Link,'../','') AS vcDefaultNavURL
         FROM
         TabMaster T
         JOIN
         GroupTabDetails G ON G.numTabId = T.numTabId
         LEFT JOIN
         ShortCutGrpConf SCUC ON G.numTabId = SCUC.numTabId AND SCUC.numDomainId = U.numDomainID AND SCUC.numGroupId = U.numGroupID AND bitDefaultTab = true
         LEFT JOIN
         ShortCutBar SCB
         ON
         SCB.Id = SCUC.numLinkId
         WHERE
			(T.numDomainID = U.numDomainID OR bitFixed = true)
         AND G.numGroupId = U.numGroupID
         AND coalesce(G.tintType,0) <> 1
         AND tintTabType = 1
         AND T.numTabId NOT IN(2,68)
         ORDER BY
         SCUC.bitInitialPage DESC LIMIT 1)  TempDefaultPage
      LEFT JOIN LATERAL(SELECT
         REPLACE(T.vcURL,'../','') AS vcDefaultNavURL
         FROM
         TabMaster T
         JOIN
         GroupTabDetails G
         ON
         G.numTabId = T.numTabId
         WHERE
			(T.numDomainID = U.numDomainID OR bitFixed = true)
         AND G.numGroupId = U.numGroupID
         AND coalesce(G.tintType,0) <> 1
         AND tintTabType = 1
         AND T.numTabId NOT IN(2,68)
         AND coalesce(G.bitInitialTab,false) = true
         AND coalesce(bitallowed,false) = true
         ORDER BY
         G.bitInitialTab DESC LIMIT 1) TEMPDefaultTab on TRUE on TRUE
      WHERE 1 =(CASE
      WHEN v_vcLinkedinId = 'N' THEN(CASE WHEN LOWER(vcEmailID) = LOWER(v_UserName) and LOWER(vcPassword) = LOWER(v_vcPassword) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN U.vcLinkedinId = v_vcLinkedinId THEN 1 ELSE 0 END)
      END) and bitActive IN(1,2) AND TIMEZONE('UTC',now()) between dtSubStartDate and dtSubEndDate LIMIT 1;
   end if;
             
   open SWV_RefCur2 for
   SELECT
   numTerritoryID
   FROM
   UserTerritory UT
   JOIN
   UserMaster U
   ON
   numUserDetailId = UT.numUserCntID
   WHERE
   1 =(CASE
   WHEN v_vcLinkedinId = 'N' THEN(CASE WHEN LOWER(vcEmailID) = LOWER(v_UserName) and LOWER(vcPassword) = LOWER(v_vcPassword) THEN 1 ELSE 0 END)
   ELSE(CASE WHEN vcLinkedinId = v_vcLinkedinId THEN 1 ELSE 0 END)
   END);

END;
$BODY$;


