-- Stored procedure definition script USP_CreateRecurringForOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateRecurringForOpportunity()
RETURNS VOID LANGUAGE plpgsql                              
                          
--Variable Declaration for OpportunityMaster Table                              
   AS $$
   DECLARE
   v_numOppId  NUMERIC(9,0);  
   v_tintOppType  SMALLINT;
   v_numDomainId  NUMERIC(9,0);                          
   v_numMaxOppId  NUMERIC(9,0);                            
   v_LastRecurringDate  TIMESTAMP;            
   v_numNoTransactions  NUMERIC(9,0);                       
   v_Day  SMALLINT;                          
   v_Month  SMALLINT;                          
   v_Year  INTEGER;                          
   v_MonthNextDate  SMALLINT;                         
                  
    
--Variable Declaration for Recurring Table                               
   v_numRecurringId  NUMERIC(9,0);                              
   v_varRecurringTemplateName  VARCHAR(50);                              
   v_chrIntervalType  CHAR(1);                              
   v_tintIntervalDays  SMALLINT;                              
   v_tintMonthlyType  SMALLINT;                              
   v_tintFirstDet  SMALLINT;                              
   v_tintWeekDays  SMALLINT;                              
   v_tintMonths  SMALLINT;                              
   v_dtStartDate  TIMESTAMP;                              
   v_dtEndDate  TIMESTAMP;          
   v_numNoTransactionsRecurring  NUMERIC(9,0);                              
   v_bitEndType  BOOLEAN;             
   v_bitEndTransactionType  BOOLEAN;                           
   v_DNextDate  TIMESTAMP;                              
   v_DLastDate  TIMESTAMP;                              
   v_DateDiff  INTEGER;                              
   v_CurrentDate  TIMESTAMP;
   v_EndDateDiff  INTEGER; -- 'End for Daily 
			    --For Monthly                        
   v_tempDiff  INTEGER;
   SWV_RowCount INTEGER;             
        
                 
----------------------------------------For OpportunityMaster------------------------------------------------------                             
																																																						
BEGIN
   select   Opp.numOppId, tintopptype, numDomainId, numRecurringId, coalesce(numNoTransactions,0), coalesce(dtLastRecurringDate,'Jan  1 1753 12:00AM') INTO v_numOppId,v_tintOppType,v_numDomainId,v_numRecurringId,v_numNoTransactions,
   v_LastRecurringDate From OpportunityMaster Opp LEFT OUTER JOIN OpportunityRecurring OPR ON OPR.numOppId = Opp.numOppId Where coalesce(numRecurringId,0) <> 0 and tintoppstatus = 1 and tintRecurringType = 1    LIMIT 1;   -- added by Pratik                           
                              
   select   max(Opp.numOppId) INTO v_numMaxOppId From OpportunityMaster Opp LEFT OUTER JOIN OpportunityRecurring OPR ON OPR.numOppId = Opp.numOppId Where coalesce(OPR.numRecurringId,0) <> 0 and tintoppstatus = 1;                              
   RAISE NOTICE '%',v_numMaxOppId;                          
   RAISE NOTICE '%',v_numOppId;                          
   While v_numOppId > 0 LOOP
      RAISE NOTICE '@numOppId = %',SUBSTR(CAST(v_numOppId AS VARCHAR(10)),1,10);
      RAISE NOTICE '@numRecurringId=%',SUBSTR(CAST(v_numRecurringId AS VARCHAR(10)),1,10);
      If v_numRecurringId <> 0 then
      
         select   varRecurringTemplateName, chrIntervalType, tintIntervalDays, tintMonthlyType, tintFirstDet, tintWeekDays, tintMonths, dtStartDate, dtEndDate, bitEndType, bitEndTransactionType, coalesce(numNoTransaction,0) INTO v_varRecurringTemplateName,v_chrIntervalType,v_tintIntervalDays,v_tintMonthlyType,
         v_tintFirstDet,v_tintWeekDays,v_tintMonths,v_dtStartDate,v_dtEndDate,
         v_bitEndType,v_bitEndTransactionType,v_numNoTransactionsRecurring From RecurringTemplate Where numRecurringId = v_numRecurringId;
         RAISE NOTICE ' @chrIntervalType=%',SUBSTR(CAST(v_chrIntervalType AS VARCHAR(10)),1,10);
         RAISE NOTICE '@dtStartDate=%',SUBSTR(CAST(v_dtStartDate AS VARCHAR(30)),1,30);
         If DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_dtStartDate) >= 0 then
              ---For Daily   
				                  
            If v_chrIntervalType = 'D' then
                    -- 'Begin for Daily     
						                        
               If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                          
                  v_DNextDate := v_dtStartDate;
                  RAISE NOTICE '@DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(100)),1,100);
                  If v_DNextDate < TIMEZONE('UTC',now()) then
                     v_DNextDate := TIMEZONE('UTC',now());
                  end if;
               Else
                  RAISE NOTICE '@LastRecurringDate=%',SUBSTR(CAST(v_LastRecurringDate AS VARCHAR(30)),1,30);
                  v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'day' as interval);
                  v_tempDiff := DATE_PART('day',v_DNextDate -TIMEZONE('UTC',now()):: timestamp);
                  RAISE NOTICE 'tempDiff';
                  RAISE NOTICE '%',v_tempDiff;
                  while (v_tempDiff < 0) LOOP
                     RAISE NOTICE '------------------';
                     RAISE NOTICE '@DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(100)),1,100);
                     v_DNextDate := CAST(v_DNextDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'day' as interval);
                     v_tempDiff := DATE_PART('day',v_DNextDate -TIMEZONE('UTC',now()):: timestamp);
                  END LOOP;
               end if;
               RAISE NOTICE '@DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(100)),1,100);
               RAISE NOTICE 'getutcdate()==%',CAST(TIMEZONE('UTC',now()) AS VARCHAR(100));
               v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
               v_EndDateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_dtEndDate);
               RAISE NOTICE 'enddate diff ==%',SUBSTR(CAST(v_EndDateDiff AS VARCHAR(5)),1,5);
               RAISE NOTICE '@bitEndType==%',SUBSTR(CAST(v_bitEndType AS VARCHAR(2)),1,2);
               RAISE NOTICE '@DateDiff==%',SUBSTR(CAST(v_DateDiff AS VARCHAR(5)),1,5);
               RAISE NOTICE '@bitEndTransactionType==%',SUBSTR(CAST(v_bitEndTransactionType AS VARCHAR(5)),1,5);
               RAISE NOTICE '@numNoTransactionsRecurring==%',SUBSTR(CAST(v_numNoTransactionsRecurring AS VARCHAR(5)),1,5);
               RAISE NOTICE '@numNoTransactions==%',SUBSTR(CAST(v_numNoTransactions AS VARCHAR(5)),1,5);
               If v_DateDiff = 0 And (v_bitEndType = true Or((v_bitEndType = false and v_EndDateDiff <= 0) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                          
                  RAISE NOTICE 'Executed';
                  PERFORM USP_RecurringForOpportunity(v_numOppId := v_numOppId,v_numDomainId := v_numDomainId,v_numNoTransactionsRecurring := v_numNoTransactionsRecurring);
               end if;
            end if; -- 'End for Daily 
			    --For Monthly                        
            If v_chrIntervalType = 'M' then
                     
               RAISE NOTICE '%',v_chrIntervalType;
               RAISE NOTICE 'Monthly Type=%',SUBSTR(CAST(v_tintMonthlyType AS VARCHAR(3)),1,3);
               RAISE NOTICE 'Last RecurringDate=%',SUBSTR(CAST(v_LastRecurringDate AS VARCHAR(20)),1,20);
               If v_tintMonthlyType = 0 then
                           -- tinymonthly Begin                        
                  If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                                
                     v_DNextDate := v_dtStartDate;
                     If v_DNextDate < TIMEZONE('UTC',now()) then
                        v_DNextDate := TIMEZONE('UTC',now());
                     end if;
                     v_Day := EXTRACT(DAY FROM v_DNextDate);
                     v_Month := EXTRACT(MONTH FROM v_DNextDate);
                     v_Year := EXTRACT(YEAR FROM v_DNextDate);
                     RAISE NOTICE '%',v_Day;
                     RAISE NOTICE '%',v_Month;
                     RAISE NOTICE '%',v_Year;
                     v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_Month -1 || 'month' as interval);
                     RAISE NOTICE '%',v_DNextDate;
                  Else
                     v_tempDiff := DATE_PART('day',v_DNextDate -TIMEZONE('UTC',now()):: timestamp);
                     v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'month' as interval);
                     while (v_tempDiff < 0) LOOP
                        v_DNextDate := CAST(v_DNextDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'month' as interval);
                        v_tempDiff := DATE_PART('day',v_DNextDate -TIMEZONE('UTC',now()):: timestamp);
                     END LOOP;
                  end if;
                  v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
                  RAISE NOTICE 'DateDiff=%',SUBSTR(CAST(v_DateDiff AS VARCHAR(10)),1,10);
                  If v_DateDiff = 0 And (v_bitEndType = true Or ((v_bitEndType = false and TIMEZONE('UTC',now()) <= v_dtEndDate) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                                  
                     PERFORM USP_RecurringForOpportunity(v_numOppId := v_numOppId,v_numDomainId := v_numDomainId,v_numNoTransactionsRecurring := v_numNoTransactionsRecurring);
                  end if;
               end if;   -- tinymonthly End--0                         
                        
               If v_tintMonthlyType = 1 then
                           
                  If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                                 
                     v_DNextDate := v_dtStartDate;
                     If v_DNextDate < TIMEZONE('UTC',now()) then
                        v_DNextDate := TIMEZONE('UTC',now());
                     end if;
                     v_Month := EXTRACT(MONTH FROM v_DNextDate);
                     v_Year := EXTRACT(YEAR FROM v_DNextDate);
                     RAISE NOTICE '@Month = %',SUBSTR(CAST(v_Month AS VARCHAR(10)),1,10);
                     RAISE NOTICE '%',v_Year;
                     RAISE NOTICE '@tintFirstDet=%',SUBSTR(CAST(v_tintFirstDet AS VARCHAR(10)),1,10);
                     RAISE NOTICE '@tintWeekDays=%',SUBSTR(CAST(v_tintWeekDays AS VARCHAR(10)),1,10);
                     RAISE NOTICE '@DNextDate=%',SUBSTR(CAST(v_DNextDate AS VARCHAR(30)),1,30);
                     v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_Month -1 || 'month' as interval);
                     RAISE NOTICE 'DNextDate==%',SUBSTR(CAST(v_DNextDate AS VARCHAR(30)),1,30);
                     v_DNextDate := fn_RecurringTemplate(v_tintFirstDet,v_tintWeekDays,v_DNextDate);
                     RAISE NOTICE '%',v_DNextDate;
                     v_MonthNextDate := EXTRACT(MONTH FROM v_DNextDate);
                     RAISE NOTICE '@MonthNextDate=%',SUBSTR(CAST(v_MonthNextDate AS VARCHAR(20)),1,20);
                  Else
                     v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(v_tintIntervalDays || 'month' as interval);
                     v_Month := EXTRACT(MONTH FROM TIMEZONE('UTC',now()));
                     RAISE NOTICE '@Month = %',SUBSTR(CAST(v_Month AS VARCHAR(10)),1,10);
                     v_MonthNextDate := EXTRACT(MONTH FROM v_DNextDate);
                     RAISE NOTICE '@MonthNextDate=%',SUBSTR(CAST(v_MonthNextDate AS VARCHAR(20)),1,20);
                     v_DNextDate := fn_RecurringTemplate(v_tintFirstDet,v_tintWeekDays,v_DNextDate);
                     RAISE NOTICE '%',v_DNextDate;
                  end if;
                  v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);
                  RAISE NOTICE 'DateDifference=%',SUBSTR(CAST(v_DateDiff AS VARCHAR(30)),1,30);
                  If v_DateDiff = 0 And v_Month = v_MonthNextDate And ((v_bitEndType = true Or (v_bitEndType = false And TIMEZONE('UTC',now()) <= v_dtEndDate) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
 								
                     PERFORM USP_RecurringForOpportunity(v_numOppId := v_numOppId,v_numDomainId := v_numDomainId,v_numNoTransactionsRecurring := v_numNoTransactionsRecurring);
                  end if;
               end if;
            end if;  --End for Monthly                        
                               
            --- For Yearly                        
                                     
            If v_chrIntervalType = 'Y' then
                      
               RAISE NOTICE '%',v_chrIntervalType;
               RAISE NOTICE '%',v_LastRecurringDate;
               If v_LastRecurringDate = 'Jan  1 1753 12:00AM' then
                             
                  If v_dtStartDate < TIMEZONE('UTC',now()) then
                     v_dtStartDate := TIMEZONE('UTC',now());
                  end if;
                  v_Year := EXTRACT(YEAR FROM v_dtStartDate);
                  RAISE NOTICE '%',v_Year;
                  v_DNextDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(((v_Year::bigint -1900)*12)+v_tintMonths -1 || 'month' as interval);
                  RAISE NOTICE '%',v_DNextDate;
               Else
                  v_DNextDate := CAST(v_LastRecurringDate AS TIMESTAMP)+CAST(CAST(1 AS NUMERIC) || 'year' as interval);
                  RAISE NOTICE '%',v_DNextDate;
               end if;
               v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DNextDate);                            
                        -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                        
               RAISE NOTICE '%',v_DateDiff;
               If v_DateDiff = 0 and (v_bitEndType = true Or ((v_bitEndType = false And TIMEZONE('UTC',now()) <= v_dtEndDate) Or (v_bitEndTransactionType = true And v_numNoTransactions < v_numNoTransactionsRecurring))) then
                           
                  PERFORM USP_RecurringForOpportunity(v_numOppId := v_numOppId,v_numDomainId := v_numDomainId,v_numNoTransactionsRecurring := v_numNoTransactionsRecurring);
               end if;
            end if;
         end if;
      end if;   --End For @numRecurringId            
      RAISE NOTICE '----------------------------------------------------------------------------';
      select   Opp.numOppId, tintopptype, numRecurringId, coalesce(numNoTransactions,0), coalesce(dtLastRecurringDate,'Jan  1 1753 12:00AM') INTO v_numOppId,v_tintOppType,v_numRecurringId,v_numNoTransactions,v_LastRecurringDate From OpportunityMaster Opp LEFT OUTER JOIN OpportunityRecurring OPR ON OPR.numOppId = Opp.numOppId Where Opp.numOppId > v_numOppId And Opp.numOppId <= v_numMaxOppId  And coalesce(OPR.numRecurringId,0) <> 0 and tintoppstatus = 1    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      If SWV_RowCount = 0 then 
         v_numOppId := 0;
      end if;
   END LOOP;
   RETURN; ---End for While                          
                        
------------------------------------------------End For OpportunityMaster-------------------------------------------------------------------                    
                   
END; $$;



