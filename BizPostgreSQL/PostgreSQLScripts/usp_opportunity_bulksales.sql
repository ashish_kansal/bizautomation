DROP FUNCTION IF EXISTS USP_Opportunity_BulkSales;

CREATE OR REPLACE FUNCTION USP_Opportunity_BulkSales(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_tempOppID  INTEGER;
   v_numNewOppID  NUMERIC(18,0);
   v_numDomainID  NUMERIC(18,0);
   v_numUserCntID  NUMERIC(18,0);
   v_tintSource  NUMERIC(9,0);
   v_txtComments  VARCHAR(1000);
   v_bitPublicFlag  BOOLEAN;
   v_monPAmount  DECIMAL(20,5);
   v_numAssignedTo  NUMERIC(18,0);                                                                                                                                                                              
   v_strItems  TEXT;
   v_dtEstimatedCloseDate  TIMESTAMP;                                                            
   v_lngPConclAnalysis  NUMERIC(18,0);
   v_numCurrencyID  NUMERIC(18,0);
   v_numOrderStatus  NUMERIC(18,0);

   v_bitBillingTerms  BOOLEAN;
   v_intBillingDays  INTEGER;
   v_bitInterestType  BOOLEAN;
   v_fltInterest  DOUBLE PRECISION;
   v_intShippingCompany  NUMERIC(18,0);
   v_numDefaultShippingServiceID  NUMERIC(18,0);
   v_vcCouponCode  VARCHAR(100);
   v_numShipFromWarehouse  NUMERIC(18,0);
   v_numWillCallWarehouseID  NUMERIC(18,0);
   v_dtReleaseDate  DATE;
   v_k  INTEGER DEFAULT 1;
   v_numTempOppItemID  NUMERIC(18,0);
   v_kCount  INTEGER;
   v_j  INTEGER DEFAULT 1;
   v_jCount  INTEGER;
   v_numTempMSOQID  NUMERIC(18,0);
   v_numTempDivisionID  NUMERIC(18,0);
   v_numTempContactID  NUMERIC(18,0);
   v_USP_OppManage_vcPOppName VARCHAR(100);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

	-- FIRST GET UNIQUE OPPID THAT NEEDS TO BE RE CREATED WHICH ARE NOT EXECUTED
BEGIN
   DROP TABLE IF EXISTS tt_TEMPBulkSales CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPBulkSales
   (
      ID INTEGER,
      numMSOQID NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      numContactID NUMERIC(18,0)
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPMasterOpp_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPMASTEROPP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPMASTEROPP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0)
   );
   DROP TABLE IF EXISTS tt_TEMPOPPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPITEMS
   (
      ID INTEGER,
      numoppitemtCode NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monPrice DECIMAL(30,16),
      monTotAmount DECIMAL(20,5),
      vcItemDesc VARCHAR(1000),
      numWarehouseItmsID NUMERIC(18,0),
      ItemType VARCHAR(30),
      DropShip BOOLEAN,
      bitDiscountType BOOLEAN,
      fltDiscount DECIMAL(30,16),
      monTotAmtBefDiscount DECIMAL(20,5),
      vcItemName VARCHAR(300),
      numUOM NUMERIC(18,0),
      bitWorkOrder BOOLEAN,
      numVendorWareHouse NUMERIC(18,0),
      numShipmentMethod NUMERIC(18,0),
      numSOVendorId NUMERIC(18,0),
      numProjectID NUMERIC(18,0),
      numProjectStageID NUMERIC(18,0),
      numToWarehouseItemID NUMERIC(18,0),
      Attributes VARCHAR(500),
      AttributeIDs VARCHAR(500),
      vcSKU VARCHAR(100),
      bitItemPriceApprovalRequired BOOLEAN,
      numPromotionID NUMERIC(18,0),
      bitPromotionTriggered BOOLEAN,
      vcPromotionDetail VARCHAR(2000),
      numSortOrder NUMERIC(18,0)
   );
   INSERT INTO
   tt_TEMPMASTEROPP(numOppID)
   SELECT
   DISTINCT numOppId
   FROM
   MassSalesOrderQueue
   WHERE
   coalesce(bitExecuted,false) = false;
	
   select   COUNT(*) INTO v_iCount FROM tt_TEMPMASTEROPP;

   WHILE v_i <= v_iCount LOOP
      select   numOppID INTO v_tempOppID FROM tt_TEMPMASTEROPP WHERE ID = v_i;
      IF EXISTS(SELECT * FROM OpportunityMaster WHERE numOppId = v_tempOppID) then
         select   numDomainId, numCreatedBy, tintSource, txtComments, bitPublicFlag, TIMEZONE('UTC',now()), lngPConclAnalysis, numCurrencyID, vcCouponCode, numassignedto, numStatus, coalesce(numShipFromWarehouse,0), dtReleaseDate INTO v_numDomainID,v_numUserCntID,v_tintSource,v_txtComments,v_bitPublicFlag,
         v_dtEstimatedCloseDate,v_lngPConclAnalysis,v_numCurrencyID,v_vcCouponCode,
         v_numAssignedTo,v_numOrderStatus,v_numShipFromWarehouse,v_dtReleaseDate FROM
         OpportunityMaster WHERE
         numOppId = v_tempOppID;
         IF coalesce(v_numShipFromWarehouse,0) > 0 then
			
            v_numWillCallWarehouseID := v_numShipFromWarehouse;
         ELSE
            v_numWillCallWarehouseID := coalesce((SELECT  numWarehouseItmsID FROM OpportunityItems WHERE numOppId = v_tempOppID AND coalesce(numWarehouseItmsID,0) > 0 LIMIT 1),0);
         end if;


			-- DELETE ENTRIES OF PREVIOUS LOOP
         DELETE FROM tt_TEMPOPPITEMS;
         INSERT INTO
         tt_TEMPOPPITEMS
         SELECT
         ROW_NUMBER() OVER(ORDER BY numoppitemtCode)
				,numoppitemtCode
				,numItemCode
				,numUnitHour
				,monPrice
				,monTotAmount
				,vcItemDesc
				,numWarehouseItmsID
				,vcType
				,bitDropShip
				,bitDiscountType
				,fltDiscount
				,monTotAmtBefDiscount
				,vcItemName
				,numUOMId
				,bitWorkOrder
				,numVendorWareHouse
				,numShipmentMethod
				,numSOVendorId
				,0 AS numProjectID
				,0 AS numProjectStageID
				,numToWarehouseItemID
				,vcAttributes AS Attributes
				,vcAttrValues AS AttributeIDs
				,vcSKU
				,bitItemPriceApprovalRequired
				,numPromotionID
				,bitPromotionTriggered
				,vcPromotionDetail
				,numSortOrder
         FROM
         OpportunityItems
         WHERE
         numOppId = v_tempOppID;
         select   COUNT(*) INTO v_kCount FROM tt_TEMPOPPITEMS;
	
			--TODO CREATE ITEMS
         v_strItems := '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>';
         WHILE v_k <= v_kCount LOOP
            select   numoppitemtCode, CONCAT(v_strItems,'<Item><Op_Flag>1</Op_Flag>','<numoppitemtCode>',ID,'</numoppitemtCode>',
            '<numItemCode>',numItemCode,'</numItemCode>','<numUnitHour>',
            numUnitHour,'</numUnitHour>','<monPrice>',monPrice,'</monPrice>',
            '<monTotAmount>',monTotAmount,'</monTotAmount>','<vcItemDesc>',vcItemDesc,
            '</vcItemDesc>','<numWarehouseItmsID>',coalesce(numWarehouseItmsID,0),
            '</numWarehouseItmsID>','<ItemType>',ItemType,'</ItemType>','<DropShip>',
            coalesce(DropShip,false),'</DropShip>','<bitDiscountType>',coalesce(bitDiscountType,false),
            '</bitDiscountType>','<fltDiscount>',coalesce(fltDiscount,0),
            '</fltDiscount>','<monTotAmtBefDiscount>',monTotAmtBefDiscount,
            '</monTotAmtBefDiscount>','<vcItemName>',vcItemName,'</vcItemName>',
            '<numUOM>',coalesce(numUOM,0),'</numUOM>','<bitWorkOrder>',
            coalesce(bitWorkOrder,false),'</bitWorkOrder>','<numVendorWareHouse>',
            coalesce(numVendorWareHouse,0),'</numVendorWareHouse>','<numShipmentMethod>',
            coalesce(numShipmentMethod,0),'</numShipmentMethod>','<numSOVendorId>',
            coalesce(numSOVendorId,0),'</numSOVendorId>','<numProjectID>',
            coalesce(numProjectID,0),'</numProjectID>','<numProjectStageID>',
            coalesce(numProjectStageID,0),'</numProjectStageID>','<numToWarehouseItemID>',
            coalesce(numToWarehouseItemID,0),'</numToWarehouseItemID>',
            '<Attributes>',Attributes,'</Attributes>','<AttributeIDs>',AttributeIDs,
            '</AttributeIDs>','<vcSKU>',vcSKU,'</vcSKU>','<bitItemPriceApprovalRequired>',
            coalesce(bitItemPriceApprovalRequired,false),'</bitItemPriceApprovalRequired>',
            '<numPromotionID>',coalesce(numPromotionID,0),
            '</numPromotionID>','<bitPromotionTriggered>',coalesce(bitPromotionTriggered,false),
            '</bitPromotionTriggered>','<vcPromotionDetail>',vcPromotionDetail,
            '</vcPromotionDetail>','<numSortOrder>',coalesce(numSortOrder,0),
            '</numSortOrder>') INTO v_numTempOppItemID,v_strItems FROM
            tt_TEMPOPPITEMS WHERE
            ID = v_k;
            IF(SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numOppID = v_tempOppID AND numOppItemID = v_numTempOppItemID) > 0 then
				
               v_strItems := CONCAT(v_strItems,'<bitHasKitAsChild>true</bitHasKitAsChild>');
               v_strItems := CONCAT(v_strItems,'<KitChildItems>',OVERLAY((SELECT
               ',' || CONCAT(OKI.numChildItemID,'#',OKI.numWareHouseItemId,'-',OKCI.numItemID,'#',OKCI.numWareHouseItemId)
               FROM
               OpportunityKitChildItems OKCI
               INNER JOIN
               OpportunityKitItems OKI
               ON
               OKCI.numOppChildItemID = OKI.numOppChildItemID
               WHERE
               OKCI.numOppId = v_tempOppID
               AND OKCI.numOppItemID = v_numTempOppItemID) placing '' from 1 for 1),
               '</KitChildItems>');
            ELSE
               v_strItems := CONCAT(v_strItems,'<bitHasKitAsChild>false</bitHasKitAsChild>');
               v_strItems := CONCAT(v_strItems,'<KitChildItems />');
            end if;
            v_strItems := CONCAT(v_strItems,'</Item>');
            v_k := v_k::bigint+1;
         END LOOP;
         v_strItems := CONCAT(v_strItems,'</NewDataSet>');
		
			-- DELETE ENTRIES OF PREVIOUS LOOP
         DELETE FROM tt_TEMPBulkSales;

			-- GET ALL ORGANIZATION FOR WHICH ORDER NEEDS TO BE CREATED
         INSERT INTO tt_TEMPBulkSales(ID,
				numMSOQID,
				numDivisionID,
				numContactID)
         SELECT
         ROW_NUMBER() OVER(ORDER BY numMSOQID),
				numMSOQID,
				numDivisionId,
				numContactId
         FROM
         MassSalesOrderQueue
         WHERE
         numOppId = v_tempOppID
         AND coalesce(bitExecuted,false) = false;
         select   COUNT(*) INTO v_jCount FROM tt_TEMPBulkSales;
         WHILE v_j <= v_jCount LOOP
            select 
				numMSOQID
				,numDivisionID
				,numContactID 
			INTO
				v_numTempMSOQID
				,v_numTempDivisionID
				,v_numTempContactID 
			FROM
				tt_TEMPBulkSales 
			WHERE
				ID = v_j;

            select(CASE WHEN coalesce(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END), coalesce(numBillingDays,0), (CASE WHEN coalesce(tintInterestType,0) = 1 THEN 1 ELSE 0 END), coalesce(fltInterest,0), coalesce(numAssignedTo,0), coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0) INTO v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_numAssignedTo,
            v_intShippingCompany,v_numDefaultShippingServiceID FROM
            DivisionMaster WHERE
            numDomainID = v_numDomainID AND numDivisionID = v_numTempDivisionID;
            BEGIN
               v_numNewOppID := 0;
               v_USP_OppManage_vcPOppName := '';

               SELECT * INTO v_numNewOppID FROM 
			   USP_OppManage(v_numOppID := v_numNewOppID,                                                                          
								  v_numContactId := v_numTempContactID::NUMERIC,                                                                          
								  v_numDivisionId := v_numTempDivisionID::NUMERIC,                                                                          
								  v_tintSource := v_tintSource::NUMERIC,                                                                          
								  v_vcPOppName := v_USP_OppManage_vcPOppName::VARCHAR,                                                                          
								  v_Comments := v_txtComments::VARCHAR,                                                                          
								  v_bitPublicFlag := v_bitPublicFlag,                                                                          
								  v_numUserCntID := v_numUserCntID,                                                                                 
								  v_monPAmount := 0::DECIMAL,                                                 
								  v_numAssignedTo := v_numAssignedTo::NUMERIC,                                                                                                        
								  v_numDomainId := v_numDomainID::NUMERIC,                                                                                                                                                                                   
								  v_strItems := v_strItems::TEXT,                                                                          
								  v_strMilestone := ''::TEXT,                                                                          
								  v_dtEstimatedCloseDate := v_dtEstimatedCloseDate::TIMESTAMP,                                                                          
								  v_CampaignID := 0::NUMERIC,                                                                          
								  v_lngPConclAnalysis := v_lngPConclAnalysis::NUMERIC,                                                                         
								  v_tintOppType := 1::SMALLINT,                                                                                                                                             
								  v_tintActive := 0::SMALLINT,                                                              
								  v_numSalesOrPurType := 0::NUMERIC,              
								  v_numRecurringId := 0::NUMERIC,
								  v_numCurrencyID := v_numCurrencyID::NUMERIC,
								  v_DealStatus := 1::SMALLINT,-- 0=Open,1=Won,2=Lost
								  v_numStatus := v_numOrderStatus::NUMERIC,
								  v_vcOppRefOrderNo := NULL::VARCHAR,
								  v_numBillAddressId := 0::NUMERIC,
								  v_numShipAddressId := 0::NUMERIC,
								  v_bitStockTransfer := false,
								  v_WebApiId := 0::INTEGER,
								  v_tintSourceType := 1::SMALLINT,
								  v_bitDiscountType := false,
								  v_fltDiscount := 0::NUMERIC,
								  v_bitBillingTerms := v_bitBillingTerms::BOOLEAN,
								  v_intBillingDays := v_intBillingDays::INTEGER,
								  v_bitInterestType := v_bitInterestType::BOOLEAN,
								  v_fltInterest := v_fltInterest::NUMERIC,
								  v_tintTaxOperator := 0::SMALLINT,
								  v_numDiscountAcntType := 0::NUMERIC(9,0),
								  v_vcWebApiOrderNo := NULL::VARCHAR,
								  v_vcCouponCode := v_vcCouponCode::VARCHAR,
								  v_vcMarketplaceOrderID := NULL::VARCHAR,
								  v_vcMarketplaceOrderDate := NULL::TIMESTAMP,
								  v_vcMarketplaceOrderReportId := NULL::VARCHAR,
								  v_numPercentageComplete := NULL::NUMERIC,
								  v_bitUseShippersAccountNo := false,
								  v_bitUseMarkupShippingRate := false,
								  v_numMarkupShippingRate := 0::DECIMAL,
								  v_intUsedShippingCompany := v_intShippingCompany::INTEGER,
								  v_numShipmentMethod := 0::NUMERIC,
								  v_dtReleaseDate :=v_dtReleaseDate::DATE,
								  v_numPartner := 0::NUMERIC,
								  v_tintClickBtn := 0::INTEGER,
								  v_numPartenerContactId := 0::NUMERIC,
								  v_numAccountClass := 0::NUMERIC,
								  v_numWillCallWarehouseID := v_numWillCallWarehouseID::NUMERIC,
								  v_numVendorAddressID := 0::NUMERIC,
								  v_numShipFromWarehouse := v_numShipFromWarehouse::NUMERIC,
								  v_numShippingService := v_numDefaultShippingServiceID::NUMERIC,
								  v_ClientTimeZoneOffset := DATEDIFF('minute',now()::TIMESTAMP,TIMEZONE('UTC',now()))
								  --v_vcCustomerPO VARCHAR(100) DEFAULT NULL,
								  --v_bitDropShipAddress BOOLEAN DEFAULT false,
								  --v_PromCouponCode VARCHAR(100) DEFAULT NULL,
								  --v_numPromotionId NUMERIC(18,0) DEFAULT 0,
								  --v_dtExpectedDate TIMESTAMP DEFAULT null,
								  --v_numProjectID NUMERIC(18,0) DEFAULT 0
								  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
								);
			   
               PERFORM USP_OpportunityMaster_CT(v_numDomainID,v_numUserCntID,v_numNewOppID);

					-- UPDATE STATUS TO EXECUTED
               UPDATE
               MassSalesOrderQueue
               SET
               bitExecuted = true
               WHERE
               numMSOQID = v_numTempMSOQID;


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
               INSERT INTO MassSalesOrderLog(numMSOQID
						,bitSuccess
						,numCreatedOrderID)
					VALUES(v_numTempMSOQID
						,true
						,v_numNewOppID);
					
               INSERT INTO CFW_Fld_Values_Opp(Fld_ID
						,Fld_Value
						,RecId
						,bintModifiedDate
						,numModifiedBy)
               SELECT
               Fld_ID
						,Fld_Value
						,v_numNewOppID
						,TIMEZONE('UTC',now())
						,numModifiedBy
               FROM
               CFW_Fld_Values_Opp
               WHERE
               RecId = v_tempOppID;
               EXCEPTION WHEN OTHERS THEN
					GET STACKED DIAGNOSTICS
					  my_ex_state   = RETURNED_SQLSTATE,
					  my_ex_message = MESSAGE_TEXT,
					  my_ex_detail  = PG_EXCEPTION_DETAIL,
					  my_ex_hint    = PG_EXCEPTION_HINT,
					  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

                  --open SWV_RefCur for SELECT my_ex_message,my_ex_state;
						
					-- UPDATE STATUS TO EXECUTED
                  UPDATE
                  MassSalesOrderQueue
                  SET
                  bitExecuted = true
                  WHERE
                  numMSOQID = v_numTempMSOQID;


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
                  INSERT INTO MassSalesOrderLog(numMSOQID
						,bitSuccess
						,vcError)
					VALUES(v_numTempMSOQID
						,false
						,my_ex_message);
            END;
            v_j := v_j::bigint+1;
         END LOOP;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;














