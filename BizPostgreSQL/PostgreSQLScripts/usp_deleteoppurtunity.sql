-- Stored procedure definition script USP_DeleteOppurtunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteOppurtunity(v_numOppId NUMERIC(9,0) ,        
 v_numDomainID NUMERIC(9,0),
 v_numUserCntID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;
   v_tintError  SMALLINT;

   v_monCreditAmount  DECIMAL(20,5);
   v_monCreditBalance  DECIMAL(20,5);
   v_numDivisionID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;

   v_tintOppStatus  SMALLINT;    
   v_tintShipped  SMALLINT;                
   v_isOrderItemsAvailable  INTEGER DEFAULT 0;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_strMsg  VARCHAR(200);
BEGIN
   if exists(select * from OpportunityMaster where numOppId = v_numOppId and numDomainId = v_numDomainID) then
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppId;

      BEGIN
         -- BEGIN TRAN
IF(SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppId = v_numOppId AND coalesce(bitExecuted,false) = false) > 0 then
	
            RAISE EXCEPTION 'MASS SALES ORDER';
            RETURN;
         end if;
         IF(SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = v_numOppId AND numDomainID = v_numDomainID AND coalesce(bitMarkForDelete,false) = false) > 0 then
	
            RAISE EXCEPTION 'RECURRING ORDER OR BIZDOC';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = v_numOppId AND coalesce(RecurrenceConfiguration.bitDisabled,false) = false) > 0
         then
	
            RAISE EXCEPTION 'RECURRED ORDER';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = 296 AND coalesce(bitFulFilled,false) = true) > 0
         then
	
            RAISE EXCEPTION 'FULFILLED_ITEMS';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM WorkOrder WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId AND coalesce(numWOStatus,0) <> 23184) > 0
         then
	
            RAISE EXCEPTION 'WORK_ORDER_EXISTS';
            RETURN;
         end if;
         IF EXISTS(SELECT * FROM    CaseOpportunities
         WHERE   numoppid = v_numOppId
         AND numDomainid = v_numDomainID) then
        
            RAISE EXCEPTION 'CASE DEPENDANT';
            RETURN;
         end if;
         IF EXISTS(SELECT * FROM    ReturnHeader
         WHERE   numOppId = v_numOppId
         AND numDomainID = v_numDomainID) then
	
            RAISE EXCEPTION 'RETURN_EXIST';
            RETURN;
         end if;
         v_tintError := 0;
         SELECT * INTO v_tintError FROM USP_CheckCanbeDeleteOppertunity(v_numOppId,v_tintError::SMALLINT);
   --  numeric(18, 0)
	 --  tinyint
         IF v_tintError = 1 then
  
            RAISE EXCEPTION 'OppItems DEPENDANT';
            RETURN;
         end if;
         PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID := v_numDomainID,v_tintRecordType := 2::SMALLINT,v_numRecordID := v_numOppId); --  numeric(18, 0)
	
  --Credit Balance
         v_monCreditAmount := 0;
         v_monCreditBalance := 0;
         v_numDivisionID := 0;
         select   numDivisionId, tintopptype INTO v_numDivisionID,v_tintOppType from OpportunityMaster WHERE  numOppId = v_numOppId  and numDomainId = v_numDomainID;
         IF v_tintOppType = 1 then --Sales
            select   coalesce(monSCreditBalance,0) INTO v_monCreditBalance from DivisionMaster WHERE  numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID;
         ELSEIF v_tintOppType = 2
         then --Purchase
            UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId = v_numDomainID AND numOppId IN(SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = v_numOppId);
         end if;
         UPDATE OpportunityItemsReleaseDates SET tintStatus = 1,numPurchasedOrderID = 0 WHERE numPurchasedOrderID = v_numOppId;
         select   coalesce(monPCreditBalance,0) INTO v_monCreditBalance from DivisionMaster WHERE  numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID;
         select   sum(monAmount) INTO v_monCreditAmount from CreditBalanceHistory where numReturnID in(select numReturnID from Returns WHERE numOppID = v_numOppId);
         IF (v_monCreditAmount > v_monCreditBalance) then
        
            RAISE EXCEPTION 'CreditBalance DEPENDANT';
            RETURN;
         end if;
         select   tintoppstatus, tintshipped INTO v_tintOppStatus,v_tintShipped from OpportunityMaster where numOppId = v_numOppId;
         select   COUNT(*) INTO v_isOrderItemsAvailable FROM
         OpportunityItems OI
         JOIN
         OpportunityMaster OM ON OI.numOppId = OM.numOppId
         JOIN
         Item I ON OI.numItemCode = I.numItemCode WHERE
		(charitemtype = 'P' OR 1 =(CASE WHEN tintopptype = 1 THEN
            CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
            WHEN coalesce(I.bitKitParent,false) = true THEN 1
            ELSE 0 END
         ELSE 0 END)) AND OI.numOppId = v_numOppId
         AND (bitDropShip = false
         OR bitDropShip IS NULL);
         if ((v_tintOppStatus = 1 and v_tintShipped = 0) OR  v_tintShipped = 1) AND v_tintCommitAllocation <> 2 then
  
            PERFORM USP_RevertDetailsOpp(v_numOppId,0,1::SMALLINT,v_numUserCntID);
         end if;
         DELETE FROM OpportunitySalesTemplate WHERE numOppId = v_numOppId;
         delete FROM OpportunityLinking where   numChildOppID =  v_numOppId or   numParentOppID =    v_numOppId;
         delete FROM RecentItems where numRecordID =  v_numOppId and chrRecordType = 'O';
         IF v_tintOppType = 1 AND v_tintOppStatus = 1 then -- SALES ORDER
	
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
            UPDATE WareHouseItmsDTL WHIDL
            SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0)+coalesce(OWSI.numQty,0) ELSE 1 END)
            FROM
            OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
            WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
            AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND OWSI.numOppID = v_numOppId;
         ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 1
         then -- PURCHASE ORDER
	
            IF(SELECT
            COUNT(*)
            FROM
            OppWarehouseSerializedItem OWSI
            INNER JOIN
            OpportunityMaster OM
            ON
            OWSI.numOppID = OM.numOppId
            AND tintopptype = 1
            WHERE
            OWSI.numWarehouseItmsDTLID IN(SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = v_numOppId)) > 0 then
		
               RAISE EXCEPTION 'SERIAL/LOT#_USED';
            ELSE
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
               UPDATE WareHouseItmsDTL WHIDL
               SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0) -coalesce(OWSI.numQty,0) ELSE 0 END)
               FROM
               OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
               WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
               AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND OWSI.numOppID = v_numOppId;
            end if;
         end if;
         delete from OppWarehouseSerializedItem where numOppID =  v_numOppId;
         delete from OpportunityItemLinking where numNewOppID = v_numOppId;
         delete from OpportunityAddress  where numOppID = v_numOppId;
         DELETE FROM OpportunityBizDocKitItems WHERE numOppBizDocItemID IN(SELECT numOppBizDocItemID FROM OpportunityBizDocItems WHERE numOppBizDocID IN(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId));
         delete from OpportunityBizDocItems where numOppBizDocID in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId);
         delete from OpportunityItemsTaxItems WHERE numOppId = v_numOppId;
         delete from OpportunityMasterTaxItems WHERE numOppId = v_numOppId;
         delete from OpportunityBizDocDtl where numBizDocID in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId);
         DELETE FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId IN(SELECT numBizDocsPaymentDetId FROM OpportunityBizDocsDetails where numBizDocsId in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId));
         delete from OpportunityBizDocsDetails where numBizDocsId in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId);
         DELETE FROM ShippingReportItems WHERE numShippingReportId IN(SELECT numShippingReportID from ShippingReport WHERE numOppBizDocId IN(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId));
         DELETE FROM ShippingBox WHERE numShippingReportID IN(SELECT numShippingReportID from ShippingReport WHERE numOppBizDocId IN(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId));
         DELETE FROM ShippingReport WHERE numOppBizDocId IN(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId);
         delete from DocumentWorkflow where numDocID in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId) and cDocType = 'B';
         delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId) and btDocType = 1);
         delete from BizActionDetails where numOppBizDocsId in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppId) and btDocType = 1;
         delete from OpportunityBizDocs where numoppid = v_numOppId;
         delete from OpportunityContact where numOppId = v_numOppId;
         delete from OpportunityDependency where numOpportunityId = v_numOppId;
         delete from timeandexpense where numOppId = v_numOppId   and numDomainID = v_numDomainID;                     
  
  --Credit Balance
         delete from CreditBalanceHistory where numReturnID in(select numReturnID from Returns WHERE numOppID = v_numOppId);
         IF v_tintOppType = 1 then --Sales
            update DivisionMaster set monSCreditBalance = coalesce(monSCreditBalance,0) -coalesce(v_monCreditAmount,0) where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
         ELSEIF v_tintOppType = 2
         then --Purchase
            update DivisionMaster set monPCreditBalance = coalesce(monPCreditBalance,0) -coalesce(v_monCreditAmount,0) where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
         end if;
         UPDATE
         DiscountCodeUsage DCU
         SET
         intCodeUsed = coalesce(intCodeUsed,0) -1
         FROM(SELECT
         numDivisionId
				,numDiscountID
         FROM
         OpportunityMaster
         WHERE
         OpportunityMaster.numOppId = v_numOppId) AS T1 WHERE(DCU.numDiscountId = T1.numDiscountID
         AND DCU.numDivisionId = T1.numDivisionId);
         DELETE FROM Returns WHERE numOppID = v_numOppId;
         delete from OpportunityKitItems where numOppId = v_numOppId;
         delete from OpportunityItems where numOppId = v_numOppId;
         DELETE FROM ProjectProgress WHERE numOppId = v_numOppId AND numDomainID = v_numDomainID;
         delete from OpportunityStageDetails where numoppid = v_numOppId;
         delete from OpportunitySubStageDetails where numOppId = v_numOppId;
         DELETE FROM OpportunityRecurring WHERE numOppId = v_numOppId;
         DELETE FROM OpportunityMasterAPI WHERE numOppId = v_numOppId;
         DELETE FROM WebApiOrderDetails WHERE numOppId = v_numOppId;
         DELETE FROM WebApiOppItemDetails WHERE numOppId = v_numOppId;
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
         DELETE FROM General_Journal_Details WHERE numJournalId IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numOppId = v_numOppId AND numDomainId = v_numDomainID);
         DELETE FROM General_Journal_Header WHERE numOppId = v_numOppId AND numDomainId = v_numDomainID;
         IF v_tintCommitAllocation = 2 then
	
            DELETE FROM OpportunityMaster WHERE numOppId = v_numOppId AND numDomainId = v_numDomainID;
         ELSE
            IF coalesce(v_tintOppStatus,0) = 0 OR coalesce(v_isOrderItemsAvailable,0) = 0 OR(SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID = v_numDomainID AND numReferenceID = v_numOppId AND tintRefType IN(3,4) AND vcDescription ilike '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE)) > 0 then
               DELETE FROM OpportunityMaster WHERE numOppId = v_numOppId and numDomainId = v_numDomainID;
            ELSE
               RAISE EXCEPTION 'INVENTORY IM-BALANCE';
            end if;
         end if;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
             GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;

            RETURN;
      END;
   end if; 
END; $$;


