-- Stored procedure definition script USP_CheckHelpLogin for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckHelpLogin(v_numAppAccessId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainId  NUMERIC(18,0);
   v_numHour  NUMERIC(18,0);
   v_maxHour  NUMERIC(18,0);
   v_flag  BOOLEAN;
BEGIN
   select((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -dtLoggedInTime)*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -dtLoggedInTime)*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -dtLoggedInTime))), numDomainId INTO v_numHour,v_numDomainId FROM UserAccessedDTL WHERE numAppAccessID = v_numAppAccessId AND dtLoggedOutTime IS NULL; 
   select   coalesce(tintSessionTimeOut,1)*60 INTO v_maxHour FROM Domain WHERE numDomainId = v_numDomainId;
   RAISE NOTICE '%',v_maxHour;
   RAISE NOTICE '%',v_numHour;
   IF  v_numHour <= v_maxHour then
 
      v_flag := true;
   ELSE
      v_flag := false;
   end if; 
 
   open SWV_RefCur for SELECT v_flag;
   RETURN;
END; $$; 













