-- Stored procedure definition script USP_Item_IsKitWithChildKits for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_IsKitWithChildKits(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT(CASE
   WHEN coalesce(Item.bitKitParent,false) = true
   THEN(CASE
      WHEN(SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID = Item.numItemCode AND coalesce(Item.bitKitParent,false) = true) > 0
      THEN 1
      ELSE 0
      END)
   ELSE 0
   END) AS bitHasKitAsChild
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode;
END; $$;












