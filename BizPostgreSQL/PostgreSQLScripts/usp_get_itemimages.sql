-- Stored procedure definition script USP_GET_ItemImages for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_ItemImages(v_numDomainID NUMERIC(9,0),
v_numItemCode NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numItemImageId
		,numItemCode
		,vcPathForImage
		,vcPathForTImage
		,bitDefault
		,intDisplayOrder
		,bitIsImage 
	FROM 
		ItemImages
	WHERE 
		numDomainId = v_numDomainID  
		AND numItemCode = v_numItemCode
	ORDER BY 
		intDisplayOrder;
END; $$;

--exec  USP_GET_ItemImages @numDomainId = 1 ,@numItemCode =197605













