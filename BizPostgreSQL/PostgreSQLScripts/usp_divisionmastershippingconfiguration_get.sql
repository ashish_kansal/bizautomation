-- Stored procedure definition script USP_DivisionMasterShippingConfiguration_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DivisionMasterShippingConfiguration_Get(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
  numDomainID
        ,numDivisionID
        ,IsAdditionalHandling
        ,IsCOD
        ,IsHomeDelivery
        ,IsInsideDelevery
        ,IsInsidePickup
        ,IsSaturdayDelivery
        ,IsSaturdayPickup
        ,IsLargePackage
        ,vcDeliveryConfirmation
        ,vcDescription
        ,vcSignatureType
        ,vcCODType
   FROM
   DivisionMasterShippingConfiguration
   WHERE
   numDomainID = v_numDomainID
   AND numDivisionID = v_numDivisionID;
END; $$;












