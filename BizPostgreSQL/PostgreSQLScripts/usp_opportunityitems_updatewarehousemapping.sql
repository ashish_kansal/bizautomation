-- Stored procedure definition script USP_OpportunityItems_UpdateWarehouseMapping for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItems_UpdateWarehouseMapping(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOrderSource  NUMERIC(18,0);
   v_tintSourceType  SMALLINT;
   v_numShipToState  NUMERIC(18,0);
   v_numShipToCountry  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numOppItemID  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_bitKitParent  BOOLEAN;
   v_numUnitHour  DOUBLE PRECISION;
   v_numWarehouseItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(OpportunityMaster.tintSource,0), coalesce(OpportunityMaster.tintSourceType,0) INTO v_numOrderSource,v_tintSourceType FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND numOppId = v_numOppID;

   select   coalesce(numState,0), coalesce(numCountry,0) INTO v_numShipToState,v_numShipToCountry FROM
   fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT);

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseID NUMERIC(18,0)
   );

   INSERT INTO tt_TEMPWAREHOUSE(numWarehouseID)
   SELECT
   MSFWP.numWarehouseID
   FROM
   MassSalesFulfillmentWM MSFWM
   INNER JOIN
   MassSalesFulfillmentWMState MSFWMS
   ON
   MSFWM.ID = MSFWMS.numMSFWMID
   INNER JOIN
   MassSalesFulfillmentWP MSFWP
   ON
   MSFWM.ID = MSFWP.numMSFWMID
   WHERE
   numDomainID = v_numDomainID
   AND numOrderSource = v_numOrderSource
   AND tintSourceType = v_tintSourceType
   AND numCountryID = v_numShipToCountry
   AND numStateID = v_numShipToState
   ORDER BY
   intOrder;

   IF EXISTS(SELECT ID FROM tt_TEMPWAREHOUSE) then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppItemID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         bitKitParent BOOLEAN,
         numWarehouseItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPITEMS(numOppItemID
			,numItemCode
			,bitKitParent
			,numWarehouseItemID
			,numUnitHour)
      SELECT
      OI.numoppitemtCode
			,OI.numItemCode
			,coalesce(I.bitKitParent,false)
			,OI.numWarehouseItmsID
			,coalesce(OI.numUnitHour,0) -coalesce(OI.numWOQty,0)
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OI.numOppId = v_numOppID
      AND coalesce(OI.numWarehouseItmsID,0) > 0
      AND coalesce(OI.bitDropShip,false) = false;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPITEMS;
      WHILE v_i <= v_iCount LOOP
         select   numOppItemID, numItemCode, bitKitParent, numUnitHour, numWarehouseItemID INTO v_numOppItemID,v_numItemCode,v_bitKitParent,v_numUnitHour,v_numWarehouseItemID FROM
         tt_TEMPITEMS WHERE
         ID = v_i;
         select   numWareHouseItemID INTO v_numWarehouseItemID FROM(SELECT
            ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) AS numRowIndex
					,* FROM(SELECT
               WI.numWareHouseItemID
						,(CASE
               WHEN v_bitKitParent = true
               THEN(CASE
                  WHEN NOT EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numChildItemID = WIInner.numItemID WHERE OKI.numOppId = v_numOppID AND OKI.numOppItemID = v_numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKI.numOppChildItemID,OKI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKI.numQtyItemsReq)
                  AND  NOT EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numItemID = WIInner.numItemID WHERE OKCI.numOppId = v_numOppID AND OKCI.numOppItemID = v_numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKCI.numOppKitChildItemID,OKCI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKCI.numQtyItemsReq)
                  THEN 1
                  ELSE 0
                  END)
               ELSE(CASE WHEN(SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID = WI.numWareHouseID) >= v_numUnitHour THEN 1 ELSE 0 END)
               END) AS titOnHandOrder
						,TW.ID AS intOrder
               FROM
               WareHouseItems WI
               INNER JOIN
               tt_TEMPWAREHOUSE TW
               ON
               WI.numWareHouseID = TW.numWarehouseID
               WHERE
               WI.numItemID = v_numItemCode
               AND coalesce(WI.numWareHouseID,0) = 0) TEMP) T1    ORDER BY
         titOnHandOrder DESC,intOrder ASC LIMIT 1;
         IF coalesce(v_numWarehouseItemID,0) > 0 then
			
            UPDATE OpportunityItems SET numWarehouseItmsID = v_numWarehouseItemID WHERE numOppId = v_numOppID AND numoppitemtCode = v_numOppItemID;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
   RETURN;
END; $$;


