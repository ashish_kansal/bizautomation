-- Stored procedure definition script USP_GetContactTypeMapping for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactTypeMapping(v_numMappingID NUMERIC(9,0) DEFAULT 0,
	v_numContactTypeID NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numMappingID as VARCHAR(255)),
	cast(numContactTypeID as VARCHAR(255)),
	cast(numAccountID as VARCHAR(255)),
	cast(numDomainID as VARCHAR(255))
   FROM ContactTypeMapping
   WHERE
(numMappingID = v_numMappingID OR v_numMappingID = 0)
   AND (numContactTypeID = v_numContactTypeID OR v_numContactTypeID = 0)
   AND numDomainID = v_numDomainID;







/****** Object:  StoredProcedure [dbo].[USP_GetCurrencyFromCountry]    Script Date: 07/26/2008 16:19:10 ******/
   RETURN;
END; $$;












