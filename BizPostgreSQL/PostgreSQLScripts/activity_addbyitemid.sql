-- Stored procedure definition script Activity_AddByItemId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
DROP TYPE IF EXISTS Activity_AddByItemId_rs CASCADE;
CREATE TYPE Activity_AddByItemId_rs AS(NewActivityID INTEGER);
CREATE OR REPLACE FUNCTION Activity_AddByItemId(v_AllDayEvent  BOOLEAN,  -- whether this is an all-day event  
 v_ActivityDescription TEXT,  -- additional information about the activity  
 v_Duration  INTEGER,  -- length of activity in seconds  
 v_Location  VARCHAR(64), -- where the activity takes place  
 v_StartDateTimeUtc TIMESTAMP, -- when the activity begins  
 v_Subject  VARCHAR(255), -- brief description of the activity  
 v_EnableReminder BOOLEAN,  -- whether this activity should notify the user  
 v_ReminderInterval INTEGER,  -- how long before start time (in seconds) to notify the user  
 v_ShowTimeAs  INTEGER,  -- how this time use is displayed in DayView to the user  
 v_Importance  INTEGER,  -- the priority of this activity  
 v_Status   INTEGER,  -- the reminder notification status of this activity (expired, pending, etc.)  
 v_RecurrenceKey  INTEGER,  -- relates the new activity to its recurrence pattern, if any  
 v_ResourceName INTEGER, -- name of the resource who created this activity  
 v_ItemId VARCHAR(500) DEFAULT '',  
 v_ChangeKey VARCHAR(500) DEFAULT '')
RETURNS SETOF Activity_AddByItemId_rs LANGUAGE plpgsql  
 --  Lookup the ResourceName I've been given and resolve a ResourceID.  
 --    
   AS $$
   DECLARE
   v_ResourceID  INTEGER;
   v_check  NUMERIC;
   v_NewActivityID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_ResourceName) = true then

      v_ResourceID := CAST(v_ResourceName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if; 
 --  When the business layer supplies a ResourceName of NULL, resolve ResourceID  
 --  to the Unassigned Resource (it must always have the ID of -999).  
 --  
-- IF ( @ResourceName IS NULL )  
--  SET @ResourceID = -999;  
-- ELSE  
-- BEGIN  
--  SELECT   
--   @ResourceID = [Resource].[ResourceID]  
--  FROM  
--   [Resource]  
--  WHERE  
--   ( [Resource].[ResourceName] = @ResourceName );  
-- END  
  
 --  ResourceID is only NULL at this point if the ResourceName was not NULL and  
 --  the ResourceName supplied was not found in the Resource table.  
 --  
 --  Right now, deal with this by not inserting any records.  The business layer should  
 --  detect that 0 rows were affected by the INSERT and take corrective action.  In  
 --  the future, the data layer may treat "Resource Not Found" as an error condition.  
 --  
   IF (v_ResourceID IS NOT NULL) then
      select   count(*) INTO v_check from Activity where itemId = v_ItemId;
      if v_check = 0 then
  
  
  -- Add Activity and ActivityResource rows atomicly, rollback  
  -- when there is an error.  
  --  
         -- BEGIN TRANSACTION

  INSERT INTO Activity(AllDayEvent,
   ActivityDescription,
   Duration,
   Location,
   StartDateTimeUtc,
   Subject,
   EnableReminder,
   ReminderInterval,
   ShowTimeAs,
   Importance,
   Status,
   RecurrenceID,
   VarianceID,
itemId,
ChangeKey)
  VALUES(v_AllDayEvent,
   v_ActivityDescription,
   v_Duration,
   v_Location,
   v_StartDateTimeUtc,
   v_Subject,
   v_EnableReminder,
   v_ReminderInterval,
   v_ShowTimeAs,
   v_Importance,
   v_Status,
   v_RecurrenceKey,
   NULL,
   v_ItemId ,
   v_ChangeKey);  
  
  --  Get the ActivityID generated by the Identity column, ID, following  
  --  the INSERT statement above.  Use SCOPE_IDENTITY( ) here to  
  --  avoid the side effect a trigger could have on @@IDENTITY.  
  --  
  
         v_NewActivityID := CURRVAL('Activity_ActivityID_seq');  
  
  --  Insert a relating row in the ActivityResource table using the New ActivityID  
  --  and my resolved ResourceID.  
  --  
         INSERT INTO ActivityResource(ActivityID,
   ResourceID)
  VALUES(v_NewActivityID,
   v_ResourceID);
  
         -- COMMIT


  return query SELECT
         v_NewActivityID;
      else
         UPDATE
         Activity
         SET
         AllDayEvent = v_AllDayEvent,ActivityDescription = v_ActivityDescription,
         Duration = v_Duration,Location = v_Location,StartDateTimeUtc = v_StartDateTimeUtc,
         Subject = v_Subject,EnableReminder = v_EnableReminder,ReminderInterval = v_ReminderInterval,
         ShowTimeAs = v_ShowTimeAs,Importance = v_Importance,
         Status = v_Status,RecurrenceID = v_RecurrenceKey,ChangeKey = v_ChangeKey
         WHERE
         Activity.itemId = v_ItemId;
      end if;
   end if;
   delete FROM Recurrence where recurrenceid not in(select RecurrenceID from Activity) and recurrenceid <> -999;
   RETURN;
END; $$;  
  
--deleting the recurrence in the table which are not used anymore (excluding the default recurring )  



