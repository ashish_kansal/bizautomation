-- FUNCTION: public.usp_getbizdocitemsforpickpackslip(numeric, numeric, numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getbizdocitemsforpickpackslip(numeric, numeric, numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbizdocitemsforpickpackslip(
	v_numdomainid numeric,
	v_numoppid numeric,
	v_numoppbizdocid numeric,
	v_numbizdocid numeric,
	v_numforbizdocid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:10:10 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT
   cast(OBDI.numOppItemID as VARCHAR(255)) AS numoppitemtcode
		,OBD.numOppBizDocsId
		,cast(OBDI.numUnitHour as VARCHAR(255)) AS OrderedQty
		,cast(TEMP.numUnitHour as VARCHAR(255)) AS UsedQty
		,coalesce(cast(OBDI.numUnitHour As Numeric),0) - coalesce(cast(TEMP.numUnitHour As Numeric),0) AS RemainingQty
   FROM
   OpportunityBizDocs OBD
   INNER JOIN
   OpportunityBizDocItems OBDI
   ON
   OBDI.numOppBizDocID = OBD.numOppBizDocsId
   LEFT JOIN
   BizDocTemplate AS B
   ON
   OBD.numBizDocTempID = B.numBizDocTempID
   LEFT JOIN LATERAL(SELECT
      cast(SUM(numUnitHour) as VARCHAR(255)) AS numUnitHour
      FROM
      OpportunityBizDocs OBDCHild
      INNER JOIN
      OpportunityBizDocItems OBDICHild
      ON
      OBDICHild.numOppBizDocID = OBDCHild.numOppBizDocsId
      WHERE
      OBDCHild.numSourceBizDocId = OBD.numOppBizDocsId
      AND OBDICHild.numOppItemID = OBDI.numOppItemID
      AND OBDCHild.numBizDocId = v_numForBizDocID) TEMP on TRUE
   WHERE
   OBD.numoppid = v_numOppID
   AND OBD.numOppBizDocsId = v_numOppBizDocID;
END;
$BODY$;

ALTER FUNCTION public.usp_getbizdocitemsforpickpackslip(numeric, numeric, numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
