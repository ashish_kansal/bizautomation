DROP FUNCTION IF EXISTS USP_MassSalesFulfillment_GetRecords;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetRecords(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_numViewID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_bitGroupByOrder BOOLEAN
	,v_bitRemoveBO BOOLEAN
	,v_numShippingZone NUMERIC(18,0)
	,v_bitIncludeSearch BOOLEAN
	,v_vcOrderStauts TEXT
	,v_tintFilterBy SMALLINT
	,v_vcFilterValue TEXT
	,v_vcCustomSearchValue TEXT
	,v_numBatchID NUMERIC(18,0)
	,v_tintPackingViewMode SMALLINT
	,v_tintPrintBizDocViewMode SMALLINT
	,v_tintPendingCloseFilter SMALLINT
	,v_numPageIndex INTEGER
	,INOUT v_vcSortColumn VARCHAR(100) 
	,INOUT v_vcSortOrder VARCHAR(4) 
	,INOUT v_numTotalRecords NUMERIC(18,0)
    ,INOUT SWV_RefCur refcursor 
    ,INOUT SWV_RefCur2 refcursor
)
 LANGUAGE plpgsql
AS $$

DECLARE
   v_tintPackingMode  SMALLINT;
   v_tintInvoicingType  SMALLINT;
   v_tintCommitAllocation  SMALLINT;
   v_bitEnablePickListMapping  BOOLEAN;
   v_bitEnableFulfillmentBizDocMapping  BOOLEAN;
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_numDefaultSalesShippingDoc  NUMERIC(18,0);
   v_vcSelect TEXT;
	v_vcFrom TEXT;
	v_vcWhere TEXT;
	v_vcGroupBy TEXT;
	v_vcOrderBy TEXT;
	v_vcOrderBy1 TEXT;

   v_vcText  TEXT;

   v_j  INTEGER DEFAULT 0;
   v_jCount  INTEGER;
   v_numFieldId  NUMERIC(18,0);
   v_vcListItemType  VARCHAR(10);
   v_vcFieldName  VARCHAR(50);                                                                                                
   v_vcAssociatedControlType  VARCHAR(10);  
   v_vcLookBackTableName  VARCHAR(100);
   v_vcDbColumnName  VARCHAR(100);
   v_Grp_id  INTEGER;                                               
   v_bitCustomField  BOOLEAN;      
   v_bitIsNumeric  BOOLEAN;
   v_vcGroupByInclude  TEXT DEFAULT '';
   v_vcFinal  TEXT DEFAULT '';
   v_vcGroupBy1  TEXT DEFAULT '';
   
   
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF v_numViewID = 4 OR v_numViewID = 5 OR v_numViewID = 6 OR (v_numViewID = 2 AND v_tintPackingViewMode = 1) then
	
      v_bitGroupByOrder := true;
   end if;

   v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate');

   IF POSITION('OpportunityItems.vcInventoryStatus' IN v_vcCustomSearchValue) > 0 then
      IF POSITION('OpportunityItems.vcInventoryStatus = 1' IN v_vcCustomSearchValue) > 0 then
         v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcInventoryStatus = 1',' position(''Not Applicable'' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT)) > 0 ');
      end if;
      IF POSITION('OpportunityItems.vcInventoryStatus = 2' IN v_vcCustomSearchValue) > 0 then
         v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcInventoryStatus = 2',' position(''Shipped'' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT)) > 0 ');
      end if;
      IF POSITION('OpportunityItems.vcInventoryStatus = 3' IN v_vcCustomSearchValue) > 0 then
         v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcInventoryStatus = 3',' position(''BO'' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT)) > 0 AND position(''Shippable'' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT)) = 0 ');
      end if;
      IF POSITION('OpportunityItems.vcInventoryStatus = 4' IN v_vcCustomSearchValue) > 0 then
         v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcInventoryStatus = 4',' position(''Shippable'' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT)) > 0 ');
      end if;
   end if;

   If v_numViewID = 6 AND POSITION('OpportunityMaster.bintCreatedDate' IN v_vcCustomSearchValue) > 0 then
	
      v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','OpportunityBizDocs.dtCreatedDate + ( ' || CAST(COALESCE(v_ClientTimeZoneOffset,0) AS VARCHAR) || ' * -1 ' || ' || '' minutes'')::interval');
   ELSEIF POSITION('OpportunityMaster.bintCreatedDate' IN v_vcCustomSearchValue) > 0
   then
	
      v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','OpportunityMaster.bintCreatedDate + ( ' || CAST(COALESCE(v_ClientTimeZoneOffset,0) AS VARCHAR) || ' * -1 ' || ' || '' minutes'')::interval');
   end if;

   IF v_bitGroupByOrder = true then
	
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID',
      'OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull',
      'OpportunityMaster.numAge','OpportunityMaster.numStatus',
      'OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo',
      'OpportunityMaster.numRecOwner') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   ELSE
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','Item.charItemType','Item.numBarCodeId','Item.numItemClassification',
      'Item.numItemGroup','Item.vcSKU','OpportunityBizDocs.monAmountPaid',
      'OpportunityMaster.dtExpectedDate','OpportunityItems.numQtyPacked',
      'OpportunityItems.numQtyPicked','OpportunityItems.numQtyShipped',
      'OpportunityItems.numRemainingQty','OpportunityItems.numUnitHour',
      'OpportunityItems.vcInvoiced','OpportunityItems.vcItemName',
      'OpportunityItems.vcItemReleaseDate','OpportunityMaster.bintCreatedDate',
      'OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus',
      'OpportunityMaster.vcPoppName','WareHouseItems.vcLocation',
      'Item.dtItemReceivedDate','OpportunityBizDocs.vcBizDocID',
      'Warehouses.vcWareHouse') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   end if;

   select   coalesce(tintCommitAllocation,1), coalesce(numShippingServiceItemID,0), coalesce(numDefaultSalesShippingDoc,0) INTO v_tintCommitAllocation,v_numShippingServiceItemID,v_numDefaultSalesShippingDoc FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   IF v_numViewID = 2 AND v_tintPackingViewMode = 2 AND v_numDefaultSalesShippingDoc = 0 then
	
      RAISE EXCEPTION 'DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED';
      RETURN;
   end if;

   select   tintPackingMode, tintInvoicingType, bitEnablePickListMapping, bitEnableFulfillmentBizDocMapping INTO v_tintPackingMode,v_tintInvoicingType,v_bitEnablePickListMapping,v_bitEnableFulfillmentBizDocMapping FROM
   MassSalesFulfillmentConfiguration WHERE
   numDomainID = v_numDomainID; 

   BEGIN
      CREATE TEMP SEQUENCE tt_TempFieldsLeft_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   
   DROP TABLE IF EXISTS tt_TEMPFIELDSLEFT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSLEFT
   (
      numPKID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      ID VARCHAR(50),
      numFieldID NUMERIC(18,0),
      vcFieldName VARCHAR(100),
      vcOrigDbColumnName VARCHAR(100),
      vcListItemType VARCHAR(10),
      numListID NUMERIC(18,0),
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      vcAssociatedControlType VARCHAR(20),
      vcLookBackTableName VARCHAR(100),
      bitCustomField BOOLEAN,
      intRowNum INTEGER,
      intColumnWidth DOUBLE PRECISION,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage TEXT,
      Grp_id INTEGER
   );

   INSERT INTO tt_TEMPFIELDSLEFT(ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id)
   SELECT
   CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,CAST((CASE WHEN v_numViewID = 6 AND DFM.numFieldID = 248 THEN 'BizDoc' ELSE DFFM.vcFieldName END) AS VARCHAR(100))
		,DFM.vcOrigDbCOlumnName
		,coalesce(DFM.vcListItemType,'')
		,coalesce(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,CAST(0 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST((CASE WHEN vcFieldDataType IN('N','M') THEN 1 ELSE 0 END) AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS INTEGER)
		,CAST(0 AS INTEGER)
		,CAST(0 AS BOOLEAN)
		,CAST('' AS TEXT)
		,0
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   DycFormField_Mapping DFFM
   ON
   DFCD.numFormID = DFFM.numFormID
   AND DFCD.numFieldID = DFFM.numFieldID
   INNER JOIN
   DycFieldMaster DFM
   ON
   DFFM.numFieldID = DFM.numFieldID
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 141
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFFM.bitDeleted,false) = false
   AND coalesce(DFFM.bitSettingField,false) = true
   AND coalesce(DFCD.bitCustom,false) = false
   UNION
   SELECT
   CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.FLd_label
		,CONCAT('CUST',CFM.Fld_id)
		,CAST('' AS VARCHAR(10))
		,coalesce(CFM.numlistid,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CFM.fld_type
		,CAST('' AS VARCHAR(100))
		,CAST(1 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,coalesce(bitIsRequired,false)
		,coalesce(bitIsEmail,false)
		,coalesce(bitIsAlphaNumeric,false)
		,coalesce(bitIsNumeric,false)
		,coalesce(bitIsLengthValidation,false)
		,coalesce(intMaxLength,0)
		,coalesce(intMinLength,0)
		,coalesce(bitFieldMessage,false)
		,coalesce(vcFieldMessage,'')
		,CFM.Grp_id
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   CFW_Fld_Master CFM
   ON
   DFCD.numFieldID = CFM.Fld_id
   LEFT JOIN
   CFW_Validation CV
   ON
   CFM.Fld_id = CV.numFieldId
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 141
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFCD.bitCustom,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPFIELDSLEFT) = 0 then
	
      INSERT INTO tt_TEMPFIELDSLEFT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,coalesce(DFM.vcListItemType,'')
			,coalesce(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 141
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND coalesce(DFFM.bitDefault,false) = true;
   end if;

   v_vcSelect := CONCAT('SELECT 
												OpportunityMaster.numOppID AS "numOppID"
												,OpportunityMaster.dtExpectedDate AS "dtExpectedDateOrig"
												,DivisionMaster.numDivisionID AS "numDivisionID"
												,COALESCE(DivisionMaster.numTerID,0) AS "numTerID"
												,COALESCE(OpportunityMaster.intUsedShippingCompany,0) "numOppShipVia"
												,COALESCE(OpportunityMaster.numShippingService,0) "numOppShipService"
												,COALESCE(DivisionMaster.intShippingCompany,0) AS "numPreferredShipVia"
												,COALESCE(DivisionMaster.numDefaultShippingServiceID,0) "numPreferredShipService"
												,COALESCE(OpportunityBizDocs.numOppBizDocsId,0) AS "numOppBizDocID"
												,COALESCE(OpportunityBizDocs.vcBizDocID,'''') AS "vcBizDocID"
												,COALESCE(OpportunityBizDocs.monAmountPaid,0) AS "monAmountPaid"
												,COALESCE(OpportunityBizDocs.monDealAmount,0) - COALESCE(OpportunityBizDocs.monAmountPaid,0) AS "monAmountToPay"
												',(CASE WHEN coalesce(v_bitGroupByOrder,false) = true THEN ',0 AS "numOppItemID"' ELSE ',COALESCE(OpportunityItems.numoppitemtCode,0) AS "numOppItemID"' END),'
												',
   (CASE WHEN coalesce(v_bitGroupByOrder,false) = true
   THEN ',0 AS "numRemainingQty"'
   ELSE ',(CASE 
				WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 1
				THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0)
				WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 3
				THEN (CASE 
						WHEN COALESCE(' || CAST(COALESCE(v_tintInvoicingType,0) AS VARCHAR) || ',0)=1 
						THEN COALESCE((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287),0)
						ELSE COALESCE(OpportunityItems.numUnitHour,0) 
					END) - COALESCE((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287),0)
				ELSE 0
			END) AS "numRemainingQty"'
   END),','''' AS "dtAnticipatedDelivery",'''' "vcShipStatus",'''' "vcPaymentStatus",0 AS "numShipRate"
													,(CASE 
														WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 2 AND ' || CAST(COALESCE(v_tintPackingViewMode,0) AS VARCHAR) || ' = 2 AND ' || CAST(COALESCE(v_bitEnablePickListMapping,false) AS VARCHAR) || ' = true 
														THEN CONCAT(''['',COALESCE((SELECT string_agg(CONCAT('' {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',COALESCE(BT.vcTemplateName,''''),''"}'') , '','')
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=55206),''''),'']'')
														WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 3 AND ' || CAST(COALESCE(v_bitEnableFulfillmentBizDocMapping,false) AS VARCHAR) || '= true 
														THEN CONCAT(''['',COALESCE((SELECT string_agg(CONCAT('' {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',COALESCE(BT.vcTemplateName,''''),''"}'') , '','') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (' || CAST(COALESCE(v_numDefaultSalesShippingDoc,0) AS VARCHAR) || ',296) ),''''),'']'')
														ELSE ''''
													END) AS "vcLinkingBizDocs",
													(CASE WHEN EXISTS (SELECT OIInner.numOppItemtcode FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND COALESCE(OIInner.bitRequiredWarehouseCorrection,false) = true) THEN true ELSE false END) AS "bitRequiredWarehouseCorrection"',
   (CASE WHEN v_numViewID = 2 AND v_tintPackingViewMode = 1 THEN ',FormatedDateFromDate(COALESCE(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)),' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') AS "dtShipByDate"' ELSE ',CAST(OpportunityMaster.dtReleaseDate AS DATE) AS "dtShipByDate"' END));

   v_vcFrom := CONCAT(' FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						LEFT JOIN 
							MassSalesFulfillmentBatchOrders
						ON
							MassSalesFulfillmentBatchOrders.numBatchID = ' || CAST(COALESCE(v_numBatchID,0) AS VARCHAR) || '
							AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
							AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR COALESCE(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						LEFT JOIN
							WareHouses
						ON
							WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
						LEFT JOIN
							WarehouseLocation
						ON
							WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						',(CASE WHEN COALESCE(v_numViewID, 0) = 6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND ' || CAST(COALESCE(v_numViewID, 0) AS VARCHAR) || ' IN (4,6)
						LEFT JOIN
							OpportunityBizDocItems
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
							AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID'
	,(CASE 
		WHEN EXISTS (SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcOrigDbColumnName = 'numQtyPicked' OR vcOrigDbColumnName = 'numQtyPacked' OR vcOrigDbColumnName = 'numQtyShipped') OR COALESCE(v_numViewID, 0) = 2
		THEN (CASE 
				WHEN v_bitGroupByOrder=true 
				THEN ' LEFT JOIN LATERAL 
						(
							SELECT 
								OBInner.numOppId
								,OBIInner.numOppItemID
								,SUM(OBIInner.numUnitHour) numPacked
							FROM
								OpportunityBizDocs OBInner
							INNER JOIN
								OpportunityBizDocItems OBIInner
							ON
								OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
							WHERE
								OBInner.numBizDocId=' || COALESCE(v_numDefaultSalesShippingDoc,0)::VARCHAR || '
							GROUP BY
								OBInner.numOppId
								,OBIInner.numOppItemID
						) TEMPPacked  ON TEMPPacked.numOppId = OpportunityMaster.numOppID AND TEMPPacked.numOppItemID = OpportunityItems.numoppitemtcode
						LEFT JOIN LATERAL
						(
							SELECT 
								OBInner.numOppId
								,OBIInner.numOppItemID
								,SUM(OBIInner.numUnitHour) numShipped
							FROM
								OpportunityBizDocs OBInner
							INNER JOIN
								OpportunityBizDocItems OBIInner
							ON
								OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
							WHERE
								OBInner.numBizDocId=296
							GROUP BY
								OBInner.numOppId
								,OBIInner.numOppItemID
						) TEMPShipped ON TEMPShipped.numOppId = OpportunityMaster.numOppID AND TEMPShipped.numOppItemID = OpportunityItems.numoppitemtcode '
				ELSE ' LEFT JOIN LATERAL 
						(
							SELECT 
								SUM(OBIInner.numUnitHour) numPacked
							FROM
								OpportunityBizDocs OBInner
							INNER JOIN
								OpportunityBizDocItems OBIInner
							ON
								OBIInner.numoppbizdocid = OBInner.numOppBizDocsId
								AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode
							WHERE
								OBInner.numOppId = OpportunityMaster.numOppID
								AND OBInner.numBizDocId=' || COALESCE(v_numDefaultSalesShippingDoc,0)::VARCHAR || '
						) TEMPPacked ON TRUE 
						LEFT JOIN LATERAL
						(
							SELECT 
								SUM(OBIInner.numUnitHour) numShipped
							FROM
								OpportunityBizDocs OBInner
							INNER JOIN
								OpportunityBizDocItems OBIInner
							ON
								OBIInner.numoppbizdocid = OBInner.numOppBizDocsId
								AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode
							WHERE
								OBInner.numOppId = OpportunityMaster.numOppID
								AND OBInner.numBizDocId=296
						) TEMPShipped ON TRUE '
			END)
		
		
		ELSE ''
	END)
	,(CASE 
		WHEN COALESCE(v_numViewID,0) = 5 AND coalesce(v_tintPendingCloseFilter,0) = 2
		THEN 
			' LEFT JOIN LATERAL
			(
				SELECT
					SUM(OBDI.numUnitHour) AS FulFilledQty
				FROM
					OpportunityBizDocs OB
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OB.numOppBizDocsId = OBDI.numOppBizDocID
					AND OBDI.numOppItemID = OpportunityItems.numOppItemtCode
				WHERE
					OB.numOppID = OpportunityMaster.numOppID
					AND COALESCE(OB.numBizDocId,0) = 296
			) TEMpBizDoc296 ON TRUE
			LEFT JOIN LATERAL
			(
				SELECT
					SUM(OBDI.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs OB
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OB.numOppBizDocsId = OBDI.numOppBizDocID
					AND OBDI.numOppItemID = OpportunityItems.numOppItemtCode
				WHERE
					OB.numOppID = OpportunityMaster.numOppID
					AND COALESCE(OB.bitAuthoritativeBizDocs,0) = 1
			) TEMpBizDoc287 ON TRUE '
	ELSE ''
	END));

   v_vcWhere := CONCAT(' WHERE
							OpportunityMaster.numDomainId=' || CAST(COALESCE(v_numDomainID, 0) AS VARCHAR) || '
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND COALESCE(OpportunityItems.bitMappingRequired,false) = false '
							,(CASE 
							   WHEN v_numViewID = 6 AND v_tintPendingCloseFilter = 1 THEN ' AND COALESCE(OpportunityMaster.tintshipped,0) = 0'
							   WHEN v_numViewID = 6 AND v_tintPendingCloseFilter = 2 THEN ' AND COALESCE(OpportunityMaster.tintshipped,0) = 1'
							   WHEN v_numViewID = 6 AND v_tintPendingCloseFilter = 3 THEN ''
							   ELSE ' AND COALESCE(OpportunityMaster.tintshipped,0) = 0'
							END),
   (CASE WHEN v_numViewID <> 6 AND coalesce(v_numWarehouseID,0) > 0 THEN ' AND (COALESCE(OpportunityItems.numWarehouseItmsID,0) = 0 OR (WareHouseItems.numWarehouseID=' || CAST(COALESCE(v_numWarehouseID, 0) AS VARCHAR) || ' OR COALESCE(OpportunityItems.bitRequiredWarehouseCorrection,false) = true ))' ELSE '' END),(CASE 
   WHEN (v_numViewID = 1 AND coalesce(v_bitRemoveBO,false) = true)
   THEN ' AND 1 = (CASE 
						WHEN (Item.charItemType = ''P'' AND COALESCE(OpportunityItems.bitDropShip,false) = false AND COALESCE(Item.bitAsset,false) = false)
						THEN 
							(CASE 
								WHEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode::NUMERIC,(CASE 
																											WHEN ' || CAST(COALESCE(v_numViewID, 0) AS VARCHAR) || ' = 1
																											THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0)
																											ELSE OpportunityItems.numUnitHour
																											END)::DOUBLE PRECISION,OpportunityItems.numoppitemtCode::NUMERIC,OpportunityItems.numWarehouseItmsID::NUMERIC,(CASE WHEN COALESCE(Item.bitKitParent,false) = true THEN 1 ELSE 0 END)::BOOLEAN) > 0 THEN 0 ELSE 1 END)
			ELSE 1
		END)'
   ELSE ''
   END),
   (CASE 
   WHEN LENGTH(coalesce(v_vcOrderStauts,'')) > 0 
   THEN(CASE 
      WHEN coalesce(v_bitIncludeSearch,false) = false 
      THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM SplitIDs(''' || CAST(COALESCE(v_vcOrderStauts,'') AS VARCHAR) || ''','','')) THEN 1 ELSE 0 END)'
      ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM SplitIDs(''' || CAST(COALESCE(v_vcOrderStauts,'') AS VARCHAR) || ''','','')) THEN 1 ELSE 0 END)'
      END)
   ELSE ''
   END),(CASE 
   WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0
   THEN
      CASE 
      WHEN v_tintFilterBy = 1 --Item Classification
      THEN ' AND 1 = (CASE 
																				WHEN ' || CAST(COALESCE(v_bitIncludeSearch,false) AS VARCHAR) || ' = false
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM SplitIDs(''' || CAST(COALESCE(v_vcFilterValue,'') AS VARCHAR) || ''','','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM SplitIDs(''' || CAST(COALESCE(v_vcFilterValue,'') AS VARCHAR) || ''','','')) THEN 1 ELSE 0 END) 
																			END)'
      ELSE ''
      END
   ELSE ''
   END),(CASE
   WHEN v_numViewID = 1 THEN ' AND 1 = (CASE WHEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
   WHEN v_numViewID = 2 AND v_tintPackingViewMode=1 THEN ' AND COALESCE((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=' || CAST(COALESCE(v_numShippingServiceItemID,0) AS VARCHAR) || ' AND OIInner.numShipFromWarehouse=' || CAST(COALESCE(v_numWarehouseID,0) AS VARCHAR) || ' AND OIInner.ItemReleaseDate=COALESCE(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))),0) = 0 '
   WHEN v_numViewID = 2 AND v_tintPackingViewMode=2 THEN ' AND (COALESCE(OpportunityItems.numQtyPicked,0) - COALESCE(TEMPPacked.numPacked,0)  > 0 OR COALESCE(TEMPPacked.numPacked,0) - COALESCE(TEMPShipped.numShipped,0)  > 0)'
   WHEN v_numViewID = 3
   THEN ' AND 1 = (CASE 
						WHEN (CASE 
								WHEN ' || CAST(COALESCE(v_tintInvoicingType,0) AS VARCHAR) || ' =1 
								THEN COALESCE((SELECT 
													SUM(OpportunityBizDocItems.numUnitHour)
												FROM
													OpportunityBizDocs
												INNER JOIN
													OpportunityBizDocItems
												ON
													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
												WHERE
													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
													AND OpportunityBizDocs.numBizDocId=287),0)
								ELSE COALESCE(OpportunityItems.numUnitHour,0) 
							END) - COALESCE((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=287),0) > 0 
						THEN 1
						ELSE 0 
					END)'
   END),
   (CASE
   WHEN COALESCE(v_numViewID,0) = 1 THEN 'AND 1 = (CASE WHEN COALESCE(WareHouseItems.numWareHouseItemID,0) > 0 AND COALESCE(OpportunityItems.bitDropship,false) = false THEN 1 ELSE 0 END)'
   WHEN COALESCE(v_numViewID,0) = 2 THEN ' AND 1=1'
   WHEN COALESCE(v_numViewID,0) = 3 THEN ' AND 1=1'
   WHEN COALESCE(v_numViewID,0) = 4 THEN ' AND 1=1'
   WHEN COALESCE(v_numViewID,0) = 5 THEN ' AND 1=1'
   WHEN COALESCE(v_numViewID,0) = 6 THEN ' AND 1=1'
   ELSE ' AND 1=0'
   END),(CASE WHEN coalesce(v_numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
   ,(CASE WHEN COALESCE(v_numViewID,0) = 4 OR COALESCE(v_numViewID,0) = 6 
   THEN CONCAT(' AND OpportunityBizDocs.numBizDocID= ',(CASE 
   WHEN COALESCE(v_numViewID, 0) = 4 THEN 287 
   WHEN COALESCE(v_numViewID, 0) = 6 AND v_tintPrintBizDocViewMode = 1 THEN 55206 
   WHEN COALESCE(v_numViewID, 0) = 6 AND v_tintPrintBizDocViewMode = 2 THEN COALESCE(v_numDefaultSalesShippingDoc,0)
   WHEN COALESCE(v_numViewID, 0) = 6 AND v_tintPrintBizDocViewMode = 3 THEN 296 
   WHEN COALESCE(v_numViewID, 0) = 6 AND v_tintPrintBizDocViewMode = 4 THEN 287 
   ELSE 0
   END), ' AND ' || CAST(COALESCE(v_numViewID, 0) AS VARCHAR) || ' IN (4,6)')
   ELSE '' END)
   ,(CASE WHEN COALESCE(v_numViewID,0) = 4 THEN ' AND 1 = (CASE WHEN COALESCE(OpportunityBizDocs.monDealAmount,0) - COALESCE(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END),
   (CASE 
   WHEN COALESCE(v_numViewID,0) = 5 AND coalesce(v_tintPendingCloseFilter,0) = 2
   THEN 
      ' AND 1 = (CASE WHEN COALESCE(TEMpBizDoc296.FulFilledQty,0) = COALESCE(OpportunityItems.numUnitHour,0) AND COALESCE(TEMpBizDoc287.InvoicedQty,0) = COALESCE(OpportunityItems.numUnitHour,0) THEN 1 ELSE 0 END) '
   ELSE ''
   END),
   (CASE 
   WHEN coalesce(v_numShippingZone,0) > 0 
   THEN ' AND 1 = (CASE 
						WHEN COALESCE((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId::NUMERIC,OpportunityMaster.numDomainID::NUMERIC,2::SMALLINT)),0) > 0 
						THEN
							CASE 
								WHEN (SELECT COUNT(*) FROM State WHERE numDomainID=' || CAST(COALESCE(v_numDomainId,0) AS VARCHAR) || ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=' || CAST(COALESCE(v_numShippingZone,0) AS VARCHAR) || ') > 0 
								THEN 1
								ELSE 0
							END
						ELSE 0 
					END)'
   ELSE ''
   END),(CASE WHEN coalesce(v_bitGroupByOrder,false) = false THEN ' AND (OpportunityBizDocs.numOppBizDocsId IS NULL OR (OpportunityBizDocs.numOppBizDocsId IS NOT NULL AND OpportunityBizDocItems.numOppItemID IS NOT NULL))' ELSE '' END));

   v_vcWhere := CONCAT(v_vcWhere,' ',v_vcCustomSearchValue);

   v_vcGroupBy := (CASE 
   WHEN v_bitGroupByOrder = true 
   THEN CONCAT(' GROUP BY OpportunityMaster.numDomainID,OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,OpportunityMaster.numShippingService
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtReleaseDate
																	,OpportunityMaster.tintSource
																	,OpportunityMaster.tintSourceType
																	,OpportunityMaster.dtExpectedDate',(CASE WHEN v_numViewID = 2 AND v_tintPackingViewMode = 1 THEN ',OpportunityItems.ItemReleaseDate' ELSE '' END))
   ELSE '' 
   END);

   v_vcOrderBy := CONCAT(' ORDER BY ',(CASE v_vcSortColumn
   WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
   WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
   WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
   WHEN 'Item.numItemClassification' THEN 'GetListIemName(Item.numItemClassification)'
   WHEN 'Item.numItemGroup' THEN 'COALESCE((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
   WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
   WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
   WHEN 'OpportunityMaster.numStatus' THEN 'GetListIemName(OpportunityMaster.numStatus)'
   WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
   WHEN 'OpportunityMaster.numAssignedTo' THEN 'fn_GetContactName(OpportunityMaster.numAssignedTo)'
   WHEN 'OpportunityMaster.numRecOwner' THEN 'fn_GetContactName(OpportunityMaster.numRecOwner)'
   WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
   WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
   WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LENGTH(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
   WHEN 'OpportunityItems.numQtyPacked' THEN '(CASE WHEN COALESCE((SELECT 
																		SUM(OBIInner.numUnitHour)
																	FROM
																		OpportunityBizDocs OBInner
																	INNER JOIN
																		OpportunityBizDocItems OBIInner
																	ON
																		OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																	WHERE
																		OBInner.numOppId = OpportunityMaster.numOppID
																		AND OBInner.numBizDocId=' || CAST(COALESCE(v_numDefaultSalesShippingDoc,0) AS VARCHAR) || '
																		AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > COALESCE((SELECT 
																																					SUM(OBIInner.numUnitHour)
																																				FROM
																																					OpportunityBizDocs OBInner
																																				INNER JOIN
																																					OpportunityBizDocItems OBIInner
																																				ON
																																					OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																				WHERE
																																					OBInner.numOppId = OpportunityMaster.numOppID
																																					AND OBInner.numBizDocId=296
					 																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) THEN COALESCE((SELECT 
																																																										SUM(OBIInner.numUnitHour)
																																																									FROM
																																																										OpportunityBizDocs OBInner
																																																									INNER JOIN
																																																										OpportunityBizDocItems OBIInner
																																																									ON
																																																										OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																									WHERE
																																																										OBInner.numOppId = OpportunityMaster.numOppID
																																																										AND OBInner.numBizDocId=' || CAST(COALESCE(v_numDefaultSalesShippingDoc,0) AS VARCHAR) || '
					 																																																					AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) - COALESCE((SELECT 
																																																																														SUM(OBIInner.numUnitHour)
																																																																													FROM
																																																																														OpportunityBizDocs OBInner
																																																																													INNER JOIN
																																																																														OpportunityBizDocItems OBIInner
																																																																													ON
																																																																														OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																																													WHERE
																																																																														OBInner.numOppId = OpportunityMaster.numOppID
																																																																														AND OBInner.numBizDocId=296
					 																																																																									AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) ELSE 0 END)'
   WHEN 'OpportunityItems.numQtyPicked' THEN '(CASE 
													WHEN COALESCE(OpportunityItems.numQtyPicked,0) >= COALESCE(TEMPPacked.numPacked,0) 
													THEN COALESCE(OpportunityItems.numQtyPicked,0) - COALESCE(TEMPPacked.numPacked,0) 
													ELSE 0 
												END)'
   WHEN 'OpportunityItems.numQtyShipped' THEN 'COALESCE(TEMPShipped.numShipped),0)'
   WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
													WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 1
													THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyPicked,0)
													ELSE 0
												END)'
   WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
   WHEN 'OpportunityItems.vcInvoiced' THEN 'COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
   WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
   WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
   WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
   WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
   WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN COALESCE(TempPaid.monAmountPaid,0) >= COALESCE(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
   WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN COALESCE(OpportunityItems.bitRequiredWarehouseCorrection,false) = true THEN '''' ELSE Warehouses.vcWareHouse END)'
   WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
   ELSE 'OpportunityMaster.bintCreatedDate'
   END),' ',
   (CASE WHEN coalesce(v_vcSortOrder,'') <> '' THEN v_vcSortOrder ELSE 'ASC' END));

   v_vcOrderBy1 := CONCAT(' ORDER BY ',(CASE v_vcSortColumn
   WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP."vcCompanyNameOrig"' 
   WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP."charItemTypeOrig"=''P''  THEN ''Inventory Item''
															WHEN TEMP."charItemTypeOrig"=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP."charItemTypeOrig"=''S'' THEN ''Service''
															WHEN TEMP."charItemTypeOrig"=''A'' THEN ''Accessory''
														END)'
   WHEN 'Item.numBarCodeId' THEN 'TEMP."numBarCodeIdOrig"'
   WHEN 'Item.numItemClassification' THEN 'GetListIemName(TEMP."numItemClassificationOrig")'
   WHEN 'Item.numItemGroup' THEN 'COALESCE((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP."numItemGroupOrig"),'''')'
   WHEN 'Item.vcSKU' THEN 'TEMP."vcSKUOrig"'
   WHEN 'OpportunityItems.vcItemName' THEN 'TEMP."vcItemNameOrig"'
   WHEN 'OpportunityMaster.numStatus' THEN 'GetListIemName(TEMP."numStatusOrig")'
   WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP."vcPoppNameOrig"'
   WHEN 'OpportunityMaster.numAssignedTo' THEN 'fn_GetContactName(TEMP."numAssignedToOrig")'
   WHEN 'OpportunityMaster.numRecOwner' THEN 'fn_GetContactName(TEMP."numRecOwnerOrig")'
   WHEN 'WareHouseItems.vcLocation' THEN 'TEMP."vcLocationOrig"'
   WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP."monAmountPaidOrig"'
   WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LENGTH(TEMP."vcBizDocID") DESC, TEMP."vcBizDocID"'
   WHEN 'OpportunityItems.numQtyPacked' THEN 'TEMP."numQtyPackedOrig"'
   WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP."numQtyPickedOrig"'
   WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP."numQtyShippedOrig"'
   WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
													WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 1
													THEN COALESCE(TEMP."numUnitHourOrig",0) - COALESCE(TEMP.numQtyPickedOrig,0)				
													ELSE 0
												END)'
   WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP."numUnitHourOrig"'
   WHEN 'OpportunityItems.vcInvoiced' THEN 'COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP."numOppId" AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP."numOppItemID"),0)'
   WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP."dtExpectedDateOrig"'
   WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP."ItemReleaseDateOrig"'
   WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP."bintCreatedDateOrig"'
   WHEN 'OpportunityMaster.numAge' THEN 'TEMP."bintCreatedDateOrig"'
   WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN COALESCE(TEMP."monAmountPaidOrig",0) >= COALESCE(TEMP."monDealAmountOrig",0) THEN 1 ELSE 0 END)'
   WHEN 'Item.dtItemReceivedDate' THEN 'TEMP."dtItemReceivedDate"'
   WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN COALESCE(OpportunityItems.bitRequiredWarehouseCorrection,false) = true THEN '''' ELSE Warehouses.vcWareHouse END)'
   ELSE 'TEMP."bintCreatedDateOrig"'
   END),' ',(CASE WHEN coalesce(v_vcSortOrder,'') <> '' THEN v_vcSortOrder ELSE 'ASC' END));
   
   IF EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcOrigDbColumnName = 'bitPaidInFull') OR v_vcSortColumn = 'OpportunityMaster.bitPaidInFull' then
	
      v_vcText := ' LEFT JOIN LATERAL
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid ON true';
      v_vcFrom := CONCAT(v_vcFrom,v_vcText);
   end if;

   SELECT COUNT(*) INTO v_jCount FROM tt_TEMPFIELDSLEFT;

   WHILE v_j <= v_jCount LOOP
      select   numFieldID, vcFieldName, vcAssociatedControlType, (CASE WHEN coalesce(bitCustomField,false) = true THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END), vcLookBackTableName, coalesce(bitCustomField,false), coalesce(Grp_id,0), coalesce(bitIsNumeric,false), coalesce(vcListItemType,'') INTO v_numFieldId,v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName,
      v_vcLookBackTableName,v_bitCustomField,v_Grp_id,v_bitIsNumeric,v_vcListItemType FROM
      tt_TEMPFIELDSLEFT WHERE
      numPKID = v_j;
      IF v_vcDbColumnName NOT IN('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus',
      'vcPaymentStatus','numShipRate') then
		
         IF coalesce(v_bitCustomField,false) = false then
			
            IF (v_vcLookBackTableName = 'Item' OR v_vcLookBackTableName = 'WareHouseItems' OR (v_vcLookBackTableName = 'OpportunityItems' AND v_vcDbColumnName <> 'vcInventoryStatus') OR v_vcLookBackTableName = 'Warehouses') AND coalesce(v_bitGroupByOrder,false) = true then
				IF COALESCE(v_numViewID,0) = 2 AND COALESCE(v_tintPackingViewMode,0)=2 AND (v_vcDbColumnName = 'numQtyPicked' OR v_vcDbColumnName = 'numQtyPacked' OR v_vcDbColumnName = 'numQtyShipped') THEN
				   v_vcSelect := CONCAT(v_vcSelect,',CASE 
														WHEN NOT EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OpportunityMaster.numOppID AND COALESCE(OB.numBizDocId,0) = ',CASE WHEN v_vcDbColumnName = 'numQtyPicked' THEN 55206 WHEN v_vcDbColumnName = 'numQtyPacked' THEN coalesce(v_numDefaultSalesShippingDoc,0)  WHEN v_vcDbColumnName = 'numQtyShipped' THEN 296 ELSE 0 END,')				
														THEN ''''  
														WHEN EXISTS (SELECT 
																			X.numoppitemtCode
																	FROM 
																	(
																		SELECT
																			OI.numoppitemtCode,
																			COALESCE(OI.numUnitHour,0) AS OrderedQty,
																			SUM(OBDI.numUnitHour) AS PickedQty
																		FROM
																			OpportunityItems OI
																		LEFT JOIN
																			OpportunityBizDocs OB
																		ON
																			OB.numOppId = OpportunityMaster.numOppID		
																			AND COALESCE(OB.numBizDocId,0) = ',CASE WHEN v_vcDbColumnName = 'numQtyPicked' THEN 55206 WHEN v_vcDbColumnName = 'numQtyPacked' THEN coalesce(v_numDefaultSalesShippingDoc,0)  WHEN v_vcDbColumnName = 'numQtyShipped' THEN 296 ELSE 0 END,'
																		LEFT JOIN
																			OpportunityBizDocItems OBDI
																		ON
																			OBDI.numOppBizDocID = OB.numOppBizDocsId
																			AND OBDI.numOppItemID = OI.numoppitemtCode
																		WHERE
																			OI.numOppID = OpportunityMaster.numOppID
																			AND COALESCE(OI.numUnitHour,0) > 0
																		GROUP BY
																			OI.numoppitemtCode,
																			OI.numUnitHour
																	) X
																	WHERE
																		COALESCE(X.OrderedQty,0) > COALESCE(X.PickedQty,0)) 
																THEN ''#fcf8e3''  
																ELSE ''#DBF9DC''
															END "',v_vcDbColumnName,'"');
				ELSE
				   v_vcSelect := CONCAT(v_vcSelect,',',(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '''''' END),' "',v_vcDbColumnName,'"');
				END IF;

				v_vcGroupByInclude := CONCAT(v_vcGroupByInclude,',','TEMP."',v_vcDbColumnName,'"');
            ELSEIF v_vcDbColumnName = 'vcItemReleaseDate'
            then
				
               v_vcSelect := CONCAT(v_vcSelect,',',(CASE WHEN v_numViewID = 2 AND v_tintPackingViewMode = 1 THEN 'FormatedDateFromDate(COALESCE(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)),' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ')' ELSE 'FormatedDateFromDate(CAST(OpportunityMaster.dtReleaseDate AS DATE),' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ')' END),
               ' "',v_vcDbColumnName,'"');
            ELSEIF v_vcDbColumnName = 'vcInclusionDetails'
            then
				
               v_vcSelect := CONCAT(v_vcSelect,',GetOrderAssemblyKitInclusionDetails(OpportunityMaster.numOppID,OpportunityItems.numoppitemtCode,OpportunityItems.numUnitHour,',
               v_tintCommitAllocation,'::SMALLINT,false) "',v_vcDbColumnName,'"');
			ELSEIF v_vcDbColumnName = 'vcInventoryStatus'
            then
               v_vcSelect := CONCAT(v_vcSelect,',COALESCE(CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1::SMALLINT),'''') "',v_vcDbColumnName,'"');
            ELSE
               IF v_vcDbColumnName = 'numAge' then
					
                  v_vcSelect := CONCAT(v_vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24) = 0 AND (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24) = 0 AND ((DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) >= 4 AND (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24) = 0 AND ((DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) >= 8 AND (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24) = 0 AND ((DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) >= 12 AND (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now()))/24,''d '',DateDiff(''hour'',OpportunityMaster.bintCreatedDate,timezone(''utc'', now())) % 24,''h''),''</span>'')',' "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'dtItemReceivedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',FormatedDateFromDate(Item.dtItemReceivedDate,' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') "',
                  v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'bitPaidInFull'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN COALESCE(TempPaid.monAmountPaid,0) >= COALESCE(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) "',v_vcDbColumnName,
                  '"');
               ELSEIF v_vcDbColumnName = 'dtExpectedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',FormatedDateFromDate(OpportunityMaster.dtExpectedDate,' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'bintCreatedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' = 6 THEN FormatedDateFromDate(OpportunityBizDocs.dtCreatedDate + ( ' || CAST(COALESCE(v_ClientTimeZoneOffset,0) AS VARCHAR) || ' * -1 ' || ' || '' minutes'')::interval,' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') ELSE FormatedDateFromDate(OpportunityMaster.bintCreatedDate + ( ' || CAST(COALESCE(v_ClientTimeZoneOffset,0) AS VARCHAR) || ' * -1 ' || ' || '' minutes'')::interval,' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') END) "',
                  v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'numQtyPicked'
               then
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN COALESCE(OpportunityItems.numQtyPicked,0) > COALESCE(TEMPPacked.numPacked,0) THEN COALESCE(OpportunityItems.numQtyPicked,0) - COALESCE(TEMPPacked.numPacked,0) ELSE 0 END) "',v_vcDbColumnName,'"');
			   ELSEIF v_vcDbColumnName = 'numQtyPacked'
               then
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE 
														WHEN COALESCE(TEMPPacked.numPacked,0) > COALESCE(TEMPShipped.numShipped,0) 
														THEN COALESCE(TEMPPacked.numPacked,0) - COALESCE(TEMPShipped.numShipped,0) ELSE 0 END) "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'numQtyShipped'
               then
					v_vcSelect := CONCAT(v_vcSelect,',COALESCE((SELECT 
																	SUM(OBIInner.numUnitHour)
																FROM
																	OpportunityBizDocs OBInner
																INNER JOIN
																	OpportunityBizDocItems OBIInner
																ON
																	OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																WHERE
																	OBInner.numOppId = OpportunityMaster.numOppID
																	AND OBInner.numBizDocId=296
																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)  "',v_vcDbColumnName,'"');
			   ELSEIF v_vcDbColumnName = 'vcInvoiced'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'vcLocation'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE(WarehouseLocation.vcLocation,'''') "',v_vcDbColumnName,
                  '"');
               ELSEIF v_vcDbColumnName = 'charItemType'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN COALESCE(Item.bitAssembly,false) = true THEN ''Assembly''
																			WHEN COALESCE(Item.bitKitParent,false) = true THEN ''Kit''
																			WHEN COALESCE(Item.bitAsset,false) = true AND COALESCE(Item.bitRental,false) = false THEN ''Asset''
																			WHEN COALESCE(Item.bitAsset,false) = true AND COALESCE(Item.bitRental,false) = true THEN ''Rental Asset''
																			WHEN COALESCE(Item.bitSerialized,false) = true AND COALESCE(Item.bitAsset,false) = false  THEN ''Serialized''
																			WHEN COALESCE(Item.bitSerialized,false) = true AND COALESCE(Item.bitAsset,false) = true AND COALESCE(Item.bitRental,false) = false THEN ''Serialized Asset''
																			WHEN COALESCE(Item.bitSerialized,false) = true AND COALESCE(Item.bitAsset,false) = true AND COALESCE(Item.bitRental,false) = true THEN ''Serialized Rental Asset''
																			WHEN COALESCE(Item.bitLotNo,false) = true THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'vcItemReleaseDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ') "',COALESCE(v_vcDbColumnName,''),'"');
               ELSEIF v_vcDbColumnName = 'numStatus'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',GetListIemName(',v_vcLookBackTableName,'.',v_vcDbColumnName,
                  ') "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'tintSource'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',fn_GetOpportunitySourceValue(COALESCE(OpportunityMaster.tintSource,0)::NUMERIC,COALESCE(OpportunityMaster.tintSourceType,0)::SMALLINT,OpportunityMaster.numDomainID::NUMERIC) "',v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'vcWareHouse'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN COALESCE(OpportunityItems.bitRequiredWarehouseCorrection,false) = true THEN '''' ELSE COALESCE(Warehouses.vcWarehouse,''-'') END) "',
                  v_vcDbColumnName,'"');
               ELSEIF v_vcDbColumnName = 'intUsedShippingCompany'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',CONCAT(COALESCE(LDOpp.vcData,''-''),'', '',COALESCE(SSOpp.vcShipmentService,''-''))  "',v_vcDbColumnName,'"');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',LDOpp.vcData,SSOpp.vcShipmentService')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = ' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ' OR LDOpp.constFlag=true)
															AND COALESCE(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ' OR COALESCE(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID');
               ELSEIF v_vcDbColumnName = 'intShippingCompany'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',CONCAT(COALESCE(LDDiv.vcData,''-''),'', '',COALESCE(SSDiv.vcShipmentService,''-''))  "',v_vcDbColumnName,'"');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',LDDiv.vcData,SSDiv.vcShipmentService')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = ' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ' OR LDDiv.constFlag=true)
															AND COALESCE(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ' OR COALESCE(SSDiv.numDomainID,0)=0) 
															AND COALESCE(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID');
               ELSEIF v_vcAssociatedControlType = 'SelectBox'
               then
					
                  IF v_vcListItemType = 'LI' then
						
                     v_vcSelect := CONCAT(v_vcSelect,',L',SUBSTR(CAST(v_j AS VARCHAR(3)),1,3),'.vcData',' "',v_vcDbColumnName,'"');
                     v_vcFrom := CONCAT(v_vcFrom,' LEFT JOIN ListDetails L',v_j,' ON L',v_j,'.numListItemID=',v_vcLookBackTableName,'.',v_vcDbColumnName);                     
                  ELSEIF v_vcListItemType = 'IG'
                  then
						
                     v_vcSelect := CONCAT(v_vcSelect,',COALESCE((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' "',v_vcDbColumnName,'"');
                  ELSEIF v_vcListItemType = 'U'
                  then
						
                     v_vcSelect := CONCAT(v_vcSelect,',fn_GetContactName(',v_vcLookBackTableName,'.',v_vcDbColumnName,
                     ') "',v_vcDbColumnName,'"');
                  ELSE
                     v_vcSelect := CONCAT(v_vcSelect,',',(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '''''' END),
                     ' "',v_vcDbColumnName,'"');
                  end if;
               ELSEIF v_vcAssociatedControlType = 'DateField'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',FormatedDateFromDate(' || COALESCE(v_vcLookBackTableName, ''),'.',COALESCE(v_vcDbColumnName,''),
                  ' + ( ' || CAST(COALESCE(v_ClientTimeZoneOffset,0) AS VARCHAR) || ' * -1 ' || ' || '' minutes'')::interval',',', COALESCE(v_numDomainID, ''), ', "', COALESCE(v_vcDbColumnName, ''),'"');
               ELSEIF v_vcAssociatedControlType = 'TextBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE(',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN ',0' ELSE ',''''' END),
                  ') "',v_vcDbColumnName,
                  '"');
               ELSEIF v_vcAssociatedControlType = 'TextArea'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE(',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN ',0' ELSE ',''''' END),
                  ') "',v_vcDbColumnName,
                  '"');
               ELSEIF v_vcAssociatedControlType = 'Label'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE(',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN ',0' ELSE '''''' END),
                  ') "',v_vcDbColumnName,
                  '"');
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,',COALESCE(',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN ',0' ELSE '''''' END),
                  ') "',v_vcDbColumnName,
                  '"');
               end if;
            end if;
         ELSEIF v_bitCustomField = true
         then
			If coalesce(v_bitGroupByOrder,false) = true then
               v_vcGroupByInclude := CONCAT(v_vcGroupByInclude,',','TEMP."',v_vcDbColumnName,'"');
			END IF;


            IF v_Grp_id = 5 then
				
               IF coalesce(v_bitGroupByOrder,false) = true then
					
                  v_vcSelect := CONCAT(v_vcSelect,',''''',' "',v_vcDbColumnName,'"');
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,CONCAT(',GetCustFldValueOppItems(',v_numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' "',v_vcDbColumnName,
                  '"');
               end if;
            ELSE
               IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
					
                  v_vcSelect := CONCAT(v_vcSelect,',CFW',v_numFieldId,'.Fld_Value  "',v_vcDbColumnName,'"');
                  v_vcText := CONCAT(' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' ON CFW',v_numFieldId, 
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',CFW',v_numFieldId,'.Fld_Value')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,v_vcText);
               ELSEIF v_vcAssociatedControlType = 'CheckBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',case when COALESCE(CFW',v_numFieldId,'.Fld_Value,0)=0 then 0 when COALESCE(CFW', 
                  v_numFieldId,'.Fld_Value,0)=1 then 1 end   "',v_vcDbColumnName, 
                  '"');
                  v_vcText := CONCAT(' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' ON CFW',v_numFieldId,
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',CFW',v_numFieldId,'.Fld_Value')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,v_vcText);
               ELSEIF v_vcAssociatedControlType = 'DateField'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',FormatedDateFromDate(CFW',v_numFieldId,'.Fld_Value,', 
                  v_numDomainID,')  "',v_vcDbColumnName,'"');
                  v_vcText := CONCAT(' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW',v_numFieldId, 
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',CFW',v_numFieldId,'.Fld_Value')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,v_vcText);
               ELSEIF v_vcAssociatedControlType = 'SelectBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',L',v_numFieldId,'.vcData',' "',v_vcDbColumnName,'"');
                  v_vcText := CONCAT(' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW',v_numFieldId,
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID');
                  v_vcGroupBy :=(CASE
                  WHEN v_bitGroupByOrder = true
                  THEN CONCAT(v_vcGroupBy,',L',v_numFieldId,'.vcData')
                  ELSE ''
                  END);
                  v_vcFrom := CONCAT(v_vcFrom,v_vcText);
                  v_vcText := CONCAT(' left Join ListDetails L',v_numFieldId,' on L',v_numFieldId,'.numListItemID=(CASE WHEN isnumeric(CFW',v_numFieldId,'.Fld_Value) THEN CFW',v_numFieldId,'.Fld_Value ELSE ''0'' END)::NUMERIC');
                  v_vcFrom := CONCAT(v_vcFrom,v_vcText);
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,CONCAT(',GetCustFldValueOpp(',v_numFieldId,',OpportunityMaster.numOppID)'),' "',v_vcDbColumnName,'"');
               end if;
            end if;
         end if;
      end if;
      v_j := v_j::bigint+1;
   END LOOP;

   If v_numViewID = 2 then
      v_vcSelect := CONCAT(v_vcSelect,',',(CASE v_vcSortColumn
      WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS "vcCompanyNameOrig"'
      WHEN 'Item.charItemType' THEN 'Item.charItemType AS "charItemTypeOrig"'
      WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS "numBarCodeIdOrig"'
      WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS "numItemClassificationOrig"'
      WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS "numItemGroupOrig"'
      WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS "vcSKUOrig"'
      WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS "vcItemNameOrig"'
      WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS "numStatusOrig" '
      WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS "vcPoppNameOrig"'
      WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS "numAssignedToOrig"'
      WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS "numRecOwnerOrig"'
      WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS "vcLocationOrig"'
      WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS "monAmountPaidOrig"'
      WHEN 'OpportunityItems.numQtyPicked' THEN '(CASE WHEN COALESCE(OpportunityItems.numQtyPicked,0) > COALESCE(TEMPPacked.numPacked,0) THEN COALESCE(OpportunityItems.numQtyPicked,0) - COALESCE(TEMPPacked.numPacked,0) ELSE 0 END) AS "numQtyPickedOrig"'
	  WHEN 'OpportunityItems.numQtyPacked' THEN '(CASE 
														WHEN COALESCE(TEMPPacked.numPacked,0) >= COALESCE(TEMPShipped.numShipped,0) 
														THEN COALESCE(TEMPPacked.numPacked,0) - COALESCE(TEMPShipped.numShipped,0) 
													ELSE 0 
												END) AS "numQtyPackedOrig"'
      WHEN 'OpportunityItems.numQtyShipped' THEN 'COALESCE(TEMPShipped.numShipped,0) AS "numQtyShippedOrig"'
      WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS "numUnitHourOrig"'
      WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS "numUnitHourOrig"'
      WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS "dtExpectedDateOrig"'
      WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS "ItemReleaseDateOrig"'
      WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS "bintCreatedDateOrig"'
      WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS "bintCreatedDateOrig"'
      WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS "monAmountPaidOrig", OpportunityMaster.monDealAmount AS "monDealAmountOrig"'
      WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS "dtItemReceivedDateOrig"'
      WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN COALESCE(OpportunityItems.bitRequiredWarehouseCorrection,false) = true THEN '''' ELSE Warehouses.vcWareHouse END)'
      ELSE 'OpportunityMaster.bintCreatedDate AS "bintCreatedDateOrig"'
      END));

	  v_vcGroupBy1 := (CASE 
							WHEN v_bitGroupByOrder = true 
							THEN CONCAT(' GROUP BY TEMP."numOppID"
								,TEMP."numOppItemID"
								,TEMP."numRemainingQty"
								,TEMP."dtAnticipatedDelivery"
								,TEMP."dtShipByDate"
								,TEMP."vcShipStatus"
								,TEMP."vcPaymentStatus"
								,TEMP."numShipRate"
								,TEMP."vcLinkingBizDocs"
								,TEMP."bitRequiredWarehouseCorrection"
								,TEMP."numDivisionID"
								,TEMP."numTerID"
								,TEMP."numOppShipVia"
								,TEMP."numOppShipService"
								,TEMP."numPreferredShipVia"
								,TEMP."numPreferredShipService"
								,TEMP."numOppBizDocID"
								,TEMP."monAmountToPay"
								,TEMP."monAmountPaid"
								,TEMP."dtExpectedDateOrig"
								,TEMP."vcBizDocID"
								,TEMP."bintCreatedDateOrig"
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'CompanyInfo' AND vcOrigDbColumnName = 'vcCompanyName') THEN ',TEMP."vcCompanyName"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'vcPoppName') THEN ',TEMP."vcPoppName"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'numAssignedTo') THEN ',TEMP."numAssignedTo"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'numRecOwner') THEN ',TEMP."numRecOwner"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'numStatus') THEN ',TEMP."numStatus"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'bintCreatedDate') THEN ',TEMP."bintCreatedDate"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'txtComments') THEN ',TEMP."txtComments"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityBizDocs' AND vcOrigDbColumnName = 'vcBizDocID') THEN ',TEMP."vcBizDocID"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'intUsedShippingCompany') THEN ',TEMP."intUsedShippingCompany"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'DivisionMaster' AND vcOrigDbColumnName = 'intShippingCompany') THEN ',TEMP."intShippingCompany"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityBizDocs' AND vcOrigDbColumnName = 'dtCreatedDate') THEN ',TEMP."dtCreatedDate"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityMaster' AND vcOrigDbColumnName = 'dtExpectedDate') THEN ',TEMP."dtExpectedDate"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'OpportunityItems' AND vcOrigDbColumnName = 'vcInventoryStatus') THEN ',TEMP."vcInventoryStatus"' ELSE '' END),'
								',(CASE WHEN EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcLookBackTableName = 'Item' AND vcOrigDbColumnName = 'dtItemReceivedDate') THEN ',TEMP."dtItemReceivedDateOrig"' ELSE '' END),v_vcGroupByInclude)
					   ELSE '' 
					END);
	  
      
	  v_vcFinal := CONCAT(v_vcSelect,v_vcFrom,v_vcWhere,v_vcGroupBy,v_vcOrderBy,' OFFSET ((' || CAST(COALESCE(v_numPageIndex,0) AS VARCHAR) || '-1) * 200) ROWS FETCH NEXT 200 ROWS ONLY ');
	  --v_vcFinal := CONCAT('SELECT COUNT(*) OVER() AS "TotalRecords", * FROM (',v_vcSelect,v_vcFrom,v_vcWhere,') TEMP',v_vcGroupBy1,v_vcOrderBy1,' OFFSET ((' || CAST(COALESCE(v_numPageIndex,1) AS VARCHAR) || '-1) * 200) ROWS FETCH NEXT 200 ROWS ONLY ');
   ELSE
      v_vcFinal := CONCAT(v_vcSelect,v_vcFrom,v_vcWhere,v_vcGroupBy,v_vcOrderBy,' OFFSET ((' || CAST(COALESCE(v_numPageIndex,0) AS VARCHAR) || '-1) * 200) ROWS FETCH NEXT 200 ROWS ONLY ');
   end if;

   RAISE NOTICE '%',CAST(v_vcFinal AS TEXT);
   
   OPEN SWV_RefCur FOR 
   EXECUTE v_vcFinal;
      
   OPEN SWV_RefCur2 FOR
   SELECT 
		numPKID AS "numPKID",
		ID AS "ID",
		numFieldID AS "numFieldID",
		vcFieldName AS "vcFieldName",
		vcOrigDbColumnName AS "vcOrigDbColumnName",
		vcListItemType AS "vcListItemType",
		numListID AS "numListID",
		bitAllowSorting AS "bitAllowSorting",
		bitAllowFiltering AS "bitAllowFiltering",
		vcAssociatedControlType AS "vcAssociatedControlType",
		vcLookBackTableName AS "vcLookBackTableName",
		bitCustomField AS "bitCustomField",
		intRowNum AS "intRowNum",
		(CASE WHEN COALESCE(intColumnWidth,0) = -1 THEN (100 + COALESCE(intRowNum,0)) ELSE intColumnWidth END) AS "intColumnWidth",
		bitIsRequired AS "bitIsRequired",
		bitIsEmail AS "bitIsEmail",
		bitIsAlphaNumeric AS "bitIsAlphaNumeric",
		bitIsNumeric AS "bitIsNumeric",
		bitIsLengthValidation AS "bitIsLengthValidation",
		intMaxLength AS "intMaxLength",
		intMinLength AS "intMinLength",
		bitFieldMessage AS "bitFieldMessage",
		vcFieldMessage AS "vcFieldMessage",
		Grp_id AS "Grp_id"
	FROM tt_TEMPFIELDSLEFT ORDER BY intRowNum;
END; 
$$;
