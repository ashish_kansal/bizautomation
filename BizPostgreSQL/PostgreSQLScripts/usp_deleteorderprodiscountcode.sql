CREATE OR REPLACE FUNCTION USP_DeleteOrderProDiscountCode(v_numProId NUMERIC(9,0) DEFAULT 0,   
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numDiscountId NUMERIC(9,0) DEFAULT NULL,
	v_numOppId NUMERIC(9,0) DEFAULT NULL,
	v_numDivisionId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
	DECLARE
	v_intDiscountUsage  INTEGER;
	v_numDiscountLineItem  NUMERIC;
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
DELETE FROM DiscountCodes
      WHERE numDiscountId = v_numDiscountId AND numPromotionID = v_numProId;
      SELECT COUNT(intCodeUsed) INTO v_intDiscountUsage FROM DiscountCodeUsage WHERE numDivisionId = v_numDivisionId AND numDiscountId = v_numDiscountId;
      IF v_intDiscountUsage > 0 then
	   
         UPDATE DiscountCodeUsage
         SET intCodeUsed = v_intDiscountUsage::bigint -1
         WHERE numDivisionId = v_numDivisionId AND numDiscountId = v_numDiscountId;
      end if;
      UPDATE OpportunityMaster
      SET numDiscountID = NULL
      WHERE numOppId = v_numOppId AND numDivisionId = v_numDivisionId AND numDiscountID = v_numDiscountId AND numDomainId = v_numDomainID;
      SELECT coalesce(numShippingServiceItemID,0) INTO v_numDiscountLineItem FROM Domain WHERE numDomainId = v_numDomainID;
      DELETE FROM OpportunityItems
      WHERE numItemCode = v_numDiscountLineItem AND numOppId = v_numOppId;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;
      
  


