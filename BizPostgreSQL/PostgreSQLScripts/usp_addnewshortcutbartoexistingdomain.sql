-- Stored procedure definition script USP_AddNewShortCutBarToExistingDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddNewShortCutBarToExistingDomain(v_vcLinkName VARCHAR(2000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO ShortCutGrpConf
   SELECT AGM.numGroupID,SCB.ID,SCB."order",SCB.bitType,D.numDomainId,SCB.numTabId,false,0,false FROM Domain D JOIN AuthenticationGroupMaster AGM ON D.numDomainId = AGM.numDomainID,ShortCutBar SCB
   WHERE SCB.vcLinkName IN(SELECT Items FROM Split(v_vcLinkName,',')); 


   INSERT INTO ShortCutUsrCnf
   SELECT UM.numGroupID,SCB.ID,SCB."order",SCB.bitType,D.numDomainId,UM.numUserDetailId,SCB.numTabId,false,0,false FROM Domain D JOIN UserMaster UM ON D.numDomainId = UM.numDomainID,ShortCutBar SCB
   WHERE SCB.vcLinkName IN(SELECT Items FROM Split(v_vcLinkName,','));


RETURN;
END; $$;


/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/



