-- Stored procedure definition script usp_GetCaseHistoryForAccount for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCaseHistoryForAccount(v_numCompanyId NUMERIC(9,0),
	--@numOrderby numeric=0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0
	--@numUserId numeric(9)=0,
	--@tintUserRightType tinyint    --For Security
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	--@numCreatedBy numeric(9)=0,
	
	--Get teh Territory ID for the User.
	--DECLARE @numTerritoryID numeric
	--SELECT @numTerritoryID=numTerritoryID FROM UserMaster WHERE numUserId=@numUserId

--IF @numCreatedby=0 and @tintUserRightType=1 
-- Means that sorting is not passed, but has got rights to view only his records
	
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   AdditionalContactsInformation.vcFirstName,
			AdditionalContactsInformation.vcLastname,
			cast(CompanyInfo.vcCompanyName as VARCHAR(255)),
			cast(Cases.numCaseId as VARCHAR(255)),
			cast(Cases.vcCaseNumber as VARCHAR(255)),
			cast(Cases.intTargetResolveDate as VARCHAR(255)),
			cast(Cases.numCreatedby as VARCHAR(255)),
			cast(Cases.textSubject as VARCHAR(255)),
			cast(Cases.numPriority as VARCHAR(255)),
			cast(Cases.numCreatedby as VARCHAR(255)),
			cast(Cases.textDesc as VARCHAR(255)),
			cast((Select cast(vcData as VARCHAR(50)) FROM Listdetails WHERE numListItemID = Cases.numStatus) as VARCHAR(255)) as vcStatus,
			cast(CompanyInfo.numCompanyId as VARCHAR(255)),
			DivisionMaster.numDivisionID,
			DivisionMaster.numTerID,
			AdditionalContactsInformation.numContactId
   FROM
   AdditionalContactsInformation INNER JOIN Cases
   ON AdditionalContactsInformation.numContactId =
   Cases.numContactId INNER JOIN DivisionMaster
   ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   Where
   Cases.numStatus = 136 AND --to avoid the closed cases
   AdditionalContactsInformation.numDomainID = v_numDomainID
	--AND Cases.numCreatedBy = @numUserId 
   and CompanyInfo.numCompanyId = v_numCompanyId;
	--COALESCE(@vccompanyname,CompanyInfo.vcCompanyName) 
	--ORDER BY
	--CASE
		--WHEN @numOrderby=3 THEN AdditionalContactsInformation.numContactId
		--WHEN @numOrderby=1 THEN Cases.vcPriority 
		--WHEN @numOrderby=2 THEN intTargetResolveDate
		--WHEN @numOrderby=0 THEN intTargetResolveDate
END; $$;  --End of the Case












