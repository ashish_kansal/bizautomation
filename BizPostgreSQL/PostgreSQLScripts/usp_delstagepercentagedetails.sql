-- Stored procedure definition script usp_DelStagePercentageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DelStagePercentageDetails(v_slpid NUMERIC     
--    
    
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE from StagePercentageDetails where slp_id = v_slpid;
   RETURN;
END; $$;


