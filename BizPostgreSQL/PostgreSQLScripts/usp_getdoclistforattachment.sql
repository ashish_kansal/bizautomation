-- Stored procedure definition script USP_GetDocListForAttachment for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDocListForAttachment(v_numDomainID NUMERIC(9,0),
    v_vcInput VARCHAR(20),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~G' AS DocID,
                vcDocName || ' (' || coalesce(vcData,'') || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments
   INNER JOIN Listdetails ON numListItemID = numDocCategory
   WHERE   GenericDocuments.numDomainId = v_numDomainID
   AND LENGTH(VcFileName) > 1
   AND numListItemID <> 369
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%'
   UNION ALL
   SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~S' AS DocID,
                coalesce(vcDocName,'NA') || ' (' || coalesce(OM.vcpOppName,'')
   || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments SD
   LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = SD.numRecID
   WHERE   SD.numDomainId = v_numDomainID
   AND SD.vcDocumentSection = 'O'
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%'
   UNION ALL
   SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~S' AS DocID,
                coalesce(vcDocName,'NA') || ' (' || coalesce(PM.vcProjectName,'') || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments SD
   LEFT OUTER JOIN ProjectsMaster PM ON PM.numProId = SD.numRecID
   WHERE   SD.numDomainId = v_numDomainID
   AND SD.vcDocumentSection = 'P'
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%'
   UNION ALL
   SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~S' AS DocID,
                coalesce(vcDocName,'NA') || ' (' || coalesce(C.vcFirstName,'')
   || ' ' || coalesce(vcLastname,'') || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments SD
   LEFT OUTER JOIN AdditionalContactsInformation C ON C.numContactId = SD.numRecID
   WHERE   SD.numDomainId = v_numDomainID
   AND SD.vcDocumentSection = 'C'
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%'
   UNION ALL
   SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~S' AS DocID,
                coalesce(vcDocName,'NA') || ' (' || coalesce(C.vcCompanyName,'') || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments SD
   LEFT OUTER JOIN DivisionMaster D ON D.numDivisionID = SD.numRecID
   left JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
   WHERE   SD.numDomainId = v_numDomainID
   AND SD.vcDocumentSection = 'A'--Prospect
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%'
   UNION ALL
   SELECT  CAST(numGenericDocID AS VARCHAR(30)) || '~S' AS DocID,
                coalesce(vcDocName,'NA') || ' (' || coalesce(C.vcCompanyName,'') || ')' AS vcDocName,
                cast(VcFileName as VARCHAR(255))
   FROM    GenericDocuments SD
   LEFT OUTER JOIN DivisionMaster D ON D.numDivisionID = SD.numRecID
   left JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
   WHERE   SD.numDomainId = v_numDomainID
   AND SD.vcDocumentSection = 'S'--Prospect
   AND vcDocName ilike '%' || coalesce(v_vcInput,'') || '%';
END; $$;

--SELECT * FROM [AdditionalContactsInformation] WHERE [numContactId]=85839
--SELECT dbo.[fn_GetComapnyName](55882)* FROM [DivisionMaster] WHERE numdivisionID =55881
--SELECT * FROM [CompanyInfo] WHERE numcompanyid=57111













