-- Stored procedure definition script USP_ManageBroadcast for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBroadcast(v_numBroadcastId NUMERIC(9,0) DEFAULT 0 ,   
v_vcBroadCastName VARCHAR(500) DEFAULT NULL,    
v_numEmailTemplateID NUMERIC(9,0) DEFAULT NULL,    
v_vcSubject VARCHAR(500) DEFAULT NULL,    
v_txtMessage TEXT DEFAULT NULL ,  
v_txtBizMessage TEXT DEFAULT NULL,  
v_numTotalRecipients NUMERIC(9,0) DEFAULT NULL,    
v_numTotalSussessfull NUMERIC(9,0) DEFAULT 0,    
v_numUserCntID NUMERIC(9,0) DEFAULT NULL,    
v_numDomainID NUMERIC(9,0) DEFAULT NULL,    
v_strBroadCast TEXT DEFAULT NULL,
v_bitBroadcasted BOOLEAN DEFAULT NULL,
v_numSearchID NUMERIC(9,0) DEFAULT NULL,
v_numConfigurationId NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM Broadcast WHERE numBroadCastId = v_numBroadcastId) then

      update Broadcast
      set vcBroadCastName = v_vcBroadCastName,numEmailTemplateID = v_numEmailTemplateID,
      vcSubject = v_vcSubject,txtMessage = v_txtMessage,txtBizMessage = v_txtBizMessage,
      numTotalRecipients = v_numTotalRecipients,numTotalSussessfull = v_numTotalSussessfull,
      bintBroadCastDate = TIMEZONE('UTC',now()),
      numBroadCastBy = v_numUserCntID,numDomainID = v_numDomainID,bitBroadcasted = v_bitBroadcasted,
      numSearchID = v_numSearchID,numConfigurationID = v_numConfigurationId
      where
      numBroadCastId = v_numBroadcastId;
      IF v_bitBroadcasted = true then
    
         update BroadCastDtls set tintSucessfull = 1 where numContactID in(select BCD.numContactID
         from
         Broadcast BC left join BroadCastDtls BCD on BC.numBroadCastId = BCD.numBroadcastID
         left join AdditionalContactsInformation AI on BCD.numContactID = AI.numContactId
         where BC.numBroadCastId = v_numBroadcastId and coalesce(AI.vcEmail,'') <> '') and numBroadcastID = v_numBroadcastId;
      end if;
      open SWV_RefCur for
      select v_numBroadcastId;
   ELSE
      insert into Broadcast(vcBroadCastName,
               numEmailTemplateID,
               vcSubject,
               txtMessage,
               txtBizMessage ,
               numTotalRecipients,
               numTotalSussessfull,
               bintBroadCastDate,
               numBroadCastBy,
               numDomainID,
               bitBroadcasted ,
               numSearchID   ,
               numConfigurationID)
         values(v_vcBroadCastName,
               v_numEmailTemplateID,
               v_vcSubject,
               v_txtMessage,
               v_txtBizMessage ,
               v_numTotalRecipients,
               v_numTotalSussessfull,
               TIMEZONE('UTC',now()),
               v_numUserCntID,
               v_numDomainID,
               v_bitBroadcasted ,
               v_numSearchID  ,
               v_numConfigurationId);
              
      open SWV_RefCur for
      select CURRVAL('Broadcast_seq');
   end if;
   RETURN;
END; $$; 



