CREATE OR REPLACE FUNCTION CFW_FLD_Values_OPP_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
DECLARE 
	v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
BEGIN
	IF (TG_OP = 'DELETE') THEN
		SELECT numDomainID,numModifiedBy INTO v_numDomainID,v_numUserCntID  FROM OpportunityMaster WHERE numOppID=OLD.RecId;
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.RecId;
	ELSIF (TG_OP = 'UPDATE') THEN
		SELECT numDomainID,numModifiedBy INTO v_numDomainID,v_numUserCntID  FROM OpportunityMaster WHERE numOppID=NEW.RecId;
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.RecId;

		IF OLD.FldDTLID <> NEW.FldDTLID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'FldDTLID,');
		END IF;
		IF OLD.Fld_ID <> NEW.Fld_ID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'Fld_ID,');
		END IF;
		IF OLD.Fld_Value <> NEW.Fld_Value THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'Fld_Value,');
		END IF;
		IF OLD.RecId <> NEW.RecId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'RecId,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');
	ELSIF (TG_OP = 'INSERT') THEN
		SELECT numDomainID,numCreatedBy INTO v_numDomainID,v_numUserCntID  FROM OpportunityMaster WHERE numOppID=NEW.RecId;
		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.RecId;
	END IF;

	 PERFORM USP_ManageWorkFlowQueueCF(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 70,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER CFW_FLD_Values_OPP_IUD AFTER INSERT OR UPDATE OR DELETE ON CFW_FLD_Values_OPP FOR EACH ROW EXECUTE PROCEDURE CFW_FLD_Values_OPP_IUD_TrFunc();


