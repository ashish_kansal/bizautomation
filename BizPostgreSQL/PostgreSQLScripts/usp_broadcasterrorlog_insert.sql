-- Stored procedure definition script USP_BroadcastErrorLog_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_BroadcastErrorLog_Insert(v_numBroadCastDtlID NUMERIC(18,0),  
 v_vcType VARCHAR(100),  
 v_vcMessage TEXT,
 v_vcStackStrace TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
  


   INSERT INTO BroadcastErrorLog(numBroadcastDtlID,
	vcType,
	vcMessage,
	vcStackStrace)
VALUES(v_numBroadCastDtlID,
	v_vcType,
	v_vcMessage,
	v_vcStackStrace);
RETURN;
END; $$;  



