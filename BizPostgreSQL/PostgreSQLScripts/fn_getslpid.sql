-- Function definition script fn_GetSlpId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetSlpId(v_numOppId NUMERIC)
RETURNS NUMERIC LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retval  NUMERIC;
BEGIN
   select   slp_id INTO v_retval from StagePercentageDetails INNER JOIN OpportunityStageDetails ON StagePercentageDetails.numStageDetailsId =  OpportunityStageDetails.numOppstageid where StagePercentageDetails.numoppid = v_numOppId;
 
   Return v_retval;
END; $$;

