-- Stored procedure definition script USP_SortItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SortItemList(v_xmlStr TEXT DEFAULT ''  ,  
 v_ListId NUMERIC DEFAULT NULL,   
 v_numDomainID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	if SUBSTR(CAST(v_xmlStr AS VARCHAR(10)),1,10) <> '' then
		delete FROM listorder  where numDomainId = v_numDomainID and numListId = v_ListId;

		INSERT INTO listorder
		(
			numListId
			,numListItemID
			,numDomainId
			,intSortOrder
		)
		SELECT
			v_ListId,
			unnest(xpath('//numListItemID/text()', v_xmlStr::xml))::text::numeric as numListItemID,
			unnest(xpath('//numDomainID/text()', v_xmlStr::xml))::text::numeric as numDomainID,
			unnest(xpath('//tintOrder/text()', v_xmlStr::xml))::text::integer as tintOrder;
			
	end if;
   RETURN;
END; $$;


