-- Stored procedure definition script usp_GetDefaultRuleId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDefaultRuleId(v_numDomainId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql                
--This procedure will return all the Default Routing Rules details      
   AS $$
   DECLARE
   v_numRoutId  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numRoutId := 0;
   select   numRoutID INTO v_numRoutId FROM RoutingLeads where bitDefault = true
   AND numDomainID = v_numDomainId    LIMIT 1;  

   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   If SWV_RowCount = 0 then
      open SWV_RefCur for
      SELECT 0 AS numRoutId;
   ELSE
      open SWV_RefCur for
      SELECT v_numRoutId AS numRoutId;
   end if;
   RETURN;
END; $$;


