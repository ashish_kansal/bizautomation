-- Stored procedure definition script USP_ValidateCustomizeLoginScreenURL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ValidateCustomizeLoginScreenURL(v_numDomainID NUMERIC DEFAULT 0,
v_vcLoginURL VARCHAR(500) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(LENGTH(v_vcLoginURL) > 0) then
	
      IF(v_numDomainID > 0) then
		
         open SWV_RefCur for
         SELECT
         coalesce(vcThemeClass,'1473b4') AS vcThemeClass,
			coalesce(vcLogoForLoginBizTheme,'') AS vcLogoForLoginBizTheme,
			coalesce(bitLogoAppliedToLoginBizTheme,false) AS bitLogoAppliedToLoginBizTheme,
			numDomainId
         FROM
         Domain
         WHERE
         vcLoginURL = v_vcLoginURL AND numDomainId <> v_numDomainID;
      ELSE
         open SWV_RefCur for
         SELECT
         coalesce(vcThemeClass,'1473b4') AS vcThemeClass,
			coalesce(vcLogoForLoginBizTheme,'') AS vcLogoForLoginBizTheme,
			coalesce(bitLogoAppliedToLoginBizTheme,false) AS bitLogoAppliedToLoginBizTheme,
			numDomainId
         FROM
         Domain
         WHERE
         vcLoginURL = v_vcLoginURL;
      end if;
   end if;
   RETURN;
END; $$;


