-- Stored procedure definition script USP_MANAGE_ORDER_STATUS for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MANAGE_ORDER_STATUS(v_vcOrderStatus			VARCHAR(100) DEFAULT '',
	v_numErrorStatus			NUMERIC(10,0) DEFAULT 0,
	v_numSuccessStatus		NUMERIC(10,0) DEFAULT 0,
	v_numReadyToShipStatus	NUMERIC(10,0) DEFAULT 0,
	v_numDomainID			NUMERIC(18,0) DEFAULT NULL,
	v_intMode				INTEGER DEFAULT NULL,
	v_numBizDocType			NUMERIC(10,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intMode = 0 then
		
      DELETE FROM OpportunityOrderStatus WHERE numDomainID = v_numDomainID;
      INSERT INTO OpportunityOrderStatus(vcOrderStatus,
				numErrorStatus,
				numSuccessStatus,
				numReadyToShipStatus,
				numDomainID,
				numBizDocType) VALUES( 
				/* vcOrderStatus - varchar(100) */ v_vcOrderStatus,
				/* numErrorStatus - numeric(10, 0) */ v_numErrorStatus,
				/* numSuccessStatus - numeric(10, 0) */ v_numSuccessStatus,
				/* numReadyToShipStatus - numeric(10, 0) */ v_numReadyToShipStatus,
				/* numDomainID - numeric(18, 0) */ v_numDomainID,
				/* numBizDocType - numeric(10, 0) */ v_numBizDocType);
   end if;
	
   IF v_intMode = 1 then
		
      open SWV_RefCur for SELECT  vcOrderStatus,
					coalesce(numErrorStatus,0) AS numErrorStatus,
					coalesce(numSuccessStatus,0) AS numSuccessStatus,
					coalesce(numReadyToShipStatus,0) AS numReadyToShipStatus,
					coalesce(numDomainID,0) AS numDomainID,
					coalesce(numBizDocType,0) AS numBizDocType
      FROM OpportunityOrderStatus WHERE numDomainID = v_numDomainID;
   end if;
END; $$;












