-- Stored procedure definition script usp_ValidateOppSerializLot for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ValidateOppSerializLot(v_numOppID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   
--    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(oppI.numQty)=SUM(opp.numUnitHour) THEN 0 ELSE 1 END Error
--FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode left JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=Opp.numOppId)
-- LEFT JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID
--where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
   AS $$
   DECLARE
   v_numOppType  SMALLINT;
   v_numOppStatus  SMALLINT;
   v_bitStockTransfer  BOOLEAN;
BEGIN
   select   tintopptype, tintoppstatus, bitStockTransfer INTO v_numOppType,v_numOppStatus,v_bitStockTransfer FROM OpportunityMaster WHERE numOppId =  v_numOppID;

--Validate serial number in case of sales order and sotck transfer only
   If (v_numOppType = 1 AND v_numOppStatus = 1) OR v_bitStockTransfer = true then

      open SWV_RefCur for
      SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(opp.numUnitHour) =(SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode = oppI.numOppItemID AND oppI.numOppID = v_numOppID) THEN 0 ELSE 1 END AS Error
      FROM OpportunityItems opp JOIN Item i ON i.numItemCode = opp.numItemCode
      where opp.numOppId = v_numOppID AND (coalesce(i.bitSerialized,false) = true OR coalesce(i.bitLotNo,false) = true) GROUP BY numoppitemtCode,opp.vcItemName;
   ELSEIF (v_numOppType = 2 AND v_numOppStatus = 1)
   then
      open SWV_RefCur for
      SELECT
      numoppitemtCode,
		opp.vcItemName,
		CASE
      WHEN SUM(opp.numUnitHour) =(SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode = oppI.numOppItemID AND oppI.numOppID = v_numOppID)
      THEN 0
      ELSE 1
      END AS Error
      FROM
      OpportunityItems opp
      JOIN
      Item i
      ON
      i.numItemCode = opp.numItemCode
      WHERE
      opp.numOppId = v_numOppID
      AND (coalesce(i.bitSerialized,false) = true OR coalesce(i.bitLotNo,false) = true)
      GROUP BY
      numoppitemtCode,opp.vcItemName;
   ELSE
      open SWV_RefCur for
      SELECT
      numoppitemtCode,
		opp.vcItemName,
		0 AS Error
      FROM
      OpportunityItems opp
      JOIN
      Item i
      ON
      i.numItemCode = opp.numItemCode
      WHERE
      opp.numOppId = v_numOppID AND
		(coalesce(i.bitSerialized,false) = true OR coalesce(i.bitLotNo,false) = true)
      GROUP BY
      numoppitemtCode,opp.vcItemName;
   end if;
   RETURN;
END; $$;


