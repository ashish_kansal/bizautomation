-- Stored procedure definition script USP_ManageReportListMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageReportListMaster(INOUT v_numReportID NUMERIC(18,0) ,
    v_vcReportName VARCHAR(200),
    v_vcReportDescription VARCHAR(500),
    v_numDomainID NUMERIC(18,0),
    v_numReportModuleID NUMERIC(18,0),
    v_numReportModuleGroupID NUMERIC(18,0),
    v_tintReportType SMALLINT,
    v_numUserCntID NUMERIC(18,0),
    v_bitActive BOOLEAN,
    v_textQuery TEXT,
    v_numSortColumnmReportFieldGroupID NUMERIC(18,0),
    v_numSortColumnFieldID NUMERIC(18,0),
    v_vcSortColumnDirection VARCHAR(10),
    v_bitSortColumnCustom BOOLEAN,
    v_vcFilterLogic VARCHAR(200),
    v_numDateReportFieldGroupID NUMERIC(18,0),
    v_numDateFieldID NUMERIC(18,0),
    v_bitDateFieldColumnCustom BOOLEAN,
    v_vcDateFieldValue VARCHAR(50),
    v_dtFromDate TIMESTAMP,
    v_dtToDate TIMESTAMP,
    v_tintRecordFilter SMALLINT,
    v_strText TEXT DEFAULT '',
    v_intNoRows INTEGER DEFAULT NULL,
    v_vcKPIMeasureFieldID VARCHAR(50) DEFAULT NULL,
    v_bitDuplicate BOOLEAN DEFAULT false,
    v_bitHideSummaryDetail BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOLDReportID  NUMERIC(18,0);
   v_numGroupId  NUMERIC(18,0);
   v_hDocItem  INTEGER;
BEGIN
   IF v_bitDuplicate = true then
      INSERT INTO ReportListMaster(vcReportName, vcReportDescription, numDomainID, numReportModuleID, numReportModuleGroupID, tintReportType, numCreatedBy, numModifiedBy, dtCreatedDate, dtModifiedDate, bitActive, textQuery, numSortColumnmReportFieldGroupID, numSortColumnFieldID, vcSortColumnDirection, bitSortColumnCustom, vcFilterLogic, numDateReportFieldGroupID, numDateFieldID, bitDateFieldColumnCustom, vcDateFieldValue, dtFromDate, dtToDate, tintRecordFilter, vcKPIMeasureFieldID, intNoRows,bitHideSummaryDetail)
      SELECT 'Duplicate - ' || vcReportName, vcReportDescription, numDomainID, numReportModuleID, numReportModuleGroupID, tintReportType, v_numUserCntID, v_numUserCntID, TIMEZONE('UTC',now()), TIMEZONE('UTC',now()), bitActive, textQuery, numSortColumnmReportFieldGroupID, numSortColumnFieldID, vcSortColumnDirection, bitSortColumnCustom, vcFilterLogic, numDateReportFieldGroupID, numDateFieldID, bitDateFieldColumnCustom, vcDateFieldValue, dtFromDate, dtToDate, tintRecordFilter, vcKPIMeasureFieldID, intNoRows,bitHideSummaryDetail
      FROM ReportListMaster WHERE numReportID = v_numReportID AND numDomainID = v_numDomainID;
      v_numOLDReportID := v_numReportID;
      v_numReportID := CURRVAL('ReportListMaster_seq');
      INSERT INTO ReportFieldsList(numReportID, numFieldID, bitCustom, numReportFieldGroupID)
      SELECT v_numReportID, numFieldID, bitCustom, numReportFieldGroupID
      FROM ReportFieldsList WHERE numReportID = v_numOLDReportID;
      INSERT INTO ReportFilterList(numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID)
      SELECT v_numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
      FROM ReportFilterList WHERE numReportID = v_numOLDReportID;
      INSERT INTO ReportSummaryGroupList(numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID)
      SELECT v_numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
      FROM ReportSummaryGroupList WHERE numReportID = v_numOLDReportID;
      INSERT INTO ReportMatrixAggregateList(numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID)
      SELECT v_numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
      FROM ReportMatrixAggregateList WHERE numReportID = v_numOLDReportID;
      INSERT INTO ReportKPIThresold(numReportID,vcThresoldType,decValue1,decValue2,decGoalValue)
      SELECT v_numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
      FROM ReportKPIThresold WHERE numReportID = v_numOLDReportID;
   ELSE
      IF v_numSortColumnmReportFieldGroupID = 0 then 
         v_numSortColumnmReportFieldGroupID := NULL;
      end if;
      IF v_numSortColumnFieldID = 0 then 
         v_numSortColumnFieldID := NULL;
      end if;
      IF v_numDateReportFieldGroupID = 0 then 
         v_numDateReportFieldGroupID := NULL;
      end if;
      IF v_numDateFieldID = 0 then 
         v_numDateFieldID := NULL;
      end if;
      IF v_vcKPIMeasureFieldID = '' then 
         v_vcKPIMeasureFieldID := NULL;
      end if;
      IF v_numReportID = 0 then

         INSERT INTO ReportListMaster(vcReportName, vcReportDescription, numDomainID, numReportModuleID,
		numReportModuleGroupID, tintReportType, numCreatedBy, dtCreatedDate,
		bitActive, textQuery,numSortColumnmReportFieldGroupID, numSortColumnFieldID, vcSortColumnDirection, bitSortColumnCustom, vcFilterLogic,
		numDateReportFieldGroupID,numDateFieldID,bitDateFieldColumnCustom,vcDateFieldValue,dtFromDate,dtToDate,tintRecordFilter,intNoRows,vcKPIMeasureFieldID,bitHideSummaryDetail)
         SELECT v_vcReportName, v_vcReportDescription, v_numDomainID, v_numReportModuleID,
	    v_numReportModuleGroupID, v_tintReportType, v_numUserCntID, TIMEZONE('UTC',now()),
	    v_bitActive, v_textQuery,v_numSortColumnmReportFieldGroupID, v_numSortColumnFieldID, v_vcSortColumnDirection, v_bitSortColumnCustom, v_vcFilterLogic,
	    v_numDateReportFieldGroupID,v_numDateFieldID,v_bitDateFieldColumnCustom,v_vcDateFieldValue,v_dtFromDate,v_dtToDate,v_tintRecordFilter,v_intNoRows,v_vcKPIMeasureFieldID,v_bitHideSummaryDetail;
         v_numReportID := CURRVAL('ReportListMaster_seq');
      ELSE
         UPDATE ReportListMaster
         SET  vcReportName = v_vcReportName,vcReportDescription = v_vcReportDescription,
         tintReportType = v_tintReportType,numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
         textQuery = v_textQuery,numSortColumnmReportFieldGroupID = v_numSortColumnmReportFieldGroupID, 
         numSortColumnFieldID = v_numSortColumnFieldID,
         vcSortColumnDirection = v_vcSortColumnDirection, 
         bitSortColumnCustom = v_bitSortColumnCustom,vcFilterLogic = v_vcFilterLogic,
         numDateReportFieldGroupID = v_numDateReportFieldGroupID,
         numDateFieldID = v_numDateFieldID,bitDateFieldColumnCustom = v_bitDateFieldColumnCustom,
         vcDateFieldValue = v_vcDateFieldValue,dtFromDate = v_dtFromDate,
         dtToDate = v_dtToDate,tintRecordFilter = v_tintRecordFilter,intNoRows = v_intNoRows,
         vcKPIMeasureFieldID = v_vcKPIMeasureFieldID,bitHideSummaryDetail = v_bitHideSummaryDetail
         WHERE  numDomainID = v_numDomainID AND numReportID = v_numReportID;
         IF OCTET_LENGTH(v_strText) > 2 then
        --Delete records into ReportFieldsList and Insert
            DELETE FROM ReportFieldsList WHERE  numReportID = v_numReportID;
            INSERT INTO ReportFieldsList(numReportID,numFieldID,bitCustom,numReportFieldGroupID)
            SELECT v_numReportID,numFieldID,bitCustom,numReportFieldGroupID
            FROM 
			XMLTABLE
			(
				'NewDataSet/ReportFieldsList'
				PASSING 
					CAST(v_strText AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom',
					numReportFieldGroupID NUMERIC PATH 'numReportFieldGroupID'
			) X; 

		--Delete records into ReportFilterList and Insert
            DELETE FROM ReportFilterList WHERE  numReportID = v_numReportID;
            INSERT INTO ReportFilterList(numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID)
            SELECT v_numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
            FROM 
			XMLTABLE
			(
				'NewDataSet/ReportFilterList'
				PASSING 
					CAST(v_strText AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom',
					vcFilterValue VARCHAR(500) PATH 'vcFilterValue',
					vcFilterOperator VARCHAR(50) PATH 'vcFilterOperator',
					vcFilterText VARCHAR(2000) PATH 'vcFilterText',
					numReportFieldGroupID NUMERIC PATH 'numReportFieldGroupID'
			) X; 
		
		--Delete records into ReportSummaryGroupList and Insert
            DELETE FROM ReportSummaryGroupList WHERE  numReportID = v_numReportID;
            INSERT INTO ReportSummaryGroupList(numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID)
            SELECT v_numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
            FROM
			XMLTABLE
			(
				'NewDataSet/ReportSummaryGroupList'
				PASSING 
					CAST(v_strText AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom',
					tintBreakType SMALLINT PATH 'tintBreakType',
					numReportFieldGroupID NUMERIC PATH 'numReportFieldGroupID'
			) X; 

		--Delete records into ReportMatrixAggregateList and Insert
            DELETE FROM ReportMatrixAggregateList WHERE  numReportID = v_numReportID;
            INSERT INTO ReportMatrixAggregateList(numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID)
            SELECT v_numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
            FROM 
			XMLTABLE
			(
				'NewDataSet/ReportMatrixAggregateList'
				PASSING 
					CAST(v_strText AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom',
					vcAggType VARCHAR(50) PATH 'vcAggType',
					numReportFieldGroupID NUMERIC PATH 'numReportFieldGroupID'
			) X; 

		--Delete records into ReportKPIThresold and Insert
            DELETE FROM ReportKPIThresold WHERE  numReportID = v_numReportID;
            INSERT INTO ReportKPIThresold(numReportID,vcThresoldType,decValue1,decValue2,decGoalValue)
            SELECT v_numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
            FROM 
			XMLTABLE
			(
				'NewDataSet/ReportKPIThresold'
				PASSING 
					CAST(v_strText AS XML)
				COLUMNS
					id FOR ORDINALITY,
					vcThresoldType VARCHAR(50) PATH 'vcThresoldType',
					decValue1 DECIMAL(18,2) PATH 'decValue1',
					decValue2 DECIMAL(18,2) PATH 'decValue2',
					decGoalValue DECIMAL(18,2) PATH 'decGoalValue'
			) X; 

         end if;
      end if;
      select   coalesce(numGroupID,0) INTO v_numGroupId FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF NOT EXISTS(SELECT numReportID  FROM ReportDashboardAllowedReports WHERE numDomainID = v_numDomainID AND numReportID = v_numReportID AND numGrpID = v_numGroupId) then

         INSERT INTO ReportDashboardAllowedReports(numDomainID
		,numGrpID
		,numReportID
		,tintReportCategory)
	VALUES(v_numDomainID
		,v_numGroupId
		,v_numReportID
		,0);
      end if;
   end if;
	


	
   RETURN;
END; $$;


