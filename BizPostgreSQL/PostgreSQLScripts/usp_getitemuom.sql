-- Stored procedure definition script USP_GetItemUOM for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemUOM(v_numDomainID NUMERIC(9,0),    
 v_numItemCode NUMERIC(9,0),
 v_bUnitAll BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
       
   
--   IF(@bUnitAll=1 AND (SELECT COUNT(*) FROM ItemUOM WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID)=0)
--   BEGIN
--       EXEC USP_GetUOM @numDomainID
--   END
--   ELSE
--   BEGIN
--        SELECT u.numUOMId,u.vcUnitName FROM 
--        ItemUOM iu INNER JOIN UOM u ON (iu.numUOMId = u.numUOMId AND iu.numItemCode=@numItemCode)
--        WHERE u.numDomainID=@numDomainID AND iu.numDomainID=@numDomainID
--   END
--   

-- IF((SELECT COUNT(*) FROM UOM WHERE numDomainID=@numDomainID)=0)
--		BEGIN
--			INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
--			SELECT @numDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0
--		END
		
   open SWV_RefCur for SELECT cast(u.numUOMId as VARCHAR(255)),cast(u.vcUnitName as VARCHAR(255)) FROM
   UOM u INNER JOIN Domain d ON u.numDomainID = d.numDomainId
   WHERE u.numDomainID = v_numDomainID AND d.numDomainId = v_numDomainID AND
   u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END);
END; $$;












