-- Stored procedure definition script USP_CheckDuplicateSiteSubscription for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckDuplicateSiteSubscription(v_numSiteID NUMERIC(18,0) DEFAULT 0, 
	v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_vcEmail VARCHAR(50) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	
   open SWV_RefCur for SELECT SSD.numSubscriberID
   FROM SiteSubscriberDetails SSD
   INNER JOIN AdditionalContactsInformation ACI
   ON SSD.numContactId = ACI.numContactId
   WHERE LOWER(ACI.vcEmail) = LOWER(v_vcEmail)
   AND ACI.numDomainID = v_numDomainID
   AND SSD.numSiteID = v_numSiteID;
END; $$;












