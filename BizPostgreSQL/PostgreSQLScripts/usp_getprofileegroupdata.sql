-- Stored procedure definition script USP_GetProfileEGroupData for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProfileEGroupData(v_strcontact TEXT DEFAULT null,    
  v_numDomainID NUMERIC DEFAULT NULL,  
  v_numUserCntID NUMERIC DEFAULT NULL,  
  v_numGroupID NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc4  INTEGER;
BEGIN
   if  v_numGroupID <> 0 then

      if COALESCE(v_strcontact,'') <> '' then
     
         open SWV_RefCur for select cast(PGD.numContactId as VARCHAR(255)),ADC.vcFirstName,ADC.vcLastname,cast(c.vcCompanyName as VARCHAR(255)),ADC.vcEmail
         FROM ProfileEGroupDTL PGD
         join AdditionalContactsInformation ADC on ADC.numContactId = PGD.numContactId
         JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
         JOIN CompanyInfo c ON DM.numCompanyID = c.numCompanyId
         join ProfileEmailGroup PEG on PGD.numEmailGroupID = PEG.numEmailGroupID
         where 
			PGD.numContactId not in (select 
											X.numContactId 
										from
										XMLTABLE
										(
											'NewDataSet/Table'
											PASSING 
												CAST(v_strcontact AS XML)
											COLUMNS
												id FOR ORDINALITY,
												numContactId NUMERIC(18,0) PATH 'numContactId'
										) X) 
			and PGD.numEmailGroupID = v_numGroupID;
      end if;

   end if;
END; $$;












