-- Stored procedure definition script USP_CategoryProfile_GetAll for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CategoryProfile_GetAll(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(ID as VARCHAR(255)),cast(vcName as VARCHAR(255)) FROM CategoryProfile WHERE numDomainID = v_numDomainID;
   RETURN;
END; $$;














