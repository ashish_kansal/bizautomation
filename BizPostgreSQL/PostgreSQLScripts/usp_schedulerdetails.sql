-- Stored procedure definition script USP_SchedulerDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SchedulerDetails(v_numScheduleId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From CustRptScheduler Where numScheduleid = v_numScheduleId;
END; $$;












