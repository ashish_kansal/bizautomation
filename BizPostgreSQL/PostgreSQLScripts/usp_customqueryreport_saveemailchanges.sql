-- Stored procedure definition script USP_CustomQueryReport_SaveEmailChanges for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_SaveEmailChanges(v_numReportID NUMERIC(18,0),
	v_vcReportName VARCHAR(300),
	v_vcReportDescription VARCHAR(300),
	v_vcEmailTo VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   CustomQueryReport
   SET
   vcReportName = v_vcReportName,vcReportDescription = v_vcReportDescription,
   vcEmailTo = v_vcEmailTo
   WHERE
   numReportID = v_numReportID;
   RETURN;
END; $$;

