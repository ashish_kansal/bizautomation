-- Function definition script GetUTCDateWithoutTime for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetUTCDateWithoutTime()
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Dt  TIMESTAMP;
BEGIN
   v_Dt := TIMEZONE('UTC',now()):: date;

   RETURN v_Dt;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[hirearchy]    Script Date: 07/26/2008 18:13:13 ******/

