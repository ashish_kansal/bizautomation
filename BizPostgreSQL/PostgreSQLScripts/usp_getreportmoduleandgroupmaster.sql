-- Stored procedure definition script USP_GetReportModuleANDGroupMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReportModuleANDGroupMaster(INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT numReportModuleID
      ,vcModuleName
      ,bitActive
   FROM ReportModuleMaster WHERE bitActive = true;
	

	
   open SWV_RefCur2 for
   SELECT numReportModuleGroupID
      ,numReportModuleID
      ,vcGroupName
      ,bitActive
   FROM ReportModuleGroupMaster WHERE bitActive = true;
   RETURN;
END; $$;


