-- Stored procedure definition script USP_ManageCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCategory(v_numCatergoryId NUMERIC(9,0),        
	v_vcCatgoryName VARCHAR(1000),       
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_vcDescription TEXT DEFAULT NULL,
	v_intDisplayOrder INTEGER DEFAULT NULL ,
	v_vcPathForCategoryImage VARCHAR(100) DEFAULT NULL,
	v_numDepCategory NUMERIC(9,0) DEFAULT NULL,
	v_numCategoryProfileID NUMERIC(18,0) DEFAULT NULL,
	v_vcCatgoryNameURL VARCHAR(1000) DEFAULT NULL,
	v_vcMetaTitle VARCHAR(1000) DEFAULT NULL,
	v_vcMetaKeywords VARCHAR(1000) DEFAULT NULL,
	v_vcMetaDescription VARCHAR(1000) DEFAULT NULL,
	v_vcMatrixGroups VARCHAR(300) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintLevel  SMALLINT;
BEGIN
   v_tintLevel := 1;

   IF	v_numDepCategory <> 0 then
	
      select(tintLevel+1) INTO v_tintLevel FROM Category WHERE numCategoryID = v_numDepCategory;
   end if;       
       
   IF v_numCatergoryId = 0 then
	
      INSERT INTO Category(vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription)
		VALUES(v_vcCatgoryName,
			v_tintLevel,
			v_numDomainID,
			v_vcDescription,
			v_intDisplayOrder,
			v_vcPathForCategoryImage,
			v_numDepCategory,
			v_numCategoryProfileID,
			v_vcCatgoryNameURL,
			v_vcMetaTitle,
			v_vcMetaKeywords,
			v_vcMetaDescription);
		
      v_numCatergoryId := CURRVAL('Category_seq');
      insert into SiteCategories(numSiteID,
			numCategoryID)
      SELECT
      numSiteID
			,v_numCatergoryId
      FROM
      CategoryProfileSites
      WHERE
      numCategoryProfileID = v_numCategoryProfileID;
   ELSEIF v_numCatergoryId > 0
   then
	
      UPDATE
      Category
      SET
      vcCategoryName = v_vcCatgoryName,vcDescription = v_vcDescription,intDisplayOrder = v_intDisplayOrder,
      tintLevel = v_tintLevel,vcPathForCategoryImage = v_vcPathForCategoryImage,
      numDepCategory = v_numDepCategory,vcCategoryNameURL = v_vcCatgoryNameURL,
      vcMetaTitle = v_vcMetaTitle,vcMetaKeywords = v_vcMetaKeywords,
      vcMetaDescription = v_vcMetaDescription
      WHERE
      numCategoryID = v_numCatergoryId;
   end if;

   IF coalesce(v_vcMatrixGroups,'') = '' then
	
      DELETE FROM CategoryMatrixGroup WHERE numDomainID = v_numDomainID AND numCategoryID = v_numCatergoryId;
   ELSE
      DELETE FROM CategoryMatrixGroup WHERE numDomainID = v_numDomainID AND numCategoryID = v_numCatergoryId AND numItemGroup NOT IN(SELECT Id FROM SplitIDs(v_vcMatrixGroups,','));
   end if;

   INSERT INTO CategoryMatrixGroup(numDomainID,numCategoryID,numItemGroup)
   SELECT
   v_numDomainID,v_numCatergoryId,Temp.Id
   FROM
   SplitIDs(v_vcMatrixGroups,',') Temp
   WHERE
   Temp.Id NOT IN(SELECT numItemGroup FROM CategoryMatrixGroup WHERE numDomainID = v_numDomainID AND numCategoryID = v_numCatergoryId);
RETURN;
END; $$;	


