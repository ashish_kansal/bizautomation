-- Stored procedure definition script USP_UpdateChartAcntOpnBalanceForJournalEntry for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateChartAcntOpnBalanceForJournalEntry(v_JournalId NUMERIC(9,0),  
v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTransactionId  NUMERIC(9,0);                              
   v_numChartAcntId  VARCHAR(8000);                              
   v_numChartAcntId1  VARCHAR(8000);                              
   v_numDebitAmt  DECIMAL(20,5);                            
   v_numCreditAmt  DECIMAL(20,5);                            
   v_strSQl  VARCHAR(8000);                             
   v_strSQl1  VARCHAR(8000);                            
   v_strSQl2  VARCHAR(8000);                
   v_strSQlBalance  VARCHAR(8000);               
   v_numChartAcntTypeId  INTEGER;
BEGIN
   v_strSQl1 := '';                             
   v_strSQl := '';                              
   v_strSQl2 := '';               
   v_strSQlBalance := '';                        
                              
   select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Details Where numJournalId = v_JournalId And numDomainId = v_numDomainId;                              
                               
   While v_numTransactionId <> 0 LOOP
      select   numChartAcntId, numDebitAmt, numCreditAmt INTO v_numChartAcntId,v_numDebitAmt,v_numCreditAmt From General_Journal_Details Where numTransactionId = v_numTransactionId And numDomainId = v_numDomainId;                             
   --Select * From General_Journal_Details Where numTransactionId=@numTransactionId                               
   --Set @numChartAcntId1= @numChartAcntId                              
      RAISE NOTICE '%',v_numChartAcntId;
      select   numAcntTypeId INTO v_numChartAcntTypeId From Chart_Of_Accounts Where numAccountId = cast(NULLIF(v_numChartAcntId,'') as NUMERIC(18,0))  And numDomainId = v_numDomainId;
      RAISE NOTICE '@numChartAcntTypeId%',SUBSTR(CAST(v_numChartAcntTypeId AS VARCHAR(10)),1,10);
      v_strSQl := fn_ParentCategory(v_numChartAcntId,v_numDomainId::INTEGER);
      RAISE NOTICE '%','@strSQl' || coalesce(v_strSQl,'');
      if v_numDebitAmt <> 0 then
    
         if v_strSQl <> '' then
      
            if v_numChartAcntTypeId = 813 Or v_numChartAcntTypeId = 814 Or v_numChartAcntTypeId = 817 Or v_numChartAcntTypeId = 818 Or v_numChartAcntTypeId = 819 Or v_numChartAcntTypeId = 823 Or v_numChartAcntTypeId = 824 Or v_numChartAcntTypeId = 826 then
               v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numDebitAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            else
               v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numDebitAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            end if;
         else
            if v_numChartAcntTypeId = 813 Or v_numChartAcntTypeId = 814 Or v_numChartAcntTypeId = 817 Or v_numChartAcntTypeId = 818 Or v_numChartAcntTypeId = 819 Or v_numChartAcntTypeId = 823 Or v_numChartAcntTypeId = 824 Or v_numChartAcntTypeId = 826 then
               v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numDebitAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            else
               v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numDebitAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            end if;
         end if;
         EXECUTE v_strSQl2;
         RAISE NOTICE '%',(v_strSQl2);
      end if;
      if v_numCreditAmt <> 0 then
    
         if v_strSQl <> '' then
      
            if v_numChartAcntTypeId = 813 Or v_numChartAcntTypeId = 814 Or v_numChartAcntTypeId = 817 Or v_numChartAcntTypeId = 818 Or v_numChartAcntTypeId = 819 Or v_numChartAcntTypeId = 823 Or v_numChartAcntTypeId = 824 Or v_numChartAcntTypeId = 826 then
               v_strSQl1 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numCreditAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            else
               v_strSQl1 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numCreditAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            end if;
         else
            if v_numChartAcntTypeId = 813 Or v_numChartAcntTypeId = 814 Or v_numChartAcntTypeId = 817 Or v_numChartAcntTypeId = 818 Or v_numChartAcntTypeId = 819 Or v_numChartAcntTypeId = 823 Or v_numChartAcntTypeId = 824 Or v_numChartAcntTypeId = 826 then
        
               RAISE NOTICE 'SP';
               v_strSQl1 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numCreditAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            else
               RAISE NOTICE 'Sivaprakasam';
               v_strSQl1 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numCreditAmt AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || coalesce(v_numChartAcntId,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
            end if;
         end if;
         RAISE NOTICE '%','@strSQl=' || coalesce(v_strSQl,'');
         EXECUTE v_strSQl1;
         RAISE NOTICE '%',(v_strSQl1);
         EXECUTE v_strSQlBalance;
         RAISE NOTICE '%',(v_strSQlBalance);
      end if;
      RAISE NOTICE '%',('Debit' || SUBSTR(CAST(v_numDebitAmt AS VARCHAR(200)),1,200));
      RAISE NOTICE '%',('Credit' || SUBSTR(CAST(v_numCreditAmt AS VARCHAR(200)),1,200));
      select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Details Where numJournalId = v_JournalId And  numTransactionId > v_numTransactionId And numDomainId = v_numDomainId;
   END LOOP;
   RETURN;
END; $$;


