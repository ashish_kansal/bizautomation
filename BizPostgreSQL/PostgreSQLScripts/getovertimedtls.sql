-- Function definition script GetOverTimeDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOverTimeDtls(v_numUserID NUMERIC(9,0),v_dtFrom TIMESTAMP,v_dtTo TIMESTAMP)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltOvrHours  DOUBLE PRECISION;
   v_fltHours  DOUBLE PRECISION;
   v_fltDailyHours  DOUBLE PRECISION;
   v_bitOverTime  BOOLEAN;
   v_dtFrom1  TIMESTAMP;
   v_Check  SMALLINT;
BEGIN
   v_fltOvrHours := 0;
   select   bitOverTime, numLimDailHrs INTO v_bitOverTime,v_fltHours from UserMaster where numUserId = v_numUserID;
   if v_bitOverTime = true then
      v_Check := 0;
      while v_Check > 0 LOOP
			--set @dtFrom1=dateadd(day,1,@dtFrom)
			--select @fltDailyHours=convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60
			--from TimeAndExpense where numUserID=@numUserID and numCategory=1 and numType=1 
			--and (dtFromDate between @dtFrom and @dtFrom1)
                        --if @fltDailyHours>@fltHours set @fltOvrHours=@fltOvrHours+@fltDailyHours-@fltHours
			--if @dtFrom=@dtTo set @Check=0
			--set @dtFrom=@dtFrom1
         v_fltOvrHours := 1;
         v_Check := 1;
      END LOOP;
   end if;
   RETURN v_fltOvrHours;
END; $$;

