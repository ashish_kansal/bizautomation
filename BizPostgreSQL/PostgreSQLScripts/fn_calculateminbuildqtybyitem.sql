-- Function definition script fn_CalculateMinBuildQtyByItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CalculateMinBuildQtyByItem(v_numKitId NUMERIC DEFAULT 0,
	v_numWareHouseID NUMERIC DEFAULT 0)
RETURNS NUMERIC(18,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMaxBuildQty  NUMERIC(18,2) DEFAULT 0;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      numItemKitID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUOMQuantity NUMERIC(18,0),
      numBaseUnit NUMERIC(18,0),
      numIDUOMId NUMERIC(18,0)
   );
   INSERT INTO tt_TEMPTABLE
   SELECT
   CAST(0 AS NUMERIC(18,0))
		,numItemCode
		,(DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))
		,coalesce(numBaseUnit,0)
		,coalesce(DTL.numUOMID,coalesce(numBaseUnit,0)) AS numIDUOMId
   FROM
   Item
   INNER JOIN
   ItemDetails DTL
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numKitId; 

   INSERT INTO tt_TEMPTABLE
   SELECT
   Dtl.numItemKitID
		,i.numItemCode
		,(Dtl.numQtyItemsReq*coalesce(fn_UOMConversion(Dtl.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
		,coalesce(i.numBaseUnit,0)
		,coalesce(Dtl.numUOMID,coalesce(i.numBaseUnit,0)) AS numIDUOMId
   FROM
   Item i
   INNER JOIN
   ItemDetails Dtl
   ON
   Dtl.numChildItemID = i.numItemCode
   INNER JOIN
   tt_TEMPTABLE c
   ON
   Dtl.numItemKitID = c.numItemCode
   WHERE
   Dtl.numChildItemID != v_numKitId;

   select   MIN(numOnHand/(CASE WHEN coalesce(c.numUOMQuantity,0) = 0 THEN 1 ELSE c.numUOMQuantity END)) INTO v_numMaxBuildQty FROM
   tt_TEMPTABLE c
   LEFT JOIN
   UOM
   ON
   UOM.numUOMId = c.numBaseUnit
   LEFT JOIN
   UOM IDUOM
   ON
   IDUOM.numUOMId = c.numIDUOMId
   LEFT JOIN LATERAL(SELECT 
      numWareHouseItemID
			,numOnHand
			,numOnOrder
			,numReorder
			,numAllocation
			,numBackOrder
			,numItemID
			,vcWareHouse
			,monWListPrice
      FROM
      WareHouseItems
      LEFT JOIN
      Warehouses W
      ON
      W.numWareHouseID = WareHouseItems.numWareHouseID
      WHERE
      WareHouseItems.numItemID = c.numItemCode
      AND WareHouseItems.numWareHouseID = v_numWareHouseID
      ORDER BY
      numWareHouseItemID LIMIT 1) WI on TRUE WHERE
   numItemKitID = 0  AND WI.numWareHouseItemID > 0;

   RETURN v_numMaxBuildQty;
END; $$;

