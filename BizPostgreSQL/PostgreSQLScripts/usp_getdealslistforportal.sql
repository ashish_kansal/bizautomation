-- Stored procedure definition script USP_GetDealsListForPortal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDealsListForPortal(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintSortOrder SMALLINT DEFAULT 4,
    v_SortChar CHAR(1) DEFAULT '0',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_tintShipped SMALLINT DEFAULT NULL,
    v_intOrder SMALLINT DEFAULT NULL,
    v_intType SMALLINT DEFAULT NULL,
    v_tintFilterBy SMALLINT DEFAULT 0,
    v_numOrderStatus NUMERIC DEFAULT NULL,
    v_vcRegularSearchCriteria VARCHAR(1000) DEFAULT '',
    v_vcCustomSearchCriteria VARCHAR(1000) DEFAULT '',
    v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  SMALLINT;
   v_numFormId  INTEGER; 
  
   v_strShareRedordWith  VARCHAR(300);
   v_strSql  VARCHAR(8000);
   v_strOrderColumn  VARCHAR(100);
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_tintOrder  SMALLINT;
   v_vcFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(1);
   v_vcAssociatedControlType  VARCHAR(30);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(40);
   v_WhereCondition  VARCHAR(2000);
   v_vcLookBackTableName  VARCHAR(2000);
   v_bitCustom  BOOLEAN;
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  CHAR(1);   
   v_bitAllowEdit  CHAR(1);                 
   v_vcColumnName  VARCHAR(500);                  
   v_Nocolumns  SMALLINT;
   v_ListRelID  NUMERIC(9,0); 

   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_PageId := 0;
   IF v_intType = 1 then
        
      v_PageId := 2;
      v_intType := 3;
      v_numFormId := 78;
   end if;
   IF v_intType = 2 then
        
      v_PageId := 6;
      v_intType := 4;
      v_numFormId := 79;
   end if;
        
  --Create a Temporary table to hold data
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
      PRIMARY KEY,
      numOppID NUMERIC(9,0),
      intTotalProgress INTEGER
   );
    
   v_strShareRedordWith := ' 1=1 ';
--    SET @strShareRedordWith = ' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='
--        + CONVERT(VARCHAR(15), @numDomainID)
--        + ' AND SR.numModuleID = 3 AND SR.numAssignedTo = '
--        + CONVERT(VARCHAR(15), @numUserCntId) + ') '

   v_strSql := 'SELECT  Opp.numOppId,PP.intTotalProgress ';
  
   v_strOrderColumn := v_columnName;

   v_strSql := coalesce(v_strSql,'')
   || ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ';
								 
      
   IF v_columnName ilike 'CFW.Cust%' then
        
      v_strSql := coalesce(v_strSql,'')
      || ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId = Opp.numOppId  and CFW.Fld_ID = '
      || REPLACE(v_columnName,'CFW.Cust','');
      v_strOrderColumn := 'CFW.Fld_Value';
   ELSEIF v_columnName ilike 'DCust%'
   then
            
      v_strSql := coalesce(v_strSql,'')
      || ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId = Opp.numOppId   and CFW.Fld_ID = '
      || REPLACE(v_columnName,'DCust','');
      v_strSql := coalesce(v_strSql,'')
      || ' left Join ListDetails LstCF on LstCF.numListItemID = cast(NULLIF(CFW.Fld_Value,'''') as NUMERIC(18,0))  ';
      v_strOrderColumn := 'LstCF.vcData';
   end if;       

             
   IF v_tintFilterBy = 1 then --Partially Fulfilled Orders 
      v_strSql := coalesce(v_strSql,'')
      || ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numRecOwner                                                               
                             LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppId 
                             WHERE Opp.numDomainId ='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' and Opp.tintOppStatus = 1 and Opp.tintshipped = '
      || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15);
   ELSE
      v_strSql := coalesce(v_strSql,'')
      || ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' and Opp.tintOppstatus = 1 and Opp.tintShipped = '
      || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15);
   end if; 
     
   IF v_intOrder <> 0 then
        
      IF v_intOrder = 1 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = false ';
      end if;
      IF v_intOrder = 2 then
         v_strSql := coalesce(v_strSql,'') || ' and Opp.bitOrder = true';
      end if;
   end if;
        
   if v_numDivisionID <> 0 then
      v_strSql := coalesce(v_strSql,'') || ' and Div.numDivisionID = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   end if;                        
    
   IF v_SortChar <> '0' then
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName like ''' || coalesce(v_SortChar,'')
      || '%''';
   end if;
 
   IF v_tintSortOrder <> cast(NULLIF('0','') as SMALLINT) then
      v_strSql := coalesce(v_strSql,'') || '  AND Opp.tintOppType = '
      || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1);
   end if;
    
    
   IF v_tintFilterBy = 1 then --Partially Fulfilled Orders
      v_strSql := coalesce(v_strSql,'')
      || ' AND OB.bitPartialFulfilment = true AND OB.bitAuthoritativeBizDocs = 1 ';
   ELSEIF v_tintFilterBy = 2
   then --Fulfilled Orders
      v_strSql := coalesce(v_strSql,'')
      || ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) ';
   ELSEIF v_tintFilterBy = 3
   then --Orders without Authoritative-BizDocs
      v_strSql := coalesce(v_strSql,'')
      || ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' AND OM.tintOppstatus=1 
                      		    AND OM.tintShipped = '
      || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15)
      || ' AND OM.tintOppType = '
      || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1)
      || ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) ';
   ELSEIF v_tintFilterBy = 4
   then --Orders with items yet to be added to Invoices
      v_strSql := coalesce(v_strSql,'')
      || ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))';
   ELSEIF v_tintFilterBy = 6
   then --Orders with deferred Income/Invoices
      v_strSql := coalesce(v_strSql,'')
      || ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)';
   end if;

   IF v_numOrderStatus <> 0 then
      v_strSql := coalesce(v_strSql,'') || ' AND Opp.numStatus = '
      || SUBSTR(CAST(v_numOrderStatus AS VARCHAR(15)),1,15);
   end if;


   IF v_vcRegularSearchCriteria <> '' then
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if; 
	
   IF v_vcCustomSearchCriteria <> '' then
      v_strSql := coalesce(v_strSql,'')
      || ' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where '
      || coalesce(v_vcCustomSearchCriteria,'') || ')';
   end if;

   IF LENGTH(v_strOrderColumn) > 0 then
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY  ' || coalesce(v_strOrderColumn,'') || ' '
      || coalesce(v_columnSortOrder,'');
   end if;    

	
   RAISE NOTICE '%',v_strSql;
   EXECUTE 'INSERT  INTO tt_TEMPTABLE
            (
              numOppId,
              intTotalProgress
            )
            ' || v_strSql;
	
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
  
   SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTABLE;
  
   v_tintOrder := 0;
   v_WhereCondition := '';
   v_Nocolumns := 0;

    
                        --AND numRelCntType = @numFormId
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicColumns
      WHERE     numFormId = v_numFormId
                        --AND numUserCntID = @numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
                        --AND numRelCntType = @numFormId
      UNION
      SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicCustomColumns
      WHERE     numFormId = v_numFormId
                        --AND numUserCntID = @numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1) TotalRows;

   RAISE NOTICE '@Nocolumns: %',SUBSTR(CAST(v_Nocolumns AS VARCHAR(10)),1,10);
	
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );
    
   v_strSql := '';
   v_strSql := 'select ADC.numContactId,DM.numDivisionID,coalesce(DM.numTerID,0) AS numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId';

   IF v_Nocolumns = 0 then
        
      INSERT  INTO DycFormConfigurationDetails(numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth)
      SELECT  v_numFormId,
                            numFieldId,
                            0,
                            Row_number() OVER(ORDER BY tintRow DESC),
                            v_numDomainID,
                            0,
                            0,
                            1,
                            0,
                            intColumnWidth
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_numFormId
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID
      ORDER BY tintOrder ASC;
   end if;

   INSERT  INTO tt_TEMPFORM
   SELECT  tintRow+1 AS tintOrder,
                    vcDbColumnName,
                    vcOrigDbColumnName,
                    coalesce(vcCultureFieldName,vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitInlineEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage AS vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    bitAllowFiltering,
                    vcFieldDataType
   FROM    View_DynamicColumns
   WHERE   numFormId = v_numFormId
                    --AND numUserCntID = @numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitSettingField,false) = true
   AND coalesce(bitCustom,false) = false
                    --AND numRelCntType = @numFormId
   UNION
   SELECT  tintRow+1 AS tintOrder,
                    vcDbColumnName,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    false,
                    ''
   FROM    View_DynamicCustomColumns
   WHERE   numFormId = v_numFormId
                    --AND numUserCntID = @numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitCustom,false) = true
                    --AND numRelCntType = @numFormId
   ORDER BY tintOrder ASC;  
    
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM     LIMIT 1;
--    ORDER BY tintOrder ASC            

	--SELECT TOP 1 * FROM    #tempForm ORDER BY tintOrder ASC            

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'Opp.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityRecurring' then
            v_Prefix := 'OPR.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~'
         || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         IF v_vcAssociatedControlType = 'SelectBox' then
                        
            IF v_vcDbColumnName = 'tintSource' then
                                
               v_strSql := coalesce(v_strSql,'')
               || ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')'
               || ' [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcDbColumnName = 'numCampainID'
            then
                                    
               v_strSql := coalesce(v_strSql,'')
               || ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) '
               || ' [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcListItemType = 'LI'
            then
                                        
               v_strSql := coalesce(v_strSql,'') || ',L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.vcData' || ' ['
               || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join ListDetails L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numListItemID='
               || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
                                            
               v_strSql := coalesce(v_strSql,'') || ',S'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.vcState' || ' ['
               || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left join State S'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on S'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numStateID='
               || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'BP'
            then
                                                
               v_strSql := coalesce(v_strSql,'')
               || ',SPLM.Slp_Name'
               || ' [' || coalesce(v_vcColumnName,'')
               || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID ';
            ELSEIF v_vcListItemType = 'PP'
            then
                                                    
               v_strSql := coalesce(v_strSql,'')
               || ',ISNULL(T.intTotalProgress,0)'
               || ' ['
               || coalesce(v_vcColumnName,'')
               || ']';
            ELSEIF v_vcListItemType = 'T'
            then
                                                        
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
                                                            
               v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcListItemType = 'WI'
            then
                                                                
               v_strSql := coalesce(v_strSql,'') || ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
                            
            IF v_Prefix = 'OPR.' then
                                    
               v_strSql := coalesce(v_strSql,'')
               || ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'
               || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
               || ', dtRecurringDate)' || ','
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['
               || coalesce(v_vcColumnName,'') || ']';
            ELSE
               v_strSql := coalesce(v_strSql,'')
               || ',case when convert(varchar(11),DateAdd(minute, '
               || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
               || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               || ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
               v_strSql := coalesce(v_strSql,'')
               || 'when convert(varchar(11),DateAdd(minute, '
               || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
               || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               || ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
               v_strSql := coalesce(v_strSql,'')
               || 'when convert(varchar(11),DateAdd(minute, '
               || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15))
               || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               || ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strSql := coalesce(v_strSql,'')
               || 'else dbo.FormatedDateFromDate('
               || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ','
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') end  [' || coalesce(v_vcColumnName,'') || ']';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
                                
            v_strSql := coalesce(v_strSql,'') || ','
            || CASE WHEN v_vcDbColumnName = 'numAge'
            THEN 'year(getutcdate())-year(bintDOB)'
            WHEN v_vcDbColumnName = 'monPAmount'
            THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
            WHEN v_vcDbColumnName = 'numDivisionID'
            THEN 'DM.numDivisionID'
            WHEN v_vcDbColumnName = 'tintOppStatus'
            THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
            WHEN v_vcDbColumnName = 'tintOppType'
            THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
            WHEN v_vcDbColumnName = 'vcPOppName'
            THEN 'Opp.vcPOppName'
            ELSE v_vcDbColumnName
            END || ' [' || coalesce(v_vcColumnName,'') || ']';
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
                                    
            v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_Prefix,'')
            || coalesce(v_vcDbColumnName,'') || ' ['
            || coalesce(v_vcColumnName,'') || ']';
         ELSEIF v_vcAssociatedControlType = 'Label'
         then
                                        
            v_strSql := coalesce(v_strSql,'') || ','
            || CASE WHEN v_vcDbColumnName = 'CalAmount'
            THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs'
            AND v_vcDbColumnName = 'monDealAmount'
            THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs'
            AND v_vcDbColumnName = 'monAmountPaid'
            THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs'
            AND v_vcDbColumnName = 'vcBizDocsList'
            THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
            ELSE v_vcDbColumnName
            END || ' [' || coalesce(v_vcColumnName,'')
            || ']';
         ELSEIF v_vcAssociatedControlType = 'Popup'
         AND v_vcDbColumnName = 'numShareWith'
         then
                                            
            v_strSql := coalesce(v_strSql,'')
            || ',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['
            || coalesce(v_vcColumnName,'') || ']';
         end if;
      ELSE
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~'
         || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
                        
            v_strSql := coalesce(v_strSql,'') || ',CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value  [' || coalesce(v_vcColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '               
                                                                                                                            on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
                            
            v_strSql := coalesce(v_strSql,'')
            || ',case when isnull(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=0 then ''No'' when isnull(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=1 then ''Yes'' end   [' || coalesce(v_vcColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
                                
            v_strSql := coalesce(v_strSql,'')
            || ',dbo.FormatedDateFromDate(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,'
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
            || ')  [' || coalesce(v_vcColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '               
                                                                                                                                        on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid    ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
                                    
            v_vcDbColumnName := 'DCust'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.vcData' || ' [' || coalesce(v_vcColumnName,'')
            || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '               
                                                                                                                                               on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid     ';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ListDetails L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM WHERE   tintOrder > v_tintOrder -1   ORDER BY tintOrder ASC LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_tintOrder := 0;
      end if;
   END LOOP; 
  
  -------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   INSERT  INTO tt_TEMPCOLORSCHEME
   SELECT  DFM.vcOrigDbCOlumnName,
                    DFM.vcLookBackTableName,
                    DFCS.vcFieldValue,
                    DFCS.vcFieldValue1,
                    DFCS.vcColorScheme,
                    DFFM.vcAssociatedControlType
   FROM    DycFieldColorScheme DFCS
   JOIN DycFormField_Mapping DFFM ON DFCS.numFieldID = DFFM.numFieldID
   JOIN DycFieldMaster DFM ON DFM.numModuleID = DFFM.numModuleID
   AND DFM.numFieldID = DFFM.numFieldID
   WHERE   DFCS.numDomainID = v_numDomainID
   AND DFFM.numFormID = v_numFormId
   AND DFCS.numFormID = v_numFormId
   AND coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*)
   FROM   tt_TEMPCOLORSCHEME) > 0 then
        
      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType FROM    tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
----------------------------                       

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0
   AND LENGTH(v_vcCSLookBackTableName) > 0 then
        
      v_strSql := coalesce(v_strSql,'') || ',tCS.vcColorScheme';
   end if;

  
   v_strSql := coalesce(v_strSql,'')
   || ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId ';
  
   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0
   AND LENGTH(v_vcCSLookBackTableName) > 0 then
        
      IF v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      IF v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      IF v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      IF v_vcCSLookBackTableName = 'OpportunityMaster' then
         v_Prefix := 'Opp.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
                
         v_strSql := coalesce(v_strSql,'')
         || ' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),'
         || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'')
         || ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'')
         || ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox'
      OR v_vcCSAssociatedControlType = 'ListBox'
      OR v_vcCSAssociatedControlType = 'CheckBox'
      then
                    
         v_strSql := coalesce(v_strSql,'')
         || ' left join #tempColorScheme tCS on tCS.vcFieldValue='
         || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'');
      end if;
   end if;
                          
   IF v_columnName ilike 'CFW.Cust%' then
        
      v_strSql := coalesce(v_strSql,'')
      || ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '
      || REPLACE(v_columnName,'CFW.Cust','') || ' ';
      v_columnName := 'CFW.Fld_Value';
   end if; 

   v_strSql := coalesce(v_strSql,'') || coalesce(v_WhereCondition,'')
   || ' join (select ID,numOppId,intTotalProgress FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10)
   || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
    
   IF (v_tintFilterBy = 5) then --Sort alphabetically
      v_strSql := coalesce(v_strSql,'') || ' order by ID ';
   ELSEIF (v_columnName = 'numAssignedBy'
   OR v_columnName = 'numAssignedTo')
   then
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY  opp.' || coalesce(v_columnName,'') || ' '
      || coalesce(v_columnSortOrder,'');
   ELSEIF (v_columnName = 'PP.intTotalProgress')
   then
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY  T.intTotalProgress '
      || coalesce(v_columnSortOrder,'');
   ELSEIF v_columnName <> 'opp.bintCreatedDate'
   AND v_columnName <> 'vcPOppName'
   AND v_columnName <> 'bintCreatedDate'
   AND v_columnName <> 'vcCompanyName'
   then
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY  ' || coalesce(v_columnName,'') || ' '
      || coalesce(v_columnSortOrder,'');
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' order by ID ';
   end if;

                                             
   RAISE NOTICE '%',v_strSql;                      
              
   OPEN SWV_RefCur FOR EXECUTE v_strSql;  

   open SWV_RefCur for
   SELECT * FROM    tt_TEMPFORM;

    

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


