-- Function definition script Split for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Split(v_String TEXT, v_Delimiter CHAR(1))
RETURNS TABLE 
(
   Items TEXT
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_INDEX  INTEGER;
   v_SLICE  TEXT;
BEGIN
   DROP TABLE IF EXISTS tt_SPLIT_RESULTS CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_SPLIT_RESULTS
   (
      Items TEXT
   );
   IF LENGTH(coalesce(v_String,'')) > 0 then
      v_INDEX := 1;
      WHILE v_INDEX != 0 LOOP	
			-- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
         v_INDEX := POSITION(v_Delimiter IN v_String);
			-- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
         IF v_INDEX != 0 then
            v_SLICE := SUBSTR(v_String,1,v_INDEX -1);
         ELSE
            v_SLICE := v_String;
         end if;
			-- PUT THE ITEM INTO THE RESULTS SET
         INSERT INTO tt_SPLIT_RESULTS(Items) VALUES(v_SLICE);
			-- CHOP THE ITEM REMOVED OFF THE MAIN STRING
			
         v_String := SUBSTR(v_String,length(v_String) -(LENGTH(v_String) -v_INDEX)+1);
			-- BREAK OUT IF WE ARE DONE
         IF LENGTH(v_String) = 0 then 
            EXIT;
         end if;
      END LOOP;
   end if;
   RETURN QUERY (SELECT * FROM tt_SPLIT_RESULTS);
END; $$;

