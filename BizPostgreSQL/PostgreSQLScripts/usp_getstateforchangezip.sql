-- Stored procedure definition script USP_GetStateForChangeZip for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetStateForChangeZip(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_vcState VARCHAR(300) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
	numStateID as "numStateID"
		,vcState as "vcState"
   FROM
   State
   WHERE
   State.numDomainID = v_numDomainID
   AND LOWER(vcState) = LOWER(v_vcState)
   order by  vcState;
END; $$;












