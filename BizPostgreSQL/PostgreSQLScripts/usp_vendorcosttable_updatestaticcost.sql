DROP FUNCTION IF EXISTS usp_vendorcosttable_updatestaticcost;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_updatestaticcost
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_vcItemGroupsIDs TEXT
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		VendorCostTable VCT
	SET
		VCT.monStaticCost = VCT.monDynamicCost
		,VCT.numStaticCostModifiedBy=p_numusercntid
		,VCT.dtStaticCostModifiedDate=timezone('utc', now())
	FROM
		Vendor V
	INNER JOIN 
		Item I
	ON
		V.numItemCode = I.numItemCode
	WHERE
		V.numDomainID = p_numdomainid
		AND I.numItemClassification IN (SELECT Id FROM SplitIDs(p_vcItemGroupsIDs,','));
END; $$;