-- Stored procedure definition script USP_UpdateCheckHeadePrintCheck for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCheckHeadePrintCheck(v_numDomainId NUMERIC(9,0) DEFAULT 0,                       
v_strRow TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc3  INTEGER;
BEGIN
	If SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then
		UPDATE 
			CheckHeader  
		SET 
			bitIsPrint = true
			,numCheckNo = X.numCheckNo
		FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numCheckHeaderID NUMERIC(9,0) PATH 'numCheckHeaderID',
				numCheckNo NUMERIC(9,0) PATH 'numCheckNo'
		) AS X 
		WHERE 
			CheckHeader.numDomainID = v_numDomainId 
			AND CheckHeader.numCheckHeaderID = X.numCheckHeaderID;
	end if;
	
	RETURN;
END; $$;                                   
	                                                                                      


