-- Function definition script GetFiscalyear for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetFiscalyear(v_date TIMESTAMP,v_numDomainId NUMERIC(9,0))
RETURNS NUMERIC(4,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_startMonth  VARCHAR(2);
   v_GetFiscalYear  NUMERIC(4,0);
BEGIN
   select   coalesce(tintFiscalStartMonth,1) INTO v_startMonth from  Domain where numDomainId = v_numDomainId;


   If v_date < CAST((CAST(EXTRACT(Year FROM v_date) AS VARCHAR(4)) || '/' || coalesce(v_startMonth,'') || '/' || '01') AS timestamp) then
	
      v_GetFiscalYear :=    EXTRACT(Year FROM v_date) -0 -1;
   else
      v_GetFiscalYear :=    EXTRACT(Year FROM v_date) -0;
   end if;
--print @GetFiscalYear
   return v_GetFiscalYear;
END; $$;

