-- Stored procedure definition script USP_SearchOrders for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SearchOrders(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_searchText VARCHAR(100),
	v_tinOppType SMALLINT, 
	v_tintUserRightType SMALLINT DEFAULT 0,
	v_tintMode SMALLINT DEFAULT NULL,
	v_tintShipped SMALLINT DEFAULT 2,
	v_tintOppStatus SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  SMALLINT;
   v_numFormId  INTEGER;
   v_tintDecimalPoints  SMALLINT;

   v_CustomFields  VARCHAR(1000);
   v_Query  TEXT;
   v_i  INTEGER DEFAULT 1;
   v_DisplayColumns  VARCHAR(1000) DEFAULT '';

   v_vcField  VARCHAR(200);
   v_vcFieldName  VARCHAR(200);
   v_vcDbColumnName  VARCHAR(200);
   v_vcAssociatedControlType  VARCHAR(50);
   v_numListID  INTEGER;
   v_vcListItemType  VARCHAR(10);

   v_strShareRedordWith  VARCHAR(300);
   v_MasterQuery  TEXT;
   v_SearchCondition  TEXT;
BEGIN
   v_PageId := 0;

   select   coalesce(tintDecimalPoints,0) INTO v_tintDecimalPoints FROM
   Domain WHERE
   numDomainId = v_numDomainID;


   IF v_tinOppType = 1 AND v_tintOppStatus = 0 then -- SALES OPPORTUNITY
	
      v_numFormId := 38;
   ELSEIF v_tinOppType = 2 AND v_tintOppStatus = 0
   then -- PURCHASE OPPORTUNITY         
	
      v_numFormId := 40;
   ELSEIF v_tinOppType = 1 AND v_tintOppStatus = 1
   then -- SALES ORDER
	
      v_numFormId := 39;
   ELSEIF v_tinOppType = 2 AND v_tintOppStatus = 1
   then -- PURCHASE ORDER
	
      v_numFormId := 41;
   end if;
	
	
	-- GET GRID COLUMN CONFIGURATION
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcField VARCHAR(300),
      vcAssociatedControlType VARCHAR(50),
      vcFieldName VARCHAR(200),
      vcDbColumnName VARCHAR(200),
      vcLookBackTableName VARCHAR(100),
      numListID INTEGER,
      vcListItemType VARCHAR(10),
      Custom BOOLEAN,
      tintRow INTEGER
   );

   INSERT INTO tt_TEMPTABLE(vcField,
		vcAssociatedControlType,
		vcFieldName,
		vcDbColumnName,
		vcLookBackTableName,
		numListID,
		vcListItemType,
		Custom,
		tintRow)
   SELECT
   CONCAT(vcLookBackTableName,'.',vcOrigDbColumnName),
		vcAssociatedControlType,
		vcFieldName,
		vcOrigDbColumnName,
		vcLookBackTableName,
		numListID,
		vcListItemType,
		CAST(0 AS BOOLEAN),
		tintRow
   FROM
   View_DynamicColumns
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   AND numFormId = v_numFormId
   AND numRelCntType = v_numFormId
   UNION
   SELECT
   CONCAT('[Cus_',numFieldId,']'),
		CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(50)),
		vcFieldName,
		vcFieldName,
		CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)),
		CAST(0 AS INTEGER),
		CAST(CAST('' AS VARCHAR(10)) AS VARCHAR(10)),
		CAST(1 AS BOOLEAN),
		tintRow
   FROm
   View_DynamicCustomColumns
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   AND numFormId = v_numFormId
   AND numRelCntType = v_numFormId
   ORDER BY
   tintRow ASC;   

   select   COALESCE(coalesce(v_CustomFields,'') || ',','') || vcField INTO v_CustomFields FROM tt_TEMPTABLE WHERE Custom = true;

   IF v_tintMode = 1 then
      v_Query := 'SELECT OpportunityMaster.numOppID'; 

		-- GETS TOP 3 COLUMNS TO DISPLAY FROM SELECTION
      WHILE v_i <= 3 LOOP
         select   vcField, vcDbColumnName, vcAssociatedControlType, numListID, vcFieldName, vcListItemType INTO v_vcField,v_vcDbColumnName,v_vcAssociatedControlType,v_numListID,v_vcFieldName,
         v_vcListItemType FROM tt_TEMPTABLE WHERE ID = v_i;
         IF v_vcAssociatedControlType = 'SelectBox' then
			
            IF v_vcDbColumnName = 'tintSource' then
				
               v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID),''Internal Order'')' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
            ELSEIF v_vcDbColumnName = 'vcInventoryStatus'
            then
				
               v_DisplayColumns := coalesce(v_DisplayColumns,'') || ', ISNULL(dbo.CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1),'''') ' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
            ELSEIF v_vcDbColumnName = 'numContactId'
            then
				
               v_DisplayColumns := CONCAT(v_DisplayColumns,',dbo.fn_GetContactName(',v_vcField,') [' || coalesce(v_vcDbColumnName,'') || ']');
            ELSEIF v_vcDbColumnName = 'numCampainID'
            then
				
               v_DisplayColumns := coalesce(v_DisplayColumns,'') || ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= OpportunityMaster.numCampainID) ' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
            ELSEIF v_vcListItemType = 'LI'
            then
				
               v_DisplayColumns := CONCAT(coalesce(v_DisplayColumns,'') || ', ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = ',v_numListID,' AND numDomainID =',v_numDomainID, 
               ' AND numListItemID =',v_vcField,'),'''')',' [' || coalesce(v_vcDbColumnName,'') || ']');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_DisplayColumns := CONCAT(v_DisplayColumns,',dbo.fn_GetContactName(',v_vcField,') [' || coalesce(v_vcDbColumnName,'') || ']');
            ELSEIF v_vcListItemType = 'PP'
            then
				
               v_DisplayColumns := coalesce(v_DisplayColumns,'') || ', (CASE WHEN ProjectProgress.numOppID >0 then CONCAT(ISNULL(ProjectProgress.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OpportunityMaster.numPercentageComplete) END) ' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
            ELSE
               v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || coalesce(v_vcField,'') || ' [' || coalesce(v_vcDbColumnName,'') || ']';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',dbo.FormatedDateFromDate(' || coalesce(v_vcField,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcDbColumnName = 'vcOrderedShipped'
         then
			
            v_DisplayColumns := CONCAT(v_DisplayColumns,',','dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,1,OpportunityMaster.tintshipped,',v_tintDecimalPoints,')',' [',
            v_vcDbColumnName,']');
         ELSEIF v_vcDbColumnName = 'vcOrderedReceived'
         then
			
            v_DisplayColumns := CONCAT(v_DisplayColumns,',','dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,2,OpportunityMaster.tintshipped,',v_tintDecimalPoints,')',' [', 
            v_vcDbColumnName,']');
         ELSEIF v_vcField = 'OpportunityMaster.monDealAmount'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || '[dbo].[getdealamount](OpportunityMaster.numOppId,Getutcdate(),0)' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'OpportunityBizDocs.monDealAmount'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,false)=true),0)' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'OpportunityBizDocs.monAmountPaid'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,false)=true),0)' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'OpportunityBizDocs.vcBizDocsList'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || 'ISNULL((SELECT STUFF((SELECT CONCAT('', '', vcBizDocID) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId FOR XML PATH('''')), 1, 1, '''')),'''')' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'OpportunityMaster.tintOppStatus'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || '(CASE OpportunityMaster.tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END)' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'OpportunityMaster.tintOppType'
         then
			
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || '(CASE OpportunityMaster.tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END)' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSEIF v_vcField = 'ShareRecord.numShareWith'
         then
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || '(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=OpportunityMaster.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=OpportunityMaster.numOppId
								AND UM.numDomainID=OpportunityMaster.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000))' || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         ELSE
            v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',' || coalesce(v_vcField,'') || ' [' || coalesce(v_vcDbColumnName,'') || ']';
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      IF LENGTH(v_DisplayColumns) = 0 OR POSITION('OpportunityMaster.vcPOppName' IN v_DisplayColumns) = 0 then
		
         v_DisplayColumns := coalesce(v_DisplayColumns,'') || ',OpportunityMaster.vcPOppName ';
      end if;
      v_strShareRedordWith := CONCAT(' OpportunityMaster.numOppId IN (SELECT SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=',v_numDomainID,' AND SR.numModuleID=3 AND SR.numAssignedTo=',
      v_numUserCntID,')');
      v_MasterQuery := CONCAT(' FROM
									OpportunityMaster
								LEFT JOIN
									DivisionMaster 
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								LEFT JOIN
									CompanyInfo 
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN
									AdditionalContactsInformation
								ON
									OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
								LEFT JOIN
									OpportunityAddress
								ON
									OpportunityMaster.numOppId = OpportunityAddress.numOppID
								LEFT JOIN
									OpportunityRecurring
								ON
									OpportunityMaster.numOppId = OpportunityRecurring.numOppId
								LEFT JOIN
									ProjectProgress
								ON
									OpportunityMaster.numOppId = ProjectProgress.numOppId
								LEFT JOIN 
									OpportunityLinking
								ON 
									OpportunityLinking.numChildOppID=OpportunityMaster.numOppId
								LEFT JOIN
									Sales_process_List_Master
								ON
									OpportunityMaster.numBusinessProcessID = Sales_process_List_Master.Slp_Id',(CASE WHEN LENGTH(v_CustomFields) > 0 THEN
         CONCAT(' OUTER APPLY
									(
										SELECT 
											* 
										FROM 
											(
												SELECT  
													CONCAT(''Cus_'',CFW_Fld_Values_Opp.Fld_ID) Fld_ID,
													(
														CASE 
														WHEN (fld_Type=''TextBox'' or fld_Type=''TextArea'')
														THEN ISNULL(Fld_Value,'''')
														WHEN fld_Type=''SelectBox''
														THEN dbo.GetListIemName(Fld_Value)
														WHEN fld_Type=''CheckBox'' 
														THEN (CASE WHEN Fld_Value=0 THEN ''No'' ELSE ''Yes'' END)
														WHEN fld_type = ''CheckBoxList''
														THEN
															(SELECT 
																STUFF((SELECT CONCAT('', '', vcData) 
															FROM 
																ListDetails 
															WHERE 
																numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))
														ELSE
															Fld_Value
														END
													) AS Fld_Value 
												FROM 
													CFW_Fld_Values_Opp 
												JOIN
													CFW_Fld_Master
												ON
													CFW_Fld_Values_Opp.Fld_ID = CFW_Fld_Master.Fld_id
												WHERE 
													RecId = OpportunityMaster.numOppID
											) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',v_CustomFields,
         ') ) AS pvt
									) AS TableCustomFields') ELSE '' END),
      '
								WHERE 
									OpportunityMaster.numDomainID=',v_numDomainID,
      ' AND (OpportunityMaster.tintShipped = ',v_tintShipped,' OR ',v_tintShipped, 
      ' = 2) AND tintOppStatus=',v_tintOppStatus,' AND OpportunityMaster.tintOppType=',
      v_tinOppType);
      v_Query := coalesce(v_Query,'') || coalesce(v_DisplayColumns,'') || coalesce(v_MasterQuery,'');


		-- GENERATES SEARCH CONDITION FOR ALL SELLECTED COLUMNS
      select   COALESCE(coalesce(v_SearchCondition,'') || ' OR ','') ||(CASE
      WHEN vcField = 'OpportunityMaster.tintSource' THEN 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID),''Internal Order'')'
      WHEN vcField = 'OpportunityMaster.numCampainID' THEN '(SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= OpportunityMaster.numCampainID)'
      WHEN vcField = 'OpportunityMaster.vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,1,OpportunityMaster.tintshipped,',v_tintDecimalPoints,')')
      WHEN vcField = 'OpportunityMaster.vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,2,OpportunityMaster.tintshipped,',v_tintDecimalPoints,')')
      WHEN vcField = 'OpportunityMaster.monDealAmount' THEN '[dbo].[getdealamount](OpportunityMaster.numOppId,Getutcdate(),0)'
      WHEN vcField = 'OpportunityBizDocs.monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,false)=true),0)'
      WHEN vcField = 'OpportunityBizDocs.monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,false)=true),0)'
      WHEN vcField = 'OpportunityBizDocs.vcBizDocsList' THEN 'ISNULL((SELECT STUFF((SELECT CONCAT('', '', vcBizDocID) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId FOR XML PATH('''')), 1, 1, '''')),'''')'
      WHEN vcField = 'OpportunityMaster.tintOppStatus' THEN '(CASE OpportunityMaster.tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END)'
      WHEN vcField = 'OpportunityMaster.tintOppType' THEN '(CASE OpportunityMaster.tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END)'
      WHEN vcField = 'ProjectProgress.intTotalProgress' THEN '(CASE WHEN ProjectProgress.numOppID >0 then CONCAT(ISNULL(ProjectProgress.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OpportunityMaster.numPercentageComplete) END)'
      WHEN vcField = 'OpportunityMaster.numContactId' THEN CONCAT('dbo.fn_GetContactName(',vcField,')')
      WHEN vcAssociatedControlType = 'SelectBox' AND vcListItemType = 'LI' THEN CONCAT('ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = ',numListID,
         ' AND numDomainID =',v_numDomainID,' AND numListItemID =',vcField, 
         '),'''')')
      WHEN vcAssociatedControlType = 'SelectBox' AND vcListItemType = 'U' THEN CONCAT('dbo.fn_GetContactName(',vcField,')')
      WHEN vcAssociatedControlType = 'DateField' THEN 'dbo.FormatedDateFromDate(' || vcField || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')'
      WHEN vcField = 'ShareRecord.numShareWith' THEN '(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=OpportunityMaster.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=OpportunityMaster.numOppId
								AND UM.numDomainID=OpportunityMaster.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000))'
      ELSE vcField
      END)
      || ' LIKE ''%' || coalesce(v_searchText,'') || '%''' INTO v_SearchCondition FROM tt_TEMPTABLE WHERE Custom = true OR POSITION(vcLookBackTableName IN v_Query) > 0;
      v_Query := coalesce(v_Query,'') || ' AND (' || coalesce(v_SearchCondition,'')  || ')';
      IF v_tintUserRightType = 0 then
		
         v_Query := coalesce(v_Query,'') || ' AND OpportunityMaster.tintOppType = 0';
      ELSEIF v_tintUserRightType = 1
      then
		
         v_Query := CONCAT(v_Query,' AND (OpportunityMaster.numRecOwner = ',v_numUserCntID,' OR OpportunityMaster.numAssignedTo = ',
         v_numUserCntID,' OR ',coalesce(v_strShareRedordWith,'') || ')');
      ELSEIF v_tintUserRightType = 2
      then
		
         v_Query := CONCAT(v_Query,' AND (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = ',v_numUserCntID,') OR DivisionMaster.numTerID=0 OR OpportunityMaster.numAssignedTo = ',v_numUserCntID,' OR ',
         v_strShareRedordWith,')');
      end if;
      RAISE NOTICE '%',v_Query;
      OPEN SWV_RefCur FOR EXECUTE v_Query;
   ELSE
      IF(SELECT COUNT(*) FROM tt_TEMPTABLE) = 0 then
		
         INSERT INTO tt_TEMPTABLE(vcField,
				vcAssociatedControlType,
				vcFieldName,
				vcDbColumnName,
				vcLookBackTableName,
				numListID,
				vcListItemType,
				Custom)
			VALUES(CAST('' AS VARCHAR(300)),
				CAST('Label' AS VARCHAR(50)),
				CAST((CASE WHEN v_tinOppType = 1 THEN 'Sales Order Name' ELSE 'Purchase Order Name' END) AS VARCHAR(200)),
				CAST('vcPOppName' AS VARCHAR(200)),
				CAST('OpportunityMaster' AS VARCHAR(100)),
				0,
				CAST('' AS VARCHAR(10)),
				CAST(0 AS BOOLEAN));
      end if;
      open SWV_RefCur for
      SELECT * FROM tt_TEMPTABLE LIMIT 3;
   end if;
   RETURN;
END; $$;                  
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/



