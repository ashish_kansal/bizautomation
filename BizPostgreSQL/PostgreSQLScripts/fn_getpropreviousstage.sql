-- Function definition script fn_getProPreviousStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getProPreviousStage(v_numProId NUMERIC(9,0),v_numCurrentStageId NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProStageId  NUMERIC(9,0);
   v_numPrevProStageId  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numPrevProStageId := 0;
   v_numProStageId := 0;

   select   numProStageId INTO v_numProStageId from ProjectsStageDetails where numProId = v_numProId and numstagepercentage not in(0,100)   order by numProStageId,numstagepercentage LIMIT 1;

   while v_numProStageId > 0 LOOP
      if v_numProStageId <> v_numCurrentStageId then
         v_numPrevProStageId := v_numProStageId;
      end if;
      if v_numProStageId = v_numCurrentStageId then
         v_numProStageId := 0;
      end if;
      if v_numProStageId <> 0 then
		
         select   numProStageId INTO v_numProStageId from ProjectsStageDetails where numProId = v_numProId and numstagepercentage not in(0,100) and  numProStageId > v_numProStageId   order by numProStageId,numstagepercentage LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numProStageId := 0;
         end if;
      end if;
   END LOOP;

   return  v_numPrevProStageId;
END; $$;

