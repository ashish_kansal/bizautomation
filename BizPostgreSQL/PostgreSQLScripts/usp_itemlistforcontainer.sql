-- Stored procedure definition script USP_ItemListForContainer for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemListForContainer(v_numDomainID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numItemCode AS "numItemCode",vcItemName AS "vcItemName"
   FROM
   Item 
   WHERE
   numDomainID = v_numDomainID AND coalesce(bitContainer,false) = true;
END; $$;












