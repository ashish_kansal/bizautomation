-- Function definition script fn_GetMarketBudgetComments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetMarketBudgetComments(v_numMarketId NUMERIC(9,0),v_numDomainId NUMERIC(9,0),v_numListItemID NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcComments  VARCHAR(50);
BEGIN
   select   coalesce(MBD.vcComments,'') INTO v_vcComments From MarketBudgetMaster MBM
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId Where MBM.numDomainID = v_numDomainId And MBD.numMarketId = v_numMarketId And MBD.numListItemID = v_numListItemID;     
   Return v_vcComments;
END; $$;

