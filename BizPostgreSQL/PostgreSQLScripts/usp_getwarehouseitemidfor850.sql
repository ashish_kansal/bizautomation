-- Stored procedure definition script USP_GetWarehouseItemIdFor850 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetWarehouseItemIdFor850(v_numDomainID NUMERIC(18,0),        
	v_numItemCode NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_numOrderSource NUMERIC(18,0),
	v_tintSourceType SMALLINT,
	v_numShipToCountry NUMERIC(18,0),
	v_numShipToState NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitKitParent  BOOLEAN;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseItemID NUMERIC(18,0),
      bitWarehouseMapped BOOLEAN
   );
   IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND coalesce(bitAutoWarehouseSelection,false) = true) then
      select   coalesce(bitKitParent,false) INTO v_bitKitParent FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
      DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
      (
         numWarehouseItemID NUMERIC(18,0),
         numWarehouseID NUMERIC(18,0),
         numOnHand DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPWAREHOUSE(numWarehouseItemID
			,numWarehouseID
			,numOnHand)
      SELECT
      WareHouseItems.numWareHouseItemID
			,WareHouseItems.numWareHouseID
			,coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0)
      FROM
      WareHouseItems
      LEFT JOIN
      WarehouseLocation
      ON
      WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
      WHERE
      WareHouseItems.numItemID = v_numItemCode
      AND WarehouseLocation.numWLocationID IS NULL;
      INSERT INTO tt_TEMP(numWarehouseItemID
			,bitWarehouseMapped)
      SELECT
      numWareHouseItemID
			,CAST(1 AS BOOLEAN)
      FROM(SELECT
         ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) AS numRowIndex
				,* FROM(SELECT
            TW.numWarehouseItemID
					,(CASE
            WHEN v_bitKitParent = true
            THEN cast(fn_IsKitCanBeBuild(v_numDomainID,v_numItemCode,v_numQty,TW.numWarehouseID,'') as INTEGER)
            ELSE(CASE WHEN TW.numOnHand >= v_numQty THEN 1 ELSE 0 END)
            END) AS titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
            FROM
            MassSalesFulfillmentWM
            INNER JOIN
            MassSalesFulfillmentWMState
            ON
            MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
            INNER JOIN
            MassSalesFulfillmentWP
            ON
            MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
            INNER JOIN
            tt_TEMPWAREHOUSE TW
            ON
            MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
            WHERE
            MassSalesFulfillmentWM.numDomainID = v_numDomainID
            AND MassSalesFulfillmentWM.numOrderSource = v_numOrderSource
            AND MassSalesFulfillmentWM.tintSourceType = v_tintSourceType
            AND MassSalesFulfillmentWM.numCountryID = v_numShipToCountry
            AND MassSalesFulfillmentWMState.numStateID = v_numShipToState) TEMP) T1
      ORDER BY
      titOnHandOrder DESC,intOrder ASC;
   end if;
	
   IF EXISTS(SELECT ID FROM tt_TEMP) then
	
      open SWV_RefCur for
      SELECT  numWarehouseItemID FROM tt_TEMP ORDER BY ID LIMIT 1;
   ELSE
      IF v_numDomainID = 209 AND EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = 1279 AND coalesce(numWLocationID,0) = 0) then
		
         open SWV_RefCur for
         SELECT  numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = 1279 AND coalesce(numWLocationID,0) = 0 LIMIT 1;
      ELSE
         open SWV_RefCur for
         SELECT  numWarehouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND WareHouseItems.numItemID = v_numItemCode LIMIT 1;
      end if;
   end if;
   RETURN;
END; $$;


