-- Stored procedure definition script USP_MultiCompInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MultiCompInventory(v_numDomainID INTEGER,
v_numSubscriberID INTEGER,
v_FromDate TIMESTAMP,
v_ToDate TIMESTAMP,
v_ReportType INTEGER,
v_TopCount INTEGER,
v_numRepDomain INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Sql  VARCHAR(500);
BEGIN
   drop table IF EXISTS tt_MOSTSOLD CASCADE;
   create TEMPORARY TABLE tt_MOSTSOLD
   (
      numDomainID INTEGER, 
      vcDomainName VARCHAR(250), 
      Item VARCHAR(250),
      SalesUnit NUMERIC(18,3)
   );

   if v_ReportType = 1 then -- MOST UNIT SOLD
	
      insert into tt_MOSTSOLD
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, sum(numUnitHour)  from VIEW_INVENTORYOPPSALES A,
	Domain DN,
	Domain DNA
      where  A.numDomainID = DN.numDomainId and DN.numSubscriberID = v_numSubscriberID and
      DN.vcDomainCode ilike DNA.vcDomainCode || '%' and
      OppDate between v_FromDate and v_ToDate and
      DNA.numSubscriberID = v_numSubscriberID and DNA.numParentDomainID = v_numDomainID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass
      UNION
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, sum(numUnitHour)  from VIEW_INVENTORYOPPSALES A,
	Domain DN
      where  A.numDomainID = DN.numDomainId and
      DN.numDomainId = v_numDomainID and
      OppDate between v_FromDate and v_ToDate and
      DN.numSubscriberID = v_numSubscriberID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass;
   ELSEIF v_ReportType = 2
   then -- MAXAVG COST 
	
      insert into tt_MOSTSOLD
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, MAX(AvgCost)  from VIEW_INVENTORYOPPSALES A,
	Domain DN,
	Domain DNA
      where  A.numDomainID = DN.numDomainId and DN.numSubscriberID = v_numSubscriberID and
      DN.vcDomainCode ilike DNA.vcDomainCode || '%' and
      OppDate between v_FromDate and v_ToDate and
      DNA.numSubscriberID = v_numSubscriberID and DNA.numParentDomainID = v_numDomainID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass
      UNION
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, MAX(AvgCost)   from VIEW_INVENTORYOPPSALES A,
	Domain DN
      where  A.numDomainID = DN.numDomainId and
      DN.numDomainId = v_numDomainID and
      OppDate between v_FromDate and v_ToDate and
      DN.numSubscriberID = v_numSubscriberID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass;
   ELSEIF v_ReportType = 3
   then -- MAXAVG COST 
	
      insert into tt_MOSTSOLD
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, sum(COGS)  from VIEW_INVENTORYOPPSALES A,
	Domain DN,
	Domain DNA
      where  A.numDomainID = DN.numDomainId and DN.numSubscriberID = v_numSubscriberID and
      DN.vcDomainCode ilike DNA.vcDomainCode || '%' and
      OppDate between v_FromDate and v_ToDate and
      DNA.numSubscriberID = v_numSubscriberID and DNA.numParentDomainID = v_numDomainID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass
      UNION
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, sum(COGS)   from VIEW_INVENTORYOPPSALES A,
	Domain DN
      where  A.numDomainID = DN.numDomainId and
      DN.numDomainId = v_numDomainID and
      OppDate between v_FromDate and v_ToDate and
      DN.numSubscriberID = v_numSubscriberID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass;
   ELSEIF v_ReportType = 4
   then -- PROFIT
	
      insert into tt_MOSTSOLD
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item, sum(Profit)   from VIEW_INVENTORYOPPSALES A,
	Domain DN,
	Domain DNA
      where  A.numDomainID = DN.numDomainId and DN.numSubscriberID = v_numSubscriberID and
      DN.vcDomainCode ilike DNA.vcDomainCode || '%' and
      OppDate between v_FromDate and v_ToDate and
      DNA.numSubscriberID = v_numSubscriberID and DNA.numParentDomainID = v_numDomainID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass
      UNION
      select DN.numDomainId,DN.vcDomainName, CAST(vcItemName || ',' || ItemClass AS VARCHAR(250)) as Item,sum(Profit)    from VIEW_INVENTORYOPPSALES A,
	Domain DN
      where  A.numDomainID = DN.numDomainId and
      DN.numDomainId = v_numDomainID and
      OppDate between v_FromDate and v_ToDate and
      DN.numSubscriberID = v_numSubscriberID
      group by DN.numDomainId,DN.vcDomainName,vcItemName || ',' || ItemClass;
   end if;


   v_Sql := 'select  ' ||  SUBSTR(cast(v_TopCount as VARCHAR(30)),1,30) || ' item,salesunit from tt_MOSTSOLD where numDomainID =' || SUBSTR(cast(v_numRepDomain as VARCHAR(30)),1,30) || ' order by salesunit Desc';
   OPEN SWV_RefCur FOR EXECUTE v_Sql;
   drop table IF EXISTS tt_MOSTSOLD CASCADE;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/



