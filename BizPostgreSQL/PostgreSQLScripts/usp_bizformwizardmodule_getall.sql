CREATE OR REPLACE FUNCTION USP_BizFormWizardModule_GetAll(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM Listdetails WHERE numListID = 5 AND numDomainid = v_numDomainID AND constFlag <> true) > 0 then
      open SWV_RefCur for
      SELECT * FROM BizFormWizardModule WHERE coalesce(bitActive,true) = true ORDER BY tintSortOrder;
   ELSE
      open SWV_RefCur for
      SELECT * FROM BizFormWizardModule WHERE coalesce(bitActive,true) = true ORDER BY tintSortOrder;
   end if;
   RETURN;
END; $$;


