-- Stored procedure definition script USP_GetUserHourlyRate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUserHourlyRate(v_numUserCntID NUMERIC(9,0),    
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select coalesce(monHourlyRate,0) as "monHourlyRate" from UserMaster
   where numUserDetailId = v_numUserCntID
   and UserMaster.numDomainID = v_numDomainID;
END; $$;












