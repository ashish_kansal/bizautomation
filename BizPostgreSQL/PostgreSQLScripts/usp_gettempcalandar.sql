-- Stored procedure definition script USP_GetTempCalandar for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTempCalandar(v_StartDateTimeUtc TIMESTAMP,
v_itemId VARCHAR(250),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from tmpCalandarTbl where StartDateTimeUtc = v_StartDateTimeUtc and itemId = v_itemId;
END; $$;












