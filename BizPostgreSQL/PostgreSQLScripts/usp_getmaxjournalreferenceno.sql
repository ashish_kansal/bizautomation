-- Stored procedure definition script USP_GetMaxJournalReferenceNo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMaxJournalReferenceNo(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT MAX(numJournalReferenceNo)+1 AS numJournalReferenceNo FROM General_Journal_Header WHERE numDomainId = v_numDomainId;
END; $$;              












