-- Stored procedure definition script usp_GetFieldAndDescription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetFieldAndDescription(   
--
v_vcTableName VARCHAR(100) DEFAULT '',
	v_vcOrderBy VARCHAR(10) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

	/*select  syscolumns.NAme ,sysproperties.Value,systypes.name as DataType   from syscolumns , sysobjects  ,sysproperties,systypes where
 syscolumns.id  = sysobjects.id 
and sysproperties.id =  sysobjects.id 
and sysproperties.SmallId = syscolumns.Colid
and systypes.xtype=syscolumns.xtype
and sysobjects.Name  =  @vcTableName*/

-- the below query will return the Views Field Name and Data type
   If v_vcOrderBy = 'yes' then

      open SWV_RefCur for
	  SELECT information_schema.columns.column_name As "NAme", information_schema.columns.data_type AS "DataType"  FROm information_schema.columns WHERE table_name = LOWER(v_vcTableName);
   else
      open SWV_RefCur for
      SELECT information_schema.columns.column_name As "NAme", information_schema.columns.data_type AS "DataType"  FROm information_schema.columns WHERE table_name = LOWER(v_vcTableName)
      Order By information_schema.columns.column_name DESC;
   end if;
   RETURN;
END; $$;


