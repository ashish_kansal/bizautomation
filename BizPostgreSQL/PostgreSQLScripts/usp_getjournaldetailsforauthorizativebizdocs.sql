-- Stored procedure definition script USP_GetJournalDetailsForAuthorizativeBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetJournalDetailsForAuthorizativeBizDocs(v_numOppId NUMERIC(9,0) DEFAULT 0,  
  v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJD.numTransactionId as numTransactionId from General_Journal_Header GJH
   join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
   Where GJH.numOppId = v_numOppId And GJH.numOppBizDocsId = v_numOppBizDocsId And chBizDocItems = 'OI'
   And (GJD.numChartAcntId = 2 Or GJD.numChartAcntId = 16 Or GJD.numChartAcntId = 17 Or GJD.numChartAcntId = 19 Or GJD.numChartAcntId = 20);
END; $$;












