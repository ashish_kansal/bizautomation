-- Stored procedure definition script USP_GetCOAwithOpeningBalance_Temp for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCOAwithOpeningBalance_Temp(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numDomainId,numAccountId,vcAccountName,numOriginalOpeningBal,dtOpeningDate,cast((SELECT  cast(numUserDetailId as NUMERIC(18,0)) FROM UserMaster WHERE numDomainID = COA.numDomainId AND bitactivateflag = true LIMIT 1) as VARCHAR(255)) AS numUserCntID FROM Chart_Of_Accounts COA WHERE numOriginalOpeningBal > 0 ORDER BY numDomainId;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[usp_GetCompanydetails]    Script Date: 07/26/2008 16:16:43 ******/













