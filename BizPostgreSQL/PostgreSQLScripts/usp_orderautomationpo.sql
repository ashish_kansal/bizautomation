-- Stored procedure definition script USP_OrderAutomationPO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OrderAutomationPO(v_numOppBizDocId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppId  NUMERIC(9,0);
   v_monPaidAmount  DECIMAL(20,5);
   v_monDealAmount  DECIMAL(20,5);
   v_numDefBizDocId  NUMERIC(9,0);
   v_numDomainId  NUMERIC(9,0);
   v_numOppBizDocItemId  NUMERIC(9,0);
   v_numBizDocId  NUMERIC(9,0);
   v_bitDropShip  BOOLEAN;
   v_numItemCode  NUMERIC(9,0);
   BizDocOrderAuto CURSOR FOR
   Select numOppBizDocItemID,bitDropShip,numItemCode from OpportunityBizDocItems where numOppBizDocID = v_numOppBizDocId;
BEGIN
   select   numoppid, numBizDocId, monAmountPaid INTO v_numOppId,v_numBizDocId,v_monPaidAmount FROM    OpportunityBizDocs WHERE   numOppBizDocsId = v_numOppBizDocId;

	

   select   numDomainId INTO v_numDomainId from OpportunityMaster where numOppId = v_numOppId;



   v_monDealAmount := GetDealAmount(v_numOppId,LOCALTIMESTAMP,v_numOppBizDocId);


   select   numAuthoritativeSales INTO v_numDefBizDocId from AuthoritativeBizDocs where numDomainId = v_numDomainId;



   OPEN BizDocOrderAuto;

   FETCH NEXT FROM BizDocOrderAuto Into v_numOppBizDocItemId,v_bitDropShip,v_numItemCode;
   WHILE FOUND LOOP
      if coalesce(v_bitDropShip,false) = true and v_numDefBizDocId = v_numBizDocId then
				
         PERFORM USP_MangeAutoPOGeneration(v_numParentOppID := v_numOppId,v_numOppBizDocId := v_numOppBizDocId,v_numItemCode := v_numItemCode,
         v_numDomainId := v_numDomainId,v_numOppBizDocItemId := v_numOppBizDocItemId,
         v_monDealAmount := v_monDealAmount,
         v_monPaidAmount := v_monPaidAmount);
      ELSEIF v_numDefBizDocId = v_numBizDocId
      then
				
         PERFORM USP_MangeAutoPOGenerationRule2(v_numParentOppID := v_numOppId,v_numOppBizDocId := v_numOppBizDocId,v_numItemCode := v_numItemCode,
         v_numDomainId := v_numDomainId,v_numOppBizDocItemId := v_numOppBizDocItemId,
         v_monDealAmount := v_monDealAmount,
         v_monPaidAmount := v_monPaidAmount);
      end if;
      FETCH NEXT FROM BizDocOrderAuto into v_numOppBizDocItemId,v_bitDropShip,v_numItemCode;
   END LOOP;

   close BizDocOrderAuto;
   RETURN;
END; $$;


