CREATE OR REPLACE FUNCTION USP_GETShippingLabels(v_numBoxID NUMERIC(9,0),
      v_vcOrderIds TEXT,
      v_numDomainID NUMERIC(10,0),
      v_tintMode SMALLINT DEFAULT 0,
      v_vcOppBizDocIds TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocType  TEXT;
   v_intRow  INTEGER;
   v_intCnt  INTEGER;
BEGIN
   IF v_tintMode = 0 then
            
      open SWV_RefCur for
      SELECT  vcShippingLabelImage
		--		   [vcTrackingNumber]
      FROM    ShippingBox
      WHERE   numBoxID = v_numBoxID;
   end if;
	
   IF v_tintMode = 1 then
      select   coalesce(numBizDocType,0) INTO v_numBizDocType FROM OpportunityOrderStatus WHERE numDomainID = v_numDomainID;
      RAISE NOTICE '%',v_numBizDocType;
      DROP TABLE IF EXISTS tt_TEMPBIZDOC CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPBIZDOC ON COMMIT DROP AS
         SELECT ROW_NUMBER() OVER(ORDER BY numOppBizDocsId) AS ID, numOppBizDocsId 
         FROM OpportunityBizDocs OBD
         INNER JOIN OpportunityMaster OM ON OM.numOppId = OBD.numoppid
         WHERE 1 = 1
         AND OM.numDomainId = v_numDomainID
         AND OBD.numoppid IN(SELECT ID FROM SplitIDs(v_vcOrderIds,','))
         AND coalesce(OBD.numBizDocId,0) =  cast(NULLIF(v_numBizDocType,'') as NUMERIC(18,0));
      v_intRow := 0;
      select   COUNT(numOppBizDocsId) INTO v_intCnt FROM tt_TEMPBIZDOC;
      RAISE NOTICE '%',v_intCnt;
      DROP TABLE IF EXISTS tt_TEMPIMAGES CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIMAGES
      (
         numShippingReportId		NUMERIC(18,0),
         numBoxID				NUMERIC(18,0),
         vcBoxName				VARCHAR(100),
         vcShippingLabelImage	TEXT,
         numShippingCompany		NUMERIC(18,0)
      );
      WHILE(v_intRow < v_intCnt) LOOP
         v_intRow := v_intRow::bigint+1;
--						SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow
--						SELECT TOP 1 numShippingReportId FROM dbo.ShippingReport 
--														WHERE 1=1
--														AND numDomainID = 135
--														AND numOppBizDocId = (SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow)
--														ORDER BY numShippingReportId DESC
						
         INSERT INTO tt_TEMPIMAGES
         SELECT SB.numShippingReportID,numBoxID,vcBoxName,coalesce(vcShippingLabelImage,''),numShippingCompany AS numShippingCompany FROM ShippingBox SB
         INNER JOIN ShippingReport SR ON SR.numShippingReportID = SB.numShippingReportID
         WHERE 1 = 1
         AND  SB.numShippingReportID =(SELECT  numShippingReportId FROM ShippingReport
            WHERE 1 = 1
            AND numDomainID = v_numDomainID
            AND numOppBizDocId =(SELECT numOppBizDocsId FROM tt_TEMPBIZDOC WHERE ID = v_intRow)
            ORDER BY numShippingReportId DESC LIMIT 1);
      END LOOP;
				
				--SELECT * FROM #tempBizDoc
      open SWV_RefCur for
      SELECT * FROM tt_TEMPIMAGES;
				
      DROP TABLE IF EXISTS tt_TEMPBIZDOC CASCADE;
   end if;
		
   IF v_tintMode = 2 then
        
      open SWV_RefCur for
      SELECT  SB.numShippingReportID,numOppBizDocId,
					numBoxID,
					vcBoxName,
					coalesce(vcShippingLabelImage,'') AS vcShippingLabelImage,
					numShippingCompany AS numShippingCompany
      FROM    ShippingBox SB
      INNER JOIN ShippingReport SR ON SR.numShippingReportID = SB.numShippingReportID
      WHERE   1 = 1
      AND SR.numOppBizDocId IN (SELECT Id FROM SplitIDs(v_vcOppBizDocIds,','));
   end if;    


   IF v_tintMode = 3 then
        
      open SWV_RefCur for
      SELECT
      SB.numShippingReportID,
				OM.numOppId,
				coalesce(vcpOppName,'') AS vcPOppName,
				numBoxID,
				vcBoxName,
				coalesce(vcShippingLabelImage,'') AS vcShippingLabelImage,
				numShippingCompany AS numShippingCompany
      FROM
      ShippingBox SB
      INNER JOIN
      ShippingReport SR
      ON
      SR.numShippingReportID = SB.numShippingReportID
      INNER JOIN
      OpportunityMaster OM
      ON
      SR.numOppID = OM.numOppId
      WHERE
      SR.numDomainID = v_numDomainID
      AND SR.numOppID IN (SELECT Distinct Id FROM SplitIDs(v_vcOrderIds,','))
      AND POSITION('.zpl' IN vcShippingLabelImage) = 0;

      open SWV_RefCur2 for
      SELECT
      SB.numShippingReportID,
				OM.numOppId,
				coalesce(vcpOppName,'') AS vcPOppName,
				numBoxID,
				vcBoxName,
				coalesce(vcShippingLabelImage,'') AS vcShippingLabelImage,
				numShippingCompany AS numShippingCompany
      FROM
      ShippingBox SB
      INNER JOIN
      ShippingReport SR
      ON
      SR.numShippingReportID = SB.numShippingReportID
      INNER JOIN
      OpportunityMaster OM
      ON
      SR.numOppID = OM.numOppId
      WHERE
      SR.numDomainID = v_numDomainID
      AND SR.numOppID IN (SELECT Distinct Id FROM SplitIds(v_vcOrderIds,','))
      AND POSITION('.zpl' IN vcShippingLabelImage) > 0;
   end if;
   RETURN;
END; $$;
    


