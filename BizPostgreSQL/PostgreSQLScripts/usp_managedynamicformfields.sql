-- Stored procedure definition script USP_ManageDynamicFormFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDynamicFormFields(v_numFormId NUMERIC(9,0),                             
 v_numAuthGroupID NUMERIC(9,0),  
 v_numDomainId NUMERIC(9,0),
 v_strFomFld TEXT,
 v_numBizDocTemplateID NUMERIC(9,0) DEFAULT 0,
 v_numSubFormId NUMERIC(18,0) DEFAULT 0,
 v_numAssignTo NUMERIC(18,0) DEFAULT 0,
 v_numDripCampaign NUMERIC(18,0) DEFAULT 0,
 v_bitByPassRoutingRules BOOLEAN DEFAULT false,
 v_bitDripCampaign BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(coalesce(v_numSubFormId,0) > 0) then

      UPDATE
      LeadsubForms
      SET
      numAssignTo = coalesce(v_numAssignTo,0),numDripCampaign = coalesce(v_numDripCampaign,0),
      bitByPassRoutingRules = v_bitByPassRoutingRules,bitDripCampaign = v_bitDripCampaign
      WHERE
      numDomainID = v_numDomainId AND numSubFormId = v_numSubFormId;
   end if;

   Delete from  DycFormConfigurationDetails
   where numFormID = v_numFormId AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
   and numAuthGroupID = v_numAuthGroupID
   and numDomainID = v_numDomainId
   and coalesce(numRelCntType,0) = v_numBizDocTemplateID;
   
	insert into DycFormConfigurationDetails
	(
		numFormID
		,numSubFormId
		,numFieldID
		,intColumnNum
		,intRowNum
		,boolAOIField
		,numAuthGroupID
		,numDomainID
		,numRelCntType
		,bitCustom
	)
	select 
		v_numFormId
		,coalesce(v_numSubFormId,0)
		,CAST(replace(replace(X.numFormFieldId,'R',''),'C','') AS NUMERIC)
		,X.intColumnNum
		,X.intRowNum
		,X.boolAOIField
		,v_numAuthGroupID
		,v_numDomainId
		,v_numBizDocTemplateID
		,Case when POSITION('C' IN X.numFormFieldId) > 0 then true else false end 
	from
	(
		SELECT 
			unnest(xpath('//numFormFieldId/text()', v_strFomFld::xml))::text::VARCHAR as numFormFieldId,
			unnest(xpath('//vcFieldType/text()', v_strFomFld::xml))::text::CHAR(1) as vcFieldType,
			unnest(xpath('//intColumnNum/text()', v_strFomFld::xml))::text::integer as intColumnNum,
			unnest(xpath('//intRowNum/text()', v_strFomFld::xml))::text::integer as intRowNum,
			unnest(xpath('//boolAOIField/text()', v_strFomFld::xml))::text::BOOLEAN as boolAOIField
	) X;      
  
   RETURN;
END; $$;


