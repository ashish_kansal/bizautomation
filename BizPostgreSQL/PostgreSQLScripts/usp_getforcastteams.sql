-- Stored procedure definition script USP_GetForCastTeams for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetForCastTeams(v_numUserCntid NUMERIC(9,0),  
v_numDomainId NUMERIC(9,0),  
v_tintType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numListitemid as VARCHAR(255)),cast(vcdata as VARCHAR(255)) from ForReportsByTeam
   join Listdetails
   on numListitemid = numteam
   where ForReportsByTeam.numUserCntID = v_numUserCntid
   and ForReportsByTeam.numDomainID = v_numDomainId
   and tintType = v_tintType;
END; $$;












