-- Stored procedure definition script usp_GetProjectStatusForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetProjectStatusForReport(               
  --                
v_numDomainID NUMERIC,                  
 v_dtDateFrom TIMESTAMP,                  
 v_numUserCntID NUMERIC DEFAULT 0,                               
 v_intType NUMERIC DEFAULT 0,                
 v_tintRights SMALLINT DEFAULT 1,
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_dtDateFrom = '1753-01-01 12:00:00' then 
      v_dtDateFrom := null;
   end if;      
      
   If v_tintRights = 1 then
 
      IF v_dtDateFrom is null then
 
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         where PM.numdomainId = v_numDomainID
         and PM.numRecOwner = v_numUserCntID;
      ELSE
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join  ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         where PM.numdomainId = v_numDomainID
         and PM.numRecOwner = v_numUserCntID
         AND PS.bintStageComDate <= v_dtDateFrom and  PS.bintStageComDate IS NOT NULL;
      end if;
   end if;                  
                  
   If v_tintRights = 2 then
 
      IF v_dtDateFrom is null then
 
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         left outer join AdditionalContactsInformation ACI
         on PM.numRecOwner = ACI.numContactId
         where PM.numdomainId = v_numDomainID
         and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                  
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      ELSE
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join  ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         left outer join AdditionalContactsInformation ACI
         on PM.numRecOwner = ACI.numContactId
         where PM.numdomainId = v_numDomainID
         and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                  
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         AND PS.bintStageComDate <= v_dtDateFrom and  PS.bintStageComDate IS NOT NULL;
      end if;
   end if;                  
                  
   If v_tintRights = 3 then
 
      IF v_dtDateFrom  is null then
 
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         left outer join DivisionMaster DM
         on PM.numDivisionId = DM.numDivisionID
         and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                   
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         and PM.numRecOwner = v_numUserCntID;
      ELSE
         open SWV_RefCur for
         SELECT distinct(PM.numProId),PS.bintStageComDate as DueDate,
  PM.vcProjectName as ProjectName,
  GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,
  GetProLstStageCompltd(PM.numProId,v_ClientTimeZoneOffset) as LastStageCompleted
         FROM ProjectsMaster PM
         left join  ProjectsStageDetails PS
         on PS.numProId = PM.numProId
         left outer join DivisionMaster DM
         on PM.numDivisionId = DM.numDivisionID
         where PM.numdomainId = v_numDomainID
         and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                   
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         AND PS.bintStageComDate <= v_dtDateFrom  and  PS.bintStageComDate IS NOT NULL;
      end if;
   end if;
   RETURN;
END; $$;


