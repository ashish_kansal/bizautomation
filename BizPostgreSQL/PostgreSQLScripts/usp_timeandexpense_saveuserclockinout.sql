-- Stored procedure definition script USP_TimeAndExpense_SaveUserClockInOut for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_TimeAndExpense_SaveUserClockInOut(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numApprovalComplete INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtFromDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_monHourlyRate  DECIMAL(20,5);
BEGIN
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_dtFromDate);

   IF EXISTS(SELECT numCategoryHDRID FROM timeandexpense WHERE numUserCntID = v_numUserCntID AND numCategory = 1 AND numtype = 7 AND dtToDate IS NULL) then
      select   coalesce(monHourlyRate,0) INTO v_monHourlyRate FROM
      UserMaster WHERE
      numUserDetailId = v_numUserCntID;
      UPDATE
      timeandexpense
      SET
      dtToDate = TIMEZONE('UTC',now()),monAmount = v_monHourlyRate
      WHERE
      numUserCntID = v_numUserCntID
      AND numCategory = 1
      AND numtype = 7
      AND dtToDate IS NULL;
      open SWV_RefCur for
      SELECT 0;
   ELSE
      INSERT INTO timeandexpense(tintTEType,
			numCategory,
			dtFromDate,
			dtToDate,
			bitFromFullDay,
			bittofullday,
			monAmount,
			numOppId,
			numDivisionID,
			txtDesc,
			numtype,
			numUserCntID,
			numDomainID,
			numCaseId,
			numProId,
			numcontractId,
			numOppBizDocsId,
			numTCreatedBy,
			numtModifiedBy,
			dtTCreatedOn,
			dtTModifiedOn,
			numStageId,
			bitReimburse,
			numOppItemID,
			numExpId,
			numApprovalComplete,
			numServiceItemID,
			numClassID)
		VALUES(0,
			1,
			TIMEZONE('UTC',now()),
			NULL,
			false,
			false,
			0,
			0,
			0,
			'',
			7,
			v_numUserCntID,
			v_numDomainID,
			0,
			0,
			0,
			0,
			v_numUserCntID,
			v_numUserCntID,
			TIMEZONE('UTC',now()),
			TIMEZONE('UTC',now()),
			0,
			false,
			0,
			0,
			v_numApprovalComplete,
			0,
			0);
		
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


