-- Stored procedure definition script USP_InsertEmailAddCCAndBCC for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertEmailAddCCAndBCC(v_vcEmailAdd VARCHAR(2000) DEFAULT '',
    v_Type SMALLINT DEFAULT 0,
    v_numEmailHSTRID NUMERIC(9,0) DEFAULT NULL,
    v_EmailName VARCHAR(1000) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_startPos  INTEGER;    
   v_EndPos  INTEGER;    
   v_EndPos1  INTEGER;    
   v_Email  VARCHAR(100);    
   v_Name  VARCHAR(100);    
   v_EmailAdd  VARCHAR(100);    
   v_numEmailId  NUMERIC;
BEGIN
   v_numEmailId := 0;  
  
   select   coalesce(numEmailId,0) INTO v_numEmailId FROM    EmailMaster WHERE   vcEmailId ilike v_vcEmailAdd;  
  
   IF v_vcEmailAdd <> '' then
        
      WHILE POSITION(',' IN v_vcEmailAdd) > 0 LOOP
         v_Email := LTRIM(RTRIM(SUBSTR(v_vcEmailAdd,1,POSITION(',' IN v_vcEmailAdd)
         -1)));
         v_startPos := POSITION('<' IN v_Email);
         v_EndPos := POSITION('>' IN v_Email);
         IF v_startPos > 0 then -- IF email address has ' <> ' in it
                        
            v_EmailAdd := LTRIM(RTRIM(SUBSTR(v_Email,v_startPos+1,v_EndPos -v_startPos -1)));
            v_startPos := POSITION('"' IN v_Email);
            v_EndPos1 := POSITION('"' IN SUBSTR(v_Email,v_startPos+1,LENGTH(v_Email)));
            IF v_EmailName <> '' then
                                
               v_Name := v_EmailName;
            ELSE
               v_Name := SUBSTR(v_Email,v_startPos+1,v_EndPos1 -1);
            end if;
            IF LENGTH(LTRIM(RTRIM(v_EmailAdd))) > 0 then 
                            --MStart Pinkal Patel Date:04-oct-2011 Check Email Id Exist in Email Master Table.
               select   coalesce(numEmailId,0) INTO v_numEmailId FROM    EmailMaster WHERE   vcEmailId ilike v_Email;
            end if;
							--MEnd Pinkal Patel Date:04-oct-2011
            IF v_numEmailId = 0 then
                                
               INSERT  INTO EmailMaster(vcEmailId, vcName)
                                    VALUES(v_EmailAdd, v_Name);
                                    
               v_numEmailId := CURRVAL('EmailMaster_seq');
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId)
                                    VALUES(v_numEmailHSTRID,
                                              v_Name,
                                              v_Type,
                                              v_numEmailId);
                                    
               v_numEmailId := 0;
            ELSE
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId)
                                    VALUES(v_numEmailHSTRID,
                                              v_Name,
                                              v_Type,
                                              v_numEmailId);
            end if;
         ELSE  -- When email address doesn't have ' <>  '
            v_EmailAdd := v_Email;
            v_Name := v_EmailName;
            IF LENGTH(LTRIM(RTRIM(v_EmailAdd))) > 0 then 
                               --MStart Pinkal Patel Date:04-oct-2011 Check Email Id Exist in Email Master Table.
               select   coalesce(numEmailId,0) INTO v_numEmailId FROM    EmailMaster WHERE   vcEmailId ilike v_EmailAdd;
            end if;
							--MEnd Pinkal Patel Date:04-oct-2011
            IF v_numEmailId = 0 then
                                
               INSERT  INTO EmailMaster(vcEmailId, vcName)
                                    VALUES(v_EmailAdd, v_Name);
                                    
               v_numEmailId := CURRVAL('EmailMaster_seq');
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId)
                                    VALUES(v_numEmailHSTRID,
                                              v_Name,
                                              v_Type,
                                              v_numEmailId);
                                    
               v_numEmailId := 0;
            ELSE
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId)
                                    VALUES(v_numEmailHSTRID,
                                              v_Name,
                                              v_Type,
                                              v_numEmailId);
            end if;
         end if;
         v_vcEmailAdd := LTRIM(RTRIM(SUBSTR(v_vcEmailAdd,POSITION(',' IN v_vcEmailAdd)+1,LENGTH(v_vcEmailAdd))));
      END LOOP;
      v_startPos := POSITION('<' IN v_vcEmailAdd);
      v_EndPos := POSITION('>' IN v_vcEmailAdd);
      IF v_startPos > 0 then
                
         v_EmailAdd := SUBSTR(v_vcEmailAdd,v_startPos+1,v_EndPos -v_startPos -1);
         v_startPos := POSITION('"' IN v_vcEmailAdd);
         v_EndPos1 := POSITION('"' IN SUBSTR(v_vcEmailAdd,v_startPos+1,LENGTH(v_vcEmailAdd)));
         IF v_EmailName <> '' then
                        
            v_Name := v_EmailName;
         ELSE
            v_Name := SUBSTR(v_Email,v_startPos+1,coalesce(NULLIF(v_EndPos1::bigint -1,-1),0));
         end if;
         IF LENGTH(LTRIM(RTRIM(v_EmailAdd))) > 0 then
            IF v_numEmailId = 0 then
                            
               INSERT  INTO EmailMaster(vcEmailId, vcName)
                                VALUES(v_EmailAdd, v_Name);
                                
               v_numEmailId := CURRVAL('EmailMaster_seq');
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId)
                                VALUES(v_numEmailHSTRID,
                                          v_Name,
                                          v_Type,
                                          v_numEmailId);
                                
               v_numEmailId := 0;
            ELSE
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId)
                                VALUES(v_numEmailHSTRID,
                                          v_Name,
                                          v_Type,
                                          v_numEmailId);
            end if;
         end if;
      ELSE
         v_EmailAdd := v_vcEmailAdd;
         v_Name := v_EmailName;    
--                PRINT @EmailAdd
--                PRINT @numEmailId
                
         IF LENGTH(LTRIM(RTRIM(v_EmailAdd))) > 0 then
            IF v_numEmailId = 0 then
                            
               INSERT  INTO EmailMaster(vcEmailId, vcName)
                                VALUES(v_EmailAdd, v_Name);
                                
               v_numEmailId := CURRVAL('EmailMaster_seq');
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId)
                                VALUES(v_numEmailHSTRID,
                                          v_Name,
                                          v_Type,
                                          v_numEmailId);
                                
               v_numEmailId := 0;
            ELSE
               INSERT  INTO EmailHStrToBCCAndCC(numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId)
                                VALUES(v_numEmailHSTRID,
                                          v_Name,
                                          v_Type,
                                          v_numEmailId);
            end if;
         end if;
      end if;
   end if;
   RETURN;
END; $$;


