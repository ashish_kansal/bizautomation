-- Stored procedure definition script USP_BizAPIThrottleCounter_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 April 2014
-- Description:	Insert or Update Throttle counter for id
-- =============================================
CREATE OR REPLACE FUNCTION USP_BizAPIThrottleCounter_Save(v_ID VARCHAR(100),
	v_TimeSpan TIMESTAMP,
	v_TotalRequest NUMERIC(18,0),
	v_ExpirationTime BIGINT,
	v_vcBizAPIAcessKey VARCHAR(50))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
   IF EXISTS(SELECT * FROM BizAPIThrottleCounter WHERE vcID = v_ID) then
      UPDATE
      BizAPIThrottleCounter
      SET
      dtTimeSpan = v_TimeSpan,numTotalRequest = v_TotalRequest,bintExpirationTime = v_ExpirationTime,
      vcBizAPIAccessKey = v_vcBizAPIAcessKey
      WHERE
      vcID = v_ID;
   ELSE
		
      INSERT INTO BizAPIThrottleCounter(vcID,dtTimeSpan,numTotalRequest,bintExpirationTime,vcBizAPIAccessKey) VALUES(v_ID,v_TimeSpan,v_TotalRequest,v_ExpirationTime,v_vcBizAPIAcessKey);
   end if;
   RETURN;
END; $$;


