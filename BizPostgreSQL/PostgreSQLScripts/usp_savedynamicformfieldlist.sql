-- Stored procedure definition script USP_saveDynamicFormFieldList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_saveDynamicFormFieldList(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numFormId NUMERIC(9,0) DEFAULT 0,  
v_numFormFieldId NUMERIC(9,0) DEFAULT 0,  
v_vcFormFieldName VARCHAR(50) DEFAULT 0,
v_vcToolTip VARCHAR(1000) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from DynamicFormField_Validation where numDomainID = v_numDomainID and numFormId = v_numFormId and numFormFieldId = v_numFormFieldId) then
	   
      UPDATE DynamicFormField_Validation SET vcNewFormFieldName = v_vcFormFieldName,vcToolTip = v_vcToolTip
      where numDomainID = v_numDomainID and numFormId = v_numFormId and numFormFieldId = v_numFormFieldId;
   ELSE
      insert into DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,vcToolTip)
			 	values(v_numFormId,v_numFormFieldId,v_numDomainID,v_vcFormFieldName,v_vcToolTip);
   end if;
   RETURN;
END; $$;
    


