-- Function definition script GetRecordsCountforCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetRecordsCountforCampaign(v_numCampaignID NUMERIC(9,0),
               v_dtStartDate TIMESTAMP  DEFAULT NULL,
               v_dtEndDate   TIMESTAMP  DEFAULT NULL,
               v_tintFlag    SMALLINT DEFAULT NULL,
               v_tintCRMType SMALLINT DEFAULT NULL)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RecordCount  INTEGER;
   v_numDomainID  NUMERIC(9,0);
BEGIN
   IF v_tintFlag = 0 then --Offline Campaign
      
      select   COUNT(* ) INTO v_RecordCount FROM   DivisionMaster DM WHERE numCampaignID = v_numCampaignID
      AND DM.tintCRMType = v_tintCRMType;
   ELSEIF v_tintFlag = 1
   then --Online Campaign
        
      IF (v_dtStartDate IS NOT NULL
      AND v_dtEndDate IS NOT NULL) then
         select   numDomainID INTO v_numDomainID FROM   CampaignMaster CM WHERE  CM.numCampaignID = v_numCampaignID;
         select   coalesce(COUNT(DISTINCT X.numDivisionID),0) INTO v_RecordCount FROM(SELECT
            DM.numDivisionID
            FROM
            DivisionMaster DM
            WHERE
            DM.tintCRMType = v_tintCRMType
            AND DM.numDomainID = v_numDomainID
            AND DM.numCampaignID = v_numCampaignID
            and DM.bintCreatedDate between  v_dtStartDate And  v_dtEndDate) X;
      end if;
   end if;
   RETURN coalesce(v_RecordCount,0);
END; $$;

