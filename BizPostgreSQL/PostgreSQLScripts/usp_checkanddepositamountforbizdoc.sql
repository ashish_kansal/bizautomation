-- Stored procedure definition script USP_CheckAndDepositAmountForBizdoc for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckAndDepositAmountForBizdoc(v_numDivisionID			NUMERIC(18,0),
	v_numOppBizDocId			NUMERIC(18,0),
	v_numAmount				DECIMAL(20,5),
	v_numDomainID			NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CASE WHEN coalesce(OBD.monDealAmount,0) = v_numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
   WHEN coalesce(OBD.monDealAmount,0) > SUM(coalesce(DD.monAmountPaid,0))+v_numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
   WHEN coalesce(OBD.monDealAmount,0) < SUM(coalesce(DD.monAmountPaid,0))+v_numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
   ELSE 0
   END AS IsPaidInFull,
		   DM.numDivisionID,
		   DM.numDomainId,
		   DD.numOppBizDocsID,
		   coalesce(OBD.monDealAmount,0)  AS monDealAmount,
		   SUM(coalesce(DD.monAmountPaid,0)) AS monAmountPaid,
		   SUM(coalesce(DD.monAmountPaid,0))+v_numAmount AS NewAmountPaid,
		   (SUM(coalesce(DD.monAmountPaid,0))+v_numAmount) -coalesce(OBD.monDealAmount,0) AS OverPaidAmount
   FROM DepositMaster AS DM
   JOIN DepositeDetails AS DD ON DM.numDepositId = DD.numDepositID
   JOIN OpportunityMaster AS OM ON OM.numOppId = DD.numOppID AND OM.numDomainId = DM.numDomainId
   JOIN OpportunityBizDocs AS OBD ON OBD.numOppBizDocsId = DD.numOppBizDocsID AND OM.numOppId = OBD.numoppid
   WHERE DM.numDomainId = v_numDomainID
   AND DM.numDivisionID = v_numDivisionID
   AND DD.numOppBizDocsID = v_numOppBizDocId
   GROUP BY OBD.monDealAmount,DM.numDivisionID,DM.numDomainId,DD.numOppBizDocsID;
END; $$;













