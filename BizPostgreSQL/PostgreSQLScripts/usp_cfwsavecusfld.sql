DROP FUNCTION IF EXISTS USP_cfwSaveCusfld;

CREATE OR REPLACE FUNCTION USP_cfwSaveCusfld(v_RecordId NUMERIC(9,0) DEFAULT null,          
v_strDetails TEXT DEFAULT '',          
v_PageId SMALLINT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intCnt  INTEGER;
   v_intCntFields  INTEGER;
   v_numFieldID  NUMERIC(9,0);
   v_numDomainID  NUMERIC(18,0);
   v_numUserID  NUMERIC(18,0);       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
   v_intCntCon  INTEGER;
   v_intCntFieldsCon  INTEGER;
   v_numFieldIDCon  NUMERIC(9,0);
   v_numDomainIDCon  NUMERIC(18,0);
   v_numUserIDCon  NUMERIC(18,0);          
          
   v_intCntCase  INTEGER;
   v_intCntFieldsCase  INTEGER;
   v_numFieldIDCase  NUMERIC(9,0);
   v_numDomainIDCase  NUMERIC(18,0);
   v_numUserIDCase  NUMERIC(18,0);          
        
   v_intCntOpp  INTEGER;
   v_intCntFieldsOpp  INTEGER;
   v_numFieldIDOpp  NUMERIC(9,0);
   v_numDomainIDOpp  NUMERIC(18,0);
   v_numUserIDOpp  NUMERIC(18,0);      
    
   v_intCntPro  INTEGER;
   v_intCntFieldsPro  INTEGER;
   v_numFieldIDPro  NUMERIC(9,0);
   v_numDomainIDPro  NUMERIC(18,0);
   v_numUserIDPro  NUMERIC(18,0);
BEGIN
	DROP TABLE IF EXISTS tt_TEMPcfwSaveCusfld CASCADE;
    CREATE TEMPORARY TABLE tt_TEMPcfwSaveCusfld
    (
		fld_id NUMERIC(18,0),
		FldDTLID NUMERIC(18,0),
		Value VARCHAR(1000)
    );

	IF COALESCE(v_strDetails,'') <> '' THEN
		INSERT INTO tt_TEMPcfwSaveCusfld(
			fld_id
			,FldDTLID
			,Value
		)
		SELECT 
			fld_id
			,FldDTLID
			,Value
		FROM
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strDetails AS XML)
			COLUMNS
				id FOR ORDINALITY,
				fld_id NUMERIC(9,0) PATH 'fld_id',
				FldDTLID NUMERIC(9,0) PATH 'flddtlid',
				Value TEXT PATH 'value'
		) AS X;
	END IF;
	      
   if v_PageId = 1 then
      Delete from CFW_FLD_Values where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld WHERE FldDTLID>0) and  RecId = v_RecordId;
      insert into CFW_FLD_Values(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId FROM tt_TEMPcfwSaveCusfld AS X WHERE X.FldDTLID=0;
      update CFW_FLD_Values set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X 
      where X.FldDTLID>0 AND CFW_FLD_Values.FldDTLID = X.FldDTLID;


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

      BEGIN
         CREATE TEMP SEQUENCE tt_CFFields_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_CFFIELDS CASCADE;
      CREATE TEMPORARY TABLE tt_CFFIELDS
      (
         RowID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         fld_id NUMERIC(9,0)
      );
      INSERT INTO tt_CFFIELDS(fld_id)
      SELECT Fld_ID from CFW_FLD_Values where RecId = v_RecordId;
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

      v_intCnt := 0;
      SELECT COUNT(*) INTO v_intCntFields FROM tt_CFFIELDS;
      select   numDomainID, numCreatedBy INTO v_numDomainID,v_numUserID from DivisionMaster where numDivisionID = v_RecordId;
      IF v_intCntFields > 0 then
	
         WHILE(v_intCnt < v_intCntFields) LOOP
			--print 'sahc'
            v_intCnt := v_intCnt::bigint+1;
            select   fld_id INTO v_numFieldID FROM tt_CFFIELDS WHERE RowID = v_intCnt;
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
            PERFORM USP_CFW_FLD_Values_CT(v_numDomainID := v_numDomainID,v_numUserCntID := 0,v_numRecordID := v_RecordId,
            v_numFormFieldId := v_numFieldID);
         END LOOP;
      end if;
   end if;       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
   if v_PageId = 128 then

      insert into CFW_FLD_Values(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE FldDTLID=0;
      update CFW_FLD_Values set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X 
      where X.FldDTLID>0 AND CFW_FLD_Values.FldDTLID = X.FldDTLID;
   end if;        
 
          
   if v_PageId = 4 then
      Delete from CFW_FLD_Values_Cont where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID > 0)
      and  RecId = v_RecordId;
      insert into CFW_FLD_Values_Cont(fld_id,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID = 0;
      update CFW_FLD_Values_Cont set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X 
      where X.FldDTLID>0 AND CFW_FLD_Values_Cont.FldDTLID = X.FldDTLID;

--        
--Delete from CFW_FLD_Values_Cont where RecId=@RecordId          
--insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
    
	
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

      BEGIN
         CREATE TEMP SEQUENCE tt_CFFieldsCon_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_CFFIELDSCON CASCADE;
      CREATE TEMPORARY TABLE tt_CFFIELDSCON
      (
         RowID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         fld_id NUMERIC(9,0)
      );
      INSERT INTO tt_CFFIELDSCON(fld_id)
      SELECT Fld_ID from CFW_FLD_Values_Cont where RecId = v_RecordId;
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

      v_intCntCon := 0;
      SELECT COUNT(*) INTO v_intCntFieldsCon FROM tt_CFFIELDSCON;
      select   numDomainID, numCreatedBy INTO v_numDomainIDCon,v_numUserIDCon from AdditionalContactsInformation where numContactId = v_RecordId;
      IF v_intCntFieldsCon > 0 then
	
         WHILE(v_intCntCon < v_intCntFieldsCon) LOOP
            v_intCntCon := v_intCntCon::bigint+1;
            select   fld_id INTO v_numFieldIDCon FROM tt_CFFIELDSCON WHERE RowID = v_intCntCon;
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
            PERFORM USP_CFW_FLD_Values_Cont_CT(v_numDomainID := v_numDomainIDCon,v_numUserCntID := v_numUserIDCon,v_numRecordID := v_RecordId,
            v_numFormFieldId := v_numFieldIDCon);
         END LOOP;
      end if;
   end if;          
          
   if v_PageId = 3 then
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         fld_id NUMERIC(9,0),
         FldDTLID NUMERIC(9,0),
         Value VARCHAR(1000)
      );
      INSERT INTO tt_TEMP(fld_id,
		FldDTLID,
		Value)
      SELECT
      fld_id,
		FldDTLID,
		Value
      FROM tt_TEMPcfwSaveCusfld;
      DELETE FROM
      CFW_FLD_Values_Case
      WHERE
      FldDTLID NOT IN(SELECT FldDTLID FROM tt_TEMP WHERE FldDTLID > 0)
      AND RecId = v_RecordId
      AND Fld_ID NOT IN(SELECT coalesce(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule = 3);
      INSERT INTO CFW_FLD_Values_Case(Fld_ID,
		Fld_Value,
		RecId)
      SELECT
      fld_id,
		Value,
		v_RecordId
      FROM
      tt_TEMP
      WHERE
      FldDTLID = 0
      AND fld_id NOT IN(SELECT coalesce(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule = 3);
      UPDATE
      CFW_FLD_Values_Case
      SET
      Fld_Value = X.Value
      FROM
      tt_TEMP X
      WHERE
      X.FldDTLID > 0
      AND CFW_FLD_Values_Case.FldDTLID = X.FldDTLID
      AND  CFW_FLD_Values_Case.Fld_ID NOT IN(SELECT coalesce(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule = 3);           
          
--Delete from CFW_FLD_Values_Case where RecId=@RecordId          
--insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
     
	 --Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

      BEGIN
         CREATE TEMP SEQUENCE tt_CFFieldsCase_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_CFFIELDSCASE CASCADE;
      CREATE TEMPORARY TABLE tt_CFFIELDSCASE
      (
         RowID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         fld_id NUMERIC(9,0)
      );
      INSERT INTO tt_CFFIELDSCASE(fld_id)
      SELECT Fld_ID from CFW_FLD_Values_Case where RecId = v_RecordId;
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

      v_intCntCase := 0;
      SELECT COUNT(*) INTO v_intCntFieldsCase FROM tt_CFFIELDSCASE;
      select   numDomainID, numCreatedby INTO v_numDomainIDCase,v_numUserIDCase from Cases where numCaseId = v_RecordId;
      IF v_intCntFieldsCase > 0 then
	
         WHILE(v_intCntCase < v_intCntFieldsCase) LOOP
			--print 'sahc'
            v_intCntCase := v_intCntCase::bigint+1;
            select   fld_id INTO v_numFieldIDCase FROM tt_CFFIELDSCASE WHERE RowID = v_intCntCase;
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
            PERFORM USP_CFW_FLD_Values_Case_CT(v_numDomainID := v_numDomainIDCase,v_numUserCntID := v_numUserIDCase,v_numRecordID := v_RecordId,
            v_numFormFieldId := v_numFieldIDCase);
         END LOOP;
      end if;
   end if;          
        
   if v_PageId = 5 then

      Delete from CFW_FLD_Values_Item where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID>0)
      and  RecId = v_RecordId;
      insert into CFW_FLD_Values_Item(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID=0;
      update CFW_FLD_Values_Item set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X
      where X.FldDTLID>0 AND CFW_FLD_Values_Item.FldDTLID = X.FldDTLID;
   end if;         
          
   if v_PageId = 6 or v_PageId = 2 then
      Delete from CFW_Fld_Values_Opp where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID>0)
      and  RecId = v_RecordId;
      insert into CFW_Fld_Values_Opp(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID=0;
      update CFW_Fld_Values_Opp set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X 
      where X.FldDTLID>0 AND CFW_Fld_Values_Opp.FldDTLID = X.FldDTLID; 
   end if;      
    
   if v_PageId = 7 or    v_PageId = 8 then

      Delete from CFW_Fld_Values_Product where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID>0)
      and  RecId = v_RecordId;
      insert into CFW_Fld_Values_Product(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID=0;
      update CFW_Fld_Values_Product set Fld_Value = X.Value  from tt_TEMPcfwSaveCusfld X
      where X.FldDTLID > 0 AND CFW_Fld_Values_Product.FldDTLID = X.FldDTLID;
   end if;     
          
   if v_PageId = 11 then
      Delete from CFW_Fld_Values_Pro where FldDTLID not in(SELECT FldDTLID FROM tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID>0)
      and  RecId = v_RecordId;
      insert into CFW_Fld_Values_Pro(Fld_ID,Fld_Value,RecId)
      select X.fld_id,X.Value,v_RecordId  from tt_TEMPcfwSaveCusfld X WHERE X.FldDTLID=0;
      update CFW_Fld_Values_Pro set Fld_Value = X.Value from tt_TEMPcfwSaveCusfld X
      where X.FldDTLID > 0 AND CFW_Fld_Values_Pro.FldDTLID = X.FldDTLID;
       
--Delete from CFW_Fld_Values_Pro where RecId=@RecordId          
--insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X    


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

      BEGIN
         CREATE TEMP SEQUENCE tt_CFFieldsPro_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_CFFIELDSPRO CASCADE;
      CREATE TEMPORARY TABLE tt_CFFIELDSPRO
      (
         RowID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         fld_id NUMERIC(9,0)
      );
      INSERT INTO tt_CFFIELDSPRO(fld_id)
      SELECT Fld_ID from CFW_Fld_Values_Pro where RecId = v_RecordId;
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

      v_intCntPro := 0;
      SELECT COUNT(*) INTO v_intCntFieldsPro FROM tt_CFFIELDSPRO;
      select   numdomainId, numCreatedBy INTO v_numDomainIDPro,v_numUserIDPro from ProjectsMaster where numProId = v_RecordId;
      IF v_intCntFieldsPro > 0 then
	
         WHILE(v_intCntPro < v_intCntFieldsPro) LOOP
			--print 'sahc'
            v_intCntPro := v_intCntPro::bigint+1;
            select   fld_id INTO v_numFieldIDPro FROM tt_CFFIELDSPRO WHERE RowID = v_intCntPro;
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
            PERFORM USP_CFW_FLD_Values_Pro_CT(v_numDomainID := v_numDomainIDPro,v_numUserCntID := v_numUserIDPro,v_numRecordID := v_RecordId,
            v_numFormFieldId := v_numFieldIDPro);
         END LOOP;
      end if;
   end if;

--Added by Neelam Kapila || 12/01/2017 - Added condition to save custom items to CFW_Fld_Values_OppItems table
   IF v_PageId = 18 then

      INSERT INTO CFW_Fld_Values_OppItems(fld_id
		,Fld_Value
		,RecId)
      SELECT
      X.fld_id
		,X.Value
		,v_RecordId
      FROM(SELECT * FROM tt_TEMPcfwSaveCusfld) X
      WHERE
      X.fld_id NOT IN(SELECT fld_id FROM CFW_Fld_Values_OppItems WHERE RecId = v_RecordId);
      UPDATE
      CFW_Fld_Values_OppItems CFVOppItems
      SET
      Fld_Value = X.Value
      FROM(SELECT * FROM tt_TEMPcfwSaveCusfld) X
      WHERE
      CFVOppItems.fld_id = X.fld_id AND CFVOppItems.RecId = v_RecordId;
   end if; 

   DROP TABLE IF EXISTS tt_TEMPcfwSaveCusfld CASCADE;

   RETURN;
END; $$;


