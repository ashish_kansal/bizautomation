-- Stored procedure definition script USP_GetShippingReportList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingReportList(v_numOppId    NUMERIC(9,0),
               v_numOppBizDocsId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numShippingReportId,
		   ('Shipping Report-' || CAST(numShippingReportId AS VARCHAR(10))) AS vcReportName,
           numOppBizDocId,
           vcBizDocID,
           LD.vcData AS ShippingCompany,
           numShippingCompany,
           (SELECT SUM(coalesce(monShippingRate,0)) FROM ShippingBox WHERE numShippingReportID = SR.numShippingReportID) AS SumTotal,
           SR.numDomainID,
           SR.numOppID
   FROM   ShippingReport SR
   LEFT JOIN Listdetails LD
   ON LD.numListItemID = SR.numShippingCompany
   INNER JOIN OpportunityBizDocs BD
   ON BD.numOppBizDocsId = SR.numOppBizDocId
   WHERE  numOppBizDocId  = v_numOppBizDocsId
   AND SR.numOppID = v_numOppId;
END; $$;

