-- Stored procedure definition script USP_GetPortalBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPortalBizDocs(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numListItemID as VARCHAR(255)),cast(vcData as VARCHAR(255)) from PortalBizDocs P
   join Listdetails
   on numListItemID = numBizDocID  and P.numDomainID = v_numDomainID;
END; $$;












