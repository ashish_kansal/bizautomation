-- Stored procedure definition script Resource_SelByName for PostgreSQL
CREATE OR REPLACE FUNCTION Resource_SelByName(v_ResourceName VARCHAR(64) -- name of the resource.  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(Resource.ResourceID as VARCHAR(255)),
  cast(Resource.ResourceName as VARCHAR(255)),
  cast(Resource.ResourceDescription as VARCHAR(255)),
  cast(Resource.EmailAddress as VARCHAR(255)),
  cast(coalesce(ResourcePreference.EnableEmailReminders,0) as VARCHAR(255)) AS EnableEmailReminders,
  cast(Resource._ts as VARCHAR(255))
   FROM
   Resource LEFT JOIN ResourcePreference ON Resource.ResourceID = ResourcePreference.ResourceID
   WHERE
   Resource.ResourceID = CAST(v_ResourceName AS NUMERIC(9,0));
END; $$;













