-- Stored procedure definition script usp_PerformDuplicateCntSearch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_PerformDuplicateCntSearch(v_vcCRMTypes VARCHAR(5),                                                  
v_numCompanyType NUMERIC,                                                  
v_vcOrgCreationStartDateRange VARCHAR(10),                                                  
v_vcOrgCreationEndDateRange VARCHAR(10),                                                  
v_boolNoItemHistory BOOLEAN,                                                  
v_boolSameParentName BOOLEAN,                                                  
v_boolSameDivisionName BOOLEAN,                                        
v_boolSamePhoneFlag BOOLEAN,                                        
v_boolIgnoreNamesFlag BOOLEAN,                                                  
v_vcFirstName VARCHAR(50),                                        
v_vcLastName VARCHAR(50),                                        
v_vcMatchingField VARCHAR(50),                                            
v_vcSortColumn VARCHAR(50),                                         
v_vcSortOrder VARCHAR(50),                                    
v_numNosRowsReturned INTEGER,                                       
v_numCurrentPage INTEGER,                                                  
v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql                                                    
   --SET THE RANGE OF RECORDS                      
   AS $$
   DECLARE
   v_intTopOfResultSet  INTEGER;
   v_intStartOfResultSet  INTEGER;                                    
   v_ContTempCustomTableColumns  VARCHAR(4000);                                              
   v_vcOrgCompanyName  VARCHAR(20);                        
   v_vcOrgDivisionName  VARCHAR(35);                        
   v_vcContPhone  VARCHAR(20);                        
   v_vcContEmail  VARCHAR(20);                        
   v_vcContFirstName  VARCHAR(20);                        
   v_vcContLastName  VARCHAR(20);                        
   v_vcContMatchingFieldName  VARCHAR(25);                        
                        
   v_vcComparisionFieldsNames  VARCHAR(200);                        
   v_ContTempCustomTableDataEntryQuery  VARCHAR(4000);                         
                        
   v_DuplicateCntFixedQuery  VARCHAR(8000);                                                  
   v_DuplicateCntCRMTypesQuery  VARCHAR(60);                                                  
   v_DuplicateCompanyTypeQuery  VARCHAR(50);                                                  
   v_DuplicateDateRangeQuery  VARCHAR(100);                        
   v_DuplicateItemHistoryQuery  VARCHAR(100);                                                 
   v_DuplicateParentOrgNameQuery  VARCHAR(200);                                                  
   v_DuplicateDivisionNameQuery  VARCHAR(200);                                                  
   v_DuplicatePhoneQuery  VARCHAR(150);                                                
   v_IgnoreNamesQuery  VARCHAR(150);                                                
   v_DuplicateMatchingFieldQuery  VARCHAR(150);                                                  
   v_GroupOrderColumnFieldQuery  VARCHAR(1000);                                                  
   v_vcDuplicateCompleteQuery  VARCHAR(4000);                      
   v_vcDuplicateCntSelect  VARCHAR(4000);                      
   v_vcDuplicateTableDelete  VARCHAR(200);                      
   v_vcDuplicateCountQuery  VARCHAR(200);
BEGIN
   v_intTopOfResultSet :=(v_numNosRowsReturned::bigint*v_numCurrentPage::bigint);                    
   v_intStartOfResultSet := v_numNosRowsReturned::bigint*(v_numCurrentPage::bigint -1);                       
   v_vcSortColumn := Replace(v_vcSortColumn,'[Date/Time  Created Last Modified & By Whom]', 
   '4');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[First / Last Name]','5');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Phone Number]','6');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Cell Number]','7');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Email Address]','8');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Zip/Postal]','9');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Has open action items]','10');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Has action item history]','11');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Parent Organization/ Division]','12');                      
                       
   --FIND THE CUSTOM FIELDS FOR THE CONTACT                        
   v_ContTempCustomTableColumns := fn_OrgAndContCustomFieldsColumns(4::SMALLINT,0::SMALLINT);                        
   --PRINT '@ContTempCustomTableColumns: ' +  @ContTempCustomTableColumns                        
   --CREATE A STRING OF FIELDS WHICH ARE TO BE COMPARED                        
   IF v_boolSameParentName = true then --Same Parent Organization                        

      v_vcOrgCompanyName := 'vcOrgCompanyName,';
   ELSE
      v_vcOrgCompanyName := '';
   end if;                        
                                                  
   IF v_boolSameDivisionName = true then  --Same Division name                        
      v_vcOrgDivisionName := 'vcOrgDivisionName,numDivisionId,';
   ELSE
      v_vcOrgDivisionName := '';
   end if;                        
                        
   IF v_boolSamePhoneFlag = true then  --Same Contact Phone number                        
      v_vcContPhone := 'vcContPhone,';
   ELSE
      v_vcContPhone := '';
   end if;                        
                                  
   v_vcContFirstName := 'vcContFirstName,';                        
   v_vcContLastName := 'vcContLastName,';                     
   v_vcContEmail := 'vcContEmail,';                        
                        
   IF v_vcMatchingField <> '' then --Other Matching Fields                        
      v_vcContMatchingFieldName := coalesce(v_vcMatchingField,'') || ',';
   ELSE
      v_vcContMatchingFieldName := '';
   end if;                        
   v_vcComparisionFieldsNames := coalesce(v_vcContFirstName,'') || coalesce(v_vcContLastName,'') || coalesce(v_vcContEmail,'');                    
   IF v_boolSameParentName = true then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcOrgCompanyName,'');
   end if;                        
   IF v_boolSameDivisionName = true then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcOrgDivisionName,'');
   end if;                        
   IF v_boolSamePhoneFlag = true then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcContPhone,'');
   end if;                        
      
   RAISE NOTICE '%',v_vcMatchingField;      
   RAISE NOTICE '%',fn_OrgAndContCustomFieldsColumns(4::SMALLINT,1::SMALLINT);      
   RAISE NOTICE '%',v_vcComparisionFieldsNames;      
   IF v_vcMatchingField <> '' AND (POSITION(v_vcMatchingField IN v_vcComparisionFieldsNames) <= 0 AND POSITION(v_vcMatchingField IN coalesce(fn_OrgAndContCustomFieldsColumns(4::SMALLINT,1::SMALLINT),'')) <= 0) then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcContMatchingFieldName,'');
   end if;                        
                                                              
   v_vcComparisionFieldsNames := SUBSTR(v_vcComparisionFieldsNames,1,LENGTH(v_vcComparisionFieldsNames) -1);                        
   --PRINT '@vcComparisionFieldsNames: ' + @vcComparisionFieldsNames                        
                        
   v_ContTempCustomTableDataEntryQuery := ' SELECT DISTINCT TOP 100 PERCENT numContactId' || ',' || coalesce(v_vcComparisionFieldsNames,'');                        
        
   IF (v_ContTempCustomTableColumns Is Not NULL) then
      v_ContTempCustomTableDataEntryQuery := coalesce(v_ContTempCustomTableDataEntryQuery,'') || ', ' || fn_OrgAndContCustomFieldsColumns(4::SMALLINT,2::SMALLINT);
   end if;                                      
                        
   v_ContTempCustomTableDataEntryQuery := coalesce(v_ContTempCustomTableDataEntryQuery,'') || ' FROM vw_Records4Duplicates ORDER BY numContactId';                              
                                
   v_ContTempCustomTableDataEntryQuery := 'Select TOP 100 PERCENT * INTO #tmpTableContactTempFlds FROM (' || coalesce(v_ContTempCustomTableDataEntryQuery,'') || ') AS tmpTableCustomFlds';                          
                      
                                    
   v_DuplicateCntFixedQuery := 'SELECT Distinct TOP 100 Percent Rec1.numCompanyId, Rec1.numDivisionId, Rec1.numContactId,                                        
 dbo.fn_GetLatestEntityEditedDateAndUser(0, Rec1.numContactId, ''Cnt'') AS [Date/Time  Created Last Modified & By Whom],                                            
 LTRIM(Rec1.vcContFirstName) + Case WHEN (LTrim(Rec1.vcContFirstName) <> '''' AND LTrim(Rec1.vcContLastName) <> '''') THEN '', '' ELSE '''' END + Rec1.vcContLastName As [First / Last Name],                                        
  Rec1.vcContPhone AS [Phone Number],                                              
  Rec1.vcContCell AS [Cell Number],                                             
  Rec1.vcContEmail AS [Email Address],                                        
  Rec1.vcContPostalCode as [Zip/Postal],                                         
  IsNull(recActionItem.boolActionItemOpen,''No'') AS [Has open action items],                                              
  IsNull(recActionItem.boolActionItemHistory,''No'') AS [Has action item history],                                         
  LTRIM(Rec1.vcOrgCompanyName + ''/'' + Rec1.vcOrgDivisionName) AS [Parent Organization/ Division]                                        
 FROM    vw_Records4Duplicates Rec1 INNER JOIN #tmpTableContactTempFlds recCustom ON Rec1.numContactId = recCustom.numContactId                        
  LEFT OUTER JOIN                                               
  (SELECT numContactId, Case WHEN (SUM(numStatus) > 0 AND (Convert(Int, SUM(numStatus)) % 356) = 0) THEN ''Yes'' ELSE ''No'' END AS boolActionItemOpen,                                               
   Case WHEN SUM(Cast(bitClosedFlag As Int)) > 0 THEN ''Yes'' ELSE ''No'' END AS boolActionItemHistory                                               
   FROM Communication GROUP BY numContactId                                            
  ) recActionItem                                              
    ON Rec1.numContactId = recActionItem.numContactId INNER JOIN                                                
   #tmpTableContactTempFlds Rec2                                            
   ON (Rec1.numContactId <> Rec2.numContactId AND Rec1.vcContFirstName + Rec1.vcContLastName = Rec2.vcContFirstName + Rec2.vcContLastName)                                 
   AND Rec1.numEmpStatus = 658 AND Rec1.numDomainID = ' || SUBSTR(Cast(v_numDomainID AS VARCHAR(30)),1,30);  
--PRINT @DuplicateCntFixedQuery                                                  
                                                  
   v_DuplicateCntCRMTypesQuery := ' WHERE Convert(NVarchar(1), Rec1.vcContCRMType) IN (' || coalesce(v_vcCRMTypes,'') || ') ';                                                  
--PRINT @DuplicateCntCRMTypesQuery                                                  
                                                  
   v_DuplicateCompanyTypeQuery := '';                        
   IF v_numCompanyType <> 0 then
      v_DuplicateCompanyTypeQuery := ' AND Rec1.vcOrgCompanyType = ' || SUBSTR(CAST(v_numCompanyType AS VARCHAR(30)),1,30);
   end if;                                                  
--PRINT @DuplicateCompanyTypeQuery                                                  
                                                  
   v_DuplicateDateRangeQuery := '';                        
   IF LTRIM(v_vcOrgCreationStartDateRange) <> '' AND LTrim(v_vcOrgCreationEndDateRange) <> '' then
      v_DuplicateDateRangeQuery := ' AND (Rec1.vcContCreatedDate >= ''' || coalesce(v_vcOrgCreationStartDateRange,'') || '''' ||
      ' AND Rec1.vcContCreatedDate <= ''' || coalesce(v_vcOrgCreationEndDateRange,'') || ''')';
   ELSEIF LTRIM(v_vcOrgCreationStartDateRange) <> '' AND LTrim(v_vcOrgCreationEndDateRange) = ''
   then
      v_DuplicateDateRangeQuery := ' AND (Rec1.vcContCreatedDate >= ''' || coalesce(v_vcOrgCreationStartDateRange,'') || ''') ';
   ELSEIF LTRIM(v_vcOrgCreationStartDateRange) = '' AND LTrim(v_vcOrgCreationEndDateRange) <> ''
   then
      v_DuplicateDateRangeQuery := ' AND (Rec1.vcContCreatedDate <= ''' || coalesce(v_vcOrgCreationEndDateRange,'') || ''') ';
   end if;                          
--PRINT @DuplicateDateRangeQuery                                                  
                                                  
                                                  
   v_DuplicateItemHistoryQuery := '';                                                 
   IF v_boolNoItemHistory = true then
      v_DuplicateItemHistoryQuery := ' AND (recActionItem.boolActionItemHistory =  ''No'' OR recActionItem.boolActionItemHistory IS NULL) ';
   end if;                                                  
--PRINT @DuplicateItemHistoryQuery                                                  
                                                  
                                                
   v_DuplicateParentOrgNameQuery := '';                                                
   IF v_boolSameParentName = true then
      v_DuplicateParentOrgNameQuery := ' AND Rec1.vcOrgCompanyName = Rec2.vcOrgCompanyName                                                
 AND Rec1.vcContEmail = Rec2.vcContEmail ';
   end if;                                                
--PRINT @DuplicateParentOrgNameQuery                                             
                                        
   v_DuplicateDivisionNameQuery := '';                                                
   IF v_boolSameDivisionName = true then
      v_DuplicateDivisionNameQuery := ' AND Rec1.vcOrgDivisionName = Rec2.vcOrgDivisionName                                                
 AND Rec1.numDivisionId <> Rec2.numDivisionId ';
   end if;                                                
--PRINT @DuplicateDivisionNameQuery                                                  
                                            
                             
   v_DuplicatePhoneQuery := '';                                                
   IF v_boolSamePhoneFlag = true then
      v_DuplicatePhoneQuery := ' AND Rec1.vcContPhone = Rec2.vcContPhone ';
   end if;                                                  
--PRINT @DuplicatePhoneQuery                                                      
                                            
                                    
   v_IgnoreNamesQuery := '';                                                
   IF v_boolIgnoreNamesFlag = true then
      v_IgnoreNamesQuery := Case WHEN (LTrim(v_vcFirstName) <> '' AND LTrim(v_vcLastName) <> '') THEN ' AND Rec1.vcContFirstName + Rec1.vcContLastName <> ''' || coalesce(v_vcFirstName,'') || coalesce(v_vcLastName,'') || ''''
      WHEN (LTrim(v_vcFirstName) <> '') THEN ' AND Rec1.vcContFirstName <> ''' || coalesce(v_vcFirstName,'') || ''''
      WHEN (LTrim(v_vcLastName) <> '') THEN ' AND Rec1.vcContLastName <> ''' || coalesce(v_vcLastName,'') || ''''
      ELSE '' END;
   end if;        
--PRINT @IgnoreNamesQuery                                                     
                                                  
                                                
   v_DuplicateMatchingFieldQuery := '';                                                
   IF v_vcMatchingField <> '' then

      IF v_ContTempCustomTableColumns IS NOT NULL AND coalesce(POSITION(substring(v_vcMatchingField from v_ContTempCustomTableColumns) IN v_vcMatchingField),0) > -1 then
         v_DuplicateMatchingFieldQuery := ' AND recCustom.' || coalesce(v_vcMatchingField,'') || ' = Rec2.' || coalesce(v_vcMatchingField,'') || ' ';
      ELSE
         v_DuplicateMatchingFieldQuery := ' AND Rec1.' || coalesce(v_vcMatchingField,'') || ' = Rec2.' || coalesce(v_vcMatchingField,'') || ' ';
      end if;
   end if;                        
--PRINT @DuplicateMatchingFieldQuery                                                  
                                    
                                                
   v_GroupOrderColumnFieldQuery := ' ORDER BY ' || coalesce(v_vcSortColumn,'') || ' ' || coalesce(v_vcSortOrder,'');                                              
--PRINT @GroupOrderColumnFieldQuery                
--PRINT (@DuplicateCntFixedQuery + @DuplicateCntCRMTypesQuery + @DuplicateCompanyTypeQuery + @DuplicateDateRangeQuery + @DuplicateItemHistoryQuery + @DuplicateParentOrgNameQuery + @DuplicateDivisionNameQuery +  @DuplicatePhoneQuery +                      
  
   
      
         
         
             
             
                
                  
-- @IgnoreNamesQuery + @DuplicateMatchingFieldQuery + @GroupOrderColumnFieldQuery)                                                
                      
   v_vcDuplicateCompleteQuery := coalesce(v_DuplicateCntFixedQuery,'') || coalesce(v_DuplicateCntCRMTypesQuery,'') || coalesce(v_DuplicateCompanyTypeQuery,'') || coalesce(v_DuplicateDateRangeQuery,'') || coalesce(v_DuplicateItemHistoryQuery,'') || coalesce(v_DuplicateParentOrgNameQuery,'') || coalesce(v_DuplicateDivisionNameQuery,'') ||  coalesce(v_DuplicatePhoneQuery,'')
   ||
   coalesce(v_IgnoreNamesQuery,'') || coalesce(v_DuplicateMatchingFieldQuery,'') || coalesce(v_GroupOrderColumnFieldQuery,'');                      
                      
   v_vcDuplicateCompleteQuery := CHR(10) || 'Select IDENTITY(int) AS numRow, * INTO #tmpTableDuplicateContact FROM (' || coalesce(v_vcDuplicateCompleteQuery,'') || ') AS tmpTableAllFields';                      
                      
   v_vcDuplicateCountQuery := CHR(10) || 'SELECT IsNull(Max(numRow),0) AS DupCount FROM #tmpTableDuplicateContact';                
   v_vcDuplicateCntSelect := coalesce(v_ContTempCustomTableDataEntryQuery,'') || coalesce(v_vcDuplicateCompleteQuery,'') || CHR(10) || 'SELECT * FROM #tmpTableDuplicateContact WHERE numRow > ' || SUBSTR(CAST(v_intStartOfResultSet AS VARCHAR(9)),1,9) ||  ' AND numRow <= ' || SUBSTR(CAST(v_intTopOfResultSet AS
   VARCHAR(9)),1,9) || ' ORDER BY ' || Cast(case when v_vcSortColumn = '' then 0 else v_vcSortColumn:: INTEGER end || 1 As VARCHAR(30)) || ' ' || coalesce(v_vcSortOrder,'') || coalesce(v_vcDuplicateCountQuery,'');                    
   v_vcDuplicateTableDelete := CHR(10) || 'DROP TABLE #tmpTableContactTempFlds' || CHR(10) || 'DROP TABLE #tmpTableDuplicateContact';                      
   RAISE NOTICE '%',coalesce(v_vcDuplicateCntSelect,'') || coalesce(v_vcDuplicateTableDelete,'');                      
   EXECUTE coalesce(v_vcDuplicateCntSelect,'') || coalesce(v_vcDuplicateTableDelete,'');
   RETURN;
END; $$;


