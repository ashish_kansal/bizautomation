-- Stored procedure definition script USP_GetTimeExpWeekly for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTimeExpWeekly(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                
v_numDomainID NUMERIC(9,0) DEFAULT 0,                
v_FromDate TIMESTAMP DEFAULT NULL ,              
v_numCategoryType INTEGER DEFAULT 0 ,        
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strDate  TIMESTAMP;
BEGIN
   v_strDate := v_FromDate;    

   IF coalesce(v_numUserCntID,0) > 0 then

      open SWV_RefCur for
      select
      '-'   as day0,
 GetDaydetails(v_numUserCntID,v_strDate::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset) as Day1,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '1 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day2,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '2 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day3,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '3 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day4,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '4 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day5,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '5 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day6,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '6 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day7,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '7 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day8,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '8 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day9,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '9 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day10,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '10 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day11,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '11 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day12,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '12 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day13,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '13 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day14,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '14 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day15,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '15 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day16,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '16 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day17,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '17 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day18,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '18 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day19,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '19 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day20,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '20 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day21,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '21 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day22,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '22 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day23,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '23 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day24,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '24 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day25,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '25 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day26,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '26 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day27,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '27 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day28,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '28 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day29,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '29 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day30,
 GetDaydetails(v_numUserCntID,(v_strDate+INTERVAL '30 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day31,
(select coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60),0)  as time
         from timeandexpense where dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 1 and numUserCntID = v_numUserCntID and numtype = 1)
      as BillableHours,
(select coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60),0)  as time
         from timeandexpense where dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 1 and numUserCntID = v_numUserCntID and numtype = 2)
      as NonBillableHours,
 (select round(CAST(coalesce(sum(monAmount),0) AS NUMERIC),1)    as  monAmount
         from timeandexpense
         where
         dtFromDate
         between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 2 and numUserCntID = v_numUserCntID and numtype IN(1,6))
      as BillableAmount,
 (select round(CAST(coalesce(sum(monAmount),0) AS NUMERIC),1)    as  monAmount
         from timeandexpense
         where  dtFromDate
         between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 2 and numUserCntID = v_numUserCntID and numtype = 2)  as NonBillableAmount;
   ELSE
      open SWV_RefCur for
      select
      '-'   as day0,
 GetDayDetailsForDomain(v_numDomainID,v_strDate::DATE,v_numCategoryType::NUMERIC(9,0),v_ClientTimeZoneOffset) as Day1,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '1 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day2,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '2 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day3,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '3 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day4,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '4 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day5,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '5 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day6,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '6 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day7,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '7 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day8,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '8 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day9,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '9 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)   as Day10,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '10 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day11,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '11 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day12,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '12 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day13,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '13 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day14,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '14 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day15,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '15 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day16,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '16 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day17,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '17 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day18,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '18 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day19,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '19 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day20,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '20 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day21,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '21 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day22,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '22 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day23,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '23 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day24,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '24 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day25,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '25 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day26,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '26 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day27,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '27 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day28,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '28 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day29,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '29 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day30,
 GetDayDetailsForDomain(v_numDomainID,(v_strDate+INTERVAL '30 day')::DATE,v_numCategoryType::NUMERIC(9,0),
      v_ClientTimeZoneOffset)  as Day31,
(select coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60),0)  as time
         from timeandexpense where dtFromDate+INTERVAL '-330 minute' between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 1 and numDomainID = v_numDomainID and numtype = 1)
      as BillableHours,
(select coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60),0)  as time
         from timeandexpense where dtFromDate+INTERVAL '-330 minute' between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 1 and numDomainID = v_numDomainID and numtype = 2)
      as NonBillableHours,
 (select round(CAST(coalesce(sum(monAmount),0) AS NUMERIC),1)    as  monAmount
         from timeandexpense
         where
         dtFromDate
         between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 2 and numDomainID = v_numDomainID and numtype IN(1,6))
      as BillableAmount,
 (select round(CAST(coalesce(sum(monAmount),0) AS NUMERIC),1)    as  monAmount
         from timeandexpense
         where  dtFromDate
         between v_strDate and v_strDate+INTERVAL '30 day'
         and numCategory = 2 and numDomainID = v_numDomainID and numtype = 2)  as NonBillableAmount;
   end if;
   RETURN;
END; $$; 


