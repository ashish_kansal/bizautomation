-- Stored procedure definition script USP_CommissionPayPeriod_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_CommissionPayPeriod_Save(INOUT v_numComPayPeriodID NUMERIC(18,0) ,
	 v_numDomainID NUMERIC(18,0),
	 v_dtStart DATE,
	 v_dtEnd DATE,
	 v_tintPayPeriod INTEGER,
	 v_numUserID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	--NEW PAY PERIOD COMMISSION CALCULATION
      IF v_numComPayPeriodID = 0 then
	
         INSERT INTO CommissionPayPeriod(numDomainID,
			dtStart,
			dtEnd,
			tintPayPeriod,
			numCreatedBy,
			dtCreated)
		VALUES(v_numDomainID,
			v_dtStart,
			v_dtEnd,
			v_tintPayPeriod,
			v_numUserID,
			LOCALTIMESTAMP) RETURNING numComPayPeriodID INTO v_numComPayPeriodID;
	-- RECALCULATE PAY PERIOD COMMISSION
      ELSE
         UPDATE
         CommissionPayPeriod
         SET
         numModifiedBy = v_numUserID,dtModified = LOCALTIMESTAMP
         WHERE
         numComPayPeriodID = v_numComPayPeriodID;
      end if;

	PERFORM USP_CalculateCommission(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_numDomainID,v_ClientTimeZoneOffset := v_ClientTimeZoneOffset);
    PERFORM USP_CalculateTimeAndExpense(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_numDomainID,v_ClientTimeZoneOffset := v_ClientTimeZoneOffset);
    PERFORM USP_CalculateCommissionSalesReturn(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_numDomainID,v_ClientTimeZoneOffset := v_ClientTimeZoneOffset);
    PERFORM USP_CalculateCommissionOverPayment(v_numDomainID := v_numDomainID);
	
	open SWV_RefCur for SELECT v_numComPayPeriodID;
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
	GET STACKED DIAGNOSTICS
	my_ex_state   = RETURNED_SQLSTATE,
	my_ex_message = MESSAGE_TEXT,
	my_ex_detail  = PG_EXCEPTION_DETAIL,
	my_ex_hint    = PG_EXCEPTION_HINT,
	my_ex_ctx     = PG_EXCEPTION_CONTEXT;

	raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


