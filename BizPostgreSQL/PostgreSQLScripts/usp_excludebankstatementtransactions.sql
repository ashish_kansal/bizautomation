-- Stored procedure definition script USP_ExcludeBankStatementTransactions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Added By : Joseph ******/
/****** Exclude the Transaction Detail in Biz Mapping from Table BankStatementTransaction******/


CREATE OR REPLACE FUNCTION USP_ExcludeBankStatementTransactions(v_numTransactionID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   UPDATE BankStatementTransactions
   SET bitIsActive = false
   WHERE
   numTransactionID = v_numTransactionID;
   RETURN;
END; $$;



