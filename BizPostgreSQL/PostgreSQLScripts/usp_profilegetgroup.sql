-- Stored procedure definition script USP_ProfileGetGroup for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProfileGetGroup(v_numDomainID INTEGER ,
v_numUserCntID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcEmailGroupName as "Name",numEmailGroupID as "GroupId" from ProfileEmailGroup where numDomainID = v_numDomainID  and numCreatedBy = v_numUserCntID;
END; $$;












