-- Stored procedure definition script USP_DelInboxItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DelInboxItems(v_numEmailHstrID NUMERIC(9,0)  ,           
v_mode BOOLEAN,
v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_mode = true then
             
/*Bug reported by Audiogenx that they can not delete mails, cause: Email was added to correspondance of Project*/
      DELETE FROM Correspondence WHERE numEmailHistoryID = v_numEmailHstrID;
      delete FROM EmailHistory
      where numEmailHstrID = v_numEmailHstrID;
      delete FROM EmailHStrToBCCAndCC
      where numEmailHstrID = v_numEmailHstrID;
      delete FROM EmailHstrAttchDtls
      where numEmailHstrID = v_numEmailHstrID;
   else  
 --OLD NODE ID REQUIRED TO GET FOLDER NAME WHERE EMAIL WAS BEFORE MOVED TO DELETED FOLDER
 --BECAUSE TO DELETE EMAIL IN GAMIL FOLDER NAME IS REQUIRED
      UPDATE EmailHistory SET numOldNodeID = numNodeId WHERE numEmailHstrID = v_numEmailHstrID;
      update EmailHistory set tintType = 3,numNodeId = coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND bitSystem = true AND numFixID = 2),
      2) where numEmailHstrID = v_numEmailHstrID;
   end if;
   RETURN;
END; $$;


