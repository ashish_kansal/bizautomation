-- Function definition script CheckIfPriceRuleApplicableToItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckIfPriceRuleApplicableToItem(v_numRuleID NUMERIC(18,0), v_numItemID NUMERIC(18,0), v_numDivisionID NUMERIC(18,0))
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_isRuleApplicable  BOOLEAN DEFAULT 0;

   v_isStep2RuleApplicable  BOOLEAN DEFAULT 0;
   v_isStep3RuleApplicable  BOOLEAN DEFAULT 0;

   v_tempStep2  INTEGER;
   v_tempStep3  INTEGER;
BEGIN
   select   tintStep2, tintStep3 INTO v_tempStep2,v_tempStep3 FROM PriceBookRules WHERE numPricRuleID = v_numRuleID;

/* Check if rule is applied to given item */
   IF v_tempStep2 = 1 then --Apply to items individually

      IF(SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = v_numRuleID AND tintType = 1 AND numValue = v_numItemID) > 0 then
         v_isStep2RuleApplicable := true;
      end if;
   ELSEIF v_tempStep2 = 2
   then --Apply to items with the selected item classifications

      IF(SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = v_numRuleID AND tintType = 2 AND numValue =(SELECT numItemClassification FROM Item WHERE numItemCode = v_numItemID)) > 0 then
         v_isStep2RuleApplicable := true;
      end if;
   ELSEIF v_tempStep2 = 3
   then -- All Items

      v_isStep2RuleApplicable := true;
   end if;

/* If step 2 rule is not applicable then there is no need to go for check in step 3 */
   IF v_isStep2RuleApplicable = true then

      IF v_tempStep3 = 1 then --Apply to customers individually
	
         IF(SELECT COUNT(*) FROM PriceBookRuleDTL WHERE numRuleID = v_numRuleID AND numValue = v_numDivisionID) > 0 then
            v_isStep3RuleApplicable := true;
         end if;
      ELSEIF v_tempStep3 = 2
      then --Apply to customers with the following Relationships & Profiles
	
         IF(SELECT
         COUNT(*)
         FROM
         PriceBookRuleDTL
         WHERE
         numRuleID = v_numRuleID AND
         numValue =(SELECT
            coalesce(numCompanyType,0)
            FROM
            CompanyInfo
            WHERE
            numCompanyId =(SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID)) AND
         numProfile =(SELECT
            coalesce(vcProfile,0)
            FROM
            CompanyInfo
            WHERE
            numCompanyId =(SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID))) > 0 then
            v_isStep3RuleApplicable := true;
         end if;
      ELSEIF v_tempStep3 = 3
      then --All Customers
	
         v_isStep3RuleApplicable := true;
      end if;
   end if;

   IF v_isStep2RuleApplicable = true AND v_isStep3RuleApplicable = true then

      v_isRuleApplicable := true;
   end if;  

   return v_isRuleApplicable;
END; $$;

