CREATE OR REPLACE FUNCTION usp_Update_StagePercentageDetails(v_strStageDetail TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(v_strStageDetail,1,10) <> '' then
            
      UPDATE  StagePercentageDetails
      SET     tintPercentage = X.tintPercentage,bintModifiedDate = LOCALTIMESTAMP
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strStageDetail AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numStageDetailsId NUMERIC(9,0) PATH 'numStageDetailsId',
				tintPercentage SMALLINT PATH 'tintPercentage'
		) AS X 
      WHERE  StagePercentageDetails.numStageDetailsId = X.numStageDetailsId;
     
   end if;
   RETURN;
END; $$;



