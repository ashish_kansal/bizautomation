-- Stored procedure definition script USP_Communication_SnoozeReminder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Communication_SnoozeReminder(v_numCommID NUMERIC(18,0),
	v_LastSnoozDateTimeUtc TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Communication SET LastSnoozDateTimeUtc = v_LastSnoozDateTimeUtc WHERE numCommId = v_numCommID;
   RETURN;
END; $$;


