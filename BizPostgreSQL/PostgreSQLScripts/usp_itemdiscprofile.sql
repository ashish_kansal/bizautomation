CREATE OR REPLACE FUNCTION USP_ItemDiscProfile(v_numItemID NUMERIC(9,0) DEFAULT 0,
v_byteMode SMALLINT DEFAULT 0,
v_numRelID NUMERIC(9,0) DEFAULT 0,
v_numProID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select numDiscProfileID,numItemID,I.numDivisionID,vcCompanyName || ', ' || vcDivisionName as Company,decDisc,bitApplicable
      from  ItemDiscountProfile I
      join DivisionMaster D
      on D.numDivisionID = I.numDivisionID
      join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      where numItemID = v_numItemID and I.numDivisionID > 0;
   end if;
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numDiscProfileID,numItemID,numRelID,numProID,decDisc,bitApplicable,fn_GetListItemName(numRelID) || ', ' || fn_GetListItemName(numProID) as RelPro
      from  ItemDiscountProfile where numItemID = v_numItemID and numProID > 0;
   end if;
   RETURN;
END; $$;


