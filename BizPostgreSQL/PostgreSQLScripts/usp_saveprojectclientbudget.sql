-- Stored procedure definition script usp_SaveProjectClientBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveProjectClientBudget(v_numDomainID NUMERIC(9,0),                             
v_numProId NUMERIC(9,0) DEFAULT null  ,    
v_numClientBudget NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE ProjectsMaster SET numClientBudget = v_numClientBudget WHERE numdomainId = v_numDomainID AND numProId = v_numProId;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/



