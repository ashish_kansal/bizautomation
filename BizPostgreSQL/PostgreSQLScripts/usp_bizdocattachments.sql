-- Stored procedure definition script USP_BizDocAttachments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDocAttachments(v_numBizDocID NUMERIC(9,0) DEFAULT 0,  
 v_numBizDocAtID NUMERIC(9,0) DEFAULT 0,  
 v_numDomainID NUMERIC(9,0) DEFAULT 0,  
 v_vcURL VARCHAR(1000) DEFAULT '',  
 v_vcDocName VARCHAR(100) DEFAULT '',
 v_numBizDocTempID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOrder  NUMERIC(9,0);
BEGIN
   if v_numBizDocAtID = 0 then
 
      select   coalesce(max(numOrder),0)+1 INTO v_numOrder from BizDocAttachments where numBizDocID = v_numBizDocID and numDomainID = v_numDomainID and numBizDocTempID = v_numBizDocTempID;
      insert into BizDocAttachments(numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)
 values(v_numBizDocID,v_numDomainID,v_vcURL,v_numOrder,v_vcDocName,v_numBizDocTempID);
 
      if not exists(select * from BizDocAttachments where numBizDocID = v_numBizDocID and numDomainID = v_numDomainID and vcDocName = 'BizDoc' and numBizDocTempID = v_numBizDocTempID) then
 
         insert into BizDocAttachments(numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)
  values(v_numBizDocID,v_numDomainID,'BizDoc',v_numOrder+1,'BizDoc',v_numBizDocTempID);
      end if;
   ELSEIF v_numBizDocAtID > 0
   then
 
      update BizDocAttachments set vcURL = v_vcURL,vcDocName = v_vcDocName,numBizDocTempID = v_numBizDocTempID
      where numAttachmntID = v_numBizDocAtID;
      if not exists(select * from BizDocAttachments where numBizDocID = v_numBizDocID and numDomainID = v_numDomainID and vcDocName = 'BizDoc' and numBizDocTempID = v_numBizDocTempID) then
 
         select   coalesce(max(numOrder),0) INTO v_numOrder from BizDocAttachments where numBizDocID = v_numBizDocID and numDomainID = v_numDomainID  and numBizDocTempID = v_numBizDocTempID;
         insert into BizDocAttachments(numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)
  values(v_numBizDocID,v_numDomainID,'BizDoc',v_numOrder+1,'BizDoc',v_numBizDocTempID);
      end if;
   end if;
   RETURN;
END; $$;


