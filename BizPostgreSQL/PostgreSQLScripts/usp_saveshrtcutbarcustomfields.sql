-- Stored procedure definition script USP_SaveShrtCutBarCustomFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveShrtCutBarCustomFields(v_numGroupID NUMERIC,  
v_numDomainId NUMERIC,  
v_strName VARCHAR(150),  
v_strUrl VARCHAR(200)  ,
v_numContactId NUMERIC DEFAULT 0,
v_numTabId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into ShortCutBar(vcLinkName,
Link,
bitdefault,
numGroupID,
numDomainID  ,
numContactId,numTabId)
values(v_strName,
v_strUrl,
false,
v_numGroupID,
v_numDomainId  ,
v_numContactId,v_numTabId);
RETURN;
END; $$;


