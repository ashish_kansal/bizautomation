DROP FUNCTION IF EXISTS USP_GetProspectsList1;

CREATE OR REPLACE FUNCTION USP_GetProspectsList1
(	
	v_CRMType NUMERIC,
    v_numUserCntID NUMERIC,
    v_tintUserRightType SMALLINT,
    v_tintSortOrder NUMERIC DEFAULT 4,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_SortChar CHAR(1) DEFAULT '0',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_numProfile NUMERIC DEFAULT NULL,
    v_bitPartner BOOLEAN DEFAULT NULL,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_bitActiveInActive BOOLEAN DEFAULT NULL,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '' ,
	v_SearchText VARCHAR(100) DEFAULT '',
	INOUT v_TotRecs INTEGER  DEFAULT NULL, 
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_tintPerformanceFilter  SMALLINT;
	v_Nocolumns  SMALLINT DEFAULT 0;
	v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
	v_numUserGroup  NUMERIC(18,0);
	v_strColumns  TEXT;
	v_tintOrder  SMALLINT;                                                      
	v_vcFieldName  VARCHAR(50);                                                      
	v_vcListItemType  VARCHAR(3);                                                 
	v_vcAssociatedControlType  VARCHAR(20);                                                      
	v_numListID  NUMERIC(9,0);                                                      
	v_vcDbColumnName  VARCHAR(200);                          
	v_WhereCondition  TEXT;                           
	v_vcLookBackTableName  VARCHAR(2000);                    
	v_bitCustom  BOOLEAN;
	v_bitAllowEdit BOOLEAN;                 
	v_numFieldId  NUMERIC;  
	v_bitAllowSorting  BOOLEAN;              
	v_vcColumnName  VARCHAR(500);  
	v_ListRelID  NUMERIC(9,0); 
	v_SearchQuery  TEXT DEFAULT ''; 
	v_strShareRedordWith  VARCHAR(300);
	v_strExternalUser  TEXT DEFAULT '';
	v_StrSql  TEXT DEFAULT '';
	v_vcCSOrigDbCOlumnName  VARCHAR(50);
	v_vcCSLookBackTableName  VARCHAR(50);
	v_vcCSAssociatedControlType  VARCHAR(50);
	v_firstRec  INTEGER;                                                              
	v_lastRec  INTEGER;                                                              
	v_strFinal  TEXT;
	v_Prefix  VARCHAR(5);
	SWV_RowCount INTEGER;
BEGIN
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'^LIKE?','ilike','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'^LIKE?','ilike','gi');

	drop table IF EXISTS tt_TempGetProspectsList1 CASCADE;

	IF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=1') IN v_vcRegularSearchCriteria),0) > 0 then --Last 3 Months
		v_tintPerformanceFilter := 1;
	ELSEIF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=2') IN v_vcRegularSearchCriteria),0) > 0 then --Last 6 Months
		v_tintPerformanceFilter := 2;
	ELSEIF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance=3') IN v_vcRegularSearchCriteria),0) > 0 then --Last 1 Year
		v_tintPerformanceFilter := 3;
	ELSE
		v_tintPerformanceFilter := 0;
	end if;

	IF POSITION('AD.numBillCountry' IN v_vcRegularSearchCriteria) > 0 then
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry');
	ELSEIF POSITION('AD.numShipCountry' IN v_vcRegularSearchCriteria) > 0 then
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry');
	end if;

	IF POSITION('ADC.vcLastFollowup' IN v_vcRegularSearchCriteria) > 0 then
		v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcLastFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=true))');
	end if;
	IF POSITION('ADC.vcNextFollowup' IN v_vcRegularSearchCriteria) > 0 then
		v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=false))');
	end if;  

	DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFORM
	(
		tintOrder SMALLINT,
		vcDbColumnName VARCHAR(200),
		vcOrigDbColumnName VARCHAR(50),
		vcFieldName VARCHAR(50),
		vcAssociatedControlType VARCHAR(50),
		vcListItemType VARCHAR(3),
		numListID NUMERIC(9,0),
		vcLookBackTableName VARCHAR(50),
		bitCustomField BOOLEAN,
		numFieldId NUMERIC,
		bitAllowSorting BOOLEAN,
		bitAllowEdit BOOLEAN,
		bitIsRequired BOOLEAN,
		bitIsEmail BOOLEAN,
		bitIsAlphaNumeric BOOLEAN,
		bitIsNumeric BOOLEAN,
		bitIsLengthValidation BOOLEAN,
		intMaxLength INTEGER,
		intMinLength INTEGER,
		bitFieldMessage BOOLEAN,
		vcFieldMessage VARCHAR(500),
		ListRelID NUMERIC(9,0),
		intColumnWidth INTEGER,
		bitAllowFiltering BOOLEAN,
		vcFieldDataType CHAR(1)
	);
    
	SELECT 
		coalesce(SUM(TotalRow),0) INTO v_Nocolumns 
	FROM
	(
		SELECT
			COUNT(*) AS TotalRow
		FROM
			View_DynamicColumns
		WHERE
			numFormId = 35
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
		UNION
		SELECT
			COUNT(*) AS TotalRow
		FROM
			View_DynamicCustomColumns
		WHERE
			numFormId = 35
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
	) TotalRows;

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	IF v_Nocolumns = 0 then
		SELECT 
			numGroupID INTO v_numUserGroup 
		FROM 
			UserMaster 
		WHERE 
			numUserDetailId = v_numUserCntID;

		IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 35 AND numRelCntType = 3 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then		
			v_IsMasterConfAvailable := true;
		end if;
	end if;

	--If MasterConfiguration is available then load it otherwise load default columns
	IF v_IsMasterConfAvailable = true then
		INSERT INTO DycFormConfigurationDetails
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT
			35,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,3,1,false,intColumnWidth
		FROM
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId = 35 AND
			View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType = 3 AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
		UNION
		SELECT
			35,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,3,1,1,intColumnWidth
		FROM
			View_DynamicCustomColumnsMasterConfig
		WHERE
			View_DynamicCustomColumnsMasterConfig.numFormId = 35 AND
			View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType = 3 AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;

		INSERT INTO tt_TEMPFORM
		SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId = 35 AND
			View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType = 3 AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
		UNION
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
		FROM
			View_DynamicCustomColumnsMasterConfig
		WHERE
			View_DynamicCustomColumnsMasterConfig.numFormId = 35 AND
			View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType = 3 AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
		ORDER BY
			tintOrder ASC;
	ELSE
		IF v_Nocolumns = 0 then
			INSERT INTO DycFormConfigurationDetails
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				35,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,3,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			where 
				numFormId = 35 and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
			order by 
				tintOrder asc;
		end if;

		INSERT INTO
			tt_TEMPFORM
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM
			View_DynamicColumns
		WHERE
			numFormId = 35
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND coalesce(bitSettingField,false) = true
			AND coalesce(bitCustom,false) = false
		UNION
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
		FROM
			View_DynamicCustomColumns
		WHERE
			numFormId = 35
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND coalesce(bitCustom,false) = true
		ORDER BY
			tintOrder ASC;
	end if;  

	v_strColumns := ' COALESCE(ADC.numContactId,0) numContactId, COALESCE(DM.numDivisionID,0) numDivisionID, COALESCE(DM.numTerID,0)numTerID, COALESCE(ADC.numRecOwner,0) numRecOwner, COALESCE(DM.tintCRMType,0) AS tintCRMType, CMP.vcCompanyName,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail  ';              

	v_tintOrder := 0;                                                      
	v_WhereCondition := '';
	
	SELECT 
		tintOrder+1
		,vcDbColumnName
		,vcFieldName
		,vcAssociatedControlType
		,vcListItemType
		,numListID
		,vcLookBackTableName
		,bitCustomField
		,numFieldId
		,bitAllowSorting
		,bitAllowEdit
		,ListRelID 
	INTO 
		v_tintOrder
		,v_vcDbColumnName
		,v_vcFieldName
		,v_vcAssociatedControlType
		,v_vcListItemType
		,v_numListID
		,v_vcLookBackTableName
		,v_bitCustom
		,v_numFieldId
		,v_bitAllowSorting
		,v_bitAllowEdit
		,v_ListRelID 
	FROM 
		tt_TEMPFORM 
	ORDER BY 
		tintOrder ASC 
	LIMIT 1;            
 
                                               
	WHILE v_tintOrder > 0 LOOP
		IF v_bitCustom = false then
			IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
				v_Prefix := 'ADC.';
			end if;
			IF v_vcLookBackTableName = 'DivisionMaster' then
				v_Prefix := 'DM.';
			end if;

			v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || COALESCE(v_numFieldId,0) || '~0';

			IF v_vcAssociatedControlType = 'Label' and v_vcDbColumnName = 'vcPerformance' then
				v_WhereCondition := coalesce(v_WhereCondition,'') || CONCAT(' LEFT JOIN LATERAL
																			(
																				SELECT
																					SUM(monDealAmount) AS monDealAmount
																					,AVG(ProfitPer) AS fltProfitPer
																				FROM
																				(
																					SELECT
																						OM.numDivisionId
																						,monDealAmount
																						,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																					FROM
																						OpportunityMaster OM
																					INNER JOIN
																						Domain D
																					ON
																						OM.numDomainID = D.numDomainID
																					INNER JOIN
																						OpportunityItems OI
																					ON
																						OM.numOppId = OI.numOppId
																					INNER JOIN
																						Item I
																					ON
																						OI.numItemCode = I.numItemCode
																					LEFT JOIN
																						Vendor V
																					ON
																						V.numVendorID = I.numVendorID
																						AND V.numItemCode = I.numItemCode
																					WHERE
																						OM.numDomainId = ',v_numDomainID,'
																						AND OM.numDivisionId = DM.numDivisionID
																						AND COALESCE(I.bitContainer,false) = false
																						AND tintOppType = 1
																						AND tintOppStatus=1
																						AND 1 = (CASE ',v_tintPerformanceFilter,
					' 
																								WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -3) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																								WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -6) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																								WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(years => -1) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																								ELSE 1
																								END)
																					GROUP BY
																						OM.numDivisionId
																						,OM.numOppId
																						,monDealAmount
																				) T1
																				GROUP BY
																					T1.numDivisionId 
																			) AS TempPerformance ON TRUE ');
				v_strColumns := coalesce(v_strColumns,'') || ', (CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(to_char(TempPerformance.monDealAmount,''FM9,999,999,999,999,999,990.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) "' || coalesce(v_vcColumnName,'') || '"';
			ELSEIF v_vcDbColumnName = 'bitEcommerceAccess' then					
				v_strColumns := coalesce(v_strColumns,'') || ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS "' || coalesce(v_vcColumnName,'') || '"';
			ELSEIF v_vcDbColumnName = 'vcLastFollowup' then
				v_strColumns := coalesce(v_strColumns,'') || ',  COALESCE((CASE WHEN (ADC.numECampaignID > 0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
			ELSEIF v_vcDbColumnName = 'vcNextFollowup' then
				v_strColumns := coalesce(v_strColumns,'') || ',  COALESCE((CASE WHEN  (ADC.numECampaignID >0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
			ELSEIF v_vcAssociatedControlType = 'SelectBox' or v_vcAssociatedControlType = 'ListBox' then
				IF v_vcDbColumnName = 'numPartenerSource' then	
					v_strColumns := coalesce(v_strColumns,'') || ' ,(DM.numPartenerSource) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
					v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource';
				ELSEIF v_vcListItemType = 'LI' then         
					IF v_numListID = 40 then
						v_strColumns := coalesce(v_strColumns,'') || ',L' || COALESCE(v_tintOrder,0) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
						
						IF LENGTH(v_SearchText) > 0 then				
							v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || COALESCE(v_tintOrder,0) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
						end if;
						
						IF v_vcDbColumnName = 'numShipCountry' then			
							v_WhereCondition := coalesce(v_WhereCondition,'')
							|| ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= DM.numDomainID '
							|| ' left Join ListDetails L' || COALESCE(v_tintOrder,0) || ' on L' || COALESCE(v_tintOrder,0) || '.numListItemID=AD2.numCountry ';
						ELSE
							v_WhereCondition := coalesce(v_WhereCondition,'')
							|| ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= DM.numDomainID '
							|| ' left Join ListDetails L' || COALESCE(v_tintOrder,0) || ' on L' || COALESCE(v_tintOrder,0) || '.numListItemID=AD1.numCountry ';
						end if;
					ELSE
						v_strColumns := coalesce(v_strColumns,'') || ',L'
										|| COALESCE(v_tintOrder,0)
										|| '.vcData' || ' "'
										|| coalesce(v_vcColumnName,'') || '"';

						IF LENGTH(v_SearchText) > 0 then			
							v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || COALESCE(v_tintOrder,0) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
						end if;
						
						v_WhereCondition := coalesce(v_WhereCondition,'')
											  || ' left Join ListDetails L'
											  || COALESCE(v_tintOrder,0)
											  || ' on L'
											  || COALESCE(v_tintOrder,0)
											  || '.numListItemID='
											  || coalesce(v_vcDbColumnName,'');
					end if;
				ELSEIF v_vcListItemType = 'S' then     
					IF LENGTH(v_SearchText) > 0 then		
						v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'S' || COALESCE(v_tintOrder,0) || '.vcState ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
					end if;

					IF v_vcDbColumnName = 'numShipState' then					
						v_strColumns := coalesce(v_strColumns,'') || ',ShipState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
						v_WhereCondition := coalesce(v_WhereCondition,'')
						|| ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=true and AD3.numDomainID= DM.numDomainID '
						|| ' left Join State ShipState on ShipState.numStateID=AD3.numState ';
					ELSEIF v_vcDbColumnName = 'numBillState' then
						v_strColumns := coalesce(v_strColumns,'') || ',BillState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
						v_WhereCondition := coalesce(v_WhereCondition,'')
						|| ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=true and AD4.numDomainID= DM.numDomainID '
						|| ' left Join State BillState on BillState.numStateID=AD4.numState ';
					ELSE
						v_strColumns := coalesce(v_strColumns,'') || ',S' || COALESCE(v_tintOrder,0) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
						v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || COALESCE(v_tintOrder,0) || ' on S' || COALESCE(v_tintOrder,0) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
					end if;
				ELSEIF v_vcListItemType = 'T' then                    
					v_strColumns := coalesce(v_strColumns,'') || ',L' || COALESCE(v_tintOrder,0) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
					
					IF LENGTH(v_SearchText) > 0 then											
						v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || COALESCE(v_tintOrder,0) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
					end if;

					v_WhereCondition := coalesce(v_WhereCondition,'')
					|| ' left Join ListDetails L'
					|| COALESCE(v_tintOrder,0)
					|| ' on L'
					|| COALESCE(v_tintOrder,0)
					|| '.numListItemID='
					|| coalesce(v_vcDbColumnName,'');
				ELSEIF v_vcListItemType = 'C' then
					v_strColumns := coalesce(v_strColumns,'') || ',C' || COALESCE(v_tintOrder,0) || '.vcCampaignName' || ' "' || coalesce(v_vcColumnName,'') || '"';
					
					IF LENGTH(v_SearchText) > 0 then							
						v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'C' || COALESCE(v_tintOrder,0) || '.vcCampaignName ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
					end if;
					
					v_WhereCondition := coalesce(v_WhereCondition,'')
					|| ' left Join CampaignMaster C'
					|| COALESCE(v_tintOrder,0)
					|| ' on C'
					|| COALESCE(v_tintOrder,0)
					|| '.numCampaignId=DM.'
					|| coalesce(v_vcDbColumnName,'');
				ELSEIF v_vcListItemType = 'U' then  
					v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
					
					IF LENGTH(v_SearchText) > 0 then								  
						v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
					end if;
				end if;
			ELSEIF v_vcAssociatedControlType = 'DateField' then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => -1)  AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => 1)  AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || '),'|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
            
				IF LENGTH(v_SearchText) > 0 then					
					v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
				end if;
			ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'Label' then
				IF v_vcDbColumnName = 'vcCompactContactDetails' then
				   v_strColumns := coalesce(v_strColumns,'') || ' ,'''' AS vcCompactContactDetails';
				ELSE
				   v_strColumns := coalesce(v_strColumns,'') || ','
				   || CASE WHEN v_vcDbColumnName = 'numAge'
				   THEN 'CAST(to_char(timezone(''utc'', now()),''yyyy'') AS INT) - CAST(to_char(bintDOB,''yyyy'') AS INT)'
				   WHEN v_vcDbColumnName = 'numDivisionID'
				   THEN 'DM.numDivisionID'
				   ELSE v_vcDbColumnName
				   END || ' "' || coalesce(v_vcColumnName,'') || '"';
				end if;
				
				IF LENGTH(v_SearchText) > 0 then		
					v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) ||
					CASE WHEN v_vcDbColumnName = 'numAge'
					THEN 'year(timezone(''utc'', now()))-year(bintDOB)'
					WHEN v_vcDbColumnName = 'numDivisionID'
					THEN 'DM.numDivisionID'
					ELSE v_vcDbColumnName
					END || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
				end if;
			ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith' then
				v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT 
																			string_agg(CONCAT(A.vcFirstName,'' '',A.vcLastName,CASE WHEN COALESCE(SR.numContactType,0)>0 THEN CONCAT(''('' , fn_GetListItemName(SR.numContactType) , '')'') ELSE '''' END),'','')
																		FROM ShareRecord SR 
																		JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
																		JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
																		WHERE 
																			SR.numDomainID=DM.numDomainID 
																			AND SR.numModuleID=1 
																			AND SR.numRecordID=DM.numDivisionID
																			AND UM.numDomainID=DM.numDomainID 
																			and UM.numDomainID=A.numDomainID),'''')) "' || coalesce(v_vcColumnName,'') || '"';
			ELSE
				v_strColumns := coalesce(v_strColumns,'') || ', ' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
			end if;
		ELSEIF v_bitCustom = true then
			v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || COALESCE(v_numFieldId,0) || '~1';
			
			select 
				FLd_label
				,fld_type
				,CONCAT('Cust',Fld_id) 
			INTO 
				v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName 
			FROM  
				CFW_Fld_Master 
			WHERE 
				CFW_Fld_Master.Fld_id = v_numFieldId;

			IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
				v_strColumns := coalesce(v_strColumns,'') || ',CFW' || COALESCE(v_tintOrder,0) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';

				v_WhereCondition := coalesce(v_WhereCondition,'') 
									|| ' left Join CFW_FLD_Values CFW'
									|| COALESCE(v_tintOrder,0)
									|| ' on CFW' || COALESCE(v_tintOrder,0) || '.Fld_Id=' || COALESCE(v_numFieldId,0)
									|| 'and CFW' || COALESCE(v_tintOrder,0) || '.RecId=DM.numDivisionId   ';
			ELSEIF v_vcAssociatedControlType = 'CheckBox' then
				v_strColumns := coalesce(v_strColumns,'') 
								|| ',case 
										when COALESCE(CFW' || COALESCE(v_tintOrder,0) || '.Fld_Value,0)='''' 
										then 0 
										else COALESCE(CFW' || COALESCE(v_tintOrder,0) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';

				v_WhereCondition := coalesce(v_WhereCondition,'')
									|| ' left Join CFW_FLD_Values CFW' || COALESCE(v_tintOrder,0)
									|| ' on CFW' || COALESCE(v_tintOrder,0) || '.Fld_Id='|| COALESCE(v_numFieldId,0)
									|| ' and CFW' || COALESCE(v_tintOrder,0) || '.RecId=DM.numDivisionId   ';
			ELSEIF v_vcAssociatedControlType = 'DateField' then
				v_strColumns := coalesce(v_strColumns,'')
								|| ',FormatedDateFromDate(CFW'
								|| COALESCE(v_tintOrder,0)
								|| '.Fld_Value,'
								|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
								|| ')  "' || coalesce(v_vcColumnName,'') || '"';
				v_WhereCondition := coalesce(v_WhereCondition,'')
									|| ' left Join CFW_FLD_Values CFW'
									|| COALESCE(v_tintOrder,0)
									|| ' on CFW' || COALESCE(v_tintOrder,0) || '.Fld_Id=' || COALESCE(v_numFieldId,0)
									|| 'and CFW' || COALESCE(v_tintOrder,0) || '.RecId=DM.numDivisionId   ';
			ELSEIF v_vcAssociatedControlType = 'SelectBox' then                    
				v_vcDbColumnName := 'DCust' || COALESCE(v_numFieldId,0);

				v_strColumns := coalesce(v_strColumns,'') || ',L' || COALESCE(v_tintOrder,0) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';

				v_WhereCondition := coalesce(v_WhereCondition,'') 
								|| ' left Join CFW_FLD_Values CFW' || COALESCE(v_tintOrder,0) 
								|| ' on CFW' || COALESCE(v_tintOrder,0) || '.Fld_Id=' || COALESCE(v_numFieldId,0)
								|| ' and CFW' || COALESCE(v_tintOrder,0) || '.RecId=DM.numDivisionId';

				v_WhereCondition := coalesce(v_WhereCondition,'') 
									|| ' left Join ListDetails L' || COALESCE(v_tintOrder,0)
									|| ' on CAST(L' || COALESCE(v_tintOrder,0) || '.numListItemID AS VARCHAR)=CFW' || COALESCE(v_tintOrder,0) || '.Fld_Value';
			ELSEIF v_vcAssociatedControlType = 'CheckBoxList' then
				v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || COALESCE(v_tintOrder,0) || '.Fld_Value,'',''))), '''')  "' || coalesce(v_vcColumnName,'') || '"';
				
				v_WhereCondition := coalesce(v_WhereCondition,'')
									|| ' left Join CFW_FLD_Values CFW' || COALESCE(v_tintOrder,0)
									|| ' on CFW' || COALESCE(v_tintOrder,0) || '.Fld_Id=' || COALESCE(v_numFieldId,0)
									|| ' and CFW' || COALESCE(v_tintOrder,0) || '.RecId=DM.numDivisionId ';
			end if;
		end if;
		
		select 
			tintOrder+1
			,vcDbColumnName
			,vcFieldName
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,vcLookBackTableName
			,bitCustomField
			,numFieldId
			,bitAllowSorting
			,bitAllowEdit
			,ListRelID 
		INTO 
			v_tintOrder
			,v_vcDbColumnName
			,v_vcFieldName
			,v_vcAssociatedControlType
			,v_vcListItemType
			,v_numListID
			,v_vcLookBackTableName
			,v_bitCustom
			,v_numFieldId
			,v_bitAllowSorting
			,v_bitAllowEdit
			,v_ListRelID 
		FROM 
			tt_TEMPFORM 
		WHERE 
			tintOrder > v_tintOrder -1 
		ORDER BY 
			tintOrder ASC 
		LIMIT 1;

		GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;

		IF SWV_RowCount = 0 then
			v_tintOrder := 0;
		end if;
	END LOOP;  

	v_strShareRedordWith := ' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=1 AND SR.numAssignedTo=' || COALESCE(v_numUserCntID,0) || ') ';

	v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
	v_numDomainID,' AND EAD.numContactID=CAST(',v_numUserCntID,' AS VARCHAR) AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))');

	v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE(VIE.Total,0) as TotalEmail ';
	v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE(VOA.OpenActionItemCount,0) as TotalActionItem ';

	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
	v_strColumns := coalesce(v_strColumns,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 COALESCE( (CASE WHEN 
				COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) 
				= COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),'''')



	ELSE '''' 
	END)) as FollowupFlag ';

	v_strColumns := coalesce(v_strColumns,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		CONCAT(''('' , CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) AS varchar)

			, ''/'' , CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)AS varchar) , '')''
			, ''<a onclick=openDrip('' , (cast(COALESCE((SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactID AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true AND ADC.bitPrimaryContact = true ),0)AS varchar)) , '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'')
	ELSE '''' 
	END)) as ReadUnreadCntNHstr ';
   
---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
	v_StrSql := '	FROM  CompanyInfo CMP                                       
						join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
						left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
						left join VIEW_OPEN_ACTION_ITEMS VOA ON VOA.numDomainID =  ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND  VOA.numDivisionId = DM.numDivisionID 
						left join View_InboxEmail VIE ON VIE.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND  VIE.numContactId = ADC.numContactId ';
   
   IF v_tintSortOrder = 9 then
      v_StrSql := coalesce(v_StrSql,'') || ' join Favorites F on F.numContactid=DM.numDivisionID ';
   end if; 

	v_StrSql := coalesce(v_StrSql,'') || coalesce(v_WhereCondition,'');

		 -------Change Row Color-------
	v_vcCSOrigDbCOlumnName := '';
	v_vcCSLookBackTableName := '';

	DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
	Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	);

	insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
	where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 35 AND DFCS.numFormID = 35 and coalesce(DFFM.bitAllowGridColor,false) = true;

	IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then
		select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
	end if;   
		----------------------------                        
 
	IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
		v_strColumns := coalesce(v_strColumns,'') || ' ,tCS.vcColorScheme ';
	end if;

	IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
		if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
			v_Prefix := 'ADC.';
		end if;
		if v_vcCSLookBackTableName = 'DivisionMaster' then
			v_Prefix := 'DM.';
		end if;
		if v_vcCSLookBackTableName = 'CompanyInfo' then
			v_Prefix := 'CMP.';
		end if;
		IF v_vcCSAssociatedControlType = 'DateField' then
			v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
		ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox' then
			v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
		end if;
	end if;

	IF v_columnName ilike 'CFW.Cust%' then
		v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','') || ' ';
		v_columnName := 'CFW.Fld_Value';
	ELSEIF v_columnName ilike 'DCust%' then
		v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' || REPLACE(v_columnName,'DCust','');
		v_StrSql := coalesce(v_StrSql,'') || ' left Join ListDetails LstCF on CAST(LstCF.numListItemID AS VARCHAR)=CFW.Fld_Value  ';
		v_columnName := 'LstCF.vcData';
	end if;        

	IF v_columnName = 'CMP.vcPerformance' then
		v_columnName := 'TempPerformance.monDealAmount';
	end if;

	v_StrSql := coalesce(v_StrSql,'') || ' WHERE (numCompanyType = 0 OR numCompanyType=46) 
									AND (COALESCE(ADC.bitPrimaryContact,false) = true OR ADC.numContactID IS NULL)
									AND CMP.numDomainID=DM.numDomainID   
									AND DM.tintCRMType= ' || SUBSTR(CAST(v_CRMType AS VARCHAR(2)),1,2) || ' AND DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20);


	IF v_tintUserRightType = 1 then
		v_StrSql := coalesce(v_StrSql,'') || CONCAT(' AND (DM.numRecOwner = ',v_numUserCntID,' or DM.numAssignedTo=',v_numUserCntID,' or ',v_strShareRedordWith,' OR ',v_strExternalUser,')');
	ELSEIF v_tintUserRightType = 2 then
		v_StrSql := coalesce(v_StrSql,'') || CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',v_numUserCntID,' ) or DM.numAssignedTo=',v_numUserCntID,' or ',v_strShareRedordWith,')');
	end if;

	IF LENGTH(v_SearchQuery) > 0 then
		v_StrSql := coalesce(v_StrSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
	end if;	

	IF v_SortChar <> '0' then
		v_StrSql := coalesce(v_StrSql,'') || ' And CMP.vcCompanyName ILIKE ''' || coalesce(v_SortChar,'') || '%'' ';
	end if;

	IF LENGTH(v_SearchQuery) > 0 then
		IF v_tintSortOrder = 1 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=2 ';
		ELSEIF v_tintSortOrder = 2 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=3 ';
		ELSEIF v_tintSortOrder = 3 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND (DM.numRecOwner = '
			|| COALESCE(v_numUserCntID,0)
			|| ' or DM.numAssignedTo='
			|| COALESCE(v_numUserCntID,0) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
		ELSEIF v_tintSortOrder = 6 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.bintCreatedDate > '''
			|| CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20))
			|| '''';
		ELSEIF v_tintSortOrder = 7 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numCreatedby=' || COALESCE(v_numUserCntID,0);
		ELSEIF v_tintSortOrder = 8 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numModifiedby=' || COALESCE(v_numUserCntID,0);
		end if;

		IF v_numProfile <> 0 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND cmp.vcProfile = '
			|| SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
		end if;
	ELSE
		IF v_tintSortOrder = 1 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=2 ';
		ELSEIF v_tintSortOrder = 2 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numStatusID=3 ';
		ELSEIF v_tintSortOrder = 3 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND (DM.numRecOwner = '
			|| COALESCE(v_numUserCntID,0)
			|| ' or DM.numAssignedTo='
			|| COALESCE(v_numUserCntID,0) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
		ELSEIF v_tintSortOrder = 6 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.bintCreatedDate > '''
			|| CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20))
			|| '''';
		ELSEIF v_tintSortOrder = 7 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numCreatedby=' || COALESCE(v_numUserCntID,0);
		ELSEIF v_tintSortOrder = 8 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND DM.numModifiedby=' || COALESCE(v_numUserCntID,0);
		end if;

		IF v_numProfile <> 0 then
			v_StrSql := coalesce(v_StrSql,'') || ' AND cmp.vcProfile = '
			|| SUBSTR(CAST(v_numProfile AS VARCHAR(15)),1,15);
		end if;
	end if;

	IF v_vcRegularSearchCriteria <> '' then
		IF coalesce(POSITION(substring(v_vcRegularSearchCriteria from E'cmp\\.vcPerformance') IN v_vcRegularSearchCriteria),0) > 0 then
			v_StrSql := v_StrSql;
		ELSE
			v_StrSql := coalesce(v_StrSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
		end if;
	end if;

	IF v_vcCustomSearchCriteria <> '' then
		v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
	end if;
                                            
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                              
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);     
		
	-- EXECUTE FINAL QUERY
	v_strFinal := CONCAT('CREATE TEMPORARY TABLE tt_TempGetProspectsList1 AS SELECT ROW_NUMBER() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,') ID, ',v_strColumns,v_StrSql,';');
	
	--RAISE NOTICE '%',v_strFinal;

	EXECUTE v_strFinal;

	open SWV_RefCur for EXECUTE CONCAT('SELECT * FROM tt_TempGetProspectsList1 WHERE ID > ', v_firstRec,' and ID <',v_lastRec,' ORDER BY ID');

	EXECUTE 'SELECT COUNT(*) FROM tt_TempGetProspectsList1;' INTO v_TotRecs;

	open SWV_RefCur2 for SELECT * FROM tt_TEMPFORM;

	RETURN;
END; $$;	

