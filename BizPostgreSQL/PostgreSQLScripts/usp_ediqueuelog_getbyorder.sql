-- Stored procedure definition script USP_EDIQueueLog_GetByOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EDIQueueLog_GetByOrder(v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		vcLog
		,bitSuccess
		,FormatedDateTimeFromDate(dtDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),numDomainID) AS vcDate
   FROM
   EDIQueueLog
   WHERE
   numDomainID = v_numDomainID
   AND numOppID = v_numOppID
   ORDER BY
   dtDate DESC;
END; $$;












