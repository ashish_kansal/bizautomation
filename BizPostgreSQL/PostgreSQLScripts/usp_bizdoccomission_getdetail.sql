-- Stored procedure definition script USP_BizDocComission_GetDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDocComission_GetDetail(v_numDomainId NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numComPayPeriodID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintMode SMALLINT
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
	
      open SWV_RefCur for
      SELECT
      OpportunityMaster.numOppId
			,OpportunityMaster.vcpOppName AS Name
			,SUM(coalesce(monOrderSubTotal,0)) AS monSOSubTotal
			,CAST(SUM(coalesce(OBDI.monTotAmount,OpportunityItems.monTotAmount) -(coalesce(OBDI.numUnitHour,OpportunityItems.numUnitHour)*coalesce(OpportunityItems.monVendorCost,0)*fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,v_numDomainId,Item.numPurchaseUnit))) AS DECIMAL(20,5)) AS monGPVendorCost
			,CAST(SUM(coalesce(OBDI.monTotAmount,OpportunityItems.monTotAmount) -(coalesce(OBDI.numUnitHour,OpportunityItems.numUnitHour)*coalesce(OpportunityItems.monAvgCost,0))) AS DECIMAL(20,5)) AS monGPAverageCost
			,coalesce(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocsId
			,coalesce(OpportunityBizDocs.vcBizDocID,'') AS vcBizDocID
			,SUM(coalesce(monInvoiceSubTotal,0)) AS monInvoiceSubTotal
			,SUM(coalesce(numComissionAmount,0)) AS monTotalCommission
			,SUM(coalesce(monCommissionPaid,0)) AS monCommissionPaid
			,SUM(coalesce(numComissionAmount,0) -coalesce(monCommissionPaid,0)) AS monCommissionToPay
      FROM
      BizDocComission
      INNER JOIN
      OpportunityMaster
      ON
      BizDocComission.numOppID = OpportunityMaster.numOppId
      INNER JOIN
      OpportunityItems
      ON
      BizDocComission.numOppItemID = OpportunityItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OpportunityItems.numItemCode = Item.numItemCode
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      LEFT JOIN
      OpportunityBizDocs
      ON
      BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
      LEFT JOIN
      OpportunityBizDocItems OBDI
      ON
      BizDocComission.numOppBizDocItemID = OBDI.numOppBizDocItemID
      WHERE
      numComPayPeriodID = v_numComPayPeriodID
      AND BizDocComission.numUserCntID = v_numUserCntID
      GROUP BY
      OpportunityMaster.numOppId,OpportunityMaster.vcpOppName,OpportunityMaster.bintCreatedDate,
      OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.vcBizDocID
      ORDER BY
      OpportunityMaster.bintCreatedDate;
   ELSEIF v_tintMode = 2
   then
	
      open SWV_RefCur for
      SELECT
      I.numItemCode
			,I.vcItemName
			,coalesce(OBDI.numUnitHour,OI.numUnitHour) AS numUnitHour
			,coalesce(OI.monVendorCost,0)*fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,v_numDomainId,I.numBaseUnit)*coalesce(OBDI.numUnitHour,OI.numUnitHour) AS VendorCost
			,coalesce(OI.monAvgCost,0)*coalesce(OBDI.numUnitHour,OI.numUnitHour) AS monAvgCost
			,coalesce(OBDI.monPrice,OI.monPrice) AS monPrice
			,coalesce(OBDI.monTotAmount,OI.monTotAmount) AS monTotAmount
			,coalesce(BDC.numComissionAmount,0) AS monCommissionEarned
			,coalesce(BDC.monCommissionPaid,0) AS monCommissionPaid
			,BDC.decCommission
			,Case when BDC.tintComBasedOn = 1 then 'Amount' when BDC.tintComBasedOn = 2 then 'Units' end AS BasedOn
			,Case when BDC.tintComType = 1 then 'Percentage' when BDC.tintComType = 2 then 'Flat' end as CommissionType
      FROM
      BizDocComission BDC
      INNER JOIN
      OpportunityItems OI
      ON
      BDC.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN
      OpportunityBizDocItems OBDI
      ON
      BDC.numOppBizDocItemID = OBDI.numOppBizDocItemID
      WHERE
      BDC.numComPayPeriodID = v_numComPayPeriodID
      AND BDC.numUserCntID = v_numUserCntID
      AND BDC.numOppID = v_numOppID
      AND coalesce(BDC.numOppBizDocId,0) = coalesce(v_numOppBizDocID,0);
   end if;
   RETURN;
END; $$;


