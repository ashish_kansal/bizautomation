-- Stored procedure definition script usp_getDynamicSearchFormFieldsForAGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getDynamicSearchFormFieldsForAGroup(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_FormId NUMERIC(18,0),
	v_vcDisplayColumns TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcLocationID  VARCHAR(100) DEFAULT '0';
BEGIN
   IF v_FormId = 15 then
	
      v_vcLocationID := '2,6';
   ELSE
      select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = v_FormId;
   end if;
      
   open SWV_RefCur for
   SELECT
   numFieldID as numFormFieldID,
		vcFieldName as vcFormFieldName,
		false AS bitCustom,
		CONCAT(numFieldID,'~0') AS vcFieldID
   FROM
   View_DynamicDefaultColumns
   WHERE
   numFormId = v_FormId
   AND bitInResults = true
   AND bitDeleted = false
   AND numDomainID = v_numDomainID
   UNION
   SELECT
   c.Fld_id AS numFormFieldID
		,c.FLd_label AS vcFormFieldName
		,true AS bitCustom,
		CONCAT(c.Fld_id,'~1') AS vcFieldID
   FROM
   CFW_Fld_Master c
   LEFT JOIN
   CFW_Validation V ON V.numFieldId = c.Fld_id
   JOIN
   CFW_Loc_Master L on c.Grp_id = L.Loc_id
   WHERE
   c.numDomainID = v_numDomainID
   AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))
   ORDER BY
   vcFormFieldName ASC; 
   
   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFormFieldID NUMERIC(18,0),
      vcFormFieldName VARCHAR(300),
      vcDbColumnName VARCHAR(300),
      tintOrder INTEGER,
      vcFieldID VARCHAR(300)
   );

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID)
      SELECT
      numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,0
			,vcFieldID
      FROM(SELECT
         numFieldID as numFormFieldID,
				vcFieldName as vcFormFieldName,
				false AS bitCustom,
				CONCAT(numFieldID,'~0') AS vcFieldID,
				vcDbColumnName
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = v_FormId
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,c.FLd_label AS vcFormFieldName
				,true AS bitCustom,
				CONCAT(c.Fld_id,'~1') AS vcFieldID,
				Fld_label AS vcDbColumnName
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT OutParam FROM SplitString(v_vcDisplayColumns,','));
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID)
      SELECT
      numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
      FROM(SELECT  A.numFormFieldID,
					vcFieldName as vcFormFieldName,
					vcDbColumnName,
					A.tintOrder,
					CONCAT(numFieldID,'~0') AS vcFieldID
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = v_FormId
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = v_FormId
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT  A.numFormFieldID,
					c.FLd_label as vcFormFieldName,
					c.FLd_label,
					A.tintOrder,
					CONCAT(c.Fld_id,'~1') AS vcFieldID
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master c ON c.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = v_FormId
         AND c.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID)
      SELECT
      numFieldID,
			vcFieldName,
			vcDbColumnName,
			0,
			CONCAT(numFieldID,'~0')
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_FormId
      AND bitInResults = true
      AND bitDeleted = false
      AND numDomainID = v_numDomainID
      AND numFieldID =(CASE v_FormId WHEN 1 THEN 3 WHEN 15 THEN 96 WHEN 19 THEN 3 WHEN 29 THEN 189 WHEN 17 THEN 127 WHEN 18 THEN 148 WHEN 59 THEN 423 END);
   end if;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPSELECTEDCOLUMNS;
   RETURN;
END; $$;


