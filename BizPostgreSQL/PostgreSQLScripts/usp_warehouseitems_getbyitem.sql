-- Stored procedure definition script USP_WarehouseItems_GetByItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WarehouseItems_GetByItem(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_numWLocationID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseUnit  NUMERIC(18,0);
   v_numSaleUnit  NUMERIC(18,0);
   v_numPurchaseUnit  NUMERIC(18,0);
   v_numSaleUOMFactor  DOUBLE PRECISION;
   v_numPurchaseUOMFactor  DOUBLE PRECISION;
   v_bitKitParent  BOOLEAN;
BEGIN
   select   coalesce(numBaseUnit,0), coalesce(numSaleUnit,0), coalesce(numPurchaseUnit,0), coalesce(bitKitParent,false) INTO v_numBaseUnit,v_numSaleUnit,v_numPurchaseUnit,v_bitKitParent FROM
   Item WHERE
   numItemCode = v_numItemCode;   

   v_numSaleUOMFactor := fn_UOMConversion(v_numBaseUnit,v_numItemCode,v_numDomainID,v_numSaleUnit);
   v_numPurchaseUOMFactor := fn_UOMConversion(v_numBaseUnit,v_numItemCode,v_numDomainID,v_numPurchaseUnit);

   DROP TABLE IF EXISTS tt_TEMPCHILDITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCHILDITEMS
   (
      numItemKitID NUMERIC(18,0), 
      numItemCode NUMERIC(18,0), 
      numQtyItemsReq DOUBLE PRECISION, 
      numCalculatedQty DOUBLE PRECISION
   );

   IF v_bitKitParent = true then
	
      INSERT INTO tt_TEMPCHILDITEMS(numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) with recursive CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS(SELECT
      CAST(0 AS NUMERIC) AS numItemKitID,
				numItemCode AS numItemCode,
				DTL.numQtyItemsReq AS numQtyItemsReq,
				DTL.numQtyItemsReq AS numCalculatedQty
      FROM
      Item
      INNER JOIN
      ItemDetails DTL
      ON
      numChildItemID = numItemCode
      WHERE
      numItemKitID = v_numItemCode
      UNION ALL
      SELECT
      DTL.numItemKitID AS numItemKitID,
				i.numItemCode AS numItemCode,
				DTL.numQtyItemsReq AS numQtyItemsReq,
				(DTL.numQtyItemsReq*c.numCalculatedQty) AS numCalculatedQty
      FROM
      Item i
      INNER JOIN
      ItemDetails DTL
      ON
      DTL.numChildItemID = i.numItemCode
      INNER JOIN
      CTE c
      ON
      DTL.numItemKitID = c.numItemCode
      WHERE
      DTL.numChildItemID != v_numItemCode) SELECT numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty FROM CTE;
   end if;

   open SWV_RefCur for SELECT
		WareHouseItems.numWareHouseItemID
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWLocationID
		,coalesce(Warehouses.vcWareHouse,'') AS vcExternalLocation
		,numItemID
		,(CASE WHEN coalesce(Item.bitAssembly,false) = true THEN fn_GetAssemblyPossibleWOQty(WareHouseItems.numItemID,WareHouseItems.numWareHouseID) ELSE 0 END) AS numBuildableQty
		,Item.numAssetChartAcntId
		,CASE
   WHEN coalesce(Item.bitKitParent,false) = true
   THEN coalesce((SELECT FLOOR(MIN(CASE
         WHEN coalesce(WIINNER.numOnHand,0) = 0 THEN 0
         WHEN coalesce(WIINNER.numOnHand,0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN coalesce(WIINNER.numOnHand,0)/TCI.numQtyItemsReq
         ELSE 0
         END)) FROM tt_TEMPCHILDITEMS TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode = WIINNER.numItemID AND WIINNER.numWareHouseID = WareHouseItems.numWareHouseID),0)
   ELSE coalesce(numOnHand,0)+coalesce((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numDomainID = v_numDomainID AND WI.numItemID = v_numItemCode AND WI.numWareHouseID = WareHouseItems.numWareHouseID),0)
   END AS OnHand
		,CASE WHEN coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numOnOrder,0) END as OnOrder
		,CASE
   WHEN coalesce(Item.bitKitParent,false) = true OR coalesce(Item.bitAssembly,false) = true
   THEN 0
   ELSE coalesce((SELECT
         SUM(numUnitHour)
         FROM
         OpportunityItems OI
         INNER JOIN
         OpportunityMaster OM
         ON
         OI.numOppId = OM.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND tintopptype = 1
         AND tintoppstatus = 0
         AND OI.numItemCode = WareHouseItems.numItemID
         AND OI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID),0)
   END AS Requisitions
		,CASE WHEN coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numAllocation,0) END as Allocation
		,CASE WHEN coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numBackOrder,0) END as BackOrder
		,coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0)+coalesce((SELECT SUM(coalesce(WI.numOnHand,0)+coalesce(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numDomainID = v_numDomainID AND WI.numItemID = v_numItemCode AND WI.numWareHouseID = WareHouseItems.numWareHouseID),0) AS TotalOnHand
		,coalesce(Item.monAverageCost,0) AS monAverageCost
		,((coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0))*(CASE WHEN coalesce(Item.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(coalesce(WareHouseItems.monWListPrice,0),2) AS Price
		,coalesce(WareHouseItems.vcWHSKU,'') as SKU
		,coalesce(WareHouseItems.vcBarCode,'') as BarCode
		,CASE
   WHEN coalesce(Item.numItemGroup,0) > 0
   THEN fn_GetAttributes(WareHouseItems.numWareHouseItemID,0::BOOLEAN)
   ELSE ''
   END AS vcAttribute
		,fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,(v_numSaleUOMFactor*CASE
   WHEN coalesce(Item.bitKitParent,false) = true
   THEN coalesce((SELECT FLOOR(MIN(CASE
         WHEN coalesce(WIINNER.numOnHand,0) = 0 THEN 0
         WHEN coalesce(WIINNER.numOnHand,0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN coalesce(WIINNER.numOnHand,0)/TCI.numQtyItemsReq
         ELSE 0
         END))
         FROM tt_TEMPCHILDITEMS TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode = WIINNER.numItemID AND WIINNER.numWareHouseID = WareHouseItems.numWareHouseID),0)
   ELSE coalesce(numOnHand,0)
   END) as OnHandUOM
		,CAST((v_numPurchaseUOMFactor*Case when coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((v_numPurchaseUOMFactor*Case when coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((v_numSaleUOMFactor*Case when coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((v_numSaleUOMFactor*Case when coalesce(Item.bitKitParent,false) = true THEN 0 ELSE coalesce(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIINNER.numWareHouseItemID,',"WarehouseLocationID":',
      WIINNER.numWLocationID,',"Warehouse":"',coalesce(W.vcWareHouse,''),
      '", "OnHand":',coalesce(WIINNER.numOnHand,0),', "Location":"',coalesce(WL.vcLocation,''),
      '"}'),', ' ORDER BY WL.vcLocation ASC)
      
      FROM
      WareHouseItems WIINNER
      INNER JOIN
      Warehouses W
      ON
      WIINNER.numWareHouseID = W.numWareHouseID
      INNER JOIN
      WarehouseLocation WL
      ON
      WIINNER.numWLocationID = WL.numWLocationID
      WHERE
      WIINNER.numDomainID = v_numDomainID
      AND WIINNER.numItemID = v_numItemCode
      AND WIINNER.numWareHouseID = WareHouseItems.numWareHouseID),''),']') AS vcInternalLocations
		,(CASE WHEN EXISTS(SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = WareHouseItems.numWareHouseItemID AND coalesce(numQty,0) > 0) THEN true ELSE false END) AS bitSerialLotExists
   FROM
   WareHouseItems
   INNER JOIN
   Item
   ON
   WareHouseItems.numItemID = Item.numItemCode
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   WHERE
   WareHouseItems.numDomainID = v_numDomainID
   AND WareHouseItems.numItemID = v_numItemCode
   AND coalesce(WareHouseItems.numWLocationID,0) = 0
   AND (coalesce(v_numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID = v_numWarehouseID)
   AND (coalesce(v_numWLocationID,0) = 0 OR EXISTS(SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID = v_numDomainID AND WI.numItemID = v_numItemCode AND WI.numWareHouseID = v_numWarehouseID AND coalesce(WI.numWLocationID,0) = v_numWLocationID));
END; $$;












