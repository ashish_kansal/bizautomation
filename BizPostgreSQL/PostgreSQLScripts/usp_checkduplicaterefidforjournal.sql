-- Stored procedure definition script USP_CheckDuplicateRefIDForJournal for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckDuplicateRefIDForJournal(v_numJournalReferenceNo NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COUNT(GJH.numJOurnal_Id) AS RowCount
   FROM General_Journal_Header AS GJH
   WHERE GJH.numDomainId = v_numDomainID
   AND GJH.numJournalReferenceNo = v_numJournalReferenceNo;
END; $$;











