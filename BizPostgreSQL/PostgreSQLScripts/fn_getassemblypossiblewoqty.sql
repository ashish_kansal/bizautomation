-- Function definition script fn_GetAssemblyPossibleWOQty for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetAssemblyPossibleWOQty(v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCount  INTEGER DEFAULT 0;
BEGIN
   with recursive CTE(numParentItemCode,numItemCode,vcItemName,numRequired,bitAssembly) AS(SELECT
   CAST(0 AS NUMERIC(18,0)) AS numParentItemCode,
			ItemDetails.numChildItemID AS numItemCode,
			Item.vcItemName AS vcItemName,
			CAST(ItemDetails.numQtyItemsReq AS DOUBLE PRECISION) AS numRequired
			,coalesce(Item.bitAssembly,false) AS bitAssembly
   FROM
   ItemDetails
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   WHERE
   ItemDetails.numItemKitID = v_numItemCode
   AND Item.charItemType = 'P'
   UNION ALL
   SELECT
   CTE.numItemCode AS numParentItemCode,
			ItemDetails.numChildItemID AS numItemCode,
			Item.vcItemName AS vcItemName,
			CAST((ItemDetails.numQtyItemsReq*CTE.numRequired) AS DOUBLE PRECISION) AS numRequired
			,coalesce(Item.bitAssembly,false) AS bitAssembly
   FROM
   CTE
   INNER JOIN
   ItemDetails
   ON
   CTE.numItemCode = ItemDetails.numItemKitID
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   AND Item.charItemType = 'P') select   MIN(CAST((coalesce(numTotalOnHand,0)/(CASE WHEN coalesce(numRequired,0) = 0 THEN 1 ELSE coalesce(numRequired,0) END)) AS DOUBLE PRECISION)) INTO v_numCount FROM
   CTE c
   LEFT JOIN LATERAL(SELECT
      SUM(numOnHand) AS numTotalOnHand
      FROM
      WareHouseItems
      INNER JOIN
      Warehouses W
      ON
      W.numWareHouseID = WareHouseItems.numWareHouseID
      WHERE
      numItemID = c.numItemCode
      AND WareHouseItems.numWareHouseID = v_numWarehouseID) WI on TRUE WHERE
   numItemCode NOT IN(SELECT numParentItemCode FROM CTE);

   RETURN coalesce(v_numCount,0);
END; $$;

