DROP FUNCTION IF EXISTS USP_ElasticSearchBizCart_GetItems;

CREATE OR REPLACE FUNCTION USP_ElasticSearchBizCart_GetItems(v_numDomainID NUMERIC(18,0),
	v_vcItemCodes TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  TEXT;

   v_fldList  TEXT;
   v_fldList1  TEXT;
   v_tmpSQL  TEXT;
BEGIN

	-- @vcItemCodes is corrently set for single item
	
   select string_agg(concat('"',REPLACE(FLd_label,' ','') || '_C','"'),',') INTO v_fldList FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 5;
   select string_agg(concat('"',REPLACE(FLd_label,' ','') || '_' || Fld_id,'"'),',') INTO v_fldList1 FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 9;
     
	-- Item Details
   v_strSQL := CONCAT(v_strSQL,'
	SELECT --I.*,
	I.numDomainID
	,I.numItemCode
	,COALESCE(I.bitMatrix,false) AS bitMatrix
	,COALESCE(I.bitKitParent,false) AS bitKitParent
	,I.numItemGroup
	,vcItemName
	,I.bintCreatedDate
	,vcManufacturer
	,COALESCE(txtItemDesc,'''') txtItemDesc
	,vcSKU
	,fltHeight
	,fltWidth
	,fltLength
	,vcModelID
	,fltWeight
	,numBaseUnit
	,numSaleUnit
	,numPurchaseUnit
	,bitFreeShipping
	,charItemType
	,monListPrice
	,bitAllowBackOrder
	--,bitShowInStock
	,COALESCE(numVendorID,0) AS numVendorID
	,numItemClassification
	,COALESCE(I.numManufacturer,0) numManufacturer
	,COALESCE(IED.txtDesc,'''') txtDesc
	,COALESCE(IED.txtShortDesc,'''') txtShortDesc 
	,IM.vcPathForImage
	,IM.vcpathForTImage
	,UOM AS UOMConversionFactor
	,UOMPurchase AS UOMPurchaseConversionFactor
	,'''' AS vcCategoryName
	,'''' AS CategoryDesc
	,0 AS numTotalPromotions
	,'''' AS vcPromoDesc
	,false AS bitRequireCouponCode
	,false AS bitInStock
	,'''' AS InStock
	,0 AS monFirstPriceLevelPrice
	,0 AS fltFirstPriceLevelDiscount
	,0 AS monListPrice
	,0 AS monMSRP
	,false AS bitHasChildKits
	,0 AS numWareHouseItemID
	',(CASE WHEN LENGTH(coalesce(v_fldList,'')) > 0 THEN CONCAT(',',v_fldList) ELSE '' END),(CASE WHEN LENGTH(coalesce(v_fldList1,'')) > 0 THEN CONCAT(',',v_fldList1) ELSE '' END),
   '

	,COALESCE(I.IsArchieve,false) AS IsArchieve
	,(CASE WHEN COALESCE(I.numItemGroup,0) > 0 AND COALESCE(I.bitMatrix,false) = true THEN fn_GetItemAttributes(I.numDomainID,I.numItemCode,true) ELSE '''' END) AS vcAttributes
	,(CASE WHEN COALESCE(I.numItemGroup,0) > 0 AND COALESCE(I.bitMatrix,false) = true THEN CONCAT(''['',coalesce((SELECT 
																								string_agg(CONCAT('',{"FieldID":'',IA.FLD_ID,'',"FieldVaue":'',IA.FLD_Value,''}''),'''' ORDER BY ID)
																							FROM
																								ItemAttributes IA
																							WHERE
																								IA.numItemCode = I.numItemCode
																							),''''),'']'') ELSE ''[]'' END) AS vcAttributeValues
	FROM Item I
	LEFT JOIN ItemExtendedDetails IED ON (IED.numItemCode = I.numItemCode)
	LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=true AND IM.bitIsImage = true
	INNER JOIN
	(
		SELECT (CASE WHEN SWF_isnumeric(OutParam) = true  THEN CAST(OutParam AS NUMERIC) ELSE 0 END) AS OutParam FROM regexp_split_to_table(''' || coalesce(v_vcItemCodes,'') || ''','','') As OutParam
	) TEMPItem ON I.numItemCode = TEMPItem.OutParam
	JOIN LATERAL (SELECT fn_UOMConversion(COALESCE(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,COALESCE(I.numBaseUnit, 0)) AS UOM) AS UOM ON true
	JOIN LATERAL (SELECT fn_UOMConversion(COALESCE(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,COALESCE(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase ON true
	');

   IF LENGTH(coalesce(v_fldList,'')) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || '
		left join (
					SELECT 
					row_id,' || v_fldList || '
				FROM 
					fn_GetPivotQueryResult(''SELECT 
												row_id,' || v_fldList || ' 
											FROM 
											crosstab (
												''''SELECT  
														CAST(t2.RecId AS TEXT) row_id
														,REPLACE(t1.Fld_label,'''''''' '''''''','''''''''''''''') || ''''''''_C'''''''' as Fld_label
														,CASE t1.fld_type WHEN ''''''''SelectBox'''''''' THEN COALESCE(fn_GetListItemName(CAST(t2.Fld_Value AS NUMERIC)),'''''''''''''''')
														WHEN ''''''''CheckBox'''''''' THEN COALESCE(fn_getCustomDataValue(CAST(t2.Fld_Value AS TEXT),9),'''''''''''''''')
														WHEN ''''''''DateField'''''''' THEN (CASE WHEN is_date(t2.Fld_Value::VARCHAR) THEN FormatedDateFromDate(t2.Fld_Value::VARCHAR::TIMESTAMP,' || v_numDomainID || ') ELSE '''''''''''''''' END)
														ELSE Replace(COALESCE(t2.Fld_Value,''''''''0''''''''),''''''''$'''''''',''''''''\$\$'''''''') end AS Fld_Value 
													FROM CFW_Fld_Master t1 
													JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' || v_numDomainID || ' AND t1.fld_type <> ''''''''Link''''''''
													AND t2.RecId IN (SELECT (CASE WHEN SWF_isnumeric(OutParam) = true  THEN CAST(OutParam AS NUMERIC) ELSE 0 END) AS OutParam FROM regexp_split_to_table(''''''''' || coalesce(v_vcItemCodes,'') || ''''''''','''''''','''''''') AS OutParam)'''') AS final_result(row_id TEXT, ' || REPLACE(v_fldList,',',' TEXT,') || ' TEXT);'',0) as t(row_id TEXT,' || REPLACE(v_fldList,',',' TEXT,') || ' TEXT)) AS pvt on I.numItemCode=CAST(pvt.row_id AS NUMERIC)';
   end if;

   IF LENGTH(coalesce(v_fldList1,'')) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || '
	  left join (
					SELECT 
					row_id,' || v_fldList1 || '
				FROM 
					fn_GetPivotQueryResult(''SELECT 
												row_id,' || v_fldList1 || ' 
											FROM 
											crosstab (
												''''SELECT  CAST(t2.numItemCode AS TEXT) row_id, REPLACE(t1.Fld_label,'''''''' '''''''','''''''''''''''') || ''''''''_'''''''' || CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
						CASE t1.fld_type WHEN ''''''''SelectBox'''''''' THEN COALESCE(fn_GetListItemName(CAST(t2.Fld_Value AS NUMERIC)),'''''''''''''''')
											WHEN ''''''''CheckBox'''''''' THEN COALESCE(fn_getCustomDataValue(CAST(t2.Fld_Value AS TEXT),9),'''''''''''''''')
											WHEN ''''''''DateField'''''''' THEN (CASE WHEN is_date(t2.Fld_Value::VARCHAR) THEN FormatedDateFromDate(t2.Fld_Value::VARCHAR::TIMESTAMP,' || v_numDomainID || ') ELSE '''''''''''''''' END)
											ELSE Replace(CAST(COALESCE(t2.Fld_Value,''''''''0'''''''') AS TEXT),''''''''$'''''''',''''''''\$\$'''''''') end AS Fld_Value FROM CFW_Fld_Master t1 
					JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' || v_numDomainID || ' AND t1.fld_type <> ''''''''Link''''''''
					AND t2.numItemCode IN (SELECT (CASE WHEN SWF_isnumeric(OutParam) = true  THEN CAST(OutParam AS NUMERIC) ELSE 0 END) AS OutParam FROM regexp_split_to_table(''''''''' || coalesce(v_vcItemCodes,'') || ''''''''','''''''','''''''')  AS OutParam)'''') AS final_result(row_id TEXT, ' || REPLACE(v_fldList1,',',' TEXT,') || ' TEXT);'',0) as t(row_id TEXT,' || REPLACE(v_fldList1,',',' TEXT,') || ' TEXT)) AS pvt1 on I.numItemCode=CAST(pvt1.row_id AS NUMERIC)';
   end if;

   v_tmpSQL := v_strSQL;
	
   RAISE NOTICE '%',CAST(v_tmpSQL AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;

	-- Item Warehouse Details
   open SWV_RefCur2 for
   SELECT numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID,coalesce(SUM(numOnHand),0) AS numOnHand ,coalesce(SUM(numAllocation),0) AS numAllocation
   FROM WareHouseItems WI
   INNER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID
   INNER JOIN(SELECT (CASE WHEN SWF_isnumeric(OutParam) = true  THEN CAST(OutParam AS NUMERIC) ELSE 0 END) AS OutParam FROM regexp_split_to_table(v_vcItemCodes,',') AS OutParam) TEMPItem ON WI.numItemID = TEMPItem.OutParam
   GROUP BY numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID;

	-- Item Category
   open SWV_RefCur3 for
   SELECT numItemID,IC.numCategoryID,C.vcCategoryName
	,C.vcDescription as CategoryDesc
   FROM ItemCategory IC
   INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
   INNER JOIN(SELECT (CASE WHEN SWF_isnumeric(OutParam) = true  THEN CAST(OutParam AS NUMERIC) ELSE 0 END) AS OutParam FROM regexp_split_to_table(v_vcItemCodes,',') AS OutParam) TEMPItem ON IC.numItemID = TEMPItem.OutParam;
   RETURN;
END; $$;


