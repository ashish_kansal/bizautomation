-- Stored procedure definition script USP_Category_GetByName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Category_GetByName(v_numDomainID NUMERIC(18,0),
	v_vcCategotyName VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numCategoryID,vcCategoryName FROM Category WHERE numDomainID = v_numDomainID AND (LOWER(coalesce(vcCategoryName,'')) = LOWER(v_vcCategotyName) OR LOWER(coalesce(vcCategoryNameURL,'')) = LOWER(v_vcCategotyName)) LIMIT 1;
END; $$;












