-- Stored procedure definition script USP_ManagePromotionOfferDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePromotionOfferDTL(v_numProID NUMERIC(9,0) DEFAULT 0,    
v_tintRuleAppType SMALLINT DEFAULT NULL,  
v_numValue NUMERIC(9,0) DEFAULT 0,  
v_numProfile NUMERIC(9,0) DEFAULT 0,  
v_numPODTLID NUMERIC(9,0) DEFAULT 0,  
v_byteMode SMALLINT DEFAULT NULL,
v_tintRecordType SMALLINT DEFAULT NULL --5=Promotion Offer Items, 6 = Promotion Disocunt Items,2=ShippingRule
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
	
      IF v_tintRuleAppType = 1 then
		
         DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
         DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
      ELSEIF v_tintRuleAppType = 2
      then
		
         DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
         DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
      ELSEIF v_tintRuleAppType = 4
      then
		
         DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
         DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId = v_numProID AND tintRecordType = v_tintRecordType;
      end if;
      INSERT INTO PromotionOfferItems(numProId,numValue,tintType,tintRecordType) VALUES(v_numProID,v_numValue,v_tintRuleAppType,v_tintRecordType);
   ELSEIF v_byteMode = 1
   then
	
      DELETE FROM PromotionOfferItems WHERE numProItemId = v_numPODTLID AND tintRecordType = v_tintRecordType;
   end if;
   RETURN;
END; $$;


