-- Stored procedure definition script USP_Delete_CustomerCCInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Delete_CustomerCCInfo(v_CCInfoId BIGINT,
v_ContactId BIGINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from CustomerCreditCardInfo where numCCInfoID = v_CCInfoId and numContactId = v_ContactId;
   RETURN;
END; $$;
--modified by anoop jayaraj              


