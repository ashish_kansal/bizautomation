-- Stored procedure definition script USP_CommissionDatePayPeriod_Get for PostgreSQL
Create or replace FUNCTION USP_CommissionDatePayPeriod_Get(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.


   open SWV_RefCur for SELECT  dtFromDate,dtToDate FROM PayrollHeader WHERE numDomainID = v_numDomainID order by numPayrollHeaderID desc LIMIT 1;
END; $$;












