-- Stored procedure definition script USP_GetCustomerVendorTransReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCustomerVendorTransReport(v_numDomainId NUMERIC(9,0),        
v_numUserId NUMERIC(9,0),        
v_vcTransactionBy VARCHAR(50),
v_vcByName VARCHAR(50) DEFAULT null,
v_dtFromDate TIMESTAMP DEFAULT NULL,
v_dtToDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJD.numCustomerId,GJH.numJOurnal_Id as JournalId,
	case when coalesce(GJH.numCheckId,0) <> 0 then 'Checks'
   Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then 'Cash'
      ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then 'Charge'
         ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintopptype = 1 then 'BizDocs Invoice'
            ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintopptype = 2 then 'BizDocs Purchase'
               ELse Case when coalesce(GJH.numDepositId,0) <> 0 then 'Deposit'
                  ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintopptype = 1 then 'Receive Amt'
                     ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintopptype = 2 then 'Vendor Amt'
                        Else Case when coalesce(GJH.numCategoryHDRID,0) <> 0 then 'Time And Expenses'
                           Else Case When GJH.numJOurnal_Id <> 0 then 'Journal' End End End End End End End End End End as TransactionType,
	GJH.datEntry_Date as EntryDate,
	cast(CI.vcCompanyName as VARCHAR(255)) AS CompanyName,
	GJH.numCheckId As CheckId,
	GJH.numCashCreditCardId as CashCreditCardId,
	GJD.varDescription as Memo,
	(case when GJD.numCreditAmt <> 0 then GJD.numCreditAmt end) as Payment,
	(case when GJD.numDebitAmt <> 0 then GJD.numDebitAmt  end) as Deposit,
	null AS numBalance,
	cast(coalesce(GJD.numChartAcntId,0) as NUMERIC(18,0)) as numChartAcntId,
	GJD.numTransactionId as numTransactionId,
	GJH.numOppId as numOppId,
	GJH.numOppBizDocsId as numOppBizDocsId,
	GJH.numDepositId as numDepositId,
	GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,
	cast(coalesce(GJH.numCategoryHDRID,0) as NUMERIC(18,0)) as numCategoryHDRID,
	cast(coalesce(TE.tintTEType,0) as VARCHAR(255)) as tintTEType,
	cast(TE.numCategory as VARCHAR(255)) as numCategory,
	cast(TE.numUserCntID as VARCHAR(255)) as numUserCntID,
	cast(TE.dtFromDate as VARCHAR(255)) as dtFromDate
   From General_Journal_Header as GJH
   Left Outer Join General_Journal_Details as GJD  on GJH.numJOurnal_Id = GJD.numJournalId
   Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID
   Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId
   Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId
   Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId
   Left outer join OpportunityMaster Opp on GJH.numOppId = Opp.numOppId
   Left outer join timeandexpense TE on GJH.numCategoryHDRID = TE.numCategoryHDRID
   Where GJH.numDomainId = v_numDomainId And (GJH.datEntry_Date >= CAST(v_dtFromDate AS TIMESTAMP) and GJH.datEntry_Date <= CAST(v_dtToDate AS TIMESTAMP));
   RETURN;
END; $$;  


/****** Object:  StoredProcedure [dbo].[usp_getCustomFormFields]    Script Date: 07/26/2008 16:17:23 ******/













