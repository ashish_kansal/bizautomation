CREATE OR REPLACE FUNCTION Activity_Add
(
	v_AllDayEvent  BOOLEAN,  -- whether this is an all-day event  
	v_ActivityDescription TEXT,  -- additional information about the activity  
	v_Duration  INTEGER,  -- length of activity in seconds  
	v_Location  VARCHAR(64), -- where the activity takes place  
	v_StartDateTimeUtc TIMESTAMP, -- when the activity begins  
	v_Subject  VARCHAR(255), -- brief description of the activity  
	v_EnableReminder BOOLEAN,  -- whether this activity should notify the user  
	v_ReminderInterval INTEGER,  -- how long before start time (in seconds) to notify the user  
	v_ShowTimeAs  INTEGER,  -- how this time use is displayed in DayView to the user  
	v_Importance  INTEGER,  -- the priority of this activity  
	v_Status   INTEGER,  -- the reminder notification status of this activity (expired, pending, etc.)  
	v_RecurrenceKey  INTEGER,  -- relates the new activity to its recurrence pattern, if any  
	v_ResourceName VARCHAR(64), -- name of the resource who created this activity  
	v_ItemId VARCHAR(500) DEFAULT '',  
	v_ChangeKey VARCHAR(500) DEFAULT '',
	v_Color SMALLINT DEFAULT 0,
	v_HtmlLink VARCHAR(500) DEFAULT NULL,
	INOUT SWV_RefCur REFCURSOR DEFAULT NULL
)
LANGUAGE plpgsql  
   AS $$
   DECLARE
   v_ResourceID  INTEGER;
   v_NewActivityID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_ResourceName) = true then

      v_ResourceID := CAST(v_ResourceName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if; 
 
	IF (v_ResourceID IS NOT NULL) then
		INSERT INTO Activity
		(
			AllDayEvent,
			ActivityDescription,
			Duration,
			Location,
			StartDateTimeUtc,
			Subject,
			EnableReminder,
			ReminderInterval,
			ShowTimeAs,
			Importance,
			Status,
			RecurrenceID,
			VarianceID,
			itemId,
			ChangeKey,
			Color,
			LastSnoozDateTimeUtc,
			HtmlLink
		)
		VALUES
		(
			v_AllDayEvent,
			v_ActivityDescription,
			coalesce(v_Duration, 0),
			v_Location,
			v_StartDateTimeUtc,
			v_Subject,
			v_EnableReminder,
			v_ReminderInterval,
			v_ShowTimeAs,
			v_Importance,
			v_Status,
			v_RecurrenceKey,
			NULL,
			v_ItemId ,
			v_ChangeKey,v_Color,
			CAST('1900-01-01 00:00:00' AS TIMESTAMP),
			v_HtmlLink
		);  
  
		v_NewActivityID := CURRVAL('Activity_ActivityID_seq');  
 
		INSERT INTO ActivityResource(ActivityID,ResourceID)
		VALUES(v_NewActivityID,v_ResourceID);
   end if;

   OPEN SWV_RefCur FOR SELECT v_NewActivityID;

   RETURN;
END; $$;





