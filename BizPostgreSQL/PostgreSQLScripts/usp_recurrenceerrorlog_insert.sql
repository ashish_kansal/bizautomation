-- Stored procedure definition script USP_RecurrenceErrorLog_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 10 April 2014
-- Description:	Logs recurrence error
-- =============================================
CREATE OR REPLACE FUNCTION USP_RecurrenceErrorLog_Insert(v_numDomainID NUMERIC(18,0),
	v_numRecurConfigID NUMERIC(18,0),
	v_Type VARCHAR(100),
	v_Source VARCHAR(100),
	v_Message TEXT,
    v_StackStrace TEXT,
	v_bitUpdateTransaction BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_ErrorID  INTEGER;
BEGIN
	
	
   INSERT INTO RecurrenceErrorLog(numDomainID,
		numRecConfigID,
		vcType,
		vcSource,
		vcMessage,
		vcStackStrace,
		CreatedDate)
	VALUES(v_numDomainID,
		v_numRecurConfigID,
		v_Type,
		v_Source,
		v_Message,
		v_StackStrace,
		LOCALTIMESTAMP) RETURNING numErrorID INTO v_ErrorID;


   IF v_bitUpdateTransaction = true then
	
      IF EXISTS(SELECT numRecTranID FROM RecurrenceTransaction WHERE numRecConfigID = v_numRecurConfigID AND CAST(dtCreatedDate AS DATE) = CAST(LOCALTIMESTAMP  AS DATE) AND numErrorID IS NULL) then
		
         UPDATE
         RecurrenceTransaction
         SET
         numErrorID = v_ErrorID
         WHERE
         numRecConfigID = v_numRecurConfigID AND
         CAST(dtCreatedDate AS DATE) = CAST(LOCALTIMESTAMP  AS DATE) AND
         numRecTranID =(SELECT
         MAX(numRecTranID)
         FROM
         RecurrenceTransaction
         WHERE
         numRecConfigID = v_numRecurConfigID AND
         CAST(dtCreatedDate AS DATE) = CAST(LOCALTIMESTAMP  AS DATE));
      ELSE
         INSERT INTO RecurrenceTransaction(numRecConfigID,numErrorID, dtCreatedDate) VALUES(v_numRecurConfigID,v_ErrorID, LOCALTIMESTAMP);
      end if;
   end if;
   RETURN;
END; $$;


