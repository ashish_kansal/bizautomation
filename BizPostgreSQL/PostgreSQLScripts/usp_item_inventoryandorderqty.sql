-- Stored procedure definition script USP_Item_InventoryAndOrderQty for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_InventoryAndOrderQty(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numCurrentPage INTEGER
	,v_numPageSize INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT * FROM(SELECT
      numItemCode,
		vcItemName,
		Listdetails.vcData AS vcItemClassification,
		SUM(numOnHand) AS numOnHand
		,SUM(numOnOrder) AS numOnOrder
		,SUM(numAllocation) AS numAllocation
		,SUM(numBackOrder) AS numBackOrder
		,coalesce(OpenPO.numQtyOpenPO,0) AS numQtyOpenPO
		,coalesce(ClosedPO.numQtyClosedPO,0) AS numQtyClosedPO
		,coalesce(OpenSO.numQtyOpenSO,0) AS numQtyOpenSO
		,coalesce(ClosedSO.numQtyClosedSO,0) AS numQtyClosedSO
		,coalesce(SOReturn.numQtySOReturn,0) AS numQtySOReturn
		,coalesce(POReturn.numQtyPOReturn,0) AS numQtyPOReturn
      FROM
      Item
      LEFT JOIN
      Listdetails
      ON
      Item.numItemClassification = Listdetails.numListItemID
      LEFT JOIN
      WareHouseItems
      ON
      WareHouseItems.numItemID = Item.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OI.numUnitHour) AS numQtyOpenPO
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND tintopptype = 2
         AND tintoppstatus = 1
         AND coalesce(tintshipped,0) = 0
         AND OI.numItemCode = Item.numItemCode
         AND coalesce(OM.bitStockTransfer,false) = false) AS  OpenPO on TRUE
      LEFT JOIN LATERAL(SELECT
         SUM(OI.numUnitHour) AS numQtyClosedPO
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND tintopptype = 2
         AND tintoppstatus = 1
         AND coalesce(tintshipped,0) = 1
         AND OI.numItemCode = Item.numItemCode
         AND coalesce(OM.bitStockTransfer,false) = false) AS  ClosedPO on TRUE
      LEFT JOIN LATERAL(SELECT
         SUM(OI.numUnitHour) AS numQtyOpenSO
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND tintopptype = 1
         AND tintoppstatus = 1
         AND coalesce(tintshipped,0) = 0
         AND OI.numItemCode = Item.numItemCode
         AND coalesce(OM.bitStockTransfer,false) = false) AS  OpenSO on TRUE
      LEFT JOIN LATERAL(SELECT
         SUM(OI.numUnitHour) AS numQtyClosedSO
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND tintopptype = 1
         AND tintoppstatus = 1
         AND coalesce(tintshipped,0) = 1
         AND OI.numItemCode = Item.numItemCode
         AND coalesce(OM.bitStockTransfer,false) = false) AS  ClosedSO on TRUE 
      LEFT JOIN LATERAL(SELECT
         SUM(RI.numUnitHour) AS numQtySOReturn
         FROM
         ReturnItems RI
         INNER JOIN
         ReturnHeader RH
         ON
         RI.numReturnHeaderID = RH.numReturnHeaderID
         WHERE
         RH.numDomainID = v_numDomainID
         AND RI.numItemCode = Item.numItemCode
         AND RH.tintReturnType = 1
         AND RH.numReturnStatus = 303) AS SOReturn on TRUE
      LEFT JOIN LATERAL(SELECT
         SUM(RI.numUnitHour) AS numQtyPOReturn
         FROM
         ReturnItems RI
         INNER JOIN
         ReturnHeader RH
         ON
         RI.numReturnHeaderID = RH.numReturnHeaderID
         WHERE
         RH.numDomainID = v_numDomainID
         AND RI.numItemCode = Item.numItemCode
         AND RH.tintReturnType = 2
         AND RH.numReturnStatus = 303) AS POReturn on TRUE
      WHERE
      Item.numDomainID = v_numDomainID
      AND charItemType = 'P'
      AND (Item.numItemCode = v_numItemCode OR coalesce(v_numItemCode,0) = 0)
      GROUP BY
      Item.numItemCode,Item.vcItemName,Listdetails.vcData,OpenPO.numQtyOpenPO,
      ClosedPO.numQtyClosedPO,OpenSO.numQtyOpenSO,ClosedSO.numQtyClosedSO,
      SOReturn.numQtySOReturn,POReturn.numQtyPOReturn) TEMP
   ORDER BY
   numItemCode;


   open SWV_RefCur2 for
   SELECT COUNT(*) FROM Item WHERE Item.numDomainID = v_numDomainID AND charItemType = 'P' AND (Item.numItemCode = v_numItemCode OR coalesce(v_numItemCode,0) = 0);
   RETURN;
END; $$;


