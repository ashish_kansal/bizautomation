-- Stored procedure definition script USP_DomainDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DomainDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numDivisionId as "numDivisionId" from Domain where numDomainId = v_numDomainID;
END; $$;












