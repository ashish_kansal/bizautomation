-- Stored procedure definition script USP_AccountsReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AccountsReports(v_numDomainID NUMERIC(18,0),
    v_dtFromDate TIMESTAMP,
    v_dtToDate TIMESTAMP,
    v_tintReportType SMALLINT,
    v_numAccountClass NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintReportType = 1 then --Report Name: Check Register

      open SWV_RefCur for
      SELECT
      CH.numCheckHeaderID
		,CH.numCheckNo
		,CH.dtCheckDate
		,CH.monAmount
		,CH.numDivisionID
		,C.vcCompanyName
		,COA.vcAccountName
		,COALESCE((SELECT string_agg(BillHeader.vcReference,',')
         FROM
         CheckHeader
         INNER JOIN
         BillPaymentHeader
         ON
         CheckHeader.numReferenceID = BillPaymentHeader.numBillPaymentID
         INNER JOIN
         BillPaymentDetails
         ON
         BillPaymentHeader.numBillPaymentID = BillPaymentDetails.numBillPaymentID
         INNER JOIN
         BillHeader
         ON
         BillPaymentDetails.numBillID = BillHeader.numBillID
         WHERE
         CheckHeader.numCheckHeaderID = CH.numCheckHeaderID),'') AS vcReference
      FROM CheckHeader CH
      JOIN DivisionMaster D ON CH.numDivisionID = D.numDivisionID
      JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      JOIN Chart_Of_Accounts COA ON CH.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      WHERE CH.numDomainID = v_numDomainID --AND ISNULL(bitIsPrint,0)=0 
      AND CH.dtCheckDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (CH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      order by dtCheckDate,numCheckNo;
   ELSEIF v_tintReportType = 2
   then --Report Name: Sales Journal Detail (By GL Account)

      open SWV_RefCur for
      SELECT vcAccountName,SUM(coalesce(numCreditAmt,0)) AS numCreditAmt,SUM(coalesce(numDebitAmt,0)) AS numDebitAmt
      FROM(SELECT GJD.numChartAcntId,COA.vcAccountName,coalesce(GJD.numCreditAmt,0) AS numCreditAmt,coalesce(GJD.numDebitAmt,0) AS numDebitAmt
         FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         JOIN OpportunityMaster OM ON OM.numOppId = GJH.numOppId AND OM.tintopptype = 1 AND OM.numDomainId = v_numDomainID
         JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid AND OBD.numOppBizDocsId = GJH.numOppBizDocsId
         AND OBD.bitAuthoritativeBizDocs = 1
         JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
         WHERE GJH.numDomainId = v_numDomainID
         AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
         AND (OM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
         UNION ALL
         SELECT GJD.numChartAcntId,COA.vcAccountName,coalesce(GJD.numCreditAmt,0) AS numCreditAmt,coalesce(GJD.numDebitAmt,0) AS numDebitAmt
         FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         JOIN ReturnHeader RH ON RH.numDomainID = v_numDomainID AND RH.numReturnHeaderID = GJH.numReturnID
         JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
         WHERE GJH.numDomainId = v_numDomainID AND RH.tintReceiveType = 2 AND RH.tintReturnType IN(1,3) AND  RH.numDomainID = v_numDomainID
         AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
         AND (RH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)) AS T
      GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName;
   ELSEIF v_tintReportType = 3
   then --Report Name: Purchase Journal

      open SWV_RefCur for
      SELECT vcAccountName,SUM(coalesce(numCreditAmt,0)) AS numCreditAmt,SUM(coalesce(numDebitAmt,0)) AS numDebitAmt
      FROM(SELECT GJD.numChartAcntId,COA.vcAccountName,coalesce(GJD.numCreditAmt,0) AS numCreditAmt,coalesce(GJD.numDebitAmt,0) AS numDebitAmt
         FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         JOIN OpportunityMaster OM ON OM.numOppId = GJH.numOppId AND OM.tintopptype = 2 AND OM.numDomainId = v_numDomainID
         JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid AND OBD.numOppBizDocsId = GJH.numOppBizDocsId AND OBD.bitAuthoritativeBizDocs = 1
         JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
         WHERE GJH.numDomainId = v_numDomainID
         AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
         AND (OM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
         UNION ALL
         SELECT GJD.numChartAcntId,COA.vcAccountName,coalesce(GJD.numCreditAmt,0) AS numCreditAmt,coalesce(GJD.numDebitAmt,0) AS numDebitAmt
         FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         JOIN BillHeader BH ON BH.numBillID = GJH.numBillID AND BH.numDomainId = v_numDomainID
         JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
         WHERE GJH.numDomainId = v_numDomainID
         AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
         AND (BH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)) AS T
      GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName;
   ELSEIF v_tintReportType = 4
   then --Report Name: Invoice Register

      open SWV_RefCur for
      SELECT OM.vcpOppName,OBD.vcBizDocID,OBD.dtFromDate AS FromDate,C.vcCompanyName,OM.monDealAmount,OBD.monDealAmount AS monBizDocAmount,OBD.monAmountPaid
      FROM OpportunityMaster OM
      JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid AND OBD.bitAuthoritativeBizDocs = 1
      JOIN DivisionMaster D ON OM.numDivisionId = D.numDivisionID
      JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      WHERE OM.tintopptype = 1 AND OM.numDomainId = v_numDomainID
      AND OBD.dtFromDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (OM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT RH.vcRMA,RH.vcBizDocName,RH.dtCreatedDate AS FromDate,C.vcCompanyName,CAST(DM.monDepositAmount*-1 AS DECIMAL(20,5)) AS monDealAMount,CAST(0 AS DECIMAL(20,5)) AS monBizDocAmount,CAST(0 AS DECIMAL(20,5)) AS monAmountPaid
      FROM ReturnHeader RH
      JOIN DepositMaster DM ON RH.numReturnHeaderID = DM.numReturnHeaderID AND DM.numDomainId = v_numDomainID
      JOIN DivisionMaster D ON RH.numDivisionId = D.numDivisionID
      JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      WHERE RH.tintReceiveType = 2 AND  RH.numDomainID = v_numDomainID
      AND RH.dtCreatedDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (RH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      ORDER BY FromDate;
   ELSEIF v_tintReportType = 5
   then --Report Name: Receipt Journal (Summary format)

      open SWV_RefCur for
      SELECT COA.vcAccountName,SUM(coalesce(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(coalesce(GJD.numDebitAmt,0)) AS numDebitAmt
      FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      WHERE coalesce(GJH.numDepositId,0) > 0 AND GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
      AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      GROUP BY GJD.numChartAcntId,COA.vcAccountName
      ORDER BY COA.vcAccountName;
   ELSEIF v_tintReportType = 6
   then --Report Name: Total Receipts (Detailed)

      open SWV_RefCur for
      SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,GJD.varDescription AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
      FROM DepositMaster DM JOIN General_Journal_Header GJH ON DM.numDepositId = GJH.numDepositId
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJD.tintReferenceType = 6
      JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      WHERE DM.numDomainId = v_numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
      AND DM.dtDepositDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
      FROM DepositMaster DM JOIN General_Journal_Header GJH ON DM.numDepositId = GJH.numDepositId
      JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJD.tintReferenceType = 7
      JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      LEFT JOIN DivisionMaster D ON GJD.numCustomerId = D.numDivisionID
      LEFT JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      WHERE DM.numDomainId = v_numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
      AND DM.dtDepositDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT DM.dtDepositDate AS datEntry_Date,null,OM.vcpOppName || ' : ' || OBD.vcBizDocID || ' (' || CAST(DD.monAmountPaid AS VARCHAR(20)) || ')',null,null,
	DM.numDepositId AS numReference,CAST(8 AS SMALLINT) AS tintReferenceType
      FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
      JOIN OpportunityBizDocs OBD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
      JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId AND DD.numOppID = OM.numOppId
      WHERE DM.numDomainId = v_numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
      AND DM.dtDepositDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      ORDER BY datEntry_Date,numReference,7;
   ELSEIF v_tintReportType = 7
   then --Report Name: Total Disbursement Journal (Summary)

      open SWV_RefCur for
      SELECT COA.vcAccountName,SUM(coalesce(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(coalesce(GJD.numDebitAmt,0)) AS numDebitAmt
      FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      WHERE (coalesce(GJH.numBillPaymentID,0) > 0 OR coalesce(GJH.numCheckHeaderID,0) > 0) AND GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
      AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      GROUP BY GJD.numChartAcntId,COA.vcAccountName
      ORDER BY COA.vcAccountName;
   ELSEIF v_tintReportType = 8
   then --Report Name: Cash disbursement Journal (Detailed)

      DROP TABLE IF EXISTS tt_TEMPGJ CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPGJ ON COMMIT DROP AS
         SELECT coalesce(GJH.numCheckHeaderID,0) AS numCheckHeaderID,coalesce(GJH.numBillPaymentID,0) AS numBillPaymentID,
	GJH.datEntry_Date,COA.vcAccountName,numDebitAmt,numCreditAmt,GJD.tintReferenceType 
         FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
         WHERE GJH.numDomainId = v_numDomainID AND GJH.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
         AND (coalesce(GJH.numCheckHeaderID,0) > 0 OR coalesce(GJH.numBillPaymentID,0) > 0)
         AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0);
      open SWV_RefCur for
      SELECT GJH.datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
      FROM CheckHeader CH JOIN tt_TEMPGJ GJH ON CH.numCheckHeaderID = GJH.numCheckHeaderID AND GJH.tintReferenceType = 1
      WHERE CH.numDomainID = v_numDomainID AND CH.tintReferenceType = 1
      AND (CH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT GJH.datEntry_Date AS datEntry_Date,null,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
      FROM CheckHeader CH JOIN tt_TEMPGJ GJH ON CH.numCheckHeaderID = GJH.numCheckHeaderID AND GJH.tintReferenceType = 2
      JOIN DivisionMaster D ON CH.numDivisionID = D.numDivisionID
      JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      WHERE CH.numDomainID = v_numDomainID AND CH.tintReferenceType = 1
      AND (CH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT BPH.dtPaymentDate AS datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
      FROM BillPaymentHeader BPH JOIN tt_TEMPGJ GJH ON BPH.numBillPaymentID = GJH.numBillPaymentID AND GJH.tintReferenceType = 8
      LEFT JOIN CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
      WHERE BPH.numDomainId = v_numDomainID
      AND BPH.dtPaymentDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (BPH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT BPH.dtPaymentDate AS datEntry_Date,null AS numCheckNo,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
      FROM BillPaymentHeader BPH JOIN tt_TEMPGJ GJH ON BPH.numBillPaymentID = GJH.numBillPaymentID AND GJH.tintReferenceType = 9
      JOIN DivisionMaster D ON BPH.numDivisionId = D.numDivisionID
      JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
      WHERE BPH.numDomainId = v_numDomainID
      AND BPH.dtPaymentDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (BPH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT BPH.dtPaymentDate AS datEntry_Date,null,null,OM.vcpOppName || ' : ' || OBD.vcBizDocID || ' (' || CAST(BPD.monAmount AS VARCHAR(20)) || ')'
	,null,null,BPH.numBillPaymentID,CAST(10 AS SMALLINT)
      FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
      JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
      WHERE BPH.numDomainId = v_numDomainID
      AND BPH.dtPaymentDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (BPH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      UNION ALL
      SELECT BPH.dtPaymentDate AS datEntry_Date,null,null,COA.vcAccountName || '(' || CAST(GJD.numDebitAmt AS VARCHAR(20)) || ' : Bill-' || CAST(BH.numBillID AS VARCHAR(10)) || ')'
	,null,null,BPH.numBillPaymentID,CAST(10 AS SMALLINT)
      FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN BillHeader BH ON BH.numBillID = BPD.numBillID
      JOIN General_Journal_Header GJH ON BH.numBillID = GJH.numBillID JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      JOIN Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId = v_numDomainID
      WHERE BPH.numDomainId = v_numDomainID AND GJD.numDebitAmt > 0
      AND BPH.dtPaymentDate BETWEEN  v_dtFromDate AND v_dtToDate
      AND (BH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
      ORDER BY 1,numReference,8;
   end if;
   RETURN;
END; $$;

