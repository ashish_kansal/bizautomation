-- Stored procedure definition script USP_WorkFlowARAgingExecutionHistory_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
-- =============================================  
-- Author:  <Author,,Priya Sharma>  
-- Create date: <Create Date,,5thSep2018>  
-- Description: <Description,,To save records that are executed for a day>  
-- =============================================  
Create or replace FUNCTION USP_WorkFlowARAgingExecutionHistory_Insert(v_numDomainID NUMERIC(18,0) 
	 ,v_numRecordID NUMERIC(18,0)
	 ,v_numFormID NUMERIC(18,0)
	 ,v_numWFID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql   
 -- Add the parameters for the stored procedure here  
 
   AS $$
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   
  
    -- Insert statements for procedure here  
  
   
   INSERT INTO WorkFlowARAgingExecutionHistory(numDomainID
		,dtCreatedDate
		,numRecordID
		,numFormID
		,numWFID)
    VALUES(v_numDomainID
		,TIMEZONE('UTC',now())
		,v_numRecordID
		,v_numFormID
		,v_numWFID);
RETURN;
END; $$;  


