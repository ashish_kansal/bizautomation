-- Stored procedure definition script usp_GetEmailIdForContact for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetEmailIdForContact(v_numContactId NUMERIC 
--  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcEmail FROM AdditionalContactsInformation where numContactId = v_numContactId;
END; $$;












