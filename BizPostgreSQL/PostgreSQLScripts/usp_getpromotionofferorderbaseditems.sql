-- Stored procedure definition script USP_GetPromotionOfferOrderBasedItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPromotionOfferOrderBasedItems(v_numProId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numSiteId NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemName  VARCHAR(200);
   v_numBaseUnit  VARCHAR(100);
   v_monListPrice  VARCHAR(100);
   v_Id  INTEGER;
   v_tot  DOUBLE PRECISION;

   v_numWareHouseID  NUMERIC(9,0);
   v_UOMConversionFactor  DECIMAL(18,5);
BEGIN
   open SWV_RefCur for
   SELECT vcProName FROM PromotionOffer WHERE numProId = v_numProId AND numDomainId = v_numDomainID AND coalesce(bitEnabled,true) = true;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numProOrderBasedID NUMERIC(18,0),
      numProID NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      cSaleType  CHAR,
      cItems  CHAR,
      fltSubTotal  DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_temptbl_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTBL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTBL
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numProOrderBasedID NUMERIC(18,0),
      numProId NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      cSaleType  CHAR,
      cItems  CHAR,
      fltSubTotal  DOUBLE PRECISION,
      Items VARCHAR(200),
      numBaseUnit VARCHAR(100),
      monListPrice  VARCHAR(100)
   );

   insert into tt_TEMP(numProOrderBasedID, numProID, numDomainID, cSaleType, cItems, fltSubTotal)(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = v_numProId AND numDomainId = v_numDomainID AND coalesce(bitEnabled,true) = true);

   insert into tt_TEMPTBL(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = v_numProId AND numDomainId = v_numDomainID AND coalesce(bitEnabled,true) = true);
	
   While(Select Count(*) FROM tt_TEMP WHERE cSaleType = 'I' AND cItems = 'M') > 0 LOOP
      select   ID, fltSubTotal INTO v_Id,v_tot From tt_TEMP WHERE cSaleType = 'I' AND cItems = 'M'    LIMIT 1;
      v_vcItemName := CONCAT('',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'M' AND fltSubTotal = v_tot) placing '' from 1 for 2)),'');
      v_numBaseUnit := CONCAT('',(SELECT OVERLAY((SELECT CONCAT(', ',coalesce(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'M' AND fltSubTotal = v_tot) placing '' from 1 for 2)),'');
      v_monListPrice := CONCAT('',(SELECT OVERLAY((SELECT CONCAT(', ',coalesce(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'M' AND fltSubTotal = v_tot) placing '' from 1 for 2)),'');
      UPDATE tt_TEMPTBL
      SET Items = v_vcItemName,numBaseUnit = v_numBaseUnit,monListPrice = v_monListPrice
      WHERE Id = v_Id;
      Delete FROM tt_TEMP Where ID = v_Id;
   END LOOP;
   While(Select Count(*) FROM tt_TEMP WHERE cSaleType = 'I' AND cItems = 'S') > 0 LOOP
      select   ID, fltSubTotal INTO v_Id,v_tot From tt_TEMP WHERE cSaleType = 'I' AND cItems = 'S'    LIMIT 1;
      select   Item.vcItemName INTO v_vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'S' AND fltSubTotal = v_tot;
      select   coalesce(Item.numBaseUnit,0) INTO v_numBaseUnit FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'S' AND fltSubTotal = v_tot;
      select   coalesce(Item.monListPrice,0) INTO v_monListPrice FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND cItems = 'S' AND fltSubTotal = v_tot;
      UPDATE tt_TEMPTBL
      SET Items = v_vcItemName,numBaseUnit = v_numBaseUnit,monListPrice = v_monListPrice
      WHERE Id = v_Id;
      Delete FROM tt_TEMP Where ID = v_Id;
   END LOOP;

   open SWV_RefCur2 for
   SELECT DISTINCT(fltSubTotal), Items, numBaseUnit, monListPrice, cSaleType FROM tt_TEMPTBL ORDER BY fltSubTotal;

		--Table 3
   select   coalesce(numDefaultWareHouseID,0) INTO v_numWareHouseID FROM eCommerceDTL WHERE numDomainID = v_numDomainID AND numSiteId = v_numSiteId;
		 
      
   select   coalesce(fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)),0) INTO v_UOMConversionFactor FROM Item I
   JOIN PromotionOfferOrderBased POB ON POB.numProItemId = I.numItemCode WHERE numProId = v_numProId AND POB.numDomainId = v_numDomainID AND coalesce(POB.bitEnabled,true) = true;

   open SWV_RefCur3 for
   SELECT I.vcItemName, numItemCode, fltSubTotal
			,coalesce(CASE WHEN I.charItemType = 'P'
   THEN v_UOMConversionFactor*W.monWListPrice
   ELSE v_UOMConversionFactor*monListPrice
   END,0) AS monListPrice
   FROM PromotionOfferOrderBased
   INNER JOIN Item I ON PromotionOfferOrderBased.numProItemId = I.numItemCode
   LEFT JOIN  WareHouseItems W  ON W.numItemID = I.numItemCode AND numWareHouseID = v_numWareHouseID
   WHERE numProId = v_numProId AND PromotionOfferOrderBased.numDomainId = v_numDomainID AND coalesce(PromotionOfferOrderBased.bitEnabled,true) = true;
   RETURN;
END; $$; 


