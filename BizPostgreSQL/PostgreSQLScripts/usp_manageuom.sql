CREATE OR REPLACE FUNCTION USP_ManageUOM(INOUT v_numUOMId NUMERIC(9,0) ,
	v_numDomainID NUMERIC(9,0),
	v_vcUnitName VARCHAR(100),
	v_tintUnitType SMALLINT,
	v_bitEnabled BOOLEAN)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numUOMId = 0 AND (EXISTS(SELECT vcUnitName from UOM Where vcUnitName = v_vcUnitName AND UOM.numDomainID = v_numDomainID AND UOM.tintUnitType = v_tintUnitType)) then

      RAISE EXCEPTION 'UOM_ALREADY_EXISTS';
      RETURN;
   end if;
   IF v_numUOMId = 0 then

      INSERT INTO UOM(numDomainID,vcUnitName,tintUnitType,bitEnabled)
	VALUES(v_numDomainID,v_vcUnitName,v_tintUnitType,v_bitEnabled);
	
      v_numUOMId := CURRVAL('UOM_seq');
   ELSE
      UPDATE UOM SET
      vcUnitName = v_vcUnitName,tintUnitType = v_tintUnitType,bitEnabled = v_bitEnabled
      WHERE numUOMId = v_numUOMId;
   end if;
END; $$;



