-- FUNCTION: public.usp_getactitemsmail(boolean, numeric, character varying, refcursor)

-- DROP FUNCTION public.usp_getactitemsmail(boolean, numeric, character varying, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getactitemsmail(
	v_mode boolean DEFAULT false,
	v_time numeric DEFAULT 30,
	v_strcomid character varying DEFAULT NULL::character varying,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 14:56:56 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF v_Mode = false then
    
      open SWV_RefCur for SELECT numCommId,
             cast(vcSubject as VARCHAR(255)),
             REPLACE(REPLACE(CAST(vcDocDesc AS TEXT),'##CommName##',fn_GetListItemName(bitTask)),'##DueDate##',FormatedDateTimeFromDate(dtEndTime,Communication.numDomainID)) AS vcbody,
             numEmailTemplate AS numEmailTemplate,
             vcEmail,
             ACI.numDomainID ---Added Domain ID for Fetching SMTP details for domain
      FROM   Communication
      JOIN AdditionalContactsInformation ACI
      ON numAssign = ACI.numContactId
      JOIN GenericDocuments
      ON numEmailTemplate = numGenericDocID
      WHERE  bitalert = 'true'
      AND bitEmailSent = false
      AND CASE
      WHEN bitsendEmailTemp = true THEN dtEndTime+CAST(tinthours || 'hour' as interval)
      ELSE dtEndTime+CAST(-tinthours || 'hour' as interval)
      END BETWEEN TIMEZONE('UTC',now())+INTERVAL '0 minute' AND TIMEZONE('UTC',now())+CAST(v_Time || 'minute' as interval);
   ELSE
      IF v_strComId <> '' then
        
         UPDATE Communication
         SET    bitEmailSent = true
         WHERE  numCommId IN(SELECT CAST(items AS NUMERIC) FROM   Split(v_strComId,','));
      end if;
   end if;
/****** Object:  StoredProcedure [dbo].[usp_GetAdvSearchResultsForCriteria]    Script Date: 07/26/2008 16:16:09 ******/
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getactitemsmail(boolean, numeric, character varying, refcursor)
    OWNER TO postgres;
