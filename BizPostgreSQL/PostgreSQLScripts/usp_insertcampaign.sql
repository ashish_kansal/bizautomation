-- Stored procedure definition script usp_InsertCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertCampaign(v_textMsgDocument TEXT DEFAULT '',
	v_textSubject TEXT DEFAULT '',
	v_vcEmailId VARCHAR(100) DEFAULT '',
	v_vcCampaignNumber VARCHAR(50) DEFAULT '',
	v_intNoOfContacts INTEGER DEFAULT 0,
	v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_bintCreatedDate BIGINT DEFAULT 0,
	v_numModifiedBy NUMERIC(9,0) DEFAULT 0,
	v_bintModifiedDate BIGINT DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_textProfile TEXT DEFAULT ''   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO Campaign(textMsgDocument,textSubject,vcEmailId,vcCampaignNumber,intNoOfContacts,numCreatedBy,
	bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainID,textProfile)
	VALUES(v_textMsgDocument,v_textSubject,v_vcEmailId,v_vcCampaignNumber,v_intNoOfContacts,v_numCreatedBy,
	v_bintCreatedDate,v_numModifiedBy,v_bintModifiedDate,v_numDomainID,v_textProfile);
RETURN;
END; $$;


