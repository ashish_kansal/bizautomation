-- Stored procedure definition script usp_InsertCntDocumentDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertCntDocumentDetails(v_numUserID NUMERIC(9,0) DEFAULT 0,
	v_vcFileName VARCHAR(100) DEFAULT 0,
	v_vcDisplayName VARCHAR(100) DEFAULT 0,
	v_bintDateOfUpload BIGINT DEFAULT 0,
	v_numCntID NUMERIC(9,0) DEFAULT 0,
	v_numSize NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
--

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	--Insertion into Table  
	
   INSERT INTO Cntdocuments(numUserID,vcFileName,vcDisplayName,bintDateOfUpload,numCntID,numSize,numDomainID)
	VALUES(v_numUserID,v_vcFileName,v_vcDisplayName,v_bintDateOfUpload,v_numCntID,v_numSize,v_numDomainID);
RETURN;
END; $$;


