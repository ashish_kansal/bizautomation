-- Function definition script GetTotalQtyByUOM for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalQtyByUOM(v_numOppBizDocID NUMERIC(18,0))
RETURNS VARCHAR(3000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcTotalQtybyUOM  VARCHAR(3000) DEFAULT '';
BEGIN
   select   COALESCE(v_vcTotalQtybyUOM,'') ||(CASE WHEN LENGTH(v_vcTotalQtybyUOM) > 0 THEN ', ' ELSE '' END) || CONCAT(TEMP.vcUOMName,' - ',SUM(numQty)) INTO v_vcTotalQtybyUOM FROM(SELECT
      OI.numUOMId,
				fn_GetUOMName(OI.numUOMId) AS vcUOMName,
				CAST(coalesce((fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0))*SUM(OBI.numUnitHour)),0) AS NUMERIC(18,2)) AS numQty
      FROM
      OpportunityBizDocItems OBI
      INNER JOIN
      OpportunityItems OI
      ON
      OBI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      numOppBizDocID = v_numOppBizDocID
      AND coalesce(OI.numUOMId,0) > 0
      GROUP BY
      I.numItemCode,I.numDomainID,I.numBaseUnit,OI.numUOMId) AS TEMP  GROUP BY
   TEMP.numUOMId,TEMP.vcUOMName  ORDER BY
   TEMP.vcUOMName;


   RETURN coalesce(v_vcTotalQtybyUOM,'');
END; $$;

