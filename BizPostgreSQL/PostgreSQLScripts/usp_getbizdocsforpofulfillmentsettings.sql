-- Stored procedure definition script USP_GetBizDocsForPOFulfillmentSettings for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBizDocsForPOFulfillmentSettings(v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_numListID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql   
	/*********************************************Master Biz Doc List Starts******************************************************************************/
   AS $$
   DECLARE
   v_BizDocType  INTEGER;
   v_numOppId  NUMERIC(9,0);
   SWV_ID INTEGER;
   SWV_numListItemID NUMERIC(18,0);
   SWV_vcData VARCHAR(100);
   v_SWV_RCur refcursor;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempMasterBizDocList_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPMASTERBIZDOCLIST CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPMASTERBIZDOCLIST
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numListItemID NUMERIC(18,0),
      vcData  VARCHAR(100)
   );

   v_BizDocType := 2;

   SELECT USP_GetBizDocType(v_BizDocType,v_numDomainID,'v_SWV_RCur') INTO v_SWV_RCur;
   FETCH v_SWV_RCur INTO SWV_numListItemID,SWV_vcData;
   WHILE FOUND LOOP
      INSERT INTO tt_TEMPMASTERBIZDOCLIST(numListItemID, vcData)  VALUES(SWV_numListItemID, SWV_vcData);

      FETCH v_SWV_RCur INTO SWV_numListItemID,SWV_vcData;
   END LOOP;
   CLOSE v_SWV_RCur;
		--EXEC USP_GetMasterListItemsForBizDocs @ListID, @numDomainID, @numOppId

   open SWV_RefCur for
   SELECT * FROM tt_TEMPMASTERBIZDOCLIST; 
	/*********************************************Master Biz Doc List Ends******************************************************************************/

	/*********************************************Opportunity Biz Doc List Starts******************************************************************************/
   BEGIN
      CREATE TEMP SEQUENCE tt_tempOppBizDocList_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPOPPBIZDOCLIST CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPBIZDOCLIST
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numBizDocID NUMERIC(18,0),
      vcTemplate VARCHAR(100)
   );

	

	--Purchase Order
   select   Opp.numOppId INTO v_numOppId FROM OpportunityMaster Opp
   JOIN OpportunityItems OI ON OI.numOppId = Opp.numOppId
   JOIN Item I ON I.numItemCode = OI.numItemCode WHERE tintopptype = 2 AND tintoppstatus = 1
   AND coalesce(bitStockTransfer,false) = false AND tintshipped = 0 AND numUnitHour -coalesce(numUnitHourReceived,0) > 0
   AND I.charItemType IN('P','N','S') AND (OI.bitDropShip = false OR OI.bitDropShip IS NULL)
   AND Opp.numDomainId = 72   ORDER BY  Opp.bintCreatedDate DESC LIMIT 1;

   INSERT INTO tt_TEMPOPPBIZDOCLIST(numBizDocID, vcTemplate)
   SELECT OPP.numBizDocId,
				coalesce((SELECT  vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = OPP.numBizDocTempID LIMIT 1),'') AS vcTemplateName
   FROM
   OpportunityBizDocs OPP
   WHERE OPP.numoppid = v_numOppId;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPOPPBIZDOCLIST; 
	/*********************************************Opportunity Biz Doc List Ends******************************************************************************/

   open SWV_RefCur3 for
   SELECT * FROM tt_TEMPMASTERBIZDOCLIST M WHERE NOT EXISTS(SELECT * FROM tt_TEMPOPPBIZDOCLIST B WHERE M.numListItemID = B.numBizDocID);

	
   RETURN;
END; $$;



