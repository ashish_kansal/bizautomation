-- Stored procedure definition script usp_GetCompanyName for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyName(v_numDivisionID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(vcCompanyname as VARCHAR(255)) FROM CompanyInfo C
   Join DivisionMaster D
   on D.numCompanyID = C.numCompanyId
   WHERE D.numDivisionID = v_numDivisionID;
END; $$;












