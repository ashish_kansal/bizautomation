-- Stored procedure definition script USP_GetEnforcedMinOrderSubTotal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetEnforcedMinOrderSubTotal(v_numDomainID NUMERIC(9,0) DEFAULT 0, 
	v_numDivisionID NUMERIC(18,0) DEFAULT NULL,
	v_numTotalAmount DECIMAL DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyId  NUMERIC(18,0);
   v_vcProfile  NUMERIC(18,0);
   v_fltMinOrderAmount  DOUBLE PRECISION ;
BEGIN
   select   numCompanyID INTO v_numCompanyId FROM DivisionMaster WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
	--SELECT @numCompanyId

   IF(SELECT COUNT(*) FROM CompanyInfo WHERE numCompanyId = v_numCompanyId AND vcProfile <> 0 AND numDomainID = v_numDomainID) > 0 then
	
      select   vcProfile INTO v_vcProfile FROM CompanyInfo WHERE numCompanyId = v_numCompanyId AND numDomainID = v_numDomainID;
		--SELECT @vcProfile 
		--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
      IF(SELECT COUNT(*) FROM Listdetails WHERE numListItemID = v_vcProfile AND numDomainid = v_numDomainID) > 0 then
		
			--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
		
         select   fltMinOrderAmount INTO v_fltMinOrderAmount FROM SalesOrderRule WHERE numlistitemid = v_vcProfile
         AND bitEnforceMinOrderAmount <> false
         AND numDomainID = v_numDomainID;
		 
		 v_fltMinOrderAmount := COALESCE(v_fltMinOrderAmount,0);
		 
         IF v_numTotalAmount >= v_fltMinOrderAmount then
			
            open SWV_RefCur for
            SELECT 1 AS returnVal;
            RETURN;
         ELSE
            open SWV_RefCur for
            SELECT 0 AS returnVal, v_fltMinOrderAmount AS MinOrderAmount;
            RETURN;
         end if;
      ELSE
         open SWV_RefCur for
         SELECT 1 AS returnVal;
         RETURN;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 1 AS returnVal;
      RETURN;
   end if;
END; $$;


