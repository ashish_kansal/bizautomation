CREATE OR REPLACE FUNCTION USP_GetProfileEGroupContacts(v_numEmailGroupID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		EG.numContactId,
        ACI.numRecOwner
	FROM 
		ProfileEGroupDTL EG
	LEFT OUTER JOIN 
		AdditionalContactsInformation ACI
	ON 
		ACI.numContactId = EG.numContactId
	WHERE 
		numEmailGroupID = v_numEmailGroupID;
END; $$;












