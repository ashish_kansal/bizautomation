-- Function definition script IsSalesOrderReadyToFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION IsSalesOrderReadyToFulfillment(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_tintCommitAllocation SMALLINT)
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitReadyToShip  BOOLEAN DEFAULT 1;
BEGIN
   IF EXISTS(SELECT tintshipped FROM OpportunityMaster WHERE numDomainId = v_numDomainID
   AND numOppId = v_numOppID AND coalesce(tintshipped,0) = 1) OR(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
																			coalesce(OI.numUnitHour,0) AS OrderedQty,
																			coalesce(TempFulFilled.FulFilledQty,0) AS FulFilledQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND coalesce(bitFulFilled,false) = true) AS TempFulFilled on TRUE
      WHERE
      OI.numOppId = v_numOppID
      AND UPPER(I.charItemType) = 'P'
      AND coalesce(OI.bitDropShip,false) = false) X
   WHERE
   X.OrderedQty <> X.FulFilledQty) = 0 then
	
      v_bitReadyToShip := false;
   ELSE
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numOppID NUMERIC(18,0),
         numOppItemID NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numWarehosueItemID NUMERIC(18,0),
         numQtyToShip DOUBLE PRECISION,
         numOnHand DOUBLE PRECISION,
         numAllocation DOUBLE PRECISION,
         bitWorkOrder BOOLEAN,
         bitBuildCompleted BOOLEAN
      );
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted)
      SELECT
      OI.numOppId
			,OI.numoppitemtCode
			,0
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,coalesce(numUnitHour,0) -coalesce(numQtyShipped,0)
			,coalesce(WI.numOnHand,0)
			,coalesce(WI.numAllocation,0)
			,coalesce(OI.bitWorkOrder,false)
			,CAST((CASE WHEN WO.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BOOLEAN)
      FROM
      OpportunityItems OI
      INNER JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      LEFT JOIN
      WorkOrder WO
      ON
      OI.numOppId = WO.numOppId
      AND OI.numoppitemtCode = WO.numOppItemID
      AND coalesce(numOppChildItemID,0) = 0
      AND coalesce(numOppKitChildItemID,0) = 0
      WHERE
      OI.numOppId = v_numOppID
      AND coalesce(OI.bitDropShip,false) = false;
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted)
      SELECT
      OKI.numOppId
			,OKI.numOppItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq_Orig*(coalesce(OI.numUnitHour,0) -coalesce(OI.numQtyShipped,0))
			,coalesce(WI.numOnHand,0)
			,coalesce(WI.numAllocation,0)
			,CAST((CASE WHEN coalesce(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) AS BOOLEAN)
			,CAST((CASE WHEN WO.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BOOLEAN)
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numOppId = OKI.numOppId
      AND OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      LEFT JOIN
      WorkOrder WO
      ON
      OKI.numOppId = WO.numOppId
      AND OKI.numOppItemID = WO.numOppItemID
      AND coalesce(WO.numOppChildItemID,0) = OKI.numOppChildItemID
      AND coalesce(WO.numOppKitChildItemID,0) = 0
      WHERE
      OI.numOppId = v_numOppID;
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted)
      SELECT
      OKCI.numOppId
			,OKCI.numOppItemID
			,0
			,OKCI.numItemID
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq_Orig*t1.numQtyToShip
			,coalesce(WI.numOnHand,0)
			,coalesce(WI.numAllocation,0)
			,CAST((CASE WHEN coalesce(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) AS BOOLEAN)
			,CAST((CASE WHEN WO.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BOOLEAN)
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      tt_TEMP t1
      ON
      OKCI.numOppChildItemID = t1.numOppChildItemID
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      LEFT JOIN
      WorkOrder WO
      ON
      OKCI.numOppId = WO.numOppId
      AND OKCI.numOppItemID = WO.numOppItemID
      AND coalesce(WO.numOppChildItemID,0) = OKCI.numOppChildItemID
      AND coalesce(WO.numOppKitChildItemID,0) = OKCI.numOppKitChildItemID
      WHERE
      OKCI.numOppId = v_numOppID;
      IF(SELECT COUNT(*) FROM tt_TEMP WHERE coalesce(bitWorkOrder,false) = true AND coalesce(bitBuildCompleted,false) = false) > 0 then
		
         v_bitReadyToShip := false;
      ELSEIF v_tintCommitAllocation = 2
      then
		
         IF(SELECT COUNT(*) FROM tt_TEMP WHERE numQtyToShip > numOnHand) > 0 then
			
            v_bitReadyToShip := false;
         end if;
      ELSEIF(SELECT COUNT(*) FROM tt_TEMP WHERE numQtyToShip > numAllocation) > 0
      then
		
         v_bitReadyToShip := false;
      end if;
   end if;

   RETURN v_bitReadyToShip;
END; $$;

