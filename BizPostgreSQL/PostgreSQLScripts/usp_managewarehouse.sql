-- Stored procedure definition script USP_ManageWarehouse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageWarehouse(v_byteMode SMALLINT DEFAULT 0,      
v_numWareHouseID NUMERIC(9,0) DEFAULT 0,      
v_vcWarehouse VARCHAR(50) DEFAULT NULL,          
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numAddressID NUMERIC(18,0) DEFAULT 0,
v_vcPrintNodeAPIKey VARCHAR(100) DEFAULT '',
v_vcPrintNodePrinterID VARCHAR(100) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewWarehouseID  NUMERIC(18,0);
BEGIN
   IF v_byteMode = 0 then
	
      IF v_numWareHouseID = 0 then
         INSERT INTO Warehouses(vcWareHouse
				,numDomainID
				,numAddressID
				,vcPrintNodeAPIKey
				,vcPrintNodePrinterID)
			VALUES(v_vcWarehouse
				,v_numDomainID
				,v_numAddressID
				,v_vcPrintNodeAPIKey
				,v_vcPrintNodePrinterID);
			
         v_numNewWarehouseID := CURRVAL('Warehouses_seq');
         INSERT INTO WareHouseItems(numItemID,
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified)
         SELECT
         numItemCode
				,v_numNewWarehouseID
				,NULL
				,0
				,0
				,monListPrice
				,''
				,vcSKU
				,numBarCodeId
				,v_numDomainID
				,LOCALTIMESTAMP
         FROM
         Item
         WHERE
         numDomainID = v_numDomainID
         AND charItemType = 'P'
         AND 1 =(CASE WHEN coalesce(numItemGroup,0) > 0 THEN(CASE WHEN coalesce(bitMatrix,false) = true THEN 1 ELSE 0 END) ELSE 1 END);
         INSERT  INTO WareHouseItems_Tracking(numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate)
         SELECT
         WHI.numWareHouseItemID
				,coalesce(WHI.numOnHand,0)
				,coalesce(WHI.numonOrder,0)
				,coalesce(WHI.numReorder,0)
				,coalesce(WHI.numAllocation,0)
				,coalesce(WHI.numBackOrder,0)
				,WHI.numDomainID
				,'INSERT WareHouse'
				,1
				,I.numItemCode
				,TIMEZONE('UTC',now())
				,0
				,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END)
				,(SELECT SUM(coalesce(numOnHand,0)) FROM WareHouseItems WHERE numItemID = I.numItemCode)
				,TIMEZONE('UTC',now())
         FROM
         Item I
         INNER JOIN
         WareHouseItems WHI
         ON
         I.numItemCode = WHI.numItemID
         WHERE
         WHI.numWareHouseID = v_numNewWarehouseID
         AND WHI.numDomainID = v_numDomainID;
         open SWV_RefCur for
         SELECT v_numNewWarehouseID;
      ELSEIF v_numWareHouseID > 0
      then
		
         UPDATE
         Warehouses
         SET
         vcWareHouse = v_vcWarehouse,numAddressID = v_numAddressID,vcPrintNodeAPIKey = v_vcPrintNodeAPIKey,
         vcPrintNodePrinterID = v_vcPrintNodePrinterID
         WHERE
         numWareHouseID = v_numWareHouseID;
         open SWV_RefCur for
         SELECT v_numWareHouseID;
      end if;
   ELSEIF v_byteMode = 1
   then
	
      IF(SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID = v_numWareHouseID  AND (coalesce(numOnHand,0) > 0 OR coalesce(numonOrder,0) > 0 OR coalesce(numAllocation,0) > 0 OR coalesce(numBackOrder,0) > 0)) > 0 then
		
         RAISE EXCEPTION 'Dependant';
      ELSEIF EXISTS(SELECT OI.numoppitemtCode FROM WareHouseItems WI INNER JOIN OpportunityItems OI ON WI.numWareHouseItemID = OI.numWarehouseItmsID WHERE WI.numWareHouseID = v_numWareHouseID)
      then
		
         RAISE EXCEPTION 'Dependant';
      ELSE
         DELETE FROM WareHouseItems WHERE coalesce(numWareHouseID,0) = coalesce(v_numWareHouseID,0);
         DELETE FROM Warehouses WHERE numWareHouseID = v_numWareHouseID;
      end if;
   end if;
   RETURN;
END; $$;


