-- Stored procedure definition script USP_DemandForecast_UpdateInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_UpdateInventory(v_numOppID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


	-- Update Inventory when order is created from demand forecast
   PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,v_numUserCntID);
   RETURN;
END; $$;


