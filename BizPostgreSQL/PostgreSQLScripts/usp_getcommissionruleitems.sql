CREATE OR REPLACE FUNCTION USP_GetCommissionRuleItems(v_numRuleID NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_tintRuleAppType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintRuleAppType = 1 then

      open SWV_RefCur for
      select numComRuleItemID,numComRuleID, vcItemName,txtItemDesc,vcModelID, GetListIemName(numItemClassification) AS ItemClassification from Item
      join CommissionRuleItems
      on numValue = numItemCode
      where numDomainID = v_numDomainID and numComRuleID = v_numRuleID and tintType = 1;
   end if;    
   if v_tintRuleAppType = 2 then

      open SWV_RefCur for
      SELECT numListItemID AS numItemClassification,coalesce(vcData,'-') AS ItemClassification  FROM Listdetails WHERE numListID = 36 AND numDomainid = v_numDomainID AND numListItemID NOT IN(select numValue from CommissionRuleItems where tintType = 2 and numComRuleID = v_numRuleID);
      open SWV_RefCur2 for
      select numComRuleItemID,numComRuleID,numValue,GetListIemName(numValue) AS ItemClassification,(SELECT COUNT(*) FROM Item I1 WHERE I1.numItemClassification = numValue) AS ItemsCount from
      CommissionRuleItems
      where numComRuleID = v_numRuleID and tintType = 2;
   end if;
   IF v_tintRuleAppType = 3 then

      open SWV_RefCur for
      select numComRuleOrgID,numComRuleID,vcCompanyName,GetListIemName(CompanyInfo.numCompanyType) || ',' || GetListIemName(CompanyInfo.vcProfile) AS Relationship,
 (SELECT coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') || ', ' || coalesce(vcEmail,'')  FROM AdditionalContactsInformation A WHERE A.numDivisionId = DivisionMaster.numDivisionID AND coalesce(A.bitPrimaryContact,false) = true) AS PrimaryContact  from DivisionMaster
      join CompanyInfo on DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      JOIN CommissionRuleOrganization ON numValue = DivisionMaster.numDivisionID
      WHERE DivisionMaster.numDomainID = v_numDomainID and numComRuleID = v_numRuleID;
   end if;
   IF v_tintRuleAppType = 4 then

      open SWV_RefCur for
      select numComRuleOrgID,numComRuleID,L1.vcData || ' - ' || L2.vcData as RelProfile from CommissionRuleOrganization DTL
      Join Listdetails L1
      on L1.numListItemID = DTL.numValue
      Join Listdetails L2
      on L2.numListItemID = DTL.numProfile
      where numComRuleID = v_numRuleID;
   end if;
   RETURN;
END; $$;  


