-- Function definition script GetMigrationString for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetMigrationString(v_value TEXT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
BEGIN
   return case
   when NULLIF(v_value,'') is null
   then 'null'
   when v_value = ''
   then '""'
   else '"' || replace(replace(replace(v_value,'"','""'),'“','""'),'”','""') || '"'
   end;
END; $$;

