-- Stored procedure definition script USP_ContractsLog for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ContractsLog(v_numDomainID NUMERIC(18,0)
	,v_numContractId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTotalTime  NUMERIC(18,0);
BEGIN
   v_numTotalTime := coalesce((SELECT((coalesce(numHours,0)*60*60)+(coalesce(numMinutes,0)*60)) FROM Contracts WHERE numDomainId = v_numDomainID AND numContractId = v_numContractId),0);

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numContractId NUMERIC(18,0),
      vcType VARCHAR(300),
      dtmCreatedOn TIMESTAMP,
      numBalance VARCHAR(300),
      dtStartTime TIMESTAMP,
      dtEndTime TIMESTAMP
   );

   INSERT INTO tt_TEMP(numContractId
		,vcType
		,dtmCreatedOn
		,numBalance
		,dtStartTime
		,dtEndTime)
   SELECT
   CL.numContractId
		,CAST(CASE
   WHEN C.intType = 3 THEN 'Incidents'
   WHEN C.intType = 1 THEN(CASE
      WHEN coalesce(CL.tintRecordType,0) = 1 THEN CONCAT('<a href="../projects/frmProjects.aspx?ProId=',SP.numProjectId,'" target="_blank">Project Task</a>')
      WHEN coalesce(CL.tintRecordType,0) = 2 THEN CONCAT('<a href="../admin/ActionItemDetailsOld.aspx?CommId=',CL.numReferenceId,
         '&lngType=0" target="_blank">Communication</a>')
      WHEN coalesce(CL.tintRecordType,0) = 3 THEN 'Calendar'
      WHEN CP.numCaseId IS NOT NULL THEN CONCAT('<a href="../cases/frmCases.aspx?CaseID=',CL.numReferenceId,'" target="_blank">Case</a>')
      ELSE '-'
      END)
   ELSE '-'
   END AS VARCHAR(300)) AS vcType,
		CL.dtmCreatedOn,
		CAST(fn_SecondsConversion(cast(NULLIF(CAST((v_numTotalTime -coalesce((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId = v_numContractId AND CLInner.numContractsLogId <= CL.numContractsLogId),0)) AS VARCHAR(30)),'') as INTEGER)) AS VARCHAR(300)),
		CASE
   WHEN COM.numCommId > 0
   THEN COM.dtStartTime
   WHEN SP.numTaskId > 0 THEN(SELECT  dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId = SP.numTaskId AND tintAction = 1 LIMIT 1)
   ELSE NULL
   END AS dtStartTime,
		CASE
   WHEN COM.numCommId > 0
   THEN COM.dtEndTime
   WHEN SP.numTaskId > 0
   THEN(SELECT  dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId = SP.numTaskId AND tintAction = 4 LIMIT 1)
   ELSE NULL
   END AS dtEndTime
   FROM
   Contracts C
   INNER JOIN
   ContractsLog CL
   ON
   C.numContractId = CL.numContractId
   LEFT JOIN
   Cases AS CP
   ON
   CL.numReferenceId = CP.numCaseId
   AND coalesce(CL.tintRecordType,0) NOT IN(1,2,3)
   LEFT JOIN
   Communication AS COM
   ON
   CL.numReferenceId = COM.numCommId
   AND coalesce(CL.tintRecordType,0) = 2
   LEFT JOIN
   StagePercentageDetailsTask AS SP
   ON
   CL.numReferenceId = SP.numTaskId
   AND coalesce(CL.tintRecordType,0) = 1
   WHERE
   C.numDomainId = v_numDomainID
   AND C.numContractId = v_numContractId
   ORDER BY
   CL.numContractsLogId ASC;

	
open SWV_RefCur for SELECT  ID AS "ID",
      numContractId AS "numContractId",
      vcType AS "vcType",
      dtmCreatedOn AS "dtmCreatedOn",
      numBalance AS "numBalance",
      dtStartTime AS "dtStartTime",
      dtEndTime AS "dtEndTime" FROM tt_TEMP;
END; $$;












