-- Stored procedure definition script USP_StagePercentageDetails_ChangeBusinessProcess for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetails_ChangeBusinessProcess(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)                        
	,v_tintProcessType SMALLINT
	,v_numRecordID NUMERIC(18,0)
	,v_numProcessID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewProcessId  NUMERIC(18,0) DEFAULT 0;
  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF EXISTS(SELECT
   ID
   FROM
   StagePercentageDetailsTaskTimeLog SPDTTL
   WHERE
   SPDTTL.numTaskId IN(SELECT
      SPDT.numTaskId
      FROM
      StagePercentageDetailsTask SPDT
      WHERE
      1 =(CASE
      WHEN v_tintProcessType = 1
      THEN(CASE WHEN SPDT.numProjectId = v_numRecordID THEN 1 ELSE 0 END)
      WHEN v_tintProcessType = 3
      THEN(CASE WHEN SPDT.numWorkOrderId = v_numRecordID THEN 1 ELSE 0 END)
      ELSE 0
      END))) then
	
      RAISE EXCEPTION 'TASK_IS_ALREADY_STARTED';
   ELSE
      BEGIN
         -- BEGIN TRANSACTION
IF v_tintProcessType = 1 then
			
            DELETE FROM StagePercentageDetailsTask WHERE numProjectId = v_numRecordID;
            DELETE FROM StagePercentageDetails WHERE numProjectid = v_numRecordID;
            DELETE FROM Sales_process_List_Master WHERE numProjectId = v_numRecordID;
            IF(v_numProcessID > 0) then
				
               INSERT INTO Sales_process_List_Master(Slp_Name,
						numDomainID,
						Pro_Type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						numOppId,
						numTaskValidatorId,
						numProjectId,
						numWorkOrderId)
               SELECT
               Slp_Name,
						numDomainID,
						Pro_Type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						0,
						numTaskValidatorId,
						v_numRecordID,
						0
               FROM
               Sales_process_List_Master
               WHERE
               Slp_Id = v_numProcessID;
               v_numNewProcessId := CURRVAL('Sales_process_List_Master_seq');
               UPDATE ProjectsMaster SET numBusinessProcessID = v_numNewProcessId,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numProId = v_numRecordID;
               INSERT INTO StagePercentageDetails(numStagePercentageId,
						tintConfiguration,
						vcStageName,
						numdomainid,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						slp_id,
						numAssignTo,
						vcMileStoneName,
						tintPercentage,
						tinProgressPercentage,
						dtStartDate,
						numParentStageID,
						intDueDays,
						numProjectid,
						numOppid,
						vcDescription,
						bitIsDueDaysUsed,
						numTeamId,
						bitRunningDynamicMode,
						numStageOrder,
						numWorkOrderId)
               SELECT
               numStagePercentageId,
						tintConfiguration,
						vcStageName,
						numdomainid,
						v_numUserCntID,
						LOCALTIMESTAMP,
						v_numUserCntID,
						LOCALTIMESTAMP,
						v_numNewProcessId,
						numAssignTo,
						vcMileStoneName,
						tintPercentage,
						tinProgressPercentage,
						LOCALTIMESTAMP,
						numParentStageID,
						intDueDays,
						v_numRecordID,
						0,
						vcDescription,
						bitIsDueDaysUsed,
						numTeamId,
						bitRunningDynamicMode,
						numStageOrder,
						0
               FROM
               StagePercentageDetails
               WHERE
               slp_id = v_numProcessID;
               INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
						vcTaskName,
						numHours,
						numMinutes,
						numAssignTo,
						numDomainID,
						numCreatedBy,
						dtmCreatedOn,
						numOppId,
						numProjectId,
						numParentTaskId,
						bitDefaultTask,
						bitSavedTask,
						numOrder,
						numReferenceTaskId,
						numWorkOrderId)
               SELECT(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numProjectid = v_numRecordID LIMIT 1),
						vcTaskName,
						numHours,
						numMinutes,
						ST.numAssignTo,
						v_numDomainID,
						v_numUserCntID,
						LOCALTIMESTAMP,
						0,
						v_numRecordID,
						0,
						true,
						CASE WHEN SP.bitRunningDynamicMode = true THEN false ELSE true END,
						numOrder,
						numTaskId,
						0
               FROM
               StagePercentageDetailsTask AS ST
               LEFT JOIN
               StagePercentageDetails As SP
               ON
               ST.numStageDetailsId = SP.numStageDetailsId
               WHERE
               coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numWorkOrderId,0) = 0 AND
               ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
                  StagePercentageDetails As ST
                  WHERE
                  ST.slp_id = v_numProcessID)
               ORDER BY ST.numOrder;
            end if;
         ELSEIF v_tintProcessType = 3
         then
			
            DELETE FROM StagePercentageDetailsTask WHERE numWorkOrderId = v_numRecordID;
            DELETE FROM StagePercentageDetails WHERE numWorkOrderId = v_numRecordID;
            DELETE FROM Sales_process_List_Master WHERE numWorkOrderId = v_numRecordID;
            IF(v_numProcessID > 0) then
				
               INSERT INTO Sales_process_List_Master(Slp_Name,
						numDomainID,
						Pro_Type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						numOppId,
						numTaskValidatorId,
						numProjectId,
						numWorkOrderId)
               SELECT
               Slp_Name,
						numDomainID,
						Pro_Type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						0,
						numTaskValidatorId,
						0,
						v_numRecordID
               FROM
               Sales_process_List_Master
               WHERE
               Slp_Id = v_numProcessID;
               v_numNewProcessId := CURRVAL('Sales_process_List_Master_seq');
               UPDATE WorkOrder SET numBuildProcessId = v_numNewProcessId,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numWOId = v_numRecordID;
               INSERT INTO StagePercentageDetails(numStagePercentageId,
						tintConfiguration,
						vcStageName,
						numdomainid,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						slp_id,
						numAssignTo,
						vcMileStoneName,
						tintPercentage,
						tinProgressPercentage,
						dtStartDate,
						numParentStageID,
						intDueDays,
						numProjectid,
						numOppid,
						vcDescription,
						bitIsDueDaysUsed,
						numTeamId,
						bitRunningDynamicMode,
						numStageOrder,
						numWorkOrderId)
               SELECT
               numStagePercentageId,
						tintConfiguration,
						vcStageName,
						numdomainid,
						v_numUserCntID,
						LOCALTIMESTAMP,
						v_numUserCntID,
						LOCALTIMESTAMP,
						v_numNewProcessId,
						numAssignTo,
						vcMileStoneName,
						tintPercentage,
						tinProgressPercentage,
						LOCALTIMESTAMP,
						numParentStageID,
						intDueDays,
						0,
						0,
						vcDescription,
						bitIsDueDaysUsed,
						numTeamId,
						bitRunningDynamicMode,
						numStageOrder,
						v_numRecordID
               FROM
               StagePercentageDetails
               WHERE
               slp_id = v_numProcessID;
               INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
						vcTaskName,
						numHours,
						numMinutes,
						numAssignTo,
						numDomainID,
						numCreatedBy,
						dtmCreatedOn,
						numOppId,
						numProjectId,
						numParentTaskId,
						bitDefaultTask,
						bitSavedTask,
						numOrder,
						numReferenceTaskId,
						numWorkOrderId)
               SELECT(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numWorkOrderId = v_numRecordID LIMIT 1),
						vcTaskName,
						numHours,
						numMinutes,
						ST.numAssignTo,
						v_numDomainID,
						v_numUserCntID,
						LOCALTIMESTAMP,
						0,
						0,
						0,
						true,
						CASE WHEN SP.bitRunningDynamicMode = true THEN false ELSE true END,
						numOrder,
						numTaskId,
						v_numRecordID
               FROM
               StagePercentageDetailsTask AS ST
               LEFT JOIN
               StagePercentageDetails As SP
               ON
               ST.numStageDetailsId = SP.numStageDetailsId
               WHERE
               coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numProjectId,0) = 0 AND
               ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
                  StagePercentageDetails As ST
                  WHERE
                  ST.slp_id = v_numProcessID)
               ORDER BY ST.numOrder;
            end if;
         end if;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   end if;
   RETURN;
END; $$;


