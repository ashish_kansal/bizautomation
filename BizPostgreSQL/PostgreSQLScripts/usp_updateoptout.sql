CREATE OR REPLACE FUNCTION USP_UpdateOptOut(v_numContactID NUMERIC,
    v_vcEmailID VARCHAR(50),
    v_bitOptOut BOOLEAN,
    v_numBroadCastID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  AdditionalContactsInformation
   SET     bitOptOut = v_bitOptOut
   WHERE   numContactId = v_numContactID
   AND vcEmail = v_vcEmailID;
   IF v_bitOptOut = true then
           
      Update BroadCastDtls set bitUnsubscribe = true where numBroadcastID = v_numBroadCastID and numContactID = v_numContactID;
   end if;
   RETURN;
END; $$;
--Created by Anoop Jayaraj



