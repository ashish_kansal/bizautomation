-- Stored procedure definition script USP_ShippingReport_Save for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ShippingReport_Save(v_numDomainId NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
    ,v_numOppBizDocId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingReportID  NUMERIC(18,0);
   v_IsCOD  BOOLEAN;
   v_IsDryIce  BOOLEAN;
   v_IsHoldSaturday  BOOLEAN;
   v_IsHomeDelivery  BOOLEAN;
   v_IsInsideDelevery  BOOLEAN;
   v_IsInsidePickup  BOOLEAN;
   v_IsReturnShipment  BOOLEAN;
   v_IsSaturdayDelivery  BOOLEAN;
   v_IsSaturdayPickup  BOOLEAN;
   v_IsAdditionalHandling  BOOLEAN;
   v_IsLargePackage  BOOLEAN;
   v_numCODAmount  NUMERIC(18,2);
   v_vcCODType  VARCHAR(50);
   v_numTotalInsuredValue  NUMERIC(18,2);
   v_vcDeliveryConfirmation  VARCHAR(1000);
   v_vcDescription  TEXT;
   v_numTotalCustomsValue  NUMERIC(18,2);
   v_tintSignatureType  SMALLINT;
   v_numOrderShippingCompany  NUMERIC(18,0);
   v_numOrderShippingService  NUMERIC(18,0);
   v_numDivisionShippingCompany  NUMERIC(18,0);
   v_numDivisionShippingService  NUMERIC(18,0);

   v_vcFromName  VARCHAR(1000);
   v_vcFromCompany  VARCHAR(1000);
   v_vcFromPhone  VARCHAR(100);
   v_vcFromAddressLine1  VARCHAR(50);
   v_vcFromAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcFromCity  VARCHAR(50);
   v_vcFromState  VARCHAR(50);
   v_vcFromZip  VARCHAR(50);
   v_vcFromCountry  VARCHAR(50);
   v_bitFromResidential  BOOLEAN DEFAULT 0;

   v_vcToName  VARCHAR(1000);
   v_vcToCompany  VARCHAR(1000);
   v_vcToPhone  VARCHAR(100);
   v_vcToAddressLine1  VARCHAR(50);
   v_vcToAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcToCity  VARCHAR(50);
   v_vcToState  VARCHAR(50);
   v_vcToZip  VARCHAR(50);
   v_vcToCountry  VARCHAR(50);
   v_bitToResidential  BOOLEAN DEFAULT 0;

	-- GET FROM ADDRESS
   v_numShippingCompany  NUMERIC(18,0);
   v_numShippingService  NUMERIC(18,0);
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
   v_bitUseBizdocAmount  BOOLEAN;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_j  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numBoxID  NUMERIC(18,0);
   v_numTempQty  DOUBLE PRECISION;
   v_numTempOppBizDocItemID  NUMERIC(18,0);
   v_numTempContainerQty  NUMERIC(18,0);
   v_numtempContainer  NUMERIC(18,0);
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempWarehouseID  NUMERIC(18,0);
   v_fltTempItemWidth  DOUBLE PRECISION;
   v_fltTempItemHeight  DOUBLE PRECISION;
   v_fltTempItemLength  DOUBLE PRECISION;
   v_fltTempItemWeight  DOUBLE PRECISION;
   v_numID  INTEGER;
   v_numTempTotalContainer  NUMERIC(18,0);
   v_numTempMainContainerQty  NUMERIC(18,0);
   v_bitTempItemAdded  BOOLEAN;
   v_fltTempWeight  DOUBLE PRECISION;
   v_fltTempHeight  DOUBLE PRECISION;
   v_fltTempWidth  DOUBLE PRECISION;
   v_fltTempLength  DOUBLE PRECISION;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   IsCOD, IsHomeDelivery, IsInsideDelevery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, vcCODType, vcDeliveryConfirmation, vcDescription, CAST(coalesce(vcSignatureType,'0') AS SMALLINT), coalesce(intUsedShippingCompany,0), coalesce(numShippingService,0), coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0) INTO v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsSaturdayDelivery,
      v_IsSaturdayPickup,v_IsAdditionalHandling,v_IsLargePackage,
      v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,v_tintSignatureType,
      v_numOrderShippingCompany,v_numOrderShippingService,v_numDivisionShippingCompany,
      v_numDivisionShippingService FROM
      OpportunityMaster
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      LEFT JOIN
      DivisionMasterShippingConfiguration
      ON
      DivisionMasterShippingConfiguration.numDomainID = v_numDomainId
      AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID WHERE
      OpportunityMaster.numDomainId = v_numDomainId
      AND DivisionMaster.numDomainID = v_numDomainId
      AND OpportunityMaster.numOppId = v_numOppID;
      select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, SUBSTR(CAST(vcState AS VARCHAR(18)),1,18), vcZipCode, SUBSTR(CAST(vcCountry AS VARCHAR(18)),1,18) INTO v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromCity,
      v_vcFromState,v_vcFromZip,v_vcFromCountry FROM
      fn_GetShippingReportAddress(1,v_numDomainId,v_numOppID);

	-- GET TO ADDRESS
      select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, SUBSTR(CAST(vcState AS VARCHAR(18)),1,18), vcZipCode, SUBSTR(CAST(vcCountry AS VARCHAR(18)),1,18) INTO v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,v_vcToCity,v_vcToState,
      v_vcToZip,v_vcToCountry FROM
      fn_GetShippingReportAddress(2,v_numDomainId,v_numOppID);
      IF coalesce(v_numOrderShippingCompany,0) > 0 then
	
         IF coalesce(v_numOrderShippingService,0) = 0 then
		
            v_numOrderShippingService := CASE v_numOrderShippingCompany
            WHEN 91 THEN 15 --FEDEX
            WHEN 88 THEN 43 --UPS
            WHEN 90 THEN 70 --USPS
            END;
         end if;
         v_numShippingCompany := v_numOrderShippingCompany;
         v_numShippingService := v_numOrderShippingService;
      ELSEIF coalesce(v_numDivisionShippingCompany,0) > 0
      then -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	
         IF coalesce(v_numDivisionShippingService,0) = 0 then
		
            v_numDivisionShippingService := CASE v_numDivisionShippingCompany
            WHEN 91 THEN 15 --FEDEX
            WHEN 88 THEN 43 --UPS
            WHEN 90 THEN 70 --USPS
            END;
         end if;
         v_numShippingCompany := v_numDivisionShippingCompany;
         v_numShippingService := v_numDivisionShippingService;
      ELSE
         select   coalesce(numShipCompany,0) INTO v_numShippingCompany FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_numShippingCompany <> 91 OR v_numShippingCompany <> 88 OR v_numShippingCompany <> 90 then
		
            v_numShippingCompany := 91;
         end if;
         v_numShippingService := CASE v_numShippingCompany
         WHEN 91 THEN 15 --FEDEX
         WHEN 88 THEN 43 --UPS
         WHEN 90 THEN 70 --USPS
         END;
      end if;
      select   bitUseBizdocAmount, coalesce(numTotalInsuredValue,0), coalesce(numTotalCustomsValue,0) INTO v_bitUseBizdocAmount,v_numTotalCustomsValue,v_numTotalInsuredValue FROM Domain WHERE numDomainId = v_numDomainId;
      IF v_bitUseBizdocAmount = true then
	
         select   coalesce(monDealAmount,0), coalesce(monDealAmount,0), coalesce(monDealAmount,0) INTO v_numTotalCustomsValue,v_numTotalInsuredValue,v_numCODAmount FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocId;
      ELSE
         select   coalesce(monDealAmount,0) INTO v_numCODAmount FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocId;
      end if;

	-- MAKE ENTRY IN ShippingReport TABLE
      INSERT INTO ShippingReport(numDomainID,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType)
	VALUES(v_numDomainId,v_numUserCntID,TIMEZONE('UTC',now()),v_numOppID,v_numOppBizDocId,v_numShippingCompany,CAST(v_numShippingService AS VARCHAR(30))
		,v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromAddressLine2,v_vcFromCity,v_vcFromState,v_vcFromZip,v_vcFromCountry,v_bitFromResidential
		,v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,v_vcToAddressLine2,v_vcToCity,v_vcToState,v_vcToZip,v_vcToCountry,v_bitToResidential
		,v_IsCOD,v_IsDryIce,v_IsHoldSaturday,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsReturnShipment,v_IsSaturdayDelivery,v_IsSaturdayPickup
		,v_numCODAmount,v_vcCODType,v_numTotalInsuredValue,v_IsAdditionalHandling,v_IsLargePackage,v_vcDeliveryConfirmation,v_vcDescription,v_numTotalCustomsValue,v_tintSignatureType);
	
      v_numShippingReportID := CURRVAL('ShippingReport_seq');

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPContainer_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPCONTAINER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCONTAINER
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numContainer NUMERIC(18,0),
         numWareHouseID NUMERIC(18,0),
         numContainerQty NUMERIC(18,0),
         numNoItemIntoContainer NUMERIC(18,0),
         numTotalContainer NUMERIC(18,0),
         numUsedContainer NUMERIC(18,0),
         bitItemAdded BOOLEAN,
         fltWeight DOUBLE PRECISION,
         fltHeight DOUBLE PRECISION,
         fltWidth DOUBLE PRECISION,
         fltLength DOUBLE PRECISION
      );
      INSERT INTO
      tt_TEMPCONTAINER(numContainer, numWareHouseID, numContainerQty, numNoItemIntoContainer, numTotalContainer, numUsedContainer, bitItemAdded, fltWeight, fltHeight, fltWidth, fltLength)
      SELECT
      T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T1.numTotalContainer
		,0 AS numTotalContainer
		,false AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
      FROM(SELECT
         I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
			,CEIL(SUM(OBI.numUnitHour)/CAST(I.numNoItemIntoContainer AS DOUBLE PRECISION)) AS numTotalContainer
			,0 AS numUsedContainer
         FROM
         OpportunityBizDocItems OBI
         INNER JOIN
         Item AS I
         ON
         OBI.numItemCode = I.numItemCode
         INNER JOIN
         WareHouseItems WI
         ON
         OBI.numWarehouseItmsID = WI.numWareHouseItemID
         WHERE
         numOppBizDocID = v_numOppBizDocId
         AND I.charItemType = 'P'
         AND coalesce(numContainer,0) > 0
         GROUP BY
         I.numContainer,WI.numWareHouseID,I.numNoItemIntoContainer) AS T1
      INNER JOIN(SELECT
         I.numItemCode
			,WI.numWareHouseID
			,coalesce(I.fltWeight,0) AS fltWeight
			,coalesce(I.fltHeight,0) AS fltHeight
			,coalesce(I.fltWidth,0) AS fltWidth
			,coalesce(I.fltLength,0) AS fltLength
         FROM
         OpportunityBizDocItems OBI
         INNER JOIN
         Item AS I
         ON
         OBI.numItemCode = I.numItemCode
         INNER JOIN
         WareHouseItems WI
         ON
         OBI.numWarehouseItmsID = WI.numWareHouseItemID
         WHERE
         numOppBizDocID = v_numOppBizDocId
         AND I.charItemType = 'P'
         AND coalesce(I.bitContainer,false) = true
         GROUP BY
         I.numItemCode,WI.numWareHouseID,coalesce(I.fltWeight,0),coalesce(I.fltHeight,0),
         coalesce(I.fltWidth,0),coalesce(I.fltLength,0)) AS T2
      ON
      T1.numContainer = T2.numItemCode
      AND T1.numWareHouseID = T2.numWareHouseID;
      IF(SELECT COUNT(*) FROM tt_TEMPCONTAINER) = 0 then
	
         RAISE EXCEPTION 'Container(s) are not available in bizdoc.';
      ELSE
         BEGIN
            CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEMS
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numOppBizDocItemID NUMERIC(18,0),
            numItemCode NUMERIC(18,0),
            numWarehouseID NUMERIC(18,0),
            numUnitHour DOUBLE PRECISION,
            numContainer NUMERIC(18,0),
            numNoItemIntoContainer NUMERIC(18,0),
            fltItemWidth DOUBLE PRECISION,
            fltItemHeight DOUBLE PRECISION,
            fltItemLength DOUBLE PRECISION,
            fltItemWeight DOUBLE PRECISION
         );
         INSERT INTO
         tt_TEMPITEMS(numOppBizDocItemID, numItemCode, numWarehouseID, numUnitHour, numContainer, numNoItemIntoContainer, fltItemWidth, fltItemHeight, fltItemLength, fltItemWeight)
         SELECT
         OBDI.numOppBizDocItemID
			,I.numItemCode
			,coalesce(WI.numWareHouseID,0)
			,coalesce(OBDI.numUnitHour,0)
			,coalesce(I.numContainer,0)
			,coalesce(I.numNoItemIntoContainer,0)
			,coalesce(fltWidth,0)
			,coalesce(fltHeight,0)
			,coalesce(fltLength,0)
			,(CASE
         WHEN coalesce(bitKitParent,false) = true
         AND(CASE
         WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
         THEN 1
         ELSE 0
         END) = 0
         THEN
            GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
         ELSE
            coalesce(fltWeight,0)
         END)
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         INNER JOIN
         OpportunityItems OI
         ON
         OBDI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         WareHouseItems WI
         ON
         OBDI.numWarehouseItmsID = WI.numWareHouseItemID
         INNER JOIN
         Item I
         ON
         OBDI.numItemCode = I.numItemCode
         WHERE
         OBD.numoppid = v_numOppID
         AND numOppBizDocsId = v_numOppBizDocId
         AND coalesce(I.numContainer,0) > 0;
         select   COUNT(*) INTO v_iCount FROM tt_TEMPITEMS;
         IF coalesce(v_iCount,0) > 0 then
		
            WHILE v_i <= v_iCount LOOP
               select   numOppBizDocItemID, numContainer, numItemCode, numWarehouseID, numUnitHour, numNoItemIntoContainer, fltItemWidth, fltItemHeight, fltItemLength, fltItemWeight INTO v_numTempOppBizDocItemID,v_numtempContainer,v_numTempItemCode,v_numTempWarehouseID,
               v_numTempQty,v_numTempContainerQty,v_fltTempItemWidth,v_fltTempItemHeight,
               v_fltTempItemLength,v_fltTempItemWeight FROM
               tt_TEMPITEMS WHERE
               ID = v_i;
               IF EXISTS(SELECT * FROM tt_TEMPCONTAINER WHERE numContainer = v_numtempContainer AND numWareHouseID = v_numTempWarehouseID AND numContainerQty = v_numTempContainerQty AND numUsedContainer < numTotalContainer) then
                  select   ID, numTotalContainer, numNoItemIntoContainer, bitItemAdded, fltWeight, fltHeight, fltWidth, fltLength INTO v_numID,v_numTempTotalContainer,v_numTempMainContainerQty,v_bitTempItemAdded,
                  v_fltTempWeight,v_fltTempHeight,v_fltTempWidth,v_fltTempLength FROM
                  tt_TEMPCONTAINER WHERE
                  numContainer = v_numtempContainer
                  AND numWareHouseID = v_numTempWarehouseID
                  AND numContainerQty = v_numTempContainerQty;
                  WHILE v_numTempQty > 0 LOOP
                     If v_numTempTotalContainer > 0 then
						 
                        IF v_numTempQty <= v_numTempMainContainerQty then
							
                           IF coalesce(v_bitTempItemAdded,false) = false OR v_numTempMainContainerQty = v_numTempContainerQty then
								
									-- CREATE NEW SHIPING BOX
                              INSERT INTO ShippingBox(vcBoxName
										,numShippingReportID
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany)
									VALUES(CONCAT('Box',v_j)
										,v_numShippingReportID
										,(CASE WHEN coalesce(v_fltTempWeight, 0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(v_fltTempHeight, 0) = 0 THEN 1 ELSE v_fltTempHeight END)*(CASE WHEN coalesce(v_fltTempWidth, 0) = 0 THEN 1 ELSE v_fltTempWidth END)*(CASE WHEN coalesce(v_fltTempLength, 0) = 0 THEN 1 ELSE v_fltTempLength END) AS DOUBLE PRECISION)/166) ELSE v_fltTempWeight END)
										,v_fltTempHeight
										,v_fltTempWidth
										,v_fltTempLength
										,TIMEZONE('UTC',now())
										,v_numUserCntID
										,(CASE
                              WHEN v_numShippingCompany = 88 THEN 19
                              WHEN v_numShippingCompany = 90 THEN(CASE WHEN coalesce((SELECT  vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = v_numDomainId AND intShipFieldID = 21 AND numListItemID = 90 LIMIT 1), 0) = '1' THEN 69 ELSE 50 END)
                              WHEN v_numShippingCompany = 91 THEN 7
                              END)
										,v_numShippingService
										,(CASE WHEN coalesce(v_fltTempWeight, 0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(v_fltTempHeight, 0) = 0 THEN 1 ELSE v_fltTempHeight END)*(CASE WHEN coalesce(v_fltTempWidth, 0) = 0 THEN 1 ELSE v_fltTempWidth END)*(CASE WHEN coalesce(v_fltTempLength, 0) = 0 THEN 1 ELSE v_fltTempLength END) AS DOUBLE PRECISION)/166) ELSE v_fltTempWeight END)
										,v_numShippingCompany);
									
                              v_numBoxID := CURRVAL('ShippingBox_seq');
                              UPDATE tt_TEMPCONTAINER SET bitItemAdded = true WHERE ID = v_numID;
                              v_j := v_j::bigint+1;
                           ELSE
                              select   numBoxID INTO v_numBoxID FROM ShippingBox WHERE numShippingReportID = v_numShippingReportID AND vcBoxName = CONCAT('Box',v_j);
                           end if;
                           v_fltTempItemWeight :=(CASE WHEN coalesce(v_fltTempItemWeight,0) > 0 THEN v_fltTempItemWeight ELSE CEIL(CAST(v_fltTempItemHeight*v_fltTempItemWidth*v_fltTempItemLength AS DOUBLE PRECISION)/166) END);
                           UPDATE ShippingBox SET fltTotalWeight = coalesce(fltTotalWeight,0)+(v_fltTempItemWeight*v_numTempQty) WHERE numBoxID = v_numBoxID;

								-- MAKE ENTRY IN ShippingReportItems Table
                           INSERT INTO ShippingReportItems(numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty)
								VALUES(v_numShippingReportID
									,v_numTempItemCode
									,v_numShippingService
									,0
									,v_fltTempItemWeight
									,1
									,v_fltTempHeight
									,v_fltTempItemWidth
									,v_fltTempItemLength
									,TIMEZONE('UTC',now())
									,v_numUserCntID
									,v_numBoxID
									,v_numTempOppBizDocItemID
									,v_numTempQty);
								
                           UPDATE tt_TEMPCONTAINER SET numNoItemIntoContainer = numNoItemIntoContainer -v_numTempQty WHERE ID = v_numID;
                           v_numTempMainContainerQty := v_numTempMainContainerQty -v_numTempQty;
                           IF v_numTempMainContainerQty = 0 then
								
									-- IF NO QTY LEFT TO ADD IN CONTAINER RESET CONAINER QTY AND MAKE FLAG bitItemAdded=0 SO IF numUsedContainer < numTotalContainer THEN NEW SHIPPING BOX WILL BE CREATED
                              UPDATE tt_TEMPCONTAINER SET numNoItemIntoContainer = numContainerQty,bitItemAdded = false,numUsedContainer = numUsedContainer+1 WHERE ID = v_numID;
                           end if;
                           v_numTempQty := 0;
                        ELSE
								-- CREATE NEW SHIPING BOX
                           INSERT INTO ShippingBox(vcBoxName
									,numShippingReportID
									,fltTotalWeight
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numPackageTypeID
									,numServiceTypeID
									,fltDimensionalWeight
									,numShipCompany)
								VALUES(CONCAT('Box',v_j)
									,v_numShippingReportID
									,(CASE WHEN coalesce(v_fltTempWeight, 0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(v_fltTempHeight, 0) = 0 THEN 1 ELSE v_fltTempHeight END)*(CASE WHEN coalesce(v_fltTempWidth, 0) = 0 THEN 1 ELSE v_fltTempWidth END)*(CASE WHEN coalesce(v_fltTempLength, 0) = 0 THEN 1 ELSE v_fltTempLength END) AS DOUBLE PRECISION)/166) ELSE v_fltTempWeight END)
									,v_fltTempHeight
									,v_fltTempWidth
									,v_fltTempLength
									,TIMEZONE('UTC',now())
									,v_numUserCntID
									,(CASE
                           WHEN v_numShippingCompany = 88 THEN 19
                           WHEN v_numShippingCompany = 90 THEN(CASE WHEN coalesce((SELECT  vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = v_numDomainId AND intShipFieldID = 21 AND numListItemID = 90 LIMIT 1), 0) = '1' THEN 69 ELSE 50 END)
                           WHEN v_numShippingCompany = 91 THEN 7
                           END)
									,v_numShippingService
									,(CASE WHEN coalesce(v_fltTempWeight, 0) = 0 THEN CEIL(CAST((CASE WHEN coalesce(v_fltTempHeight, 0) = 0 THEN 1 ELSE v_fltTempHeight END)*(CASE WHEN coalesce(v_fltTempWidth, 0) = 0 THEN 1 ELSE v_fltTempWidth END)*(CASE WHEN coalesce(v_fltTempLength, 0) = 0 THEN 1 ELSE v_fltTempLength END) AS DOUBLE PRECISION)/166) ELSE v_fltTempWeight END)
									,v_numShippingCompany);
								
                           v_numBoxID := CURRVAL('ShippingBox_seq');
                           UPDATE tt_TEMPCONTAINER SET numNoItemIntoContainer = numContainerQty,bitItemAdded = false,numUsedContainer = numUsedContainer+1 WHERE ID = v_numID;
                           v_j := v_j::bigint+1;

								--SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

                           v_fltTempItemWeight :=(CASE WHEN coalesce(v_fltTempItemWeight,0) > CEIL(CAST(v_fltTempItemHeight*v_fltTempItemWidth*v_fltTempItemLength AS DOUBLE PRECISION)/139) THEN v_fltTempItemWeight ELSE CEIL(CAST(v_fltTempItemHeight*v_fltTempItemWidth*v_fltTempItemLength AS DOUBLE PRECISION)/139) END);
                           UPDATE ShippingBox SET fltTotalWeight = coalesce(fltTotalWeight,0)+(v_fltTempItemWeight*v_numTempMainContainerQty) WHERE numBoxID = v_numBoxID;

								-- MAKE ENTRY IN ShippingReportItems Table
                           INSERT INTO ShippingReportItems(numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty)
								VALUES(v_numShippingReportID
									,v_numTempItemCode
									,v_numShippingService
									,0
									,v_fltTempItemWeight
									,1
									,v_fltTempHeight
									,v_fltTempItemWidth
									,v_fltTempItemLength
									,TIMEZONE('UTC',now())
									,v_numUserCntID
									,v_numBoxID
									,v_numTempOppBizDocItemID
									,v_numTempMainContainerQty);
								
                           UPDATE tt_TEMPCONTAINER SET numNoItemIntoContainer = v_numTempContainerQty,numTotalContainer = numTotalContainer -1 WHERE ID = v_numID;
                           v_numTempQty := v_numTempQty -v_numTempMainContainerQty;
                        end if;
                     ELSE
                        RAISE EXCEPTION 'Sufficient container(s) are not available.';
                        EXIT;
                     end if;
                  END LOOP;
               ELSE
                  RAISE EXCEPTION 'Sufficient container(s) require for item and warehouse are not available.';
                  EXIT;
               end if;
               v_i := v_i::bigint+1;
            END LOOP;
         ELSE
            RAISE EXCEPTION 'Inventory item(s) are not available in bizdoc.';
         end if;
      end if;
      open SWV_RefCur for SELECT v_numShippingReportID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;











