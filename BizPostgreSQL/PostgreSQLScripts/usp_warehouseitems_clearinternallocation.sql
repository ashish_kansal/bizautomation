-- Stored procedure definition script USP_WarehouseItems_ClearInternalLocation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WarehouseItems_ClearInternalLocation(v_numDomainID NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   WareHouseItems
   SET
   numWLocationID = 0
   WHERE
   numDomainID = v_numDomainID
   AND numWareHouseItemID = v_numWarehouseItemID;
   RETURN;
END; $$;


