-- Stored procedure definition script USP_GetShippingExceptionsForOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingExceptionsForOrder(v_numDomainID NUMERIC(18,0)
	,v_vcItemCodes TEXT
	,v_ShippingAmt DOUBLE PRECISION
	,v_bitFreeShipping BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

	--DECLARE @vcItemCodes VARCHAR(MAX) = '8,240'
  
   AS $$
   DECLARE
   v_totalRecords  INTEGER;
   v_I  INTEGER;
   v_numShipClass  NUMERIC;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempExceptions_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPEXCEPTIONS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEXCEPTIONS
   (
      SNO INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcItemName VARCHAR(200),
      bitApplyException BOOLEAN,
      FlatAmt DOUBLE PRECISION,
      numItemCode NUMERIC
   );
   INSERT INTO tt_TEMPEXCEPTIONS(vcItemName, bitApplyException, FlatAmt, numItemCode)
   SELECT CAST('' AS VARCHAR(200)),CAST(0 AS BOOLEAN),0, id
   FROM  SplitIDs(v_vcItemCodes,',');  
	
--select * from 	@tempExceptions	

   v_I := 1;
   select   COUNT(numItemCode) INTO v_totalRecords FROM tt_TEMPEXCEPTIONS;
   WHILE (v_I <= v_totalRecords) LOOP
      SELECT I.numShipClass INTO v_numShipClass FROM tt_TEMPEXCEPTIONS t
      INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = v_I;
      UPDATE tt_TEMPEXCEPTIONS
      SET vcItemName =(SELECT I.vcItemName FROM tt_TEMPEXCEPTIONS t
      INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = v_I),bitApplyException =(SELECT CASE WHEN(SELECT COUNT(numClassificationID) FROM ShippingExceptions WHERE numDomainID = v_numDomainID AND numClassificationID = v_numShipClass) > 0
      THEN true
      ELSE false END),
      FlatAmt =(SELECT
      CASE WHEN v_ShippingAmt > 0 THEN(SELECT(v_ShippingAmt*PercentAbove)/100 FROM ShippingExceptions WHERE numDomainID = v_numDomainID)
      WHEN v_ShippingAmt = 0 AND v_bitFreeShipping = true THEN(SELECT FlatAmt FROM ShippingExceptions WHERE numDomainID = v_numDomainID)
      ELSE 0 END)
      WHERE SNO = v_I;
      v_I := v_I::bigint+1;
   END LOOP;						
				
   open SWV_RefCur for SELECT vcItemName, bitApplyException, FlatAmt, numItemCode FROM tt_TEMPEXCEPTIONS WHERE bitApplyException = true;
END; $$; 












