-- Stored procedure definition script USP_ParentChildCustomFieldMap_GetDynamicFields for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ParentChildCustomFieldMap_GetDynamicFields(v_tintModule SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      fld_id NUMERIC(18,0),
      Fld_label VARCHAR(300)
   );
   If v_tintModule = 2 OR v_tintModule = 6 then
	
      INSERT INTO tt_TEMP(fld_id
			,Fld_label)
		VALUES(100,'Assigned To'),(122,'Comments'),
      (771,'Ship Via');
   end if;

   open SWV_RefCur for SELECT * FROM tt_TEMP;
END; $$;












