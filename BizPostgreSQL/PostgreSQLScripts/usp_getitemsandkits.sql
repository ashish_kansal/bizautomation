-- Stored procedure definition script USP_GetItemsAndKits for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemsAndKits(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_strItem VARCHAR(100) DEFAULT NULL,
	v_type INTEGER DEFAULT NULL,
	v_bitAssembly BOOLEAN DEFAULT false,
	v_numItemCode NUMERIC(18,0) DEFAULT 0,
	v_numPageIndex INTEGER DEFAULT NULL,
	v_numPageSize INTEGER DEFAULT NULL,     
	INOUT v_TotalCount INT DEFAULT 0,
	p_SearchType VARCHAR(10) DEFAULT '1',
	INOUT SWV_RefCur refcursor DEFAULT null, 
	INOUT SWV_RefCur2 refcursor DEFAULT null
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_strSQL  TEXT DEFAULT '';
   v_strSQL2  TEXT DEFAULT '';
   v_vcWareHouseSearch  VARCHAR(1000) DEFAULT '';
   v_vcVendorSearch  VARCHAR(1000) DEFAULT '';
   TotalCount INTEGER  DEFAULT 0;
   v_tintDecimalPoints  SMALLINT;

   v_tintOrder  INTEGER;
   v_Fld_id  VARCHAR(20);
   v_Fld_Name  VARCHAR(20);
        
   v_strSearch  TEXT;
   v_CustomSearch  TEXT;
   v_numFieldId  NUMERIC(18,0);
        
   v_strSQLCount  TEXT DEFAULT '';
   SWV_RowCount INTEGER;

BEGIN
   
   IF v_type = 0 THEN
		v_TotalCount := 0;

		OPEN SWV_RefCur FOR
		SELECT 
			numItemCode AS "numItemCode"
			,vcItemName || '' || CASE WHEN txtItemDesc IS NULL OR txtItemDesc = '' THEN '' ELSE ' - ' || coalesce(txtItemDesc,'') END AS "vcItemName"
      FROM Item     
	  WHERE numDomainID = v_numDomainID
      AND vcItemName ILIKE (CASE WHEN p_SearchType = '2'  THEN COALESCE(v_strItem,'') || '%' WHEN p_SearchType = '3' THEN '%' || COALESCE(v_strItem,'') ELSE '%' || COALESCE(v_strItem,'') || '%' END);
	  
   ELSEIF v_type = 1 THEN
      DROP TABLE IF EXISTS tt_TEMPITEMCODE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMCODE
      (
         numItemCode NUMERIC(18,0)
      );
      
	  SELECT COALESCE(tintDecimalPoints,4) INTO v_tintDecimalPoints FROM
      Domain WHERE
      numDomainId = v_numDomainID;

	DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
    CREATE TEMPORARY TABLE tt_TEMP1 AS
    SELECT * FROM(SELECT numFieldID ,
				vcDbColumnName ,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
				tintRow AS tintOrder ,
				0 AS Custom
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 22
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND bitCustom = false
         AND coalesce(bitSettingField,false) = true
         AND numRelCntType = 0
         UNION
         SELECT numFieldId ,
				vcFieldName ,
				vcFieldName ,
				tintRow AS tintOrder ,
				1 AS Custom
         FROM
         View_DynamicCustomColumns
         WHERE
         Grp_id = 5
         AND numFormId = 22
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND bitCustom = true
         AND numRelCntType = 0) X;
      
	  IF NOT EXISTS(SELECT * FROM tt_TEMP1) then
         INSERT INTO tt_TEMP1
         SELECT numFieldID
				,vcDbColumnName
				,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,tintOrder
				,0
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 22
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID;
      END IF;
      
	  select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name 
	  FROM tt_TEMP1 
	  WHERE Custom = 1   ORDER BY
      tintOrder LIMIT 1;
	  
      WHILE v_tintOrder > 0 LOOP
         v_strSQL := coalesce(v_strSQL,'') || ', GetCustFldValueItem(' || coalesce(v_Fld_id,'') || ', I.numItemCode) as "' || coalesce(v_Fld_Name,'') || '"';
         select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name 
		 FROM tt_TEMP1 
		 WHERE Custom = 1
         AND tintOrder >= v_tintOrder   ORDER BY
         tintOrder 
		 LIMIT 1;
         
		 GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         
		 IF SWV_RowCount = 0 then
            v_tintOrder := 0;
         end if;
      END LOOP;

		--Temp table for Item Search Configuration

      DROP TABLE IF EXISTS tt_TEMPSEARCH CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSEARCH AS
         SELECT * FROM(SELECT
         numFieldID,
                vcDbColumnName ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                tintRow AS tintOrder ,
                0 AS Custom
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 22
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND bitCustom = false
         AND coalesce(bitSettingField,false) = true
         AND numRelCntType = 1
         UNION
         SELECT
         numFieldId,
                vcFieldName ,
                vcFieldName ,
                tintRow AS tintOrder ,
                1 AS Custom
         FROM
         View_DynamicCustomColumns
         WHERE
         Grp_id = 5
         AND numFormId = 22
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND bitCustom = true
         AND numRelCntType = 1) X;
		 
      IF NOT EXISTS(SELECT * FROM tt_TEMPSEARCH) then
         INSERT INTO
         tt_TEMPSEARCH
         SELECT
         numFieldID ,
                vcDbColumnName ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                tintOrder ,
                0
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 22
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID;
      end if;
      
	  v_strSearch := '';
      
	  select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId FROM
      tt_TEMPSEARCH WHERE
      Custom = 0   ORDER BY
      tintOrder LIMIT 1;
      
	  WHILE v_tintOrder > -1 LOOP
         IF v_Fld_Name = 'vcPartNo' then
			
            INSERT INTO tt_TEMPITEMCODE
            SELECT DISTINCT
            Item.numItemCode
            FROM    Vendor
            JOIN Item ON Item.numItemCode = Vendor.numItemCode
            WHERE   Item.numDomainID = v_numDomainID
            AND Vendor.numDomainID = v_numDomainID
            AND Vendor.vcPartNo IS NOT NULL
            AND LENGTH(Vendor.vcPartNo) > 0
            AND vcPartNo ilike (CASE WHEN p_SearchType = '2'  THEN COALESCE(v_strItem,'') || '%' WHEN p_SearchType = '3' THEN '%' || COALESCE(v_strItem,'') ELSE '%' || COALESCE(v_strItem,'') || '%' END);
            v_vcVendorSearch := 'Vendor.vcPartNo ILIKE ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END);
         ELSEIF v_Fld_Name = 'vcBarCode'
         then
			
            INSERT INTO tt_TEMPITEMCODE
            SELECT DISTINCT
            Item.numItemCode
            FROM    Item
            JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
            AND Item.numItemCode = WI.numItemID
            JOIN Warehouses W ON W.numDomainID = v_numDomainID
            AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = v_numDomainID
            AND coalesce(Item.numItemGroup,0) > 0
            AND WI.vcBarCode IS NOT NULL
            AND LENGTH(WI.vcBarCode) > 0
            AND WI.vcBarCode ilike (CASE WHEN p_SearchType = '2'  THEN COALESCE(v_strItem,'') || '%' WHEN p_SearchType = '3' THEN '%' || COALESCE(v_strItem,'') ELSE '%' || COALESCE(v_strItem,'') || '%' END);
            v_vcWareHouseSearch := 'WareHouseItems.vcBarCode ILIKE ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END);
         ELSEIF v_Fld_Name = 'vcWHSKU'
         then
			
            INSERT INTO tt_TEMPITEMCODE
            SELECT DISTINCT
            Item.numItemCode
            FROM    Item
            JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
            AND Item.numItemCode = WI.numItemID
            JOIN Warehouses W ON W.numDomainID = v_numDomainID
            AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = v_numDomainID
            AND coalesce(Item.numItemGroup,0) > 0
            AND WI.vcWHSKU IS NOT NULL
            AND LENGTH(WI.vcWHSKU) > 0
            AND WI.vcWHSKU ilike (CASE WHEN p_SearchType = '2'  THEN COALESCE(v_strItem,'') || '%' WHEN p_SearchType = '3' THEN '%' || COALESCE(v_strItem,'') ELSE '%' || COALESCE(v_strItem,'') || '%' END);
            IF LENGTH(v_vcWareHouseSearch) > 0 then
               v_vcWareHouseSearch := coalesce(v_vcWareHouseSearch,'') || ' OR WareHouseItems.vcWHSKU ILIKE ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END);
            ELSE
               v_vcWareHouseSearch := 'WareHouseItems.vcWHSKU ILIKE ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END);
            end if;
         ELSE
            v_strSearch := coalesce(v_strSearch,'') ||(CASE v_Fld_Name WHEN 'numItemCode' THEN ' I.' || coalesce(v_Fld_Name,'') ELSE ' ' || coalesce(v_Fld_Name,'') END)  || '::VARCHAR ILIKE ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END);
         end if;
		 
         select tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId 
		 FROM tt_TEMPSEARCH 
		 WHERE tintOrder >= v_tintOrder
         AND Custom = 0   
		 ORDER BY tintOrder 
		 LIMIT 1;
         
		 GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         
		 IF SWV_RowCount = 0 then   
            v_tintOrder := -1;
         ELSE
            IF v_Fld_Name NOT IN ('vcPartNo' ,'vcBarCode', 'vcWHSKU') then       
               v_strSearch := coalesce(v_strSearch,'') || ' or ';
            end if;
         end if;
		 
      END LOOP;
	  
      IF(SELECT COUNT(*) FROM tt_TEMPITEMCODE) > 0 then
         v_strSearch := coalesce(v_strSearch,'') || ' OR  I.numItemCode = ' || v_numItemCode;
      end if;

		--Custom Search
      v_CustomSearch := '';
      select tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId 
	  FROM tt_TEMPSEARCH 
	  WHERE Custom = 1   
	  ORDER BY tintOrder 
	  LIMIT 1;
	  
      WHILE v_tintOrder > -1 LOOP
         
		 v_CustomSearch := coalesce(v_CustomSearch,'') || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
         
		 select tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId 
		 FROM tt_TEMPSEARCH 
		 WHERE tintOrder >= v_tintOrder
         AND Custom = 1   
		 ORDER BY tintOrder LIMIT 1;
         
		 GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         
		 IF SWV_RowCount = 0 then   
            v_tintOrder := -1;
         ELSE
            v_CustomSearch := coalesce(v_CustomSearch,'') || ' , ';
         end if;
      
	  END LOOP;
	  
      IF LENGTH(v_CustomSearch) > 0 then  
         v_CustomSearch := ' I.numItemCode in (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID IN ('
         || coalesce(v_CustomSearch,'') || ') and GetCustFldValueItem(Fld_ID,RecId) ilike ' || (CASE WHEN p_SearchType = '2'  THEN '''' || COALESCE(v_strItem,'') || '%''' WHEN p_SearchType = '3' THEN '''%' || COALESCE(v_strItem,'') || '''' ELSE '''%' || COALESCE(v_strItem,'') || '%''' END) || ')';
         IF LENGTH(v_strSearch) > 0 then
            v_strSearch := coalesce(v_strSearch,'') || ' OR ' || coalesce(v_CustomSearch,'');
         ELSE
            v_strSearch := v_CustomSearch;
         end if;
      end if;
	  
      IF(SELECT coalesce(bitAssembly,false) FROM Item  WHERE numItemCode = v_numItemCode) = true then
         v_strSearch := 'AND COALESCE(I.bitLotNo,false) <> true 
							AND COALESCE(I.bitSerialized,false) <> true 
							AND COALESCE(I.bitKitParent,false) <> true
							AND COALESCE(I.bitRental,false) <> true
							AND COALESCE(I.bitAsset,false) <> true
							AND I.numItemCode NOT IN (' || SUBSTR(CAST(COALESCE(v_numItemCode, 0) AS VARCHAR(30)),1,30) || ')' ||(CASE WHEN LENGTH(COALESCE(v_strSearch,'')) > 0 THEN ' AND (' || COALESCE(v_strSearch,'') || ')' ELSE '' END);
      ELSE
			--ONLY NON INVENTORY, SERVICE, INVENTORY ITEMS AND KIT ITEMS WITHOUT HAVING ANOTHER KIT AS SUB ITEM WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
         v_strSearch := 'AND COALESCE(I.bitLotNo,false) <> true 
							AND COALESCE(I.bitSerialized,false) <> true 
							AND 1 = (CASE 
										WHEN I.bitKitParent = true 
										THEN 
											(
												CASE 
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID = IInner.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(IInner.bitKitParent,false) = true) > 0
													THEN 0
													ELSE 1
												END
											)
										ELSE 1 
									END)
							AND I.numItemCode <> ' || SUBSTR(CAST(COALESCE(v_numItemCode, 0) AS VARCHAR(30)),1,30) ||(CASE WHEN LENGTH(COALESCE(v_strSearch,'')) > 0 THEN ' AND (' || COALESCE(v_strSearch,'') || ')' ELSE '' END);
      end if;
      
	  v_strSQLCount := 'SELECT COALESCE((SELECT 
											COUNT(I.numItemCode) 
										FROM 
											Item  I
										LEFT JOIN 
											DivisionMaster D              
										ON 
											I.numVendorID=D.numDivisionID  
										LEFT JOIN 
											CompanyInfo C  
										ON 
											C.numCompanyID=D.numCompanyID
										LEFT JOIN 
											CustomerPartNumber  CPN  
										ON 
											I.numItemCode=CPN.numItemCode 
											AND CPN.numCompanyID=C.numCompanyID
										LEFT JOIN
											WareHouseItems WHT
										ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' ||
      CASE LENGTH(v_vcWareHouseSearch)
      WHEN 0
      THEN
         'SELECT 
																					numWareHouseItemID 
																				FROM 
																					WareHouseItems 
																				WHERE 
																					WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																					AND WareHouseItems.numItemID = I.numItemCode 
																					LIMIT 1'
      ELSE
         'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								WareHouseItems 
																							WHERE 
																								WareHouseItems.numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || ' 
																								AND WareHouseItems.numItemID = I.numItemCode 
																								AND (' || COALESCE(v_vcWareHouseSearch,'') || ')) > 0 
																					THEN
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (' || coalesce(v_vcWareHouseSearch,'') || ')
																							LIMIT 1)
																					ELSE
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							LIMIT 1
																						)
																					END'
      END
      ||
      ') 
										WHERE ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
      || ') > 0)) AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID='
      || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(20)),1,20) || coalesce(v_strSearch,'');
      
	  v_strSQLCount := coalesce(v_strSQLCount,'') || '),0)';
		
	  v_strSQLCount := REPLACE(v_strSQLCount, 'I.vcCompanyName', 'C.vcCompanyName');
	  v_strSQLCount := REPLACE(v_strSQLCount, 'monListPrice::VARCHAR ILIKE', 'CAST(monListPrice AS VARCHAR) ILIKE');                

	  RAISE NOTICE '%', v_strSQLCount;

	  EXECUTE (v_strSQLCount) INTO TotalCount;

	  v_strSQL := '
	  DROP TABLE IF EXISTS tt_TEMPItems CASCADE;
	  
	  CREATE TEMPORARY TABLE tt_TEMPItems 
	  AS
	  SELECT I.numItemCode AS "numItemCode", COALESCE(CPN.CustomerPartNo,'''')  AS "CustomerPartNo",
									  (SELECT vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForImage",
									  (SELECT vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForTImage",
									  COALESCE(vcItemName,'''') "vcItemName",
									  CAST(CAST(CASE 
									  WHEN I.charItemType=''P''
									  THEN COALESCE((SELECT monWListPrice FROM WareHouseItems WHERE numItemID=I.numItemCode LIMIT 1),0) 
									  ELSE 
										monListPrice 
									  END AS DECIMAL(20,' || SUBSTR(CAST(COALESCE(v_tintDecimalPoints, 0) AS VARCHAR(30)),1,30) || ')) AS VARCHAR) AS "monListPrice",
									  COALESCE(vcSKU,'''') AS "vcSKU",
									  COALESCE(numBarCodeId,'''') AS "numBarCodeId",
									  COALESCE(vcModelID,'''') AS "vcModelID",
									  COALESCE(txtItemDesc,'''') as "txtItemDesc",
									  COALESCE(C.vcCompanyName,'''') as "vcCompanyName",
									  D.numDivisionID AS "numDivisionID",
									  WHT.numWareHouseItemID AS "numWareHouseItemID",
									  fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS "vcAttributes",
									  (CASE 
											WHEN COALESCE(I.bitKitParent,false) = true 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) AS "bitHasKitAsChild"
										,COALESCE(I.bitMatrix,false) AS "bitMatrix"
										,COALESCE(I.numItemGroup,0) AS "numItemGroup"
										,I.charItemType AS "charItemType"
										,COALESCE(I.bitKitParent,false) AS "bitKitParent"
										,COALESCE(I.bitAssembly,false) AS "bitAssembly" '
      || coalesce(v_strSQL,'')
      || ' 
	  FROM Item  I
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID
									  LEFT JOIN CustomerPartNumber  CPN  
									 ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=C.numCompanyID
									  LEFT JOIN
											WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' ||
      CASE LENGTH(v_vcWareHouseSearch)
      WHEN 0
      THEN
         'SELECT 
																					numWareHouseItemID 
																				FROM 
																					WareHouseItems 
																				WHERE 
																					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																					AND WareHouseItems.numItemID = I.numItemCode 
																					LIMIT 1'
      ELSE
         'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								WareHouseItems 
																							WHERE 
																								WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																								AND WareHouseItems.numItemID = I.numItemCode 
																								AND (' || coalesce(v_vcWareHouseSearch,'') || ')
																								LIMIT 1) > 0 
																					THEN
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (' || coalesce(v_vcWareHouseSearch,'') || ')
																							LIMIT 1)
																					ELSE
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode
																							LIMIT 1
																						)
																					END'
      END
      ||
      ') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ') > 0)) AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID='
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || COALESCE(v_strSearch,'');
      
	   IF LENGTH(v_strSQL) > 0 THEN
	   v_strSQL := COALESCE(v_strSQL,'') || CONCAT(' ORDER BY I.vcItemName OFFSET ',(v_numPageIndex::bigint -1)*v_numPageSize::bigint,
	   ' ROWS FETCH NEXT ',v_numPageSize,' ROWS ONLY; ');
	   END IF;  	   
	   
	   v_strSQL := REPLACE(v_strSQL, 'I.vcCompanyName', 'C.vcCompanyName');
	   v_strSQL := REPLACE(v_strSQL, 'monListPrice::VARCHAR ILIKE', 'CAST(monListPrice AS VARCHAR) ILIKE');                

	   RAISE NOTICE '%', v_strSQL;
	   EXECUTE (v_strSQL);

	   v_strSQL2 := '
	   DROP TABLE IF EXISTS tt_TEMPItemsFinal CASCADE; 
	  
	   CREATE TEMPORARY TABLE tt_TEMPItemsFinal 
	   AS
	   SELECT ROW_NUMBER() OVER( ORDER BY "vcItemName") AS "SRNO",(CASE WHEN ("vcSKU"=''' || coalesce(v_strItem,'') || ''' OR ' || CAST(coalesce(v_numDomainID,0) AS VARCHAR(30)) || '=209) THEN false ELSE true END) AS "bitSKUMatch"
	   , * 
	   FROM tt_TEMPItems;';
      							
	   EXECUTE (v_strSQL2);

	   RAISE NOTICE '%', CONCAT(v_strSQLCount, v_strSQL, v_strSQL2) || 
	   ' SELECT * FROM tt_TEMPItemsFinal ORDER BY "SRNO";'; --Print the query before executing it

	   v_TotalCount := TotalCount;

	   OPEN SWV_RefCur FOR 
	   SELECT * FROM tt_TEMPItemsFinal ORDER BY "SRNO"; 

	   OPEN SWV_RefCur2 FOR
	   SELECT numFieldID AS "numFieldID",
				vcDbColumnName AS "vcDbColumnName",
				vcFieldName AS "vcFieldName",
				tintOrder AS "tintOrder",
				Custom AS "Custom"
	   FROM	tt_TEMP1
	   WHERE vcDbColumnName NOT IN ('vcPartNo','vcBarCode','vcWHSKU') 
	   ORDER BY tintOrder; 

	
      
   END IF;
   RETURN;
END;
$$;


