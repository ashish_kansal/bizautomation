-- Stored procedure definition script USP_ManageStageGradeDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageStageGradeDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
	v_numStageDetailsGradeId NUMERIC(18,0) DEFAULT 0,
	v_numStageDetailsId NUMERIC(18,0) DEFAULT 0,                
	v_vcAssignedId VARCHAR(500) DEFAULT 0,           
	v_numCreatedBy NUMERIC(18,0) DEFAULT 0,
	v_numTaskId NUMERIC DEFAULT 0,
	v_tintMode INTEGER DEFAULT 0,
	v_vcGradeId VARCHAR(100) DEFAULT NULL,
	INOUT v_status VARCHAR(100)  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_tintMode = 1) then
	
      IF((SELECT COUNT(*) FROM tblStageGradeDetails WHERE numDomainID = v_numDomainID AND numStageDetailsId = v_numStageDetailsId AND numTaskID = v_numTaskId AND vcGradeId = v_vcGradeId AND numAssigneId IN(SELECT Id FROM
         SplitIds(v_vcAssignedId,','))) > 0) then
		
         v_status := '2';
      ELSE
         INSERT INTO  tblStageGradeDetails(numStageDetailsId,
				numAssigneId,
				numTaskID,
				vcGradeId,
				numDomainID,
				numCreatedBy,
				bintCreatedDate,
				numModifiedBy,
				bintModifiedDate)
         SELECT
         v_numStageDetailsId,
				Id,
				v_numTaskId,
				v_vcGradeId,
				v_numDomainID,
				v_numCreatedBy,
				LOCALTIMESTAMP,
				v_numCreatedBy,
				LOCALTIMESTAMP
         FROM
         SplitIds(v_vcAssignedId,',');
         v_status := '1';
      end if;
   end if;
   IF(v_tintMode = 2) then
	
      DELETE FROM
      tblStageGradeDetails
      WHERE
      numStageDetailsGradeId = v_numStageDetailsGradeId;
      v_status := '3';
   end if;
   IF(v_tintMode = 3) then
	
      open SWV_RefCur for
      SELECT
      coalesce(AC.vcFirstName || ' ' || AC.vcLastname,'-') As vcAssignedId,
			ST.vcTaskName,
			S.vcGradeId,
			S.numStageDetailsId,
			S.numStageDetailsGradeId
      FROM
      tblStageGradeDetails AS S
      LEFT JOIN
      AdditionalContactsInformation AS AC
      ON
      S.numAssigneId = AC.numContactId
      LEFT JOIN
      StagePercentageDetailsTask AS ST
      ON
      S.numTaskID = ST.numTaskId
      WHERE
      S.numDomainID = v_numDomainID AND S.numStageDetailsId = v_numStageDetailsId;
   end if;
   RETURN;
END; $$;



