-- Stored procedure definition script USP_GetTimeLineAddedBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeLineAddedBizDocs(v_numDivId NUMERIC,
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select distinct cast(TE.numOppBizDocsId as VARCHAR(255)),vcBizDocID,OBD.numoppid from timeandexpense TE
   join OpportunityBizDocs OBD on OBD.numOppBizDocsId = TE.numOppBizDocsId
   where TE.numDivisionID = v_numDivId and numDomainId =  v_numDomainId;
END; $$;












