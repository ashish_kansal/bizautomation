-- Stored procedure definition script USP_EmployeeList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EmployeeList(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT A.numContactId AS "numContactId",coalesce(A.vcFirstName,'') || ' ' || coalesce(A.vcLastname,'') as "vcUserName"
   from UserMaster UM
   join AdditionalContactsInformation A
   on UM.numUserDetailId = A.numContactId
   where UM.numDomainID = v_numDomainID;
END; $$;












