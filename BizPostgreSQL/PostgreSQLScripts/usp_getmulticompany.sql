-- Stored procedure definition script USP_GetMultiCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMultiCompany(v_numDomainID INTEGER,
v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_MULTICOMP CASCADE;
   create TEMPORARY TABLE tt_MULTICOMP
   (
      vcDomainCode VARCHAR(100), 
      vcDomainName VARCHAR(250),
      numDomainID NUMERIC
   );

   INSERT INTO tt_MULTICOMP
   select DN.vcDomainCode, DN.vcDomainName,DN.numDomainId from
   Domain DN
   where  			DN.numDomainId = v_numDomainID and
   DN.numSubscriberID = v_numSubscriberID
   union
   select DN.vcDomainCode, DN.vcDomainName,DN.numDomainId from
   Domain DN,
	Domain DNA
   where  DN.numSubscriberID = v_numSubscriberID and
   DN.vcDomainCode ilike DNA.vcDomainCode || '%' and
   DNA.numSubscriberID = v_numSubscriberID and DNA.numParentDomainID = v_numDomainID;

			

open SWV_RefCur for SELECT * FROM tt_MULTICOMP ORDER BY vcDomainCode;
END; $$;












