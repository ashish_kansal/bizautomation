CREATE OR REPLACE FUNCTION USP_GetTransactionHistory
(
	v_numTransHistoryID NUMERIC(18,0),
    v_numDomainID NUMERIC(18,0),
    v_numDivisionID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSQL  TEXT;
	v_strWHERE  TEXT;
BEGIN
	v_strWHERE := ' WHERE TH.numDomainID = ' || COALESCE(v_numDomainID,0);
    
	IF (v_numOppID > 0) then        
		v_strWHERE := COALESCE(v_strWHERE,'') || ' AND TH.numOppID = ' || COALESCE(v_numOppID,0);
	END IF;
    
	IF (v_numTransHistoryID > 0) then
		v_strWHERE := coalesce(v_strWHERE,'') || ' AND numTransHistoryID = ' || COALESCE(v_numTransHistoryID,0);
	END IF;
    
   IF (v_numDivisionID > 0) then
      v_strWHERE := coalesce(v_strWHERE,'') || ' AND TH.numDivisionID = ' || COALESCE(v_numDivisionID,0);
   end if;
    

	v_strSQL := 'SELECT
					numTransHistoryID,
					TH.numDivisionID,
					TH.numContactID,
					TH.numOppID,
					TH.numOppBizDocsID,
					vcTransactionID,
					tintTransactionStatus,
					(CASE WHEN tintTransactionStatus = 1 THEN ''Authorized/Pending Capture'' 
						 WHEN tintTransactionStatus = 2 THEN ''Captured''
						 WHEN tintTransactionStatus = 3 THEN ''Void''
						 WHEN tintTransactionStatus = 4 THEN ''Failed''
						 WHEN tintTransactionStatus = 5 THEN ''Credited''
						 ELSE ''Not Authorized''
					END) AS vcTransactionStatus,	 
					vcPGResponse,
					tintType,
					monAuthorizedAmt,
					monCapturedAmt,
					monRefundAmt,
					vcCardHolder,
					vcCreditCardNo,
					coalesce((SELECT vcData FROM ListDetails WHERE numListItemID = coalesce(numCardType,0)),''-'') as vcCardType,
					vcCVV2,
					tintValidMonth,
					intValidYear,
					TH.dtCreatedDate,
					coalesce(OBD.vcBizDocID,coalesce(OM.vcPOppName,'''')) AS vcBizOrderID,
					vcSignatureFile,
					coalesce(tintOppType,0) AS tintOppType,
					coalesce(vcResponseCode,'''') AS vcResponseCode
				FROM
					TransactionHistory TH
				LEFT JOIN OpportunityMaster OM ON OM.numOppId = TH.numOppID AND TH.numDomainID = OM.numDomainId
				LEFT JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId AND OBD.numOppBizDocsId = TH.numOppBizDocsID ';

	v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strWHERE,'');
	RAISE NOTICE '%',v_strSQL;
	OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


