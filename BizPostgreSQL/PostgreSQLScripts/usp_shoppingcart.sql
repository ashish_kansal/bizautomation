DROP FUNCTION IF EXISTS USP_ShoppingCart;

CREATE OR REPLACE FUNCTION USP_ShoppingCart(v_numDivID NUMERIC(18,0),                            
INOUT v_numOppID NUMERIC(18,0) DEFAULT null  ,                                              
INOUT v_vcPOppName VARCHAR(200) DEFAULT '' ,  
v_numContactId NUMERIC(18,0) DEFAULT NULL,                            
v_numDomainId NUMERIC(18,0) DEFAULT NULL,
v_vcSource VARCHAR(50) DEFAULT NULL,
v_numCampainID NUMERIC(9,0) DEFAULT 0,
v_numCurrencyID NUMERIC(9,0) DEFAULT NULL,
v_numBillAddressId NUMERIC(9,0) DEFAULT NULL,
v_numShipAddressId NUMERIC(9,0) DEFAULT NULL,
v_bitDiscountType BOOLEAN DEFAULT NULL,
v_fltDiscount NUMERIC DEFAULT NULL,
v_numProId NUMERIC(9,0) DEFAULT NULL,
v_numSiteID NUMERIC(9,0) DEFAULT NULL,
v_tintOppStatus INTEGER DEFAULT NULL,
v_monShipCost DECIMAL(20,5) DEFAULT NULL,
v_tintSource BIGINT DEFAULT NULL ,
v_tintSourceType SMALLINT DEFAULT NULL ,                          
v_numPaymentMethodId NUMERIC(9,0) DEFAULT NULL,
v_txtComments VARCHAR(1000) DEFAULT NULL,
v_numPartner NUMERIC(18,0) DEFAULT NULL,
v_txtFuturePassword VARCHAR(50) DEFAULT NULL,
v_intUsedShippingCompany NUMERIC(18,0) DEFAULT 0,
v_vcPartenerContact VARCHAR(200) DEFAULT '',
v_numShipmentMethod NUMERIC(18,0) DEFAULT 0,
v_vcOppRefOrderNo VARCHAR(200) DEFAULT '',
v_bitBillingTerms BOOLEAN DEFAULT NULL,
v_intBillingDays INTEGER DEFAULT NULL,
v_bitInterestType BOOLEAN DEFAULT NULL,
v_fltInterest NUMERIC DEFAULT NULL,
v_numOrderPromotionID NUMERIC(18,0) DEFAULT NULL,
v_numOrderPromotionDiscountID NUMERIC(18,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CRMType  INTEGER;               
   v_numRecOwner  INTEGER;       
   v_numPartenerContact  NUMERIC(18,0) DEFAULT 0;
   v_bitAllocateInventoryOnPickList  BOOLEAN;
   v_numDiscountItemID  NUMERIC(18,0);
   v_numRelationship  NUMERIC(18,0) DEFAULT 0;
   v_numProfile  NUMERIC(18,0) DEFAULT 0;
   v_tintCommitAllocation  SMALLINT;
   v_tintDefaultClassType  INTEGER DEFAULT 0;
	
   v_TotalAmount  DOUBLE PRECISION;
       

   v_numTemplateID  NUMERIC;
   v_fltExchangeRate  DOUBLE PRECISION;                                 
    
   v_numAccountClass  NUMERIC(18,0) DEFAULT 0;

   v_tintShipped  SMALLINT;      
   v_tintOppType  SMALLINT;
	v_numAddressID NUMERIC(18,0);
   v_vcStreet  VARCHAR(100);
   v_vcCity  VARCHAR(50);
   v_vcPostalCode  VARCHAR(15);
   v_vcCompanyName  VARCHAR(100);
   v_vcAddressName  VARCHAR(50);
   v_vcAltContact  VARCHAR(200);     
   v_numState  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numCompanyId  NUMERIC(9,0);
   v_numContact  NUMERIC(18,0);
   v_bitIsPrimary  BOOLEAN;
   v_bitAltContact  BOOLEAN;

	--Bill Address
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numExtranetID  NUMERIC(18,0);
BEGIN
   -- BEGIN TRANSACTION
select   coalesce(numDiscountServiceItemID,0), coalesce(tintCommitAllocation,1), coalesce(tintDefaultClassType,0) INTO v_numDiscountItemID,v_tintCommitAllocation,v_tintDefaultClassType FROM
   Domain WHERE
   numDomainId = v_numDomainId;
   SELECT  numContactId INTO v_numPartenerContact FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainId AND numDivisionId = v_numPartner AND vcEmail = v_vcPartenerContact     LIMIT 1;
   select   tintCRMType, numRecOwner, coalesce(bitAllocateInventoryOnPickList,false), coalesce(numCompanyType,0), coalesce(vcProfile,0) INTO v_CRMType,v_numRecOwner,v_bitAllocateInventoryOnPickList,v_numRelationship,
   v_numProfile FROM
   DivisionMaster
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
   numDivisionID = v_numDivID;


	--VALIDATE PROMOTION OF COUPON CODE
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPShoppingCart_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPShoppingCart CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPShoppingCart
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numCartID NUMERIC(18,0),
      numPromotionID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMPShoppingCart(numCartID,
		numPromotionID)
   SELECT
   numCartId,
		PromotionID
   FROM
   CartItems
   WHERE
   numUserCntId = v_numContactId
   AND coalesce(PromotionID,0) > 0;
   IF(SELECT COUNT(*) FROM tt_TEMPShoppingCart) > 0 then
	
		-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
      IF(SELECT
      COUNT(PO.numProId)
      FROM
      PromotionOffer PO
      LEFT JOIN
      PromotionOffer POOrder
      ON
      PO.numOrderPromotionID = POOrder.numProId
      INNER JOIN
      tt_TEMPShoppingCart T1
      ON
      PO.numProId = T1.numPromotionID
      WHERE
      PO.numDomainId = v_numDomainId
      AND coalesce(PO.IsOrderBasedPromotion,false) = false
      AND coalesce(PO.bitEnabled,false) = true
      AND 1 =(CASE
      WHEN coalesce(PO.numOrderPromotionID,0) > 0
      THEN(CASE WHEN POOrder.bitNeverExpires = true THEN true ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN true ELSE false END) END)
      ELSE(CASE WHEN PO.bitNeverExpires = true THEN true ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN true ELSE false END) END)
      END)
      AND 1 =(CASE
      WHEN coalesce(PO.numOrderPromotionID,0) > 0
      THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
      ELSE(CASE PO.tintCustomersBasedOn
         WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivID) > 0 THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
         WHEN 3 THEN 1
         ELSE 0
         END)
      END)) <>(SELECT COUNT(*) FROM tt_TEMPShoppingCart) then
		
         RAISE NOTICE 'ITEM_PROMOTION_EXPIRED';
         RETURN;
      end if;

		-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
      IF(SELECT
      COUNT(*)
      FROM
      PromotionOffer PO
      INNER JOIN
      tt_TEMPShoppingCart T1
      ON
      PO.numProId = T1.numPromotionID
      WHERE
      PO.numDomainId = v_numDomainId
      AND coalesce(IsOrderBasedPromotion,false) = false
      AND coalesce(numOrderPromotionID,0) > 0) = 1 then
		
         IF coalesce(v_numOrderPromotionDiscountID,0) > 0 then
			
            IF(SELECT
            COUNT(*)
            FROM
            DiscountCodes DC
            WHERE
            DC.numDiscountId = v_numOrderPromotionDiscountID) = 0 then
				
               RAISE NOTICE 'INVALID_COUPON_CODE_ITEM_PROMOTION';
               RETURN;
            ELSE
					-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
               IF(SELECT
               COUNT(*)
               FROM
               PromotionOffer PO
               INNER JOIN
               PromotionOffer POOrder
               ON
               PO.numOrderPromotionID = POOrder.numProId
               INNER JOIN
               tt_TEMPShoppingCart T1
               ON
               PO.numProId = T1.numPromotionID
               INNER JOIN
               DiscountCodes DC
               ON
               POOrder.numProId = DC.numPromotionID
               WHERE
               PO.numDomainId = v_numDomainId
               AND coalesce(PO.IsOrderBasedPromotion,false) = false
               AND DC.numDiscountId = v_numOrderPromotionDiscountID
               AND 1 =(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
               AND coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivID),0) >= DC.CodeUsageLimit) > 0 then
					
                  RAISE NOTICE 'ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED';
                  RETURN;
               end if;
            end if;
         ELSE
            RAISE NOTICE 'COUPON_CODE_REQUIRED_ITEM_PROMOTION';
            RETURN;
         end if;
      end if;
   end if;
   IF coalesce(v_numOrderPromotionID,0) > 0 then
	
		-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
      IF EXISTS(SELECT numProId FROM PromotionOffer WHERE numDomainId = v_numDomainId AND numProId = v_numOrderPromotionID AND coalesce(IsOrderBasedPromotion,false) = true) then
		
			-- CHECK IF ORDER PROMOTION IS STILL VALID
         IF NOT EXISTS(SELECT
         PO.numProId
         FROM
         PromotionOffer PO
         INNER JOIN
         PromotionOfferOrganizations PORG
         ON
         PO.numProId = PORG.numProId
         WHERE
         numDomainId = v_numDomainId
         AND PO.numProId = v_numOrderPromotionID
         AND coalesce(IsOrderBasedPromotion,false) = true
         AND coalesce(bitEnabled,false) = true
         AND true =(CASE WHEN bitNeverExpires = true THEN true ELSE(CASE WHEN TIMEZONE('UTC',now()) >= dtValidFrom AND TIMEZONE('UTC',now()) <= dtValidTo THEN true ELSE false END) END)
         AND numRelationship = v_numRelationship
         AND numProfile = v_numProfile) then
			
            RAISE NOTICE 'ORDER_PROMOTION_EXPIRED';
            RETURN;
         ELSEIF coalesce((SELECT coalesce(bitRequireCouponCode,false) FROM PromotionOffer WHERE numDomainId = v_numDomainId AND numProId = v_numOrderPromotionID),false) = true
         then
			
            IF coalesce(v_numOrderPromotionDiscountID,0) > 0 then
				
               IF(SELECT
               COUNT(*)
               FROM
               PromotionOffer PO
               INNER JOIN
               DiscountCodes DC
               ON
               PO.numProId = DC.numPromotionID
               WHERE
               PO.numDomainId = v_numDomainId
               AND PO.numProId = v_numOrderPromotionID
               AND coalesce(IsOrderBasedPromotion,false) = true
               AND coalesce(bitRequireCouponCode,false) = true
               AND coalesce(DC.numDiscountId,0) = v_numOrderPromotionDiscountID) = 0 then
					
                  RAISE NOTICE 'INVALID_COUPON_CODE_ORDER_PROMOTION';
                  RETURN;
               ELSE
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
                  IF(SELECT
                  COUNT(*)
                  FROM
                  PromotionOffer PO
                  INNER JOIN
                  DiscountCodes DC
                  ON
                  PO.numProId = DC.numPromotionID
                  WHERE
                  PO.numDomainId = v_numDomainId
                  AND PO.numProId = v_numOrderPromotionID
                  AND coalesce(IsOrderBasedPromotion,false) = true
                  AND coalesce(bitRequireCouponCode,false) = true
                  AND coalesce(DC.numDiscountId,0) = v_numOrderPromotionDiscountID
                  AND 1 =(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
                  AND coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivID),0) >= DC.CodeUsageLimit) > 0 then
						
                     RAISE NOTICE 'ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED';
                     RETURN;
                  ELSE
                     IF EXISTS(SELECT DC.numDiscountId FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId WHERE DC.numPromotionID = v_numOrderPromotionID AND DCU.numDivisionId = v_numDivID AND DC.numDiscountId = v_numOrderPromotionDiscountID) then
							
                        UPDATE
                        DiscountCodeUsage DCU
                        SET
                        intCodeUsed = coalesce(DCU.intCodeUsed,0)+1
                        FROM
                        DiscountCodes DC
                        WHERE
                        DC.numDiscountId = DCU.numDiscountId AND(DC.numPromotionID = v_numOrderPromotionID
                        AND DCU.numDivisionId = v_numDivID
                        AND DC.numDiscountId = v_numOrderPromotionDiscountID);
                     ELSE
                        INSERT INTO DiscountCodeUsage(numDiscountId
									,numDivisionId
									,intCodeUsed)
                        SELECT
                        DC.numDiscountId
									,v_numDivID
									,1
                        FROM
                        DiscountCodes DC
                        WHERE
                        DC.numPromotionID = v_numOrderPromotionID
                        AND DC.numDiscountId = v_numOrderPromotionDiscountID;
                     end if;
                  end if;
               end if;
            ELSE
               RAISE NOTICE 'COUPON_CODE_REQUIRED_ORDER_PROMOTION';
               RETURN;
            end if;
         end if;
      end if;
   ELSE
		-- IF PROMOTION ID NOT AVAILABLE THAN REMOVE ORDER PROMOTION DISCOUNT ITEM
      DELETE FROM CartItems WHERE numUserCntId = v_numContactId AND numItemCode = v_numDiscountItemID;
   end if;
   IF v_CRMType = 0 then
	
      UPDATE
      DivisionMaster
      SET
      tintCRMType = 1
      WHERE
      numDivisionID = v_numDivID;
   end if;
   IF LENGTH(coalesce(v_txtFuturePassword,'')) > 0 then
      select   numExtranetID INTO v_numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivID;
      UPDATE ExtranetAccountsDtl SET vcPassword = v_txtFuturePassword WHERE numExtranetID = v_numExtranetID;
   end if;
   select   numMirrorBizDocTemplateId INTO v_numTemplateID FROM
   eCommercePaymentConfig WHERE
   numSiteId = v_numSiteID
   AND numPaymentMethodId = v_numPaymentMethodId
   AND numDomainID = v_numDomainId;

   IF v_numCurrencyID = 0 then
	
      select   coalesce(numCurrencyID,0) INTO v_numCurrencyID FROM
      Domain WHERE
      numDomainId = v_numDomainId;
   end if;
   IF v_numCurrencyID = 0 then
	
      v_fltExchangeRate := 1;
   ELSE
      v_fltExchangeRate := GetExchangeRate(v_numDomainId,v_numCurrencyID);
   end if;
   IF v_tintDefaultClassType <> 0 then
	
      select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
      UserMaster UM
      JOIN
      Domain D
      ON
      UM.numDomainID = D.numDomainId WHERE
      D.numDomainId = v_numDomainId
      AND UM.numUserDetailId = v_numContactId;
   ELSEIF v_tintDefaultClassType = 2
   then --COMPANY
	
      select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
      DivisionMaster DM WHERE
      DM.numDomainID = v_numDomainId
      AND DM.numDivisionID = v_numDivID;
   ELSE
      v_numAccountClass := 0;
   end if;

   SELECT  numDefaultClass INTO v_numAccountClass FROM eCommerceDTL WHERE numSiteId = v_numSiteID AND numDomainID = v_numDomainId     LIMIT 1;
   INSERT INTO OpportunityMaster(numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcpOppName,monPAmount,
		numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,tintopptype,tintoppstatus,intPEstimatedCloseDate,
		numrecowner,bitOrder,numCurrencyID,fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,monShipCost,numOppBizDocTempID
		,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
		bitBillingTerms,intBillingDays,bitInterestType,fltInterest,numDiscountID)
	VALUES(v_numContactId,v_numDivID,v_txtComments,v_numCampainID,false,v_tintSource,v_tintSourceType,coalesce(v_vcPOppName, 'SO'),0,
		v_numContactId,TIMEZONE('UTC',now()),v_numContactId,TIMEZONE('UTC',now()),v_numDomainId,1,v_tintOppStatus,TIMEZONE('UTC',now()),v_numRecOwner,
		true,v_numCurrencyID,v_fltExchangeRate,v_fltDiscount,v_bitDiscountType,v_monShipCost,v_numTemplateID,v_numAccountClass,v_numPartner
		,v_intUsedShippingCompany,v_numPartenerContact,v_numShipmentMethod,LOCALTIMESTAMP ,v_vcOppRefOrderNo,v_bitBillingTerms,v_intBillingDays,
		v_bitInterestType,v_fltInterest,v_numOrderPromotionDiscountID) RETURNING numOppID INTO v_numOppID;

   PERFORM USP_UpdateNameTemplateValue(1::SMALLINT,v_numDomainId,v_numOppID);
   select   vcpOppName INTO v_vcPOppName FROM OpportunityMaster WHERE numOppId = v_numOppID;
   INSERT INTO OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,vcItemName,
		vcModelID,vcManufacturer,vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,
		bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems)
   SELECT
   v_numOppID,X.numItemCode,X.numUnitHour*X.decUOMConversionFactor,X.monPrice/X.decUOMConversionFactor,X.monTotAmount,X.vcItemDesc,
		NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM Item WHERE numItemCode = X.numItemCode),
		(SELECT vcModelID FROM Item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM Item WHERE numItemCode = X.numItemCode),
		(SELECT   vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = true AND numDomainId = v_numDomainId LIMIT 1),
		(SELECT coalesce(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode = VN.numItemCode and VN.numVendorID = IT.numVendorID WHERE VN.numItemCode = X.numItemCode),
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitPromotionTrigerred
		,coalesce((SELECT bitAllowDropShip FROM Item WHERE numItemCode = X.numItemCode),false),X.PromotionDesc,coalesce(vcChildKitItemSelection,'')
   FROM
   CartItems X
   WHERE
   numUserCntId = v_numContactId;

	--INSERT KIT ITEMS 
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityItems OI
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN
   Warehouses W
   ON
   WI.numWareHouseID = W.numWareHouseID
   JOIN
   ItemDetails ID
   ON
   OI.numItemCode = ID.numItemKitID
   JOIN
   Item I
   ON
   ID.numChildItemID = I.numItemCode
   WHERE
   OI.numOppId = v_numOppID
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OI.numWarehouseItmsID,0) > 0
   AND I.charItemType = 'P'
   AND coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ID.numChildItemID AND numWareHouseID = W.numWareHouseID LIMIT 1),0) = 0) > 0 then
	
      RAISE NOTICE 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
   end if;

   RAISE NOTICE '%','test';

   INSERT INTO OpportunityKitItems(numOppId,
		numOppItemID,
		numChildItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost)
   SELECT
   v_numOppID,
		OI.numoppitemtCode,
		ID.numChildItemID,
		(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),
		(ID.numQtyItemsReq*coalesce(fn_UOMConversion(ID.numUOMID,ID.numChildItemID,v_numDomainId,I.numBaseUnit),1))*OI.numUnitHour,
		ID.numQtyItemsReq,
		ID.numUOMID,
		0,
		coalesce(I.monAverageCost,0)
   FROM
   OpportunityItems OI
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN
   Warehouses W
   ON
   WI.numWareHouseID = W.numWareHouseID
   JOIN
   ItemDetails ID
   ON
   OI.numItemCode = ID.numItemKitID
   JOIN
   Item I
   ON
   ID.numChildItemID = I.numItemCode
   WHERE
   OI.numOppId = v_numOppID; 

   RAISE NOTICE '%','test1';

	--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
   DROP TABLE IF EXISTS tt_TEMPShoppingCartKITCONFIGURATION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPShoppingCartKITCONFIGURATION
   (
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      KitChildItems TEXT
   );
   INSERT INTO
   tt_TEMPShoppingCartKITCONFIGURATION
   SELECT
   numoppitemtCode,
		numItemCode,
		vcChildKitSelectedItems
   FROM
   OpportunityItems
   WHERE
   numOppId = v_numOppID
   AND LENGTH(coalesce(vcChildKitSelectedItems,'')) > 0;
   IF(SELECT
   COUNT(*)
   FROM(SELECT
      t1.numOppItemID
				,t1.numItemCode
				,SUBSTR(items,0,POSITION('-' IN items))::NUMERIC AS numKitItemID
				,SUBSTR(items,POSITION('-' IN items)+1,LENGTH(items) -POSITION('-' IN items))::NUMERIC As numKitChildItemID
      FROM
      tt_TEMPShoppingCartKITCONFIGURATION t1
      CROSS JOIN LATERAL(SELECT * FROM
         Split((SELECT KitChildItems FROM tt_TEMPShoppingCartKITCONFIGURATION t3 WHERE t3.numOppItemID = t1.numOppItemID AND t3.numItemCode = t1.numItemCode),',')) as t2) TempChildItems
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKI.numOppId = v_numOppID
   AND OKI.numOppItemID = TempChildItems.numOppItemID
   AND OKI.numChildItemID = TempChildItems.numKitItemID
   INNER JOIN
   OpportunityItems OI
   ON
   OI.numItemCode = TempChildItems.numItemCode
   AND OI.numoppitemtCode = OKI.numOppItemID
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN
   Warehouses W
   ON
   WI.numWareHouseID = W.numWareHouseID
   INNER JOIN
   ItemDetails
   ON
   TempChildItems.numKitItemID = ItemDetails.numItemKitID
   AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   WHERE
   Item.charItemType = 'P'
   AND coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID LIMIT 1),0) = 0) > 0 then
	
      RAISE NOTICE 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
   end if;

   RAISE NOTICE '%','test3';

   INSERT INTO OpportunityKitChildItems(numOppId,
		numOppItemID,
		numOppChildItemID,
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost)
   SELECT
   v_numOppID,
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		ItemDetails.numChildItemID,
		(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),
		(ItemDetails.numQtyItemsReq*coalesce(fn_UOMConversion(ItemDetails.numUOMID,ItemDetails.numChildItemID,v_numDomainId,Item.numBaseUnit),1))*OKI.numQtyItemsReq,
		ItemDetails.numQtyItemsReq,
		ItemDetails.numUOMID,
		0,
		coalesce(Item.monAverageCost,0)
   FROM(SELECT
      t1.numOppItemID
			,t1.numItemCode
			,SUBSTR(items,0,POSITION('-' IN items))::NUMERIC AS numKitItemID
			,SUBSTR(items,POSITION('-' IN items)+1,LENGTH(items) -POSITION('-' IN items))::NUMERIC As numKitChildItemID
      FROM
      tt_TEMPShoppingCartKITCONFIGURATION t1
      CROSS JOIN LATERAL(SELECT * FROM
         Split((SELECT KitChildItems FROM tt_TEMPShoppingCartKITCONFIGURATION t3 WHERE t3.numOppItemID = t1.numOppItemID AND t3.numItemCode = t1.numItemCode),',')) as t2) TempChildItems
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKI.numOppId = v_numOppID
   AND OKI.numOppItemID = TempChildItems.numOppItemID
   AND OKI.numChildItemID = TempChildItems.numKitItemID
   INNER JOIN
   OpportunityItems OI
   ON
   OI.numItemCode = TempChildItems.numItemCode
   AND OI.numoppitemtCode = OKI.numOppItemID
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN
   Warehouses W
   ON
   WI.numWareHouseID = W.numWareHouseID
   INNER JOIN
   ItemDetails
   ON
   TempChildItems.numKitItemID = ItemDetails.numItemKitID
   AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode;

   BEGIN
      DROP TABLE IF EXISTS tt_TEMPShoppingCartCONTAINER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPShoppingCartCONTAINER
      (
         numConainer NUMERIC(18,0),
         numNoItemIntoContainer NUMERIC(18,0),
         numWareHouseID NUMERIC(18,0),
         monPrice DECIMAL(20,5),
         monAverageCost DECIMAL(20,5),
         numQtyToFit DOUBLE PRECISION,
         vcItemName VARCHAR(300),
         vcItemDesc VARCHAR(1000),
         vcModelID VARCHAR(300)
      );
      INSERT INTO tt_TEMPShoppingCartCONTAINER(numConainer
			,numNoItemIntoContainer
			,numWareHouseID
			,monPrice
			,monAverageCost
			,numQtyToFit
			,vcItemName
			,vcItemDesc
			,vcModelID)
      SELECT
      I.numContainer
			,I.numNoItemIntoContainer
			,WI.numWareHouseID
			,coalesce(IContainer.monListPrice,0)
			,coalesce(IContainer.monAverageCost,0)
			,SUM(numUnitHour)
			,coalesce(IContainer.vcItemName,'')
			,coalesce(IContainer.txtItemDesc,'')
			,coalesce(IContainer.vcModelID,'')
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      Item IContainer
      ON
      I.numContainer = IContainer.numItemCode
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      numOppID = v_numOppID
      AND coalesce(OI.bitDropShip,false) = false
      AND coalesce(I.bitContainer,false) = false
      AND coalesce(IContainer.bitContainer,false) = true
      AND coalesce(I.numContainer,0) > 0
      AND coalesce(I.numNoItemIntoContainer,0) > 0
      GROUP BY
      I.numContainer,I.numNoItemIntoContainer,IContainer.monListPrice,IContainer.monAverageCost,
      WI.numWareHouseID,IContainer.vcItemName,IContainer.txtItemDesc,
      IContainer.vcModelID;
      INSERT INTO OpportunityItems(numOppId
			,numItemCode
			,numWarehouseItmsID
			,numUnitHour
			,monPrice
			,fltDiscount
			,bitDiscountType
			,monTotAmount
			,monTotAmtBefDiscount
			,monAvgCost
			,vcItemName
			,vcItemDesc
			,vcModelID)
      SELECT
      v_numOppID
			,numConainer
			,(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = numConainer AND WareHouseItems.numWareHouseID = T1.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1)
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEIL(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)
			,monPrice
			,0
			,1
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEIL(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)*monPrice
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEIL(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)*monPrice
			,monAverageCost
			,vcItemName
			,vcItemDesc
			,vcModelID
      FROM
      tt_TEMPShoppingCartCONTAINER T1;
      select   tintoppstatus, tintopptype, tintshipped INTO v_tintOppStatus,v_tintOppType,v_tintShipped FROM
      OpportunityMaster WHERE
      numOppId = v_numOppID;
      EXCEPTION WHEN OTHERS THEN
		-- DO NOT RAISE ERROR
         NULL;
   END;
   PERFORM USP_OpportunityItems_UpdateWarehouseMapping(v_numDomainId,v_numOppID);
   IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
	
      PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,v_numContactId);
   end if;
   PERFORM USP_OpportunityItems_CheckWarehouseMapping(v_numDomainId,v_numOppID);
   select   SUM(monTotAmount) INTO v_TotalAmount FROM
   OpportunityItems WHERE
   numOppId = v_numOppID;
   UPDATE
   OpportunityMaster
   SET
   monPAmount = v_TotalAmount -v_fltDiscount
   WHERE
   numOppId = v_numOppID;
   IF(v_vcSource IS NOT NULL) then
	
      insert into OpportunityLinking(numParentOppID,numChildOppID,vcSource,numParentOppBizDocID,numPromotionId,numSiteID)  values(null,v_numOppID,v_vcSource,NULL,v_numProId,v_numSiteID);
   end if;

	--Add/Update Address Details
   IF v_numBillAddressId > 0 then
	
		 --  numeric(9, 0)
		 --  tinyint
		 --  varchar(100)
		 --  varchar(50)
		 --  varchar(15)
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  varchar(100)
		 --  numeric(9, 0)
      select numAddressID,coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, numContact, bitAltContact, vcAltContact 
	  INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails WHERE
      numDomainID = v_numDomainId
      AND numAddressID = v_numBillAddressId;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 0::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := '',v_numCompanyId := 0,
      v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;
  
  
	--Ship Address
   IF v_numShipAddressId > 0 then
	
      select  numAddressID,coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, numContact, bitAltContact, vcAltContact 
	  INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails WHERE
      numDomainID = v_numDomainId
      AND numAddressID = v_numShipAddressId;
		 --  numeric(9, 0)
		 --  tinyint
		 --  varchar(100)
		 --  varchar(50)
		 --  varchar(15)
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  varchar(100)
		 --  numeric(9, 0)
      select   vcCompanyName, div.numCompanyID INTO v_vcCompanyName,v_numCompanyId from CompanyInfo Com
      join DivisionMaster div
      on div.numCompanyID = Com.numCompanyId where div.numDivisionID = v_numDivID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := v_numCompanyId,v_vcAddressName := v_vcAddressName,
      v_bitCalledFromProcedure := true,v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
   INSERT INTO OpportunityMasterTaxItems(numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID)
   SELECT
   v_numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
   FROM
   TaxItems TI
   JOIN
   DivisionTaxTypes DTT
   ON
   TI.numTaxItemID = DTT.numTaxItemID
   CROSS JOIN LATERAL(SELECT
      decTaxValue,
			tintTaxType,
			numTaxID
      FROM
      fn_CalItemTaxAmt(v_numDomainId,v_numDivID,0,TI.numTaxItemID,v_numOppID,false,NULL)) AS TEMPTax
   WHERE
   DTT.numDivisionID = v_numDivID AND DTT.bitApplicable = true
   UNION
   SELECT
   v_numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
   FROM
   DivisionMaster
   CROSS JOIN LATERAL(SELECT
      decTaxValue,
			tintTaxType,
			numTaxID
      FROM
      fn_CalItemTaxAmt(v_numDomainId,v_numDivID,0,0,v_numOppID,true,NULL)) AS TEMPTax
   WHERE
   bitNoTax = false
   AND numDivisionID = v_numDivID
   UNION
   SELECT
   v_numOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
   FROM
   fn_CalItemTaxAmt(v_numDomainId,v_numDivID,0,1,v_numOppID,true,NULL);
  
	--Delete Tax for Opportunity Items if item deleted 
   DELETE FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID AND numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID);

	--Insert Tax for Opportunity Items
   INSERT INTO OpportunityItemsTaxItems(numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID)
   SELECT
   v_numOppID,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0
   FROM
   OpportunityItems OI
   JOIN
   ItemTax IT
   ON
   OI.numItemCode = IT.numItemCode
   JOIN
   TaxItems TI
   ON
   TI.numTaxItemID = IT.numTaxItemID
   WHERE
   OI.numOppId = v_numOppID
   AND IT.bitApplicable = true
   AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
   UNION
   SELECT
   v_numOppID,
		OI.numoppitemtCode,
		0,
		0
   FROM
   OpportunityItems OI
   JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   OI.numOppId = v_numOppID
   AND I.bitTaxable = true
   AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
   UNION
   SELECT
   v_numOppID,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
   FROM
   OpportunityItems OI
   INNER JOIN
   ItemTax IT
   ON
   IT.numItemCode = OI.numItemCode
   INNER JOIN
   TaxDetails TD
   ON
   TD.numTaxID = IT.numTaxID
   AND TD.numDomainId = v_numDomainId
   WHERE
   OI.numOppId = v_numOppID
   AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID);
   UPDATE
   OpportunityItems OI
   SET
   vcInclusionDetail = GetOrderAssemblyKitInclusionDetails(OI.numOppId,OI.numoppitemtCode,OI.numUnitHour,1::SMALLINT,true)
   FROM
   Item I
   WHERE
   OI.numItemCode = I.numItemCode AND(numOppId = v_numOppID
   AND coalesce(I.bitKitParent,false) = true);
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
	  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


