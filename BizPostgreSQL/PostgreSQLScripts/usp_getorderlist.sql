-- Stored procedure definition script USP_GetOrderList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOrderList(v_SortChar CHAR(1) DEFAULT '0',       
 v_CurrentPage INTEGER DEFAULT NULL,    
 v_PageSize INTEGER DEFAULT NULL,    
 INOUT v_TotRecs INTEGER  DEFAULT NULL,    
 v_columnName VARCHAR(50) DEFAULT NULL,    
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,  
 v_numCompanyID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql    
    
--Create a Temporary table to hold data    
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);    
   v_firstRec  INTEGER;     
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numOppID VARCHAR(15),
      Name VARCHAR(100),
      STAGE VARCHAR(100),
      closeDate VARCHAR(50),
      numcreatedby VARCHAR(100),
      Contact VARCHAR(110),
      Company VARCHAR(100),
      numCompanyID VARCHAR(15),
      tintCRMType VARCHAR(10),
      numcontactid VARCHAR(15),
      numDivisionID  VARCHAR(15),
      numTerID VARCHAR(15),
      monPAmount DECIMAL(20,5),
      vcusername VARCHAR(100)
   );    
   v_strSql := 'SELECT  Opp.numOppId,     
  Opp.vcPOppName AS Name,     
  GetOppStatus(Opp.numOppId) as STAGE,     
  Opp.intPEstimatedCloseDate AS CloseDate,     
  coalesce(Opp.numCreatedBy,0) ,    
  ADC.vcFirstName || '' '' || ADC.vcLastName AS Contact,     
  C.vcCompanyName AS Company,    
                C.numCompanyId,    
  Div.tintCRMType,    
  ADC.numContactId,    
  Div.numDivisionID,    
  Div.numTerID,    
  Opp.monPAmount,    
  coalesce(UserMaster.vcusername,'''')     
  FROM OpportunityMaster Opp     
  INNER JOIN AdditionalContactsInformation ADC     
  ON Opp.numContactId = ADC.numContactId     
  INNER JOIN DivisionMaster Div     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID     
  INNER JOIN CompanyInfo C     
  ON Div.numCompanyID = C.numCompanyId    
  left join UserMaster on UserMaster.numuserid = Opp.numCreatedBy     
  WHERE Opp.tintOppStatus = 1 ';    
  
   if v_numCompanyID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And Div.numCompanyID =' || SUBSTR(CAST(v_numCompanyID AS VARCHAR(15)),1,15);
   end if;    
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;     
  
    
   EXECUTE 'insert into tt_TEMPTABLE(numOppId,    
      Name,    
      STAGE,    
      CloseDate,    
 numcreatedby ,    
 Contact,    
 Company,    
 numCompanyID,    
 tintCRMType,    
 numContactID,    
 numDivisionID,    
 numTerID,    
 monPAmount,    
 vcusername)    
' || v_strSql;     
    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;    
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);    
   open SWV_RefCur for
   select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;    
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;    
   RETURN;
END; $$;


