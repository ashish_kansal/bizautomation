-- Stored procedure definition script USP_GetEmbeddedCostClass for PostgreSQL
Create or replace FUNCTION USP_GetEmbeddedCostClass(v_numDOmainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numDomainID as VARCHAR(255)),
                cast(numItemClassification as VARCHAR(255))
   FROM    EmbeddedCostConfig
   WHERE   numDomainID = v_numDOmainID;
END; $$;











