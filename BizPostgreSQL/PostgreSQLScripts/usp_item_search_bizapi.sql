-- Stored procedure definition script USP_Item_Search_BIZAPI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE OR REPLACE FUNCTION USP_Item_Search_BIZAPI(v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_SearchText VARCHAR(100) DEFAULT NULL,
	v_numPageIndex INTEGER DEFAULT 0,
    v_numPageSize INTEGER DEFAULT 0,
    INOUT v_TotalRecords INTEGER  DEFAULT NULL,
	v_dtCreatedAfter TIMESTAMP DEFAULT NULL,
	v_dtModifiedAfter TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

   select   COUNT(*) INTO v_TotalRecords FROM
   Item WHERE
   Item.numDomainID = v_numDomainID
   AND	Item.vcItemName ilike '%' || coalesce(v_SearchText,'') || '%'
   AND (v_dtCreatedAfter IS NULL OR Item.bintCreatedDate > v_dtCreatedAfter)
   AND (v_dtModifiedAfter IS NULL OR Item.bintModifiedDate > v_dtModifiedAfter OR(SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = Item.numItemCode AND dtModified+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'hour' as interval) > v_dtModifiedAfter) > 0);

   DROp TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      SELECT * FROM(SELECT
      ROW_NUMBER() OVER(ORDER BY Item.vcItemName asc) AS "RowNumber",
		Item.numItemCode AS "numItemCode",
		Item.vcItemName AS "vcItemName",
		(SELECT  vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND coalesce(bitDefault,false) = true LIMIT 1) AS "vcPathForImage",
		(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND coalesce(bitDefault,false) = true LIMIT 1) AS "vcPathForTImage",
		Item.txtItemDesc AS "txtItemDesc",
		Item.charItemType AS "charItemType",
		CASE
      WHEN Item.charItemType = 'P' THEN 'Inventory Item'
      WHEN Item.charItemType = 'N' THEN 'Non Inventory Item'
      WHEN Item.charItemType = 'S' THEN 'Service'
      WHEN Item.charItemType = 'A' THEN 'Accessory'
      END AS "ItemType",
		Item.vcSKU AS "vcSKU",
		Item.vcModelID AS "vcModelID",
		Item.numBarCodeId AS "numBarCodeId",
		Item.vcManufacturer AS "vcManufacturer",
		Item.vcUnitofMeasure AS "vcUnitofMeasure",
		Item.monListPrice AS "monListPrice",
		(CASE WHEN coalesce(Item.bitVirtualInventory,false) = true THEN 0 ELSE Item.monAverageCost END) AS "monAverageCost",
		Item.monCampaignLabourCost AS "monCampaignLabourCost",
		Item.numItemGroup AS "numItemGroup",
		Item.numItemClass AS "numItemClass",
		Item.numShipClass AS "numShipClass",
		Item.numItemClassification AS "numItemClassification",
		Item.fltWidth AS "fltWidth",
		Item.fltWeight AS "fltWeight",
		Item.fltHeight AS "fltHeight",
		Item.fltLength AS "fltLength",
		Item.numBaseUnit AS "numBaseUnit",
		Item.numSaleUnit AS "numSaleUnit",
		Item.numPurchaseUnit AS "numPurchaseUnit",
		Item.bitKitParent AS "bitKitParent",
		Item.bitLotNo AS "bitLotNo",
		Item.bitAssembly AS "bitAssembly",
		Item.bitSerialized AS "bitSerialized",
		Item.bitTaxable AS "bitTaxable",
		Item.bitAllowBackOrder AS "bitAllowBackOrder",
		Item.bitFreeShipping AS "bitFreeShipping",
		Item.numCOGsChartAcntId AS "numCOGsChartAcntId",
		Item.numAssetChartAcntId AS "numAssetChartAcntId",
		Item.numIncomeChartAcntId AS "numIncomeChartAcntId",
		Item.numVendorID AS "numVendorID",
		SUM(numOnHand) as "numOnHand",
		SUM(numOnOrder) as "numOnOrder",
		SUM(numReorder)  as "numReorder",
		SUM(numAllocation)  as "numAllocation",
		SUM(numBackOrder)  as "numBackOrder",
		bintCreatedDate AS "bintCreatedDate",
		bintModifiedDate AS "bintModifiedDate",
		COALESCE((SELECT string_agg(numCategoryID::VARCHAR,',') FROM ItemCategory WHERE numItemID = Item.numItemCode GROUP BY numCategoryID),'') AS "vcCategories"
      FROM
      Item
      LEFT JOIN
      WareHouseItems W
      ON
      W.numItemID = Item.numItemCode
      WHERE
      Item.numDomainID = v_numDomainID AND
      Item.vcItemName ilike '%' || coalesce(v_SearchText,'') || '%'
      AND (v_dtCreatedAfter IS NULL OR Item.bintCreatedDate > v_dtCreatedAfter)
      AND (v_dtModifiedAfter IS NULL OR Item.bintModifiedDate > v_dtModifiedAfter OR(SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = Item.numItemCode AND dtModified+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'hour' as interval) > v_dtModifiedAfter) > 0)
      GROUP BY
      Item.numItemCode,Item.vcItemName,Item.txtItemDesc,Item.charItemType,Item.vcSKU,
      Item.vcModelID,Item.numBarCodeId,Item.vcManufacturer,Item.vcUnitofMeasure,
      Item.monListPrice,Item.monAverageCost,Item.bitVirtualInventory,
      Item.monCampaignLabourCost,Item.numItemGroup,Item.numItemClass,Item.numShipClass,
      Item.numItemClassification,Item.fltWidth,Item.fltWeight,
      Item.fltHeight,Item.fltLength,Item.numBaseUnit,Item.numSaleUnit,Item.numPurchaseUnit,
      Item.bitKitParent,Item.bitLotNo,Item.bitAssembly,Item.bitSerialized,
      Item.bitTaxable,Item.bitAllowBackOrder,Item.bitFreeShipping,
      Item.numCOGsChartAcntId,Item.numAssetChartAcntId,Item.numIncomeChartAcntId,
      Item.numVendorID,Item.bintCreatedDate,Item.bintModifiedDate) AS I
      WHERE
		(v_numPageIndex = 0 OR v_numPageSize = 0) OR
		("RowNumber" >((v_numPageIndex::bigint -1)*v_numPageSize::bigint) and "RowNumber" <((v_numPageIndex::bigint*v_numPageSize::bigint)+1));

   open SWV_RefCur for
   SELECT * FROM tt_TEMP;

	--GET CHILD ITEMS
   open SWV_RefCur2 for
   SELECT
   ItemDetails.numItemKitID AS "numItemKitID",
		ItemDetails.numChildItemID AS "numChildItemID",
		ItemDetails.numQtyItemsReq AS "numQtyItemsReq",
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMID) AS "vcUnitName"
   FROM
   ItemDetails
   WHERE
   ItemDetails.numItemKitID IN(SELECT "numItemCode" FROM tt_TEMP);

	--GET CUSTOM FIELDS
   open SWV_RefCur3 for
   SELECT
   I.numItemCode AS "numItemCode",
		TEMPCustFields.Fld_id AS "Id",
		TEMPCustFields.Fld_label AS "Name",
		TEMPCustFields.Fld_type AS "Type",
		TEMPCustFields.Fld_Value AS "ValueId",
		CASE
   WHEN TEMPCustFields.Fld_type = 'TextBox' or TEMPCustFields.Fld_type = 'TextArea' THEN(CASE WHEN TEMPCustFields.Fld_Value = '0' OR TEMPCustFields.Fld_Value = '' THEN '-' ELSE TEMPCustFields.Fld_Value END)
   WHEN TEMPCustFields.Fld_type = 'SelectBox' AND SWF_IsNumeric(TEMPCustFields.Fld_Value) THEN(CASE WHEN TEMPCustFields.Fld_Value = '' THEN '-' ELSE GetListIemName(TEMPCustFields.Fld_Value::NUMERIC) END)
   WHEN TEMPCustFields.Fld_type = 'CheckBox' THEN(CASE WHEN TEMPCustFields.Fld_Value = '1' THEN 'Yes' ELSE 'No' END)
   ELSE
      ''
   END AS Value
   FROM
   Item I
   LEFT JOIN LATERAL(SELECT
      CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.FLd_label,
			CFW_Fld_Master.fld_type,
			CFW_FLD_Values_Item.Fld_Value
      FROM
      CFW_Fld_Master
      LEFT JOIN
      CFW_FLD_Values_Item
      ON
      CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
      AND I.numItemCode = CFW_FLD_Values_Item.RecId
      WHERE
      numDomainID = v_numDomainID
      AND Grp_id = 5) TEMPCustFields on TRUE
   WHERE
   I.numDomainID = v_numDomainID
   AND I.numItemCode IN(SELECT "numItemCode" FROM tt_TEMP);

   RETURN;
END; $$;


