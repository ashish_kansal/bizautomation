-- Stored procedure definition script USP_BizAPI_GetSecretKey for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizAPI_GetSecretKey(v_BizAPIPublicKey VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN                                                                
   open SWV_RefCur for 
   SELECT 
		U.numUserId as  numUserId,
        numUserDetailId as  numUserDetailId,
        coalesce(vcEmailID,'') as vcEmailID ,
        coalesce(numGroupID,0) as numGroupID ,
        bitActivateFlag as  bitActivateFlag,
        coalesce(vcMailNickName,'') as vcMailNickName ,
        coalesce(txtSignature,'') as txtSignature ,
        coalesce(U.numDomainID,0) as numDomainID ,
        coalesce(Div.numDivisionID,0) AS numDivisionID ,
        coalesce(vcCompanyName,'') as vcCompanyName ,
        coalesce(E.bitExchangeIntegration,false) as bitExchangeIntegration ,
        coalesce(E.bitAccessExchange,false) as bitAccessExchange ,
        coalesce(E.vcExchPath,'') as vcExchPath ,
        coalesce(E.vcExchDomain,'') as vcExchDomain ,
        coalesce(D.vcExchUserName,'') as vcExchUserName ,
        coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS ContactName ,
        CASE WHEN E.bitAccessExchange = false THEN E.vcExchPassword
   WHEN E.bitAccessExchange = true THEN D.vcExchPassword
   END as vcExchPassword ,
        tintCustomPagingRows as  tintCustomPagingRows,
        vcDateFormat,
        numDefCountry,
        tintComposeWindow,
        TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval) AS StartDate ,
        TIMEZONE('UTC',now())+CAST(-sintStartDate || 'day' as interval)+CAST(sintNoofDaysInterval || 'day' as interval) AS EndDate ,
        tintAssignToCriteria,
        bitIntmedPage,
        tintFiscalStartMonth,
        numAdminID,
        coalesce(A.numTeam,0) AS numTeam ,
        coalesce(D.vcCurrency,'') as vcCurrency ,
        coalesce(D.numCurrencyID,0) as numCurrencyID ,
        coalesce(D.bitMultiCurrency,false) as bitMultiCurrency ,
        bitTrial,
        coalesce(tintChrForComSearch,0) as tintChrForComSearch ,
        coalesce(tintChrForItemSearch,0) as tintChrForItemSearch ,
        CASE WHEN bitSMTPServer = true THEN vcSMTPServer
   ELSE ''
   END AS vcSMTPServer ,
        CASE WHEN bitSMTPServer = true THEN coalesce(numSMTPPort,0)
   ELSE 0
   END AS numSMTPPort ,
        coalesce(bitSMTPAuth,false)  AS bitSMTPAuth ,
        coalesce(vcSmtpPassword,'')  AS vcSmtpPassword ,
        coalesce(bitSMTPSSL,false)  AS bitSMTPSSL ,
        coalesce(bitSMTPServer,false)  AS bitSMTPServer ,
        coalesce(IM.bitImap,false)  AS bitImapIntegration ,
        coalesce(charUnitSystem,'E')  AS UnitSystem ,
        DATE_PART('day',dtSubEndDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) AS NoOfDaysLeft ,
        coalesce(bitCreateInvoice,0)  AS bitCreateInvoice ,
        coalesce(numDefaultSalesBizDocId,0)  AS numDefaultSalesBizDocId ,
        coalesce(numDefaultPurchaseBizDocId,0)  AS numDefaultPurchaseBizDocId ,
        vcDomainName  ,
        S.numSubscriberID  ,
        D.tintBaseTax  ,
        coalesce(D.numShipCompany,0)  AS numShipCompany ,
        coalesce(D.bitEmbeddedCost,false)  AS bitEmbeddedCost ,
        (SELECT  coalesce(numAuthoritativeSales,0) 
      FROM      AuthoritativeBizDocs
      WHERE     numDomainId = D.numDomainId) AS numAuthoritativeSales ,
        (SELECT    coalesce(numAuthoritativePurchase,0) 
      FROM      AuthoritativeBizDocs
      WHERE     numDomainId = D.numDomainId) AS numAuthoritativePurchase ,
        coalesce(D.numDefaultSalesOppBizDocId,0)  AS numDefaultSalesOppBizDocId ,
        coalesce(tintDecimalPoints,2)  AS tintDecimalPoints ,
        coalesce(D.tintBillToForPO,0)  AS tintBillToForPO ,
        coalesce(D.tintShipToForPO,0)  AS tintShipToForPO ,
        coalesce(D.tintOrganizationSearchCriteria,0)  AS tintOrganizationSearchCriteria ,
        coalesce(D.tintLogin,0)  AS tintLogin ,
        coalesce(bitDocumentRepositary,false)  AS bitDocumentRepositary ,
        coalesce(D.tintComAppliesTo,0)  AS tintComAppliesTo ,
        coalesce(D.vcPortalName,'')  AS vcPortalName ,
        (SELECT    COUNT(*)
      FROM      Subscribers
      WHERE     TIMEZONE('UTC',now()) BETWEEN dtSubStartDate
      AND     dtSubEndDate
      AND bitActive = 1) ,
        coalesce(D.bitGtoBContact,false)  AS bitGtoBContact ,
        coalesce(D.bitBtoGContact,false)  AS bitBtoGContact ,
        coalesce(D.bitGtoBCalendar,false)  AS bitGtoBCalendar ,
        coalesce(D.bitBtoGCalendar,false)  AS bitBtoGCalendar ,
        coalesce(bitExpenseNonInventoryItem,false)  AS bitExpenseNonInventoryItem ,
        coalesce(D.bitInlineEdit,false)  AS bitInlineEdit ,
        coalesce(U.numDefaultClass,0)  AS numDefaultClass ,
        coalesce(D.bitRemoveVendorPOValidation,false)  AS bitRemoveVendorPOValidation ,
        coalesce(D.bitTrakDirtyForm,true)  AS bitTrakDirtyForm ,
        coalesce(D.tintBaseTaxOnArea,0)  AS tintBaseTaxOnArea ,
        coalesce(D.bitAmountPastDue,false)  AS bitAmountPastDue ,
        coalesce(D.monAmountPastDue,0) AS monAmountPastDue ,
        coalesce(D.bitSearchOrderCustomerHistory,false)  AS bitSearchOrderCustomerHistory ,
        coalesce(U.tintTabEvent,0)  AS tintTabEvent ,
        coalesce(D.bitRentalItem,false)  AS bitRentalItem ,
        coalesce(D.numRentalItemClass,0)  AS numRentalItemClass ,
        coalesce(D.numRentalHourlyUOM,0)  AS numRentalHourlyUOM ,
        coalesce(D.numRentalDailyUOM,0)  AS numRentalDailyUOM ,
        coalesce(D.tintRentalPriceBasedOn,0)  AS tintRentalPriceBasedOn ,
        coalesce(D.tintCalendarTimeFormat,24)  AS tintCalendarTimeFormat ,
        coalesce(D.tintDefaultClassType,0)  AS tintDefaultClassType ,
        coalesce(U.numDefaultWarehouse,0)  AS numDefaultWarehouse ,
        coalesce(D.tintPriceBookDiscount,0)  AS tintPriceBookDiscount ,
        coalesce(D.bitAutoSerialNoAssign,false)  AS bitAutoSerialNoAssign ,
        coalesce(IsEnableProjectTracking,false)  AS IsEnableProjectTracking ,
        coalesce(IsEnableCampaignTracking,false)  AS IsEnableCampaignTracking ,
        coalesce(IsEnableResourceScheduling,false)  AS IsEnableResourceScheduling ,
        coalesce(D.numSOBizDocStatus,0)  AS numSOBizDocStatus ,
        coalesce(D.bitAutolinkUnappliedPayment,false)  AS bitAutolinkUnappliedPayment ,
        coalesce(numShippingServiceItemID,0)  AS numShippingServiceItemID ,
        coalesce(D.numDiscountServiceItemID,0)  AS numDiscountServiceItemID ,
        coalesce(D.tintCommissionType,1)  AS tintCommissionType ,
        coalesce(D.bitDiscountOnUnitPrice,false)  AS bitDiscountOnUnitPrice ,
        coalesce(D.intShippingImageWidth,0)  AS intShippingImageWidth ,
        coalesce(D.intShippingImageHeight,0)  AS intShippingImageHeight ,
        coalesce(D.numTotalInsuredValue,0)  AS numTotalInsuredValue ,
        coalesce(D.numTotalCustomsValue,0)  AS numTotalCustomsValue ,
        coalesce(D.bitUseBizdocAmount,true)  AS bitUseBizdocAmount ,
        coalesce(D.bitDefaultRateType,true)  AS bitDefaultRateType ,
        coalesce(D.bitIncludeTaxAndShippingInCommission,true)  AS bitIncludeTaxAndShippingInCommission,
        U.vcBizAPISecretKey 
   FROM   UserMaster U
   LEFT JOIN ExchangeUserDetails E ON E.numUserID = U.numUserId
   LEFT JOIN ImapUserDetails IM ON IM.numUserCntID = U.numUserDetailId
   JOIN Domain D ON D.numDomainId = U.numDomainID
   LEFT JOIN DivisionMaster Div ON D.numDivisionId = Div.numDivisionID
   LEFT JOIN CompanyInfo C ON C.numCompanyId = Div.numDivisionID
   LEFT JOIN AdditionalContactsInformation A ON A.numContactId = U.numUserDetailId
   JOIN Subscribers S ON S.numTargetDomainID = D.numDomainId
   WHERE  vcBizAPIPublicKey = v_BizAPIPublicKey
   AND bitActive IN(1,2)
   AND U.bitBizAPIAccessEnabled = true
   AND TIMEZONE('UTC',now()) BETWEEN dtSubStartDate
   AND dtSubEndDate LIMIT 1;
END; $$;

