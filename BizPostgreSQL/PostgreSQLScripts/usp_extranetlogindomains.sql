-- Stored procedure definition script USP_ExtranetLoginDomains for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ExtranetLoginDomains(v_Email VARCHAR(100),
    v_Password VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  DISTINCT A.numDomainID,cast((SELECT cast(vcDomainName as NUMERIC(18,0)) FROM Domain WHERE Domain.numDomainId = A.numDomainID) as VARCHAR(255)) AS vcDomainName
   FROM    AdditionalContactsInformation A
   JOIN ExtranetAccountsDtl E ON CAST(A.numContactId AS VARCHAR) = E.numContactID AND A.numDomainID = E.numDomainID
   WHERE   vcEmail = v_Email
   AND vcPassword = v_Password
   AND tintAccessAllowed = 1;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetCaseListForPortal]    Script Date: 15/10/2013 ******/













