-- Function definition script fn_GetProcurementBudgetComments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetProcurementBudgetComments(v_numProcurementId NUMERIC(9,0),v_numDomainId NUMERIC(9,0),v_numItemGroupId NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcComments  VARCHAR(50);
BEGIN
   select   coalesce(PBD.vcComments,'') INTO v_vcComments From ProcurementBudgetMaster PBM
   inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId Where PBM.numDomainID = v_numDomainId And PBD.numProcurementId = v_numProcurementId And PBD.numItemGroupId = v_numItemGroupId;    
   Return v_vcComments;
END; $$;

