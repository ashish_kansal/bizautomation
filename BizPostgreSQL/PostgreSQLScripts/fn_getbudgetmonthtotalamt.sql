-- Function definition script fn_GetBudgetMonthTotalAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetBudgetMonthTotalAmt(v_numBudgetId NUMERIC(9,0),v_stDate TIMESTAMP,v_numChartAcntId NUMERIC(9,0) DEFAULT 0,v_numDomainId NUMERIC(9,0) DEFAULT NULL)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  DECIMAL(20,5);              
   v_montotalAmt  DECIMAL(20,5);             
   v_numMonth  SMALLINT;           
   v_montotAmt  VARCHAR(10);
   v_dtFiscalStDate  TIMESTAMP;
BEGIN
   v_numMonth := 1;            
   v_montotalAmt := 0;         
   v_montotAmt := CAST(0 AS VARCHAR(10));    
   v_montotalAmt := 0;      
   While v_numMonth <= 12 LOOP
      v_monAmount := 0;
                              
  --Declare @dtFiscalEndDate as datetime                       
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_stDate,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);                      
  --Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)))                      
      select   coalesce(OBD.monAmount,0) INTO v_monAmount From OperationBudgetMaster OBM
      inner join OperationBudgetDetails OBD on OBM.numBudgetId = OBD.numBudgetID Where OBM.numDomainId = v_numDomainId And OBD.numChartAcntId = v_numChartAcntId And OBD.numBudgetID = v_numBudgetId
      and  OBD.tintMonth = EXTRACT(month FROM v_dtFiscalStDate) And OBD.intYear = EXTRACT(year FROM v_dtFiscalStDate);
      v_montotalAmt := v_montotalAmt+v_monAmount;
      v_numMonth := v_numMonth+1;
   END LOOP;          
   if v_montotalAmt = 0.00 then 
      v_montotAmt := '';  
   Else 
      v_montotAmt := SUBSTR(CAST(v_montotalAmt AS VARCHAR(10)),1,10);
   end if;        
                    
   Return v_montotAmt;
END; $$;

