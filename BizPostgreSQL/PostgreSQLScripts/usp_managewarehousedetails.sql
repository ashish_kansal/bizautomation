-- Stored procedure definition script usp_ManageWarehouseDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageWarehouseDetails(v_numWareHouseDetailID NUMERIC(18,0) ,    
 v_numCountryID NUMERIC(18,0) ,    
 v_numStateID NUMERIC(18,0),    
 v_numWareHouse NUMERIC(18,2),    
 v_numDomainId NUMERIC(18,0) ,    
 v_mode  BOOLEAN  ,  
 v_numFromPin INTEGER,  
 v_numToPin INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   if v_mode = false then
      if v_numFromPin = 0 then
         v_numFromPin := null;
      end if;
      if v_numToPin = 0 then
         v_numToPin := null;
      end if;
      v_Check := 0;
      if v_numFromPin is null then
			
         select   count(*) INTO v_Check from WareHouseDetails where numCountryID = v_numCountryID and numStateID = v_numStateID
         and numDomainID = v_numDomainId and numFromPinCode is null and numToPinCode is null;
      else
         select   count(*) INTO v_Check from WareHouseDetails where numCountryID = v_numCountryID and numStateID = v_numStateID
         and numDomainID = v_numDomainId and numFromPinCode = v_numFromPin and numToPinCode = v_numToPin;
      end if;
   end if;

   if v_mode = false then

      if v_Check = 0 then
	 
         INSERT INTO WareHouseDetails(numCountryID
			   ,numStateID
			   ,numWareHouse
			   ,numDomainID
			   ,numFromPinCode
			   ,numToPinCode)
		  VALUES(v_numCountryID
			   ,v_numStateID
			   ,v_numWareHouse
			   ,v_numDomainId
			   ,v_numFromPin
			   ,v_numToPin);
		  
         open SWV_RefCur for
         select CURRVAL('WareHouseDetails_seq');
      else
         open SWV_RefCur for
         select 0;
      end if;
   else
      DELETE FROM WareHouseDetails
      WHERE numWareHouseDetailID = v_numWareHouseDetailID and numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;


