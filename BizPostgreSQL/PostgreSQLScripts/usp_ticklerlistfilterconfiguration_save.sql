-- Stored procedure definition script USP_TicklerListFilterConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TicklerListFilterConfiguration_Save(v_numDomainID NUMERIC(18,0),
      v_numUserCntID NUMERIC(18,0),
	  v_vcSelectedEmployee VARCHAR(500),
	  v_numCriteria INTEGER,
	  v_vcActionTypes VARCHAR(500),
	  v_vcSelectedEmployeeOpp VARCHAR(500),
	  v_vcSelectedEmployeeCases VARCHAR(500),
	  v_vcAssignedStages VARCHAR(500),
	  v_numCriteriaCases INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM TicklerListFilterConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
	
      UPDATE
      TicklerListFilterConfiguration
      SET
      vcSelectedEmployee = v_vcSelectedEmployee,numCriteria = v_numCriteria,vcActionTypes = v_vcActionTypes,
      vcSelectedEmployeeOpp = v_vcSelectedEmployeeOpp,
      vcSelectedEmployeeCases = v_vcSelectedEmployeeCases,vcAssignedStages = v_vcAssignedStages,
      numCriteriaCases = v_numCriteriaCases
      WHERE
      numDomainID = v_numDomainID AND
      numUserCntID = v_numUserCntID;
   ELSE
      INSERT INTO TicklerListFilterConfiguration(numDomainID,
				numUserCntID,
				vcSelectedEmployee,
				numCriteria,
				vcActionTypes,
				vcSelectedEmployeeOpp,
				vcSelectedEmployeeCases,
				vcAssignedStages,
				numCriteriaCases)
		VALUES(v_numDomainID,
				v_numUserCntID,
				v_vcSelectedEmployee,
				v_numCriteria,
				v_vcActionTypes,
				v_vcSelectedEmployeeOpp,
				v_vcSelectedEmployeeCases,
				v_vcAssignedStages,
				v_numCriteriaCases);
   end if;
   RETURN;
END; $$;


