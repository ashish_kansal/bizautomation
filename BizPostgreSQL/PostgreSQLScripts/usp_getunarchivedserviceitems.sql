CREATE OR REPLACE FUNCTION USP_GetUnArchivedServiceItems(v_numDomainID BIGINT DEFAULT 0,
v_str VARCHAR(20) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numItemCode
		,vcItemName 
	FROM 
		Item 
		WHERE 
		numDomainID  = v_numDomainID
		AND charItemType = 'S' 
		AND IsArchieve = false;
END; $$;













