CREATE OR REPLACE FUNCTION OpportunityMaster_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
DECLARE 
	v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
	 v_tintPerformance  NUMERIC(18,0);
	 v_TempnumOppId NUMERIC(18,0);
BEGIN
	IF (TG_OP = 'DELETE') THEN
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numOppID;
		v_numUserCntID := OLD.numModifiedBy;
		v_numDomainID := OLD.numDomainID;

		DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = v_numRecordID;

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) VALUES ('Opp/Order',OLD.numOppId,'Delete',OLD.numDomainID);

		IF(SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = OLD.numDomainID) > 0 then
			SELECT DISTINCT tintPerformance INTO v_tintPerformance FROM OrganizationRatingRule WHERE numDomainID = OLD.numDomainID;

			PERFORM USP_CompanyInfo_UpdateRating(OLD.numDomainID,OLD.numDivisionID,v_tintPerformance::SMALLINT);
		end if;
	ELSIF (TG_OP = 'UPDATE') THEN
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numOppID;
		v_numUserCntID := NEW.numModifiedBy;
		v_numDomainID := NEW.numDomainID;

		IF OLD.tintActive <> NEW.tintActive THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintActive,');
		END IF;
		IF OLD.monPAmount <> NEW.monPAmount THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'monPAmount,');
		END IF;
		IF OLD.numAssignedBy <> NEW.numAssignedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedBy,');
		END IF;
		IF OLD.numAssignedTo <> NEW.numAssignedTo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedTo,');
		END IF;
		IF OLD.numCampainID <> NEW.numCampainID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCampainID,');
		END IF;
		IF OLD.bintAccountClosingDate <> NEW.bintAccountClosingDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintAccountClosingDate,');
		END IF;
		IF OLD.txtComments <> NEW.txtComments THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'txtComments,');
		END IF;
		IF OLD.lngPConclAnalysis <> NEW.lngPConclAnalysis THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'lngPConclAnalysis,');
		END IF;
		IF OLD.bintCreatedDate <> NEW.bintCreatedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintCreatedDate,');
		END IF;
		IF OLD.vcOppRefOrderNo <> NEW.vcOppRefOrderNo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcOppRefOrderNo,');
		END IF;
		IF OLD.tintOppStatus <> NEW.tintOppStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintOppStatus,');
		END IF;
		IF OLD.intpEstimatedCloseDate <> NEW.intpEstimatedCloseDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intpEstimatedCloseDate,');
		END IF;
		IF OLD.vcMarketplaceOrderID <> NEW.vcMarketplaceOrderID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcMarketplaceOrderID,');
		END IF;
		IF OLD.tintOppType <> NEW.tintOppType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintOppType,');
		END IF;
		IF OLD.numStatus <> NEW.numStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numStatus,');
		END IF;
		IF OLD.monDealAmount <> NEW.monDealAmount THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'monDealAmount,');
		END IF;
		IF OLD.numRecOwner <> NEW.numRecOwner THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numRecOwner,');
		END IF;
		IF OLD.vcPoppName <> NEW.vcPoppName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcPoppName,');
		END IF;
		IF OLD.numSalesOrPurType <> NEW.numSalesOrPurType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numSalesOrPurType,');
		END IF;
		IF OLD.tintSource <> NEW.tintSource THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintSource,');
		END IF;
		IF OLD.bitRecurred <> NEW.bitRecurred THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitRecurred,');
		END IF;
		IF OLD.numPartner <> NEW.numPartner THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numPartner,');
		END IF;
		IF OLD.numPartenerContact <> NEW.numPartenerContact THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numPartenerContact,');
		END IF;
		IF OLD.dtReleaseDate <> NEW.dtReleaseDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'dtReleaseDate,');
		END IF;
		IF OLD.numReleaseStatus <> NEW.numReleaseStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numReleaseStatus,');
		END IF;
		IF OLD.intUsedShippingCompany <> NEW.intUsedShippingCompany THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intUsedShippingCompany,');
		END IF;
		IF OLD.numShipmentMethod <> NEW.numShipmentMethod THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numShipmentMethod,');
		END IF;
		IF OLD.numShippingService <> NEW.numShippingService THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numShippingService,');
		END IF;
		IF OLD.numPercentageComplete <> NEW.numPercentageComplete THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numPercentageComplete,');
		END IF;
		IF OLD.bintClosedDate <> NEW.bintClosedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintClosedDate,');
		END IF;
		

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Opp/Order', NEW.numOppId, 'Update', NEW.numDomainId);

		IF NEW.numAssignedTo <> OLD.numAssignedTo OR NEW.numRecOwner <> OLD.numRecOwner OR NEW.tintOppType <> OLD.tintOppType OR NEW.tintOppStatus <> OLD.tintOppStatus THEN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',NEW.numDomainID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numoppid = NEW.numOppID;
		END IF;

		IF NEW.numStatus <> OLD.numStatus THEN
			PERFORM USP_EDIQueue_Insert(v_numDomainID,v_numUserCntID,NEW.numOppId,NEW.numStatus);
		END IF;

		UPDATE
			OpportunityMaster
		SET
			monDealAmount = GetDealAmount(NEW.numOppId,TIMEZONE('UTC',now()),0::NUMERIC)
		WHERE
			numOppId = NEW.numOppId;

		IF(SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID) > 0 then
			 SELECT DISTINCT tintPerformance INTO v_tintPerformance FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID;

			 PERFORM USP_CompanyInfo_UpdateRating(NEW.numDomainID,NEW.numDivisionId,v_tintPerformance::SMALLINT);
		end if;

		SELECT numOppId INTO v_TempnumOppId FROM OpportunityMaster OM WHERE  (
				(OM.bintClosedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintClosedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(OM.intPEstimatedCloseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.intPEstimatedCloseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(OM.dtReleaseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.dtReleaseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
      AND numOppId = v_numRecordID;
      IF(v_TempnumOppId IS NOT NULL) then
		
         UPDATE OpportunityMaster_TempDateFields AS OMT
         SET
         bintCreatedDate = OM.bintCreatedDate,bintClosedDate = OM.bintClosedDate,
         intPEstimatedCloseDate = OM.intPEstimatedCloseDate,dtReleaseDate = OM.dtReleaseDate
         FROM
         OpportunityMaster AS OM
         WHERE  OMT.numOppId = OM.numOppId AND(((OM.bintClosedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintClosedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(OM.intPEstimatedCloseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.intPEstimatedCloseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(OM.dtReleaseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.dtReleaseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND
         OMT.numOppId = v_numRecordID);
      ELSE
         DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = v_numRecordID;
      end if;
	ELSIF (TG_OP = 'INSERT') THEN
		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.numOppId;
		v_numUserCntID := New.numCreatedBy;
		v_numDomainID := NEW.numDomainID;

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES ('Opp/Order', NEW.numOppId, 'Insert', NEW.numDomainId);
		
        PERFORM USP_EDIQueue_Insert(NEW.numDomainId,v_numUserCntID,NEW.numOppId,NEW.numStatus);

		UPDATE
			OpportunityMaster
		SET
			monDealAmount = GetDealAmount(NEW.numOppId,TIMEZONE('UTC',now()),0::NUMERIC)
		WHERE
			numOppId = NEW.numOppId;

		IF(SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID) > 0 then
			 SELECT DISTINCT tintPerformance INTO v_tintPerformance FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID;

			 PERFORM USP_CompanyInfo_UpdateRating(NEW.numDomainID,NEW.numDivisionId,v_tintPerformance::SMALLINT);
		end if;

		IF EXISTS(SELECT numOppId FROM OpportunityMaster_TempDateFields WHERE numOppId = v_numRecordID) then
		
         SELECT numOppId INTO v_TempnumOppId FROM OpportunityMaster OM WHERE  (
					(OM.bintClosedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintClosedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(OM.intPEstimatedCloseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.intPEstimatedCloseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(OM.dtReleaseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.dtReleaseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND numOppId = v_numRecordID;
         IF(v_TempnumOppId IS NOT NULL) then
			
            UPDATE OpportunityMaster_TempDateFields AS OMT
            SET
            bintCreatedDate = OM.bintCreatedDate,bintClosedDate = OM.bintClosedDate,
            intPEstimatedCloseDate = OM.intPEstimatedCloseDate,dtReleaseDate = OM.dtReleaseDate
            FROM
            OpportunityMaster AS OM
            WHERE  OMT.numOppId = OM.numOppId AND(((OM.bintClosedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintClosedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(OM.intPEstimatedCloseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.intPEstimatedCloseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(OM.dtReleaseDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND OM.dtReleaseDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
            AND
            OMT.numOppId = v_numRecordID);
         ELSE
            DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = v_numRecordID;
         end if;
      ELSE
         INSERT INTO OpportunityMaster_TempDateFields(numOppId, numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate)
         SELECT numOppId,numDomainId, bintClosedDate, bintCreatedDate,intPEstimatedCloseDate,dtReleaseDate
         FROM OpportunityMaster
         WHERE (
					(CAST(bintClosedDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintClosedDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(bintCreatedDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintCreatedDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(intPEstimatedCloseDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(intPEstimatedCloseDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(dtReleaseDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(dtReleaseDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND numOppId = v_numRecordID;
      end if;
	END IF;

	 PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 70,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER OpportunityMaster_IUD AFTER INSERT OR UPDATE OR DELETE ON OpportunityMaster FOR EACH ROW WHEN (pg_trigger_depth() < 1) EXECUTE PROCEDURE OpportunityMaster_IUD_TrFunc();


