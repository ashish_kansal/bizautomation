-- Stored procedure definition script usp_GetPageElementsAndAvailableActions for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetPageElementsAndAvailableActions(v_numModuleID INTEGER,
	v_numPageID INTEGER   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     ModuleMaster.vcModuleName, ModuleMaster.numModuleID, PageActionElements.numPageID,
                      PageActionElements.vcElementInitiatingAction, PageActionElements.vcElementInitiatingActionDesc,
                      PageActionElements.vcElementInitiatingActionType, PageActionElements.vcChildElementInitiatingAction,
                      PageActionElements.vcChildElementInitiatingActionType, PageActionElements.vcActionName, PageMaster.vcPageDesc
   FROM         ModuleMaster INNER JOIN
   PageActionElements ON ModuleMaster.numModuleID = PageActionElements.numModuleID INNER JOIN
   PageMaster ON ModuleMaster.numModuleID = PageMaster.numModuleID AND
   PageActionElements.numPageID = PageMaster.numPageID AND
   PageActionElements.numModuleID = PageMaster.numModuleID
   WHERE     ModuleMaster.numModuleID = v_numModuleID AND PageActionElements.numPageID = v_numPageID  AND PageActionElements.bitStatus = true;
END; $$;
