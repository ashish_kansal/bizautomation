-- Stored procedure definition script usp_getCustomReportResultSetForMyReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getCustomReportResultSetForMyReport(v_numCustReportID NUMERIC,          
 v_numUserCntID NUMERIC,  
 v_numRefreshTimeInterval NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sCustomReportQuickQuery  VARCHAR(4000);      
   v_vcCustomReportQueryMiddle  VARCHAR(4000);          
   v_sCustomReportQuickQueryTrailer  VARCHAR(4000);          
   v_vcAggregateReportColumns  VARCHAR(1000);          
   v_vcReportTypeconfig  VARCHAR(500);          
   v_bitRecordCountSummation  BOOLEAN;                         
   v_ReportType  NUMERIC;             
   v_CustomFields  BOOLEAN;
   v_iDoc  INTEGER;
   SWV_ExecDyn  VARCHAR(5000);
BEGIN
   select   SUBSTR(vcReportQuickQuery,1,4000), SUBSTR(vcReportQuickQuery,4001,4000), SUBSTR(vcReportQuickQuery,8001,OCTET_LENGTH(vcReportQuickQuery)), vcReportSummationOrder, vcReportTypeconfig INTO v_sCustomReportQuickQuery,v_vcCustomReportQueryMiddle,v_sCustomReportQuickQueryTrailer,
   v_vcAggregateReportColumns,v_vcReportTypeconfig FROM CustRptConfigMaster WHERE numCustReportID = v_numCustReportID
   AND numCreatedBy = v_numUserCntID;       
    
   SELECT * INTO v_iDoc FROM SWF_Xml_PrepareDocument(v_vcReportTypeconfig);    
   select   ReportType, CustomFields INTO v_ReportType,v_CustomFields FROM SWF_OpenXml(v_iDoc,'/ReportTypeConfig','ReportType |CustomFields') SWA_OpenXml(ReportType NUMERIC,CustomFields BOOLEAN);    
 --PRINT 'ReportType: ' + Cast(@ReportType As NVarchar)    
 --PRINT 'CustomFields: ' + Cast(@CustomFields As NVarchar)    
    
   PERFORM SWF_Xml_RemoveDocument(v_iDoc);    
        
   IF v_ReportType  = 2 then
      SWV_ExecDyn := 'usp_OppItemCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   ELSEIF v_ReportType  = 1
   then
 
      SWV_ExecDyn := 'usp_OrganizationContactsCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   end if;          
          
   EXECUTE coalesce(v_sCustomReportQuickQuery,'') || coalesce(v_vcCustomReportQueryMiddle,'') || coalesce(v_sCustomReportQuickQueryTrailer,'');      
   SWV_ExecDyn := 'SELECT ''' ||  coalesce(v_vcAggregateReportColumns,'') || ''' AS AggregateReportColumns';
   OPEN SWV_RefCur FOR EXECUTE SWV_ExecDyn;       
      
   open SWV_RefCur2 for
   SELECT bitRecordCountSummation AS RecordCountSummation FROM CustRptConfigLayoutAndStdFilters
   WHERE numCustReportID = v_numCustReportID;
   RETURN;
END; $$;


