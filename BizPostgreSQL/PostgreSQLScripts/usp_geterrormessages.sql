-- FUNCTION: public.usp_geterrormessages(numeric, numeric, numeric, integer, refcursor)

-- DROP FUNCTION public.usp_geterrormessages(numeric, numeric, numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_geterrormessages(
	v_numdomainid numeric,
	v_numsiteid numeric,
	v_numcultureid numeric,
	v_tintapplication integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:46:46 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT cast(EM.numErrorID as VARCHAR(255)) ,
             cast(EM.vcErrorCode as VARCHAR(255)) ,
             cast(coalesce((SELECT cast(numErrorDetailId as VARCHAR(255)) FROM ErrorDetail WHERE numDomainID = v_numDomainId AND numSiteId = v_numSiteId AND numCultureId = v_numCultureId AND numErrorID = EM.numErrorID),'0') as VARCHAR(255)) AS numErrorDetailId,
             cast(CASE WHEN coalesce((SELECT cast(vcErrorDesc as VARCHAR(255)) FROM ErrorDetail WHERE numDomainID = v_numDomainId AND numSiteId = v_numSiteId AND numCultureId = v_numCultureId AND numErrorID = EM.numErrorID),vcErrorDesc) <> ''
   THEN
      coalesce((SELECT cast(vcErrorDesc as VARCHAR(255)) FROM ErrorDetail WHERE numDomainID = v_numDomainId AND numSiteId = v_numSiteId AND numCultureId = v_numCultureId AND numErrorID = EM.numErrorID),vcErrorDesc)
   ELSE
      vcErrorDesc
   END as VARCHAR(255))  AS vcErrorDesc ,
             cast(EM.vcErrorDesc as VARCHAR(255)) AS DescParent ,
             cast(coalesce((SELECT cast(vcErrorDesc as VARCHAR(255)) FROM ErrorDetail WHERE numDomainID = v_numDomainId AND numSiteId = v_numSiteId AND numCultureId = v_numCultureId AND numErrorID = EM.numErrorID),'') as VARCHAR(255))  AS DescChild
   FROM   ErrorMaster EM
   WHERE tintApplication = v_tintApplication
   AND    EM.tintApplication = v_tintApplication;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_geterrormessages(numeric, numeric, numeric, integer, refcursor)
    OWNER TO postgres;
