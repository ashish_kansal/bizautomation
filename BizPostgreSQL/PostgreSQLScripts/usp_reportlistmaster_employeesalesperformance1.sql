-- Stored procedure definition script USP_ReportListMaster_EmployeeSalesPerformance1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportListMaster_EmployeeSalesPerformance1(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,v_vcFilterValue TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtCreatedDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_TotalLeads  DOUBLE PRECISION;
   v_LeadsToProspect  DOUBLE PRECISION;
   v_LeadsToAccount  DOUBLE PRECISION;
   v_TotalProspects  DOUBLE PRECISION;
   v_ProspectToAccount  DOUBLE PRECISION;

   v_TempAssignedTo  NUMERIC(18,0);
BEGIN
   IF v_vcTimeLine = 'Last12Months' then
	
      v_dtCreatedDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last6Months' then
	
      v_dtCreatedDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last3Months' then
	
      v_dtCreatedDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last30Days' then
     v_dtCreatedDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -30), v_dtEndDate));
   end if;
   IF v_vcTimeLine = 'Last7Days' then
	
     v_dtCreatedDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -7), v_dtEndDate));
   end if;

   DROP TABLE IF EXISTS tt_TEMPFINAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFINAL
   (
      vcUsername VARCHAR(300),
      LeadsToAccount DOUBLE PRECISION,
      LeadsToProspect DOUBLE PRECISION,
      ProspectToAccount DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPEmployees_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPEMPLOYEES CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEMPLOYEES
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numAssignedTo NUMERIC(18,0)
   );

   INSERT INTO tt_TEMPEMPLOYEES(numAssignedTo)
   SELECT DISTINCT
   numAssignedTo
   FROM
   DivisionMaster
   WHERE
   numDomainID = v_numDomainID
   AND bintCreatedDate >= v_dtCreatedDate;


   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numDivisionID NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      FirstRecordID NUMERIC(18,0),
      LastRecordID NUMERIC(18,0),
      tintFirstCRMType SMALLINT,
      tintLastCRMType SMALLINT
   );

   INSERT INTO tt_TEMP(numDivisionID
		,numAssignedTo
		,FirstRecordID
		,LastRecordID)
   SELECT
   DivisionMasterPromotionHistory.numDivisionID
		,DM.numAssignedTo
		,MIN(ID)
		,MAX(ID)
   FROM
   DivisionMasterPromotionHistory
   INNER JOIN
   DivisionMaster DM
   ON
   DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
   WHERE
   DivisionMasterPromotionHistory.numDomainID = v_numDomainID
   AND DM.numDomainID = v_numDomainID
   AND DM.bintCreatedDate >= v_dtCreatedDate
   GROUP BY
   DivisionMasterPromotionHistory.numDivisionID,DM.numAssignedTo;

   UPDATE
   tt_TEMP T1
   SET
   tintFirstCRMType = DMPH1.tintPreviousCRMType,tintLastCRMType = DMPH2.tintNewCRMType
   FROM
   DivisionMasterPromotionHistory DMPH1, DivisionMasterPromotionHistory DMPH2 WHERE T1.FirstRecordID = DMPH1.ID AND T1.LastRecordID = DMPH2.ID;


   select   COUNT(*) INTO v_iCount FROM tt_TEMPEMPLOYEES;

   WHILE v_i <= v_iCount LOOP
      select   numAssignedTo INTO v_TempAssignedTo FROM tt_TEMPEMPLOYEES WHERE ID = v_i;
      v_TotalLeads := coalesce((SELECT
      COUNT(*)
      FROM
      DivisionMaster
      WHERE
      numDomainID = v_numDomainID
      AND numAssignedTo = v_TempAssignedTo
      AND bintCreatedDate >= v_dtCreatedDate
      AND tintCRMType = 0
      AND numDivisionID NOT IN(SELECT numDivisionID FROM tt_TEMP)),0)+coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE tintFirstCRMType = 0 AND numAssignedTo = v_TempAssignedTo),0);
      v_LeadsToProspect := coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE tintFirstCRMType = 0 ANd tintLastCRMType = 1 AND numAssignedTo = v_TempAssignedTo),0);
      v_LeadsToAccount := coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE tintFirstCRMType = 0 ANd tintLastCRMType = 2 AND numAssignedTo = v_TempAssignedTo),0);
      v_TotalProspects := coalesce((SELECT
      COUNT(*)
      FROM
      DivisionMaster
      WHERE
      numDomainID = v_numDomainID
      AND numAssignedTo = v_TempAssignedTo
      AND bintCreatedDate >= v_dtCreatedDate
      AND tintCRMType = 1
      AND numDivisionID NOT IN(SELECT numDivisionID FROM tt_TEMP)),0)+coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE tintFirstCRMType = 1 AND numAssignedTo = v_TempAssignedTo),0);
      v_ProspectToAccount := coalesce((SELECT COUNT(*) FROM tt_TEMP WHERE tintFirstCRMType = 1 ANd tintLastCRMType = 2 AND numAssignedTo = v_TempAssignedTo),0);
      INSERT INTO tt_TEMPFINAL(vcUsername
			,LeadsToAccount
			,LeadsToProspect
			,ProspectToAccount)
      SELECT
      vcUserName
			,(v_LeadsToAccount*100)/(CASE WHEN coalesce(v_TotalLeads,0) = 0 THEN 1 ELSE v_TotalLeads END)
			,(v_LeadsToProspect*100)/(CASE WHEN coalesce(v_TotalLeads,0) = 0 THEN 1 ELSE v_TotalLeads END)
			,(v_ProspectToAccount*100)/(CASE WHEN coalesce(v_TotalProspects,0) = 0 THEN 1 ELSE v_TotalProspects END)
      FROM
      UserMaster
      JOIN
      UserTeams UT
      ON
      UT.numDomainID = v_numDomainID
      AND UserMaster.numUserDetailId = UT.numUserCntID
      WHERE
      UserMaster.numDomainID = v_numDomainID
      AND UserMaster.numUserDetailId = v_TempAssignedTo
      AND 1 =(CASE WHEN v_vcFilterBy = 4  THEN(CASE WHEN UT.numUserCntID IN(SELECT Id FROM SplitIDs((v_vcFilterValue),',')) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 THEN(CASE WHEN UT.numTeam IN(SELECT Id FROM SplitIDs((v_vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
      END);
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for
   SELECT vcUsername,LeadsToProspect FROM tt_TEMPFINAL WHERE coalesce(LeadsToProspect,0.0) > 0;
   open SWV_RefCur2 for
   SELECT vcUsername,LeadsToAccount FROM tt_TEMPFINAL WHERE coalesce(LeadsToAccount,0.0) > 0;
   open SWV_RefCur3 for
   SELECT vcUsername,ProspectToAccount FROM tt_TEMPFINAL WHERE coalesce(ProspectToAccount,0.0) > 0;
   RETURN;
END; $$;


