-- Stored procedure definition script USP_GET_TOP_IMPORT_FILE_ID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_TOP_IMPORT_FILE_ID(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT *
		,fn_GetContactName(IFM.numUserContactID) AS CreatedBy
   FROM
   Import_File_Master IFM
   INNER JOIN
   dynamicFormMaster DM
   ON
   DM.numFormID = IFM.numMasterID
   WHERE
   coalesce(IFM.tintStatus,0) = 0 ORDER BY intImportFileID LIMIT 1;
END; $$;


/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719












