CREATE OR REPLACE FUNCTION fn_GetContactCreationDate4SurveyRespondent(v_vcEmail VARCHAR(250))
RETURNS VARCHAR(20) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtRetVal  VARCHAR(20);
BEGIN
   select  SUBSTR(cast(TO_CHAR(bintCreatedDate,'mm/dd/yyyy') AS VARCHAR(10)),1,10) INTO v_dtRetVal FROM AdditionalContactsInformation WHERE vcEmail = v_vcEmail LIMIT 1;    
    
   RETURN v_dtRetVal;
END; $$;

