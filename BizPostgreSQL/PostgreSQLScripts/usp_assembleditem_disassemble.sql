CREATE OR REPLACE FUNCTION USP_AssembledItem_Disassemble(
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_ID NUMERIC(18,0),
	v_numQty NUMERIC(18,0),
	v_tintType SMALLINT -- 1 - Assembled, 2 - Work Order
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemCode  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_monAssemblyAverageCost  DECIMAL(20,5);
   v_numRemainingQty  NUMERIC(18,0);

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_TEMPItemCode  NUMERIC(18,0);
   v_TEMPbitAssembly  BOOLEAN;
   v_TEMPWarehouseItemID  NUMERIC(18,0);
   v_TEMPCurrentAverageCost  DECIMAL(20,5);
   v_TEMPBuildAverageCost  DECIMAL(20,5);
   v_TEMPToalOnHand  DOUBLE PRECISION;
   v_TEMPQty  DOUBLE PRECISION;
   v_Description  VARCHAR(1000);

	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   -- BEGIN TRANSACTION
BEGIN
      CREATE TEMP SEQUENCE tt_TMEP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TMEP CASCADE;
   CREATE TEMPORARY TABLE tt_TMEP
   (
      RowNo INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numItemCode NUMERIC(18,0),
      bitAssembly BOOLEAN,
      numWarehouseItemID NUMERIC(18,0),
      numQty DOUBLE PRECISION,
      monAverageCost DECIMAL(20,5)
   );
   IF v_tintType = 1 then
	
      select   numItemCode, numWarehouseItemID, monAverageCost, (numAssembledQty -coalesce(numDisassembledQty,0)) INTO v_numItemCode,v_numWarehouseItemID,v_monAssemblyAverageCost,v_numRemainingQty FROM AssembledItem WHERE ID = v_ID;
      IF coalesce((SELECT numOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseItemID = v_numWarehouseItemID),0) >= v_numQty AND  v_numRemainingQty >= v_numQty then
		
         INSERT INTO tt_TMEP(numItemCode,
				bitAssembly,
				numWarehouseItemID,
				numQty,
				monAverageCost)
         SELECT
         AIC.numItemCode,
				bitAssembly,
				AIC.numWarehouseItemID,
				AIC.fltQtyRequiredForSingleBuild*v_numQty,
				AIC.monAverageCost
         FROM
         AssembledItemChilds AIC
         INNER JOIN
         Item
         ON
         AIC.numItemCode = Item.numItemCode
         WHERE
         numAssembledItemID = v_ID;
         select   COUNT(*) INTO v_COUNT FROM tt_TMEP;
         WHILE v_i <= v_COUNT LOOP
            select   coalesce(numItemCode,0), coalesce(bitAssembly,false), coalesce(numWarehouseItemID,0), coalesce(numQty,0), coalesce(monAverageCost,0) INTO v_TEMPItemCode,v_TEMPbitAssembly,v_TEMPWarehouseItemID,v_TEMPQty,v_TEMPBuildAverageCost FROM
            tt_TMEP WHERE
            RowNo = v_i;
            select   SUM(numOnHand), coalesce(Item.monAverageCost,0) INTO v_TEMPToalOnHand,v_TEMPCurrentAverageCost FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode WHERE numItemID = v_TEMPItemCode GROUP BY Item.numItemCode,Item.monAverageCost;

				-- UPDATE AVERAGE COST
            UPDATE
            Item
            SET
            monAverageCost = CAST(((v_TEMPToalOnHand*v_TEMPCurrentAverageCost)+(v_TEMPQty*v_TEMPBuildAverageCost))/(v_TEMPToalOnHand+v_TEMPQty) AS DECIMAL(20,5))
            WHERE
            numItemCode = v_TEMPItemCode;


				-- INCREASE ON HAND QTY
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0)+v_TEMPQty,dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_TEMPWarehouseItemID;

				--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
				 --  numeric(9, 0)
				 --  tinyint,
				 --  varchar(100)
            v_Description := CONCAT('Assembly Item:',v_numItemCode,' is disassembled ','(Qty:',v_TEMPQty,')');

            PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_TEMPWarehouseItemID,v_numReferenceID := v_TEMPItemCode,
            v_tintRefType := 1::SMALLINT,v_vcDescription := v_Description,
            v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
            v_i := v_i::bigint+1;
         END LOOP;
         select   SUM(numOnHand), coalesce(Item.monAverageCost,0) INTO v_TEMPToalOnHand,v_TEMPCurrentAverageCost FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode WHERE numItemID = v_numItemCode GROUP BY Item.numItemCode,Item.monAverageCost;
         IF(v_TEMPToalOnHand -v_numQty) > 0 then
			
				-- UPDATE AVERAGE COST
            UPDATE
            Item
            SET
            monAverageCost = CAST(((v_TEMPToalOnHand*v_TEMPCurrentAverageCost) -(v_numQty*v_monAssemblyAverageCost))/(v_TEMPToalOnHand -v_numQty) AS DECIMAL(20,5))
            WHERE
            numItemCode = v_numItemCode;
         end if;

			-- DECREASE ON HAND QTY OF ASSEMBLY
         UPDATE
         WareHouseItems
         SET
         numOnHand = coalesce(numOnHand,0) -v_numQty,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWarehouseItemID;

			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
         v_Description := CONCAT('Disassembled ','(Qty:',v_numQty,')');
         
		 PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_Description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
      ELSE
         RAISE EXCEPTION 'INSUFFICIENT_ONHAND_QTY';
         RETURN;
      end if;
   ELSEIF v_tintType = 2
   then
	
      select   numItemCode, numWarehouseItemID, monAverageCost INTO v_numItemCode,v_numWarehouseItemID,v_monAssemblyAverageCost FROM WorkOrder WHERE numWOId = v_ID;
      IF coalesce((SELECT numOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseItemID = v_numWarehouseItemID),0) >= v_numQty then
		
         INSERT INTO tt_TMEP(numItemCode,
				bitAssembly,
				numWarehouseItemID,
				numQty,
				monAverageCost)
         SELECT
         WOD.numChildItemID,
				bitAssembly,
				WOD.numWarehouseItemID,
				WOD.numQtyItemsReq_Orig*v_numQty,
				WOD.monAverageCost
         FROM
         WorkOrderDetails WOD
         INNER JOIN
         Item
         ON
         WOD.numChildItemID = Item.numItemCode
         WHERE
         WOD.numWOId = v_ID;
         select   COUNT(*) INTO v_COUNT FROM tt_TMEP;
         WHILE v_i <= v_COUNT LOOP
            select   coalesce(numItemCode,0), coalesce(bitAssembly,false), coalesce(numWarehouseItemID,0), coalesce(numQty,0), coalesce(monAverageCost,0) INTO v_TEMPItemCode,v_TEMPbitAssembly,v_TEMPWarehouseItemID,v_TEMPQty,v_TEMPBuildAverageCost FROM
            tt_TMEP WHERE
            RowNo = v_i;
            select   SUM(numOnHand), coalesce(Item.monAverageCost,0) INTO v_TEMPToalOnHand,v_TEMPCurrentAverageCost FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode WHERE numItemID = v_TEMPItemCode GROUP BY Item.numItemCode,Item.monAverageCost;

				-- UPDATE AVERAGE COST
            UPDATE
            Item
            SET
            monAverageCost = CAST(((v_TEMPToalOnHand*v_TEMPCurrentAverageCost)+(v_TEMPQty*v_TEMPBuildAverageCost))/(v_TEMPToalOnHand+v_TEMPQty) AS DECIMAL(20,5))
            WHERE
            numItemCode = v_TEMPItemCode;


				-- INCREASE ON HAND QTY
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0)+v_TEMPQty,dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_TEMPWarehouseItemID;

				--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
				 --  numeric(9, 0)
				 --  tinyint,
				 --  varchar(100)
            v_Description := CONCAT('Work Order of Assembly Item:',v_numItemCode,' is disassembled ','(Qty:',
            v_TEMPQty,')');
            
			PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_TEMPWarehouseItemID,v_numReferenceID := v_TEMPItemCode,
            v_tintRefType := 1::SMALLINT,v_vcDescription := v_Description,
            v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
            v_i := v_i::bigint+1;
         END LOOP;
         select   SUM(numOnHand), coalesce(Item.monAverageCost,0) INTO v_TEMPToalOnHand,v_TEMPCurrentAverageCost FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode WHERE numItemID = v_numItemCode GROUP BY Item.numItemCode,Item.monAverageCost;
         IF(v_TEMPToalOnHand -v_numQty) > 0 then
			
				-- UPDATE AVERAGE COST
            UPDATE
            Item
            SET
            monAverageCost = CAST(((v_TEMPToalOnHand*v_TEMPCurrentAverageCost) -(v_numQty*v_monAssemblyAverageCost))/(v_TEMPToalOnHand -v_numQty) AS DECIMAL(20,5))
            WHERE
            numItemCode = v_numItemCode;
         end if;

			-- DECREASE ON HAND QTY OF ASSEMBLY
         UPDATE
         WareHouseItems
         SET
         numOnHand = coalesce(numOnHand,0) -v_numQty,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWarehouseItemID;

			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
         v_Description := CONCAT('Work Order Disassembled ','(Qty:',v_numQty,')');
         
		 PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_Description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
      ELSE
         RAISE EXCEPTION 'INSUFFICIENT_ONHAND_QTY';
         RETURN;
      end if;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK TRANSACTION
		GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;  


