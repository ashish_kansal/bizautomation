-- Function definition script GetOppItemReleaseDates for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppItemReleaseDates(v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_bitReturnHtml BOOLEAN)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT DEFAULT '';
BEGIN
   IF EXISTS(SELECT * FROM OpportunityItemsReleaseDates WHERE	numOppId = v_numOppID AND numOppItemID = v_numOppItemID) then
	
      SELECT  FormatedDateFromDate(dtReleaseDate,v_numDomainID) INTO v_vcValue FROM OpportunityItemsReleaseDates WHERE	numOppId = v_numOppID AND numOppItemID = v_numOppItemID     LIMIT 1;
   ELSE
      v_vcValue := 'Not Available';
   end if;

   RETURN v_vcValue;
END; $$;

--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or .....

