CREATE OR REPLACE FUNCTION USP_ValidPromotionCode(v_vcPromoCode VARCHAR(100) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numSiteID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_validFrom  TIMESTAMP;
   v_validTo  TIMESTAMP;
   v_bitAppliesToSite  BOOLEAN;
   v_bitRequireCouponCode  BOOLEAN;
   v_tintUsageLimit  INTEGER;
   v_tintOfferTriggerValueType  INTEGER; -- Based Offer On 1-Quantiy 2-Amount 
   v_fltOfferTriggerValue  INTEGER;-- Based Offer Amount/Quanity
   v_tintOfferBasedOn  INTEGER;-- Based On Classification 1: individual item 2:item classification
   v_fltDiscountValue  INTEGER;
BEGIN
   IF EXISTS(SELECT numProId FROM PromotionOffer WHERE txtCouponCode = v_vcPromoCode AND bitEnabled = true) then
      NULL;
   end if;
   RETURN;
END; $$;


