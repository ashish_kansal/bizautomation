-- Stored procedure definition script USP_ShippingService_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShippingService_Save(v_numShippingServiceID NUMERIC(18,0),   
	v_numDomainID NUMERIC(9,0),   
	v_numShipViaID NUMERIC(18,0),
	v_vcShippingService VARCHAR(300),
	v_vcAbbreviation VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numShippingServiceID,0) = 0 then
	
      INSERT INTO ShippingService(numDomainID,numShipViaID,vcShipmentService)
		VALUES(v_numDomainID,v_numShipViaID,v_vcShippingService);
		
      IF LENGTH(coalesce(v_vcAbbreviation,'')) > 0 then
		
         INSERT INTO ShippingServiceAbbreviations(numDomainID,numShippingServiceID,vcAbbreviation)
			VALUES(v_numDomainID,CURRVAL('Table_seq'),v_vcAbbreviation);
      end if;
   ELSE
      UPDATE
      ShippingService
      SET
      vcShipmentService = v_vcShippingService
      WHERE
      numShippingServiceID = v_numShippingServiceID
      AND numDomainID = v_numDomainID;
      IF LENGTH(coalesce(v_vcAbbreviation,'')) > 0 then
		
         IF EXISTS(SELECT ID FROM ShippingServiceAbbreviations WHERE numDomainID = v_numDomainID AND numShippingServiceID = v_numShippingServiceID) then
			
            UPDATE
            ShippingServiceAbbreviations
            SET
            vcAbbreviation = v_vcAbbreviation
            WHERE
            numDomainID = v_numDomainID
            AND numShippingServiceID = v_numShippingServiceID;
         ELSE
            INSERT INTO ShippingServiceAbbreviations(numDomainID,numShippingServiceID,vcAbbreviation)
				VALUES(v_numDomainID,v_numShippingServiceID,v_vcAbbreviation);
         end if;
      ELSE
         DELETE FROM ShippingServiceAbbreviations WHERE numDomainID = v_numDomainID AND numShippingServiceID = v_numShippingServiceID;
      end if;
   end if;
   RETURN;
END; $$;


