-- Stored procedure definition script USP_GetChartAcountIdForAuthorizativeBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartAcountIdForAuthorizativeBizDocs(v_numDomainId NUMERIC(9,0) DEFAULT 0,
 v_numAccountTypeId NUMERIC(9,0) DEFAULT 0,
 v_chBizDocItems CHAR(2) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numAccountId From Chart_Of_Accounts Where numAcntTypeId = v_numAccountTypeId And chBizDocItems = v_chBizDocItems And numDomainId = v_numDomainId;
END; $$;












