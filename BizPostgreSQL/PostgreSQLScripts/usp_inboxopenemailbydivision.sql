-- Stored procedure definition script USP_InboxOpenEmailByDivision for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InboxOpenEmailByDivision(v_numDomainID NUMERIC(9,0) DEFAULT 0,                  
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,
 v_bDivision SMALLINT DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
		EH.numEmailHstrID,
        FormatedDateTimeFromDate(EH.dtReceivedOn,EH.numDomainID) AS dtReceivedOn,
        vcSubject  AS Subject,
        coalesce(vcSize,'0') AS vcSize,
        vcFrom || ',' || vcTo AS "From",
        CASE WHEN LENGTH(CAST(vcBodyText AS VARCHAR(1000))) > 150
   THEN SUBSTR(vcBodyText,0,150) || '...'
   ELSE vcBodyText
   END AS vcBody
   FROM    EmailHistory EH join
   EmailMaster EM ON EM.numEmailId = EH.numEMailId
   WHERE   EH.numDomainID = v_numDomainID
   AND EH.bitIsRead = false
   AND EH.tintType = 1
   AND EM.numContactID IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND
      1 =(Case when v_bDivision = 1 then 1 else Case when bitPrimaryContact = true then 1 else 0 end end));
END; $$;
         
