-- Stored procedure definition script usp_getSalesProcess for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getSalesProcess(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
--
   AS $$
BEGIN
	open SWV_RefCur for 
	Select 
		Slp_Id as "Slp_Id"
		,Slp_Name as "Slp_Name" 
	from 
		Sales_process_List_Master 
	order by 
		Slp_Name;
END; $$;












