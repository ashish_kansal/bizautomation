-- Stored procedure definition script USP_GetItemVendorDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemVendorDetails(v_numVendorID BIGINT,
	v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(coalesce(V.numVendorTcode,0) as VARCHAR(255)) AS numVendorTcode,
		cast(coalesce(V.numVendorID,0) as VARCHAR(255)) AS numVendorID,
		cast(coalesce(V.vcPartNo,'') as VARCHAR(255)) AS vcPartNo,
		cast(coalesce(V.monCost,0) as VARCHAR(255)) AS monCost,
		cast(coalesce(V.numItemCode,0) as VARCHAR(255)) AS numItemCode,
		cast(coalesce(V.intMinQty,0) as VARCHAR(255)) AS intMinQty
   FROM
   Vendor V
   WHERE
   V.numVendorID = v_numVendorID
   AND V.numDomainID = v_numDomainID
   AND V.numItemCode = v_numItemCode;
END; $$;











