DROP FUNCTION IF EXISTS USP_GetShippingReport;

CREATE OR REPLACE FUNCTION USP_GetShippingReport(v_numDomainID NUMERIC(9,0),
    v_numOppBizDocId NUMERIC(9,0),
    v_ShippingReportId NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LENGTH(BDI.vcItemDesc) > 100
   THEN CAST(BDI.vcItemDesc AS VARCHAR(100)) || '..'
   ELSE BDI.vcItemDesc
   END AS vcItemDesc,
                FormatedDateFromDate(SRI.dtDeliveryDate,v_numDomainID) AS dtDeliveryDate,
                SRI.tintServiceType,
                coalesce(I.fltWeight,0) AS fltTotalWeight,
                coalesce(I.fltHeight,0) AS fltHeight,
                coalesce(I.fltLength,0) AS fltLength,
                coalesce(I.fltWidth,0) AS fltWidth,
                coalesce(SRI.monShippingRate,(SELECT monTotAmount FROM OpportunityItems
      WHERE SR.numOppID = OpportunityItems.numOppId
      AND numItemCode =(SELECT numShippingServiceItemID FROM Domain WHERE numDomainId = v_numDomainID))) AS monShippingRate,
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.vcValue3,
                SR.vcValue4,
                CASE WHEN SWF_IsNumeric(SR.vcFromState) = true
   THEN(SELECT  vcStateCode
         FROM    ShippingStateMaster
         WHERE   vcStateName ilike (SELECT 
            vcState
            FROM    State
            WHERE   CAST(numStateID AS VARCHAR) = SR.vcFromState LIMIT 1)
         AND numShipCompany = SR.numShippingCompany)
   ELSE ''
   END AS vcFromState,
                CASE WHEN SWF_IsNumeric(SR.vcFromCountry) = true
   THEN(SELECT  vcCountryCode
         FROM    ShippingCountryMaster
         WHERE   vcCountryName ilike (SELECT 
            vcData
            FROM      Listdetails
            WHERE     CAST(numListItemID AS VARCHAR) = SR.vcFromCountry LIMIT 1)
         AND numShipCompany = SR.numShippingCompany)
   ELSE ''
   END AS vcFromCountry,
                CASE WHEN SWF_IsNumeric(SR.vcToState) = true
   THEN(SELECT  vcStateCode
         FROM    ShippingStateMaster
         WHERE   vcStateName ilike (SELECT 
            vcState
            FROM    State
            WHERE   numStateID = CAST(SR.vcToState AS NUMERIC) LIMIT 1)
         AND numShipCompany = SR.numShippingCompany)
   ELSE ''
   END AS vcToState,
                CASE WHEN SWF_IsNumeric(SR.vcToCountry) = true
   THEN(SELECT  vcCountryCode
         FROM    ShippingCountryMaster
         WHERE   vcCountryName  ilike (SELECT 
            vcData
            FROM      Listdetails
            WHERE     numListItemID = CAST(SR.vcToCountry AS NUMERIC) LIMIT 1)
         AND numShipCompany = SR.numShippingCompany)
   ELSE ''
   END AS vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.numShippingCompany,
                SB.numBoxID,
                SB.vcBoxName,
                BDI.numOppBizDocItemID,
                coalesce(SR.tintPayorType,0) AS tintPayorType,
                coalesce(SR.vcPayorAccountNo,'') AS vcPayorAccountNo,
				SR.vcPayerStreet,
				SR.vcPayerCity,
                coalesce(SR.numPayorCountry,0) AS numPayorCountry,
				coalesce(SR.numPayerState,0) AS numPayerState,
                (SELECT    vcCountryCode
      FROM      ShippingCountryMaster
      WHERE     vcCountryName ilike (SELECT 
         vcData
         FROM      Listdetails
         WHERE     numListItemID = SR.numPayorCountry LIMIT 1)
      AND numShipCompany = SR.numShippingCompany) AS vcPayorCountryCode,
				(SELECT  vcStateCode
      FROM    ShippingStateMaster
      WHERE   vcStateName ilike (SELECT 
         vcState
         FROM    State
         WHERE   numStateID = SR.numPayerState LIMIT 1)
      AND numShipCompany = SR.numShippingCompany) AS vcPayorStateCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                coalesce(SR.vcFromCountry,'0') AS numFromCountry,
                coalesce(SR.vcFromState,'0') AS numFromState,
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                coalesce(SR.vcToCountry,'0') AS numToCountry,
                coalesce(SR.vcToState,'0') AS numToState,
                SRI.intBoxQty,
                coalesce(SB.numServiceTypeID,0) AS numServiceTypeID,
                coalesce(SR.vcFromCompany,'') AS vcFromCompany,
                coalesce(SR.vcFromName,'') AS vcFromName,
                coalesce(SR.vcFromPhone,'') AS vcFromPhone,
                coalesce(SR.vcToCompany,'') AS vcToCompany,
                coalesce(SR.vcToName,'') AS vcToName,
                coalesce(SR.vcToPhone,'') AS vcToPhone,
                coalesce(SB.fltDimensionalWeight,0) AS fltDimensionalWeight,
                fn_GetContactName(SR.numCreatedBy) || ' : '
   || CAST(SR.dtCreateDate AS VARCHAR(20)) AS CreatedDetails,
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
   WHEN SR.numShippingCompany = 88 THEN 'UPS'
   WHEN SR.numShippingCompany = 90 THEN 'USPS'
   ELSE 'Other'
   END AS vcShipCompany,
                CAST(ROUND(CAST((coalesce(SRI.fltTotalWeight,0)*intBoxQty) AS NUMERIC), 
   2) AS DECIMAL(9,2)) AS fltTotalRegularWeight,
                CAST(ROUND(CAST(coalesce(fltDimensionalWeight,0) AS NUMERIC),2) AS DECIMAL(9,2)) AS fltTotalDimensionalWeight,
                coalesce(monTotAmount,0) AS monTotAmount,
                coalesce(bitFromResidential,false) AS bitFromResidential,
                coalesce(bitToResidential,false) AS bitToResidential,
                coalesce(IsCOD,false) AS IsCOD,
				coalesce(IsDryIce,false) AS IsDryIce,
				coalesce(IsHoldSaturday,false) AS IsHoldSaturday,
				coalesce(IsHomeDelivery,false) AS IsHomeDelivery,
				coalesce(IsInsideDelevery,false) AS IsInsideDelevery,
				coalesce(IsInsidePickup,false) AS IsInsidePickup,
				coalesce(IsReturnShipment,false) AS IsReturnShipment,
				coalesce(IsSaturdayDelivery,false) AS IsSaturdayDelivery,
				coalesce(IsSaturdayPickup,false) AS IsSaturdayPickup,
				coalesce(IsAdditionalHandling,false) AS IsAdditionalHandling,
				coalesce(IsLargePackage,false) AS IsLargePackage,
				coalesce(vcCODType,'0') AS vcCODType,
				SR.numOppID,
				coalesce(numCODAmount,0) AS numCODAmount,
				coalesce(SR.numTotalInsuredValue,0) AS numTotalInsuredValue,
				coalesce(SR.numTotalCustomsValue,0) AS numTotalCustomsValue,
				coalesce(tintSignatureType,0) AS tintSignatureType,
				OpportunityMaster.numShipFromWarehouse,
				coalesce(vcDeliveryConfirmation,'0') AS vcDeliveryConfirmation,
				coalesce(SR.vcDescription,'') AS vcDescription
   FROM    ShippingReport AS SR
   INNER JOIN OpportunityMaster ON SR.numOppID = OpportunityMaster.numOppId
   INNER JOIN ShippingBox SB ON SB.numShippingReportID = SR.numShippingReportID
   INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportID = SRI.numShippingReportId
   AND SRI.numBoxID = SB.numBoxID
   INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
   AND SR.numOppBizDocId = BDI.numOppBizDocID
   INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
   WHERE   SR.numOppBizDocId = v_numOppBizDocId
   AND SR.numDomainID = v_numDomainID
   AND SR.numShippingReportID = v_ShippingReportId;
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
   open SWV_RefCur2 for
   SELECT  vcBizDocID
   FROM    OpportunityBizDocs
   WHERE   numOppBizDocsId = v_numOppBizDocId;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetUsageReport]    Script Date: 07/26/2008 16:18:46 ******/



