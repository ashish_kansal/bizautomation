-- Stored procedure definition script USP_TransactionHistory_GetAuthrizedTrasaction for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TransactionHistory_GetAuthrizedTrasaction(v_numDomainID NUMERIC(18,0),    
	v_numDivisionID NUMERIC(18,0),
	v_monAmount DECIMAL(20,5),
	v_tintMode SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM
   TransactionHistory
   WHERE
   numDomainID = v_numDomainID
   AND numDivisionID = v_numDivisionID
   AND 1 =(CASE WHEN v_tintMode = 1 THEN(CASE WHEN  coalesce(monAuthorizedAmt,0) -coalesce(monCapturedAmt,0) = v_monAmount THEN 1 ELSE 0 END) ELSE 1 END)
   AND coalesce(tintTransactionStatus,0) = 1
   AND 1 =(CASE WHEN v_tintMode = 1 THEN(CASE WHEN  DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -dtCreatedDate:: timestamp) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) ELSE 1 END)
   ORDER BY
   numTransHistoryID DESC LIMIT 1;
END; $$;












