-- Stored procedure definition script USP_ReportListMaster_ARPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_ARPreBuildReport(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalAR  DECIMAL(20,5);
   v_ARWithin30Days  DECIMAL(20,5) DEFAULT 0;
   v_ARWithin31to60Days  DECIMAL(20,5) DEFAULT 0;
   v_ARWithin61to90Days  DECIMAL(20,5) DEFAULT 0;
   v_ARPastDue  DECIMAL(20,5) DEFAULT 0;
   v_dtFromDate  TIMESTAMP DEFAULT '1990-01-01 00:00:00.000';
   v_dtToDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_AuthoritativeSalesBizDocId  INTEGER;
BEGIN
   v_dtFromDate := v_dtFromDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);
   v_dtToDate :=  v_dtToDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);

   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM
   AuthoritativeBizDocs WHERE
   numDomainId = v_numDomainID;
  
   DROP TABLE IF EXISTS tt_TEMPARRECORD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPARRECORD
   (
      numOppId NUMERIC(18,0),
      numOppBizDocsId NUMERIC(18,0),
      DealAmount DECIMAL(20,5),
      numBizDocId NUMERIC(18,0),
      numCurrencyID NUMERIC(18,0),
      dtDueDate TIMESTAMP,
      AmountPaid DECIMAL(20,5),
      monUnAppliedAmount DECIMAL(20,5)
   );

   BEGIN
      INSERT INTO tt_TEMPARRECORD(numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid)
      SELECT
      OB.numoppid,
			OB.numOppBizDocsId,
			coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount,
			OB.numBizDocId,
			coalesce(Opp.numCurrencyID,0) AS numCurrencyID,
			dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER) ELSE 0 END || 'day' as interval) AS dtDueDate,
			coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0) as AmountPaid
      FROM
      OpportunityMaster Opp
      INNER JOIN
      DivisionMaster AS DIV
      ON
      DIV.numDivisionID = Opp.numDivisionId
      INNER JOIN
      OpportunityBizDocs OB
      ON
      OB.numoppid = Opp.numOppId
      LEFT OUTER JOIN
      Currency C
      ON
      Opp.numCurrencyID = C.numCurrencyID
      WHERE
      tintopptype = 1
      AND tintoppstatus = 1
      AND Opp.numDomainId = v_numDomainID
      AND numBizDocId = v_AuthoritativeSalesBizDocId
      AND OB.bitAuthoritativeBizDocs = true
      AND coalesce(OB.tintDeferred,0) <> 1
      AND CAST(OB.dtCreatedDate AS DATE) BETWEEN v_dtFromDate:: DATE AND v_dtToDate:: DATE
      GROUP BY
      OB.numoppid,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,OB.dtCreatedDate,
      Opp.bitBillingTerms,Opp.intBillingDays,OB.monDealAmount,Opp.numCurrencyID,
      OB.dtFromDate,OB.monAmountPaid
      HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(OB.monAmountPaid*coalesce(Opp.fltExchangeRate,1),0)) != 0
		--Show Impact of AR journal entries in report as well 
      UNION
      SELECT
      CAST(0 AS NUMERIC(18,0)),
			CAST(0 AS NUMERIC(18,0)),
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			CAST(0 AS NUMERIC(18,0)),
			GJD.numCurrencyID,
			GJH.datEntry_Date,
			CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      General_Journal_Header GJH
      INNER JOIN
      General_Journal_Details GJD
      ON
      GJH.numJOurnal_Id = GJD.numJournalId
      AND GJH.numDomainId = GJD.numDomainId
      INNER JOIN
      Chart_Of_Accounts COA
      ON
      COA.numAccountId = GJD.numChartAcntId
      INNER JOIN
      DivisionMaster AS DIV
      ON
      DIV.numDivisionID = GJD.numCustomerId
      WHERE
      GJH.numDomainId = v_numDomainID
      AND COA.vcAccountCode ilike '0101010501'
      AND coalesce(numOppId,0) = 0 AND coalesce(numOppBizDocsId,0) = 0
      AND coalesce(GJH.numDepositId,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
      AND CAST(GJH.datEntry_Date AS DATE) BETWEEN v_dtFromDate:: DATE AND v_dtToDate:: DATE
      UNION ALL
      SELECT
      CAST(0 AS NUMERIC(18,0)),
			CAST(0 AS NUMERIC(18,0)),
			CAST((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))*-1 AS DOUBLE PRECISION),
			CAST(0 AS NUMERIC(18,0)),
			DM.numCurrencyID,
			DM.dtDepositDate,
			CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      DepositMaster DM
      INNER JOIN
      DivisionMaster AS DIV
      ON
      DM.numDivisionID = DIV.numDivisionID
      WHERE
      DM.numDomainId = v_numDomainID
      AND tintDepositePage IN(2,3)
      AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0
      AND CAST(DM.dtDepositDate AS DATE) BETWEEN v_dtFromDate:: DATE AND v_dtToDate:: DATE
      UNION ALL --Standalone Refund against Account Receivable
      SELECT
      CAST(0 AS NUMERIC(18,0)),
			CAST(0 AS NUMERIC(18,0)),
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			CAST(0 AS NUMERIC(18,0)),
			GJD.numCurrencyID,
			GJH.datEntry_Date,
			CAST(0 AS DOUBLE PRECISION) AS AmountPaid
      FROM
      ReturnHeader RH
      INNER JOIN
      General_Journal_Header GJH
      ON
      RH.numReturnHeaderID = GJH.numReturnID
      JOIN
      DivisionMaster AS DIV
      ON
      DIV.numDivisionID = RH.numDivisionId
      JOIN
      General_Journal_Details GJD
      ON
      GJH.numJOurnal_Id = GJD.numJournalId
      JOIN
      Chart_Of_Accounts COA
      ON
      COA.numAccountId = GJD.numChartAcntId
      AND COA.vcAccountCode ilike '0101010501'
      WHERE
      RH.tintReturnType = 4
      AND RH.numDomainID = v_numDomainID
      AND coalesce(RH.numParentID,0) = 0
      AND coalesce(RH.IsUnappliedPayment,0) = 0
      AND monBizDocAmount > 0 AND coalesce(RH.numBillPaymentIDRef,0) = 0 --AND ISNULL(RH.numDepositIDRef,0) > 0
      AND CAST(GJH.datEntry_Date AS DATE)  BETWEEN v_dtFromDate:: DATE AND v_dtToDate:: DATE;
      INSERT INTO tt_TEMPARRECORD(numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid
			,monUnAppliedAmount)
      SELECT
      0
			,0
			,0
			,0
			,0
			,NULL
			,0
			,SUM(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))
      FROM
      DepositMaster DM
      INNER JOIN
      DivisionMaster AS DIV
      ON
      DIV.numDivisionID = DM.numDivisionID
      WHERE
      DM.numDomainId = v_numDomainID
      AND tintDepositePage IN(2,3)
      AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0
      AND CAST(DM.dtDepositDate AS DATE) BETWEEN v_dtFromDate:: DATE AND v_dtToDate:: DATE
      GROUP BY
      DM.numDivisionID;
      SELECT SUM(DealAmount) INTO v_ARWithin30Days FROM tt_TEMPARRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 0 AND 30;
      SELECT SUM(DealAmount) INTO v_ARWithin31to60Days FROM tt_TEMPARRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 31 AND 60;
      SELECT SUM(DealAmount) INTO v_ARWithin61to90Days FROM tt_TEMPARRECORD WHERE DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 61 AND 90;
      SELECT SUM(DealAmount) INTO v_ARPastDue FROM tt_TEMPARRECORD WHERE GetUTCDateWithoutTime() > dtDueDate;
      EXCEPTION WHEN OTHERS THEN
		-- DO NOT RAISE ERROR
         NULL;
   END;

   open SWV_RefCur for SELECT cast(coalesce(v_TotalAR,0) as DECIMAL(20,5)) AS TotalAR, cast(coalesce(v_ARWithin30Days,0) as DECIMAL(20,5)) AS ARWithin30Days,cast(coalesce(v_ARWithin31to60Days,0) as DECIMAL(20,5)) AS ARWithin31to60Days,cast(coalesce(v_ARWithin61to90Days,0) as DECIMAL(20,5)) AS ARWithin61to90Days,cast(coalesce(v_ARPastDue,0) as DECIMAL(20,5)) AS ARPastDue;
END; $$;












