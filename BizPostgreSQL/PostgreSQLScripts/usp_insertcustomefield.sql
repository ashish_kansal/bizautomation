-- Stored procedure definition script usp_InsertCustomeField for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertCustomeField(v_numFldID  NUMERIC(9,0) DEFAULT 0,    
 v_vcFldValue VARCHAR(100) DEFAULT NULL,    
 v_numRecId  NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageID  SMALLINT;
BEGIN
   select   Grp_id INTO v_PageID from CFW_Fld_Master where Fld_id = v_numFldID;  
  
   if v_PageID = 1  OR v_PageID = 12 OR v_PageID = 13 OR v_PageID = 14 then
  
      DELETE FROM CFW_FLD_Values WHERE RecId = v_numRecId AND Fld_ID = v_numFldID;
      INSERT INTO CFW_FLD_Values(Fld_ID,Fld_Value,RecId)
		 VALUES(v_numFldID,v_vcFldValue,v_numRecId);
   end if;
	
  
   if v_PageID = 4 then
 
      DELETE FROM CFW_FLD_Values_Cont WHERE  RecId = v_numRecId AND fld_id = v_numFldID;
      INSERT INTO CFW_FLD_Values_Cont(fld_id,Fld_Value,RecId)
	VALUES(v_numFldID,v_vcFldValue,v_numRecId);
   end if;
 


   if v_PageID = 5 then
 
      DELETE FROM CFW_FLD_Values_Item WHERE  RecId = v_numRecId AND Fld_ID = v_numFldID;
      INSERT INTO CFW_FLD_Values_Item(Fld_ID,Fld_Value,RecId)
	 VALUES(v_numFldID,v_vcFldValue,v_numRecId);
   end if;
   RETURN;
END; $$; 


