CREATE OR REPLACE FUNCTION usp_SurveySelectFields(v_numIsLeadsField Integer, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select * from SurveySelectFields where tintIsLeadsField = v_numIsLeadsField;
   open SWV_RefCur2 for
   SELECT last_value FROM SurveySelectFields_seq;

   RETURN;
END; $$;


