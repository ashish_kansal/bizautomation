-- Function definition script fn_GetItemPromotions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemPromotions(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numItemClassification NUMERIC(18,0)
	,v_numRelationship NUMERIC(18,0)
	,v_numProfile NUMERIC(18,0)
	,v_tintMode SMALLINT)
RETURNS TABLE
(
   numTotalPromotions INTEGER,
   vcPromoDesc TEXT,
   bitRequireCouponCode BOOLEAN
) LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETITEMPROMOTIONS_ITEMPROMOTION CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETITEMPROMOTIONS_ITEMPROMOTION
   (
      numTotalPromotions INTEGER,
      vcPromoDesc TEXT,
      bitRequireCouponCode BOOLEAN
   );
   INSERT INTO tt_FN_GETITEMPROMOTIONS_ITEMPROMOTION(numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode)
   SELECT 
   COUNT(PO.numProId) OVER() AS numTotalPromotions
		,CONCAT((CASE WHEN COUNT(PO.numProId) OVER() > 1 THEN '' ELSE coalesce((CASE WHEN v_tintMode = 2 THEN PO.vcLongDesc ELSE PO.vcShortDesc END),
      '-') END),(CASE WHEN coalesce(PO.numOrderPromotionID,0) > 0 THEN ' <br/><i>(coupon code required)</i>' ELSE '' END)) AS vcShortDesc
		,(CASE WHEN coalesce(PO.numOrderPromotionID,0) > 0 THEN true ELSE false END) AS bitRequireCouponCode
   FROM
   PromotionOffer PO
   LEFT JOIN
   PromotionOffer POOrder
   ON
   PO.numOrderPromotionID = POOrder.numProId
   WHERE
   PO.numDomainId = v_numDomainID
   AND coalesce(PO.bitEnabled,false) = true
   AND coalesce(PO.IsOrderBasedPromotion,false) = false
   AND 1 =(CASE
   WHEN coalesce(PO.numOrderPromotionID,0) > 0
   THEN(CASE WHEN POOrder.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN 1 ELSE 0 END) END)
   ELSE(CASE WHEN PO.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN 1 ELSE 0 END) END)
   END)
   AND 1 =(CASE
   WHEN coalesce(PO.numOrderPromotionID,0) > 0
   THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
   ELSE(CASE PO.tintCustomersBasedOn
      WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionID) > 0 THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
      WHEN 3 THEN 1
      ELSE 0
      END)
   END)
   AND 1 =(CASE
   WHEN PO.tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
   WHEN PO.tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
   WHEN PO.tintOfferBasedOn = 4 THEN 1
   ELSE 0
   END)
   ORDER BY
   CASE
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
   END LIMIT 1;

	RETURN QUERY (SELECT * FROM tt_FN_GETITEMPROMOTIONS_ITEMPROMOTION);
END; $$;

