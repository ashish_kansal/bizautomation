-- Stored procedure definition script USP_BankReconcileMatchRule_UpdateOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BankReconcileMatchRule_UpdateOrder(v_numDomainID NUMERIC(18,0),
	v_vcRules TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numRuleID  NUMERIC(18,0);
   v_tintOrder  SMALLINT;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION

      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numRuleID NUMERIC(18,0),
         tintOrder SMALLINT
      );
      INSERT INTO tt_TEMP(numRuleID
		,tintOrder)
      SELECT
      numRuleID
		,tintOrder
      FROM
	   XMLTABLE
		(
			'NewDataSet/Rule'
			PASSING 
				CAST(v_vcRules AS XML)
			COLUMNS
				numRuleID NUMERIC(18,0) PATH 'numRuleID',
				tintOrder SMALLINT PATH 'tintOrder'
		) AS X
     ;

      select   COUNT(*) INTO v_COUNT FROM tt_TEMP;
      WHILE v_i <= v_COUNT LOOP
         select   numRuleID, tintOrder INTO v_numRuleID,v_tintOrder FROM tt_TEMP WHERE ID = v_i;
         IF NOT EXISTS(SELECT * FROM tt_TEMP WHERE numRuleID <> v_numRuleID AND tintOrder = v_tintOrder) then
		
            UPDATE BankReconcileMatchRule SET tintOrder = v_tintOrder WHERE numDomainID = v_numDomainID AND ID = v_numRuleID;
         ELSE
            RAISE EXCEPTION 'DUPLICATE_ORDER_NUMBER';
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;

