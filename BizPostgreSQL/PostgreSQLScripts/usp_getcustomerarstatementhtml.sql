-- Stored procedure definition script USP_GetCustomerARStatementHtml for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCustomerARStatementHtml(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_dtToDate DATE, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcBluePayFormName  VARCHAR(500);
   v_bitShowCardConnectLink  BOOLEAN;
   v_vcBluePayID  VARCHAR(1000);
   v_vcBluePayPassword  VARCHAR(1000);
   v_AuthoritativeSalesBizDocId  INTEGER;
   v_vcCompanyName  VARCHAR(300);
   v_bitTest  BOOLEAN;
BEGIN
   select   coalesce(vcFirstFldValue,''), coalesce(vcSecndFldValue,''), coalesce(bitTest,false) INTO v_vcBluePayID,v_vcBluePayPassword,v_bitTest FROM
   PaymentGatewayDTLID WHERE
   numDomainID = v_numDomainID
   AND intPaymentGateWay = 9
   AND coalesce(numSiteId,0) = 0;

   v_vcCompanyName := coalesce((SELECT CompanyInfo.vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = v_numDivisionID),'-');

   select(CASE WHEN coalesce(bitShowCardConnectLink,false) = true AND coalesce(vcBluePayFormName,'') <> '' THEN 1 ELSE 0 END), coalesce(vcBluePayFormName,'') INTO v_bitShowCardConnectLink,v_vcBluePayFormName FROM
   Domain WHERE
   numDomainId = v_numDomainID;
    
   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM
   AuthoritativeBizDocs WHERE
   numDomainId = v_numDomainID;

   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;
    

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0),
      vcPOppName VARCHAR(200),
      numOppBizDocsId NUMERIC(18,0),
      vcBizDocID VARCHAR(300),
      monAmountPaid DECIMAL(20,2),
      monAmountDue DECIMAL(20,2),
      dtDueDate VARCHAR(50),
      numPastDays NUMERIC(18,0),
      "vcCustomerPO#" VARCHAR(100)
   );

   INSERT INTO tt_TEMP(numOppID
		,vcPOppName
		,numOppBizDocsId
		,vcBizDocID
		,monAmountPaid
		,monAmountDue
		,dtDueDate
		,numPastDays
		,"vcCustomerPO#")
   SELECT
   OB.numoppid,
		OM.vcpOppName,
        OB.numOppBizDocsId,
        OB.vcBizDocID,
        coalesce(TablePayments.monPaidAmount,0)*OM.fltExchangeRate AS monAmountPaid,
		coalesce((OB.monDealAmount*OM.fltExchangeRate) -(coalesce(TablePayments.monPaidAmount,0)*OM.fltExchangeRate),0) AS monAmountDue,
        CAST(CASE coalesce(bitBillingTerms,false)
   WHEN true THEN FormatedDateFromDate(OB.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) AS INTEGER) || 'day' as interval),v_numDomainID)
   WHEN false THEN FormatedDateFromDate(OB.dtFromDate,v_numDomainID)
   END AS VARCHAR(50)) AS dtDueDate,
		DATE_PART('day',LOCALTIMESTAMP  -CAST(FormatedDateFromDate((CASE coalesce(bitBillingTerms,false)
   WHEN true THEN OB.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) AS INTEGER) || 'day' as interval)
   WHEN false THEN OB.dtFromDate
   END),v_numDomainID) AS timestamp):: timestamp),
		CAST(coalesce(OM."vcCustomerPO#",'') AS VARCHAR(100)) AS "vcCustomerPO#"
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   OpportunityBizDocs OB
   ON
   OB.numoppid = OM.numOppId
   LEFT JOIN LATERAL(SELECT
      SUM(monAmountPaid) AS monPaidAmount
      FROM
      DepositeDetails DD
      INNER JOIN
      DepositMaster DM
      ON
      DD.numDepositID = DM.numDepositId
      WHERE
      numOppID = OM.numOppID
      AND numOppBizDocsId = OB.numOppBizDocsID
      AND CAST(DM.dtDepositDate AS DATE) <= v_dtToDate) TablePayments on TRUE
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND OM.numDivisionId = v_numDivisionID
   AND OB.numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND coalesce((OB.monDealAmount*OM.fltExchangeRate) -(coalesce(TablePayments.monPaidAmount,0)*OM.fltExchangeRate),0) > 0;


   open SWV_RefCur for
   SELECT
   v_vcCompanyName AS vcCompanyName
		,FormatedDateFromDate(v_dtToDate,v_numDomainID) AS dtStatementDate
		,TO_CHAR(coalesce((SELECT SUM(monAmountDue) FROM tt_TEMP WHERE numPastDays <= 0),
   0),'FM9,999,999,999,999,990.00') AS monCurrent
		,TO_CHAR(coalesce((SELECT SUM(monAmountDue) FROM tt_TEMP WHERE numPastDays > 0),0),'FM9,999,999,999,999,990.00') AS monPastDue
		,v_vcBluePayID AS vcBluePayID
		,v_vcBluePayPassword AS vcBluePayPassword
		,v_bitTest AS bitTest
		,v_vcBluePayFormName AS vcBluePayFormName
		,v_bitShowCardConnectLink AS bitShowCardConnectLink;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMP;
   RETURN;
END; $$;


