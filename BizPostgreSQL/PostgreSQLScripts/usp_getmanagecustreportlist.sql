-- Stored procedure definition script Usp_getManageCustReportList for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_getManageCustReportList(v_numDomainId NUMERIC(9,0) ,
v_numGroupId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select
   numCustomReportId as ReportId,vcReportName,vcReportDescription,
false as bitallowed
   from CustomReport Cr
--left join  DashboardAllowedReports DAR on Cr.numCustomReportId=DAR.numReportId 
   where numdomainId = v_numDomainId and numCustomReportId not in(select cast(numReportId as NUMERIC(18,0)) from DashboardAllowedReports where numGrpId =  v_numGroupId)
   union
   Select
   numCustomReportId as ReportId,vcReportName,vcReportDescription,
true as bitallowed
   from CustomReport Cr
   join  DashboardAllowedReports DAR on Cr.numCustomReportId = DAR.numReportId
   where numdomainId = v_numDomainId and numgrpId = v_numGroupId
   order by 1;
END; $$;












