-- Stored procedure definition script USP_ForecastManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ForecastManage(v_strForecast TEXT DEFAULT '',        
v_tintForecastType SMALLINT DEFAULT null,        
v_numUserCntID NUMERIC(9,0) DEFAULT null,        
v_numItemCode NUMERIC(9,0) DEFAULT null,        
v_sintYear SMALLINT DEFAULT null,        
v_tintquarter SMALLINT DEFAULT null,        
v_numDomainId NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_tintForecastType = 1 then

      delete from Forecast where numCreatedBy = v_numUserCntID
      and sintYear = v_sintYear and tintQuarter = v_tintquarter
      and tintForecastType = v_tintForecastType
      and numDomainID = v_numDomainId;
   else
      delete from Forecast where numCreatedBy = v_numUserCntID
      and sintYear = v_sintYear and tintQuarter = v_tintquarter
      and tintForecastType = v_tintForecastType
      and numItemCode = v_numItemCode and numDomainID = v_numDomainId;
   end if;           
        
   insert into Forecast
   (
		sintYear,
		tintQuarter,
		tintMonth,
		monQuota,
		monForecastAmount,
		numItemCode,
		tintForecastType,
		numCreatedBy,
		numDomainID
	)
	select 
		X.sintYear
		,X.tintquarter
		,X.tintMonth
		,X.monQuota
		,X.monForecastAmount
		,X.numItemCode
		,v_tintForecastType
		,v_numUserCntID
		,v_numDomainId 
	from
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strForecast AS XML)
		COLUMNS
			id FOR ORDINALITY,
			sintYear SMALLINT PATH 'sintYear',
			tintquarter SMALLINT PATH 'tintquarter',
			tintMonth SMALLINT PATH 'tintMonth',
			monQuota DECIMAL(20,5) PATH 'monQuota',
			monForecastAmount DECIMAL(20,5) PATH 'monForecastAmount',
			numItemCode NUMERIC(9,0) PATH 'numItemCode'
	) X;        

   RETURN;
END; $$;


