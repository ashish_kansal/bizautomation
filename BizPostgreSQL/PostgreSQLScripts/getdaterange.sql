CREATE OR REPLACE FUNCTION GetDateRange
(
	v_ViewRange VARCHAR(50),  
	v_Period CHAR(1)
)
RETURNS TIMESTAMP LANGUAGE plpgsql
AS $$
	DECLARE
	v_PeriodDate  TIMESTAMP;
	v_getDate  TIMESTAMP;
	v_TodayDayOfWeek  INTEGER;
BEGIN
	v_getDate := '1900-01-01':: date+INTERVAL '1 day'+CAST(datediff('day','1900-01-01'::date,TIMEZONE('UTC',now())::date) || 'day' as interval);

	--get number of a current day (1-Monday, 2-Tuesday... 7-Sunday)
	v_TodayDayOfWeek := EXTRACT(DOW FROM v_getDate)+1; 
   
	IF v_Period = 'S' then
		v_PeriodDate := CASE v_ViewRange
							WHEN 'TW' THEN v_getDate+CAST(-v_TodayDayOfWeek || 'day' as interval)
							WHEN 'LW' THEN v_getDate+CAST(-(v_TodayDayOfWeek::bigint+7) || 'day' as interval)
							WHEN 'TM' THEN '1900-01-01'::date + CAST((DATE_PART('year',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))*12+DATE_PART('month',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))) || 'month' as interval)
							WHEN 'LM' THEN '1900-01-01'::date + CAST((DATE_PART('year',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))*12+DATE_PART('month',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))) -1 || 'month' as interval)
							WHEN 'TQ' THEN '1900-01-01'::date + make_interval(months => CAST(DATEDIFF('month','1900-01-01'::TIMESTAMP,TIMEZONE('UTC',now())) AS INTEGER) / 3 * 3)
							WHEN 'LQ' THEN '1900-01-01'::date + make_interval(months => (((CAST(DATEDIFF('month','1900-01-01'::TIMESTAMP,TIMEZONE('UTC',now())) AS INTEGER) / 3) -1) * 3)) 
							WHEN 'TY' THEN make_date(CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS INT),1,1):: timestamp+INTERVAL '0 day'
							WHEN 'LY' THEN make_date(CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) -1 AS INT),1,1):: timestamp+INTERVAL '0 day'
						END;
	ELSE
		v_PeriodDate := CASE v_ViewRange
						  WHEN 'TW' THEN v_getDate+CAST(-v_TodayDayOfWeek+7 || 'day' as interval)
						  WHEN 'LW' THEN v_getDate+CAST(-v_TodayDayOfWeek || 'day' as interval)
						  WHEN 'TM' THEN '1900-01-01':: date+CAST((DATE_PART('year',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))*12+DATE_PART('month',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE)))+1 || 'month' as interval)
						  WHEN 'LM' THEN '1900-01-01':: date+CAST((DATE_PART('year',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))*12+DATE_PART('month',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))) || 'month' as interval)
						  WHEN 'TQ' THEN '1900-01-01':: date+ make_interval(months => ((CAST(DATEDIFF('month','1900-01-01'::TIMESTAMP,TIMEZONE('UTC',now())) AS INTEGER) / 3) + 1) * 3)
						  WHEN 'LQ' THEN '1900-01-01'::date + make_interval(months => CAST(DATEDIFF('month','1900-01-01'::TIMESTAMP,TIMEZONE('UTC',now())) AS INTEGER) / 3 * 3)   
						  WHEN 'TY' THEN  make_date(CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS INT),12,31):: timestamp+INTERVAL '0 day'
						  WHEN 'LY' THEN make_date(CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) - 1 AS INT),12,31):: timestamp+INTERVAL '0 day'
						END;
	end if;   
  
	RETURN v_PeriodDate;
END; $$;

