-- Stored procedure definition script usp_GetPageAuthorizationForUser for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetPageAuthorizationForUser(     --Added the module Id for specific calling of the page  
--  
v_numUserID NUMERIC(9,0) DEFAULT 0,  
 v_vcFileName VARCHAR(50) DEFAULT NULL,  
 v_numModuleId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPageID  NUMERIC(9,0);
BEGIN
   IF v_numModuleId = 0 then
  
      RAISE NOTICE '%',v_vcFileName;
      RAISE NOTICE '%',v_numUserID;
      open SWV_RefCur for
      SELECT     G.numPageID, G.numModuleID, MAX(G.intViewAllowed) AS intViewAllowed,
                         MAX(G.intAddAllowed) AS intAddAllowed, MAX(G.intUpdateAllowed) AS intUpdateAllowed,
                         MAX(G.intDeleteAllowed) AS intDeleteAllowed, MAX(G.intExportAllowed) AS intExportAllowed,
                         MAX(G.intPrintAllowed) AS intPrintAllowed
      FROM         GroupAuthorization G INNER JOIN
      PageMaster ON G.numPageID = PageMaster.numPageID AND
      G.numModuleID = PageMaster.numModuleID INNER JOIN
      UserGroups ON G.numGroupID = UserGroups.numGroupID
      WHERE     UserGroups.numUserID = v_numUserID AND PageMaster.vcFileName = v_vcFileName
      GROUP BY G.numPageID,G.numModuleID;
   ELSE
      RAISE NOTICE '%',v_vcFileName;
      RAISE NOTICE '%',v_numUserID;
      open SWV_RefCur for
      SELECT     G.numPageID, G.numModuleID, MAX(G.intViewAllowed) AS intViewAllowed,
                         MAX(G.intAddAllowed) AS intAddAllowed, MAX(G.intUpdateAllowed) AS intUpdateAllowed,
                         MAX(G.intDeleteAllowed) AS intDeleteAllowed, MAX(G.intExportAllowed) AS intExportAllowed,
                         MAX(G.intPrintAllowed) AS intPrintAllowed
      FROM   GroupAuthorization      G INNER JOIN
      PageMaster ON G.numPageID = PageMaster.numPageID AND
      G.numModuleID = PageMaster.numModuleID INNER JOIN
      UserGroups ON G.numGroupID = UserGroups.numGroupID
      WHERE     UserGroups.numUserID = v_numUserID AND PageMaster.vcFileName = v_vcFileName AND PageMaster.numModuleID = v_numModuleId
      GROUP BY G.numPageID,G.numModuleID;
   end if;
   RETURN;
END; $$;


