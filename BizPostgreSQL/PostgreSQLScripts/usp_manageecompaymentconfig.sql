-- Stored procedure definition script USP_ManageEComPaymentConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEComPaymentConfig(v_numDomainID NUMERIC,
    v_numSiteID NUMERIC,
    v_strItems XML,
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 1 then
        
      open SWV_RefCur for
      SELECT  coalesce(PC.numId,0) as numId,
                            coalesce(PC.numPaymentMethodId,0) AS numPaymentMethodId,
                            GetListIemName(PC.numPaymentMethodId) AS PaymentMethod,
                            coalesce(PC.bitEnable,0) AS bitEnable,
                            coalesce(PC.numBizDocId,0) AS numBizDocId,
                            coalesce(PC.numBizDocStatus,0) AS numBizDocStatus,
                            coalesce(PC.numMirrorBizDocTemplateId,0) AS numMirrorBizDocTemplateId,
                            coalesce(PC.numOrderStatus,0) AS numOrderStatus,
                            coalesce(PC.numFailedOrderStatus,0)  AS numFailedOrderStatus ,
                            coalesce(PC.numRecordOwner,0) AS numRecordOwner,
                            coalesce(PC.numAssignTo,0) AS numAssignTo,
                            CASE WHEN PC.numPaymentMethodId = 84 THEN 'Sales Opportunity'
      ELSE 'Sales Order'
      END AS strType
      FROM
      eCommercePaymentConfig PC
      WHERE
      PC.numDomainID = v_numDomainID
      AND numSiteId = v_numSiteID
      UNION
      SELECT   0 as numId,
                            LD.numListItemID AS numPaymentMethodId,
                            LD.vcData AS PaymentMethod,
							0 AS bitEnable,
                            0 AS numBizDocId,
                            0 AS numBizDocStaus,
                            0 AS numMirrorBizDocTemplateId,
                            0 AS numOrderStatus,
                            0 AS numFailedOrderStatus,
                            0 AS numRecordOwner,
                            0 AS numAssignTo,
                            CASE WHEN LD.numListItemID = 84 THEN 'Sales Opportunity'
      ELSE 'Sales Order'
      END AS strType
      FROM    Listdetails LD
      WHERE   LD.numListID = 31
      AND LD.constFlag = true
      AND LD.numListItemID NOT IN(SELECT coalesce(PC.numPaymentMethodId,0) AS numPaymentMethodId FROM eCommercePaymentConfig PC
         WHERE PC.numDomainID = v_numDomainID AND numSiteId = v_numSiteID)
      ORDER BY numPaymentMethodId;
   ELSEIF v_tintMode = 2
   then
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
         DELETE  FROM eCommercePaymentConfig
         WHERE   numSiteId = v_numSiteID
         AND numDomainID = v_numDomainID;
         INSERT  INTO eCommercePaymentConfig(numSiteId,
                                                                  numPaymentMethodId,
                                                                  bitEnable,
                                                                  numBizDocId,
															      numBizDocStatus,
                                                                  numMirrorBizDocTemplateId ,
                                                                  numOrderStatus,
                                                                  numFailedOrderStatus,
                                                                  numRecordOwner,
                                                                  numAssignTo,
                                                                  numDomainID)
         SELECT  v_numSiteID,
                                        X.numPaymentMethodId,
                                        X.bitEnable,
                                        X.numBizDocId,
                                        X.numBizdocStatus,
                                        X.numMirrorBizDocTemplateId,
                                        X.numOrderStatus,
                                        X.numFailedOrderStatus,
                                        X.numRecordOwner,
                                        X.numAssignTo,
                                        v_numDomainID
         FROM
			XMLTABLE
		(
			'NewDataSet/WorkFlowConditionList'
			PASSING 
				v_strItems 
			COLUMNS
				id FOR ORDINALITY,
				numPaymentMethodId NUMERIC(18,0) PATH 'numPaymentMethodId'
				,bitEnable numeric  PATH 'bitEnable'
				,numBizDocId NUMERIC(18,0) PATH 'numBizDocId'
				,numBizdocStatus NUMERIC(18,0) PATH 'numBizdocStatus'
				,numMirrorBizDocTemplateId NUMERIC(18,0) PATH 'numMirrorBizDocTemplateId'
				,numOrderStatus NUMERIC(18,0) PATH 'numOrderStatus'
				,numFailedOrderStatus NUMERIC(18,0) PATH 'numFailedOrderStatus'
				,numRecordOwner NUMERIC(18,0) PATH 'numRecordOwner'
				,numAssignTo NUMERIC(18,0) PATH 'numAssignTo'
		) X;	
      end if;
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


