DROP FUNCTION IF EXISTS USP_Import_SaveItemDetails;

CREATE OR REPLACE FUNCTION USP_Import_SaveItemDetails(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,INOUT v_numItemCode NUMERIC(18,0) 
	,v_numShipClass NUMERIC(18,0)
	,v_vcItemName VARCHAR(300)
	,v_monListPrice DECIMAL(20,5)
	,v_txtItemDesc VARCHAR(1000)
	,v_vcModelID VARCHAR(200)
	,v_vcManufacturer VARCHAR(250)
	,v_numBarCodeId VARCHAR(50)
	,v_fltWidth DOUBLE PRECISION
	,v_fltHeight DOUBLE PRECISION
	,v_fltWeight DOUBLE PRECISION
	,v_fltLength DOUBLE PRECISION
	,v_monAverageCost DECIMAL(20,5)
	,v_bitSerialized BOOLEAN
	,v_bitKitParent BOOLEAN
	,v_bitAssembly BOOLEAN
	,v_IsArchieve BOOLEAN
	,v_bitLotNo BOOLEAN
	,v_bitAllowBackOrder BOOLEAN
	,v_numIncomeChartAcntId NUMERIC(18,0)
	,v_numAssetChartAcntId NUMERIC(18,0)
	,v_numCOGsChartAcntId NUMERIC(18,0)
	,v_numItemClassification NUMERIC(18,0)
	,v_numItemGroup NUMERIC(18,0)
	,v_numPurchaseUnit NUMERIC(18,0)
	,v_numSaleUnit NUMERIC(18,0)
	,v_numItemClass NUMERIC(18,0)
	,v_numBaseUnit NUMERIC(18,0)
	,v_vcSKU VARCHAR(50)
	,v_charItemType CHAR(1)
	,v_bitTaxable BOOLEAN
	,v_txtDesc TEXT
	,v_vcExportToAPI VARCHAR(50)
	,v_bitAllowDropShip BOOLEAN
	,v_bitMatrix BOOLEAN
	,v_vcItemAttributes VARCHAR(2000)
	,v_vcCategories VARCHAR(1000)
	,v_txtShortDesc TEXT
	,v_numReOrder DOUBLE PRECISION
	,v_bitAutomateReorderPoint BOOLEAN)
LANGUAGE plpgsql
   AS $$
	DECLARE
	v_bitAttributesChanged  BOOLEAN DEFAULT 1;
	v_bitOppOrderExists  BOOLEAN DEFAULT 0;
	v_hDoc  INTEGER;     
	                                                                        	
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
	v_bitDuplicate  BOOLEAN DEFAULT 0;
	v_strAttribute  VARCHAR(500) DEFAULT '';
	v_i  INTEGER DEFAULT 1;
	v_COUNT  INTEGER; 
	v_numFldID  NUMERIC(18,0);
	v_numFldValue  NUMERIC(18,0);
	v_OldGroupID  NUMERIC; 
	v_monOldAverageCost  DECIMAL(18,4);
	v_numWareHouseItemID  NUMERIC(9,0);
	v_numTotal  INTEGER;
	v_vcDescription  VARCHAR(100);
	v_j  INTEGER DEFAULT 1;
	v_jCount  INTEGER;
	v_numTempWarehouseID  NUMERIC(18,0);
	v_USP_WarehouseItems_Save_numWareHouseItemID NUMERIC(18,0);
	v_numDomain  NUMERIC(18,0);
BEGIN
   -- BEGIN TRANSACTION

	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF v_numAssetChartAcntId = 0 then
	
      v_numAssetChartAcntId := NULL;
   end if;
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      Fld_ID NUMERIC(18,0),
      Fld_Value NUMERIC(18,0)
   );
   IF coalesce(v_bitMatrix,false) = true AND coalesce(v_numItemGroup,0) = 0 then
	
      RAISE EXCEPTION 'ITEM_GROUP_NOT_SELECTED';
      RETURN;
   ELSEIF coalesce(v_bitMatrix,false) = true AND LENGTH(coalesce(v_vcItemAttributes,'')) = 0 AND coalesce(v_numItemCode,0) = 0
   then
	
      RAISE EXCEPTION 'ITEM_ATTRIBUTES_NOT_SELECTED';
      RETURN;
   ELSEIF coalesce(v_bitMatrix,false) = true AND LENGTH(coalesce(v_vcItemAttributes,'')) > 0
   then
      IF OCTET_LENGTH(coalesce(v_vcItemAttributes,'')) > 2 then
         INSERT INTO tt_TEMPTABLE(Fld_ID,
				Fld_Value)
         SELECT Fld_ID,Fld_Value FROM
		 XMLTABLE
		(
			'NewDataSet/CusFlds'
			PASSING 
				CAST(v_vcItemAttributes AS XML)
			COLUMNS
				Fld_ID NUMERIC(18,0) PATH 'Fld_ID'
				,Fld_Value NUMERIC(18,0) PATH 'Fld_Value'
				
		) AS X
         ORDER BY
         Fld_ID;
         select   COUNT(*) INTO v_COUNT FROM tt_TEMPTABLE;
         WHILE v_i <= v_COUNT LOOP
            select   Fld_ID, Fld_Value INTO v_numFldID,v_numFldValue FROM tt_TEMPTABLE WHERE ID = v_i;
            IF isnumeric(v_numFldValue::VARCHAR) = true then
				
               select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute FROM CFW_Fld_Master WHERE Fld_id = v_numFldID AND Grp_id = 9;
               IF LENGTH(v_strAttribute) > 0 then
					
                  select   coalesce(v_strAttribute,'') || ':' || vcData || ',' INTO v_strAttribute FROM Listdetails WHERE numListItemID = v_numFldValue;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         v_strAttribute := SUBSTR(v_strAttribute,0,LENGTH(v_strAttribute));
         IF(SELECT
         COUNT(*)
         FROM
         Item
         INNER JOIN
         ItemAttributes
         ON
         Item.numItemCode = ItemAttributes.numItemCode
         AND ItemAttributes.numDomainID = v_numDomainID
         WHERE
         Item.numItemCode <> v_numItemCode
         AND Item.vcItemName = v_vcItemName
         AND Item.numItemGroup = v_numItemGroup
         AND Item.numDomainID = v_numDomainID
         AND fn_GetItemAttributes(v_numDomainID,Item.numItemCode,0::BOOLEAN) = v_strAttribute) > 0 then
			
            RAISE EXCEPTION 'ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS';
            RETURN;
         end if;
         If coalesce(v_numItemCode,0) > 0 AND fn_GetItemAttributes(v_numDomainID,v_numItemCode,0::BOOLEAN) = v_strAttribute then
			
            v_bitAttributesChanged := false;
         end if;
         
      end if;
   ELSEIF coalesce(v_bitMatrix,false) = false
   then
	
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
      DELETE FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
   end if;
   IF coalesce(v_numItemGroup,0) > 0 AND coalesce(v_bitMatrix,false) = true then
	
      IF(SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = v_numItemGroup AND tintType = 2) = 0 then
		
         RAISE EXCEPTION 'ITEM_GROUP_NOT_CONFIGURED';
         RETURN;
      ELSEIF EXISTS(SELECT
      CFM.Fld_id
      FROM
      CFW_Fld_Master CFM
      LEFT JOIN
      Listdetails LD
      ON
      CFM.numlistid = LD.numListID
      WHERE
      CFM.numDomainID = v_numDomainID
      AND CFM.Fld_id IN(SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID = v_numItemGroup AND tintType = 2)
      GROUP BY
      CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
      then
		
         RAISE EXCEPTION 'ITEM_GROUP_NOT_CONFIGURED';
         RETURN;
      end if;
   end if;
   IF coalesce(v_numItemCode,0) = 0 then
	
      IF (coalesce(v_vcItemName,'') = '') then
		
         RAISE EXCEPTION 'ITEM_NAME_NOT_SELECTED';
         RETURN;
      ELSEIF (coalesce(v_charItemType,'') = '' OR coalesce(v_charItemType,'') = '0')
      then
		
         RAISE EXCEPTION 'ITEM_TYPE_NOT_SELECTED';
         RETURN;
      ELSE
         If (v_charItemType = 'P') then
			
            IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numAssetChartAcntId) = 0) then
				
               RAISE EXCEPTION 'INVALID_ASSET_ACCOUNT';
               RETURN;
            end if;
         ELSEIF v_numAssetChartAcntId = 0
         then
			
            v_numAssetChartAcntId := NULL;
         end if;

			-- This is common check for CharItemType P and Other
         IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numCOGsChartAcntId) = 0) then
			
            RAISE EXCEPTION 'INVALID_COGS_ACCOUNT';
            RETURN;
         end if;
         IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numIncomeChartAcntId) = 0) then
			
            RAISE EXCEPTION 'INVALID_INCOME_ACCOUNT';
            RETURN;
         end if;
      end if;
      INSERT INTO Item(numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
			,bitAutomateReorderPoint)
		VALUES(v_numDomainID
			,v_numUserCntID
			,TIMEZONE('UTC',now())
			,TIMEZONE('UTC',now())
			,v_numUserCntID
			,v_numShipClass
			,v_vcItemName
			,v_monListPrice
			,v_txtItemDesc
			,v_vcModelID
			,v_vcManufacturer
			,v_numBarCodeId
			,v_fltWidth
			,v_fltHeight
			,v_fltWeight
			,v_fltLength
			,v_monAverageCost
			,v_bitSerialized
			,v_bitKitParent
			,v_bitAssembly
			,v_IsArchieve
			,v_bitLotNo
			,v_bitAllowBackOrder
			,NULLIF(v_numIncomeChartAcntId,0)
			,NULLIF(v_numAssetChartAcntId,0)
			,NULLIF(v_numCOGsChartAcntId,0)
			,v_numItemClassification
			,v_numItemGroup
			,v_numPurchaseUnit
			,v_numSaleUnit
			,v_numItemClass
			,v_numBaseUnit
			,v_vcSKU
			,v_charItemType
			,v_bitTaxable
			,v_vcExportToAPI
			,v_bitAllowDropShip
			,v_bitMatrix
			,(CASE WHEN LENGTH(coalesce(v_vcManufacturer, '')) > 0 THEN coalesce((SELECT  numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId = DivisionMaster.numCompanyID WHERE CompanyInfo.numDomainID = v_numDomainID AND LOWER(vcCompanyName) = LOWER(v_vcManufacturer) LIMIT 1), 0) ELSE 0 END)
			,v_bitAutomateReorderPoint);
		
      v_numItemCode := CURRVAL('Item_seq');
      INSERT INTO ItemExtendedDetails(numItemCode,txtDesc,txtShortDesc) VALUES(v_numItemCode,v_txtDesc,v_txtShortDesc);
		
      IF v_charItemType = 'P' then
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numWarehouseID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMPWAREHOUSE(numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID = v_numDomainID;
         select   COUNT(*) INTO v_jCount FROM tt_TEMPWAREHOUSE;
         WHILE v_j <= v_jCount LOOP
            select   numWarehouseID INTO v_numTempWarehouseID FROM tt_TEMPWAREHOUSE WHERE ID = v_j;
            v_USP_WarehouseItems_Save_numWareHouseItemID := 0;
            PERFORM USP_WarehouseItems_Save(v_numDomainID,v_numUserCntID,v_USP_WarehouseItems_Save_numWareHouseItemID,
            v_numTempWarehouseID,0,v_numItemCode,'',v_monListPrice,0,0,'','','',
            '',false);
            v_j := v_j::bigint+1;
         END LOOP;
      end if;
   ELSEIF coalesce(v_numItemCode,0) > 0 AND EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode)
   then
      IF(SELECT COUNT(OI.numOppId) FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode) > 0 then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitItems OI WHERE OI.numChildItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitChildItems OI WHERE OI.numItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      end if;
      select   coalesce(numItemGroup,0) INTO v_OldGroupID FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
      v_monOldAverageCost := 0;
      select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_monOldAverageCost FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
      UPDATE
      Item
      SET
      vcItemName = v_vcItemName,txtItemDesc = v_txtItemDesc,charItemType = v_charItemType,
      monListPrice = v_monListPrice,numItemClassification = v_numItemClassification,
      bitTaxable = v_bitTaxable,vcSKU = v_vcSKU,bitKitParent = v_bitKitParent,
      numDomainID = v_numDomainID,bintModifiedDate = TIMEZONE('UTC',now()),
      numModifiedBy = v_numUserCntID,bitSerialized = v_bitSerialized,vcModelID = v_vcModelID,
      numItemGroup = v_numItemGroup,numCOGsChartAcntId = NULLIF(v_numCOGsChartAcntId,0),
      numAssetChartAcntId = NULLIF(v_numAssetChartAcntId,0),
      numIncomeChartAcntId = NULLIF(v_numIncomeChartAcntId,0),fltWeight = v_fltWeight,
      fltHeight = v_fltHeight,fltWidth = v_fltWidth,fltLength = v_fltLength,
      bitAllowBackOrder = v_bitAllowBackOrder,bitAssembly = v_bitAssembly,
      numBarCodeId = v_numBarCodeId,vcManufacturer = v_vcManufacturer,numBaseUnit = v_numBaseUnit,
      numPurchaseUnit = v_numPurchaseUnit,numSaleUnit = v_numSaleUnit,
      bitLotNo = v_bitLotNo,IsArchieve = v_IsArchieve,numItemClass = v_numItemClass,
      vcExportToAPI = v_vcExportToAPI,numShipClass = v_numShipClass,bitAllowDropShip = v_bitAllowDropShip, 
      bitArchiveItem = v_IsArchieve,bitMatrix = v_bitMatrix,
      numManufacturer =(CASE WHEN LENGTH(coalesce(v_vcManufacturer,'')) > 0 THEN coalesce((SELECT  numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId = DivisionMaster.numCompanyID WHERE CompanyInfo.numDomainID = v_numDomainID AND LOWER(vcCompanyName) = LOWER(v_vcManufacturer) LIMIT 1),0) ELSE 0 END),bitAutomateReorderPoint = v_bitAutomateReorderPoint
      WHERE
      numItemCode = v_numItemCode
      AND numDomainID = v_numDomainID;
      IF NOT EXISTS(SELECT numItemCode FROM OpportunityItems WHERE numItemCode = v_numItemCode) then
		
         IF (((SELECT coalesce(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = v_numItemCode AND numDomainID = v_numDomainID) = 0) AND ((SELECT(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) FROM Item  WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID) = 0)) then
			
            UPDATE Item SET monAverageCost = v_monAverageCost  where numItemCode = v_numItemCode  AND numDomainID = v_numDomainID;
         end if;
      end if;
	
		--If Average Cost changed then add in Tracking
      IF coalesce(v_monAverageCost,0) != coalesce(v_monOldAverageCost,0) then
         v_numWareHouseItemID := 0;
         v_numTotal := 0;
         v_i := 0;
         v_vcDescription := '';
         select   COUNT(*) INTO v_numTotal FROM WareHouseItems where numItemID = v_numItemCode AND numDomainID = v_numDomainID;
         WHILE v_i < v_numTotal LOOP
            select   min(numWareHouseItemID) INTO v_numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numDomainID = v_numDomainID and numWareHouseItemID > v_numWareHouseItemID;
            IF v_numWareHouseItemID > 0 then
               v_vcDescription := 'Average Cost Changed Manually OLD : ' || CAST(coalesce(v_monOldAverageCost,0) AS VARCHAR(20));
					 --  numeric(9, 0)		
						 --  numeric(9, 0)
						 --  tinyint
						 --  varchar(100)
						 --  tinyint
						 --  numeric(9, 0)
               v_numDomain := v_numDomainID;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
               v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
               v_tintMode := 0::SMALLINT,v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,
               SWV_RefCur := null);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
      IF	v_bitTaxable = true then
		
         IF NOT EXISTS(SELECT * FROM ItemTax WHERE numItemCode = v_numItemCode AND numTaxItemID = 0) then
			
            INSERT INTO ItemTax(numItemCode,
					numTaxItemID,
					bitApplicable)
				VALUES(v_numItemCode,
					0,
					true);
         end if;
      end if;
      IF v_charItemType = 'S' or v_charItemType = 'N' then
		
         DELETE FROM WareHouseItmsDTL where numWareHouseItemID in(select numWareHouseItemID from WareHouseItems where numItemID = v_numItemCode AND WareHouseItems.numDomainID = v_numDomainID);
         DELETE FROM WareHouseItems where numItemID = v_numItemCode AND WareHouseItems.numDomainID = v_numDomainID;
      end if;
      IF NOT EXISTS(SELECT numItemCode FROM ItemExtendedDetails  where numItemCode = v_numItemCode) then
		
         INSERT INTO ItemExtendedDetails(numItemCode,txtDesc,txtShortDesc) VALUES(v_numItemCode,v_txtDesc,v_txtShortDesc);
      ELSE
         SELECT txtShortDesc INTO v_txtShortDesc FROM ItemExtendedDetails  WHERE numItemCode = v_numItemCode LIMIT 1;
         UPDATE ItemExtendedDetails SET txtDesc = v_txtDesc  WHERE numItemCode = v_numItemCode;
      end if;
   end if;
   IF coalesce(v_numItemGroup,0) = 0 OR coalesce(v_bitMatrix,false) = true then
	
      UPDATE WareHouseItems SET monWListPrice = v_monListPrice,vcWHSKU = v_vcSKU,vcBarCode = v_numBarCodeId WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode;
   end if;
   IF v_vcCategories IS NOT NULL then
	
      IF v_vcCategories <> '' then
		
         INSERT INTO ItemCategory(numItemID,numCategoryID)  SELECT v_numItemCode,ID from SplitIDs(v_vcCategories,',');
      ELSE
         DELETE FROM ItemCategory WHERE numItemID = v_numItemCode;
      end if;
   end if;

	-- SAVE MATRIX ITEM ATTRIBUTES
   IF v_numItemCode > 0 AND coalesce(v_bitAttributesChanged,false) = true AND coalesce(v_bitMatrix,false) = true AND LENGTH(coalesce(v_vcItemAttributes,'')) > 0 AND coalesce(v_bitOppOrderExists,false) <> true AND(SELECT COUNT(*) FROM tt_TEMPTABLE) > 0 then
	
		-- DELETE PREVIOUS ITEM ATTRIBUTES
      DELETE FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;

		-- INSERT NEW ITEM ATTRIBUTES
      INSERT INTO ItemAttributes(numDomainID
			,numItemCode
			,Fld_ID
			,Fld_Value)
      SELECT
      v_numDomainID
			,v_numItemCode
			,Fld_ID
			,Fld_Value
      FROM
      tt_TEMPTABLE;
      IF(SELECT COUNT(*) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode) > 0 then
		

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
         DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN(SELECT coalesce(numWareHouseItemID,0) FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode);

			-- INSERT NEW WAREHOUSE ATTRIBUTES
         INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized)
         SELECT
         Fld_ID,
				coalesce(CAST(Fld_Value AS VARCHAR(30)),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0
         FROM
         tt_TEMPTABLE
         LEFT JOIN LATERAL(SELECT
            numWareHouseItemID
            FROM
            WareHouseItems
            WHERE
            numDomainID = v_numDomainID
            AND numItemID = v_numItemCode) AS TEMPWarehouse on TRUE;
      end if;
   end if;
   UPDATE WareHouseItems SET numReorder = coalesce(v_numReOrder,0) WHERE numDomainID = v_numDomain AND numItemID = v_numItemCode;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;



