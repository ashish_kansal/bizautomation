-- Stored procedure definition script USP_WareHouseItems_ValidateLocation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WareHouseItems_ValidateLocation(v_numDomainID NUMERIC(18,0),
	v_numWareHouseID NUMERIC(18,0),
	v_numWLocationID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitDuplicate  BOOLEAN DEFAULT 0;
BEGIN
   IF(SELECT
   COUNT(*)
   FROM
   WareHouseItems WI
   WHERE
   WI.numDomainID = v_numDomainID
   AND WI.numItemID = v_numItemCode
   AND WI.numWareHouseID = v_numWareHouseID
   AND coalesce(WI.numWLocationID,0) = coalesce(v_numWLocationID,0)) > 0 then
	
      v_bitDuplicate := true;
   end if;

   open SWV_RefCur for SELECT v_bitDuplicate;
END; $$;













