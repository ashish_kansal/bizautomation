-- Function definition script fn_RegexReplace for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

--sp_configure 'show advanced options', 1;
--GO
--RECONFIGURE with override
--go
--sp_configure 'Ole Automation Procedures',1
--GO
--RECONFIGURE with override

CREATE OR REPLACE FUNCTION fn_RegexReplace(v_pattern VARCHAR(255),
v_replacement VARCHAR(255),
v_Subject TEXT,
v_global BOOLEAN DEFAULT true,
v_Multiline BOOLEAN DEFAULT true)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_objRegexExp  INTEGER;
   v_objErrorObject  INTEGER;
   v_strErrorMessage  VARCHAR(255);
   v_Substituted  VARCHAR(8000);
   v_hr  INTEGER;
   v_Replace  BOOLEAN;
   v_Source  VARCHAR(255);
   v_Description  VARCHAR(255);
   v_Helpfile  VARCHAR(255);
   v_HelpID  INTEGER;
BEGIN
   v_strErrorMessage := 'creating a regex object';
   SELECT * INTO v_hr FROM sp_OACreate('VBScript.RegExp',v_objRegexExp);
   IF v_hr = 0 then
      v_strErrorMessage := 'Setting the Regex pattern';
      v_objErrorObject := v_objRegexExp;
   end if;
   IF v_hr = 0 then
      SELECT * INTO v_hr FROM sp_OASetProperty(v_objRegexExp,'Pattern',v_pattern);
   end if;
   IF v_hr = 0 then /*By default, the regular expression is case sensitive. Set the IgnoreCase property to True to make it case insensitive.*/
      v_strErrorMessage := 'Specifying the type of match';
   end if;
   IF v_hr = 0 then
      SELECT * INTO v_hr FROM sp_OASetProperty(v_objRegexExp,'IgnoreCase',1);
   end if;
   IF v_hr = 0 then
      SELECT * INTO v_hr FROM sp_OASetProperty(v_objRegexExp,'MultiLine',v_Multiline);
   end if;
   IF v_hr = 0 then
      SELECT * INTO v_hr FROM sp_OASetProperty(v_objRegexExp,'Global',v_global);
   end if;
   IF v_hr = 0 then
      v_strErrorMessage := 'Doing a Replacement';
   end if;
   IF v_hr = 0 then
      SELECT * INTO v_hr FROM sp_OAMethod(v_objRegexExp,'Replace',v_Substituted,v_Subject,v_replacement);
   end if;
/*If the RegExp.Global property is False (the default), Replace will return the @subject string with the first regex match (if any) substituted with the replacement text. If RegExp.Global is true, the @Subject string will be returned with all matches replaced.*/
   IF v_hr <> 0 then
      PERFORM sp_OAGetErrorInfo(v_objErrorObject,v_Source,v_Description,v_Helpfile,v_HelpID);
      v_strErrorMessage := 'Error whilst '
      || COALESCE(v_strErrorMessage,'doing something') || ', '
      || COALESCE(v_Description,'');
      RETURN v_strErrorMessage;
   end if;
   PERFORM sp_OADestroy(v_objRegexExp);
   RETURN v_Substituted;
END; $$;

