-- Stored procedure definition script usp_deleteOppurtunitySubStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_deleteOppurtunitySubStage(v_numOppId NUMERIC
	
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Delete from OpportunitySubStageDetails where numOppId = v_numOppId;
   RETURN;
END; $$;


