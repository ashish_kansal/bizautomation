-- Stored procedure definition script USP_SaveImportConfg for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveImportConfg(v_numDomainID NUMERIC,
    v_FormId NUMERIC,
    v_numUserCntId NUMERIC,
    v_StrXml TEXT DEFAULT '',
    v_tintPageType NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	DELETE FROM DycFormConfigurationDetails WHERE numDomainID = v_numDomainID AND numFormID = v_FormId AND tintPageType = v_tintPageType AND coalesce(numRelCntType,0) = 0;

	INSERT  INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intRowNum,
				intColumnNum,
				numUserCntID,
				numDomainID,
				bitCustom,
				tintPageType,
				numRelCntType)
	SELECT  v_FormId,
					X.numFormFieldID,
					X.intRowNum,
					X.intColumnNum,
					NULL,--@numUserCntId,
					v_numDomainID,
					X.bitCustom,
					v_tintPageType,
					0
	FROM
	XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_StrXml AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFormFieldID NUMERIC(9,0) PATH 'numFormFieldID',
					intRowNum NUMERIC(9,0) PATH 'intRowNum',
					intColumnNum NUMERIC(9,0) PATH 'intColumnNum',
					bitCustom BOOLEAN PATH 'bitCustom'
			) AS X;                                     

   RETURN;
END; $$;


