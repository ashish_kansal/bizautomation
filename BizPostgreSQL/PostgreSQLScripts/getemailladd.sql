-- Function definition script GetEmaillAdd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetEmaillAdd(v_numEmailHSTRID NUMERIC,v_tintType SMALLINT)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_EmailAdd  VARCHAR(2000);  
   v_Name  VARCHAR(50);  
   v_Email  VARCHAR(50);  
   v_numEmailID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numEmailID := 0;  
   v_EmailAdd := '';  

   select Distinct  EM.numEmailId, coalesce(EHS.vcName,''), EM.vcEmailId INTO v_numEmailID,v_Name,v_Email from EmailHStrToBCCAndCC  EHS
   join EmailMaster EM on EM.numEmailId =  EHS.numEmailId where numEmailHstrID = v_numEmailHSTRID and tintType = v_tintType    LIMIT 1;  

   while v_numEmailID > 0 LOOP
      v_EmailAdd := coalesce(v_EmailAdd,'') || SUBSTR(Cast(v_numEmailID AS VARCHAR(30)),1,30) || '|' || coalesce(v_Name,'') || '|' || coalesce(v_Email,'') || '~';
      select DISTINCT  EM.numEmailId, coalesce(EHS.vcName,''), EM.vcEmailId INTO v_numEmailID,v_Name,v_Email from EmailHStrToBCCAndCC  EHS
      join EmailMaster EM on EM.numEmailId =  EHS.numEmailId where numEmailHstrID = v_numEmailHSTRID and tintType = v_tintType and EM.numEmailId > v_numEmailID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then
   
         v_numEmailID := 0;
         v_EmailAdd := SUBSTR(v_EmailAdd,1,LENGTH(v_EmailAdd) -1);
      end if;
   END LOOP;  
   
   return v_EmailAdd;  

END; $$;

