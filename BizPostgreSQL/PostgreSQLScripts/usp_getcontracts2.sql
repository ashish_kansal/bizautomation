CREATE OR REPLACE FUNCTION USP_GetContracts2(v_numUserCntID NUMERIC(9,0),
v_numDomainID NUMERIC(18,0),
v_CurrentPage INTEGER,                                                         
v_PageSize INTEGER,
INOUT v_TotRecs INTEGER  ,
v_ClientTimeZoneOffset  INTEGER,         
v_intType INTEGER,      
v_vcRegularSearchCriteria TEXT DEFAULT '',
v_vcCustomSearchCriteria TEXT DEFAULT '',
v_SearchText VARCHAR(300) DEFAULT '', 
INOUT SWV_RefCur refcursor default null,
INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDynamicQuery  TEXT DEFAULT '';
   v_vcStatusQuery  VARCHAR(500) DEFAULT '';
BEGIN
   IF(LENGTH(v_vcRegularSearchCriteria) > 0) then
	
      v_vcRegularSearchCriteria := ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'.vcCompanyName','CI.vcCompanyName');
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'.vcContractsItemClassificationName',
      'C.vcItemClassification');
   end if;
   v_vcDynamicQuery := ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								C.numContractId,
								C.numIncidents,
								CI.vcCompanyName,
								C.numDivisonId AS numDivisionID,
								C.numIncidentLeft,
								C.numHours,
								C.numMinutes,
								C.timeLeft,
								C.vcItemClassification,
								C.numWarrantyDays,
								C.vcNotes,
								C.vcContractNo,
								C.timeUsed,
								COALESCE((
								SELECT
									string_agg(vcData,'','' order by vcData)
								FROM 
									ListDetails 
								WHERE 
									numListID=36
									AND numListItemID IN (SELECT Id FROM SplitIds(C.vcItemClassification,'',''))),'''') AS vcContractsItemClassificationName,
								fn_GetContactName(COALESCE(C.numCreatedBy,0)) || FormatedDateFromDate(C.dtmCreatedOn + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(15)),1,15) || '),''' || CAST(v_numDomainID AS TEXT) || ''') AS CreatedByOn,
								fn_GetContactName(COALESCE(C.numModifiedBy,0)) || FormatedDateFromDate(C.dtmModifiedOn + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(15)),1,15) || '),''' || CAST(v_numDomainID AS TEXT) || ''') AS ModifiedByOn,
								CONCAT(''<a href="javascript:void(0)" onclick="openContractPopup('' || CAST(C.numContractId AS VARCHAR) || '','' || CAST(C.intType AS VARCHAR) || '')">'',COALESCE(C.vcContractNo,''-''),''</a>'') AS "vcContractNo~1~0",
								FormatedDateFromDate(C.dtmCreatedOn + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(15)),1,15) || '),''' || CAST(v_numDomainID AS TEXT) || ''')||'',''||fn_GetContactName(COALESCE(C.numCreatedBy,0)) AS "CreatedByOn~2~0",
								FormatedDateFromDate(C.dtmModifiedOn + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(15)),1,15) || '),''' || CAST(v_numDomainID AS TEXT) || ''')||'',''||fn_GetContactName(COALESCE(C.numModifiedBy,0)) AS "ModifiedByOn~3~0",
								CI.vcCompanyName AS "vcCompanyName~4~0",
								D.tintCRMType,
								COALESCE((
								SELECT
									string_agg(vcData,'','' order by vcData)
								FROM 
									ListDetails 
								WHERE 
									numListID=36
									AND numListItemID IN (SELECT Id FROM SplitIds(C.vcItemClassification,'',''))),'''') AS "vcContractsItemClassificationName~5~0",
								
								(COALESCE(C.numIncidents,0)  - COALESCE(C.numIncidentsUsed,0)) AS "numIncidentLeft~6~0",
								fn_SecondsConversion((((COALESCE(C.numHours,0) * 60 * 60) + (COALESCE(C.numMinutes,0) * 60)) - COALESCE((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0))::INTEGER) AS "timeLeft~7~0",
								C.numWarrantyDays AS "numWarrantyDays~8~0",
								C.numCreatedBy AS numRecOwner,
								D.numTerID
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID=''' || CAST(v_numDomainID AS TEXT) || ''' 
								AND C.intType=' || CAST(v_intType AS TEXT) || ' ' || coalesce(v_vcRegularSearchCriteria,'') || ' ORDER BY C.dtmCreatedOn desc OFFSET ' || CAST(((v_CurrentPage::bigint -1)*v_PageSize::bigint) AS VARCHAR(500)) || ' ROWS FETCH NEXT ' || SUBSTR(CAST(v_PageSize AS VARCHAR(500)),1,500) || ' ROWS ONLY ';

   RAISE NOTICE '%',CAST(v_vcDynamicQuery AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_vcDynamicQuery;

   DROP TABLE IF EXISTS tt_TEMPCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLUMNS
   (
      vcFieldName VARCHAR(200),
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(200),
      bitAllowSorting BOOLEAN,
      numFieldId INTEGER,
      bitAllowEdit BOOLEAN,
      bitCustomField BOOLEAN,
      vcAssociatedControlType VARCHAR(50),
      vcListItemType  VARCHAR(200),
      numListID INTEGER,
      ListRelID INTEGER,
      vcLookBackTableName VARCHAR(200),
      bitAllowFiltering BOOLEAN,
      vcFieldDataType  VARCHAR(20),
      intColumnWidth INTEGER,
      bitClosedColumn BOOLEAN
   );
   INSERT INTO tt_TEMPCOLUMNS(vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn)
		VALUES(CAST('Contract ID' AS VARCHAR(200)),CAST('vcContractNo' AS VARCHAR(200)),CAST('vcContractNo' AS VARCHAR(200)),CAST(0 AS BOOLEAN),1,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),(CAST('Create on, by' AS VARCHAR(200)),CAST('CreatedByOn' AS VARCHAR(200)),CAST('CreatedByOn' AS VARCHAR(200)),CAST(0 AS BOOLEAN),2,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),
   (CAST('Modified on, by' AS VARCHAR(200)),CAST('ModifiedByOn' AS VARCHAR(200)),CAST('ModifiedByOn' AS VARCHAR(200)),CAST(0 AS BOOLEAN),3,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),
   (CAST('Organizations' AS VARCHAR(200)),CAST('vcCompanyName' AS VARCHAR(200)),CAST('vcCompanyName' AS VARCHAR(200)),CAST(0 AS BOOLEAN),4,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(1 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),(CAST('Item Classifications' AS VARCHAR(200)),CAST('vcContractsItemClassificationName' AS VARCHAR(200)),CAST('vcContractsItemClassificationName' AS VARCHAR(200)),CAST(0 AS BOOLEAN),5,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('SelectBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(1 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),
   (CAST('Incidents Left' AS VARCHAR(200)),CAST('numIncidentLeft' AS VARCHAR(200)),CAST('numIncidentLeft' AS VARCHAR(200)),CAST(0 AS BOOLEAN),6,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),
   (CAST('Time Left' AS VARCHAR(200)),CAST('timeLeft' AS VARCHAR(200)),CAST('timeLeft' AS VARCHAR(200)),CAST(0 AS BOOLEAN),7,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN)),(CAST('Days Left' AS VARCHAR(200)),CAST('numWarrantyDays' AS VARCHAR(200)),CAST('numWarrantyDays' AS VARCHAR(200)),CAST(0 AS BOOLEAN),8,CAST(0 AS BOOLEAN),CAST(0 AS BOOLEAN),CAST('TextBox' AS VARCHAR(50)),CAST('' AS VARCHAR(200)),0,0,CAST('Contracts' AS VARCHAR(200)),CAST(0 AS BOOLEAN),CAST('V' AS VARCHAR(20)),CAST(0 AS BOOLEAN));

	
   UPDATE
   tt_TEMPCOLUMNS TC
   SET
   intColumnWidth = coalesce(DFCD.intColumnWidth,0)
   FROM
   DycFormConfigurationDetails DFCD
   WHERE
   TC.numFieldId = DFCD.numFieldID AND(DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 146
   AND DFCD.tintPageType = 1);

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPCOLUMNS;
   RETURN;
	
END; $$;


