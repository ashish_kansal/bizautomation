-- Stored procedure definition script USP_AddAPIOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddAPIOpportunity(v_numDomainId	NUMERIC(18,0),
v_WebApiId	INTEGER,
v_numOppId	NUMERIC(18,0),
v_vcAPIOppId	VARCHAR(50),
v_numCreatedby	NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO OpportunityMasterAPI(WebApiId
           ,numDomainId
           ,numOppId
           ,vcAPIOppId
           ,numCreatedby
           ,dtCreated
           ,numModifiedby
           ,dtModified)
     VALUES(v_WebApiId,
           v_numDomainId,
           v_numOppId,
           v_vcAPIOppId,
           v_numCreatedby,
           TIMEZONE('UTC',now()),
           v_numCreatedby,
           TIMEZONE('UTC',now()));
RETURN;
END; $$;


