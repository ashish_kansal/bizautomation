-- Stored procedure definition script USP_OPPItemDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OPPItemDetails(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
      v_OpportunityId NUMERIC(9,0) DEFAULT 0,
      v_numDomainId NUMERIC(9,0) DEFAULT NULL,
      v_ClientTimeZoneOffset  INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);
   v_TaxPercentage  DOUBLE PRECISION;
   v_tintOppType  SMALLINT;
   v_fld_Name  VARCHAR(500);
   v_strSQL  TEXT;
   v_fld_ID  NUMERIC(9,0);
   v_numBillCountry  NUMERIC(9,0);
   v_numBillState  NUMERIC(9,0);
   v_avgCost  INTEGER;
   v_tintCommitAllocation  SMALLINT;
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   IF v_OpportunityId > 0 then
      select   numDivisionId, tintopptype INTO v_DivisionID,v_tintOppType FROM    OpportunityMaster WHERE   numOppId = v_OpportunityId;
      drop table IF EXISTS tt_TEMPFORM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFORM
      (
         tintOrder SMALLINT,
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldName VARCHAR(50),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC(9,0),
         vcLookBackTableName VARCHAR(50),
         bitCustomField BOOLEAN,
         numFieldId NUMERIC,
         bitAllowSorting BOOLEAN,
         bitAllowEdit BOOLEAN,
         bitIsRequired BOOLEAN,
         bitIsEmail BOOLEAN,
         bitIsAlphaNumeric BOOLEAN,
         bitIsNumeric BOOLEAN,
         bitIsLengthValidation BOOLEAN,
         intMaxLength INTEGER,
         intMinLength INTEGER,
         bitFieldMessage BOOLEAN,
         vcFieldMessage VARCHAR(500),
         ListRelID NUMERIC(9,0),
         intColumnWidth INTEGER,
         intVisible INTEGER,
         vcFieldDataType CHAR(1)
      );
      IF(SELECT
      COUNT(*)
      FROM
      View_DynamicColumns DC
      WHERE
      DC.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND
      coalesce(DC.bitSettingField,false) = true AND coalesce(DC.bitDeleted,false) = false AND coalesce(DC.bitCustom,false) = false) > 0
      OR(SELECT
      COUNT(*)
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID = v_numUserCntID AND numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0 AND coalesce(bitCustom,false) = true) > 0 then
		
         INSERT INTO tt_TEMPFORM
         select coalesce(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,coalesce(DDF.vcCultureFieldName,DDF.vcFieldName),
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName
		,CAST(COALESCE(DDF.bitCustom,0) AS BOOLEAN),DDF.numFieldID,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage AS vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
         FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldID = DC.numFieldID and DC.numUserCntID = v_numUserCntID and DC.numDomainID = v_numDomainId and numRelCntType = 0 AND tintPageType = 1
         where DDF.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID = v_numDomainId AND coalesce(DDF.bitSettingField,false) = true AND coalesce(DDF.bitDeleted,false) = false AND coalesce(DDF.bitCustom,0) = 0
         UNION
         select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,
		 vcAssociatedControlType,'' as vcListItemType,numListID,''
		,bitCustom,numFieldId,bitAllowSorting,false as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,CAST(1 AS INTEGER),''
         from View_DynamicCustomColumns
         where numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID = v_numUserCntID and numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0
         AND coalesce(bitCustom,false) = true;
      ELSE
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			
         select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
			
            v_IsMasterConfAvailable := true;
         end if;

			--If MasterConfiguration is available then load it otherwise load default columns
         IF v_IsMasterConfAvailable = true then
			
            INSERT INTO tt_TEMPFORM
            SELECT(intRowNum+1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
            UNION
            SELECT
            tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,
					 vcAssociatedControlType,'' as vcListItemType,numListID,''
					,bitCustom,numFieldId,bitAllowSorting,false as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
            FROM
            View_DynamicCustomColumnsMasterConfig
            WHERE
            View_DynamicCustomColumnsMasterConfig.numFormId =(CASE WHEN v_tintOppType = 2 THEN 129 ELSE 26 END) AND
            View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
            coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
            ORDER BY
            tintOrder ASC;
         end if;
      end if;
      v_strSQL := '';
      select   coalesce(numCost,0), coalesce(tintCommitAllocation,1) INTO v_avgCost,v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainId;

--------------Make Dynamic Query--------------
      v_strSQL := coalesce(v_strSQL,'') || CONCAT('
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(coalesce(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost,
coalesce(Opp.bitItemPriceApprovalRequired,false) AS bitItemPriceApprovalRequired,
coalesce(Opp.numSOVendorID,0) AS numVendorID,',
      CONCAT('COALESCE(Opp.monTotAmount,0) - (COALESCE(Opp.numUnitHour,0) * (CASE ',v_avgCost,
      ' WHEN 3 THEN COALESCE(V.monCost,0) / fn_UOMConversion(numPurchaseUnit,I.numItemCode,',v_numDomainId,',numBaseUnit) WHEN 2 THEN (CASE WHEN COALESCE(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE COALESCE(V.monCost,0) / fn_UOMConversion(numPurchaseUnit,I.numItemCode,',v_numDomainId,
      ',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
      ,'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
coalesce(I.bitAsset,false) as bitAsset,
coalesce(I.bitRental,false) as bitRental,
coalesce(I.bitSerialized,false) AS bitSerialized,
coalesce(I.bitLotNo,false) AS bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
coalesce(Opp.bitDiscountType,false) AS bitDiscountType,
fn_UOMConversion(Opp.numUOMId,Opp.numItemCode,',SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30),',NULL) AS UOMConversionFactor,
coalesce(Opp.bitWorkOrder,false) AS bitWorkOrder,
coalesce(Opp.numUnitHourReceived,0) AS numUnitHourReceived,
coalesce((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId = Opp.numOppID AND numOppItemID = Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
					AND (OB.bitAuthoritativeBizDocs = 1 OR OB.numBizDocId = 304) AND OBI.numOppItemID = Opp.numoppitemtCode AND OB.numOppId = Opp.numOppId) > 0 THEN true ELSE false END  AS bitIsAuthBizDoc,
(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID = Opp.numoppitemtCode AND OB.numBizDocId = 296) > 0 THEN true ELSE false END) AS bitAddedFulFillmentBizDoc,
coalesce(numPromotionID,0) AS numPromotionID,
coalesce((SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID),0) AS numReturnID,
fn_GetReturnItemQty(Opp.numoppitemtCode::BIGINT) AS ReturrnedQty,
fn_GetReturnStatus(Opp.numoppitemtCode::BIGINT) AS ReturnStatus,
coalesce(opp.numUOMId,0) AS numUOM,
(SELECT vcUnitName FROM UOM WHERE numUOMId = opp.numUOMId AND coalesce(bitEnabled,false) = true)  AS  vcBaseUOMName,
Opp.numUnitHour AS numOriginalUnitHour, 
coalesce(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN coalesce(bitKitParent,false) = true
                               AND coalesce(bitAssembly,false) = true THEN 0
                          WHEN coalesce(bitKitParent,false) = true THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE 
	WHEN (I.charItemType = ''p'' AND coalesce(bitDropShip,false) = false AND coalesce(bitAsset,false) = false) 
	THEN CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,       (CASE WHEN coalesce(bitKitParent,false) = true
                               AND coalesce(bitAssembly,false) = true THEN false
                          WHEN coalesce(bitKitParent,false) = true THEN true
                          ELSE false
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM ListDetails WHERE 1 = 1 AND numListItemID = numShipClass) AS numShipClass,coalesce(Opp.numQtyShipped,0) AS numQtyShipped,coalesce(Opp.numUnitHourReceived,0) AS numUnitHourReceived,coalesce(OM.fltExchangeRate,0) AS fltExchangeRate,coalesce(OM.numCurrencyID,0) AS numCurrencyID,coalesce(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = false
	THEN(CASE WHEN OM.tintShipped = 1 THEN CAST(coalesce(Opp.numUnitHour,0) AS VARCHAR(30)) ELSE CAST(coalesce(Opp.numQtyShipped,0) AS VARCHAR(30)) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = false
	THEN(CASE WHEN OM.tintShipped = 1 THEN CAST(coalesce(Opp.numUnitHour,0) AS VARCHAR(30)) ELSE CAST(coalesce(Opp.numUnitHourReceived,0) AS VARCHAR(30)) END)
	ELSE '''' END) AS vcShippedReceived,
coalesce(i.numIncomeChartAcntId,0) as itemIncomeAccount,coalesce(i.numAssetChartAcntId,0) as itemInventoryAsset,coalesce(i.numCOGsChartAcntId,0) as itemCoGs,
coalesce(Opp.numRentalIN,0) as numRentalIN,
coalesce(Opp.numRentalLost,0) as numRentalLost,
coalesce(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM ListDetails WHERE 1 = 1 AND numListItemID = numShipClass) AS numShipClass,coalesce(bitFreeShipping,false) AS IsFreeShipping,Opp.bitDiscountType,coalesce(Opp.monLandedCost,0) as monLandedCost, coalesce(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND coalesce(bitDropShip,false) = false THEN coalesce(FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, ');

--	IF @fld_Name = 'vcPathForTImage' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcPathForTImage,';
--	ELSE IF @fld_Name = 'vcItemName' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcItemName,';
--	ELSE IF @fld_Name = 'vcModelID,' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcModelID,';
--	ELSE IF @fld_Name = 'vcWareHouse' 
      v_strSQL := coalesce(v_strSQL,'') || 'vcWarehouse,WItems.numWarehouseID,';
      v_strSQL := coalesce(v_strSQL,'') || 'WItems.numOnHand,coalesce(WItems.numAllocation,0) as numAllocation,';
      v_strSQL := coalesce(v_strSQL,'') || 'WL.vcLocation,';
--	ELSE IF @fld_Name = 'ItemType' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcType AS ItemType,Opp.vcType,';
--	ELSE IF @fld_Name = 'vcItemDesc' 
      v_strSQL := coalesce(v_strSQL,'') || 'coalesce(Opp.vcItemDesc,'''') AS vcItemDesc,';
--	ELSE IF @fld_Name = 'Attributes' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcAttributes,'; --fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
      v_strSQL := coalesce(v_strSQL,'') || 'coalesce(bitDropShip,false) as DropShip,CASE coalesce(bitDropShip,false) WHEN true THEN ''Yes'' ELSE '''' END AS bitDropShip,';
--	ELSE IF @fld_Name = 'numUnitHour' 
      v_strSQL := coalesce(v_strSQL,'') || 'CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,coalesce(opp.numUOMId,0))*Opp.numUnitHour) AS NUMERIC(18,2)) AS numUnitHour,';
--	ELSE IF @fld_Name = 'monPrice' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.monPrice,';
--	ELSE IF @fld_Name = 'monPriceUOM' 
      v_strSQL := coalesce(v_strSQL,'') || 'CAST((fn_UOMConversion(coalesce(opp.numUOMId,0),Opp.numItemCode,om.numDomainId,coalesce(I.numBaseUnit,0))*Opp.monPrice) AS NUMERIC(18,4)) AS monPriceUOM,';
--	ELSE IF @fld_Name = 'monTotAmount' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,';
--	ELSE IF @fld_Name = 'vcSKU' 
      v_strSQL := coalesce(v_strSQL,'') || 'coalesce(WItems.vcWHSKU,coalesce(I.vcSKU,'''')) AS vcSKU,';
--	ELSE IF @fld_Name = 'vcManufacturer' 
      v_strSQL := coalesce(v_strSQL,'') || 'Opp.vcManufacturer,';
--	ELSE IF @fld_Name = 'vcItemClassification' 
      v_strSQL := coalesce(v_strSQL,'') || ' fn_GetListItemName(I.numItemClassification) AS numItemClassification,';
--	ELSE IF @fld_Name = 'numItemGroup' 
      v_strSQL := coalesce(v_strSQL,'') || '(SELECT    vcItemGroup FROM      ItemGroups WHERE     numItemGroupID = I.numItemGroup) AS numItemGroup,';
--	ELSE IF @fld_Name = 'vcUOMName' 
      v_strSQL := coalesce(v_strSQL,'') || 'coalesce(u.vcUnitName,'''') AS numUOMId,';
--	ELSE IF @fld_Name = 'fltWeight' 
      v_strSQL := coalesce(v_strSQL,'') || 'I.fltWeight,';
--	ELSE IF @fld_Name = 'fltHeight' 
      v_strSQL := coalesce(v_strSQL,'') || 'I.fltHeight,';
--	ELSE IF @fld_Name = 'fltWidth' 
      v_strSQL := coalesce(v_strSQL,'') || 'I.fltWidth,';
--	ELSE IF @fld_Name = 'fltLength' 
      v_strSQL := coalesce(v_strSQL,'') || 'I.fltLength,';
--	ELSE IF @fld_Name = 'numBarCodeId' 
      v_strSQL := coalesce(v_strSQL,'') || 'I.numBarCodeId,';
      v_strSQL := coalesce(v_strSQL,'') || 'I.IsArchieve,coalesce(I.numContainer,0) AS numContainer,coalesce(I.numNoItemIntoContainer,0) AS numContainerQty,';
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
      v_strSQL := coalesce(v_strSQL,'') || CONCAT('GetWorkOrderStatus(OM.numDomainID,',v_tintCommitAllocation,'::SMALLINT,Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,');
--	ELSE IF @fld_Name = 'DiscAmt' 
      v_strSQL := coalesce(v_strSQL,'') || 'CASE WHEN Opp.bitDiscountType = false
				 THEN TO_CHAR(COALESCE(Opp.monTotAmtBefDiscount, Opp.monTotAmount) - Opp.monTotAmount,''999,999,999,999,990.00999'') || '' ('' || TO_CHAR(Opp.fltDiscount,''999,999,999,999,990.00999'') || ''%)''
				 ELSE TO_CHAR(COALESCE(Opp.monTotAmtBefDiscount, Opp.monTotAmount) - Opp.monTotAmount,''999,999,999,999,990.00999'')
			END AS DiscAmt,';	
--	ELSE IF @fld_Name = 'numProjectID' 
      v_strSQL := coalesce(v_strSQL,'') || ' COALESCE(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT 
								COALESCE(OBI.numOppBizDocItemID, 0)
						FROM    OpportunityBizDocs OBD
								INNER JOIN OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE  OBD.numOppID=OM.numOppID AND COALESCE(OBD.bitAuthoritativeBizDocs, 0) = 1 AND OBI.numOppItemID = Opp.numoppitemtCode
					  LIMIT 1) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,';	
--	ELSE IF @fld_Name = 'numClassID' 
      v_strSQL := coalesce(v_strSQL,'') || 'COALESCE(Opp.numClassID, 0) numClassID,';	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
      v_strSQL := coalesce(v_strSQL,'') || ' COALESCE(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,';
      IF EXISTS(SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName = 'vcInclusionDetails') then
	   
         v_strSQL := coalesce(v_strSQL,'') || CONCAT('GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',v_tintCommitAllocation,'::SMALLINT,false) AS vcInclusionDetails,');
      end if;     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
      select   vcFieldName, numFieldId INTO v_fld_Name,v_fld_ID FROM tt_TEMPFORM WHERE coalesce(bitCustomField,false) = true   ORDER BY numFieldId LIMIT 1;
      WHILE v_fld_ID > 0 LOOP ------------------

         RAISE NOTICE '%',v_fld_Name;
         v_strSQL := coalesce(v_strSQL,'') || 'GetCustFldValueOppItems('
         || SUBSTR(CAST(v_fld_ID AS VARCHAR(15)),1,15)
         || ',Opp.numoppitemtCode,Opp.numItemCode) as "' || coalesce(v_fld_Name,'') || '",';
         select   vcFieldName, numFieldId INTO v_fld_Name,v_fld_ID FROM tt_TEMPFORM WHERE coalesce(bitCustomField,false) = true AND numFieldId > v_fld_ID   ORDER BY numFieldId LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_fld_ID := 0;
         end if;
      END LOOP; ------------------


                                
--Purchase Opportunity ?
      IF v_tintOppType = 2 then
    
         v_strSQL :=  coalesce(v_strSQL,'') || 'COALESCE((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  COALESCE(( SELECT string_agg(CONCAT(vcSerialNo
                        ,CASE WHEN COALESCE(I.bitLotNo, false) = true
                                THEN CONCAT('' (''
                                    ,COALESCE(oppI.numQty,0)
                                    ,'')'')
                                ELSE ''''
                            END),'', '' ORDER BY vcSerialNo) 
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ),'''') AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM UOM WHERE numUOMId = opp.numUOMId AND COALESCE(bitEnabled,false) = true)  AS vcBaseUOMName,
		  I.bitSerialized,
		  COALESCE(I.bitLotNo, false) AS bitLotNo,
		  false AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,';
      ELSE
         v_strSQL :=  coalesce(v_strSQL,'') || ' COALESCE((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  string_agg(CONCAT(vcSerialNo
                        , CASE WHEN COALESCE(I.bitLotNo, false) = true
                                THEN CONCAT('' (''
                                    ,COALESCE(oppI.numQty,0)
                                    ,'')'')
                                ELSE ''''
                            END),'', '' ORDER BY vcSerialNo)
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ),'''') AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  COALESCE(I.bitLotNo, false) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,';
      end if;
      v_strSQL := SUBSTR(v_strSQL,1,LENGTH(v_strSQL) -1) || '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || '
        AND Opp.numOppId = ' || SUBSTR(CAST(v_OpportunityId AS VARCHAR(15)),1,15) || '
ORDER BY opp.numSortOrder,numoppitemtCode ASC ';
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;

      open SWV_RefCur2 for
      SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWareHouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        fn_GetAttributes(W.numWareHouseItmsDTLID,1::BOOLEAN) AS Attributes
      FROM    WareHouseItmsDTL W
      JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
      WHERE   numOppID = v_OpportunityId;

      open SWV_RefCur3 for
      select * from tt_TEMPFORM order by tintOrder;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/



