-- Stored procedure definition script USP_OrganizationRatingRule_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OrganizationRatingRule_Delete(v_numDomainID NUMERIC(18,0)
	,v_numORRID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID AND numORRID = v_numORRID;
   RETURN;
END; $$;

