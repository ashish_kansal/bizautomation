CREATE OR REPLACE FUNCTION USP_GetPartnerContacts
(
	p_numDomainID NUMERIC DEFAULT 0
	,p_numOppID NUMERIC DEFAULT 0
	,p_DivisionID NUMERIC DEFAULT 0
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF(p_numOppID>0 AND p_DivisionID=0) THEN
		SELECT numPartner INTO p_DivisionID FROM OpportunityMaster where numOppID=p_numOppID and numDomainId=p_numDomainID;
	END IF;

	OPEN SWV_RefCur FOR
	SELECT A.numContactId AS "numContactId",A.vcGivenName AS "vcGivenName" FROM AdditionalContactsInformation AS A 
	LEFT JOIN DivisionMaster AS D 
	ON D.numDivisionID=A.numDivisionId
	WHERE D.numDomainID=p_numDomainID AND D.numDivisionID=p_DivisionID AND A.vcGivenName<>'';
   RETURN;
END; $$;