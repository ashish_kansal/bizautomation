CREATE OR REPLACE FUNCTION USP_GetPerAnaylsCaseList
(
	v_numUserCntId NUMERIC(9,0) DEFAULT 0,                                              
	v_numDomainID NUMERIC(9,0) DEFAULT 0,                                  
	v_dtStartDate TIMESTAMP DEFAULT NULL,                                   
	v_dtEndDate TIMESTAMP DEFAULT NULL,                                    
	v_RepType SMALLINT DEFAULT NULL,                                  
	v_Condition CHAR(1) DEFAULT NULL,                                       
	v_CurrentPage INTEGER DEFAULT NULL,                                      
	v_PageSize INTEGER DEFAULT NULL,                                      
	INOUT v_TotRecs INTEGER  DEFAULT NULL,                                      
	v_columnName VARCHAR(50) DEFAULT NULL,                                      
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                
	v_strUserIDs VARCHAR(1000) DEFAULT NULL,                  
	v_TeamType SMALLINT DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSql TEXT;
	v_firstRec INTEGER;                                      
	v_lastRec INTEGER;
BEGIN
	if COALESCE(v_strUserIDs,'') = '' then 
		v_strUserIDs := '0';
	end if;                                           
	
	if v_RepType <> 3 then
		BEGIN
			CREATE TEMP SEQUENCE tt_tempTable_seq;
			EXCEPTION WHEN OTHERS THEN
			NULL;
		END;

		drop table IF EXISTS tt_GetPerAnaylsCaseList CASCADE;
		Create TEMPORARY TABLE tt_GetPerAnaylsCaseList 
		( 
		   ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
		   Name VARCHAR(110),
		   vcCompanyName VARCHAR(100),
		   numCaseID VARCHAR(15),
		   vcCasenumber VARCHAR(25),
		   intTargetResolveDate TEXT,
		   numcreatedby VARCHAR(15),
		   textSubject TEXT,
		   Priority VARCHAR(50),
		   numCompanyID VARCHAR(15),
		   numDivisionID VARCHAR(15),
		   numTerID VARCHAR(15),
		   tintCRMType  VARCHAR(10),
		   ActCntID VARCHAR(15),
		   CaseOwner VARCHAR(110)
		);
    
		v_strSql := 'SELECT                                         
						CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) as Name,                                      
						CMP.vcCompanyName,                                        
						Cs.numCaseID,                                        
						Cs.vcCaseNumber,                                        
						Cs.intTargetResolveDate,                                        
						Cs.numRecOwner as numCreatedBy,                                        
						Cs.textSubject,                                        
						lst.vcdata as Priority,                                            
						CMP.numCompanyID,                                        
						Div.numDivisionID,                                        
						Div.numTerId,                                        
						Div.tintCRMType,                                        
						ADC.numContactID as ActCntID,                                      
						CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) as CaseOwner                                      
					FROM                                         
						AdditionalContactsInformation ADC                                       
					JOIN 
						Cases Cs                                        
					ON 
						ADC.numContactId =Cs.numRecOwner                                       
					JOIN 
						DivisionMaster Div                                         
					ON 
						ADC.numDivisionId = Div.numDivisionID                                         
					JOIN 
						CompanyInfo CMP                                       
					ON 
						Div.numCompanyID = CMP.numCompanyId                                       
					left join 
						listdetails lst                                      
					on 
						lst.numListItemID= cs.numPriority                                      
					Where';                               
                          
		if v_Condition = '0' then 
			v_Condition := 'O';
		end if;
	
		if v_Condition = 'O' and v_RepType = 1 then 
			v_strSql := coalesce(v_strSql,'') 
						|| ' Cs.bintCreatedDate between ''' || v_dtStartDate || ''' and ''' || v_dtEndDate || ''' 
						and Cs.numRecOwner in (select 
													numContactId 
												from 
													AdditionalContactsInformation 
												where 
													numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntId,0) || ' and numDomainID=' || COALESCE(v_numDomainID,0) || ' and tintType=' || COALESCE(v_TeamType,0) || ')) ';
		end if;
		if v_Condition = 'O' and v_RepType = 2 then 
			 v_strSql := coalesce(v_strSql,'') 
						|| ' Cs.bintCreatedDate between ''' || v_dtStartDate || ''' and ''' || v_dtEndDate || ''' 
						and Cs.numRecOwner =' || COALESCE(v_numUserCntId,0);
		end if;                                  
                                  
	-- cases Completed                                  
		if v_Condition = 'C' and v_RepType = 1 then 
			v_strSql := coalesce(v_strSql,'') 
						|| ' Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' and Cs.numStatus=136                                   
						and Cs.numModifiedBy in (select numContactId from AdditionalContactsInformation                     
						where numTeam in(select numTeam from ForReportsByTeam where                               
						numUserCntID=' || COALESCE(v_numUserCntId,0) || ' and numDomainID=' || COALESCE(v_numDomainID,0) || ' and tintType=' || COALESCE(v_TeamType,0) || ')) ';
		end if;
		if v_Condition = 'C' and v_RepType = 2 then 
			 v_strSql := coalesce(v_strSql,'') 
						|| ' Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' and Cs.numStatus=136 and Cs.numModifiedBy =' || COALESCE(v_numUserCntId,0);
		end if;                                  
                      
		-- cases Completed                                  
		if v_Condition = 'P' and v_RepType = 1 then 
			 v_strSql := coalesce(v_strSql,'') || ' Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' and numStatus<>136        
						and intTargetResolvedate< ''' || TIMEZONE('UTC',now()) || '''                                   
						and Cs.numRecOwner in (select numContactId from AdditionalContactsInformation                                     
						where numTeam in(select numTeam from ForReportsByTeam where                               
						numUserCntID=' || COALESCE(v_numUserCntId,0) || ' and numDomainID=' || COALESCE(v_numDomainID,0) || ' and tintType=' || COALESCE(v_TeamType,0) || '))';
		end if;
      
		if v_Condition = 'P' and v_RepType = 2 then 
			 v_strSql := coalesce(v_strSql,'') || ' Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' and numStatus<>136                                   
			and intTargetResolvedate< ''' || TIMEZONE('UTC',now()) || '''        
			and Cs.numRecOwner =' || COALESCE(v_numUserCntId,0);
		end if;

		RAISE NOTICE '%',(v_strSql);

		EXECUTE 'insert into tt_GetPerAnaylsCaseList 
					(Name,                                      
					vcCompanyName ,                                      
					numCaseID,                                      
					vcCaseNumber,                                      
					intTargetResolveDate,                                      
					numCreatedBy,                                      
					textSubject,                                      
					Priority,                                      
					numCompanyID,                                      
					numDivisionID,                                      
					numTerId,                      
					tintCRMType,                                      
					ActCntID,                                      
					CaseOwner)' || v_strSql;

		v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
		v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      
		open SWV_RefCur for select * from tt_GetPerAnaylsCaseList where ID > v_firstRec and ID < v_lastRec;
    
		select count(*) INTO v_TotRecs from tt_GetPerAnaylsCaseList;
	end if;                                
                                
   if v_RepType = 3 then
		v_strSql := 'SELECT 
						ADC1.numContactId as numUserID
						,COALESCE(vcFirstName,''-'') as vcFirstName
						,COALESCE(vcLastName,''-'') as vcLastName
						,(SELECT count(*) FROM  Cases Cs ';

		if v_Condition = '0' then  
			v_strSql := coalesce(v_strSql,'') 
						|| ' where Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' 
						and Cs.numRecOwner  =ADC1.numContactId) as NoofOpp 
						FROM  AdditionalContactsInformation ADC1 WHERE ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
		end if;
		if v_Condition = 'C' then 
			v_strSql := coalesce(v_strSql,'') 
						|| 'where Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' 
						and Cs.numStatus=136 and Cs.numModifiedBy   =ADC1.numContactId) as NoofOpp 
						FROM AdditionalContactsInformation ADC1 WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
		end if;
		if v_Condition = 'P' then 
			v_strSql := coalesce(v_strSql,'') 
						|| 'where Cs.bintCreatedDate between ''' || v_dtStartDate || '''  and ''' || v_dtEndDate || ''' 
						and numStatus <> 136 
						and intTargetResolvedate < ''' || TIMEZONE('UTC',now()) || ''' 
						and Cs.numRecOwner =ADC1.numContactId) as NoofOpp FROM   AdditionalContactsInformation ADC1                                   
						WHERE ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
		end if;
		
		RAISE NOTICE '%',v_strSql;
		
		OPEN SWV_RefCur FOR EXECUTE v_strSql;
	end if;

	RETURN;
END; $$;


