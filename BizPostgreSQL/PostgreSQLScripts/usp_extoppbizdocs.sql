-- Stored procedure definition script USP_ExtOppBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ExtOppBizDocs(v_numOppId NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  opp.numOppBizDocsId,
 opp.numoppid,
 opp.numBizDocId,
 oppmst.vcpOppName,
 cast(mst.vcData as VARCHAR(255)) as BizDoc,
 opp.numCreatedBy,
opp.dtCreatedDate AS CreatedDate,
 vcBizDocID
   from
   OpportunityBizDocs  opp
   left join Listdetails mst
   on mst.numListItemID = opp.numBizDocId
   join OpportunityMaster oppmst
   on oppmst.numOppId = opp.numoppid
   left join PortalBizDocs P
   on P.numBizDocID = opp.numBizDocId
   where opp.numoppid = v_numOppId;
END; $$;












