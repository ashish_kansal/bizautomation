DROP FUNCTION IF EXISTS USP_Domain_GetColumnValue;
CREATE OR REPLACE FUNCTION USP_Domain_GetColumnValue(v_numDomainID NUMERIC(18,0),
	v_vcDbColumnName VARCHAR(300), INOUT SWV_RefCur refcursor )
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcDbColumnName = 'tintCommitAllocation' then
	
      open SWV_RefCur for
      SELECT coalesce(tintCommitAllocation,1) AS "tintCommitAllocation" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitEDI'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitEDI,false) AS "bitEDI" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitReOrderPoint'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitReOrderPoint,false) AS "bitReOrderPoint" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'tintMarkupDiscountOption'
   then
	
      open SWV_RefCur for
      SELECT coalesce(tintMarkupDiscountOption,0) AS "tintMarkupDiscountOption" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'tintMarkupDiscountValue'
   then
	
      open SWV_RefCur for
      SELECT coalesce(tintMarkupDiscountValue,0) AS "tintMarkupDiscountValue" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitDoNotShowDropshipPOWindow'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitDoNotShowDropshipPOWindow,false) AS "bitDoNotShowDropshipPOWindow" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'tintReceivePaymentTo'
   then
	
      open SWV_RefCur for
      SELECT coalesce(tintReceivePaymentTo,0) AS "tintReceivePaymentTo" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'numReceivePaymentBankAccount'
   then
	
      open SWV_RefCur for
      SELECT coalesce(numReceivePaymentBankAccount,0) AS "numReceivePaymentBankAccount" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitCloneLocationWithItem'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitCloneLocationWithItem,false) AS "bitCloneLocationWithItem" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'vcBluePaySuccessURL'
   then
	
      open SWV_RefCur for
      SELECT coalesce(vcBluePaySuccessURL,'') AS "vcBluePaySuccessURL" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'vcBluePayDeclineURL'
   then
	
      open SWV_RefCur for
      SELECT coalesce(vcBluePayDeclineURL,'') AS "vcBluePayDeclineURL" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitUseDeluxeCheckStock'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitUseDeluxeCheckStock,false) AS "bitUseDeluxeCheckStock" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'vcPOHiddenColumns'
   then
	
      open SWV_RefCur for
      SELECT coalesce(vcPOHiddenColumns,'') AS "vcPOHiddenColumns" FROM SalesOrderConfiguration WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'tintReorderPointBasedOn'
   then
	
      open SWV_RefCur for
      SELECT coalesce(tintReorderPointBasedOn,1) AS "tintReorderPointBasedOn" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'intReorderPointDays'
   then
	
      open SWV_RefCur for
      SELECT coalesce(intReorderPointDays,30) AS "intReorderPointDays" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'intReorderPointPercent'
   then
	
      open SWV_RefCur for
      SELECT coalesce(intReorderPointPercent,0) AS "intReorderPointPercent" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitPurchaseTaxCredit'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitPurchaseTaxCredit,false) AS "intReorderPointPercent" FROM Domain WHERE numDomainId = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'vcDefaultCostRange'
   then
	
      open SWV_RefCur for
      SELECT vcDefaultCostRange AS "vcDefaultCostRange" FROM Domain WHERE numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


