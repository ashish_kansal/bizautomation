-- Stored procedure definition script USP_GetContactFromEmail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactFromEmail(v_Email VARCHAR(50) ,    
v_DomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select adc.numContactId,adc.numDivisionId ,DM.tintCRMType
   from AdditionalContactsInformation adc
   join DivisionMaster DM on adc.numDivisionId = DM.numDivisionID
   where adc.vcEmail ilike '%' || LTRIM(RTRIM(v_Email)) || '%' and adc.numDomainID = v_DomainId;
END; $$;












