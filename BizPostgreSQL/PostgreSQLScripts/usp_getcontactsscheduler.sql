-- FUNCTION: public.usp_getcontactsscheduler(numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getcontactsscheduler(numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcontactsscheduler(
	v_numdivisionid numeric,
	v_numdomainid numeric,
	v_numscheduleid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:06:06 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for select
   vcFirstName || ' ' || vcLastname as Name,
	vcEmail as  Email,
	numContactId
   FROM AdditionalContactsInformation AC join
   DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
   WHERE AC.numDivisionId = v_numDivisionId   and
   AC.numDomainID = v_numDomainId
   and numContactId not in(select numcontactId from CustRptSchContacts where numScheduleId = v_numScheduleid);
END;
$BODY$;

ALTER FUNCTION public.usp_getcontactsscheduler(numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
