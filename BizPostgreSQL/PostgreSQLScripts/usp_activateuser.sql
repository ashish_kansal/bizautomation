-- Stored procedure definition script usp_ActivateUser for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ActivateUser(v_intUserId INTEGER,
	v_bitActivate BOOLEAN 
--  

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	--Update ActivationStatus into Table  
   Update UserMaster set bitactivateflag = v_bitActivate
   WHERE numUserId = v_intUserId;
   RETURN;
END; $$;


