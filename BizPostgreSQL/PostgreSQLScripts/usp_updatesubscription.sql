-- Stored procedure definition script USP_UpdateSubscription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateSubscription(v_UserId NUMERIC,
v_strSubscriptionId VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update UserMaster set SubscriptionId = v_strSubscriptionId where numUserId = v_UserId;
   RETURN;
END; $$;


