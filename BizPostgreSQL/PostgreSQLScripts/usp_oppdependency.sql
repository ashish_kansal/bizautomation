-- Stored procedure definition script USP_OPPDependency for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OPPDependency(v_byteMode SMALLINT DEFAULT null,
v_ProcessId NUMERIC(9,0) DEFAULT null,
v_Percentage NUMERIC(9,0) DEFAULT null, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select numOppId,vcpOppName from OpportunityMaster;
   end if;
----OppMilestone
   if v_byteMode = 1 then

      open SWV_RefCur for
      select dtl.numStagePercentage,'Milestone - ' || CAST(dtl.numStagePercentage AS VARCHAR(4)) || '%' as numstagepercentageText from OpportunityStageDetails dtl
      where  dtl.numStagePercentage != 100 and dtl.numStagePercentage != 0 and dtl.numoppid = v_ProcessId
      group by dtl.numStagePercentage;
   end if;

--OppStageDetails
   if v_byteMode = 2 then

      open SWV_RefCur for
      select numOppStageId,vcstagedetail from OpportunityStageDetails where numoppid = v_ProcessId and numStagePercentage = v_Percentage;
   end if;

---ProjectMilestone
   if v_byteMode = 3 then

      open SWV_RefCur for
      select dtl.numstagepercentage,'Milestone - ' || CAST(dtl.numstagepercentage AS VARCHAR(4)) || '%' as numstagepercentageText from ProjectsStageDetails dtl
      where  dtl.numstagepercentage != 100 and dtl.numstagepercentage != 0 and dtl.numProId = v_ProcessId
      group by dtl.numstagepercentage;
   end if;

--ProStageDetails
   if v_byteMode = 4 then

      open SWV_RefCur for
      select numProStageId as numOppStageId,vcstagedetail from ProjectsStageDetails where numProId = v_ProcessId and numstagepercentage = v_Percentage;
   end if;
   RETURN;
END; $$;


