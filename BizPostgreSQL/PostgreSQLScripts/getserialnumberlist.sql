-- Function definition script GetSerialNumberList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetSerialNumberList(v_numOppItemtCode NUMERIC)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SerialList  VARCHAR(2000);
BEGIN
   select string_agg(coalesce(vcSerialNo,''),', ') INTO v_SerialList FROM    WareHouseItmsDTL WID
   LEFT JOIN OppWarehouseSerializedItem Ser ON WID.numWareHouseItmsDTLID = Ser.numWarehouseItmsDTLID WHERE   Ser.numOppItemID = v_numOppItemtCode;
        
   RETURN coalesce(v_SerialList,'');
END; $$; 
/****** Object:  UserDefinedFunction [dbo].[GetTabViewStatus]    Script Date: 07/26/2008 18:13:11 ******/

