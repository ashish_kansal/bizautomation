-- Function definition script fn_calculateTotalHoursMinutesByStageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_calculateTotalHoursMinutesByStageDetails(v_numStageDetailsId NUMERIC DEFAULT 0,
v_numOppId NUMERIC(18,0) DEFAULT 0,
v_numProjectId NUMERIC(18,0) DEFAULT 0)
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN COALESCE((SELECT 
						CONCAT(CAST(MAX(Days) AS NUMERIC(18,2)),'_',CAST(MAX(Hours) AS NUMERIC(18,2)),'_',CAST(MAX(Minutes) AS NUMERIC(18,2))) 
					FROM
					(
						SELECT 
							coalesce(numAssignTo,0) AS numAssignTo,
							FLOOR(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0))/1440) AS Days,
							CAST((MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),1440)) AS decimal)/60 AS Hours,
							MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),60) AS Minutes
						FROM 
							StagePercentageDetailsTask 
						WHERE 
							numStageDetailsId = v_numStageDetailsId 
							AND bitSavedTask = true 
							AND 1 = (CASE WHEN v_numOppId > 0 THEN (CASE WHEN numOppId = v_numOppId THEN 1 ELSE 0 END) WHEN v_numProjectId > 0 THEN (CASE WHEN numProjectId = v_numProjectId THEN 1 ELSE 0 END) ELSE 0 END)
      GROUP BY numAssignTo) AS T),'0.00_0.00_0.00');

END; $$;

