-- Stored procedure definition script USP_GetChartAcntTypeId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartAcntTypeId(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numAcntTypeId from Chart_Of_Accounts Where numAccountId = v_numChartAcntId And numDomainId = v_numDomainID;
END; $$;












