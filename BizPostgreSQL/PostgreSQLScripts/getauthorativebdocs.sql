-- Stored procedure definition script GetAuthorativeBDocs for PostgreSQL
CREATE OR REPLACE FUNCTION GetAuthorativeBDocs(v_numDivID NUMERIC,    
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select vcBizDocID,numOppBizDocsId From OpportunityMaster  OM
   Inner join OpportunityBizDocs OBD on OM.numOppId = OBD.numoppid Where   OM.numDivisionId = v_numDivID and
   OBD.numBizDocId in(Select numAuthoritativeSales From AuthoritativeBizDocs where numDomainId = v_numDomainId);
END; $$;













