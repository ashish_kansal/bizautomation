-- Function definition script fn_GetProfitLossAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetProfitLossAmount(v_numDomainID NUMERIC(18,0), 
	v_dtFromDate TIMESTAMP, 
	v_dtToDate TIMESTAMP, 
	v_numPLAccountID NUMERIC(18,0),
	v_numAccountClass NUMERIC(18,0))
RETURNS NUMERIC(19,4) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalIncome  NUMERIC(19,4);
   v_TotalExpense  NUMERIC(19,4);
   v_TotalCOGS  NUMERIC(19,4);
   v_CURRENTPL  NUMERIC(19,4);
   v_PLOPENING  NUMERIC(19,4);
BEGIN
   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL
   (
      numAccountId NUMERIC(18,0),
      vcAccountCode VARCHAR(50),
      datEntry_Date TIMESTAMP,
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );
   INSERT INTO tt_VIEW_JOURNAL  SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainID AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   select   coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) INTO v_TotalIncome FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0103%' AND datentry_date between v_dtFromDate and v_dtToDate;
   select   coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) INTO v_TotalExpense FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0104%' AND datentry_date between v_dtFromDate and v_dtToDate;
   select   coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) INTO v_TotalCOGS FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0106%' AND datentry_date between v_dtFromDate and v_dtToDate;

   v_CURRENTPL :=(v_TotalIncome+v_TotalExpense+v_TotalCOGS);

   select   coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) INTO v_PLOPENING FROM
   tt_VIEW_JOURNAL VJ WHERE
		(vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode ilike '0106%')
   AND datentry_date <=  v_dtFromDate+ make_interval(secs => -.3);

   select   coalesce(v_PLOPENING,cast(0 as NUMERIC(19,4)))+coalesce(sum(Credit),cast(0 as NUMERIC(19,4))) -coalesce(sum(Debit),cast(0 as NUMERIC(19,4))) INTO v_PLOPENING FROM
   tt_VIEW_JOURNAL VJ WHERE
   numaccountid = v_numPLAccountID; 

   v_CURRENTPL := v_PLOPENING+v_CURRENTPL;

   RETURN v_CURRENTPL;
END; $$;

