-- Stored procedure definition script USP_ManageShippingPromotions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingPromotions(v_bitFreeShiping BOOLEAN,
	v_monFreeShippingOrderAmount DECIMAL(20,5),
	v_numFreeShippingCountry NUMERIC(18,0),
	v_bitFixShipping1 BOOLEAN,
	v_monFixShipping1OrderAmount DECIMAL(20,5),
	v_monFixShipping1Charge DECIMAL(20,5),
	v_bitFixShipping2 BOOLEAN,
	v_monFixShipping2OrderAmount DECIMAL(20,5),
	v_monFixShipping2Charge DECIMAL(20,5),	
    v_numDomainId NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShipProId  NUMERIC(9,0);
BEGIN
   IF EXISTS(SELECT * FROM ShippingPromotions WHERE numDomainID = v_numDomainId) then
      select   numShipProId INTO v_numShipProId FROM ShippingPromotions WHERE numDomainID = v_numDomainId    LIMIT 1;
      UPDATE ShippingPromotions
      SET bitFixShipping1 = v_bitFixShipping1,monFixShipping1OrderAmount = v_monFixShipping1OrderAmount,
      monFixShipping1Charge = v_monFixShipping1Charge,
      bitFixShipping2 = v_bitFixShipping2,monFixShipping2OrderAmount = v_monFixShipping2OrderAmount,
      monFixShipping2Charge = v_monFixShipping2Charge,
      bitFreeShiping = v_bitFreeShiping,monFreeShippingOrderAmount = v_monFreeShippingOrderAmount,
      numFreeShippingCountry = v_numFreeShippingCountry,
      numModifiedBy = v_numUserCntID,dtModified = TIMEZONE('UTC',now())
      WHERE numDomainID = v_numDomainId;
      open SWV_RefCur for
      SELECT v_numShipProId;
   ELSE
      INSERT INTO ShippingPromotions(bitFixShipping1, monFixShipping1OrderAmount, monFixShipping1Charge, bitFixShipping2, monFixShipping2OrderAmount, monFixShipping2Charge,
		   bitFreeShiping, monFreeShippingOrderAmount, numFreeShippingCountry, numDomainID, numCreatedBy, dtCreated)
        VALUES(v_bitFixShipping1, v_monFixShipping1OrderAmount, v_monFixShipping1Charge, v_bitFixShipping2, v_monFixShipping2OrderAmount, v_monFixShipping2Charge,
		   v_bitFreeShiping, v_monFreeShippingOrderAmount, v_numFreeShippingCountry, v_numDomainId, v_numUserCntID, TIMEZONE('UTC',now()));
		
      open SWV_RefCur for
      SELECT CURRVAL('ShippingPromotions_seq');
   end if;
   RETURN;
END; $$;


