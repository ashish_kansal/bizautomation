CREATE OR REPLACE FUNCTION AfterInsertUpdateOpportunityBizDocItems_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppBizDocsId  NUMERIC(9,0);
   v_numOppId  NUMERIC(9,0);
   v_numOppItemID  NUMERIC(9,0);
   v_QtyShipped  NUMERIC(9,0);
BEGIN
	v_numOppBizDocsId := NEW.numOppBizDocId;
	v_numOppItemID := NEW.numOppItemID;
	v_QtyShipped := NEW.numUnitHour;
 
	SELECT numoppid INTO v_numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocsId;
  
	UPDATE 
		OpportunityBizDocs
	SET 
		monDealAmount = GetDealAmount(v_numOppId,TIMEZONE('UTC',now()),v_numOppBizDocsId)
	WHERE 
		numOppBizDocsId = v_numOppBizDocsId;
   
	RETURN NULL;
END; $$;
CREATE TRIGGER AfterInsertUpdateOpportunityBizDocItems AFTER INSERT OR UPDATE ON OpportunityBizDocItems FOR EACH ROW EXECUTE PROCEDURE AfterInsertUpdateOpportunityBizDocItems_TrFunc();

