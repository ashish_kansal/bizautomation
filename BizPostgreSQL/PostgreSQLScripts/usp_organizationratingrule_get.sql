-- Stored procedure definition script USP_OrganizationRatingRule_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OrganizationRatingRule_Get(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numORRID
		,tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID
		,CONCAT('If the performance (Order Sub-total for period selected) amount for ',(CASE tintPerformance WHEN 1 THEN 'Last 3 MTD' WHEN 2 THEN 'Last 6 MTD' WHEN 3 THEN 'Last 1 YTD' END),' is >= ',numFromAmount,' and <= ',numToAmount,
   ' set Organization Rating to ',coalesce((SELECT cast(vcData as VARCHAR(255)) FROM Listdetails WHERE numListID = 2 AND numListItemID = numOrganizationRatingID),'')) AS vcOrganizationRatingRule
   FROM
   OrganizationRatingRule
   WHERE
   numDomainID = v_numDomainID;
END; $$;











