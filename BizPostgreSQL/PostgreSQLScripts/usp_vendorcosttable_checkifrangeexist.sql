DROP FUNCTION IF EXISTS usp_vendorcosttable_checkifrangeexist;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_checkifrangeexist
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numvendorid NUMERIC(18,0)
	,p_numitemcode NUMERIC(18,0)
	,p_numcurrencyid NUMERIC(18,0)
	,p_numQty NUMERIC
	,INOUT p_swvrefcur REFCURSOR
)
LANGUAGE plpgsql
   AS $$
	DECLARE 
		v_IsRangeExist BOOLEAN DEFAULT TRUE;
BEGIN
	IF (SELECT vcDefaultCostRange FROM Domain WHERE numDomainID=p_numdomainid) IS NOT NULL THEN
		IF NOT EXISTS (SELECT Vendor.numVendorTcode FROM Vendor WHERE Vendor.numDomainID=p_numdomainid AND Vendor.numVendorID=p_numvendorid AND Vendor.numItemCode=p_numitemcode) THEN
			v_IsRangeExist := true;
		ELSEIF EXISTS (SELECT 
						VendorCostTable.numvendorcosttableid
					FROM 
						Vendor 
					INNER JOIN 
						VendorCostTable 
					ON 
						Vendor.numvendortcode=VendorCostTable.numvendortcode 
					WHERE
						Vendor.numDomainID=p_numdomainid 
						AND Vendor.numVendorID=p_numvendorid 
						AND Vendor.numItemCode=p_numitemcode
						AND numCurrencyID = p_numcurrencyid
						AND p_numQty BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty) THEN
			v_IsRangeExist := true;
		ELSE
			v_IsRangeExist := false;
		END IF;
	ELSE
		v_IsRangeExist := true;
	END IF;

	OPEN p_swvrefcur FOR SELECT v_IsRangeExist AS IsRangeExist;
END; $$;