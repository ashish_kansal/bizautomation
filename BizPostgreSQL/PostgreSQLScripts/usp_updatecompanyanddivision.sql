-- Stored procedure definition script usp_UpdateCompanyAndDivision for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateCompanyAndDivision(v_vcCompanyName VARCHAR(100) DEFAULT '',    
 v_vcWebsite VARCHAR(255) DEFAULT '',    
 v_numNoOfEmployeeId  NUMERIC(9,0) DEFAULT 0,    
 v_numAnnualRevID NUMERIC(9,0) DEFAULT 0,    
 v_numCompanyID NUMERIC(9,0) DEFAULT 0,    
 v_vcHow VARCHAR(100) DEFAULT '',    
 v_vcProfile VARCHAR(100) DEFAULT '',    
 v_vcDivisionName VARCHAR(10) DEFAULT '',    
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,  
 v_numCampaignID NUMERIC(9,0) DEFAULT 0,
 v_vcCity VARCHAR(500) DEFAULT '',
 v_vcStreet VARCHAR(500) DEFAULT '',
 v_vcState VARCHAR(500) DEFAULT '',
 v_vcCountry VARCHAR(500) DEFAULT '',
 v_vcPostCode VARCHAR(500) DEFAULT '',
 v_numModifiedBy NUMERIC DEFAULT 0,
 v_bintModifiedDate NUMERIC DEFAULT 0   
     --
)
RETURNS VOID LANGUAGE plpgsql    
/*
set @vcCompanyName = replace (@vcCompanyName,'''','''''')
set @vcWebsite = replace (@vcWebsite,'''','''''')
set @vcHow = replace (@vcHow,'''','''''')
set @vcCompanyName = replace (@vcCompanyName,'''','''''')
set @vcProfile = replace (@vcProfile,'''','''''')
set @vcDivisionName = replace (@vcDivisionName,'''','''''')
--set @vcCompanyName = replace (@vcCompanyName,'''','''''')
*/
   AS $$
   DECLARE
   v_numLinkID  NUMERIC;
BEGIN
   UPDATE CompanyInfo SET vcCompanyName = v_vcCompanyName,vcWebSite = v_vcWebsite,numNoOfEmployeesId = v_numNoOfEmployeeId,
   numAnnualRevID = v_numAnnualRevID,vcHow = v_vcHow,vcProfile = v_vcProfile,
   numModifiedBy = CASE WHEN v_numModifiedBy > 0 THEN v_numModifiedBy ELSE NULL END,bintModifiedDate = CASE WHEN v_bintModifiedDate > 0 THEN v_bintModifiedDate ELSE NULL END
   WHERE numCompanyId = v_numCompanyID;    
  
   UPDATE DivisionMaster SET vcDivisionName = v_vcDivisionName,numModifiedBy = CASE WHEN v_numModifiedBy > 0 THEN v_numModifiedBy ELSE NULL END,bintModifiedDate = CAST(CASE WHEN v_bintModifiedDate > 0 THEN v_bintModifiedDate ELSE NULL END AS TIMESTAMP)
   WHERE numDivisionID = v_numDivisionID;  
  
   UPDATE AddressDetails
   SET vcStreet = v_vcStreet,vcCity = v_vcCity,numCountry = v_vcCountry,vcPostalCode = v_vcPostCode,
   numState = v_vcState
   WHERE  numRecordID = v_numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = 1;
  
  
   select   numLinkId INTO v_numLinkID from CampaignDivision where numDivisionID = v_numDivisionID;  
   If v_numLinkID > 0 then
 
      Update CampaignDivision Set numCampaignID = v_numCampaignID where numDivisionID = v_numDivisionID;
   Else
      Insert into CampaignDivision(numDivisionID, numCampaignID) values(v_numDivisionID, v_numCampaignID);
   end if;
   RETURN;
END; $$;


