-- Stored procedure definition script Delete_Action_Template_Data for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Delete_Action_Template_Data(v_rowID INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Delete From tblActionItemData Where RowID = v_rowID;
   RETURN;
END; $$;


