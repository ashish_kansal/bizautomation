-- Stored procedure definition script USP_FetchCheckDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_FetchCheckDetails(v_numDomainID NUMERIC(18,0),
    v_numCheckHeaderID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CD.*,cast(coalesce(GJD.numTransactionId,0) as NUMERIC(10,0)) AS numTransactionId,cast(CH.numCheckHeaderID as VARCHAR(30)),
	FormatedDateFromDate(CH.dtCheckDate,v_numDomainID) AS dtCheckDate , fn_GetComapnyName(CH.numDivisionID) AS CheckTo
   FROM CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
   LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType = 2 AND GJD.numReferenceID = CD.numCheckDetailID
   WHERE CH.numDomainID = v_numDomainID AND CH.numCheckHeaderID = v_numCheckHeaderID;
END; $$; 












