-- Stored procedure definition script USP_UpdateAccountTypeCode for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateAccountTypeCode(v_numDomainID NUMERIC(9,0),
v_numAccountTypeID NUMERIC(9,0), --- AccountId for which the parent id is changing
v_numNewParentID NUMERIC(9,0) --- new parentid

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcNewAccountCode  VARCHAR(50);
   v_vcOldAccountCode  VARCHAR(50);
BEGIN
   v_vcNewAccountCode := GetAccountTypeCode(v_numDomainID,v_numNewParentID,0::SMALLINT);  

   select   vcAccountCode INTO v_vcOldAccountCode FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND numAccountTypeID = v_numAccountTypeID;


   UPDATE AccountTypeDetail SET vcAccountCode = coalesce(v_vcNewAccountCode,'') || SUBSTR(vcAccountCode,LENGTH(v_vcOldAccountCode)+1,LENGTH(vcAccountCode))
   WHERE numDomainID = v_numDomainID and vcAccountCode ilike coalesce(v_vcOldAccountCode,'') || '%'; 

   UPDATE AccountTypeDetail set numParentID = v_numNewParentID
   where numDomainID = v_numDomainID AND numAccountTypeID = v_numAccountTypeID;
   RETURN;
END; $$;



