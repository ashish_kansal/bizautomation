-- Stored procedure definition script USP_AddBroadcastingLink for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddBroadcastingLink(v_strBroadcastingLink TEXT DEFAULT '',
    v_numLinkId NUMERIC DEFAULT 0,
    v_numContactId NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intNoOfClick  INTEGER; 
   v_vcLinkIds  VARCHAR(1000); 
   v_numBroadcastID  NUMERIC;
BEGIN
   IF EXISTS(SELECT numLinkId FROM BroadcastingLink WHERE numLinkId = v_numLinkId) then
      select   numBroadcastID INTO v_numBroadcastID from BroadcastingLink where numLinkId = v_numLinkId;
      select(intNoOfClick+1), coalesce(vcLinkIds,'') || SUBSTR(CAST(v_numLinkId AS VARCHAR(20)),1,20) || ',' INTO v_intNoOfClick,v_vcLinkIds FROM BroadCastDtls WHERE numBroadcastID = v_numBroadcastID and numContactID = v_numContactId;
      UPDATE BroadCastDtls set  vcLinkIds = v_vcLinkIds,intNoOfClick = v_intNoOfClick  where numBroadcastID = v_numBroadcastID   and numContactID = v_numContactId;
   ELSE
      INSERT  INTO BroadcastingLink(numBroadcastID,vcOriginalLink)
      SELECT  X.numBroadcastId,
                 X.vcOriginalLink
      FROM
		XMLTABLE
		(
			'LinkRecords/Table'
			PASSING 
				CAST(v_strBroadcastingLink AS XML)
			COLUMNS
				numBroadcastId NUMERIC(18,0) PATH 'numBroadcastId',
				vcOriginalLink VARCHAR(2000) PATH 'vcOriginalLink'
		) AS X;
   end if;
   RETURN;
END; $$;
--exec USP_AddBroadcastingLink @strBroadcastingLink=NULL,@numLinkId=83,@numContactId=613
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/



