-- Stored procedure definition script USP_ReBuildIndex for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReBuildIndex()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_vcTableName  VARCHAR(100);
   v_sql  VARCHAR(1000);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcTableName VARCHAR(100)
   );
   INSERT INTO tt_TEMP(vcTableName) SELECT Name from sys.tables WHERE type_desc = 'USER_TABLE' AND Name <> 'Internet_Sales_Order_Details' ORDER BY Name;

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;
	
   WHILE v_i <= v_iCount LOOP
      select   vcTableName INTO v_vcTableName FROM tt_TEMP WHERE ID = v_i;
      BEGIN
         v_sql := 'ALTER INDEX ALL ON ' || coalesce(v_vcTableName,'') || ' /** SDynExp 1 **/ REINDEX TABLE ?1?';
         EXECUTE v_sql;
         RAISE NOTICE '%',CONCAT('INDEX REBUILD COMPLETED: ',v_vcTableName);
         EXCEPTION WHEN OTHERS THEN
            RAISE NOTICE '%',CONCAT('INDEX REBUILD FAILED: ',v_vcTableName);
      END;
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


