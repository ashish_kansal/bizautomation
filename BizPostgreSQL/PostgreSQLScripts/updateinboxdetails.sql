-- Stored procedure definition script UpdateInboxDetails for PostgreSQL
CREATE OR REPLACE FUNCTION UpdateInboxDetails(v_tintMode SMALLINT DEFAULT 1,
    v_strXML TEXT DEFAULT '',
    v_numUserCntID NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc4  INTEGER;
   v_numEmailHstrID  NUMERIC; 
   v_AttachmentName  VARCHAR(500);            
   v_AttachmentType  VARCHAR(500);    
   v_NewAttachmentName  VARCHAR(500);  
   v_minID  NUMERIC;
BEGIN
   IF v_tintMode = 1 then
            
      open SWV_RefCur for SELECT 
      cast(numEmailHstrID as NUMERIC),
                        cast(numUid as VARCHAR(255)),
                        cast(numDomainID as VARCHAR(255)),
                        cast(numUserCntId as VARCHAR(255))
      FROM    EmailHistory
      WHERE   numUid > 0
      AND OCTET_LENGTH(coalesce(vcBody,'')) = 0
      AND numUserCntId = v_numUserCntID
      ORDER BY numDomainID,numUserCntId,numUid LIMIT 10;
   end if;
   IF v_tintMode = 2 then
            
      IF SUBSTR(CAST(v_strXML AS VARCHAR(10)),1,10) <> '' then
         SELECT * INTO v_hDoc4 FROM SWF_Xml_PrepareDocument(v_strXML);
         UPDATE  EmailHistory
         SET     vcBody = Y.Body,vcBodyText = Y.BodyText,vcFrom = GetEmaillName(Y.numEmailHstrID,4),
         vcTo = GetEmaillName(Y.numEmailHstrID,1),vcCC = GetEmaillName(Y.numEmailHstrID,3)
         FROM(SELECT    X.UId,
                                            coalesce(X.Body,'<br>') AS Body,
                                            X.BodyText,
                                            X.numEmailHstrID
         FROM(SELECT * FROM      SWF_OpenXml(v_hDoc4,'/NewDataSet/Table','numEmailHstrID |UId |Body |BodyText') SWA_OpenXml(numEmailHstrID NUMERIC(9,0),UId NUMERIC(9,0),Body TEXT,BodyText TEXT)) X) Y
         WHERE   EmailHistory.numUid = Y.UID
         AND EmailHistory.numUserCntId = v_numUserCntID;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            numEmailHstrID NUMERIC,
            UId NUMERIC(9,0),
            HasAttachments BOOLEAN,
            AttachmentName VARCHAR(500),
            AttachmentType VARCHAR(500),
            NewAttachmentName VARCHAR(500)
         );
         INSERT  INTO tt_TEMP
         SELECT  0 AS numEmailHstrID,
                                        X.UId,
                                        X.HasAttachments,
                                        X.AttachmentName,
                                        X.AttachmentType,
                                        X.NewAttachmentName
         FROM(SELECT * FROM      SWF_OpenXml(v_hDoc4,'/NewDataSet/Table','UId |HasAttachments |AttachmentName |AttachmentType |NewAttachmentName') SWA_OpenXml(UId NUMERIC(9,0),HasAttachments BOOLEAN,AttachmentName VARCHAR(500),AttachmentType VARCHAR(500),
            NewAttachmentName VARCHAR(500))) X
         WHERE   X.HasAttachments = 1;
         UPDATE  tt_TEMP
         SET     numEmailHstrID = X.numEmailHstrID
         FROM(SELECT    tt_TEMP.UId,
                                            EH.numEmailHstrID
         FROM      tt_TEMP
         INNER JOIN EmailHistory EH ON tt_TEMP.UId = EH.numUid) X
         WHERE   tt_TEMP.UId = EH.numUid AND tt_TEMP.UId = X.UId;
         select   MIN(numEmailHstrID) INTO v_minID FROM    tt_TEMP;
         select   numEmailHstrID, AttachmentName, AttachmentType, NewAttachmentName INTO v_numEmailHstrID,v_AttachmentName,v_AttachmentType,v_NewAttachmentName FROM    tt_TEMP    ORDER BY numEmailHstrID DESC LIMIT 1;
         WHILE v_numEmailHstrID > v_minID LOOP
            DELETE  FROM EmailHstrAttchDtls
            WHERE   numEmailHstrID = v_numEmailHstrID;
            PERFORM USP_InsertAttachment(v_AttachmentName,'',v_AttachmentType,v_numEmailHstrID,v_NewAttachmentName);
            select   numEmailHstrID, AttachmentName, AttachmentType, NewAttachmentName INTO v_numEmailHstrID,v_AttachmentName,v_AttachmentType,v_NewAttachmentName FROM    tt_TEMP WHERE   numEmailHstrID < v_numEmailHstrID   ORDER BY numEmailHstrID DESC LIMIT 1;
         END LOOP;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
      end if;
   end if;
   IF v_tintMode = 3 then
            
      UPDATE EmailHistory SET vcBody = '<br/>' WHERE numUid IN(SELECT ID FROM SplitIDs(v_strXML,','))
      AND numUserCntId = v_numUserCntID;
   end if;
   RETURN;
END; $$;
--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 














