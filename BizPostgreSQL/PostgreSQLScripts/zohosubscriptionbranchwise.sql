-- Stored procedure definition script dbo.ZohoSubscriptionBranchWise for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Warning: object conversion was canceled because of license limitations. The target script equals to the source script.
----

CREATE PROCEDURE ZohoSubscriptionBranchWise
@AccountId INT,
@CompanyId INT,
@BranchId INT
as
SELECT TOP 1 * FROM dbo.ZohoSubscription
WHERE  AccountId=@AccountId AND CompanyId=@CompanyId AND BranchId=@BranchId
ORDER BY id DESC



