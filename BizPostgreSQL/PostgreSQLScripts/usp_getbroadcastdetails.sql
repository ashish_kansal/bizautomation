-- Stored procedure definition script USP_GetBroadcastDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBroadcastDetails(v_numBroadCastId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * from BroadCastDtls where numBroadcastID = v_numBroadCastId) then
   
      open SWV_RefCur for SELECT * from BroadCastDtls where numBroadcastID = v_numBroadCastId;
   end if;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_GetBroadcastDTLs]    Script Date: 07/26/2008 16:16:24 ******/













