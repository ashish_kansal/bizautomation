-- Function definition script fn_GetPayrollEmployeeExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetPayrollEmployeeExpense(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID INTEGER
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_ClientTimeZoneOffset INTEGER)
RETURNS TABLE
(	
   numRecordID NUMERIC(18,0),
   tintRecordType SMALLINT,
   vcSource VARCHAR(200),
   dtDate DATE,
   vcDate VARCHAR(50),
   numType SMALLINT,
   vcType VARCHAR(50),
   monExpense DECIMAL(20,5),
   vcDetails TEXT,
   numApprovalComplete INTEGER
) LANGUAGE plpgsql
   AS $$
BEGIN


	-- Time & Expense
   DROP TABLE IF EXISTS tt_FN_GETPAYROLLEMPLOYEEEXPENSE_ITEMPRICE CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETPAYROLLEMPLOYEEEXPENSE_ITEMPRICE
   (
      numRecordID NUMERIC(18,0),
      tintRecordType SMALLINT,
      vcSource VARCHAR(200),
      dtDate DATE,
      vcDate VARCHAR(50),
      numType SMALLINT,
      vcType VARCHAR(50),
      monExpense DECIMAL(20,5),
      vcDetails TEXT,
      numApprovalComplete INTEGER
   );
   INSERT INTO tt_FN_GETPAYROLLEMPLOYEEEXPENSE_ITEMPRICE(numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,monExpense
		,vcDetails
		,numApprovalComplete)
   SELECT
   timeandexpense.numCategoryHDRID
		,1
		,'Expense' AS vcSource
		,dtFromDate
		,FormatedDateFromDate(dtFromDate + make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) AS vcDate
		,timeandexpense.numType
		,(CASE
   WHEN timeandexpense.numtype = 1 THEN 'Billable'
   WHEN timeandexpense.numtype = 2 THEN 'Reimbursable'
   WHEN timeandexpense.numtype = 6 Then 'Billable + Reimbursable'
   ELSE ''
   END) AS vcType
		,coalesce(timeandexpense.monAmount,0)
		,coalesce(timeandexpense.txtDesc,'') AS vcDetails
		,coalesce(timeandexpense.numApprovalComplete,0) AS numApprovalComplete
   FROM
   timeandexpense
   LEFT JOIN
   OpportunityMaster
   ON
   timeandexpense.numOppId = OpportunityMaster.numOppId
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND timeandexpense.numUserCntID = v_numUserCntID
   AND coalesce(numCategory,0) = 2
   AND CAST(dtFromDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   ORDER BY
   dtFromDate;

	
	RETURN QUERY (SELECT * FROM tt_FN_GETPAYROLLEMPLOYEEEXPENSE_ITEMPRICE);
END; $$;

