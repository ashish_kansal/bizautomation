-- Stored procedure definition script USP_GetUsageReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetUsageReport(v_numDomainID NUMERIC(9,0) DEFAULT 0,          
v_SearchWord VARCHAR(100) DEFAULT NULL,          
v_tintLastLoggedIn SMALLINT DEFAULT NULL,      
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
v_vcSortColumn VARCHAR(200) DEFAULT NULL,
v_vcSortOrder VARCHAR(4) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);
BEGIN
   v_strSQL := 'select numSubscriberID,vcCompanyName,COALESCE(vcFirstname,'''') || '' '' || COALESCE(vcLastname,'''') as Name,vcPosition,          
FormatedDateTimeFromDate(dtLastLoggedIn + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') as dtLastLoggedIn,(select count(*) from UserAccessedDTL where numDivisionID = A.numDivisionID           
and numDomainID = S.numTargetDomainID and bitPortal = false';           
          
   if v_tintLastLoggedIn = 1 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLoggedInTime between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
   if v_tintLastLoggedIn = 2 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLoggedInTime between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-14 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
   if v_tintLastLoggedIn = 3 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLoggedInTime between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-30 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
          
          
   v_strSQL := coalesce(v_strSQL,'') || ') as TotalCount,          
intNoofUsersSubscribed,intNoOfPartners,FormatedDateTimeFromDate(dtSubStartDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') as dtSubStartDate,  
FormatedDateTimeFromDate(dtSubEndDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') as dtSubEndDate ,bitTrial          
from Subscribers S           
join DivisionMaster D          
on D.numDivisionID = S.numDivisionID          
Join CompanyInfo C          
on C.numCompanyId = D.numCompanyID          
Join AdditionalContactsInformation A          
on A.numContactId = S.numAdminContactID          
where D.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);          
          
   if v_tintLastLoggedIn = 1 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLastLoggedIn between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
   if v_tintLastLoggedIn = 2 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLastLoggedIn between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-14 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
   if v_tintLastLoggedIn = 3 then 
      v_strSQL := coalesce(v_strSQL,'') || ' and dtLastLoggedIn between ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-30 day' AS VARCHAR(30))
      || ''' and ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS VARCHAR(30)) || '''';
   end if;          
          
   if v_SearchWord <> '' then 
      v_strSQL := coalesce(v_strSQL,'') || ' and vcCompanyName ilike ''%' || coalesce(v_SearchWord,'') || '%''';
   end if;          
  
   IF coalesce(v_vcSortColumn,'') = '' then

      v_strSQL := coalesce(v_strSQL,'') || ' ORDER BY vcCompanyName ASC';
   ELSE
      IF v_vcSortColumn = 'dtSubStartDate' OR v_vcSortColumn = 'dtSubEndDate'  OR v_vcSortColumn = 'dtLastLoggedIn' then
	
         v_vcSortColumn := CONCAT('S.',v_vcSortColumn);
      end if;
      v_strSQL := CONCAT(v_strSQL,' ORDER BY ',v_vcSortColumn,' ',v_vcSortOrder);
   end if;
     
   RAISE NOTICE '%',CAST(v_strSQL AS TEXT);
	      
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


