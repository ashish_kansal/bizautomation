-- Stored procedure definition script usp_GetSurveyListDdl for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyListDdl(v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT sm.numSurID as "numSurID", vcSurName as "vcSurName"
   FROM SurveyMaster sm
   WHERE sm.numDomainID = v_numDomainID;
END; $$;












