-- Stored procedure definition script USP_ManageOpportunityFulfillmentRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageOpportunityFulfillmentRules(v_numDomainID NUMERIC,
    v_numBizDocTypeID NUMERIC,
    v_strItems TEXT,
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 0 then
        
      open SWV_RefCur for
      SELECT * FROM OpportunityFulfillmentRules WHERE numDomainID = v_numDomainID AND (numBizDocTypeID = v_numBizDocTypeID OR v_numBizDocTypeID = 0);
   ELSEIF v_tintMode = 1
   then
      DELETE FROM OpportunityFulfillmentRules WHERE numDomainID = v_numDomainID AND numBizDocTypeID = v_numBizDocTypeID;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
                
         SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
         INSERT  INTO OpportunityFulfillmentRules(numDomainID,numRuleID,numBizDocTypeID,numBizDocStatus)
         SELECT  v_numDomainID,
                                    X.numRuleID,
                                    v_numBizDocTypeID,
                                    X.numBizDocStatus
         FROM(SELECT * FROM      SWF_OpenXml(v_hDocItem,'/NewDataSet/Table','numRuleID |numBizDocStatus') SWA_OpenXml(numRuleID NUMERIC(18,0),numBizDocStatus NUMERIC(18,0))) X;
         PERFORM SWF_Xml_RemoveDocument(v_hDocItem);
      end if;
      open SWV_RefCur for
      SELECT  '';
   end if;
   RETURN;
END; $$;






