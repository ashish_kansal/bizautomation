DROP FUNCTION IF EXISTS fn_GetItemPriceBasedOnMethod;

CREATE OR REPLACE FUNCTION fn_GetItemPriceBasedOnMethod
(
	v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numQty DOUBLE PRECISION
	,v_bitCalculatedPrice BOOLEAN
	,v_monCalculatedPrice DECIMAL(20,5)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_vcSelectedKitChildItems TEXT
	,v_numCurrencyID NUMERIC(18,0)
)
RETURNS TABLE
(
   monPrice DECIMAL(20,5),
   tintDisountType SMALLINT,
   decDiscount DOUBLE PRECISION,
   monFinalPrice DECIMAL(20,5) 
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monListPrice  DECIMAL(20,5);
   v_monVendorCost  DECIMAL(20,5);
   v_monVendorDynamicCost  DECIMAL(20,5);
   v_monVendorStaticCost  DECIMAL(20,5);
   v_tintPriceLevel  SMALLINT;
   v_tintRuleType  SMALLINT;
   v_tintDisountType  SMALLINT;
   v_decDiscount  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5) DEFAULT NULL;
   v_monFinalPrice  DECIMAL(20,5) DEFAULT NULL;
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_numItemClassification  NUMERIC(18,0);
   v_tintPriceBookDiscount  SMALLINT;
   v_tintPriceMethod  SMALLINT;
   v_fltUOMConversionFactor  INTEGER DEFAULT 1;
   v_bitKitParent  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitCalAmtBasedonDepItems  BOOLEAN;
   v_tintKitAssemblyPriceBasedOn  SMALLINT;
   v_numBaseCurrency  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;
   v_numPriceRuleID  NUMERIC(18,0);
   v_tintPriceRuleType  SMALLINT;
   v_tintPricingMethod  SMALLINT;
   v_tintPriceBookDiscountType  SMALLINT;
   v_decPriceBookDiscount  DOUBLE PRECISION;
   v_intQntyItems  INTEGER;
   v_decMaxDedPerAmt  DOUBLE PRECISION;
   v_bitRoundTo BOOLEAN;
   v_tintRoundTo SMALLINT;
   v_tintVendorCostType SMALLINT;
BEGIN
	DROP TABLE IF EXISTS tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE CASCADE;
	CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE
	(
		monPrice DECIMAL(20,5),
		tintDisountType SMALLINT,
		decDiscount DOUBLE PRECISION,
		monFinalPrice DECIMAL(20,5)
	);
   
	SELECT
		(CASE
			WHEN coalesce(v_bitCalculatedPrice,false) = true
			THEN v_monCalculatedPrice
			ELSE (CASE
					WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID = v_numDomainID AND ItemCurrencyPrice.numItemCode = v_numItemCode AND ItemCurrencyPrice.numCurrencyID = coalesce(v_numCurrencyID,0))
					THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID = v_numDomainID AND ItemCurrencyPrice.numItemCode = v_numItemCode AND ItemCurrencyPrice.numCurrencyID = coalesce(v_numCurrencyID,0)),0)
					ELSE (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(Item.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(Item.monListPrice,0) END)
				END)
		END)
		,numItemClassification
		,(CASE 
			WHEN COALESCE(bitKitParent,false) = true
			THEN (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(fn_GetKitVendorCost(v_numDomainID,Item.numItemCode,v_vcSelectedKitChildItems),0) / v_fltExchangeRate) AS INT) ELSE COALESCE(fn_GetKitVendorCost(v_numDomainID,Item.numItemCode,v_vcSelectedKitChildItems),0) END) * fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit)
			ELSE (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monCost,0) END) * fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit)
		END)
		, coalesce(bitKitParent,false)
		, coalesce(bitAssembly,false)
		, coalesce(bitCalAmtBasedonDepItems,false)
		, coalesce(tintKitAssemblyPriceBasedOn,1) 
	INTO 
		v_monListPrice
		,v_numItemClassification
		,v_monVendorCost
		,v_bitKitParent
		,v_bitAssembly
		,v_bitCalAmtBasedonDepItems
		,v_tintKitAssemblyPriceBasedOn 
	FROM
		Item
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode 
	WHERE
		Item.numItemCode = v_numItemCode;

	IF EXISTS (SELECT
					VendorCostTable.numvendorcosttableid
				FROM 
					Item
				INNER JOIN
					Vendor
				ON
					Item.numVendorID = Vendor.numVendorID
				INNER JOIN
					VendorCostTable
				ON
					Vendor.numVendorTCode = VendorCostTable.numVendorTCode
				WHERE
					Item.numItemCode = v_numItemCode
					AND VendorCostTable.numCurrencyID = v_numCurrencyID
					AND v_numQty BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty) THEN
		SELECT
			COALESCE(VendorCostTable.monDynamicCost,v_monVendorCost)
			,COALESCE(VendorCostTable.monStaticCost,v_monVendorCost)
		INTO 
			v_monVendorDynamicCost
			,v_monVendorStaticCost
		FROM 
			Item
		INNER JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
		INNER JOIN
			VendorCostTable
		ON
			Vendor.numVendorTCode = VendorCostTable.numVendorTCode
		WHERE
			Item.numItemCode = v_numItemCode
			AND VendorCostTable.numCurrencyID = v_numCurrencyID
			AND v_numQty BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty;
	ELSE
		v_monVendorDynamicCost := v_monVendorCost;
		v_monVendorStaticCost := v_monVendorCost;
	END IF;


	SELECT COALESCE(numCurrencyID,0) INTO v_numBaseCurrency FROM Domain WHERE numDomainId = v_numDomainID;

	IF v_numBaseCurrency = v_numCurrencyID then
		v_fltExchangeRate := 1;
	ELSE
		v_fltExchangeRate := coalesce((SELECT fltExchangeRate FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = v_numCurrencyID),1);

		IF coalesce(v_fltExchangeRate,0) = 0 then
			v_fltExchangeRate := 1;
		END IF;
	END IF;

	IF v_bitCalculatedPrice = false AND (coalesce(v_bitKitParent,false) = true OR coalesce(v_bitAssembly,false) = true) AND coalesce(v_bitCalAmtBasedonDepItems,false) = true THEN	
		SELECT 
			monPrice 
		INTO 
			v_monListPrice 
		FROM
			fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,v_numWarehouseItemID,v_tintKitAssemblyPriceBasedOn,0,0,v_vcSelectedKitChildItems,v_numCurrencyID,v_fltExchangeRate);
	END IF;
		
	SELECT 
		COALESCE(numDefaultSalesPricing,0)
		,tintPriceBookDiscount 
	INTO 
		v_tintPriceMethod
		,v_tintPriceBookDiscount 
	FROM
		Domain 
	WHERE
		numDomainId = v_numDomainID;

	-- GET ORGANIZATION DETAL
   select   numCompanyType, vcProfile, coalesce(D.tintPriceLevel,0) INTO v_numRelationship,v_numProfile,v_tintPriceLevel FROM
   DivisionMaster D
   JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID WHERE
   numDivisionID = v_numDivisionID;       

   IF v_tintPriceMethod = 1 then -- PRICE LEVEL
	
      IF(SELECT
      COUNT(*)
      FROM
      PricingTable
      WHERE
      numItemCode = v_numItemCode
      AND coalesce(numPriceRuleID,0) = 0
      AND 1 =(CASE
      WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
      THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
      ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
      END)) > 0 then
		
         select   1, 0, tintRuleType, (CASE tintRuleType
         WHEN 1 -- Deduct From List Price
         THEN
            CASE tintDiscountType
            WHEN 1 -- PERCENT
            THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(TEMP.decDiscount,0)/100)) ELSE 0 END)
            WHEN 2 -- FLAT AMOUNT
            THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty*v_fltUOMConversionFactor::bigint) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END) > 0 THEN((v_monListPrice*v_numQty*v_fltUOMConversionFactor::bigint) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END))/(v_numQty*v_fltUOMConversionFactor::bigint) ELSE 0 END) ELSE 0 END)
            WHEN 3 -- NAMED PRICE
            THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
            END
         WHEN 2 -- Add to primary vendor cost
         THEN
            CASE
            WHEN coalesce(v_bitKitParent,false) = true
            THEN
               v_monListPrice
            ELSE
               CASE tintDiscountType
               WHEN 1  -- PERCENT
               THEN (CASE 
						WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
						WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
						ELSE v_monVendorCost 
					END) + ((CASE 
								WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
								WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
								ELSE v_monVendorCost 
							END) * (coalesce(TEMP.decDiscount,0)/100))*v_fltUOMConversionFactor::bigint
               WHEN 2 -- FLAT AMOUNT
               THEN (CASE WHEN((CASE 
								WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
								WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
								ELSE v_monVendorCost 
							END)*v_numQty*v_fltUOMConversionFactor::bigint)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END) > 0 THEN((v_monVendorCost*v_numQty*v_fltUOMConversionFactor::bigint)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END))/(v_numQty*v_fltUOMConversionFactor::bigint)  ELSE 0 END)
               WHEN 3 -- NAMED PRICE
               THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
               END
            END
         WHEN 3 -- Named price
         THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
         END) INTO v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPrice FROM(SELECT
            ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
					* FROM
            PricingTable
            WHERE
            PricingTable.numItemCode = v_numItemCode
            AND coalesce(PricingTable.numPriceRuleID,0) = 0
            AND 1 =(CASE
            WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
            THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
            END)) TEMP WHERE
         numItemCode = v_numItemCode
         AND coalesce(numPriceRuleID,0) = 0
         AND 1 =(CASE
         WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
         THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
         ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
         END)
         AND 1 =(CASE
         WHEN coalesce(v_tintPriceLevel,0) > 0
         THEN(CASE WHEN Id = v_tintPriceLevel THEN 1 ELSE 0 END)
         ELSE(CASE WHEN v_numQty*v_fltUOMConversionFactor::bigint >= intFromQty AND v_numQty*v_fltUOMConversionFactor::bigint <= intToQty THEN 1 ELSE 0 END)
         END);
      end if;
      INSERT INTO tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE(monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice)
      SELECT
      coalesce(v_monPrice,v_monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			coalesce(v_monPrice,v_monListPrice);
   ELSEIF v_tintPriceMethod = 2 AND(SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID = v_numDomainID AND tintRuleFor = 1) > 0
   then -- PRICE RULE
      select 
		numPricRuleID, tintPricingMethod, p.tintRuleType,p.tintVendorCostType, p.tintDiscountType, p.decDiscount, p.intQntyItems, p.decMaxDedPerAmt,p.bitRoundTo,p.tintRoundTo  
		INTO v_numPriceRuleID,v_tintPricingMethod,v_tintPriceRuleType,v_tintVendorCostType,v_tintPriceBookDiscountType,v_decPriceBookDiscount,v_intQntyItems,v_decMaxDedPerAmt,v_bitRoundTo,v_tintRoundTo  
		FROM
      PriceBookRules p
      LEFT JOIN
      PriceBookRuleDTL PDTL
      ON
      p.numPricRuleID = PDTL.numRuleID
      LEFT JOIN
      PriceBookRuleItems PBI
      ON
      p.numPricRuleID = PBI.numRuleID
      LEFT JOIN
      PriceBookPriorities PP
      ON
      PP.Step2Value = p.tintStep2
      AND PP.Step3Value = p.tintStep3 WHERE
      p.numDomainID = v_numDomainID
      AND tintRuleFor = 1
      AND (
					((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
      OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
      OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

      OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
      OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
      OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

      OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
      OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND tintStep3 = 3) -- Priority 8
      OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
				)   ORDER BY
      PP.Priority ASC LIMIT 1;
      IF coalesce(v_numPriceRuleID,0) > 0 then
		
         IF v_tintPricingMethod = 1 then -- BASED ON PRICE TABLE
			
            select   
				tintDiscountType, coalesce(TEMP.decDiscount,0)
				,tintRuleType
				,(CASE tintRuleType 
					WHEN 1 THEN v_monListPrice 
					WHEN 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) 
				END)
				,(CASE tintRuleType
					WHEN 1 -- Deduct From List Price
					THEN
					   CASE tintDiscountType
					   WHEN 1 -- PERCENT
					   THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(TEMP.decDiscount,0)/100)) ELSE 0 END)
					   WHEN 2 -- FLAT AMOUNT
					   THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty*v_fltUOMConversionFactor::bigint) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END) > 0 THEN((v_monListPrice*v_numQty*v_fltUOMConversionFactor::bigint) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END))/(v_numQty*v_fltUOMConversionFactor::bigint) ELSE 0 END) ELSE 0 END)
					   WHEN 3 -- NAMED PRICE
					   THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
					   END
					WHEN 2 -- Add to primary vendor cost
					THEN
					   CASE
					   WHEN coalesce(v_bitKitParent,false) = true
					   THEN v_monListPrice
					   ELSE
						  CASE tintDiscountType
						  WHEN 1  -- PERCENT
						  THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)+((CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_monVendorCost 
									END)*(coalesce(TEMP.decDiscount,0)/100))
						  WHEN 2 -- FLAT AMOUNT
						  THEN(CASE WHEN((CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_monVendorCost 
									END)*v_numQty*v_fltUOMConversionFactor::bigint)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END) > 0 THEN((v_monVendorCost*v_numQty*v_fltUOMConversionFactor::bigint)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END))/(v_numQty*v_fltUOMConversionFactor::bigint)  ELSE 0 END)
						  WHEN 3 -- NAMED PRICE
						  THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
						  END
					   END
				WHEN 3 -- Named price
				THEN (CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(TEMP.decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(TEMP.decDiscount,0) END)
				END) 
			INTO v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPrice,v_monFinalPrice 
			FROM
			(SELECT
               ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
						* FROM
               PricingTable
               WHERE
               coalesce(PricingTable.numPriceRuleID,0) = v_numPriceRuleID
               AND 1 =(CASE
               WHEN EXISTS(SELECT * FROM PricingTable WHERE coalesce(PricingTable.numPriceRuleID,0) = v_numPriceRuleID AND numCurrencyID = v_numCurrencyID)
               THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
               ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
               END)) TEMP WHERE
					((tintRuleType = 3 AND Id = v_tintPriceLevel) OR (tintRuleType <> 3 AND v_numQty*v_fltUOMConversionFactor::bigint >= intFromQty AND v_numQty*v_fltUOMConversionFactor::bigint <= intToQty));
         ELSEIF v_tintPricingMethod = 2
         then -- BASED ON RULE
			
            v_monPrice := (CASE 
								WHEN COALESCE(v_bitCalculatedPrice,false) = true 
								THEN v_monCalculatedPrice 
								ELSE (CASE 
										WHEN v_tintPriceRuleType = 2 
										THEN (CASE 
												WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
												WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
												ELSE v_monVendorCost 
											END) 
										ELSE v_monListPrice 
									END) 
							END);
            IF (v_tintPriceBookDiscountType = 1) then -- Percentage 
				
               v_tintDisountType := v_tintPriceBookDiscountType;
               v_decPriceBookDiscount := v_decPriceBookDiscount*(v_numQty/v_intQntyItems::bigint);
               v_decDiscount := v_decPriceBookDiscount*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
																									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
																									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
																									ELSE v_monVendorCost 
																								END) ELSE v_monListPrice END)/100;
               v_decMaxDedPerAmt := v_decMaxDedPerAmt*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
																									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
																									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
																									ELSE v_monVendorCost 
																								END) ELSE v_monListPrice END)/100;
               IF (v_decDiscount > v_decMaxDedPerAmt) then
                  v_decDiscount := v_decMaxDedPerAmt;
               end if;
               IF (v_tintPriceRuleType = 1) then
                  v_monFinalPrice :=(v_monListPrice -v_decDiscount);
               end if;
               IF (v_tintPriceRuleType = 2) then
                  v_monFinalPrice :=((CASE 
										WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_monVendorCost 
									END)+v_decDiscount);
               end if;
               v_monFinalPrice :=(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST((coalesce(v_monFinalPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(v_monFinalPrice,0) END);
            end if;
            IF (v_tintPriceBookDiscountType = 2) then -- Flat discount 
				
               v_decDiscount :=(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST((coalesce(v_decPriceBookDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(v_decPriceBookDiscount,0) END)*((v_numQty*v_fltUOMConversionFactor::bigint)/v_intQntyItems::bigint);
               IF (v_decDiscount > v_decMaxDedPerAmt) then
                  v_decDiscount := v_decMaxDedPerAmt;
               end if;
               IF (v_tintPriceRuleType = 1) then
                  v_monFinalPrice :=((v_monListPrice*v_numQty*v_fltUOMConversionFactor::bigint) -v_decDiscount)/(v_numQty*v_fltUOMConversionFactor::bigint);
               end if;
               IF (v_tintPriceRuleType = 2) then
                  v_monFinalPrice :=(((CASE 
										WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_monVendorCost 
									END)*v_numQty*v_fltUOMConversionFactor::bigint)+v_decDiscount)/(v_numQty*v_fltUOMConversionFactor::bigint);
               end if;
            end if;
         end if;
      end if;
      INSERT INTO tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE(monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice)
      SELECT
			(CASE 
				WHEN v_monFinalPrice IS NULL 
				THEN v_monListPrice 
				ELSE (CASE 
						WHEN v_tintPriceBookDiscount = 1 
						THEN coalesce(v_monPrice,0) 
						ELSE (CASE 
									WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
									THEN ROUND(coalesce(v_monFinalPrice,0)) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
									ELSE coalesce(v_monFinalPrice,0) 
								END) 
						END) 
			END),
			(CASE WHEN v_monFinalPrice IS NULL THEN 1 ELSE(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN v_monFinalPrice IS NULL THEN 0 ELSE(CASE WHEN v_tintPriceBookDiscount = 1 THEN coalesce(v_decPriceBookDiscount,0) ELSE 0 END) END),
			(CASE 
				WHEN v_monFinalPrice IS NULL 
				THEN v_monListPrice 
				ELSE (CASE 
							WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
							THEN ROUND(coalesce(v_monFinalPrice,0)) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
							ELSE coalesce(v_monFinalPrice,0) 
						END) 
			END);
   ELSEIF v_tintPriceMethod = 3
   then -- Last Price
	
      select(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST(((coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate) AS INTEGER) ELSE(coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1)) END), coalesce(OpportunityItems.bitDiscountType,true), (CASE
      WHEN coalesce(OpportunityItems.bitDiscountType,true) = true
      THEN(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST(((coalesce(OpportunityItems.fltDiscount,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate) AS INTEGER) ELSE(coalesce(OpportunityItems.fltDiscount,0)*coalesce(OpportunityMaster.fltExchangeRate,1)) END)
      ELSE coalesce(OpportunityItems.fltDiscount,0) END), (CASE
      WHEN coalesce(OpportunityItems.fltDiscount,0) > 0 AND coalesce(numUnitHour,0) > 0
      THEN(CASE
         WHEN coalesce(OpportunityItems.bitDiscountType,true) = true
         THEN(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST(((((coalesce(monTotAmtBefDiscount,0) -coalesce(monTotAmount,0))/numUnitHour)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate) AS INTEGER) ELSE(((coalesce(monTotAmtBefDiscount,0) -coalesce(monTotAmount,0))/numUnitHour)*coalesce(OpportunityMaster.fltExchangeRate,1)) END)
         ELSE CAST((CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST(((coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate) AS INTEGER) ELSE(coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1)) END)  -((CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST(((coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate) AS INTEGER) ELSE(coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1)) END)*(coalesce(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END)
      ELSE monPrice
      END), 3 INTO v_monPrice,v_tintDisountType,v_decDiscount,v_monFinalPrice,v_tintRuleType FROM
      OpportunityItems
      JOIN
      OpportunityMaster
      ON
      OpportunityItems.numOppId = OpportunityMaster.numOppId WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND numItemCode = v_numItemCode   ORDER BY
      OpportunityMaster.numOppId DESC LIMIT 1;
      INSERT INTO tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE(monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice)
		VALUES(coalesce(v_monPrice, 0),
			v_tintDisountType,
			v_decDiscount,
			v_monFinalPrice);
   ELSE
      INSERT INTO tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE(monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice)
		VALUES(coalesce(v_monListPrice, 0),
			1,
			0,
			coalesce(v_monListPrice, 0));
   end if;

   RETURN QUERY (SELECT * FROM tt_FN_GETITEMPRICEBASEDONMETHOD_ITEMPRICE);
END; $$;

