-- Stored procedure definition script USP_ManageECampaign for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageECampaign(v_numECampaignID NUMERIC(9,0) DEFAULT 0,  
v_vcCampaignName VARCHAR(100) DEFAULT '',  
v_txtDesc TEXT DEFAULT '',  
v_strECampaignDTLs TEXT DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT 0 ,
v_dtStartTime TIMESTAMP DEFAULT NULL,
v_fltTimeZone DOUBLE PRECISION DEFAULT NULL,
v_tintTimeZoneIndex SMALLINT DEFAULT NULL,
v_tintFromField SMALLINT DEFAULT NULL,
v_numFromContactID NUMERIC DEFAULT NULL,
v_numUserContactID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_numECampaignID = 0 then

      insert into ECampaign(vcECampName,txtDesc,numDomainID,dtStartTime,tintFromField,numFromContactID,fltTimeZone,tintTimeZoneIndex,numCreatedBy)
values(v_vcCampaignName,v_txtDesc,v_numDomainID,v_dtStartTime,v_tintFromField,v_numFromContactID,v_fltTimeZone,v_tintTimeZoneIndex,v_numUserContactID);

      v_numECampaignID := CURRVAL('ECampaign_seq');

      insert into ECampaignDTLs(numECampID,numEmailTemplate,numActionItemTemplate,tintDays,tintWaitPeriod,numFollowUpID)        --,[numEmailGroupID],[dtStartDate]  ,@numEmailGroupID,@dtStartDate
         
		select 
			v_numECampaignID,X.numEmailTemplate,X.numActionItemTemplate,x.tintDays,X.tintWaitPeriod,X.numFollowUpID 
		from
		 XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strECampaignDTLs AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numEmailTemplate NUMERIC(9,0) PATH 'numEmailTemplate',
				numActionItemTemplate NUMERIC(9,0) PATH 'numActionItemTemplate',
				tintDays SMALLINT PATH 'tintDays',
				tintWaitPeriod SMALLINT PATH 'tintWaitPeriod',
				numFollowUpID NUMERIC PATH 'numFollowUpID'
		) AS X;

   else
      update ECampaign set vcECampName = v_vcCampaignName,txtDesc = v_txtDesc,dtStartTime = v_dtStartTime,
      fltTimeZone = v_fltTimeZone,tintTimeZoneIndex = v_tintTimeZoneIndex,tintFromField = v_tintFromField,
      numFromContactID = v_numFromContactID
      where numECampaignID = v_numECampaignID;

      update ECampaignDTLs set
      numEmailTemplate = X.numEmailTemplate,numActionItemTemplate = X.numActionItemTemplate,
      tintDays = X.tintDays,tintWaitPeriod = X.tintWaitPeriod,numFollowUpID = X.numFollowUpID
      From
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strECampaignDTLs AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numECampDTLId NUMERIC(9,0) PATH 'numECampDTLId',
				numEmailTemplate NUMERIC(9,0) PATH 'numEmailTemplate',
				numActionItemTemplate NUMERIC(9,0) PATH 'numActionItemTemplate',
				tintDays SMALLINT PATH 'tintDays',
				tintWaitPeriod SMALLINT PATH 'tintWaitPeriod',
				numFollowUpID NUMERIC PATH 'numFollowUpID'
		) AS X 
      where  ECampaignDTLs.numECampDTLId = X.numECampDTLId;

   end if;  
  
   open SWV_RefCur for select v_numECampaignID;
/****** Object:  StoredProcedure [dbo].[Usp_ManageInboxTree]    Script Date: 07/26/2008 16:19:51 ******/
   RETURN;
END; $$;












