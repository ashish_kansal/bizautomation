-- Stored procedure definition script USP_GetShippingExceptions for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingExceptions(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numShippingExceptionID as "numShippingExceptionID",
		(SELECT vcData from Listdetails LD JOIN ShippingExceptions SE ON LD.numListItemID = SE.numClassificationID WHERE LD.numListItemID = SE.numClassificationID) as "vcClassification"
		,PercentAbove as "PercentAbove"
		,FlatAmt as "FlatAmt"
	FROM 
		ShippingExceptions 
	WHERE 
		numDomainID = v_numDomainID;

   RETURN;
END; $$;













