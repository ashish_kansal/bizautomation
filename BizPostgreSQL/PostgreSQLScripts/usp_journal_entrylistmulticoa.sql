-- Stored procedure definition script USP_Journal_EntryListMultiCOA for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Journal_EntryListMultiCOA(v_dtDateFrom TIMESTAMP,                        
v_numDomainId NUMERIC(9,0),
v_numDivisionID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--@numChartAcntId as numeric(9),                                                                                                                          
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);                                                                                                                      
   v_strChildCategory  VARCHAR(5000);                                                                              
   v_numAcntTypeId  INTEGER;                                                                                                                 
   v_OpeningBal  VARCHAR(8000);  
   v_vcAccountCode  VARCHAR(50);
   SWV_ExecDyn  VARCHAR(5000);
   v_TotRecs  NUMERIC(9,0);                                                                                            
   v_i  NUMERIC(9,0);                                                                                                  
   v_numCustId  VARCHAR(9);                                                       
   v_numPayment  DECIMAL(20,5);                                                                                          
   v_numDeposit  DECIMAL(20,5);                                                             
   v_numBalanceAmt  DECIMAL(20,5);                                                                                                
   v_bitBalance  BOOLEAN;                                                                 
   v_numCheckId  NUMERIC(9,0);                                                            
   v_numCashCreditCardId  NUMERIC(9,0);          
   v_numOppBizDocsId  NUMERIC(9,0);                                                                
--Declare @numCashCreditCardId as numeric(9)                                                                                         
   v_j  NUMERIC(9,0);                                     
   v_numCustomerId  NUMERIC(9,0);                                                               
   v_bitChargeBack  BOOLEAN;                                                
   v_bitMoneyOut  BOOLEAN;
BEGIN
   v_OpeningBal := 'Opening Balance';                                                                                                  
   v_strSQL := '';                                                                                              
--Create a Temporary table to hold data      

   drop table IF EXISTS tt_TEMPCOA CASCADE;
   create TEMPORARY TABLE tt_TEMPCOA
   (
      numChartAcntId NUMERIC(18,0)
   );

   SELECT max(dtPeriodFrom) INTO v_dtDateFrom FROM FinancialYear where bitCurrentYear = true and numDomainId = v_numDomainId;

   INSERT INTO tt_TEMPCOA
   select coalesce(numARAccountID,0) AS numChartAccountID from COARelationships CR,
							CompanyInfo CI,
							DivisionMaster DM
   WHERE CR.numRelationshipID = CI.numCompanyType and
   CR.numDomainID = DM.numDomainID and
   CI.numCompanyId = DM.numCompanyID and
   DM.numDivisionID = v_numDivisionID AND
   DM.numDomainID = v_numDomainId
   UNION
   select coalesce(numAPAccountID,0) from COARelationships CR,
							CompanyInfo CI,
							DivisionMaster DM
   WHERE CR.numRelationshipID = CI.numCompanyType and
   CR.numDomainID = DM.numDomainID and
   CI.numCompanyId = DM.numCompanyID and
   DM.numDivisionID = v_numDivisionID AND
   DM.numDomainID = v_numDomainId;


                                                                                                                                
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCustomerId NUMERIC(9,0),
      JournalId NUMERIC(9,0),
      TransactionType VARCHAR(500),
      EntryDate TIMESTAMP,
      CompanyName VARCHAR(8000),
      CheckId NUMERIC(9,0),
      CashCreditCardId NUMERIC(9,0),
      Memo VARCHAR(8000),
      Payment DECIMAL(20,5),
      Deposit DECIMAL(20,5),
      numBalance DECIMAL(20,5),
      numChartAcntId NUMERIC(9,0),
      numTransactionId NUMERIC(9,0),
      numOppID NUMERIC(9,0),
      numOppBizDocsId NUMERIC(9,0),
      numDepositId NUMERIC(9,0),
      numBizDocsPaymentDetId NUMERIC(9,0),
      numCategoryHDRID NUMERIC(9,0),
      tintTEType   NUMERIC(9,0),
      numCategory  NUMERIC(9,0),
      numUserCntID   NUMERIC(9,0),
      dtFromDate TIMESTAMP,
      numCheckNo VARCHAR(20),
      bitReconcile BOOLEAN,
      bitCleared BOOLEAN
   );                                                                                                                                      
                                            
   if v_dtDateFrom = '1900-01-01 00:00:00.000' then 
      v_dtDateFrom := CAST('Jan  1 1753 12:00:00:000AM' AS TIMESTAMP);
   end if;                                               
   
   select   vcAccountCode INTO v_vcAccountCode FROM Chart_Of_Accounts WHERE numAccountId in(select numChartAcntId from tt_TEMPCOA); -- = @numChartAcntId
   select   COALESCE(coalesce(v_strChildCategory,'') || ',','') ||
   SUBSTR(CAST(numAccountId AS VARCHAR(10)),1,10) INTO v_strChildCategory FROM Chart_Of_Accounts WHERE vcAccountCode ilike coalesce(v_vcAccountCode,'') || '%';

   RAISE NOTICE '%',v_strChildCategory;
                                           
   if v_dtDateFrom = 'Jan  1 1753 12:00:00:000AM' then
                                                                                  
--Commented by chintan
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                           

                                                                          
      select   coalesce(numParntAcntTypeID,0) INTO v_numAcntTypeId from Chart_Of_Accounts Where numAccountId in(select MAX(numChartAcntId) from tt_TEMPCOA)  And numDomainId = v_numDomainId;
      RAISE NOTICE '%',v_numAcntTypeId;
      if v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then

         v_strSQL := '  Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks''  
Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then ''Cash''        
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then ''Charge''        
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 1 then ''BizDocs Invoice''        
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 2 then ''BizDocs Purchase''        
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 1 then ''Receive Amt''        
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 2 then ''Vendor Amt''        
Else Case when coalesce(GJH.numCategoryHDRID,0) <> 0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <> 0 then ''Journal'' End End End End End End End End End End  as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit,        
(case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,        
null AS numBalance,        
coalesce(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,               
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,          
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
coalesce(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
coalesce(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
coalesce(CAST(CD.numCheckNo AS VARCHAR(20)),'''') AS numCheckNo,
coalesce(GJD.bitReconcile,false) as bitReconcile,
coalesce(GJD.bitCleared,false) as bitCleared      
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id = GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId = Opp.numOppId    
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID = TE.numCategoryHDRID     
Where  GJH.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4)       
--And GJH.numChartAcntId is null      
         || ' ORDER BY GJH.datEntry_Date';
      Else
         v_strSQL := ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''  
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''        
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt<>0 then numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared             
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' And (GJD.numCustomerId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' or ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' = 0)'
         || ' ORDER BY GJH.datEntry_Date';
      end if;
      RAISE NOTICE '%',(v_strChildCategory);
      RAISE NOTICE '%',(v_strSQL);
   Else                                                                               
--Set @strChildCategory = dbo.fn_ChildCategory((select MAX(numChartAcntId) from #TempCoa),@numDomainId) + Convert(varchar(10),(select MAX(numChartAcntId) from #TempCoa))                                                                                                               
----Set @strChildCategory = @strChildCategory + ',' + dbo.fn_ChildCategory((select MIN(numChartAcntId) from #TempCoa),@numDomainId) + Convert(varchar(10),(select MIN(numChartAcntId) from #TempCoa))                                                                                                               

      select   coalesce(numParntAcntTypeID,0) INTO v_numAcntTypeId from Chart_Of_Accounts Where numAccountId in(select MAX(numChartAcntId) from tt_TEMPCOA);
      if v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then

         v_strSQL := coalesce(v_strSQL,'') || ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,         
case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then ''Cash''        
ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then  ''Charge''        
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 1 then ''BizDocs Invoice''        
ELse Case when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 2 then ''BizDocs Purchase''        
ELse Case when coalesce(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 1 then ''Receive Amt''        
ELse Case when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 2 then ''Vendor Amt''        
Else Case when coalesce(GJH.numCategoryHDRID,0) <> 0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <> 0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit,        
(case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,        
null AS numBalance,        
coalesce(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
coalesce(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
coalesce(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
coalesce(CAST(CD.numCheckNo AS VARCHAR(20)),'''') AS numCheckNo,
coalesce(GJD.bitReconcile,false) as bitReconcile,
coalesce(GJD.bitCleared,false) as bitCleared 
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id = GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID        
Left outer join CompanyInfo as CI on DM.numCompanyID = CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId = Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID = TE.numCategoryHDRID        
Where GJH.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' And GJH.datEntry_Date >= ''' || SUBSTR(CAST(v_dtDateFrom AS VARCHAR(300)),1,300) || ''' And (GJD.numCustomerId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' or ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' = 0)'
         || ' ORDER BY GJH.datEntry_Date';
      Else
         v_strSQL := ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''    
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''      
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo, 
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId        
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID   
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || 'And GJH.datEntry_Date>=''' || SUBSTR(CAST(v_dtDateFrom AS VARCHAR(300)),1,300) || ''' And (GJD.numCustomerId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' or ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || ' = 0)'
         || ' ORDER BY GJH.datEntry_Date';
      end if;
      RAISE NOTICE '%',(v_strChildCategory);
      RAISE NOTICE '%',(v_strSQL);
   end if;                                                                                                                                                                                 
   SWV_ExecDyn := 'insert into tt_TEMPTABLE' || coalesce(v_strSQL,'');
   EXECUTE SWV_ExecDyn;                                                                                           
   v_bitBalance := false;                         
   v_i := 1;                                                                                                  
   v_j := 0;                                               
                                         
                                                        
                                                                                               
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                                                        
   RAISE NOTICE '%',(v_TotRecs);                                                                                                  
   While v_i <= v_TotRecs LOOP
      select   coalesce(numCustomerId,0), coalesce(Payment,0), coalesce(Deposit,0), coalesce(CheckId,0), coalesce(CashCreditCardId,0), coalesce(numOppBizDocsId,0) INTO v_numCustId,v_numPayment,v_numDeposit,v_numCheckId,v_numCashCreditCardId,
      v_numOppBizDocsId From tt_TEMPTABLE Where ID = v_i;
      RAISE NOTICE '@CheckId1%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
      select   bitChargeBack, bitMoneyOut INTO v_bitChargeBack,v_bitMoneyOut From CashCreditCardDetails Where numCashCreditId = v_numCashCreditCardId And numDomainID = v_numDomainId;
      RAISE NOTICE '%',v_bitChargeBack;
      RAISE NOTICE '%',v_bitMoneyOut;
      if v_i = 1 then
 
         If v_numPayment <> 0 then
                                                                  
  -- print '@numPayment' +convert(varchar(50),@numPayment)                                                              
 --  Print 'Siva'                                                               
 -- print '@numAcntTypeId' +Convert(varchar(50),@numAcntTypeId)                                                     
            Update tt_TEMPTABLE Set numBalance = -v_numPayment Where ID = v_i;
            if v_numAcntTypeId = 815 then
     
               if v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  if  v_numCashCreditCardId <> 0 then
            
                     if v_bitChargeBack = false And v_bitMoneyOut = true then
                        Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                  end if;
               Else
                  Update tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 then
               Update tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
            end if;
         else
            Update  tt_TEMPTABLE Set numBalance = v_numDeposit Where ID = v_i;
            if v_numAcntTypeId = 815 then
    
               if v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
        
                  if  v_numCashCreditCardId <> 0 then
            
                     if v_bitChargeBack = false And v_bitMoneyOut = true then
                        Update tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     Else
                        Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                  end if;
               else
                  Update tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 And v_numOppBizDocsId = 0 then
               Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
            Else
               Update tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
            end if;
         end if;
      end if;
      if v_i > 1 then

         if v_numPayment <> 0 then

            v_j := v_i -1;
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update tt_TEMPTABLE Set numBalance = v_numBalanceAmt -v_numPayment Where ID = v_i;
            if v_numAcntTypeId = 815 then
   
               if v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  if  v_numCashCreditCardId <> 0 then
            
                     if v_bitChargeBack = false And v_bitMoneyOut = true then
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = -v_numPayment Where ID = v_i;
                     Else
                        Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                     end if;
                  Else
                     Update tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
                  end if;
               Else
                  Update  tt_TEMPTABLE Set Deposit = -v_numPayment,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 then
               Update tt_TEMPTABLE Set Deposit = null,Payment = v_numPayment Where ID = v_i;
            end if;
         Else
            v_j := v_i -1;
            RAISE NOTICE '@CheckId2%',SUBSTR(CAST(v_numCheckId AS VARCHAR(30)),1,30);
            select   numBalance INTO v_numBalanceAmt From tt_TEMPTABLE Where ID = v_j;
            Update tt_TEMPTABLE Set numBalance = v_numBalanceAmt+v_numDeposit Where ID = v_i;
            if v_numAcntTypeId = 815 then
  
               If v_numCheckId <> 0 Or v_numCashCreditCardId <> 0 then
       
                  if  v_numCashCreditCardId <> 0 then
            
                     if v_bitChargeBack = false And v_bitMoneyOut = true then
                        Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
                     Else
                        Update tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                     end if;
                  Else
                     Update  tt_TEMPTABLE Set Deposit = null,Payment = v_numDeposit Where ID = v_i;
                  end if;
               Else
                  Update tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
               end if;
            end if;
            if v_numAcntTypeId = 814 And v_numOppBizDocsId = 0 then
               Update tt_TEMPTABLE Set Deposit = null,Payment = -v_numDeposit Where ID = v_i;
            Else
               Update tt_TEMPTABLE Set Deposit = v_numDeposit,Payment = null Where ID = v_i;
            end if;
         end if;
      end if;
      v_i := v_i+1;
   END LOOP;                  
   drop table IF EXISTS tt_TEMPCOA CASCADE;                                                                            
   open SWV_RefCur for Select numCustomerId,JournalId,TransactionType,EntryDate,CompanyName,
CheckId,CashCreditCardId,Memo,MAX(coalesce(Payment,0)) AS Payment, max(coalesce(Deposit,0)) as Deposit,max(coalesce(numBalance,0)) as numBalance ,0 as numChartAcntId, 0 as numTransactionID
 ,numOppID,numOppBizDocsId,numDepositId,numBizDocsPaymentDetId,numCategoryHDRID,
tintTEType,numCategory,numUserCntID,dtFromDate,numCheckNo  from tt_TEMPTABLE
   group by
   numCustomerId,JournalId,TransactionType,EntryDate,CompanyName,CheckId,
   CashCreditCardId,Memo,numOppID,numOppBizDocsId,numDepositId,numOppID,numOppBizDocsId,
   numBizDocsPaymentDetId,numCategoryHDRID,tintTEType,numCategory,
   numUserCntID,dtFromDate,numCheckNo; 
                                                                                              
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;












