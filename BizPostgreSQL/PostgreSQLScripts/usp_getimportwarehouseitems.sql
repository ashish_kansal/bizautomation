CREATE OR REPLACE FUNCTION USP_GetImportWareHouseItems
(
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numItemGroupID NUMERIC(9,0) DEFAULT 0,
    v_byteMode SMALLINT DEFAULT 0,
    v_bitSerialized BOOLEAN DEFAULT false,
    v_bitLotNo BOOLEAN DEFAULT false, 
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_str  VARCHAR(2000);
	v_str1  VARCHAR(2000);               
	v_ColName  VARCHAR(25);                      
	v_strSQL  TEXT;
	v_strWHERE  TEXT;
	v_ID  INTEGER;
	v_vcFormFieldName  VARCHAR(50);
	v_numFormFieldId  NUMERIC(18,0);
	v_tintOrder  SMALLINT;
	v_cCtype  CHAR(1);
	v_vcAssociatedControlType  VARCHAR(50);
	v_vcDbColumnName  VARCHAR(50);
	v_vcLookBackTableName  VARCHAR(20);
	SWV_RowCount INTEGER;
BEGIN
	v_str := '';               
	BEGIN
		CREATE TEMP SEQUENCE tt_tempTable_seq;
		EXCEPTION WHEN OTHERS THEN NULL;
	END;
   
	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPTABLE
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
		PRIMARY KEY,
		vcFormFieldName VARCHAR(50),
		numFormFieldId NUMERIC(18,0),
		tintOrder SMALLINT,
		cCtype CHAR(1),
		vcAssociatedControlType VARCHAR(50),
		vcDbColumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50)
	);                         
	
	v_strWHERE := ' WHERE numFormID = 48 AND coalesce(DFFM.bitImport,false) <> false ';
	v_strSQL := 'INSERT INTO tt_TEMPTABLE
						(vcFormFieldName,numFormFieldId,tintOrder,cCtype,vcAssociatedControlType,vcDbColumnName,vcLookBackTableName)
					SELECT 
						DFM.vcFieldName AS vcFormFieldName,
						DFM.numFieldId AS numFormFieldId,
						coalesce(DFFM."order",0) as tintOrder,
						''R'' as cCtype,
						DFM.vcAssociatedControlType AS vcAssociatedControlType,
						DFM.vcDbColumnName AS vcDbColumnName,
						coalesce(DFM.vcLookBackTableName,'''') AS vcLookBackTableName
					FROM 
						DycFieldMaster DFM
					INNER JOIN 
						DycFormField_Mapping DFFM 
					ON 
						DFM.numFieldId = DFFM.numFieldID '; 
            
	IF coalesce(v_numItemGroupID,0) > 0 then
		v_strWHERE := coalesce(v_strWHERE,'') || ' AND DFM.numFieldId IN (SELECT numFieldID FROM DycFormField_Mapping WHERE numFormID = 48)';
		
		v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strWHERE,'') || ' 
					UNION
					SELECT  
						Fld_label AS vcFormFieldName,
						Fld_id AS numFormFieldId,
						CAST(100 AS SMALLINT) AS tintOrder,
						''C'' AS cCtype,
						Fld_Type AS vcAssociatedControlType,
						'''' AS vcDbColumnName,
						'''' AS vcLookBackTableName
					FROM 
						CFW_Fld_Master
					JOIN 
						ItemGroupsDTL ON numOppAccAttrID = Fld_id
					WHERE 
						numItemGroupID = ' || v_numItemGroupID || ' 
						AND tintType = 2
					ORDER BY 
						tintOrder';
	ELSE
		v_strWHERE := coalesce(v_strWHERE,'') || ' AND DFM.numFieldID NOT IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN (''UPC (M)'',''SKU (M)'',''Serial No'',''Comments'',''Lot Qty''))';
		v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strWHERE,'') || ' ORDER BY tintOrder ';
	end if;
                  
	RAISE NOTICE '%',v_strSQL;
	EXECUTE v_strSQL;

	IF v_byteMode = 1 then    
		SELECT 
			ID
			,vcFormFieldName
			,numFormFieldId
			,tintOrder
			,cCtype
			,vcAssociatedControlType
			,vcDbColumnName
			,vcLookBackTableName 
		INTO 
			v_ID
			,v_vcFormFieldName
			,v_numFormFieldId
			,v_tintOrder
			,v_cCtype
			,v_vcAssociatedControlType
			,v_vcDbColumnName
			,v_vcLookBackTableName 
		FROM 
			tt_TEMPTABLE
		ORDER BY
			ID
		LIMIT 1;

		WHILE v_ID > 0 LOOP
			IF v_ID > 1 then
				v_str := coalesce(v_str,'') || ',';
			end if;

			IF v_vcDbColumnName = 'numWareHouseID' then
				v_vcDbColumnName := 'vcWareHouse';
			end if;
			IF v_cCtype = 'R' then
				IF v_vcDbColumnName = 'vcLocation' then       
					v_str := coalesce(v_str,'') || 'WL.' || coalesce(v_vcDbColumnName,'') || ' as "' || coalesce(v_vcFormFieldName,'') || '"';
				ELSE
					v_str := coalesce(v_str,'') || coalesce(v_vcLookBackTableName,'') || '.' || coalesce(v_vcDbColumnName,'') || ' as "' || coalesce(v_vcFormFieldName,'') || '"';
				end if;
			ELSE
				v_str := coalesce(v_str,'') || ' GetCustFldItemsValue('
						|| COALESCE(v_numFormFieldId,0)
						|| ',9,WareHouseItems.numWareHouseItemID,0,'''
						|| coalesce(v_vcAssociatedControlType,'') || ''') as "'
						|| coalesce(v_vcFormFieldName,'') || '"';
			end if;
         
			SELECT 
				ID
				,vcFormFieldName
				,numFormFieldId, tintOrder
				,cCtype
				,vcAssociatedControlType
				,vcDbColumnName
				,vcLookBackTableName 
			INTO 
				v_ID
				,v_vcFormFieldName
				,v_numFormFieldId
				,v_tintOrder
				,v_cCtype
				,v_vcAssociatedControlType
				,v_vcDbColumnName
				,v_vcLookBackTableName 
			FROM 
				tt_TEMPTABLE 
			WHERE 
				ID > v_ID 
			ORDER BY 
				ID LIMIT 1;
			
			GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
			
			IF SWV_RowCount = 0 then
				v_ID := 0;
			end if;
		END LOOP;

		v_str := RTRIM(v_str);
		v_str1 := ' select ' || coalesce(v_str,'') || ' 
					from Item 
					LEFT join WareHouseItems on Item.numItemCode=WareHouseItems.numItemID
					LEFT join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID                                    
					LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					LEFT JOIN WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
					where Item.numDomainID=' || COALESCE(v_numDomainID,0) || ' 
					AND Item.numItemGroup=' || COALESCE(v_numItemGroupID,0) || ' 
					ORDER BY WareHouseItems.numWareHouseItemID DESC';
		RAISE NOTICE '%',(v_str1);
		OPEN SWV_RefCur FOR EXECUTE v_str1;
	ELSEIF v_byteMode = 2 then
		select 
			ID
			,vcFormFieldName
			,numFormFieldId
			,tintOrder
			,cCtype
			,vcAssociatedControlType
			,vcDbColumnName
			,vcLookBackTableName 
		INTO 
			v_ID
			,v_vcFormFieldName
			,v_numFormFieldId
			,v_tintOrder
			,v_cCtype
			,v_vcAssociatedControlType
			,v_vcDbColumnName
			,v_vcLookBackTableName 
		FROM 
			tt_TEMPTABLE
		ORDER BY
			ID
		LIMIT 1;

		WHILE v_ID > 0 LOOP
			IF v_ID > 1 then
				v_str := coalesce(v_str,'') || ',';
			end if;

			IF v_vcDbColumnName = 'numWareHouseID' then
				v_vcDbColumnName := 'vcWareHouse';
			end if;

			IF v_cCtype = 'R' then
				IF v_vcDbColumnName = 'vcLocation' then
					v_str := coalesce(v_str,'') || 'WL.' || coalesce(v_vcDbColumnName,'') || ' as "' || coalesce(v_vcFormFieldName,'') || '"';
				ELSE
					v_str := coalesce(v_str,'') || coalesce(v_vcLookBackTableName,'') || '.' || coalesce(v_vcDbColumnName,'') || ' as "' || coalesce(v_vcFormFieldName,'') || '"';
				end if;
			ELSE
				v_str := coalesce(v_str,'') || ' GetCustFldItemsValue('
						|| COALESCE(v_numFormFieldId,0)
						|| ',9,numWareHouseItmsDTLID,0,'''
						|| coalesce(v_vcAssociatedControlType,'') || ''') as "'
						|| coalesce(v_vcFormFieldName,'') || '"';
			end if;
			
			select 
				ID
				,vcFormFieldName
				,numFormFieldId
				,tintOrder
				,cCtype
				,vcAssociatedControlType
				,vcDbColumnName
				,vcLookBackTableName 
			INTO 
				v_ID
				,v_vcFormFieldName
				,v_numFormFieldId
				,v_tintOrder
				,v_cCtype
				,v_vcAssociatedControlType
				,v_vcDbColumnName
				,v_vcLookBackTableName 
			FROM 
				tt_TEMPTABLE
			WHERE 
				ID > v_ID
			ORDER BY
				ID;

			GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;

			IF SWV_RowCount = 0 then
				v_ID := 0;
			end if;
		END LOOP;
		
		RAISE NOTICE '%',v_str1;
		
		v_str1 := ' SELECT   DISTINCT ' || coalesce(v_str,'') || ',WareHouseItmsDTL.vcComments AS Comments';

		IF v_bitLotNo = true then
		   v_str1 := coalesce(v_str1,'') || ',WareHouseItmsDTL.numQty AS Qty';
		end if;

		v_str1 := coalesce(v_str1,'')|| ' FROM ITEM 
										LEFT JOIN WareHouseItems  ON Item.numItemCode=WareHouseItems.numItemID
										LEFT JOIN Warehouses ON Warehouses.numWareHouseID=WareHouseItems.numWareHouseID      
										LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
										LEFT JOIN WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
										WHERE Item.numDomainID='|| COALESCE(v_numDomainID,0) || ' 
										AND Item.numItemGroup=' || COALESCE(v_numItemGroupID,0) || '
										AND COALESCE(WareHouseItmsDTL.numQty,0)>0 
										AND (Item.bitSerialized=' || COALESCE(v_bitSerialized,false) || ' AND Item.bitLotNo=' || COALESCE(v_bitLotNo,false) || ')' || ' 
										ORDER BY WareHouseItems.numWareHouseItemID DESC';

		v_str1 := REPLACE(v_str1,'"Serial No"','"Serial/Lot #s"');
		v_str1 := REPLACE(v_str1,'WareHouseItmsDTL.vcSerialNo','NULL');
		v_str1 := REPLACE(v_str1,'WareHouseItmsDTL.vcComments','NULL');
		v_str1 := REPLACE(v_str1,'WareHouseItmsDTL.vcSerialNo','NULL');
		v_str1 := REPLACE(v_str1,'WareHouseItmsDTL.numQty','NULL');
		v_str1 := REPLACE(v_str1,'WareHouseItmsDTL.numQty','NULL');

		RAISE NOTICE '%',(v_str1);
		OPEN SWV_RefCur FOR EXECUTE v_str1;
	ELSE
		open SWV_RefCur for
		SELECT  
			vcFormFieldName,
            numFormFieldId,
            tintOrder,
            cCtype,
            vcAssociatedControlType,
            vcDbColumnName,
            vcLookBackTableName
		FROM 
			tt_TEMPTABLE
		ORDER BY 
			tintOrder;
	end if;

	RETURN;
END; $$;