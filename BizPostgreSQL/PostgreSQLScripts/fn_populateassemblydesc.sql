-- Function definition script fn_PopulateAssemblyDesc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_PopulateAssemblyDesc(v_numOppItemCode NUMERIC(9,0),v_Units DOUBLE PRECISION)
RETURNS VARCHAR(300) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Desc  VARCHAR(300);
   v_numChildItem  NUMERIC(9,0);
   v_Quantity  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   v_Desc := '';
   v_numChildItem := 0;

   select   numChildItemID, numQtyItemsReq INTO v_numChildItem,v_Quantity from ItemDetails ID
   join Item I
   on ID.numChildItemID = I.numItemCode
   Join OpportunityItems OI
   on OI.numItemCode = ID.numItemKitID where numoppitemtCode = v_numOppItemCode   order by numChildItemID LIMIT 1;

   while v_numChildItem > 0 LOOP
      select   coalesce(v_Desc,'') || vcItemName || '(' || CAST(v_Quantity::bigint*v_Units AS VARCHAR(30)) || ' ' || coalesce(vcUnitofMeasure,'Units') || '), ' INTO v_Desc from Item where numItemCode = v_numChildItem;
      select   numChildItemID, numQtyItemsReq INTO v_numChildItem,v_Quantity from ItemDetails ID
      join Item I
      on ID.numChildItemID = I.numItemCode
      Join OpportunityItems OI
      on OI.numItemCode = ID.numItemKitID where numoppitemtCode = v_numOppItemCode and numChildItemID > v_numChildItem   order by numChildItemID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numChildItem := 0;
      end if;
   END LOOP;

   return v_Desc;
END; $$;

