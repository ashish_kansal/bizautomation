-- Stored procedure definition script usp_GetArrayListOfStates for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetArrayListOfStates(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcState  VARCHAR(1);
BEGIN
   open SWV_RefCur for SELECT Distinct cast(numCountryId as VARCHAR(255)), fn_GetStateInACountry(numCountryId,v_numDomainId,'NM') AS vcState,
fn_GetStateInACountry(numCountryId,v_numDomainId,'ID') AS vcStateIds
   FROM AllCountriesAndStates AllStates
   WHERE (numDomainID = v_numDomainId  OR constFlag = 1)
   ORDER BY numCountryId;
END; $$;












