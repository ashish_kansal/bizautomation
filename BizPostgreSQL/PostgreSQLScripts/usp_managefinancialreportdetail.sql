-- Stored procedure definition script USP_ManageFinancialReportDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFinancialReportDetail(INOUT v_numFRDtlID NUMERIC(18,0) DEFAULT 0 ,
    v_numFRID NUMERIC(18,0) DEFAULT NULL,
    v_numValue1 NUMERIC(18,0) DEFAULT null,
    v_numValue2 NUMERIC(18,0) DEFAULT null,
    v_vcValue1 VARCHAR(50) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numFRDtlID > 0 then
    
      UPDATE  FinancialReportDetail
      SET     numFRID = v_numFRID,numValue1 = v_numValue1,numValue2 = v_numValue2,vcValue1 = v_vcValue1
      WHERE   numFRDtlID = v_numFRDtlID;
   ELSE
      INSERT  INTO FinancialReportDetail(numFRID,
                                                     numValue1,
                                                     numValue2,
                                                     vcValue1)
      SELECT  v_numFRID,
                        v_numValue1,
                        v_numValue2,
                        v_vcValue1;
      v_numFRDtlID := CURRVAL('FinancialReportDetail_seq');
   end if;
   RETURN;
END; $$;	
	


