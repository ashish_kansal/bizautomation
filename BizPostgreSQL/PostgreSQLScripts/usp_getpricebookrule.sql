DROP FUNCTION IF EXISTS USP_GetPriceBookRule;

CREATE OR REPLACE FUNCTION USP_GetPriceBookRule(v_numPricRuleID NUMERIC(9,0) DEFAULT 0,
    v_byteMode SMALLINT DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_bitSort BOOLEAN DEFAULT NULL,
    v_numItemCode NUMERIC(9,0) DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF v_byteMode = 0 then        
		OPEN SWV_RefCur FOR
		SELECT 
			numPricRuleID,
            vcRuleName,
            coalesce(tintRuleType,0) AS tintRuleType,
			tintVendorCostType,
            CASE 
				WHEN tintDiscountType = 1 
				THEN CAST(coalesce(decDiscount,0) AS DECIMAL(9,2))
				ELSE CAST(coalesce(decDiscount,0) AS INTEGER) 
			END AS decDiscount,
			CASE WHEN intQntyItems = 0 THEN 1 ELSE intQntyItems END AS intQntyItems,
            decMaxDedPerAmt,
            numDomainID,
            coalesce(vcRuleDescription,'') AS vcRuleDescription,
            coalesce(tintDiscountType,0) AS tintDiscountType,
            coalesce(tintPricingMethod,0) AS tintPricingMethod,
            coalesce(tintStep2,0) AS tintStep2,
            coalesce(tintStep3,0) AS tintStep3,
			CASE coalesce(tintStep2,0)
			  WHEN 1 THEN(SELECT COUNT(*) FROM PriceBookRuleItems PD WHERE PD.numRuleID = numPricRuleID AND PD.tintType = 1)
			  WHEN 2 THEN(SELECT COUNT(*) FROM PriceBookRuleItems PD WHERE PD.numRuleID = numPricRuleID AND PD.tintType = 2)
			END AS Step2Count,
            CASE coalesce(tintStep3,0)
				WHEN 1 THEN(SELECT COUNT(*) FROM PriceBookRuleDTL PD WHERE PD.numRuleID = numPricRuleID AND PD.tintType = 3)
				WHEN 2 THEN(SELECT COUNT(*) FROM PriceBookRuleDTL PD WHERE PD.numRuleID = numPricRuleID AND PD.tintType = 4)
			END AS Step3Count,
			tintRuleFor,
			bitRoundTo,
			tintRoundTo
		FROM 
			PriceBookRules
		WHERE 
			numPricRuleID = v_numPricRuleID;
	ELSEIF v_byteMode = 1 AND v_bitSort = true THEN
		OPEN SWV_RefCur FOR
		SELECT 
			numPricRuleID,
            vcRuleName,
            'Deduct ' 
			|| CASE WHEN tintDiscountType = 0 THEN CAST(decDiscount AS VARCHAR(20)) || '%' ELSE CAST(CAST(decDiscount AS INTEGER) AS VARCHAR(20)) END 
			|| ' for every '
			|| CAST(CASE WHEN intQntyItems = 0 THEN 1 ELSE intQntyItems END AS VARCHAR(20))
			|| ' units, until maximum of '
			|| CAST(decMaxDedPerAmt AS VARCHAR(20)) 
			|| ' %' AS PricingRule,
            coalesce(PP.Priority,0) AS Priority,
            CASE WHEN tintStep2 = 3 THEN 'All Items' ELSE GetPriceBookItemList(numPricRuleID,tintStep2) END AS ItemList,
            CASE WHEN tintStep3 = 3 THEN 'All Customers' ELSE GetPriceBookItemList(numPricRuleID,CASE tintStep3 WHEN 1 THEN 3 WHEN 2 THEN 4 END::SMALLINT) END AS CustomerList,
			CASE WHEN PB.tintRuleFor = 2 THEN 'Purchase' ELSE 'Sale' END AS vcRuleFor,
			PB.bitRoundTo,
			PB.tintRoundTo
		FROM 
			PriceBookRules PB
		LEFT OUTER JOIN 
			PriceBookPriorities PP 
		ON 
			PP.Step2Value = PB.tintStep2
			AND PP.Step3Value = PB.tintStep3
		WHERE 
			numDomainID = v_numDomainID 
			AND 1 =(CASE 
						WHEN v_numItemCode > 0 
						THEN CASE 
								WHEN PB.numPricRuleID IN (SELECT * FROM fn_GetItemPriceRule(v_numDomainID,2::SMALLINT,v_numItemCode,v_numDivisionID)) 
								THEN 1 
								ELSE 0 
							END 
						ELSE 1 
					END)
		ORDER BY 
			PP.Priority
			,vcRuleName ASC;
	ELSEIF v_byteMode = 1 AND v_bitSort = false then
		OPEN SWV_RefCur FOR
		SELECT 
			numPricRuleID,
            vcRuleName,
            CASE 
				WHEN tintPricingMethod = 1 
				THEN '<a href="#" onclick="return PriceTable(' 
						|| CAST(numPricRuleID AS VARCHAR(30)) 
						|| ')">' 
						|| Case PB.tintRuleFor 
								when 2 then 'Costing Table' 
								else 'Pricing Table' 
							end
				WHEN tintPricingMethod = 2 
				THEN CASE tintRuleType 
						WHEN 1 THEN 'Deduct from List price ' 
						WHEN 2 THEN 'Add to primary vendor cost ' 
						ELSE '' 
					END
					|| CASE WHEN tintVendorCostType = 1 THEN ' using dynamic cost ' WHEN tintVendorCostType = 2 THEN ' using static cost ' ELSE '' END
					|| CASE WHEN tintDiscountType = 1 THEN CAST(decDiscount AS VARCHAR(20)) || '%' ELSE 'flat ' || CAST(CAST(decDiscount AS INTEGER) AS VARCHAR(20)) END 
					|| ' for every '
					|| CAST(CASE WHEN intQntyItems = 0 THEN 1 ELSE intQntyItems END AS VARCHAR(20))
					|| ' units, until maximum of '
					|| CAST(decMaxDedPerAmt AS VARCHAR(20))
					|| CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END
				ELSE ''
			END AS PricingRule,
            coalesce(PP.Priority,0) AS Priority,
            CASE WHEN tintStep2 = 3 THEN 'All Items' ELSE GetPriceBookItemList(numPricRuleID,tintStep2) END AS ItemList,
            CASE WHEN tintStep3 = 3 THEN 'All Customers' ELSE GetPriceBookItemList(numPricRuleID,CASE tintStep3 WHEN 1 THEN 3 WHEN 2 THEN 4 END::SMALLINT) END AS CustomerList,
			CASE WHEN PB.tintRuleFor = 2 THEN 'Purchase' ELSE 'Sale' END AS vcRuleFor,
			PB.bitRoundTo,
			PB.tintRoundTo
		FROM 
			PriceBookRules PB
		LEFT OUTER JOIN 
			PriceBookPriorities PP 
		ON 
			PP.Step2Value = PB.tintStep2
			AND PP.Step3Value = PB.tintStep3
		WHERE 
			numDomainID = v_numDomainID 
			AND 1 =(CASE 
						WHEN v_numItemCode > 0 
						THEN CASE 
								WHEN PB.numPricRuleID IN (SELECT * FROM fn_GetItemPriceRule(v_numDomainID,2::SMALLINT,v_numItemCode,v_numDivisionID)) THEN 1 ELSE 0 END 
						ELSE 1 
					END)
		ORDER BY 
			PP.Priority,vcRuleName DESC;
	end if;
	RETURN;
END; $$;


