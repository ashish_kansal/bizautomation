-- Stored procedure definition script usp_GetDuplicateCompany_FirstSave for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDuplicateCompany_FirstSave(v_vcCompanyName VARCHAR(100)
--@numDivisionID numeric
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

--@numCompanyID numeric,
   AS $$
BEGIN
   open SWV_RefCur for select cast(CI.numCompanyId as VARCHAR(255)), cast(CI.vcCompanyName as VARCHAR(255)), DM.vcDivisionName
   from CompanyInfo CI INNER JOIN DivisionMaster DM ON CI.numCompanyId = DM.numCompanyID
   where
   CI.vcCompanyName = v_vcCompanyName
--And DM.numDivisionID <> @numDivisionID and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)
   Group by CI.numCompanyId,CI.vcCompanyName,DM.vcDivisionName;
END; $$;












