DROP FUNCTION IF EXISTS USP_ElasticSearch_GetKnowledgeBase;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetKnowledgeBase(v_numDomainID NUMERIC(18,0)
	,v_vcSolnID TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numSolnID AS id
		,'knowledgebase' AS module
		,CONCAT('/cases/frmSolution.aspx?SolId=',numSolnID) AS url
		,CONCAT('<b style="color:#7f6000">','KB Article:</b> ',coalesce(vcSolnTitle,'')) AS displaytext
		,vcSolnTitle AS text
		,coalesce(vcSolnTitle,'') AS "Search_vcSolnTitle"
   FROM
   SolutionMaster
   WHERE
   SolutionMaster.numDomainID = v_numDomainID
   AND (NULLIF(v_vcSolnID,'') IS NULL OR numSolnID IN (SELECT id FROM SplitIDs(coalesce(v_vcSolnID,''),',')));
END; $$;












