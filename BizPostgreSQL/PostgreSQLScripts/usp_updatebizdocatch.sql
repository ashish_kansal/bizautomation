-- Stored procedure definition script USP_UpdateBizDocAtch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateBizDocAtch(v_vcOrder VARCHAR(300) DEFAULT '',
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numBizDocID NUMERIC(9,0) DEFAULT 0,
	v_numBizDocTempID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE 
		BizDocAttachments 
	SET 
		numOrder = X.numOrder
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_vcOrder AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numBizAtchID NUMERIC(9,0) PATH 'numBizAtchID',
			numOrder NUMERIC(9,0) PATH 'numOrder'
	) AS X 
	WHERE 
		numAttachmntID = X.numBizAtchID;        

   RETURN;
END; $$;


