CREATE OR REPLACE FUNCTION USP_WorkOrder_GetWIPMilestones(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		vcMileStoneName AS "vcMileStoneName"
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',coalesce(tintPercentage,0),'% of total)') AS "vcMileStone"
	FROM
		WorkOrder
	INNER JOIN
		StagePercentageDetails
	ON
		WorkOrder.numBuildProcessId = StagePercentageDetails.slp_id
	WHERE
		WorkOrder.numDomainId = v_numDomainID
		AND WorkOrder.numWOId = v_numWOID
	GROUP BY
		vcMileStoneName,tintPercentage;
END; $$;

