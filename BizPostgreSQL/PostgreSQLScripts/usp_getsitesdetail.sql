-- Stored procedure definition script USP_GetSitesDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSitesDetail(v_numSiteID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   S.numDomainID,
        D.numDivisionId,
        coalesce(S.bitIsActive,true),
        coalesce(E.numDefaultWareHouseID,0) AS numDefaultWareHouseID,
        coalesce(E.numRelationshipId,0) AS numRelationshipId,
        coalesce(E.numProfileId,0) AS numProfileId,
        coalesce(E.bitHidePriceBeforeLogin,false) AS bitHidePriceBeforeLogin,
		fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),numAdminID) AS DomainAdminEmail,
		S.vcLiveURL,
		coalesce(E.bitAuthOnlyCreditCard,false) AS bitAuthOnlyCreditCard,
		coalesce(D.numDefCountry,0) AS numDefCountry,
		coalesce(E.bitSendEmail,false) AS bitSendMail,
		coalesce((SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = S.numDomainID),0) AS AuthSalesBizDoc,
		coalesce((SELECT  numBizDocId FROM eCommercePaymentConfig WHERE numSiteId = v_numSiteID AND numPaymentMethodId = 27261 AND bitEnable = 1 LIMIT 1),0) AS numCreditTermBizDocID,
		coalesce((SELECT  numBizDocId FROM eCommercePaymentConfig WHERE numSiteId = v_numSiteID AND numPaymentMethodId = 1 AND bitEnable = 1 LIMIT 1),0) AS numCreditCardBizDocID,
		coalesce((SELECT  numBizDocId FROM eCommercePaymentConfig WHERE numSiteId = v_numSiteID AND numPaymentMethodId = 31488 AND bitEnable = 1 LIMIT 1),0) AS numGoogleCheckoutBizDocID,
		coalesce((SELECT  numBizDocId FROM eCommercePaymentConfig WHERE numSiteId = v_numSiteID AND numPaymentMethodId = 35141 AND bitEnable = 1 LIMIT 1),0) AS numPaypalCheckoutBizDocID,
		coalesce((SELECT  numBizDocId FROM eCommercePaymentConfig WHERE numSiteId = v_numSiteID AND numPaymentMethodId = 84 AND bitEnable = 1 LIMIT 1),0) AS numSalesInquiryBizDocID,
		coalesce(D.bitSaveCreditCardInfo,false) AS bitSaveCreditCardInfo,
		coalesce(bitOnePageCheckout,false) AS bitOnePageCheckout,
		coalesce(D.tintBaseTaxOnArea,0) AS tintBaseTaxOnArea,
		coalesce(D.numShippingServiceItemID,0) AS numShippingServiceItemID,
		coalesce(D.bitAutolinkUnappliedPayment,false) AS bitAutolinkUnappliedPayment,
		coalesce(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		coalesce(D.tintBaseTax,0) AS tintBaseTax,
		coalesce(E.bitSkipStep2,false) AS bitSkipStep2,
		coalesce(E.bitDisplayCategory,false) AS bitDisplayCategory,
		coalesce(E.bitShowPriceUsingPriceLevel,false) AS bitShowPriceUsingPriceLevel,
		coalesce(E.bitPreSellUp,false) AS bitPreSellUp,
		coalesce(E.bitPostSellUp,false) AS bitPostSellUp,
		coalesce(E.dcPostSellDiscount,0) AS dcPostSellDiscount,
		coalesce(E.numDefaultClass,0) AS numDefaultClass,
		E.vcPreSellUp,E.vcPostSellUp,coalesce(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl,
		coalesce(D.vcSalesOrderTabs,'') AS vcSalesOrderTabs,
		coalesce(D.vcSalesQuotesTabs,'') AS vcSalesQuotesTabs,
		coalesce(D.vcItemPurchaseHistoryTabs,'') AS vcItemPurchaseHistoryTabs,
		coalesce(D.vcItemsFrequentlyPurchasedTabs,'') AS vcItemsFrequentlyPurchasedTabs,
		coalesce(D.vcOpenCasesTabs,'') AS vcOpenCasesTabs,
		coalesce(D.vcOpenRMATabs,'') AS vcOpenRMATabs,
		coalesce(D.bitSalesOrderTabs,false) AS bitSalesOrderTabs,
		coalesce(D.bitSalesQuotesTabs,false) AS bitSalesQuotesTabs,
		coalesce(D.bitItemPurchaseHistoryTabs,false) AS bitItemPurchaseHistoryTabs,
		coalesce(D.bitItemsFrequentlyPurchasedTabs,false) AS bitItemsFrequentlyPurchasedTabs,
		coalesce(D.bitOpenCasesTabs,false) AS bitOpenCasesTabs,
		coalesce(D.bitOpenRMATabs,false) AS bitOpenRMATabs,
		coalesce(D.bitSupportTabs,false) AS bitSupportTabs,
		coalesce(D.vcSupportTabs,'') AS vcSupportTabs,
		coalesce(bitHideAddtoCart,false) AS bitHideAddtoCart,
		coalesce(bitShowPromoDetailsLink,false) AS bitShowPromoDetailsLink,
		coalesce(D.bitDefaultProfileURL,false) AS bitDefaultProfileURL,
		coalesce(S.bitConfirmAddressRequired,false) AS bitConfirmAddressRequired
   FROM
   Sites S
   INNER JOIN
   Domain D
   ON
   D.numDomainId = S.numDomainID
   LEFT OUTER JOIN
   eCommerceDTL E
   ON
   D.numDomainId = E.numDomainID
   AND S.numSiteID = E.numSiteId
   WHERE
   S.numSiteID = v_numSiteID;
END; $$;
