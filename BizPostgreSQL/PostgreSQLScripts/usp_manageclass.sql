-- Stored procedure definition script USP_ManageClass for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageClass(v_numDomainID NUMERIC,
v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
      DELETE FROM ClassDetails WHERE numDomainID = v_numDomainID;

      INSERT INTO ClassDetails(numParentClassID,
			numChildClassID,
			numDomainID)
      SELECT X.numParentClassID,
                 X.numChildClassID,
                 v_numDomainID
      FROM
	   XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numParentClassID NUMERIC(9,0) PATH 'numParentClassID',
				numChildClassID NUMERIC(9,0) PATH 'numChildClassID'
		) AS X;
   end if;
   RETURN;
END; $$;



