-- Stored procedure definition script USP_ArranageHierChartActForBudgetEdit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Created By Siva                   
CREATE OR REPLACE FUNCTION USP_ArranageHierChartActForBudgetEdit(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,                                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,          
v_numBudgetDetId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountId  NUMERIC(9,0);
   v_vcCatgyName  VARCHAR(100);
   v_numAcntType  NUMERIC(9,0);
   v_numParentChartAcntId  NUMERIC(9,0);
BEGIN
   SELECT vcCatgyName INTO v_vcCatgyName FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId And v_numDomainID = numDomainId;                         
   SELECT coalesce(numAcntType,0) INTO v_numAcntType FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId And v_numDomainID = numDomainId;                    
   SELECT numParntAcntId INTO v_numParentChartAcntId FROM Chart_Of_Accounts WHERE numAccountId = v_numParntAcntId And v_numDomainID = numDomainId;                    
                 
       
   insert into HierChartOfActForBudget(numChartOfAcntID,numParentChartAcntId,numDomainID,vcCategoryName,numAcntType,numBudgetDetId)
	values(v_numParntAcntId,v_numParentChartAcntId,v_numDomainID,REPEAT(' ', (v_NESTLEVEL -3)*4) || coalesce(v_vcCatgyName, ''),v_numAcntType,v_numBudgetDetId);                                
            
      
	 
   SELECT MIN(numAcntId) INTO v_numAccountId FROM HierarchyArrangeDetails WHERE numParentAcntId = v_numParntAcntId;             
   RAISE NOTICE '@numAccountId==%',SUBSTR(CAST(v_numAccountId AS VARCHAR(10)),1,10);        
   Select numBudgetDetId INTO v_numBudgetDetId FROM HierarchyArrangeDetails WHERE numAcntId = v_numAccountId;          
   RAISE NOTICE '@numBudgetDetId==%',SUBSTR(CAST(v_numBudgetDetId AS VARCHAR(10)),1,10);                                 
                      
   WHILE v_numAccountId IS NOT NULL LOOP
      PERFORM USP_ArranageHierChartActForBudgetEdit(v_numAccountId,v_numDomainID,v_numBudgetDetId);
      SELECT MIN(numAcntId) INTO v_numAccountId FROM HierarchyArrangeDetails WHERE numParentAcntId = v_numParntAcntId AND numAcntId > v_numAccountId;
      Select numBudgetDetId INTO v_numBudgetDetId FROM HierarchyArrangeDetails WHERE numAcntId = v_numAccountId;
   END LOOP;
   RETURN;                             
 --Delete From HierarchyArrangeDetails                       
      
END; $$;


