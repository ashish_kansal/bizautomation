-- Stored procedure definition script USP_ExtranetAccountsDtl_ValidateResetLink for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ExtranetAccountsDtl_ValidateResetLink(v_numDomainID NUMERIC(18,0),
v_vcResetID VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitValidResetID  BOOLEAN DEFAULT 0;
BEGIN
   IF EXISTS(SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID = v_vcResetID AND(EXTRACT(DAY FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)) <= 10) then
	
      v_bitValidResetID := true;
   end if;

   open SWV_RefCur for SELECT v_bitValidResetID;
END; $$;












