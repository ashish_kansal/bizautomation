-- Stored procedure definition script USP_CalendarEmployees for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CalendarEmployees(v_numUserCntId NUMERIC(9,0) DEFAULT 0,            
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_GroupId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
            
--select *,resourceName as [Name] from resource      
   AS $$
   DECLARE
   v_UserID  NUMERIC(9,0);
BEGIN
   select   numUserId INTO v_UserID from UserMaster where numUserDetailId = v_numUserCntId;            

--1000 is added so that Myself is always first even if resourceId is greater than its previous records
   open SWV_RefCur for SELECT * FROM(select 1000 AS intOrder,cast(resourceId as NUMERIC(18,0)),vcFirstName || ' ' || vcLastname as Name from AdditionalContactsInformation
      join UserMaster on numContactId = numUserDetailID
      join Resource on numusercntid = numUserDetailID
      where   
--numTeam in (select numTeam from ForReportsByTeam where tintType=@intType and numUserCntID=@numUserCntId)  and                        
      numUserId <> v_UserID
      and UserMaster.numDomainID = v_numDomainID    and numGroupID = v_GroupId
      union
      select 1 AS intOrder,cast(resourceId as NUMERIC(18,0)),CAST('Myself' AS VARCHAR(50)) as Name from AdditionalContactsInformation
      join UserMaster on numUserDetailID = numUserDetailID
      join Resource on numusercntid = numUserDetailID
      where numUserDetailID = v_numUserCntId and UserMaster.numDomainID = v_numDomainID and numGroupID = v_GroupId) X ORDER BY intOrder;
END; $$;













