-- Stored procedure definition script usp_DeleteCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteCases(v_numCaseID NUMERIC(9,0) DEFAULT 0,
	v_numCreatedBy NUMERIC(9,0) DEFAULT 0   
--
 )
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE from Cases where numCaseId = v_numCaseID and numCreatedby = v_numCreatedBy;
   RETURN;
END; $$;


