-- Stored procedure definition script USP_ManagePortalDashboardReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePortalDashboardReports(v_numDomainID NUMERIC,
    v_strItems TEXT,
    v_numRelationshipID NUMERIC(18,0), 
    v_numProfileID NUMERIC(18,0),
    v_numContactID NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) = 'Reset' then
    
      DELETE FROM PortalDashboardDtl WHERE numDomainID = v_numDomainID AND numRelationshipID = v_numRelationshipID AND numProfileID = v_numProfileID AND numContactID = v_numContactID;
      RETURN;
   end if;
    
    
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
      DELETE FROM PortalDashboardDtl WHERE numDomainID = v_numDomainID AND numRelationshipID = v_numRelationshipID AND numProfileID = v_numProfileID AND numContactID = v_numContactID;
     
      INSERT  INTO PortalDashboardDtl(numDomainID,
                                                  numRelationshipID,
                                                  numProfileID,
                                                  numReportID,
                                                  tintColumnNumber,
                                                  tintRowOrder,
                                                  numContactID)
      SELECT  v_numDomainID,
                            X.numRelationshipID,
                            X.numProfileID,
                            X.numReportID,
                            X.tintColumnNumber,
                            X.tintRowOrder,
                            v_numContactID
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numRelationshipID NUMERIC(18,0) PATH 'numRelationshipID',
				numProfileID NUMERIC(18,0) PATH 'numProfileID',
				numReportID NUMERIC(18,0) PATH 'numReportID',
				tintColumnNumber SMALLINT PATH 'tintColumnNumber',
				tintRowOrder SMALLINT PATH 'tintRowOrder'
		) AS X;
	  
      IF v_numContactID = 0 then
            
				-- Remove User's customized portlet if admin disallow it to show
         DELETE FROM PortalDashboardDtl WHERE
         numDomainID = v_numDomainID
         AND numRelationshipID = v_numRelationshipID
         AND numProfileID = v_numProfileID AND
         numReportID IN(SELECT  numReportID
         FROM    PortalDashboardReports
         WHERE numReportID NOT IN(SELECT numReportID FROM PortalDashboardDtl WHERE  numDomainID = v_numDomainID
            AND numRelationshipID = v_numRelationshipID
            AND numProfileID = v_numProfileID AND numContactID = v_numContactID));
      end if;
   end if;
END; $$;


