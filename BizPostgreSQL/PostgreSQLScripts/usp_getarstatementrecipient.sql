-- Stored procedure definition script USP_GetARStatementRecipient for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetARStatementRecipient(v_numDomainID NUMERIC(18,0)
	,v_vcDivIDs TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numARContactPosition  NUMERIC(18,0);
BEGIN
   select   coalesce(numARContactPosition,0) INTO v_numARContactPosition FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   open SWV_RefCur for SELECT
   DM.numDivisionID
		,cast(coalesce(CI.vcCompanyName,'') as VARCHAR(255)) AS vcCompanyName
		,numContactId
		,cast(CONCAT(coalesce(ACI.vcFirstName,'-'),' ',coalesce(ACI.vcFirstName,'-')) as VARCHAR(255)) AS vcContactName
		,cast(coalesce(ACI.vcEmail,'') as VARCHAR(50)) AS vcContactEmail
   FROM
   DivisionMaster DM
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   AdditionalContactsInformation ACI
   ON
   DM.numDivisionID = ACI.numDivisionId
   WHERE
   DM.numDomainID = v_numDomainID
   AND DM.numDivisionID IN(SELECT cast(ID as NUMERIC(18,0)) FROM SplitIDs(v_vcDivIDs,','))
   AND 1 =(CASE
   WHEN coalesce(v_numARContactPosition,0) > 0 THEN(CASE WHEN coalesce(ACI.vcPosition,0) = v_numARContactPosition THEN 1 ELSE 0 END)
   ELSE(CASE WHEN coalesce(ACI.bitPrimaryContact,false) = true THEN 1 ELSE 0 END)
   END);
END; $$;












