-- Stored procedure definition script USP_RepManageFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RepManageFields(v_strFields VARCHAR(8000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;      
   v_strPosition  VARCHAR(1000);
BEGIN
   update ReportFields set tintSelected = 0;  
	update ReportFields set tintSelected = 1 where numRepFieldID IN (SELECT Id FROM SplitIDs(v_strFields,','));
   RETURN;
END; $$;


