CREATE OR REPLACE FUNCTION USP_EformDetails(v_numSpecDocID NUMERIC(9,0) DEFAULT 0,          
v_strSql VARCHAR(5000) DEFAULT NULL,          
v_byteMode SMALLINT DEFAULT NULL,        
v_vcColumnName VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_str  VARCHAR(5000);          
   v_ContactID  VARCHAR(20);    
   v_DivisionID  VARCHAR(20);    
   v_CompanyID  VARCHAR(20);       
   v_ListID  VARCHAR(20);
   v_check  VARCHAR(100);       
      
   v_checkCon  VARCHAR(10);   
  
  
--- Updating Division Master and Company Info  
   v_checkDivi  VARCHAR(100);       
      

   v_checkDiv  VARCHAR(10);
BEGIN
   if v_byteMode = 0 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_str := 'select ' || coalesce(v_strSql,'') || ' from AdditionalContactsInformation where numContactId =' || coalesce(v_ContactID,'');
      OPEN SWV_RefCur FOR EXECUTE v_str;
   end if;         
        
   if v_byteMode = 1 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_str := 'update AdditionalContactsInformation set ' || coalesce(v_strSql,'') || ' where numContactId=' || coalesce(v_ContactID,'');
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;          
        
   if v_byteMode = 2 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         v_str := 'update AdditionalContactsInformation set ' || coalesce(v_vcColumnName,'') || ' = ' || coalesce(v_ListID,'') || ' where numContactId =' || coalesce(v_ContactID,'');
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;         
        
   if v_byteMode = 3 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_str := '        
declare @CoulmnValue as varchar(20)        
declare @numListID as varchar(20)        
select @CoulmnValue= isnull( ' || coalesce(v_vcColumnName,'') || '/** SDynExp 1 **/         
 :CoulmnValue VARCHAR(20)        
 :numListID VARCHAR(20)        
select    coalesce(?1?,0)  INTO :CoulmnValue from AdditionalContactsInformation where numContactId = ' || coalesce(v_ContactID,'') || '        
        
if cast(NULLIF(:CoulmnValue,'''') as INTEGER) = 0 then        
        
     select   coalesce(numListItemID,0) INTO :numListID from ListDetails where vcData = ''' || coalesce(v_strSql,'') || '''        
     if cast(NULLIF(:numListID,'''') as INTEGER) > 0 then        
         
            update AdditionalContactsInformation set ' || coalesce(v_vcColumnName,'') || ' = :numListID where numContactId =' || coalesce(v_ContactID,'') || '        
 end        
         
end';
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;        
--updating Contacts Customfields      
   if v_byteMode = 4 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_ListID := v_vcColumnName;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         update CFW_FLD_Values_Cont set Fld_Value = v_strSql where RecId = v_ContactID and fld_id = v_ListID;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;        
      
   if v_byteMode = 5 then

      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         update CFW_FLD_Values_Cont set Fld_Value = v_ListID where RecId = v_ContactID and fld_id = v_vcColumnName;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;        
      
--updating Contacts Customfields      
   if v_byteMode = 6 then
      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_ListID := v_vcColumnName;
      select   coalesce(Fld_Value,'') INTO v_check from CFW_FLD_Values_Cont where RecId = v_ContactID and fld_id = v_ListID;
      if v_check = '' then

         update CFW_FLD_Values_Cont set Fld_Value = v_strSql where RecId = v_ContactID and fld_id = v_ListID;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;       
      
   if v_byteMode = 7 then
      select   numRecID INTO v_ContactID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      select   coalesce(Fld_Value,0) INTO v_checkCon from CFW_FLD_Values_Cont where RecId = v_ContactID and fld_id = v_vcColumnName;
      if cast(NULLIF(v_checkCon,'') as INTEGER) = 0 then

         update CFW_FLD_Values_Cont set Fld_Value = v_ListID where RecId = v_ContactID and fld_id = v_vcColumnName;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;   
  
  
--- Updating Division Master and Company Info  
   if v_byteMode = 8 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_str := 'select ' || coalesce(v_strSql,'') || ' from DivisionMaster div  
join CompanyInfo com  
on div.numCompanyID = com.numCompanyId  
where numdivisionid =' || coalesce(v_DivisionID,'');
      OPEN SWV_RefCur FOR EXECUTE v_str;
   end if;    
  
   if v_byteMode = 9 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   numCompanyID INTO v_CompanyID from DivisionMaster where numDivisionID = cast(NULLIF(v_DivisionID,'') as NUMERIC(18,0));
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         v_str := 'update CompanyInfo set ' || coalesce(v_vcColumnName,'') || ' = ' || coalesce(v_ListID,'') || ' where numCompanyId =' || coalesce(v_CompanyID,'');
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;    
  
   if v_byteMode = 10 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         v_str := 'update DivisionMaster set ' || coalesce(v_vcColumnName,'') || ' = ' || coalesce(v_ListID,'') || ' where numdivisionid =' || coalesce(v_DivisionID,'');
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;    
  
-- updating Division Master Custom Fields  
   if v_byteMode = 11 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_ListID := v_vcColumnName;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         update CFW_FLD_Values set Fld_Value = v_strSql where RecId = v_DivisionID and Fld_ID = v_ListID;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;   
  
   if v_byteMode = 12 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      if cast(NULLIF(v_ListID,'') as INTEGER) > 0 then

         update CFW_FLD_Values set Fld_Value = v_ListID where RecId = v_DivisionID and Fld_ID = v_vcColumnName;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;    
  
   if v_byteMode = 13 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   numCompanyID INTO v_CompanyID from DivisionMaster where numDivisionID = cast(NULLIF(v_DivisionID,'') as NUMERIC(18,0));
      v_str := 'update CompanyInfo set ' || coalesce(v_strSql,'') || ' where numCompanyId=' || coalesce(v_CompanyID,'');
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;    
  
  
   if v_byteMode = 14 then

      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_str := 'update DivisionMaster set ' || coalesce(v_strSql,'') || ' where numDivisionId=' || coalesce(v_DivisionID,'');
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;    
  
--updating Contacts Customfields      
   if v_byteMode = 15 then
      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      v_ListID := v_vcColumnName;
      select   coalesce(Fld_Value,'') INTO v_checkDivi from CFW_FLD_Values where RecId = v_DivisionID and Fld_ID = v_ListID;
      if v_checkDivi = '' then

         update CFW_FLD_Values set Fld_Value = v_strSql where RecId = v_DivisionID and Fld_ID = v_ListID;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;       
      

   if v_byteMode = 16 then
      select   numRecID INTO v_DivisionID from SpecificDocuments where numSpecificDocID = v_numSpecDocID;
      select   coalesce(numListItemID,0) INTO v_ListID from Listdetails where vcData = v_strSql;
      select   coalesce(Fld_Value,0) INTO v_checkDiv from CFW_FLD_Values where RecId = v_DivisionID and Fld_ID = v_vcColumnName;
      if cast(NULLIF(v_checkCon,'') as INTEGER) = 0 then

         update CFW_FLD_Values set Fld_Value = v_ListID where RecId = v_DivisionID and Fld_ID = v_vcColumnName;
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_str;
      open SWV_RefCur2 for
      select 1;
   end if;
   RETURN;
END; $$;


