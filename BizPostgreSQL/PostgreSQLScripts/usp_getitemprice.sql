-- Stored procedure definition script USP_GetItemPrice for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemPrice(v_numItemCode NUMERIC(9,0),                  
v_units NUMERIC(9,0),             
v_numDivisionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(monListPrice as VARCHAR(255)) from Item where numItemCode = v_numItemCode;
END; $$;












