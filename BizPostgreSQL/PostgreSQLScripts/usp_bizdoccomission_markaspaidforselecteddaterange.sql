-- Stored procedure definition script USP_BizDocComission_MarkAsPaidForSelectedDateRange for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDocComission_MarkAsPaidForSelectedDateRange(v_numDomainId NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate TIMESTAMP DEFAULT NULL,
	v_dtToDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   v_dtFromDate := v_dtFromDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);
   v_dtToDate :=  v_dtToDate+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval);

   UPDATE
   BizDocComission BDC
   SET
   bitCommisionPaid = true
   FROM
   OpportunityBizDocs OBD INNER JOIN OpportunityMaster Opp ON Opp.numOppId = OBD.numoppid
   WHERE
   BDC.numOppBizDocId = OBD.numOppBizDocsId AND(Opp.numDomainId = v_numDomainId
   AND BDC.numDomainId = v_numDomainId
   AND OBD.dtCreatedDate  BETWEEN v_dtFromDate AND v_dtToDate
   AND coalesce(BDC.bitCommisionPaid,false) = false
   AND OBD.bitAuthoritativeBizDocs = 1);
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_cfwSaveCusfld]    Script Date: 07/26/2008 16:15:10 ******/



