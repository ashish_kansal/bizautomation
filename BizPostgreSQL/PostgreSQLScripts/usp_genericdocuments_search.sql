-- Stored procedure definition script USP_GenericDocuments_Search for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GenericDocuments_Search(v_numDomainID NUMERIC(18,0)
	,v_numDocCategory NUMERIC(18,0)
	,v_tintDocType SMALLINT
	,v_vcSearchText VARCHAR(100)
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS "numTotalRecords"
		,numGenericDocID AS id
		,vcDocName AS text
		,coalesce(EMM.vcModuleName,'-') AS "vcModuleName"
   FROM
   GenericDocuments
   LEFT JOIN
   EmailMergeModule EMM
   ON
   EMM.numModuleID = GenericDocuments.numModuleId
   AND EMM.tintModuleType = 0
   WHERE
   numDomainID = v_numDomainID
   AND tintDocumentType = 1
   AND (coalesce(v_numDocCategory,0) = 0 OR numDocCategory = v_numDocCategory)
   AND 1 =(CASE v_tintDocType
   WHEN 2 THEN(CASE WHEN coalesce(VcFileName,'') ilike '#SYS#%' THEN 1 ELSE 0 END)
   WHEN 1 THEN(CASE WHEN coalesce(VcFileName,'') NOT ilike '#SYS#%' THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND vcDocName ilike CONCAT('%',v_vcSearchText,'%')
   ORDER BY
   vcDocName;
END; $$;












