-- Stored procedure definition script USP_GetPromotionOfferForIncentiveSaleItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPromotionOfferForIncentiveSaleItems(v_numDomainID NUMERIC(18,0),
	v_monTotAmtBefDiscount DOUBLE PRECISION,
	v_itemIdSearch VARCHAR(100), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_max  DOUBLE PRECISION;
   v_Id  INTEGER;
   v_ProID  NUMERIC(18,0);
BEGIN
   IF(SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId = v_numDomainID AND IsOrderBasedPromotion = true) = 0 then
      open SWV_RefCur for
      SELECT 0 AS SubTotal;
      RETURN;
   end if; 

   BEGIN
      CREATE TEMP SEQUENCE tt_tempPromotionOrderBased_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN NULL;
   END;

   DROP TABLE IF EXISTS tt_TEMPPROMOTIONORDERBASED CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPROMOTIONORDERBASED
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numProOrderBasedID NUMERIC(18,0),
      numProID NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      numProItemID NUMERIC(18,0),
      fltSubTotal  DOUBLE PRECISION,
      cSaleType CHAR,
      cItems CHAR,
      bitEnabled BOOLEAN
   );

   INSERT INTO tt_TEMPPROMOTIONORDERBASED
   (
		numProOrderBasedID,
      numProID,
      numDomainID,
      numProItemID,
      fltSubTotal,
      cSaleType,
      cItems,
      bitEnabled
   )
	SELECT numProOrderBasedID,
      numProID,
      numDomainID,
      numProItemID,
      fltSubTotal,
      cSaleType,
      cItems,
      bitEnabled FROM PromotionOfferOrderBased WHERE cSaleType = 'I' AND bitEnabled = true AND numDomainId = v_numDomainID; 

	--select * from #temp
	--drop table #temp

   IF(SELECT COUNT(*) from tt_TEMPPROMOTIONORDERBASED) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
         fltSubTotal  DOUBLE PRECISION,
         numProID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(fltSubTotal, numProID)
      SELECT fltSubTotal, numProID FROM tt_TEMPPROMOTIONORDERBASED;
      WHILE(SELECT COUNT(*) FROM tt_TEMP) > 0 LOOP
				--SELECT @Id = Id, @max = MAX(fltSubTotal) FROM #temp GROUP BY Id

         select   MAX(fltSubTotal) INTO v_max FROM tt_TEMP;
         select   ID, numProID INTO v_Id,v_ProID FROM tt_TEMP WHERE fltSubTotal = v_max;
         IF(v_monTotAmtBefDiscount >= v_max) then
				
            open SWV_RefCur for
            SELECT v_max AS SubTotal, v_ProID AS PromotionID;
            RETURN;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
         end if;
         DELETE FROM tt_TEMP WHERE ID = v_Id;
      END LOOP;
      open SWV_RefCur2 for
      SELECT -1 AS SubTotal;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      DROP TABLE IF EXISTS tt_TEMPPROMOTIONORDERBASED CASCADE;
   ELSE
      open SWV_RefCur for
      SELECT 0 AS SubTotal;
      RETURN;
   end if;
END; $$;




