CREATE OR REPLACE FUNCTION fn_SecondsConversion(v_TimeinSec INTEGER)
RETURNS TEXT LANGUAGE plpgsql
AS $$
	DECLARE
	v_op TEXT;
BEGIN
	v_op := CONCAT(RIGHT('0' || CAST(v_TimeinSec / 3600 AS VARCHAR),2),':',RIGHT('0' || CAST((v_TimeinSec / 60) % 60 AS VARCHAR),2));
	RETURN COALESCE(v_op,'');
END; $$;

