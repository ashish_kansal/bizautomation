-- Stored procedure definition script USP_GetWarehouseItemsBySKU for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWarehouseItemsBySKU(v_numItemCode NUMERIC(18,0),
	v_numWarehouseID NUMERIC(18,0) DEFAULT 0,
	v_vcSKU VARCHAR(50) DEFAULT NULL,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numWareHouseItemID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM WareHouseItems WI
   WHERE numWareHouseID = v_numWarehouseID
   AND numItemID = v_numItemCode
   AND numDomainID = v_numDomainID
   AND coalesce(vcWHSKU,'') = v_vcSKU
   AND (numWareHouseItemID = v_numWareHouseItemID OR v_numWareHouseItemID = 0);
END; $$;












