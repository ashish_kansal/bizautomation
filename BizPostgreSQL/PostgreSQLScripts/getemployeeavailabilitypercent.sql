-- Function definition script GetEmployeeAvailabilityPercent for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetEmployeeAvailabilityPercent(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER)
RETURNS TABLE
(	
   numTotalAvailableMinutes NUMERIC(18,0),
   numAvailabilityPercent NUMERIC(18,0),
   numProductiveMinutes NUMERIC(18,0),
   numProductivityPercent NUMERIC(18,0)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWorkScheduleID  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_numTotalProductiveMinutes  NUMERIC(18,0) DEFAULT 0;
   v_numTotalAbsentMinutes  NUMERIC(18,0) DEFAULT 0;
   v_numActualProductiveMinutes  NUMERIC(18,0) DEFAULT coalesce(GetEmployeeProductiveMinutes(v_numDomainID,v_numUserCntID,v_dtFromDate,v_dtToDate,v_ClientTimeZoneOffset),0);
BEGIN
   DROP TABLE IF EXISTS tt_GETEMPLOYEEAVAILABILITYPERCENT_EMPLOYEEAVAILABILITY CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETEMPLOYEEAVAILABILITYPERCENT_EMPLOYEEAVAILABILITY
   (
      numTotalAvailableMinutes NUMERIC(18,0),
      numAvailabilityPercent NUMERIC(18,0),
      numProductiveMinutes NUMERIC(18,0),
      numProductivityPercent NUMERIC(18,0)
   );
   select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), CONCAT(',',vcWorkDays,',') INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_vcWorkDays FROM
   UserMaster
   INNER JOIN
   WorkSchedule WS
   ON
   WS.numUserCntID = UserMaster.numUserDetailId WHERE
   UserMaster.numDomainID = v_numDomainID
   AND UserMaster.numUserDetailId = v_numUserCntID;

   WHILE v_dtFromDate <= v_dtToDate LOOP
      IF EXISTS(SELECT numCategoryHDRID FROM timeandexpense WHERE timeandexpense.numDomainID = v_numDomainID AND timeandexpense.numUserCntID = v_numUserCntID AND coalesce(numCategory,0) = 1 AND numtype = 4) then
		
         v_numTotalAbsentMinutes := coalesce(v_numTotalAbsentMinutes,0)+v_numProductiveTimeInMinutes;
      ELSEIF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtFromDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND v_dtFromDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) BETWEEN dtDayOffFrom AND dtDayOffTo)
      then
		
         v_numTotalProductiveMinutes := coalesce(v_numTotalProductiveMinutes,0)+v_numProductiveTimeInMinutes;
      end if;
      v_dtFromDate := v_dtFromDate+INTERVAL '1 day';
   END LOOP;

   INSERT INTO tt_GETEMPLOYEEAVAILABILITYPERCENT_EMPLOYEEAVAILABILITY(numTotalAvailableMinutes
		,numAvailabilityPercent
		,numProductiveMinutes
		,numProductivityPercent)
	VALUES(v_numTotalProductiveMinutes -v_numTotalAbsentMinutes
		,(CASE WHEN coalesce(v_numTotalProductiveMinutes, 0) > 0 THEN(((v_numTotalProductiveMinutes -v_numTotalAbsentMinutes)*100)/v_numTotalProductiveMinutes) ELSE 0 END)
		,v_numActualProductiveMinutes
		,(CASE WHEN coalesce(v_numTotalProductiveMinutes, 0) > 0 THEN(v_numActualProductiveMinutes*100)/v_numTotalProductiveMinutes ELSE 0 END));

	RETURN QUERY (SELECT * FROM tt_GETEMPLOYEEAVAILABILITYPERCENT_EMPLOYEEAVAILABILITY);
END; $$;

