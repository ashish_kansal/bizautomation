-- Stored procedure definition script USP_SaveCashCreditCardRecurringDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCashCreditCardRecurringDetails(v_numCashCreditCardId NUMERIC(9,0) DEFAULT 0,    
v_dtRecurringDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Insert into CashCreditCardRecurringDetails(numCashCreditCardId,dtRecurringDate) Values(v_numCashCreditCardId,v_dtRecurringDate);
RETURN;
END; $$;


