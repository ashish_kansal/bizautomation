-- Stored procedure definition script USP_EmailHistory_DeleteArchived for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistory_DeleteArchived(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_dtFrom DATE,
	v_dtTo DATE)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numEmailArchiveNodeID  NUMERIC(18,0);
   v_strSQL  TEXT;
BEGIN
   -- BEGIN TRANSACTION
BEGIN
		--numFixID = 7 = Email Archive
      select   coalesce(numNodeID,0) INTO v_numEmailArchiveNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numFixID = 2 AND coalesce(bitSystem,false) = true;
      DELETE FROM Correspondence WHERE numEmailHistoryID IN(SELECT coalesce(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = v_numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= v_dtFrom AND CAST(dtReceivedOn AS DATE) <= v_dtTo);
      DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN(SELECT coalesce(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = v_numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= v_dtFrom AND CAST(dtReceivedOn AS DATE) <= v_dtTo);
      DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN(SELECT coalesce(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = v_numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= v_dtFrom AND CAST(dtReceivedOn AS DATE) <= v_dtTo);
      DELETE FROM EmailHistory WHERE numEmailHstrID IN(SELECT coalesce(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = v_numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= v_dtFrom AND CAST(dtReceivedOn AS DATE) <= v_dtTo);
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;

   -- COMMIT
RETURN;
END; $$;





