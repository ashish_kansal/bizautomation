-- Stored procedure definition script USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcFilterValue TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ExpenseLastMonth  DECIMAL(20,5);
   v_ExpenseSamePeriodLastYear  DECIMAL(20,5);
   v_ProfitLastMonth  DECIMAL(20,5);
   v_ProfitSamePeriodLastYear  DECIMAL(20,5);
   v_RevenueLastMonth  DECIMAL(20,5);
   v_RevenueSamePeriodLastYear  DECIMAL(20,5);
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_dtLastMonthFromDate  TIMESTAMP;                                       
   v_dtLastMonthToDate  TIMESTAMP;
   v_dtSameMonthLYFromDate  TIMESTAMP;                                       
   v_dtSameMonthLYToDate  TIMESTAMP;

   v_FinancialYearStartDate  TIMESTAMP;
   v_StartDate  TIMESTAMP;
BEGIN
   v_FinancialYearStartDate := GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP)::INTEGER,v_numDomainID);

	--SELECT @dtLastMonthFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), @dtLastMonthToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));
	--SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);

   v_dtSameMonthLYFromDate := v_dtLastMonthFromDate+INTERVAL '-1 year';
   v_dtSameMonthLYToDate := v_dtLastMonthToDate+INTERVAL '-1 year';
   v_StartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval);

   IF v_vcFilterValue = 'ThisWeekVSLastWeek' then
	
      v_dtLastMonthFromDate := v_StartDate+0*INTERVAL '1 week';
      v_dtLastMonthToDate := v_StartDate+INTERVAL '0 month'+-1*INTERVAL '1 week';
   end if;
   IF v_vcFilterValue = 'ThisMonthVSLastMonth' then
	
      v_dtLastMonthFromDate := v_StartDate+INTERVAL '0 month';
      v_dtLastMonthToDate := v_StartDate+INTERVAL '0 month'+INTERVAL '-1 month';
   end if;
   IF v_vcFilterValue = 'ThisQTDVSLastQTD' then
	
      v_dtLastMonthFromDate := v_StartDate+INTERVAL '0 ';
      v_dtLastMonthToDate := v_StartDate+INTERVAL '0 month'+INTERVAL '-1 ';
   end if;
   IF v_vcFilterValue = 'ThisYTDVSLastYTD' then
	
      v_dtLastMonthFromDate := v_StartDate+INTERVAL '0 year';
      v_dtLastMonthToDate := v_StartDate+INTERVAL '0 month'+INTERVAL '-1 year';
   end if;
	


   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID;


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
   select   SUM(numDebitAmt) -SUM(numCreditAmt) INTO v_ExpenseLastMonth FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId = General_Journal_Header.numJOurnal_Id WHERE General_Journal_Details.numDomainId = v_numDomainID AND datEntry_Date BETWEEN  v_dtLastMonthFromDate AND v_dtLastMonthToDate AND General_Journal_Details.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND bitActive = true AND numAcntTypeId IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode ilike '0104%'));
   select   SUM(numDebitAmt) -SUM(numCreditAmt) INTO v_ExpenseSamePeriodLastYear FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId = General_Journal_Header.numJOurnal_Id WHERE General_Journal_Details.numDomainId = v_numDomainID AND datEntry_Date BETWEEN  v_dtSameMonthLYFromDate AND v_dtSameMonthLYToDate AND General_Journal_Details.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND bitActive = true AND numAcntTypeId IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode ilike '0104%'));

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
   select   SUM(Profit) INTO v_ProfitLastMonth FROM(SELECT
      coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtLastMonthFromDate AND v_dtLastMonthToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;

   select   SUM(Profit) INTO v_ProfitSamePeriodLastYear FROM(SELECT
      coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtSameMonthLYFromDate AND v_dtSameMonthLYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
   select   SUM(monTotAmount) INTO v_RevenueLastMonth FROM(SELECT
      coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtLastMonthFromDate AND v_dtLastMonthToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1) TEMP;

   select   SUM(monTotAmount) INTO v_RevenueSamePeriodLastYear FROM(SELECT
      coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtSameMonthLYFromDate AND v_dtSameMonthLYToDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1) TEMP;

   open SWV_RefCur for SELECT
   100.0*(coalesce(v_RevenueLastMonth,0) -coalesce(v_RevenueSamePeriodLastYear,0))/CASE WHEN coalesce(v_RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE v_RevenueSamePeriodLastYear END As RevenueDifference
		,100.0*(coalesce(v_ProfitLastMonth,0) -coalesce(v_ProfitSamePeriodLastYear,0))/CASE WHEN coalesce(v_ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE v_ProfitSamePeriodLastYear END As ProfitDifference
		,100.0*(coalesce(v_ExpenseLastMonth,0) -coalesce(v_ExpenseSamePeriodLastYear,0))/CASE WHEN coalesce(v_ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE v_ExpenseSamePeriodLastYear END As ExpenseDifference;
END; $$;












