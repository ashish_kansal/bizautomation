-- Stored procedure definition script usp_GetSurveyForContact for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyForContact(v_numDomainID NUMERIC DEFAULT 0,
	v_numContactID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numSurID as VARCHAR(255))
		,cast(vcSurName as VARCHAR(255))
		,cast(bintCreatedOn as VARCHAR(255))
   FROM
   SurveyMaster SM
   INNER JOIN
   surveyrespondants SR
   ON
   SM.numSurID = SR.numSurveyID
   WHERE
   SM.numDomainID = v_numDomainID
   AND SR.vcContactFName || ' ' || SR.vcContactLName || ' ' || SR.vcEmail =(Select ACI.vcFirstName || ' ' || ACI.vcLastname || ' ' || ACI.vcEmail from AdditionalContactsInformation ACI where numContactId = v_numContactID);
END; $$;












