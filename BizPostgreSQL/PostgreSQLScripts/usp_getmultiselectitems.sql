-- Stored procedure definition script USP_GetMultiSelectItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMultiSelectItems(v_numDomainID NUMERIC(18,0),
	v_numItemCodes VARCHAR(8000) DEFAULT '',
	v_tintOppType SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		I.numItemCode
		,I.vcItemName AS ItemName
		,I.charItemType
		,fn_GetItemAttributes(v_numDomainID,I.numItemCode,false) AS Attr
		,(SELECT SUM(numOnHand)  FROM WareHouseItems WHERE numItemID = I.numItemCode) AS numOnHand
		,(SELECT SUM(numAllocation) FROM WareHouseItems WHERE numItemID = I.numItemCode) AS numAllocation
		,coalesce(I.numSaleUnit,0) AS numSaleUnit
		,fn_UOMConversion((CASE WHEN  v_tintOppType = 1 THEN coalesce(I.numSaleUnit,0) ELSE coalesce(I.numPurchaseUnit,0) END),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)) AS fltUOMConversionFactor
   FROM
   Item I
   WHERE
   I.numItemCode IN(SELECT Id FROM SplitIDs(v_numItemCodes,','));
END; $$;
