-- Stored procedure definition script USP_ManageAddAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAddAmount(v_byteMode SMALLINT,    
v_numAmtCategory NUMERIC(9,0) DEFAULT 0,    
v_monAmount DECIMAL(20,5) DEFAULT NULL,    
v_dtAmtAdded TIMESTAMP DEFAULT NULL,    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,    
v_numAddHDRID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      insert into TimeExpAddAmount(numAmtCategory,monAmount,dtAmtAdded,numUserCntID)
        values(v_numAmtCategory,v_monAmount,v_dtAmtAdded,v_numUserCntID);
        
      open SWV_RefCur for
      select 1;
   end if;    
   if v_byteMode = 1 then

      open SWV_RefCur for
      select * from TimeExpAddAmount
      join Listdetails on numListItemID = numAmtCategory
      where numUserCntID = v_numUserCntID;
   end if;    
   if v_byteMode = 2 then

      delete from TimeExpAddAmount where numAddHDRID = v_numAddHDRID;
      open SWV_RefCur for
      select 1;
   end if;
   RETURN;
END; $$;


