-- Stored procedure definition script USP_GET_RULE_AS_PER_ORDER for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_RULE_AS_PER_ORDER(v_numOrderID			NUMERIC(18,0),
	v_numBizDocItemID	NUMERIC(18,0),
	v_numShipClass		NUMERIC(18,0),
	v_numDomainID		NUMERIC(18,0),
	v_numQty				DOUBLE PRECISION,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

--SET @OrderIDs = '26171,26172,26173'
--SET @numDomainID = 72
--SET @intShipBasedOn = 1
--SET @tintType = 1

-- WANT TO FETCH DATA
   AS $$
   DECLARE
   v_numPriceValue  DECIMAL(20,5);
   v_numWeightValue  NUMERIC(18,2);
   v_numQtyValue  NUMERIC(18,0);
   v_numCountryID  NUMERIC(18,0);
   v_numStateID  NUMERIC(18,0);
   v_Zip  VARCHAR(50);
   v_numZip  NUMERIC(10,0);
   v_numItemID  NUMERIC(10,0);
   v_numSourceCompanyID  NUMERIC(10,0);
--DECLARE @numShipClass AS NUMERIC(10)

BEGIN
   v_numSourceCompanyID := 0;
   select   coalesce(numWebApiId,0) INTO v_numSourceCompanyID FROM OpportunityLinking WHERE numChildOppID = v_numOrderID;


         --,@numShipClass=I.numShipClass
   select   coalesce(OBDI.monPrice,coalesce(OI.monPrice,0)), coalesce(I.fltWeight,0), v_numQty, AD.vcPostalCode, CASE WHEN coalesce(OM.tintShipToType,0) IN(0,1) THEN AD.numCountry ELSE OppAD.numShipCountry END, CASE WHEN coalesce(OM.tintShipToType,0) IN(0,1) THEN AD.numState ELSE OppAD.numShipState END, OBDI.numItemCode INTO v_numPriceValue,v_numWeightValue,v_numQtyValue,v_Zip,v_numCountryID,v_numStateID,
   v_numItemID FROM OpportunityMaster OM
   INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
   LEFT JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId
   LEFT JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID  AND OI.numoppitemtCode = OBDI.numOppItemID
   INNER JOIN Item I ON I.numItemCode = OI.numItemCode AND I.numDomainID = OM.numDomainId
   LEFT JOIN AddressDetails AD ON AD.numDomainID = OM.numDomainId AND AD.numRecordID = OM.numDivisionId AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true
   LEFT JOIN OpportunityAddress OppAD ON OppAD.numOppID = OM.numOppId WHERE 1 = 1
   AND OM.numOppId = v_numOrderID
   AND (OBDI.numOppBizDocItemID = v_numBizDocItemID OR coalesce(v_numBizDocItemID,0) = 0)
   AND I.numDomainID = v_numDomainID;

   RAISE NOTICE '%',v_numWeightValue;             
   RAISE NOTICE 'PRICE : %',SUBSTR(CAST(v_numPriceValue AS VARCHAR(100)),1,100);
   RAISE NOTICE 'Weight : %',SUBSTR(CAST(v_numWeightValue AS VARCHAR(100)),1,100);
   RAISE NOTICE 'Quantity : %',SUBSTR(CAST(v_numQtyValue AS VARCHAR(100)),1,100);
   RAISE NOTICE 'numCountryID : %',SUBSTR(CAST(v_numCountryID AS VARCHAR(100)),1,100);
   RAISE NOTICE 'numStateID : %',SUBSTR(CAST(v_numStateID AS VARCHAR(100)),1,100);
   RAISE NOTICE 'numItemCode : %',SUBSTR(CAST(v_numItemID AS VARCHAR(100)),1,100);
   RAISE NOTICE '@numSourceCompanyID : %',SUBSTR(CAST(v_numSourceCompanyID AS VARCHAR(100)),1,100);

   RAISE NOTICE '%',v_Zip;
   IF SWF_IsNumeric(v_Zip) = true then

      v_numZip := v_Zip;
   ELSE
      v_numZip := 0;
   end if;
   RAISE NOTICE '%',v_numZip;

      
   open SWV_RefCur for SELECT DISTINCT v_numZip,(CASE WHEN SWF_IsNumeric(SRSL.vcFromZip) = true THEN CAST(SRSL.vcFromZip AS NUMERIC(18,0)) ELSE 0 END),(CASE WHEN SWF_IsNumeric(SRSL.vcToZip) = true THEN CAST(SRSL.vcToZip AS NUMERIC(18,0)) ELSE 0 END),SR.*,SRC.*,
				cast(coalesce(SST1.vcServiceName,'') as VARCHAR(255)) AS Domestic,
				cast(coalesce(SST2.vcServiceName,'') as VARCHAR(255)) AS International,
				SST1.numShippingCompanyID AS numShippingCompanyID1,
				SST2.numShippingCompanyID AS numShippingCompanyID2
   FROM ShippingLabelRuleMaster SR
   INNER JOIN ShippingLabelRuleChild SRC ON SR.numShippingRuleID = SRC.numShippingRuleID
   LEFT JOIN ShippingRuleStateList SRSL ON SR.numShippingRuleID = SRSL.numRuleID AND SR.numDomainID = SRSL.numDomainID
   LEFT JOIN PromotionOfferItems POI ON SR.numShippingRuleID = POI.numProId
   INNER JOIN ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SRC.numDomesticShipID
   INNER JOIN ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SRC.numInternationalShipID
   WHERE 1 = 1
   AND SR.numDomainID = v_numDomainID
   AND
(
	-- Check MarketPlace is Available or not for shipping
	(
		(SR.numSourceCompanyID = v_numSourceCompanyID OR coalesce(SR.numSourceCompanyID,0) = 0 OR coalesce(v_numSourceCompanyID,0) = 0))
   AND	
	--Items Affected
	(
		(intItemAffected = 1 AND POI.numValue = v_numItemID) -- Priority 1
   OR
		(intItemAffected = 2 AND POI.numValue = v_numShipClass) -- Priority 2
   OR
   intItemAffected = 3 -- Priority 3
	)
	
	--Shipping Based On
   AND
    
   1 =(CASE WHEN SR.tintShipBasedOn = 1 THEN CASE WHEN v_numWeightValue BETWEEN SRC.numFromValue:: NUMERIC(18,2) AND SRC.numToValue:: NUMERIC(18,2) THEN 1 ELSE 0 END
   WHEN SR.tintShipBasedOn = 2 THEN CASE WHEN v_numPriceValue BETWEEN SRC.numFromValue:: DECIMAL(20,5) AND SRC.numToValue:: DECIMAL(20,5) THEN 1 ELSE 0 END
   WHEN SR.tintShipBasedOn = 3 THEN CASE WHEN v_numQtyValue BETWEEN SRC.numFromValue:: NUMERIC(18,0) AND SRC.numToValue:: NUMERIC(18,0) THEN 1 ELSE 0 END
   ELSE 0
   END)
    
   AND
    
   1 =(CASE WHEN SR.tintType = 1 AND SRSL.numRuleStateID IS NOT NULL THEN
      CASE WHEN SR.numShippingRuleID  IN(SELECT  cast(numRuleID as NUMERIC(10,0)) FROM ShippingRuleStateList SRSL WHERE numDomainID = v_numDomainID AND SRSL.numRuleID = SR.numShippingRuleID
         AND (SRSL.numCountryID = v_numCountryID OR v_numCountryID = 0)
         AND (SRSL.numStateID = v_numStateID OR v_numStateID = 0)
         AND
											(
											(v_numZip >=(CASE WHEN SWF_IsNumeric(SRSL.vcFromZip) = true THEN CAST(SRSL.vcFromZip AS NUMERIC(18,0)) ELSE 0 END)
         AND
         v_numZip <=(CASE WHEN SWF_IsNumeric(SRSL.vcToZip) = true THEN CAST(SRSL.vcToZip AS NUMERIC(18,0)) ELSE 0 END))
         OR
         coalesce(v_numZip,0) = 0)
         ORDER BY(CASE WHEN LENGTH(coalesce(SRSL.vcToZip,'')) >= 5 THEN 1 WHEN coalesce(SRSL.numStateID,0) > 0 THEN 2 WHEN coalesce(SRSL.numCountryID,0) > 0 THEN 3 END) LIMIT 1) THEN 1 ELSE 0 END
   WHEN SR.tintType = 2 AND SRSL.numRuleStateID IS NOT NULL  THEN
      CASE WHEN SR.numShippingRuleID not IN(SELECT  cast(numRuleID as NUMERIC(10,0)) FROM ShippingRuleStateList SRSL WHERE numDomainID = v_numDomainID AND SRSL.numRuleID = SR.numShippingRuleID
         AND (SRSL.numCountryID = v_numCountryID OR v_numCountryID = 0)
         AND (SRSL.numStateID = v_numStateID OR v_numStateID = 0)
         AND
										(
											(v_numZip >=(CASE WHEN SWF_IsNumeric(SRSL.vcFromZip) = true THEN CAST(SRSL.vcFromZip AS NUMERIC(18,0)) ELSE 0 END)
         AND
         v_numZip <=(CASE WHEN SWF_IsNumeric(SRSL.vcToZip) = true THEN CAST(SRSL.vcToZip AS NUMERIC(18,0)) ELSE 0 END))
         OR
         coalesce(v_numZip,0) = 0)
         ORDER BY(CASE WHEN LENGTH(coalesce(SRSL.vcToZip,'')) >= 5 THEN 1
         WHEN coalesce(SRSL.numStateID,0) > 0 THEN 2 WHEN coalesce(SRSL.numCountryID,0) > 0 THEN 3 END) LIMIT 1) THEN 1 ELSE 0 END
   ELSE 1
   END))
   ORDER BY SR.numSourceCompanyID DESC,intItemAffected,SR.numShippingRuleID;
END; $$; 
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719












