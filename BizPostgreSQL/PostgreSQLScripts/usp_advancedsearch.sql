DROP FUNCTION IF EXISTS USP_AdvancedSearch;

CREATE OR REPLACE FUNCTION USP_AdvancedSearch(v_WhereCondition VARCHAR(4000) DEFAULT '',                            
v_AreasofInt VARCHAR(100) DEFAULT '',                              
v_tintUserRightType SMALLINT DEFAULT NULL,                              
v_numDomainID NUMERIC(9,0) DEFAULT 0,                              
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                        
v_CurrentPage INTEGER DEFAULT NULL,                                                                    
v_PageSize INTEGER DEFAULT NULL,                                                                    
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                                             
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                              
v_ColumnSearch VARCHAR(100) DEFAULT '',                           
v_ColumnName VARCHAR(20) DEFAULT '',                             
v_GetAll BOOLEAN DEFAULT NULL,                              
v_SortCharacter CHAR(1) DEFAULT NULL,                        
v_SortColumnName VARCHAR(100) DEFAULT '',        
v_bitMassUpdate BOOLEAN DEFAULT NULL,        
v_LookTable VARCHAR(200) DEFAULT '' ,        
v_strMassUpdate TEXT DEFAULT '',
v_vcRegularSearchCriteria TEXT DEFAULT '',
v_vcCustomSearchCriteria TEXT DEFAULT '',
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
v_vcDisplayColumns TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  SMALLINT;
   v_vcFormFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(3);
   v_vcAssociatedControlType  VARCHAR(20);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(50);
   v_vcLookBackTableName  VARCHAR(50);
   v_WCondition  VARCHAR(1000);
   v_bitContactAddressJoinAdded  BOOLEAN;
   v_bitBillAddressJoinAdded  BOOLEAN;
   v_bitShipAddressJoinAdded  BOOLEAN;
   v_vcOrigDbColumnName  VARCHAR(50);
   v_bitAllowSorting BOOLEAN;
   v_bitAllowEdit  BOOLEAN;
   v_bitCustomField  BOOLEAN;
   v_ListRelID  NUMERIC(9,0);
   v_intColumnWidth  INTEGER;
   v_bitAllowFiltering  BOOLEAN;
   v_vcfieldatatype VARCHAR(100);
   v_zipCodeSearch  VARCHAR(100) DEFAULT '';
   v_Sql  VARCHAR(8000);
   v_numMaxFieldId  NUMERIC(9,0);
   v_Where  VARCHAR(1000);
   v_OrderBy  VARCHAR(1000);
   v_GroupBy  VARCHAR(1000);
   v_SelctFields  VARCHAR(4000);
   v_InneJoinOn  VARCHAR(1000);
   v_strSql  VARCHAR(8000);                                                              
   v_Prefix  VARCHAR(5);
   v_bitCustom  BOOLEAN;
   v_numFieldGroupID  INTEGER;
   v_vcColumnName  VARCHAR(200);
   v_numFieldID  NUMERIC(18,0);

   v_vcLocationID  VARCHAR(100) DEFAULT '0';
   v_firstRec  INTEGER;                                                                    
   v_lastRec  INTEGER;
   v_Strtemp  VARCHAR(500);
   v_strReplace  VARCHAR(8000) DEFAULT '';
   SWV_RowCount INTEGER;
BEGIN
	v_WhereCondition := REPLACE(LOWER(v_WhereCondition),LOWER('dbo.'),'');
	v_WhereCondition := REPLACE(LOWER(v_WhereCondition),LOWER('ISNULL'),LOWER('COALESCE'));
	v_vcRegularSearchCriteria := REPLACE(LOWER(v_vcRegularSearchCriteria),LOWER('dbo.'),'');
	v_vcRegularSearchCriteria := REPLACE(LOWER(v_vcRegularSearchCriteria),LOWER('ISNULL'),LOWER('COALESCE'));
	v_vcCustomSearchCriteria := REPLACE(LOWER(v_vcCustomSearchCriteria),LOWER('dbo.'),'');
	v_vcCustomSearchCriteria := REPLACE(LOWER(v_vcCustomSearchCriteria),LOWER('ISNULL'),LOWER('COALESCE'));

   IF POSITION('$SZ$' IN v_WhereCondition) > 0 then
	
      v_zipCodeSearch := SUBSTR(v_WhereCondition,POSITION('$SZ$' IN v_WhereCondition)+4,POSITION('$EZ$' IN v_WhereCondition) -POSITION('$SZ$' IN v_WhereCondition) -4);
      DROP TABLE IF EXISTS tt_TEMPADDRESS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPADDRESS
      (
         numAddressID NUMERIC(18,0) NOT NULL,
         vcAddressName VARCHAR(50),
         vcStreet VARCHAR(100),
         vcCity VARCHAR(50),
         vcPostalCode VARCHAR(15),
         numState NUMERIC(18,0),
         numCountry NUMERIC(18,0),
         bitIsPrimary BOOLEAN DEFAULT false NOT NULL,
         tintAddressOf SMALLINT NOT NULL,
         tintAddressType SMALLINT DEFAULT 0 NOT NULL,
         numRecordID NUMERIC(18,0) NOT NULL,
         numDomainID NUMERIC(18,0) NOT NULL
      );
      IF LENGTH(v_zipCodeSearch) > 0 then
         v_Strtemp := 'insert into tt_TEMPADDRESS select * from AddressDetails  where numDomainID=:1  
							and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' || coalesce(v_zipCodeSearch,'') || ')';
         EXECUTE format(v_Strtemp,v_numDomainID);
         v_WhereCondition := replace(v_WhereCondition,'$SZ$' || coalesce(v_zipCodeSearch,'') || '$EZ$',
         ' (ADC.numContactID in (select  numRecordID from tt_TEMPADDRESS where tintAddressOf=1 AND tintAddressType=0)   
			or (DM.numDivisionID in (select  numRecordID from tt_TEMPADDRESS where tintAddressOf=2 AND tintAddressType in(1,2))))');
      end if;
   end if;

   v_WCondition := '';
   
   IF (v_SortCharacter <> '0' AND v_SortCharacter <> '') then
      v_ColumnSearch := v_SortCharacter;
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numcontactid NUMERIC(18,0)
   );


---------------------Find Duplicate Code Start-----------------------------
   v_InneJoinOn := '';
   v_OrderBy := '';
   v_Sql := '';
   v_Where := '';
   v_GroupBy := '';
   v_SelctFields := '';
    
   IF (v_WhereCondition = 'DUP' OR v_WhereCondition = 'DUP-PRIMARY') then
	
      select   MAX(numFieldID) INTO v_numMaxFieldId FROM View_DynamicColumns WHERE numFormId = 24 AND numDomainId = v_numDomainID;
		--PRINT @numMaxFieldId
      WHILE v_numMaxFieldId > 0 LOOP
         select   vcFieldName, vcDbColumnName, vcLookBackTableName INTO v_vcFormFieldName,v_vcDbColumnName,v_vcLookBackTableName FROM View_DynamicColumns WHERE numFieldID = v_numMaxFieldId and numFormId = 24 AND numDomainId = v_numDomainID;
			--	PRINT @vcDbColumnName
         v_SelctFields := coalesce(v_SelctFields,'') ||  coalesce(v_vcDbColumnName,'') || ',';
         IF(v_vcLookBackTableName = 'AdditionalContactsInformation') then
				
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || 'ADC.' || coalesce(v_vcDbColumnName,'') || ' = a1.' || coalesce(v_vcDbColumnName,'') || ' and ';
            v_GroupBy := coalesce(v_GroupBy,'') || 'ADC.' ||  coalesce(v_vcDbColumnName,'') || ',';
         ELSEIF (v_vcLookBackTableName = 'DivisionMaster')
         then
				
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || 'DM.' || coalesce(v_vcDbColumnName,'') || ' = a1.' || coalesce(v_vcDbColumnName,'') || ' and ';
            v_GroupBy := coalesce(v_GroupBy,'') || 'DM.' ||  coalesce(v_vcDbColumnName,'') || ',';
         ELSE
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || 'C.' || coalesce(v_vcDbColumnName,'') || ' = a1.' || coalesce(v_vcDbColumnName,'') || ' and ';
            v_GroupBy := coalesce(v_GroupBy,'') || 'C.' || coalesce(v_vcDbColumnName,'') || ',';
         end if;
         v_Where := coalesce(v_Where,'') || 'and LEN(' || coalesce(v_vcDbColumnName,'') || ')>0 ';
         select   MAX(numFieldID) INTO v_numMaxFieldId FROM View_DynamicColumns WHERE numFormId = 24 AND numDomainId = v_numDomainID AND numFieldID < v_numMaxFieldId;
      END LOOP;
      IF(LENGTH(v_Where) > 2) then
				
         v_Where := SUBSTR(v_Where,length(v_Where) -(LENGTH(v_Where) -3)+1);
         v_OrderBy :=  SUBSTR(v_GroupBy,1,(LENGTH(v_GroupBy) -1));
         v_GroupBy :=  SUBSTR(v_GroupBy,1,(LENGTH(v_GroupBy) -1));
         v_InneJoinOn := SUBSTR(v_InneJoinOn,1,LENGTH(v_InneJoinOn) -3);
         v_SelctFields := SUBSTR(v_SelctFields,1,(LENGTH(v_SelctFields) -1));
         v_Sql := coalesce(v_Sql,'') || ' INSERT INTO tt_TEMPTABLE (numContactID)SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
						JOIN (SELECT   ' || coalesce(v_SelctFields,'') || '
					   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					   WHERE  ' || coalesce(v_Where,'') || ' AND C.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND ADC.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||
         ' GROUP BY ' || coalesce(v_GroupBy,'') || '
					   HAVING   COUNT(* ) > 1) a1
						ON  ' || coalesce(v_InneJoinOn,'') || ' where ADC.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and DM.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' ORDER BY ' || coalesce(v_OrderBy,'');
      ELSE
         v_Where := ' 1=0 ';
         v_Sql := coalesce(v_Sql,'') || ' INSERT INTO tt_TEMPTABLE (numContactID)SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
         || ' Where ' || coalesce(v_Where,'');
      end if;
      RAISE NOTICE '%',v_Sql;
      EXECUTE v_Sql;
      v_Sql := '';
      v_GroupBy := '';
      v_SelctFields := '';
      v_Where := '';
      IF (v_WhereCondition = 'DUP-PRIMARY') then
		  
         v_WCondition := ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM tt_TEMPTABLE) and COALESCE(ADC.bitPrimaryContact,false)=true';
      ELSE
         v_WCondition := ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM tt_TEMPTABLE)';
      end if;
      v_Sql := '';
      v_WhereCondition := '';
      v_Where := 'DUP';
   end if;
-----------------------------End Find Duplicate ------------------------

   v_strSql := 'Select ADC.numContactId                    
	  FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId';   

   IF v_SortColumnName = 'vcStreet' OR v_SortColumnName = 'vcCity' OR v_SortColumnName = 'vcPostalCode' OR v_SortColumnName = 'numCountry' OR v_SortColumnName = 'numState'
   OR v_ColumnName = 'vcStreet' OR v_ColumnName = 'vcCity' OR v_ColumnName = 'vcPostalCode' OR v_ColumnName = 'numCountry' OR v_ColumnName = 'numState' then
	
      v_strSql := coalesce(v_strSql,'') || ' left Join AddressDetails AD on AD.numRecordID = ADC.numContactId and AD.tintAddressOf = 1 and AD.tintAddressType = 0 AND AD.bitIsPrimary = true and AD.numDomainID = ADC.numDomainID';
   ELSEIF v_SortColumnName = 'vcBillStreet' OR v_SortColumnName = 'vcBillCity' OR v_SortColumnName = 'vcBillPostCode' OR v_SortColumnName = 'numBillCountry' OR v_SortColumnName = 'numBillState'
   OR v_ColumnName = 'vcBillStreet' OR v_ColumnName = 'vcBillCity' OR v_ColumnName = 'vcBillPostCode' OR v_ColumnName = 'numBillCountry' OR v_ColumnName = 'numBillState'
   then
	
      v_strSql := coalesce(v_strSql,'') || ' left Join AddressDetails AD1 on AD1.numRecordID = DM.numDivisionID and AD1.tintAddressOf = 2 and AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true and AD1.numDomainID = ADC.numDomainID ';
   ELSEIF v_SortColumnName = 'vcShipStreet' OR v_SortColumnName = 'vcShipCity' OR v_SortColumnName = 'vcShipPostCode' OR v_SortColumnName = 'numShipCountry' OR v_SortColumnName = 'numShipState'
   OR v_ColumnName = 'vcShipStreet' OR v_ColumnName = 'vcShipCity' OR v_ColumnName = 'vcShipPostCode' OR v_ColumnName = 'numShipCountry' OR v_ColumnName = 'numShipState'
   then
	
      v_strSql := coalesce(v_strSql,'') || ' left Join AddressDetails AD2 on AD2.numRecordID = DM.numDivisionID and AD2.tintAddressOf = 2 and AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true and AD2.numDomainID = ADC.numDomainID ';
   end if;	
                         
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then
	
      select   vcListItemType, numListID INTO v_vcListItemType,v_numListID from View_DynamicDefaultColumns where vcDbColumnName = v_ColumnName and numFormId = 1 and numDomainId = v_numDomainID;
      if v_vcListItemType = 'LI' then
		
         IF v_numListID = 40 then--Country
			
            IF v_ColumnName = 'numCountry' then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID = AD.numCountry';
            ELSEIF v_ColumnName = 'numBillCountry'
            then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID = AD1.numCountry';
            ELSEIF v_ColumnName = 'numShipCountry'
            then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID = AD2.numCountry';
            end if;
         ELSE
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
         end if;
      ELSEIF v_vcListItemType = 'S'
      then
		
         IF v_ColumnName = 'numState' then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID = AD.numState';
         ELSEIF v_ColumnName = 'numBillState'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID = AD1.numState';
         ELSEIF v_ColumnName = 'numShipState'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID = AD2.numState';
         end if;
      ELSEIF v_vcListItemType = 'T'
      then
		
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
      end if;
   end if;       
                      
   if (v_SortColumnName <> '') then
	
      select   vcListItemType, numListID INTO v_vcListItemType1,v_numListID from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 1 and numDomainId = v_numDomainID;
      if v_vcListItemType1 = 'LI' then
		
         IF v_numListID = 40 then--Country
			
            IF v_SortColumnName = 'numCountry' then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID = AD.numCountry';
            ELSEIF v_SortColumnName = 'numBillCountry'
            then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID = AD1.numCountry';
            ELSEIF v_SortColumnName = 'numShipCountry'
            then
				
               v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID = AD2.numCountry';
            end if;
         ELSE
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
         end if;
      ELSEIF v_vcListItemType1 = 'S'
      then
		
         IF v_SortColumnName = 'numState' then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID = AD.numState';
         ELSEIF v_SortColumnName = 'numBillState'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID = AD1.numState';
         ELSEIF v_SortColumnName = 'numShipState'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID = AD2.numState';
         end if;
      ELSEIF v_vcListItemType1 = 'T'
      then
		
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      end if;
   end if;                          
  
   v_strSql := coalesce(v_strSql,'') || ' where DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||   coalesce(v_WhereCondition,'');                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
   IF v_vcRegularSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if; 
   IF v_vcCustomSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;                          
                                            
   if    v_AreasofInt <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in(' || coalesce(v_AreasofInt,'') || '))';
   end if;
                             
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then

      if v_vcListItemType = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' and L1.vcData like ''' || coalesce(v_ColumnSearch,'') || '%''';
      ELSEIF v_vcListItemType = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' and S1.vcState like ''' || coalesce(v_ColumnSearch,'') || '%''';
      ELSEIF v_vcListItemType = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' and T1.vcTerName like ''' || coalesce(v_ColumnSearch,'')  || '%''';
      else
         IF v_ColumnName = 'vcStreet' OR v_ColumnName = 'vcBillStreet' OR v_ColumnName = 'vcShipStreet' then
            v_strSql := coalesce(v_strSql,'') || ' and vcStreet like ''' || coalesce(v_ColumnSearch,'')  || '%''';
         ELSEIF v_ColumnName = 'vcCity' OR v_ColumnName = 'vcBillCity' OR v_ColumnName = 'vcShipCity'
         then
            v_strSql := coalesce(v_strSql,'') || ' and vcCity like ''' || coalesce(v_ColumnSearch,'')  || '%''';
         ELSEIF v_ColumnName = 'vcPostalCode' OR v_ColumnName = 'vcBillPostCode' OR v_ColumnName = 'vcShipPostCode'
         then
            v_strSql := coalesce(v_strSql,'') || ' and vcPostalCode like ''' || coalesce(v_ColumnSearch,'')  || '%''';
         else
            v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_ColumnName,'') || ' like ''' || coalesce(v_ColumnSearch,'')  || '%''';
         end if;
      end if;
   end if;                          
   
   if (v_SortColumnName <> '') then
	
      if v_vcListItemType1 = 'LI' then
		
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
		
         v_strSql := coalesce(v_strSql,'') || ' order by S2.vcState ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'T'
      then
		
         v_strSql := coalesce(v_strSql,'') || ' order by T2.vcTerName ' || coalesce(v_columnSortOrder,'');
      else
         IF v_SortColumnName = 'vcStreet' OR v_SortColumnName = 'vcBillStreet' OR v_SortColumnName = 'vcShipStreet' then
            v_strSql := coalesce(v_strSql,'') || ' order by vcStreet ' || coalesce(v_columnSortOrder,'');
         ELSEIF v_SortColumnName = 'vcCity' OR v_SortColumnName = 'vcBillCity' OR v_SortColumnName = 'vcShipCity'
         then
            v_strSql := coalesce(v_strSql,'') || ' order by vcCity ' || coalesce(v_columnSortOrder,'');
         ELSEIF v_SortColumnName = 'vcPostalCode' OR v_SortColumnName = 'vcBillPostCode' OR v_SortColumnName = 'vcShipPostCode'
         then
            v_strSql := coalesce(v_strSql,'') || ' order by vcPostalCode ' || coalesce(v_columnSortOrder,'');
         ELSEIF v_SortColumnName = 'numRecOwner'
         then
            v_strSql := coalesce(v_strSql,'') || ' order by fn_GetContactName(ADC.numRecOwner) ' || coalesce(v_columnSortOrder,'');
         ELSEIF v_SortColumnName = 'Opp.vcLastSalesOrderDate'
         then
            v_strSql := coalesce(v_strSql,'') || ' order by (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ') ' || coalesce(v_columnSortOrder,'');
         else
            v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');
         end if;
      end if;
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' order by  ADC.numContactID asc ';
   end if;
   IF(v_Where <> 'DUP') then

      RAISE NOTICE '%',v_strSql;
      EXECUTE 'INSERT INTO tt_TEMPTABLE (numcontactid)
	' || v_strSql;
      v_strSql := '';
   end if;

   v_bitBillAddressJoinAdded := false;
   v_bitShipAddressJoinAdded := false;
   v_bitContactAddressJoinAdded := false;
   v_tintOrder := 0;
   v_WhereCondition := '';
   v_strSql := 'select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType ';                              
   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN,
      tintOrder INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 1;    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempIDs_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPIDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIDS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         vcFieldID VARCHAR(300)
      );
      INSERT INTO tt_TEMPIDS(vcFieldID)
      SELECT
      OutParam
      FROM
      SplitString(v_vcDisplayColumns,',');
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,(SELECT(ID::bigint -1) FROM tt_TEMPIDS T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
      FROM(SELECT
         numFieldID as numFormFieldID,
				0 AS bitCustomField,
				CONCAT(numFieldID,'~0') AS vcFieldID
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 1
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,true AS bitCustomField
				,CONCAT(Fld_id,'~1') AS vcFieldID
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT vcFieldID FROM tt_TEMPIDS);
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
      FROM(SELECT
         A.numFormFieldID,
				false AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = 1
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = 1
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT
         A.numFormFieldID,
				true AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = 1
         AND C.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFieldID,
			CAST(0 AS BOOLEAN),
			1,
			0
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 1
      AND numDomainID = v_numDomainID
      AND numFieldID = 3;
   end if;

   select   tintOrder, numFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, vcOrigDbColumnName, bitAllowSorting, bitAllowEdit, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,
   v_vcOrigDbColumnName,v_bitAllowSorting,v_bitAllowEdit,v_ListRelID,
   v_intColumnWidth,v_bitAllowFiltering,v_vcfieldatatype,v_numFieldGroupID FROM(SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,
			0 AS numGroupID
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 1
      AND coalesce(T1.bitCustomField,false) = false
      AND D.vcDbColumnName not in('numBillShipCountry','numBillShipState')
      UNION
      SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			'',
			false,
			false,
			CAST(null as INTEGER),
			CAST(null as DOUBLE PRECISION),
			false,
			'',
			Grp_id
      FROM
      CFW_Fld_Master c
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      c.Fld_id = T1.numFieldID
      WHERE
      c.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1    ORDER BY
   tintOrder asc LIMIT 1;                              

   while v_tintOrder > 0 LOOP
      IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      IF v_vcLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      IF v_bitCustom = false then
	
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || '~0';
         if v_vcAssociatedControlType = 'SelectBox' then
		
            RAISE NOTICE '%',v_vcDbColumnName;
            IF v_vcDbColumnName = 'numDefaultShippingServiceID' then
				
               v_strSql := coalesce(v_strSql,'') || ' ,COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR COALESCE(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'LI'
            then
			  
               IF v_numListID = 40 then--Country
				  
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF v_vcDbColumnName = 'numCountry' then
					
                     IF v_bitContactAddressJoinAdded = false then
					  
                        v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
                        v_bitContactAddressJoinAdded := true;
                     end if;
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD.numCountry';
                  ELSEIF v_vcDbColumnName = 'numBillCountry'
                  then
					
                     IF v_bitBillAddressJoinAdded = false then
						
                        v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                        v_bitBillAddressJoinAdded := true;
                     end if;
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry';
                  ELSEIF v_vcDbColumnName = 'numShipCountry'
                  then
					
                     IF v_bitShipAddressJoinAdded = false then
						
                        v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                        v_bitShipAddressJoinAdded := true;
                     end if;
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry';
                  end if;
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
			  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'ListBox'
         then
		
            if v_vcListItemType = 'S' then
			  
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF v_vcDbColumnName = 'numState' then
				
                  IF v_bitContactAddressJoinAdded = false then
					   
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
                     v_bitContactAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD.numState';
               ELSEIF v_vcDbColumnName = 'numBillState'
               then
				
                  IF v_bitBillAddressJoinAdded = false then
					
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                     v_bitBillAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD1.numState';
               ELSEIF v_vcDbColumnName = 'numShipState'
               then
				
                  IF v_bitShipAddressJoinAdded = false then
					
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                     v_bitShipAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD2.numState';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
		
            IF v_vcDbColumnName = 'vcStreet' OR v_vcDbColumnName = 'vcCity' OR v_vcDbColumnName = 'vcPostalCode' then
			
               v_strSql := coalesce(v_strSql,'') || ',AD.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF v_bitContactAddressJoinAdded = false then
			   
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
                  v_bitContactAddressJoinAdded := true;
               end if;
            ELSEIF v_vcDbColumnName = 'vcBillStreet' OR v_vcDbColumnName = 'vcBillCity' OR v_vcDbColumnName = 'vcBillPostCode'
            then
			
               v_strSql := coalesce(v_strSql,'') || ',AD1.' || REPLACE(REPLACE(v_vcDbColumnName,'Bill',''),'PostCode','PostalCode') || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF v_bitBillAddressJoinAdded = false then
			   
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                  v_bitBillAddressJoinAdded := true;
               end if;
            ELSEIF v_vcDbColumnName = 'vcShipStreet' OR v_vcDbColumnName = 'vcShipCity' OR v_vcDbColumnName = 'vcShipPostCode'
            then
			
               v_strSql := coalesce(v_strSql,'') || ',AD2.' || REPLACE(REPLACE(v_vcDbColumnName,'Ship',''),'PostCode','PostalCode') || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF v_bitShipAddressJoinAdded = false then
			   
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                  v_bitShipAddressJoinAdded := true;
               end if;
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' when v_vcDbColumnName = 'bintCreatedDate' then 'FormatedDateFromDate(DM.bintCreatedDate + make_interval() DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(30)) || ',),DM.numDomainID)' else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField' and v_vcDbColumnName = 'vcLastSalesOrderDate'
         then
		
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' LEFT JOIN LATERAL(' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON TRUE ';
            v_strSql := coalesce(v_strSql,'') || ',' || CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate + make_interval(mins => ',-v_ClientTimeZoneOffset,'),',v_numDomainID,'),'''')') || ' "' || coalesce(v_vcColumnName,'') || '"';
         ELSE
            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' when v_vcDbColumnName = 'bintCreatedDate' then 'FormatedDateFromDate(DM.bintCreatedDate,DM.numDomainID)' else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then
	
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
         v_vcColumnName := CONCAT('Cust',v_numFieldID,'~',v_numFieldID,'~',1);
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
		
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ' ||(CASE WHEN v_numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN v_numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) || ' CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId= (CASE WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 1 THEN DM.numDivisionId WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 4 THEN ADC.numContactID ELSE 0 END) ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ' ||(CASE WHEN v_numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN v_numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) || ' CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId= (CASE WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 1 THEN DM.numDivisionId WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 4 THEN ADC.numContactID ELSE 0 END) ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ' ||(CASE WHEN v_numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN v_numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) || ' CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId= (CASE WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 1 THEN DM.numDivisionId WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 4 THEN ADC.numContactID ELSE 0 END) ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
		
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ' ||(CASE WHEN v_numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN v_numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) || ' CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId= (CASE WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 1 THEN DM.numDivisionId WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 4 THEN ADC.numContactID ELSE 0 END) ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID::VARCHAR=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
		
            RAISE NOTICE 'Custom1';
            v_strSql := coalesce(v_strSql,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'',''))), '''')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ' ||(CASE WHEN v_numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN v_numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) || ' CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId= (CASE WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 1 THEN DM.numDivisionId WHEN ' || SUBSTR(CAST(v_numFieldGroupID AS VARCHAR(30)),1,30) || ' = 4 THEN ADC.numContactID ELSE 0 END) ';
         end if;
      end if;
      select   tintOrder, numFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, vcOrigDbColumnName, bitAllowSorting, bitAllowEdit, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,
      v_vcOrigDbColumnName,v_bitAllowSorting,v_bitAllowEdit,v_ListRelID,
      v_intColumnWidth,v_bitAllowFiltering,v_vcfieldatatype,v_numFieldGroupID FROM(SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,
			0 AS numGroupID
         FROM
         View_DynamicDefaultColumns D
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         D.numFieldID = T1.numFieldID
         WHERE
         D.numDomainID = v_numDomainID
         AND D.numFormId = 1
         AND coalesce(T1.bitCustomField,false) = false
         AND D.vcDbColumnName not in('numBillShipCountry','numBillShipState')
         AND T1.tintOrder > v_tintOrder -1
         UNION
         SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			'',
			false,
			false,
			CAST(null as INTEGER),
			CAST(null as DOUBLE PRECISION),
			false,
			'',
			Grp_id
         FROM
         CFW_Fld_Master c
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         c.Fld_id = T1.numFieldID
         WHERE
         c.numDomainID = v_numDomainID
         AND coalesce(T1.bitCustomField,false) = true
         AND T1.tintOrder > v_tintOrder -1) T1    ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                              

                                                                  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                     
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                   
                                          
   if v_GetAll = false then

      if v_bitMassUpdate = true then
         v_strReplace := case when v_LookTable = 'DivisionMaster' then 'DM.numDivisionID' when v_LookTable = 'AdditionalContactsInformation' then 'ADC.numContactId' when v_LookTable = 'ContactAddress' then 'ADC.numContactId' when v_LookTable = 'ShipAddress' then 'DM.numDivisionID' when v_LookTable = 'BillAddress' then 'DM.numDivisionID' when v_LookTable = 'CompanyInfo' then 'C.numCompanyId' end
         || ' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    ' || coalesce(v_WhereCondition,'');
         v_strMassUpdate := replace(v_strMassUpdate,'@$replace',v_strReplace);
         RAISE NOTICE '%',CONCAT('MassUPDATE:',v_strMassUpdate);
         EXECUTE v_strMassUpdate;
      end if;
      IF (v_Where = 'DUP') then
 
         v_strSql := coalesce(v_strSql,'') || ' FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join tt_TEMPTABLE T on T.numContactId=ADC.numContactId '
         || coalesce(v_WhereCondition,'') || '
	  WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) ||  coalesce(v_WCondition,'') || ' order by T.ID, C.vcCompanyName'; -- @OrderBy
         RAISE NOTICE '%',v_strSql;
         OPEN SWV_RefCur FOR EXECUTE v_strSql;
         
         RETURN;
      ELSE
         v_strSql := coalesce(v_strSql,'') || ' FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   ' || coalesce(v_WhereCondition,'') || ' inner join tt_TEMPTABLE T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
         v_strSql := coalesce(v_strSql,'') || ' order by T.ID, C.vcCompanyName ';
      end if;
   else
      v_strSql := coalesce(v_strSql,'') || ' FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  ' || coalesce(v_WhereCondition,'') || ' join tt_TEMPTABLE T on T.numContactId=ADC.numContactId';
      v_strSql := coalesce(v_strSql,'') || ' order by T.ID, C.vcCompanyName';
   end if;       
   RAISE NOTICE '%',v_strSql;                                                      
   OPEN SWV_RefCur FOR EXECUTE v_strSql; 
   


   open SWV_RefCur2 for
   SELECT
   tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
   FROM(SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,
			0 AS numGroupID
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 1
      AND coalesce(T1.bitCustomField,false) = false
      AND D.vcDbColumnName not in('numBillShipCountry','numBillShipState')
      UNION
      SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			'',
			false,
			false,
			CAST(null as INTEGER),
			CAST(null as DOUBLE PRECISION),
			false,
			'',
			Grp_id
      FROM
      CFW_Fld_Master c
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      c.Fld_id = T1.numFieldID
      WHERE
      c.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1
   ORDER BY
   tintOrder asc;       

   IF EXISTS(SELECT * FROM pg_tables WHERE tablename = ' "tempdb..tt_TEMPADDRESS"') then

      DROP TABLE IF EXISTS tt_TEMPADDRESS CASCADE;
   end if;
END; $$;
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  


