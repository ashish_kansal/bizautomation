-- Stored procedure definition script USP_NewGetTBSummary for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC [USP_NewGetTBSummary] @numDomainId=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
CREATE OR REPLACE FUNCTION USP_NewGetTBSummary(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
          

--select * from view_journal where numDomainid=72

   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
CAST(coalesce(COA.numOriginalOpeningBal,0)+coalesce((SELECT sum(Debit -Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date < v_dtFromDate),cast(0 as NUMERIC(19,4))) AS NUMERIC(19,4)) AS OPENING,
coalesce((SELECT sum(Debit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as DEBIT,
coalesce((SELECT sum(Credit) FROM VIEW_JOURNAL VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as CREDIT
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId;

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID,CAST(ATD.vcAccountCode AS VARCHAR(250)), CAST('' AS VARCHAR(50)),
 coalesce(SUM(Opening),cast(0 as NUMERIC(19,4))) as Opening,
coalesce(Sum(Debit),cast(0 as NUMERIC(19,4))) as Debit,coalesce(Sum(Credit),cast(0 as NUMERIC(19,4))) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;
--------------------------------------------------------
   open SWV_RefCur for
   SELECT *,Opening+Debit -Credit as Balance,
CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE P.vcAccountCode
   END AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   UNION
   SELECT *,Opening+Debit -Credit as Balance,
CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE O.vcAccountCode
   END AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, 2 as Type
   FROM tt_PLOUTPUT O;
 
   open SWV_RefCur2 for
   SELECT SUM(Debit) FROM tt_PLOUTPUT O WHERE O.TYPE = 1;


   RETURN;
END; $$;


