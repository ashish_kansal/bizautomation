-- Stored procedure definition script USP_Item_GetKits for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetKits(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		I.numItemCode
		,I.vcItemName
	FROM
		Item I
	WHERE
		I.numDomainID = v_numDomainID
		AND coalesce(I.bitKitParent,false) = true
		AND(SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true) = 0
	ORDER BY
		I.vcItemName;
END; $$;

