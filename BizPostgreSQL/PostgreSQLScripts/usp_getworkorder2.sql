DROP FUNCTION IF EXISTS USP_GetWorkOrder2;

CREATE OR REPLACE FUNCTION USP_GetWorkOrder2(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_tintUserRightType SMALLINT
	,v_CurrentPage INTEGER                                                
	,v_PageSize INTEGER
	,v_ClientTimeZoneOffset INTEGER
	,v_numWOStatus NUMERIC(18,0)
	,v_vcRegularSearchCriteria TEXT
	,v_vcCustomSearchCriteria TEXT
	,v_SearchText VARCHAR(300) DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCurrencySymbol  VARCHAR(20) DEFAULT '';
   v_tintDecimalPoints  SMALLINT;
   v_vcDynamicQuery  TEXT DEFAULT '';
   v_vcStatusQuery  VARCHAR(500) DEFAULT '';
BEGIN
   select   coalesce(Currency.varCurrSymbol,''), coalesce(Domain.tintDecimalPoints,0) INTO v_vcCurrencySymbol,v_tintDecimalPoints FROM
   Domain
   LEFT JOIN
   Currency
   ON
   Currency.numDomainId = v_numDomainID
   AND Domain.numCurrencyID = Currency.numCurrencyID WHERE
   Domain.numDomainId = v_numDomainID; 

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWorkOrderStatus_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWORKORDERSTATUS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWORKORDERSTATUS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0),
      numWorkOrderStatus SMALLINT
   );


   IF(v_vcRegularSearchCriteria <> '') then
	
      IF POSITION('WO.TotalProgress' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.TotalProgress','GetTotalProgress(WO.numDomainID,WO.numWOID,1::SMALLINT,1::SMALLINT,'''',0)');
      end if;
      IF POSITION('WO.dtmStartDate' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.dtmStartDate',CONCAT('FormatedDateFromDate(WO.dtmStartDate + make_interval(mins => -1 * ',v_ClientTimeZoneOffset,'),',v_numDomainID,')'));
      end if;
      IF POSITION('WO.dtmActualStartDate' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.dtmActualStartDate',CONCAT('FormatedDateFromDate((SELECT SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime LIMIT 1) + make_interval(mins => -1 * ',v_ClientTimeZoneOffset,')',',',v_numDomainID,')'));
      end if;
      IF POSITION('WO.dtmEndDate' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.dtmEndDate',CONCAT('FormatedDateFromDate( WO.dtmEndDate + make_interval(mins => -1 * ',v_ClientTimeZoneOffset,')',',',v_numDomainID,')'));
      end if;
      IF POSITION('WO.dtmProjectFinishDate' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.dtmProjectFinishDate',(CASE WHEN v_numWOStatus = 1 THEN CONCAT('FormatedDateFromDate(WO.bintCompletedDate + make_interval(mins => -1 * ',v_ClientTimeZoneOffset,')',',',v_numDomainID,')') ELSE 'TableProjectedFinish.dtProjectedFinish AS DATE)' END));
      end if;
      IF POSITION('WO.ForecastDetails=1' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(''day'',WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) > 0');
      end if;
      IF POSITION('WO.ForecastDetails=2' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(''day'',WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) < 0');
      end if;
      IF POSITION('WO.SchedulePerformance=1' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) > 0');
      end if;
      IF POSITION('WO.SchedulePerformance=2' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) < 0');
      end if;
      IF POSITION('WO.ActualProjectDifference=1' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ActualProjectDifference=1','COALESCE(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND COALESCE(TableHourCost.numActualTimeInMinutes,0) > COALESCE(TableHourCost.numEstimatedTimeInMinutes,0)');
      end if;
      IF POSITION('WO.ActualProjectDifference=2' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.ActualProjectDifference=2','COALESCE(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND COALESCE(TableHourCost.numActualTimeInMinutes,0) < COALESCE(TableHourCost.numEstimatedTimeInMinutes,0)');
      end if;
      IF POSITION('WO.CapacityLoad' IN v_vcRegularSearchCriteria) > 0 then
		
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.CapacityLoad','TableCapacity.numCapacityLoad');
      end if;
      IF POSITION('WO.vcWorkOrderStatus' IN v_vcRegularSearchCriteria) > 0 then
		
         INSERT INTO tt_TEMPWORKORDERSTATUS(numWOID
				,numWorkOrderStatus)
         SELECT
         numWOID
				,numWorkOrderStatus
         FROM
         GetWOStatus(v_numDomainID,'')
         WHERE
         numWorkOrderStatus =(CASE
         WHEN POSITION('WO.vcWorkOrderStatus ilike ''%1%''' IN v_vcRegularSearchCriteria) > 0 THEN 1
         WHEN POSITION('WO.vcWorkOrderStatus ilike ''%2%''' IN v_vcRegularSearchCriteria) > 0 THEN 2
         WHEN POSITION('WO.vcWorkOrderStatus ilike ''%3%''' IN v_vcRegularSearchCriteria) > 0 THEN 3
         WHEN POSITION('WO.vcWorkOrderStatus ilike ''%4%''' IN v_vcRegularSearchCriteria) > 0 THEN 4
         ELSE 1
         END);
         v_vcStatusQuery := 'INNER JOIN 
										tt_TEMPWORKORDERSTATUS TWOS 
									ON 
										WO.numWOID = TWOS.numWOID';
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.vcWorkOrderStatus ilike ''%1%''',
         '1=1');
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.vcWorkOrderStatus ilike ''%2%''',
         '1=1');
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.vcWorkOrderStatus ilike ''%3%''',
         '1=1');
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WO.vcWorkOrderStatus ilike ''%4%''',
         '1=1');
      end if;
      v_vcRegularSearchCriteria := ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;

   v_vcDynamicQuery := ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								WO.numWOID,
								WO.vcWorkOrderName,
								WO.numQtyItemsReq,
								WHI.numWareHouseID,
								WO.numWarehouseItemID,
								WHI.numWareHouseID,
								(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS MaxBuildQty,
								I.vcItemName,
								(SELECT FormatedDateFromDate(WO.dtmStartDate + make_interval(mins => -1 * ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')' || ',' || v_numDomainID || ')) AS dtmStartDate,
								(SELECT SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime LIMIT 1) AS dtmActualStartDate,
								WO.dtmEndDate + make_interval(mins => -1 * ' || v_ClientTimeZoneOffset || ') dtmEndDate,
								WO.bintCompletedDate AS dtmProjectFinishDate,
								OI.ItemReleaseDate,
								'''' AS ForecastDetails,
								SP.Slp_Name,
								fn_GetContactName(COALESCE(WO.numAssignedTo,0)) AS vcAssignedTo,
								0 AS CapacityLoad,
								Opp.vcPOppName,
								WO.numWOStatus,
								WO.numWOID AS "numWOID~0~0",
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',COALESCE(WO.vcWorkOrderName,''-''),'' ('',COALESCE(WO.numQtyItemsReq,0),'')'',''</a>'') AS "vcWorkOrderName~16~0",
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',COALESCE(WO.vcWorkOrderName,''-''),'' ('',COALESCE(WO.numQtyItemsReq,0),'')'',''</a>'') AS "vcWorkOrderNameWithQty~1~0",
								CONCAT(I.vcItemName,(CASE WHEN COALESCE(I.vcSKU,'''') = '''' THEN '''' ELSE CONCAT('' ('',I.vcSKU,'')'') END)) AS ' ||(CASE WHEN v_numWOStatus = 1 THEN '"AssemblyItemSKU~17~0"' ELSE '"AssemblyItemSKU~4~0"' END) || ',
								(CASE 
									WHEN COALESCE(TableHourCost.numEstimatedTimeInMinutes,0) > 0 
									THEN CONCAT((CASE 
													WHEN COALESCE(TableHourCost.numActualTimeInMinutes,0) > COALESCE(TableHourCost.numEstimatedTimeInMinutes,0) 
													THEN ''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">''
													ELSE ''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">''
												END),COALESCE(TableHourCost.vcActualTaskTime,''-''),'' & '',''' || coalesce(v_vcCurrencySymbol,'') || ''',CAST(TableHourCost.monActualLabourCost AS DECIMAL(20,' || v_tintDecimalPoints || ')),(CASE WHEN COALESCE(TableHourCost.numActualTimeInMinutes,0) > COALESCE(TableHourCost.numEstimatedTimeInMinutes,0) THEN '' > Projected'' ELSE '' < Projected'' END),''</lable>'') 
									ELSE '''' 
								END) AS "ActualProjectDifference~23~0",
								WO.numQtyItemsReq AS "numQtyItemsReq~18~0",
								WHI.numWareHouseID AS "numWareHouseID~0~0",
								WO.numWarehouseItemID AS "numWarehouseItemID~0~0",
								WHI.numWareHouseID AS "numWareHouseID~0~0",
								(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS "MaxBuildQty~2~0",
								GetTotalProgress(WO.numDomainID,WO.numWOID,1::SMALLINT,1::SMALLINT,'''',0) AS "TotalProgress~3~0",
								I.vcItemName AS "vcItemName~0~0",
								(SELECT FormatedDateFromDate(WO.dtmStartDate + make_interval(mins =>  -1 * ' || v_ClientTimeZoneOffset || ')' || ',' || v_numDomainID || ')) AS "dtmStartDate~5~0",
								(SELECT FormatedDateFromDate((SELECT SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime LIMIT 1) + make_interval(mins => -1 * ' || v_ClientTimeZoneOffset || ')' || ',' || v_numDomainID || ')) AS "dtmActualStartDate~6~0",
								(SELECT FormatedDateFromDate(WO.dtmEndDate + make_interval(mins => -1 * ' || v_ClientTimeZoneOffset || ')' || ',' || v_numDomainID || ')) AS ' ||(CASE WHEN v_numWOStatus = 1 THEN '"dtmEndDate~19~0"' ELSE '"dtmEndDate~7~0"' END) || ',
								' ||(CASE WHEN v_numWOStatus = 1 THEN CONCAT('FormatedDateFromDate(WO.bintCompletedDate + make_interval(mins => -1 * ',v_ClientTimeZoneOffset,')',',',v_numDomainID,') AS "dtmProjectFinishDate~20~0"') ELSE '''<label class="lblProjectedFinish"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></lable>'' AS "dtmProjectFinishDate~8~0"' END) || ',
								(SELECT FormatedDateFromDate(OI.ItemReleaseDate::TIMESTAMP + make_interval(mins => -1 * ' || v_ClientTimeZoneOffset || ')' || ',' || v_numDomainID || ')) AS ' ||(CASE WHEN v_numWOStatus = 1 THEN '"ItemReleaseDate~21~0"' ELSE '"ItemReleaseDate~9~0"' END) || ',
								''<label class="lblForecastDetail"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>'' AS "ForecastDetails~10~0",
								(CASE 
									WHEN DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) > 0 
									THEN CONCAT(''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">'',DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate),(CASE WHEN DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) > 1 THEN '' Days'' ELSE '' Day'' END),'' Late</lable>'')
									WHEN DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) <= 0
									THEN CONCAT(''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">'',DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) * -1,(CASE WHEN DATEDIFF(''day'',WO.dtmEndDate,WO.bintCompletedDate) * -1 > 1 THEN '' Days'' ELSE '' Day'' END),'' Early</lable>'')
									ELSE ''''
								END) AS "SchedulePerformance~22~0",
								SP.Slp_Name AS ' ||(CASE WHEN v_numWOStatus = 1 THEN '"Slp_Name~24~0"' ELSE '"Slp_Name~11~0"' END) || ',
								fn_GetContactName(COALESCE(WO.numAssignedTo,0)) AS ' ||(CASE WHEN v_numWOStatus = 1 THEN '"vcAssignedTo~25~0"' ELSE '"vcAssignedTo~12~0"' END) || ',
								''<label class="lblCapacityLoad"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''	AS "CapacityLoad~13~0",
								Opp.vcPOppName AS "vcPOppName~0~0",
								WO.numWOStatus AS "numWOStatus~0~0",
								''<label class="lblWorkOrderStatus"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''AS "vcWorkOrderStatus~14~0",
								GetWorkOrderPickedRemainingQty(WO.numWOId,WO.numQtyItemsReq) "vcPickedRemaining~15~0"
							FROM 
								WorkOrder AS WO 
							' || coalesce(v_vcStatusQuery,'') || '
							LEFT JOIN  
								Item I 
							ON 
								WO.numItemCode=I.numItemCode 
							LEFT JOIN
								WareHouseItems AS WHI
							ON
								WO.numWarehouseItemID = WHI.numWareHouseItemID
							LEFT JOIN
								Warehouses AS WH
							ON
								WHI.numWareHouseID = WH.numWareHouseID
							LEFT JOIN
								OpportunityMaster AS Opp
							ON
								WO.numOppId = Opp.numOppId 
							LEFT JOIN
								OpportunityItems AS OI
							ON
								OPP.numOppId=OI.numOppId
								AND OI.numOppItemtcode = WO.numOppItemID
							LEFT JOIN
								Sales_process_List_Master AS SP
							ON
								WO.numBuildProcessId = SP.Slp_id
							LEFT JOIN LATERAL
							(
								SELECT
									GetProjectedFinish(' || v_numDomainID || ',2::SMALLINT,WO.numWOID,0,0,NULL,' || coalesce(v_ClientTimeZoneOffset,0) || ',0) AS dtProjectedFinish
							) TableProjectedFinish ON TRUE
							LEFT JOIN LATERAL
							(
								SELECT 
									GetCapacityLoad(' || v_numDomainID || ',0,0,WO.numWOID,0,4::SMALLINT,COALESCE(WO.dtmStartDate,WO.bintCreatedDate),' || coalesce(v_ClientTimeZoneOffset,0) || ') AS numCapacityLoad
							) TableCapacity ON TRUE
							LEFT JOIN LATERAL
							(
								SELECT
									LTRIM(TO_CHAR(FLOOR(SUM(numEstimatedTimeInMinutes) / 60),''99999999999900'')) || '':'' || LTRIM(TO_CHAR(SUM(numEstimatedTimeInMinutes) % 60,''99999999999900'')) vcEstimatedTaskTime
									,LTRIM(TO_CHAR(FLOOR(SUM(numActualTimeInMinutes) / 60),''99999999999900'')) || '':'' || LTRIM(TO_CHAR(SUM(numActualTimeInMinutes) % 60,''99999999999900'')) vcActualTaskTime
									,SUM(numEstimatedTimeInMinutes) numEstimatedTimeInMinutes
									,SUM(numActualTimeInMinutes) numActualTimeInMinutes
									,SUM(monEstimatedLabourCost) monEstimatedLabourCost
									,SUM(monActualLabourCost) monActualLabourCost
								FROM
								(
									SELECT 
										((COALESCE(SPDT.numHours,0) * 60) + COALESCE(SPDT.numMinutes,0)) * COALESCE(WO.numQtyItemsReq,0)  numEstimatedTimeInMinutes
										,TEMPTime.vcTime numActualTimeInMinutes
										,((COALESCE(SPDT.numHours,0) * 60) + COALESCE(SPDT.numMinutes,0)) * COALESCE(WO.numQtyItemsReq,0) * (COALESCE(SPDT.monHourlyRate,0)/60) monEstimatedLabourCost
										,TEMPTime.vcTime * (COALESCE(SPDT.monHourlyRate,0)/60) monActualLabourCost
									FROM 
										StagePercentageDetailsTask SPDT
									LEFT JOIN LATERAL
									(
										SELECT GetTimeSpendOnTaskInMinutes AS vcTime FROM GetTimeSpendOnTaskInMinutes(205,SPDT.numTaskId)
									) TEMPTime ON TRUE
									WHERE 
										numWorkOrderId=WO.numWOId
								) TEMP
							) TableHourCost ON TRUE
							WHERE
								WO.numDomainID=' || v_numDomainID  || ' 
								AND 1 = (CASE ' || v_tintUserRightType || '
											WHEN 3 THEN 1 
											WHEN 1 THEN (CASE 
															WHEN WO.numAssignedTo=' || v_numUserCntID || ' 
																	OR EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WO.numWOID AND numAssignTo=' || v_numUserCntID || ') 
																	OR (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=' || v_numDomainID || ' AND numUserDetailID=' || v_numUserCntID || ') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=' || v_numDomainID || ' AND EAD.numContactID=CAST(' || v_numUserCntID || ' AS VARCHAR) AND EA.numDivisionID=Opp.numDivisionID))
															THEN 1 
															ELSE 0 
														END) 
											ELSE 0
										END)
								AND 1 = (CASE WHEN COALESCE(' || v_numWOStatus || ',0) = 1 AND WO.numWOStatus=23184 THEN 1  WHEN COALESCE(''' || v_numWOStatus || ''',0)=0 AND WO.numWOStatus<>23184 THEN 1 ELSE 0 END) ' || coalesce(v_vcRegularSearchCriteria,'') || ' ORDER BY WO.bintCreatedDate desc OFFSET ' || ((v_CurrentPage::bigint -1)*v_PageSize::bigint) || ' ROWS FETCH NEXT ' || v_PageSize || ' ROWS ONLY ';

   RAISE NOTICE '%',v_vcDynamicQuery;
   OPEN SWV_RefCur FOR EXECUTE v_vcDynamicQuery;

   DROP TABLE IF EXISTS tt_TEMPCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLUMNS
   (
      vcFieldName VARCHAR(200),
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(200),
      bitAllowSorting BOOLEAN,
      numFieldId INTEGER,
      bitAllowEdit BOOLEAN,
      bitCustomField BOOLEAN,
      vcAssociatedControlType VARCHAR(50),
      vcListItemType  VARCHAR(200),
      numListID INTEGER,
      ListRelID INTEGER,
      vcLookBackTableName VARCHAR(200),
      bitAllowFiltering BOOLEAN,
      vcFieldDataType  VARCHAR(20),
      intColumnWidth INTEGER,
      bitClosedColumn BOOLEAN
   );

   IF v_numWOStatus = 1 then
	
      INSERT INTO tt_TEMPCOLUMNS(vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn)
		VALUES('Work Order','vcWorkOrderName','vcWorkOrderName',false,16,false,false,'TextBox','',0,0,'WorkOrder',true,'V',true),('Assembly Item (SKU)','AssemblyItemSKU','AssemblyItemSKU',false,17,false,false,'TextBox','',0,0,'Item',true,'V',true),
      ('Quantity','numQtyItemsReq','numQtyItemsReq',false,18,false,false,'TextBox','',0,0,'WorkOrder',true,'V',true),
      ('Requested Finish','dtmEndDate','dtmEndDate',false,19,false,false,'DateField','',0,0,'WorkOrder',true,'V',true),
      ('Actual Finish','dtmProjectFinishDate','dtmProjectFinishDate',false,20,false,false,'DateField','',0,0,'WorkOrder',true,'V',true),
      ('Item Release','ItemReleaseDate','ItemReleaseDate',false,21,false,false,'DateField','',0,0,'OpportunityItems',true,'V',true),('Schedule Performance','SchedulePerformance','SchedulePerformance',false,22,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',true),
      ('Actual Time & $ vs Projected','ActualProjectDifference','ActualProjectDifference',false,23,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',true),('Build Process','Slp_Name','Slp_Name',false,24,false,false,'SelectBox','',0,0,'Sales_process_List_Master',true,'V',true),
      ('Build Manager','vcAssignedTo','vcAssignedTo',false,25,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',true);
   ELSE
      INSERT INTO tt_TEMPCOLUMNS(vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn)
		VALUES('Work Order(Qty)','vcWorkOrderNameWithQty','vcWorkOrderNameWithQty',false,1,false,false,'TextBox','',0,0,'WorkOrder',true,'V',false),('Max Build Qty','MaxBuildQty','MaxBuildQty',false,2,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',false),('Total Progress','TotalProgress','TotalProgress',false,3,false,false,'TextBoxRangePercentage','',0,0,'WorkOrder',true,'V',false),
      ('Item','AssemblyItemSKU','AssemblyItemSKU',false,4,false,false,'TextBox','',0,0,'Item',true,'V',false),
      ('Planned Start','dtmStartDate','dtmStartDate',false,5,false,false,'DateField','',0,0,'WorkOrder',true,'V',false),
      ('Actual Start','dtmActualStartDate','dtmActualStartDate',false,6,false,false,'DateField','',0,0,'WorkOrder',true,'V',false),('Requested Finish','dtmEndDate','dtmEndDate',false,7,false,false,'DateField','',0,0,'WorkOrder',true,'V',false),
      ('Projected Finish','dtmProjectFinishDate','dtmProjectFinishDate',false,8,false,false,'DateField','',0,0,'WorkOrder',true,'V',false),('Item Release','ItemReleaseDate','ItemReleaseDate',false,9,false,false,'DateField','',0,0,'OpportunityItems',true,'V',false),
      ('Forecast','ForecastDetails','ForecastDetails',false,10,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',false),
      ('Build Process','Slp_Name','Slp_Name',false,11,false,false,'SelectBox','',0,0,'Sales_process_List_Master',true,'V',false),
      ('Build Manager','vcAssignedTo','vcAssignedTo',false,12,false,false,'SelectBox','',0,0,'WorkOrder',true,'V',false),
      ('Capacity Load','CapacityLoad','CapacityLoad',false,13,false,false,'TextBoxRangePercentage','',0,0,'WorkOrder',true,'V',false),('Work Order Status','vcWorkOrderStatus','WorkOrderStatus',false,14,false,false,'Label','',0,0,'WorkOrder',true,'V',false),
      ('Picked | Remaining','vcPickedRemaining','PickedRemaining',false,15,false,false,'Label','',0,0,'WorkOrder',true,'V',false);
   end if;

   UPDATE
   tt_TEMPCOLUMNS TC
   SET
   intColumnWidth = coalesce(DFCD.intColumnWidth,0)
   FROM
   DycFormConfigurationDetails DFCD
   WHERE
   TC.numFieldId = DFCD.numFieldID AND(DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 145
   AND DFCD.tintPageType = 1);


   IF v_numWOStatus <> 1 AND EXISTS(SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numFormID = 145 AND tintPageType = 1) then
	
      open SWV_RefCur2 for
      SELECT
      TC.* FROM
      DycFormConfigurationDetails DFCD
      INNER JOIN
      tt_TEMPCOLUMNS TC
      ON
      DFCD.numFieldID = TC.numFieldId
      WHERE
      numDomainId = v_numDomainID
      AND numUserCntID = v_numUserCntID
      AND numFormId = 145
      AND tintPageType = 1
      ORDER BY
      DFCD.intRowNum;
   ELSE
      open SWV_RefCur2 for
      SELECT * FROM tt_TEMPCOLUMNS;
   end if;
   RETURN;
END; $$;


