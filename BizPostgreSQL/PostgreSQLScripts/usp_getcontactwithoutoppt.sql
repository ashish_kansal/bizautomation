-- Stored procedure definition script usp_GetContactWithoutOppt for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactWithoutOppt(v_numOppId NUMERIC,
	v_numDivId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select a.numContactId, a.vcFirstName,a.vcLastname
   from AdditionalContactsInformation a
   WHERE a.numContactId not in(select cast(numContactID as NUMERIC(18,0)) from OpportunityContact where numOppId = v_numOppId)
   AND a.numContactId not in(select numContactId from OpportunityMaster where numOppId = v_numOppId)
   AND a.numDivisionId = v_numDivId group by a.numContactId,a.vcFirstName,a.vcLastname;
END; $$;












