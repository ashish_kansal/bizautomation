-- Stored procedure definition script USP_ManageVendorShipmentMethod for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageVendorShipmentMethod(v_numDomainID NUMERIC(18,0) DEFAULT 0,    
	v_numVendorid NUMERIC(18,0) DEFAULT 0,
	v_numAddressID NUMERIC(18,0) DEFAULT 0,
	v_numWarehouseID NUMERIC(18,0) DEFAULT 0,
	v_str TEXT DEFAULT '',
	v_bitPrimary BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   DELETE FROM VendorShipmentMethod WHERE numDomainId = v_numDomainID AND numVendorID = v_numVendorid AND numAddressID = v_numAddressID AND numWarehouseID = v_numWarehouseID;
	
   UPDATE VendorShipmentMethod SET bitPrimary = false WHERE numDomainId = v_numDomainID AND numVendorID = v_numVendorid AND numWarehouseID = v_numWarehouseID;

   INSERT INTO VendorShipmentMethod(numDomainId,numVendorID,numAddressID,numWarehouseID,bitPrimary,
		numListItemID,
		numListValue,
		bitPreferredMethod)
   SELECT
   v_numDomainID,v_numVendorid,v_numAddressID,v_numWarehouseID,v_bitPrimary,
		X.numListItemID,
		X.numListValue,
		coalesce(X.bitPreferredMethod,false)
   FROM
    XMLTABLE
			(
				'NewDataSet/ShipmentMethod'
				PASSING 
					CAST(v_str AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numListItemID NUMERIC(9,0) PATH 'numListItemID',
					numListValue INTEGER PATH 'numListValue',
					bitPreferredMethod BOOLEAN PATH 'bitPreferredMethod'
			) AS X;

   RETURN;
END; $$;


