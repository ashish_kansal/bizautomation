-- Stored procedure definition script usp_GetContractDtl for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContractDtl(v_numContractID  BIGINT,                                                  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                                        
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue  VARCHAR(250);
BEGIN
   open SWV_RefCur for select
   fn_GetComapnyName(numDivisionID) as       CompanyName,
c.numContractId,
numDivisionID,
vcContractName,                                 
--case when bitdays=1 then                                                 
--datediff(day,bintStartDate,bintExpDate)                                 
--else                                
--0                                
--end as Days,                                                  
bintStartDate,
FormatedDateFromDate(bintStartDate,numdomainId) AS ExpDate,
bintExpDate,
numincidents,
c.numHours,
vcNotes,
numAmount,
bintcreatedOn,
case when bitHour = true then
      getContractRemainingHrsAmt(0::BOOLEAN,v_numContractID::NUMERIC)
   else 0 end as RemHours,
case when bitAmount = true then
      getContractRemainingHrsAmt(1::BOOLEAN,v_numContractID::NUMERIC)
   else 0 end as RemAmount ,
case when bitDays = true then
      coalesce(CAST(case when
      DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) > 0 then DATE_PART('day',bintExpDate:: timestamp -TIMEZONE('UTC',now()):: timestamp) else CAST('0' as INTEGER) end AS VARCHAR(100)),'0')
   else '0' end as Days,
case when bitIncidents = true then
      numincidents 
   else '0' end as Incidents,
bitAmount,
bitHour,
bitDays,
bitIncidents ,
bitEHour,
bitEDays,
bitEIncidents  ,
numEmailDays ,
numEmailIncidents  ,
numEmailHours,
numTemplateId ,decrate,
(Select CAST(coalesce(sum(monAmount),0) as VARCHAR) From OpportunityBizDocsDetails Where numDomainId = c.numdomainId And numContractId = c.numContractId) As BalanceAmt
   from ContractManagement c
   where c.numdomainId = v_numDomainID and c.numContractId = v_numContractID;
END; $$;












