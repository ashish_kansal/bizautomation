DROP FUNCTION IF EXISTS USP_ElasticSearch_GetEmails;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetEmails(v_numDomainID NUMERIC(18,0)
	,v_numCurrentPage INTEGER
	,v_numPageSize INTEGER
	,v_vcEmailHistoryIds TEXT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numCurrentPage,0) > 0 AND coalesce(v_numPageSize,0) > 0 then
	
      open SWV_RefCur for
      SELECT
      numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS text
			,CONCAT('<b style="color:#efe129">','Email:</b> ',FormatedDateFromDate(dtReceivedOn,v_numDomainID),
      ', From:',coalesce((CASE WHEN(CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END) > 0 THEN SUBSTR(EmailHistory.vcFrom,CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3,
         LENGTH(EmailHistory.vcFrom) -CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3) ELSE '' END),''),
      ', To:',coalesce(TEMPTO.vcTo,''),(CASE WHEN LENGTH(coalesce(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',coalesce(TEMPCC.vcCC,'')) ELSE '' END),
      (CASE WHEN LENGTH(coalesce(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',coalesce(TEMPBCC.vcBCC,'')) ELSE '' END),', ',coalesce(vcSubject,'')) AS displaytext
			,coalesce(vcSubject,'') AS "Search_vcSubject"
			,coalesce(vcBodyText,'') AS "Search_vcBodyText"
			,dtReceivedOn AS "SearchDate_dtReceivedOn"
			,(CASE WHEN(CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END) > 0 THEN SUBSTR(EmailHistory.vcFrom,CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3,
         LENGTH(EmailHistory.vcFrom) -CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3) ELSE '' END) AS "Search_vcFromEmail"
			,coalesce(TEMPTO.vcTo,'') AS "Search_vcToEmail"
			,coalesce(TEMPCC.vcCC,'') AS "Search_vcCCEmail"
			,coalesce(TEMPBCC.vcBCC,'') AS "Search_vcBCCEmail"
			,dtReceivedOn AS "dtReceivedOn"
			,numUserCntId AS "numUserCntId"
      FROM
      EmailHistory
      LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ') FROM DelimitedSplit8K(EmailHistory.vcTo,'#^#')) AS vcTo
		) AS TEMPTO ON TRUE
		LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ') FROM DelimitedSplit8K(EmailHistory.vcCC,'#^#')) AS vcCC
		) AS TEMPCC ON TRUE
		LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ')  FROM DelimitedSplit8K(EmailHistory.vcBCC,'#^#')) AS vcBCC
		) AS TEMPBCC ON TRUE
      WHERE
      numDomainID = v_numDomainID
      AND (NULLIF(v_vcEmailHistoryIds,'') IS NULL OR numEmailHstrID IN(SELECT Id FROM SplitIDs(coalesce(v_vcEmailHistoryIds,'0'),',')))
      ORDER BY
      dtReceivedOn DESC;
   ELSE
      open SWV_RefCur for
      SELECT
      numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS text
			,CONCAT('<b style="color:#efe129">','Email:</b> ',FormatedDateFromDate(dtReceivedOn,v_numDomainID),
      ', From:',coalesce((CASE WHEN(CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END) > 0 THEN SUBSTR(EmailHistory.vcFrom,CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3,
         LENGTH(EmailHistory.vcFrom) -CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3) ELSE '' END),''),
      ', To:',coalesce(TEMPTO.vcTo,''),(CASE WHEN LENGTH(coalesce(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',coalesce(TEMPCC.vcCC,'')) ELSE '' END),
      (CASE WHEN LENGTH(coalesce(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',coalesce(TEMPBCC.vcBCC,'')) ELSE '' END),', ',coalesce(vcSubject,'')) AS displaytext
			,coalesce(vcSubject,'') AS "Search_vcSubject"
			,coalesce(vcBodyText,'') AS "Search_vcBodyText"
			,dtReceivedOn AS "SearchDate_dtReceivedOn"
			,(CASE WHEN(CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END) > 0 THEN SUBSTR(EmailHistory.vcFrom,CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3,
         LENGTH(EmailHistory.vcFrom) -CASE WHEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1))) > 0 THEN POSITION('$^$' IN SUBSTR(LTRIM(RTRIM(EmailHistory.vcFrom)),(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1)))+(POSITION('$^$' IN LTRIM(RTRIM(EmailHistory.vcFrom)))+1) -1 ELSE 0 END+3) ELSE '' END) AS "Search_vcFromEmail"
			,coalesce(TEMPTO.vcTo,'') AS "Search_vcToEmail"
			,coalesce(TEMPCC.vcCC,'') AS "Search_vcCCEmail"
			,coalesce(TEMPBCC.vcBCC,'') AS "Search_vcBCCEmail"
			,dtReceivedOn AS "dtReceivedOn"
			,numUserCntId AS "numUserCntId"
      FROM
      EmailHistory
      LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ') FROM DelimitedSplit8K(EmailHistory.vcTo,'#^#')) AS vcTo
		) AS TEMPTO ON TRUE
		LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ') FROM DelimitedSplit8K(EmailHistory.vcCC,'#^#')) AS vcCC
		) AS TEMPCC ON TRUE
		LEFT JOIN LATERAL
		(
			Select (SELECT string_agg((CASE WHEN strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') > 0 THEN SUBSTRING(item,strpos(LTRIM(RTRIM(item)),'$^$') + strpos(SUBSTRING(LTRIM(RTRIM(item)),POSITION('$^$' IN LTRIM(RTRIM(item)))+1),'$^$') + 3) ELSE '' END),', ')  FROM DelimitedSplit8K(EmailHistory.vcBCC,'#^#')) AS vcBCC
		) AS TEMPBCC ON TRUE
      WHERE
      numDomainID = v_numDomainID
      AND (NULLIF(v_vcEmailHistoryIds,'') IS NULL OR numEmailHstrID IN(SELECT Id FROM SplitIDs(coalesce(v_vcEmailHistoryIds,'0'),',')))
      ORDER BY
      dtReceivedOn DESC;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetAlertIDs]    Script Date: 07/26/2008 16:16:10 ******/



