-- Stored procedure definition script USP_MassSalesFulfillment_GetShippingLabelForPrint for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetShippingLabelForPrint(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   vcShippingLabelImage as "vcShippingLabelImage"
   FROM
   ShippingReport
   INNER JOIN
   ShippingBox
   ON
   ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
   WHERE
   ShippingReport.numDomainID = v_numDomainID
   AND ShippingReport.numOppID = v_numOppID
   AND ShippingReport.numOppBizDocId = v_numOppBizDocID
   AND LENGTH(coalesce(vcShippingLabelImage,'')) > 0;
END; $$;












