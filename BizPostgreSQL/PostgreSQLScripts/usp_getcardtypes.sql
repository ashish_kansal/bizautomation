-- FUNCTION: public.usp_getcardtypes(numeric, smallint, refcursor)

-- DROP FUNCTION public.usp_getcardtypes(numeric, smallint, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcardtypes(
	v_numdomainid numeric,
	v_sinttype smallint DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_numListID  NUMERIC(9,0); 
BEGIN
   v_numListID := 120; --fixed list item for card type
        
   IF v_sintType = 0 then
            
      open SWV_RefCur for
      SELECT DISTINCT
      vcData /* + ' - '
                        + CONVERT(VARCHAR(10), ISNULL([fltTransactionCharge],
                                                      0)) + '--'*/ 
      AS CardType,
                        vcData,
                        numListItemID,
                        coalesce(fltTransactionCharge,0) AS fltTransactionCharge
      FROM    Listdetails
      INNER JOIN COACreditCardCharge ON COACreditCardCharge.numCreditCardTypeId = Listdetails.numListItemID
      AND COACreditCardCharge.numDomainID = v_numDomainID
      WHERE   numListID = v_numListID
      AND COACreditCardCharge.numAccountID > 0
      AND (Listdetails.constFlag = TRUE
      OR Listdetails.numDomainid = v_numDomainID);
   ELSEIF v_sintType = 1
   then
            
      open SWV_RefCur for
      SELECT  numListItemID,
                        vcData /*+ ' - '
                        + CONVERT(VARCHAR(10), ISNULL([fltTransactionCharge],
                                                      0)) + '--'*/ 
      AS CardType
      FROM    Listdetails
      Left JOIN COACreditCardCharge ON COACreditCardCharge.numCreditCardTypeId = Listdetails.numListItemID
      AND COACreditCardCharge.numDomainID = v_numDomainID
      WHERE   numListID = v_numListID
      AND (Listdetails.constFlag = TRUE
      OR Listdetails.numDomainid = v_numDomainID);
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getcardtypes(numeric, smallint, refcursor)
    OWNER TO postgres;
