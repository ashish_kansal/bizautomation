-- Stored procedure definition script USP_GetJournalEntries_Virtual for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetJournalEntries_Virtual(v_numDomainID INTEGER,
      v_vcAccountId VARCHAR(4000),
      v_dtFromDate TIMESTAMP,
      v_dtToDate TIMESTAMP,
      v_vcTranType VARCHAR(50) DEFAULT '',
      v_varDescription VARCHAR(50) DEFAULT '',
      v_CompanyName VARCHAR(50) DEFAULT '',
      v_vcBizPayment VARCHAR(50) DEFAULT '',
      v_vcCheqNo VARCHAR(50) DEFAULT '',
      v_vcBizDocID VARCHAR(50) DEFAULT '',
      v_vcTranRef VARCHAR(50) DEFAULT '',
      v_vcTranDesc VARCHAR(50) DEFAULT '',
	  v_numDivisionID NUMERIC(9,0) DEFAULT 0,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Chintan to enable calculation of date according to client machine
      v_charReconFilter CHAR(1) DEFAULT '',
	  v_tintMode SMALLINT DEFAULT 0,
      v_numItemID NUMERIC(9,0) DEFAULT 0,
      v_CurrentPage INTEGER DEFAULT 0,
	  v_PageSize INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAccountIDs  TEXT;
   v_dtFinFromDate  TIMESTAMP;
   v_dtFinYearFromJournal  TIMESTAMP;
   v_dtFinYearFrom  TIMESTAMP;
   v_numMaxFinYearID  NUMERIC;
   v_Month  INTEGER;
   v_year  INTEGER;
   v_first_id  NUMERIC;
   v_startRow  INTEGER;
   v_CurrRecord  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   select   COALESCE(coalesce(v_vcAccountIDs,'') || ',','') || SUBSTR(CAST(COA.numAccountId AS VARCHAR(20)),1,20) INTO v_vcAccountIDs FROM Chart_Of_Accounts AS COA WHERE COA.numDomainId = v_numDomainID;
   RAISE NOTICE '%',v_vcAccountIDs;
   v_vcAccountId := v_vcAccountIDs;

   SELECT   dtPeriodFrom INTO v_dtFinFromDate FROM     FinancialYear WHERE    dtPeriodFrom <= v_dtFromDate
   AND dtPeriodTo >= v_dtFromDate
   AND numDomainId = v_numDomainID;

   RAISE NOTICE '%',v_dtFromDate;
   RAISE NOTICE '%',v_dtToDate;

   select   MIN(datEntry_Date) INTO v_dtFinYearFromJournal FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainID; 
   SELECT   dtPeriodFrom INTO v_dtFinYearFrom FROM     FinancialYear WHERE    dtPeriodFrom <= v_dtFromDate
   AND dtPeriodTo >= v_dtFromDate
   AND numDomainId = v_numDomainID;

   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL AS
      SELECT * FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainID; 

   select   MAX(numFinYearId) INTO v_numMaxFinYearID FROM FinancialYear WHERE numDomainId = v_numDomainID;
   select   EXTRACT(MONTH FROM dtPeriodFrom), EXTRACT(YEAR FROM dtPeriodFrom) INTO v_Month,v_year FROM FinancialYear WHERE numDomainId = v_numDomainID AND numFinYearId = v_numMaxFinYearID;
   RAISE NOTICE 'MONTH:%',SUBSTR(CAST(v_Month AS VARCHAR(100)),1,100);
   RAISE NOTICE 'YEAR:%',SUBSTR(CAST(v_year AS VARCHAR(100)),1,100);
		 
   if v_tintMode = 0 then

      DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP1 AS
         SELECT  COA1.numAccountId  /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
         FROM    Chart_Of_Accounts COA1
         WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcAccountId,','))
         AND COA1.numDomainId = v_numDomainID;
      v_vcAccountId := coalesce(OVERLAY((SELECT ',' || CAST(numAccountID AS VARCHAR(10)) FROM tt_TEMP1) placing '' from 1 for 1),''); 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
      open SWV_RefCur for
      SELECT  v_numDomainID AS "numDomainID"
	  ,numAccountId AS "numAccountId"
	  ,vcAccountName AS "vcAccountName"
	  ,numParntAcntTypeID AS "numParntAcntTypeID"
	  ,vcAccountDescription AS "vcAccountDescription"
	  ,vcAccountCode AS "vcAccountCode"
	  ,dtAsOnDate AS "dtAsOnDate"
	  ,CASE WHEN (Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%') AND(v_year::bigint+v_Month::bigint) =(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN 0
      WHEN(v_year::bigint+v_Month::bigint) >(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t1.OPENING,0)
      WHEN(v_year::bigint+v_Month::bigint) <(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t2.OPENING,0)
      ELSE coalesce(mnOpeningBalance,0)
      END AS "mnOpeningBalance",
							(SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
         FROM
         General_Journal_Details GJD
         JOIN Domain D ON D.numDomainId = GJD.numDomainId AND D.numDomainId = v_numDomainID
         INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
         GJD.numDomainId = v_numDomainID AND
         GJD.numChartAcntId = Table1.numAccountId AND
         GJH.numDomainId = D.numDomainId AND
         GJH.datEntry_Date <= v_dtFinFromDate)
      FROM fn_GetOpeningBalance(v_vcAccountId,v_numDomainID,v_dtFromDate,v_ClientTimeZoneOffset) Table1
      LEFT JOIN LATERAL(SELECT   SUM(Debit -Credit) AS OPENING
         FROM     tt_VIEW_JOURNAL VJ
         WHERE    VJ.numDomainId = v_numDomainID
         AND VJ.numaccountid = Table1.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
         AND datentry_date BETWEEN(CASE WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '-1 minute') AS t1
      LEFT JOIN LATERAL(SELECT   SUM(Debit -Credit) AS OPENING
         FROM     tt_VIEW_JOURNAL VJ
         WHERE    VJ.numDomainId = v_numDomainID
         AND VJ.numaccountid = Table1.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
         AND datentry_date BETWEEN(CASE WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '1 minute') AS t2 on TRUE on TRUE;

      open SWV_RefCur2 for
      SELECT * FROM(SELECT 
         General_Journal_Details.numDomainId AS "numDomainId",
 TABLE1.numAccountId AS "numAccountId",
General_Journal_Header.numJOurnal_Id AS "numJOurnal_Id",
FormatedDateFromDate(General_Journal_Header.datEntry_Date,General_Journal_Details.numDomainId) AS "Date",
General_Journal_Header.varDescription AS "varDescription",
CompanyInfo.vcCompanyName AS "CompanyName",
General_Journal_Details.numTransactionId AS "numTransactionId",
coalesce(coalesce(coalesce(VIEW_BIZPAYMENT.Narration,General_Journal_Header.varDescription),'') || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
         ELSE  VIEW_JOURNALCHEQ.Narration
         END),
         '') as "Narration",
coalesce(General_Journal_Details.vcReference,'') AS "TranRef",
coalesce(General_Journal_Details.varDescription,'') AS "TranDesc",
CAST(coalesce(GJD.numDebitAmt,0) AS VARCHAR(20)) AS "numDebitAmt",
CAST(coalesce(General_Journal_Details.numDebitAmt,0) AS VARCHAR(20)) AS "numDebitAmt_General_Journal_Details",
CAST(coalesce(General_Journal_Details.numCreditAmt,0) AS VARCHAR(20)) AS "numCreditAmt",
CAST(coalesce(GJD.numCreditAmt,0) AS VARCHAR(20)) AS "numCreditAmt_GJD",
CASE WHEN coalesce(General_Journal_Details.numDebitAmt,0) = 0 THEN TABLE1.vcAccountName ELSE '' END AS "vcAccountName",
CASE WHEN coalesce(GJD.numCreditAmt,0) = 0 THEN TABLE2.vcAccountName ELSE '' END AS "vcSplitAccountName",
'' as "balance",
CASE
         WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 'Checks'
         WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
         WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
         OpportunityMaster.tintopptype = 1 THEN 'BizDocs Invoice'
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
         OpportunityMaster.tintopptype = 2 THEN 'BizDocs Purchase'
         WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
         WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
         WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN 'Bill'
         WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 'Pay Bill'
         WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN
            CASE
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
            WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
            WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
            WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund Against Credit Memo'
            END
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
         WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 'Journal'
         END AS "TransactionType",
coalesce(General_Journal_Details.bitCleared,false) AS "bitCleared",
coalesce(General_Journal_Details.bitReconcile,false) AS "bitReconcile",
coalesce(General_Journal_Details.numItemID,0) AS "numItemID",
Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS "numLandedCostOppId",
General_Journal_Details.numEntryDateSortOrder1 AS "numEntryDateSortOrder1"
         FROM General_Journal_Header
         INNER JOIN General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId
         INNER JOIN General_Journal_Details GJD ON General_Journal_Header.numJOurnal_Id = GJD.numJournalId
         INNER JOIN Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId AND General_Journal_Details.numChartAcntId <> GJD.numChartAcntId
         LEFT JOIN LATERAL(SELECT vcAccountName,COA.numAccountId FROM Chart_Of_Accounts AS COA
            WHERE Chart_Of_Accounts.numAccountId = COA.numAccountId
            AND COA.numDomainId = General_Journal_Header.numDomainId
            AND dbo.General_Journal_Details.numChartAcntId = COA.numAccountId) TABLE1
         LEFT JOIN LATERAL(SELECT vcAccountName,COA1.numAccountId FROM Chart_Of_Accounts AS COA1
            WHERE Chart_Of_Accounts.numAccountId <> COA1.numAccountId
            AND COA1.numDomainId = General_Journal_Header.numDomainId
            AND GJD.numChartAcntId = COA1.numAccountId) TABLE2
         LEFT OUTER JOIN timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID
         LEFT OUTER JOIN DivisionMaster ON General_Journal_Details.numCustomerId = DivisionMaster.numDivisionID and
         General_Journal_Details.numDomainId = DivisionMaster.numDomainID
         LEFT OUTER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         LEFT OUTER JOIN OpportunityMaster ON General_Journal_Header.numOppId = OpportunityMaster.numOppId LEFT OUTER JOIN
         OpportunityBizDocs ON General_Journal_Header.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
         VIEW_JOURNALCHEQ ON (General_Journal_Header.numCheckHeaderID = VIEW_JOURNALCHEQ.numCheckHeaderID
         or (General_Journal_Header.numBillPaymentID = VIEW_JOURNALCHEQ.numReferenceID and VIEW_JOURNALCHEQ.tintReferenceType = 8)) LEFT OUTER JOIN
         VIEW_BIZPAYMENT ON General_Journal_Header.numBizDocsPaymentDetId = VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
         DepositMaster DM ON DM.numDepositId = General_Journal_Header.numDepositId  LEFT OUTER JOIN
         ReturnHeader RH ON RH.numReturnHeaderID = General_Journal_Header.numReturnID LEFT OUTER JOIN
         CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID AND CH.tintReferenceType = 10
         LEFT OUTER JOIN BillHeader BH  ON coalesce(General_Journal_Header.numBillID,0) = BH.numBillID on TRUE on TRUE
         WHERE   General_Journal_Header.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate -- Removed Tmezone bug id 1029 
         AND General_Journal_Header.numDomainId = v_numDomainID
         AND (DivisionMaster.numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
         AND (General_Journal_Details.bitCleared =(CASE v_charReconFilter WHEN 'C' THEN 1 ELSE 0 END)  OR RTRIM(v_charReconFilter) = 'A')
         AND (General_Journal_Details.bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN 1 ELSE 0 END)  OR RTRIM(v_charReconFilter) = 'A')
         AND TABLE1.numAccountId IN(SELECT numAccountID FROM tt_TEMP1)
         AND numJOurnal_Id = 181544 LIMIT 1) TABLE1 WHERE coalesce(TransactionType,'') = 'Journal'
      ORDER BY TABLE1.numEntryDateSortOrder1 ASC;
   ELSEIF v_tintMode = 1
   then


--SELECT @vcAccountID

      open SWV_RefCur for
      SELECT  v_numDomainID AS "numDomainID"
	  ,numAccountId AS "numAccountId"
	  ,vcAccountName AS "vcAccountName"
	  ,numParntAcntTypeID AS "numParntAcntTypeID"
	  ,vcAccountDescription AS "vcAccountDescription"
	  ,vcAccountCode AS "vcAccountCode"
	  ,dtAsOnDate AS "dtAsOnDate"
	  ,CASE WHEN (Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%') AND(v_year::bigint+v_Month::bigint) =(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN 0
      WHEN(v_year::bigint+v_Month::bigint) >(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t1.OPENING,0)
      WHEN(v_year::bigint+v_Month::bigint) <(EXTRACT(YEAR FROM v_dtFromDate)+EXTRACT(MONTH FROM v_dtFromDate)) THEN coalesce(t2.OPENING,0)
      ELSE coalesce(mnOpeningBalance,0)
      END  AS "mnOpeningBalance"
	  ,(SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
         FROM
         General_Journal_Details GJD
         JOIN Domain D ON D.numDomainId = GJD.numDomainId AND D.numDomainId = v_numDomainID
         INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
         GJD.numDomainId = v_numDomainID AND
         GJD.numChartAcntId = Table1.numAccountId AND
         GJH.numDomainId = D.numDomainId AND
         GJH.datEntry_Date <= v_dtFinFromDate)
      FROM fn_GetOpeningBalance(v_vcAccountId,v_numDomainID,v_dtFromDate,v_ClientTimeZoneOffset) Table1
      LEFT JOIN LATERAL(SELECT   SUM(Debit -Credit) AS OPENING
         FROM     tt_VIEW_JOURNAL VJ
         WHERE    VJ.numDomainId = v_numDomainID
         AND VJ.numaccountid = Table1.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
         AND datentry_date BETWEEN(CASE WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '-1 minute') AS t1
      LEFT JOIN LATERAL(SELECT   SUM(Debit -Credit) AS OPENING
         FROM     tt_VIEW_JOURNAL VJ
         WHERE    VJ.numDomainId = v_numDomainID
         AND VJ.numaccountid = Table1.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
         AND datentry_date BETWEEN(CASE WHEN Table1.vcAccountCode ilike '0103%' OR Table1.vcAccountCode ilike '0104%' OR Table1.vcAccountCode ilike '0106%'
         THEN v_dtFinYearFrom
         ELSE v_dtFinYearFromJournal
         END) AND  v_dtFromDate+INTERVAL '1 minute') AS t2 on TRUE on TRUE;

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

      v_CurrRecord :=((v_CurrentPage::bigint -1)*v_PageSize::bigint)+1;
      RAISE NOTICE '%',v_CurrRecord;

      select   General_Journal_Details.numEntryDateSortOrder1 INTO v_first_id FROM General_Journal_Header
      INNER JOIN General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId
      INNER JOIN Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId
      LEFT OUTER JOIN timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID WHERE General_Journal_Header.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
      AND General_Journal_Header.numDomainId = v_numDomainID
      AND General_Journal_Details.numDomainId = v_numDomainID
      AND Chart_Of_Accounts.numDomainId = v_numDomainID
      AND (bitCleared =(CASE v_charReconFilter WHEN 'C' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND (bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A') 
--AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT [SS].[OutParam] FROM [dbo].[SplitString](@vcAccountId,',') AS SS))
      AND (General_Journal_Details.numItemID = v_numItemID OR v_numItemID = 0)
      AND General_Journal_Details.numEntryDateSortOrder1 > 0   ORDER BY General_Journal_Details.numEntryDateSortOrder1 asc;
      RAISE NOTICE '%',v_first_id;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      RAISE NOTICE '%',SWV_RowCount;

		open SWV_RefCur2 for
		SELECT 
			numDomainId AS "numDomainId"
			,numAccountId AS "numAccountId"
			,numJOurnal_Id AS "numJOurnal_Id"
			,Date AS "Date"
			,varDescription AS "varDescription"
			,CompanyName AS "CompanyName"
			,numTransactionId AS "numTransactionId"
			,Narration AS "Narration"
			,TranRef AS "TranRef"
			,TranDesc AS "TranDesc"
			,numDebitAmt AS "numDebitAmt"
			,numDebitAmt_General_Journal_Details AS "numDebitAmt_General_Journal_Details"
			,numCreditAmt AS "numCreditAmt"
			,numCreditAmt_GJD AS "numCreditAmt_GJD"
			,vcAccountName AS "vcAccountName"
			,vcSplitAccountName AS "vcSplitAccountName"
			,balance AS "balance"
			,TransactionType AS "TransactionType"
			,bitCleared AS "bitCleared"
			,bitReconcile AS "bitReconcile"
			,numItemID AS "numItemID"
			,numEntryDateSortOrder AS "numEntryDateSortOrder"
			,"numLandedCostOppId" AS "numLandedCostOppId"
			,numEntryDateSortOrder1 AS "numEntryDateSortOrder1"
		FROM
		(
			SELECT 
				General_Journal_Details.numDomainId
				,TABLE1.numAccountId
				,General_Journal_Header.numJOurnal_Id
				,FormatedDateFromDate(General_Journal_Header.datEntry_Date,General_Journal_Details.numDomainId) AS Date
				,General_Journal_Header.varDescription
				,CompanyInfo.vcCompanyName AS CompanyName
				,General_Journal_Details.numTransactionId
				,coalesce(coalesce(coalesce(VIEW_BIZPAYMENT.Narration,General_Journal_Header.varDescription),'') || ' ' ||(CASE WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN ' Chek No: ' || coalesce(CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq')
				 ELSE  VIEW_JOURNALCHEQ.Narration
				 END),'') as Narration,
				coalesce(General_Journal_Details.vcReference,'') AS TranRef,
				coalesce(General_Journal_Details.varDescription,'') AS TranDesc,
				CAST(coalesce(GJD.numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt,
				CAST(coalesce(General_Journal_Details.numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt_General_Journal_Details,
				CAST(coalesce(General_Journal_Details.numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt,
				CAST(coalesce(GJD.numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt_GJD,
				CASE WHEN coalesce(General_Journal_Details.numDebitAmt,0) = 0 THEN TABLE1.vcAccountName ELSE '' END AS vcAccountName,
				CASE WHEN coalesce(GJD.numCreditAmt,0) = 0 THEN TABLE2.vcAccountName ELSE '' END AS vcSplitAccountName,
				'' as balance,
CASE
         WHEN coalesce(General_Journal_Header.numCheckHeaderID,0) <> 0 THEN 'Checks'
         WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
         WHEN coalesce(General_Journal_Header.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
         OpportunityMaster.tintopptype = 1 THEN 'BizDocs Invoice'
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) <> 0 AND coalesce(General_Journal_Header.numBizDocsPaymentDetId,0) = 0 AND
         OpportunityMaster.tintopptype = 2 THEN 'BizDocs Purchase'
         WHEN coalesce(General_Journal_Header.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
         WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
         WHEN coalesce(General_Journal_Header.numBillID,0) <> 0 THEN 'Bill'
         WHEN coalesce(General_Journal_Header.numBillPaymentID,0) <> 0 THEN 'Pay Bill'
         WHEN coalesce(General_Journal_Header.numReturnID,0) <> 0 THEN
            CASE
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
            WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
            WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
            WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund Against Credit Memo'
            END
         WHEN coalesce(General_Journal_Header.numOppId,0) <> 0 AND coalesce(General_Journal_Header.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
         WHEN General_Journal_Header.numJOurnal_Id <> 0 THEN 'Journal'
         END AS TransactionType,
coalesce(General_Journal_Details.bitCleared,false) AS bitCleared,
coalesce(General_Journal_Details.bitReconcile,false) AS bitReconcile,
coalesce(General_Journal_Details.numItemID,0) AS numItemID,
General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS numLandedCostOppId,
General_Journal_Details.numEntryDateSortOrder1
         FROM General_Journal_Header
         LEFT JOIN General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId
         LEFT JOIN General_Journal_Details GJD ON General_Journal_Header.numJOurnal_Id = GJD.numJournalId
         INNER JOIN Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId AND General_Journal_Details.numChartAcntId <> GJD.numChartAcntId
         LEFT JOIN LATERAL(SELECT vcAccountName,COA.numAccountId FROM Chart_Of_Accounts AS COA
            WHERE Chart_Of_Accounts.numAccountId = COA.numAccountId
            AND COA.numDomainId = General_Journal_Header.numDomainId
            AND dbo.General_Journal_Details.numChartAcntId = COA.numAccountId) TABLE1
         LEFT JOIN LATERAL(SELECT vcAccountName,COA1.numAccountId FROM Chart_Of_Accounts AS COA1
            WHERE Chart_Of_Accounts.numAccountId <> COA1.numAccountId
            AND COA1.numDomainId = General_Journal_Header.numDomainId
            AND GJD.numChartAcntId = COA1.numAccountId) TABLE2
         LEFT OUTER JOIN timeandexpense ON General_Journal_Header.numCategoryHDRID = timeandexpense.numCategoryHDRID
         LEFT OUTER JOIN DivisionMaster ON General_Journal_Details.numCustomerId = DivisionMaster.numDivisionID and
         General_Journal_Details.numDomainId = DivisionMaster.numDomainID
         LEFT OUTER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         LEFT OUTER JOIN OpportunityMaster ON General_Journal_Header.numOppId = OpportunityMaster.numOppId LEFT OUTER JOIN
         OpportunityBizDocs ON General_Journal_Header.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
         VIEW_JOURNALCHEQ ON (General_Journal_Header.numCheckHeaderID = VIEW_JOURNALCHEQ.numCheckHeaderID
         or (General_Journal_Header.numBillPaymentID = VIEW_JOURNALCHEQ.numReferenceID and VIEW_JOURNALCHEQ.tintReferenceType = 8)) LEFT OUTER JOIN
         VIEW_BIZPAYMENT ON General_Journal_Header.numBizDocsPaymentDetId = VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
         DepositMaster DM ON DM.numDepositId = General_Journal_Header.numDepositId  LEFT OUTER JOIN
         ReturnHeader RH ON RH.numReturnHeaderID = General_Journal_Header.numReturnID LEFT OUTER JOIN
         CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID AND CH.tintReferenceType = 10
         LEFT OUTER JOIN BillHeader BH  ON coalesce(General_Journal_Header.numBillID,0) = BH.numBillID on TRUE on TRUE
         WHERE   General_Journal_Header.datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate -- Removed Tmezone bug id 1029 
         AND General_Journal_Header.numDomainId = v_numDomainID
         AND (DivisionMaster.numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
         AND (General_Journal_Details.bitCleared =(CASE v_charReconFilter WHEN 'C' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
         AND (General_Journal_Details.bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A') 
         AND (General_Journal_Details.numItemID = v_numItemID OR v_numItemID = 0)
         AND numJOurnal_Id = 181544
         AND General_Journal_Details.numEntryDateSortOrder1  >= v_first_id) TABLE1 WHERE coalesce(TransactionType,'') = 'Journal'
      ORDER BY TABLE1.numEntryDateSortOrder1 ASC;
   end if;
	
   RETURN;
END; $$;
