-- Stored procedure definition script USP_ManageShippingRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingRule(v_numRuleID NUMERIC(9,0),
	v_vcRuleName VARCHAR(50),
	v_vcDescription VARCHAR(1000),
	v_tintBasedOn SMALLINT,
	v_tinShippingMethod SMALLINT,
	v_tintType SMALLINT,
	v_numDomainID NUMERIC(9,0),
	v_tintTaxMode SMALLINT,
	v_tintFixedShippingQuotesMode SMALLINT,
	v_bitFreeshipping BOOLEAN,
	v_FreeShippingAmt INTEGER DEFAULT NULL,
	v_numRelationship NUMERIC(9,0) DEFAULT NULL,
	v_numProfile NUMERIC(9,0) DEFAULT NULL,
	v_bitItemClassification BOOLEAN DEFAULT NULL,
	v_numSiteId NUMERIC(18,0) DEFAULT NULL,
	v_strWarehouseIds VARCHAR(1000) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM ShippingRules WHERE vcRuleName = v_vcRuleName AND numDomainId = v_numDomainID AND numRuleID <> v_numRuleID) > 0 then
	
      RAISE EXCEPTION 'DUPLICATE';
      RETURN;
   end if;

   IF coalesce(v_numRuleID,0) = 0 then
    
      INSERT INTO ShippingRules(vcRuleName
            ,vcDescription
            ,tintBasedOn
            ,tinShippingMethod
            ,tintIncludeType
            ,numDomainId
            ,tintTaxMode
            ,tintFixedShippingQuotesMode
			,bitFreeShipping
			,FreeShippingOrderAmt
			,numRelationship
			,numProfile
			,numWareHouseID
			,bitItemClassification
			,numSiteID)
        VALUES(v_vcRuleName
            ,v_vcDescription
            ,v_tintBasedOn
            ,v_tinShippingMethod
            ,v_tintType
            ,v_numDomainID
            ,v_tintTaxMode
            ,v_tintFixedShippingQuotesMode
			,v_bitFreeshipping
			,CASE WHEN v_bitFreeshipping = false THEN NULL ELSE v_FreeShippingAmt END
			,v_numRelationship
			,v_numProfile
			,v_strWarehouseIds
			,v_bitItemClassification
			,v_numSiteId);
        
      open SWV_RefCur for
      SELECT CURRVAL('ShippingRules_seq') AS numRuleID;
   ELSE
      IF(SELECT
      COUNT(*)
      FROM
      ShippingRules SR
      WHERE
      numDomainId = v_numDomainID
      AND numRuleID <> coalesce(v_numRuleID,0)
      AND coalesce(numRelationship,0) = coalesce(v_numRelationship,0)
      AND coalesce(numProfile,0) = coalesce(v_numProfile,0)
      AND EXISTS(SELECT
         SRSL1.numRuleID
         FROM
         ShippingRuleStateList SRSL1
         INNER JOIN
         ShippingRuleStateList SRSL2
         ON
         coalesce(SRSL1.numStateID,0) = coalesce(SRSL2.numStateID,0)
         AND coalesce(SRSL1.vcZipPostal,'') = coalesce(SRSL2.vcZipPostal,'')
         AND coalesce(SRSL1.numCountryID,0) = coalesce(SRSL2.numCountryID,0)
         WHERE
         SRSL1.numDomainID = v_numDomainID
         AND SRSL1.numRuleID = SR.numRuleID
         AND SRSL2.numRuleID = v_numRuleID)) > 0 then
		
         RAISE EXCEPTION 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP';
         RETURN;
      end if;
      UPDATE
      ShippingRules
      SET
      vcRuleName = v_vcRuleName,vcDescription = v_vcDescription,tintBasedOn = v_tintBasedOn,
      tinShippingMethod = v_tinShippingMethod,tintIncludeType = v_tintType,
      tintTaxMode = v_tintTaxMode,tintFixedShippingQuotesMode = v_tintFixedShippingQuotesMode,
      bitFreeShipping = v_bitFreeshipping,FreeShippingOrderAmt =(CASE WHEN v_bitFreeshipping = false THEN NULL Else v_FreeShippingAmt End),
      numRelationship = v_numRelationship,numProfile = v_numProfile,
      numWareHouseID = v_strWarehouseIds,bitItemClassification = v_bitItemClassification,
      numSiteID = v_numSiteId
      WHERE
      numRuleID = v_numRuleID
      AND numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT  v_numRuleID;
   end if;
END; $$;


