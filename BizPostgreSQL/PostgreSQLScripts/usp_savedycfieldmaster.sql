-- Stored procedure definition script USP_saveDycFieldMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_saveDycFieldMaster(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numModuleId NUMERIC(9,0) DEFAULT 0,  
v_numFieldId NUMERIC(9,0) DEFAULT 0,  
v_vcFieldName VARCHAR(50) DEFAULT 0,
v_vcToolTip VARCHAR(1000) DEFAULT NULL,
v_numCultureID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from DycField_Globalization where numDomainID = v_numDomainID and numModuleID = v_numModuleId and numFieldID = v_numFieldId and numCultureID = v_numCultureID) then
	   
      UPDATE DycField_Globalization SET vcNewFieldName = v_vcFieldName,vcToolTip = v_vcToolTip
      where numDomainID = v_numDomainID and numModuleID = v_numModuleId and numFieldID = v_numFieldId and numCultureID = v_numCultureID;
   ELSE
      insert into DycField_Globalization(numModuleID,numFieldID,numDomainID,vcNewFieldName,vcToolTip,numCultureID)
			 	values(v_numModuleId,v_numFieldId,v_numDomainID,v_vcFieldName,v_vcToolTip,v_numCultureID);
   end if;
   RETURN;
END; $$;
    


