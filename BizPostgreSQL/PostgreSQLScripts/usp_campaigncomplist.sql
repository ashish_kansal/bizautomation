-- Stored procedure definition script USP_CampaignCompList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CampaignCompList(v_numCampaignid NUMERIC(9,0),
    v_tintComptype INTEGER,
    INOUT v_NoofRecds INTEGER ,
	v_dtStartDate TIMESTAMP,
    v_dtEndDate   TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql 
--    declare @intLaunchDate as varchar(20)        
--    declare @intEndDate as varchar(20) 
--DECLARE @vcLandingPage VARCHAR(200)
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(9,0);
   v_bitOnline  BOOLEAN;

   v_strSql  VARCHAR(4000);
BEGIN

--            @intLaunchDate = intLaunchDate,
--            @intEndDate = intEndDate,
            
--@vcLandingPage=vcLandingPage,
   select   bitIsOnline, numDomainID INTO v_bitOnline,v_numDomainID from    CampaignMaster where   numCampaignID = v_numCampaignid    LIMIT 1;    


   v_strSql :=
   '
select  CMP.numCompanyId,DM.numDivisionID,ADC.numContactId,DM.tintCRMType,CMP.vcCompanyName as CompanyName,           
		 ADC.vcLastName || '' '' || ADC.vcFirstName as PrimaryContact,          
		 coalesce(LD2.vcData,''-'') as Follow,          
		 GetListIemName(CMP.numNoOfEmployeesId) AS numNoOfEmployeesId,
		FormatedDateFromDate(DM.bintCreatedDate,DM.numDomainID) as bintCreatedDate,
		CMP.txtComments as OrganizationComments       
		FROM  CompanyInfo CMP        
		join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId           
		join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID        
		left join ListDetails LD2 on LD2.numListItemID = DM.numFollowUpStatus        
	  WHERE         
	 coalesce(ADC.bitPrimaryContact,false) = true
	 and DM.numCampaignID = ' || SUBSTR(CAST(v_numCampaignid AS VARCHAR(10)),1,10) || '
	 and DM.bintCreatedDate between ''' || SUBSTR(CAST(v_dtStartDate AS VARCHAR(25)),1,25) || ''' And ''' || SUBSTR(CAST(v_dtEndDate AS VARCHAR(25)),1,25) || '''';
    
--    if @tintComptype = 4 -- All company Types        
--        begin        
--        end        
   if v_tintComptype < 4 then
        
      v_strSql := coalesce(v_strSql,'') || ' and DM.tintCRMType =' || SUBSTR(CAST(v_tintComptype AS VARCHAR(10)),1,10);
   end if;        
      
   if v_tintComptype > 4 then
        
      v_strSql := coalesce(v_strSql,'') || ' and CMP.numCompanyType =' || SUBSTR(CAST(v_tintComptype AS VARCHAR(10)),1,10);
   end if;

   RAISE NOTICE '%',v_strSql;
   OPEN SWV_RefCur FOR EXECUTE v_strSql;

   RETURN;
END; $$;       




