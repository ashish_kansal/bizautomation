CREATE OR REPLACE FUNCTION usp_GetEcosystemReport
(                    
	v_numDomainID NUMERIC(18,0),                           
	v_numDivisionID NUMERIC(18,0),                
	v_numUserCntID NUMERIC DEFAULT 0,                 
	v_intType NUMERIC DEFAULT 0,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
DECLARE
	v_strSql TEXT;         
	v_strFields TEXT;      
	v_entity1 TEXT;      
	v_entity2 TEXT;
BEGIN
	v_strFields := fn_GetReortFieldsData();      
      
	v_entity1 := replace(v_strFields,'CM','CM1');      
	v_entity1 := replace(v_entity1,'DM','DM1');      
	v_entity2 := replace(v_strFields,'CM','CM2');      
	v_entity2 := replace(v_entity2,'DM','DM2');      
      
	v_strSql := 'select 
					CM1.numCompanyId as numCompanyID1
					,DM1.numDivisionID as numDivisionID1
					,fn_GetPrimaryContact(DM1.numDivisionID) as ContactID1
					,DM1.tintCRMType as tintCRMType1
					,CM2.numCompanyId as numCompanyID2
					,DM2.numDivisionID as numDivisionID2
					,fn_GetPrimaryContact(DM2.numDivisionID) as ContactID2
					,DM2.tintCRMType as tintCRMType2
					,CM1.vcCompanyName || '', '' || DM1.vcDivisionName as Entity1
					,CM2.vcCompanyName || '', '' || DM2.vcDivisionName as Entity2
					,fn_GetListItemName(numReferralType)  as Type
					,' || coalesce(v_entity1,'') || ' as SelectedData1
					,' || coalesce(v_entity2,'') || ' as SelectedData2      
				from CompanyAssociations Ass      
				join DivisionMaster DM1      
				on DM1.numDivisionID = Ass.numAssociateFromDivisionID      
				join CompanyInfo CM1       
				on CM1.numCompanyId = DM1.numCompanyID      
				join DivisionMaster DM2      
				on DM2.numDivisionID = Ass.numDivisionID      
				join CompanyInfo CM2       
				on CM2.numCompanyId = DM2.numCompanyID      
				where DM1.numDomainID = DM2.numDomainID and DM1.numDomainID =' || coalesce(v_numDomainID,0) || ' and  (numAssociateFromDivisionID =' || coalesce(v_numDivisionID,0) || ' or Ass.numDivisionID =' || coalesce(v_numDivisionID,0) || ')';     
    

	RAISE NOTICE '%',(v_strSql);       
	OPEN SWV_RefCur FOR EXECUTE v_strSql;
	RETURN;
END; $$;


