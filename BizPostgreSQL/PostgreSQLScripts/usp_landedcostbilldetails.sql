-- Stored procedure definition script USP_LandedCostBillDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LandedCostBillDetails(v_numDomainID NUMERIC,
    v_numOppID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT  BH.numBillID,
            BH.numDivisionId,
            BH.dtBillDate,
            BH.numTermsID,
            BH.dtDueDate,
            BH.monAmountDue,
            BH.monAmtPaid,
            BH.bitIsPaid,
            BH.vcMemo,
            BH.vcReference,
			BD.numBillDetailID,
            BD.numExpenseAccountID,
            coalesce(GJD.numTransactionId,0) AS numTransactionId_Bill,
            coalesce(GJDD.numTransactionId,0) AS numTransactionId_BillDetail,
			BH.dtBillDate,(SELECT BillingTerms.numTermsID || '~' || BillingTerms.numNetDueInDays FROM BillingTerms WHERE numTermsID = BH.numTermsID AND numDomainID = BH.numDomainId)  AS vcTermName,BH.dtDueDate,
			OM.vcLanedCost
   FROM    BillHeader BH
   JOIN BillDetails AS BD ON BH.numBillID = BD.numBillID
   JOIN OpportunityMaster AS OM ON BH.numOppId = OM.numOppId
   LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType = 3 AND GJD.numReferenceID = BH.numBillID
   LEFT OUTER JOIN General_Journal_Details GJDD ON GJDD.tintReferenceType = 4 AND GJDD.numReferenceID = BD.numBillDetailID
   WHERE  BH.numDomainId = v_numDomainID AND BH.numOppId = v_numOppID AND coalesce(BH.bitLandedCost,false) = true;
END; $$;
            












