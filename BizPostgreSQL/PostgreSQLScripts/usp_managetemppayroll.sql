-- Stored procedure definition script USP_ManageTEMPPayroll for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTEMPPayroll()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppBizDocId  NUMERIC(18,0);
   v_numcontactid  NUMERIC(9,0);
   v_numComission  NUMERIC(19,4);
   v_numCOmissionID  NUMERIC(9,0);
   v_vcContactName  VARCHAR(250);
   v_numJournalId  NUMERIC(18,0);

   v_numAPAccount  NUMERIC(9,0);
   v_numPayExpAccount  NUMERIC(9,0);

   v_monTotal  NUMERIC(19,4);
   BizDocPay_Cursor CURSOR FOR

   SELECT OB.numOppBizDocsId FROM OpportunityMaster OM inner join OpportunityBizDocs OB
   on OM.numOppId = OB.numoppid
   where OM.tintopptype = 1 and OM.numDomainId = 110 and OB.bitAuthoritativeBizDocs = 1 and 
   numOppBizDocsId not in(select numOppBizDocId from BizDocComission);
   TempUser CURSOR FOR
		
   select * from tt_TEMPUSERCOMISSION1;

   SWV_numcontactid NUMERIC(9,0);
   SWV_numComission NUMERIC(19,4);
   SWV_numCOmissionID NUMERIC(9,0);
   SWV_vcContactName VARCHAR(250);
   SWV_RCur refcursor;
BEGIN
   select   AC.numAccountID INTO v_numAPAccount from AccountingCharges AC inner join
   AccountingChargeTypes AT
   on AC.numChargeTypeId = AT.numChargeTypeId and
   AT.chChargeCode = 'PL' and AC.numDomainID = 110;

   select   AC.numAccountID INTO v_numPayExpAccount from AccountingCharges AC inner join
   AccountingChargeTypes AT
   on AC.numChargeTypeId = AT.numChargeTypeId and
   AT.chChargeCode = 'EP' and AC.numDomainID = 110;				


   DROP TABLE IF EXISTS tt_TEMPUSERCOMISSION1 CASCADE;
   create TEMPORARY TABLE tt_TEMPUSERCOMISSION1
   (
      numcontactid NUMERIC(9,0),
      numComission NUMERIC(19,4),
      numCOmissionID NUMERIC(9,0),
      vcContactName  VARCHAR(250)
   );
    
   DROP TABLE IF EXISTS tt_TEMPJOURNAL CASCADE;
   create TEMPORARY TABLE tt_TEMPJOURNAL
   (
      numJournalId NUMERIC(9,0)
   );


   OPEN BizDocPay_Cursor;



   FETCH NEXT FROM BizDocPay_Cursor into v_numOppBizDocId;
   WHILE FOUND LOOP
      delete from tt_TEMPUSERCOMISSION1;
      SELECT GetCommissionAmountOfBizDoc(110,v_numOppBizDocId,'SWV_RCur') INTO SWV_RCur;
      FETCH SWV_RCur INTO SWV_numcontactid,SWV_numComission,SWV_numCOmissionID,SWV_vcContactName;
      WHILE FOUND LOOP
         insert into tt_TEMPUSERCOMISSION1  VALUES(SWV_numcontactid, SWV_numComission, SWV_numCOmissionID, SWV_vcContactName);

         FETCH SWV_RCur INTO SWV_numcontactid,SWV_numComission,SWV_numCOmissionID,SWV_vcContactName;
      END LOOP;
      CLOSE SWV_RCur;
      delete from tt_TEMPJOURNAL;
      v_monTotal := CAST(0 AS NUMERIC(19,4));
      open TempUser;
      select   coalesce(SUM(numComission),cast(0 as NUMERIC(19,4))) INTO v_numComission from  tt_TEMPUSERCOMISSION1;
      if v_numComission > 0 then
			
         insert into tt_TEMPJOURNAL  SELECT * FROM USP_InsertJournalEntryHeaderForReceivePayment('01/Apr/2010',v_numComission,0,110,0,85098,0,v_numOppBizDocId,0,0);
         select   numJournalId INTO v_numJournalId from tt_TEMPJOURNAL;
         FETCH NEXT FROM TempUser into v_numcontactid,v_numComission,v_numCOmissionID,v_vcContactName;
         WHILE FOUND LOOP
            insert into General_Journal_Details(numJournalId,
									numDebitAmt,
									numCreditAmt,
									numChartAcntId,
									varDescription,
									numDomainId,
									numCurrencyID,
									fltExchangeRate,
									monOrgDebitAmt,
									monOrgCreditAmt,
									numcontactid)
									values(v_numJournalId,0,CAST(v_numComission AS DOUBLE PRECISION),v_numAPAccount,
								'Comission Expenses - ' || coalesce(v_vcContactName, ''),110,0,1,0,v_numComission,v_numCOmissionID);
								
            v_monTotal := v_monTotal+v_numComission;
            FETCH NEXT FROM TempUser into v_numcontactid,v_numComission,v_numCOmissionID,v_vcContactName;
         END LOOP;
         insert into General_Journal_Details(numJournalId,
									numDebitAmt,
									numCreditAmt,
									numChartAcntId,
									varDescription,
									numDomainId,
									numCurrencyID,
									fltExchangeRate,
									monOrgDebitAmt,
									monOrgCreditAmt,
									numcontactid)
									values(v_numJournalId,CAST(v_monTotal AS DOUBLE PRECISION),0,v_numPayExpAccount,
								'Comission Expenses',110,0,1,v_monTotal,0,v_numCOmissionID);
      end if;
      close TempUser;
				
      FETCH NEXT FROM BizDocPay_Cursor into v_numOppBizDocId;
   END LOOP;

   CLOSE BizDocPay_Cursor;
   RETURN;
END; $$;

