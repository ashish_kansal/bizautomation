CREATE OR REPLACE FUNCTION usp_GetUserNameFromUserID
(
	v_numUserID NUMERIC(9,0) DEFAULT 0   
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$
BEGIN
   open SWV_RefCur for SELECT vcUserName as "vcUserName",vcUserDesc as "vcUserDesc" FROM UserMaster WHERE numUserId = v_numUserID;
END; $$;

