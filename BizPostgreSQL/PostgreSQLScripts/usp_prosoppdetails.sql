CREATE OR REPLACE FUNCTION usp_ProsOppDetails(v_bytemode SMALLINT DEFAULT NULL,
    v_numDivisionId NUMERIC(9,0) DEFAULT NULL,
    v_numContactId NUMERIC(9,0) DEFAULT NULL,
    v_tintOppStatus SMALLINT DEFAULT 0,
    v_tintOppType SMALLINT DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_intQuarter INTEGER DEFAULT NULL,
    v_intYear INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql 

/*paging logic*/  
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_strSql  VARCHAR(4000);
   v_SELECT  VARCHAR(4000);
   v_FROM  VARCHAR(4000);
   v_WHERE  VARCHAR(4000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);  

/*Dynamic sql*/
   v_strSql := '';
   v_SELECT := '';
   v_FROM := '';
   v_WHERE := '';
        

   IF v_bytemode = 0 then
            
      v_SELECT := '
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        CASE coalesce(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        FormatedDateFromDate(Opp.bintCreatedDate,Opp.numDomainId) bintCreatedDate';
      v_FROM := ' FROM    OpportunityMaster opp
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId ';
      v_WHERE := ' WHERE   OPP.tintOppStatus = 1 AND
                        opp.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
      IF v_tintOppType > 0 then
         v_WHERE := coalesce(v_WHERE,'') || ' AND opp.tintOppType = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(3)),1,3);
      end if;
   end if;  
            

        
              
      
            
            
   IF v_bytemode = 1 then
            
      v_SELECT := '
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						OPP.numOppId,
                        OBD.numOppBizDocsId,
                        FormatedDateFromDate(OBD.dtCreatedDate,
                                                 Opp.numDomainId) dtCreatedDate,
                        opp.numContactID,
                        vcBizDocID,
                        OBD.monDealAmount,
                        CASE coalesce(opp.bitBillingTerms, false)
                          WHEN true
						  THEN FormatedDateFromDate(dtFromDate + (coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)), 0) || '' day'')::INTERVAL, Opp.numDomainID)	                                                            
                          WHEN false
                          THEN FormatedDateFromDate(obd.dtFromDate, Opp.numDomainID)
                        END AS dtDueDate,
                        obd.monDealAmount - monamountpaid AS baldue,
                        GetListIemName(OBD.numBizDocStatus) vcStatus';
      v_FROM := ' FROM    OpportunityMaster opp
                        INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OPP.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId ';
      v_WHERE := ' WHERE OPP.tintOppStatus = 1 AND
                        opp.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '
                        AND opp.tintOppType = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(3)),1,3) || '
						AND OBD.bitAuthoritativeBizDocs =1';
   end if;
            
            
   IF v_bytemode = 2 then
            
      v_SELECT := '
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						Opp.numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate,
                        CASE coalesce(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        CAST(coalesce(PP.intTotalProgress, 0) AS VARCHAR(3)) || '' %'' intTotalProgress,
                        CASE tintoppType
                          WHEN 1 THEN ''Sales''
                          WHEN 2 THEN ''Purchase''
                        END AS OppType,
                        FormatedDateFromDate(Opp.intPEstimatedCloseDate,
                                                 Opp.numDomainId) intPEstimatedCloseDate ';
      v_FROM := ' FROM    OpportunityMaster opp
                        left JOIN ProjectProgress PP ON PP.numOppId = Opp.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId ';
      v_WHERE := ' WHERE   
                        opp.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '
                        AND tintOppStatus =  ' || SUBSTR(CAST(v_tintOppStatus AS VARCHAR(3)),1,3);
      IF v_tintOppType > 0 then
         v_WHERE := coalesce(v_WHERE,'') || ' AND opp.tintOppType = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(3)),1,3);
      end if;
   end if;

   IF v_bytemode = 3 then
            
      v_SELECT := '
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        CASE coalesce(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate ';
      v_FROM := ' FROM    OpportunityMaster opp
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId ';
      v_WHERE := ' WHERE   OPP.tintOppStatus = 1 AND to_char(opp.bintCreatedDate,''Q'')=''' || SUBSTR(CAST(COALESCE(v_intQuarter,0) AS VARCHAR(15)),1,15) || ''' And to_char(opp.bintCreatedDate,''YYYY'')=''' || SUBSTR(CAST(COALESCE(v_intYear,0) AS VARCHAR(15)),1,15) || ''' and
                        opp.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
      IF v_tintOppType > 0 then
         v_WHERE := coalesce(v_WHERE,'') || ' AND opp.tintOppType = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(3)),1,3);
      end if;
   end if;  
    
   IF v_bytemode = 4 then
            
      v_SELECT := '
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						Opp.numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate,
                        CASE coalesce(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        CAST(coalesce(PP.intTotalProgress, 0) AS VARCHAR(3))
                        || '' %'' intTotalProgress,
                        CASE tintoppType
                          WHEN 1 THEN ''Sales''
                          WHEN 2 THEN ''Purchase''
                        END AS OppType,
                        FormatedDateFromDate(Opp.intPEstimatedCloseDate,
                                                 Opp.numDomainId) intPEstimatedCloseDate ';
      v_FROM := ' FROM    OpportunityMaster opp
                        left JOIN ProjectProgress PP ON PP.numOppId = Opp.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId ';
      v_WHERE := ' WHERE   
                        opp.numDomainID = ' || COALESCE(v_numDomainID,0) || '
                        AND tintOppStatus =  ' || SUBSTR(CAST(v_tintOppStatus AS VARCHAR(3)),1,3) || '
						AND to_char(opp.bintCreatedDate,''Q'')=''' || COALESCE(v_intQuarter,0) || ''' And to_char(opp.bintCreatedDate,''YYYY'')=''' || COALESCE(v_intYear,0) || '''';
      IF v_tintOppType > 0 then
         v_WHERE := coalesce(v_WHERE,'') || ' AND opp.tintOppType = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(3)),1,3);
      end if;
   end if;

   IF v_numDivisionId > 0 then
      v_WHERE := coalesce(v_WHERE,'') || ' AND opp.numDivisionId = ' || COALESCE(v_numDivisionId,0);
   end if; 
   IF v_numContactId > 0 then
      v_WHERE := coalesce(v_WHERE,'') || ' AND Opp.numContactID = ' || COALESCE(v_numContactId,0);
   end if; 

	IF v_PageSize > 0 then
		v_strSql := coalesce(v_SELECT,'') || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ') SELECT * FROM Orders WHERE row > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and row <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
	ELSE
		v_strSql := coalesce(v_SELECT,'') || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ') SELECT * FROM Orders';
	end if; 

	RAISE NOTICE '%',v_strSql;         
	OPEN SWV_RefCur FOR EXECUTE v_strSql;
   
	v_strSql :=  'SELECT COUNT(*) ' || coalesce(v_FROM,'') || coalesce(v_WHERE,'');        
	EXECUTE v_strSql INTO v_TotRecs;

   RETURN;
END; $$; 


