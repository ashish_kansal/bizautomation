-- Stored procedure definition script USP_ShippingReport_UpdateMassSalesFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShippingReport_UpdateMassSalesFulfillment(v_numDomainId NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
    ,v_numShippingReportID NUMERIC(18,0)
	,v_vcShippingDetail TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcFromName  VARCHAR(1000);
   v_vcFromCompany  VARCHAR(1000);
   v_vcFromPhone  VARCHAR(100);
   v_vcFromAddressLine1  VARCHAR(50);
   v_vcFromAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcFromCity  VARCHAR(50);
   v_vcFromState  VARCHAR(50);
   v_vcFromZip  VARCHAR(50);
   v_vcFromCountry  VARCHAR(50);
   v_bitFromResidential  BOOLEAN DEFAULT 0;

   v_vcToName  VARCHAR(1000);
   v_vcToCompany  VARCHAR(1000);
   v_vcToPhone  VARCHAR(100);
   v_vcToAddressLine1  VARCHAR(50);
   v_vcToAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcToCity  VARCHAR(50);
   v_vcToState  VARCHAR(50);
   v_vcToZip  VARCHAR(50);
   v_vcToCountry  VARCHAR(50);
   v_bitToResidential  BOOLEAN DEFAULT 0;

   v_IsCOD  BOOLEAN;
   v_IsDryIce  BOOLEAN;
   v_IsHoldSaturday  BOOLEAN;
   v_IsHomeDelivery  BOOLEAN;
   v_IsInsideDelevery  BOOLEAN;
   v_IsInsidePickup  BOOLEAN;
   v_IsReturnShipment  BOOLEAN;
   v_IsSaturdayDelivery  BOOLEAN;
   v_IsSaturdayPickup  BOOLEAN;
   v_IsAdditionalHandling  BOOLEAN;
   v_IsLargePackage  BOOLEAN;
   v_numCODAmount  NUMERIC(18,2);
   v_vcCODType  VARCHAR(50);
   v_vcDeliveryConfirmation  VARCHAR(1000);
   v_vcDescription  TEXT;
   v_tintSignatureType  SMALLINT;
   v_tintPayorType  SMALLINT DEFAULT 0;
   v_vcPayorAccountNo  VARCHAR(20) DEFAULT '';
   v_numPayorCountry  NUMERIC(18,0);
   v_vcPayorZip  VARCHAR(50) DEFAULT '';
   v_numTotalCustomsValue  NUMERIC(18,2);
   v_numTotalInsuredValue  NUMERIC(18,2);
   v_hDocShippingDetail  INTEGER;
BEGIN
   IF LENGTH(coalesce(v_vcShippingDetail,'')) > 0 then
      SELECT * INTO v_hDocShippingDetail FROM SWF_Xml_PrepareDocument(v_vcShippingDetail);
      select   FromContact, FromCompany, FromPhone, FromAddress1, FromAddress2, FromCity, SUBSTR(CAST(FromState AS VARCHAR(18)),1,18), FromZip, SUBSTR(CAST(FromCountry AS VARCHAR(18)),1,18), IsFromResidential, ToContact, ToCompany, ToPhone, ToAddress1, ToAddress2, ToCity, SUBSTR(CAST(ToState AS VARCHAR(18)),1,18), ToZip, SUBSTR(CAST(ToCountry AS VARCHAR(18)),1,18), IsToResidential, PayerType, AccountNo, Country, ZipCode, IsCOD, IsHomeDelivery, IsInsideDelivery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, TotalInsuredValue, TotalCustomsValue, CODType, CODAmount, Description, CAST(coalesce(SignatureType,'0') AS SMALLINT) INTO v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromAddressLine2,
      v_vcFromCity,v_vcFromState,v_vcFromZip,v_vcFromCountry,
      v_bitFromResidential,v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,
      v_vcToAddressLine2,v_vcToCity,v_vcToState,v_vcToZip,v_vcToCountry,
      v_bitToResidential,v_tintPayorType,v_vcPayorAccountNo,v_numPayorCountry,
      v_vcPayorZip,v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,
      v_IsSaturdayDelivery,v_IsSaturdayPickup,v_IsAdditionalHandling,
      v_IsLargePackage,v_numTotalInsuredValue,v_numTotalCustomsValue,v_vcCODType,
      v_numCODAmount,v_vcDescription,v_tintSignatureType FROM
      SWF_OpenXml(v_hDocShippingDetail,'/NewDataSet/Table1','FromContact |FromPhone |FromCompany |FromAddress1 |FromAddress2 |FromCountry |FromState |FromCity |FromZip |IsFromResidential |ToContact |ToPhone |ToCompany |ToAddress1 |ToAddress2 |ToCountry |ToState |ToCity |ToZip |IsToResidential |PayerType |ReferenceNo |AccountNo |ZipCode |Country |IsAdditionalHandling |IsCOD |IsHomeDelivery |IsInsideDelivery |IsInsidePickup |IsLargePackage |IsSaturdayDelivery |IsSaturdayPickup |SignatureType |Description |TotalInsuredValue |TotalCustomsValue |CODType |CODAmount') SWA_OpenXml(FromContact VARCHAR(1000),FromPhone VARCHAR(100),FromCompany VARCHAR(1000),
      FromAddress1 VARCHAR(50),FromAddress2 VARCHAR(50),FromCountry NUMERIC(18,0),
      FromState NUMERIC(18,0),FromCity VARCHAR(50),FromZip VARCHAR(50),
      IsFromResidential BOOLEAN,ToContact VARCHAR(1000),ToPhone VARCHAR(100),
      ToCompany VARCHAR(1000),ToAddress1 VARCHAR(50),ToAddress2 VARCHAR(50),
      ToCountry NUMERIC(18,0),ToState NUMERIC(18,0),ToCity VARCHAR(50),
      ToZip VARCHAR(50),IsToResidential BOOLEAN,PayerType SMALLINT,ReferenceNo VARCHAR(200),
      AccountNo VARCHAR(20),ZipCode VARCHAR(50),Country NUMERIC(18,0),
      IsAdditionalHandling BOOLEAN,IsCOD BOOLEAN,IsHomeDelivery BOOLEAN,
      IsInsideDelivery BOOLEAN,IsInsidePickup BOOLEAN,IsLargePackage BOOLEAN,
      IsSaturdayDelivery BOOLEAN,IsSaturdayPickup BOOLEAN,SignatureType SMALLINT,
      Description TEXT,TotalInsuredValue NUMERIC(18,2),TotalCustomsValue NUMERIC(18,2),
      CODType VARCHAR(50),CODAmount NUMERIC(18,2));
      PERFORM SWF_Xml_RemoveDocument(v_hDocShippingDetail);
      UPDATE
      ShippingReport
      SET
      IsCOD = v_IsCOD,IsHomeDelivery = v_IsHomeDelivery,IsInsideDelevery = v_IsInsideDelevery,
      IsInsidePickup = v_IsInsidePickup,IsSaturdayDelivery = v_IsSaturdayDelivery,
      IsSaturdayPickup = v_IsSaturdayPickup,IsAdditionalHandling = v_IsAdditionalHandling,
      IsLargePackage = v_IsLargePackage,tintSignatureType = v_tintSignatureType,
      vcFromName = v_vcFromName,vcFromCompany = v_vcFromCompany,
      vcFromPhone = v_vcFromPhone,vcFromAddressLine1 = v_vcFromAddressLine1,
      vcFromCity = v_vcFromCity,vcFromState = SUBSTR(CAST(v_vcFromState AS VARCHAR(18)),1,18),
      vcFromZip = v_vcFromZip,vcFromCountry = SUBSTR(CAST(v_vcFromCountry AS VARCHAR(18)),1,18),
      bitFromResidential = v_bitFromResidential,
      vcToName = v_vcToName,vcToCompany = v_vcToCompany,vcToPhone = v_vcToPhone,
      vcToAddressLine1 = v_vcToAddressLine1,vcToCity = v_vcToCity,vcToState = SUBSTR(CAST(v_vcToState AS VARCHAR(18)),1,18),
      vcToZip = v_vcToZip,vcToCountry = SUBSTR(CAST(v_vcToCountry AS VARCHAR(18)),1,18),
      bitToResidential = v_bitToResidential,
      vcCODType = v_vcCODType,vcDeliveryConfirmation = v_vcDeliveryConfirmation,
      vcDescription = v_vcDescription,numTotalInsuredValue = v_numTotalInsuredValue,
      numTotalCustomsValue = v_numTotalInsuredValue,
      numCODAmount = v_numCODAmount,tintPayorType = v_tintPayorType,vcPayorAccountNo = v_vcPayorAccountNo,
      numPayorCountry = v_numPayorCountry,vcPayorZip = v_vcPayorZip
      WHERE
      numShippingReportID = v_numShippingReportID;
   end if;
   RETURN;
END; $$;


