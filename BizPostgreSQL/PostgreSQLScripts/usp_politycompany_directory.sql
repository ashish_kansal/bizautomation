-- Stored procedure definition script USP_PolityCompany_Directory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PolityCompany_Directory(v_numRecordIndex INTEGER
	,v_numPageSize INTEGER
	,v_vcSortColumn VARCHAR(5)
	,v_vcSortDirection VARCHAR(4)
	,v_vcSearchText VARCHAR(500)
	,v_vcPrimaryBusinessIds VARCHAR(1000)
	,v_vcSubCategoryIds VARCHAR(1000)
	,v_vcBusinessStructureIds VARCHAR(1000)
	,v_vcHeadquarterIds VARCHAR(1000)
	,v_vcPoliticalScoreIds VARCHAR(1000),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS "TotalReords"
		,(CASE WHEN coalesce(CFVLinkToSite.Fld_Value,'0') = '1' AND coalesce(CI.vcWebSite,'') <> 'http://' THEN CONCAT('<a href="',coalesce(CI.vcWebSite,''),'" target="_blank">',coalesce(CI.vcCompanyName,''),
      '</a>') ELSE coalesce(CI.vcCompanyName,'') END) AS "vcCompanyName"
		,coalesce(CI.txtComments,'') AS "txtComments"
		,coalesce(CI.vcWebSite,'') AS "vcWebSite"
		,(CASE WHEN CFVPoliticalScore.Fld_Value = '87653' THEN CONCAT('<b style="color:green">',coalesce(LD.vcData,''),'</b>') ELSE CONCAT('<b style="color:red">',coalesce(LD.vcData,''),'</b>') END) AS "vcPoliticalScore"
		,CONCAT((CASE WHEN coalesce(CFVEvidence1.Fld_Value,'') <> '' THEN CONCAT('<a href="',coalesce(CFVEvidence1.Fld_Value,''),'" target="_blank">(1)</a>') ELSE '' END),(CASE WHEN coalesce(CFVEvidence2.Fld_Value,'') <> '' THEN CONCAT(' <a href="',coalesce(CFVEvidence2.Fld_Value,''),'" target="_blank">(2)</a>') ELSE '' END),(CASE WHEN coalesce(CFVEvidence3.Fld_Value,'') <> '' THEN CONCAT(' <a href="',coalesce(CFVEvidence3.Fld_Value,''),'" target="_blank">(3)</a>') ELSE '' END),(CASE WHEN coalesce(CFVEvidence4.Fld_Value,'') <> '' THEN CONCAT(' <a href="',coalesce(CFVEvidence4.Fld_Value,''),'" target="_blank">(4)</a>') ELSE '' END),
   (CASE WHEN coalesce(CFVEvidence5.Fld_Value,'') <> '' THEN CONCAT(' <a href="',coalesce(CFVEvidence5.Fld_Value,''),'" target="_blank">(5)</a>') ELSE '' END)) AS "vcEvidence"
   FROM
   CompanyInfo CI
   INNER JOIN
   DivisionMaster DM
   ON
   CI.numCompanyId = DM.numCompanyID
   LEFT JOIN
   CFW_FLD_Values CFVApproved
   ON
   CFVApproved.Fld_ID = 13479
   AND CFVApproved.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVPrimaryBusiness
   ON
   CFVPrimaryBusiness.Fld_ID = 13465
   AND CFVPrimaryBusiness.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVSubCategory
   ON
   CFVSubCategory.Fld_ID = 13466
   AND CFVSubCategory.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVBusinessStructure
   ON
   CFVBusinessStructure.Fld_ID = 13468
   AND CFVBusinessStructure.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVHeadquarter
   ON
   CFVHeadquarter.Fld_ID = 13469
   AND CFVHeadquarter.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVPoliticalScore
   ON
   CFVPoliticalScore.Fld_ID = 13467
   AND CFVPoliticalScore.RecId = DM.numDivisionID
   LEFT JOIN
   Listdetails LD
   ON
   CFVPoliticalScore.Fld_Value = LD.numListItemID::VARCHAR
   LEFT JOIN
   CFW_FLD_Values CFVEvidence1
   ON
   CFVEvidence1.Fld_ID = 13474
   AND CFVEvidence1.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVEvidence2
   ON
   CFVEvidence2.Fld_ID = 13475
   AND CFVEvidence2.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVEvidence3
   ON
   CFVEvidence3.Fld_ID = 13476
   AND CFVEvidence3.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVEvidence4
   ON
   CFVEvidence4.Fld_ID = 13477
   AND CFVEvidence4.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVEvidence5
   ON
   CFVEvidence5.Fld_ID = 13478
   AND CFVEvidence5.RecId = DM.numDivisionID
   LEFT JOIN
   CFW_FLD_Values CFVLinkToSite
   ON
   CFVLinkToSite.Fld_ID = 13471
   AND CFVLinkToSite.RecId = DM.numDivisionID
   WHERE
   CI.numDomainID = 167
   AND CI.numCompanyType = 47
   AND coalesce(CFVApproved.Fld_Value,'0') = '1'
   AND (coalesce(v_vcPrimaryBusinessIds,'') = '' OR CFVPrimaryBusiness.Fld_Value IN(SELECT Id::VARCHAR  FROM SplitIDs(v_vcPrimaryBusinessIds,',')))
   AND (coalesce(v_vcSubCategoryIds,'') = '' OR CFVSubCategory.Fld_Value IN(SELECT Id::VARCHAR FROM SplitIDs(v_vcSubCategoryIds,',')))
   AND (coalesce(v_vcBusinessStructureIds,'') = '' OR CFVBusinessStructure.Fld_Value IN(SELECT Id::VARCHAR FROM SplitIDs(v_vcBusinessStructureIds,',')))
   AND (coalesce(v_vcHeadquarterIds,'') = '' OR CFVHeadquarter.Fld_Value IN(SELECT Id::VARCHAR FROM SplitIDs(v_vcHeadquarterIds,',')))
   AND (coalesce(v_vcPoliticalScoreIds,'') = '' OR CFVPoliticalScore.Fld_Value IN(SELECT Id::VARCHAR FROM SplitIDs(v_vcPoliticalScoreIds,',')))
   AND (coalesce(v_vcSearchText,'') = ''  OR (coalesce(CI.vcCompanyName,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CI.txtComments,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CI.vcWebSite,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(LD.vcData,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CFVEvidence1.Fld_Value,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CFVEvidence2.Fld_Value,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CFVEvidence3.Fld_Value,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CFVEvidence4.Fld_Value,'') ilike CONCAT('%',v_vcSearchText,'%')
   OR coalesce(CFVEvidence5.Fld_Value,'') ilike CONCAT('%',v_vcSearchText,'%')))
   ORDER BY
   CASE WHEN v_vcSortDirection = 'asc' THEN
      CASE v_vcSortColumn
      WHEN '0' THEN CI.vcCompanyName
      WHEN '2' THEN CI.vcWebSite
      WHEN '3' THEN LD.vcData
      END
   END,CASE WHEN v_vcSortDirection = 'desc' THEN
      CASE v_vcSortColumn
      WHEN '0' THEN CI.vcCompanyName
      WHEN '2' THEN CI.vcWebSite
      WHEN '3' THEN LD.vcData
      END
   END DESC;
END; $$;












