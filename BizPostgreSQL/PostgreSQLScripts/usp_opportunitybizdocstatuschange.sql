DROP FUNCTION IF EXISTS USP_OpportunityBizDocStatusChange;

CREATE OR REPLACE FUNCTION USP_OpportunityBizDocStatusChange(v_numDomainID NUMERIC(18,0),
    v_numOppId NUMERIC(18,0),
    v_numOppBizDocsId NUMERIC(18,0),
    v_numBizDocStatus NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocId  NUMERIC(9,0);		
   v_bitAuthoritativeBizDocs  BOOLEAN;		
		
   v_tintOppType  SMALLINT;
   v_tintOppstatus  SMALLINT;
   v_numBizDocStatus11  NUMERIC(9,0);
   v_numBizDocStatus12  NUMERIC(9,0);
   v_numBizDocStatus12_2  NUMERIC(9,0);
BEGIN
   IF EXISTS(SELECT numoppid FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId AND coalesce(numBizDocStatus,0) = v_numBizDocStatus) then
      RETURN;
   end if;
		
   IF v_tintMode = 0 then
		
      IF EXISTS(SELECT * FROM OpportunityAutomationQueue WHERE numDomainID = v_numDomainID AND numOppId = v_numOppId
      AND numOppBizDocsId = v_numOppBizDocsId AND tintProcessStatus = 2) then
			
         RAISE EXCEPTION 'NOT_ALLOWED_ChangeBizDicStatus';
         RETURN;
      end if;
   end if;
				
   v_numBizDocId := 0;		
   v_bitAuthoritativeBizDocs := false;		
		
   SELECT numBizDocId,coalesce(bitAuthoritativeBizDocs,0) INTO v_numBizDocId,v_bitAuthoritativeBizDocs FROM OpportunityBizDocs  WHERE numoppid = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId LIMIT 1;
   UPDATE OpportunityBizDocs SET numBizDocStatusOLD = numBizDocStatus,numBizDocStatus = v_numBizDocStatus,
   numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())  WHERE numoppid = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId; 

		--IF @numBizDocStatus>0
		--BEGIN
   INSERT INTO OppFulfillmentBizDocsStatusHistory(numDomainID,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate) VALUES(v_numDomainID,v_numOppId,v_numOppBizDocsId,v_numBizDocStatus,v_numUserCntID,TIMEZONE('UTC',now()));  
		 --END	
		 
		 
		 
   v_tintOppType := 0;
   v_tintOppstatus := 0;
		 
   select   coalesce(tintopptype,0), coalesce(tintoppstatus,0) INTO v_tintOppType,v_tintOppstatus FROM OpportunityMaster WHERE numOppId = v_numOppId;
		 
   IF v_tintOppType = 1 AND v_tintOppstatus = 1 then
		 
				 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- tinyint
      IF v_bitAuthoritativeBizDocs = true OR v_numBizDocId = 296 then
         v_numBizDocStatus11 := 0;
         select   numBizDocStatus1 INTO v_numBizDocStatus11 FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND numRuleID = 11;
         IF coalesce(v_numBizDocStatus11,0) > 0 then
				
            IF NOT EXISTS(SELECT OI.numoppitemtCode FROM OpportunityMaster OM JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
            LEFT JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId AND OBD.numBizDocId = 296 AND coalesce(OBD.numBizDocStatus,0) = v_numBizDocStatus11 AND OBD.numBizDocId = v_numBizDocId
            LEFT JOIN OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID = OBD.numOppBizDocsId AND OBDI.numOppItemID = OI.numoppitemtCode
            WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppId GROUP BY OI.numoppitemtCode HAVING coalesce(SUM(OI.numUnitHour),0) -coalesce(SUM(OBDI.numUnitHour),0) > 0) then
					
							 -- numeric(18, 0)
								 -- numeric(18, 0)
								 -- numeric(18, 0)
								 -- numeric(18, 0)
								 -- numeric(18, 0)
								 -- numeric(18, 0)
								 -- tinyint
								 -- TINYINT
               PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
               v_numOppBizDocsId := 0,v_numBizDocStatus := 0,v_numUserCntID := v_numUserCntID,
               v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT,
               v_numRuleID := 11);
            end if;
         end if;
         v_numBizDocStatus12 := 0;
         v_numBizDocStatus12_2 := 0;
         select   numBizDocStatus1, numBizDocStatus2 INTO v_numBizDocStatus12,v_numBizDocStatus12_2 FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND numRuleID = 12;
         IF coalesce(v_numBizDocStatus12,0) > 0 AND coalesce(v_numBizDocStatus12_2,0) > 0 then
				
            IF NOT EXISTS(SELECT OI.numoppitemtCode FROM OpportunityMaster OM JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
            LEFT JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId AND OBD.numBizDocId = 296 AND coalesce(OBD.numBizDocStatus,0) = v_numBizDocStatus12
            LEFT JOIN OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID = OBD.numOppBizDocsId AND OBDI.numOppItemID = OI.numoppitemtCode
            WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppId GROUP BY OI.numoppitemtCode HAVING coalesce(SUM(OI.numUnitHour),0) -coalesce(SUM(OBDI.numUnitHour),0) > 0) then
					
               IF NOT EXISTS(SELECT OI.numoppitemtCode FROM OpportunityMaster OM JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
               LEFT JOIN OpportunityBizDocs OBD ON OBD.numoppid = OM.numOppId AND coalesce(OBD.bitAuthoritativeBizDocs,0) = 1 AND coalesce(OBD.numBizDocStatus,0) = v_numBizDocStatus12_2
               LEFT JOIN OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID = OBD.numOppBizDocsId AND OBDI.numOppItemID = OI.numoppitemtCode
               WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppId GROUP BY OI.numoppitemtCode HAVING coalesce(SUM(OI.numUnitHour),0) -coalesce(SUM(OBDI.numUnitHour),0) > 0) then
							
										 -- numeric(18, 0)
												 -- numeric(18, 0)
												 -- numeric(18, 0)
												 -- numeric(18, 0)
												 -- numeric(18, 0)
												 -- numeric(18, 0)
												 -- tinyint
												 -- TINYINT
                  PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
                  v_numOppBizDocsId := 0,v_numBizDocStatus := 0,v_numUserCntID := v_numUserCntID,
                  v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT,
                  v_numRuleID := 12);
               end if;
            end if;
         end if;
      end if;
      PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
      v_numOppBizDocsId := v_numOppBizDocsId,v_numBizDocStatus := v_numBizDocStatus,
      v_numUserCntID := v_numUserCntID,v_tintProcessStatus := 1::SMALLINT,
      v_tintMode := 1::SMALLINT);
   end if;
END; $$;



/****** Object:  StoredProcedure [dbo].[USP_OppUpDateBizDocsInvoiceDetails]    Script Date: 07/26/2008 16:20:26 ******/



