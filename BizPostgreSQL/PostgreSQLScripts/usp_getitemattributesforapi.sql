CREATE OR REPLACE FUNCTION Usp_GetItemAttributesForAPI
(
	v_numItemCode NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,
    v_WebApiId INTEGER DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_numDefaultWarehouseID  NUMERIC(18,0);
	v_bitSerialize  BOOLEAN;                      
	v_str  TEXT;                 
	v_str1  TEXT;               
	v_ColName TEXT;                      
	v_bitKitParent  BOOLEAN;
	v_numItemGroupID  NUMERIC(9,0);              
	v_ID  NUMERIC(9,0);                        
	v_numCusFlDItemID  VARCHAR(20);                        
	v_fld_label  VARCHAR(100);
	v_fld_type  VARCHAR(100);                        
	v_KitOnHand  DOUBLE PRECISION;
	SWV_RowCount INTEGER;
BEGIN
	SELECT 
		coalesce(numwarehouseID,0) INTO v_numDefaultWarehouseID 
	FROM 
		WebAPIDetail AS WAD 
	WHERE 
		WAD.numDomainID = v_numDomainID
		AND WAD.WebApiId = v_WebApiId;

	IF coalesce(v_numDefaultWarehouseID,0) = 0 then
		SELECT 
			WHI.numWareHouseID INTO v_numDefaultWarehouseID 
		FROM 
			WareHouseItems AS WHI
		WHERE 
			WHI.numItemID = v_numItemCode
			AND WHI.numDomainID = v_numDomainID LIMIT 1;
	end if;

	DROP TABLE IF EXISTS tt_TMPSPEC;
	CREATE TEMPORARY TABLE tt_TMPSPEC AS
    SELECT  DISTINCT
		FMST.Fld_id ,
        Fld_label ,
        coalesce(FMST.numlistid,0) AS numlistid ,
        LD.numListItemID ,
        LD.vcData
    FROM 
		CFW_Fld_Master FMST
	LEFT JOIN 
		CFw_Grp_Master Tmst 
	ON 
		Tmst.Grp_id = CAST(FMST.subgrp AS NUMERIC)
	JOIN 
		CFW_Loc_Master Lmst 
	ON 
		Lmst.Loc_id = FMST.Grp_id
	JOIN 
		listmaster AS LM 
	ON 
		FMST.numlistid = LM.numListID
	JOIN 
		Listdetails AS LD 
	ON 
		LD.numListID = LM.numListID
		AND FMST.numlistid = LD.numListID
	JOIN 
		ItemGroupsDTL AS IGD 
	ON 
		IGD.numOppAccAttrID = FMST.Fld_id
	JOIN 
		CFW_Fld_Values_Serialized_Items AS CFVSI 
	ON 
		CFVSI.Fld_ID = FMST.Fld_id 
		AND CAST(CFVSI.Fld_Value AS NUMERIC) = LD.numListItemID 
		AND CFVSI.RecId IN (SELECT
								numWareHouseItemID
							  FROM
								WareHouseItems
							  WHERE
								  numWareHouseID = v_numDefaultWarehouseID
								  AND numDomainID = v_numDomainID
								  AND numItemID = v_numItemCode)
	WHERE 
		FMST.numDomainID = v_numDomainID
		AND Lmst.Loc_id = 9
    ORDER BY 
		FMST.Fld_id;

	open SWV_RefCur for
	SELECT DISTINCT
		TS.fld_label AS Name
		,COALESCE((SELECT string_agg(vcData,',') FROM tt_TMPSPEC WHERE TS.Fld_id = tt_TMPSPEC.Fld_id),'') AS Value
	FROM  
		tt_TMPSPEC AS TS;
    
	v_str := '';                       
                        
	select 
		numItemGroup
		,CASE WHEN bitSerialized = false THEN bitLotNo ELSE bitSerialized END
		,(CASE WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true THEN 0 WHEN coalesce(bitKitParent,false) = true THEN 1 ELSE 0 END) 
	INTO 
		v_numItemGroupID
		,v_bitSerialize
		,v_bitKitParent 
	FROM  
		Item 
	WHERE 
		numItemCode = v_numItemCode;                        
   
	v_ColName := 'numWareHouseItemID,CAST(0 AS SMALLINT)';
                      
              
	--Create a Temporary table to hold data                                                            
	BEGIN
		CREATE TEMP SEQUENCE tt_tempTable_seq;
		EXCEPTION WHEN OTHERS THEN NULL;
	END;
	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPTABLE
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
		PRIMARY KEY,
		numCusFlDItemID NUMERIC(9,0)
	);                         
                        
	INSERT INTO tt_TEMPTABLE
	(numCusFlDItemID)
	SELECT DISTINCT
	(numOppAccAttrID)
	FROM 
		ItemGroupsDTL
	WHERE 
		numItemGroupID = v_numItemGroupID
		AND tintType = 2;                       

	v_ID := 0;                        
	SELECT 
		ID
		,numCusFlDItemID
		,fld_label
		,fld_type 
	INTO 
		v_ID
		,v_numCusFlDItemID
		,v_fld_label
		,v_fld_type 
	FROM  
		tt_TEMPTABLE
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID = Fld_ID 
	ORDER BY
		ID
	LIMIT 1;                        
                         
	WHILE v_ID > 0 LOOP                             
		v_str := coalesce(v_str,'') || ', GetCustFldItemsValue(' || coalesce(v_numCusFlDItemID,'') || ',CAST(9 AS SMALLINT),' || coalesce(v_ColName,'') || ',''' || coalesce(v_fld_type,'') || ''') as "' || coalesce(v_fld_label,'') || '_c"';

		SELECT 
			ID
			,numCusFlDItemID
			,fld_label
			,fld_type 
		INTO 
			v_ID
			,v_numCusFlDItemID
			,v_fld_label
			,v_fld_type 
		FROM  
			tt_TEMPTABLE
		JOIN 
			CFW_Fld_Master 
		ON 
			numCusFlDItemID = Fld_ID
		WHERE
			ID > v_ID 
		ORDER BY
			ID
		LIMIT 1;  

		GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
		IF SWV_RowCount = 0 then
			v_ID := 0;
		end if;
	END LOOP;                        
                       
	v_KitOnHand := 0;

	v_str1 := 'select ' || coalesce(v_str1,'') || 'I.numItemCode,'|| '
					numWareHouseItemID,COALESCE(vcWarehouse,'''') || '': '' || COALESCE(WL.vcLocation,'''') vcWarehouse
					,W.numWareHouseID
					,Case when bitKitParent=true then COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else COALESCE(numOnHand,0) end as OnHand
					,CAST((fn_UOMConversion(COALESCE(I.numBaseUnit,0),I.numItemCode,I.numDomainId,COALESCE(I.numPurchaseUnit,0)) * Case when bitKitParent=true THEN COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE COALESCE(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand
					,CAST((fn_UOMConversion(COALESCE(I.numBaseUnit,0),I.numItemCode,I.numDomainId,COALESCE(I.numSaleUnit,0)) * Case when bitKitParent=true THEN COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE COALESCE(numOnHand,0) end) as numeric(18,2)) as SalesOnHand
					,Case when bitKitParent=true THEN 0 ELSE COALESCE(numReorder,0) END as Reorder 
					,Case when bitKitParent=true THEN 0 ELSE COALESCE(numOnOrder,0) END as OnOrder
					,Case when bitKitParent=true THEN 0 ELSE COALESCE(numAllocation,0) END as Allocation 
					,Case when bitKitParent=true THEN 0 ELSE COALESCE(numBackOrder,0) END as BackOrder
					,0 as Op_Flag
					,COALESCE(WL.vcLocation,'''') "Location"
					,COALESCE(WL.numWLocationID,0) as numWLocationID
					,round(COALESCE(monWListPrice,0),2) Price,COALESCE(vcWHSKU,'''') as SKU,COALESCE(vcBarCode,'''') as BarCode ' || CASE WHEN v_bitSerialize = true THEN ' ' ELSE v_str END || '                   
					,(SELECT 
						CASE 
							WHEN COALESCE(subI.bitKitParent,false) = true AND COALESCE((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
						ELSE 0 
						END 
					FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) IsDeleteKitWarehouse
					,' || v_bitKitParent || ' bitKitParent
				from WareHouseItems                           
				join Warehouses W                             
				on W.numWareHouseID=WareHouseItems.numWareHouseID 
				left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
				join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
				where numItemID=' || COALESCE(v_numItemCode,0)|| ' 
				AND WareHouseItems.numWarehouseItemID IN (SELECT numWarehouseItemID FROM WareHouseItems WHERE numwarehouseID = ' || COALESCE(v_numDefaultWarehouseID,0) || '
                AND numDomainID = ' || COALESCE(v_numDomainID,0) || '
                AND numItemID = ' || COALESCE(v_numItemCode,0) || ' ) ';
   
	RAISE NOTICE '%',(v_str1);           

	OPEN SWV_RefCur2 FOR EXECUTE v_str1;
	RETURN;
END; $$;
                       



