-- FUNCTION: public.usp_getbizdocstatusapprove(integer, refcursor)

-- DROP FUNCTION public.usp_getbizdocstatusapprove(integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbizdocstatusapprove(
	v_numdomainid integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:11:11 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for select cast(numBizDocStatusAppId as VARCHAR(255)) as numBizDocStatusID,
 cast(numBizDocTypeID as VARCHAR(255)) AS BizDocTypeID,
cast(BDT.vcData as VARCHAR(255)) as BizDocTypeValue,
cast(numBizDocStatusID as VARCHAR(255)) AS BizDocStatusID,
cast(BDS.vcData as VARCHAR(255)) as BizDocStatusValue,
cast(numEmployeeID as VARCHAR(255)) AS EmployeeID,
vcFirstName || ' ' || vcLastname as EmployeeValue,
cast(numActionTypeID as VARCHAR(255)) AS ActionTypeID,
cast(ACT.vcData as VARCHAR(255)) as ActionTypeValue
   from(SELECT * FROM BizDocStatusApprove WHERE numDomainID = v_numDomainID) BZA
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) BDT ON BDT.numListItemID = BZA.numBizDocTypeID
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) BDS on BDS.numListItemID = BZA.numBizDocStatusID
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) ACT ON ACT.numListItemID = BZA.numActionTypeID
   INNER JOIN(SELECT * FROM AdditionalContactsInformation  WHERE numDomainID = v_numDomainID) ACI ON ACI.numContactID = BZA.numEMployeeID;
END;
$BODY$;

ALTER FUNCTION public.usp_getbizdocstatusapprove(integer, refcursor)
    OWNER TO postgres;
