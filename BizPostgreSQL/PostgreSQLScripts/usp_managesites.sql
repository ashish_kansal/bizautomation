-- Stored procedure definition script USP_ManageSites for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSites(INOUT v_numSiteID NUMERIC(9,0) DEFAULT 0 ,
	v_vcSiteName VARCHAR(50) DEFAULT NULL,
	v_vcDescription VARCHAR(1000) DEFAULT NULL,
	v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_vcHostName VARCHAR(500) DEFAULT NULL,
	v_bitIsActive BOOLEAN DEFAULT NULL,
	v_vcLiveURL VARCHAR(100) DEFAULT NULL,
	v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
	v_bitOnePageCheckout BOOLEAN DEFAULT false ,
	v_tintRateType SMALLINT DEFAULT 0 ,
	v_vcCategories VARCHAR(2000) DEFAULT NULL,
	v_Mode INTEGER DEFAULT 0,
	v_bitHtml5 BOOLEAN DEFAULT NULL,
	v_vcMetaTags TEXT DEFAULT NULL,
	v_bitSSLRedirect BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(v_vcLiveURL) > 1 then

      UPDATE Sites SET
      vcLiveURL = v_vcLiveURL
      WHERE numSiteID = v_numSiteID;
      RETURN;
   end if;


   IF v_numSiteID = 0 AND (NOT EXISTS(SELECT vcSiteName from Sites Where (vcSiteName = v_vcSiteName OR vcHostName = v_vcHostName)))
   AND (NOT EXISTS(SELECT vcPortalName from Domain Where vcPortalName = v_vcHostName)) then

      INSERT INTO Sites(vcSiteName,
		vcDescription,
		vcHostName,
		numCreatedBy,
		dtCreateDate,
		numModifiedBy,
		dtModifiedDate,
		numDomainID,
		bitIsActive,numCurrencyID,bitOnePageCheckout,
		tintRateType,
		bitHtml5,
		vcMetaTags,
		bitSSLRedirect)
	VALUES(v_vcSiteName,
		v_vcDescription,
		v_vcHostName,
		v_numUserCntID,
		TIMEZONE('UTC',now()),
		v_numUserCntID,
		TIMEZONE('UTC',now()),
		v_numDomainID,
		v_bitIsActive,v_numCurrencyID,v_bitOnePageCheckout,
		v_tintRateType,
		v_bitHtml5,
		v_vcMetaTags,
		v_bitSSLRedirect);
	
      v_numSiteID := CURRVAL('Sites_seq');
   ELSEIF v_numSiteID > 0 AND (NOT EXISTS(SELECT vcSiteName from Sites Where (vcSiteName = v_vcSiteName OR vcHostName = v_vcHostName) AND numSiteID <> v_numSiteID))
   AND (NOT EXISTS(SELECT vcPortalName from Domain Where vcPortalName = v_vcHostName))
   then

      IF v_Mode = 0 then
	 
         UPDATE Sites SET
         vcSiteName = v_vcSiteName,vcDescription = v_vcDescription,vcHostName		= v_vcHostName,
         numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
         bitIsActive = v_bitIsActive,numCurrencyID = v_numCurrencyID,
         bitOnePageCheckout = v_bitOnePageCheckout,tintRateType = v_tintRateType,
         bitHtml5 = v_bitHtml5,vcMetaTags = v_vcMetaTags,bitSSLRedirect = v_bitSSLRedirect
         WHERE numSiteID = v_numSiteID;
      ELSEIF v_Mode = 1
      then
         IF	v_numSiteID > 0 then
	
            IF EXISTS(SELECT * FROM SiteCategories WHERE numSiteID = v_numSiteID) then
			 
               DELETE FROM SiteCategories WHERE numSiteID = v_numSiteID;
            end if;
            INSERT INTO SiteCategories(numSiteID,
			numCategoryID)
            SELECT  v_numSiteID AS numSiteID,  ID  from SplitIDs(v_vcCategories,',');
         end if;
      end if;
   ELSE
      v_numSiteID := 0;
   end if;
END; $$;



