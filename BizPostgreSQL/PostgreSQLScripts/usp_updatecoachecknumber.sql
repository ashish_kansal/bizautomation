-- Stored procedure definition script USP_UpdateCOACheckNumber for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCOACheckNumber(v_numDomainId NUMERIC,
    v_numAccountID NUMERIC,
    v_vcStartingCheckNumber VARCHAR(50))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  Chart_Of_Accounts   SET     vcStartingCheckNumber = v_vcStartingCheckNumber
   WHERE   numDomainId = v_numDomainId
   AND numAccountId = v_numAccountID;
   RETURN;
END; $$;


/*
declare @p5 bigint
set @p5=5266
exec USP_UpdateDealStatus @numOppID=5885,@byteMode=1,@Amount=$100.0000,@ShippingCost=$0.0000,@numOppBizDocID=@p5 output,@numUserContactID=82933,@numDomainID=72
select @p5
*/
--created by anoop jayaraj                    


