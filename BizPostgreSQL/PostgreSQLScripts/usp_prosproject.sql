-- Stored procedure definition script USP_ProsProject for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProsProject(v_numDivisionId NUMERIC(9,0) DEFAULT null,    
v_numProjectStatus NUMERIC(9,0) DEFAULT NULL,  
v_numDomainID NUMERIC(9,0) DEFAULT 0  ,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT distinct pro.numProId as "numProId",
    pro.vcProjectName AS "Name",
    opp.vcpOppName as "DealID", addcon.numContactId AS "numContactId",
    addcon.vcFirstName || ' ' || addcon.vcLastname AS "CustJobContact",
    GetProLstMileStoneCompltd(pro.numProId) as "LstCmtMilestone",
    GetProLstStageCompltd(pro.numProId,v_ClientTimeZoneOffset) as "lstCompStage",
	COALESCE((SELECT string_agg(CONCAT( A.vcFirstName,' ',A.vcLastname,'(',fn_GetListItemName(numProjectRole),')'),',') 
         FROM ProjectTeamRights TR
         join AdditionalContactsInformation A on TR.numContactId = A.numContactId
         join DivisionMaster DM on DM.numDivisionID = A.numDivisionId where
         TR.numProId = pro.numProId and TR.tintUserType = 2 and DM.numDivisionID = v_numDivisionId and DM.numDomainID = v_numDomainID),'') AS "vcResource"
   FROM ProjectsMaster pro
   left join  OpportunityMaster opp
   on opp.numOppId = pro.numOppId
   join AdditionalContactsInformation addcon
   on addcon.numContactId = pro.numCustPrjMgr
   where (pro.numDivisionId = v_numDivisionId
   or pro.numProId  in (select distinct numProId from ProjectTeamRights TR
      join AdditionalContactsInformation A on TR.numContactId = A.numContactId
      join DivisionMaster DM on DM.numDivisionID = A.numDivisionId where TR.tintUserType = 2 and DM.numDivisionID = v_numDivisionId and DM.numDomainID = v_numDomainID))
   and coalesce(pro.numProjectStatus,0) = v_numProjectStatus  and pro.numdomainId = v_numDomainID;
END; $$;















