CREATE OR REPLACE FUNCTION StagePercentageDetails_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_action  CHAR(1) DEFAULT 'I';
   v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
	 v_TempnumStageDetailsId  NUMERIC;
BEGIN
	IF (TG_OP = 'DELETE') THEN
		SELECT SPLM.numDomainID INTO v_numDomainID FROM StagePercentageDetails SPD INNER JOIN Sales_process_List_Master SPLM ON SPD.Slp_Id = SPLM.Slp_Id WHERE SPD.numStageDetailsId = OLD.numStageDetailsId;
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numStageDetailsId;
		v_numUserCntID := OLD.numModifiedBy;
	ELSIF (TG_OP = 'UPDATE') THEN
		SELECT SPLM.numDomainID INTO v_numDomainID FROM StagePercentageDetails SPD INNER JOIN Sales_process_List_Master SPLM ON SPD.Slp_Id = SPLM.Slp_Id WHERE SPD.numStageDetailsId = NEW.numStageDetailsId;
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numStageDetailsId;
		v_numUserCntID := NEW.numModifiedBy;


		IF OLD.numStageDetailsId <> NEW.numStageDetailsId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numStageDetailsId,');
		END IF;
		IF OLD.dtStartDate <> NEW.dtStartDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'dtStartDate,');
		END IF;
		IF OLD.dtEndDate <> NEW.dtEndDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'dtEndDate,');
		END IF;
		IF OLD.tintPercentage <> NEW.tintPercentage THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintPercentage,');
		END IF;
		IF OLD.tinProgressPercentage <> NEW.tinProgressPercentage THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tinProgressPercentage,');
		END IF;
		IF OLD.numAssignTo <> NEW.numAssignTo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignTo,');
		END IF;
		IF OLD.intDueDays <> NEW.intDueDays THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intDueDays,');
		END IF;
		IF OLD.numModifiedBy <> NEW.numModifiedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numModifiedBy,');
		END IF;
		IF OLD.numCreatedBy <> NEW.numCreatedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCreatedBy,');
		END IF;
	
		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');
	ELSIF (TG_OP = 'INSERT') THEN
		SELECT SPLM.numDomainID INTO v_numDomainID FROM StagePercentageDetails SPD INNER JOIN Sales_process_List_Master SPLM ON SPD.Slp_Id = SPLM.Slp_Id WHERE SPD.numStageDetailsId = NEW.numStageDetailsId;
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numStageDetailsId;
		v_numUserCntID := NEW.numCreatedBy;
	END IF;

	IF v_tintWFTriggerOn = 1 then
	
      IF EXISTS(SELECT numStageDetailsId FROM StagePercentageDetails_TempDateFields WHERE COALESCE(numStageDetailsId,0) = v_numRecordID) then
		
         SELECT SP.numStageDetailsId INTO v_TempnumStageDetailsId FROM StagePercentageDetails SP WHERE  (
				(SP.dtEndDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtEndDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
				(SP.dtStartDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtStartDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND SP.numStageDetailsId = v_numRecordID;
         IF(v_TempnumStageDetailsId IS NOT NULL) then
			
            UPDATE StagePercentageDetails_TempDateFields AS SPT
            SET
            dtEndDate  = SP.dtEndDate,dtStartDate  = SP.dtStartDate
            FROM
            StagePercentageDetails AS SP
            WHERE  SPT.numStageDetailsId = SP.numStageDetailsId AND(((SP.dtEndDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtEndDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(SP.dtStartDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtStartDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
            AND
            SPT.numStageDetailsId = v_numRecordID);
         ELSE
            DELETE FROM StagePercentageDetails_TempDateFields WHERE COALESCE(numStageDetailsId,0) = v_numRecordID;
         end if;
      ELSE
         INSERT INTO StagePercentageDetails_TempDateFields(numStageDetailsId, numDomainID, dtEndDate, dtStartDate)
         SELECT numStageDetailsId,numdomainid, dtEndDate, dtStartDate
         FROM StagePercentageDetails
         WHERE (
					(CAST(dtEndDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(dtEndDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
					(CAST(dtStartDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(dtStartDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND COALESCE(numStageDetailsId,0) = v_numRecordID;
      end if;
   ELSEIF v_tintWFTriggerOn = 2
   then
	
      SELECT SP.numStageDetailsId INTO v_TempnumStageDetailsId FROM StagePercentageDetails SP WHERE  (
				(SP.dtEndDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtEndDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(SP.dtStartDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtStartDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
      AND SP.numStageDetailsId = v_numRecordID;
      IF(v_TempnumStageDetailsId IS NOT NULL) then
		
         UPDATE StagePercentageDetails_TempDateFields AS SPT
         SET
         dtEndDate  = SP.dtEndDate,dtStartDate  = SP.dtStartDate
         FROM
         StagePercentageDetails AS SP
         WHERE  SPT.numStageDetailsId = SP.numStageDetailsId AND(((SP.dtEndDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtEndDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(SP.dtStartDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND SP.dtStartDate <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND
         SPT.numStageDetailsId = v_numRecordID);
      ELSE
         DELETE FROM StagePercentageDetails_TempDateFields WHERE COALESCE(numStageDetailsId,0) = v_numRecordID;
      end if;
   ELSEIF v_tintWFTriggerOn = 5
   then
      DELETE FROM StagePercentageDetails_TempDateFields WHERE COALESCE(numStageDetailsId,0) = v_numRecordID;
   end if;	

	--DateField WorkFlow Logic
	
    PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 94,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER StagePercentageDetails_IUD AFTER INSERT OR UPDATE OR DELETE ON StagePercentageDetails FOR EACH ROW EXECUTE PROCEDURE StagePercentageDetails_IUD_TrFunc();


