-- Stored procedure definition script usp_GetMasterListDetailsDynamicForm for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetMasterListDetailsDynamicForm(v_numListID NUMERIC(9,0),                        
 v_vcItemType CHAR(3),                      
 v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcItemType = 'SYS' then
	
      open SWV_RefCur for
      SELECT 0 AS numItemID,'Lead' AS vcItemName
      UNION ALL
      SELECT 1 AS numItemID,'Prospect' AS vcItemName
      UNION ALL
      SELECT 2 AS numItemID,'Account' AS vcItemName;
   end if;
	
   IF v_vcItemType = 'LI' AND v_numListID = 9 then --Opportunity Source
	 
      SELECT * INTO SWV_RefCur FROM USP_GetOpportunitySource(v_numDomainID := v_numDomainID,SWV_RefCur := SWV_RefCur);
   ELSEIF v_vcItemType = 'LI'
   then    --Master List                    
	
      open SWV_RefCur for
      SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst
      FROM Listdetails LD left join listorder LO on LD.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE LD.numListID = v_numListID AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true)
      order by coalesce(intSortOrder,LD.sintOrder);
   ELSEIF v_vcItemType = 'L' AND coalesce(v_numListID,0) > 0
   then    --Master List                    
	
      open SWV_RefCur for
      SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst
      FROM Listdetails LD left join listorder LO on LD.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE LD.numListID = v_numListID AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true)
      order by coalesce(intSortOrder,LD.sintOrder);
   ELSEIF v_vcItemType = 'T'
   then    --Territories                    
	
      open SWV_RefCur for
      SELECT vcData AS vcItemName, numListItemID AS numItemID, 'T' As vcItemType, 0 As flagConst FROM Listdetails WHERE numListID = 78 AND numDomainid = v_numDomainID;
   ELSEIF v_vcItemType = 'AG'
   then   --Lead Groups                     
	
      open SWV_RefCur for
      SELECT vcGrpName AS vcItemName, numGrpId AS numItemID, 'G' As vcItemType, 0 As flagConst FROM Groups;
   ELSEIF v_vcItemType = 'S'
   then    --States                    
	
      open SWV_RefCur for
      SELECT vcState AS vcItemName, numStateID AS numItemID, 'S' As vcItemType, numCountryID As flagConst FROM State WHERE (numDomainID = v_numDomainID OR ConstFlag = true) AND 1 = 2;
   ELSEIF v_vcItemType = 'U'
   then    --Users                    
	
      open SWV_RefCur for
      SELECT A.numContactId AS numItemID,A.vcFirstName || ' ' || A.vcLastname AS vcItemName
      from UserMaster UM
      join AdditionalContactsInformation A
      on UM.numUserDetailId = A.numContactId
      where UM.numDomainID = v_numDomainID
      union
      select  A.numContactId,vcCompanyName || ' - ' || A.vcFirstName || ' ' || A.vcLastname
      from AdditionalContactsInformation A
      join DivisionMaster D
      on D.numDivisionID = A.numDivisionId
      join ExtarnetAccounts E
      on E.numDivisionID = D.numDivisionID
      join ExtranetAccountsDtl DTL
      on DTL.numExtranetID = E.numExtranetID
      join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      where A.numDomainID = v_numDomainID and bitPartnerAccess = true
      and D.numDivisionID <>(select numDivisionId from Domain where numDomainId = v_numDomainID);
   ELSEIF v_vcItemType = 'C'
   then    --Organization Campaign                    
	
      open SWV_RefCur for
      SELECT  numCampaignID AS numItemID,
                vcCampaignName || CASE bitIsOnline WHEN true THEN ' (Online)' ELSE ' (Offline)' END AS vcItemName
      FROM    CampaignMaster
      WHERE   numDomainID = v_numDomainID;
   ELSEIF v_vcItemType = 'IG'
   then
	
      open SWV_RefCur for
      SELECT vcItemGroup AS vcItemName, numItemGroupID AS numItemID, 'I' As vcItemType, 0 As flagConst FROM ItemGroups WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcItemType = 'PP'
   then
   
      open SWV_RefCur for
      SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst
      UNION
      SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst
      UNION
      SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst;
   ELSEIF v_vcItemType = 'V'
   then
   
      open SWV_RefCur for
      SELECT DISTINCT coalesce(C.vcCompanyName,'') AS vcItemName,  numVendorID AS numItemID ,'V' As vcItemType, 0 As flagConst   FROM Vendor V INNER JOIN DivisionMaster DM ON DM.numDivisionID = V.numVendorID
      INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE V.numDomainID = v_numDomainID;
   ELSEIF v_vcItemType = 'OC'
   then
   
      open SWV_RefCur for
      SELECT DISTINCT C.vcCurrencyDesc AS vcItemName,C.numCurrencyID AS numItemID FROM OpportunityMaster OM INNER JOIN Currency C ON C.numCurrencyID = OM.numCurrencyID
      WHERE OM.numDomainId = v_numDomainID;
   ELSEIF v_vcItemType = 'UOM'
   then    --Organization Campaign                    
	
	   
	   
	   --SELECT  numUOMId AS numItemID,
    --            vcUnitName AS vcItemName
    --   FROM    UOM
    --   WHERE   numDomainID = @numDomainID

	--Modified by Sachin Sadhu ||Date:7thAug2014
	--Purpose :Boneta facing problem in updating units 
	   
      open SWV_RefCur for
      SELECT u.numUOMId as numItemID , u.vcUnitName as vcItemName FROM
      UOM u INNER JOIN Domain d ON u.numDomainID = d.numDomainId
      WHERE u.numDomainID = v_numDomainID AND d.numDomainId = v_numDomainID AND
      u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END);
   ELSEIF v_vcItemType = 'O'
   then    --Opp Type                   
	 
      open SWV_RefCur for
      SELECT  1 AS numItemID,'Sales' AS vcItemName
      UNion ALL
      SELECT  2 AS numItemID,'Purchase' AS vcItemName;
	ELSEIF v_vcItemType = 'OT'
   then    --Opp Type                   
	 
      open SWV_RefCur for
      SELECT  1 AS numItemID,'Sales' AS vcItemName
      UNion ALL
      SELECT  2 AS numItemID,'Purchase' AS vcItemName;
   ELSEIF v_vcItemType = 'CHK'
   then    --Opp Type                   
	
      open SWV_RefCur for
      SELECT  1 AS numItemID,'Yes' AS vcItemName
      UNion ALL
      SELECT  0 AS numItemID,'No' AS vcItemName;
   ELSEIF v_vcItemType = 'COA'
   then
	
      open SWV_RefCur for
      SELECT  C.numAccountId AS numItemID,coalesce(C.vcAccountName,'') AS vcItemName
      FROM    Chart_Of_Accounts C
      INNER JOIN AccountTypeDetail ATD
      ON C.numParntAcntTypeID = ATD.numAccountTypeID
      WHERE   C.numDomainId = v_numDomainID
      ORDER BY C.vcAccountCode;
   ELSEIF v_vcItemType = 'PSS'
   then
	
      open SWV_RefCur for
      SELECT
      numShippingServiceID AS numItemID
			,vcShipmentService AS vcItemName
      FROM
      ShippingService
      WHERE
			(numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0)
      ORDER BY
      vcShipmentService;
   end if;
   RETURN;
END; $$;
       


