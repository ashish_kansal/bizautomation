-- Stored procedure definition script USP_OpportunityMaster_GetDataForEDI856Acknowledgement for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetDataForEDI856Acknowledgement(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numOppId
		,vcpOppName
   FROM
   OpportunityMaster
   WHERE
   numDomainId = v_numDomainID
   AND numOppId = v_numOppID;
END; $$;












