-- Function definition script fn_getValidEmailIds for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getValidEmailIds(v_vcEmailList VARCHAR(2000))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intCharIndex  INTEGER;
   v_vcEmailSelect  VARCHAR(100);
   v_intReturnValue  INTEGER;
BEGIN
   v_intReturnValue  := 0;
   v_vcEmailList := ',' || Replace(v_vcEmailList,' ','');
   select   POSITION(',' || vcEmail || ',' IN v_vcEmailList), MAX(vcEmail) INTO v_intCharIndex,v_vcEmailSelect FROM AdditionalContactsInformation WHERE numContactType = 92 Group by vcEmail   LIMIT 1;
			--SELECT @intCharIndex ,@vcEmailSelect
   IF v_intCharIndex >= 1 then
		
      select   POSITION(',' || vcEmail || ',' IN Replace(v_vcEmailList,v_vcEmailSelect,'')) INTO v_intCharIndex FROM AdditionalContactsInformation WHERE LENGTH(RTRIM(LTRIM(vcEmail))) >= 1    LIMIT 1;
      IF v_intCharIndex  >= 1 then
				
         v_intReturnValue :=  1;
      ELSE
         v_intReturnValue := 0;
      end if;
   ELSE
      v_intReturnValue := 0;
   end if;
   RETURN v_intReturnValue;
END; $$;

