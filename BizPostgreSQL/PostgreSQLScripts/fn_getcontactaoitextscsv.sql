CREATE OR REPLACE FUNCTION fn_GetContactAOITextsCSV(v_numContactID NUMERIC)
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ContactAOITexts  VARCHAR(500);
BEGIN
   select STRING_AGG(Aom.vcAOIName,', ') INTO v_ContactAOITexts FROM AOIContactLink Aln, AOIMaster Aom WHERE numContactID = v_numContactID AND Aln.numAOIID = Aom.numAOIID;

   IF v_ContactAOITexts Is NULL then
      v_ContactAOITexts := '';
   end if;

   RETURN v_ContactAOITexts;
END; $$;

