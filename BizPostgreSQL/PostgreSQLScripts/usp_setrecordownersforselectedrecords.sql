-- Stored procedure definition script usp_SetRecordOwnersForSelectedRecords for PostgreSQL
CREATE OR REPLACE FUNCTION usp_SetRecordOwnersForSelectedRecords(v_vcEntityIdList VARCHAR(4000),          
  v_numOwnerId NUMERIC(9,0),    
  v_bitIncludeContactRecords BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intFirstComma  INTEGER;        
   v_intFirstUnderScore  INTEGER;        
      
   v_numCompanyId  NUMERIC(9,0);      
   v_numContactId  NUMERIC(9,0);      
   v_numDivisionId  NUMERIC(9,0);      
      
   v_vcCompContDivValue  VARCHAR(30);      
   v_recordCount  NUMERIC;
BEGIN
   v_vcEntityIdList := coalesce(v_vcEntityIdList,'') || ',';      
   v_intFirstComma := POSITION(',' IN v_vcEntityIdList);        
      
   WHILE v_intFirstComma > 0 LOOP
      v_vcCompContDivValue := SUBSTR(v_vcEntityIdList,0,v_intFirstComma);
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numCompanyId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_vcCompContDivValue := SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue));
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numContactId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_numDivisionId := CAST(SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue)) AS NUMERIC(9,0));
      RAISE NOTICE '%',v_numCompanyId;
      RAISE NOTICE '%',v_numContactId;
      RAISE NOTICE '%',v_numDivisionId;
      IF v_bitIncludeContactRecords = true then
         UPDATE AdditionalContactsInformation SET
         numRecOwner = v_numOwnerId
         WHERE numDivisionId = v_numDivisionId;
      end if;
      UPDATE DivisionMaster SET
      numRecOwner = v_numOwnerId
      WHERE numCompanyID = v_numCompanyId
      AND numDivisionID = v_numDivisionId;
      GET DIAGNOSTICS v_recordCount = ROW_COUNT;
      v_vcEntityIdList := SUBSTR(v_vcEntityIdList,v_intFirstComma+1,LENGTH(v_vcEntityIdList));
      v_intFirstComma := POSITION(',' IN v_vcEntityIdList);
   END LOOP;      
   open SWV_RefCur for SELECT v_recordCount;
END; $$;












