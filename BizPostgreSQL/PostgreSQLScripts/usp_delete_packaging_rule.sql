-- Stored procedure definition script USP_DELETE_PACKAGING_RULE for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DELETE_PACKAGING_RULE(v_numPackagingRuleID NUMERIC(18,0),
	v_numDomainID	NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PackagingRules WHERE numPackagingRuleID = v_numPackagingRuleID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;



