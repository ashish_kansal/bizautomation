-- Stored procedure definition script USP_DeleteSpecDocuments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSpecDocuments(v_numSpecificDocID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM DocumentWorkflow WHERE numDocID = v_numSpecificDocID;

   delete from SpecificDocuments where numSpecificDocID = v_numSpecificDocID;
   RETURN;
END; $$;


