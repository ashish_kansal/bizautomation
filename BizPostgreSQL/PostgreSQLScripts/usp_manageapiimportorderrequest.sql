-- Stored procedure definition script USP_ManageAPIImportOrderRequest for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021



--created by Joseph
CREATE OR REPLACE FUNCTION USP_ManageAPIImportOrderRequest(v_numImportApiOrderReqId NUMERIC(9,0),
          v_numDomainID NUMERIC(9,0),
		  v_numWebApiId NUMERIC(9,0),         
		  v_vcWebApiOrderId VARCHAR(25),
          v_bitIsActive BOOLEAN,
		  v_tintRequestType SMALLINT,
		  v_dtFromDate TIMESTAMP DEFAULT null,
		  v_dtToDate TIMESTAMP DEFAULT null,
		  v_numOppId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   IF v_numImportApiOrderReqId = 0 then
  
      IF v_tintRequestType = 1 then
		
         select   COUNT(*) INTO v_Check FROM   ImportApiOrder WHERE  numDomainID = v_numDomainID AND numWebApiId = v_numWebApiId AND vcApiOrderId = v_vcWebApiOrderId AND bitIsActive = true;
      end if;
      IF v_Check = 0 then
	 
         INSERT INTO ImportApiOrder(numWebApiId,
                    numDomainID,
                    vcApiOrderId,
                    bitIsActive,
				    tintRequestType,
					dtFromDate,
					dtToDate,
                    dtCreatedDate,
					numOppId)
        VALUES(v_numWebApiId,
                    v_numDomainID,
                    v_vcWebApiOrderId,
                    v_bitIsActive,
				    v_tintRequestType,
					v_dtFromDate,
					v_dtToDate,
                    LOCALTIMESTAMP ,
					v_numOppId);
      end if;
   ELSEIF v_numImportApiOrderReqId <> 0
   then
  
      UPDATE ImportApiOrder
      SET    bitIsActive = v_bitIsActive,dtModifiedDate = LOCALTIMESTAMP
      WHERE  numDomainID = v_numDomainID AND vcApiOrderId = v_vcWebApiOrderId
      AND numWebApiId = v_numWebApiId;
   end if;
   RETURN;
END; $$;








/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/



