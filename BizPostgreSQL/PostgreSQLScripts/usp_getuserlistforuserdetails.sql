-- Stored procedure definition script usp_GetUserListForUserDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetUserListForUserDetails(v_numUserID NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select A.numContactId, coalesce(A.vcFirstName,'') || ' ' || coalesce(A.vcLastname,'') as vcGivenName
   From AdditionalContactsInformation A
   left join Domain D
   on D.numDomainId = A.numDomainID
   where A.numDivisionId = D.numDivisionId and A.numDomainID = v_numDomainID
   and A.numContactId not in(select distinct cast((numUserDetailId) as NUMERIC(18,0)) from UserMaster where numDomainID = v_numDomainID and numUserId <> v_numUserID);

/****** Object:  StoredProcedure [dbo].[usp_GetUserNameFromContacts]    Script Date: 07/26/2008 16:18:48 ******/
   RETURN;
END; $$;












