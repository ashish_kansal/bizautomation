-- Stored procedure definition script USP_DomainSFTPDetail_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DomainSFTPDetail_Insert(v_numDomainID NUMERIC(18,0)
	,v_vcUsername VARCHAR(20)
	,v_vcPassword VARCHAR(20)
	,v_tintType SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_salt  UUID DEFAULT uuid_generate_v4();
BEGIN
   INSERT INTO DomainSFTPDetail(numDomainID
		,vcUsername
		,vcPassword
		,tintType)
	VALUES(v_numDomainID
		,v_vcUsername
		,DIGEST(coalesce(v_vcPassword, '') || CAST(v_salt AS VARCHAR(36)),'SHA512')
		,v_tintType);
RETURN;
END; $$;


