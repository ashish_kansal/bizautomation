-- Stored procedure definition script USP_GetStateCountryIDs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetStateCountryIDs(v_vcState VARCHAR(50),
	--@vcCountryCode VARCHAR(50),
	v_vcCountryName VARCHAR(50),
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
		--DECLARE @vcCountryName Varchar(50)
   AS $$
   DECLARE
   v_numCountryID  NUMERIC;	
   v_numStateID  NUMERIC;

		--SET @vcCountryName = (SELECT TOP 1 vcCountryName from ShippingCountryMaster where vcCountryCode = @vcCountryCode)

BEGIN
   SELECT  numListItemID INTO v_numCountryID FROM Listdetails WHERE numListID = 40 AND (constFlag = true OR numDomainid = v_numDomainID) AND LOWER(vcData) = LOWER(v_vcCountryName) LIMIT 1;

   IF coalesce(v_numCountryID,0) = 0 AND coalesce(v_vcCountryName,'') <> '' then
		
      INSERT INTO Listdetails(numListID
					,vcData
					,numCreatedBy
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,bitDelete
					,numDomainid
					,constFlag
					,sintOrder)
				VALUES(40
					,v_vcCountryName
					,1
					,LOCALTIMESTAMP
					 ,1
					,LOCALTIMESTAMP
					 ,false
					,v_numDomainID
					,false
					,0);
				
      v_numCountryID := CURRVAL('ListDetails_seq');
   end if;

   IF coalesce(v_numCountryID,0) > 0 then
		
      SELECT  numStateID INTO v_numStateID FROM State WHERE numDomainID = v_numDomainID AND numCountryID = v_numCountryID AND (LOWER(vcState) = LOWER(v_vcState) OR LOWER(vcAbbreviations) = LOWER(v_vcState)) LIMIT 1;
      IF coalesce(v_numStateID,0) = 0 AND coalesce(v_vcState,'') <> '' then
			
         INSERT INTO State(numCountryID
					,vcState
					,numCreatedBy
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,numDomainID
					,ConstFlag
					,vcAbbreviations
					,numShippingZone)
				VALUES(v_numCountryID
					,v_vcState
					,1
					,LOCALTIMESTAMP
					 ,1
					,LOCALTIMESTAMP
					 ,v_numDomainID
					,false
					,NULL
					,NULL);
				
         v_numStateID := CURRVAL('STATE_seq');
      end if;
   end if;

		

   open SWV_RefCur for SELECT v_numCountryID AS CountryId, v_numStateID AS StateId;
END; $$;












