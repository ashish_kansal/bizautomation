-- Stored procedure definition script USP_ReportListMaster_First10SavedSearch for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_First10SavedSearch(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for SELECT 
		numSearchID as "numSearchID"
		,vcSearchName as "vcSearchName"
		,S.numFormId as "numFormId"
   FROM
   SavedSearch S
   INNER JOIN
   dynamicFormMaster DFM
   ON
   DFM.numFormID = S.numFormId
   WHERE
   numDomainID = v_numDomainID
   ORDER BY
   numSearchID ASC LIMIT 10;
END; $$;












