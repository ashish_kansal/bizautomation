-- Stored procedure definition script USP_REpProductsReceivedOrShipped for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_REpProductsReceivedOrShipped(v_numDomainID NUMERIC,                  
        v_dtFromDate TIMESTAMP,                  
        v_dtToDate TIMESTAMP,                  
        v_numUserCntID NUMERIC DEFAULT 0,                       
        v_tintType SMALLINT DEFAULT NULL,            
		v_SortOrder INTEGER DEFAULT NULL,
		INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(4000);
BEGIN
   v_strSql := 'select div1.numDivisionID,fn_GetPrimaryContact(Div1.numDivisionID) as ContactID,           
 case when tintOppType=1 then ''Shipped'' when tintOppType=2 then ''Received'' end as Event,            
 vcPOppName,numOppID, '''' as Company            
 from OpportunityMaster Opp              
 join DivisionMaster Div1            
 on Div1.numDivisionID=Opp.numDivisionID                    
  where tintshipped= 1 and (bintAccountClosingDate between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''') and          
  Opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and Div1.numTerId in(select F.numTerritory from ForReportsByTerritory F                        
  where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ')';                         
          
           
          
   if v_SortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintOppType=1';
   end if;          
   if v_SortOrder = 2 then 
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintOppType=2';
   end if;          
          
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


