-- Stored procedure definition script USP_StagePercentageDetails_GetStagesByProcess for PostgreSQL
CREATE OR REPLACE FUNCTION USP_StagePercentageDetails_GetStagesByProcess(v_numDomainID NUMERIC(18,0)                            
	,v_numProcessID NUMERIC(18,0)
	,v_vcMileStoneName VARCHAR(300),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numStageDetailsId AS "numStageDetailsId"
		,vcStageName AS "vcStageName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   AND vcMileStoneName = v_vcMileStoneName;
END; $$;












