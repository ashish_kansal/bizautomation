CREATE OR REPLACE FUNCTION usp_DeleteAssociations
(
	v_numAssociationID NUMERIC
	,v_numUserCntID NUMERIC
)
RETURNS VOID LANGUAGE plpgsql
AS $$
BEGIN
   DELETE FROM CompanyAssociations WHERE numAssociationID = v_numAssociationID;

   RETURN;
END; $$;


