-- Stored procedure definition script USP_GetRelProfile for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRelProfile(v_numRelationship NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numSecondaryListItemID as VARCHAR(255)) AS numProfileID,
        cast(L2.vcData as VARCHAR(255)) AS ProName,
        cast(numPrimaryListItemID as VARCHAR(255)) AS numRelationshipID,
        cast(L1.vcData as VARCHAR(255)) AS RelName
   FROM    FieldRelationship FR
   JOIN FieldRelationshipDTL FRDTL ON FR.numFieldRelID = FRDTL.numFieldRelID
   JOIN Listdetails L1 ON numPrimaryListItemID = L1.numListItemID
   JOIN Listdetails L2 ON numSecondaryListItemID = L2.numListItemID
   WHERE   numPrimaryListID = 5
   AND numSecondaryListID = 21
   AND FR.numDomainID = v_numDomainID
   AND numPrimaryListItemID = v_numRelationship
   AND L2.numDomainid = v_numDomainID;
END; $$; 












