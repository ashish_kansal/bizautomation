-- Stored procedure definition script USP_ItemDepreciation_UpdateRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemDepreciation_UpdateRate(v_numDomainID NUMERIC(18,0)
	,v_ID NUMERIC(18,0)
	,v_numRate NUMERIC(18,2))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monPrice  DECIMAL(20,5);
   v_numUnitHourReceived  DOUBLE PRECISION;
   v_numItemCode  NUMERIC(18,0);
   v_bitAsset  BOOLEAN;
   v_numDepreciationPeriod  NUMERIC(18,1);
   v_monResidualValue  DECIMAL(20,5);
   v_tintDepreciationMethod  SMALLINT;
   v_dtItemReceivedDate  TIMESTAMP;
   v_intYear  INTEGER;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_numItemDepreciationID  NUMERIC(18,0);
   v_monStartingValue  DECIMAL(20,5);
BEGIN
   select   I.numItemCode, coalesce(I.bitAsset,false), coalesce(IDC.numDepreciationPeriod,0), coalesce(IDC.monResidualValue,0), coalesce(IDC.tintDepreciationMethod,1), coalesce(OI.numUnitHourReceived,0), coalesce(monPrice,0), dtItemReceivedDate, coalesce(ID.intYear,0) INTO v_numItemCode,v_bitAsset,v_numDepreciationPeriod,v_monResidualValue,v_tintDepreciationMethod,
   v_numUnitHourReceived,v_monPrice,v_dtItemReceivedDate,
   v_intYear FROM
   ItemDepreciation ID
   INNER JOIN
   OpportunityItems OI
   ON
   ID.numOppItemID = OI.numoppitemtCode
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   INNER JOIN
   ItemDepreciationConfig IDC
   ON
   IDC.numItemCode = I.numItemCode WHERE
   ID.ID = v_ID;

   IF EXISTS(SELECT numFinYearId FROM FinancialYear WHERE numDomainId = v_numDomainID AND v_intYear BETWEEN EXTRACT(YEAR FROM dtPeriodFrom) AND EXTRACT(YEAR FROM dtPeriodTo) AND coalesce(bitCloseStatus,false) = true) then
	
      RAISE EXCEPTION 'FY_CLOSED';
   ELSEIF NOT EXISTS(SELECT ID FROM ItemDepreciation WHERE numItemCode = v_numItemCode AND intYear > v_intYear)
   then
	
      RAISE EXCEPTION 'CANT_EDIT_LAST_YEAR';
   ELSE
      UPDATE
      ItemDepreciation
      SET
      numRate = v_numRate,monDepreciation =(CASE WHEN v_tintDepreciationMethod = 2 THEN monStartingValue*(v_numRate/100) ELSE v_monPrice*(v_numRate/100) END),
      monEndingValue =(CASE WHEN v_tintDepreciationMethod = 2 THEN monStartingValue -(monStartingValue*(v_numRate/100)) ELSE monStartingValue -(v_monPrice*(v_numRate/100)) END)
      WHERE
      ID = v_ID;
   end if;
	
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numItemDepreciationID NUMERIC(18,0)
   );

   INSERT INTO tt_TEMP(numItemDepreciationID) SELECT ID FROM ItemDepreciation WHERE numItemCode = v_numItemCode AND intYear > v_intYear;

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;
   select   monEndingValue INTO v_monStartingValue FROM ItemDepreciation WHERE ID = v_ID;

   IF v_tintDepreciationMethod = 2 then
	
      v_numRate :=((v_monStartingValue/v_iCount::bigint)/v_monStartingValue)*100;
   ELSE
      v_numRate :=((v_monStartingValue/v_iCount::bigint)/v_monPrice)*100;
   end if;

   WHILE v_i <= v_iCount LOOP
      select   numItemDepreciationID INTO v_numItemDepreciationID FROM tt_TEMP WHERE ID = v_i;
      UPDATE
      ItemDepreciation
      SET
      monStartingValue = v_monStartingValue,numRate = v_numRate,monDepreciation =(CASE WHEN v_tintDepreciationMethod = 2 THEN v_monStartingValue*(v_numRate/100) ELSE v_monPrice*(v_numRate/100) END),
      monEndingValue =(CASE
      WHEN v_numDepreciationPeriod = 1
      THEN v_monResidualValue
      ELSE(CASE WHEN v_tintDepreciationMethod = 2 THEN v_monStartingValue -(v_monStartingValue*(v_numRate/100))  ELSE v_monStartingValue -(v_monPrice*(v_numRate/100))  END)
      END)
      WHERE
      ID = v_numItemDepreciationID;
      IF v_tintDepreciationMethod = 2 then
		
         v_monStartingValue := v_monStartingValue -(v_monStartingValue*(v_numRate/100));
      ELSE
         v_monStartingValue := v_monStartingValue -(v_monPrice*(v_numRate/100));
      end if;
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


