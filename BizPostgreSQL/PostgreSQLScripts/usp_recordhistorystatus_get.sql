-- Stored procedure definition script USP_RecordHistoryStatus_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RecordHistoryStatus_Get(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM RecordHistoryStatus WHERE Id = 1;
   RETURN;
END; $$;














