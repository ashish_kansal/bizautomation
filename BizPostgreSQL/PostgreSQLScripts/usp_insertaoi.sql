-- Stored procedure definition script usp_InsertAOI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertAOI(v_vcAOIName VARCHAR(50),
 v_numUserID NUMERIC,  
 v_numDomainID NUMERIC      
--  
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO AOIMaster  Values(v_vcAOIName, v_numDomainID,
   v_numUserID, TIMEZONE('UTC',now()), NULL, NULL, 0);
RETURN;
END; $$;


