-- Stored procedure definition script USP_GetSiteTemplateByName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSiteTemplateByName(v_vcTemplateName VARCHAR(50),
                v_numSiteID      NUMERIC(9,0),
                v_numDomainID    NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numTemplateID as VARCHAR(255)),
		   cast(vcTemplateName as VARCHAR(255)),
		   cast(txtTemplateHTML as VARCHAR(255))
   FROM   SiteTemplates
   WHERE  numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID
   AND lower(vcTemplateName) = LOWER(v_vcTemplateName);
END; $$;













