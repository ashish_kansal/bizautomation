-- Stored procedure definition script usp_GetDiv for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDiv(  
--  
v_numCompanyID NUMERIC,  
v_vcDivisionName VARCHAR(100),  
v_intLead NUMERIC DEFAULT 0,  
v_numUserCntID NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_intLead = 0 then

      open SWV_RefCur for
      select DM.vcDivisionName, CI.vcCompanyName, DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,
DM.vcBillPostCode, DM.vcBillCountry, DM.tintCRMType,  CI.numCompanyId, DM.numDivisionID
      from CompanyInfo CI, DivisionMaster DM
      where CI.numCompanyId = DM.numCompanyID
      and CI.numCompanyId = v_numCompanyID  and DM.vcDivisionName = v_vcDivisionName and DM.bitPublicFlag = false and DM.tintCRMType in(1,2);
   ELSE
      open SWV_RefCur for
      select DM.vcDivisionName, CI.vcCompanyName, DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,
DM.vcBillPostCode, DM.vcBillCountry, DM.tintCRMType,  CI.numCompanyId, DM.numDivisionID
      from CompanyInfo CI, DivisionMaster DM
      where CI.numCompanyId = DM.numCompanyID
      and CI.numCompanyId = v_numCompanyID  and DM.vcDivisionName = v_vcDivisionName and DM.bitPublicFlag = false and DM.tintCRMType in(1,2) and DM.numCreatedBy = v_numUserCntID;
   end if;
   RETURN;
END; $$;


