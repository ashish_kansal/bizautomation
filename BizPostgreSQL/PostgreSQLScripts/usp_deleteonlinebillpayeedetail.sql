CREATE OR REPLACE FUNCTION USP_DeleteOnlineBillPayeeDetail
(
	v_numBankDetailId NUMERIC(18,0),
	v_PayeeDetailId NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OnlineBillPayeeDetails
   WHERE
   numPayeeDetailId = v_PayeeDetailId
   AND numBankDetailId = v_numBankDetailId;
   RETURN;
END; $$;




