-- Stored procedure definition script USP_GetAOI for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAOI(v_numContactID NUMERIC(9,0),     
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numListItemID as VARCHAR(255)) as numAOIId,cast(vcData as VARCHAR(255)) as vcAOIName,GetAOIStatus(numListItemID,v_numContactID) as Status from Listdetails
   where  numDomainid = v_numDomainID  and numListID = 74;
END; $$;












