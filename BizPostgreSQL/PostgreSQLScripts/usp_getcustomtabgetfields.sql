CREATE OR REPLACE FUNCTION USP_GetCustomTabGetFields(v_PageId SMALLINT,                              
v_numRelation NUMERIC(9,0),                              
v_numRecordId NUMERIC(9,0),                  
v_numDomainID NUMERIC(9,0),
v_numUserCntID NUMERIC(9,0),
v_numFormID NUMERIC(9,0),
v_numTabID NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT 0, 
INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then

      if((select count(*) from DycFormConfigurationDetails DFCD where DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
      DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = v_numTabID) <> 0) then

         if v_PageId = 1 then

            open SWV_RefCur for
            select CFM.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            and DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
            DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = CGM.Grp_id
            AND CGM.Grp_id = v_numTabID
            order by TabId,DFCD.intRowNum,DFCD.intColumnNum;
         end if;                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
         IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 then

            open SWV_RefCur for
            select CFM.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            and DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
            DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = CGM.Grp_id
            AND CGM.Grp_id = v_numTabID
            UNION
            select CFM.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master  CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(1,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldID = CFM.Fld_id
            where CFM.Grp_id = 1 and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            and DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
            DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = CGM.Grp_id
            AND CGM.Grp_id = v_numTabID
            order by TabId,13,12;
         end if;
         if v_PageId = 4 then

            open SWV_RefCur for
            select CFM.Fld_id,fld_type,fld_label,numlistid,numOrder,
Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master   CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            and DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
            DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = CGM.Grp_id
            AND CGM.Grp_id = v_numTabID
            order by TabId,DFCD.intRowNum,DFCD.intColumnNum;
         end if;
         if v_PageId = 2 or  v_PageId = 3 or v_PageId = 5 or v_PageId = 6 or v_PageId = 7 or v_PageId = 8     or v_PageId = 11 then

            open SWV_RefCur for
            select CFM.Fld_id,fld_type,fld_label,numlistid,coalesce(Rec.FldDTLID,0) AS FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum
            from CFW_Fld_Master  CFM join CFw_Grp_Master   CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and CFM.numDomainID = v_numDomainID
            and DFCD.numUserCntID = v_numUserCntID and DFCD.numDomainID = v_numDomainID and DFCD.numFormID = v_numFormID and
            DFCD.numRelCntType = v_numRelation AND DFCD.tintPageType = 5 AND DFCD.numAuthGroupID = CGM.Grp_id
            AND CGM.Grp_id = v_numTabID
            order by TabId,DFCD.intRowNum,DFCD.intColumnNum;
         end if;
      ELSE
         BEGIN
            CREATE TEMP SEQUENCE tt_tempFieldsList_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPFIELDSLIST CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPFIELDSLIST
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
            fld_id NUMERIC(9,0),
            fld_type VARCHAR(20),
            fld_label VARCHAR(45),
            numListID NUMERIC(9,0),
            FldDTLID NUMERIC(9,0),
            Value TEXT,
            TabId VARCHAR(30),
            Grp_Name VARCHAR(50),
            vcURL VARCHAR(1000),
            vcToolTip VARCHAR(1000),
            RowNo INTEGER,
            intcoulmn INTEGER,
            intRowNum INTEGER
         );
         if v_PageId = 1 then

            insert into tt_TEMPFIELDSLIST(fld_id,fld_type,fld_label,numListID,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)
            select CFM.Fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            AND CGM.Grp_id = v_numTabID;
         end if;                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
         IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 then

            insert into tt_TEMPFIELDSLIST(fld_id,fld_type,fld_label,numListID,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)
            select CFM.Fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            where (CFM.Grp_id = v_PageId or CFM.Grp_id = 1) and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            AND CGM.Grp_id = v_numTabID
            Union
            select CFM.Fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(1,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            where  CFM.Grp_id = 1 and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            AND CGM.Grp_id = v_numTabID;
         end if;
         if v_PageId = 4 then

            insert into tt_TEMPFIELDSLIST(fld_id,fld_type,fld_label,numListID,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)
            select CFM.Fld_id,fld_type,fld_label,numlistid,
Rec.FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
            from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id = numFieldId
            join CFw_Grp_Master   CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and numRelation = v_numRelation and CFM.numDomainID = v_numDomainID
            AND CGM.Grp_id = v_numTabID;
         end if;
         if v_PageId = 2 or  v_PageId = 3 or v_PageId = 5 or v_PageId = 6 or v_PageId = 7 or v_PageId = 8     or v_PageId = 11 then

            insert into tt_TEMPFIELDSLIST(fld_id,fld_type,fld_label,numListID,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)
            select CFM.Fld_id,fld_type,fld_label,numlistid,coalesce(Rec.FldDTLID,0) AS FldDTLID,coalesce(Rec.Fld_Value,'0') as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
            from CFW_Fld_Master  CFM join CFw_Grp_Master   CGM on subgrp = CGM.Grp_id::VARCHAR
            left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec on Rec.Fld_ID = CFM.Fld_id
            where CFM.Grp_id = v_PageId and CFM.numDomainID = v_numDomainID
            AND CGM.Grp_id = v_numTabID;
         end if;
         Update tt_TEMPFIELDSLIST set RowNo = ID::bigint -1;
         Update tt_TEMPFIELDSLIST set  intRowNum =(CAST(RowNo::bigint AS decimal)/2)+1,intcoulmn =(MOD(RowNo,2))+1;
         open SWV_RefCur for
         SELECT * FROM tt_TEMPFIELDSLIST order by intRowNum,intcoulmn;
      end if;
   ELSE
      open SWV_RefCur for
      select Grp_id as TabID,Grp_Name AS TabName from CFw_Grp_Master where loc_Id = v_PageId and numDomainID = v_numDomainID
/*tintType stands for static tabs */ 
      AND tintType <> 2;
   end if;
/****** Object:  StoredProcedure [dbo].[usp_GetCustomTabLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
   RETURN;
END; $$;


