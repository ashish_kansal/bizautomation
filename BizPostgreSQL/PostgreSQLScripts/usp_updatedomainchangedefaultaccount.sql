-- Stored procedure definition script USP_UpdateDomainChangeDefaultAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateDomainChangeDefaultAccount(v_numDomainID NUMERIC(18,0)
	,v_numIncomeAccID NUMERIC(18,0)
	,v_numCOGSAccID NUMERIC(18,0)
	,v_numAssetAccID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Domain
   SET
   numIncomeAccID = v_numIncomeAccID,numCOGSAccID = v_numCOGSAccID,numAssetAccID = v_numAssetAccID
   WHERE
   numDomainId = v_numDomainID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/



