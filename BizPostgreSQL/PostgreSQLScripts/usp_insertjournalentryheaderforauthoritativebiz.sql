-- Stored procedure definition script USP_InsertJournalEntryHeaderForAuthoritativeBiz for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryHeaderForAuthoritativeBiz( -- Added to keep link between Project Income through TimeAndExpense Table
v_numAmount DECIMAL(20,5),                                    
INOUT v_numJournalId NUMERIC(9,0) DEFAULT 0 ,                                
v_numDomainId NUMERIC(9,0) DEFAULT 0,                        
v_numRecurringId NUMERIC(9,0) DEFAULT 0,                    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                
v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,              
v_numOppId NUMERIC(9,0) DEFAULT 0,
v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numRecurringId = 0 then 
      v_numRecurringId := null;
   end if;                                    
   IF v_numCategoryHDRID = 0 then 
      v_numCategoryHDRID := null;
   end if;
   If v_numJournalId = 0 then
  
      Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numRecurringId,numCreatedBy,datCreatedDate,numOppBizDocsId,numOppId,numCategoryHDRID)
   Values(TIMEZONE('UTC',now()),v_numAmount,v_numDomainId,v_numRecurringId,v_numUserCntID,TIMEZONE('UTC',now()),v_numOppBizDocsId,v_numOppId,v_numCategoryHDRID);
   
      v_numJournalId := CURRVAL('General_Journal_Header_seq');
      open SWV_RefCur for
      Select v_numJournalId;
   end if;
   RETURN;
END; $$;


