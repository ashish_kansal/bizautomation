-- Stored procedure definition script usp_ImportItemAmountXML for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ImportItemAmountXML(v_numOppId NUMERIC
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTotalAmt  NUMERIC;
BEGIN
   select   sum(numUnitHour*monPrice) INTO v_numTotalAmt FROM OpportunityItems WHERE numOppId = v_numOppId;
   UPDATE OpportunityMaster SET monPAmount = v_numTotalAmt WHERE numOppId = v_numOppId;
   RETURN;
END; $$;


