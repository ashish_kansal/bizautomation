-- Stored procedure definition script USP_WorkScheduleDaysOff_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkScheduleDaysOff_Delete(v_numWorkScheduleID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID;
   RETURN;
END; $$;


