CREATE OR REPLACE FUNCTION usp_getconversionpercentorders(
	v_numdomainid numeric,
	v_clienttimezoneoffset integer,
	v_vcdivisionids text,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:07:07 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT
   CONCAT(CompanyInfo.vcCompanyName,' (',FormatedDateFromDate(DivisionMaster.bintCreatedDate,v_numDomainID),')') AS vcCompanyName
		,vcpOppName
		,monDealAmount
		,fn_GetContactName(DivisionMaster.numRecOwner) AS vcRecordOwner
		,fn_GetContactName(DivisionMaster.numAssignedTo) AS vcAssignee
		,DATE_PART('day',TableFirstOrder.bintCreatedDate -DivisionMaster.bintCreatedDate) AS intDaysTookToConvert
   FROM
   DivisionMaster
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   CROSS JOIN LATERAL(SELECT 
      CONCAT(vcpOppName,' (',FormatedDateFromDate(OpportunityMaster.bintCreatedDate,v_numDomainID),')') AS vcPOppName
			,OpportunityMaster.bintCreatedDate
			,cast(coalesce(monDealAmount,0) as DECIMAL(20,5)) AS monDealAmount
      FROM
      OpportunityMaster
      WHERE
      numDomainId = v_numDomainID
      AND OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      ORDER BY
      OpportunityMaster.bintCreatedDate ASC LIMIT 1) TableFirstOrder
   WHERE
   DivisionMaster.numDomainID = v_numDomainID
   AND LENGTH(coalesce(v_vcDivisionIds,'')) > 0 AND DivisionMaster.numDivisionID IN(SELECT Id FROM SplitIDs(v_vcDivisionIds,','))
   ORDER BY
   vcCompanyName;
END;
$BODY$;
