-- Stored procedure definition script USP_GetOpportunityBizDocsDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpportunityBizDocsDetails(v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  BD.numBizDocsPaymentDetId,
            numBizDocsId,
            numPaymentMethod,
            BD.monAmount,
            numDepoistToChartAcntId,
            numDomainId,
            vcReference,
            vcMemo,
            bitAuthoritativeBizDocs,
            bitIntegratedToAcnt,
            numCreatedBy,
            dtCreationDate,
            numModifiedBy,
            dtModifiedDate,
            bitDeferredIncome,
            sintDeferredIncomePeriod,
            dtDeferredIncomeStartDate,
            NoofDeferredIncomeTransaction,
            numContractId,
            bitSalesDeferredIncome,
            numExpenseAccount,
            numDivisionID,
            numReturnID,
            tintPaymentType,
            numCurrencyID,
            fltExchangeRate,
            numCardTypeID,
            FormatedDateFromDate(coalesce(PD.dtDueDate,LOCALTIMESTAMP),BD.numDomainId) AS dtDueDate,
            coalesce(numLiabilityAccount,0) AS numLiabilityAccount,
            coalesce(numClassID,0) AS numClassID,
            coalesce(numProjectID,0) AS numProjectID,
            coalesce(numTermsID,0) AS numTermsID,
            coalesce(dtBillDate,null) AS dtBillDate
   FROM    OpportunityBizDocsDetails BD
   LEFT JOIN OpportunityBizDocsPaymentDetails PD
   ON PD.numBizDocsPaymentDetId = BD.numBizDocsPaymentDetId
   WHERE   BD.numBizDocsPaymentDetId = v_numBizDocsPaymentDetId
   AND BD.numDomainId = v_numDomainId;
END; $$;












