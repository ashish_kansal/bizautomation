
CREATE OR REPLACE FUNCTION USP_ManagePageElementDetails(v_numElementID NUMERIC(9,0),
          v_numSiteID    NUMERIC(9,0),
          v_numDomainID  NUMERIC(9,0),
          v_strItems XML  DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PageElementDetail
   WHERE       numSiteID = v_numSiteID
   AND numElementID = v_numElementID;
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      
      INSERT INTO PageElementDetail(numElementID,
                    numAttributeID,
                    vcAttributeValue,
                    numSiteID,
                    numDomainID,vcHtml)
      SELECT v_numElementID,
               X.numAttributeID,
               coalesce(X.vcAttributeValue,''),
               v_numSiteID,
               v_numDomainID,X.vcHtml FROM
		XMLTABLE('NewDataSet/Item'
         PASSING v_strItems
         COLUMNS
			id FOR ORDINALITY,
            numAttributeID NUMERIC(9,0) PATH 'numAttributeID'
			,vcAttributeValue VARCHAR(500) PATH 'vcAttributeValue'
			,vcHtml TEXT PATH 'vcHtml') AS X;
   end if;
   open SWV_RefCur for SELECT 0;
END; $$;













