-- Function definition script GetDealAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDealAmount(v_numOppID NUMERIC,v_dt TIMESTAMP,v_numOppBizDocsId NUMERIC DEFAULT 0)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);                         
                                  
   v_TaxPercentage  DOUBLE PRECISION;                    
  
   v_shippingAmt  DECIMAL(20,5);    
   v_TotalTaxAmt  DECIMAL(20,5);     
   v_decDisc  DOUBLE PRECISION; 
   v_bitDiscType  BOOLEAN;
   v_decInterest  DOUBLE PRECISION;    
    
   v_bitBillingTerms  BOOLEAN;     
   v_intBillingDays  INTEGER;    
   v_bitInterestType  BOOLEAN;    
   v_fltInterest  DOUBLE PRECISION;    
   v_dtFromDate TEXT;    
   v_strDate  TIMESTAMP;    
   v_TotalAmt  DECIMAL(20,5);    
   v_TotalAmtAfterDisc  DECIMAL(20,5);    
   v_fltDiscount  DOUBLE PRECISION;  
   v_tintOppStatus  SMALLINT;  
   v_tintshipped  SMALLINT; 
   v_dtshipped  TIMESTAMP; 
   v_numBillCountry  NUMERIC(9,0);
   v_numBillState  NUMERIC(9,0);
   v_numDomainID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;
   v_bitTaxApplicable  BOOLEAN;
   v_tintTaxOperator  SMALLINT;
BEGIN
   v_bitDiscType := false;     
   v_decInterest := 0;    
   v_decDisc := 0;    
   v_TotalAmt := 0;


--declare @monCreditAmount as DECIMAL(20,5);set @monCreditAmount=0    
   v_tintTaxOperator := 0;

   select   numDivisionId, 0, 0, 0, 0, 0, 0, tintoppstatus, tintshipped, numDomainId, tintopptype, coalesce(tintTaxOperator,0) INTO v_DivisionID,v_shippingAmt,v_bitBillingTerms,v_intBillingDays,v_bitInterestType,
   v_fltInterest,v_fltDiscount,v_tintOppStatus,v_tintshipped,v_numDomainID,
   v_tintOppType,v_tintTaxOperator from OpportunityMaster where numOppId = v_numOppID;          
   

   IF v_numOppBizDocsId = 0 then

      select   coalesce(sum(X.amount),0) INTO v_TotalAmt from(SELECT monTotAmount as Amount from OpportunityItems opp
         join Item i on opp.numItemCode = i.numItemCode where numoppid = v_numOppID
         union ALL --timeandexpense (expense)  
         SELECT monAmount as Amount from timeandexpense
         where (numOppId = v_numOppID or numOppBizDocsId in(select numOppBizDocsId from OpportunityBizDocs where numoppid = v_numOppID))
         and numCategory = 2 and numtype = 1) x;
   ELSE
      select   coalesce(bitBillingTerms,false), coalesce(intBillingDays,0), coalesce(bitInterestType,false), coalesce(fltInterest,0), coalesce(fltDiscount,0), coalesce(bitDiscountType,false) INTO v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_fltDiscount,
      v_bitDiscType FROM OpportunityMaster WHERE numOppId = v_numOppID;
	
--	@monCreditAmount=isnull(monCreditAmount,0),
      select   coalesce(monShipCost,0), dtShippedDate, dtFromDate INTO v_shippingAmt,v_dtshipped,v_dtFromDate from OpportunityBizDocs where numOppBizDocsId = v_numOppBizDocsId;
      select   coalesce(sum(X.amount),0) INTO v_TotalAmt from(SELECT OBD.monTotAmount as Amount from OpportunityItems opp
         join OpportunityBizDocItems OBD on OBD.numOppItemID = opp.numoppitemtCode
         where numOppId = v_numOppID  and OBD.numOppBizDocID = v_numOppBizDocsId
         union ALL  --timeandexpense (expense)  
         select monAmount as Amount from timeandexpense  where (numOppId = v_numOppID or numOppBizDocsId = v_numOppBizDocsId)
         and numCategory = 2 and numtype = 1) x;
   end if;

   v_TotalTaxAmt := coalesce(GetDealTaxAmount(v_numOppID,v_numOppBizDocsId,v_numDomainID),
   0);


   v_TotalAmtAfterDisc := v_TotalAmt; 
   if v_tintOppStatus = 1 then

      if v_tintshipped = 1 then 
         v_dt := coalesce(v_dtshipped,v_dt);
      end if;
      if  v_bitBillingTerms = true then
	    
		--set @strDate = DateAdd(Day, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(@intBillingDays,0)),0)),@dtFromDate)    
         v_strDate := v_dtFromDate:: timestamp+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(v_intBillingDays,0)),0) AS INTEGER) || 'day' as interval);    
		--(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	     
         if v_bitInterestType = false then
		
            if v_strDate > v_dt then
			
               v_decDisc := 0;
            else
               v_decDisc := v_fltInterest;
            end if;
         ELSE
            if v_strDate > v_dt then
			
               v_decInterest := 0;
            else
               v_decInterest := v_fltInterest;
            end if;
         end if;
      end if;
      if v_decDisc > 0 then
	
         if v_bitDiscType = false then 
            v_TotalAmtAfterDisc := v_TotalAmt -((v_TotalAmt*v_fltDiscount/100)+((v_TotalAmt*(100 -v_fltDiscount)/100)*v_decDisc/100))+v_shippingAmt+v_TotalTaxAmt;
         ELSEIF v_bitDiscType = true
         then  
            v_TotalAmtAfterDisc := v_TotalAmt -(v_fltDiscount+(v_TotalAmt -v_fltDiscount)*v_decDisc/100)+v_shippingAmt+v_TotalTaxAmt;
         end if;
      else
         if v_bitDiscType = false then 	
            v_TotalAmtAfterDisc := v_TotalAmt+(v_decInterest*(v_TotalAmt*(100 -v_fltDiscount)/100)/100) -(v_TotalAmt*v_fltDiscount/100)+v_shippingAmt+v_TotalTaxAmt;
         ELSEIF v_bitDiscType = true
         then 	
            v_TotalAmtAfterDisc := v_TotalAmt+(v_decInterest*(v_TotalAmt -v_fltDiscount)/100) -(v_fltDiscount)+v_shippingAmt+v_TotalTaxAmt;
         end if;
      end if;
   else
      if v_bitDiscType = false then 	
         v_TotalAmtAfterDisc := v_TotalAmt+v_shippingAmt+v_TotalTaxAmt -(v_TotalAmt*v_fltDiscount/100);
      ELSEIF v_bitDiscType = true
      then 	
         v_TotalAmtAfterDisc := v_TotalAmt+v_shippingAmt+v_TotalTaxAmt -v_fltDiscount;
      end if;
   end if;

 --SET @TotalAmtAfterDisc = @TotalAmtAfterDisc - isnull(@monCreditAmount,0)
   v_TotalAmtAfterDisc := ROUND(v_TotalAmtAfterDisc,2);

   return v_TotalAmtAfterDisc; -- Set Accuracy of Two precision
END; $$;

