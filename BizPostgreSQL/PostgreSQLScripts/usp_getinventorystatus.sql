-- Stored procedure definition script USP_GetInventoryStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetInventoryStatus(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 1 AS vcValue,CAST('Not Applicable' AS CHAR(14)) AS vcInventoryStatus
   UNION
   SELECT 2 AS vcValue,CAST('Allocation Cleared' AS CHAR(18)) AS vcInventoryStatus
   UNION
   SELECT 3 AS vcValue,CAST('Back Order' AS CHAR(10)) AS vcInventoryStatus
   UNION
   SELECT 4 AS vcValue,CAST('Ready to Ship' AS CHAR(13)) AS vcInventoryStatus;
END; $$;

        












