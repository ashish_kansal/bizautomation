CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetFieldValue(v_numDomainID NUMERIC(18,0),    
	v_numOppID NUMERIC(18,0),
	v_vcFieldName VARCHAR(100), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'numDivisionID' then
	
      open SWV_RefCur for
      SELECT numDivisionId FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
   ELSEIF v_vcFieldName = 'vcPOppName'
   then
	
      open SWV_RefCur for
      SELECT coalesce(vcpOppName,'') FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
   ELSE
      RAISE EXCEPTION 'FIELD_NOT_FOUND';
   end if;
   RETURN;
END; $$;


