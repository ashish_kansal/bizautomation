CREATE OR REPLACE FUNCTION USP_DeleteWarehouseLocation(v_numDomainID NUMERIC(18,0),
    v_numWLocationID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWLocationID = v_numWLocationID) then
	
      RAISE EXCEPTION 'WAREHOUSE_LOCATION_USED_FOR_ITEM';
      RETURN;
   ELSE
      DELETE FROM WarehouseLocation WHERE numDomainID = v_numDomainID AND numWLocationID = v_numWLocationID;
   end if;
END; $$; 


