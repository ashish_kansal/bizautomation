-- Function definition script GetCustFldValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldValue(v_numFldId NUMERIC(9,0),v_pageId SMALLINT,v_numRecordId NUMERIC(9,0))
RETURNS VARCHAR(1000) LANGUAGE plpgsql PARALLEL SAFE
   AS $$
   DECLARE
   v_Fld_type  VARCHAR(100);
   v_vcURL  TEXT;
   v_vcValue  VARCHAR(1000);
BEGIN
   select coalesce(fld_type,''), coalesce(vcURL,'') INTO v_Fld_type,v_vcURL FROM CFW_Fld_Master WHERE Fld_id = v_numFldId;

	IF v_pageId = -1 THEN
		select grp_id INTO v_pageId FROM CFW_Fld_Master WHERE Fld_id = v_numFldId;
	END IF;

    
   if v_pageId = 1 then
      select   Fld_Value INTO v_vcValue from CFW_FLD_Values where Fld_ID = v_numFldId and RecId = v_numRecordId;
   elseif v_pageId = 4 then
      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Cont where fld_id = v_numFldId and RecId = v_numRecordId;
  elseif v_pageId = 3 then
      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Case where Fld_ID = v_numFldId and RecId = v_numRecordId;
  elseif v_pageId = 5 then
      select   Fld_Value INTO v_vcValue from CFW_FLD_Values_Item where Fld_ID = v_numFldId and RecId = v_numRecordId;
  elseif v_pageId = 2 or v_pageId = 6 then
      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Opp where Fld_ID = v_numFldId and RecId = v_numRecordId;
  elseif v_pageId = 7 or v_pageId = 8 then
      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Product where Fld_ID = v_numFldId and RecId = v_numRecordId;
   elseif v_pageId = 9 then
		IF EXISTS (SELECT * FROM ItemAttributes WHERE Fld_ID = v_numFldId AND numItemCode = v_numRecordId) THEN
			SELECT Fld_Value INTO v_vcValue FROM ItemAttributes WHERE Fld_ID = v_numFldId AND numItemCode = v_numRecordId;
		ELSE
			select Fld_Value INTO v_vcValue from CFW_Fld_Values_Serialized_Items where Fld_ID = v_numFldId and RecId = v_numRecordId;
		END IF;
  elseif v_pageId = 11 then
      select   Fld_Value INTO v_vcValue from CFW_Fld_Values_Pro where Fld_ID = v_numFldId and RecId = v_numRecordId;
  end if;
  
   if v_vcValue is null then 
      v_vcValue :=(CASE WHEN LOWER(v_Fld_type) = LOWER('Link') THEN v_vcURL 
						WHEN LOWER(v_Fld_type) = LOWER('SelectBox') OR LOWER(v_Fld_type) = LOWER('CheckBox') THEN '0' 
						ELSE '' 
					END);
   end if;
    
	IF LOWER(v_Fld_type) = LOWER('SelectBox') AND NOT isnumeric(v_vcValue) THEN
		v_vcValue := '0';
	END IF;

   RETURN v_vcValue;
END; $$;

