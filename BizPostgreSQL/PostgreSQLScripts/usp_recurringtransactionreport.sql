-- Stored procedure definition script USP_RecurringTransactionReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecurringTransactionReport(v_numDomainID NUMERIC(9,0),
	v_CurrentPage INTEGER,                                                              
	v_PageSize INTEGER,                                                              
	INOUT v_TotRecs INTEGER , INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql 
	-- Add the parameters for the stored procedure here
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                              
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS  tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE  
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numRecTranRepId NUMERIC(9,0)  
   );  


   insert into tt_TEMPTABLE(numRecTranRepId)
   select numRecTranRepId from RecurringTransactionReport RTR INNER JOIN OpportunityMaster OM ON OM.numOppId = coalesce(RTR.numRecTranOppID,RTR.numRecTranSeedId)
   WHERE OM.numDomainId = v_numDomainID
   UNION
   SELECT numOppRecID FROM OpportunityRecurring WHERE tintRecurringType = 2 AND numDomainID = v_numDomainID;


   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                             
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                              
                                                             
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;

   RAISE NOTICE '%',v_firstRec;
   RAISE NOTICE '%',v_lastRec;

   open SWV_RefCur for
   SELECT X.* FROM(SELECT OPR.numOppRecID AS numRecTranRepId,
       OPR.numOppBizDocID AS numRecTranSeedId,
       OPR.numOppId AS numRecTranOppID,
       OBD.numOppBizDocsId,
	   OPR.tintRecurringType AS tintRecType,
       CAST('Recurring Biz Doc' AS VARCHAR(7)) AS TranType,
        RT.varRecurringTemplateName,
        OBD.monDealAmount AS TotalCostAmount,
       FormatedDateFromDate(OPR.dtRecurringDate,OM.numDomainId) AS dteOppCreationDate,
       OBD.vcBizDocID AS RecurringName,
       OPR.numRecurringId,
       0 AS Frequency,
       (SELECT  dtRecurringDate FROM OpportunityRecurring WHERE numOppId = OM.numOppId ORDER BY numOppRecID asc LIMIT 1) AS StartDate,
       (SELECT  dtRecurringDate FROM OpportunityRecurring WHERE numOppId = OM.numOppId ORDER BY numOppRecID asc LIMIT 1) AS EndDate,
       (SELECT COUNT(*) AS dtRecurringDate FROM OpportunityRecurring WHERE numOppId = OM.numOppId AND numOppBizDocID > 0) AS numNoTransactions,
       (SELECT COUNT(*) AS dtRecurringDate FROM OpportunityRecurring WHERE numOppId = OM.numOppId) AS ExpectedChildRecords
      FROM   OpportunityRecurring AS OPR
      INNER JOIN OpportunityMaster AS OM
      ON OPR.numOppId = OM.numOppId
      INNER JOIN RecurringTemplate AS RT
      ON OPR.numRecurringId = RT.numRecurringId
      INNER JOIN OpportunityBizDocs AS OBD
      ON OM.numOppId = OBD.numoppid
      AND OBD.numOppBizDocsId = OPR.numOppBizDocID
      WHERE OPR.numDomainID = v_numDomainID
      AND OPR.tintRecurringType = 2
      UNION
      SELECT   RTR.numRecTranRepId,
         RTR.numRecTranSeedId,
         RTR.numRecTranOppID,
         CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
         RTR.tintRecType,
         (CASE
      WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
      WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
      ELSE 'Unknown'
      END) AS TranType,
         RecurringTemplate.varRecurringTemplateName,
         GetDealAmount(o1.numOppId,TIMEZONE('UTC',now()),0::NUMERIC) AS TotalCostAmount,
         FormatedDateFromDate(RTR.dteOppCreationDate,o1.numDomainId) AS dteOppCreationDate,
         o1.vcpOppName AS RecurringName,
         OPR.numRecurringId,
         0 AS Frequency,
         LOCALTIMESTAMP  AS StartDate,
         LOCALTIMESTAMP  AS  EndDate,
         OPR.numNoTransactions,
         CAST(0 AS INTEGER) AS ExpectedChildRecords
      FROM     RecurringTransactionReport RTR
      INNER JOIN OpportunityMaster AS o1
      ON RTR.numRecTranOppID = o1.numOppId
      INNER JOIN OpportunityMaster AS o2
      ON RTR.numRecTranSeedId = o2.numOppId
      LEFT OUTER JOIN OpportunityRecurring OPR
      ON OPR.numOppId = o2.numOppId
      INNER JOIN RecurringTemplate
      ON OPR.numRecurringId = RecurringTemplate.numRecurringId
      AND o1.numDomainId = v_numDomainID
      AND o2.numDomainId = v_numDomainID
      WHERE o1.numDomainId = v_numDomainID) AS X
--			LEFT OUTER JOIN #tempTable
--				ON X.numRecTranRepId = #tempTable.numRecTranRepId
--					AND ID > @firstRec
--					AND ID < @lastRec
   ORDER BY X.dteOppCreationDate DESC;
   drop table IF EXISTS  tt_TEMPTABLE CASCADE;
   open SWV_RefCur2 for
   select  v_TotRecs as TotRecs;
   RETURN;
END; $$;
 



