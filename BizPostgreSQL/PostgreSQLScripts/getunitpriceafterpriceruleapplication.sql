DROP FUNCTION IF EXISTS GetUnitPriceAfterPriceRuleApplication;
CREATE OR REPLACE FUNCTION GetUnitPriceAfterPriceRuleApplication(v_numRuleID NUMERIC(18,0), 
	v_numDomainID NUMERIC(18,0), 
	v_numItemCode NUMERIC(18,0), 
	v_numQuantity DOUBLE PRECISION, 
	v_numWarehouseItemID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0))
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_finalUnitPrice  DOUBLE PRECISION DEFAULT 0;
   v_ItemPrice  DOUBLE PRECISION DEFAULT 0;
   v_tintRuleType  INTEGER;
   v_tintVendorCostType SMALLINT;
   v_tintPricingMethod  INTEGER;
   v_tintDiscountType  INTEGER;
   v_decDiscount  DOUBLE PRECISION;
   v_decMaxDiscount  DOUBLE PRECISION;
   v_tempDecMaxDiscount  DOUBLE PRECISION;
   v_decDiscountPerQty  INTEGER;
   v_monCalculatePrice  DECIMAL(20,5);
   v_bitRoundTo BOOLEAN;
   v_tintRoundTo SMALLINT;
   v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
BEGIN
	SELECT 
		tintPricingMethod,tintRuleType,tintVendorCostType,tintDiscountType,decDiscount,decMaxDedPerAmt,intQntyItems,bitRoundTo,tintRoundTo  
	INTO 
		v_tintPricingMethod,v_tintRuleType,v_tintVendorCostType,v_tintDiscountType,v_decDiscount,v_decMaxDiscount,v_decDiscountPerQty,v_bitRoundTo,v_tintRoundTo 
	FROM
		PriceBookRules 
	WHERE 
		numPricRuleID = v_numRuleID;

   DROP TABLE IF EXISTS tt_TEMPPRICE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPRICE
   (
      bitSuccess BOOLEAN,
      monPrice DECIMAL(20,5)
   );

   If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then

      INSERT INTO tt_TEMPPRICE(bitSuccess
		,monPrice)
      SELECT
      bitSuccess
		,monPrice
      FROM
      fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQuantity,v_numWarehouseItemID,
      (SELECT coalesce(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode),0,0,'',
      0,1);
      IF(SELECT bitSuccess FROM tt_TEMPPRICE) = true then
	
         SELECT monPrice INTO v_monCalculatePrice FROM tt_TEMPPRICE;
      end if;
   end if;

   IF v_tintPricingMethod = 1 then --Use Pricing Table

      SELECT * INTO v_finalUnitPrice FROM GetUnitPriceAfterPriceLevelApplication(v_numDomainID := v_numDomainID,v_numItemCode := v_numItemCode,v_numQuantity := v_numQuantity,
      v_numWarehouseItemID := v_numWarehouseItemID,v_isPriceRule := 1::BOOLEAN,
      v_numPriceRuleID := v_numRuleID,v_numDivisionID := 0);
   ELSEIF v_tintPricingMethod = 2
   then --Use Pricing Formula

      IF v_tintRuleType = 1 then --Deduct from List price
	
         IF(SELECT coalesce(charItemType,'') FROM Item WHERE numItemCode = v_numItemCode) = 'P' then
            If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then -- Kit OR Assembly
               v_ItemPrice := v_monCalculatePrice;
            ELSE
               select   coalesce(monWListPrice,0) INTO v_ItemPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numWarehouseItemID;
            end if;
         ELSE
            select   coalesce(monListPrice,0) INTO v_ItemPrice FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
         end if;

		/* If price > 0 then apply rule calculation */
         IF v_ItemPrice > 0 then
		
            IF v_decDiscount > 0 then
			
               IF v_numQuantity < v_decDiscountPerQty then
                  v_tempDecMaxDiscount := 1*v_decDiscount;
               ELSE
                  v_tempDecMaxDiscount :=(v_numQuantity/v_decDiscountPerQty::bigint)*v_decDiscount;
               end if;
               IF v_decMaxDiscount > 0 AND v_tempDecMaxDiscount > v_decMaxDiscount then
                  v_tempDecMaxDiscount := v_decMaxDiscount;
               end if;
               IF v_tintDiscountType = 1 then --Percentage
				
                  v_finalUnitPrice := v_ItemPrice -(v_ItemPrice*(v_tempDecMaxDiscount/100));
               ELSEIF v_tintDiscountType = 2
               then --Flat Amount
				
                  v_finalUnitPrice := v_ItemPrice -v_tempDecMaxDiscount;
               end if;
            end if;
         end if;
      ELSEIF v_tintRuleType = 2
      then --Add to Primary Vendor Cost
	
         If(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND (bitKitParent = true OR bitAssembly = true)) > 0 then -- Kit OR Assembly
            v_ItemPrice := v_monCalculatePrice;
         ELSE
            select   coalesce(monCost,0) INTO v_ItemPrice FROM Vendor WHERE numItemCode = v_numItemCode AND numVendorID =(SELECT numVendorID FROM Item WHERE numItemCode = v_numItemCode);

			IF EXISTS (SELECT 
							VendorCostTable.numvendorcosttableid
						FROM 
							Item
						INNER JOIN
							Vendor 
						ON
							Item.numVendorID = Vendor.numVendorID
						INNER JOIN 
							VendorCostTable 
						ON 
							Vendor.numvendortcode=VendorCostTable.numvendortcode 
						WHERE
							Item.numItemCode = v_numItemCode
							AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
							AND COALESCE(v_numQuantity,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit)) THEN
				SELECT 
					COALESCE(VendorCostTable.monDynamicCost,v_ItemPrice)
					,COALESCE(VendorCostTable.monStaticCost,v_ItemPrice)
				INTO 
					v_monVendorDynamicCost
					,v_monVendorStaticCost
				FROM 
					Vendor 
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Vendor.numDomainID=v_numDomainID
					AND Vendor.numVendorID=v_numDivisionID 
					AND Vendor.numItemCode=v_numItemCode 
					AND VendorCostTable.numCurrencyID = (SELECT numCurrencyID FROM Domain WHERE numDomainID=v_numDomainID)
					AND COALESCE(v_numQuantity,0) BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit);
			ELSE
				v_monVendorDynamicCost := v_ItemPrice;
				v_monVendorStaticCost := v_ItemPrice;
			END IF;
         end if;
		/* If price > 0 then apply rule calculation */
         IF v_ItemPrice > 0 then
		
            IF v_decDiscount > 0 then
			
               IF v_numQuantity < v_decDiscountPerQty then
                  v_tempDecMaxDiscount := 1*v_decDiscount;
               ELSE
                  v_tempDecMaxDiscount :=(v_numQuantity/v_decDiscountPerQty::bigint)*v_decDiscount;
               end if;
               IF v_decMaxDiscount > 0 AND v_tempDecMaxDiscount > v_decMaxDiscount then
                  v_tempDecMaxDiscount := v_decMaxDiscount;
               end if;
               IF v_tintDiscountType = 1 then --Percentage
				
                  v_finalUnitPrice := (CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)+((CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)*(v_tempDecMaxDiscount/100));
               ELSEIF v_tintDiscountType = 2
               then --Flat Amount
				
                  v_finalUnitPrice := (CASE 
										WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
										WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
										ELSE v_ItemPrice 
									END)+v_tempDecMaxDiscount;
               end if;
            end if;
         end if;
      end if;
   end if;


   return (CASE 
				WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
				THEN ROUND(coalesce(v_finalUnitPrice,0)) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
				ELSE coalesce(v_finalUnitPrice,0) 
			END);
END; $$;

