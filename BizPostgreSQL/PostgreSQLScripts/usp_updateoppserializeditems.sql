-- Stored procedure definition script USP_UpdateOppSerializedItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOppSerializedItems(v_numOppItemTcode NUMERIC(9,0) DEFAULT 0,  
v_numOppID NUMERIC(9,0) DEFAULT 0,  
v_strItems TEXT DEFAULT '',
v_OppType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_rows  INTEGER;
BEGIN
	if v_OppType = 2 then
		UPDATE 
			WareHouseItmsDTL 
		SET
			numWareHouseItemID = X.numWareHouseItemID
			,vcSerialNo = X.vcSerialNo
		From
		(
			SELECT 
				numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag
			FROM
			XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(100) PATH 'vcSerialNo'
			) AS TEMP
			WHERE 
				TEMP.Op_Flag=2 
				AND WareHouseItmsDTLID = X.WareHouseItmsDTLID) X;

			DELETE FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID
			FROM 
			XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(100) PATH 'vcSerialNo'
			) AS X 
			WHERE 
				X.Op_Flag=3);

			DELETE FROM OppWarehouseSerializedItem WHERE numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID
			FROM XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(100) PATH 'vcSerialNo'
			) AS X 
			WHERE 
				X.Op_Flag=2);

			DELETE FROM OppWarehouseSerializedItem WHERE numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID
      FROM XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(100) PATH 'vcSerialNo'
			) AS X 
			WHERE 
				X.Op_Flag=3);

			Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID)(SELECT numWareHouseItmsDTLID,v_numOppID,v_numOppItemTcode,numWareHouseItemID
         FROM XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					vcSerialNo VARCHAR(100) PATH 'vcSerialNo'
			) AS X 
			WHERE 
				X.Op_Flag=2);

      select   count(*) INTO v_rows FROM 
	  XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(100) PATH 'Fld_Value',
					RecId NUMERIC(9,0) PATH 'RecId'
			) AS X;
	  
      if v_rows > 0 then

         BEGIN
            CREATE TEMP SEQUENCE tt_tempTable_seq;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         drop table IF EXISTS tt_TEMPTABLE CASCADE;
         Create TEMPORARY TABLE tt_TEMPTABLE 
         ( 
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
            Fld_ID NUMERIC(9,0),
            Fld_Value VARCHAR(100),
            RecId NUMERIC(9,0),
            bitSItems BOOLEAN
         );
         insert into tt_TEMPTABLE(Fld_ID,Fld_Value,RecId,bitSItems)
			SELECT * FROM 
			XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(100) PATH 'Fld_Value',
					RecId NUMERIC(9,0) PATH 'RecId',
					bitSItems BOOLEAN PATH 'bitSItems'
			) AS X;
		 
		 
         DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID and C.RecId = T.RecId and bitSerialized = bitSItems;
         drop table IF EXISTS tt_TEMPTABLE CASCADE;
         insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)
         select Fld_ID,coalesce(Fld_Value,'') as Fld_Value,RecId,bitSItems from XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(100) PATH 'Fld_Value',
					RecId NUMERIC(9,0) PATH 'RecId',
					bitSItems BOOLEAN PATH 'bitSItems'
			) AS X;
      end if;
   ELSEIF v_OppType = 1
   then

      update WareHouseItmsDTL set tintStatus = 0 where numWareHouseItmsDTLID in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID = v_numOppItemTcode);
      delete from OppWarehouseSerializedItem where numOppItemID = v_numOppItemTcode;
      Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty) SELECT numWareHouseItmsDTLID,v_numOppID,v_numOppItemTcode,numWareHouseItemID,UsedQty
         FROM
		 XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					numWareHouseItemID NUMERIC(9,0) PATH 'numWareHouseItemID',
					UsedQty NUMERIC(9,0) PATH 'UsedQty'
			) AS X;

      update WareHouseItmsDTL set tintStatus = 1 where numWareHouseItmsDTLID in(SELECT numWareHouseItmsDTLID
      FROM 
	  XMLTABLE
			(
				'NewDataSet/SerializedItems'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numWareHouseItmsDTLID NUMERIC(9,0) PATH 'numWareHouseItmsDTLID',
					UsedQty NUMERIC(9,0) PATH 'UsedQty',
					TotalQty NUMERIC(9,0) PATH 'TotalQty'
			) AS X WHERE(TotalQty -UsedQty) = 0);
   end if;  
  

   RETURN;
END; $$;


