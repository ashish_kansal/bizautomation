-- Stored procedure definition script USP_GetAllStates for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAllStates(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numStateID as VARCHAR(255)),cast(vcState as VARCHAR(255)),cast(numCountryID as VARCHAR(255)),cast(vcData as VARCHAR(255)),cast(vcAbbreviations as VARCHAR(255)) from State
   join Listdetails on numListItemID =  numCountryID
   where State.numDomainID = v_numDomainID
   order by  vcState;
END; $$;












