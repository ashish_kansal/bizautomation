-- Stored procedure definition script USP_SaveLayoutList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveLayoutList(v_numUserCntID NUMERIC DEFAULT 0,                                  
 v_numDomainID NUMERIC DEFAULT 0,                         
v_strRow TEXT DEFAULT '',                      
v_intcoulmn NUMERIC DEFAULT 0,   
v_numFormID NUMERIC DEFAULT 0,                          
v_numRelCntType NUMERIC DEFAULT 0,
  v_tintPageType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc4  INTEGER;
BEGIN
   delete  FROM DycFormConfigurationDetails where numUserCntID = v_numUserCntID and numDomainID = v_numDomainID
   and intColumnNum >= v_intcoulmn and  numRelCntType = v_numRelCntType AND numFormID = v_numFormID
   AND tintPageType = v_tintPageType  AND coalesce(bitDefaultByAdmin,false) = false;
  
                        
   if SUBSTR(CAST(v_strRow AS VARCHAR(10)),1,10) <> '' then

		insert into DycFormConfigurationDetails
		(
			numFormID,numFieldID,intRowNum,intColumnNum,numUserCntID,numDomainID,numRelCntType,bitCustom,tintPageType
		)
		select 
			v_numFormID,numFieldID,tintRow,intColumn,numUserCntId,numDomainID,numRelCntType,bitCustomField,v_tintPageType 
		FROM  
			XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC(9,0) PATH 'numFieldID',
					tintRow NUMERIC(9,0) PATH 'tintRow',
					intColumn NUMERIC(9,0) PATH 'intColumn',
					numUserCntId NUMERIC(9,0) PATH 'numUserCntId',
					numDomainID NUMERIC(9,0) PATH 'numDomainID',
					numRelCntType NUMERIC(9,0) PATH 'numRelCntType',
					bitCustomField BOOLEAN PATH 'bitCustomField'
			) AS X 
		WHERE 
			numFieldID NOT IN (SELECT 
									numFieldID 
								FROM 
									DycFormConfigurationDetails 
								WHERE 
									numUserCntID = v_numUserCntID 
									AND numDomainID = v_numDomainID
									AND intColumnNum >= v_intcoulmn 
									AND numRelCntType = v_numRelCntType 
									AND numFormID = v_numFormID
									AND tintPageType = v_tintPageType 
									AND coalesce(bitDefaultByAdmin,false) = true);


		UPDATE 
			DycFormConfigurationDetails AS D  
		SET 
			intRowNum = tintRow
			,intColumnNum = intColumn
		FROM 
			XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC(9,0) PATH 'numFieldID',
					tintRow NUMERIC(9,0) PATH 'tintRow',
					intColumn NUMERIC(9,0) PATH 'intColumn',
					numUserCntId NUMERIC(9,0) PATH 'numUserCntId',
					numDomainID NUMERIC(9,0) PATH 'numDomainID',
					numRelCntType NUMERIC(9,0) PATH 'numRelCntType',
					bitCustomField BOOLEAN PATH 'bitCustomField'
			) AS X  
		WHERE 
			X.numFieldID = D.numFieldID 
			AND (X.numFieldID IN (SELECT numFieldID FROM DycFormConfigurationDetails where numUserCntID = v_numUserCntID and numDomainID = v_numDomainID
      and  numRelCntType = v_numRelCntType AND numFormID = v_numFormID
      AND tintPageType = v_tintPageType  AND coalesce(bitDefaultByAdmin,false) = true) AND tintPageType = v_tintPageType AND
      D.numUserCntID = v_numUserCntID and D.numDomainID = v_numDomainID
      and  D.numRelCntType = v_numRelCntType AND numFormId = v_numFormID
      AND tintPageType = v_tintPageType  AND coalesce(bitDefaultByAdmin,false) = true);
   end if;                          

   RETURN;
END; $$;


