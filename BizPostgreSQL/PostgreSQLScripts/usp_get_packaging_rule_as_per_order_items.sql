CREATE OR REPLACE FUNCTION USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
(
	v_numShipClass NUMERIC(18,0),
	v_numPackageTypeID NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_numDomainID NUMERIC(18,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSQL  TEXT;
	v_strWhere  TEXT;
	v_numQtyToCheck  NUMERIC(18,0);
BEGIN
	SELECT 
		CASE 
			WHEN numFromQty < v_numQty THEN ' ORDER BY numFromQty DESC'
			WHEN numFromQty > v_numQty THEN ' ORDER BY numFromQty ASC'
			WHEN numFromQty = v_numQty THEN ' ORDER BY numFromQty ASC'
			ELSE ' AND 1=1 '
		END INTO v_strWhere 
	FROM 
		PackagingRules PR
	LEFT JOIN CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
	WHERE
		PR.numDomainID = v_numDomainID
		AND PR.numShipClassId = v_numShipClass
		AND CP.numCustomPackageID = v_numPackageTypeID;

	v_strSQL := 'SELECT DISTINCT 
					numPackagingRuleID
					,vcRuleName
					,numPackagingTypeID
					,numFromQty
					,numShipClassID
					,numDomainID
					,CP.numCustomPackageID
					,CP.fltHeight
					,CP.fltLength
					,CP.fltTotalWeight
					,CP.fltWidth
					,CP.numPackageTypeID
					,CP.vcPackageName 
				FROM 
					PackagingRules PR    
				LEFT JOIN 
					CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
				WHERE 
					PR.numDomainID = ' ||  v_numDomainID ||
					' AND PR.numShipClassID = ' || v_numShipClass ||
					' AND CP.numCustomPackageID = ' || v_numPackageTypeID; 				

	v_strSQL := coalesce(v_strSQL,'') || ' ' || coalesce(v_strWhere,' AND 1=1 '); 
   
	OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   
	RETURN; 
END; $$;

