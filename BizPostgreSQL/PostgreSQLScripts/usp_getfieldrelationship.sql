-- Stored procedure definition script USP_GetFieldRelationship for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetFieldRelationship(v_byteMode SMALLINT,
v_numFieldRelID NUMERIC(18,0),
v_numDomainID NUMERIC(18,0),
v_numModuleID NUMERIC(18,0),
v_numPrimaryListID NUMERIC(18,0),
v_numPrimaryListItemID NUMERIC(18,0),
v_numSecondaryListID NUMERIC(18,0),
v_bitTaskRelation BOOLEAN, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 1 then
	
      IF(v_bitTaskRelation = true) then
		
         open SWV_RefCur for
         SELECT
			FR.numFieldRelID AS "numFieldRelID",
			FR.numPrimaryListID AS "numPrimaryListID",
			SD.vcStageName as "RelationshipName"
         FROM
         FieldRelationship AS FR
         LEFT JOIN
         StagePercentageDetails AS SD
         ON
         FR.numPrimaryListID = SD.numStageDetailsId
         WHERE
         FR.numDomainID = v_numDomainID
         AND coalesce(FR.bitTaskRelation,false) = true
         AND 1 =(CASE WHEN coalesce(v_numModuleID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(numModuleID,0) = v_numModuleID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numPrimaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FR.numPrimaryListID,0) = v_numPrimaryListID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numSecondaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FR.numSecondaryListID,0) = v_numSecondaryListID THEN 1 ELSE 0 END) END);
      ELSE
         open SWV_RefCur for
         SELECT
			 numFieldRelID AS "numFieldRelID"
			 ,L1.vcListName || ' - ' || L2.vcListName as "RelationshipName"
         FROM
         FieldRelationship
         JOIN
         listmaster L1
         ON
         L1.numListID = numPrimaryListID
         JOIN
         listmaster L2
         ON
         L2.numListID = numSecondaryListID
         WHERE
         FieldRelationship.numDomainID = v_numDomainID
         AND coalesce(FieldRelationship.numModuleID,0) <> 14
         AND coalesce(bitTaskRelation,false) = false
         AND 1 =(CASE WHEN coalesce(v_numModuleID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FieldRelationship.numModuleID,0) = v_numModuleID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numPrimaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FieldRelationship.numPrimaryListID,0) = v_numPrimaryListID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numSecondaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FieldRelationship.numSecondaryListID,0) = v_numSecondaryListID THEN 1 ELSE 0 END) END)
         UNION
         SELECT
         numFieldRelID,L1.vcItemName || ' - ' || L2.vcItemName as "RelationshipName"
         FROM
         FieldRelationship
         JOIN
         Item L1
         ON
         L1.numItemCode = numPrimaryListID
         JOIN
         Item L2
         ON
         L2.numItemCode = numSecondaryListID
         WHERE
         FieldRelationship.numDomainID = v_numDomainID
         AND coalesce(FieldRelationship.numModuleID,0) = 14
         AND coalesce(bitTaskRelation,false) = false
         AND 1 =(CASE WHEN coalesce(v_numModuleID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(numModuleID,0) = v_numModuleID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numPrimaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FieldRelationship.numPrimaryListID,0) = v_numPrimaryListID THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(v_numSecondaryListID,0) = 0 THEN 1 ELSE(CASE WHEN coalesce(FieldRelationship.numSecondaryListID,0) = v_numSecondaryListID THEN 1 ELSE 0 END) END);
      end if;
   ELSEIF v_byteMode = 2
   then
	
      open SWV_RefCur for
      SELECT
		numFieldRelID AS "numFieldRelID"
		,coalesce(FieldRelationship.numModuleID,0) AS "numModuleID"
		,numPrimaryListID AS "numPrimaryListID"
		,numSecondaryListID AS "numSecondaryListID"
		,L1.vcListName || ' - ' || L2.vcListName AS "RelationshipName"
		,L1.vcListName AS "Item1"
		,L2.vcListName AS "Item2"
      FROM
      FieldRelationship
      JOIN
      listmaster L1
      ON
      L1.numListID = numPrimaryListID
      JOIN
      listmaster L2
      ON
      L2.numListID = numSecondaryListID
      WHERE
      numFieldRelID = v_numFieldRelID
      AND coalesce(FieldRelationship.numModuleID,0) <> 14
      AND coalesce(bitTaskRelation,false) = false
      UNION
      SELECT
      numFieldRelID AS "numFieldRelID"
			,coalesce(numModuleID,0) AS "numModuleID"
			,numPrimaryListID AS "numPrimaryListID"
			,numSecondaryListID AS "numSecondaryListID"
			,L1.vcItemName || ' - ' || L2.vcItemName AS "RelationshipName"
			,L1.vcItemName as "Item1"
			,L2.vcItemName as "Item2"
      FROM
      FieldRelationship
      JOIN
      Item L1
      ON
      L1.numItemCode = numPrimaryListID
      JOIN
      Item L2
      ON
      L2.numItemCode = numSecondaryListID
      WHERE
      numFieldRelID = v_numFieldRelID
      AND coalesce(numModuleID,0) = 14
      AND coalesce(bitTaskRelation,false) = false;

      IF v_bitTaskRelation = true then
		
         open SWV_RefCur2 for
         SELECT
			FieldRelationship.numFieldRelID AS "numFieldRelID"
			,numPrimaryListItemID AS "numPrimaryListItemID"
			,numSecondaryListItemID AS "numSecondaryListItemID"
			,numFieldRelDTLID,SLI1.vcTaskName as "PrimaryListItem"
			,SLI2.vcTaskName as "SecondaryListItem"
         FROM
         FieldRelationship
         JOIN
         FieldRelationshipDTL
         ON
         FieldRelationshipDTL.numFieldRelID = FieldRelationship.numFieldRelID
         JOIN
         StagePercentageDetailsTask SLI1
         ON
         SLI1.numTaskId = numPrimaryListItemID
         JOIN
         StagePercentageDetailsTask SLI2
         ON
         SLI2.numTaskId = numSecondaryListItemID
         WHERE
         FieldRelationship.numFieldRelID = v_numFieldRelID
         AND numPrimaryListItemID = v_numPrimaryListItemID
         AND coalesce(bitTaskRelation,false) = true;
      ELSE
         open SWV_RefCur2 for
         SELECT
			FieldRelationship.numFieldRelID AS "numFieldRelID"
			,numFieldRelDTLID AS "numFieldRelDTLID"
			,numPrimaryListItemID AS "numPrimaryListItemID"
			,numSecondaryListItemID AS "numSecondaryListItemID"
			,L1.vcData AS "PrimaryListItem"
			,L2.vcData AS "SecondaryListItem"
         FROM
         FieldRelationship
         JOIN
         FieldRelationshipDTL
         ON
         FieldRelationshipDTL.numFieldRelID = FieldRelationship.numFieldRelID
         JOIN
         Listdetails L1
         ON
         L1.numListItemID = numPrimaryListItemID
         JOIN
         Listdetails L2
         ON
         L2.numListItemID = numSecondaryListItemID
         WHERE
         FieldRelationship.numFieldRelID = v_numFieldRelID
         AND (coalesce(v_numPrimaryListItemID,0) = 0 OR numPrimaryListItemID = v_numPrimaryListItemID)
         AND coalesce(numModuleID,0) <> 14
         AND coalesce(bitTaskRelation,false) = false
         UNION
         SELECT
         FieldRelationship.numFieldRelID AS "numFieldRelID"
				,numFieldRelDTLID AS "numFieldRelDTLID"
				,numPrimaryListItemID AS "numPrimaryListItemID"
				,numSecondaryListItemID AS "numSecondaryListItemID"
				,L1.vcItemName AS "PrimaryListItem"
				,L2.vcItemName AS "SecondaryListItem"
         FROM
         FieldRelationship
         JOIN
         FieldRelationshipDTL
         ON
         FieldRelationshipDTL.numFieldRelID = FieldRelationship.numFieldRelID
         JOIN
         Item L1
         ON
         L1.numItemCode = numPrimaryListItemID
         JOIN
         Item L2
         ON
         L2.numItemCode = numSecondaryListItemID
         WHERE
         FieldRelationship.numFieldRelID = v_numFieldRelID
         AND (coalesce(v_numPrimaryListItemID,0) = 0 OR numPrimaryListItemID = v_numPrimaryListItemID)
         AND coalesce(numModuleID,0) = 14
         AND coalesce(bitTaskRelation,false) = false
         AND coalesce(numSecondaryListItemID,0) <> -1
         UNION
         SELECT
         FieldRelationship.numFieldRelID AS "numFieldRelID"
				,numFieldRelDTLID AS "numFieldRelDTLID"
				,numPrimaryListItemID AS "numPrimaryListItemID"
				,numSecondaryListItemID AS "numSecondaryListItemID"
				,L1.vcItemName AS "PrimaryListItem"
				,'Do not display' AS "SecondaryListItem"
         FROM
         FieldRelationship
         JOIN
         FieldRelationshipDTL
         ON
         FieldRelationshipDTL.numFieldRelID = FieldRelationship.numFieldRelID
         JOIN
         Item L1
         ON
         L1.numItemCode = numPrimaryListItemID
         WHERE
         FieldRelationship.numFieldRelID = v_numFieldRelID
         AND (coalesce(v_numPrimaryListItemID,0) = 0 OR numPrimaryListItemID = v_numPrimaryListItemID)
         AND coalesce(numModuleID,0) = 14
         AND coalesce(bitTaskRelation,false) = false
         AND coalesce(numSecondaryListItemID,0) = -1;
      end if;
   ELSEIF v_byteMode = 3
   then
	
		open SWV_RefCur for
		Select 
			L2.numListItemID AS "numListItemID"
			,L2.vcData as "SecondaryListItem"  
		from FieldRelationship
		Join FieldRelationshipDTL
		on FieldRelationshipDTL.numFieldRelID = FieldRelationship.numFieldRelID
		Join Listdetails L2
		on L2.numListItemID = numSecondaryListItemID
		where FieldRelationship.numDomainID = v_numDomainID and numPrimaryListItemID = v_numPrimaryListItemID
		and FieldRelationship.numSecondaryListID = v_numSecondaryListID AND coalesce(bitTaskRelation,false) = false ORDER BY L2.sintOrder;
   end if;
   RETURN;
END; $$;


