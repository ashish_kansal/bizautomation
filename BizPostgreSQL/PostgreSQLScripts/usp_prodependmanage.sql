-- Stored procedure definition script USP_ProDependManage for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProDependManage(v_byteMode SMALLINT DEFAULT null,
v_strDependency TEXT DEFAULT '',
v_numProId NUMERIC(9,0) DEFAULT null,
v_numProStageId NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_byteMode = 0 then
      delete from ProjectsDependency where numProId = v_numProId and numProStageId = v_numProStageId;
  
      insert into  ProjectsDependency(numProId,
	numProStageId,
	numProcessStageId,
	vcType)
		select v_numProId,v_numProStageId,X.numProcessStageId,X.vcType
		from
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strDependency AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numProcessStageId NUMERIC(9,0) PATH 'numProcessStageId',
				vcType VARCHAR(1) PATH 'vcType'
		) X;
   end if;

   if v_byteMode = 1 then

      open SWV_RefCur for select cast(dep.numProcessStageId as VARCHAR(255)),cast(dtl.vcstagedetail as VARCHAR(255)),vcType,
	'Milestone - ' || CAST(dtl.numStagePercentage AS VARCHAR(4)) || '%' as numstagepercentage
	,mst.vcpOppName as vcPOppName,case when dtl.bitstagecompleted = false then '<font color=red>Pending</font>'  when dtl.bitstagecompleted = true then '<font color=blue>Completed</font>' end as Status
      from ProjectsDependency dep
      join OpportunityStageDetails dtl
      on dtl.numOppStageId = dep.numProcessStageId
      join OpportunityMaster mst on
      dtl.numoppid = mst.numOppId
      where dep.numProId = v_numProId and dep.numProStageId = v_numProStageId and vcType = 'O'
      union
      select cast(dep.numProcessStageId as VARCHAR(255)),cast(dtl.vcstagedetail as VARCHAR(255)),vcType,
	'Milestone - ' || CAST(dtl.numstagepercentage AS VARCHAR(4)) || '%' as numstagepercentage ,
	cast(mst.vcProjectName as VARCHAR(100)) as vcPOppName,
	case when dtl.bitStageCompleted = false then '<font color=red>Pending</font>'  when dtl.bitStageCompleted = true then '<font color=blue>Completed</font>' end as Status
      from ProjectsDependency dep
      join ProjectsStageDetails dtl
      on dtl.numProStageId = dep.numProcessStageId
      join ProjectsMaster mst on
      dtl.numProId = mst.numProId
      where  vcType = 'P' and dep.numProId = v_numProId and dep.numProStageId = v_numProStageId;
   end if;
END; $$;












