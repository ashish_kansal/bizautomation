-- Stored procedure definition script USP_GETEmailFromDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GETEmailFromDetail(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcEmail AS FromEmail,
           coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS FromName
   FROM   AdditionalContactsInformation
   WHERE  numContactId IN(SELECT numAdminID
      FROM   Domain
      WHERE  numDomainId = v_numDomainID);
                            
  --SELECT * FROM [UserMaster] WHERE [numUserDetailId] = [numAdminID]
END; $$;












