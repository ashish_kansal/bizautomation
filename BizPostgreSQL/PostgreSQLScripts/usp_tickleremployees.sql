-- Stored procedure definition script USP_TicklerEmployees for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TicklerEmployees(v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_intType INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_UserID  NUMERIC(9,0);
BEGIN
   select   numUserId INTO v_UserID FROM    UserMaster WHERE   numUserDetailId = v_numUserCntId;    
               
   open SWV_RefCur for SELECT  CONCAT(vcUserName,',',CAST(numUserDetailID AS VARCHAR(15)),',',vcEmailID) AS numUserId,
            CONCAT(vcFirstName,' ',vcLastname) AS Name
   FROM    AdditionalContactsInformation
   JOIN UserMaster ON numContactId = numUserDetailID
   JOIN UserTeams ON UserMaster.numDomainID = UserTeams.numDomainID
   AND AdditionalContactsInformation.numContactId = UserTeams.numUserCntID
   WHERE   UserTeams.numTeam IN(SELECT numTeam 
      FROM   UserTeams
      WHERE  numUserCntID = v_numUserCntId)
   AND numUserId <> v_UserID
   AND UserMaster.numDomainID = v_numDomainID
   UNION
   SELECT  CONCAT(vcUserName,',',CAST(numUserDetailID AS VARCHAR(15)),',',vcEmailID) AS numUserId,
            CAST('Myself' AS VARCHAR(50)) AS Name
   FROM    AdditionalContactsInformation
   JOIN UserMaster ON numContactId = numUserDetailID
   WHERE   numUserId = v_UserID
   AND UserMaster.numDomainID = v_numDomainID;
END; $$;












