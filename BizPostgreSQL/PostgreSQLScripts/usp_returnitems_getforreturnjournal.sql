-- Stored procedure definition script USP_ReturnItems_GetForReturnJournal for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReturnItems_GetForReturnJournal(v_numReturnHeaderID NUMERIC(18,0),
    v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   coalesce(RH.numDivisionId,0) AS numDivisionId
		,numReturnItemID
		,RI.numItemCode
		,coalesce(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,coalesce(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,coalesce(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID = RI.numOppBizDocItemID),
      coalesce(I.monAverageCost,'0')) END) as AverageCost
		,CAST((coalesce(monTotAmount,0)/RI.numUnitHour) AS DECIMAL(20,5)) AS monUnitSalePrice
		,coalesce(I.bitKitParent,false) AS bitKitParent
		,0 AS tintChildLevel
   FROM
   ReturnHeader RH
   LEFT JOIN ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
   LEFT JOIN OpportunityMaster OM ON OM.numOppId = RH.numOppId AND RH.numDomainID = OM.numDomainId
   LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
   LEFT JOIN DivisionMaster D ON RH.numDivisionId = D.numDivisionID
   LEFT JOIN CompanyInfo C2 ON C2.numCompanyId = D.numCompanyID
   LEFT JOIN OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
   LEFT JOIN AdditionalContactsInformation con ON con.numDivisionId = RH.numDivisionId AND con.numContactId = RH.numContactID
   LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
   WHERE
   RH.numDomainID = v_numDomainId
   AND RH.numReturnHeaderID = v_numReturnHeaderID
   UNION ALL
   SELECT
   coalesce(RH.numDivisionId,0) AS numDivisionId
		,numReturnItemID
		,RI.numItemCode
		,coalesce(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,coalesce(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,coalesce(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived*coalesce(OKI.numQtyItemsReq_Orig,0) AS numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(OBDKI.monAverageCost,coalesce(I.monAverageCost,0)) END) as AverageCost
		,CAST(0 AS DECIMAL(20,5)) AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,coalesce(I.bitKitParent,false) AS bitKitParent
		,1 AS tintChildLevel
   FROM
   ReturnHeader RH
   INNER JOIN ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
   INNER JOIN OpportunityBizDocKitItems OBDKI ON RI.numOppBizDocItemID = OBDKI.numOppBizDocItemID
   INNER JOIN OpportunityKitItems OKI ON OBDKI.numOppChildItemID = OKI.numOppChildItemID
   INNER JOIN OpportunityMaster OM ON OM.numOppId = RH.numOppId AND RH.numDomainID = OM.numDomainId
   INNER JOIN Item I ON I.numItemCode = OBDKI.numChildItemID
   LEFT JOIN DivisionMaster D ON RH.numDivisionId = D.numDivisionID
   LEFT JOIN CompanyInfo C2 ON C2.numCompanyId = D.numCompanyID
   LEFT JOIN OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
   LEFT JOIN AdditionalContactsInformation con ON con.numDivisionId = RH.numDivisionId AND con.numContactId = RH.numContactID
   LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
   WHERE
   RH.numDomainID = v_numDomainId
   AND RH.numReturnHeaderID = v_numReturnHeaderID
   AND coalesce(I.bitKitParent,false) = false
   UNION ALL
   SELECT
   coalesce(RH.numDivisionId,0) AS numDivisionId
		,numReturnItemID
		,RI.numItemCode
		,coalesce(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,coalesce(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,coalesce(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived*coalesce(OKCI.numQtyItemsReq_Orig,0) AS numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(OBDKCI.monAverageCost,coalesce(I.monAverageCost,0)) END) as AverageCost
		,CAST(0 AS DECIMAL(20,5)) AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,coalesce(I.bitKitParent,false) AS bitKitParent
		,2 AS tintChildLevel
   FROM
   ReturnHeader RH
   INNER JOIN ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
   INNER JOIN OpportunityBizDocKitChildItems OBDKCI ON RI.numOppBizDocItemID = OBDKCI.numOppBizDocItemID
   INNER JOIN OpportunityKitChildItems OKCI ON OBDKCI.numOppKitChildItemID = OKCI.numOppKitChildItemID
   INNER JOIN OpportunityMaster OM ON OM.numOppId = RH.numOppId AND RH.numDomainID = OM.numDomainId
   INNER JOIN Item I ON I.numItemCode = OBDKCI.numChildItemID
   LEFT JOIN DivisionMaster D ON RH.numDivisionId = D.numDivisionID
   LEFT JOIN CompanyInfo C2 ON C2.numCompanyId = D.numCompanyID
   LEFT JOIN OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
   LEFT JOIN AdditionalContactsInformation con ON con.numDivisionId = RH.numDivisionId AND con.numContactId = RH.numContactID
   LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
   WHERE
   RH.numDomainID = v_numDomainId
   AND RH.numReturnHeaderID = v_numReturnHeaderID
   AND coalesce(I.bitKitParent,false) = false;
END; $$;












