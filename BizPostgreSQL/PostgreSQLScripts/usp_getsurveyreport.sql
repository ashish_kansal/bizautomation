-- Stored procedure definition script usp_GetSurveyReport for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyReport(v_numSurveyID NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This SP will get the report for a particular Survey.

	--Declare variables for the SurveyQuestions.
   AS $$
   DECLARE
   v_vcSurName  VARCHAR(50);
   v_numQID  NUMERIC;
   v_vcQuestion  VARCHAR(100);
   v_tintAnsType  SMALLINT;
   v_numAnsID  NUMERIC;
   v_vcAnsLabel  VARCHAR(100);
   v_intCount  INTEGER;
   v_numRespondantID  NUMERIC;
   v_vcAnsText  VARCHAR(500);
   SurveyMaster_cursor CURSOR FOR 
   SELECT vcSurName, numQID, vcQuestion, tintAnstype, 
			numAnsID, vcAnsLabel
   FROM vw_SurveyQuestions
   WHERE numSurID = v_numSurveyID
   ORDER BY numQID;
   TextAns_cursor CURSOR
   FOR SELECT numRespondantID, vcAnsText 
   FROM vw_SurveyResponses 
   WHERE numAnsID = v_numAnsID;

	--Create A Temp Table for the Resultset.
BEGIN
   DROP TABLE IF EXISTS tt_TMPSRQ CASCADE;
   CREATE TEMPORARY TABLE tt_TMPSRQ 
   (
      numSurveyID NUMERIC,
      vcSurveyName VARCHAR(50),
      numQID NUMERIC,
      vcQuestion VARCHAR(100),
      numAnsID NUMERIC,
      vcAnsLabel VARCHAR(100),
      tintAnsType SMALLINT,
      intCount INTEGER,
      numRespondantID NUMERIC,
      vcAnsText VARCHAR(500)
   );
	
	--Create Cursor for the Questions.
	

	--Open The Cursor.
   OPEN SurveyMaster_cursor;

	--Loop thru the Cursor and get the data from answers.
   FETCH NEXT FROM SurveyMaster_cursor
   INTO v_vcSurName,v_numQID,v_vcQuestion,v_tintAnsType,v_numAnsID,v_vcAnsLabel;

	    --No Error
   WHILE FOUND LOOP
		--Set the Default value of the Count to 0.
      v_intCount := 0;
      IF v_tintAnsType = 0 OR v_tintAnsType = 1 then -- CheckBox OR Radio Buttons.
		
			--Get the Count For the Answer.
         select   COUNT(numAnsID) INTO v_intCount FROM vw_SurveyResponses WHERE numAnsID = v_numAnsID;

			--Insert the data into the Temp Table.
         INSERT INTO tt_TMPSRQ  values(v_numSurveyID, v_vcSurName, v_numQID, v_vcQuestion, v_numAnsID,
				v_vcAnsLabel, v_tintAnsType, v_intCount, 0, CAST('' AS VARCHAR(500)));
      ELSE 
			--Create a new Cursor for the Text Box Values.
			

			--Open the Cursor.
			 -- Text Box
         OPEN TextAns_cursor;
	
			--Get the First Record.
         FETCH NEXT FROM TextAns_cursor
         INTO v_numRespondantID,v_vcAnsText;

			--Loop thru all the records and insert into Temp Table.
			    --No Error
         WHILE FOUND LOOP
				--Insert the data into the Temp Table.
            INSERT INTO tt_TMPSRQ  values(v_numSurveyID, v_vcSurName, v_numQID, v_vcQuestion, v_numAnsID,
					v_vcAnsLabel, v_tintAnsType, 0, v_numRespondantID, v_vcAnsText);
				--Get the Next Record.	
				
            FETCH NEXT FROM TextAns_cursor
            INTO v_numRespondantID,v_vcAnsText;
         END LOOP;
			
			--Close the Cursor and dispose the cursor.
         CLOSE TextAns_cursor;
      end if;
			
		--Get the Next Record
      FETCH NEXT FROM SurveyMaster_cursor
      INTO v_vcSurName,v_numQID,v_vcQuestion,v_tintAnsType,v_numAnsID,v_vcAnsLabel;
   END LOOP;
	--Close the Cursor and dispose the cursor.
   CLOSE SurveyMaster_cursor;
	

	--Return the Resulting Recordset frm teh Temp Table.
   open SWV_RefCur for SELECT * FROM tt_TMPSRQ;
END; $$;












