-- Function definition script CheckAllItemQuantityAddedToBizDoc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckAllItemQuantityAddedToBizDoc(v_numOppID NUMERIC(18,0),
	  v_numOppBizDocID NUMERIC(18,0))
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_isAllItemWithQuantityIncluded  BOOLEAN DEFAULT 0;

	
	-- check if all quantity of for each item is added to bizdoc
BEGIN
   IF(SELECT
   SUM(QtyDifference) AS QtyDifference
   FROM(SELECT
      OpportunityItems.numOppId,
					(coalesce(OpportunityItems.numUnitHour,0) -coalesce(TEMP1.numUnitHour,0)) AS QtyDifference
      FROM
      OpportunityItems
      LEFT JOIN(SELECT
         TEMP2.numOppItemID,
							SUM(TEMP2.numUnitHour) AS numUnitHour
         FROM(SELECT
            OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID AND bitAuthoritativeBizDocs = 1) AS TEMP2
         GROUP BY
         TEMP2.numOppItemID) AS TEMP1
      ON
      OpportunityItems.numoppitemtCode = TEMP1.numOppItemID
      WHERE
      OpportunityItems.numOppId = v_numOppID AND
      numoppitemtCode In(SELECT numOppItemID FROM OpportunityBizDocItems WHERE OpportunityBizDocItems.numOppBizDocID = v_numOppBizDocID)) AS TEMP
   GROUP BY
   TEMP.numOppId) > 0 then
	
      v_isAllItemWithQuantityIncluded := false;
   ELSE
      v_isAllItemWithQuantityIncluded := true;
   end if;
	
   RETURN v_isAllItemWithQuantityIncluded;
END; $$;

