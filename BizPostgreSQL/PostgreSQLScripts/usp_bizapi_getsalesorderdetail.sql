-- Stored procedure definition script USP_BizAPI_GetSalesOrderDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 April 2014
-- Description:	Gets details of sales order created between supplied date
-- =============================================
CREATE OR REPLACE FUNCTION USP_BizAPI_GetSalesOrderDetail(v_numDomainID NUMERIC(18,0),
	v_dtFromDate TIMESTAMP,
	v_dtToDate TIMESTAMP DEFAULT NULL,
	v_numPageIndex INTEGER DEFAULT 0,
    v_numPageSize INTEGER DEFAULT 0,
    INOUT v_TotalRecords INTEGER  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
   IF v_dtToDate IS NOT NULL then
      v_dtToDate := v_dtToDate+INTERVAL '1 day';
   end if;
	
   select   COUNT(*) INTO v_TotalRecords FROM
   OpportunityMaster WHERE
   tintopptype = 1 AND
   OpportunityMaster.numDomainId = v_numDomainID AND
   OpportunityMaster.bintCreatedDate >= v_dtFromDate AND
			(OpportunityMaster.bintCreatedDate <= v_dtToDate OR v_dtToDate IS NULL);
	
	
   open SWV_RefCur for
   SELECT * FROM(SELECT
      ROW_NUMBER() OVER(ORDER BY OpportunityMaster.numOppId asc) AS RowNumber,
				OpportunityMaster.numOppId,
				OpportunityMaster.vcpOppName,
				CampaignMaster.numCampaignID,
				CampaignMaster.vcCampaignName,
				CompanyInfo.numCompanyId,
				CompanyInfo.vcCompanyName,
				OpportunityMaster.numContactId,
				fn_GetContactName(OpportunityMaster.numContactId) AS numContactIdName,
				OpportunityMaster.tintSource,
				fn_GetOpportunitySourceValue(coalesce(OpportunityMaster.tintSource,0),coalesce(OpportunityMaster.tintSourceType,0)::SMALLINT,
      OpportunityMaster.numDomainId) AS tintSourceName ,
				fn_getOPPAddress(OpportunityMaster.numOppId,v_numDomainID,1::SMALLINT) AS BillingAddress ,
				fn_getOPPAddress(OpportunityMaster.numOppId,v_numDomainID,2::SMALLINT) AS ShippingAddress,
				GetDealAmount(OpportunityMaster.numOppId,TIMEZONE('UTC',now()),0::NUMERIC) AS CalAmount,
				CAST(coalesce(monPAmount,0) AS DECIMAL(10,2)) AS monPAmount,
				coalesce(tintshipped,0) AS tintshipped,
				OpportunityMaster.bintCreatedDate,
				OpportunityMaster.bintClosedDate,
				OpportunityMaster.numassignedto,
				(SELECT
         A.vcFirstName || ' ' || A.vcLastname
         FROM
         AdditionalContactsInformation A
         WHERE
         A.numContactId = OpportunityMaster.numassignedto) AS numAssignedToName,
				coalesce(Currency.varCurrSymbol,'') AS varCurrSymbol ,
				coalesce(OpportunityMaster.fltExchangeRate,0) AS fltExchangeRate ,
				coalesce(Currency.chrCurrency,'') AS chrCurrency ,
				coalesce(Currency.numCurrencyID,0) AS numCurrencyID,
				(SELECT
         vcData
         FROM
         Listdetails
         WHERE
         numListItemID = OpportunityMaster.numStatus) AS numStatusName,
		COALESCE((SELECT string_agg(CONCAT('<Item',' ItemCode="',OpportunityItems.numItemCode,'"'
                            , ' ItemName="',OpportunityItems.vcItemName,'"'
							, ' ItemType="',OpportunityItems.vcType ,'"'
							, ' ModelID="',OpportunityItems.vcModelID ,'"'
							, ' Manufacturer="',OpportunityItems.vcManufacturer ,'"'
							, ' UPC="',Item.numBarCodeId ,'"'
							, ' SKU="',Item.vcSKU ,'"'
							, ' Description="',Item.txtItemDesc ,'"'
							, ' WarehouseID="',Warehouses.numWareHouseID ,'"'
							, ' Warehouse="',Warehouses.vcWareHouse ,'"'
							, ' Quantity="',OpportunityItems.numUnitHour ,'"'
							, ' UnitPrice="',OpportunityItems.monPrice ,'"'
							, ' TotalAmount="',OpportunityItems.monTotAmount ,'"','/>'),'')
            FROM
            OpportunityItems
            LEFT JOIN
            Item
            ON
            OpportunityItems.numItemCode = Item.numItemCode
            LEFT JOIN
            WareHouseItems
            ON
            OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
            LEFT JOIN
            Warehouses
            ON
            WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
            WHERE
            numOppId = OpportunityMaster.numOppId),'<Item />') AS Items
      FROM
      OpportunityMaster
      INNER JOIN
      AdditionalContactsInformation ADC
      ON
      OpportunityMaster.numContactId = ADC.numContactId
      INNER JOIN
      DivisionMaster Div
      ON
      OpportunityMaster.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
      INNER JOIN
      CompanyInfo cmp
      ON
      Div.numCompanyID = cmp.numCompanyId
      LEFT JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      LEFT JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      LEFT JOIN
      CampaignMaster
      ON
      OpportunityMaster.numCampainID = CampaignMaster.numCampaignID
      LEFT JOIN
      Currency
      ON
      OpportunityMaster.numCurrencyID = Currency.numCurrencyID
      WHERE
      tintopptype = 1 AND
      tintoppstatus = 1 AND
      OpportunityMaster.numDomainId = v_numDomainID AND
      OpportunityMaster.bintCreatedDate >= v_dtFromDate AND
				(OpportunityMaster.bintCreatedDate <= v_dtToDate OR v_dtToDate IS NULL)) AS I
   WHERE
				(v_numPageIndex = 0 OR v_numPageSize = 0) OR
				(RowNumber >((v_numPageIndex::bigint -1)*v_numPageSize::bigint) and RowNumber <((v_numPageIndex::bigint*v_numPageSize::bigint)+1));
   RETURN;
END; $$;


