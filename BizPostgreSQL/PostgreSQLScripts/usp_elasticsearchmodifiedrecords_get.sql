-- Stored procedure definition script USP_ElasticSearchModifiedRecords_Get for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ElasticSearchModifiedRecords_Get(INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID NOT IN(SELECT numDomainId FROM Domain WHERE bitElasticSearch = true);

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      vcModule VARCHAR(15),
      numRecordID NUMERIC(18,0),
      vcAction VARCHAR(10)
   );
   INSERT INTO tt_TEMP(ID,numDomainID,vcModule,numRecordID,vcAction) SELECT  ID,numDomainID,vcModule,numRecordID,vcAction FROM ElasticSearchModifiedRecords ORDER BY ID LIMIT 8000;
   DELETE FROM ElasticSearchModifiedRecords WHERE ID IN(SELECT ID FROM tt_TEMP);

   open SWV_RefCur for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM tt_TEMP T1 INNER JOIN Domain ON T1.numDomainID = Domain.numDomainId WHERE coalesce(Domain.bitElasticSearch,false) = true AND vcAction = 'Insert' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID;
   open SWV_RefCur2 for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM tt_TEMP T1 INNER JOIN Domain ON T1.numDomainID = Domain.numDomainId WHERE coalesce(Domain.bitElasticSearch,false) = true AND vcAction = 'Update' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID;
   open SWV_RefCur3 for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM tt_TEMP T1 INNER JOIN Domain ON T1.numDomainID = Domain.numDomainId WHERE coalesce(Domain.bitElasticSearch,false) = true AND vcAction = 'Delete' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/



