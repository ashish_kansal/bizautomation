CREATE OR REPLACE FUNCTION usp_GetListValues(  
--
v_numDomainID NUMERIC,
	v_numMainListID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numMainListID = 1 then 
	--Tertritory
	
      open SWV_RefCur for
      SELECT numTerID As "numID", vcTerName As "vcName"
      FROM TerritoryMaster
      WHERE numDomainid = v_numDomainID
      ORDER BY vcTerName;
   end if;

   IF v_numMainListID = 3 then 
	--Annual Revenue. List Item ID = 6
	
      open SWV_RefCur for
      SELECT numListItemID As "numID", vcData As "vcName"
      FROM Listdetails
      WHERE numListID = 6
      AND numDomainid = v_numDomainID;
   end if;

   IF v_numMainListID = 4 then 
	--No Of Employees. List Item ID = 7
	
      open SWV_RefCur for
      SELECT numListItemID As "numID", vcData As "vcName"
      FROM Listdetails
      WHERE numListID = 7
      AND numDomainid = v_numDomainID;
   end if;
   RETURN;
END; $$;


