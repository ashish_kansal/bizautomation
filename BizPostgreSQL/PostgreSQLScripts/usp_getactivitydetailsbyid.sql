-- Stored procedure definition script usp_GetActivityDetailsById for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetActivityDetailsById(v_ActivityID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
	SELECT 
		ac.Duration AS "timeInSecounds"
		,ac.ActivityID AS "ActivityID"
		,ac.StartDateTimeUtc AS "StartDateTimeUtc"
		,ac.StartDateTimeUtc+CAST(ac.Duration || 'second' as interval) AS "EndDateTime"
		,ACI.numContactId AS "numContactId"
		,ac.Subject AS "Subject"
		,coalesce(fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),ACI.numContactId),'') AS "AssignEmail"
		,ACI.numDivisionId AS "ActivityID"
		,coalesce(bitTimeAddedToContract,false) AS "bitTimeAddedToContract"
		,coalesce(fn_GetContactName(ACI.numContactId),'') AS "AssignName"
		,fn_SecondsConversion(coalesce((SELECT  cast(coalesce(CAST(timeLeft AS INTEGER),0) as INTEGER) FROM Contracts WHERE numDomainId = ACI.numDomainID AND numDivisonId = ACI.numDivisionId AND intType = 1 LIMIT 1),0)) AS "timeLeft"
		,cast(coalesce((SELECT  cast(coalesce(CAST(timeLeft AS INTEGER),0) as INTEGER) FROM Contracts WHERE numDomainId = ACI.numDomainID AND numDivisonId = ACI.numDivisionId AND intType = 1 LIMIT 1),0) as INTEGER) AS "timeLeftinSec"
   From Activity ac join
   ActivityResource ar on  ac.ActivityID = ar.ActivityID
   join Resource res on ar.ResourceID = res.ResourceID
   join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId
   JOIN DivisionMaster Div
   ON ACI.numDivisionId = Div.numDivisionID
   JOIN CompanyInfo  Comp
   ON Div.numCompanyID = Comp.numCompanyId
   WHERE ac.ActivityID = v_ActivityID;
END; $$;












