CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_CanBillAgainstVendorInvoice(v_numVendorInvoiceBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityBizDocItems
   WHERE
   numOppBizDocID = v_numVendorInvoiceBizDocID
   AND coalesce(numUnitHour,0) -coalesce(numVendorInvoiceUnitReceived,0) > 0) > 0 then
	
		-- IF VENDOR INVOICE QTY IS PENDING TO RECEIVE THAN BILL CAN NOT BE CREATED AGAINST VENDOR INVOICE
      open SWV_RefCur for
      SELECT 0;
   ELSEIF(SELECT
   COUNT(*)
   FROM(SELECT
      OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS VendorInvoiceQty
					,OtherBills.BilledQty
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numoppitemtCode = OBDI.numOppItemID
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID
         AND coalesce(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1) AS OtherBills on TRUE
      WHERE
      OBD.numOppBizDocsId = v_numVendorInvoiceBizDocID) X
   WHERE
   coalesce(X.VendorInvoiceQty,0) >(coalesce(X.OrderedQty,0) -coalesce(X.BilledQty,0))) > 0
   then
	
		-- IF SOMEONE HAS ALREADY CREATED BILL AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF VENDOR INVOICE THAN BILL CAN NOT BE CREATED AGAINST VENDOR INVOICE
      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;

	


