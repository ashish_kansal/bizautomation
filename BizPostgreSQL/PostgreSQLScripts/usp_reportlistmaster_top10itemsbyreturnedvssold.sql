-- Stored procedure definition script USP_ReportListMaster_Top10ItemsByReturnedVsSold for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ItemsByReturnedVsSold(v_numDomainID NUMERIC(18,0),
	v_numRecordCount INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT I.vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',I.numItemCode,'&frm=All Items') AS URL
		,SUM(RI.numUnitHour)
		,SUM(OI.numUnitHour)
		,(SUM(RI.numUnitHour)*100)/(CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) AS ReturnPercent
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   ReturnItems RI
   ON
		(RI.numOppItemID = OI.numoppitemtCode
   OR RI.numReturnItemID = I.numItemCode)
   LEFT JOIN
   ReturnHeader RH
   ON
   RI.numReturnHeaderID = RH.numReturnHeaderID
   WHERE
   OM.numDomainId = v_numDomainID
   AND RH.numDomainID = v_numDomainID
   AND RH.tintReturnType IN(1,3)
   AND coalesce(tintopptype,0) = 1
   AND coalesce(tintoppstatus,0) = 1
   GROUP BY
   I.numItemCode,I.vcItemName
   ORDER BY(SUM(RI.numUnitHour)*100)/(CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) DESC,I.numItemCode ASC;
END; $$;












