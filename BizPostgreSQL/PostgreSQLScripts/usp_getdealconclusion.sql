-- Stored procedure definition script usp_GetDealConclusion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDealConclusion(             
--          
v_numDomainID NUMERIC,          
        v_dtFromDate TIMESTAMP,          
        v_dtToDate TIMESTAMP,          
        v_numUserCntID NUMERIC DEFAULT 0,                 
        v_tintType NUMERIC DEFAULT NULL,          
        v_tintRights NUMERIC DEFAULT 0,          
        v_intDealStatus NUMERIC DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 0 then          
        -- ALL RECORDS RIGHTS          
        
      open SWV_RefCur for
      SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppId) AS Total,
                        (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM INNER JOIN Listdetails LD ON LD.numListItemID =  OM.lngPConclAnalysis AND LD.numListID = 12
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = v_intDealStatus
         AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
         and OM.numModifiedBy = v_numUserCntID -- Added By Anoop           
                                ) as Percentage
      FROM OpportunityMaster OM INNER JOIN Listdetails LD ON LD.numListItemID = OM.lngPConclAnalysis AND LD.numListID = 12
      WHERE
      OM.numrecowner = v_numUserCntID -- Added By Anoop        
                    
      AND OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intDealStatus
      AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
      GROUP BY OM.lngPConclAnalysis,LD.vcData;
   end if;          
          
   IF v_tintRights = 1 then          
        -- OWNER RECORDS RIGHTS          
        
      open SWV_RefCur for
      SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppId) AS Total,
                        (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.lngPConclAnalysis AND LD.numListID = 12
         INNER JOIN AdditionalContactsInformation  ADC ON ADC.numContactId = OM.numrecowner
         WHERE OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = v_intDealStatus
         AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
         and ADC.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop        
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)) as Percentage
      FROM OpportunityMaster OM
      INNER JOIN Listdetails LD ON LD.numListItemID = OM.lngPConclAnalysis AND LD.numListID = 12
      INNER JOIN AdditionalContactsInformation  ADC ON ADC.numContactId = OM.numrecowner
      WHERE   OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intDealStatus
      AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
      and  ADC.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
      GROUP BY OM.lngPConclAnalysis,LD.vcData;
   end if;          
          
   IF v_tintRights = 2 then          
        -- TERRITORY RECORDS RIGHTS          
        
      open SWV_RefCur for
      SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppId) AS Total,
                        (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.lngPConclAnalysis AND LD.numListID = 12
         INNER JOIN DivisionMaster Div ON OM.numDivisionId = Div.numDivisionID
         WHERE OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = v_intDealStatus
         AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
         and Div.numTerID  in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)) as Percentage
      FROM OpportunityMaster OM INNER JOIN Listdetails LD ON LD.numListItemID = OM.lngPConclAnalysis AND LD.numListID = 12
      INNER JOIN DivisionMaster Div ON OM.numDivisionId = Div.numDivisionID
      WHERE   OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = v_intDealStatus
      AND OM.bintCreatedDate >= v_dtFromDate AND OM.bintCreatedDate <= v_dtToDate
      and Div.numTerID  in(select F.numTerritory from ForReportsByTerritory F     -- Added By Anoop         
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
      GROUP BY OM.lngPConclAnalysis,LD.vcData;
   end if;
   RETURN;
END; $$;


