-- Stored procedure definition script USP_Item_GetWarehouseBasedonAddress for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetWarehouseBasedonAddress(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numAddressID NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTempWarehouseID  NUMERIC(18,0) DEFAULT 0;
BEGIN
   IF(SELECT COUNT(*) FROM Warehouses WHERE numDomainID = v_numDomainID AND numAddressID = v_numAddressID) > 0 then
	
      IF EXISTS(SELECT WI.numWareHouseItemID FROM Warehouses W INNER JOIN WareHouseItems WI ON W.numWareHouseID = WI.numWareHouseID WHERE W.numDomainID = v_numDomainID AND W.numAddressID = v_numAddressID AND numWarehouseItemID = v_numWarehouseItemID) then
		
         select   WI.numWareHouseItemID INTO v_numTempWarehouseID FROM Warehouses W INNER JOIN WareHouseItems WI ON W.numWareHouseID = WI.numWareHouseID WHERE W.numDomainID = v_numDomainID AND W.numAddressID = v_numAddressID AND numWarehouseItemID = v_numWarehouseItemID;
      ELSE
         select   WI.numWareHouseItemID INTO v_numTempWarehouseID FROM
         Warehouses W
         INNER JOIN
         WareHouseItems WI
         ON
         W.numWareHouseID = WI.numWareHouseID WHERE
         W.numDomainID = v_numDomainID
         AND W.numAddressID = v_numAddressID   ORDER BY
         coalesce(numWLocationID,0) DESC LIMIT 1;
      end if;
   end if;

   open SWV_RefCur for SELECT v_numTempWarehouseID AS numWarehouseItemID;
END; $$;












