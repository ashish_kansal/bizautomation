-- Stored procedure definition script USP_ECommerceTiggerPromotion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE  OR REPLACE FUNCTION USP_ECommerceTiggerPromotion(v_numDomainId NUMERIC(9,0),
	v_numUserCntId NUMERIC(9,0),
	v_vcCookieId VARCHAR(100),
	v_numCartID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numItemClassification NUMERIC(18,0),
    v_numUnitHour NUMERIC(18,0),
	v_monPrice DECIMAL(20,5),
	v_numSiteID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monTotalAmount  DECIMAL(20,5) DEFAULT v_numUnitHour*v_monPrice;
   v_numAppliedPromotion  NUMERIC(18,0) DEFAULT 0;
   v_numPromotionID  NUMERIC(18,0);
   v_fltOfferTriggerValue  DOUBLE PRECISION;
   v_tintOfferTriggerValueType  SMALLINT;
   v_tintOfferBasedOn  SMALLINT;
   v_fltDiscountValue  DOUBLE PRECISION;
   v_tintDiscountType  SMALLINT;
   v_tintDiscoutBaseOn  SMALLINT;
   v_vcPromotionDescription  TEXT;
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;
   v_k  INTEGER DEFAULT 1;
   v_kCount  INTEGER;
   v_usedDiscountAmount  DOUBLE PRECISION;
   v_usedDiscountQty  DOUBLE PRECISION;
   v_remainingDiscountValue  DOUBLE PRECISION;
   v_numTempUnitHour  DOUBLE PRECISION;
   v_discountQty  DOUBLE PRECISION;
   v_remainingOfferValue  DOUBLE PRECISION DEFAULT v_fltOfferTriggerValue;
   v_numTempCartID  NUMERIC(18,0);
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempItemClassification  NUMERIC(18,0);
   v_monTempPrice  NUMERIC(18,0);
   v_monTempTotalAmount  NUMERIC(18,0);
   v_j  INTEGER DEFAULT 1; 
   v_jCount  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPPromotion_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPPROMOTION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPROMOTION
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numPromotionID NUMERIC(18,0),
      fltOfferTriggerValue DOUBLE PRECISION,
      tintOfferTriggerValueType SMALLINT,
      tintOfferBasedOn SMALLINT,
      fltDiscountValue DOUBLE PRECISION,
      tintDiscountType SMALLINT,
      tintDiscoutBaseOn SMALLINT,
      vcPromotionDescription TEXT
   );
   INSERT INTO
   tt_TEMPPROMOTION(numPromotionID, fltOfferTriggerValue, tintOfferTriggerValueType, tintOfferBasedOn, fltDiscountValue, tintDiscountType, tintDiscoutBaseOn, vcPromotionDescription)
   SELECT
   PO.numProId,
		PO.fltOfferTriggerValue,
		PO.tintOfferTriggerValueType,
		PO.tintOfferBasedOn,
		PO.fltDiscountValue,
		PO.tintDiscountType,
		PO.tintDiscoutBaseOn,
		(SELECT CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN CAST(fltOfferTriggerValue AS VARCHAR(30)) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
      WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
      WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
         '"')
      END,
      ' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END,CASE
      WHEN tintDiscoutBaseOn = 1 THEN
         CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 2 THEN
         CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
      ELSE ''
      END))
   FROM
   PromotionOffer PO
   INNER JOIN
   PromotionOfferSites POS
   ON
   PO.numProId = POS.numPromotionID
   AND POS.numSiteID = v_numSiteID
   WHERE
   PO.numDomainId = v_numDomainId
   AND coalesce(bitEnabled,false) = true
   AND coalesce(bitRequireCouponCode,false) = false
   AND 1 =(CASE WHEN coalesce(bitNeverExpires,false) = true THEN 1 ELSE(CASE WHEN LOCALTIMESTAMP  BETWEEN dtValidFrom:: BYTEA AND dtValidTo:: BYTEA THEN 1 ELSE 0 END) END)
   AND 1 =(CASE PO.tintOfferBasedOn
   WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = PO.numProId AND POI.numValue = v_numItemCode AND tintRecordType = 5 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
   WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = PO.numProId AND POI.numValue = v_numItemClassification AND tintRecordType = 5 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
   ELSE 0
   END)
   ORDER BY
   CASE
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
   END;

   select   COUNT(*) INTO v_Count FROM tt_TEMPPROMOTION;

   IF v_Count > 0 then
	
      WHILE v_i <= v_Count LOOP
         select   numPromotionID, fltOfferTriggerValue, tintOfferTriggerValueType, tintOfferBasedOn, fltDiscountValue, tintDiscountType, tintDiscoutBaseOn, vcPromotionDescription INTO v_numPromotionID,v_fltOfferTriggerValue,v_tintOfferTriggerValueType,v_tintOfferBasedOn,
         v_fltDiscountValue,v_tintDiscountType,v_tintDiscoutBaseOn,
         v_vcPromotionDescription FROM
         tt_TEMPPROMOTION WHERE
         ID = v_i;
         IF v_tintOfferBasedOn = 1 -- individual items
         AND(SELECT COUNT(*) FROm CartItems WHERE numDomainId = v_numDomainId AND numUserCntId = v_numUserCntId AND vcCookieId = v_vcCookieId AND PromotionID = v_numPromotionID AND bitParentPromotion = true) = 0
         AND 1 =(CASE
         WHEN v_tintOfferTriggerValueType = 1 -- Quantity
         THEN CASE WHEN v_numUnitHour >= v_fltOfferTriggerValue THEN 1 ELSE 0 END
         WHEN v_tintOfferTriggerValueType = 2 -- Amount
         THEN CASE WHEN v_monTotalAmount >= v_fltOfferTriggerValue THEN 1 ELSE 0 END
         ELSE 0
         END) then
			
            UPDATE
            CartItems
            SET
            PromotionID = v_numPromotionID,PromotionDesc = v_vcPromotionDescription,bitParentPromotion = true
            WHERE
            numCartId = v_numCartID;
            IF 1 =(CASE v_tintDiscoutBaseOn
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND POI.numValue = v_numItemCode AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND POI.numValue = v_numItemClassification AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
            WHEN 3 THEN(CASE v_tintOfferBasedOn
               WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainId AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
               ELSE 0
               END)
            ELSE 0
            END) then
				
               IF v_tintDiscountType = 1  AND v_fltDiscountValue > 0 then --Percentage
					
                  UPDATE
                  CartItems
                  SET
                  fltDiscount = v_fltDiscountValue,bitDiscountType = 0,monTotAmount =(v_monTotalAmount -((v_monTotalAmount*v_fltDiscountValue)/100))
                  WHERE
                  numCartId = v_numCartID;
               ELSEIF v_tintDiscountType = 2 AND v_fltDiscountValue > 0
               then --Flat Amount
					
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
                  select   SUM(fltDiscount) INTO v_usedDiscountAmount FROM
                  CartItems WHERE
                  PromotionID = v_numPromotionID;
                  IF v_usedDiscountAmount < v_fltDiscountValue then
						
                     UPDATE
                     CartItems
                     SET
                     monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTotalAmount THEN 0 ELSE(v_monTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTotalAmount THEN v_monTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                     WHERE
                     numCartId = v_numCartID;
                  end if;
               ELSEIF v_tintDiscountType = 3  AND v_fltDiscountValue > 0
               then --Quantity
					
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
                  select   SUM(fltDiscount/monPrice) INTO v_usedDiscountQty FROM
                  CartItems WHERE
                  PromotionID = v_numPromotionID;
                  v_remainingDiscountValue :=(v_fltDiscountValue -v_usedDiscountQty);
                  IF v_usedDiscountQty < v_fltDiscountValue then
						
                     IF(SELECT COUNT(*) FROM CartItems WHERE numDomainId = v_numDomainId AND numCartId = v_numCartID AND bitParentPromotion = true AND v_tintOfferBasedOn = 1) > 0 then
							
                        IF v_tintOfferTriggerValueType = 1 then
								
                           IF v_numUnitHour > v_fltOfferTriggerValue then
                              v_numTempUnitHour := v_numUnitHour -v_fltOfferTriggerValue;
                              UPDATE
                              CartItems
                              SET
                              monTotAmount =(CASE WHEN v_remainingDiscountValue >= v_numTempUnitHour THEN v_fltOfferTriggerValue*monPrice ELSE(v_monTotalAmount -(v_remainingDiscountValue*v_monPrice)) END),bitDiscountType = true,fltDiscount =(CASE WHEN v_remainingDiscountValue >= v_numTempUnitHour THEN(v_numTempUnitHour*v_monPrice) ELSE(v_remainingDiscountValue*v_monPrice) END)
                              WHERE
                              numCartId = v_numCartID;
                              v_remainingDiscountValue := v_remainingDiscountValue -(CASE WHEN v_remainingDiscountValue >= v_numTempUnitHour THEN v_numTempUnitHour ELSE v_remainingDiscountValue END);
                           ELSE
                              UPDATE
                              CartItems
                              SET
                              fltDiscount = 0,monTotAmount = numUnitHour*monPrice,monTotAmtBefDiscount = numUnitHour*monPrice
                              WHERE
                              numCartId = v_numCartID;
                           end if;
                        ELSEIF v_tintOfferTriggerValueType = 2
                        then
								
                           IF(v_numUnitHour*v_monPrice) > v_fltOfferTriggerValue then
                              v_discountQty := FLOOR(((v_numUnitHour*v_monPrice) -v_fltOfferTriggerValue)/(CASE WHEN coalesce(v_monPrice,0) = 0 THEN 1 ELSE v_monPrice END));
                              IF v_discountQty > 0 then
										
                                 UPDATE
                                 CartItems
                                 SET
                                 monTotAmount =(CASE WHEN v_remainingDiscountValue >= v_discountQty THEN(v_numTempUnitHour -v_discountQty)*monPrice ELSE((numUnitHour*monPrice) -(v_remainingDiscountValue*monPrice)) END),bitDiscountType = true,
                                 fltDiscount =(CASE WHEN v_remainingDiscountValue >= v_discountQty THEN(v_discountQty*monPrice) ELSE(v_remainingDiscountValue*monPrice) END)
                                 WHERE
                                 numCartId = v_numCartID;
                                 v_remainingDiscountValue := v_remainingDiscountValue -v_discountQty;
                              ELSE
                                 UPDATE
                                 CartItems
                                 SET
                                 fltDiscount = 0,monTotAmount = v_numUnitHour*v_monPrice,monTotAmtBefDiscount = v_numUnitHour*v_monPrice
                                 WHERE
                                 numCartId = v_numCartID;
                              end if;
                           ELSE
                              UPDATE
                              CartItems
                              SET
                              fltDiscount = 0,monTotAmount = numUnitHour*monPrice,monTotAmtBefDiscount = numUnitHour*monPrice
                              WHERE
                              numCartId = v_numCartID;
                           end if;
                        end if;
                     ELSE
                        UPDATE
                        CartItems
                        SET
                        monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= numUnitHour THEN 0 ELSE(v_monTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monPrice)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= numUnitHour THEN(v_numUnitHour*v_monPrice) ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monPrice) END)
                        WHERE
                        numCartId = v_numCartID;
                     end if;
                  end if;
               end if;
            end if;
            v_numAppliedPromotion := v_numPromotionID;
            EXIT;
         ELSEIF v_tintOfferBasedOn = 2
         then -- item classifications
            DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPITEMS
            (
               ID INTEGER,
               numCartID NUMERIC(18,0),
               numItemCode NUMERIC(18,0),
               numItemClassification NUMERIC(18,0),
               numUnitHour NUMERIC(18,0),
               monPrice NUMERIC(18,0),
               monTotalAmount NUMERIC(18,0)
            );
            INSERT INTO
            tt_TEMPITEMS
            SELECT
            CAST(1 AS INTEGER),
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour*CI.monPrice)
            FROM
            CartItems CI
            INNER JOIN
            Item I
            ON
            CI.numItemCode = I.numItemCode
            WHERE
            numCartId = v_numCartID
            UNION
            SELECT(ROW_NUMBER() OVER(ORDER BY numCartID ASC)+1),
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour*CI.monPrice)
            FROM
            CartItems CI
            INNER JOIN
            Item I
            ON
            CI.numItemCode = I.numItemCode
            WHERE
            numCartId <> v_numCartID
            AND CI.numDomainId = v_numDomainId
            AND CI.numUserCntId = v_numUserCntId
            AND CI.vcCookieId = v_vcCookieId
            AND coalesce(CI.PromotionID,0) = 0
            AND I.numItemClassification IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = v_numPromotionID AND tintRecordType = 5 AND tintType = 2);
            IF(SELECT(CASE v_tintOfferTriggerValueType WHEN  1 THEN SUM(numUnitHour) WHEN 2 THEN SUM(monTotalAmount) ELSE 0 END)
            FROM
            tt_TEMPITEMS) >= v_fltOfferTriggerValue then
               select   COUNT(*) INTO v_jCount FROM tt_TEMPITEMS;
               WHILE v_j <= v_jCount LOOP
                  select   numCartID, numItemCode, coalesce(numItemClassification,0), numUnitHour, monPrice, (numUnitHour*monPrice) INTO v_numTempCartID,v_numTempItemCode,v_numTempItemClassification,v_numTempUnitHour,
                  v_monTempPrice,v_monTempTotalAmount FROM
                  tt_TEMPITEMS WHERE
                  ID = v_j;
                  UPDATE
                  CartItems
                  SET
                  PromotionID = v_numPromotionID,PromotionDesc = v_vcPromotionDescription,fltDiscount = 0,
                  bitDiscountType = false,bitParentPromotion = true
                  WHERE
                  numCartId = v_numCartID;


						-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
                  IF 1 =(CASE v_tintDiscoutBaseOn
                  WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND POI.numValue = v_numTempItemCode AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
                  WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND POI.numValue = v_numTempItemClassification AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
                  WHEN 3 THEN(CASE v_tintOfferBasedOn
                     WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
                     WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainId AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numPromotionID AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
                     ELSE 0
                     END)
                  ELSE 0
                  END) then
						
                     IF v_tintDiscountType = 1 then --Percentage
							
                        UPDATE
                        CartItems
                        SET
                        fltDiscount = v_fltDiscountValue,bitDiscountType = false,monTotAmount =(v_monTempTotalAmount -((v_monTempTotalAmount*v_fltDiscountValue)/100))
                        WHERE
                        numCartId = v_numCartID;
                     ELSEIF v_tintDiscountType = 2
                     then --Flat Amount
							
                        select   SUM(coalesce(fltDiscount,0)) INTO v_usedDiscountAmount FROM
                        CartItems WHERE
                        PromotionID = v_numPromotionID;
                        IF v_usedDiscountAmount < v_fltDiscountValue then
								
                           UPDATE
                           CartItems
                           SET
                           monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN 0 ELSE(v_monTempTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN v_monTempTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                           WHERE
                           numCartId = v_numCartID;
                        end if;
                     ELSEIF v_tintDiscountType = 3
                     then --Quantity
							
								-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
                        select   SUM(fltDiscount/monPrice) INTO v_usedDiscountQty FROM
                        CartItems WHERE
                        PromotionID = v_numPromotionID;
                        IF v_usedDiscountQty < v_fltDiscountValue then
								
                           UPDATE
                           CartItems
                           SET
                           monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN 0 ELSE(v_monTempTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN(v_numTempUnitHour*v_monTempPrice) ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice) END)
                           WHERE
                           numCartId = v_numCartID;
                        end if;
                     end if;
                  end if;
                  v_remainingOfferValue := v_remainingOfferValue -(CASE v_tintOfferTriggerValueType WHEN  1 THEN v_numTempUnitHour WHEN 2 THEN v_monTempTotalAmount ELSE 0 END);
                  IF v_remainingOfferValue <= 0 then
						
                     EXIT;
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;
               v_numAppliedPromotion := v_numPromotionID;
               EXIT;
            end if;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;

	-- CHECK IF Promotion discount can be applied to other items already added to cart.
   IF coalesce(v_numAppliedPromotion,0) > 0 then
      DELETE FROM tt_TEMPITEMS;
      INSERT INTO
      tt_TEMPITEMS
      SELECT
      ROW_NUMBER() OVER(ORDER BY numCartID ASC),
			numCartId,
			CI.numItemCode,
			I.numItemClassification,
			CI.numUnitHour,
			CI.monPrice,
			(CI.numUnitHour*CI.monPrice)
      FROM
      CartItems CI
      INNER JOIN
      Item I
      ON
      CI.numItemCode = I.numItemCode
      WHERE
      CI.numDomainId = v_numDomainId
      AND CI.numUserCntId = v_numUserCntId
      AND CI.vcCookieId = v_vcCookieId
      AND coalesce(CI.PromotionID,0) = 0
      AND 1 =(CASE v_tintDiscoutBaseOn
      WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue = I.numItemCode AND numProId = v_numAppliedPromotion AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue = I.numItemClassification AND numProId = v_numAppliedPromotion AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
      WHEN 3 THEN(CASE v_tintOfferBasedOn
         WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = I.numItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numAppliedPromotion AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = I.numItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainId AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_numAppliedPromotion AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
         ELSE 0
         END)
      ELSE 0
      END);
      select   COUNT(*) INTO v_kCount FROM tt_TEMPITEMS;
      IF v_kCount > 0 then
		
         WHILE v_k <= v_kCount LOOP
            select   numCartID, numItemCode, coalesce(numItemClassification,0), numUnitHour, monPrice, (numUnitHour*monPrice) INTO v_numTempCartID,v_numTempItemCode,v_numTempItemClassification,v_numTempUnitHour,
            v_monTempPrice,v_monTempTotalAmount FROM
            tt_TEMPITEMS WHERE
            ID = v_k;
            IF v_tintDiscountType = 1 then --Percentage
				
               UPDATE
               CartItems
               SET
               PromotionID = v_numPromotionID,PromotionDesc = v_vcPromotionDescription,bitParentPromotion = false,
               fltDiscount = v_fltDiscountValue,bitDiscountType = false,monTotAmount =(v_monTempTotalAmount -((v_monTempTotalAmount*v_fltDiscountValue)/100))
               WHERE
               numCartId = v_numTempCartID;
            ELSEIF v_tintDiscountType = 2
            then --Flat Amount
				
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
               select   SUM(coalesce(fltDiscount,0)) INTO v_usedDiscountAmount FROM
               CartItems WHERE
               PromotionID = v_numAppliedPromotion;
               IF v_usedDiscountAmount < v_fltDiscountValue then
					
                  UPDATE
                  CartItems
                  SET
                  monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount)  >= v_monTempTotalAmount THEN 0 ELSE(v_monTempTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),PromotionID = v_numPromotionID,PromotionDesc = v_vcPromotionDescription,
                  bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN v_monTempTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                  WHERE
                  numCartId = v_numTempCartID;
               ELSE
                  EXIT;
               end if;
            ELSEIF v_tintDiscountType = 3
            then --Quantity
				
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
               select   SUM(coalesce(fltDiscount,0)/coalesce(monPrice,1)) INTO v_usedDiscountQty FROM
               CartItems WHERE
               PromotionID = v_numAppliedPromotion;
               IF v_usedDiscountQty < v_fltDiscountValue then
					
                  UPDATE
                  CartItems
                  SET
                  monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty)  >= numUnitHour THEN 0 ELSE(v_monTempTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice)) END),PromotionID = v_numPromotionID,
                  PromotionDesc = v_vcPromotionDescription,bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= numUnitHour THEN v_monTempTotalAmount ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice) END)
                  WHERE
                  numCartId = v_numTempCartID;
               ELSE
                  EXIT;
               end if;
            end if;
            v_k := v_k::bigint+1;
         END LOOP;
      end if;
   end if;
   RETURN;
END; $$;



