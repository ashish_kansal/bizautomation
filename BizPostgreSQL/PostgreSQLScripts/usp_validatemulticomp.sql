-- Stored procedure definition script USP_ValidateMultiComp for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ValidateMultiComp(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DomainSubscriberID  NUMERIC(9,0);
   v_MultiCompSubscriberID  NUMERIC(9,0);
        /* Only Top patent company should be able to see multicomp data */
    
BEGIN
   select   coalesce(numSubscriberID,0) INTO v_DomainSubscriberID FROM Subscribers WHERE numTargetDomainID = v_numDomainID;
        
   open SWV_RefCur for SELECT COUNT(*) FROM Domain WHERE numSubscriberID = v_DomainSubscriberID;
   RETURN;
        
        /*
        SELECT  @MultiCompSubscriberID = ISNULL([numSubscriberID],0)
        FROM    [Domain]
        WHERE   [numDomainId] = @numDomainID
 
        SELECT  @DomainSubscriberID = ISNULL([numSubscriberID],0)
        FROM    [Subscribers]
        WHERE   [numTargetDomainID] = @numDomainID	
        
        
        PRINT @MultiCompSubscriberID
        PRINT @DomainSubscriberID
        
        
        IF @DomainSubscriberID = 0
            OR @MultiCompSubscriberID = 0 
            BEGIN
                SELECT  0 
                RETURN	
            END
			
        
        IF @DomainSubscriberID = @MultiCompSubscriberID 
            SELECT  1
        ELSE 
            SELECT  0 
	*/
END; $$;













