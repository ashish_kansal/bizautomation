DROP FUNCTION IF EXISTS USP_GetBackOrderPermission;

CREATE OR REPLACE FUNCTION USP_GetBackOrderPermission(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_UserAuthGroupId NUMERIC(9,0) DEFAULT NULL,
	v_numitemcode NUMERIC(9,0) DEFAULT NULL,
	v_numQuantity DOUBLE PRECISION DEFAULT NULL,
	v_numWareHouseItemid NUMERIC(9,0) DEFAULT NULL,
	v_kitChildItem TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_UserGroupId  NUMERIC(18,0);
   v_numBackOrder  NUMERIC(18,0);
   v_bitKitParent  BOOLEAN;
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   SELECT COUNT(*) INTO v_UserGroupId FROM AuthenticationGroupBackOrder WHERE numGroupID = v_UserAuthGroupId AND numDomainID = v_numDomainID;

   IF v_UserGroupId > 0 then
      select   coalesce(bitKitParent,false) INTO v_bitKitParent FROM Item WHERE numItemCode = v_numitemcode;
      IF v_bitKitParent = true then
         select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWareHouseItemid;
         DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEMS
         (
            numItemCode NUMERIC(18,0),
            numWarehouseItemID NUMERIC(18,0),
            numQtyItemsReq DOUBLE PRECISION
         );
         DROP TABLE IF EXISTS tt_TEMPEXISTINGITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPEXISTINGITEMS
         (
            vcItem VARCHAR(100)
         );
         INSERT INTO tt_TEMPEXISTINGITEMS(vcItem)
         SELECT
         OutParam
         FROM
         SplitString(v_kitChildItem,',');
         DROP TABLE IF EXISTS tt_TEMPSELECTEDKITCHILDS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPSELECTEDKITCHILDS
         (
            ChildKitItemID NUMERIC(18,0),
            ChildKitWarehouseItemID NUMERIC(18,0),
            ChildKitChildItemID NUMERIC(18,0),
            ChildKitChildWarehouseItemID NUMERIC(18,0),
            ChildQty DOUBLE PRECISION
         );
         INSERT INTO tt_TEMPSELECTEDKITCHILDS(ChildKitItemID
				,ChildKitChildItemID
				,ChildQty)
         SELECT
			split_part(vcItem,'-',1)::NUMERIC
			,split_part(vcItem,'-',2)::NUMERIC
			,split_part(vcItem,'-',3)::NUMERIC
         FROM(SELECT
            vcItem
            FROM
            tt_TEMPEXISTINGITEMS) As x;
         UPDATE
         tt_TEMPSELECTEDKITCHILDS
         SET
         ChildKitWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ChildKitItemID AND numWareHouseID = v_numWarehouseID LIMIT 1),ChildKitChildWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ChildKitChildItemID AND numWareHouseID = v_numWarehouseID LIMIT 1);
         IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0 then
			
            INSERT INTO tt_TEMPITEMS(numItemCode
					,numWarehouseItemID
					,numQtyItemsReq)
            SELECT
            I.numItemCode
					,T1.ChildKitChildWarehouseItemID
					,v_numQuantity*ChildQty
            FROM
            tt_TEMPSELECTEDKITCHILDS T1
            INNER JOIN
            Item I
            ON
            T1.ChildKitChildItemID = I.numItemCode
            WHERE
            coalesce(ChildKitItemID,0) = 0
            AND coalesce(bitKitParent,false) = false;
         ELSE
            INSERT INTO tt_TEMPITEMS(numItemCode
					,numWarehouseItemID
					,numQtyItemsReq) with recursive CTE(numItemCode,bitKitParent,numQtyItemsReq) AS(SELECT
            ID.numChildItemID AS numItemCode,
						coalesce(I.bitKitParent,false) AS bitKitParent,
						CAST(v_numQuantity*(coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQtyItemsReq
            FROM
            ItemDetails ID
            INNER JOIN
            Item I
            ON
            ID.numChildItemID = I.numItemCode
            WHERE
            numItemKitID = v_numitemcode
            AND 1 =(CASE
            WHEN coalesce(I.bitKitParent,false) = true AND LENGTH(coalesce(v_kitChildItem,'')) > 0 THEN(CASE
               WHEN numChildItemID IN(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS)
               THEN 1
               ELSE 0
               END)
            ELSE(CASE
               WHEN(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0
               THEN(CASE
                  WHEN numChildItemID IN(SELECT ChildKitChildItemID FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0)
                  THEN 1
                  ELSE 0
                  END)
               ELSE 1
               END)
            END)
            UNION ALL
            SELECT
            ID.numChildItemID AS numItemCode,
						coalesce(I.bitKitParent,false) AS bitKitParent,
						CAST((coalesce(Temp1.numQtyItemsReq,0)*coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQtyItemsReq
            FROM
            CTE As Temp1
            INNER JOIN
            ItemDetails ID
            ON
            ID.numItemKitID = Temp1.numItemCode
            INNER JOIN
            Item I
            ON
            ID.numChildItemID = I.numItemCode
            WHERE
            Temp1.bitKitParent = true
            AND EXISTS(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS T2 WHERE coalesce(Temp1.bitKitParent,false) = true AND T2.ChildKitItemID = ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)) SELECT
            numItemCode
					,(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = numItemCode AND numWareHouseID = v_numWarehouseID LIMIT 1)
					,numQtyItemsReq
            FROM
            CTE
            WHERE
            coalesce(bitKitParent,false) = false;
         end if;
         IF EXISTS(SELECT
         T1.numItemCode
         FROM
         tt_TEMPITEMS T1
         INNER JOIN
         WareHouseItems WI
         ON
         T1.numWarehouseItemID = WI.numWareHouseItemID
         WHERE coalesce(numOnHand,0) < numQtyItemsReq) then
			
            open SWV_RefCur for
            SELECT 0;
         ELSE
            open SWV_RefCur for
            SELECT 1;
         end if;
      ELSE
         SELECT
         COUNT(*) INTO v_numBackOrder FROM
         WareHouseItems WHERE
         numItemID = v_numitemcode
         AND coalesce(numOnHand,0) < v_numQuantity
         AND numWareHouseItemID = v_numWareHouseItemid;
         IF v_numBackOrder > 0 then
            open SWV_RefCur for
            SELECT 0;
         ELSE
            open SWV_RefCur for
            SELECT 1;
         end if;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


