-- Stored procedure definition script USP_GetGrossProfitEstimate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetGrossProfitEstimate(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numOppID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_avgCost  INTEGER;
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_monShippingProfit  DECIMAL(20,5);
   v_monShippingAverageCost  DECIMAL(20,5) DEFAULT 0;
   v_monMarketplaceShippingCost  DECIMAL(20,5);
BEGIN
   select   numCost, coalesce(numShippingServiceItemID,0) INTO v_avgCost,v_numShippingServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   select   coalesce(monMarketplaceShippingCost,0) INTO v_monMarketplaceShippingCost FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID; 

   IF coalesce(v_monMarketplaceShippingCost,0) > 0 then
	
      IF EXISTS(SELECT * FROM OpportunityItems WHERE numOppId = v_numOppID AND numItemCode = v_numShippingServiceItemID) then
		
         v_monShippingAverageCost := v_monMarketplaceShippingCost/coalesce((SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = v_numOppID AND numItemCode = v_numShippingServiceItemID),0);
         v_monShippingProfit := coalesce((SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppId = v_numOppID AND numItemCode = v_numShippingServiceItemID),0) -v_monMarketplaceShippingCost;
      ELSE
         v_monShippingProfit := 0;
      end if;
   ELSE
      v_monShippingProfit := 0;
   end if;

   open SWV_RefCur for SELECT
   numOppId
		,v_monShippingProfit AS monShippingProfit
		,vcpOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,(coalesce(monTotAmount,0)*fltExchangeRate)/coalesce(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour
		,(coalesce(monTotAmount,0)*fltExchangeRate) -(CASE WHEN numItemCode = v_numShippingServiceItemID THEN(coalesce(numUnitHour,0)*v_monShippingAverageCost) ELSE(coalesce(numUnitHour,0)*(CASE v_avgCost WHEN 3 THEN VendorCost*fn_UOMConversion(numBaseUnit,numItemCode,v_numDomainID,numPurchaseUnit) WHEN 2 THEN numCost*fn_UOMConversion(numBaseUnit,numItemCode,v_numDomainID,numUOMId) ELSE monAverageCost END)) END) AS Profit
		,((((coalesce(monTotAmount,0)*fltExchangeRate)/coalesce(NULLIF(numUnitHour,0),1)) -(CASE
   WHEN numItemCode = v_numShippingServiceItemID
   THEN v_monShippingAverageCost
   ELSE(CASE
      WHEN numPOItemID IS NOT NULL
      THEN coalesce(monPOCost,0)
      ELSE(CASE v_avgCost
         WHEN 3 THEN VendorCost*fn_UOMConversion(numBaseUnit,numItemCode,v_numDomainID,numPurchaseUnit)
         ELSE monAverageCost
         END)
      END)
   END))/(CASE WHEN((coalesce(monTotAmount,0)*fltExchangeRate)/coalesce(NULLIF(numUnitHour,0),1)) = 0 then 1 ELSE((coalesce(monTotAmount,0)*fltExchangeRate)/coalesce(NULLIF(numUnitHour,0),1)) end))*100 AS ProfitPer
		,bitItemPriceApprovalRequired
		,(CASE WHEN numPOID IS NOT NULL THEN CONCAT('<a href="javascript:OpenOpp(',numPOID,')">',vcPOName,'</a>') ELSE '' END) AS vcPurchaseOrder
   FROM(SELECT
      opp.numOppId
			,opp.vcpOppName
			,OppI.vcItemName
			,(CASE
      WHEN coalesce(I.bitVirtualInventory,false) = true
      THEN 0
      ELSE(CASE
         WHEN coalesce(I.bitKitParent,false) = true
         THEN coalesce((SELECT cast(SUM(coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKI.monAvgCost,0)) as NUMERIC(18,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID = IInner.numItemCode WHERE OKI.numOppId = opp.numOppId AND OKI.numOppItemID = OppI.numoppitemtCode),
            0)+coalesce((SELECT cast(SUM(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKCI.monAvgCost,0)) as NUMERIC(18,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID = IInner.numItemCode WHERE OKI.numOppId = opp.numOppId AND OKI.numOppItemID = OppI.numoppitemtCode),0)
         ELSE coalesce(monAverageCost,0)
         END)
      END) AS monAverageCost,
			V.numVendorID AS numVendorID
			,fn_GetComapnyName(V.numVendorID) as vcVendor
			,OppI.numUnitHour
			,coalesce(OppI.monPrice,0) AS monPrice
			,coalesce(OppI.monTotAmount,0) AS monTotAmount
			,coalesce(V.monCost,0) AS VendorCost
			,coalesce(OppI.bitItemPriceApprovalRequired,false) AS bitItemPriceApprovalRequired
			,coalesce(OppI.numCost,0) AS numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,coalesce(OppI.numUOMId,numBaseUnit) AS numUOMId
			,coalesce(opp.fltExchangeRate,1) AS fltExchangeRate
			,coalesce(TEMPMatchedPO.numOppId,TEMPPO.numOppId) AS numPOID
			,coalesce(TEMPMatchedPO.vcPOppName,TEMPPO.vcPOppName) AS vcPOName
			,coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) AS numPOItemID
			,coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice) AS monPOCost
      FROM
      OpportunityMaster opp
      INNER JOIN
      OpportunityItems OppI
      ON
      opp.numOppId = OppI.numOppId
      LEFT JOIN LATERAL(SELECT 
         OM.numOppId
				,OM.vcpOppName
				,OI.numoppitemtCode
				,OI.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OM
         ON
         SOLIPL.numPurchaseOrderID = OM.numOppId
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OI.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = opp.numOppId
         AND SOLIPL.numSalesOrderItemID = OppI.numoppitemtCode LIMIT 1) TEMPMatchedPO ON TRUE
      LEFT JOIN LATERAL(SELECT 
         OM.numOppId
				,OM.vcpOppName
				,OI.numoppitemtCode
				,OI.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OM
         ON
         OL.numChildOppID = OM.numOppId
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OL.numParentOppID = opp.numOppId
         AND OI.numItemCode = OppI.numItemCode
         AND OI.vcAttrValues = OppI.vcAttrValues LIMIT 1) TEMPPO ON TRUE
      INNER JOIN
      Item I
      ON
      OppI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      opp.numDomainId = v_numDomainID
      AND opp.numOppId = v_numOppID
      AND coalesce(I.bitContainer,false) = false) temp;
END; $$;












