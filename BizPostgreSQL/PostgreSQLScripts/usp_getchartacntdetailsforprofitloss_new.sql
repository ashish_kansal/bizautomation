-- Stored procedure definition script USP_GetChartAcntDetailsForProfitLoss_New for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForProfitLoss_New(v_numDomainId NUMERIC(9,0),                                          
v_dtFromDate TIMESTAMP,                                        
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine                                                   
v_numAccountClass NUMERIC(9,0) DEFAULT 0,
v_DateFilter VARCHAR(20) DEFAULT NULL,
v_ReportColumn VARCHAR(20) DEFAULT NULL,
v_numDivisionID NUMERIC(18,0) DEFAULT NULL,
v_bitAddTotalRow BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_FinancialYearStartDate  TIMESTAMP;
   v_PLCHARTID  NUMERIC(18,0);

   v_columns TEXT;--SET @columns = '';
   v_SUMColumns TEXT;--SET @SUMColumns = '';
   v_PivotColumns TEXT;--SET @PivotColumns = '';
   v_ProfitLossCurrentColumns TEXT DEFAULT '';
   v_ProfitLossSumCurrentColumns TEXT DEFAULT ',0';
   v_ProfitLossOpeningColumns TEXT DEFAULT '';
   v_ProfitLossSumOpeningColumns TEXT DEFAULT '';

   v_Select TEXT;
   v_Where TEXT;
   SWV_ExecDyn TEXT;
BEGIN
   v_FinancialYearStartDate := GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP)::INT,v_numDomainId);

   DROP TABLE IF EXISTS tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss
   (
      ID INTEGER,
      StartDate TIMESTAMP,
      EndDate TIMESTAMP
   );

	INSERT INTO tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss  VALUES(1,v_FinancialYearStartDate,v_FinancialYearStartDate + make_interval(months => 3) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss  VALUES(2,v_FinancialYearStartDate + make_interval(months => 3),v_FinancialYearStartDate + make_interval(months => 6) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss  VALUES(3,v_FinancialYearStartDate + make_interval(months => 6),v_FinancialYearStartDate + make_interval(months => 9) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss  VALUES(4,v_FinancialYearStartDate + make_interval(months => 9),v_FinancialYearStartDate + make_interval(months => 12) + make_interval(secs => -0.003));

	
   select   COA.numAccountId INTO v_PLCHARTID FROM
   Chart_Of_Accounts COA WHERE
   numDomainId = v_numDomainId
   AND bitProfitLoss = true;

   DROP TABLE IF EXISTS tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss
   (
      numAccountId NUMERIC(18,0),
      vcAccountCode VARCHAR(50),
      COAvcAccountCode VARCHAR(100),
      datEntry_Date TIMESTAMP,
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

   INSERT INTO
   tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss
   SELECT
   numAccountId
		,AccountTypeCode
		,AccountCode
		,datEntry_Date
		,SUM(Debit)
		,SUM(Credit)
   FROM
   view_journal_master
   WHERE
   numDomainId = v_numDomainId
   AND (numClassIDDetail = v_numAccountClass OR v_numAccountClass = 0)
   AND (numDivisionId = v_numDivisionID OR v_numDivisionID = 0)
   GROUP BY
	 numAccountId
		,AccountTypeCode
		,AccountCode
		,datEntry_Date;

   IF v_DateFilter = 'CurYear' then
		v_dtFromDate := v_FinancialYearStartDate;
		v_dtToDate := v_FinancialYearStartDate + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'PreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => -1) + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CuQur' then
		select StartDate, EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'CurPreQur' then
		select StartDate + make_interval(months => -3), EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'PreQur' then
		select StartDate + make_interval(months => -3), EndDate + make_interval(months => -3) INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetChartAcntDetailsForProfitLoss WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'LastMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'ThisMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	end if;

   DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss AS
	WITH RECURSIVE DirectReport AS
	(  
		WITH RECURSIVE DirectReportInternal AS
		(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID, '#', 0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(ATD.vcAccountType AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), 'FM000'), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
			FALSE AS bitTotal,
			FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0103','0104','0106')
			AND ATD.bitActive = true
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0),'FM000'),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal,
			FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0103','0104','0106')
			AND ATD.bitActive = true
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
            CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
            ATD.numAccountTypeID,
            CAST(0 AS NUMERIC) AS numAccountID,
            CAST(ATD.vcAccountType AS VARCHAR(300)) AS vcAccountType,
            ATD.vcAccountCode,
            LEVEL + 1 AS LEVEL, 
            CAST(d.Struc || '#' || CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), 'FM000'), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
            FALSE AS bitTotal,
            FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReportInternal D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.bitActive = true
		)	
		SELECT * FROM DirectReportInternal
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
			ATD.numAccountTypeID,
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)) AS vcAccountType,
			ATD.vcAccountCode,
			LEVEL + 1 AS LEVEL, 
			CAST(CONCAT(d.Struc,'#',TO_CHAR(COALESCE(ATD.tintSortOrder,0),'FM000'),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal,
			FALSE AS bitIsSubAccount
        FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReport D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
        WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.bitActive = true
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
	), DirectReport1  AS
	(
		WITH RECURSIVE DirectReport1INTERNAL AS 
		(
			SELECT 
				ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitTotal, bitIsSubAccount
			FROM
				DirectReport
			UNION ALL
			SELECT 
				CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
				CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
				CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
				CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
				COA.vcAccountCode,
				LEVEL + 1 AS LEVEL,
				COA.numAccountId, 
				CAST(CONCAT(d.Struc ,'#',COA.numAccountId) AS VARCHAR(300)) AS Struc,
				FALSE AS bitTotal,
				COALESCE(COA.bitIsSubAccount, FALSE) AS bitIsSubAccount
			FROM 
				Chart_of_Accounts AS COA 
			JOIN 
				DirectReport1INTERNAL D 
			ON 
				D.numAccountTypeID = COA.numParntAcntTypeId 
				AND D.bitTotal = FALSE
			WHERE 
				COA.numDomainID = v_numdomainid 
				AND COALESCE(COA.bitIsSubAccount, FALSE) = FALSE 
		)
		SELECT 
			ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,bitTotal,bitIsSubAccount
		FROM
			DirectReport1INTERNAL
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
			CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
			CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
			COA.vcAccountCode,
			LEVEL + 1 AS LEVEL,
			COA.numAccountId, 
			CAST(d.Struc || '#' || CAST(COA.numAccountId AS VARCHAR) AS VARCHAR(300)) AS Struc,
			FALSE AS bitTotal,
			COALESCE(COA.bitIsSubAccount, FALSE) AS bitIsSubAccount
		FROM 
			Chart_of_Accounts AS COA 
		JOIN 
			DirectReport1 D 
		ON 
			D.numAccountId = COA.numParentAccId 
			AND D.bitTotal = FALSE
        WHERE 
			COA.numDomainID = v_numdomainid 
			AND COALESCE(COA.bitIsSubAccount, FALSE) = TRUE
	)
	SELECT 
		ParentId
		,vcCompundParentKey
		,numAccountTypeID
		,vcAccountType
		,LEVEL
		,vcAccountCode
		,numAccountId
		,Struc
		,bitTotal
		,bitIsSubAccount 
	FROM 
		DirectReport1;  

   IF v_bitAddTotalRow = true then
	
      INSERT INTO tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss(ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			LEVEL,
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal,
			bitIsSubAccount)
      SELECT
      ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			CONCAT('Total ',vcAccountType),
			LEVEL,
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			true,
			bitIsSubAccount
      FROM
      tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COA
      WHERE
      coalesce(COA.numAccountId,0) > 0
      AND coalesce(COA.bitTotal,false) = false
      AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COAInner WHERE COAInner.Struc ilike COA.Struc || '%') > 1;
      UPDATE tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss DRMain SET vcAccountType = SUBSTR(vcAccountType,7,LENGTH(vcAccountType) -6)  WHERE coalesce(DRMain.bitTotal,false) = true AND coalesce(numAccountId,0) = 0 AND DRMain.Struc IN(SELECT CONCAT(DRMain1.Struc,'#Total') FROm tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss DRMain1 WHERE coalesce(DRMain1.bitTotal,false) = false AND coalesce(DRMain1.numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain1.Struc || '%') = 0);
      DELETE FROM tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss DRMain  WHERE coalesce(DRMain.bitTotal,false) = false AND coalesce(numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain.Struc || '%') = 0;
   end if;

   INSERT INTO tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss
   SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL::NUMERIC,'-1',false,false
   UNION ALL
   SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL::NUMERIC,'-2',false,false;

	IF coalesce(v_bitAddTotalRow,false) = true THEN
		INSERT INTO tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss
		SELECT '','-1',-1,'Total Ordinary Income/Expense',0,NULL,NULL::NUMERIC,'-1#Total',true,false
		UNION ALL
		SELECT '','-2',-2,'Total Other Income and Expenses',0,NULL,NULL::NUMERIC,'-2#Total',true,false;
	END	IF;

	UPDATE 
		tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss 
	SET 
		Struc='-1#' || Struc 
	WHERE 
		vcAccountCode NOT ILIKE '010302%' 
		AND vcAccountCode NOT ILIKE '010402%';

	UPDATE 
		tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss 
	SET 
		Struc='-2#' || Struc
		,LEVEL = LEVEL - 1
	WHERE 
		vcAccountCode ILIKE '010302%' 
		OR vcAccountCode ILIKE '010402%';

	UPDATE 
		tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss 
	SET 
		ParentId=-1
	WHERE 
		vcAccountCode NOT IN ('010302','010402')
		AND LEVEL = 1;

	UPDATE 
		tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss 
	SET 
		ParentId=-2 
	WHERE 
		vcAccountCode IN ('010302','010402');

	DROP TABLE IF EXISTS tt_TEMPVIEWDATAGetChartAcntDetailsForProfitLoss CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPVIEWDATAGetChartAcntDetailsForProfitLoss AS
    SELECT
		COA.ParentId
		,COA.vcCompundParentKey
		,COA.numAccountTypeID
		,COA.vcAccountType
		,COA.LEVEL
		,COA.vcAccountCode
		,COA.numAccountId
		,COA.Struc
		,(CASE
		  WHEN COA.vcAccountCode ilike '010301%' OR COA.vcAccountCode ilike '010302%'
		  THEN(coalesce(Credit,0) -coalesce(Debit,0))
		  ELSE(coalesce(Debit,0) -coalesce(Credit,0))
		  END) AS Amount
		,V.datEntry_Date
		,COA.bitTotal
		,COA.bitIsSubAccount
    FROM
		tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COA
    JOIN
		tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss V
    ON
		V.numAccountId = COA.numAccountId
		AND datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
    WHERE
		COA.numAccountId IS NOT NULL
		AND coalesce(COA.bitTotal,false) = false;

	v_Select := 'SELECT ParentId AS "ParentId", vcCompundParentKey AS "vcCompundParentKey", numAccountTypeID AS "numAccountTypeID", vcAccountType AS "vcAccountType", LEVEL AS "LEVEL", vcAccountCode AS "vcAccountCode", numAccountId AS "numAccountId",(''#'' || Struc || ''#'') AS "Struc"';

	IF v_ReportColumn = 'Year' OR v_ReportColumn = 'Quarter' then
		DROP TABLE IF EXISTS tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss CASCADE;
		
		CREATE TEMPORARY TABLE tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss ON COMMIT DROP AS
		WITH RECURSIVE CTE AS 
		( 
			SELECT
				(CASE WHEN v_ReportColumn = 'Quarter' THEN GetFiscalyear(v_dtFromDate,v_numDomainID) ELSE EXTRACT(YEAR FROM v_dtFromDate)::INT END) AS yr,
				EXTRACT(MONTH FROM v_dtFromDate)::INT AS mm,
				to_char(v_dtFromDate, 'FMMonth') AS mon,
				EXTRACT(DAY FROM v_dtFromDate)::INT AS dd,
				GetFiscalQuarter(v_dtFromDate,v_numDomainID) AS qq,
				v_dtFromDate AS new_date
			UNION ALL
			SELECT
				(CASE WHEN v_ReportColumn = 'Quarter' THEN GetFiscalyear(new_date + make_interval(days => 1),v_numDomainID) ELSE EXTRACT(YEAR FROM new_date + make_interval(days => 1))::INT END) AS yr,
				EXTRACT(MONTH FROM new_date + make_interval(days => 1))::INT AS mm,
				to_char(new_date + make_interval(days => 1), 'FMMonth') AS mon,
				EXTRACT(DAY FROM new_date + make_interval(days => 1))::INT AS dd,
				GetFiscalQuarter(new_date + make_interval(days => 1),v_numDomainID) AS qq,
				new_date + make_interval(days => 1) AS new_date
			FROM CTE
			WHERE new_date + make_interval(days => 1) < v_dtToDate
		)
		SELECT yr AS Year1,qq AS Quarter1, mm as Month1, mon AS MonthName1, count(dd) AS Days1 
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq;

		IF v_ReportColumn = 'Year' then
			v_ProfitLossCurrentColumns := COALESCE((SELECT string_agg(CONCAT(',0 AS "',cast(monthname1 as varchar),' ',cast(Year1 as varchar),'"'),'' ORDER BY Year1,Month1)
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss)T),'');
			
			v_ProfitLossOpeningColumns := COALESCE((SELECT string_agg(',COALESCE((select COALESCE(sum(credit),0)-COALESCE(sum(debit),0) from tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss vj WHERE ((vcaccountcode ilike ''0103%'' or  vcaccountcode ilike ''0104%'' or  vcaccountcode ilike ''0106%'') OR numaccountid = ' || CAST(v_PLCHARTID AS VARCHAR) || ') and datentry_date <  ''' || '1900-01-01'::DATE + make_interval(years =>  (Year1-1900)::INT) + make_interval(months => (Month1 - 1)::INT) + make_interval(days => 0) || '''::DATE),0) AS "' || cast(monthname1 as varchar) || ' ' || cast(Year1 as varchar) || '"','' ORDER BY Year1,Month1)
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss)T),'');

			v_ProfitLossSumOpeningColumns := ',COALESCE((select COALESCE(sum(credit),0)-COALESCE(sum(debit),0) from tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss vj WHERE (( vcaccountcode  ilike ''0103%'' or  vcaccountcode  ilike ''0104%'' or  vcaccountcode  ilike ''0106%'') OR numaccountid = ' || CAST(v_PLCHARTID AS VARCHAR) || ') and datentry_date <  ''' || v_dtFromDate || '''::DATE),0) AS "Total"';

			SELECT
				',' || string_agg('COALESCE("' || cast(monthname1 as varchar) || ' ' || cast(Year1 as varchar) || '",''0'') AS "' || cast(monthname1 as varchar) || ' ' || cast(Year1 as varchar) || '"',','  ORDER BY Year1,MONTH1),
				',' || string_agg('COALESCE("' || cast(monthname1 as varchar) || ' ' || cast(Year1 as varchar) || '",''0'')','+ '  ORDER BY Year1,MONTH1),
				string_agg('"'|| cast(monthname1 as varchar) || ' ' || cast(Year1 as varchar) || '"',','  ORDER BY Year1,MONTH1)
			INTO 
				v_columns,v_SUMColumns,v_PivotColumns
			FROM 
				tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss;
			
			v_SUMColumns := coalesce(v_SUMColumns,'') || ' AS "Total", (CASE WHEN COALESCE(numAccountId,0) > 0 AND COALESCE(bitTotal,false) = false THEN 1 ELSE 2 END) "Type", bitTotal AS "bitTotal", bitIsSubAccount AS "bitIsSubAccount"';

			DROP TABLE IF EXISTS tt_IncomeExpenceYearGetChartAcntDetailsForProfitLoss CASCADE;
			CREATE TEMPORARY TABLE tt_IncomeExpenceYearGetChartAcntDetailsForProfitLoss AS 
			Select 
				COA.Struc row_id
				,COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.bitTotal
				,COA.bitIsSubAccount
				,CONCAT(t2.monthname1,' ',t2.year1) AS MonthYear
				,COALESCE(SUM(CASE 
						WHEN (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode ilike '0104%' OR V.vcAccountCode ilike '0106%' THEN Amount * -1 ELSE Amount END)
 						ELSE Amount 
					END),0) AS Amount
			FROM 
				tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COA 
			LEFT JOIN LATERAL
			(
				SELECT * FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss
			) t2 ON TRUE
			LEFT JOIN 
				tt_TEMPVIEWDATAGetChartAcntDetailsForProfitLoss V 
			ON  
				V.Struc ilike (REPLACE(COA.Struc,'#Total','') || (CASE WHEN v_bitAddTotalRow =true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '' ELSE '%' END))
				AND CONCAT(to_char(V.datentry_date, 'FMMonth'),' ',EXTRACT(YEAR FROM V.datentry_date)) = CONCAT(t2.monthname1,' ',t2.year1)
			GROUP BY 
				COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID	
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.bitTotal
				,COA.bitIsSubAccount
				,CONCAT(t2.monthname1,' ',t2.year1);

			v_Where := CONCAT('	FROM (SELECT ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,vcAccountCode,numAccountId,Struc,bitTotal,bitIsSubAccount,
											' || v_PivotColumns || '
										FROM 
											crosstab (
												''SELECT
	  	t1.Struc as row_id
		,t1.ParentId
		,t1.vcCompundParentKey
		,t1.numAccountTypeID
		,t1.vcAccountType
		,t1.LEVEL
		,t1.vcAccountCode
		,t1.numAccountId
		,t1.Struc
		,t1.bitTotal
		,t1.bitIsSubAccount
		,MonthYear
		,Amount
	FROM
		tt_IncomeExpenceYearGetChartAcntDetailsForProfitLoss t1 order by 1'', $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
											
											) AS final_result(
											row_id TEXT
											,ParentId TEXT
											,vcCompundParentKey TEXT
											,numAccountTypeID NUMERIC
											,vcAccountType TEXT
											,LEVEL INT
											,vcAccountCode TEXT
											,numAccountId NUMERIC
											,Struc TEXT
											,bitTotal BOOLEAN
											,bitIsSubAccount BOOLEAN, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' numeric(20,5))) AS TEMP
											UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''',v_ProfitLossCurrentColumns,v_ProfitLossSumCurrentColumns,',2,true,false
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''',v_ProfitLossOpeningColumns,v_ProfitLossSumOpeningColumns,',2,true,false');
		ELSEIF v_ReportColumn = 'Quarter' THEN
			v_ProfitLossCurrentColumns := COALESCE((SELECT string_agg(CONCAT(',0 AS "Q',cast(Quarter1 as varchar),' ',cast(Year1 as varchar),'"'),'' ORDER BY Year1,Quarter1)
			FROM (SELECT DISTINCT Year1,Quarter1  FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss)T),'');
			
			v_ProfitLossOpeningColumns := COALESCE((SELECT string_agg(',COALESCE((select COALESCE(sum(credit),0)-COALESCE(sum(debit),0) from tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss vj WHERE ((vcaccountcode ilike ''0103%'' or  vcaccountcode ilike ''0104%'' or  vcaccountcode ilike ''0106%'') OR numaccountid = ' || CAST(v_PLCHARTID AS VARCHAR) || ') and datentry_date <  ''' || GetFiscalStartDate(Year1::INT,v_numDomainID) + make_interval(months => ((Quarter1 - 1) * 3)::INT) || '''::DATE),0) AS "Q' || cast(Quarter1 as varchar) || ' ' || cast(Year1 as varchar) || '"','' ORDER BY Year1,Quarter1)
			FROM (SELECT DISTINCT Year1,Quarter1  FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss)T),'');

			v_ProfitLossSumOpeningColumns := ',COALESCE((select COALESCE(sum(credit),0)-COALESCE(sum(debit),0) from tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss vj WHERE (( vcaccountcode  ilike ''0103%'' or  vcaccountcode  ilike ''0104%'' or  vcaccountcode  ilike ''0106%'') OR numaccountid = ' || CAST(v_PLCHARTID AS VARCHAR) || ') and datentry_date <  ''' || v_dtFromDate || '''::DATE),0) AS "Total"';


			SELECT
				',' || string_agg('COALESCE("Q' || cast(Quarter1 as varchar) || ' ' || cast(Year1 as varchar) || '",0) AS "Q' || cast(Quarter1 as varchar) || ' ' || cast(Year1 as varchar) || '"',',' ORDER BY Year1,Quarter1)
				,',' || string_agg('COALESCE("Q' || cast(Quarter1 as varchar) || ' ' || cast(Year1 as varchar) || '",0)','+ ' ORDER BY Year1,Quarter1)
				,string_agg('"Q' || cast(Quarter1 as varchar) || ' ' || cast(Year1 as varchar) || '"',',' ORDER BY Year1,Quarter1)
			INTO 
				v_columns,v_SUMColumns,v_PivotColumns
				FROM (SELECT DISTINCT Year1,Quarter1  FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss) T;

			v_SUMColumns := coalesce(v_SUMColumns,'') || ' AS "Total", (CASE WHEN COALESCE(numAccountId,0) > 0 AND COALESCE(bitTotal,false) = false THEN 1 ELSE 2 END) "Type", bitTotal AS "bitTotal",bitIsSubAccount AS "bitIsSubAccount"';

			DROP TABLE IF EXISTS tt_IncomeExpenceQuarter CASCADE;
			CREATE TEMPORARY TABLE tt_IncomeExpenceQuarter AS 
			Select 
				COA.Struc row_id
				,COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.bitTotal
				,COA.bitIsSubAccount
				,CONCAT('Q',t2.Quarter1,' ',t2.Year1) AS MonthYear
				,SUM(Case 
					When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
					THEN (Case WHEN V.vcAccountCode ilike '0104%' OR V.vcAccountCode ilike '0106%' THEN Amount * -1
					ELSE Amount END)
				ELSE Amount END) AS Amount
			FROM 
				tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COA 
			LEFT JOIN LATERAL
			(
				SELECT DISTINCT Quarter1,Year1 FROM tt_TEMPYEARMONTHGetChartAcntDetailsForProfitLoss
			) t2 ON TRUE
			LEFT JOIN 
				tt_TEMPVIEWDATAGetChartAcntDetailsForProfitLoss V 
			ON  
				V.Struc ilike REPLACE(COA.Struc,'#Total','') || (CASE WHEN v_bitAddTotalRow=true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '' ELSE '%' END) 
				AND CONCAT(GetFiscalQuarter(datEntry_Date,v_numDomainId),' ',GetFiscalyear(datEntry_Date,v_numDomainId)) = CONCAT(t2.Quarter1,' ',t2.Year1)
			GROUP BY 
				t2.Year1
				,t2.Quarter1
				,COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.bitTotal
				,COA.bitIsSubAccount;

			v_Where := CONCAT('	FROM (SELECT ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,vcAccountCode,numAccountId,Struc,bitTotal,bitIsSubAccount,
											' || v_PivotColumns || '
										FROM 
											crosstab (
												''SELECT
	  	t1.Struc as row_id
		,t1.ParentId
		,t1.vcCompundParentKey
		,t1.numAccountTypeID
		,t1.vcAccountType
		,t1.LEVEL
		,t1.vcAccountCode
		,t1.numAccountId
		,t1.Struc
		,t1.bitTotal
		,t1.bitIsSubAccount
		,MonthYear
		,Amount
	FROM
		tt_IncomeExpenceQuarter t1 order by 1'', $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
											
											) AS final_result(
											row_id TEXT
											,ParentId TEXT
											,vcCompundParentKey TEXT
											,numAccountTypeID NUMERIC
											,vcAccountType TEXT
											,LEVEL INT
											,vcAccountCode TEXT
											,numAccountId NUMERIC
											,Struc TEXT
											,bitTotal BOOLEAN
											,bitIsSubAccount BOOLEAN, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' numeric(20,5))) AS TEMP
											UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''',v_ProfitLossCurrentColumns,v_ProfitLossSumCurrentColumns ,',2,true,false
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''',v_ProfitLossOpeningColumns,v_ProfitLossSumOpeningColumns ,',2,true,false');
		end if;

		RAISE NOTICE '% % % %',v_Select,v_columns,v_SUMColumns,v_Where;

		SWV_ExecDyn := 'SELECT * FROM (' || coalesce(v_Select,'') || coalesce(v_columns,'') || coalesce(v_SUMColumns,'')  || ' ' || coalesce(v_Where,'') || ') TEMP ORDER BY (CASE WHEN "Struc" = ''-3'' THEN ''#9999999999#'' WHEN "Struc" = ''-4'' THEN ''#99999999999#'' ELSE "Struc" END)';
   
		OPEN SWV_RefCur FOR EXECUTE SWV_ExecDyn;
	ELSE
		open SWV_RefCur for
		SELECT
			COA.ParentId AS "ParentId"
			,COA.vcCompundParentKey AS "vcCompundParentKey"
			,COA.numAccountId AS "numAccountId"
			,COA.numAccountTypeID AS "numAccountTypeID"
			,COA.vcAccountType AS "vcAccountType"
			,COA.LEVEL AS "LEVEL"
			,COA.vcAccountCode AS "vcAccountCode"
			,('#' || COA.Struc || '#') AS "Struc"
			,COALESCE(SUM(Case 
				When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
				THEN (Case WHEN V.vcAccountCode ilike '0104%' OR V.vcAccountCode ilike '0106%' THEN Amount * -1
				ELSE Amount END)
			ELSE Amount END),0) AS "Total"
			,(CASE WHEN coalesce(COA.numAccountId,0) > 0 AND coalesce(COA.bitTotal,false) = false THEN 1 ELSE 2 END) AS "Type"
			,COA.bitTotal AS "bitTotal"
			,COA.bitIsSubAccount AS "bitIsSubAccount"
		FROM 
			tt_TEMPDIRECTREPORTGetChartAcntDetailsForProfitLoss COA 
		LEFT OUTER JOIN 
			tt_TEMPVIEWDATAGetChartAcntDetailsForProfitLoss V 
		ON
			V.Struc ilike REPLACE(COA.Struc,'#Total','') ||(CASE WHEN coalesce(v_bitAddTotalRow,false) = true AND coalesce(COA.numAccountId,0) > 0 AND COA.bitTotal = false THEN '' ELSE '%' END)
		GROUP BY
			COA.ParentId
			,COA.vcCompundParentKey
			,COA.numAccountTypeID
			,COA.vcAccountType
			,COA.LEVEL
			,COA.vcAccountCode
			,COA.numAccountId
			,COA.Struc
			,COA.bitTotal
			,COA.bitIsSubAccount
		UNION SELECT '', '-3', null, -3,'Profit / (Loss) Current', 0, NULL,'#-3#',0,2,true,false
		UNION SELECT '', '-4', null, -4,'Profit / (Loss) Opening', 0, NULL,'#-4#',COALESCE((select COALESCE(sum(credit),0)-COALESCE(sum(debit),0) from tt_VIEW_JOURNALGetChartAcntDetailsForProfitLoss vj WHERE (( vcaccountcode  ilike '0103%' or  vcaccountcode  ilike '0104%' or  vcaccountcode  ilike '0106%') OR numaccountid = COALESCE(v_PLCHARTID,0)) and datentry_date <  v_dtFromDate::DATE),0),2,true,false
		ORDER BY
		"Struc","Type" desc;
	end if;

   RETURN;
END; $$;


