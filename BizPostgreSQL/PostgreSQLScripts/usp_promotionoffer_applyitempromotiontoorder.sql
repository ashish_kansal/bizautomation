DROP FUNCTION IF EXISTS USP_PromotionOffer_ApplyItemPromotionToOrder;

CREATE OR REPLACE FUNCTION USP_PromotionOffer_ApplyItemPromotionToOrder(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_vcItems TEXT,
	v_bitBasedOnDiscountCode BOOLEAN,
	v_vcDiscountCode VARCHAR(100),
	v_numCurrencyID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_l  INTEGER DEFAULT 1;
   v_lCount  INTEGER;
   v_numTempPromotionID  NUMERIC(18,0);
   v_tintOfferTriggerValueType  SMALLINT;
   v_tintOfferBasedOn  SMALLINT;
   v_tintOfferTriggerValueTypeRange  SMALLINT;
   v_fltOfferTriggerValue  DOUBLE PRECISION;
   v_fltOfferTriggerValueRange  DOUBLE PRECISION;
   v_vcShortDesc  VARCHAR(500);
   v_tintItemCalDiscount  SMALLINT;
   v_monDiscountedItemPrice  DECIMAL(20,5);
   v_numItemCode  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numUnits  DOUBLE PRECISION;
   v_monTotalAmount  DECIMAL(20,5);
   v_numItemClassification  NUMERIC(18,0);

   v_bitRemainingCheckRquired  BOOLEAN;
   v_numRemainingPromotion  DOUBLE PRECISION;
   v_fltDiscountValue  DOUBLE PRECISION;
   v_tintDiscountType  SMALLINT;
   v_tintDiscoutBaseOn  SMALLINT;

   v_j  INTEGER DEFAULT 1;
   v_jCount  INTEGER;

   v_k  INTEGER DEFAULT 1;
   v_kCount  INTEGER;
BEGIN
   v_fltExchangeRate := coalesce((SELECT fltExchangeRate FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = v_numCurrencyID),1);

   IF coalesce(v_fltExchangeRate,0) = 0 then
	
      v_fltExchangeRate := 1;
   end if;

   select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile FROM
   DivisionMaster D
   JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID WHERE
   numDivisionID = v_numDivisionID; 		

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPApplyItemPromotionToOrder_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPApplyItemPromotionToOrder CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPApplyItemPromotionToOrder
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnits DOUBLE PRECISION,
      numWarehouseItemID NUMERIC(18,0),
      monUnitPrice DECIMAL(20,5),
	  monUnitPriceOrig DECIMAL(20,5),
      fltDiscount DECIMAL(20,5),
      bitDiscountType BOOLEAN,
      monTotalAmount DECIMAL(20,5),
      numPromotionID NUMERIC(18,0),
      bitPromotionTriggered BOOLEAN,
      vcPromotionDescription TEXT,
      bitPromotionDiscount BOOLEAN,
      vcKitChildItems TEXT,
      vcInclusionDetail TEXT,
      numSelectedPromotionID NUMERIC(18,0),
      bitChanged BOOLEAN,
	  bitDisablePromotion BOOLEAN
   );

   IF coalesce(v_vcItems,'') <> '' then      
      INSERT INTO tt_TEMPApplyItemPromotionToOrder(numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,monUnitPriceOrig
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
			,bitDisablePromotion)
      SELECT
      numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice*v_fltExchangeRate
			,monUnitPrice*v_fltExchangeRate
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,CAST(0 AS BOOLEAN)
			,bitDisablePromotion
      FROM
	  XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_vcItems AS XML)
			COLUMNS
				numOppItemID NUMERIC(18,0) PATH 'numOppItemID'
				,numItemCode NUMERIC(18,0) PATH 'numItemCode'
				,numUnits DOUBLE PRECISION PATH 'numUnits'
				,numWarehouseItemID NUMERIC(18,0) PATH 'numWarehouseItemID'
				,monUnitPrice DECIMAL(20,5) PATH 'monUnitPrice'
				,fltDiscount DECIMAL(20,5) PATH 'fltDiscount'
				,bitDiscountType BOOLEAN PATH 'bitDiscountType'
				,monTotalAmount DECIMAL(20,5) PATH 'monTotalAmount'
				,numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
				,bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered'
				,bitPromotionDiscount BOOLEAN PATH 'bitPromotionDiscount'
				,vcKitChildItems TEXT PATH 'vcKitChildItems'
				,vcInclusionDetail TEXT PATH 'vcInclusionDetail'
				,vcPromotionDescription TEXT PATH 'vcPromotionDescription'
				,numSelectedPromotionID NUMERIC(18,0) PATH 'numSelectedPromotionID'
				,bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
		) X;

		--UPDATE
		--	tt_TEMPApplyItemPromotionToOrder T1
		--SET
		--	bitChanged = true
		--	,numPromotionID = 0
		--	,bitPromotionTriggered = false
		--	,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
		--	,fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
		--	,bitDiscountType = CASE WHEN coalesce((select tintDisountType from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0) = 1 THEN true ELSE false END
		--WHERE
		--	bitPromotionDiscount = true AND bitDisablePromotion = true;


      SELECT COUNT(*) INTO v_iCount FROM tt_TEMPApplyItemPromotionToOrder;
      SELECT COUNT(*) INTO v_lCount FROM tt_TEMPApplyItemPromotionToOrder;
      DROP TABLE IF EXISTS tt_TEMPUSEDPROMOTION CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPUSEDPROMOTION
      (
         ID INTEGER,
         numPromotionID NUMERIC(18,0),
         numUnits DOUBLE PRECISION,
         monTotalAmount DECIMAL(20,5)
      );
      INSERT INTO tt_TEMPUSEDPROMOTION(ID
			,numPromotionID
			,numUnits
			,monTotalAmount)
      SELECT
      ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
      FROM
      tt_TEMPApplyItemPromotionToOrder T1
      WHERE
      coalesce(numPromotionID,0) > 0
      AND coalesce(bitPromotionTriggered,false) = true
      GROUP BY
      numPromotionID;
      v_jCount := coalesce((SELECT COUNT(*) FROM tt_TEMPUSEDPROMOTION),0);

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE
			tt_TEMPApplyItemPromotionToOrder T1
		SET
			numPromotionID = 0
			,bitPromotionTriggered = false
			,vcPromotionDescription = ''
			,bitPromotionDiscount = false
			,bitChanged = true
			,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
			,fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
			,bitDiscountType = (CASE WHEN coalesce((select tintDisountType from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0) = 1 THEN true ELSE false END)
		WHERE
			(numPromotionID > 0
			AND coalesce(bitPromotionTriggered,false) = false
			AND(SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder T3 WHERE T3.numPromotionID = T1.numPromotionID AND coalesce(T3.bitPromotionTriggered,false) = true) = 0);

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
      WHILE v_j <= v_jCount LOOP
         select   numPromotionID, numUnits, monTotalAmount INTO v_numTempPromotionID,v_numUnits,v_monTotalAmount FROM
         tt_TEMPUSEDPROMOTION WHERE
         ID = v_j;
         IF NOT EXISTS(SELECT
         PO.numProId
         FROM
         PromotionOffer PO
         LEFT JOIN
         PromotionOffer POOrder
         ON
         PO.numOrderPromotionID = POOrder.numProId
         LEFT JOIN
         DiscountCodes DC
         ON
         PO.numOrderPromotionID = DC.numPromotionID
         WHERE
         PO.numDomainId = v_numDomainID
         AND PO.numProId = v_numTempPromotionID
         AND coalesce(PO.bitEnabled,false) = true
         AND coalesce(PO.IsOrderBasedPromotion,false) = false
         AND 1 =(CASE
         WHEN coalesce(PO.numOrderPromotionID,0) > 0
         THEN(CASE WHEN POOrder.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN 1 ELSE 0 END) END)
         ELSE(CASE WHEN PO.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN 1 ELSE 0 END) END)
         END)
         AND 1 =(CASE
         WHEN coalesce(PO.numOrderPromotionID,0) > 0
         THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
         ELSE(CASE PO.tintCustomersBasedOn
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionID) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
            WHEN 3 THEN 1
            ELSE 0
            END)
         END)
         AND 1 =(CASE
         WHEN coalesce(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
         THEN
            CASE
            WHEN coalesce(PO.tintOfferTriggerValueTypeRange,1) = 2
            THEN(CASE WHEN coalesce(v_numUnits,0) BETWEEN coalesce(PO.fltOfferTriggerValue,0):: DOUBLE PRECISION AND coalesce(PO.fltOfferTriggerValueRange,0):: DOUBLE PRECISION THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(v_numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
            END
         WHEN coalesce(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
         THEN
            CASE
            WHEN coalesce(PO.tintOfferTriggerValueTypeRange,1) = 2
            THEN(CASE WHEN coalesce(v_monTotalAmount,0) BETWEEN coalesce(PO.fltOfferTriggerValue,0):: DECIMAL(20,5) AND coalesce(PO.fltOfferTriggerValueRange,0):: DECIMAL(20,5) THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(v_monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
            END
         END)
         AND 1 =(CASE
         WHEN coalesce(PO.numOrderPromotionID,0) > 0
         THEN(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN(CASE WHEN coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivisionID),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
         ELSE 1
         END)) then
				-- IF Promotion is not valid than revert line item price to based on default pricing
            UPDATE
				tt_TEMPApplyItemPromotionToOrder T1
            SET
				numPromotionID = 0
				,bitPromotionTriggered = false
				,vcPromotionDescription = ''
				,bitPromotionDiscount = false
				,bitChanged = true
				,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
				,fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
				,bitDiscountType = CASE WHEN coalesce((select tintDisountType from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0) = 1 THEN true ELSE false END
            WHERE
            numPromotionID = v_numTempPromotionID;
         end if;
         v_j := v_j::bigint+1;
      END LOOP;
      UPDATE
      tt_TEMPApplyItemPromotionToOrder T1
      SET
		bitPromotionDiscount = false
		,bitChanged = true
		,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
		,fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
		,bitDiscountType = CASE WHEN coalesce((select tintDisountType from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0) = 1 THEN true ELSE false END
      FROM
      PromotionOffer PO
      WHERE
      T1.numPromotionID = PO.numProId AND(coalesce(numPromotionID,0) > 0
      AND coalesce(bitPromotionDiscount,false) = true
      AND coalesce(PO.tintDiscountType,0) = 3);
      IF coalesce(v_bitBasedOnDiscountCode,false) = true then
         BEGIN
            CREATE TEMP SEQUENCE tt_TableItemCouponPromotion_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TABLEITEMCOUPONPROMOTION CASCADE;
         CREATE TEMPORARY TABLE tt_TABLEITEMCOUPONPROMOTION
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numPromotionID NUMERIC(18,0),
            tintOfferTriggerValueType SMALLINT,
            tintOfferTriggerValueTypeRange SMALLINT,
            fltOfferTriggerValue DOUBLE PRECISION,
            fltOfferTriggerValueRange DOUBLE PRECISION,
            tintOfferBasedOn SMALLINT,
            vcShortDesc VARCHAR(300)
         );
         INSERT INTO tt_TABLEITEMCOUPONPROMOTION(numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc)
         SELECT
         PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,coalesce(PO.vcShortDesc,'-')
         FROM
         PromotionOffer PO
         INNER JOIN
         PromotionOffer POOrder
         ON
         PO.numOrderPromotionID = POOrder.numProId
         INNER JOIN
         DiscountCodes DC
         ON
         POOrder.numProId = DC.numPromotionID
         WHERE
         PO.numDomainId = v_numDomainID
         AND POOrder.numDomainId = v_numDomainID
         AND coalesce(PO.bitEnabled,false) = true
         AND coalesce(POOrder.bitEnabled,false) = true
         AND coalesce(PO.bitUseOrderPromotion,false) = true
         AND coalesce(POOrder.IsOrderBasedPromotion,false) = true
         AND coalesce(POOrder.bitRequireCouponCode,false) = true
         AND 1 =(CASE WHEN POOrder.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN 1 ELSE 0 END) END)
         AND 1 =(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN(CASE WHEN coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivisionID),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
         AND 1 =(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = POOrder.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
         AND DC.vcDiscountCode = v_vcDiscountCode;
         v_l := 1;
         SELECT COUNT(*) INTO v_kCount FROM tt_TABLEITEMCOUPONPROMOTION;
         WHILE v_k <= v_kCount LOOP
            select   numPromotionID, tintOfferTriggerValueType, tintOfferTriggerValueTypeRange, fltOfferTriggerValue, fltOfferTriggerValueRange, tintOfferBasedOn, vcShortDesc INTO v_numTempPromotionID,v_tintOfferTriggerValueType,v_tintOfferTriggerValueTypeRange,
            v_fltOfferTriggerValue,v_fltOfferTriggerValueRange,v_tintOfferBasedOn,
            v_vcShortDesc FROM
            tt_TABLEITEMCOUPONPROMOTION WHERE
            ID = v_k;
            IF(SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true) = 0 then
				
               IF coalesce((SELECT(CASE WHEN v_tintOfferTriggerValueType = 2 THEN SUM(coalesce(monTotalAmount,0)) ELSE SUM(coalesce(numUnits,0)) END)
               FROM
               tt_TEMPApplyItemPromotionToOrder T1
               INNER JOIN
               Item I
               ON
               T1.numItemCode = I.numItemCode
               WHERE
               coalesce(numPromotionID,0) = 0
               AND 1 =(CASE
               WHEN v_tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 1 AND numValue = T1.numItemCode) > 0 THEN 1 ELSE 0 END)
               WHEN v_tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
               WHEN v_tintOfferBasedOn = 4 THEN 1
               ELSE 0
               END)),0) >= coalesce(v_fltOfferTriggerValue,0) then
					
                  v_i := 1;
                  WHILE v_i <= v_iCount LOOP
                     IF coalesce((SELECT(CASE WHEN v_tintOfferTriggerValueType = 2 THEN SUM(coalesce(monTotalAmount,0)) ELSE SUM(coalesce(numUnits,0)) END)
                     FROM
                     tt_TEMPApplyItemPromotionToOrder T1
                     WHERE
                     coalesce(numPromotionID,0) = v_numTempPromotionID
                     AND coalesce(bitPromotionTriggered,false) = true),0) < coalesce(v_fltOfferTriggerValue,0) then
							
                        UPDATE
                        tt_TEMPApplyItemPromotionToOrder T1
                        SET
                        bitChanged = true,numPromotionID = v_numTempPromotionID,bitPromotionTriggered = true,
                        vcPromotionDescription = coalesce(v_vcShortDesc,'')
                        FROM
                        Item I
                        WHERE
                        T1.numItemCode = I.numItemCode AND(T1.ID = v_i
                        AND coalesce(numPromotionID,0) = 0
                        AND 1 =(CASE
                        WHEN coalesce(v_tintOfferTriggerValueType,1) = 1 --Based on quantity
                        THEN
                           CASE
                           WHEN coalesce(v_tintOfferTriggerValueTypeRange,1) = 2
                           THEN(CASE WHEN coalesce(T1.numUnits,0)+coalesce((SELECT SUM(coalesce(numUnits,0)) FROM tt_TEMPApplyItemPromotionToOrder WHERE coalesce(numPromotionID,0) = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true),0) BETWEEN coalesce(v_fltOfferTriggerValue,0) AND coalesce(v_fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
                           ELSE
                              1
                           END
                        WHEN coalesce(v_tintOfferTriggerValueType,1) = 2 --Based on amount
                        THEN
                           CASE
                           WHEN coalesce(v_tintOfferTriggerValueTypeRange,1) = 2
                           THEN(CASE WHEN coalesce(T1.monTotalAmount,0)+coalesce((SELECT SUM(coalesce(monTotalAmount,0)) FROM tt_TEMPApplyItemPromotionToOrder WHERE coalesce(numPromotionID,0) = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true),0) BETWEEN coalesce(v_fltOfferTriggerValue,0) AND coalesce(v_fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
                           ELSE
                              1
                           END
                        END)
                        AND 1 =(CASE
                        WHEN v_tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 1 AND numValue = T1.numItemCode) > 0 THEN 1 ELSE 0 END)
                        WHEN v_tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
                        WHEN v_tintOfferBasedOn = 4 THEN 1
                        ELSE 0
                        END));
                     end if;
                     v_i := v_i::bigint+1;
                  END LOOP;
               end if;
            end if;
            v_k := v_k::bigint+1;
         END LOOP;
      end if;

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
      v_i := 1;
      WHILE v_i <= v_iCount LOOP
         v_numTempPromotionID := NULL;
         select   numProId, tintOfferTriggerValueType, tintOfferTriggerValueTypeRange, fltOfferTriggerValue, fltOfferTriggerValueRange, tintOfferBasedOn, vcShortDesc INTO v_numTempPromotionID,v_tintOfferTriggerValueType,v_tintOfferTriggerValueTypeRange,
         v_fltOfferTriggerValue,v_fltOfferTriggerValueRange,v_tintOfferBasedOn,
         v_vcShortDesc FROM
         tt_TEMPApplyItemPromotionToOrder T1
         INNER JOIN
         Item I
         ON
         T1.numItemCode = I.numItemCode
         CROSS JOIN LATERAL(SELECT 
            PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,coalesce(PO.vcShortDesc,'') AS vcShortDesc
            FROM
            PromotionOffer PO
            WHERE
            PO.numDomainId = v_numDomainID
            AND 1 =(CASE WHEN coalesce(T1.numSelectedPromotionID,0) > 0 THEN(CASE WHEN PO.numProId = T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END)
            AND coalesce(PO.bitEnabled,false) = true
            AND coalesce(PO.IsOrderBasedPromotion,false) = false
            AND coalesce(PO.bitUseOrderPromotion,false) = false
            AND coalesce(PO.bitRequireCouponCode,false) = false
            AND 1 =(CASE WHEN PO.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN 1 ELSE 0 END) END)
            AND 1 =(CASE
            WHEN coalesce(numOrderPromotionID,0) > 0
            THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
            ELSE(CASE tintCustomersBasedOn
               WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionID) > 0 THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
               WHEN 3 THEN 1
               ELSE 0
               END)
            END)
            AND 1 =(CASE
            WHEN PO.tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = T1.numItemCode) > 0 THEN 1 ELSE 0 END)
            WHEN PO.tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
            WHEN PO.tintOfferBasedOn = 4 THEN 1
            ELSE 0
            END)
            ORDER BY(CASE
            WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
            WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
            WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
            WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
            WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
            WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
            END) ASC,coalesce(PO.fltOfferTriggerValue,0) DESC LIMIT 1) T2 WHERE
         coalesce(T1.numPromotionID,0)  = 0
         AND T1.ID = v_i
         AND coalesce((SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder T3 WHERE T3.numPromotionID = T2.numProId AND T3.bitPromotionTriggered = true),0) = 0;
         IF coalesce(v_numTempPromotionID,0) > 0 AND(SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true) = 0 then
			
            IF coalesce((SELECT(CASE WHEN v_tintOfferTriggerValueType = 2 THEN SUM(coalesce(monTotalAmount,0)) ELSE SUM(coalesce(numUnits,0)) END)
            FROM
            tt_TEMPApplyItemPromotionToOrder T1
            INNER JOIN
            Item I
            ON
            T1.numItemCode = I.numItemCode
            WHERE
            coalesce(numPromotionID,0) = 0
            AND 1 =(CASE
            WHEN v_tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 1 AND numValue = T1.numItemCode) > 0 THEN 1 ELSE 0 END)
            WHEN v_tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
            WHEN v_tintOfferBasedOn = 4 THEN 1
            ELSE 0
            END)),0) >= coalesce(v_fltOfferTriggerValue,0) then
				
               v_l := 1;
               WHILE v_l <= v_lCount LOOP
                  IF coalesce((SELECT(CASE WHEN v_tintOfferTriggerValueType = 2 THEN SUM(coalesce(monTotalAmount,0)) ELSE SUM(coalesce(numUnits,0)) END)
                  FROM
                  tt_TEMPApplyItemPromotionToOrder T1
                  WHERE
                  coalesce(numPromotionID,0) = v_numTempPromotionID
                  AND coalesce(bitPromotionTriggered,false) = true),0) < coalesce(v_fltOfferTriggerValue,0) then
						
                     UPDATE
                     tt_TEMPApplyItemPromotionToOrder T1
                     SET
                     bitChanged = true,numPromotionID = v_numTempPromotionID,bitPromotionTriggered = true,
                     vcPromotionDescription = coalesce(v_vcShortDesc,'')
                     FROM
                     Item I
                     WHERE
                     T1.numItemCode = I.numItemCode AND(T1.ID = v_l
                     AND coalesce(numPromotionID,0) = 0
                     AND 1 =(CASE
                     WHEN coalesce(v_tintOfferTriggerValueType,1) = 1 --Based on quantity
                     THEN
                        CASE
                        WHEN coalesce(v_tintOfferTriggerValueTypeRange,1) = 2
                        THEN(CASE WHEN coalesce(T1.numUnits,0)+coalesce((SELECT SUM(coalesce(numUnits,0)) FROM tt_TEMPApplyItemPromotionToOrder WHERE coalesce(numPromotionID,0) = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true),0) BETWEEN coalesce(v_fltOfferTriggerValue,0) AND coalesce(v_fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
                        ELSE
                           1
                        END
                     WHEN coalesce(v_tintOfferTriggerValueType,1) = 2 --Based on amount
                     THEN
                        CASE
                        WHEN coalesce(v_tintOfferTriggerValueTypeRange,1) = 2
                        THEN(CASE WHEN coalesce(T1.monTotalAmount,0)+coalesce((SELECT SUM(coalesce(monTotalAmount,0)) FROM tt_TEMPApplyItemPromotionToOrder WHERE coalesce(numPromotionID,0) = v_numTempPromotionID AND coalesce(bitPromotionTriggered,false) = true),0) BETWEEN coalesce(v_fltOfferTriggerValue,0) AND coalesce(v_fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
                        ELSE
                           1
                        END
                     END)
                     AND 1 =(CASE
                     WHEN v_tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 1 AND numValue = T1.numItemCode) > 0 THEN 1 ELSE 0 END)
                     WHEN v_tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 5 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
                     WHEN v_tintOfferBasedOn = 4 THEN 1
                     ELSE 0
                     END));
                  end if;
                  v_l := v_l::bigint+1;
               END LOOP;
            end if;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      DROP TABLE IF EXISTS tt_TEMPPROMOTION CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPPROMOTION
      (
         ID INTEGER,
         numPromotionID NUMERIC(18,0),
         vcShortDesc VARCHAR(500),
         numTriggerItemCode NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numUnits DOUBLE PRECISION,
         monTotalAmount DECIMAL(20,5)
      );
      INSERT INTO tt_TEMPPROMOTION(ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount)
      SELECT
      ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
      FROM
      tt_TEMPApplyItemPromotionToOrder T1
      INNER JOIN
      PromotionOffer PO
      ON
      T1.numPromotionID = PO.numProId
      WHERE
      coalesce(numPromotionID,0) > 0
      AND coalesce(bitPromotionTriggered,false) = true
      AND 1 =(CASE WHEN v_bitBasedOnDiscountCode = false THEN(CASE WHEN coalesce(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END);
      v_j := 1;
      v_jCount := coalesce((SELECT COUNT(*) FROM tt_TEMPPROMOTION),0);
      WHILE v_j <= v_jCount LOOP
         select   numPromotionID, numTriggerItemCode, numWarehouseItemID, numUnits, monTotalAmount INTO v_numTempPromotionID,v_numItemCode,v_numTempWarehouseItemID,v_numUnits,
         v_monTotalAmount FROM
         tt_TEMPPROMOTION WHERE
         ID = v_j;
         select   coalesce(fltDiscountValue,0), coalesce(tintDiscountType,0), tintDiscoutBaseOn, coalesce(vcShortDesc,''), coalesce(tintItemCalDiscount,1), coalesce(monDiscountedItemPrice,0) INTO v_fltDiscountValue,v_tintDiscountType,v_tintDiscoutBaseOn,v_vcShortDesc,
         v_tintItemCalDiscount,v_monDiscountedItemPrice FROM
         PromotionOffer WHERE
         numProId = v_numTempPromotionID;

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
         IF v_tintDiscountType = 2 OR v_tintDiscountType = 3 then
			
            v_bitRemainingCheckRquired := true;
         ELSE
            v_bitRemainingCheckRquired := false;
            v_numRemainingPromotion := 0;
         end if;

			-- If promotion is valid than check whether any item left to apply promotion
         v_i := 1;
         WHILE v_i <= v_iCount LOOP
            IF v_bitRemainingCheckRquired = true then
				
               IF v_tintDiscountType = 2 then -- Discount by amount
					
                  v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(fltDiscount) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
               ELSEIF v_tintDiscountType = 3
               then -- Discount by quantity
					
                  IF v_tintDiscoutBaseOn IN(5,6) then
						
                     v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(numUnits) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
                  ELSE
                     v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(fltDiscount/monUnitPrice) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
                  end if;
               end if;
            end if;
            IF v_bitRemainingCheckRquired = false OR (v_bitRemainingCheckRquired = true AND v_numRemainingPromotion > 0) then
               UPDATE
               tt_TEMPApplyItemPromotionToOrder TMain
               SET
               monUnitPrice = (CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END)
               ,fltDiscount =(CASE
               WHEN v_bitRemainingCheckRquired = false
               THEN
                  v_fltDiscountValue
               ELSE(CASE
                  WHEN v_tintDiscountType = 2 -- Discount by amount
                  THEN(CASE
                     WHEN(T1.numUnits*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END)) >= v_numRemainingPromotion
                     THEN v_numRemainingPromotion
                     ELSE(T1.numUnits*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                     END)
                  WHEN v_tintDiscountType = 3 -- Discount by quantity
                  THEN(CASE
                     WHEN v_tintDiscoutBaseOn IN(5,6)
                     THEN(CASE
                        WHEN(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                        THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                        ELSE T1.numUnits
                        END) >= v_numRemainingPromotion
                        THEN v_numRemainingPromotion
                        ELSE(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                           THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                           ELSE T1.numUnits
                           END)
                        END)*(CASE
                        WHEN(CASE WHEN v_tintItemCalDiscount = 1 THEN coalesce(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END) > v_monDiscountedItemPrice
                        THEN((CASE WHEN v_tintItemCalDiscount = 1 THEN coalesce(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END) -v_monDiscountedItemPrice)
                        ELSE(CASE WHEN v_tintItemCalDiscount = 1 THEN coalesce(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END)
                        END)
                     ELSE(CASE
                        WHEN(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                        THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                        ELSE T1.numUnits
                        END) >= v_numRemainingPromotion
                        THEN(v_numRemainingPromotion*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                        ELSE((CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                           THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                           ELSE T1.numUnits
                           END)*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                        END)
                     END)
                  ELSE 0
                  END)
               END),bitDiscountType =(CASE WHEN v_bitRemainingCheckRquired = false THEN false ELSE true END),numPromotionID = v_numTempPromotionID,bitPromotionTriggered =(CASE WHEN T1.bitPromotionTriggered = true THEN true ELSE false END),
               vcPromotionDescription = v_vcShortDesc,bitChanged = true,bitPromotionDiscount = true
               FROM
                tt_TEMPApplyItemPromotionToOrder T1
				INNER JOIN
				Item I
				ON
				T1.numItemCode = I.numItemCode 
				LEFT JOIN 
				PromotionOffer PO
				ON
				T1.numPromotionID = PO.numProId
               WHERE
					TMain.ID = v_i
					AND T1.ID = v_i 
					AND (coalesce(T1.numPromotionID,0) = 0 OR (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionDiscount,false) = false))
					AND 1 =(CASE
								WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
								THEN (CASE WHEN(CASE WHEN PO.tintOfferTriggerValueType = 2 THEN coalesce(T1.monTotalAmount,0) ELSE coalesce(T1.numUnits,0) END) >= coalesce(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								ELSE 1
							END)
					AND 1 =(CASE
							   WHEN v_tintDiscoutBaseOn = 1 -- Selected Items
							   THEN (CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 1 AND numValue = I.numItemCode) > 0 THEN 1 ELSE 0 END)
							   WHEN v_tintDiscoutBaseOn = 2 -- Selected Item Classification
							   THEN (CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							   WHEN v_tintDiscoutBaseOn = 3 -- Related Items
							   THEN (CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numDomainId = v_numDomainID AND numParentItemCode = v_numItemCode AND numItemCode = I.numItemCode) > 0 THEN 1 ELSE 0 END)
							   WHEN v_tintDiscoutBaseOn IN(4,5) -- Item with list price lesser or equal
							   THEN (CASE WHEN coalesce(I.monListPrice,0) <= coalesce((SELECT monListPrice FROM Item WHERE numItemCode = v_numItemCode),0) AND coalesce(T1.bitPromotionTriggered,false) = false THEN 1 ELSE 0 END)
								WHEN v_tintDiscoutBaseOn = 6 -- Selected Items 
								THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 4 AND numValue = I.numItemCode) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         v_j := v_j::bigint+1;
      END LOOP;

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
      IF(SELECT
      COUNT(*)
      FROM
      tt_TEMPApplyItemPromotionToOrder T1
      INNER JOIN
      PromotionOffer
      ON
      T1.numPromotionID = PromotionOffer.numProId
      AND coalesce(PromotionOffer.bitUseOrderPromotion,false) = true
      WHERE
      coalesce(T1.numPromotionID,0) > 0
      AND coalesce(T1.bitPromotionTriggered,false) = true
      AND coalesce(T1.bitPromotionDiscount,false) = false
      AND(SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder T2 WHERE T2.numPromotionID = T1.numPromotionID AND coalesce(bitPromotionTriggered,false) = false) = 0) > 0 then
		
         UPDATE
			tt_TEMPApplyItemPromotionToOrder T1
         SET
			numPromotionID = 0
			,bitPromotionTriggered = false
			,bitPromotionDiscount = false
			,vcPromotionDescription = ''
			,bitChanged = true
			,monUnitPrice = coalesce((select monPrice from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
			,fltDiscount = coalesce((select decDiscount from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0)
			,bitDiscountType = CASE WHEN coalesce((select tintDisountType from fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,T1.numItemCode,T1.numUnits,false,0,T1.numWarehouseItemID,T1.vcKitChildItems,v_numCurrencyID)),0) = 1 THEN true ELSE false END
         FROM
			PromotionOffer
         WHERE(T1.numPromotionID = PromotionOffer.numProId
         AND coalesce(PromotionOffer.bitUseOrderPromotion,false) = true) AND(coalesce(T1.numPromotionID,0) > 0
         AND coalesce(T1.bitPromotionTriggered,false) = true
         AND coalesce(T1.bitPromotionDiscount,false) = false
         AND(SELECT COUNT(*) FROM tt_TEMPApplyItemPromotionToOrder T2 WHERE T2.numPromotionID = T1.numPromotionID AND coalesce(bitPromotionTriggered,false) = false) = 0);
         v_j := 1;
         v_jCount := coalesce((SELECT COUNT(*) FROM tt_TEMPPROMOTION),0);
         WHILE v_j <= v_jCount LOOP
            select   numPromotionID, numTriggerItemCode, numWarehouseItemID, numUnits, monTotalAmount INTO v_numTempPromotionID,v_numItemCode,v_numTempWarehouseItemID,v_numUnits,
            v_monTotalAmount FROM
            tt_TEMPPROMOTION WHERE
            ID = v_j;
            select   coalesce(fltDiscountValue,0), coalesce(tintDiscountType,0), tintDiscoutBaseOn, coalesce(vcShortDesc,''), coalesce(tintItemCalDiscount,1) INTO v_fltDiscountValue,v_tintDiscountType,v_tintDiscoutBaseOn,v_vcShortDesc,
            v_tintItemCalDiscount FROM
            PromotionOffer WHERE
            numProId = v_numTempPromotionID;

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
            IF v_tintDiscountType = 2 OR v_tintDiscountType = 3 then
				
               v_bitRemainingCheckRquired := true;
            ELSE
               v_bitRemainingCheckRquired := false;
               v_numRemainingPromotion := 0;
            end if;

				-- If promotion is valid than check whether any item left to apply promotion
            v_i := 1;
            WHILE v_i <= v_iCount LOOP
               IF v_bitRemainingCheckRquired = true then
					
                  IF v_tintDiscountType = 2 then -- Discount by amount
						
                     v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(fltDiscount) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
                  ELSEIF v_tintDiscountType = 3
                  then -- Discount by quantity
						
                     IF v_tintDiscoutBaseOn IN(5,6) then
							
                        v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(numUnits) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
                     ELSE
                        v_numRemainingPromotion := v_fltDiscountValue -coalesce((SELECT SUM(fltDiscount/monUnitPrice) FROM tt_TEMPApplyItemPromotionToOrder WHERE numPromotionID = v_numTempPromotionID	AND coalesce(bitDiscountType,false) = true AND bitPromotionDiscount = true),0);
                     end if;
                  end if;
               end if;
               IF v_bitRemainingCheckRquired = false OR (v_bitRemainingCheckRquired = true AND v_numRemainingPromotion > 0) then
					
                  UPDATE
                  tt_TEMPApplyItemPromotionToOrder TMain
                  SET
                  monUnitPrice = (CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END)
					,fltDiscount =(CASE
                  WHEN v_bitRemainingCheckRquired = false
                  THEN
                     v_fltDiscountValue
                  ELSE(CASE
                     WHEN v_tintDiscountType = 2 -- Discount by amount
                     THEN(CASE
                        WHEN(T1.numUnits*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END)) >= v_numRemainingPromotion
                        THEN v_numRemainingPromotion
                        ELSE(T1.numUnits*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                        END)
                     WHEN v_tintDiscountType = 3 -- Discount by quantity
                     THEN(CASE
                        WHEN v_tintDiscoutBaseOn IN(5,6)
                        THEN(CASE
                           WHEN(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                           THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                           ELSE T1.numUnits
                           END) >= v_numRemainingPromotion
                           THEN v_numRemainingPromotion
                           ELSE(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                              THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                              ELSE T1.numUnits
                              END)
                           END)*(CASE
                           WHEN(CASE WHEN v_tintItemCalDiscount = 1 THEN COALESCE(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END) > v_monDiscountedItemPrice
                           THEN((CASE WHEN v_tintItemCalDiscount = 1 THEN COALESCE(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END) -v_monDiscountedItemPrice)
                           ELSE(CASE WHEN v_tintItemCalDiscount = 1 THEN COALESCE(T1.monUnitPriceOrig,0) ELSE coalesce(I.monListPrice,0) END)
                           END)
                        ELSE(CASE
                           WHEN(CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                           THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                           ELSE T1.numUnits
                           END) >= v_numRemainingPromotion
                           THEN(v_numRemainingPromotion*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                           ELSE((CASE WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionTriggered,false) = true AND coalesce(T1.bitPromotionDiscount,false) = false)
                              THEN T1.numUnits -coalesce(PO.fltOfferTriggerValue,0)
                              ELSE T1.numUnits
                              END)*(CASE 
									WHEN v_tintItemCalDiscount = 1 
									THEN COALESCE(T1.monUnitPriceOrig,0)
									ELSE COALESCE(CASE
													WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
													THEN (GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,I.numItemCode,T1.numUnits,v_numTempWarehouseItemID,coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,coalesce(T1.vcKitChildItems,''),v_numCurrencyID,v_fltExchangeRate)*v_fltExchangeRate)
													ELSE coalesce(I.monListPrice,0) 
												END,0) 
								END))
                           END)
                        END)
                     ELSE 0
                     END)
                  END),bitDiscountType =(CASE WHEN v_bitRemainingCheckRquired = false THEN false ELSE true END),numPromotionID = v_numTempPromotionID,
                  bitPromotionTriggered =(CASE WHEN T1.bitPromotionTriggered = true THEN true ELSE false END),vcPromotionDescription = v_vcShortDesc,bitChanged = true,bitPromotionDiscount = true
                  FROM
				  tt_TEMPApplyItemPromotionToOrder T1
				  INNER JOIN
                  Item I ON T1.numItemCode = I.numItemCode
                  LEFT JOIN PromotionOffer PO ON T1.numPromotionID = PO.numProId
				  WHERE
					TMain.ID = v_i
					AND T1.ID = v_i
					AND(coalesce(T1.numPromotionID,0) = 0 OR(coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionDiscount,false) = false))
					AND 1 =(CASE
							  WHEN (coalesce(T1.numPromotionID,0) = v_numTempPromotionID AND coalesce(T1.bitPromotionDiscount,false) = false)
							  THEN(CASE WHEN(CASE WHEN PO.tintOfferTriggerValueType = 2 THEN coalesce(T1.monTotalAmount,0) ELSE coalesce(T1.numUnits,0) END) >= coalesce(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
							  ELSE 1
							END)
					AND 1 =(CASE
							  WHEN v_tintDiscoutBaseOn = 1 -- Selected Items
							  THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 1 AND numValue = I.numItemCode) > 0 THEN 1 ELSE 0 END)
							  WHEN v_tintDiscoutBaseOn = 2 -- Selected Item Classification
							  THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 2 AND numValue = coalesce(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							  WHEN v_tintDiscoutBaseOn = 3 -- Related Items
							  THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numDomainId = v_numDomainID AND numParentItemCode = v_numItemCode AND numItemCode = I.numItemCode) > 0 THEN 1 ELSE 0 END)
							  WHEN v_tintDiscoutBaseOn IN(4,5) -- Item with list price lesser or equal
							  THEN(CASE WHEN coalesce(I.monListPrice,0) <= coalesce((SELECT monListPrice FROM Item WHERE numItemCode = v_numItemCode),
								 0) AND coalesce(T1.bitPromotionTriggered,false) = false THEN 1 ELSE 0 END)
							  WHEN v_tintDiscoutBaseOn = 6 -- Selected Items
							  THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numTempPromotionID AND tintRecordType = 6 AND tintType = 4 AND numValue = I.numItemCode) > 0 THEN 1 ELSE 0 END)
							  ELSE 0
							END);
               end if;
               v_i := v_i::bigint+1;
            END LOOP;
            v_j := v_j::bigint+1;
         END LOOP;
      end if;
   end if;

   UPDATE
   tt_TEMPApplyItemPromotionToOrder
   SET
   monUnitPrice =(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monUnitPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monUnitPrice,0) END),fltDiscount =(CASE WHEN coalesce(bitDiscountType,false) = false THEN(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(fltDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE fltDiscount END) ELSE fltDiscount END)
   WHERE
   bitChanged = true;

   open SWV_RefCur for SELECT * FROM tt_TEMPApplyItemPromotionToOrder WHERE bitChanged = true;
END; $$;












