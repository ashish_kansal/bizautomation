-- Stored procedure definition script USP_BillingTerms for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BillingTerms(v_nmDivisionID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		coalesce(tintBillingTerms,0) as tintBillingTerms
		,coalesce(numBillingDays,0) as numBillingDays
		,coalesce(tintInterestType,0) as tintInterestType
		,coalesce(fltInterest,0) as fltInterest
		,coalesce(numAssignedTo,0) AS numAssignedTo
		,intShippingCompany
		,numDefaultShippingServiceID
   FROM
   DivisionMaster
   WHERE
   numDivisionID = v_nmDivisionID;
END; $$;
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  













