CREATE OR REPLACE FUNCTION USP_GetChartAcntDetForBalanceReconcile(v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql          
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);
BEGIN
   v_strSQL := '  with recursive RecursionCTE(RecordID,ParentRecordID,vcCatgyName,TOC,T)                      
 as                      
 (                      
 select numAccountId AS RecordID,numParntAcntId AS ParentRecordID,vcCatgyName AS vcCatgyName,CAST('''' AS VARCHAR(1000)) AS TOC,CAST('''' AS VARCHAR(1000)) AS T     
                     
    from Chart_Of_Accounts                      
    where numParntAcntId is null and numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
   || ' union all                      
   select R1.numAccountId AS RecordID,                      
          R1.numParntAcntId AS ParentRecordID,                      
    R1.vcCatgyName AS vcCatgyName,                      
          case when OCTET_LENGTH(R2.TOC) > 0                      
                    then CAST(case when POSITION(''.'' IN R2.TOC) > 0 then R2.TOC else R2.TOC || ''.'' end                      
                                 || cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                       
       else CAST(cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                       
                    end AS TOC,case when OCTET_LENGTH(R2.TOC) > 0                      
                    then  CAST(''..'' || R2.T AS VARCHAR(1000))                      
else CAST('''' AS VARCHAR(1000))                      
                    end AS T                       
      from Chart_Of_Accounts as R1                            
      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numAcntType not in(814,815,822,823,824,825,826) and R1.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || '                       
  )                       
select RecordID as numChartOfAcntID,T+vcCatgyName as vcCategoryName,TOC  from RecursionCTE Where ParentRecordID is not null order by TOC asc';    
   RAISE NOTICE '%',(v_strSQL);  
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


