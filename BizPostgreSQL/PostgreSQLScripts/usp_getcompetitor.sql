-- Stored procedure definition script USP_GetCompetitor for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCompetitor(v_numDomainID NUMERIC(9,0),    
v_numItemCode NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numCompetitorID as VARCHAR(255)),cast(vcPrice as VARCHAR(255)),vcCompanyName || ', ' || vcDivisionName as Competitor,cast(dtDateEntered as VARCHAR(255)),
vcFirstName || ', ' || vcLastname as ConName,
CAST(numPhone AS VARCHAR(15)) || ', ' || CAST(numPhoneExtension AS VARCHAR(15)) as Phone
   from Competitor
   join DivisionMaster div
   on div.numDivisionID = numCompetitorID
   join CompanyInfo com
   on com.numCompanyId = div.numCompanyID
   left join AdditionalContactsInformation ADC
   on ADC.numDivisionId = div.numDivisionID
   where  coalesce(ADC.bitPrimaryContact,false) = true and Competitor.numDomainID = v_numDomainID  and numItemCode = v_numItemCode;
END; $$;












