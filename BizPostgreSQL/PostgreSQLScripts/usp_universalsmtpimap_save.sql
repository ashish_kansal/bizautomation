-- Stored procedure definition script USP_UniversalSMTPIMAP_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UniversalSMTPIMAP_Save(v_numDomainID NUMERIC(18,0),
    v_vcSMTPServer VARCHAR(100),
	v_numSMTPPort NUMERIC(18,0),
	v_bitSMTPSSL BOOLEAN,
	v_bitSMTPAuth BOOLEAN,
	v_vcIMAPServer VARCHAR(100),
	v_numIMAPPort NUMERIC(18,0),
	v_bitIMAPSSL BOOLEAN,
	v_bitIMAPAuth BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM UniversalSMTPIMAP WHERE numDomainID = v_numDomainID) then
	
      UPDATE
      UniversalSMTPIMAP
      SET
      vcSMTPServer = v_vcSMTPServer,numSMTPPort = v_numSMTPPort,bitSMTPSSL = v_bitSMTPSSL,
      bitSMTPAuth = v_bitSMTPAuth,vcIMAPServer = v_vcIMAPServer,numIMAPPort = v_numIMAPPort,
      bitIMAPSSL = v_bitIMAPSSL,bitIMAPAuth = v_bitIMAPAuth
      WHERE
      numDomainID = v_numDomainID;
   ELSE
      INSERT INTO UniversalSMTPIMAP(numDomainID
			,vcSMTPServer
			,numSMTPPort
			,bitSMTPSSL
			,bitSMTPAuth
			,vcIMAPServer
			,numIMAPPort
			,bitIMAPSSL
			,bitIMAPAuth)
		VALUES(v_numDomainID
			,v_vcSMTPServer
			,v_numSMTPPort
			,v_bitSMTPSSL
			,v_bitSMTPAuth
			,v_vcIMAPServer
			,v_numIMAPPort
			,v_bitIMAPSSL
			,v_bitIMAPAuth);
   end if;
   RETURN;
END; $$;


