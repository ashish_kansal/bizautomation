CREATE OR REPLACE FUNCTION USP_OpportunityMaster_MergeVendorPO (
	p_numDomainID NUMERIC(18,0),
	p_numUserCntID NUMERIC(18,0),
	p_vcXML VARCHAR(4000)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE v_VendorID  NUMERIC(18,0)  DEFAULT  0;
		v_MasterPOID  NUMERIC(18,0)  DEFAULT  0;
		v_numOppID   NUMERIC(18,0)  DEFAULT  NULL;                                                                    
		v_numContactId NUMERIC(18,0) DEFAULT null;                                                                        
		v_numDivisionId numeric(9) DEFAULT null;                                                                          
		v_tintSource numeric(9) DEFAULT null;                                                                          
		v_vcPOppName Varchar(100) DEFAULT '';                                                                 
		v_Comments varchar(1000) DEFAULT '';                                                                          
		v_bitPublicFlag BOOLEAN DEFAULT false;                                                                                                                                                     
		v_monPAmount DECIMAL(20,5)  DEFAULT 0;                                                 
		v_numAssignedTo  numeric(9) DEFAULT 0;                                                                                                                                                                                                                                                                                         
		v_strItems TEXT DEFAULT null;                                                                          
		v_strMilestone  TEXT DEFAULT null;                                                                        
		v_dtEstimatedCloseDate TIMESTAMP;                                                                          
		v_CampaignID  numeric(9) DEFAULT null;                                                                          
		v_lngPConclAnalysis  numeric(9) DEFAULT null;                                                                         
		v_tintOppType  SMALLINT;                                                                                                                                             
		v_tintActive  SMALLINT;                                                              
		v_numSalesOrPurType  numeric(9)  ;            
		v_numRecurringId  numeric(9);
		v_numCurrencyID  numeric(9) DEFAULT 0;
		v_DealStatus  SMALLINT  DEFAULT 0;
		v_numStatus  NUMERIC(9);
		v_vcOppRefOrderNo VARCHAR(100);
		v_numBillAddressId numeric(9) DEFAULT 0;
		v_numShipAddressId numeric(9) DEFAULT 0;
		v_bitStockTransfer BOOLEAN DEFAULT false;
		v_WebApiId INT  DEFAULT  0;
		v_tintSourceType SMALLINT DEFAULT 0;
		v_bitDiscountType  BOOLEAN;
		v_fltDiscount   NUMERIC;
		v_bitBillingTerms  BOOLEAN;
		v_intBillingDays  integer;
		v_bitInterestType  BOOLEAN;
		v_fltInterest  NUMERIC;
		v_tintTaxOperator  SMALLINT;
		v_numDiscountAcntType  NUMERIC(9);
		v_vcWebApiOrderNo VARCHAR(100) DEFAULT NULL;
		v_vcCouponCode		VARCHAR(100)  DEFAULT  '';
		v_vcMarketplaceOrderID VARCHAR(100) DEFAULT NULL;
		v_vcMarketplaceOrderDate TIMESTAMP DEFAULT NULL;
		v_vcMarketplaceOrderReportId VARCHAR(100) DEFAULT NULL;
		v_numPercentageComplete NUMERIC(9);
		v_bitUseShippersAccountNo BOOLEAN  DEFAULT  false;
		v_bitUseMarkupShippingRate BOOLEAN  DEFAULT  false;
		v_numMarkupShippingRate DECIMAL(20,5)  DEFAULT  0;
		v_intUsedShippingCompany INT  DEFAULT  0;
		v_numShipmentMethod NUMERIC(18,0) DEFAULT 0;
		v_dtReleaseDate DATE  DEFAULT  NULL;
		v_numPartner NUMERIC(18,0) DEFAULT 0;
		v_tintClickBtn INT DEFAULT 0;
		v_numPartenerContactId NUMERIC(18,0) DEFAULT 0;
		v_numAccountClass NUMERIC(18,0)  DEFAULT  0;
		v_numWillCallWarehouseID NUMERIC(18,0)  DEFAULT  0;
		v_numVendorAddressID NUMERIC(18,0)  DEFAULT  0;
		my_ex_state TEXT;
		my_ex_message TEXT;
		my_ex_detail TEXT;
		my_ex_hint TEXT;
		my_ex_ctx TEXT;
		v_i INT DEFAULT 1;
		v_iCOUNT INT;
		v_numChildItemCode NUMERIC(18,0) DEFAULT 0;
		v_numChildWaerehouseItemID NUMERIC(18,0) DEFAULT 0;
		v_numChildUnitHour DOUBLE PRECISION DEFAULT 0;
		v_monChildPrice DECIMAL(20,5) DEFAULT 0;
		v_K INT DEFAULT 1;
		v_KCOUNT INT;
		v_j INT DEFAULT 1;
		v_jCOUNT INT;
		v_POID INT DEFAULT 0;
BEGIN 
BEGIN
	DROP TABLE IF EXISTS tt_VendorMergeVendorPO CASCADE;
	CREATE TEMPORARY TABLE tt_VendorMergeVendorPO
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
		numVendorID NUMERIC(18,0),
		numMasterPOID NUMERIC(18,0)
	);

	DROP TABLE IF EXISTS tt_VendorPOToMerger CASCADE;
	CREATE TEMPORARY TABLE tt_VendorPOToMerger
	(
		numVendorID NUMERIC(18,0),
		numPOID NUMERIC(18,0)
	);

	DROP TABLE IF EXISTS tt_TEMPMasterItemsMergeVendorPO CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPMasterItemsMergeVendorPO
	(
		numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip Boolean,bitDiscountType Boolean, fltDiscount NUMERIC, monTotAmtBefDiscount DECIMAL(20,5),
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder Boolean,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired Boolean
	);

	DROP TABLE IF EXISTS tt_TEMPChildItemsMergeVendorPO CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPChildItemsMergeVendorPO
	(
		numRowID INT, numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip Boolean,bitDiscountType Boolean, fltDiscount NUMERIC, monTotAmtBefDiscount DECIMAL(20,5),
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder Boolean,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired Boolean
	);

	DROP TABLE IF EXISTS tt_TEMPPOMergeVendorPO CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPPOMergeVendorPO
	(
		ID INT,
		numPOID NUMERIC(18,0)
	);
	--RETRIVE VENDOR AND MASTER PURCHASE ORDER IDS
	INSERT INTO tt_VendorMergeVendorPO
	(
		numVendorID,
		numMasterPOID
	)
	SELECT 
		VendorID,
		MasterPOID
	FROM 
	XMLTABLE
	(
		'Vendors/Vendor'
		PASSING 
			CAST(p_vcXML AS XML)
		COLUMNS
			VendorID NUMERIC(18,0) PATH 'VendorID',
			MasterPOID NUMERIC(18,0) PATH 'MasterPOID'
	) AS X;

	--RETIRIVE ORDERS OF EACH VENDOR
	INSERT INTO tt_VendorPOToMerger
	(
		numVendorID,
		numPOID
	)
	SELECT 
		VendorID,
		OrderID
	FROM 
	XMLTABLE
	(
		'Vendors/Vendor/Orders/OrderID'
		PASSING 
			CAST(p_vcXML AS XML)
		COLUMNS
			VendorID NUMERIC(18,0) PATH '../../VendorID',
			OrderID NUMERIC(18,0) PATH '.'
	) AS X;

	
	SELECT COUNT(*) INTO v_iCOUNT FROM tt_VendorMergeVendorPO;

	--LOOP ALL VENDORS
	WHILE v_i <= v_iCOUNT LOOP
		--GET VENDOR AND MASTER PO ID
		SELECT numVendorID,numMasterPOID INTO v_VendorID,v_MasterPOID  FROM tt_VendorMergeVendorPO WHERE ID = v_i;

		--GET MASTER PO FIELDS 
		SELECT
			numOppId,numContactId,numDivisionId,tintSource,vcPOppName,txtComments,
			bitPublicFlag,monPAmount,numAssignedTo,null,intPEstimatedCloseDate,
			numCampainID,lngPConclAnalysis,tintOppType,tintActive,numSalesOrPurType,
			null,numCurrencyID,tintOppStatus,numStatus,vcOppRefOrderNo,0,
			0,false,0,tintSourceType,bitDiscountType,fltDiscount,
			bitBillingTerms,intBillingDays,bitInterestType,fltInterest,tintTaxOperator,
			numDiscountAcntType,vcWebApiOrderNo,vcCouponCode,vcMarketplaceOrderID,
			bintCreatedDate,vcMarketplaceOrderReportId,numPercentageComplete,
			bitUseShippersAccountNo,bitUseMarkupShippingRate,numMarkupShippingRate,
			intUsedShippingCompany,numShipmentMethod,dtReleaseDate,numPartner,numPartenerContact
			,numAccountClass,numVendorAddressID
		INTO
			v_numOppID,v_numContactId,v_numDivisionId,v_tintSource,v_vcPOppName,v_Comments,
			v_bitPublicFlag,v_monPAmount,v_numAssignedTo,v_strMilestone,v_dtEstimatedCloseDate,
			v_CampaignID,v_lngPConclAnalysis,v_tintOppType,v_tintActive,v_numSalesOrPurType,
			v_numRecurringId,v_numCurrencyID,v_DealStatus,v_numStatus,v_vcOppRefOrderNo,v_numBillAddressId,
			v_numShipAddressId,v_bitStockTransfer,v_WebApiId,v_tintSourceType,v_bitDiscountType,v_fltDiscount,
			v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_tintTaxOperator,
			v_numDiscountAcntType,v_vcWebApiOrderNo,v_vcCouponCode,v_vcMarketplaceOrderID,
			v_vcMarketplaceOrderDate,v_vcMarketplaceOrderReportId,v_numPercentageComplete,
			v_bitUseShippersAccountNo,v_bitUseMarkupShippingRate,v_numMarkupShippingRate,
			v_intUsedShippingCompany,v_numShipmentMethod,v_dtReleaseDate,v_numPartner,v_numPartenerContactId
			,v_numAccountClass,v_numVendorAddressID
		FROM
			OpportunityMaster
		WHERE
			numDomainId = p_numDomainID
			AND numOppId = v_MasterPOID;

		--CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM tt_TEMPMasterItemsMergeVendorPO;
		DELETE FROM tt_TEMPChildItemsMergeVendorPO;

		
		--GET ITEMS OF MASTER PO
		INSERT INTO 
			tt_TEMPMasterItemsMergeVendorPO
		SELECT 
			numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId = v_MasterPOID;

		--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
		INSERT INTO 
			tt_TEMPChildItemsMergeVendorPO
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numoppitemtCode),numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			1,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId IN (SELECT numPOID FROM tt_VendorPOToMerger WHERE numVendorID=v_VendorID AND numPOID <> v_MasterPOID);

		
		SELECT COUNT(*) INTO v_KCOUNT FROM tt_TEMPChildItemsMergeVendorPO;

		-- LOOP ITEMS OF EACH PO NEEDS TO MERGED
		WHILE v_K <= v_KCOUNT LOOP
			SELECT 
				numItemCode,
				COALESCE(numWarehouseItmsID,0),
				COALESCE(numUnitHour,0), 
				COALESCE(monPrice,0) 
			INTO
				v_numChildItemCode
				,v_numChildWaerehouseItemID
				,v_numChildUnitHour
				,v_monChildPrice
			FROM 
				tt_TEMPChildItemsMergeVendorPO 
			WHERE 
				numRowID = v_K;

			-- IF ITEM WITH SAME ITEM ID AND WAREHOUSE IS EXIST IN MASTER ORDER THEN UPDATE QTY IN MASTER PO AND SET PRICE TO WHICHEVER LOWER
			-- ELSE ADD ITEMS MASTER PO ITEMS
			IF EXISTS(SELECT numoppitemtCode FROM tt_TEMPMasterItemsMergeVendorPO WHERE numItemCode=v_numChildItemCode AND COALESCE(numWarehouseItmsID,0)=v_numChildWaerehouseItemID) THEN
				UPDATE 
					tt_TEMPMasterItemsMergeVendorPO
				SET 
					numUnitHour = COALESCE(numUnitHour,0) + v_numChildUnitHour,
					monPrice = (CASE WHEN COALESCE(monPrice,0) > v_monChildPrice THEN v_monChildPrice ELSE monPrice END),
					monTotAmount = (COALESCE(numUnitHour,0) + v_numChildUnitHour) * (CASE WHEN COALESCE(monPrice,0) > v_monChildPrice THEN v_monChildPrice ELSE monPrice END),
					monTotAmtBefDiscount = (COALESCE(numUnitHour,0) + v_numChildUnitHour) * (CASE WHEN COALESCE(monPrice,0) > v_monChildPrice THEN v_monChildPrice ELSE monPrice END)
				WHERE
					numItemCode=v_numChildItemCode 
					AND COALESCE(numWarehouseItmsID,0)=v_numChildWaerehouseItemID;
			ELSE
				INSERT INTO 
					tt_TEMPMasterItemsMergeVendorPO
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,ItemType,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
				FROM 
					tt_TEMPChildItemsMergeVendorPO 
				WHERE 
					numRowID = v_K;

			END IF;

			v_K := v_K + 1;
		END LOOP;
		
		-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
		v_strItems := CONCAT('<NewDataSet>',COALESCE((SELECT string_agg(CONCAT('<Item>'
															,'<numoppitemtCode>',numoppitemtCode,'</numoppitemtCode>'
														   	,'<numItemCode>',numItemCode,'</numItemCode>'
														   	,'<numUnitHour>',COALESCE(numUnitHour,0),'</numUnitHour>'
														   	,'<monPrice>',COALESCE(monPrice,0),'</monPrice>'
													   		,'<monTotAmount>',COALESCE(monTotAmount,0),'</monTotAmount>'
														   	,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
													   	   	,'<numWarehouseItmsID>',COALESCE(numWarehouseItmsID,0),'</numWarehouseItmsID>'
														   	,'<numToWarehouseItemID>',COALESCE(numToWarehouseItemID,0),'</numToWarehouseItemID>'
														   	,'<Op_Flag>',Op_Flag,'</Op_Flag>'
														   	,'<ItemType>',ItemType,'</ItemType>'
															,'<DropShip>',(CASE WHEN DropShip THEN 1 ELSE 0 END),'</DropShip>'
														   	,'<bitDiscountType>',(CASE WHEN bitDiscountType THEN 1 ELSE 0 END),'</bitDiscountType>'
														   	,'<fltDiscount>',COALESCE(fltDiscount,0),'</fltDiscount>'
														   	,'<monTotAmtBefDiscount>',COALESCE(monTotAmtBefDiscount,0),'</monTotAmtBefDiscount>'
														   	,'<vcItemName>',vcItemName,'</vcItemName>'
														   	,'<numUOM>',COALESCE(numUOM,0),'</numUOM>'
															,'<bitWorkOrder>',(CASE WHEN bitWorkOrder THEN 1 ELSE 0 END),'</bitWorkOrder>'
														   	,'<numVendorWareHouse>',COALESCE(numVendorWareHouse,0),'</numVendorWareHouse>'
														   	,'<numShipmentMethod>',COALESCE(numShipmentMethod,0),'</numShipmentMethod>'
														   	,'<numSOVendorId>',COALESCE(numSOVendorId,0),'</numSOVendorId>'
														   	,'<numProjectID>',COALESCE(numProjectID,0),'</numProjectID>'
														   	,'<numProjectStageID>',COALESCE(numProjectStageID,0),'</numProjectStageID>'
														   	,'<Attributes>',Attributes,'</Attributes>'
														   	,'<bitItemPriceApprovalRequired>',(CASE WHEN bitItemPriceApprovalRequired THEN 1 ELSE 0 END),'</bitItemPriceApprovalRequired>'
															 ,'</Item>'),'')
															FROM 
																tt_TEMPMasterItemsMergeVendorPO),''),'</NewDataSet>');
		
			RAISE NOTICE '%',v_strItems;

		--UPDATE MASTER PO AND ADD MERGE PO ITEMS
		PERFORM USP_OppManage(v_numOppID := v_numOppID,v_numContactId := v_numContactId,v_numDivisionId := v_numDivisionId,v_tintSource := v_tintSource,v_vcPOppName := v_vcPOppName,
							v_Comments := v_Comments,v_bitPublicFlag := v_bitPublicFlag,v_numUserCntID := p_numUserCntID,v_monPAmount := v_monPAmount,v_numAssignedTo := v_numAssignedTo,
							v_numDomainID := p_numDomainID,v_strItems := v_strItems,v_strMilestone := v_strMilestone,v_dtEstimatedCloseDate := v_dtEstimatedCloseDate,v_CampaignID := v_CampaignID,
							v_lngPConclAnalysis := v_lngPConclAnalysis,v_tintOppType := v_tintOppType,v_tintActive := v_tintActive,v_numSalesOrPurType := v_numSalesOrPurType,
							v_numRecurringId := v_numRecurringId,v_numCurrencyID := v_numCurrencyID,v_DealStatus := v_DealStatus,v_numStatus := v_numStatus,v_vcOppRefOrderNo := v_vcOppRefOrderNo,
							v_numBillAddressId := v_numBillAddressId,v_numShipAddressId := v_numShipAddressId,v_bitStockTransfer := v_bitStockTransfer,v_WebApiId := v_WebApiId,
							v_tintSourceType := v_tintSourceType,v_bitDiscountType := v_bitDiscountType,v_fltDiscount := v_fltDiscount,v_bitBillingTerms := v_bitBillingTerms,
							v_intBillingDays := v_intBillingDays,v_bitInterestType := v_bitInterestType,v_fltInterest := v_fltInterest,v_tintTaxOperator := v_tintTaxOperator,
							v_numDiscountAcntType := v_numDiscountAcntType,v_vcWebApiOrderNo := v_vcWebApiOrderNo,v_vcCouponCode := v_vcCouponCode,v_vcMarketplaceOrderID := v_vcMarketplaceOrderID,
							v_vcMarketplaceOrderDate := v_vcMarketplaceOrderDate,v_vcMarketplaceOrderReportId := v_vcMarketplaceOrderReportId,v_numPercentageComplete := v_numPercentageComplete,
							v_bitUseShippersAccountNo := v_bitUseShippersAccountNo,v_bitUseMarkupShippingRate := v_bitUseMarkupShippingRate,
							v_numMarkupShippingRate := v_numMarkupShippingRate,v_intUsedShippingCompany := v_intUsedShippingCompany,v_numShipmentMethod := v_numShipmentMethod,v_dtReleaseDate := v_dtReleaseDate,v_numPartner := v_numPartner,v_tintClickBtn := 0,v_numPartenerContactId := v_numPartenerContactId
							,v_numAccountClass := v_numAccountClass,v_numWillCallWarehouseID := 0, v_numVendorAddressID := v_numVendorAddressID, v_numShipFromWarehouse := 0, v_numShippingService := 0,
							v_ClientTimeZoneOffset := 0,v_vcCustomerPO := '',v_bitDropShipAddress := false,v_PromCouponCode := NULL,v_numPromotionId := 0,v_dtExpectedDate := null,v_numProjectID := 0);
		
		-- DELETE MERGED PO

		---- CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM tt_TEMPPOMergeVendorPO;

		---- GET ALL POS EXCEPT MASTER PO
		INSERT INTO tt_TEMPPOMergeVendorPO SELECT Row_Number() OVER(ORDER BY numPOID), numPOID FROM tt_VendorPOToMerger WHERE numVendorID=v_VendorID AND numPOID <> v_MasterPOID;

		
		SELECT COUNT(*) INTO v_jCOUNT FROM tt_TEMPPOMergeVendorPO;

		WHILE v_j <= v_jCOUNT LOOP	
			SELECT numPOID INTO v_POID FROM tt_TEMPPOMergeVendorPO WHERE ID = v_j;
			
			PERFORM USP_DeleteOppurtunity(v_numOppId := v_POID,v_numDomainID := p_numDomainID,v_numUserCntID := p_numUserCntID);

			v_j := v_j + 1;
		END LOOP;

		v_i := v_i + 1;
	END LOOP;
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
 END;
END; $$;



