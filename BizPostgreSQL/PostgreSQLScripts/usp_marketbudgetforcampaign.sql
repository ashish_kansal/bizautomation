-- Stored procedure definition script USP_MarketBudgetForCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MarketBudgetForCampaign(v_numCampaignId NUMERIC(9,0) DEFAULT 0,  
 v_numDomainId NUMERIC(9,0) DEFAULT 0,    
 v_numListItemID NUMERIC(9,0) DEFAULT 0,    
 INOUT v_bitMarketCampaign BOOLEAN  DEFAULT NULL,      
 INOUT v_monmonthlyAmt DOUBLE PRECISION DEFAULT 0 ,      
 INOUT v_monTotalYearlyAmt DOUBLE PRECISION DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMarketBudgetId  NUMERIC(9,0);      
   v_OppMonthAmount  DECIMAL(20,5);      
   v_CampaignCost  DECIMAL(20,5);      
   v_BudgetAmt  DECIMAL(20,5);      
   v_TotalBudgetAmt  DECIMAL(20,5);      
     
 --Select @numMarketBudgetId=numMarketBudgetId,@bitMarketCampaign=bitMarketCampaign From MarketBudgetMaster Where numDomainId=@numDomainId  --   intFiscalYear=year(getutcdate()) And 
BEGIN
   select   bitMarketCampaign INTO v_bitMarketCampaign From MarketBudgetMaster Where numDomainID = v_numDomainId; 
   If  v_bitMarketCampaign = true then      --@numMarketBudgetId is not null And
 
      select   monCampaignCost INTO v_CampaignCost From CampaignMaster Where numCampaignID = v_numCampaignId;
      Select coalesce(monAmount,0) INTO v_BudgetAmt From MarketBudgetMaster MBM
      inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId
      Where MBD.intYear = EXTRACT(year FROM TIMEZONE('UTC',now())) And MBD.tintMonth = EXTRACT(month FROM TIMEZONE('UTC',now()))  And MBD.numListItemID = v_numListItemID And MBM.numDomainID = v_numDomainId;
      Select sum(monAmount) INTO v_TotalBudgetAmt From MarketBudgetMaster MBM
      inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId
      Where MBD.intYear = EXTRACT(year FROM TIMEZONE('UTC',now()))  And MBD.numListItemID = v_numListItemID And MBM.numDomainID = v_numDomainId; 


----  Select @BudgetAmt=(case when  month(getutcdate())=1 then monJan         
----  when month(getutcdate())=2 then monFeb      
----  when month(getutcdate())=3 then monMar      
----  when month(getutcdate())=4 then monApr      
----  when month(getutcdate())=5 then monMay      
----  when month(getutcdate())=6 then monJune      
----  when month(getutcdate())=7 then monJuly      
----  when month(getutcdate())=8 then monAug      
----  when month(getutcdate())=9 then monSep      
----  when month(getutcdate())=10 then monOct      
----  when month(getutcdate())=11 then monNov      
----  when month(getutcdate())=12 then monDec      
----  End) ,@TotalBudgetAmt=monTotal From MarketbudgetMaster MBM      
----  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId      
     
        
      v_monmonthlyAmt := coalesce(v_BudgetAmt,0) -coalesce(v_CampaignCost,0);
    
--  print  @TotalBudgetAmt      
      v_monTotalYearlyAmt := coalesce(v_TotalBudgetAmt,0) -coalesce(v_CampaignCost,0);

      v_bitMarketCampaign := v_bitMarketCampaign;
   end if;
   RETURN;
END; $$;


