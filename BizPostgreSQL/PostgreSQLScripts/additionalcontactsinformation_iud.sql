CREATE OR REPLACE FUNCTION AdditionalContactsInformation_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
   v_action  CHAR(1) DEFAULT 'I';
   v_TempnumContactId  NUMERIC;
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID)  VALUES ('Contact', OLD.numcontactid, 'Delete', OLD.numdomainid);
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numContactId;
		v_numUserCntID := OLD.numModifiedBy;
		v_numDomainID := OLD.numDomainID;

		DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = OLD.numcontactid;
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID)  VALUES ('Contact', NEW.numcontactid, 'Update', NEW.numdomainid);
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numContactId;
		v_numUserCntID := NEW.numModifiedBy;
		v_numDomainID := NEW.numDomainID;

		IF OLD.vcAsstEmail <> NEW.vcAsstEmail THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcAsstEmail,');
		END IF;
		IF OLD.vcAsstFirstName <> NEW.vcAsstFirstName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcAsstFirstName,');
		END IF;
		IF OLD.vcAsstLastName <> NEW.vcAsstLastName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcAsstLastName,');
		END IF;
		IF OLD.numAsstPhone <> NEW.numAsstPhone THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAsstPhone,');
		END IF;
		IF OLD.vcAltEmail <> NEW.vcAltEmail THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcAltEmail,');
		END IF;
		IF OLD.vcCategory <> NEW.vcCategory THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcCategory,');
		END IF;
		IF OLD.numCell <> NEW.numCell THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCell,');
		END IF;
		IF OLD.vcEmail <> NEW.vcEmail THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcEmail,');
		END IF;
		IF OLD.numPhone <> NEW.numPhone THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numPhone,');
		END IF;
		IF OLD.vcPosition <> NEW.vcPosition THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcPosition,');
		END IF;
		IF OLD.bitPrimaryContact <> NEW.bitPrimaryContact THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitPrimaryContact,');
		END IF;
		IF OLD.bitOptOut <> NEW.bitOptOut THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitOptOut,');
		END IF;
		IF OLD.numManagerID <> NEW.numManagerID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numManagerID,');
		END IF;
		IF OLD.vcFirstName <> NEW.vcFirstName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcFirstName,');
		END IF;
		IF OLD.charSex <> NEW.charSex THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'charSex,');
		END IF;
		IF OLD.vcLastName <> NEW.vcLastName THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcLastName,');
		END IF;
		IF OLD.bintDOB <> NEW.bintDOB THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintDOB,');
		END IF;
		IF OLD.numContactType <> NEW.numContactType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numContactType,');
		END IF;
		IF OLD.numTeam <> NEW.numTeam THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numTeam,');
		END IF;
		IF OLD.txtNotes <> NEW.txtNotes THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'txtNotes,');
		END IF;
		IF OLD.bintCreatedDate <> NEW.bintCreatedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintCreatedDate,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');

		SELECT ACI.numContactId INTO v_TempnumContactId FROM AdditionalContactsInformation ACI WHERE  (
				(ACI.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
      OR
				(ACI.bintDOB >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintDOB <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
      AND ACI.numContactId = NEW.numContactId;
      IF(v_TempnumContactId IS NOT NULL) then
		
         UPDATE AdditionalContactsInformation_TempDateFields AS ACIT
         SET
         bintCreatedDate = ACI.bintCreatedDate,bintDOB = ACI.bintDOB
         FROM
         AdditionalContactsInformation AS ACI
         WHERE  ACIT.numContactId = ACI.numContactId AND(((ACI.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR(ACI.bintDOB >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintDOB <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND
         ACIT.numContactId = NEW.numContactId);
      ELSE
         DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = NEW.numContactId;
      end if;
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID)  VALUES ('Contact', NEW.numcontactid, 'Insert', NEW.numdomainid);
		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.numContactId;
		v_numUserCntID := New.numCreatedBy;
		v_numDomainID := NEW.numDomainID;

		IF EXISTS(SELECT numContactId FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = NEW.numContactId) then
		
         SELECT ACI.numContactId INTO v_TempnumContactId FROM AdditionalContactsInformation ACI WHERE  (
				(ACI.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
				(ACI.bintDOB >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintDOB <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND ACI.numContactId = NEW.numContactId;
         IF(v_TempnumContactId IS NOT NULL) then
			
            UPDATE AdditionalContactsInformation_TempDateFields AS ACIT
            SET
            bintCreatedDate = ACI.bintCreatedDate,bintDOB = ACI.bintDOB
            FROM
            AdditionalContactsInformation AS ACI
            WHERE  ACIT.numContactId = ACI.numContactId AND(((ACI.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day')
            OR(ACI.bintDOB >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND ACI.bintDOB <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
            AND
            ACIT.numContactId = NEW.numContactId);
         ELSE
            DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = NEW.numContactId;
         end if;
      ELSE
         INSERT INTO AdditionalContactsInformation_TempDateFields(numContactId, numDomainID, bintCreatedDate, bintDOB)
         SELECT numContactId,numDomainID, bintCreatedDate,bintDOB
         FROM AdditionalContactsInformation
         WHERE (
				(CAST(bintCreatedDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintCreatedDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day')
         OR
				(CAST(bintDOB AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintDOB AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND numContactId = v_numRecordID;
      end if;
	END IF;
       
	    PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 69,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

    RETURN NULL;
END; $$;
CREATE TRIGGER AdditionalContactsInformation_IUD AFTER INSERT OR UPDATE OR DELETE ON AdditionalContactsInformation FOR EACH ROW EXECUTE PROCEDURE AdditionalContactsInformation_IUD_TrFunc();


