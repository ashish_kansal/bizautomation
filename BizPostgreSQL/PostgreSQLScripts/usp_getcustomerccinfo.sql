-- Stored procedure definition script USP_GetCustomerCCInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCustomerCCInfo(v_numCCInfoId BIGINT DEFAULT 0,
v_numContactId BIGINT DEFAULT 0,
v_bitdefault INTEGER DEFAULT 0,
v_numDivisionID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numCCInfoId = 0 then
	
      IF v_bitdefault = 0 then
		
         IF v_numContactId = 0 AND v_numDivisionID > 0 then
			
            select   numContactId INTO v_numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID   ORDER BY bitPrimaryContact DESC LIMIT 1;
         end if;
         open SWV_RefCur for
         SELECT
         numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
         FROM
         CustomerCreditCardInfo
         LEFT JOIN
         Listdetails
         ON
         Listdetails.numListItemID = CustomerCreditCardInfo.numCardTypeID
         WHERE
         numContactId = v_numContactId
         ORDER BY
         bitIsDefault DESC,numCCInfoID ASC;
      ELSE
         open SWV_RefCur for
         select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
         from CustomerCreditCardInfo left join Listdetails on Listdetails.numListItemID = CustomerCreditCardInfo.numCardTypeID
         where numContactId = v_numContactId ORDER BY
         bitIsDefault DESC,numCCInfoID ASC;
      end if;
   else
      open SWV_RefCur for
      select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault , vcData
      from CustomerCreditCardInfo left join Listdetails on Listdetails.numListItemID = CustomerCreditCardInfo.numCardTypeID
      where numCCInfoID = v_numCCInfoId
      order by numCCInfoID desc;
   end if;
   RETURN;
END; $$;



