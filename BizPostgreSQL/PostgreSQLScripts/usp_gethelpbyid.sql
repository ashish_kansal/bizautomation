-- Stored procedure definition script Usp_GetHelpByID for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetHelpByID(v_numHelpId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM    HelpMaster
   WHERE   numHelpID = v_numHelpId;
END; $$; 












