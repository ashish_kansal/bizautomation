-- Stored procedure definition script USP_GetChildCategory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChildCategory(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,    
  v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From Chart_Of_Accounts COA
   inner join Listdetails LD on COA.numAcntTypeId = LD.numListItemID  Where COA.numParntAcntTypeID = v_numParntAcntId And COA.numDomainId = v_numDomainId;
END; $$;












