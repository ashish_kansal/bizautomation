-- Stored procedure definition script USP_ReptGrossProfit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReptGrossProfit(v_fromDate TIMESTAMP,    
v_toDate TIMESTAMP,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,    
v_numDepID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select numOppId,vcpOppName,bintAccountClosingDate,GetDealAmount(numOppId,TIMEZONE('UTC',now()),0::NUMERIC) as totAmount,numPClosingPercent as AmtPaid
   from OpportunityMaster where tintopptype = 1 and tintoppstatus = 1
   and bintAccountClosingDate between v_fromDate and v_toDate and numDomainId = v_numDomainId;    
    
   open SWV_RefCur2 for
   select  numOppId,vcpOppName,bintAccountClosingDate,GetDealAmount(numOppId,TIMEZONE('UTC',now()),0::NUMERIC) as totAmount,numPClosingPercent as AmtPaid
   from OpportunityMaster where tintopptype = 2 and tintoppstatus = 1
   and bintAccountClosingDate between v_fromDate and v_toDate and numDomainId = v_numDomainId;    
    
    
    
   if v_numDepID = 0 then

      open SWV_RefCur3 for
      select X.*,coalesce(X.OvertimeHrs*X.monOverTimeRate,0) as OvertimeAmt from(select numUserID,vcFirstName || vcLastname as Name,coalesce(U.monHourlyRate,0) as monHourlyRate,case when bitSalary = true then 'Yes' when bitSalary = false then 'No' end as Salary,
 case when bitHourlyRate = true then 'Yes' when bitHourlyRate = false then 'No' end as Hourly,
 coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0) as BillTime,
  coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0) as NonBillTime,
coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)*monAmount/60)
            from timeandexpense where numCategory = 1 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as BillTimeAmt,
  coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0)*coalesce(U.monHourlyRate,0) as NonBillTimeAmt,
  coalesce((select sum(monAmount)
            from timeandexpense where numCategory = 2 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as BillExp,
  coalesce((select sum(monAmount)
            from timeandexpense where numCategory = 2 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as NonBillExp,
 coalesce((select sum(monAmount) from TimeExpAddAmount where dtAmtAdded between v_fromDate and v_toDate and numUserID = U.numUserId),0) as AddAmount,
  GetOverTimeDtls(U.numUserId,v_fromDate,v_toDate) as OvertimeHrs,monOverTimeRate ,GetCommisionDtls(U.numUserId,v_fromDate,v_toDate) as CommAmt
         from UserMaster U
         join AdditionalContactsInformation A
         on  numContactId = numUserDetailId and A.numDomainID = v_numDomainId) X;
   ELSEIF v_numDepID > 0
   then

      open SWV_RefCur3 for
      select X.*,coalesce(X.OvertimeHrs*X.monOverTimeRate,0) as OvertimeAmt from(select numUserID,vcFirstName || vcLastname as Name,coalesce(U.monHourlyRate,0) as monHourlyRate,case when bitSalary = true then 'Yes' when bitSalary = false then 'No' end as Salary,
 case when bitHourlyRate = true then 'Yes' when bitHourlyRate = false then 'No' end as Hourly,
 coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0) as BillTime,
  coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0) as NonBillTime,
coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)*monAmount/60)
            from timeandexpense where numCategory = 1 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as BillTimeAmt,
  coalesce((select sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)
            from timeandexpense where numCategory = 1 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),
         0)*coalesce(U.monHourlyRate,0) as NonBillTimeAmt,
  coalesce((select sum(monAmount)
            from timeandexpense where numCategory = 2 and numtype = 1 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as BillExp,
  coalesce((select sum(monAmount)
            from timeandexpense where numCategory = 2 and numtype = 2 and numUserID = U.numUserId and
            dtFromDate between v_fromDate and v_toDate and  dtToDate between v_fromDate and v_toDate),0) as NonBillExp,
 coalesce((select sum(monAmount) from TimeExpAddAmount where dtAmtAdded between v_fromDate and v_toDate and numUserID = U.numUserId),0) as AddAmount,
  GetOverTimeDtls(U.numUserId,v_fromDate,v_toDate) as OvertimeHrs,monOverTimeRate ,GetCommisionDtls(U.numUserId,v_fromDate,v_toDate) as CommAmt
         from UserMaster U
         join AdditionalContactsInformation A
         on  numContactId = numUserDetailId and A.numDomainID = v_numDomainId and vcDepartment = v_numDepID) X;
   end if;
   RETURN;
END; $$;


