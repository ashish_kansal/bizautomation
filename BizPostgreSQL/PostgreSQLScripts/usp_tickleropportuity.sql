DROP FUNCTION IF EXISTS usp_TicklerOpportuity;

CREATE OR REPLACE FUNCTION usp_TicklerOpportuity(v_numUserCntID NUMERIC(9,0) DEFAULT null,                                      
v_numDomainID NUMERIC(9,0) DEFAULT null,                                      
v_startDate TIMESTAMP DEFAULT NULL,                                      
v_endDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSelectedEmployess  VARCHAR(500);
   v_vcAssignedStages  VARCHAR(500);
   v_vcActionTypes  VARCHAR(100);
   v_strSql  VARCHAR(8000) DEFAULT '';
BEGIN
   select   vcSelectedEmployeeOpp, vcAssignedStages INTO v_vcSelectedEmployess,v_vcAssignedStages FROM
   TicklerListFilterConfiguration WHERE
   numDomainID = v_numDomainID AND
   numUserCntID = v_numUserCntID;


   DROP TABLE IF EXISTS tt_TEMPTicklerOpportuity CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTicklerOpportuity AS
      SELECT
      Split.a AS Data
	
      FROM(SELECT CAST('<M>' || REPLACE(coalesce(v_vcAssignedStages,''),',','</M><M>') || '</M>' AS XML) AS Data) AS A
      CROSS JOIN LATERAL(select * from XMLTABLE('/M'
      PASSING Data
      COLUMNS a VARCHAR(100) PATH '.') AS Split) AS TabAl; 

   IF coalesce(LENGTH(v_vcAssignedStages),0) = 0 OR(SELECT COUNT(*) FROM tt_TEMPTicklerOpportuity WHERE data = 1) > 0 then --Sales Opportunity

      v_strSql := 'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Opportunity'' AS Type,
						Div.numTerID                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppId = SPD.numOppID 
					JOIN 
						DivisionMaster Div 
					ON 
						Div.numDivisionID = opp.numDivisionId  
					WHERE 
						opp.tintOppType = 1 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
      || ' AND (SPD.numAssignTo IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or SPD.numCreatedBy IN(' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))'
      || ' AND (SPD.dtEndDate  >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' AND SPD.dtEndDate  <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''')';
   end if;

   IF coalesce(LENGTH(v_vcAssignedStages),0) = 0 OR(SELECT COUNT(*) FROM tt_TEMPTicklerOpportuity WHERE data = 2) > 0 then --Purchase Opportunity

      v_strSql :=  coalesce(v_strSql,'') || CASE WHEN LENGTH(v_strSql) > 0 THEN ' UNION ' ELSE '' END ||
      'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
      || ' AND (SPD.numAssignTo IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or SPD.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))'
      || ' AND (SPD.dtEndDate  >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' AND SPD.dtEndDate  <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''')';
   end if;
	
   IF coalesce(LENGTH(v_vcAssignedStages),0) = 0 OR(SELECT COUNT(*) FROM tt_TEMPTicklerOpportuity WHERE data = 3) > 0 then --Sales Order

      v_strSql := coalesce(v_strSql,'') || CASE WHEN LENGTH(v_strSql) > 0 THEN ' UNION ' ELSE '' END ||
      'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
      || ' AND (SPD.numAssignTo IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or SPD.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))'
      || ' AND (SPD.dtEndDate  >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' AND SPD.dtEndDate  <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''')';
   end if;

   IF coalesce(LENGTH(v_vcAssignedStages),0) = 0 OR(SELECT COUNT(*) FROM tt_TEMPTicklerOpportuity WHERE data = 4) > 0 then --Purchase Order

      v_strSql := coalesce(v_strSql,'') || CASE WHEN LENGTH(v_strSql) > 0 THEN ' UNION ' ELSE '' END ||
      'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
      || ' AND (SPD.numAssignTo IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or SPD.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))'
      || ' AND (SPD.dtEndDate  >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' AND SPD.dtEndDate  <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''')';
   end if;

   IF coalesce(LENGTH(v_vcAssignedStages),0) = 0 OR(SELECT COUNT(*) FROM tt_TEMPTicklerOpportuity WHERE data = 5) > 0 then --Project

      v_strSql := coalesce(v_strSql,'') || CASE WHEN LENGTH(v_strSql) > 0 THEN ' UNION ' ELSE '' END ||
      'SELECT 
								SPD.numStageDetailsId,
								SPD.vcStageName,
								SPD.tinProgressPercentage as Tprogress,
								SPD.dtEndDate as CloseDate,
								Pro.numProId as ID,
								vcProjectName as vcPOppName,
								''Project'' as Type,Div.numTerId  
							FROM 
								ProjectsMaster Pro 
							JOIN 
								StagePercentageDetails SPD 
							ON 
								Pro.numProId=SPD.numProjectID
							JOIN 
								DivisionMaster div 
							ON 
								div.numDivisionID=Pro.numDivisionID   
							WHERE 
								Pro.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
      || ' AND (SPD.numAssignTo IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ')  or SPD.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE SUBSTR(Cast(v_numUserCntID as VARCHAR(10)),1,10) END || ' ))'
      || ' AND (SPD.dtEndDate  >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and SPD.dtEndDate  <= ''' || SUBSTR(CAST(v_endDate AS VARCHAR(30)),1,30) || ''') ORDER BY dtEndDate Desc';
   end if;


   RAISE NOTICE '%',v_strSql;
   OPEN SWV_RefCur FOR EXECUTE v_strSql;   

   DROP TABLE IF EXISTS tt_TEMPTicklerOpportuity CASCADE;
   RETURN;
END; $$;                           


