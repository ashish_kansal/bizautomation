-- Stored procedure definition script USP_EmpWeeklyAvailability for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmpWeeklyAvailability(v_numUserCntId NUMERIC(9,0),                  
v_dtDate TIMESTAMP,               
v_ClientTimeZoneOffset INTEGER,      
v_TeamType INTEGER,      
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_Duration BIGINT DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql                              
--Create a Temporary table to hold data                                        
   AS $$
   DECLARE
   v_dtWeekStartDate  TIMESTAMP;
   v_dtWeekEndDate  TIMESTAMP;    
   v_dtDurationEnd  TIMESTAMP;
BEGIN
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   (
      numcontactid NUMERIC(9,0),
      Name VARCHAR(100)
   );     
   
	v_dtWeekStartDate := '1900-01-01'::TIMESTAMP + make_interval(days => DATEDIFF('week','1900-01-01','2021-07-03') * 7);
	v_dtWeekEndDate := '1900-01-01 23:59:59'::TIMESTAMP  + make_interval(days => 6) + make_interval(days => DATEDIFF('week','1900-01-01','2021-07-03') * 7);

   RAISE NOTICE '%',v_dtWeekStartDate;
   RAISE NOTICE '%',v_dtWeekEndDate;

   IF coalesce(v_Duration,0) = 0 then
      v_dtDurationEnd := v_dtDate;
   ELSE
      v_dtDurationEnd := v_dtDate+CAST(v_Duration || 'minute' as interval);
   end if;		

   insert into    tt_TEMPTABLE
   SELECT numContactId,Name FROM(select distinct ACI.numContactId,ACI.vcFirstName || ' ' || ACI.vcLastname as Name
      from  AdditionalContactsInformation ACI
      join UserMaster UM on ACI.numContactId = UM.numUserDetailId
      JOIN UserTeams UT ON UT.numUserCntID = UM.numUserDetailId
      where bitActivateFlag = true and ACI.numDomainID = v_numDomainID AND UM.numDomainID = v_numDomainID  AND UT.numDomainID = v_numDomainID
      and UT.numTeam in(SELECT numTeam FROM ForReportsByTeam WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntId AND tintType = v_TeamType)) TABLE1
   WHERE (numContactId NOT IN(SELECT numAssign FROM Communication
      WHERE numDomainID = v_numDomainID
      AND (dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '1 second' BETWEEN v_dtDate AND v_dtDurationEnd
      OR dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 second' BETWEEN v_dtDate AND v_dtDurationEnd)
      UNION
      SELECT numUserCntId from Activity A join ActivityResource AR on AR.ActivityID = A.ActivityID
      join Resource R on R.ResourceID = AR.ResourceID
      where (StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '1 second' > v_dtDate
      and  StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '1 second' < v_dtDurationEnd)
      or (StartDateTimeUtc+CAST(Duration || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 second' > v_dtDate
      AND StartDateTimeUtc+CAST(Duration || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 second' < v_dtDurationEnd)) OR coalesce(v_Duration,0) = 0);
  
   open SWV_RefCur for
   select * from tt_TEMPTABLE;  
  
   v_dtDate := CAST(TO_CHAR(v_dtDate,'yyyy/mm/dd') || ' 00:00:00' AS TIMESTAMP); --will return date like 'XXXX-XX-XX 00:00:00'
  
   open SWV_RefCur2 for
   SELECT numCommId,CAST(0 AS INTEGER) AS ActivityID,dtStartTime,dtEndTime,numContactId,vcdata,SUBSTR(CAST(dtStartTime AS VARCHAR(30)),length(CAST(dtStartTime AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime AS VARCHAR(30)),length(CAST(dtEndTime AS VARCHAR(30))) -8+1) as Schedule,bitTask,ActivityDescription from(select numCommId,dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtStartTime,
CASE WHEN bitTask = 974 THEN dtStartTime ELSE dtEndTime END+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtEndTime,T.numContactID,vcdata,bitTask,textDetails as ActivityDescription from Communication
      left join Listdetails L
      on L.numListItemID = bitTask
      join tt_TEMPTABLE T
      on T.numContactID = numAssign
      where  (dtStartTime between v_dtWeekStartDate and v_dtWeekEndDate or
      dtEndTime between v_dtWeekStartDate and v_dtWeekEndDate
      OR (bitTask = 974 AND dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtWeekStartDate AND v_dtWeekEndDate)) and bitClosedFlag = false) X;   --974 -- Follow-up Anytime
 
 
   open SWV_RefCur3 for
   SELECT CAST(0 AS NUMERIC(18,0)) AS numCommId,ActivityID,dtStartTime,dtEndTime,numContactId,vcdata,SUBSTR(CAST(dtStartTime AS VARCHAR(30)),length(CAST(dtStartTime AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime AS VARCHAR(30)),length(CAST(dtEndTime AS VARCHAR(30))) -8+1) as Schedule,CAST(0 AS NUMERIC(18,0)) AS bitTask,ActivityDescription from(select A.ActivityID,StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtStartTime,
StartDateTimeUtc+CAST(Duration || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtEndTime,T.numContactID,'Outlook - ' || Subject as vcdata,ActivityDescription
      from Activity A
      join ActivityResource AR
      on AR.ActivityID = A.ActivityID
      join  Resource R
      on R.ResourceID = AR.ResourceID
      join tt_TEMPTABLE T
      on T.numContactID = numUserCntId
      where  (StartDateTimeUtc between v_dtWeekStartDate and v_dtWeekEndDate
      or StartDateTimeUtc+CAST(Duration || 'second' as interval) between v_dtWeekStartDate and v_dtWeekEndDate)
      and A.ActivityID not in(select distinct(numActivityId) from Communication Comm where Comm.numDomainID = v_numDomainID)) X;  
  
  
   RETURN;
END; $$;


