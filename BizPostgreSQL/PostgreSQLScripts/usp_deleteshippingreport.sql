-- Stored procedure definition script USP_DeleteShippingReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteShippingReport(v_ShippingReportId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingReportItems
   WHERE ShippingReportItemId IN(SELECT ShippingReportItemId FROM ShippingReportItems WHERE numShippingReportId = v_ShippingReportId);
    
   DELETE  FROM ShippingBox WHERE   numShippingReportID = v_ShippingReportId;
        
   DELETE FROM ShippingReport WHERE numShippingReportID = v_ShippingReportId;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_DeleteShippingReportItem]    Script Date: 05/07/2009 17:40:20 ******/



