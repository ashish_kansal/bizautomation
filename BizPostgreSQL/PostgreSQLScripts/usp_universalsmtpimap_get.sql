-- Stored procedure definition script USP_UniversalSMTPIMAP_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_UniversalSMTPIMAP_Get(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		vcSMTPServer
		,numSMTPPort
		,bitSMTPSSL
		,bitSMTPAuth
		,vcIMAPServer
		,numIMAPPort
		,bitIMAPSSL
		,bitIMAPAuth
   FROM
   UniversalSMTPIMAP
   WHERE
   numDomainID = v_numDomainID;
END; $$;












