-- Function definition script fn_GetKitInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetKitInventory(v_numKitId NUMERIC
	,v_numWarehouseID NUMERIC(18,0))
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OnHand  DOUBLE PRECISION;
BEGIN
   v_OnHand := 0;       
    
   with recursive CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS(SELECT
   CAST(0 AS NUMERIC(18,0)) AS numItemKitID,
			numItemCode AS numItemCode,
			DTL.numQtyItemsReq AS numQtyItemsReq,
			CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty
   FROM
   Item
   INNER JOIN
   ItemDetails DTL
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numKitId
   UNION ALL
   SELECT
   DTL.numItemKitID AS numItemKitID,
			i.numItemCode AS numItemCode,
			DTL.numQtyItemsReq AS numQtyItemsReq,
			CAST((DTL.numQtyItemsReq*c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
   FROM
   Item i
   INNER JOIN
   ItemDetails DTL
   ON
   DTL.numChildItemID = i.numItemCode
   INNER JOIN
   CTE c
   ON
   DTL.numItemKitID = c.numItemCode
   WHERE
   DTL.numChildItemID != v_numKitId) select   CAST(MIN(FLOOR(numOnHand)) AS DOUBLE PRECISION) INTO v_OnHand FROM(SELECT(CASE WHEN coalesce(SUM(numOnHand),0) = 0 THEN 0 WHEN coalesce(SUM(numOnHand),0) >= numQtyItemsReq AND numQtyItemsReq > 0 THEN coalesce(SUM(numOnHand),0)/numQtyItemsReq ELSE 0 end) AS numOnHand
      FROM
      cte c
      LEFT JOIN(SELECT
         numItemID
				,numWareHouseID
				,coalesce(numOnHand,0) AS numOnHand
         FROM
         WareHouseItems) WI
      ON
      WI.numItemID = c.numItemCode
      AND WI.numWareHouseID = v_numWarehouseID
      GROUP BY
      c.numItemCode,c.numQtyItemsReq) TEMP;

   RETURN v_OnHand;
END; $$;

