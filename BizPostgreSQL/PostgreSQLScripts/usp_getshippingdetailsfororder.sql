-- Stored procedure definition script Usp_GetShippingDetailsForOrder for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetShippingDetailsForOrder(v_numOppID		NUMERIC(18,0),
	v_numDomainID	NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  OM.numOppId,OBD.numBizDocId,OBD.numOppBizDocsId, SR.numShippingReportID, SR.numShippingCompany,
	COALESCE((SELECT string_agg(coalesce(vcTrackingNumber,''),',') FROM ShippingBox WHERE numShippingReportID = SR.numShippingReportID),'') AS vcTrackingNumber,
	(SELECT COUNT(numBoxID) FROM ShippingBox WHERE numShippingReportID = SR.numShippingReportID) AS numBoxCount,
	(SELECT  cast(numBoxID as NUMERIC(18,0)) FROM ShippingBox WHERE numShippingReportID = SR.numShippingReportID LIMIT 1) AS numBoxID,
	CASE WHEN(SELECT COUNT(*) FROM ShippingReport WHERE ShippingReport.numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = v_numDomainID) > 0 THEN 1
   ELSE 0
   END AS IsShippingReportGenerated,
	CASE WHEN(SELECT COUNT(*) FROM ShippingBox
      WHERE numShippingReportID IN(SELECT cast(numShippingReportID as NUMERIC(18,0)) FROM ShippingReport WHERE numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = v_numDomainID)
      AND LENGTH(coalesce(vcTrackingNumber,'')) > 0) > 0 THEN true
   ELSE false
   END AS ISTrackingNumGenerated,
	LD.vcData as BizDoc,
	CASE WHEN tintopptype = 1  THEN(CASE WHEN coalesce(numAuthoritativeSales,0) = numBizDocId THEN 'Authoritative' || CASE WHEN coalesce(OBD.tintDeferred,0) = 1 THEN ' (Deferred)'
         ELSE ''
         END
      ELSE 'Non-Authoritative'
      END)
   WHEN tintopptype = 2  THEN(CASE WHEN coalesce(numAuthoritativePurchase,0) = numBizDocId THEN 'Authoritative' || CASE WHEN coalesce(OBD.tintDeferred,0) = 1 THEN ' (Deferred)'
         ELSE ''
         END
      ELSE 'Non-Authoritative'
      END)
   END AS BizDocType,
	coalesce(vcBizDocID,'') AS vcBizDocID,
	(SELECT SUM(fltTotalWeight) FROM ShippingBox SBOX WHERE SBOX.numShippingReportID = SR.numShippingReportID
      GROUP BY SBOX.numShippingReportID) AS BoxWeight,
	COALESCE((SELECT string_agg(coalesce(ShippingBox.vcBoxName,'') || '- Weight: ' || CAST(coalesce(fltTotalWeight,0) AS VARCHAR(20)) || ' lbs, LxBxH: ' || CAST(coalesce(fltLength,0) AS VARCHAR(20)) || 'x' ||  CAST(coalesce(fltWidth,0) AS VARCHAR(20)) || 'x' || CAST(coalesce(fltHeight,0) AS VARCHAR(20)),'~')  FROM ShippingBox WHERE numShippingReportID = SR.numShippingReportID),'') AS WeightPerBox,
	COALESCE((SELECT string_agg(vcBizDocID || '~' || CAST(numOppBizDocsId AS VARCHAR(10)),', ') FROM OpportunityBizDocs OppBiz WHERE OppBiz.numoppid = OM.numOppId),'') AS vcBizDocs,
	coalesce((SELECT cast(MAX(SB.numShippingReportID) as NUMERIC(18,0)) FROM ShippingBox SB
      WHERE SB.numShippingReportID = SR.numShippingReportID
      AND SR.numOppBizDocId = OBD.numOppBizDocsId AND SR.numDomainID = OM.numDomainId
      AND LENGTH(coalesce(SB.vcTrackingNumber,'')) > 0),
   0) AS numShippingLabelReportId
   FROM OpportunityBizDocs OBD
   JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
   JOIN OpportunityItems AS OI ON OI.numOppId = OM.numOppId
   LEFT JOIN ShippingReport SR ON OBD.numOppBizDocsId = SR.numOppBizDocId AND SR.numOppID = OM.numOppId
   LEFT JOIN Listdetails LD on LD.numListItemID = OBD.numBizDocId
   LEFT JOIN AuthoritativeBizDocs AB on AB.numDomainId = OM.numDomainId
   WHERE OM.numDomainId = v_numDomainID
   AND OM.numOppId = v_numOppID
   ORDER BY SR.numShippingReportID DESC LIMIT 1;
END; $$;













