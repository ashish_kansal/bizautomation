CREATE OR REPLACE FUNCTION USP_CheckCodeForPromotion(v_numDomainID NUMERIC(18,0)
	,v_txtCouponCode VARCHAR(100)
	,v_numProdId NUMERIC(18,0) DEFAULT 0
	,v_numDivisionId NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCouponCode  NUMERIC(18,0);
   v_intCodeUsageLimit  NUMERIC(18,0);
   v_intCodeUsed  INTEGER;
   v_ERRORMESSAGE  VARCHAR(50);
   v_intUsedCountForDiv  INTEGER;
BEGIN
   SELECT COUNT(DC.vcDiscountCode) INTO v_numCouponCode FROM PromotionOffer PO
   INNER JOIN DiscountCodes DC
   ON PO.numProId = DC.numPromotionID WHERE numProId = v_numProdId AND numDomainId = v_numDomainID AND bitRequireCouponCode = true
   AND DC.vcDiscountCode = v_txtCouponCode;

   IF v_numCouponCode = 1 then
	
      SELECT DC.CodeUsageLimit INTO v_intCodeUsageLimit FROM PromotionOffer PO
      INNER JOIN DiscountCodes DC
      ON PO.numProId = DC.numPromotionID WHERE numProId = v_numProdId AND numDomainId = v_numDomainID AND bitRequireCouponCode = true
      AND DC.vcDiscountCode = v_txtCouponCode;
      IF v_intCodeUsageLimit = 0 then
		
         open SWV_RefCur for
         SELECT 1;
      ELSEIF v_intCodeUsageLimit > 0
      then
		
         SELECT COUNT(DCU.numDivisionId) INTO v_intUsedCountForDiv FROM DiscountCodeUsage DCU
         INNER JOIN DiscountCodes DC
         ON  DCU.numDiscountId = DC.numDiscountId WHERE DCU.numDivisionId = v_numDivisionId AND DC.vcDiscountCode = v_txtCouponCode;
         IF v_intUsedCountForDiv = 0 then
			
            open SWV_RefCur for
            SELECT 1;
         ELSEIF v_intUsedCountForDiv > 0
         then
			
            SELECT intCodeUsed INTO v_intCodeUsed FROM DiscountCodeUsage DCU
            INNER JOIN DiscountCodes DC
            ON  DCU.numDiscountId = DC.numDiscountId WHERE DCU.numDivisionId = v_numDivisionId AND DC.vcDiscountCode = v_txtCouponCode;
            IF v_intCodeUsed < v_intCodeUsageLimit then
				
               open SWV_RefCur for
               SELECT 1;
            ELSE
               v_ERRORMESSAGE := 'CODE_USAGE_LIMIT_EXCEEDS';
               RAISE EXCEPTION '%',v_ERRORMESSAGE;
               RETURN;
            end if;
         end if;
      end if;
   end if;	

   IF v_numCouponCode = 0 then
	
      v_ERRORMESSAGE := 'INVALID_CODE';
      RAISE EXCEPTION '%',v_ERRORMESSAGE;
      RETURN;
   end if;
END; $$; 


