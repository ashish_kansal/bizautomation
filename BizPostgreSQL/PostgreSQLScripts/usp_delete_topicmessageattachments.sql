-- Stored procedure definition script USP_DELETE_TOPICMESSAGEATTACHMENTS for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DELETE_TOPICMESSAGEATTACHMENTS(v_numAttachmentId NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numAttachmentId != 0) then
	
      DELETE FROM TOPICMESSAGEATTACHMENTS WHERE numAttachmentId = v_numAttachmentId AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;

	



