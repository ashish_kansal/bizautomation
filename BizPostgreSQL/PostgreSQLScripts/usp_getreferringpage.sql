CREATE OR REPLACE FUNCTION USP_GetReferringPage
(
	v_ReferringPage VARCHAR(100),                        
	v_searchTerm VARCHAR(100),                        
	v_From TIMESTAMP,                        
	v_To TIMESTAMP,                        
	v_CurrentPage INTEGER,                                
	v_PageSize INTEGER,                                
	INOUT v_TotRecs INTEGER ,                                
	v_columnName VARCHAR(50),                                
	v_columnSortOrder VARCHAR(10),                
	v_strCrawler VARCHAR(5) DEFAULT '',              
	v_MinimumPages VARCHAR(5) DEFAULT NULL,        
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,               
	v_ClientOffsetTime INTEGER DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql                                
AS $$
	DECLARE
	v_strSql  VARCHAR(5000);                                
	v_firstRec  INTEGER;                                
	v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN NULL;
   END;
                                                            
	v_strSql := 'select 
					THDR.numTrackingID as numTrackingID
					,THDR.vcUserHostAddress as vcUserHostAddress
					,case when NULLIF(THDR.vcUserDomain,'''') is null then THDR.vcUserHostAddress when THDR.vcUserDomain = ''-'' then THDR.vcUserHostAddress else THDR.vcUserDomain end as vcUserDomain
					,coalesce(THDR.vcURL,''-'') as vcURL
					,coalesce(THDR.vcReferrer,''-'') as vcReferrer
					,coalesce(THDR.vcSearchTerm,''-'') as vcSearchTerm
					,THDR.numVisits
					,THDR.numVistedPages
					,CAST(THDR.vcTotalTime AS VARCHAR(8)) as vcTotalTime
					,coalesce(THDR.vcCountry,''-'') as vcCountry                         
				from 
					TrackingVisitorsHDR THDR             
				where numDomainId =' || v_numDomainID || ' and  THDR.dtCreated between ''' || CAST(v_From+CAST(v_ClientOffsetTime || 'minute' as interval) AS VARCHAR(50))
				|| ''' and ''' || CAST(v_To+CAST(v_ClientOffsetTime || 'minute' as interval) AS VARCHAR(50)) || ''' and THDR.numVistedPages >= cast(NULLIF('''
				|| coalesce(v_MinimumPages,'') || ''','''') as NUMERIC(18,0))';            
        
	if (v_searchTerm <> '' and v_searchTerm is not null) then
		v_strSql := coalesce(v_strSql,'') || ' and THDR.vcSearchTerm ilike ''%' || coalesce(v_searchTerm,'') || '%''';
	end if;                 
	if v_strCrawler <> '' then  
		v_strSql := coalesce(v_strSql,'') || ' and THDR.vcCrawler = ''' || coalesce(v_strCrawler,'') || '''';
	end if;                   
	if (v_ReferringPage <> '' and v_ReferringPage is not null) then
		v_strSql := coalesce(v_strSql,'') || ' and THDR.vcOrginalRef ilike ''%' || coalesce(v_ReferringPage,'') || '%''';
	end if;                                 
	
	v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                              
                        
	EXECUTE 'drop table IF EXISTS tt_TEMPTABLE CASCADE;
			Create TEMPORARY TABLE tt_TEMPTABLE
			( 
				ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
				numTrackingID NUMERIC(9,0),
				vcUserHostAddress VARCHAR(100),
				vcUserDomain VARCHAR(100),
				vcURL VARCHAR(500),
				vcReferrer VARCHAR(500),
				vcSearchTerm VARCHAR(100),
				numVisits NUMERIC,
				numVistedPages NUMERIC(9,0),
				vcTotalTime VARCHAR(8),
				vcCountry  VARCHAR(50)
			); 
   
			insert into tt_TEMPTABLE 
			(
				numTrackingID,
				vcUserHostAddress,                                
				vcUserDomain ,                                
				vcURL ,                               
				vcReferrer,                               
				vcSearchTerm ,                                
				numVisits,                                
				numVistedPages,                                
				vcTotalTime,vcCountry
			) ' || v_strSql;
			
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                
   
	open SWV_RefCur for
	select 
		numTrackingID,
		vcUserHostAddress,
		vcUserDomain ,
		vcURL ,
		vcReferrer,
		vcSearchTerm ,
		numVisits,
		numVistedPages,
		vcTotalTime,
		vcCountry,
		CASE WHEN LENGTH(vcURL) > 45 then SUBSTR(vcURL,1,45) || '..' else vcURL end as URL,
        CASE WHEN LENGTH(vcReferrer) > 45 then SUBSTR(vcReferrer,1,45) || '..' else vcReferrer end as Referrer
	from 
		tt_TEMPTABLE 
	where 
		ID > v_firstRec and ID < v_lastRec;
   
	select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                
	RETURN;
END; $$;


