-- Stored procedure definition script USP_GetVendorsWareHouse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetVendorsWareHouse(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemCode NUMERIC(9,0) DEFAULT 0,
v_numVendorid NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
 
--tintAddressType: 2 Shipping tintAddressOf:2 Organization
      open SWV_RefCur for
      select div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,
vcFirstName || ' ' || vcLastname as ConName,
CAST(numPhone AS VARCHAR(15)) || ', ' || CAST(numPhoneExtension AS VARCHAR(15)) as Phone,coalesce(monCost,0) AS monCost,vcItemName,coalesce(intMinQty,0) AS intMinQty
,numContactId,Ad.numAddressID,Ad.vcAddressName,
coalesce(Ad.vcStreet,'') || ' ,' || coalesce(Ad.vcCity,'') || ' ,'
      || coalesce(fn_GetState(Ad.numState),'') || ' '
      || coalesce(Ad.vcPostalCode,'') || ' ,'
      || coalesce(fn_GetListItemName(Ad.numCountry),'') AS vcFullAddress,
                         fn_GetVendorTransitCount(Vendor.numVendorID,Ad.numAddressID,Vendor.numDomainID) AS VendorTransitCount
      from Vendor join DivisionMaster div  on div.numDivisionID = Vendor.numVendorID
      join CompanyInfo com on com.numCompanyId = div.numCompanyID
      left join AdditionalContactsInformation ADC on ADC.numDivisionId = div.numDivisionID
      join Item I on I.numItemCode = Vendor.numItemCode
      JOIN AddressDetails Ad ON Ad.numRecordID = div.numDivisionID
      WHERE coalesce(ADC.bitPrimaryContact,false) = true and Vendor.numDomainID = v_numDomainID and Vendor.numItemCode = v_numItemCode
      AND Ad.tintAddressOf = 2 AND Ad.tintAddressType = 2 AND (Vendor.numVendorID = v_numVendorid OR v_numVendorid = 0);
   ELSEIF v_tintMode = 2
   then

      open SWV_RefCur for
      select Ad.numAddressID,coalesce(Ad.vcAddressName,'') || ' - ' ||
      coalesce(Ad.vcStreet,'') || ' ,' || coalesce(Ad.vcCity,'') || ' ,'
      || coalesce(fn_GetState(Ad.numState),'') || ' '
      || coalesce(Ad.vcPostalCode,'') || ' ,'
      || coalesce(fn_GetListItemName(Ad.numCountry),'') AS vcFullAddress
      from DivisionMaster div
      join CompanyInfo com on com.numCompanyId = div.numCompanyID
      left join AdditionalContactsInformation ADC on ADC.numDivisionId = div.numDivisionID
      JOIN AddressDetails Ad ON Ad.numRecordID = div.numDivisionID
      WHERE coalesce(ADC.bitPrimaryContact,false) = true and div.numDomainID = v_numDomainID
      AND Ad.tintAddressOf = 2 AND Ad.tintAddressType = 2 AND div.numDivisionID = v_numVendorid ORDER BY coalesce(Ad.bitIsPrimary,false) DESC;
   end if;
   RETURN;
END; $$;



