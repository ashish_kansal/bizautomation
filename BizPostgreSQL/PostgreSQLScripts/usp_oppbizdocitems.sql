CREATE OR REPLACE FUNCTION USP_OPPBizDocItems
(
	v_numOppId NUMERIC(9,0) DEFAULT NULL,
    v_numOppBizDocsId NUMERIC(9,0) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null,
	INOUT SWV_RefCur3 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_DivisionID  NUMERIC(9,0);      
	v_tintTaxOperator  SMALLINT;                                                                                                                                                                                                                                                                                                                                
	v_DecimalPoint  SMALLINT;
	v_strSQL  TEXT;                                                                                          
	v_strSQL1 TEXT;                                       
	v_strSQLCusFields TEXT;                      
	v_strSQLEmpFlds TEXT;                                                                         
	v_tintType  SMALLINT;   
	v_tintOppType  SMALLINT;                                                      
	v_intRowNum  INTEGER;                                                  
	v_numFldID  VARCHAR(15);                                          
	v_vcFldname  VARCHAR(50);                                            
	v_numBizDocId  NUMERIC(9,0);
	v_numBizDocTempID  NUMERIC(9,0);
	v_strSQLUpdate TEXT;
	v_vcTaxName  VARCHAR(100);
	v_numTaxItemID  NUMERIC(9,0);
	v_vcDbColumnName  VARCHAR(50);
	SWV_ExecDyn TEXT;
	SWV_RowCount INTEGER;
BEGIN
	SELECT numDivisionId, tintTaxOperator INTO v_DivisionID,v_tintTaxOperator FROM OpportunityMaster WHERE numOppId = v_numOppId; 
	--Decimal points to show in bizdoc 
	SELECT tintDecimalPoints INTO v_DecimalPoint FROM Domain WHERE numDomainId = v_numDomainID;
                                                                                          
	v_strSQLCusFields := '';                                                                                          
	v_strSQLEmpFlds := '';                                                                                          
	v_numBizDocTempID := 0;
                                           
   SELECT coalesce(numBizDocTempID,0), numBizDocId INTO v_numBizDocTempID,v_numBizDocId FROM OpportunityBizDocs WHERE   numOppBizDocsId = v_numOppBizDocsId;
   SELECT tintopptype, tintopptype INTO v_tintType,v_tintOppType FROM    OpportunityMaster WHERE numOppId = v_numOppId;                                                                                          
                                                                                   
	IF v_tintType = 1 then
		v_tintType := 7;
	ELSE
		v_tintType := 8;
	end if;                                                               
                                                                                       
	DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS
    SELECT 
		* 
	FROM
	(
		SELECT 
			Opp.vcItemName AS vcItemName
			,Opp.numCost
			,Opp.vcNotes as txtNotes
			,coalesce(OBD.numOppBizDocItemID,0) AS OppBizDocItemID
			,CASE WHEN charitemType = 'P' THEN 'Product'
			WHEN charitemType = 'S' THEN 'Service'
			END AS charitemType,
							CASE WHEN charitemType = 'P' THEN 'Inventory'
			WHEN charitemType = 'N' THEN 'Non-Inventory'
			WHEN charitemType = 'S' THEN 'Service'
			END AS vcItemType,
			CASE WHEN bitShowDeptItemDesc = false
			THEN OBD.vcItemDesc
			WHEN bitShowDeptItemDesc = true
			AND bitKitParent = true
			AND bitAssembly = false
			THEN fn_PopulateKitDesc(Opp.numOppId,Opp.numoppitemtCode)
			WHEN bitShowDeptItemDesc = true
			AND bitKitParent = true
			AND bitAssembly = true
			THEN fn_PopulateAssemblyDesc(Opp.numoppitemtCode,OBD.numUnitHour)
			ELSE OBD.vcItemDesc
			END AS txtItemDesc,
			coalesce(OBD.numUnitHour,0) AS numOrigUnitHour,
            fn_UOMConversion(coalesce(i.numBaseUnit,0),i.numItemCode,v_numDomainID,coalesce(Opp.numUOMId,0))*OBD.numUnitHour AS numUnitHour,
            CAST((fn_UOMConversion(coalesce(Opp.numUOMId,0),i.numItemCode,v_numDomainID,coalesce(i.numBaseUnit,0))*OBD.monPrice) AS DECIMAL(20,5)) AS monPrice,
            CAST(OBD.monTotAmount AS DECIMAL(20,5)) AS Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            coalesce(CAST(i.monListPrice AS VARCHAR(30)),cast(0 as TEXT)) AS monListPrice,
            i.numItemCode AS ItemCode,
            Opp.numoppitemtCode,
            L.vcData AS numItemClassification,
            CASE WHEN bitTaxable = false THEN 'No'
			ELSE 'Yes'
			END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            coalesce(GJH.numJOurnal_Id,0) AS numJournalId,
            (SELECT 
			coalesce(GJD.numTransactionId,0)
			FROM      General_Journal_Details GJD
			WHERE     GJH.numJOurnal_Id = GJD.numJournalId
			AND GJD.chBizDocItems = 'NI'
			AND GJD.numoppitemtCode = Opp.numoppitemtCode LIMIT 1) AS numTransactionId,
			coalesce(Opp.vcModelID,'') AS vcModelID,
			(CASE WHEN charitemType = 'P' THEN USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE coalesce(OBD.vcAttributes,'') END) AS vcAttributes,
			coalesce(vcPartNo,'') AS vcPartNo,
            (coalesce(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CAST(coalesce(OBD.monTotAmount,0)/OBD.numUnitHour AS DECIMAL(20,5)) END) As monUnitSalePrice,
            OBD.vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            FormatedDateFromDate(CAST(OBD.dtDeliveryDate AS TEXT),v_numDomainID) AS dtDeliveryDate,
            Opp.numWarehouseItmsID,
            coalesce(u.vcUnitName,'') AS numUOMId,
            coalesce(Opp.vcManufacturer,'') AS vcManufacturer,
            coalesce(V.monCost,0) AS VendorCost,
            Opp.vcPathForTImage,
            i.fltLength,
            i.fltWidth,
            i.fltHeight,
            (CASE
			WHEN coalesce(bitKitParent,false) = true
			AND(CASE
			WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = i.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
			THEN 1
			ELSE 0
			END) = 0
			THEN
				GetKitWeightBasedOnItemSelection(i.numItemCode,Opp.vcChildKitSelectedItems)
			ELSE
				coalesce(fltWeight,0)
			END) AS fltWeight,
            coalesce(i.numVendorID,0) AS numVendorID,
            Opp.bitDropShip AS DropShip,
            coalesce((SELECT  
						string_agg(vcSerialNo || CASE WHEN coalesce(i.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')' ELSE '' END,', '  ORDER BY vcSerialNo)
					 FROM    OppWarehouseSerializedItem oppI
					 JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
					 WHERE   oppI.numOppID = Opp.numOppId
					 AND oppI.numOppItemID = Opp.numoppitemtCode),'') AS SerialLotNo,
			coalesce(Opp.numUOMId,0) AS numUOM,
			coalesce(u.vcUnitName,'') AS vcUOMName,
			fn_UOMConversion(Opp.numUOMId,Opp.numItemCode,v_numDomainID,NULL::NUMERIC) AS UOMConversionFactor,
			coalesce(Opp.numSOVendorId,0) AS numSOVendorId,
			FormatedDateTimeFromDate(OBD.dtRentalStartDate,v_numDomainID) AS dtRentalStartDate,
			FormatedDateTimeFromDate(OBD.dtRentalReturnDate,v_numDomainID) AS dtRentalReturnDate,
			coalesce(W.vcWareHouse,'') AS Warehouse,
			(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation,')') ELSE '' END) ELSE '' END) AS vcWarehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
			ELSE i.vcSKU
			END AS vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode
			ELSE i.numBarCodeId
			END AS vcBarcode,
            coalesce(Opp.numClassID,0) AS numClassID,
            coalesce(Opp.numProjectID,0) AS numProjectID,
            coalesce(i.numShipClass,0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,coalesce(bitSerialized,false) AS bitSerialized,coalesce(bitLotNo,false) AS bitLotNo,
            Opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,coalesce(i.numItemCode,0) AS numItemCode,
			coalesce(Opp.vcPromotionDetail,'') AS vcPromotionDetail
			,CASE WHEN Opp.ItemReleaseDate IS null THEN
			coalesce(OM.dtReleaseDate,'1900-01-01 00:00:00.000')
			ELSE Opp.ItemReleaseDate
			END AS ItemRequiredDate,
			coalesce(SOLIPL.numPurchaseOrderID,0) AS numPOID,coalesce(SOLIPL.numPurchaseOrderItemID,0) AS numPOItemID,coalesce(OMPO.vcpOppName,'') AS vcPOName
			,coalesce(Opp.vcAttrValues,'') AS vcAttrValues
		FROM 
		OpportunityItems Opp
		JOIN OpportunityBizDocItems OBD ON CAST(OBD.numOppItemID AS NUMERIC) = Opp.numoppitemtCode
		LEFT JOIN Item i ON Opp.numItemCode = i.numItemCode
		LEFT JOIN Listdetails L ON i.numItemClassification = L.numListItemID
		LEFT JOIN Vendor V ON V.numVendorID = v_DivisionID
		AND V.numItemCode = Opp.numItemCode
		LEFT JOIN General_Journal_Header GJH ON Opp.numOppId = GJH.numOppId
		AND GJH.numBizDocsPaymentDetId IS NULL
		AND GJH.numOppBizDocsId = v_numOppBizDocsId
		LEFT JOIN UOM u ON u.numUOMId = Opp.numUOMId
		LEFT JOIN WareHouseItems WI ON Opp.numWarehouseItmsID = WI.numWareHouseItemID
		AND WI.numDomainID = v_numDomainID
		LEFT JOIN Warehouses W ON WI.numDomainID = W.numDomainID
		AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
		LEFT JOIN OpportunityMaster OM ON Opp.numOppId = OM.numOppId
		LEFT JOIN SalesOrderLineItemsPOLinking SOLIPL ON Opp.numOppId = SOLIPL.numSalesOrderID AND Opp.numoppitemtCode = SOLIPL.numSalesOrderItemID
		AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0
		LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numPurchaseOrderID = OMPO.numOppId
		WHERE     Opp.numOppId = v_numOppId
		AND (bitShowDeptItem IS NULL
		OR bitShowDeptItem = false)
		AND OBD.numOppBizDocID = v_numOppBizDocsId
	) X;

	v_strSQLUpdate := '';
   
	EXECUTE 'ALTER TABLE tt_TEMP1 add TotalTax DECIMAL(20,5)';
	EXECUTE 'ALTER TABLE tt_TEMP1 add CRV DECIMAL(20,5)';

   IF v_tintTaxOperator = 1 then --add Sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',CRV = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
   ELSEIF v_tintTaxOperator = 2
   then -- remove sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', TotalTax = 0';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
   ELSE
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ', CRV = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
   end if;


   v_numTaxItemID := 0;
   select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID    LIMIT 1;


   WHILE v_numTaxItemID > 0 LOOP
      SWV_ExecDyn := 'ALTER TABLE tt_TEMP1 add "' || coalesce(v_vcTaxName,'') || '" DECIMAL(20,5)';
      EXECUTE SWV_ExecDyn;
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', "' || coalesce(v_vcTaxName,'')
      || '" = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ','
      || SUBSTR(CAST(v_numTaxItemID AS VARCHAR(20)),1,20) || ','
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID
      AND numTaxItemID > v_numTaxItemID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numTaxItemID := 0;
      end if;
   END LOOP;

   IF v_strSQLUpdate <> '' then
    
      v_strSQLUpdate := 'UPDATE tt_TEMP1 SET ItemCode = ItemCode' || coalesce(v_strSQLUpdate,'') || ' WHERE ItemCode > 0';
      EXECUTE v_strSQLUpdate;
   end if;

   select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
   AND Grp_id = 5
   AND numDomainID = v_numDomainID
   AND numAuthGroupID = v_numBizDocId
   AND bitCustom = true
   AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;                                                                                          
   WHILE v_intRowNum > 0 LOOP
      SWV_ExecDyn := 'alter table tt_TEMP1 add "' || coalesce(v_vcDbColumnName,'')
      || '" VARCHAR(100)';
      EXECUTE SWV_ExecDyn;
      v_strSQLUpdate := 'update tt_TEMP1 set ItemCode = ItemCode,"'
      || coalesce(v_vcDbColumnName,'') || '" = GetCustFldValueItem(' || coalesce(v_numFldID,'')
      || ',ItemCode) where ItemCode > 0';
      EXECUTE v_strSQLUpdate;
      select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND tintRow >= v_intRowNum
      AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_intRowNum := 0;
      end if;
   END LOOP;


   open SWV_RefCur for
   SELECT  Row_NUmber() OVER(ORDER BY numoppitemtCode) AS RowNumber,
            * FROM    tt_TEMP1
   ORDER BY numoppitemtCode ASC; 

    

   open SWV_RefCur2 for
   SELECT  coalesce(tintoppstatus,0)
   FROM    OpportunityMaster
   WHERE   numOppId = v_numOppId;                                                                                                 


   IF(SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormId = v_tintType AND numDomainID = v_numDomainID
   AND numAuthGroupId = v_numBizDocId AND vcFieldType = 'R' AND coalesce(numRelCntType,0) = v_numBizDocTempID) = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
      select numFormId,numFieldID,1,tintRow,v_numDomainID,v_numBizDocId,v_numBizDocTempID
      FROM View_DynamicDefaultColumns
      where numFormId = v_tintType and bitDefault = true and numDomainID = v_numDomainID order by tintRow ASC;
      IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID = v_numBizDocId and Ds.numFormID = v_tintType
      and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID) = 0 then
	
         INSERT INTO DycFrmConfigBizDocsSumm(numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
         SELECT v_tintType,v_numBizDocId,DFM.numFieldID,v_numDomainID,v_numBizDocTempID FROM
         DycFormField_Mapping DFFM
         JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
         where numFormID = 16 and DFFM.numDomainID is null;
      end if;
   end if;      


   open SWV_RefCur3 for
   SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
   FROM    View_DynamicColumns
   WHERE   numFormId = v_tintType
   AND numDomainID = v_numDomainID
   AND numAuthGroupId = v_numBizDocId
   AND vcFieldType = 'R'
   AND coalesce(numRelCntType,0) = v_numBizDocTempID
   UNION
   SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' AS vcFieldDataType,
            tintRow AS intRowNum
   FROM    View_DynamicCustomColumns
   WHERE   numFormId = v_tintType
   AND Grp_id = 5
   AND numDomainID = v_numDomainID
   AND numAuthGroupID = v_numBizDocId
   AND bitCustom = true
   AND coalesce(numRelCntType,0) = v_numBizDocTempID
   ORDER BY 4;
   RETURN;
END; $$;


