-- Stored procedure definition script USP_ManageNewItemRequiredFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 30 May 2014
-- Description:	Adds default required fields for Field Management -> New Item in administation section
-- =============================================
CREATE OR REPLACE FUNCTION USP_ManageNewItemRequiredFields(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new inventory item form
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 86 AND numFieldID = 189 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(86,189,1,1,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 86 AND numFieldID = 270 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(86,270,1,2,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 86 AND numFieldID = 271 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(86,271,2,1,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 86 AND numFieldID = 272 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(86,272,2,2,v_numDomainID,0,0,2,false);
   end if;
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new inventory item form
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 189 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(86,189,v_numDomainID,'Item',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 270 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(86,270,v_numDomainID,'Income Account',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 271 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(86,271,v_numDomainID,'Asset Account',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 272 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(86,272,v_numDomainID,'COGS Account',true);
   end if;
		
		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new non inventory item form
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 87 AND numFieldID = 189 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(87,189,1,1,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 87 AND numFieldID = 270 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(87,270,1,2,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 87 AND numFieldID = 272 AND numDomainID = v_numDomainID) then
			
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(87,272,2,1,v_numDomainID,0,0,2,false);
   end if;
		
		--- Inserts resuired field mapping in DynamicFormField_Validation for validation on form
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 189 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(87,189,v_numDomainID,'Item',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 270 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(87,270,v_numDomainID,'Income Account',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 272 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(87,272,v_numDomainID,'COGS Account',true);
   end if;
		
		 ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new service/lot item form
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 88 AND numFieldID = 189 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(88,189,1,1,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 88 AND numFieldID = 270 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(88,270,1,2,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 88 AND numFieldID = 271 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(88,271,2,1,v_numDomainID,0,0,2,false);
   end if;
   IF NOT EXISTS(SELECT * FROM DycFormConfigurationDetails WHERE numFormID = 88 AND numFieldID = 272 AND numDomainID = v_numDomainID) then
	    
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom) VALUES(88,272,2,2,v_numDomainID,0,0,2,false);
   end if;
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new service lot item form
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 189 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(88,189,v_numDomainID,'Item',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 270 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(88,270,v_numDomainID,'Income Account',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 271 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(88,271,v_numDomainID,'Asset Account',true);
   end if;
   IF NOT EXISTS(SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 272 AND numDomainID = v_numDomainID) then
		
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,vcNewFormFieldName,bitIsRequired) VALUES(88,272,v_numDomainID,'COGS Account',true);
   end if;
   RETURN;
END; $$;


