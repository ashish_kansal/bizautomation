-- Stored procedure definition script USP_Sites_GetSettingFieldValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Sites_GetSettingFieldValue(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_vcFieldName VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'tintWarehouseAvailability' then
	
      open SWV_RefCur for
      SELECT coalesce(tintWarehouseAvailability,1)  FROM eCommerceDTL WHERE numDomainID = v_numDomainID AND numSiteId = v_numSiteID;
   ELSEIF v_vcFieldName = 'bitDisplayQtyAvailable'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitDisplayQtyAvailable,false)  FROM eCommerceDTL WHERE numDomainID = v_numDomainID AND numSiteId = v_numSiteID;
   ELSEIF v_vcFieldName = 'bitElasticSearch'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitElasticSearch,false)  FROM eCommerceDTL WHERE numDomainID = v_numDomainID AND numSiteId = v_numSiteID;
   ELSE
      RAISE EXCEPTION 'FIELD_NOT_FOUND';
   end if;
   RETURN;
END; $$;


