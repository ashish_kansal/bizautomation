-- Function definition script GetProjectedFinish for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectedFinish(v_numDomainID NUMERIC(18,0)
	,v_tintRecordType SMALLINT --1: Item, 2: Workorder
	,v_numRecordID NUMERIC(18,0)
	,v_numQtyToBuild DOUBLE PRECISION
	,v_numBuildProcessID NUMERIC(18,0)
	,v_dtProjectedStartDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER
	,v_numWarehouseID NUMERIC(18,0))
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtPlannedStartDate  TIMESTAMP;
   v_dtProjectedFinishDate  TIMESTAMP;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numWorkScheduleID  NUMERIC(18,0);
   v_numTempUserCntID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_bitParallelStartSet  BOOLEAN DEFAULT false;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numAssignedTo NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      numTaskAssignee NUMERIC(18,0),
      intTaskType INTEGER --1:Parallel, 2:Sequential
		,
      dtPlannedStartDate TIMESTAMP,
      dtFinishDate TIMESTAMP
   );
   IF v_tintRecordType = 2 then
	
		
			--,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
      select   numQtyItemsReq, GetWorkOrderPlannedStartDate(v_numDomainID,WorkOrder.numWOId) INTO v_numQtyToBuild,v_dtPlannedStartDate FROM
      WorkOrder WHERE
      WorkOrder.numDomainId = v_numDomainID
      AND WorkOrder.numWOId = v_numRecordID;
      IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = v_numRecordID) AND tintAction = 1) then
		
         select   MIN(SPDTTL.dtActionTime) INTO v_dtPlannedStartDate FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = v_numRecordID) AND tintAction = 1;
      end if;
      v_dtProjectedFinishDate := v_dtPlannedStartDate;
      INSERT INTO tt_TEMPTASKS(numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee)
      SELECT
      SPDT.numTaskId
			,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*v_numQtyToBuild
			,coalesce(SPDT.intTaskType,1)
			,SPDT.numAssignTo
      FROM
      StagePercentageDetailsTask SPDT
      INNER JOIN
      WorkOrder WO
      ON
      SPDT.numWorkOrderId = WO.numWOId
      WHERE
      SPDT.numDomainID = v_numDomainID
      AND SPDT.numWorkOrderId = v_numRecordID;
   ELSE
      v_dtPlannedStartDate := GetItemWOPlannedStartDate(v_numDomainID,v_numRecordID,v_numWarehouseID,v_numQtyToBuild,v_dtProjectedStartDate);
      v_dtProjectedFinishDate := v_dtPlannedStartDate;
      INSERT INTO tt_TEMPTASKS(numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee)
      SELECT
      SPDT.numTaskId
			,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*v_numQtyToBuild
			,coalesce(SPDT.intTaskType,1)
			,SPDT.numAssignTo
      FROM
      StagePercentageDetails SPD
      INNER JOIN
      StagePercentageDetailsTask SPDT
      ON
      SPD.numStageDetailsId = SPDT.numStageDetailsId
      WHERE
      SPD.numdomainid = v_numDomainID
      AND SPD.slp_id = v_numBuildProcessID
      AND coalesce(SPDT.numWorkOrderId,0) = 0;
   end if;

   UPDATE tt_TEMPTASKS SET dtPlannedStartDate = v_dtPlannedStartDate;

   INSERT INTO tt_TEMPTASKASSIGNEE(numAssignedTo)
   SELECT DISTINCT
   numTaskAssignee
   FROM
   tt_TEMPTASKS;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

   WHILE v_i <= v_iCount LOOP
      select   numTaskAssignee, numTaskTimeInMinutes, intTaskType INTO v_numTaskAssignee,v_numTotalTaskInMinutes,v_intTaskType FROM
      tt_TEMPTASKS WHERE
      ID = v_i;

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
      select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), tmStartOfDay, CONCAT(',',vcWorkDays,','), CAST(TO_CHAR(v_dtPlannedStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval) INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_tmStartOfDay,v_vcWorkDays,
      v_dtStartDate FROM
      WorkSchedule WS
       WHERE
      WS.numUserCntID = v_numTaskAssignee;
      IF v_intTaskType = 1 AND EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
      ELSEIF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
      then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
      ELSEIF v_dtStartDate IS NULL
      then
		
         v_dtStartDate := v_dtPlannedStartDate;
      end if;
      UPDATE tt_TEMPTASKS SET dtPlannedStartDate = v_dtStartDate WHERE ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 AND coalesce(v_vcWorkDays,'') <> '' then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtStartDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
               IF CAST(v_dtStartDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE) then
					
						-- CHECK TIME LEFT FOR DAY BASED
                  v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
                  'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
                  'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
                  'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));
               ELSE
                  v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
               end if;
               IF v_numTimeLeftForDay > 0 then
					
                  IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
						
                     v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                  ELSE
                     v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
                  end if;
                  v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
               ELSE
                  v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
               end if;
            ELSE
               v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
            end if;
         END LOOP;
      end if;
      UPDATE tt_TEMPTASKS SET dtFinishDate = v_dtStartDate WHERE ID = v_i;
      UPDATE tt_TEMPTASKASSIGNEE SET dtLastTaskCompletionTime = v_dtStartDate WHERE numAssignedTo = v_numTaskAssignee;
      v_i := v_i::bigint+1;
   END LOOP;

   IF(SELECT COUNT(*) FROM tt_TEMPTASKS) > 0 then
	
      select   MAX(dtFinishDate) INTO v_dtProjectedFinishDate FROM tt_TEMPTASKS;
   end if;

   RETURN v_dtProjectedFinishDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
END; $$;

