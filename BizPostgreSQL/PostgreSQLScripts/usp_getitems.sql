-- Stored procedure definition script USP_GetItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItems(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numItemCode as VARCHAR(255)),cast(vcItemName as VARCHAR(255)),cast(monListPrice as VARCHAR(255)) as Price,cast(vcSKU as VARCHAR(255)) as SKU,cast(vcModelID as VARCHAR(255)) as "Model ID" from Item where numDomainID = v_numDomainID order by  vcItemName;
END; $$;












