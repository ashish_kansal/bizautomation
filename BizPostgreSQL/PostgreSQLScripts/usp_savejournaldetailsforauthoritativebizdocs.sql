-- Stored procedure definition script USP_SaveJournalDetailsForAuthoritativeBizDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveJournalDetailsForAuthoritativeBizDocs(v_numJournalId NUMERIC(9,0) DEFAULT 0,                                                                                          
v_strRow TEXT DEFAULT '',                                                                          
v_Mode NUMERIC(9,0) DEFAULT NULL,                                              
v_RecurringMode NUMERIC(9,0) DEFAULT 0,                        
v_numDomainId NUMERIC(9,0) DEFAULT 0,                      
v_numOppId NUMERIC(9,0) DEFAULT 0,
v_OppBizDocId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMaxJournalId  NUMERIC(9,0);
   v_hDoc3  INTEGER;
BEGIN
	If v_RecurringMode = 0 then
		If SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then
		
			If v_Mode = 0 then
			
				DELETE FROM General_Journal_Details WHERE numJournalId = v_numJournalId AND numDomainId = v_numDomainId;

				Insert Into General_Journal_Details
				(
					numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,numBalance,bitMainCheck,numoppitemtCode
					,chBizDocItems,bitReconcile,numCurrencyID,fltExchangeRate,monOrgCreditAmt,monOrgDebitAmt,numTaxItemID,numcontactid,numItemID,numProjectID
					,numClassID,numCommissionID
				)
				SELECT 
					numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,numBalance,bitMainCheck
					,NULLIF(numoppitemtCode,0),chBizDocItems,bitReconcile,numCurrencyID,fltExchangeRate,monOrgCreditAmt,monOrgDebitAmt,numTaxItemID
					,numContactID,numItemID,NULLIF(numProjectID,0),NULLIF(numClassID,0),numCommissionID	 
				FROM
				XMLTABLE
				(
					'NewDataSet/Table1'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						TransactionId NUMERIC(18,0) PATH 'TransactionId',
						numJournalId NUMERIC(9,0) PATH 'numJournalId',
						numDebitAmt NUMERIC(20,5) PATH 'numDebitAmt',
						numCreditAmt NUMERIC(20,5) PATH 'numCreditAmt',
						numChartAcntId NUMERIC(9,0) PATH 'numChartAcntId',
						varDescription TEXT PATH 'varDescription',
						numCustomerId NUMERIC(9,0) PATH 'numCustomerId',
						numDomainId NUMERIC(9,0) PATH 'numDomainId',
						numBalance NUMERIC(19,4) PATH 'numBalance',
						bitMainCheck BOOLEAN PATH 'bitMainCheck',
						numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode',
						chBizDocItems CHAR(10) PATH 'chBizDocItems',
						bitReconcile BOOLEAN PATH 'bitReconcile',
						numCurrencyID NUMERIC(9,0) PATH 'numCurrencyID',
						fltExchangeRate DOUBLE PRECISION PATH 'fltExchangeRate',
						monOrgCreditAmt NUMERIC(19,4) PATH 'monOrgCreditAmt',
						monOrgDebitAmt NUMERIC(19,4) PATH 'monOrgDebitAmt',
						numTaxItemID NUMERIC(9,0) PATH 'numTaxItemID',
						numContactID NUMERIC(9,0) PATH 'numContactID',
						numItemID NUMERIC(9,0) PATH 'numItemID',
						numProjectID NUMERIC(9,0) PATH 'numProjectID',
						numClassID NUMERIC(9,0) PATH 'numClassID',
						numCommissionID NUMERIC(9,0) PATH 'numCommissionID'
				) AS X
				WHERE
					COALESCE(TransactionId,0) = 0;                                                                                              

				Update 
					OpportunityBizDocsDetails 
				Set 
					bitIntegratedToAcnt = true
					,dtModifiedDate = TIMEZONE('UTC',now())
					,tintPaymentType = 2 
				Where 
					numBizDocsPaymentDetId in (Select numBizDocsPaymentDetId From XMLTABLE
				(
					'NewDataSet/Table1'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						TransactionId NUMERIC(18,0) PATH 'TransactionId',
						numBizDocsPaymentDetId NUMERIC(9,0) PATH 'numBizDocsPaymentDetId'
				) AS X WHERE COALESCE(TransactionId,0) = 0);
         end if;     
	                                                                        
      end if;
   end if;                
---For Recurring Transaction                                              
                                              
   if v_RecurringMode = 1 then
                                            
/*Commented by chintan Opening balance Will be updated by Trigger*/   
--   Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                                
      select   max(numJOurnal_Id) INTO v_numMaxJournalId From General_Journal_Header;
      insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,numBalance,bitMainCheck,bitMainCashCredit)
      Select v_numMaxJournalId,CAST(numDebitAmt AS DOUBLE PRECISION),CAST(numCreditAmt AS DOUBLE PRECISION),numChartAcntId,varDescription,numCustomerId,numDomainId,numBalance,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId = v_numJournalId;
   end if;                                          

                                          
/****** Object:  StoredProcedure [dbo].[USP_SaveLayoutList]    Script Date: 07/26/2008 16:21:06 ******/
   RETURN;
END; $$;


