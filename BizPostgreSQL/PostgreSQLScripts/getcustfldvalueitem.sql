DROP FUNCTION IF EXISTS GetCustFldValueItem;

CREATE OR REPLACE FUNCTION GetCustFldValueItem(v_numFldId NUMERIC(18,0),
	v_numRecordId NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT DEFAULT '';
   v_fld_Type  VARCHAR(50);
   v_numListID  NUMERIC(18,0);
BEGIN
   select   numlistid, fld_type INTO v_numListID,v_fld_Type FROM
   CFW_Fld_Master WHERE
   Fld_id = v_numFldId;
       
   select   coalesce(Fld_Value,'') INTO v_vcValue FROM
   CFW_FLD_Values_Item WHERE
   Fld_ID = v_numFldId
   AND RecId = v_numRecordId;

   IF (v_fld_Type = 'TextBox' OR v_fld_Type = 'TextArea') then
    
      IF v_vcValue = '' then 
         v_vcValue := '-';
      end if;
   ELSEIF v_fld_Type = 'SelectBox'
   then
    
      IF v_vcValue = '' then
         v_vcValue := '-';
      ELSEIF swf_isnumeric(v_vcValue) THEN
         v_vcValue := GetListIemName(CAST(v_vcValue AS NUMERIC));
      end if;
   ELSEIF v_fld_Type = 'CheckBox'
   then
	
      v_vcValue :=(CASE WHEN cast(NULLIF(v_vcValue,'') as INTEGER) = 0 THEN 'No' ELSE 'Yes' END);
   ELSEIF v_fld_Type = 'CheckBoxList'
   then
	
      v_vcValue := COALESCE((SELECT string_agg(vcData,',')
      FROM
      Listdetails
      WHERE
      numListItemID IN (SELECT (CASE WHEN SWF_isnumeric(Id) = true THEN coalesce(CAST(Id AS NUMERIC),0)  ELSE 0 END) FROM regexp_split_to_table(v_vcValue,',') AS Id)),'');
   end if;

   IF v_vcValue IS NULL then 
      v_vcValue := '';
   end if;

   RETURN v_vcValue;
END; $$;

