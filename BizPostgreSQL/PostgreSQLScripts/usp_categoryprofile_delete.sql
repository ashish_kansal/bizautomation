-- Stored procedure definition script USP_CategoryProfile_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CategoryProfile_Delete(v_ID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numItemID FROM ItemCategory WHERE numCategoryID IN(SELECT numCategoryID FROM Category WHERE numDomainID = v_numDomainID AND numCategoryProfileID = v_ID)) then
	
      RAISE EXCEPTION 'ITEMS_EXITS';
      RETURN;
   ELSE
      DELETE FROM SiteCategories WHERE numCategoryID IN(SELECT coalesce(numCategoryID,0) FROM Category WHERE numCategoryProfileID = v_ID);
      DELETE FROM Category WHERE numCategoryProfileID = v_ID;
      DELETE FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID;
      DELETE FROM CategoryProfile WHERE ID = v_ID;
   end if;
END; $$;



