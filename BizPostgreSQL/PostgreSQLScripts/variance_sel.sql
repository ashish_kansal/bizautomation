CREATE OR REPLACE FUNCTION Variance_Sel (
 p_StartDateTimeUtc TIMESTAMP, -- the start date before which no variances are retrieved  
 p_EndDateTimeUtc TIMESTAMP, -- the end date, any variances starting after this date are excluded  
 p_OrganizerName VARCHAR(200), -- resource name (will be used to lookup ID)  
 p_RecurrenceKey integer, -- required to identify the recurring series  
 p_VarianceKey  Varchar   -- required to identify correlated Activity rows that make up a recurring series' variances  
, INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
   AS $$
   DECLARE p_ResourceID integer;
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
 
 
if swf_isnumeric(p_OrganizerName) = true THEN
   p_ResourceID := CAST(p_OrganizerName AS numeric(9));
else 
	p_ResourceID := -999;
end if;

 OPEN SWV_RefCur FOR 
 SELECT   
  Activity.AllDayEvent,   
  Activity.ActivityDescription,   
  Activity.Duration,   
  Activity.Location,   
  Activity.ActivityID,   
  Activity.StartDateTimeUtc,   
  Activity.Subject,   
  Activity.EnableReminder,   
  Activity.ReminderInterval,  
  Activity.ShowTimeAs,  
  Activity.Importance,  
  Activity.Status,  
  Activity.RecurrenceID,  
  Activity.VarianceID,  
  Activity.OriginalStartDateTimeUtc,  
  Activity._ts,  
  Activity.ItemId,  
  Activity.ChangeKey,COALESCE(Activity.Color,0) as Color,
 COALESCE(LastSnoozDateTimeUtc,'1900-01-01 00:00:00') AS LastSnoozDateTimeUtc    
 FROM   
  Activity INNER JOIN ActivityResource ON Activity.ActivityID = ActivityResource.ActivityID  
 WHERE   
  ((Activity.ActivityID = ActivityResource.ActivityID) AND  
   (ActivityResource.ResourceID = p_ResourceID)) AND   
   (( Activity.StartDateTimeUtc >= p_StartDateTimeUtc ) AND  
    ( Activity.StartDateTimeUtc < p_EndDateTimeUtc ) AND   
    ( Activity.RecurrenceID = p_RecurrenceKey ) AND  
    ( Activity.VarianceID::VARCHAR = p_VarianceKey ) AND  
    ( NOT ( Activity.OriginalStartDateTimeUtc IS NULL ) ) );  
RETURN;
END; $$;

