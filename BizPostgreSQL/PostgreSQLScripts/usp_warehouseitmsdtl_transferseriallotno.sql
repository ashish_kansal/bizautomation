-- Stored procedure definition script USP_WareHouseItmsDTL_TransferSerialLotNo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WareHouseItmsDTL_TransferSerialLotNo(v_numOppID NUMERIC(18,0),
v_numOppItemID NUMERIC(18,0),
v_numFormWarehouseID NUMERIC(18,0),
v_numToWarehouseID NUMERIC(18,0),
v_bitSerialized BOOLEAN,
v_bitLotNo BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ErrMsg  VARCHAR(4000);
   v_ErrSeverity  INTEGER;
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;


	--UPDATE TRANSFER COMPLETES SO USERS WILL BE ABLE TO USE SERIAL/LOT NUMBR OTHERWISE USER WILL NOT BE ABLE TO SELECT SERIAL/LOT NUMBR
   v_numWarehouseItmsDTLID  NUMERIC(18,0);
   v_vcSerialNo  VARCHAR(100);
   v_dExpiryDate  TIMESTAMP;
   v_bitAddedFromPO  BOOLEAN;
   v_numQty  INTEGER;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF v_bitSerialized = true then
	
		--TRANSFER SERIAL NUMBERS TO SELECTED WAREHOUSE
         UPDATE
         WareHouseItmsDTL
         SET
         numWareHouseItemID = v_numToWarehouseID,numQty = 1
         WHERE
         numWareHouseItmsDTLID IN(SELECT
         t1.numWarehouseItmsDTLID
         FROM
         OppWarehouseSerializedItem t1
         WHERE
         t1.numOppID = v_numOppID AND
         t1.numOppItemID = v_numOppItemID AND
         t1.numWarehouseItmsID = v_numFormWarehouseID);
      end if;
      IF v_bitLotNo = true then
         BEGIN
            CREATE TEMP SEQUENCE tt_TableLotNoSelected_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TABLELOTNOSELECTED CASCADE;
         CREATE TEMPORARY TABLE tt_TABLELOTNOSELECTED
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numWarehouseItmsDTLID NUMERIC(18,0),
            numOppID NUMERIC(18,0),
            numOppItemID NUMERIC(18,0),
            numWarehouseItmsID NUMERIC(18,0),
            numQty NUMERIC(18,0)
         );
         INSERT INTO
         tt_TABLELOTNOSELECTED(numWarehouseItmsDTLID, numOppID, numOppItemID, numWarehouseItmsID, numQty)
         SELECT
         numWarehouseItmsDTLID,
			numOppID,
			numOppItemID,
			numWarehouseItmsID,
			numQty
         FROM
         OppWarehouseSerializedItem
         WHERE
         numOppID = v_numOppID
         AND numOppItemID = v_numOppItemID
         AND numWarehouseItmsID = v_numFormWarehouseID;
         select   COUNT(*) INTO v_Count FROM tt_TABLELOTNOSELECTED;
         WHILE v_i <= v_Count LOOP
            select   numQty, numWarehouseItmsDTLID INTO v_numQty,v_numWarehouseItmsDTLID FROM tt_TABLELOTNOSELECTED WHERE ID = v_i;
            select   vcSerialNo, dExpirationDate, coalesce(bitAddedFromPO,0) INTO v_vcSerialNo,v_dExpiryDate,v_bitAddedFromPO FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWarehouseItmsDTLID;

			--IF SAME LOT NUMBER IS ALREADY AVAILABLE IN DESTINATION WAREHOUSE THEN UPDATE QUANITY ELSE CREATE NEW ENTRY
            IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numToWarehouseID AND vcSerialNo = v_vcSerialNo) then
			
               UPDATE
               WareHouseItmsDTL
               SET
               numQty = coalesce(numQty,0)+coalesce(v_numQty,0)
               WHERE
               numWareHouseItemID = v_numToWarehouseID AND vcSerialNo = v_vcSerialNo;
            ELSE
               INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
					vcSerialNo,
					numQty,
					dExpirationDate,
					bitAddedFromPO)
				VALUES(v_numToWarehouseID,
					v_vcSerialNo,
					v_numQty,
					v_dExpiryDate,
					v_bitAddedFromPO);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;


	--UPDATE TRANSFER COMPLETES SO USERS WILL BE ABLE TO USE SERIAL/LOT NUMBR OTHERWISE USER WILL NOT BE ABLE TO SELECT SERIAL/LOT NUMBR
      UPDATE OppWarehouseSerializedItem SET bitTransferComplete = true WHERE numOppID = v_numOppID AND numOppItemID = v_numOppItemID AND numWarehouseItmsID = v_numFormWarehouseID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
  -- Whoops, there was an error
         -- ROLLBACK


  -- Raise an error with the details of the exception
  v_ErrMsg := SQLERRM;
         v_ErrSeverity := ERROR_SEVERITY();
         RAISE NOTICE '%',v_ErrMsg;
   END;
   RETURN;
END; $$;



