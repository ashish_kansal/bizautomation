-- Stored procedure definition script USP_GetItemsForAPI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemsForAPI(v_numDomainID NUMERIC(9,0),
    v_numItemCode NUMERIC(9,0),
    v_bitMode BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitMode = false then  --select item for given itemcode
            
      open SWV_RefCur for
      SELECT  I.numItemCode,
                        I.vcItemName,
                        I.txtItemDesc,
                        I.charItemType,
                        coalesce((SELECT monWListPrice
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT  numDefaultWareHouseID
            FROM    eCommerceDTL
            WHERE   numDomainID = I.numDomainID)),I.monListPrice) AS monListPrice,
                        I.vcSKU,
                        --I.dtDateEntered,
                        I.bintCreatedDate,
                        (SELECT  vcPathForImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true AND II.bitIsImage = true LIMIT 1) AS vcPathForImage,
                        I.vcModelID,
                        I.numItemGroup,
                        (CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END) AS monAverageCost,
                        I.monCampaignLabourCost,
                        coalesce(I.fltWeight,0) AS fltWeight,
                        coalesce(I.fltHeight,0) AS fltHeight,
                        coalesce(I.fltWidth,0) AS fltWidth,
                        coalesce(I.fltLength,0) AS fltLength,
                        I.vcUnitofMeasure,
                        coalesce((SELECT numOnHand
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT  numDefaultWareHouseID
            FROM    eCommerceDTL
            WHERE   numDomainID = I.numDomainID)),0) AS QtyOnHand
      FROM    Item I
      WHERE   I.numDomainID = v_numDomainID
      AND I.numItemCode = v_numItemCode;
   ELSE
      open SWV_RefCur for
      SELECT  I.numItemCode,
                        I.vcItemName,
                        I.txtItemDesc,
                        I.charItemType,
                        coalesce((SELECT monWListPrice
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT  numDefaultWareHouseID
            FROM    eCommerceDTL
            WHERE   numDomainID = I.numDomainID LIMIT 1) LIMIT 1),I.monListPrice) AS monListPrice,
                        I.vcSKU,
                        --I.dtDateEntered,
                        I.bintCreatedDate,
                        (SELECT  vcPathForImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true AND II.bitIsImage = true LIMIT 1) AS vcPathForImage,
                        I.vcModelID,
                        I.numItemGroup,
                        (CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE I.monAverageCost END) AS monAverageCost,
                        I.monCampaignLabourCost,
                        coalesce(I.fltWeight,0) AS fltWeight,
                        coalesce(I.fltHeight,0) AS fltHeight,
                        coalesce(I.fltWidth,0) AS fltWidth,
                        coalesce(I.fltLength,0) AS fltLength,
                        I.vcUnitofMeasure,
                        coalesce((SELECT numOnHand
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT  numDefaultWareHouseID
            FROM    eCommerceDTL
            WHERE   numDomainID = I.numDomainID LIMIT 1) LIMIT 1),0) AS QtyOnHand
      FROM    Item I
      WHERE   I.numDomainID = v_numDomainID
      AND I.numItemCode > v_numItemCode;
   end if;
   RETURN;
END; $$;



