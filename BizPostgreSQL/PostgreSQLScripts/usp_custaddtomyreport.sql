-- Stored procedure definition script Usp_CustAddToMyReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_CustAddToMyReport(v_numReportId NUMERIC(9,0),
v_numUserCntId NUMERIC(9,0),
v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintRptSequence  SMALLINT;
BEGIN
   select   coalesce(max(tintRptSequence),0) INTO v_tintRptSequence from MyReportList where  numUserCntID = v_numUserCntId;    
   v_tintRptSequence := v_tintRptSequence+1;    
   delete from MyReportList where numRPTId = v_numReportId and numUserCntID = v_numUserCntId  and tintType = 1;  
   insert into MyReportList(numRPTId,numUserCntID,tintRptSequence,tintType)
values(v_numReportId,v_numUserCntId,v_tintRptSequence,1);
RETURN;
END; $$;


