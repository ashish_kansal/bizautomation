-- Stored procedure definition script USP_GetMasterListItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMasterListItems(v_ListID NUMERIC(9,0) DEFAULT 0,        
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bit3PL  BOOLEAN;
   v_bitEDI  BOOLEAN;
BEGIN
   select   coalesce(bit3PL,false), coalesce(bitEDI,false) INTO v_bit3PL,v_bitEDI FROM Domain WHERE numDomainId = v_numDomainID;

   open SWV_RefCur for SELECT
		Ld.numListItemID AS "numListItemID"
		,coalesce(LDN.vcName,coalesce(vcRenamedListName,vcData)) AS "vcData"
		,coalesce(Ld.numListType,0) AS "numListType"
		,coalesce(Ld.constFlag,false) AS "constFlag"
   FROM
   Listdetails Ld
   LEFT JOIN
   listorder LO
   ON
   Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   LEFT JOIN
   ListDetailsName LDN
   ON
   LDN.numDomainID = v_numDomainID
   AND LDN.numListID = v_ListID
   AND LDN.numListItemID = Ld.numListItemID
   WHERE
   Ld.numListID = v_ListID
   AND 1 =(CASE
   WHEN Ld.numListItemID IN(15445,15446) THEN(CASE WHEN v_bit3PL = true THEN 1 ELSE 0 END)
   WHEN Ld.numListItemID IN(15447,15448) THEN(CASE WHEN v_bitEDI = true THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND (constFlag = true OR Ld.numDomainid = v_numDomainID)
   ORDER BY
   coalesce(intSortOrder,Ld.sintOrder);
END; $$;












