-- Stored procedure definition script USP_UpdateWarehouseLocation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE OR REPLACE FUNCTION USP_UpdateWarehouseLocation(v_numDomainID NUMERIC(9,0) ,
    v_numWLocationID NUMERIC(9,0) DEFAULT 0,
	v_vcAisle VARCHAR(500) DEFAULT '',
	v_vcRack VARCHAR(500) DEFAULT '',
	v_vcShelf VARCHAR(500) DEFAULT '',
	v_vcBin VARCHAR(500) DEFAULT '',
	v_vcLocation VARCHAR(500) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(coalesce(v_vcLocation,'')) = 0 then
	
      v_vcLocation := coalesce(v_vcAisle,'') || ',' || coalesce(v_vcRack,'') || ',' || coalesce(v_vcShelf,'') || ',' || coalesce(v_vcBin,'');
      v_vcLocation := Replace(v_vcLocation,',,,',',');
      v_vcLocation := Replace(v_vcLocation,',,',',');
      v_vcLocation := REPLACE(LTRIM(RTRIM(REPLACE(v_vcLocation,',',' '))),' ',',');
   end if;

   UPDATE
   WarehouseLocation
   SET
   vcAisle = v_vcAisle,vcRack = v_vcRack,vcShelf = v_vcShelf,vcBin = v_vcBin,vcLocation = v_vcLocation
   WHERE
   numDomainID = v_numDomainID AND
   numWLocationID = v_numWLocationID;

   UPDATE
   WareHouseItems
   SET
   vcLocation = v_vcLocation
   WHERE
   numDomainID = v_numDomainID AND
   numWLocationID = v_numWLocationID;
   RETURN;
END; $$;


