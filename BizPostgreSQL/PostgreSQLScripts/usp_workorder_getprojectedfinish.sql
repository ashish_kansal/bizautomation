CREATE OR REPLACE FUNCTION USP_WorkOrder_GetProjectedFinish(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)   
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT GetProjectedFinish(v_numDomainID,2::SMALLINT,v_numWOID,0,0,NULL,v_ClientTimeZoneOffset,0);
END; $$;

