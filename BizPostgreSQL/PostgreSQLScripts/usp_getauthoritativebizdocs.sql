-- Stored procedure definition script USP_GetAuthoritativeBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAuthoritativeBizDocs(v_numDomainID NUMERIC(9,0) DEFAULT 0,        
v_numListID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select count(*) from AuthoritativeBizDocs
   where  numDomainId = v_numDomainID;
END; $$;












