CREATE OR REPLACE FUNCTION USP_GetOrderListInEcommerce(v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
 v_tintOppType SMALLINT DEFAULT 1,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,
 v_CurrentPage INTEGER DEFAULT NULL,
 v_PageSize INTEGER DEFAULT NULL,
 v_tintShipped INTEGER DEFAULT 0,
 v_tintOppStatus INTEGER DEFAULT 0,
 v_UserId NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER DEFAULT 0;
   v_lastRec  INTEGER DEFAULT 0;
   v_ClientTimeZoneOffset  INTEGER;
BEGIN
   v_ClientTimeZoneOffset := -330;
   IF (v_tintOppType = 1 OR  v_tintOppType = 2) then

      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
	
      open SWV_RefCur for
      SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY numoppid) AS Rownumber,* FROM(SELECT distinct OM.numOppId,OM.vcpOppName,(SELECT SUBSTR((SELECT '$^$' || CAST(numOppBizDocsId AS VARCHAR(18)) || '#^#' || vcBizDocID
                  FROM OpportunityBizDocs WHERE numoppid = OM.numOppId AND numBizDocId = 287),4,200000)) AS vcBizDoc,
		(SELECT SUBSTR((SELECT '$^$' || CAST(vcTrackingNo AS VARCHAR(18)) || '#^#' || vcTrackingURL
                  FROM OpportunityBizDocs WHERE numoppid = OM.numOppId),
               4,200000)) AS vcShipping,
		(coalesce(GetDealAmount(OM.numOppId,LOCALTIMESTAMP,0::NUMERIC),0)*OM.fltExchangeRate) as monDealAmount,
		(coalesce(GetPaidAmount(OM.numOppId),0)*OM.fltExchangeRate) AS monAmountPaid,
		(coalesce(GetDealAmount(OM.numOppId,LOCALTIMESTAMP,0::NUMERIC),0)*OM.fltExchangeRate)
            -(coalesce(GetPaidAmount(OM.numOppId),0)*OM.fltExchangeRate) AS BalanceDue,
		(select   vcData from Listdetails where numListItemID =(SELECT  numShipVia FROM OpportunityBizDocs WHERE numoppid = OM.numOppId AND numShipVia > 0 LIMIT 1) LIMIT 1) AS vcShippingMethod,
		(select   vcData from Listdetails where numListItemID =(SELECT  intUsedShippingCompany FROM OpportunityMaster where numOppId = OM.numOppId LIMIT 1) LIMIT 1) AS vcMasterShippingMethod,
		(SELECT  intUsedShippingCompany FROM OpportunityMaster where numOppId = OM.numOppId LIMIT 1) AS numOppMasterShipVia,
		(SELECT  numShipVia FROM OpportunityBizDocs WHERE numoppid = OM.numOppId  AND numShipVia > 0 LIMIT 1) AS numShipVia,
		(SELECT  vcTrackingNo FROM OpportunityBizDocs WHERE numoppid = OM.numOppId  AND numShipVia > 0 LIMIT 1) AS vcTrackingNo,
		(SELECT  numShippingReportID from ShippingReport WHERE numOppBizDocId =(SELECT  numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = OM.numOppId  AND numShipVia > 0 LIMIT 1) LIMIT 1) AS numShippingReportId,
		(SELECT  numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = OM.numOppId  AND numShipVia > 0 LIMIT 1) AS numShippingBizDocId,
		FormatedDateFromDate((SELECT  dtDeliveryDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId  AND numShipVia > 0 LIMIT 1),OM.numDomainId) AS DeliveryDate,
		FormatedDateFromDate((SELECT  dtFromDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId order by dtFromDate desc LIMIT 1),OM.numDomainId) AS BillingDate,
		FormatedDateFromDate(CASE WHEN (SELECT dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc LIMIT 1) IS NULL THEN OM.bintCreatedDate ELSE (SELECT dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc LIMIT 1) END ,OM.numDomainId) AS BillingDate,
		CASE WHEN(SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numoppid = OM.numOppId) = 1 THEN(CASE coalesce(OM.bitBillingTerms,false)
               WHEN true THEN FormatedDateFromDate((SELECT  dtFromDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId order by dtFromDate desc LIMIT 1)+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) AS INTEGER) || 'day' as interval),OM.numDomainId)
               WHEN false THEN FormatedDateFromDate((SELECT  dtFromDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId order by dtFromDate desc LIMIT 1),OM.numDomainId)
               END)
            ELSE 'Multiple' END AS DueDate,
		CASE WHEN(SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numoppid = OM.numOppId) = 1 THEN(CASE coalesce(OM.bitBillingTerms,false)
               WHEN true THEN(SELECT  dtFromDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId order by dtFromDate desc LIMIT 1)+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) AS INTEGER) || 'day' as interval)
               WHEN false THEN(SELECT  dtFromDate FROM OpportunityBizDocs WHERE numoppid = OM.numOppId order by dtFromDate desc LIMIT 1)
               END) ELSE (SELECT dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc LIMIT 1) END AS DateDueDate,
		FormatedDateFromDate(OM.dtReleaseDate,OM.numDomainId) AS ReleaseDate,
		FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId) AS CreatedDate,
		(select  vcData from Listdetails WHERE numListItemID = OM.numStatus LIMIT 1) AS vcOrderStatus,
        GetCreditTerms(OM.numOppId) as Credit,
		fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CAST(OM.bintCreatedDate AS TIMESTAMP) AS  dtCreatedDate,
		CAST(OM.bintCreatedDate AS TIMESTAMP) AS  numCreatedDate,
		CASE WHEN(select  coalesce(numPaymentMethod,0) from General_Journal_Details WHERE numJournalId IN(select numJOurnal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numoppid = OM.numOppId)) LIMIT 1) = 0
            THEN(select  vcData from Listdetails WHERE numListItemID IN(select coalesce(numDefaultPaymentMethod,0) from DivisionMaster where numDivisionID = v_numDivisionID) LIMIT 1)
            ELSE(select  vcData from Listdetails WHERE numListItemID IN(select numPaymentMethod from General_Journal_Details WHERE numJournalId IN(select numJOurnal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numoppid = OM.numOppId))) LIMIT 1)
            END
            AS PaymentMethod,
		coalesce(GetDealAmount(OM.numOppId,LOCALTIMESTAMP,0::NUMERIC),0) as TotalAmt,
		OM.numContactId,
		coalesce(OM.tintoppstatus,0) As tintOppStatus,
		CASE
            WHEN CAST(OM.bintCreatedDate AS VARCHAR(11)) = CAST(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11))
            then CONCAT('<b><font color="#FF0000" style="font-size:14px">Today','</font></b>')
            WHEN CAST(OM.bintCreatedDate AS VARCHAR(11)) = CAST(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '1 day' AS VARCHAR(11))
            THEN CONCAT('<b><font color=#ED8F11 style="font-size:14px">Tommorow','</font></b>')
            WHEN OM.bintCreatedDate < TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '7 day'
            AND OM.bintCreatedDate > TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '1 day'
            then CONCAT('<b><font color=#8FAADC style="font-size:14px">',TO_CHAR(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Day'),'<span style="color:#000;font-weight:500"> - ',
               CAST(EXTRACT(DAY FROM OM.bintCreatedDate) AS VARCHAR(10)),
               CASE
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,100) IN(11,12,13) THEN 'th'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 1 THEN 'st'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 2 THEN 'nd'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 3 THEN 'rd'
               ELSE 'th' END,', ',TO_CHAR(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval), 'Mon'),' ',EXTRACT(YEAR FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)),'</span>','</font></b>','<b>','<br/>',
               '<Span style="font-size:14px;font-weight:normal;font-style: italic;">','</span>') ELSE
               CONCAT('<b><font color=#8FAADC style="font-size:14px">',TO_CHAR(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Day'),'<span style="color:#000;font-weight:500"> - ',
               CAST(EXTRACT(DAY FROM OM.bintCreatedDate) AS VARCHAR(10)),
               CASE
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,100) IN(11,12,13) THEN 'th'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 1 THEN 'st'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 2 THEN 'nd'
               WHEN MOD(EXTRACT(DAY FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,10) = 3 THEN 'rd'
               ELSE 'th' END,', ',TO_CHAR(OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval), 'Mon'),' ',EXTRACT(YEAR FROM OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)),'</span>','</font></b>','<b>','<br/>',
               '<Span style="font-size:14px;font-weight:normal;font-style: italic;">','</span>')
            END AS FormattedCreatedDate,
"vcCustomerPO#"
            FROM OpportunityMaster OM
            LEFT JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
            LEFT JOIN DivisionMaster D ON D.numDivisionID = OM.numDivisionId
            LEFT JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
            WHERE OM.numDomainId = v_numDomainID AND OM.tintopptype = v_tintOppType
            AND D.numDivisionID = v_numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
            AND OM.tintoppstatus = v_tintOppStatus  AND OM.numContactId = v_UserId
            AND 1 =(CASE WHEN v_tintOppStatus = 1 AND OM.tintshipped = v_tintShipped THEN 1 WHEN v_tintOppStatus = 0 THEN 1 ELSE 0 END)) AS K) AS T
      WHERE T.Rownumber > v_firstRec AND T.Rownumber < v_lastRec;
      open SWV_RefCur2 for
      SELECT DISTINCT COUNT(OM.numOppId) FROM OpportunityMaster OM
      LEFT JOIN DivisionMaster D ON D.numDivisionID = OM.numDivisionId
      LEFT JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
      WHERE OM.numDomainId = v_numDomainID AND OM.tintopptype = v_tintOppType
      AND D.numDivisionID = v_numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
      AND OM.tintoppstatus = v_tintOppStatus  AND OM.numContactId = v_UserId
      AND 1 =(CASE WHEN v_tintOppStatus = 1 AND OM.tintshipped = v_tintShipped THEN 1 WHEN v_tintOppStatus = 0 THEN 1 ELSE 0 END);
   end if; 
--For bills
   IF v_tintOppType =  3 then
      open SWV_RefCur for
      SELECT 0 AS numOppId,'Bill' AS vcPOppName,0 AS numOppBizDocsId,'Bill' || CASE WHEN LENGTH(BH.vcReference) = 0 THEN '' ELSE '-' || BH.vcReference END AS vcBizDocID,
						   BH.monAmountDue as monDealAmount,
						   coalesce(BH.monAmtPaid,0) as monAmountPaid,
					       coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) as BalanceDue,
					       FormatedDateFromDate(BH.dtBillDate,BH.numDomainId) AS BillingDate,
						   FormatedDateFromDate(BH.dtDueDate,BH.numDomainId) AS DueDate
      FROM
      BillHeader BH
      WHERE BH.numDomainId = v_numDomainID AND BH.numDivisionId = v_numDivisionID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0;
   end if;
   RETURN;
END; $$;




