-- Stored procedure definition script USP_GetVendorCostTable for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetVendorCostTable(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemcode NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intRowNum  INTEGER;
   v_Vendor  VARCHAR(100);
   v_ItemCode  NUMERIC(9,0);
   v_numVendorId  NUMERIC(9,0);                                                 
   v_strSQLUpdate  VARCHAR(2000);

   v_MaxTier  INTEGER;
   SWV_ExecDyn  VARCHAR(5000);
   SWV_RowCount INTEGER;
BEGIN
   drop table IF EXISTS tt_TEMPVENDORS CASCADE;
   Create TEMPORARY TABLE tt_TEMPVENDORS
   (
      numItemCode NUMERIC(9,0),
      numCompanyId NUMERIC(9,0),
      numVendorId NUMERIC(9,0),
      Vendor VARCHAR(100),
      monCost DECIMAL(20,5),
      intRowNum INTEGER
   );

   insert into tt_TEMPVENDORS
   select I.numItemCode,div.numCompanyID,Vendor.numVendorID,SUBSTR(vcCompanyName,0,30) as Vendor,
coalesce(monCost,0) AS monCost,row_number() Over(order by Vendor.numVendorID) as intRowNum
   from Vendor
   join DivisionMaster div
   on div.numDivisionID = numVendorid
   join CompanyInfo com
   on com.numCompanyId = div.numCompanyID
   join Item I
   on I.numItemCode = Vendor.numItemCode
   WHERE Vendor.numDomainID = v_numDomainID and Vendor.numItemCode = v_numItemcode;


   drop table IF EXISTS tt_TEMPDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDATA ON COMMIT DROP AS
      SELECT t.numItemCode,t.numVendorid,PT.intFromQty,PT.intToQty,PT.tintDiscountType, CASE PT.tintDiscountType
      WHEN 1
      THEN CAST(CAST(PT.decDiscount AS DECIMAL(18,2)) AS VARCHAR(30))
      ELSE CAST(CAST(PT.decDiscount AS INTEGER) AS VARCHAR(30))
      END AS decDiscount,
		Case when t.monCost > 0 then
         Case PT.tintDiscountType when 1 then t.monCost -(PT.decDiscount*t.monCost/100)
         when 2 then t.monCost -PT.decDiscount
         else 0 end
      else 0 end as monCost,row_number() Over(partition by t.numItemCode,t.numVendorid order by PT.intFromQty,PT.intToQty) as Tier
		
      FROM  Item I
      LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
      LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
      LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
      LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2
      AND PP.Step3Value = P.tintStep3
      Left join PricingTable PT on PT.numPriceRuleID = P.numPricRuleID AND coalesce(PT.numCurrencyID,0) = 0,tt_TEMPVENDORS t
      WHERE   I.numItemCode = t.numItemCode AND tintRuleFor = 2
      AND ((tintStep2 = 1 AND PBI.numValue = t.numItemCode)
      AND (tintStep3 = 1 AND PDTL.numValue = t.numVendorid));


   drop table IF EXISTS tt_TEMPVT CASCADE;
   Create TEMPORARY TABLE tt_TEMPVT 
   (
      iTier INTEGER,
      "0~Tier" VARCHAR(20)
   );

   select   max(Tier) INTO v_MaxTier from tt_TEMPDATA;
   insert into tt_TEMPVT(iTier) with recursive cte AS(SELECT CAST(0 AS INTEGER) AS x
   UNION ALL
   SELECT x+1
   FROM cte
   WHERE x < v_MaxTier) select x from cte;

   select   intRowNum, Vendor, numItemCode, numVendorid INTO v_intRowNum,v_Vendor,v_ItemCode,v_numVendorId from tt_TEMPVENDORS    order by intRowNum LIMIT 1;                                                                                          
   while v_intRowNum > 0 LOOP
      SWV_ExecDyn := 'alter table tt_TEMPVT add "' || coalesce(v_numVendorId,0) || '~' || coalesce(v_Vendor,'') || '" varchar(100)';
      EXECUTE SWV_ExecDyn;
      v_strSQLUpdate := 'update tt_TEMPVT VT set "' || SUBSTR(Cast(v_numVendorId as VARCHAR(20)),1,20) || '~' || coalesce(v_Vendor,'') || '" = 
							(CAST(CAST(V.monCost AS DECIMAL(9,2)) AS VARCHAR(30))) from tt_TEMPVENDORS V 
							where VT.iTier = 0 AND(V.numItemCode =' || SUBSTR(Cast(v_ItemCode as VARCHAR(20)),1,20) || ' and V.numVendorId =' || SUBSTR(Cast(v_numVendorId as VARCHAR(20)),1,20) || ') ';
      EXECUTE v_strSQLUpdate;
      v_strSQLUpdate := 'update tt_TEMPVT VT set "' || SUBSTR(Cast(v_numVendorId as VARCHAR(20)),1,20) || '~' || coalesce(v_Vendor,'') || '" = 
							(SUBSTR(Cast(D.intFromQty as VARCHAR(20)),1,20) || '' - '' || SUBSTR(Cast(D.intToQty as VARCHAR(20)),1,20) || '' ('' || CAST(CAST(D.monCost AS DECIMAL(9,2)) AS VARCHAR(30)) || '')'')
							from tt_TEMPDATA D 
							where VT.iTier = D.Tier AND(D.numItemCode =' || SUBSTR(Cast(v_ItemCode as VARCHAR(20)),1,20) || ' and D.numVendorId =' || SUBSTR(Cast(v_numVendorId as VARCHAR(20)),1,20) || ') ';
      EXECUTE v_strSQLUpdate;
      select   intRowNum, Vendor, numItemCode, numVendorid INTO v_intRowNum,v_Vendor,v_ItemCode,v_numVendorId from tt_TEMPVENDORS where intRowNum > v_intRowNum   order by intRowNum LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_intRowNum := 0;
      end if;
   END LOOP;

   update tt_TEMPVT set "0~Tier" = 'Tier ' || SUBSTR(Cast(iTier as VARCHAR(20)),1,20) where iTier > 0;
   update tt_TEMPVT set "0~Tier" = 'Base'  where iTier = 0;

   ALTER TABLE tt_TEMPVT DROP COLUMN iTier;

--select * from #tempVendors
--select * from #tempData
   open SWV_RefCur for select * from tt_TEMPVT;

   drop table IF EXISTS tt_TEMPVENDORS CASCADE;
   drop table IF EXISTS tt_TEMPDATA CASCADE;

   RETURN;
END; $$;












