-- Stored procedure definition script USP_GetFinancialReportDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetFinancialReportDetail(v_numFRDtlID NUMERIC(18,0),
    v_numFRID NUMERIC(18,0),
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
 
/*SELECT * FROM dbo.ListDetails WHERE  numListID=387*/
   open SWV_RefCur for SELECT  numFRDtlID,
        FRD.numFRID,
        numValue1,
        numValue2,
        vcValue1,
        FR.numFinancialDimensionID,
        CASE  GetListIemName(FR.numFinancialDimensionID)
   WHEN 'By Class' THEN GetListIemName(numValue1)
   WHEN 'By Country' THEN GetListIemName(numValue1)
   WHEN 'By Employee' THEN(SELECT vcUserName FROM UserMaster WHERE numUserDetailId = numValue1)
   WHEN 'By Item' THEN coalesce((SELECT vcItemName FROM Item WHERE numItemCode = numValue1),'')
   WHEN 'By Item Class' THEN GetListIemName(numValue1)
   WHEN 'By Lot#' THEN FRD.vcValue1
   WHEN 'By Project' THEN coalesce((SELECT vcProjectName FROM ProjectsMaster WHERE numProId = numValue1),'')
   WHEN 'By Project Type' THEN GetListIemName(numValue1)
   WHEN 'By Relationship / Profile' THEN GetListIemName(numValue1)
   WHEN 'By State / Province' THEN coalesce((SELECT vcState FROM State WHERE numStateID = numValue2),'')
   WHEN 'By Team' THEN GetListIemName(numValue1)
   WHEN 'By Territory' THEN GetListIemName(numValue1)
   ELSE ''
   END AS Value1,
        CASE GetListIemName(FR.numFinancialDimensionID)
   WHEN 'By Class' THEN GetListIemName(numValue2)
   WHEN 'By Country' THEN GetListIemName(numValue2)
   WHEN 'By Item' THEN coalesce((SELECT vcItemName FROM Item WHERE numItemCode = numValue2),'')
   WHEN 'By Item Class' THEN GetListIemName(numValue2)
   WHEN 'By Lot#' THEN FRD.vcValue1
   WHEN 'By Project' THEN coalesce((SELECT vcProjectName FROM ProjectsMaster WHERE numProId = numValue2),'')
   WHEN 'By Project Type' THEN GetListIemName(numValue2)
   WHEN 'By Relationship / Profile' THEN GetListIemName(numValue2)
   WHEN 'By State / Province' THEN coalesce((SELECT vcState FROM State WHERE numStateID = numValue2),'')
   WHEN 'By Team' THEN GetListIemName(numValue2)
   WHEN 'By Territory' THEN GetListIemName(numValue2)
   ELSE ''
   END AS Value2
   FROM    FinancialReportDetail FRD
   INNER JOIN FinancialReport FR
   ON FRD.numFRID = FR.numFRID
   WHERE   (FRD.numFRDtlID = v_numFRDtlID
   OR v_numFRDtlID = 0)
   AND FR.numFRID = v_numFRID
   AND FR.numDomainID = v_numDomainID;
END; $$;


	












