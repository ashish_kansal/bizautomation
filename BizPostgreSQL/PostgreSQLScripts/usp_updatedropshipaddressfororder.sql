-- Stored procedure definition script USP_UpdateDropShipAddressForOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateDropShipAddressForOrder(v_numOppID NUMERIC(9,0) DEFAULT 0,      
v_tintShipToType SMALLINT DEFAULT NULL,                       
v_vcShipStreet VARCHAR(50) DEFAULT NULL,                  
v_vcShipCity VARCHAR(50) DEFAULT NULL,                  
v_numShipState NUMERIC(9,0) DEFAULT NULL,                  
v_vcShipPostCode VARCHAR(15) DEFAULT NULL,                  
v_numShipCountry NUMERIC(9,0) DEFAULT NULL, 
v_bitAltShippingContact BOOLEAN DEFAULT false,
v_vcAltShippingContact VARCHAR(200) DEFAULT '',
v_numShipToAddressID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_tintOppType  SMALLINT;
BEGIN
   UPDATE
   OpportunityMaster
   SET
   tintShipToType = v_tintShipToType,numShipToAddressID = v_numShipToAddressID
   WHERE
   numOppId = v_numOppID;  	           
	        
   IF (v_tintShipToType = 2  AND coalesce(v_numShipToAddressID,0) = 0) then
	
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) = 0 then
		
         INSERT  INTO OpportunityAddress(numOppID)
			VALUES(v_numOppID);
      end if;
   end if;	
    
   IF v_tintShipToType = 2 AND coalesce(v_numShipToAddressID,0) = 0 then
	
      UPDATE  OpportunityAddress
      SET    	vcShipStreet = v_vcShipStreet,vcShipCity = v_vcShipCity,numShipState = v_numShipState,
      vcShipPostCode = v_vcShipPostCode,numShipCountry = v_numShipCountry,
      bitAltShippingContact = v_bitAltShippingContact,vcAltShippingContact = v_vcAltShippingContact
      WHERE   numOppID = v_numOppID;
   end if;

   select   numDomainId, numDivisionId, tintopptype INTO v_numDomainID,v_numDivisionID,v_tintOppType FROM
   OpportunityMaster WHERE
   numOppId = v_numOppID;

   DELETE FROM OpportunityMasterTaxItems WHERE numOppId = v_numOppID;

	--INSERT TAX FOR DIVISION   
   IF v_tintOppType = 1 OR (v_tintOppType = 2 and(select coalesce(bitPurchaseTaxCredit,false) from Domain where numDomainId = v_numDomainID) = true) then
	
      INSERT INTO OpportunityMasterTaxItems(numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID)
      SELECT
      v_numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
      FROM
      TaxItems TI
      JOIN
      DivisionTaxTypes DTT
      ON
      TI.numTaxItemID = DTT.numTaxItemID
      CROSS JOIN LATERAL(SELECT
         decTaxValue,
				tintTaxType
         FROM
         fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,TI.numTaxItemID,v_numOppID,false,NULL)) AS TEMPTax
      WHERE
      DTT.numDivisionID = v_numDivisionID
      AND DTT.bitApplicable = true
      UNION
      SELECT
      v_numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
      FROM
      DivisionMaster
      CROSS JOIN LATERAL(SELECT
         decTaxValue,
				tintTaxType
         FROM
         fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,0,v_numOppID,true,NULL)) AS TEMPTax
      WHERE
      bitNoTax = false
      AND numDivisionID = v_numDivisionID
      UNION
      SELECT
      v_numOppID
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
      FROM
      fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,1,v_numOppID,true,NULL);
   end if;
   RETURN;
END; $$;


