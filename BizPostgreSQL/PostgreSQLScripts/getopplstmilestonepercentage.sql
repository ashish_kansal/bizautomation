-- Function definition script GetOppLstMileStonePercentage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppLstMileStonePercentage(v_numoppid NUMERIC(9,0))
RETURNS VARCHAR(15) LANGUAGE plpgsql
/*Don't know exact purpose of old function so changin it to show total progress of Business process */
   AS $$
   DECLARE
   v_vcPercentage  VARCHAR(15);
BEGIN
   select   intTotalProgress INTO v_vcPercentage FROM ProjectProgress WHERE numOppId = v_numoppid;
   IF v_vcPercentage IS NULL then
      v_vcPercentage := CAST(0 AS VARCHAR(15));
   end if;
   RETURN v_vcPercentage;
END; $$;

