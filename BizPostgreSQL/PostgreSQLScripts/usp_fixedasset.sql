-- Stored procedure definition script USP_FixedAsset for PostgreSQL
CREATE OR REPLACE FUNCTION USP_FixedAsset(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	Select 
		LD.numListItemID as "numListItemID"
		,LD.vcData as "AssetGroup"
		,coalesce(FAD.monCost,0) as "Cost"
		,coalesce(FAD.monCurrentValue,0) as "CurrentValue"
		,coalesce(FAD.monFiscalDepOrAppAmt,0) as "Fiscal YTD Depreciation or Appreciation (Total Amount)"
	from 
		Listdetails LD
	Left outer join 
		FixedAssetDetails FAD on LD.numListItemID = FAD.numAssetClass
	Where numListId = 64;
END; $$;












