-- Stored procedure definition script USP_EmailHistory_MoveCopyIDs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistory_MoveCopyIDs(v_numEmailHstrID NUMERIC(18,0),
v_numNodeId NUMERIC(9,0),
v_ModeType BOOLEAN,
v_numUID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql          
--COPY
   AS $$
   DECLARE
   v_NewEmailHstrID  INTEGER;
BEGIN
   IF v_ModeType = true then
   

	--MAKE A COPY IN EmailHistory TABLE
      INSERT INTO EmailHistory(vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTo,numEMailId)
      SELECT
      vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,v_numUID ,v_numNodeId,vcFrom,vcTo,numEMailId
      FROM
      EmailHistory
      WHERE
      numEmailHstrID = v_numEmailHstrID;
      v_NewEmailHstrID := currval(pg_get_serial_sequence('EmailHistory','numemailhstrid'));              
        
		--MAKE A COPY IN EmailHStrToBCCAndCC TABLE
      INSERT INTO EmailHStrToBCCAndCC(numEmailHstrID,vcName,tintType,numDivisionId,numEmailId)
      SELECT
      v_NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId
      FROM
      EmailHStrToBCCAndCC
      WHERE
      numEmailHstrID = v_numEmailHstrID;              

		--MAKE A COPY IN EmailHstrAttchDtls TABLE                    
      INSERT INTO EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType)
      SELECT
      v_NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType
      FROM
      EmailHstrAttchDtls
      WHERE
      numEmailHstrID = v_numEmailHstrID;
--MOVE
   ELSE
      UPDATE EmailHistory SET numNodeId = v_numNodeId,numUid = v_numUID WHERE numEmailHstrID = v_numEmailHstrID;
   end if;
   RETURN;
END; $$;


