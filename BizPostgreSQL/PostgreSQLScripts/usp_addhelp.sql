-- Stored procedure definition script USP_AddHelp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddHelp(INOUT v_numHelpId NUMERIC(9,0) ,
    v_helpcategory VARCHAR(100),
    v_helpheader VARCHAR(400),
    v_helpMetatag VARCHAR(400),
    v_helpDescription TEXT,
    v_helpPageUrl VARCHAR(200),
    v_helpshortdesc VARCHAR(400),
    v_helpLinkingPageURL VARCHAR(1000))
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intDisplayOrder  INTEGER;
BEGIN
   select   coalesce(MAX(intDisplayOrder),0)+1 INTO v_intDisplayOrder FROM HelpMaster WHERE helpCategory = v_helpcategory;
   INSERT  INTO HelpMaster(helpCategory,
                  helpHeader,
                  helpMetatag,
                  helpDescription,
                  helpPageUrl,
                  helpshortdesc,helpLinkingPageURL,
                  intDisplayOrder)
        VALUES(v_helpcategory,
                  v_helpheader,
                  v_helpMetatag,
                  v_helpDescription,
                  v_helpPageUrl,
                  v_helpshortdesc,v_helpLinkingPageURL,
                  v_intDisplayOrder);      
      
        
   v_numHelpId := CURRVAL('HelpMaster_seq');
   RETURN;
END; $$; 


