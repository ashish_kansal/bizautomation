-- Stored procedure definition script USP_CheckProfileItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckProfileItem(v_numDomainId NUMERIC(18,0) DEFAULT null  ,
v_numItemCode NUMERIC(18,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemGroupID  NUMERIC(18,0);
BEGIN
   SELECT  numItemGroupID INTO v_numItemGroupID FROM ItemGroups WHERE numProfileItem = v_numItemCode AND numDomainID = v_numDomainId   order by numItemGroupID desc  LIMIT 1;
   IF(v_numItemGroupID > 0) then

      open SWV_RefCur for SELECT
      I.vcItemName || '~' || CAST(DTL.numOppAccAttrID AS VARCHAR(30)) AS numOppAccAttrID,cast(LT.vcData as VARCHAR(255))
      FROM
      ItemGroupsDTL AS DTL
      LEFT JOIN
      Item AS I
      ON
      DTL.numOppAccAttrID = I.numItemCode
      LEFT JOIN
      Listdetails AS LT
      ON
      DTL.numListid = LT.numListItemID
      WHERE
      DTL.numItemGroupID = v_numItemGroupID;
   end if;
END; $$;












