-- Function definition script SWF_NEWID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
drop sequence if exists SWS_Newid;
create sequence SWS_Newid;
create or replace function SWF_NEWID()
returns char(36)
AS $function$
DECLARE
vUid char(32);
vStr char(36);
begin
vUid := md5(cast(nextval('SWS_Newid') as text)||cast(localtimestamp as text));
vStr := substr(vUid,1,8)||'-'||substr(vUid,8,4)||'-'||substr(vUid,12,4)||'-'||substr(vUid,16,4)||'-'||substr(vUid,20,12);
RETURN UPPER(vStr);
END; $function$
LANGUAGE plpgsql;

