CREATE OR REPLACE FUNCTION USP_GetTaxAmountOppForTaxItem(v_numDomainID NUMERIC(9,0),    
v_numTaxItemID NUMERIC(9,0),    
v_numOppID NUMERIC(9,0),    
v_numDivisionID NUMERIC(9,0),
v_numOppBizDocID NUMERIC(9,0),
v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltPercentage  DOUBLE PRECISION DEFAULT 0;
   v_TotalTaxAmt  DECIMAL(20,5) DEFAULT 0;    
   v_numUnitHour  NUMERIC(18,0) DEFAULT 0;
   v_ItemAmount  DECIMAL(20,5);
BEGIN
   IF v_tintMode = 0 then --Order

      open SWV_RefCur for
      SELECT coalesce(fn_CalOppItemTotalTaxAmt(v_numDomainID,v_numTaxItemID,v_numOppID,v_numOppBizDocID),0);
   ELSEIF v_tintMode = 5 OR v_tintMode = 7 OR v_tintMode = 8 OR v_tintMode = 9
   then --RMA Return
      select   SUM(coalesce(CASE
      WHEN OMTI.tintTaxType = 2
      THEN
         coalesce(OMTI.fltPercentage,0)*coalesce((CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(RI.numUOMId,0)),0)
      ELSE
         CAST((coalesce(OMTI.fltPercentage,0)*(CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN coalesce(numUnitHour,0) ELSE coalesce(numUnitHourReceived,0) END)*(CASE
         WHEN coalesce(RI.fltDiscount,0) > 0 THEN
            CASE WHEN RI.bitDiscountType = true THEN monPrice -(RI.fltDiscount/RI.numUnitHour) ELSE	monPrice -(monPrice*(RI.fltDiscount/100)) END
         ELSE
            monPrice
         END)) AS decimal)/100 END,0)) INTO v_TotalTaxAmt FROM
      ReturnItems RI
      JOIN
      Item I
      ON
      RI.numItemCode = I.numItemCode
      JOIN
      OpportunityItemsTaxItems OITI
      ON
      RI.numReturnItemID = OITI.numReturnItemID
      AND RI.numReturnHeaderID = OITI.numReturnHeaderID
      JOIN
      OpportunityMasterTaxItems OMTI
      ON
      OITI.numReturnHeaderID = OMTI.numReturnHeaderID
      AND OITI.numTaxItemID = OMTI.numTaxItemID
      AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0) WHERE
      RI.numReturnHeaderID = v_numOppID
      AND OITI.numTaxItemID = v_numTaxItemID;
      open SWV_RefCur for
      SELECT coalesce(v_TotalTaxAmt,0) AS TotalTaxAmt;
   end if;
   RETURN;
END; $$;  
    


