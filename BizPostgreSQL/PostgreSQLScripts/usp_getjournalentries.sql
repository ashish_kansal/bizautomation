-- Stored procedure definition script Usp_GetJournalEntries for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_GetJournalEntries(v_numDomainID INTEGER ,
      v_vcAccountId VARCHAR(4000) ,
      v_dtFromDate TIMESTAMP ,
      v_dtToDate TIMESTAMP ,
      v_vcTranType VARCHAR(50) DEFAULT '' ,
      v_varDescription VARCHAR(50) DEFAULT '' ,
      v_CompanyName VARCHAR(50) DEFAULT '' ,
      v_vcBizPayment VARCHAR(50) DEFAULT '' ,
      v_vcCheqNo VARCHAR(50) DEFAULT '' ,
      v_vcBizDocID VARCHAR(50) DEFAULT '' ,
      v_vcTranRef VARCHAR(50) DEFAULT '' ,
      v_vcTranDesc VARCHAR(50) DEFAULT '' ,
      v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL ,
      v_charReconFilter CHAR(1) DEFAULT '' ,
      v_tintMode SMALLINT DEFAULT 0 ,
      v_numItemID NUMERIC(9,0) DEFAULT 0 ,
      v_CurrentPage INTEGER DEFAULT 0 ,
      v_PageSize INTEGER DEFAULT 0 ,
      INOUT v_TotRecs INTEGER DEFAULT 0  ,
      v_TransactionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAccountIDs  TEXT;
   v_iCnt  INTEGER;
   v_intDiff  INTEGER;
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   select   COALESCE(coalesce(v_vcAccountIDs,'') || ',','')
   || SUBSTR(CAST(COA.numAccountId AS VARCHAR(20)),1,20) INTO v_vcAccountIDs FROM    Chart_Of_Accounts AS COA WHERE   COA.numDomainId = v_numDomainID;
   RAISE NOTICE '%',v_vcAccountIDs;
   v_vcAccountId := v_vcAccountIDs;

		/*RollUp of Sub Accounts */
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      SELECT  COA2.numAccountId
        
      FROM    Chart_Of_Accounts COA1
      INNER JOIN Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
      AND COA2.vcAccountCode ilike COA1.vcAccountCode
      || '%'
      WHERE   COA1.numAccountId IN(SELECT  CAST(ID AS NUMERIC(9,0))
      FROM    SplitIDs(v_vcAccountId,','))
      AND COA1.numDomainId = v_numDomainID;
   v_vcAccountId := coalesce(OVERLAY((SELECT    ','
   || CAST(numAccountID AS VARCHAR(10))
   FROM      tt_TEMP) placing '' from 1 for 1),''); 
    
   open SWV_RefCur for
   SELECT  v_numDomainID AS numDomainID ,
                FormatedDateFromDate(dtAsOnDate,v_numDomainID::NUMERIC) AS Date ,
                * FROM    fn_GetOpeningBalance(v_vcAccountId,v_numDomainID,v_dtFromDate,v_ClientTimeZoneOffset);
		
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT  numDomainId ,
                numAccountId ,
                TransactionType ,
                CompanyName ,
                FormatedDateFromDate(datEntry_Date,numDomainId) AS Date ,
                varDescription ,
                coalesce(BizPayment,'') || ' ' || coalesce(CheqNo,'') AS Narration ,
                BizDocID ,
                TranRef ,
                TranDesc ,
                CAST(coalesce(numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt ,
                CAST(coalesce(numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt ,
                vcAccountName ,
                '' AS balance ,
                numJournal_Id AS JournalId ,
                numCheckHeaderID AS CheckId ,
                numCashCreditCardId AS CashCreditCardId ,
                numTransactionId ,
                numOppId ,
                numOppBizDocsId ,
                numDepositId ,
                numCategoryHDRID ,
                tintTEType ,
                numCategory ,
                dtFromDate ,
                numUserCntID ,
                bitReconcile ,
                bitCleared ,
                numBillID ,
                numBillPaymentID ,
                numReturnID ,
                ROW_NUMBER() OVER(ORDER BY datEntry_Date DESC) AS RowNumber
        
      FROM    VIEW_GENERALLEDGER
      WHERE   datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate
      AND (varDescription ilike coalesce(v_varDescription,'') || '%'
      OR LENGTH(v_varDescription) = 0)
      AND (BizPayment ilike coalesce(v_vcBizPayment,'') || '%'
      OR LENGTH(v_vcBizPayment) = 0)
      AND (CheqNo ilike coalesce(v_vcCheqNo,'') || '%'
      OR LENGTH(v_vcCheqNo) = 0)
      AND numDomainId = v_numDomainID
      AND (BizDocID ilike v_vcBizDocID
      OR LENGTH(v_vcBizDocID) = 0)
      AND (TranRef ilike v_vcTranRef
      OR LENGTH(v_vcTranRef) = 0)
      AND (TranDesc ilike v_vcTranDesc
      OR LENGTH(v_vcTranDesc) = 0)
      AND (TransactionType ilike coalesce(v_vcTranType,'') || '%'
      OR LENGTH(v_vcTranType) = 0)
      AND (bitCleared =(CASE v_charReconFilter
      WHEN 'C' THEN true
      ELSE false
      END)
      OR RTRIM(v_charReconFilter) = 'A')
      AND (bitReconcile =(CASE v_charReconFilter
      WHEN 'R' THEN true
      ELSE false
      END)
      OR RTRIM(v_charReconFilter) = 'A')
      AND (CompanyName ilike coalesce(v_CompanyName,'') || '%'
      OR LENGTH(v_CompanyName) = 0)
      AND (numDivisionID = v_numDivisionID
      OR v_numDivisionID = 0)
      AND (numAccountId IN(SELECT  numAccountID
      FROM    tt_TEMP)
      OR v_tintMode = 1)
      AND (numItemID = v_numItemID
      OR v_numItemID = 0)
      ORDER BY datEntry_Date ASC; 
         
   IF v_tintMode = 1 then
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTABLE;
      open SWV_RefCur2 for
      SELECT * FROM    tt_TEMPTABLE
      WHERE   ROWNUMBER > v_firstRec
      AND ROWNUMBER < v_lastRec
      ORDER BY ROWNUMBER;
   ELSEIF v_tintMode = 2
   then
                
      open SWV_RefCur2 for
      SELECT * FROM    tt_TEMPTABLE
      WHERE   numTransactionId = v_TransactionID;
   ELSE
      open SWV_RefCur2 for
      SELECT * FROM    tt_TEMPTABLE;
   end if;
        
   v_iCnt := 0;

		--DECLARE @dtFromDate AS DATETIME
		--DECLARE @dtToDate AS DATETIME
		--SET @dtFromDate = GETDATE()
		--SET @dtToDate = DATEADD(DAY,30,GETDATE())
   DROP TABLE IF EXISTS tt_TMPDATES CASCADE;
   CREATE TEMPORARY TABLE tt_TMPDATES 
   ( 
      dt TIMESTAMP NOT NULL 
   );

   v_intDiff := DATE_PART('day',v_dtToDate -v_dtFromDate);

   WHILE (v_intDiff > v_iCnt) LOOP
      RAISE NOTICE '%',v_dtFromDate:: date;
      INSERT  INTO tt_TMPDATES(dt)
      SELECT  v_dtFromDate:: date;
      v_iCnt := v_iCnt::bigint+1;
   END LOOP;

   open SWV_RefCur3 for
   SELECT  FormatedDateFromDate(dt::VARCHAR(50),v_numDomainID::NUMERIC) AS dt
   FROM    tt_TMPDATES;
        

        		  
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   RETURN;
END; $$;
-- CREATED BY MANISH ANJARA : 1st OCT, 2014
/*
EXEC USP_GetJournalEntries_Virtual @numDomainId = 169, @vcAccountId = '6013',
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-31 23:59:59',
    @vcTranType = '', @varDescription = '', @CompanyName = '',
    @vcBizPayment = '', @vcCheqNo = '', @vcBizDocID = '', @vcTranRef = '',
    @vcTranDesc = '', @numDivisionID = 0, @ClientTimeZoneOffset = -330,
    @charReconFilter = 'A', @tintMode = 1, @numItemID = 0, @CurrentPage = 1,
    @PageSize = 100

*/



