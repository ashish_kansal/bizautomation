DROP FUNCTION IF EXISTS USP_GetItems_API;

CREATE OR REPLACE FUNCTION USP_GetItems_API(v_numDomainID NUMERIC(9,0),
          v_WebAPIId    INTEGER,
          v_CreatedDate TIMESTAMP DEFAULT NULL,
          v_ModifiedDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDefaultWarehouse  NUMERIC(18,0);
BEGIN
   IF v_WebAPIId  = 5 then

      open SWV_RefCur for
      SELECT   numItemCode,
				   vcItemName,
					coalesce((SELECT(monWListPrice)
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail
            WHERE  numDomainID = I.numDomainID AND WebApiId = v_WebAPIId) LIMIT 1),I.monListPrice) AS monListPrice,
				   vcModelID,
				   coalesce(fltWeight,0) AS intWeight,
				   coalesce((SELECT(numOnHand)
         FROM   WareHouseItems
         WHERE  numItemID = numItemCode
         AND numWareHouseID IN(SELECT coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail
            WHERE  numDomainID = I.numDomainID AND WebApiId = v_WebAPIId) LIMIT 1),
      0) AS QtyOnHand,
				   coalesce(IA.vcAPIItemID,'0') AS vcAPIItemID, coalesce(IA.vcSKU,coalesce(I.vcSKU,'')) AS vcAPISKU,I.numDomainID
      FROM     Item I
      LEFT OUTER JOIN ItemAPI IA ON I.numItemCode = IA.numItemID AND IA.WebApiId = v_WebAPIId
      WHERE
      I.numDomainID = v_numDomainID
      AND 1 =(CASE
      WHEN v_CreatedDate IS NULL THEN 1
      ELSE(CASE WHEN I.bintCreatedDate > v_CreatedDate THEN 1	ELSE 0 END)
      END)
      AND 1 =(CASE
      WHEN v_ModifiedDate IS NULL THEN 1
      ELSE(CASE WHEN I.bintModifiedDate > v_ModifiedDate THEN 1 ELSE 0 END)
      END)
      AND  LENGTH(vcItemName) > 0
      AND I.charItemType = 'P'
      AND vcExportToAPI IS NOT NULL
      AND CONCAT(',',vcExportToAPI,',') ILIKE CONCAT('%,',v_WebAPIId,',%');
   ELSE
	-- GET DEFAULT WAREHOUSE SET FOR MARKET PLACE     
	
      select   numwarehouseID INTO v_numDefaultWarehouse FROM WebAPIDetail WHERE numDomainID = v_numDomainID AND WebApiId = v_WebAPIId;
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEM
      (
         numDomainID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         vcItemName VARCHAR(300),
         vcModelID VARCHAR(200),
         numItemGroup NUMERIC(18,0),
         fltWeight DOUBLE PRECISION,
         fltLength DOUBLE PRECISION,
         fltHeight DOUBLE PRECISION,
         fltWidth DOUBLE PRECISION,
         vcManufacturer VARCHAR(250),
         IsArchieve BOOLEAN,
         txtItemDesc VARCHAR(1000),
         vcSKU VARCHAR(50),
         numBarCodeId VARCHAR(50),
         bintCreatedDate TIMESTAMP,
         bintModifiedDate TIMESTAMP
      );
      INSERT INTO
      tt_TEMPITEM
      SELECT
      numDomainID,
		numItemCode,
		coalesce(vcItemName,''),
		coalesce(vcModelID,''),
		coalesce(numItemGroup,0) AS numItemGroup,
		coalesce(fltWeight,0) AS fltWeight,
		coalesce(fltLength,0) AS fltLength,
		coalesce(fltHeight,0) AS fltHeight,
		coalesce(fltWidth,0) AS fltWidth,
		coalesce(vcManufacturer,'') AS vcManufacturer,
		coalesce(IsArchieve,false) AS IsArchieve,
		coalesce(txtItemDesc,'') AS txtItemDesc,
		coalesce(vcSKU,''),
		coalesce(numBarCodeId,''),
		bintCreatedDate,
		bintModifiedDate
      FROM
      Item
      WHERE
      Item.numDomainID = v_numDomainID
      AND  LENGTH(Item.vcItemName) > 0
      AND Item.charItemType = 'P'
      AND Item.vcExportToAPI IS NOT NULL
      AND CONCAT(',',vcExportToAPI,',') ILIKE CONCAT('%,',v_WebAPIId,',%');
      open SWV_RefCur for
      SELECT
      X.* FROM(SELECT
         Item.numDomainID,
			coalesce(ItemAPI.vcAPIItemID,'0') AS vcAPIItemID,
			Item.numItemCode,
			Item.vcItemName,
			Item.vcModelID,
			Item.numItemGroup,
			Item.fltWeight AS intWeight,
			Item.fltWeight,
			Item.fltLength,
			Item.fltHeight,
			Item.fltWidth,
			Item.vcManufacturer,
			Item.IsArchieve,
			Item.txtItemDesc,
			Item.vcSKU,
			Item.numBarCodeId,
			TempWarehouseItem.numOnHand AS QtyOnHand,
			TempWarehouseItem.monWListPrice AS monListPrice,
			CONCAT('<Images>',(SELECT string_agg(CONCAT('<ItemImages ',
						' numItemImageId="',numItemImageId,'"'
						' vcPathForImage="',vcPathForImage,'"'
						' vcPathForTImage="',vcPathForTImage,'"'
						' bitDefault="',(CASE WHEN bitDefault THEN 1 ELSE 0 END),'"'
						' intDisplayOrder="',(CASE
												WHEN bitDefault = true THEN -1
												ELSE coalesce(intDisplayOrder,0)
											END),'" />'),'' ORDER BY CASE WHEN bitDefault = true THEN -1 ELSE coalesce(intDisplayOrder,0) END ASC)
					FROM
						ItemImages
					WHERE
						numItemCode = Item.numItemCode
					),'</Images>') AS xmlItemImages,
			CAST(coalesce(ItemExtendedDetails.txtDesc,'') AS TEXT) AS vcExtendedDescToAPI
         FROM
         tt_TEMPITEM Item
         LEFT JOIN
         ItemExtendedDetails
         ON
         ItemExtendedDetails.numItemCode = Item.numItemCode
         LEFT JOIN
         ItemAPI
         ON
         ItemAPI.WebApiId = v_WebAPIId
         AND Item.numItemCode = ItemAPI.numItemID
         AND Item.vcSKU = ItemAPI.vcSKU
         CROSS JOIN LATERAL(SELECT
            coalesce(SUM(numOnHand),0) AS numOnHand,
				coalesce(MAX(monWListPrice),0) AS monWListPrice,
				MAX(dtModified) AS dtModified
            FROM
            WareHouseItems
            WHERE
            WareHouseItems.numItemID = Item.numItemCode
            AND WareHouseItems.numWareHouseID = v_numDefaultWarehouse
            GROUP BY
            numWareHouseID) TempWarehouseItem
         WHERE
         numItemGroup = 0 --INVENTORY ITEM
         AND 1 =(CASE
         WHEN v_CreatedDate IS NULL THEN 1
         ELSE(CASE
            WHEN Item.bintCreatedDate > CAST(v_CreatedDate AS timestamp) THEN 1
            ELSE 0
            END)
         END)
         AND 1 =(CASE
         WHEN v_ModifiedDate IS NULL THEN 1
         ELSE(CASE
            WHEN Item.bintModifiedDate > CAST(v_ModifiedDate AS timestamp) THEN 1
            ELSE 0
            END)
         END)
         UNION
         SELECT
         Item.numDomainID,
			coalesce(ItemAPI.vcAPIItemID,'0') AS vcAPIItemID,
			Item.numItemCode,
			Item.vcItemName,
			Item.vcModelID,
			Item.numItemGroup,
			Item.fltWeight AS intWeight,
			Item.fltWeight,
			Item.fltLength,
			Item.fltHeight,
			Item.fltWidth,
			Item.vcManufacturer,
			Item.IsArchieve,
			Item.txtItemDesc,
			coalesce(WareHouseItems.vcWHSKU,'') AS vcSKU,
			coalesce(WareHouseItems.vcBarCode,'') AS numBarCodeId,
			WareHouseItems.numOnHand AS QtyOnHand,
			WareHouseItems.monWListPrice AS monListPrice,
			CONCAT('<Images>',(SELECT string_agg(CONCAT('<ItemImages ',
						' numItemImageId="',numItemImageId,'"'
						' vcPathForImage="',vcPathForImage,'"'
						' vcPathForTImage="',vcPathForTImage,'"'
						' bitDefault="',(CASE WHEN bitDefault THEN 1 ELSE 0 END),'"'
						' intDisplayOrder="',(CASE
												WHEN bitDefault = true THEN -1
												ELSE coalesce(intDisplayOrder,0)
											END),'" />'),'' ORDER BY CASE WHEN bitDefault = true THEN -1 ELSE coalesce(intDisplayOrder,0) END ASC)
					FROM
						ItemImages
					WHERE
						numItemCode = Item.numItemCode
					),'</Images>') AS xmlItemImages,
			CAST(coalesce(ItemExtendedDetails.txtDesc,'') AS TEXT) AS vcExtendedDescToAPI
         FROM
         tt_TEMPITEM Item
         INNER JOIN
         WareHouseItems
         ON
         WareHouseItems.numItemID = Item.numItemCode
         AND WareHouseItems.numWareHouseID = v_numDefaultWarehouse
         LEFT JOIN
         ItemExtendedDetails
         ON
         ItemExtendedDetails.numItemCode = Item.numItemCode
         LEFT JOIN
         ItemAPI
         ON
         ItemAPI.WebApiId = v_WebAPIId
         AND Item.numItemCode = ItemAPI.numItemID
         AND Item.vcSKU = ItemAPI.vcSKU
         WHERE
         numItemGroup > 0 --MATRIX ITEM
         AND 1 =(CASE
         WHEN v_CreatedDate IS NULL THEN 1
         ELSE(CASE
            WHEN Item.bintCreatedDate > CAST(v_CreatedDate AS timestamp) THEN 1
            ELSE 0
            END)
         END)
         AND 1 =(CASE
         WHEN v_ModifiedDate IS NULL THEN 1
         ELSE(CASE
            WHEN Item.bintModifiedDate > CAST(v_ModifiedDate AS timestamp) THEN 1
            ELSE 0
            END)
         END)) AS X
      ORDER BY
      X.numItemCode ASC;
   end if;
   RETURN;
END; $$;



  
  



