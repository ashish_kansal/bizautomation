-- Stored procedure definition script usp_getDycModuleMaster for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDycModuleMaster(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(numModuleID as VARCHAR(255)),cast(vcModuleName as VARCHAR(255))
   from DycModuleMaster
   where numDomainID = v_numDomainID or coalesce(numDomainID,0) = 0;
END; $$;          












