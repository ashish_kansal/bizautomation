-- Stored procedure definition script USP_GetCustReportKPI for PostgreSQL
create or replace FUNCTION USP_GetCustReportKPI(v_numCustomReportId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numReportOptionGroupId as VARCHAR(255)),cast(numFieldGroupID as VARCHAR(255)),cast(vcIndicator as VARCHAR(255)),cast(vcPeriod as VARCHAR(255)),cast(vcPriority as VARCHAR(255)),cast(vcStatusIcon as VARCHAR(255))
   from CustReportKPI;
--UNION
--SELECT 0,0,'','','',''

--select * from CustReportKPIMaster where numKPIType=1 ---'Period
--
--select * from CustReportKPIMaster where numKPIType=2 ---'Priority
--
--select * from CustReportKPIMaster where numKPIType=3 ---'Status Icon

END; $$;











