-- Function definition script GetCartItemInclusionDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCartItemInclusionDetails(v_numCartID NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcInclusionDetails  TEXT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;

	--FIRST LEVEL CHILD
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numCartID NUMERIC(18,0),
      vcChildKitItemSelection TEXT,
      numItemCode NUMERIC(18,0),
      bitKitParent BOOLEAN,
      numParentItemCode NUMERIC(18,0),
      numTotalQty NUMERIC(18,2),
      numUOMID NUMERIC(18,0),
      tintLevel SMALLINT,
      vcInclusionDetail VARCHAR(1000)
   );
   INSERT INTO tt_TEMPTABLE(numCartID,vcChildKitItemSelection,numItemCode,bitKitParent,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail)
   SELECT
   CI.numCartId,
		CI.vcChildKitItemSelection,
		ID.numChildItemID,
		coalesce(I.bitKitParent,false),
		0,
		CAST((CI.numUnitHour*ID.numQtyItemsReq) AS DOUBLE PRECISION),
		coalesce(ID.numUOMID,0),
		1,
		CASE
   WHEN coalesce(I.bitKitParent,false) = true
   THEN
      CONCAT('<li><b>',I.vcItemName,':</b><br/> ')
   ELSE
      CONCAT('<li>',I.vcItemName,' (',CAST((CI.numUnitHour*ID.numQtyItemsReq) AS DOUBLE PRECISION),
      ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
   END
   FROM
   CartItems CI
   INNER JOIN
   ItemDetails ID
   ON
   CI.numItemCode = ID.numItemKitID
   INNER JOIN
   Item I
   ON
   ID.numChildItemID = I.numItemCode
   LEFT JOIN
   UOM
   ON
   UOM.numUOMId =(CASE WHEN coalesce(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
   WHERE
   CI.numCartId = v_numCartID;
	
	
   INSERT INTO tt_TEMPTABLE(numCartID,vcChildKitItemSelection,numItemCode,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail)
   SELECT
   c.numCartID,
		c.vcChildKitItemSelection,
		ID.numChildItemID,
		c.numItemCode,
		CAST((c.numTotalQty*ID.numQtyItemsReq) AS DOUBLE PRECISION),
		coalesce(ID.numUOMID,0),
		2,
		CONCAT('<li>',I.vcItemName,' (',CAST((c.numTotalQty*ID.numQtyItemsReq) AS DOUBLE PRECISION),
   ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
   FROM
   tt_TEMPTABLE c
   CROSS JOIN LATERAL(SELECT
      CAST(SUBSTR(items,0,POSITION('-' IN items)) AS NUMERIC) AS numKitItemID
			, CAST(SUBSTR(items,POSITION('-' IN items)+1,LENGTH(items) -POSITION('-' IN items)) AS NUMERIC) As numKitChildItemID
      FROM
      Split(c.vcChildKitItemSelection,',')) AS t2
   INNER JOIN
   ItemDetails ID
   ON
   ID.numItemKitID = t2.numKitItemID
   AND ID.numChildItemID = t2.numKitChildItemID
   AND c.numItemCode = t2.numKitItemID
   INNER JOIN
   Item I
   ON
   t2.numKitChildItemID = I.numItemCode
   LEFT JOIN
   UOM
   ON
   UOM.numUOMId =(CASE WHEN coalesce(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
   WHERE
   LENGTH(coalesce(c.vcChildKitItemSelection,'')) > 0;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPTABLEFINAL_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLEFINAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLEFINAL
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numCartID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      bitKitParent BOOLEAN,
      numParentItemCode NUMERIC(18,0),
      numTotalQty NUMERIC(18,2),
      numUOMID NUMERIC(18,0),
      tintLevel SMALLINT,
      vcInclusionDetail TEXT
   );

   INSERT INTO tt_TEMPTABLEFINAL(numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail)
   SELECT
   numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
   FROM
   tt_TEMPTABLE
   WHERE
   tintLevel = 1;
		
   select   COUNT(*) INTO v_iCount FROM tt_TEMPTABLEFINAL WHERE tintLevel = 1;

   WHILE v_i <= v_iCount LOOP
      IF (coalesce((SELECT bitKitParent FROM tt_TEMPTABLEFINAL WHERE ID = v_i),false) = false OR (coalesce((SELECT bitKitParent FROM tt_TEMPTABLEFINAL WHERE ID = v_i),false) = true AND(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE tintLevel = 2 AND numCartID =(SELECT numCartID FROM tt_TEMPTABLEFINAL WHERE ID = v_i) AND numParentItemCode =(SELECT numItemCode FROM tt_TEMPTABLEFINAL WHERE ID = v_i)) > 0)) then
		
         v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,(SELECT vcInclusionDetail FROM tt_TEMPTABLEFINAL WHERE ID = v_i));
      end if;
      IF(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE tintLevel = 2 AND numCartID =(SELECT numCartID FROM tt_TEMPTABLEFINAL WHERE ID = v_i) AND numParentItemCode =(SELECT numItemCode FROM tt_TEMPTABLEFINAL WHERE ID = v_i)) > 0 then
		
         v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,'<ul class="list-unstyled">');
         select   COALESCE(v_vcInclusionDetails,'') || vcInclusionDetail INTO v_vcInclusionDetails FROM tt_TEMPTABLE WHERE tintLevel = 2 AND numCartID =(SELECT numCartID FROM tt_TEMPTABLEFINAL WHERE ID = v_i) AND numParentItemCode =(SELECT numItemCode FROM tt_TEMPTABLEFINAL WHERE ID = v_i);
         v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,'</ul></li>');
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   v_vcInclusionDetails := CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',v_vcInclusionDetails,
   '</ul>');
	
   RETURN v_vcInclusionDetails;
END; $$;

