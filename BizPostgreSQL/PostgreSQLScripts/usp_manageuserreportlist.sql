-- Stored procedure definition script Usp_ManageUserReportList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_ManageUserReportList(v_numReportId NUMERIC(9,0),
v_numDomainId NUMERIC(9,0),
v_numUserCntId NUMERIC(9,0),
v_tintReportType SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT 1 FROM UserReportList WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId
   AND numReportID = v_numReportId AND tintReportType = v_tintReportType) then

      INSERT INTO UserReportList(numReportID,numDomainID,numUserCntID,tintReportType) VALUES(v_numReportId,v_numDomainId,v_numUserCntId,v_tintReportType);
   end if;
   RETURN;
END; $$;				


