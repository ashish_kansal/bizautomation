-- Stored procedure definition script usp_GetContactInformation for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactInformation(v_numContactID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcLastname, vcFirstName,vcGivenName, vcPosition, vcEmail, vcDepartment,
    numPhone, numPhoneExtension, vcFax, numContactType,   cast(vcData as VARCHAR(255)) ,
    cast(vcState as VARCHAR(255)), cast(vcCity as VARCHAR(255)), cast(vcStreet as VARCHAR(255)), cast(vcCountry as VARCHAR(255)), cast(intPostalCode as VARCHAR(255)),
    AC.numDivisionId, bintDOB, txtNotes, AC.numCreatedBy,VcAsstFirstName || ' ' || vcAsstLastName as AsstName,numCell,numHomePhone,  numAsstPhone,
   DM.numTerID, --New    
   DM.numCreatedBy --New    
   FROM AdditionalContactsInformation AC join
   DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
   left join contactAddress Addc on  Addc.numContactID = AC.numContactId
   left join Listdetails lst on AC.numContactType = lst.numListItemID
   WHERE AC.numContactId = v_numContactID;
END; $$;












