-- Stored procedure definition script USP_GetDivisionForContactID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivisionForContactID(v_numContactId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numDivisionId
   From AdditionalContactsInformation
   Where numContactId = v_numContactId;
END; $$;












