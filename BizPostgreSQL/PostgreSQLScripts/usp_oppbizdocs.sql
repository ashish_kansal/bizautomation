DROP FUNCTION IF EXISTS USP_OppBizDocs;

CREATE OR REPLACE FUNCTION USP_OppBizDocs(v_byteMode SMALLINT DEFAULT null,                        
	v_numOppId NUMERIC(9,0) DEFAULT null,                        
	v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ParentBizDoc  NUMERIC(9,0) DEFAULT 0;
   v_lngJournalId  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numBizDocID  INTEGER;
   v_bitFulfilled  BOOLEAN;
   v_tintOppType  SMALLINT;
   v_tintShipped  BOOLEAN;
   v_bitAuthoritativeBizDoc  BOOLEAN;
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;

   v_monCreditAmount  DECIMAL(20,5);
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;                       
                        
   v_BaseCurrencySymbol  VARCHAR(3);
   v_numCurrencyID  NUMERIC(18,0);
   v_numARContactPosition  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
BEGIN
   select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numOppId = v_numOppId;
    
   open SWV_RefCur for
   SELECT
   coalesce(CompanyInfo.numCompanyType,0) AS numCompanyType,
		coalesce(CompanyInfo.vcProfile,0) AS vcProfile,
		coalesce(OpportunityMaster.numAccountClass,0) AS numAccountClass
   FROM
   OpportunityMaster
   JOIN
   DivisionMaster
   ON
   DivisionMaster.numDivisionID = OpportunityMaster.numDivisionId
   JOIN
   CompanyInfo
   ON
   CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
   WHERE
   numOppId = v_numOppId;
		                                  
   IF v_byteMode = 1 then
      select   tintopptype, coalesce(tintshipped,0) INTO v_tintOppType,v_tintShipped FROM OpportunityMaster WHERE  numOppId = v_numOppId  and numDomainId = v_numDomainID;
      select   numBizDocId, bitFulFilled, coalesce(bitAuthoritativeBizDocs,0) INTO v_numBizDocID,v_bitFulfilled,v_bitAuthoritativeBizDoc FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocsId;
      IF coalesce(v_tintShipped,false) = true AND (v_numBizDocID = 296 OR coalesce(v_bitAuthoritativeBizDoc,false) = true) then
		
         RAISE EXCEPTION 'NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED';
         RETURN;
      end if;
      IF EXISTS(SELECT numDepositeDetailID FROM DepositeDetails WHERE numOppBizDocsID = v_numOppBizDocsId) then
		
         RAISE EXCEPTION 'PAID';
         RETURN;
      end if;
      IF EXISTS(SELECT OBPD.* FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID = v_numOppBizDocsId) then
		
         RAISE EXCEPTION 'PAID';
         RETURN;
      end if;
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      IF coalesce(v_tintCommitAllocation,0) = 2 AND coalesce(v_bitAllocateInventoryOnPickList,false) = true AND v_numBizDocID = 29397 AND(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = 296 AND numSourceBizDocId = v_numOppBizDocsId) > 0 then
		
         RAISE EXCEPTION 'NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT';
         RETURN;
      end if;
      IF coalesce(v_tintCommitAllocation,0) = 2 AND coalesce(v_bitAllocateInventoryOnPickList,false) = true AND v_numBizDocID = 29397 then
		
			-- Revert Allocation
         PERFORM USP_RevertAllocationPickList(v_numDomainID,v_numOppId,v_numOppBizDocsId,v_numUserCntID);
      end if;
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppBizDocsId NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numOppBizDocsId)
      SELECT
      numOppBizDocsId
      FROM
      OpportunityBizDocs
      WHERE
      numSourceBizDocId = v_numOppBizDocsId;
      BEGIN
         -- BEGIN TRANSACTION
select   COUNT(*) INTO v_iCount FROM tt_TEMP;
         WHILE v_i <= v_iCount LOOP
            select   numOppBizDocsId INTO v_ParentBizDoc FROM tt_TEMP WHERE ID = v_i;
            IF NOT EXISTS(SELECT numDepositeDetailID FROM DepositeDetails WHERE numOppBizDocsID = v_ParentBizDoc) then
				
               PERFORM USP_OppBizDocs(v_byteMode := v_byteMode::SMALLINT,v_numOppId := v_numOppId,v_numOppBizDocsId := v_ParentBizDoc,
               v_ClientTimeZoneOffset := v_ClientTimeZoneOffset,
               v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,SWV_RefCur := null,SWV_RefCur2 := null);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;

			--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
         IF v_tintOppType = 1 AND v_numBizDocID = 296 AND v_bitFulfilled = true then
			
            PERFORM USP_OpportunityBizDocs_RevertFulFillment(v_numDomainID,v_numUserCntID,v_numOppId,v_numOppBizDocsId);
         end if;
         IF v_numBizDocID = 55206 then
			
            UPDATE
            OpportunityItems OI
            SET
            numQtyPicked =(CASE
            WHEN coalesce(OI.numQtyPicked,0) > coalesce(OBDI.numUnitHour,0)
            THEN(coalesce(OI.numQtyPicked,0) -coalesce(OBDI.numUnitHour,0))
            ELSE 0
            END)
            FROM
            OpportunityBizDocItems OBDI
            WHERE
            OBDI.numOppItemID = OI.numoppitemtCode AND OBDI.numOppBizDocID = v_numOppBizDocsId;

			PERFORM usp_masssalesfulfillment_revertitemspickedqty(v_numDomainID,v_numUserCntID,v_numOppId,v_numOppBizDocsId);
         end if;
         DELETE FROM ProjectsOpportunities WHERE numOppBizDocID = v_numOppBizDocsId;
         DELETE FROM ShippingReportItems WHERE numShippingReportId IN(SELECT numShippingReportID from ShippingReport WHERE numOppBizDocId = v_numOppBizDocsId);
         DELETE FROM ShippingBox WHERE numShippingReportID IN(SELECT numShippingReportID from ShippingReport WHERE numOppBizDocId = v_numOppBizDocsId);
         DELETE FROM ShippingReport WHERE numOppBizDocId = v_numOppBizDocsId;
         DELETE  FROM OpportunityBizDocItems WHERE numOppBizDocID = v_numOppBizDocsId;
         DELETE FROM OpportunityRecurring WHERE numOppBizDocID = v_numOppBizDocsId and tintRecurringType = 4;
         UPDATE OpportunityRecurring SET numOppBizDocID = NULL WHERE numOppBizDocID = v_numOppBizDocsId;
         DELETE FROM RecurringTransactionReport WHERE numRecTranBizDocID = v_numOppBizDocsId;
				--Delete All entries for current bizdoc
			--EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
         DELETE FROM OpportunityBizDocsPaymentDetails OBPD using EmbeddedCost EC where OBPD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId AND EC.numOppBizDocID = v_numOppBizDocsId;
         DELETE FROM EmbeddedCostItems ECI using EmbeddedCost EC where ECI.numEmbeddedCostID = EC.numEmbeddedCostID AND EC.numOppBizDocID = v_numOppBizDocsId AND EC.numDomainID = v_numDomainID;
         DELETE FROM EmbeddedCost WHERE numOppBizDocID = v_numOppBizDocsId	AND numDomainID = v_numDomainID;
         DELETE FROM OpportunityBizDocsDetails OBDD using EmbeddedCost EC where OBDD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId AND numOppBizDocID = v_numOppBizDocsId;

		

			--Credit Balance
         v_monCreditAmount := 0;
         v_numDivisionID := 0;
         select   coalesce(oppBD.monCreditAmount,0), Om.numDivisionId, Om.numOppId INTO v_monCreditAmount,v_numDivisionID,v_numOppId FROM
         OpportunityBizDocs oppBD
         JOIN
         OpportunityMaster Om
         ON
         Om.numOppId = oppBD.numoppid WHERE
         numOppBizDocsId = v_numOppBizDocsId;
         DELETE FROM CreditBalanceHistory where numOppBizDocsId = v_numOppBizDocsId;
         IF v_tintOppType = 1 then --Sales
            UPDATE DivisionMaster SET monSCreditBalance = coalesce(monSCreditBalance,0)+coalesce(v_monCreditAmount,0) where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
         ELSEIF v_tintOppType = 2
         then --Purchase
            UPDATE DivisionMaster SET monPCreditBalance = coalesce(monPCreditBalance,0)+coalesce(v_monCreditAmount,0) where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
         end if;
         DELETE FROM DocumentWorkflow WHERE numDocID = v_numOppBizDocsId AND cDocType = 'B';
         DELETE FROM BizDocAction BA using BizActionDetails BAD where BA.numBizActionId = BAD.numBizActionId AND BAD.numOppBizDocsId = v_numOppBizDocsId and btDocType = 1;
         DELETE FROM BizActionDetails where numOppBizDocsId = v_numOppBizDocsId and btDocType = 1;
         DELETE FROM General_Journal_Details GJD using General_Journal_Header GJH where GJD.numJournalId = GJH.numJOurnal_Id AND GJH.numOppId = v_numOppId AND GJH.numOppBizDocsId = v_numOppBizDocsId AND GJH.numDomainId = v_numDomainID;
         DELETE FROM General_Journal_Header WHERE numOppId = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId AND numDomainId = v_numDomainID;
         DELETE FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocsId;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   end if;                        
                        
   if v_byteMode = 2 then
      v_BaseCurrencySymbol := '$';
      select   coalesce(numCurrencyID,0), coalesce(numARContactPosition,0) INTO v_numCurrencyID,v_numARContactPosition FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(varCurrSymbol,'$') INTO v_BaseCurrencySymbol FROM Currency WHERE numCurrencyID = v_numCurrencyID;
      open SWV_RefCur2 for
      select *, coalesce(X.monPAmount,0) -X.monAmountPaid as BalDue from(select  opp.numOppBizDocsId,
 opp.numoppid,
 opp.numBizDocId,
 coalesce((SELECT  numOrientation FROM BizDocTemplate WHERE numBizDocTempID = opp.numBizDocTempID LIMIT 1),0) AS numOrientation,
 coalesce((SELECT  vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = opp.numBizDocTempID LIMIT 1),'') AS vcTemplateName,
 coalesce((SELECT  bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = opp.numBizDocTempID LIMIT 1),false) AS bitKeepFooterBottom,
 oppmst.vcpOppName,
 coalesce(ListDetailsName.vcName,mst.vcData) as BizDoc,
 opp.numCreatedBy,
opp.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS CreatedDate,
 opp.vcRefOrderNo,
 vcBizDocID,
 CASE WHEN LENGTH(coalesce(vcBizDocName,'')) = 0 THEN vcBizDocID ELSE vcBizDocName END AS vcBizDocName,
  GetDealAmount(v_numOppId,TIMEZONE('UTC',now()),opp.numOppBizDocsId) as monPAmount,
  CASE
         WHEN tintopptype = 1
         THEN(CASE
            WHEN (coalesce(numAuthoritativeSales,0) = numBizDocId OR numBizDocId = 296)
            THEN 'Authoritative' || CASE WHEN coalesce(opp.tintDeferred,0) = 1 THEN ' (Deferred)' ELSE '' END
            WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
            ELSE 'Non-Authoritative' END)
         when tintopptype = 2  then(case when coalesce(numAuthoritativePurchase,0) = numBizDocId then 'Authoritative' || Case when coalesce(opp.tintDeferred,0) = 1 then ' (Deferred)' else '' end else 'Non-Authoritative' end)  end as BizDocType,
  coalesce(monAmountPaid,0) AS monAmountPaid,
  coalesce(C.varCurrSymbol,'') AS varCurrSymbol ,
  coalesce(opp.bitAuthoritativeBizDocs,0) AS bitAuthoritativeBizDocs,
  coalesce(oppmst.tintshipped,0) AS tintshipped,coalesce(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,coalesce(opp.tintDeferred,0) as tintDeferred,
  coalesce(opp.numBizDocTempID,0) AS numBizDocTempID
  ,con.numContactId
  ,coalesce(con.vcEmail,'') AS vcEmail
  ,coalesce(opp.numBizDocStatus,0) AS numBizDocStatus,
  CASE WHEN isnumeric((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(oppmst.intBillingDays,0))::VARCHAR) = true
         THEN(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(oppmst.intBillingDays,0))
         ELSE 0
         END AS numBillingDaysName,
  opp.dtFromDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtFromDate,
  opp.dtFromDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS BillingDate,
  coalesce(C.fltExchangeRate,1) AS fltExchangeRateCurrent,
  coalesce(oppmst.fltExchangeRate,1) AS fltExchangeRateOfOrder,
  v_BaseCurrencySymbol AS BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,coalesce(opp.fltExchangeRateBizDoc,coalesce(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN(SELECT COUNT(*) FROM ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = v_numDomainID) > 0 THEN 1 ELSE 0 END AS IsShippingReportGenerated,
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN(SELECT COUNT(*) FROM ShippingBox
            WHERE numShippingReportID IN(SELECT numShippingReportID FROM ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = v_numDomainID)
            AND LENGTH(coalesce(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS ISTrackingNumGenerated,
  coalesce((SELECT MAX(numShippingReportID) from ShippingReport SR
            WHERE SR.numOppBizDocId = opp.numOppBizDocsId
            AND SR.numDomainID = oppmst.numDomainId),0) AS numShippingReportId,
  coalesce((SELECT MAX(SB.numShippingReportID) FROM ShippingBox SB
            JOIN ShippingReport SR ON SB.numShippingReportID = SR.numShippingReportID
            where SR.numOppBizDocId = opp.numOppBizDocsId AND SR.numDomainID = oppmst.numDomainId
            AND LENGTH(coalesce(SB.vcTrackingNumber,'')) > 0),
         0) AS numShippingLabelReportId
 ,coalesce(OVERLAY((SELECT  ',' ||  CASE WHEN LENGTH(coalesce(W.vcWareHouse,'')  || ':' || coalesce(WL.vcLocation,'')) > 0 THEN coalesce(W.vcWareHouse,'')  || ':' || coalesce(WL.vcLocation,'') ELSE '' END
            FROM    WarehouseLocation AS WL
            JOIN Warehouses W ON WL.numWarehouseID = W.numWareHouseID
            WHERE  WL.numWLocationID IN(SELECT  WHI.numWLocationID
               FROM    WareHouseItems AS WHI
               JOIN OpportunityItems AS OI ON WHI.numWareHouseItemID = OI.numWarehouseItmsID
               WHERE   OI.numOppId = oppmst.numOppId)) placing '' from 1 for 1),'') AS vcLocation,
		(CASE WHEN coalesce(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN false ELSE true END) AS bitRecur,
		coalesce(opp.bitRecurred,false) AS bitRecurred,
		coalesce((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND coalesce(numDeferredBizDocID,0) = coalesce(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		coalesce(opp.bitShippingGenerated,false) AS bitShippingGenerated,
		coalesce(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		coalesce((SELECT  vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsId = opp.numSourceBizDocId LIMIT 1),'NA') AS vcSourceBizDocID,
		coalesce(oppmst.numShipmentMethod,0) AS numShipmentMethod,
		coalesce(oppmst.numShippingService,0) AS numShippingService,
		(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numoppid = v_numOppId AND OBInner.numBizDocId = 296 AND OBInner.numSourceBizDocId = opp.numOppBizDocsId AND opp.numBizDocId = 29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
         FROM
         OpportunityBizDocs  opp
         left join Listdetails mst
         on mst.numListItemID = opp.numBizDocId
         LEFT JOIN ListDetailsName
         ON ListDetailsName.numListItemID = opp.numBizDocId
         AND ListDetailsName.numDomainID = v_numDomainID
         join OpportunityMaster oppmst
         on oppmst.numOppId = opp.numoppid
         left join  AuthoritativeBizDocs  AB
         on AB.numDomainId =  oppmst.numDomainId
         LEFT OUTER JOIN Currency C
         ON C.numCurrencyID = oppmst.numCurrencyID
         LEFT JOIN AdditionalContactsInformation con ON con.numContactId = oppmst.numContactId
         LEFT JOIN
         RecurrenceConfiguration
         ON
         RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
         where opp.numoppid = v_numOppId
         UNION ALL
         select  CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
 RH.numOppId,
 CAST(0 AS NUMERIC(18,0)) AS numBizDocId,
 1 AS numOrientation,
 '' AS vcTemplateName,
 false AS bitKeepFooterBottom,
 '' AS vcPOppName,
 'RMA' as BizDoc,
 RH.numCreatedBy,
RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS CreatedDate,
 '' AS vcRefOrderNo,
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,
  coalesce(monAmount,0) as monPAmount,
  CAST('RMA' AS VARCHAR(17)) as BizDocType,
  CAST(0 AS DECIMAL(20,5)) AS monAmountPaid,
  '' AS varCurrSymbol ,
  CAST(0 AS INTEGER) AS bitAuthoritativeBizDocs,
  CAST(0 AS SMALLINT) AS tintshipped,CAST(0 AS NUMERIC(18,0)) as numShipVia,RH.numDivisionId,CAST(0 AS SMALLINT) as tintDeferred,
  CAST(0 AS NUMERIC(18,0)) AS numBizDocTempID,con.numContactId,coalesce(con.vcEmail,'') AS vcEmail,CAST(0 AS NUMERIC(18,0)) AS numBizDocStatus,
  CAST(0 AS INTEGER) AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 AS fltExchangeRateCurrent,
  CAST(1 AS DOUBLE PRECISION) AS fltExchangeRateOfOrder,
  v_BaseCurrencySymbol AS BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,CAST(1 AS DOUBLE PRECISION) AS fltExchangeRateBizDoc
  ,CAST(0 AS INTEGER) AS IsShippingReportGenerated,CAST(0 AS INTEGER) AS ISTrackingNumGenerated,
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId
  ,coalesce(OVERLAY((SELECT  ',' ||  CASE WHEN LENGTH(coalesce(W.vcWareHouse,'')  || ':' || coalesce(WL.vcLocation,'')) > 0 THEN coalesce(W.vcWareHouse,'')  || ':' || coalesce(WL.vcLocation,'') ELSE '' END
            FROM    WarehouseLocation AS WL
            JOIN Warehouses W ON WL.numWarehouseID = W.numWareHouseID
            WHERE  WL.numWLocationID IN(SELECT  WHI.numWLocationID
               FROM    WareHouseItems AS WHI
               JOIN OpportunityItems AS OI ON WHI.numWareHouseItemID = OI.numWarehouseItmsID
               WHERE   OI.numOppId = RH.numOppId)) placing '' from 1 for 1),'') AS vcLocation,
			  false AS bitRecur,
			  false AS bitRecurred,
		CAST(0 AS INTEGER) AS intInvoiceForDiferredBizDoc ,
		false AS bitShippingGenerated,
		CAST(0 AS INTEGER) as intUsedShippingCompany,
		CAST(0 AS NUMERIC(18,0)) AS numSourceBizDocId,
		CAST('NA' AS VARCHAR(100)) AS vcSourceBizDocID,
		CAST(0 AS NUMERIC(18,0)) AS numShipmentMethod,
		CAST(0 AS NUMERIC(18,0)) AS numShippingService
		,CAST(0 AS INTEGER) AS IsFulfillmentGeneratedOnPickList
         from
         ReturnHeader RH
         LEFT JOIN AdditionalContactsInformation con ON con.numContactId = RH.numContactID
         where RH.numOppId = v_numOppId) X ORDER BY CreatedDate ASC;
   end if;
END; $$;


