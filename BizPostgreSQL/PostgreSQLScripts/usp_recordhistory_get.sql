-- Stored procedure definition script USP_RecordHistory_Get for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecordHistory_Get(v_numDomainID NUMERIC(18,0),
	v_numModuleID NUMERIC(18,0),
	v_numRecordID NUMERIC(18,0),
	v_PageSize INTEGER,
	v_PageNumber INTEGER,
	INOUT v_TotalRecords INTEGER , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   COUNT(*) INTO v_TotalRecords FROM
   RecordHistory WHERE
   RecordHistory.numDomainID = v_numDomainID
   AND RecordHistory.numRHModuleMasterID = v_numModuleID
   AND RecordHistory.numRecordID = v_numRecordID;

   open SWV_RefCur for
   SELECT
   coalesce(RecordHistory.vcEvent,'') AS vcEvent,
		coalesce(UserMaster.vcUserName,'') AS vcUserName,
		coalesce(RecordHistory.vcFieldName,'') AS vcFieldName,
		coalesce(RecordHistory.vcOldValue,'') AS vcOldValue,
		coalesce(RecordHistory.vcNewValue,'') AS vcNewValue,
		coalesce(RecordHistory.vcDescription,'') AS vcDescription
   FROM
   RecordHistory
   LEFT JOIN
   UserMaster
   ON
   RecordHistory.numUserCntID = UserMaster.numUserDetailId
   WHERE
   RecordHistory.numDomainID = v_numDomainID
   AND RecordHistory.numRHModuleMasterID = v_numModuleID
   AND RecordHistory.numRecordID = v_numRecordID
   ORDER BY
   RecordHistory.dtDate DESC;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/



