CREATE OR REPLACE FUNCTION USP_TicklerActItemsV2TopPurchaseRecordCount
(
	v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                                        
	v_numDomainID NUMERIC(9,0) DEFAULT null,                                                                        
	v_startDate TIMESTAMP DEFAULT NULL,                                                                        
	v_endDate TIMESTAMP DEFAULT NULL,                                                                    
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numViewID NUMERIC(18,0) DEFAULT NULL,
	INOUT v_numTotalRecords NUMERIC(18,0) DEFAULT NULL
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_bitGroupByOrder  BOOLEAN DEFAULT 0;
	v_numBatchID  NUMERIC(18,0) DEFAULT 0;
	v_bitIncludeSearch  BOOLEAN DEFAULT 1;
	v_tintPrintBizDocViewMode  INTEGER DEFAULT 1;
	v_tintPendingCloseFilter  INTEGER DEFAULT 1;
	v_numShippingZone  NUMERIC(18,0) DEFAULT 0;
	v_vcOrderStauts  TEXT DEFAULT ''; 
	v_tintFilterBy  SMALLINT DEFAULT 0;
	v_vcFilterValue  TEXT DEFAULT '';
	v_numPageIndex  INTEGER DEFAULT 1;
	v_vcSortColumn  VARCHAR(100) DEFAULT '';
	v_vcSortOrder  VARCHAR(4) DEFAULT '';
	v_numWarehouseID  VARCHAR(100);
	v_bitRemoveBO  BOOLEAN DEFAULT 0;
	v_bitShowOnlyFullyReceived  BOOLEAN;
	v_tintBillType  INTEGER;
	v_tintPackingViewMode  INTEGER DEFAULT 1;
	v_tintPackingMode  INTEGER;
	v_tintInvoicingType  INTEGER; 
	v_tintCommitAllocation  SMALLINT;
	v_numShippingServiceItemID  NUMERIC(18,0);
	v_numDefaultSalesShippingDoc  NUMERIC(18,0);
	v_vcCustomSearchValue  TEXT DEFAULT ''; 
	v_vcSQL  TEXT;
	v_vcSQLFinal  TEXT;
	v_WarehouseCount  INTEGER DEFAULT 0;
	v_I  INTEGER DEFAULT 1;
BEGIN
	IF v_numViewID = 4 OR v_numViewID = 5 OR v_numViewID = 6 then
		v_bitGroupByOrder := true;
	end if;
   
	IF EXISTS(SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
		SELECT 
			coalesce(bitShowOnlyFullyReceived,false), coalesce(tintBillType,1) INTO v_bitShowOnlyFullyReceived,v_tintBillType 
		FROM
			MassPurchaseFulfillmentConfiguration 
		WHERE
			numDomainID = v_numDomainID
			AND numUserCntID = v_numUserCntID;
	end if;

	select 
		coalesce(tintCommitAllocation,1), coalesce(numShippingServiceItemID,0), coalesce(numDefaultSalesShippingDoc,0) INTO v_tintCommitAllocation,v_numShippingServiceItemID,v_numDefaultSalesShippingDoc 
	FROM
		Domain 
	WHERE
		numDomainId = v_numDomainID; 

	v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC LIMIT 1)');
	
	DROP TABLE IF EXISTS tt_TEMPMSRECORDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPMSRECORDS
	(
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0)
	);
	BEGIN
		CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
		EXCEPTION WHEN OTHERS THEN
			NULL;
	END;

	DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
		numWarehouseID NUMERIC(18,0)
	);

	INSERT INTO tt_TEMPWAREHOUSE
	(
		numWarehouseID
	)
	SELECT
		numWareHouseID
	FROM 
		Warehouses W
	LEFT JOIN
		AddressDetails AD
	ON
		W.numAddressID = AD.numAddressID
	WHERE
		W.numDomainID = v_numDomainID;

	SELECT COUNT(*) INTO v_WarehouseCount FROM tt_TEMPWAREHOUSE;
   
	WHILE(v_I <= v_WarehouseCount) LOOP
		select numWarehouseID INTO v_numWarehouseID FROM tt_TEMPWAREHOUSE WHERE ID = v_I;
      
		v_vcSQL := CONCAT('INSERT INTO tt_TEMPMSRECORDS
							(
								numOppID
								,numOppItemID
							)
							SELECT
								OpportunityMaster.numOppId
								,',(CASE WHEN v_bitGroupByOrder = true THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							FROM
								OpportunityMaster
							INNER JOIN
								DivisionMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							INNER JOIN
								CompanyInfo
							ON
								DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
							INNER JOIN 
								OpportunityItems
							ON
								OpportunityMaster.numOppId = OpportunityItems.numOppId
							INNER JOIN
								Item
							ON
								OpportunityItems.numItemCode = Item.numItemCode
							LEFT JOIN
								WareHouseItems
							ON
								OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
							WHERE
								OpportunityMaster.numDomainId=@numDomainID
								AND OpportunityMaster.tintOppType = 2
								AND OpportunityMaster.tintOppStatus = 1
								AND COALESCE(OpportunityMaster.tintshipped,0) = 0
								AND 1 = (CASE 
											WHEN @numViewID=1 
											THEN (CASE WHEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
											WHEN @numViewID=2
											THEN (CASE WHEN COALESCE(OpportunityItems.numQtyReceived,0) > 0 AND COALESCE(OpportunityItems.numQtyReceived,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
											WHEN @numViewID=3
											THEN (CASE 
													WHEN (CASE 
															WHEN @tintBillType=2 
															THEN COALESCE(OpportunityItems.numQtyReceived,0) 
															WHEN @tintBillType=3 THEN COALESCE(OpportunityItems.numUnitHourReceived,0) 
															ELSE COALESCE(OpportunityItems.numUnitHour,0) 
															END)  - COALESCE((SELECT 
																				SUM(OpportunityBizDocItems.numUnitHour)
																			FROM
																				OpportunityBizDocs
																			INNER JOIN
																				OpportunityBizDocItems
																			ON
																				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																			WHERE
																				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																				AND OpportunityBizDocs.numBizDocId=644
																				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
													THEN 1
													ELSE 0 
												END)
											WHEN @numViewID=4 AND COALESCE(@bitShowOnlyFullyReceived,false) = true
											THEN (CASE 
													WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND COALESCE(OpportunityItems.numUnitHour,0) <> COALESCE(OpportunityItems.numUnitHourReceived,0) ) > 0 
													THEN 0 
													ELSE 1 
												END)
											ELSE 1
										END)
								AND 1 = (CASE WHEN @numViewID IN (1,2) AND COALESCE(@numWarehouseID,0) > 0 AND COALESCE(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
								AND 1 = (CASE WHEN LENGTH(COALESCE(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus '
								,(CASE WHEN coalesce(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END)
								,' (SELECT Id FROM SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)',v_vcCustomSearchValue,(CASE WHEN v_bitGroupByOrder = true THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));
		
		v_vcSQL := REPLACE(v_vcSQL,'@numDomainID',CAST(COALESCE(v_numDomainID,0) AS VARCHAR));
		v_vcSQL := REPLACE(v_vcSQL,'@vcOrderStauts',CONCAT('''',COALESCE(v_vcOrderStauts,''),''''));
		v_vcSQL := REPLACE(v_vcSQL,'@numViewID',CAST(COALESCE(v_numViewID,0) AS VARCHAR));
		v_vcSQL := REPLACE(v_vcSQL,'@numWarehouseID',CONCAT('''',COALESCE(v_numWarehouseID,''),''''));
		v_vcSQL := REPLACE(v_vcSQL,'@bitShowOnlyFullyReceived',CAST(COALESCE(v_bitShowOnlyFullyReceived,false) AS VARCHAR));
		v_vcSQL := REPLACE(v_vcSQL,'@tintBillType',CAST(COALESCE(v_tintBillType,0) AS VARCHAR));		
		
		EXECUTE v_vcSQL;

		v_I := v_I::bigint+1;
	END LOOP;

   SELECT COUNT(*) INTO v_numTotalRecords FROM tt_TEMPMSRECORDS;

   RETURN;
END; $$;
