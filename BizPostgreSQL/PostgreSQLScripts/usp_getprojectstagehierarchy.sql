-- Stored procedure definition script USP_GetProjectStageHierarchy for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProjectStageHierarchy(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numProId NUMERIC(9,0) DEFAULT 0,
v_numContactId NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT -1, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRights  INTEGER;
   v_ProjectName  VARCHAR(50);
   v_TotalProgress  INTEGER;
BEGIN
   v_numRights := 1;

   select   numRights INTO v_numRights from ProjectTeamRights where numProId = v_numProId and numContactId = v_numContactId;

   v_numRights :=(case when v_tintMode = 0 then 1 else v_numRights end);

   IF v_tintMode = 2 then --tintMode=2 (from portal) show all stages

      v_numRights := 1;
      v_tintMode := 1;
   end if;

   open SWV_RefCur for
   with recursive MileStone(numParentStageID,numStageDetailsId,numStage,bitClose,dtEndDate,dtStartDate,
   vcStageName,tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,
   slp_id,tintPercentage,numStagePercentageId,numStagePercentage,
   numExpense,numTime)
   AS(
-- Anchor member definition
    SELECT numParentStageID AS numParentStageID, numStageDetailsId AS numStageDetailsId ,
        CAST(0 AS INTEGER) AS numStage,bitClose AS bitClose,dtEndDate AS dtEndDate,dtStartDate AS dtStartDate
,vcStageName AS vcStageName,tinProgressPercentage AS tinProgressPercentage,coalesce(numAssignTo,0) AS numAssignTo,
  coalesce(fn_GetContactName(numAssignTo),'-') AS vcAssignTo,vcMileStoneName AS vcMileStoneName,slp_id AS slp_id,tintPercentage AS tintPercentage,numStagePercentageId AS numStagePercentageId,
(select numstagepercentage from stagepercentagemaster where numstagepercentageid = SPD.numStagePercentageId) AS numStagePercentage,
  coalesce(fn_GetExpenseDtlsbyProStgID(v_numProId,numStageDetailsId,0::SMALLINT),cast(0 as TEXT)) AS numExpense,
  coalesce(fn_GeTimeDtlsbyProStgID_BT_NBT(v_numProId,numStageDetailsId,1::SMALLINT),
   'Billable Time (0) Non Billable Time (0)') AS numTime
   FROM StagePercentageDetails SPD
   WHERE numParentStageID = 0 and v_numProId =(case v_tintMode when 0 then numOppid
   when 1 then numProjectid end)
   UNION ALL
-- Recursive member definition
   SELECT p.numParentStageID AS numParentStageID, p.numStageDetailsId AS numStageDetailsId ,
        m.numStage+1 AS numStage,p.bitClose AS bitClose,p.dtEndDate AS dtEndDate,p.dtStartDate AS dtStartDate
,p.vcStageName AS vcStageName,p.tinProgressPercentage AS tinProgressPercentage,coalesce(p.numAssignTo,0) AS numAssignTo,
  coalesce(fn_GetContactName(p.numAssignTo),'-') AS vcAssignTo,p.vcMileStoneName AS vcMileStoneName,p.slp_id AS slp_id,p.tintPercentage AS tintPercentage,p.numStagePercentageId AS numStagePercentageId,
(select numstagepercentage from stagepercentagemaster where numstagepercentageid = p.numStagePercentageId) AS numStagePercentage,
  coalesce(fn_GetExpenseDtlsbyProStgID(v_numProId,p.numStageDetailsId,0::SMALLINT),cast(0 as TEXT)) AS numExpense,
  coalesce(fn_GeTimeDtlsbyProStgID_BT_NBT(v_numProId,p.numStageDetailsId,1::SMALLINT),
   'Billable Time (0) Non Billable Time (0)') AS numTime
   FROM StagePercentageDetails AS p
   INNER JOIN MileStone AS m
   ON p.numParentStageID = m.numStageDetailsId where v_numProId =(case v_tintMode when 0 then p.numOppid
   when 1 then p.numProjectid end))
   SELECT numParentStageID, numStageDetailsId, numStage,coalesce(bitClose,false) AS bitClose,slp_id,
FormatedDateFromDate(coalesce(dtEndDate,LOCALTIMESTAMP),v_numDomainID) AS dtEndDate,
FormatedDateFromDate(coalesce(dtStartDate,LOCALTIMESTAMP),v_numDomainID) AS dtStartDate,vcStageName,
coalesce(tinProgressPercentage,0) as tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,tintPercentage,numStagePercentage,numExpense,numTime
   FROM MileStone  where 1 =(case when v_numRights = 1 Then 1
   when v_numRights = 0 Then case when numAssignTo = v_numContactId then 1 else 0 end
   when v_numRights != -1 Then case when numAssignTo in(select numContactId from ProjectTeam where numProjectTeamId = v_numRights) then 1 else 0 end end) order by numStagePercentageId,numStageDetailsId;

   if v_tintMode = 0 then

      select vcpOppName INTO v_ProjectName from OpportunityMaster opp where numOppId = v_numProId     and opp.numDomainId =  v_numDomainID;
      select   coalesce(intTotalProgress,0) INTO v_TotalProgress FROM ProjectProgress WHERE numDomainID = v_numDomainID AND numOppId = v_numProId;
   ELSEIF v_tintMode = 1
   then

      select vcProjectName INTO v_ProjectName from ProjectsMaster pro where numProId = v_numProId     and pro.numdomainId =  v_numDomainID;
      select   coalesce(intTotalProgress,0) INTO v_TotalProgress FROM ProjectProgress WHERE numDomainID = v_numDomainID AND numProId = v_numProId;
   end if;
   open SWV_RefCur2 for
   SELECT v_ProjectName AS ProjectName,coalesce(v_TotalProgress,0) AS TotalProgress;


/****** Object:  StoredProcedure [dbo].[USP_GetProjectTeamContact]    Script Date: 09/17/2010 17:35:27 ******/
   RETURN;
END; $$;


