-- Stored procedure definition script USP_OpportunityMaster_CreateBackOrderPO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_CreateBackOrderPO(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitReOrderPoint  BOOLEAN DEFAULT 0;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_tintOppStautsForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_numReOrderPointOrderStatus  NUMERIC(18,0) DEFAULT 0;
   v_numCurrencyID  NUMERIC(18,0);
   v_tintOppStatus  SMALLINT;
   v_bitIncludeRequisitions  BOOLEAN;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempVendorID  NUMERIC(18,0);
   v_numNewOppID  NUMERIC(18,0);
   v_numContactID  NUMERIC(18,0);
   v_strItems  TEXT;
   v_bitBillingTerms  BOOLEAN;
   v_intBillingDays  INTEGER;
   v_bitInterestType  BOOLEAN;
   v_fltInterest  NUMERIC;
   v_intShippingCompany INTEGER;
   v_numAssignedTo  NUMERIC(18,0);
   v_numDefaultShippingServiceID  NUMERIC(18,0);
   v_dtEstimatedCloseDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempUnitHour  DOUBLE PRECISION;

   v_k  INTEGER;
   v_kCount  INTEGER;
  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_USP_OppManage_vcPOppName VARCHAR(100);
BEGIN
   select   coalesce(bitReOrderPoint,false), coalesce(numReOrderPointOrderStatus,0), coalesce(tintOppStautsForAutoPOBackOrder,0), coalesce(tintUnitsRecommendationForAutoPOBackOrder,0), coalesce(numCurrencyID,0), coalesce(bitIncludeRequisitions,false) INTO v_bitReOrderPoint,v_numReOrderPointOrderStatus,v_tintOppStautsForAutoPOBackOrder,
   v_tintUnitsRecommendationForAutoPOBackOrder,v_numCurrencyID,
   v_bitIncludeRequisitions FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   v_tintOppStatus :=(CASE WHEN v_tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END);


   IF v_bitReOrderPoint = true AND (v_tintOppStautsForAutoPOBackOrder = 2 OR v_tintOppStautsForAutoPOBackOrder = 3) then --2:Purchase Opportunity, 3: Purchase Order
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPBackOrderItems_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPBACKORDERITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPBACKORDERITEMS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numItemCode NUMERIC(18,0),
         bitAssembly BOOLEAN,
         bitKit BOOLEAN,
         numVendorID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numVendorMinQty DOUBLE PRECISION,
         monCost DECIMAL(20,5),
         numReorderQty DOUBLE PRECISION,
         numBackOrder DOUBLE PRECISION,
         numOnOrder DOUBLE PRECISION,
         numRequisitions DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPBACKORDERITEMS(numItemCode
			,bitAssembly
			,bitKit
			,numVendorID
			,numWarehouseItemID
			,numVendorMinQty
			,monCost
			,numReorderQty
			,numBackOrder
			,numRequisitions)
      SELECT
      OI.numItemCode
			,coalesce(I.bitAssembly,false)
			,coalesce(I.bitKitParent,false)
			,coalesce(I.numVendorID,0)
			,OI.numWarehouseItmsID
			,coalesce(v.intMinQty,0)
			,coalesce(v.monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
			,coalesce(I.fltReorderQty,0)
			,coalesce(WI.numBackOrder,0)
			,coalesce((SELECT
         SUM(numUnitHour)
         FROM
         OpportunityItems
         INNER JOIN
         OpportunityMaster
         ON
         OpportunityItems.numOppId = OpportunityMaster.numOppId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND OpportunityMaster.tintopptype = 2
         AND OpportunityMaster.tintoppstatus = 0
         AND OpportunityItems.numItemCode = OI.numItemCode
         AND OpportunityItems.numWarehouseItmsID = OI.numWarehouseItmsID),0)
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      Vendor v
      ON
      I.numVendorID = v.numVendorID
      AND I.numItemCode = v.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      numOppId = v_numOppID
      AND coalesce(numWarehouseItmsID,0) > 0
      AND coalesce(bitDropShip,false) = false
      AND coalesce(bitWorkOrder,false) = false
      AND coalesce(WI.numReorder,0) > 0
      AND coalesce(I.bitKitParent,false) = false
      AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
      UNION
      SELECT
      IChild.numItemCode
			,coalesce(IChild.bitAssembly,false)
			,coalesce(IChild.bitKitParent,false)
			,coalesce(IChild.numVendorID,0)
			,OKI.numWareHouseItemId
			,coalesce(v.intMinQty,0)
			,coalesce(v.monCost,0)*fn_UOMConversion(IChild.numBaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numPurchaseUnit)
			,coalesce(IChild.fltReorderQty,0)
			,coalesce(WI.numBackOrder,0)
			,coalesce((SELECT
         SUM(numUnitHour)
         FROM
         OpportunityItems
         INNER JOIN
         OpportunityMaster
         ON
         OpportunityItems.numOppId = OpportunityMaster.numOppId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND OpportunityMaster.tintopptype = 2
         AND OpportunityMaster.tintoppstatus = 0
         AND OpportunityItems.numItemCode = OKI.numChildItemID
         AND OpportunityItems.numWarehouseItmsID = OKI.numWareHouseItemId),0)
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      Item IChild
      ON
      OKI.numChildItemID = IChild.numItemCode
      AND coalesce(IChild.bitKitParent,false) = false
      INNER JOIN
      Vendor v
      ON
      IChild.numVendorID = v.numVendorID
      AND IChild.numItemCode = v.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OI.numOppId = v_numOppID
      AND coalesce(bitDropShip,false) = false
      AND coalesce(bitWorkOrder,false) = false
      AND coalesce(WI.numReorder,0) > 0
      AND coalesce(I.bitKitParent,false) = true
      AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
      UNION
      SELECT
      IChild.numItemCode
			,coalesce(IChild.bitAssembly,false)
			,coalesce(IChild.bitKitParent,false)
			,coalesce(IChild.numVendorID,0)
			,OKCI.numWareHouseItemId
			,coalesce(v.intMinQty,0)
			,coalesce(v.monCost,0)*fn_UOMConversion(IChild.numBaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numPurchaseUnit)
			,coalesce(IChild.fltReorderQty,0)
			,coalesce(WI.numBackOrder,0)
			,coalesce((SELECT
         SUM(numUnitHour)
         FROM
         OpportunityItems
         INNER JOIN
         OpportunityMaster
         ON
         OpportunityItems.numOppId = OpportunityMaster.numOppId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND OpportunityMaster.tintopptype = 2
         AND OpportunityMaster.tintoppstatus = 0
         AND OpportunityItems.numItemCode = OKCI.numItemID
         AND OpportunityItems.numWarehouseItmsID = OKCI.numWareHouseItemId),0)
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      INNER JOIN
      OpportunityKitChildItems OKCI
      ON
      OKCI.numOppChildItemID = OKI.numOppChildItemID
      INNER JOIN
      Item IChild
      ON
      OKCI.numItemID = IChild.numItemCode
      AND coalesce(IChild.bitKitParent,false) = false
      INNER JOIN
      Vendor v
      ON
      IChild.numVendorID = v.numVendorID
      AND IChild.numItemCode = v.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OI.numOppId = v_numOppID
      AND coalesce(bitDropShip,false) = false
      AND coalesce(bitWorkOrder,false) = false
      AND coalesce(WI.numReorder,0) > 0
      AND coalesce(I.bitKitParent,false) = true
      AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0);
      BEGIN
         CREATE TEMP SEQUENCE tt_TempVendors_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPVENDORS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVENDORS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numVendorId NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPVENDORS(numVendorid)
      SELECT DISTINCT
      numVendorID
      FROM
      tt_TEMPBACKORDERITEMS
      WHERE
      coalesce(numVendorID,0) > 0;
      DROP TABLE IF EXISTS tt_TEMPVENDORITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVENDORITEMS
      (
         ID INTEGER,
         numItemCode NUMERIC(18,0),
         bitAssembly BOOLEAN,
         bitKit BOOLEAN,
         numWarehouseItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION,
         monCost DECIMAL(20,5)
      );
      select   COUNT(*) INTO v_iCount FROM tt_TEMPVENDORS;
      WHILE v_i <= v_iCount LOOP
         select   numVendorid INTO v_numTempVendorID FROM tt_TEMPVENDORS WHERE ID = v_i;
         select(CASE WHEN coalesce(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END), coalesce(numBillingDays,0), (CASE WHEN coalesce(tintInterestType,0) = 1 THEN 1 ELSE 0 END), coalesce(fltInterest,0), coalesce(numAssignedTo,0), coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0) INTO v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_numAssignedTo,
         v_intShippingCompany,v_numDefaultShippingServiceID FROM
         DivisionMaster WHERE
         numDomainID = v_numDomainID AND numDivisionID = v_numTempVendorID;
         select   numContactId INTO v_numContactID FROM AdditionalContactsInformation WHERE numDivisionId = v_numTempVendorID AND bitPrimaryContact = true;
         IF v_numContactID IS NULL then
			
            select   numContactId INTO v_numContactID FROM AdditionalContactsInformation WHERE numDivisionId = v_numTempVendorID    LIMIT 1;
         end if;

			-- NORMAL INVENTORY ITEMS
         DELETE FROM tt_TEMPVENDORITEMS;
         INSERT INTO tt_TEMPVENDORITEMS(ID
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,numUnitHour
				,monCost)
         SELECT
         ROW_NUMBER() OVER(ORDER BY numItemCode ASC)
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,(CASE
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
         THEN(CASE WHEN coalesce(numReorderQty,0) > coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
         THEN(CASE
            WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) AND coalesce(numReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numReorderQty,0)
            WHEN coalesce(numVendorMinQty,0) >= coalesce(numReorderQty,0) AND coalesce(numVendorMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numVendorMinQty,0)
            WHEN coalesce(numBackOrder,0) >= coalesce(numReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numBackOrder,0)
            ELSE coalesce(numReorderQty,0)
            END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
         THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)
         ELSE 0
         END)
				,monCost
         FROM
         tt_TEMPBACKORDERITEMS
         WHERE
         numVendorID = v_numTempVendorID
         AND coalesce(bitAssembly,false) = false
         AND coalesce(bitKit,false) = false
         AND(CASE
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
         THEN(CASE WHEN coalesce(numReorderQty,0) > coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+coalesce(numRequisitions,0))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
         THEN(CASE
            WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) AND coalesce(numReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numReorderQty,0)
            WHEN coalesce(numVendorMinQty,0) >= coalesce(numReorderQty,0) AND coalesce(numVendorMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numVendorMinQty,0)
            WHEN coalesce(numBackOrder,0) >= coalesce(numReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numBackOrder,0)
            ELSE coalesce(numReorderQty,0)
            END) -(coalesce(numOnOrder,0)+coalesce(numRequisitions,0))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
         THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)
         ELSE 0
         END) > 0;
         IF(SELECT COUNT(*) FROM tt_TEMPVENDORITEMS) > 0 then
            v_k := 1;
            select   COUNT(*) INTO v_kCount FROM tt_TEMPVENDORITEMS;
            v_strItems := '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>';
            WHILE v_k <= v_kCount LOOP
               select   CONCAT(v_strItems,'<Item><Op_Flag>1</Op_Flag>','<numoppitemtCode>',ID,'</numoppitemtCode>',
               '<numItemCode>',T1.numItemCode,'</numItemCode>','<numUnitHour>',
               numUnitHour,'</numUnitHour>','<monPrice>',monCost,'</monPrice>',
               '<monTotAmount>',numUnitHour*monCost,'</monTotAmount>','<vcItemDesc>',
               txtItemDesc,'</vcItemDesc>','<numWarehouseItmsID>',numWarehouseItemID,
               '</numWarehouseItmsID>','<ItemType>',charItemType,'</ItemType>','<DropShip>',
               0,'</DropShip>','<bitDiscountType>',1,'</bitDiscountType>',
               '<fltDiscount>',0,'</fltDiscount>','<monTotAmtBefDiscount>',numUnitHour*monCost,
               '</monTotAmtBefDiscount>','<vcItemName>',vcItemName,'</vcItemName>',
               '<numUOM>',coalesce(numBaseUnit,0),'</numUOM>','<bitWorkOrder>0</bitWorkOrder>',
               '<numVendorWareHouse>0</numVendorWareHouse>','<numShipmentMethod>0</numShipmentMethod>',
               '<numSOVendorId>0</numSOVendorId>',
               '<numProjectID>0</numProjectID>','<numProjectStageID>0</numProjectStageID>',
               '<numToWarehouseItemID>0</numToWarehouseItemID>','<Attributes></Attributes>',
               '<AttributeIDs></AttributeIDs>','<vcSKU>',vcSKU,'</vcSKU>',
               '<bitItemPriceApprovalRequired>0</bitItemPriceApprovalRequired>',
               '<numPromotionID>0</numPromotionID>','<bitPromotionTriggered>0</bitPromotionTriggered>',
               '<vcPromotionDetail></vcPromotionDetail>','<numSortOrder>',
               v_k,'</numSortOrder>') INTO v_strItems FROM
               tt_TEMPVENDORITEMS T1
               INNER JOIN
               Item
               ON
               T1.numItemCode = Item.numItemCode WHERE
               ID = v_k;
               v_strItems := CONCAT(v_strItems,'</Item>');
               v_k := v_k::bigint+1;
            END LOOP;
            v_strItems := CONCAT(v_strItems,'</NewDataSet>');
            BEGIN
               v_numNewOppID := 0;
               v_USP_OppManage_vcPOppName := '';
               SELECT * INTO v_numNewOppID,v_USP_OppManage_vcPOppName FROM USP_OppManage(v_numOppID := v_numNewOppID,v_numContactId := v_numContactID,v_numDivisionId := v_numTempVendorID,v_tintSource := 0,v_vcPOppName := v_USP_OppManage_vcPOppName,
               v_Comments := '',v_bitPublicFlag := false,v_numUserCntID := v_numUserCntID,v_monPAmount := 0,v_numAssignedTo := v_numAssignedTo,v_numDomainId := v_numDomainID,v_strItems := v_strItems,
               v_strMilestone := '', v_dtEstimatedCloseDate := v_dtEstimatedCloseDate,v_CampaignID := 0,v_lngPConclAnalysis := 0,v_tintOppType := 2::SMALLINT,v_tintActive := 0::SMALLINT,v_numSalesOrPurType := 0,v_numRecurringId := 0,v_numCurrencyID := v_numCurrencyID,
               v_DealStatus := v_tintOppStatus::SMALLINT,v_numStatus := v_numReOrderPointOrderStatus,v_vcOppRefOrderNo := NULL,v_numBillAddressId := 0,v_numShipAddressId := 0,
               v_bitStockTransfer := false,v_WebApiId := 0,v_tintSourceType := 1::SMALLINT,v_bitDiscountType := false,v_fltDiscount := 0,v_bitBillingTerms := v_bitBillingTerms,v_intBillingDays := v_intBillingDays,v_bitInterestType := v_bitInterestType,
               v_fltInterest := v_fltInterest,v_tintTaxOperator := 0::SMALLINT,v_numDiscountAcntType := 0,v_vcWebApiOrderNo := NULL,v_vcCouponCode := '',v_vcMarketplaceOrderID := NULL,v_vcMarketplaceOrderDate := NULL,v_vcMarketplaceOrderReportId := NULL,v_numPercentageComplete := NULL,v_bitUseShippersAccountNo := false,
               v_bitUseMarkupShippingRate := false,v_numMarkupShippingRate := 0,v_intUsedShippingCompany := v_intShippingCompany,v_numShipmentMethod := 0,v_dtReleaseDate := NULL,v_numPartner := 0,v_tintClickBtn := 0,v_numPartenerContactId := 0,v_numAccountClass := 0,
			   v_numWillCallWarehouseID := 0,v_numVendorAddressID := 0, v_numShipFromWarehouse := 0,v_numShippingService := v_numDefaultShippingServiceID,
               v_ClientTimeZoneOffset := 0,v_vcCustomerPO := '',v_bitDropShipAddress := false,v_PromCouponCode := null,v_numPromotionId := 0,v_dtExpectedDate := null,v_numProjectID := 0);

			   RAISE NOTICE '%',v_numNewOppID;

               PERFORM USP_OpportunityMaster_CT(v_numDomainID,v_numUserCntID,v_numNewOppID);
               EXCEPTION WHEN OTHERS THEN
                  -- ROLLBACK TRANSACTION
v_numOppID := 0;
                 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
            END;
         end if;

			-- ASSEMBLY ITEMS
         DELETE FROM tt_TEMPVENDORITEMS;
         INSERT INTO tt_TEMPVENDORITEMS(ID
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,numUnitHour
				,monCost)
         SELECT
         ROW_NUMBER() OVER(ORDER BY numItemCode ASC)
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,(CASE
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
         THEN(CASE WHEN coalesce(numReorderQty,0) > coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
         THEN(CASE
            WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) AND coalesce(numReorderQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numReorderQty,0)
            WHEN coalesce(numVendorMinQty,0) >= coalesce(numReorderQty,0) AND coalesce(numVendorMinQty,0) >= coalesce(numBackOrder,0) THEN coalesce(numVendorMinQty,0)
            WHEN coalesce(numBackOrder,0) >= coalesce(numReorderQty,0) AND coalesce(numBackOrder,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numBackOrder,0)
            ELSE coalesce(numReorderQty,0)
            END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
         THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)
         ELSE 0
         END)
				,monCost
         FROM
         tt_TEMPBACKORDERITEMS
         WHERE
         numVendorID = v_numTempVendorID
         AND coalesce(bitAssembly,false) = true
         AND coalesce(bitKit,false) = false
         AND(CASE
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
         THEN coalesce(numReorderQty,0)+coalesce(numBackOrder,0) -(coalesce(numOnOrder,0)+coalesce(numRequisitions,0))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
         THEN coalesce(numReorderQty,0) -(coalesce(numOnOrder,0)+coalesce(numRequisitions,0))
         WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
         THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce(numRequisitions,0) ELSE 0 END)) -coalesce(numBackOrder,0)+(CASE WHEN coalesce(numReorderQty,0) >= coalesce(numVendorMinQty,0) THEN coalesce(numReorderQty,0) ELSE coalesce(numVendorMinQty,0) END)
         ELSE 0
         END) > 0;
         IF(SELECT COUNT(*) FROM tt_TEMPVENDORITEMS) > 0 then
			
            v_k := 1;
            select   COUNT(*) INTO v_kCount FROM tt_TEMPVENDORITEMS;
            WHILE v_k <= v_kCount LOOP
               select   numItemCode, numWarehouseItemID, numUnitHour INTO v_numTempItemCode,v_numTempWarehouseItemID,v_numTempUnitHour FROM
               tt_TEMPVENDORITEMS WHERE
               ID = v_k;
               BEGIN
                  PERFORM USP_ManageWorkOrder(v_numTempItemCode,v_numTempWarehouseItemID,v_numTempUnitHour,v_numDomainID,
                  v_numUserCntID,'Auto created work order for back orders',NULL,v_numAssignedTo);
                  EXCEPTION WHEN OTHERS THEN
						--DO NOT THROW ERROR
                     NULL;
               END;
               v_k := v_k::bigint+1;
            END LOOP;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
   RETURN;
END; $$;


