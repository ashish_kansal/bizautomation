-- Stored procedure definition script usp_GetCommissionContactsReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCommissionContactsReport(v_numDivisionID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintComAppliesTo  SMALLINT;
BEGIN
   select   coalesce(tintComAppliesTo,0) INTO v_tintComAppliesTo FROM Domain WHERE numDomainId = v_numDomainID;

   if v_tintComAppliesTo = 1 then

      open SWV_RefCur for
      select I.vcItemName || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=1' AS textQueryGrp
      from CommissionReports cus JOIN Item I ON cus.numItemCode = I.numItemCode
      where cus.numDomainID = v_numDomainID  AND cus.bitCommContact = true AND cus.numContactID = v_numDivisionID;
   ELSEIF v_tintComAppliesTo = 2
   then

      open SWV_RefCur for
      select GetListIemName(numItemCode) || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=1' AS textQueryGrp
      from
      CommissionReports cus
      where cus.numDomainID = v_numDomainID  AND cus.bitCommContact = true AND cus.numContactID = v_numDivisionID;
   ELSEIF v_tintComAppliesTo = 3
   then

      open SWV_RefCur for
      select 'All Items' || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=1' AS textQueryGrp
      from
      CommissionReports cus
      where cus.numDomainID = v_numDomainID  AND cus.bitCommContact = true AND cus.numContactID = v_numDivisionID;
   end if;
   RETURN;
END; $$; 




