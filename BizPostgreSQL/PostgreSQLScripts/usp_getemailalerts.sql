-- Stored procedure definition script usp_getEmailAlerts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getEmailAlerts( 
v_intUserId NUMERIC(9,0),
	v_numDomainId NUMERIC(9,0),
	v_tintUserRightType SMALLINT DEFAULT 0,
	v_tintSortOrder SMALLINT DEFAULT 4,
	v_intLastDate INTEGER DEFAULT 0,
	v_SortChar CHAR(1) DEFAULT '0', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintSortOrder = 0 then   --All Contacts
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;

   IF v_tintSortOrder = 1 then   --My Contacts
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND  alertCreatedBy = v_intUserId
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;
	
   IF v_tintSortOrder = 2 then  --Employees
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND A.numContactType = 92
      AND (D.bitPublicFlag = false
      OR (D.bitPublicFlag = true
      AND
      D.numCreatedBy = v_intUserId))
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;
	
   IF (v_tintSortOrder = 3) then --Added in Last 7 days
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND alertCreatedDate > v_intLastDate
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;

   IF (v_tintSortOrder = 5) then  --Last 20 Added by me
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND alertCreatedDate > v_intLastDate
      AND alertCreatedBy = v_intUserId
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;

   IF v_tintSortOrder = 6 then  --Last 20 Modified by me
	
      open SWV_RefCur for
      SELECT E.*,vcFirstName || ' ' || vcLastname AS ContactName,bintDOB,D.numDivisionID,numCompanyID,vcEmail FROM EmailAlert E,AdditionalContactsInformation A,DivisionMaster D
      WHERE D.numDivisionID = A.numDivisionId AND E.numContactId = A.numContactId AND alertStatus = 0 AND (alertDeletedBy IS NULL OR alertDeletedBy = 0)
      AND D.numDomainID = v_numDomainId
      AND alertModifiedDate > v_intLastDate
      AND alertModifiedBy = v_intUserId
      AND A.vcFirstName ilike
      CASE
      WHEN v_SortChar = '0' THEN '%'
      ELSE coalesce(v_SortChar,'') || '%'
      END;
   end if;
   RETURN;
END; $$;


