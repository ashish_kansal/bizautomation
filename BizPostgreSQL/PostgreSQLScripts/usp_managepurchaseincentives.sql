-- Stored procedure definition script usp_ManagePurchaseIncentives for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManagePurchaseIncentives(v_numDivisionID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),
v_strXML VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   DELETE FROM PurchaseIncentives WHERE numDivisionId = v_numDivisionID;    
	
   INSERT INTO PurchaseIncentives(numDivisionId
		,numDomainID
		,vcBuyingQty
		,intType
		,vcIncentives
		,tintDiscountType)
   SELECT
		v_numDivisionID
		,v_numDomainID
		,X.BuyIncentives
		,X.IncentivesType
		,X.IncentiveDesc
		,X.DiscountType
	FROM
	XMLTABLE
	(
		'NewDataSet/PurchaseIncentives'
		PASSING 
			CAST(v_strXML AS XML)
		COLUMNS
			id FOR ORDINALITY,
			BuyIncentives DOUBLE PRECISION PATH 'BuyIncentives',
			IncentivesType INTEGER PATH 'IncentivesType',
			IncentiveDesc DOUBLE PRECISION PATH 'IncentiveDesc',
			DiscountType SMALLINT PATH 'DiscountType'
	) X;    

   RETURN;
END; $$;

