CREATE OR REPLACE FUNCTION USP_GetGenralJournalDetailsReport(v_numDomainID NUMERIC(18,0) ,
      v_dtFromDate TIMESTAMP ,
      v_dtToDate TIMESTAMP ,
      v_CurrentPage INTEGER DEFAULT 0 ,
      v_PageSize INTEGER DEFAULT 0 ,
      v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_first_id  NUMERIC;
   v_startRow  INTEGER;

   v_CurrRecord  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   IF v_tintMode = 0 then
            
      open SWV_RefCur for
      SELECT 
      GJH.numJOurnal_Id AS "numJOurnal_Id",
                        GJD.numTransactionId AS "numTransactionId",
                        FormatedDateFromDate(GJH.datEntry_Date,GJD.numDomainId) AS "Date" ,
                        COA.numAccountId AS "numAccountId",
                        coalesce(COA.vcAccountName,'') AS "vcAccountName",
                        coalesce(GJD.numDebitAmt,0) AS "numDebitAmt" ,
                        coalesce(GJD.numCreditAmt,0) AS "numCreditAmt"
      FROM    General_Journal_Header AS GJH
      JOIN General_Journal_Details AS GJD ON GJD.numJournalId = GJH.numJOurnal_Id
      AND GJD.numDomainId = GJH.numDomainId
      JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
      AND GJH.numDomainId = COA.numDomainId
      WHERE   GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN v_dtFromDate
      AND     v_dtToDate
      AND GJD.tintReferenceType = 5
      ORDER BY GJD.numEntryDateSortOrder1 ASC LIMIT 1;
   ELSEIF v_tintMode = 1
   then
                
      v_CurrRecord :=((v_CurrentPage::bigint -1)*v_PageSize::bigint)+1;
      RAISE NOTICE '%',v_CurrRecord;
                    
      select   GJD.numEntryDateSortOrder1 INTO v_first_id FROM    General_Journal_Header AS GJH
      JOIN General_Journal_Details AS GJD ON GJD.numJournalId = GJH.numJOurnal_Id
      AND GJD.numDomainId = GJH.numDomainId
      JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
      AND GJH.numDomainId = COA.numDomainId WHERE   GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN v_dtFromDate
      AND     v_dtToDate
      AND GJD.tintReferenceType = 5   ORDER BY GJD.numEntryDateSortOrder1 ASC;
      RAISE NOTICE '%',v_first_id;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      RAISE NOTICE '%',SWV_RowCount;
                    
      open SWV_RefCur for
      SELECT  GJD.numJournalId AS "numJournalId",
                            GJD.numTransactionId AS "numTransactionId",
                            FormatedDateFromDate(GJH.datEntry_Date,GJD.numDomainId) AS "Date" ,
                            COA.numAccountId AS "numAccountId",
                            coalesce(COA.vcAccountName,'') AS "vcAccountName" ,
                            coalesce(GJD.numDebitAmt,0) AS "numDebitAmt" ,
                            coalesce(GJD.numCreditAmt,0) AS "numCreditAmt"
      FROM    General_Journal_Header AS GJH
      JOIN General_Journal_Details AS GJD ON GJD.numJournalId = GJH.numJOurnal_Id
      AND GJD.numDomainId = GJH.numDomainId
      JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
      AND GJH.numDomainId = COA.numDomainId
      WHERE   GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN v_dtFromDate
      AND     v_dtToDate
      AND GJD.tintReferenceType = 5
      AND GJD.numEntryDateSortOrder1 >= v_first_id
      ORDER BY GJH.datEntry_Date,GJD.numJournalId ASC;
   ELSEIF v_tintMode = 3
   then
                    
      open SWV_RefCur for
      SELECT  FormatedDateFromDate(GJH.datEntry_Date,GJD.numDomainId) AS "Date" ,
                                COA.numAccountId AS "AccountID",
                                CASE WHEN coalesce(GJD.numDebitAmt,0) = 0 THEN coalesce(COA.vcAccountName,'') ELSE '' END AS "Reference" ,
								CASE WHEN coalesce(GJD.numCreditAmt,0) = 0 THEN coalesce(COA.vcAccountName,'')  ELSE '' END AS "TransDesc" ,
                                coalesce(GJD.numDebitAmt,0) AS "Debit" ,
                                coalesce(GJD.numCreditAmt,0) AS "Credit"
      FROM    General_Journal_Header AS GJH
      JOIN General_Journal_Details AS GJD ON GJD.numJournalId = GJH.numJOurnal_Id
      AND GJD.numDomainId = GJH.numDomainId
      JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
      AND GJH.numDomainId = COA.numDomainId
      WHERE   GJH.numDomainId = v_numDomainID
      AND GJH.datEntry_Date BETWEEN v_dtFromDate
      AND v_dtToDate
      AND GJD.tintReferenceType = 5
      ORDER BY GJD.numEntryDateSortOrder1 ASC;
   end if;
   RETURN;
END; $$;


