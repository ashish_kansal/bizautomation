-- Stored procedure definition script USP_OpportunityMaster_GlobalLocationFulfilled for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GlobalLocationFulfilled(v_numDomainID NUMERIC(18,0),
v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numOppId = v_numOppID
   AND WI.numWLocationID = -1
   AND OI.numUnitHour <> coalesce(OI.numUnitHourReceived,0)) > 0 then
	
      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/



