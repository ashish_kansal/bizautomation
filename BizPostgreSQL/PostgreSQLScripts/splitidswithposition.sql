-- Function definition script SplitIDsWithPosition for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--SELECT * FROM dbo.SplitIDsWithPosition('197605,197606,197595,197598', ',')
CREATE OR REPLACE FUNCTION SplitIDsWithPosition(v_IdList VARCHAR(8000),
	v_Delimiter CHAR(1) DEFAULT '/')
RETURNS table
(
   RowNumber INTEGER,
   Id INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Id  VARCHAR(10);
   v_Pos  INTEGER;
   v_rownumber  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_SPLITIDSWITHPOSITION_PARSEDLIST CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_SPLITIDSWITHPOSITION_PARSEDLIST
   (
      RowNumber INTEGER,
      Id INTEGER
   );
   v_IdList := LTRIM(RTRIM(v_IdList)) || coalesce(v_Delimiter,'');
   v_Pos := POSITION(v_Delimiter IN v_IdList);
   v_rownumber := 0;
   IF REPLACE(v_IdList,v_Delimiter,'') <> '' then
	
      WHILE v_Pos > 0 LOOP
         v_Id := LTRIM(RTRIM(SUBSTR(v_IdList,1,v_Pos -1)));
         IF v_Id <> '' then
			
            v_rownumber := v_rownumber::bigint+1;
            INSERT INTO tt_SPLITIDSWITHPOSITION_PARSEDLIST(RowNumber,Id)
				VALUES(v_rownumber ,case when v_Id = '' then 0 else v_Id:: INTEGER end);
         end if;
         v_IdList := SUBSTR(v_IdList,length(v_IdList) -(LENGTH(v_IdList) -v_Pos)+1);
         v_Pos := POSITION(v_Delimiter IN v_IdList);
      END LOOP;
   end if;	
   RETURN QUERY (SELECT * FROM tt_SPLITIDSWITHPOSITION_PARSEDLIST);
END; $$;

