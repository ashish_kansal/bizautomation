-- Stored procedure definition script USP_ContractDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ContractDetails(v_numContractId NUMERIC(18,0),
v_numDomainID NUMERIC(18,0),
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
	C.numContractId,
	C.numIncidents,
	coalesce(CI.vcCompanyName,'') AS vcCompanyName,
	C.numDivisonId,
	(coalesce(C.numIncidents,0)  -coalesce(C.numIncidentsUsed,0)) AS numIncidentLeft,
	C.numHours,
	C.numMinutes,
	fn_SecondsConversion((((coalesce(C.numHours,0)*60*60)+(coalesce(C.numMinutes,0)*60)) -coalesce((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId = C.numContractId),0))::INT) AS timeLeft,
	C.vcItemClassification,
	C.numWarrantyDays,
	C.vcNotes,
	C.vcContractNo,
	C.timeUsed,
	fn_GetContactName(coalesce(C.numCreatedBy,0)) AS CreatedBy,
	FormatedDateFromDate(C.dtmCreatedOn+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID) AS CreatedOn,
	fn_GetContactName(coalesce(C.numModifiedBy,0)) AS ModifiedBy,
	coalesce(FormatedDateFromDate(C.dtmModifiedOn+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID),'')  AS ModifiedOn
   FROM
   Contracts AS C
   LEFT JOIN
   DivisionMaster AS D
   ON
   C.numDivisonId = D.numDivisionID
   LEFT JOIN
   CompanyInfo AS CI
   ON
   D.numCompanyID = CI.numCompanyId
   WHERE
   C.numDomainId = v_numDomainID AND
   C.numContractId = v_numContractId;
END; $$;













