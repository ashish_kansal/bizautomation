DROP FUNCTION IF EXISTS usp_item_createnewitemfromkitconfiguration;

CREATE OR REPLACE FUNCTION usp_item_createnewitemfromkitconfiguration
(
	p_numDomainID NUMERIC(18,0),
	p_numUserCntID NUMERIC(18,0),
	p_numItemCode NUMERIC(18,0),
	p_vcCurrentKitConfiguration TEXT,
	p_bitAssembly BOOLEAN,
	p_bitKitParent BOOLEAN,
	INOUT p_numNewItemCode NUMERIC(18,0)
)
LANGUAGE plpgsql
   AS $$
	DECLARE 
		v_vcKitItemName TEXT;
		v_SelectedChildCodes TEXT;
		v_SKU TEXT;
		v_monListPrice DECIMAL(20,5);
		v_bitTaxable BOOLEAN;
		v_bitCalAmtBasedonDepItems BOOLEAN;
		v_numItemClassification NUMERIC(18,0);
		v_strChildItems TEXT;
		v_numCOGSChartAcntId NUMERIC(18,0);                                   
		v_numAssetChartAcntId NUMERIC(18,0);                            
		v_numIncomeChartAcntId NUMERIC(18,0);
		my_ex_state TEXT;
		my_ex_message TEXT;
		my_ex_detail TEXT;
		my_ex_hint TEXT;
		my_ex_ctx TEXT;
BEGIN
	SELECT COALESCE(vcItemName,'') INTO v_vcKitItemName FROM Item WHERE numItemCode = p_numItemCode;

	DROP TABLE IF EXISTS tt_TEMPKITCONFIGURATION CASCADE;
    CREATE TEMPORARY TABLE tt_TEMPKITCONFIGURATION
    (
		numItemDetailID NUMERIC(18,0),
		numKitItemID NUMERIC(18,0),
		numKitChildItemID NUMERIC(18,0),
		numQtyItemsReq NUMERIC,
		vcItemDesc TEXT,
		numUOMId NUMERIC(18,0),
		sintOrder INT,
		tintView SMALLINT,
		bitUseInDynamicSKU BOOLEAN
    );

	INSERT INTO tt_TEMPKITCONFIGURATION
	(
		numItemDetailID,
		numKitItemID,
		numKitChildItemID,
		numQtyItemsReq,
		vcItemDesc,
		numUOMId,
		sintOrder,
		tintView,
		bitUseInDynamicSKU
	)
	SELECT
		ItemDetails.numItemDetailID
		,ItemDetails.numItemKitID
		,ItemDetails.numChildItemID
		,ItemDetails.numQtyItemsReq
		,ItemDetails.vcItemDesc
		,ItemDetails.numUOMId
		,ItemDetails.sintOrder
		,ItemDetails.tintView
		,ItemDetails.bitUseInDynamicSKU
	FROM
		ItemDetails
	WHERE
		ItemDetails.numItemKitID = p_numItemCode
		AND coalesce((SELECT COUNT(*) FROM ItemDetails IDInner WHERE IDInner.numItemKitID = ItemDetails.numChildItemID),0) = 0
	ORDER BY
		ItemDetails.sintOrder
		,ItemDetails.numItemDetailID;


	INSERT INTO tt_TEMPKITCONFIGURATION
	(
		numItemDetailID,
		numKitItemID,
		numKitChildItemID,
		numQtyItemsReq,
		vcItemDesc,
		numUOMId,
		sintOrder,
		tintView,
		bitUseInDynamicSKU
	)
	SELECT
		ItemDetails.numItemDetailID
		,ItemDetails.numItemKitID
		,ItemDetails.numChildItemID
		,coalesce((SELECT numQtyItemsReq FROM ItemDetails IDInner WHERE IDInner.numItemKitID = p_numItemCode AND IDInner.numChildItemID = ItemDetails.numItemKitID),1) * ItemDetails.numQtyItemsReq
		,ItemDetails.vcItemDesc
		,ItemDetails.numUOMId
		,coalesce((SELECT IDInner.sintOrder FROM ItemDetails IDInner WHERE IDInner.numItemKitID = p_numItemCode AND IDInner.numChildItemID = ItemDetails.numItemKitID),1) sintOrder
		,ItemDetails.tintView
		,ItemDetails.bitUseInDynamicSKU
	FROM
		ItemDetails
	INNER JOIN
	(
        SELECT
			split_part(OutParam,'-',1)::NUMERIC As numKitItemID
			,split_part(OutParam,'-',2)::NUMERIC As numChildItemID
			,CASE WHEN COALESCE(split_part(OutParam,'-',4),'') = '' THEN '0' ELSE split_part(OutParam,'-',4)::NUMERIC END As numSequence
        FROM
		(
			SELECT
				OutParam
            FROM
				SplitString(p_vcCurrentKitConfiguration,',')
		) X 
	) Y
	ON 
		ItemDetails.numItemKitID = Y.numKitItemID 
		AND ItemDetails.numChildItemID= Y.numChildItemID;

	v_SelectedChildCodes := coalesce((SELECT string_agg(numKitChildItemID::VARCHAR,',' ORDER BY numKitChildItemID) FROM tt_TEMPKITCONFIGURATION),'');


	IF EXISTS (
				SELECT 
					I.numItemCode
				FROM 
					Item I 
				WHERE 
					I.numDomainID=p_numDomainID 
					AND I.numItemClass <> p_numItemCode 
					AND (coalesce(I.bitKitParent,false) = true or coalesce(I.bitassembly,false) = true)
					AND coalesce((SELECT string_agg(ID.numChildItemID::VARCHAR,',' ORDER BY ID.numChildItemID) FROM ItemDetails ID WHERE ID.numItemKitID = I.numItemCode),'') = v_SelectedChildCodes)
	
	THEN
		RAISE EXCEPTION '%', ('Item with name "' || (SELECT 
														I.vcItemName
													FROM 
														Item I 
													WHERE 
														I.numDomainID=p_numDomainID 
														AND I.numItemClass <> p_numItemCode 
														AND (coalesce(I.bitKitParent,false) = true or coalesce(I.bitassembly,false) = true)
														AND coalesce((SELECT string_agg(ID.numChildItemID::VARCHAR,',' ORDER BY ID.numChildItemID) FROM ItemDetails ID WHERE ID.numItemKitID = I.numItemCode),'') = v_SelectedChildCodes
													LIMIT 1) || '" already exist with same configutation.');
        RETURN;
	ELSE
		IF p_bitKitParent THEN
			v_SKU := COALESCE((SELECT 
									string_agg(vcChildItems,'-' ORDER BY T1.sintOrder)  
								FROM (SELECT 
											T.numKitItemID
											,T.sintOrder
											,COALESCE((SELECT 
															string_agg(I.vcItemName,',' ORDER BY coalesce(TInner.sintOrder,0), TInner.numItemDetailID)
														FROM 
															tt_TEMPKITCONFIGURATION TInner
														INNER JOIN
															Item I 
														ON
															TInner.numKitChildItemID=I.numItemCode
														WHERE
															TInner.numKitItemID = T.numKitItemID
															AND COALESCE((SELECT bitUseInDynamicSKU FROM ItemDetails ID WHERE ID.numItemKitID = p_numItemCode AND ID.numChildItemID = T.numKitItemID),false) = true),'') AS vcChildItems
										 FROM 
											tt_TEMPKITCONFIGURATION T
										GROUP BY
											T.numKitItemID,T.sintOrder) T1 WHERE COALESCE(T1.vcChildItems,'') <> ''),'');

			RAISE NOTICE '%',v_SKU;
		END IF;

		SELECT 
			monListPrice
			,bitTaxable
			,bitCalAmtBasedonDepItems
			,numItemClassification
			,numCOGSChartAcntId                            
			,numAssetChartAcntId             
			,numIncomeChartAcntId
		INTO 
			v_monListPrice
			,v_bitTaxable
			,v_bitCalAmtBasedonDepItems
			,v_numItemClassification
			,v_numCOGSChartAcntId                            
			,v_numAssetChartAcntId             
			,v_numIncomeChartAcntId
		FROM 
			Item 
		WHERE 
			numItemCode = p_numItemCode;

		BEGIN
			SELECT v_numItemCode FROM USP_ManageItemsAndKits(v_numItemCode :=  p_numNewItemCode,                                                                
												v_vcItemName := (CASE WHEN coalesce(v_SKU,'') = '' THEN CONCAT('New Item ',TO_CHAR(now(),'yyyymmddhhmmssms')) ELSE CONCAT(v_vcKitItemName,'-', v_SKU) END),                                                                
												v_txtItemDesc := '',                                                                
												v_charItemType := 'P',                                                                
												v_monListPrice := v_monListPrice,                                                                
												v_numItemClassification := v_numItemClassification,                                                                
												v_bitTaxable := v_bitTaxable,                                                                
												v_vcSKU := CONCAT(v_vcKitItemName,'-', v_SKU),                                                                                                                                      
												v_bitKitParent := p_bitKitParent,                                                                                                                                               
												v_numDomainID := p_numDomainID,                                                                
												v_numUserCntID := p_numUserCntID,  
												v_bitSerialized := false,   
												v_numVendorID := 0,                 
												v_strFieldList := '', 
												v_bitCalAmtBasedonDepItems := v_bitCalAmtBasedonDepItems,    
												v_bitAssembly := p_bitAssembly,
												v_numCOGSChartAcntId := v_numCOGSChartAcntId,                                      
												v_numAssetChartAcntId := v_numAssetChartAcntId,                                      
												v_numIncomeChartAcntId :=  v_numIncomeChartAcntId) INTO p_numNewItemCode;

			RAISE NOTICE '%',p_numNewItemCode;

			INSERT INTO ItemDetails
			(
				numItemKitID
				,numChildItemID
				,numQtyItemsReq
				,vcItemDesc
				,numUOMId
				,sintOrder
				,tintView
			)
			SELECT
				p_numNewItemCode,
				numKitChildItemID,
				numQtyItemsReq,
				vcItemDesc,
				numUOMId,
				sintOrder,
				tintView
			FROM
				tt_TEMPKITCONFIGURATION
			ORDER BY
				coalesce(sintOrder,0), numItemDetailID;

		EXCEPTION WHEN OTHERS THEN
			GET STACKED DIAGNOSTICS
				  my_ex_state   = RETURNED_SQLSTATE,
				  my_ex_message = MESSAGE_TEXT,
				  my_ex_detail  = PG_EXCEPTION_DETAIL,
				  my_ex_hint    = PG_EXCEPTION_HINT,
				  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
		END;

	END IF;
  
END; $$;