-- Stored procedure definition script usp_GetSurveyRating for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyRating(v_numDomainID NUMERIC,       
 v_numSurId NUMERIC,
 v_numSurRating NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM SurveyCreateRecord WHERE
   numSurID = v_numSurId AND (numRatingMin <= v_numSurRating AND numRatingMax >= v_numSurRating) LIMIT 1;
END; $$;












