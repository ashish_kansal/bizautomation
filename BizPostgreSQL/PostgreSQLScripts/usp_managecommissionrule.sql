-- Stored procedure definition script USP_ManageCommissionRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCommissionRule(v_numComRuleID NUMERIC,
    v_vcCommissionName VARCHAR(100),
    v_tintComAppliesTo SMALLINT,
    v_tintComBasedOn SMALLINT,
    v_tinComDuration SMALLINT,
    v_tintComType SMALLINT,
    v_numDomainID NUMERIC,
    v_strItems TEXT,
    v_tintAssignTo SMALLINT,
    v_tintComOrgType SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_isCommissionRuleUsed  BOOLEAN DEFAULT 0;
   v_hDocItem  INTEGER;
   v_bitItemDuplicate  BOOLEAN DEFAULT false;
   v_bitOrgDuplicate  BOOLEAN DEFAULT false;
   v_bitContactDuplicate  BOOLEAN DEFAULT false;
BEGIN
   -- BEGIN TRANSACTION
IF v_numComRuleID = 0 then
    
      INSERT  INTO CommissionRules(vcCommissionName,
					numDomainID)
		VALUES(v_vcCommissionName,
					v_numDomainID);
		
      v_numComRuleID := CURRVAL('CommissionRules_seq');
      open SWV_RefCur for
      Select v_numComRuleID;
   ELSE
      IF(SELECT COUNT(*) FROM BizDocComission WHERE numDomainId = v_numDomainID AND numComRuleID = v_numComRuleID) > 0 then
		
         v_isCommissionRuleUsed := true;
      end if;
      UPDATE
      CommissionRules
      SET
      vcCommissionName = v_vcCommissionName,tintComBasedOn = CASE WHEN v_isCommissionRuleUsed = true THEN tintComBasedOn ELSE v_tintComBasedOn END,tinComDuration = v_tinComDuration,
      tintComType = CASE WHEN v_isCommissionRuleUsed = true THEN tintComType ELSE v_tintComType END,tintComAppliesTo = v_tintComAppliesTo,
      tintComOrgType = v_tintComOrgType,tintAssignTo = v_tintAssignTo
      WHERE
      numComRuleID = v_numComRuleID
      AND numDomainID = v_numDomainID;
      IF v_tintComAppliesTo = 3 then
		
         DELETE FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID;
      end if;
      DELETE FROM CommissionRuleContacts WHERE numComRuleId = v_numComRuleID;
    
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
         IF v_isCommissionRuleUsed = false then
			
            DELETE FROM CommissionRuleDtl WHERE numComRuleID = v_numComRuleID;
            INSERT  INTO CommissionRuleDtl(numComRuleID,
							intFrom,
							intTo,
							decCommission)
            SELECT  v_numComRuleID,
								X.intFrom,
								X.intTo,
								X.decCommission
            FROM
			 XMLTABLE
			(
				'NewDataSet/CommissionTable'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					intFrom DECIMAL(18,2) PATH 'intFrom',
					intTo DECIMAL(18,2) PATH 'intTo',
					decCommission DECIMAL(18,2) PATH 'decCommission'
			) AS X;
         end if;

        INSERT INTO CommissionRuleContacts
		(
			numComRuleId,
            numValue,
			bitCommContact
		)
        SELECT 
			v_numComRuleID,
            X.numValue,
			X.bitCommContact
        FROM
		 XMLTABLE
		(
			'NewDataSet/ContactTable'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numValue NUMERIC PATH 'numValue',
				bitCommContact BOOLEAN PATH 'bitCommContact'
		) AS X;

      end if;
		

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
      IF(SELECT COUNT(*) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID) > 0 then
		
			-- ITEM
         IF v_tintComAppliesTo = 1 then
			
				-- ITEM ID
            IF(SELECT
            COUNT(*)
            FROM
            CommissionRuleItems
            WHERE
            tintType = 1
            AND numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID)
            AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID)) > 0 then
				
               v_bitItemDuplicate := true;
            end if;
         ELSEIF v_tintComAppliesTo = 2
         then
			
				-- ITEM CLASSIFICATION
            IF(SELECT
            COUNT(*)
            FROM
            CommissionRuleItems
            WHERE
            tintType = 2
            AND numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID)
            AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID)) > 0 then
				
               v_bitItemDuplicate := true;
            end if;
         ELSEIF v_tintComAppliesTo = 3
         then
			
				-- ALL ITEMS
            v_bitItemDuplicate := true;
         end if;

			--ORGANIZATION
         IF v_tintComOrgType = 1 then
			
				-- ORGANIZATION ID
            IF(SELECT
            COUNT(*)
            FROM
            CommissionRuleOrganization
            WHERE
            numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID)
            AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID)) > 0 then
				
               v_bitOrgDuplicate := true;
            end if;
         ELSEIF v_tintComOrgType = 2
         then
			
				-- ORGANIZATION RELATIONSHIP OR PROFILE
            IF(SELECT
            COUNT(*)
            FROM
            CommissionRuleOrganization
            WHERE
            numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID)
            AND numProfile IN(SELECT coalesce(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID)
            AND numComRuleID IN(SELECT coalesce(numComRuleID,0) FROM CommissionRules WHERE numDomainID = v_numDomainID AND tintComAppliesTo = v_tintComAppliesTo AND tintComOrgType = v_tintComOrgType AND tintAssignTo = v_tintAssignTo AND numComRuleID <> v_numComRuleID)) > 0 then
				
               v_bitOrgDuplicate := true;
            end if;
         ELSEIF v_tintComOrgType = 3
         then
			
				-- ALL ORGANIZATIONS
            v_bitOrgDuplicate := true;
         end if;

			--CONTACT
         IF(SELECT
         COUNT(*)
         FROM
         CommissionRuleContacts
         WHERE
         numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleContacts WHERE numComRuleId = v_numComRuleID)
         AND numComRuleId IN(SELECT
            coalesce(numComRuleID,0)
            FROM
            CommissionRules
            WHERE
            numDomainID = v_numDomainID
            AND tintComAppliesTo = v_tintComAppliesTo
            AND 1 =(CASE tintComAppliesTo
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType = 1 AND numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleId) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType = 2 AND numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleId) > 0 THEN 1 ELSE 0 END)
            WHEN 3 THEN 1
            END)
            AND tintComOrgType = v_tintComOrgType
            AND 1 =(CASE tintComOrgType
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleId) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN(SELECT coalesce(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID) AND numProfile IN(SELECT coalesce(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleId) > 0 THEN 1 ELSE 0 END)
            WHEN 3 THEN 1
            END)
            AND tintAssignTo = v_tintAssignTo
            AND numComRuleID <> v_numComRuleID)) > 0 then
			
            v_bitContactDuplicate := true;
         end if;
      end if;
      IF (coalesce(v_bitItemDuplicate,false) = true AND coalesce(v_bitOrgDuplicate,false) = true AND coalesce(v_bitContactDuplicate,false) = true) then
		
         RAISE EXCEPTION 'DUPLICATE';
         RETURN;
      end if;
      open SWV_RefCur for
      SELECT  v_numComRuleID;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


