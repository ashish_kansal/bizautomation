-- Stored procedure definition script USP_GetState for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetState(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numCountryID NUMERIC(9,0) DEFAULT 0,
v_vcAbbreviations VARCHAR(300) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(coalesce(v_vcAbbreviations,'')) > 0 AND coalesce(v_numCountryID,0) > 0 then
	
      open SWV_RefCur for
      SELECT
      numStateID AS "numStateID"
      FROM
      State
      JOIN
      Listdetails
      ON
      numListItemID = numCountryID
      WHERE
      State.numDomainID = v_numDomainID
      AND numCountryID = v_numCountryID
      AND v_vcAbbreviations IN(vcAbbreviations,vcState)
      ORDER BY
      vcState;
   ELSEIF LENGTH(v_vcAbbreviations) > 0 AND coalesce(v_numCountryID,0) = 0
   then
	
      open SWV_RefCur for
      SELECT
      numStateID AS "numStateID"
			,numCountryID AS "numCountryID" 
			,vcState AS "vcState"
      FROM
      State
      JOIN
      Listdetails
      ON
      numListItemID = numCountryID
      WHERE
      State.numDomainID = v_numDomainID
      AND (LOWER(coalesce(vcAbbreviations,'')) = LOWER(v_vcAbbreviations) OR LOWER(coalesce(vcState,'')) = LOWER(v_vcAbbreviations))
      ORDER BY
      vcState;
   ELSE
      open SWV_RefCur for
      SELECT
      numStateID AS "numStateID"
			,vcState AS "vcState"
			,numCountryID AS "numCountryID"
			,LD.vcData AS "vcData"
			,vcAbbreviations AS "vcAbbreviations"
			,coalesce(numShippingZone,0) AS "numShippingZone"
			,coalesce(LDShippingZone.vcData,'') AS "vcShippingZone"
      FROM
      State
      JOIN
      Listdetails LD
      ON
      LD.numListItemID = numCountryID
      LEFT JOIN
      Listdetails LDShippingZone
      ON
      State.numShippingZone = LDShippingZone.numListItemID
      WHERE
      State.numDomainID = v_numDomainID
      AND numCountryID = v_numCountryID
      ORDER BY
      vcState;
   end if;
   RETURN;
END; $$;


