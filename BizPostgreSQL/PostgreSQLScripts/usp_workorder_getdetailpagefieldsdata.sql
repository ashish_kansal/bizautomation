-- Stored procedure definition script USP_WorkOrder_GetDetailPageFieldsData for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_GetDetailPageFieldsData(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		WorkOrder.numWOId AS "numWOId"
		,coalesce(OpportunityMaster.numOppId,0) AS "numOppId"
		,(CASE
		   WHEN OpportunityMaster.numOppId IS NOT NULL
		   THEN CONCAT('<a href="../opportunity/frmOpportunities.aspx?frm=deallist&amp;OpID=',OpportunityMaster.numOppId,'">',OpportunityMaster.vcpOppName,' (',coalesce(CompanyInfo.vcCompanyName,'-'),
			  ')</a>')
		   ELSE ''
		END) AS "vcPoppName"
		,coalesce(WorkOrder.numAssignedTo,0) AS "numAssignedTo"
		,fn_GetContactName(WorkOrder.numAssignedTo) AS "numAssignedToName"
		,coalesce(Warehouses.vcWareHouse,'') AS "vcWarehouse"
		,coalesce(WorkOrder.vcInstruction,'') AS "vcInstruction"
		,WorkOrder.dtmStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS "dtmStartDate"
		,WorkOrder.dtmEndDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS "dtmEndDate"
		,(SELECT MIN(SPDTTL.dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId IN(SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = WorkOrder.numWOId) AND tintAction = 1)+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS "dtActualStartDate"
		,GetProjectedFinish(v_numDomainID,2::SMALLINT,v_numWOID,0,0,NULL,v_ClientTimeZoneOffset,0) AS "vcProjectedFinish"
		,'<label class="lblWorkOrderStatus"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>' AS "vcWorkOrderStatus"
		,coalesce(WorkOrder.numBuildPlan,0) AS "numBuildPlan"
		,GetListIemName(WorkOrder.numBuildPlan) AS "numBuildPlanName"
		,WorkOrder.numQtyItemsReq AS "numQtyItemsReq"
   FROM
   WorkOrder
   INNER JOIN
   WareHouseItems
   ON
   WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   OpportunityMaster
   ON
   WorkOrder.numOppId = OpportunityMaster.numOppId
   LEFT JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   LEFT JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID;
END; $$;












