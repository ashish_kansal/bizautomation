-- Stored procedure definition script usp_GetAutoComCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAutoComCompany(                            
--                          
v_numDomainId NUMERIC DEFAULT 0,                                                                   
 v_numUserCntID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                               
-- checking the company rights                      
                      
   AS $$
   DECLARE
   v_ProspectsRights  SMALLINT;
   v_accountsRights  SMALLINT;                     
   v_leadsRights  SMALLINT;

   v_strSQL  VARCHAR(8000);
BEGIN
   SELECT 
   MAX(GA.intViewAllowed) As intViewAllowed INTO v_ProspectsRights FROM
   UserMaster UM
   INNER JOIN
   GroupAuthorization GA
   ON
   UM.numGroupID = GA.numGroupID
   INNER JOIN
   PageMaster PM
   ON
   PM.numPageID = GA.numPageID
   AND PM.numModuleID = GA.numModuleID
   AND PM.numPageID = 2
   AND PM.numModuleID = 3 WHERE
   UM.numUserId =(SELECT numUserId FROM UserMaster WHERE numUserDetailId = v_numUserCntID) GROUP BY
   UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                          
                      
   SELECT 
   MAX(GA.intViewAllowed) As intViewAllowed INTO v_accountsRights FROM
   UserMaster UM
   INNER JOIN
   GroupAuthorization GA
   ON
   UM.numGroupID = GA.numGroupID
   INNER JOIN
   PageMaster PM
   ON
   PM.numPageID = GA.numPageID
   AND PM.numModuleID = GA.numModuleID
   AND PM.numPageID = 2
   AND PM.numModuleID = 4 WHERE
   UM.numUserId =(SELECT numUserId FROM UserMaster WHERE numUserDetailId = v_numUserCntID) GROUP BY
   UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                  
                
   SELECT 
   MAX(GA.intViewAllowed) As intViewAllowed INTO v_leadsRights FROM
   UserMaster UM
   INNER JOIN
   GroupAuthorization GA
   ON
   UM.numGroupID = GA.numGroupID
   INNER JOIN
   PageMaster PM
   ON
   PM.numPageID = GA.numPageID
   AND PM.numModuleID = GA.numModuleID
   AND PM.numPageID = 2
   AND PM.numModuleID = 2 WHERE
   UM.numUserId =(select numUserId from UserMaster where numUserDetailId = v_numUserCntID) GROUP BY
   UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                     
                      
   v_strSQL := 'SELECT a.vcCompanyName,d.numDivisionID,a.numCompanyType                             
   FROM CompanyInfo a                        
   join DivisionMaster d                        
   on  a.numCompanyId = d.numCompanyID                        
   WHERE a.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType <> 0 and d.tintCRMType = 1';
   if v_ProspectsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner = 0 ';
   end if;                      
   if v_ProspectsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                       
   if v_ProspectsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in(select numTerritoryID  from UserTerritory where numUserCntID =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID = 0)';
   end if;                      
   v_strSQL := coalesce(v_strSQL,'') || ' union SELECT a.vcCompanyName,d.numDivisionID,a.numCompanyType                             
   FROM CompanyInfo a                        
   join DivisionMaster d                        
   on  a.numCompanyId = d.numCompanyID                        
   WHERE a.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType <> 0 and d.tintCRMType = 2';                   
   if v_accountsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner = 0 ';
   end if;                      
   if v_accountsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                       
   if v_accountsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in(select numTerritoryID  from UserTerritory where numUserCntID =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID = 0)';
   end if;                   
   v_strSQL := coalesce(v_strSQL,'') || ' union SELECT a.vcCompanyName,d.numDivisionID,a.numCompanyType                             
   FROM CompanyInfo a                        
   join DivisionMaster d              
   on  a.numCompanyId = d.numCompanyID                        
   WHERE a.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType = 0';                    
   if v_leadsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner = 0 ';
   end if;                      
   if v_leadsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                       
   if v_leadsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in(select numTerritoryID  from UserTerritory where numUserCntID =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID = 0)';
   end if;                     
   v_strSQL := coalesce(v_strSQL,'') || ' ORDER BY 1';                                       
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


