-- Stored procedure definition script USP_CreateUsers for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateUsers(v_vcUserName VARCHAR(100),                
v_vcDomain VARCHAR(100),                
v_noOfLicences NUMERIC(9,0),          
v_vcEmailID VARCHAR(100),        
v_vcDisplayName VARCHAR(100),        
v_vcMailName VARCHAR(100), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numUserId  NUMERIC(9,0);                
   v_numDomainID  NUMERIC(9,0);      
   v_numResourceId  NUMERIC(9,0);
   v_UserId  INTEGER;
BEGIN
   select numUserId INTO v_numUserId from UserMaster where vcMailNickName = v_vcMailName;                
   select ResourceID INTO v_numResourceId from Resource where ResourceName = v_vcMailName;                
   select numDomainId INTO v_numDomainID from Domain where vcDomainName = v_vcDomain;              
   if v_noOfLicences >(select count(*) from UserMaster where bitactivateflag = true) then
 
      if not exists(select numUserId from UserMaster where vcMailNickName = v_vcMailName) then
         insert into UserMaster(vcUserName,
      vcUserDesc,
      vcMailNickName,
      numGroupID,
      numDomainID,
      numCreatedBy,
      bintCreatedDate,
      numModifiedBy,
      bintModifiedDate,
      bitactivateflag,
      numUserDetailId,
      vcEmailID)
  values(v_vcUserName,
     v_vcDisplayName,
     v_vcMailName,
     0,
     v_numDomainID,
     1,
     TIMEZONE('UTC',now()),
     1,
     TIMEZONE('UTC',now()),
     1,0,v_vcEmailID);
		
         v_UserId := CURRVAL('UserMaster_seq');
         PERFORM Resource_Add(v_ResourceName := v_vcUserName,v_ResourceDesc := vcUserDesc,v_ResourceEmail := v_vcEmailID,
         v_EnableEmailReminders := 1,v_DomainId := v_numDomainID,
         v_numUserCntId := v_UserId);
      else
         update UserMaster
         set bitactivateflag = true,numDomainID = v_numDomainID,vcEmailID = v_vcEmailID,vcUserDesc = v_vcDisplayName,
         vcMailNickName = v_vcMailName,vcUserName =  v_vcUserName
         where numUserId = v_numUserId;
         PERFORM Resource_Upd(v_ResourceID := v_numResourceId,v_ResourceName := v_vcUserName,v_ResourceDesc := vcUserDesc,
         v_ResourceEmail := v_vcEmailID,v_EnableEmailReminders := 1);
      end if;
      open SWV_RefCur for
      select Count(*) from UserMaster where bitactivateflag = true and numDomainID = v_numDomainID;
   else 
      open SWV_RefCur for
      select -1;
   end if;
   RETURN;
END; $$;


