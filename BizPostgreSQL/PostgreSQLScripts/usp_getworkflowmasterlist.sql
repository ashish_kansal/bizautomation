-- Stored procedure definition script USP_GetWorkFlowMasterList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetWorkFlowMasterList(v_numDomainId NUMERIC(9,0),  
v_numFormID NUMERIC(18,0), 
v_CurrentPage INTEGER,                                                        
v_PageSize INTEGER,                                                        
INOUT v_TotRecs INTEGER ,     
v_SortChar CHAR(1) DEFAULT '0' ,                                                       
v_columnName VARCHAR(50) DEFAULT NULL,                                                        
v_columnSortOrder VARCHAR(50) DEFAULT NULL  ,
v_SearchStr  VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                  
    
   v_firstRec  INTEGER;                                                        
   v_lastRec  INTEGER;
BEGIN

     
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numWFID NUMERIC(18,0)
   );     
   v_strSql := 'Select numWFID from WorkFlowMaster where numdomainid =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15); 
    
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcWFName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;     

   if v_numFormID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And numFormID = ' || SUBSTR(CAST(v_numFormID AS VARCHAR(15)),1,15) || '';
   end if;   

   if v_SearchStr <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' And (vcWFName ilike ''%' || coalesce(v_SearchStr,'') || '%'' or 
vcWFDescription ilike ''%' || coalesce(v_SearchStr,'') || '%'') ';
   end if; 
    
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');    
    
   RAISE NOTICE '%',v_strSql;
   EXECUTE 'insert into tt_TEMPTABLE(numWFID) ' || v_strSql;    
    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                        
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                         

   select   COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLE;	
	
   open SWV_RefCur for
   Select ROW_NUMBER()  OVER(ORDER BY ID ASC) AS RowNo, WM.numWFID,WM.numDomainID,WM.vcWFName,WM.vcWFDescription,fn_StripHTML(WM.vcWFAction) AS vcWFAction ,CASE WHEN WM.bitActive = true THEN 'Active' ELSE 'InActive' END AS Status,WM.numCreatedBy,
		WM.vcDateField,WM.intDays,WM.intActionOn,WM.numModifiedBy,WM.dtCreatedDate,WM.dtModifiedDate,WM.bitActive,WM.numFormID,WM.tintWFTriggerOn,DFM.vcFormName,CASE WHEN (WM.tintWFTriggerOn = 1 AND WM.intDays = 0) THEN 'Create' WHEN WM.tintWFTriggerOn = 2 THEN 'Edit' WHEN WM.tintWFTriggerOn = 3 THEN 'Date Field' WHEN WM.tintWFTriggerOn = 4 THEN 'Fields Update' WHEN WM.tintWFTriggerOn = 5 THEN 'Delete' WHEN WM.tintWFTriggerOn = 6 THEN 'A/R Aging' ELSE 'NA' end AS TriggeredOn
   from WorkFlowMaster WM join tt_TEMPTABLE T on T.numWFID = WM.numWFID
   JOIN dynamicFormMaster DFM ON WM.numFormID = DFM.numFormID AND DFM.tintFlag = 3
   WHERE ID > v_firstRec and ID < v_lastRec order by ID;
   
   RETURN;
END; $$;


