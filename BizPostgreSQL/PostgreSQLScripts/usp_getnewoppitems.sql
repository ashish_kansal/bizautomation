-- Stored procedure definition script USP_GetNewOppItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetNewOppItems(v_tintOppType SMALLINT,      
v_numDomainID NUMERIC(9,0),      
v_numDivisionID NUMERIC(9,0) DEFAULT 0,      
v_str VARCHAR(20) DEFAULT NULL ,
v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
v_tintSearchOrderCustomerHistory SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql   

--IF @tintOppType=0
--BEGIN
--	SELECT '' AS NoValue
--	SELECT '' AS NoValue
--	RETURN 
--END

   AS $$
   DECLARE
   v_bitRemoveVendorPOValidation  BOOLEAN;
   v_strSQL  VARCHAR(8000);
   v_tintOrder  INTEGER;
   v_Fld_id  VARCHAR(20);
   v_Fld_Name  VARCHAR(20);
   v_strSearch  VARCHAR(8000);
   v_CustomSearch  VARCHAR(4000);
   v_numFieldId  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   drop table IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS
      select * from(select numFieldID,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
      from View_DynamicColumns
      where numFormId = 22 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      AND bitCustom = false AND coalesce(bitSettingField,false) = true AND numRelCntType = 0
      union
      select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
      from View_DynamicCustomColumns
      where Grp_id = 5 AND numFormId = 22 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID
      AND tintPageType = 1 AND bitCustom = true AND numRelCntType = 0) X; 
  
   if not exists(select * from tt_TEMP1) then
  
      insert into tt_TEMP1
      select numFieldID,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,tintOrder,0 from View_DynamicDefaultColumns
      where numFormId = 22 and bitDefault = true and coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID;
   end if;

   drop table IF EXISTS tt_TEMPITEMCODE CASCADE;
   Create TEMPORARY TABLE tt_TEMPITEMCODE 
   (
      numItemCode NUMERIC(9,0)
   );

   if v_tintOppType > 0 then
      select   coalesce(bitRemoveVendorPOValidation,false) INTO v_bitRemoveVendorPOValidation FROM Domain WHERE numDomainId = v_numDomainID;
      v_strSQL := '';
      select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name from tt_TEMP1 where Custom = 1   order by tintOrder LIMIT 1;
      while v_tintOrder > 0 LOOP
         v_strSQL := coalesce(v_strSQL,'') || ', GetCustFldValueItem(' || coalesce(v_Fld_id,'') || ', I.numItemCode) as "' || coalesce(v_Fld_Name,'') || '"';
         select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name from tt_TEMP1 where Custom = 1 and tintOrder >= v_tintOrder   order by tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      END LOOP;


--Temp table for Item Search Configuration

      DROP TABLE IF EXISTS tt_TEMPSEARCH CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSEARCH AS
         select * from(select numFieldID,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
         from View_DynamicColumns
         where numFormId = 22 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
         AND bitCustom = false  AND coalesce(bitSettingField,false) = true AND numRelCntType = 1
         union
         select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
         from View_DynamicCustomColumns
         where Grp_id = 5 AND numFormId = 22 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID
         AND tintPageType = 1 AND bitCustom = true AND numRelCntType = 1) X;
      if not exists(select * from tt_TEMPSEARCH) then
  
         insert into tt_TEMPSEARCH
         select numFieldID,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,tintOrder,0 from View_DynamicDefaultColumns
         where numFormId = 22 and bitDefault = true and coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID;
      end if;

--Regular Search
      v_strSearch := '';
      select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId from tt_TEMPSEARCH WHERE Custom = 0   order by tintOrder LIMIT 1;
      while v_tintOrder > -1 LOOP
	
         IF v_Fld_Name = 'vcPartNo' then
            insert into tt_TEMPITEMCODE  SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode = Vendor.numItemCode
            where Item.numDomainID = v_numDomainID and Vendor.numDomainID = v_numDomainID
            and Vendor.vcPartNo is not null and LENGTH(Vendor.vcPartNo) > 0 and vcPartNo ilike '%' || coalesce(v_str,'') || '%';
         ELSEIF v_Fld_Name = 'vcBarCode'
         then
            insert into tt_TEMPITEMCODE  SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID = v_numDomainID and Item.numItemCode = WI.numItemID join Warehouses W on W.numDomainID = v_numDomainID and W.numWareHouseID = WI.numWareHouseID
            where Item.numDomainID = v_numDomainID and coalesce(Item.numItemGroup,0) > 0 and WI.vcBarCode is not null and LENGTH(WI.vcBarCode) > 0 and WI.vcBarCode ilike '%' || coalesce(v_str,'') || '%';
         ELSEIF v_Fld_Name = 'vcWHSKU'
         then
            insert into tt_TEMPITEMCODE  SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID = v_numDomainID and Item.numItemCode = WI.numItemID join Warehouses W on W.numDomainID = v_numDomainID and W.numWareHouseID = WI.numWareHouseID
            where Item.numDomainID = v_numDomainID and coalesce(Item.numItemGroup,0) > 0 and WI.vcWHSKU is not null and LENGTH(WI.vcWHSKU) > 0 and WI.vcWHSKU ilike '%' || coalesce(v_str,'') || '%';
         else
            v_strSearch := coalesce(v_strSearch,'') || ' CAST(' || coalesce(v_Fld_Name,'') || ' AS VARCHAR) iLike ''%' || coalesce(v_str,'') || '%''';
         end if;
         select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId from tt_TEMPSEARCH where tintOrder >= v_tintOrder AND Custom = 0   order by tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then
		
            v_tintOrder := -1;
         ELSEIF v_Fld_Name != 'vcPartNo' and v_Fld_Name != 'vcBarCode' and v_Fld_Name != 'vcWHSKU'
         then
		
            v_strSearch := coalesce(v_strSearch,'') || ' or ';
         end if;
      END LOOP;
      IF(select count(*) from tt_TEMPITEMCODE) > 0 then
         v_strSearch := coalesce(v_strSearch,'') || ' or  I.numItemCode in (select numItemCode from tt_TEMPITEMCODE)';
      end if; 
--Custom Search
      v_CustomSearch := '';
      select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId from tt_TEMPSEARCH WHERE Custom = 1   order by tintOrder LIMIT 1;
      while v_tintOrder > -1 LOOP
         v_CustomSearch := coalesce(v_CustomSearch,'') || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
         select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId from tt_TEMPSEARCH where tintOrder >= v_tintOrder AND Custom = 1   order by tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then
		
            v_tintOrder := -1;
         ELSE
            v_CustomSearch := coalesce(v_CustomSearch,'') || ' , ';
         end if;
      END LOOP;
      IF LENGTH(v_CustomSearch) > 0 then

         v_CustomSearch := ' I.numItemCode in (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID IN (' || coalesce(v_CustomSearch,'')  || ') and Fld_Value iLike ''%' || coalesce(v_str,'') || '%'')';
         IF LENGTH(v_strSearch) > 0 then
            v_strSearch := coalesce(v_strSearch,'') || ' OR ' ||  coalesce(v_CustomSearch,'');
         ELSE
            v_strSearch :=  v_CustomSearch;
         end if;
      end if;
      if v_tintOppType = 1 OR (v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2) then

         v_strSQL := 'select I.numItemCode,COALESCE(vcItemName,'''') vcItemName,CASE WHEN I.charItemType=''P'' AND COALESCE(I.bitSerialized,false) = false AND  COALESCE(I.bitLotNo,false) = false THEN COALESCE((SELECT ROUND(monWListPrice,2) FROM WareHouseItems WHERE WareHouseItems.numItemID=I.numItemCode LIMIT 1),0) ELSE ROUND(monListPrice, 2) END AS monListPrice,COALESCE(vcSKU,'''') vcSKU,COALESCE(numBarCodeId,'''') numBarCodeId,COALESCE(vcModelID,'''') vcModelID,COALESCE(txtItemDesc,'''') as txtItemDesc,COALESCE(C.vcCompanyName,'''') as vcCompanyName' || coalesce(v_strSQL,'') || ' from Item  I     
  Left join DivisionMaster D              
  on I.numVendorID=D.numDivisionID  
  left join CompanyInfo C  
  on C.numCompanyID=D.numCompanyID  
  where COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and (' || coalesce(v_strSearch,'') || ') ';         
--- added Asset validation  by sojan

         IF v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 then
            v_strSQL := coalesce(v_strSQL,'') || ' and COALESCE(I.bitKitParent,false) = false ';
         end if;
         if v_tintSearchOrderCustomerHistory = 1 and v_numDivisionID > 0 then

            v_strSQL := coalesce(v_strSQL,'') || ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and oppM.numDivisionId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20) || ')';
         end if;
         v_strSQL := coalesce(v_strSQL,'') || ' order by  vcItemName LIMIT 20';
         RAISE NOTICE '%',v_strSQL;
         OPEN SWV_RefCur FOR EXECUTE v_strSQL;
      ELSEIF v_tintOppType = 2
      then

         v_strSQL := 'SELECT I.numItemCode,COALESCE(vcItemName,'''') vcItemName,convert(varchar(200),round(monListPrice,2)) as monListPrice,COALESCE(vcSKU,'''') vcSKU,COALESCE(numBarCodeId,0) numBarCodeId,COALESCE(vcModelID,'''') vcModelID,COALESCE(txtItemDesc,'''') as txtItemDesc,COALESCE(C.vcCompanyName,'''') as vcCompanyName' || coalesce(v_strSQL,'') || '  from item I              
 join Vendor V              
 on V.numItemCode=I.numItemCode   
 Left join DivisionMaster D              
 on V.numVendorID=D.numDivisionID  
 left join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID             
 where COALESCE(I.Isarchieve,false) <> true and COALESCE(I.bitKitParent,false) = false And I.charItemType NOT IN(''A'') AND V.numVendorID=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20) || ' and (' || coalesce(v_strSearch,'') || ') '; 
--- added Asset validation  by sojan

         if v_tintSearchOrderCustomerHistory = 1 and v_numDivisionID > 0 then

            v_strSQL := coalesce(v_strSQL,'') || ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and oppM.numDivisionId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20) || ')';
         end if;
         v_strSQL := coalesce(v_strSQL,'') || ' order by  vcItemName LIMIT 20';
         RAISE NOTICE '%',v_strSQL;
         OPEN SWV_RefCur2 FOR EXECUTE v_strSQL;
      else
         open SWV_RefCur for
         select 0;
      end if;
   ELSE
      open SWV_RefCur for
      select 0;
   end if;  

   open SWV_RefCur2 for
   select * from  tt_TEMP1 where vcDbColumnName not in('vcPartNo','vcBarCode','vcWHSKU') ORDER BY tintOrder; 

   RETURN;
END; $$;
  


