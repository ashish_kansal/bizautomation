DROP FUNCTION IF EXISTS USP_MassPurchaseFulfillment_GetRecords;

CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_GetRecords(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_numViewID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_bitGroupByOrder BOOLEAN
	,v_tintFlag SMALLINT -- if 1 then ShowBilled = true, 2 then received but not billed
	,v_bitIncludeSearch BOOLEAN
	,v_vcOrderStatus TEXT
	,v_vcCustomSearchValue TEXT
	,v_numPageIndex INTEGER
	,INOUT v_vcSortColumn VARCHAR(100) 
	,INOUT v_vcSortOrder VARCHAR(4) 
	,INOUT v_numTotalRecords NUMERIC(18,0)
	,INOUT SWV_RefCur refcursor
	,INOUT SWV_RefCur2 refcursor
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_numDivisionID  NUMERIC(18,0);
   v_bitShowOnlyFullyReceived  BOOLEAN DEFAULT false;
   v_tintBillType  SMALLINT DEFAULT 1;
   v_j  INTEGER DEFAULT 0;
   v_jCount  INTEGER;
   v_numFieldId  NUMERIC(18,0);
   v_vcFieldName  VARCHAR(50);                                                                                                
   v_vcAssociatedControlType  VARCHAR(10);  
   v_vcDbColumnName  VARCHAR(100);                                                
	--DECLARE v_numListID AS NUMERIC(9)       
   v_bitCustomField  BOOLEAN;       
   v_vcCustomFields  TEXT DEFAULT '';
   v_vcCustomWhere  TEXT DEFAULT '';

   v_vcSQL  TEXT;
   v_vcSQLFinal  TEXT;
BEGIN
   select   coalesce(numDivisionId,0) INTO v_numDivisionID FROM Domain WHERE numDomainId = v_numDomainID;
	
   IF EXISTS(SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = 187 AND numUserCntID = v_numUserCntID) then
	
      select   coalesce(bitShowOnlyFullyReceived,false), coalesce(tintBillType,1) INTO v_bitShowOnlyFullyReceived,v_tintBillType FROM
      MassPurchaseFulfillmentConfiguration WHERE
      numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntID;
   end if;

   IF v_numViewID = 4 then
	
      v_bitGroupByOrder := true;
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TempFieldsLeft_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFIELDSLEFT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSLEFT
   (
      numPKID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      ID VARCHAR(50),
      numFieldID NUMERIC(18,0),
      vcFieldName VARCHAR(100),
      vcOrigDbColumnName VARCHAR(100),
      vcListItemType VARCHAR(10),
      numListID NUMERIC(18,0),
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      vcAssociatedControlType VARCHAR(20),
      vcLookBackTableName VARCHAR(100),
      bitCustomField BOOLEAN,
      intRowNum INTEGER,
      intColumnWidth DOUBLE PRECISION,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage TEXT
   );

   IF v_bitGroupByOrder = true then
	
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName',
      'OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo',
      'OpportunityMaster.numRecOwner') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   ELSE
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName',
      'OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo',
      'OpportunityMaster.numRecOwner','WarehouseItems.numOnHand',
      'WarehouseItems.numOnOrder','WarehouseItems.numAllocation','WarehouseItems.numBackOrder',
      'Item.numItemCode','Item.vcItemName','OpportunityMaster.numUnitHour',
      'OpportunityMaster.numUnitHourReceived') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   end if;

   INSERT INTO tt_TEMPFIELDSLEFT(ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage)
   SELECT
   CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,CAST((CASE WHEN v_numViewID = 6 AND DFM.numFieldID = 248 THEN 'BizDoc' ELSE DFFM.vcFieldName END) AS VARCHAR(100))
		,DFM.vcOrigDbCOlumnName
		,coalesce(DFM.vcListItemType,'')
		,coalesce(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,CAST(0 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS INTEGER)
		,CAST(0 AS INTEGER)
		,CAST(0 AS BOOLEAN)
		,CAST('' AS TEXT)
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   DycFormField_Mapping DFFM
   ON
   DFCD.numFormID = DFFM.numFormID
   AND DFCD.numFieldID = DFFM.numFieldID
   INNER JOIN
   DycFieldMaster DFM
   ON
   DFFM.numFieldID = DFM.numFieldID
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 135
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFFM.bitDeleted,false) = false
   AND coalesce(DFFM.bitSettingField,false) = true
   AND coalesce(DFCD.bitCustom,false) = false
   UNION
   SELECT
   CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.FLd_label
		,CONCAT('CUST',CFM.Fld_id)
		,CAST('' AS VARCHAR(10))
		,coalesce(CFM.numlistid,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CFM.fld_type
		,CAST('' AS VARCHAR(100))
		,CAST(1 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,coalesce(bitIsRequired,false)
		,coalesce(bitIsEmail,false)
		,coalesce(bitIsAlphaNumeric,false)
		,coalesce(bitIsNumeric,false)
		,coalesce(bitIsLengthValidation,false)
		,coalesce(intMaxLength,0)
		,coalesce(intMinLength,0)
		,coalesce(bitFieldMessage,false)
		,coalesce(vcFieldMessage,'')
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   CFW_Fld_Master CFM
   ON
   DFCD.numFieldID = CFM.Fld_id
   LEFT JOIN
   CFW_Validation CV
   ON
   CFM.Fld_id = CV.numFieldId
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 135
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFCD.bitCustom,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPFIELDSLEFT) = 0 then
	
      INSERT INTO tt_TEMPFIELDSLEFT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,coalesce(DFM.vcListItemType,'')
			,coalesce(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 135
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND coalesce(DFFM.bitDefault,false) = true;
   end if;

   SELECT COUNT(*) INTO v_jCount FROM tt_TEMPFIELDSLEFT;
   
   WHILE v_j <= v_jCount LOOP
      select   numFieldID, vcFieldName, vcAssociatedControlType, CONCAT('Cust',numFieldID), coalesce(bitCustomField,false) INTO v_numFieldId,v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName,
      v_bitCustomField FROM
      tt_TEMPFIELDSLEFT WHERE
      numPKID = v_j;
      IF v_bitCustomField = true then
		
         IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
			
            v_vcCustomFields := CONCAT(v_vcCustomFields,',CFW',v_numFieldId,'.Fld_Value  "',v_vcDbColumnName,
            '"');
            v_vcCustomWhere := CONCAT(v_vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' ON CFW', 
            v_numFieldId,'.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=T1.numOppID');
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            v_vcCustomFields := CONCAT(v_vcCustomFields,',case when COALESCE(CFW',v_numFieldId,'.Fld_Value,0)=0 then 0 when COALESCE(CFW', 
            v_numFieldId,'.Fld_Value,0)=1 then 1 end   "',  
            v_vcDbColumnName,'"');
            v_vcCustomWhere := CONCAT(v_vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' ON CFW',
            v_numFieldId,'.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=T1.numOppID   ');
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_vcCustomFields := CONCAT(v_vcCustomFields,',FormatedDateFromDate(CFW',v_numFieldId,'.Fld_Value,', 
            v_numDomainID,')  "',v_vcDbColumnName,'"');
            v_vcCustomWhere := CONCAT(v_vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW', 
            v_numFieldId,'.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=T1.numOppID   ');
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            v_vcCustomFields := CONCAT(v_vcCustomFields,',L',v_numFieldId,'.vcData',' "',v_vcDbColumnName,'"');
            v_vcCustomWhere := CONCAT(v_vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW',
            v_numFieldId,'.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=T1.numOppID    ');
            v_vcCustomWhere := CONCAT(v_vcCustomWhere,' left Join ListDetails L',v_numFieldId,' on L',v_numFieldId, 
            '.numListItemID=CFW',v_numFieldId,'.Fld_Value');
         ELSE
            v_vcCustomFields := CONCAT(v_vcCustomFields,CONCAT(',GetCustFldValueOpp(',v_numFieldId,',T1.numOppID)'),
            ' "',v_vcDbColumnName,'"');
         end if;
      end if;
      v_j := v_j::bigint+1;
   END LOOP;
	
   DROP TABLE IF EXISTS tt_TEMPMSRECORDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPMSRECORDS
   (
      numOppID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numWOID NUMERIC(18,0)
   );

   v_vcSQL := CONCAT('INSERT INTO tt_TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN v_bitGroupByOrder = true THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,0
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || '
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND COALESCE(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 
										THEN (CASE WHEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2
										THEN (CASE WHEN COALESCE(OpportunityItems.numQtyReceived,0) > 0 AND COALESCE(OpportunityItems.numQtyReceived,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=3
										THEN (CASE 
												WHEN (CASE 
														WHEN ' || CAST(COALESCE(v_tintBillType,0) AS VARCHAR) || '=2 
														THEN COALESCE(OpportunityItems.numQtyReceived,0) 
														WHEN ' || CAST(COALESCE(v_tintBillType,0) AS VARCHAR) || '=3 THEN COALESCE(OpportunityItems.numUnitHourReceived,0) 
														ELSE COALESCE(OpportunityItems.numUnitHour,0) 
														END)  - COALESCE((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=4 AND ' || CAST(COALESCE(v_bitShowOnlyFullyReceived,false) AS VARCHAR) || ' = true
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND COALESCE(OIInner.numUnitHour,0) <> COALESCE(OIInner.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' IN (1,2) AND ' || CAST(COALESCE(v_numWarehouseID,0) AS VARCHAR) || ' > 0 AND COALESCE(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN ' || CAST(COALESCE(v_numWarehouseID,0) AS VARCHAR) || '=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LENGTH(''' || COALESCE(v_vcOrderStatus,'') || ''') > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',
   (CASE WHEN COALESCE(v_bitIncludeSearch,false) = false THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM SplitIDs(''' || COALESCE(v_vcOrderStatus,'') || ''','','')) THEN 1 ELSE 0 END) ELSE 1 END)',COALESCE(v_vcCustomSearchValue,''),(CASE WHEN COALESCE(v_bitGroupByOrder, false) = true THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));
           
   --EXECUTE format(v_vcSQL,v_numDomainID,v_vcOrderStatus,v_numViewID,v_numWarehouseID,v_bitShowOnlyFullyReceived,v_tintBillType);
   RAISE NOTICE '%', v_vcSQL;
   EXECUTE v_vcSQL;

   v_vcSQL := CONCAT('INSERT INTO tt_TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							0
							,0
							,WorkOrder.numWOID
						FROM
							WorkOrder
						LEFT JOIN
							OpportunityMaster
						ON
							WorkOrder.numOppID = OpportunityMaster.numOppID
						LEFT JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = ' || CAST(COALESCE(v_numDivisionID,0) AS VARCHAR) || '
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							Item
						ON
							WorkOrder.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							WorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
						WHERE
							WorkOrder.numDomainId=' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || '
							AND 1 = (CASE 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 
										THEN (CASE WHEN COALESCE(WorkOrder.numQtyBuilt,0) - COALESCE(WorkOrder.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2
										THEN (CASE WHEN COALESCE(WorkOrder.numQtyReceived,0) > 0 AND COALESCE(WorkOrder.numQtyReceived,0) - COALESCE(WorkOrder.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
							AND 1 = (CASE WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || ' IN (1,2) AND ' || CAST(COALESCE(v_numWarehouseID,0) AS VARCHAR) || ' > 0 AND COALESCE(WorkOrder.numWareHouseItemId,0) > 0 THEN (CASE WHEN ' || CAST(COALESCE(v_numWarehouseID,0) AS VARCHAR) || '=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)',v_vcCustomSearchValue);

   --EXECUTE format(v_vcSQL,v_numDomainID,v_vcOrderStatus,v_numViewID,v_numWarehouseID,v_bitShowOnlyFullyReceived,v_tintBillType,v_numDivisionID);
   
   RAISE NOTICE '%', v_vcSQL;
   EXECUTE v_vcSQL;
   
   SELECT COUNT(*) INTO v_numTotalRecords FROM tt_TEMPMSRECORDS;
	
   DROP TABLE IF EXISTS tt_TEMPRESULT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRESULT
   (
      numOppID NUMERIC(18,0),
      numWOID NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      numTerID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      vcCompanyName VARCHAR(300),
      vcPoppName VARCHAR(300),
      numStatus VARCHAR(300),
      bintCreatedDate VARCHAR(50),
      numAssignedTo VARCHAR(100),
      numRecOwner VARCHAR(100),
      vcItemName VARCHAR(300),
      numOnHand DOUBLE PRECISION,
      numOnOrder DOUBLE PRECISION,
      numAllocation DOUBLE PRECISION,
      numBackOrder DOUBLE PRECISION,
      numBarCodeId VARCHAR(50),
      vcLocation VARCHAR(100),
      vcModelID VARCHAR(200),
      vcItemDesc VARCHAR(2000),
      numUnitHour DOUBLE PRECISION,
      numUOMId VARCHAR(50),
      vcAttributes VARCHAR(300),
      vcPathForTImage VARCHAR(300),
      numUnitHourReceived DOUBLE PRECISION,
      numQtyReceived DOUBLE PRECISION,
      vcNotes TEXT,
      SerialLotNo TEXT,
      vcSKU VARCHAR(50),
      numRemainingQty DOUBLE PRECISION,
      numQtyToShipReceive DOUBLE PRECISION,
      monPrice DECIMAL(20,5),
      monTotAmount DECIMAL(20,5),
      vcBilled DOUBLE PRECISION
   );
	
   IF v_bitGroupByOrder = true then
	
      v_vcSQLFinal := CONCAT('INSERT INTO tt_TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,COALESCE(DivisionMaster.numTerID,0) numTerID
									,COALESCE(CompanyInfo.vcCompanyName,'''')
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN CONCAT(COALESCE(WorkOrder.vcWorkOrderName,''-''),(CASE WHEN (COALESCE(WorkOrder.numQtyItemsReq,0) - COALESCE(WorkOrder.numQtyBuilt,0)) > 0 THEN CONCAT('' Remaining build qty:'',COALESCE(WorkOrder.numQtyItemsReq,0) - COALESCE(WorkOrder.numQtyBuilt,0)) ELSE '''' END)) ELSE COALESCE(OpportunityMaster.vcPoppName,'''') END)
									,FormatedDateFromDate((CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate ELSE OpportunityMaster.bintCreatedDate END) + (((' || v_ClientTimeZoneOffset || ' * -1) || ''minutes'')::interval), '||CAST(COALESCE(v_numDomainID,0) AS VARCHAR)||')
									,GetListIemName(OpportunityMaster.numStatus)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numAssignedTo) ELSE fn_GetContactName(OpportunityMaster.numAssignedTo) END)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numCreatedBy) ELSE fn_GetContactName(OpportunityMaster.numRecOwner) END)
								FROM
									tt_TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN ' || v_numDivisionID || ' ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',(CASE v_vcSortOrder
      WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
      WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
      WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
      WHEN 'OpportunityMaster.numStatus' THEN 'GetListIemName(OpportunityMaster.numStatus)'
      WHEN 'OpportunityMaster.numAssignedTo' THEN 'fn_GetContactName(OpportunityMaster.numAssignedTo)'
      WHEN 'OpportunityMaster.numRecOwner' THEN 'fn_GetContactName(OpportunityMaster.numRecOwner)'
      ELSE 'OpportunityMaster.bintCreatedDate'
      END),' ',(CASE WHEN coalesce(v_vcSortOrder,'') <> '' THEN v_vcSortOrder ELSE 'DESC' END),' OFFSET ((' || COALESCE(v_numPageIndex,1) || '-1) * 200) ROWS FETCH NEXT 200 ROWS ONLY');
      
	  --EXECUTE format(v_vcSQLFinal,v_numDomainID,v_ClientTimeZoneOffset,v_numPageIndex,v_numDivisionID);
	  RAISE NOTICE '%', v_vcSQLFinal;
	  EXECUTE v_vcSQLFinal;
					 
   ELSE
      v_vcSQLFinal := CONCAT('INSERT INTO tt_TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,COALESCE(DivisionMaster.numTerID,0) numTerID
									,COALESCE(CompanyInfo.vcCompanyName,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN COALESCE(WorkOrder.vcWorkOrderName,'''')
										ELSE COALESCE(OpportunityMaster.vcPoppName,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN FormatedDateFromDate(WorkOrder.bintCreatedDate + (((' || v_ClientTimeZoneOffset || ' * -1) || ''minutes'')::interval),' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ')
										ELSE FormatedDateFromDate(OpportunityMaster.bintCreatedDate + (((' || v_ClientTimeZoneOffset || ' * -1) || ''minutes'')::interval), ' || CAST(COALESCE(v_numDomainID,0) AS VARCHAR) || ')
									END)
									,GetListIemName(OpportunityMaster.numStatus)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numAssignedTo)
										ELSE fn_GetContactName(OpportunityMaster.numAssignedTo)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numCreatedBy)
										ELSE fn_GetContactName(OpportunityMaster.numRecOwner)
									END) 
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN 0
										ELSE OpportunityItems.numoppitemtCode
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId
										ELSE COALESCE(OpportunityItems.numWarehouseItmsID,0)
									END)
									,Item.numItemCode
									,COALESCE(Item.vcItemName,'''')
									,COALESCE(numOnHand,0) + COALESCE(numAllocation,0)
									,COALESCE(numOnOrder,0)
									,COALESCE(numAllocation,0)
									,COALESCE(numBackOrder,0)
									,COALESCE(Item.numBarCodeId,'''')
									,COALESCE(WarehouseLocation.vcLocation,'''')
									,COALESCE(Item.vcModelID,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN COALESCE(WorkOrder.vcInstruction,'''')
										ELSE COALESCE(OpportunityItems.vcItemDesc,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN COALESCE(WorkOrder.numQtyItemsReq,0)
										ELSE COALESCE(OpportunityItems.numUnitHour,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''Units''
										ELSE fn_GetUOMName(OpportunityItems.numUOMId)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE COALESCE(OpportunityItems.vcAttributes,'''')
									END)
									,COALESCE(Item.vcPathForTImage,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN COALESCE(WorkOrder.numUnitHourReceived,0)
										ELSE COALESCE(OpportunityItems.numUnitHourReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN COALESCE(WorkOrder.numQtyReceived,0)
										ELSE COALESCE(OpportunityItems.numQtyReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE COALESCE(OpportunityItems.vcNotes,'''')
									END)
									, (SELECT string_agg(vcSerialNo || CASE WHEN COALESCE(Item.bitLotNo,false)=true THEN '' ('' || CAST(whi.numQty AS VARCHAR(15)) || '')'' ELSE '''' end, '','')
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode)
									,COALESCE(Item.vcSKU,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 THEN COALESCE(WorkOrder.numQtyBuilt,0) - COALESCE(WorkOrder.numQtyReceived,0) 
																					WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2 THEN COALESCE(WorkOrder.numQtyReceived,0) - COALESCE(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyReceived,0) 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2 THEN COALESCE(OpportunityItems.numQtyReceived,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=3 THEN (CASE 
																	WHEN ' || CAST(COALESCE(v_tintBillType,0) AS VARCHAR) || '=2 
																	THEN COALESCE(OpportunityItems.numQtyReceived,0) 
																	WHEN ' || CAST(COALESCE(v_tintBillType,0) AS VARCHAR) || '=3 THEN COALESCE(OpportunityItems.numUnitHourReceived,0) 
																	ELSE COALESCE(OpportunityItems.numUnitHour,0) 
																END)  - COALESCE((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 THEN COALESCE(WorkOrder.numQtyBuilt,0) - COALESCE(WorkOrder.numQtyReceived,0) 
																					WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2 THEN COALESCE(WorkOrder.numQtyReceived,0) - COALESCE(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=1 THEN COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyReceived,0) 
										WHEN ' || CAST(COALESCE(v_numViewID,0) AS VARCHAR) || '=2 THEN COALESCE(OpportunityItems.numQtyReceived,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) 
										ELSE COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,COALESCE(OpportunityItems.monPrice,0)
									,COALESCE(OpportunityItems.monTotAmount,0)
									,COALESCE((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
												
								FROM
									tt_TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN ' || CAST(COALESCE(v_numDivisionID,0) AS VARCHAR) || ' ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
								LEFT JOIN
									WareHouseItems
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId ELSE OpportunityItems.numWarehouseItmsID END) = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',(CASE v_vcSortOrder
      WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
      WHEN 'OpportunityMaster.vcPoppName' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.vcWorkOrderName 
																					ELSE OpportunityMaster.vcPoppName
																				END)'
      WHEN 'OpportunityMaster.bintCreatedDate' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
																						ELSE OpportunityMaster.bintCreatedDate
																					END)'
      WHEN 'OpportunityMaster.numStatus' THEN 'GetListIemName(OpportunityMaster.numStatus)'
      WHEN 'OpportunityMaster.numAssignedTo' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numAssignedTo) 
																						ELSE fn_GetContactName(OpportunityMaster.numAssignedTo)
																					END)'
      WHEN 'OpportunityMaster.numRecOwner' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN fn_GetContactName(WorkOrder.numCreatedBy) 
																					ELSE fn_GetContactName(OpportunityMaster.numRecOwner)
																				END)'
      WHEN 'numOnHand' THEN 'COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0)'
      WHEN 'numOnOrder' THEN 'COALESCE(WareHouseItems.numOnOrder,0)'
      WHEN 'numAllocation' THEN 'COALESCE(WareHouseItems.numAllocation,0)'
      WHEN 'numBackOrder' THEN 'COALESCE(WareHouseItems.numBackOrder,0)'
      WHEN 'numItemCode' THEN 'Item.numItemCode'
      WHEN 'vcItemName' THEN 'Item.vcItemName'
      WHEN 'numUnitHour' THEN '(CASE 
																WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numQtyItemsReq
																ELSE OpportunityItems.numUnitHour
															END)'
      WHEN 'numUnitHourReceived' THEN '(CASE 
																		WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numUnitHourReceived
																		ELSE OpportunityItems.numUnitHourReceived
																	END)'
      ELSE '(CASE 
												WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
												ELSE OpportunityMaster.bintCreatedDate
											END)'
      END),' ',(CASE WHEN coalesce(v_vcSortOrder,'') <> '' THEN v_vcSortOrder ELSE 'DESC' END),' OFFSET ((' || COALESCE(v_numPageIndex,1) || '-1) * 200) ROWS FETCH NEXT 200 ROWS ONLY');
      
	  --EXECUTE format(v_vcSQLFinal,v_numDomainID,v_numViewID,v_ClientTimeZoneOffset,v_numPageIndex,v_tintBillType,v_numDivisionID);
	  RAISE NOTICE '%', v_vcSQLFinal;
	  EXECUTE v_vcSQLFinal;
					 
   end if;
	
	/*Show records which have been billed*/
   IF v_numViewID = 1 AND  v_tintFlag = 1 then
	
      v_vcSQLFinal := CONCAT('SELECT T1.*',v_vcCustomFields,' FROM tt_TEMPResult T1 where vcBilled>0',
      v_vcCustomWhere);
      SELECT COUNT(*) INTO v_numTotalRecords FROM tt_TEMPRESULT where vcBilled > 0;
   ELSEIF v_numViewID = 3 AND  v_tintFlag = 2
   then /* Received but not billed*/
	
      v_vcSQLFinal := CONCAT('SELECT numOppID AS "numOppID",
      numWOID AS "numWOID",
      numDivisionID AS "numDivisionID",
      numTerID  AS "numTerID",
      numItemCode AS "numItemCode",
      numOppItemID AS "numOppItemID",
      numWarehouseItemID AS "numWarehouseItemID",
      vcCompanyName AS "vcCompanyName",
      vcPoppName AS "vcPoppName",
      numStatus AS "numStatus",
      bintCreatedDate AS "bintCreatedDate",
      numAssignedTo AS "numAssignedTo",
      numRecOwner AS "numRecOwner",
      vcItemName AS "vcItemName",
      numOnHand AS "numOnHand",
      numOnOrder AS "numOnOrder",
      numAllocation AS "numAllocation",
      numBackOrder AS "numBackOrder",
      numBarCodeId AS "numBarCodeId",
      vcLocation AS "vcLocation",
      vcModelID AS "vcModelID",
      vcItemDesc AS "vcItemDesc",
      numUnitHour AS "numUnitHour",
      numUOMId AS "numUOMId",
      vcAttributes AS "vcAttributes",
      vcPathForTImage AS "vcPathForTImage",
      numUnitHourReceived AS "numUnitHourReceived",
      numQtyReceived AS "numQtyReceived",
      vcNotes AS "vcNotes",
      SerialLotNo AS "SerialLotNo",
      vcSKU AS "vcSKU",
      numRemainingQty AS "numRemainingQty",
      numQtyToShipReceive AS "numQtyToShipReceive",
      monPrice AS "monPrice",
      monTotAmount AS "monTotAmount",
      vcBilled AS "vcBilled"',v_vcCustomFields,' FROM tt_TEMPResult T1 where numQtyReceived>0',
      v_vcCustomWhere);
      SELECT COUNT(*) INTO v_numTotalRecords FROM tt_TEMPRESULT where numQtyReceived > 0;
   else
      v_vcSQLFinal := 'SELECT numOppID AS "numOppID",
      numWOID AS "numWOID",
      numDivisionID AS "numDivisionID",
      numTerID  AS "numTerID",
      numItemCode AS "numItemCode",
      numOppItemID AS "numOppItemID",
      numWarehouseItemID AS "numWarehouseItemID",
      vcCompanyName AS "vcCompanyName",
      vcPoppName AS "vcPoppName",
      numStatus AS "numStatus",
      bintCreatedDate AS "bintCreatedDate",
      numAssignedTo AS "numAssignedTo",
      numRecOwner AS "numRecOwner",
      vcItemName AS "vcItemName",
      numOnHand AS "numOnHand",
      numOnOrder AS "numOnOrder",
      numAllocation AS "numAllocation",
      numBackOrder AS "numBackOrder",
      numBarCodeId AS "numBarCodeId",
      vcLocation AS "vcLocation",
      vcModelID AS "vcModelID",
      vcItemDesc AS "vcItemDesc",
      numUnitHour AS "numUnitHour",
      numUOMId AS "numUOMId",
      vcAttributes AS "vcAttributes",
      vcPathForTImage AS "vcPathForTImage",
      numUnitHourReceived AS "numUnitHourReceived",
      numQtyReceived AS "numQtyReceived",
      vcNotes AS "vcNotes",
      SerialLotNo AS "SerialLotNo",
      vcSKU AS "vcSKU",
      numRemainingQty AS "numRemainingQty",
      numQtyToShipReceive AS "numQtyToShipReceive",
      monPrice AS "monPrice",
      monTotAmount AS "monTotAmount",
      vcBilled AS "vcBilled"' || v_vcCustomFields || ' FROM tt_TEMPResult T1 ' || v_vcCustomWhere;
   end if;
   
   OPEN SWV_RefCur FOR 
   EXECUTE v_vcSQLFinal;
   
	OPEN SWV_RefCur2 FOR
	SELECT
		numPKID AS "numPKID",
		ID AS "ID",
		numFieldID AS "numFieldID",
		vcFieldName AS "vcFieldName",
		vcOrigDbColumnName AS "vcOrigDbColumnName",
		vcListItemType AS "vcListItemType",
		numListID AS "numListID",
		bitAllowSorting AS "bitAllowSorting",
		bitAllowFiltering AS "bitAllowFiltering",
		vcAssociatedControlType AS "vcAssociatedControlType",
		vcLookBackTableName AS "vcLookBackTableName",
		bitCustomField AS "bitCustomField",
		intRowNum AS "intRowNum",
		intColumnWidth AS "intColumnWidth",
		bitIsRequired AS "bitIsRequired",
		bitIsEmail AS "bitIsEmail",
		bitIsAlphaNumeric AS "bitIsAlphaNumeric",
		bitIsNumeric AS "bitIsNumeric",
		bitIsLengthValidation AS "bitIsLengthValidation",
		intMaxLength AS "intMaxLength",
		intMinLength AS "intMinLength",
		bitFieldMessage AS "bitFieldMessage",
		vcFieldMessage AS "vcFieldMessage"
	FROM 
		tt_TEMPFIELDSLEFT 
	ORDER BY 
		intRowNum;
END; 
$$;


