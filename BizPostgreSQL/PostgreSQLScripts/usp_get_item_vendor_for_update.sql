-- Stored procedure definition script USP_GET_ITEM_VENDOR_FOR_UPDATE for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_ITEM_VENDOR_FOR_UPDATE
(
	v_numDomainID NUMERIC(18,0)
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		IT.numItemCode AS "Item ID",
		IT.vcItemName AS "Item",
		(SELECT vcCompanyName FROM CompanyInfo WHERE numCompanyId =(SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = VEN.numVendorID)) AS "Vendor",
		VEN.vcPartNo AS "Vendor Part#",
		VEN.monCost AS "Vendor Cost",
		VEN.intMinQty AS "Vendor Minimum Qty",
		(CASE WHEN IT.numVendorID = VEN.numVendorID THEN 'TRUE' ELSE 'FALSE' END) AS "Is Primary Vendor"
   FROM Vendor VEN
   INNER JOIN Item IT ON VEN.numItemCode = IT.numItemCode AND VEN.numDomainID = IT.numDomainID
   WHERE  IT.numDomainID = v_numDomainID;
END; $$;











