-- Stored procedure definition script USP_AddtoMyReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddtoMyReports(v_numRPTId NUMERIC(9,0) DEFAULT 0,    
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintRptSequence  SMALLINT;
BEGIN
   select   coalesce(max(tintRptSequence),0) INTO v_tintRptSequence from MyReportList where numUserCntID = v_numUserCntID;    
   v_tintRptSequence := v_tintRptSequence+1;    
   delete from MyReportList where numRPTId = v_numRPTId and numUserCntID = v_numUserCntID    and tintType = 0;
   insert into MyReportList(numRPTId,numUserCntID,tintRptSequence,tintType)
values(v_numRPTId,v_numUserCntID,v_tintRptSequence,0);
RETURN;
END; $$;


