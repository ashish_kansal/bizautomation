CREATE OR REPLACE FUNCTION USP_DeleteOrderBasedPromotionRule(v_numDomainID NUMERIC(18,0),
	v_fltSubTotal DOUBLE PRECISION)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	v_numProId  NUMERIC(18,0);
	v_numProOrderBasedID  NUMERIC(18,0);
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	BEGIN
		SELECT 
			numProID, numProOrderBasedID INTO v_numProId,v_numProOrderBasedID 
		FROM 
			PromotionOfferOrderBased 
		WHERE 
			fltSubTotal = v_fltSubTotal AND numDomainId = v_numDomainID;

		DELETE FROM PromotionOfferItems POI using PromotionOfferOrderBased POB where POB.numProItemId = POI.numValue AND POI.numProId = v_numProId AND numProOrderBasedID = v_numProOrderBasedID AND tintType = 1 AND tintRecordType = 5;
		DELETE FROM PromotionOfferOrderBased WHERE fltSubTotal = v_fltSubTotal AND numDomainId = v_numDomainID;
      
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
	END;

   RETURN;
END; $$;


