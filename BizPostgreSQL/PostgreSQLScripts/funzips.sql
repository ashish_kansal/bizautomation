-- Function definition script funZips for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION funZips(v_Zip CHAR(5),     
 v_Radius INTEGER)
RETURNS TABLE     
(    
 -- Add the column definitions for the TABLE variable here    
   ZipCode CHAR(5),
   Distance NUMERIC(10,2)    
) LANGUAGE plpgsql    
 -- Add the parameters for the function here    
   AS $$
   DECLARE
   v_LAT1  DOUBLE PRECISION;    
   v_LON1  DOUBLE PRECISION;
BEGIN
   DROP TABLE IF EXISTS tt_FUNZIPS_RESULT CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FUNZIPS_RESULT
   (    
 -- Add the column definitions for the TABLE variable here    
      ZipCode CHAR(5),
      Distance NUMERIC(10,2)
   );
   select   Latitude, Longitude INTO v_LAT1,v_LON1 FROM ZipCodes WHERE CAST(ZipCodes.ZipCode AS CHAR) = v_Zip;    
     
--In ZipCodes table actually Latitude=Longitude ,Longitude=Latitude values
 -- Fill the table variable with the rows for your result set    
   INSERT INTO tt_FUNZIPS_RESULT
   SELECT ZipCodes.ZipCode,funDistance(v_LON1,v_LAT1,Longitude,Latitude) as Distance FROM ZipCodes WHERE funDistance(v_LON1,v_LAT1,ZipCodes.Longitude,ZipCodes.Latitude) < v_Radius;    
 RETURN QUERY (SELECT * FROM tt_FUNZIPS_RESULT);
END; $$;

