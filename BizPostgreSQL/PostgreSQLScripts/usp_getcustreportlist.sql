-- Stored procedure definition script Usp_getCustReportList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_getCustReportList(v_numDomainId NUMERIC(9,0),      
v_numUserCntId NUMERIC(9,0),    
v_SortChar CHAR(1) DEFAULT '0' ,
v_CurrentPage INTEGER DEFAULT 1,                                                        
v_PageSize INTEGER DEFAULT 10,                                                        
INOUT v_TotRecs INTEGER DEFAULT 0,     
v_columnName VARCHAR(50) DEFAULT NULL,                                                        
v_columnSortOrder VARCHAR(50) DEFAULT NULL  ,
v_numModuleId NUMERIC(9,0) DEFAULT NULL,   
v_SearchStr  VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                  
    
   v_firstRec  INTEGER;                                                        
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      ReportId VARCHAR(15)
   );     
   v_strSql := 'Select     
numCustomReportId as ReportId     
from CustomReport where coalesce(bitDefault,false) = false and coalesce(bitReportDisable,false) = false and numdomainid =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15); 
    
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CustomReport.vcReportName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;     
   if v_numModuleId <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And CustomReport.numModuleId = ' || SUBSTR(CAST(v_numModuleId AS VARCHAR(15)),1,15) || '';
   end if;   
   if v_SearchStr <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' And (CustomReport.vcReportName ilike ''%' || coalesce(v_SearchStr,'') || '%'' or 
CustomReport.vcReportDescription ilike ''%' || coalesce(v_SearchStr,'') || '%'') ';
   end if;     
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');    
    
   RAISE NOTICE '%',v_strSql;            
   EXECUTE 'insert into tt_TEMPTABLE(ReportId)                                                        
' || v_strSql;    
    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                        
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                         
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;       
    
    
   open SWV_RefCur for
   Select
   numCustomReportId as ReportId,vcReportName,vcReportDescription
   from CustomReport
   join tt_TEMPTABLE T on cast(NULLIF(T.ReportId,'') as NUMERIC(18,0)) = CustomReport.numCustomReportId
   WHERE ID > v_firstRec and ID < v_lastRec order by ID;
   RETURN;
END; $$;


