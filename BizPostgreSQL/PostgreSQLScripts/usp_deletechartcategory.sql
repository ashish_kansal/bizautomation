CREATE OR REPLACE FUNCTION USP_DeleteChartCategory(v_numAccountId NUMERIC(9,0) DEFAULT 0,                              
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOpeningBalance  DECIMAL(20,5);                                            
   v_numAcntType  NUMERIC(9,0);                           
   v_bitFixed  BOOLEAN;                      
   v_JournalId  NUMERIC(9,0);             
   v_numListItemId  NUMERIC(9,0);
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
   v_bitProfitLoss  BOOLEAN;
BEGIN
   select   coalesce(numOriginalOpeningBal,0), numAcntTypeId, bitFixed, coalesce(numListItemID,0), coalesce(bitProfitLoss,false) INTO v_numOpeningBalance,v_numAcntType,v_bitFixed,v_numListItemId,v_bitProfitLoss from Chart_Of_Accounts Where numAccountId = v_numAccountId And bitFixed = false and numDomainId = v_numDomainId;              

 --Do not allow accounts to be deleted if it is used as default account in Global Settings -> Accounting -> Set Default Accounts
   IF(SELECT COUNT(*) FROM AccountingCharges WHERE numDomainID = v_numDomainId AND numAccountID = v_numAccountId) > 0 then
 
      RAISE EXCEPTION 'DEFAULT_ACCOUNT';
      RETURN;
   end if;

--Do not allow accounts to be deleted if it has child accounts available
   IF(SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND numParentAccId = v_numAccountId) > 0 then
 
      RAISE EXCEPTION 'CHILD_AVAILABLE';
      RETURN;
   end if;
 
   IF(SELECT COUNT(*) FROM TaxItems WHERE numDomainID = v_numDomainId AND numChartofAcntID = v_numAccountId) > 0 then
 	 
      RAISE EXCEPTION 'TaxItems_Depend';
      RETURN;
   end if;

 --bug id 882 #2
   IF v_bitProfitLoss = true then
 
	--Do not allow accounts to be deleted if it is last Profit and Loss Account
      IF(SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND bitProfitLoss = true) = 1 then
 	 
         RAISE EXCEPTION 'LAST_PL_ACCOUNT';
         RETURN;
      end if;
   end if;
 
 
  
   Select numAccountId INTO v_numAccountIdOpeningEquity From Chart_Of_Accounts Where bitOpeningBalanceEquity = true  and numDomainId = v_numDomainId;
   select   numJOurnal_Id INTO v_JournalId From General_Journal_Header Where numChartAcntId = v_numAccountId And numDomainId = v_numDomainId;                      
   
   If v_numAcntType = 813 then
      Delete from General_Journal_Details Where numJournalId = v_JournalId and numDomainId = v_numDomainId;
      Delete from General_Journal_Header Where numChartAcntId = v_numAccountId And numDomainId = v_numDomainId;
   end if;                      
   If v_numAcntType = 816 then            
      Delete from General_Journal_Details Where numJournalId = v_JournalId And numDomainId = v_numDomainId;
      Delete from General_Journal_Header Where numChartAcntId = v_numAccountId And numDomainId = v_numDomainId;
   end if;                                               
 
   UPDATE COARelationships SET numARAccountId = 0,numARParentAcntTypeID = 0 WHERE numARAccountId = v_numAccountId;
   UPDATE COARelationships SET numAPAccountId = 0,numAPParentAcntTypeID = 0 WHERE numAPAccountId = v_numAccountId;
   DELETE FROM COAShippingMapping WHERE numIncomeAccountID = v_numAccountId AND numDomainID = v_numDomainId;
 
   If v_bitFixed = false then
  
      Delete From General_Journal_Details Where numJournalId = v_JournalId And numDomainId = v_numDomainId;
      Delete From General_Journal_Header Where  numChartAcntId = v_numAccountId And numDomainId = v_numDomainId;
   end if;   
 
 
   DELETE FROM ChartAccountOpening WHERE numAccountId = v_numAccountId;
   Delete From Chart_Of_Accounts Where numAccountId = v_numAccountId And bitFixed = false and numDomainId = v_numDomainId;                  
                          
END; $$;




