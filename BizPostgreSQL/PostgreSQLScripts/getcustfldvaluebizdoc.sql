-- Function definition script GetCustFldValueBizdoc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldValueBizdoc(v_numFldId NUMERIC(9,0),v_pageId SMALLINT,v_numRecordId NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(100);
   v_fld_Type  VARCHAR(50);
   v_numListID  NUMERIC(9,0);
BEGIN
   v_vcValue := '';
   if v_pageId = 7 or v_pageId = 8 then

      select   numlistid, fld_type INTO v_numListID,v_fld_Type from CFW_Fld_Master where Fld_id = v_numFldId;
      select   coalesce(Fld_Value,'') INTO v_vcValue from CFW_Fld_Values_Product where Fld_ID = v_numFldId and RecId = v_numRecordId;
      if (v_fld_Type = 'TextBox' or v_fld_Type = 'TextArea') then
       
         if v_vcValue = '0' then 
            v_vcValue := '-';
         end if;
         if  v_vcValue = '' then 
            v_vcValue := '-';
         end if;
      ELSEIF v_fld_Type = 'SelectBox'
      then 
		IF SWF_IsNumeric(v_vcValue) THEN
			v_vcValue := GetListIemName(CAST(v_vcValue AS NUMERIC));
		END IF;
        
      ELSEIF v_fld_Type = 'CheckBox'
      then 
         v_vcValue := case when cast(NULLIF(v_vcValue,'') as INTEGER) = 0 then 'No' else 'Yes' end;
      end if;
   end if;

   if v_vcValue is null then 
      v_vcValue := '';
   end if;

   RETURN v_vcValue;
END; $$;

