CREATE OR REPLACE FUNCTION USP_GetWebAPI(
               v_WebApiID NUMERIC(9,0) DEFAULT 0,
               v_numDomainID NUMERIC(9,0) DEFAULT 0,
               v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   v_WebApiID := coalesce(v_WebApiID,0);
  
   IF v_tintMode = 1 then
 
      open SWV_RefCur for
      SELECT DISTINCT
      WebApiId,
		   vcProviderName,
		   vcFirstFldName,
		   vcSecondFldName,
		   vcThirdFldName,
		   vcFourthFldName,
		   vcFifthFldName,
		   vcSixthFldName,
		   vcSeventhFldName,
		   vcEighthFldName,
		   vcNinthFldName,
		   vcTenthFldName,
		   vcEleventhFldName,
           vcTwelfthFldName,
           vcThirteenthFldName,
           vcFourteenthFldName,
		   vcSettingURL,
		   dtCreateDate,
		   dtEndDate
      FROM WebAPI W;
   ELSEIF v_tintMode = 2
   then
 
      open SWV_RefCur for
      SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
             WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
             WD.vcFourteenthFldValue,
             WD.vcFifteenthFldValue,
			 WD.vcSixteenthFldValue,
           coalesce(vcSettingURL,'#') AS vcSettingURL,
           coalesce(WD.bitEnableAPI,false) AS bitEnableAPI,
		   coalesce(WD.bitEnableItemUpdate,false) AS bitEnableItemUpdate,
		   coalesce(WD.bitEnableOrderImport,false) AS bitEnableOrderImport,
		   coalesce(WD.bitEnableTrackingUpdate,false) AS bitEnableTrackingUpdate,
		   coalesce(WD.bitEnableInventoryUpdate,false) AS bitEnableInventoryUpdate,
           W.WebApiId,
           WD.numDomainID,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
           --ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    coalesce(WD.numBizDocId,0) AS numBizDocId,
			coalesce(WD.numBizDocStatusId,'') AS numBizDocStatusId,
		    coalesce(WD.numOrderStatus,0) AS numOrderStatus,
			coalesce(WD.numFBABizDocId,0) AS numFBABizDocId,
			coalesce(WD.numFBABizDocStatusId,0) AS numFBABizDocStatusId,
			coalesce(WD.numUnShipmentOrderStatus,0) AS numUnShipmentOrderStatus,
		    coalesce(WD.numRecordOwner,0) AS numRecordOwner ,
		    coalesce(WD.numAssignTo,0) AS numAssignTo,
		    coalesce(WD.numwarehouseID,0) AS numWareHouseID,
		    coalesce(WD.numRelationshipId,0) AS numRelationshipId,
		     coalesce(WD.numProfileId,0) AS numProfileId,
		    coalesce(WD.numExpenseAccountId,0) AS numExpenseAccountId,
		   --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			coalesce(WD.numSalesTaxItemMapping,0) AS numSalesTaxItemMapping,
		coalesce((SELECT numShippingServiceItemID FROM Domain WHERE numDomainId = WD.numDomainID),0) AS numShippingServiceItemID,
		coalesce((SELECT numDiscountServiceItemID FROM Domain WHERE numDomainId = WD.numDomainID),0) AS numDiscountItemMapping,
		coalesce((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainId = WD.numDomainID),'') AS vcShipToPhoneNo,
		coalesce((SELECT numAdminID FROM Domain WHERE numDomainId = WD.numDomainID),
      0) AS numAdminID
      FROM   WebAPI AS W
      left JOIN WebAPIDetail AS WD
      ON W.WebApiId = WD.WebApiId
      WHERE WD.bitEnableAPI = true;
   ELSEIF v_tintMode = 3
   then
 
      open SWV_RefCur for
      SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
            WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
              WD.vcFourteenthFldValue,
                WD.vcFifteenthFldValue,
				WD.vcSixteenthFldValue,
           coalesce(vcSettingURL,'#') AS vcSettingURL,
           coalesce(WD.bitEnableAPI,false) AS bitEnableAPI,
		   coalesce(WD.bitEnableItemUpdate,false) AS bitEnableItemUpdate,
		   coalesce(WD.bitEnableOrderImport,false) AS bitEnableOrderImport,
		   coalesce(WD.bitEnableTrackingUpdate,false) AS bitEnableTrackingUpdate,
		   coalesce(WD.bitEnableInventoryUpdate,false) AS bitEnableInventoryUpdate,
           W.WebApiId,
           WD.numDomainID,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
          -- ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    coalesce(WD.numBizDocId,0) AS numBizDocId,
			coalesce(WD.numBizDocStatusId,'') AS numBizDocStatusId,
		    coalesce(WD.numOrderStatus,0) AS numOrderStatus,
			coalesce(WD.numFBABizDocId,0) AS numFBABizDocId,
			coalesce(WD.numFBABizDocStatusId,0) AS numFBABizDocStatusId,
			coalesce(WD.numUnShipmentOrderStatus,0) AS numUnShipmentOrderStatus,
		    coalesce(WD.numRecordOwner,0) AS numRecordOwner ,
		    coalesce(WD.numAssignTo,0) AS numAssignTo,
		    coalesce(WD.numwarehouseID,0) AS numWareHouseID,
		    coalesce(WD.numRelationshipId,0) AS numRelationshipId,
		     coalesce(WD.numProfileId,0) AS numProfileId,
		    coalesce(WD.numExpenseAccountId,0) AS numExpenseAccountId,
		    --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			coalesce(WD.numSalesTaxItemMapping,0) AS numSalesTaxItemMapping,
		coalesce((SELECT numShippingServiceItemID FROM Domain WHERE numDomainId = v_numDomainID),0) AS numShippingServiceItemID,
		coalesce((SELECT numDiscountServiceItemID FROM Domain WHERE numDomainId = v_numDomainID),0) AS numDiscountItemMapping,
		coalesce((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainId = v_numDomainID),'') AS vcShipToPhoneNo,
		coalesce((SELECT numAdminID FROM Domain WHERE numDomainId = v_numDomainID),
      0) AS numAdminID
      FROM   WebAPI AS W
      left JOIN WebAPIDetail AS WD
      ON W.WebApiId = WD.WebApiId
   
             --LEFT JOIN [eCommerceDTL] ED
             --ON WD.[numDomainId] = ED.[numDomainId]
      AND WD.numDomainID = v_numDomainID
      WHERE  (W.WebApiId = v_WebApiID OR v_WebApiID = 0);
   ELSEIF v_tintMode = 4
   then
 
      open SWV_RefCur for
      SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
            WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
              WD.vcFourteenthFldValue,
                WD.vcFifteenthFldValue,
				WD.vcSixteenthFldValue,
           coalesce(vcSettingURL,'#') AS vcSettingURL,
           coalesce(WD.bitEnableAPI,false) AS bitEnableAPI,
		   coalesce(WD.bitEnableItemUpdate,false) AS bitEnableItemUpdate,
		   coalesce(WD.bitEnableOrderImport,false) AS bitEnableOrderImport,
		   coalesce(WD.bitEnableTrackingUpdate,false) AS bitEnableTrackingUpdate,
		   coalesce(WD.bitEnableInventoryUpdate,false) AS bitEnableInventoryUpdate,
           W.WebApiId,
           WD.numDomainID,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
          -- ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    coalesce(WD.numBizDocId,0) AS numBizDocId,
			coalesce(WD.numBizDocStatusId,'') AS numBizDocStatusId,
		    coalesce(WD.numOrderStatus,0) AS numOrderStatus,
			coalesce(WD.numFBABizDocId,0) AS numFBABizDocId,
			coalesce(WD.numFBABizDocStatusId,0) AS numFBABizDocStatusId,
			coalesce(WD.numUnShipmentOrderStatus,0) AS numUnShipmentOrderStatus,
		    coalesce(WD.numRecordOwner,0) AS numRecordOwner ,
		    coalesce(WD.numAssignTo,0) AS numAssignTo,
		    coalesce(WD.numwarehouseID,0) AS numWareHouseID,
		    coalesce(WD.numRelationshipId,0) AS numRelationshipId,
		     coalesce(WD.numProfileId,0) AS numProfileId,
		    coalesce(WD.numExpenseAccountId,0) AS numExpenseAccountId,
		    --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			coalesce(WD.numSalesTaxItemMapping,0) AS numSalesTaxItemMapping,
			coalesce((SELECT numShippingServiceItemID FROM Domain WHERE numDomainId = WD.numDomainID),0) AS numShippingServiceItemID,
			coalesce((SELECT numDiscountServiceItemID FROM Domain WHERE numDomainId = WD.numDomainID),0) AS numDiscountItemMapping,
		coalesce((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainId = WD.numDomainID),'') AS vcShipToPhoneNo,
		coalesce((SELECT numAdminID FROM Domain WHERE numDomainId = WD.numDomainID),
      0) AS numAdminID
      FROM   WebAPI AS W
      left JOIN WebAPIDetail AS WD
      ON W.WebApiId = WD.WebApiId
   
             --LEFT JOIN [eCommerceDTL] ED
             --ON WD.[numDomainId] = ED.[numDomainId]
      AND WD.numDomainID = v_numDomainID
      WHERE  (W.WebApiId = v_WebApiID OR v_WebApiID = 0) AND WD.bitEnableAPI = true;
   end if;
   RETURN;
END; $$;




