-- Stored procedure definition script usp_SaveSurveyRespondent for PostgreSQL
CREATE OR REPLACE FUNCTION usp_SaveSurveyRespondent(v_numSurID NUMERIC(9,0),   
 v_vcSurveyRespondentInformation TEXT,         
 v_numDomainId  NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO SurveyRespondentsMaster(numSurID,
    numSurRating,
    dtDateofResponse, numDomainId) values(v_numSurID, 0, TIMEZONE('UTC',now()), v_numDomainId);                       
   
open SWV_RefCur for SELECT cast(CURRVAL('SurveyRespondentsMaster_seq') as VARCHAR(255));
END; $$;












