-- Stored procedure definition script USP_GetFiscalYearStartDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetFiscalYearStartDate(v_numYear NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_FiscalStartDate  TIMESTAMP;
   v_FiscalDate  TIMESTAMP;
   v_month  INTEGER;
   v_currentMonth  INTEGER;
BEGIN
   v_FiscalDate := GetFiscalStartDate(v_numYear::INTEGER,v_numDomainId);
   RAISE NOTICE '%',v_FiscalDate;
   RAISE NOTICE 'getutcdate()===%',CAST(TIMEZONE('UTC',now()) AS VARCHAR(100));
   v_month := EXTRACT(month FROM v_FiscalDate);
   v_currentMonth := EXTRACT(month FROM TIMEZONE('UTC',now()));
   RAISE NOTICE '%',v_month;
   RAISE NOTICE '%',v_currentMonth;
   if v_month > v_currentMonth then
      v_FiscalStartDate := v_FiscalDate+INTERVAL '-1 year';
   Else
      v_FiscalStartDate := v_FiscalDate;
   end if;
 
   open SWV_RefCur for
   Select v_FiscalStartDate;
   RETURN;
END; $$;


