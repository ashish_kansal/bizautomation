CREATE OR REPLACE FUNCTION USP_GetMatchingDeposits(v_monAmount DECIMAL(20,5),
v_numDomainId NUMERIC(18,0),
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		DM.numDepositId
		,DD.numDepositeDetailID
		,DM.numChartAcntId
		,DM.dtDepositDate
		,DD.vcMemo
		,ROUND(ABS(DD.monAmountPaid::NUMERIC),2) AS monAmountPaid
		,DD.numClassID
		,DD.numProjectID
		,DD.numReceivedFrom
		,DD.numAccountID
		,C.vcCompanyName 
	FROM 
		DepositMaster DM
    INNER JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
    INNER JOIN DivisionMaster D on DD.numReceivedFrom = D.numDivisionID
    INNER JOIN CompanyInfo C on D.numCompanyID = C.numCompanyId
    WHERE 
		DM.numDomainId = v_numDomainId 
		AND DD.monAmountPaid = ROUND(ABS(v_monAmount),2) OR  DD.monAmountPaid < ROUND(ABS(v_monAmount),2)
		AND DM.dtDepositDate BETWEEN v_dtFromDate AND v_dtToDate;

   RETURN;
END; $$;












