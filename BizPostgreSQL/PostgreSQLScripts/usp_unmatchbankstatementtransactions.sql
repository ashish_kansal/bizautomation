-- Stored procedure definition script USP_UnMatchBankStatementTransactions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Added By : Joseph ******/
/****** Deletes the Transaction Detail from Table BankStatementTransaction if the Transaction is not Mapped******/


CREATE OR REPLACE FUNCTION USP_UnMatchBankStatementTransactions(v_numTransactionID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


--UPDATE [dbo].[BankStatementTransactions] SET numReferenceID = 0 , tintReferenceType = 0 
   DELETE FROM BankTransactionMapping
   WHERE
   numTransactionID = v_numTransactionID;
   RETURN;
END; $$;




