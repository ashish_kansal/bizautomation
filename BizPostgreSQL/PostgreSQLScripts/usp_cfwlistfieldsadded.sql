CREATE OR REPLACE FUNCTION usp_cfwListFieldsAdded(v_TabId INTEGER DEFAULT null,  
v_loc_id SMALLINT DEFAULT null,  
v_relId NUMERIC(9,0) DEFAULT null,
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_TabId = -1 then

      open SWV_RefCur for
      Select fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where Grp_id = v_loc_id and numrelation = v_relId and numDomainID = v_numDomainID
      order by numOrder;
   end if;  
  
   if v_TabId != -1 then

      open SWV_RefCur for
      Select fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where Grp_id = v_loc_id  and subgrp = CAST(v_TabId AS VARCHAR) and numrelation = v_relId  and numDomainID = v_numDomainID
      order by numOrder;
   end if;
   RETURN;
END; $$;


