-- Stored procedure definition script USP_OPPSalesProcess for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPSalesProcess(v_numProType SMALLINT,
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select Slp_Id as "Slp_Id",Slp_Name as "Slp_Name" from Sales_process_List_Master where Pro_Type = v_numProType and numDomainID = v_numDomainID;
END; $$;












