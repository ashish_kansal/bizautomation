CREATE OR REPLACE FUNCTION USP_GetItemFromUPCSKU
(
	v_numDomainID NUMERIC(9,0) DEFAULT 0
	,v_vcUPCSKU VARCHAR(50) DEFAULT '', 
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_sql  TEXT;
BEGIN
	v_sql := 'select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID from Item I 
where I.numDomainID=' || COALESCE(v_numDomainID,0) || ' and COALESCE(numItemGroup,0)=0 
and ((I.numBarCodeId is not null and length(COALESCE(I.numBarCodeId,''''))>1 and I.numBarCodeId=''' || COALESCE(v_vcUPCSKU,'') || ''')
	OR (I.vcSKU is not null and length(COALESCE(I.vcSKU,''''))>0 and I.vcSKU=''' || COALESCE(v_vcUPCSKU,'') || '''))
 
Union

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID from Item I 
	join WareHouseItems WI on WI.numDomainID=' || COALESCE(v_numDomainID,0) || ' and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID=' || COALESCE(v_numDomainID,0) || ' and W.numWareHouseID=WI.numWareHouseID  
	where I.numDomainID=' || COALESCE(v_numDomainID,0) || ' and COALESCE(numItemGroup,0)>0 
			and ((WI.vcBarCode is not null and length(COALESCE(WI.vcBarCode,''''))>0 and WI.vcBarCode=''' || COALESCE(v_vcUPCSKU,'') || ''')
				OR (WI.vcWHSKU is not null and length(COALESCE(WI.vcWHSKU,''''))>0 and WI.vcWHSKU =''' || COALESCE(v_vcUPCSKU,'') || '''))

UNION

select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID
from Vendor join Item I on I.numItemCode= Vendor.numItemCode
WHERE I.numDomainID=' || COALESCE(v_numDomainID,0) || ' and Vendor.numDomainID=' || COALESCE(v_numDomainID,0) || ' 
and Vendor.vcPartNo is not null and length(COALESCE(Vendor.vcPartNo,''''))>0 and Vendor.vcPartNo=''' || COALESCE(v_vcUPCSKU,'') || '''

UNION

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID
from Item I 
join WareHouseItems WI on WI.numDomainID=' || COALESCE(v_numDomainID,0) || ' and I.numItemCode=WI.numItemID  
join Warehouses W on W.numDomainID=' || COALESCE(v_numDomainID,0) || ' and W.numWareHouseID=WI.numWareHouseID  
join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
where I.numDomainID=' || COALESCE(v_numDomainID,0) || ' and (I.bitSerialized=true or I.bitLotNo=true) 
and WDTL.vcSerialNo is not null and length(COALESCE(WDTL.vcSerialNo,''''))>0 and WDTL.vcSerialNo=''' || COALESCE(v_vcUPCSKU,'') || '''
and COALESCE(WDTL.numQty,0) > 0 ';


   RAISE NOTICE '%',v_sql;
   OPEN SWV_RefCur FOR EXECUTE v_sql;
   RETURN;
END; $$;



