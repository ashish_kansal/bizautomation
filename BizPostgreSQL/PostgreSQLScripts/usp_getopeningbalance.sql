-- Stored procedure definition script USP_GetOpeningBalance for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpeningBalance(v_numAccountId NUMERIC(9,0),    
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numOpeningBal,0) as DECIMAL(20,5)) From Chart_Of_Accounts
   Where numAccountId = v_numAccountId And numDomainId = v_numDomainId;
END; $$;












