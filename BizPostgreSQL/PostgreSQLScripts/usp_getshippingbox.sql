CREATE OR REPLACE FUNCTION USP_GetShippingBox
(
	v_ShippingReportId NUMERIC(9,0)
	,INOUT SWV_RefCur refcursor
	,INOUT SWV_RefCur2 refcursor
)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for
	SELECT DISTINCT
		numBoxID,
        vcBoxName,
        numShippingReportId,
        CASE WHEN fltTotalWeight = 0 THEN SUM(fltWeight) ELSE fltTotalWeight END AS fltTotalWeight,
        fltHeight,
        fltWidth,
        fltLength,
		FormatedDateFromDate(dtDeliveryDate,numDomainID) AS dtDeliveryDate,
		coalesce(monShippingRate,0) AS monShippingRate,
		vcShippingLabelImage,
		coalesce(vcTrackingNumber,'') AS vcTrackingNumber,
		tintServiceType,--Your Packaging
		tintPayorType,
		vcPayorAccountNo,
		vcPayorZip,
		numPayorCountry,
		numPackageTypeID,
		coalesce(vcPackageName,'') AS vcPackageName,
		numServiceTypeID,
		CAST(ROUND(CAST(SUM(fltTotalRegularWeight) AS NUMERIC),2) AS DECIMAL(9,2)) AS fltTotalWeightForShipping,
		fltDimensionalWeight AS fltTotalDimensionalWeight,
		coalesce(fltDimensionalWeight,0) AS fltDimensionalWeight,
		coalesce(numShipCompany,0) AS numShipCompany,
		numOppID
	FROM 
		View_ShippingBox
	WHERE 
		numShippingReportId = v_ShippingReportId
	GROUP BY 
		numBoxID,vcBoxName,numShippingReportId,fltHeight,fltWidth,fltLength,fltTotalWeight,
		dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,monShippingRate,
		vcShippingLabelImage,vcTrackingNumber,numDomainID,tintServiceType,
		tintPayorType,vcPayorAccountNo,vcPayorZip,numPayorCountry,numPackageTypeID,
		vcPackageName,numServiceTypeID,fltDimensionalWeight,numShipCompany;--,intBoxQty
				                
	open SWV_RefCur2 for
	SELECT 
		numBoxID,
        vcBoxName,
        ShippingReportItemId,
        numShippingReportId,
        fltTotalWeightItem AS fltTotalWeight,
        fltHeightItem AS fltHeight,
        fltWidthItem AS fltWidth,
        fltLengthItem AS fltLength,
        numOppBizDocItemID,
        numItemCode,
        numoppitemtCode,
        coalesce(vcItemName,'') AS vcItemName,
        vcModelID,
        CASE WHEN LENGTH(coalesce(vcItemDesc,'')) > 100 THEN  CAST(vcItemDesc AS VARCHAR(100)) || '..'
   ELSE coalesce(vcItemDesc,'') END AS vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
   FROM    View_ShippingBox
   WHERE   numShippingReportId = v_ShippingReportId;
   RETURN;
END; $$;



