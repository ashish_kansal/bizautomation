-- Stored procedure definition script USP_ChartofAccountsDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ChartofAccountsDetail(v_numDomainID NUMERIC(9,0),
    v_dtToDate TIMESTAMP,
    v_vcAccountId VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

    
    	/*RollUp of Sub Accounts */
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP AS
      SELECT  COA2.numAccountId  /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
      FROM    Chart_Of_Accounts COA1
      INNER JOIN Chart_Of_Accounts COA2
      ON COA1.numDomainId = COA2.numDomainId
      AND COA2.vcAccountCode ilike COA1.vcAccountCode  || '%'
      WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcAccountId,','))
      AND COA1.numDomainId = v_numDomainID;
   v_vcAccountId := coalesce((SELECT string_agg(numAccountID::VARCHAR,',') FROM tt_TEMP),''); 

   drop table IF EXISTS tt_ACCOUNTS CASCADE;
   create TEMPORARY TABLE tt_ACCOUNTS
   (
      ID NUMERIC(9,0)
   );
   insert into tt_ACCOUNTS
   SELECT CAST(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcAccountId,',');

		
open SWV_RefCur for SELECT
   h.numDomainId,
			coa.numAccountId,
			ci.vcCompanyName AS CompanyName,
			dm.numDivisionID,
			0 AS Opening,
			SUM(coalesce(d.numDebitAmt,0))  AS TotalDebit,
			SUM(coalesce(d.numCreditAmt,0)) AS TotalCredit,
			(SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0)))  AS Closing,
			coa.numParntAcntTypeID
   FROM General_Journal_Header h
   INNER JOIN General_Journal_Details d
   ON h.numJOurnal_Id = d.numJournalId
   LEFT JOIN Chart_Of_Accounts coa
   ON d.numChartAcntId = coa.numAccountId
   LEFT JOIN DivisionMaster dm
   LEFT JOIN CompanyInfo ci
   ON dm.numCompanyID = ci.numCompanyId
   ON d.numCustomerId = dm.numDivisionID
   WHERE
   h.numDomainId = v_numDomainID
   AND coalesce(dm.numDivisionID,0) > 0
   AND coa.numAccountId IN(SELECT ID FROM tt_ACCOUNTS)
   GROUP BY h.numDomainId,coa.numAccountId,ci.vcCompanyName,dm.numDivisionID,coa.numParntAcntTypeID
   HAVING
   ROUND(CAST((SUM(coalesce(d.numDebitAmt,0)) -SUM(coalesce(d.numCreditAmt,0))) AS NUMERIC),2) <> 0
   order by  ci.vcCompanyName;

   drop table IF EXISTS tt_ACCOUNTS CASCADE;
   RETURN;
END; $$;












