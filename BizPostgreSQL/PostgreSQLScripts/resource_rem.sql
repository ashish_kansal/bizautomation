-- Stored procedure definition script Resource_Rem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Resource_Rem(v_ResourceID		INTEGER		-- primary key number of the resource to delete
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	-- Start a transaction to commit both deletions on the Resource
	-- and ActivityResource tables simultaneously.
   -- BEGIN TRANSACTION
UPDATE
   ActivityResource
   SET
   ResourceID =(-999)
   WHERE
   ActivityResource.ResourceID = v_ResourceID;

   DELETE FROM ResourcePreference
   WHERE  ResourcePreference.ResourceID = v_ResourceID;

   DELETE FROM Resource
   WHERE  Resource.ResourceID = v_ResourceID;

   -- COMMIT
RETURN;
END; $$;


