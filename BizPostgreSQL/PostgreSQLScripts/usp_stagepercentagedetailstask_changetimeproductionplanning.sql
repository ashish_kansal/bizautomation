-- Stored procedure definition script USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning(v_numDomainID NUMERIC(18,0)                            
	,v_numTaskID NUMERIC(18,0)
	,v_dtStartTime TIMESTAMP
	,v_dtEndTime TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(EXTRACT(DAY FROM v_dtEndTime -v_dtStartTime)*60*24+EXTRACT(HOUR FROM v_dtEndTime -v_dtStartTime)*60+EXTRACT(MINUTE FROM v_dtEndTime -v_dtStartTime)) > 0 then
	
      UPDATE
      StagePercentageDetailsTask
      SET
      dtStartTime = v_dtStartTime,dtEndTime = v_dtEndTime,numHours = FLOOR(CAST((EXTRACT(DAY FROM v_dtEndTime -v_dtStartTime)*60*24+EXTRACT(HOUR FROM v_dtEndTime -v_dtStartTime)*60+EXTRACT(MINUTE FROM v_dtEndTime -v_dtStartTime)) AS decimal)/60),
      numMinutes = MOD((EXTRACT(DAY FROM v_dtEndTime -v_dtStartTime)*60*24+EXTRACT(HOUR FROM v_dtEndTime -v_dtStartTime)*60+EXTRACT(MINUTE FROM v_dtEndTime -v_dtStartTime))::INT,60)
      WHERE
      numDomainID = v_numDomainID
      AND numTaskId = v_numTaskID;
   ELSE
      RAISE EXCEPTION 'INVALID_START_AND_END_TIME';
   end if;
   RETURN;
END; $$;


