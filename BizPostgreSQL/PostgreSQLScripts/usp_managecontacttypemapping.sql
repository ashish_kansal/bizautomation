-- Stored procedure definition script USP_ManageContactTypeMapping for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageContactTypeMapping(INOUT v_numMappingID NUMERIC(9,0) DEFAULT 0 ,
	v_numContactTypeID NUMERIC(9,0) DEFAULT NULL,
	v_numAccountID NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_numMappingID = 0 then 
      INSERT INTO ContactTypeMapping(numContactTypeID,
		numAccountID,
		numDomainID)
	VALUES(v_numContactTypeID,
		v_numAccountID,
		v_numDomainID);
	
      v_numMappingID := CURRVAL('ContactTypeMapping_seq');
   ELSE
      UPDATE ContactTypeMapping SET
      numAccountID = v_numAccountID
      WHERE numMappingID = v_numMappingID;
   end if;
   RETURN;
END; $$;


