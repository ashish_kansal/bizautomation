-- Stored procedure definition script USP_ReportListMaster_SourceOfSalesOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_SourceOfSalesOrder(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_TotalSalesOrderCount  NUMERIC(18,4);
   v_StartDate  TIMESTAMP;
   v_EndDate  TIMESTAMP;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_tintSource  VARCHAR(100);
   v_bitPartner  BOOLEAN;
   v_numPartner  NUMERIC(18,0);
   v_TotalSalesOrderBySource  INTEGER;
   v_TotalSalesOrderAmountBySource  NUMERIC(19,4);
BEGIN
   v_EndDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '-1 second';
   v_StartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_EndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_EndDate,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '-11 month';

   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   select DISTINCT  COUNT(*) INTO v_TotalSalesOrderCount FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND bintCreatedDate BETWEEN v_StartDate AND v_EndDate;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      tintSource VARCHAR(100),
      bitPartner BOOLEAN,
      numPartner NUMERIC(18,0),
      vcSource VARCHAR(300),
      TotalOrdersCount INTEGER,
      TotalOrdersPercent NUMERIC(18,2),
      TotalOrdersAmount NUMERIC(19,4)
   );

   INSERT INTO tt_TEMP(tintSource
		,vcSource
		,bitPartner
		,numPartner)
   SELECT
   CAST('0~0' AS VARCHAR(100))
		,CAST('-' AS VARCHAR(300))
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   UNION
   SELECT
   CAST('0~1' AS VARCHAR(100))
		,CAST('Internal Order' AS VARCHAR(300))
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   UNION
   SELECT
   CONCAT(numSiteID,'~2')
		,vcSiteName
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   Sites
   WHERE
   numDomainID = v_numDomainID
   UNION
   SELECT DISTINCT
   CONCAT(WebApiId,'~3')
		,vcProviderName
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   WebAPI
   UNION
   SELECT
   CONCAT(Ld.numListItemID,'~1')
		,coalesce(vcRenamedListName,vcData)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   Listdetails Ld
   LEFT JOIN
   listorder LO
   ON
   Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   WHERE
   Ld.numListID = 9
   AND (constFlag = true OR Ld.numDomainid = v_numDomainID)
   UNION
   SELECT DISTINCT
   CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(300))
		,CAST(1 AS BOOLEAN)
		,numPartner
   FROM
   OpportunityMaster
   WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND coalesce(numPartner,0) > 0
   AND bintCreatedDate BETWEEN v_StartDate AND v_EndDate;

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;

   WHILE v_i <= v_iCount LOOP
      select   coalesce(tintSource,''), bitPartner, numPartner INTO v_tintSource,v_bitPartner,v_numPartner FROM tt_TEMP WHERE ID = v_i;
      select   COUNT(numOppId) INTO v_TotalSalesOrderBySource FROM
      OpportunityMaster WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND 1 =(CASE
      WHEN v_bitPartner = true
      THEN CASE WHEN numPartner = v_numPartner THEN 1 ELSE 0 END
      ELSE CASE WHEN CONCAT(coalesce(tintSource,0),'~',coalesce(tintSourceType,0)) = v_tintSource AND coalesce(numPartner,0) = 0 THEN 1 ELSE 0 END
      END)
      AND bintCreatedDate BETWEEN v_StartDate AND v_EndDate;
      select   SUM(monTotAmount) INTO v_TotalSalesOrderAmountBySource FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode WHERE
      OM.numDomainId = v_numDomainID
      AND I.numDomainID = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND 1 =(CASE
      WHEN v_bitPartner = true
      THEN CASE WHEN numPartner = v_numPartner THEN 1 ELSE 0 END
      ELSE CASE WHEN CONCAT(coalesce(tintSource,0),'~',coalesce(tintSourceType,0)) = v_tintSource AND coalesce(numPartner,0) = 0 THEN 1 ELSE 0 END
      END)
      AND OM.bintCreatedDate BETWEEN v_StartDate AND v_EndDate
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID);
      UPDATE
      tt_TEMP
      SET
      vcSource =(CASE
      WHEN v_bitPartner = true
      THEN coalesce((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE DM.numDomainID = v_numDomainID AND numDivisionID = v_numPartner),'-')
      ELSE vcSource
      END),TotalOrdersPercent =(v_TotalSalesOrderBySource::bigint*100)/coalesce(v_TotalSalesOrderCount,1),
      TotalOrdersAmount = coalesce(v_TotalSalesOrderAmountBySource,cast(0 as NUMERIC(19,4))),TotalOrdersCount = v_TotalSalesOrderBySource
      WHERE
      ID = v_i;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT vcSource,TotalOrdersPercent,TotalOrdersAmount FROM tt_TEMP ORDER BY TotalOrdersPercent DESC;
END; $$;












