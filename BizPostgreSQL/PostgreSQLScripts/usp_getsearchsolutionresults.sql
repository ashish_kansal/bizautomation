CREATE OR REPLACE FUNCTION usp_GetSearchSolutionResults(  
	v_vcSearchText VARCHAR(100) DEFAULT '',  
	v_numSolnID NUMERIC DEFAULT 0,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql  
   AS $$
BEGIN
   IF v_numSolnID = 0 then
      open SWV_RefCur for
      SELECT * FROM SolutionMaster
      WHERE vcSolnTitle ilike '%' || coalesce(v_vcSearchText,'') || '%'
      OR txtSolution ilike '%' || coalesce(v_vcSearchText,'') || '%';
   ELSE
      open SWV_RefCur for
      SELECT * FROM SolutionMaster
      WHERE numSolnID = v_numSolnID;
   end if;

   RETURN;
END; $$;


