-- Stored procedure definition script USP_SaveFixedAssetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveFixedAssetDetails(v_numFixedAssetId NUMERIC(9,0) DEFAULT 0,      
v_numAssetClass NUMERIC(9,0) DEFAULT 0,      
v_varAssetName VARCHAR(50) DEFAULT NULL,      
v_varAssetDescription VARCHAR(100) DEFAULT NULL,      
v_strLocation TEXT DEFAULT NULL,      
v_varSerialNo VARCHAR(50) DEFAULT NULL,      
v_monCost DECIMAL(20,5) DEFAULT NULL,      
v_dtDatePurchased TIMESTAMP DEFAULT NULL,      
v_decDepreciationPer DECIMAL DEFAULT NULL,      
v_numAssetAccount NUMERIC(9,0) DEFAULT 0,      
v_numVendor NUMERIC(9,0) DEFAULT 0,      
v_monFiscalDepOrAppAmt DECIMAL(20,5) DEFAULT NULL,      
v_monCurrentValue DECIMAL(20,5) DEFAULT NULL,      
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numFixedAssetId = 0 then

      Insert Into FixedAssetDetails(numAssetClass,vcAssetName,vcAssetDescription,ntxtLocation,vcSerialNo,
    dtDatePurchased,monCost,decDep_Year_Per,numVendor,monFiscalDepOrAppAmt,
    monCurrentValue,numCreatedBy,dtCreatedOn,numDomainID) Values(v_numAssetClass,v_varAssetName,v_varAssetDescription,
    v_strLocation,v_varSerialNo,v_dtDatePurchased,v_monCost,v_decDepreciationPer,v_numVendor,
    v_monFiscalDepOrAppAmt,v_monCurrentValue,v_numUserCntID,TIMEZONE('UTC',now()),v_numDomainId);
   ELSEIF v_numFixedAssetId > 0
   then

      Update FixedAssetDetails Set numAssetClass = v_numAssetClass,vcAssetName = v_varAssetName,vcAssetDescription = v_varAssetDescription,
      ntxtLocation = v_strLocation,vcSerialNo = v_varSerialNo,
      dtDatePurchased = v_dtDatePurchased,monCost = v_monCost,decDep_Year_Per = v_decDepreciationPer,
      numVendor = v_numVendor,monFiscalDepOrAppAmt = v_monFiscalDepOrAppAmt,
      monCurrentValue = v_monCurrentValue,numDomainID = v_numDomainId Where numFixedAssetId = v_numFixedAssetId;
   end if;
   RETURN;
END; $$;


