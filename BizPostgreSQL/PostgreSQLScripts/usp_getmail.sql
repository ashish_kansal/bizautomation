-- Stored procedure definition script USP_GetMail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMail(v_numEmailHstrID NUMERIC,
v_ClientTimeZoneOffset INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   update  EmailHistory set bitIsRead = true where numEmailHstrID = v_numEmailHstrID; 
              
   open SWV_RefCur for select
   vcFrom   as FromEmail,
vcTo   as ToEmail,
vcCC  as CCEmail,
vcBCC as BCCEmail,
EH.numEmailHstrID,
ROW_NUMBER() OVER(ORDER BY EH.numEmailHstrID ASC) AS  count,
coalesce(EH.vcSubject,'') as Subject,
coalesce(EH.vcBody,'') as Body,
coalesce(EH.bintCreatedOn,TIMEZONE('UTC',now())) as created,
coalesce(EH.vcItemId,'') as ItemId,
coalesce(EH.vcChangeKey,'') as ChangeKey,
coalesce(EH.bitIsRead,false) as IsRead,
coalesce(EH.vcSize,'') as Size,
coalesce(EH.bitHasAttachments,false) as HasAttachments,
coalesce(EH.dtReceivedOn,TIMEZONE('UTC',now()))+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as sent,
coalesce(EH.tintType,1) as type,
coalesce(EH.chrSource,'B') as Source ,
coalesce(dtl.vcFileName,'') as AttachmentPath       ,
coalesce(EH.vcBodyText,'') as vcBodyText  ,
EH.numListItemId AS numEmailStatusId,
dtl.vcAttachmentItemId as AttachmentItemId   ,
coalesce(dtl.vcLocation,'') AS vcLocation,
EH.numDomainID,
EH.numUserCntId,EH.numUid ,
coalesce(EM.numContactID,0) as numContactID,
coalesce(ADC.numDivisionId,0) AS numDivisionId,
(SELECT  DM.tintCRMType FROM DivisionMaster DM WHERE DM.numDivisionID = coalesce(ADC.numDivisionId,0) LIMIT 1) AS tintCRMType    ,
IT.vcNodeName AS vcNodeName ,
EH.numNodeId
   from EmailHistory  EH
   JOIN EmailMaster EM on EH.numEMailId = EM.numEmailId
   left join AdditionalContactsInformation As ADC on ADC.numContactId = coalesce(EM.numContactID,0)
   left join EmailHstrAttchDtls dtl on EH.numEmailHstrID = dtl.numEmailHstrID
   left join InboxTreeSort	as IT ON EH.numNodeId = IT.numNodeID
   where EH.numEmailHstrID = v_numEmailHstrID;
END; $$;
