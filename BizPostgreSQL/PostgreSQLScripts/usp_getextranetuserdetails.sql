-- FUNCTION: public.usp_getextranetuserdetails(numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getextranetuserdetails(numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getextranetuserdetails(
	v_numdomainid numeric,
	v_numcontactid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for SELECT  numExtranetDtlID,
            numExtranetID,
            EAD.numContactID,
            vcPassword,
            tintAccessAllowed,
            bintLastLoggedIn,
            numNoOfTimes,
            bitPartnerAccess,
            coalesce(ACI.vcEmail,'') AS vcEmail,
            coalesce(D.vcPortalName,'') AS vcPortalName
   FROM    ExtranetAccountsDtl
   EAD INNER JOIN AdditionalContactsInformation ACI ON EAD.numDomainID = ACI.numDomainID
   AND CAST(EAD.numContactID AS Numeric) = ACI.numContactId
   INNER JOIN Domain D ON D.numDomainId = ACI.numDomainID
   WHERE   EAD.numDomainID = v_numDomainID
   AND CAST(EAD.numContactID As Numeric) = v_numContactID;
END;
$BODY$;

ALTER FUNCTION public.usp_getextranetuserdetails(numeric, numeric, refcursor)
    OWNER TO postgres;
