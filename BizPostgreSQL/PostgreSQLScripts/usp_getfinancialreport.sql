-- Stored procedure definition script USP_GetFinancialReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetFinancialReport(v_numFRID NUMERIC(18,0) ,
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numFRID as VARCHAR(255)),
        cast(vcReportName as VARCHAR(255)),
        cast(numFinancialViewID as VARCHAR(255)),
        fn_GetListItemName(numFinancialViewID) AS FinancialView,
        cast(numFinancialDimensionID as VARCHAR(255)),
        fn_GetListItemName(numFinancialDimensionID) AS FinancialDimension,
        cast(intDateRange as VARCHAR(255)),
         'Last ' || CAST(coalesce(intDateRange,0) AS VARCHAR(10)) || ' days' AS DateRange,
        cast(numDomainID as VARCHAR(255)),
        cast(numCreatedBy as VARCHAR(255)),
        cast(dtCreateDate as VARCHAR(255)),
        cast(numModifiedBy as VARCHAR(255)),
        cast(dtModifiedDate as VARCHAR(255))
   FROM    FinancialReport
   WHERE   (numFRID = v_numFRID
   OR v_numFRID = 0)
   AND numDomainID = v_numDomainID;
END; $$;

	












