-- Stored procedure definition script usp_GetInitialPageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetInitialPageDetails(v_numGroupType NUMERIC,
    v_numDomainId NUMERIC,
    v_numContactId NUMERIC,
	v_numTabId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(select count(*) from ShortCutUsrCnf where numGroupID = v_numGroupType and numDomainID = v_numDomainId and numContactId = v_numContactId and coalesce(numTabId,0) = v_numTabId and coalesce(bitInitialPage,false) = true) > 0 then

      open SWV_RefCur for(Select  Hdr.ID,Hdr.vcLinkName as Name,Hdr.Link,Hdr.numTabId
      from ShortCutUsrCnf Sconf join  ShortCutBar Hdr on  Hdr.ID = Sconf.numLinkid  and Sconf.numTabId = Hdr.numTabId
      where Sconf.numDomainID = v_numDomainId and Sconf.numGroupID = v_numGroupType  and Sconf.numContactId = v_numContactId and coalesce(Hdr.numTabId,0) = v_numTabId and coalesce(Sconf.bitInitialPage,false) = true and coalesce(Sconf.tintLinkType,0) = 0 LIMIT 1)
      UNION(Select  LD.numListItemID,LD.vcData  as Name,'../prospects/frmCompanyList.aspx?RelId=' || CAST(LD.numListItemID AS VARCHAR(10)) as link,Sconf.numTabId
      from ShortCutUsrCnf Sconf join  Listdetails LD on  LD.numListItemID = Sconf.numLinkid
      where LD.numListID = 5 and LD.numListItemID <> 46 and(LD.numDomainid = v_numDomainId or constflag = 1)  and
      Sconf.numDomainID = v_numDomainId and Sconf.numGroupID = v_numGroupType  and Sconf.numContactId = v_numContactId and coalesce(Sconf.numTabId,0) = v_numTabId and coalesce(Sconf.bitInitialPage,false) = true and coalesce(Sconf.tintLinkType,0) = 5 LIMIT 1);
   ELSEIF(select count(*) from ShortCutGrpConf where numDomainId = v_numDomainId and numGroupId = v_numGroupType  and coalesce(numTabId,0) = v_numTabId and coalesce(bitInitialPage,false) = true) > 0
   then
 
      open SWV_RefCur for(select  Hdr.ID,Hdr.vcLinkName as Name,Hdr.Link,Hdr.numTabId
      from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.ID = Sconf.numLinkId and Sconf.numTabId = Hdr.numTabId
      where Sconf.numGroupId = v_numGroupType and Sconf.numDomainId = v_numDomainId   and Hdr.numContactId = 0 and coalesce(Hdr.numTabId,0) = v_numTabId and coalesce(Sconf.bitInitialPage,false) = true and coalesce(Sconf.tintLinkType,0) = 0 LIMIT 1)
      UNION(Select  LD.numListItemID,LD.vcData as Name,'../prospects/frmCompanyList.aspx?RelId=' || CAST(LD.numListItemID AS VARCHAR(10)) as link,Sconf.numTabId
      from ShortCutGrpConf Sconf join  Listdetails LD on  LD.numListItemID = Sconf.numLinkId
      where LD.numListID = 5 and LD.numListItemID <> 46 and(LD.numDomainid = v_numDomainId or constflag = 1)  and
      Sconf.numDomainId = v_numDomainId and Sconf.numGroupId = v_numGroupType and coalesce(Sconf.numTabId,0) = v_numTabId and coalesce(Sconf.bitInitialPage,false) = true and coalesce(Sconf.tintLinkType,0) = 5 LIMIT 1);
   ELSEIF(SELECT COUNT(*) FROM ShortCutBar WHERE coalesce(numTabId,0) = v_numTabId and coalesce(bitInitialPage,false) = true) > 0
   then
      open SWV_RefCur for
      select  Hdr.ID,Hdr.vcLinkName as Name,Hdr.Link,Hdr.numTabId
      from ShortCutBar Hdr
      where coalesce(numTabId,0) = v_numTabId and coalesce(bitInitialPage,false) = true LIMIT 1;
   ELSE
	--NO INITIAL LINK AVAILABLE
      IF v_numTabId = 80 then
	
         open SWV_RefCur for
         SELECT
         0 AS id,'All Items' as Name,'../Items/frmItemList.aspx?Page=All Items&ItemGroup=0' AS Link,80 AS numTabId;
      end if;
   end if;
   RETURN;
END; $$;  


