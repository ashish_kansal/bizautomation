-- Function definition script fn_GetUserName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetUserName(v_numuserID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetName  VARCHAR(20);
BEGIN
   select   vcUserName INTO v_RetName from UserMaster where numUserId = v_numuserID;
   RETURN v_RetName;
END; $$;

