-- FUNCTION: public.usp_getddllistitems(numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getddllistitems(numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getddllistitems(
	v_listid numeric DEFAULT 0,
	v_listvalue numeric DEFAULT 0,
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:18:18 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT  cast(vcData as VARCHAR(255)) FROM Listdetails
   WHERE numListID = v_ListID and (constFlag = true or numDomainid = v_numDomainID) and numListItemID = v_Listvalue
   order by sintOrder LIMIT 1;
END;
$BODY$;

ALTER FUNCTION public.usp_getddllistitems(numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
