-- Stored procedure definition script USP_InsertUpdatePromotionOfferForOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertUpdatePromotionOfferForOrder(v_numProId NUMERIC(9,0) DEFAULT 0,
    v_vcProName VARCHAR(100) DEFAULT NULL,
    v_numDomainId NUMERIC(18,0) DEFAULT NULL,
    v_dtValidFrom TIMESTAMP DEFAULT NULL,
    v_dtValidTo TIMESTAMP DEFAULT NULL,
	v_bitNeverExpires BOOLEAN DEFAULT NULL,
	v_bitUseForCouponManagement BOOLEAN DEFAULT NULL,
	v_bitRequireCouponCode BOOLEAN DEFAULT NULL,
	v_txtCouponCode TEXT DEFAULT NULL,
	v_tintUsageLimit SMALLINT DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
	v_tintCustomersBasedOn SMALLINT DEFAULT NULL,
	v_strOrderAmount TEXT DEFAULT NULL,
	v_strfltDiscount TEXT DEFAULT NULL,
	v_tintDiscountType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numProId = 0 then
	
      INSERT INTO PromotionOffer(vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion)
        VALUES(v_vcProName
           ,v_numDomainId
           ,v_numUserCntID
		   ,TIMEZONE('UTC',now())
		   ,false
		   ,true) RETURNING numProId INTO v_numProId;
		
      open SWV_RefCur for
      SELECT v_numProId AS "numProId";
   ELSE
      IF coalesce(v_bitRequireCouponCode,false) = true AND(SELECT
      COUNT(*)
      FROM
      PromotionOffer POInner
      INNER JOIN
      DiscountCodes DCInner
      ON
      POInner.numProId = DCInner.numPromotionID
      WHERE
      POInner.numDomainId = v_numDomainId
      AND coalesce(POInner.IsOrderBasedPromotion,false) = true
      AND DCInner.numPromotionID <> v_numProId
      AND DCInner.vcDiscountCode IN(SELECT OutParam FROM SplitString(v_txtCouponCode,','))) > 0 then
		
         RAISE EXCEPTION 'DUPLICATE_COUPON_CODE';
         RETURN;
      end if;
      IF v_tintCustomersBasedOn = 2 then
		
         IF coalesce(v_tintCustomersBasedOn,0) = 2 AND(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = v_numProId AND tintType = 2) = 0 then
			
            UPDATE PromotionOffer SET bitEnabled = false WHERE numProId = v_numProId;
            RAISE EXCEPTION 'SELECT_RELATIONSHIP_PROFILE';
            RETURN;
         end if;
      end if;
      IF(SELECT
      COUNT(*)
      FROM
      PromotionOffer
      LEFT JOIN
      PromotionOfferOrganizations
      ON
      PromotionOffer.numProId = PromotionOfferOrganizations.numProId
      AND 1 =(CASE
      WHEN v_tintCustomersBasedOn = 1 THEN(CASE WHEN PromotionOfferOrganizations.tintType = 1 THEN 1 ELSE 0 END)
      WHEN v_tintCustomersBasedOn = 2 THEN(CASE WHEN PromotionOfferOrganizations.tintType = 2 THEN 1 ELSE 0 END)
      ELSE 1
      END)
      WHERE
      PromotionOffer.numDomainId = v_numDomainId
      AND PromotionOffer.numProId <> v_numProId
      AND tintCustomersBasedOn = v_tintCustomersBasedOn
      AND coalesce(bitEnabled,false) = true
      AND 1 =(CASE
      WHEN v_tintCustomersBasedOn = 2 AND IsOrderBasedPromotion = true THEN(CASE
         WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0 AND coalesce(v_bitRequireCouponCode,false) = false
         THEN
            1
         ELSE
            0
         END)
      END)) > 0 then
		
         UPDATE PromotionOffer SET bitEnabled = false WHERE numProId = v_numProId;
         RAISE EXCEPTION 'RELATIONSHIPPROFILE ALREADY EXISTS';
         RETURN;
      end if;
      UPDATE
      PromotionOffer
      SET
      vcProName = v_vcProName,numDomainId = v_numDomainId,dtValidFrom = v_dtValidFrom,
      dtValidTo = v_dtValidTo,bitNeverExpires = v_bitNeverExpires,
      bitRequireCouponCode = v_bitRequireCouponCode,bitUseForCouponManagement = v_bitUseForCouponManagement,
      numModifiedBy = v_numUserCntID,dtModified = TIMEZONE('UTC',now()),
      tintCustomersBasedOn = v_tintCustomersBasedOn,
      tintDiscountType = v_tintDiscountType
      WHERE
      numProId = v_numProId;
      IF(SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = v_numProId AND numDomainID = v_numDomainId) > 0 then
		
         DELETE FROM PromotionOfferOrder WHERE numPromotionID = v_numProId AND numDomainID = v_numDomainId;
      end if;
      IF coalesce(v_bitUseForCouponManagement,false) = false then
         DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDATA
         (
            numPromotionID NUMERIC,
            numDomainID NUMERIC,
            numOrderAmount TEXT,
            fltDiscountValue TEXT
         );
         INSERT INTO tt_TEMPDATA  SELECT v_numProId, v_numDomainId, v_strOrderAmount, v_strfltDiscount;
         INSERT INTO PromotionOfferOrder (numPromotionID,numOrderAmount,fltDiscountValue,numDomainId)  with recursive tmp(numPromotionID,numDomainID,numOrderAmount,OrderAmount,numfltDiscountValue,
         fltDiscountValue) AS(SELECT
         numPromotionID AS numPromotionID,
					numDomainID AS numDomainID,
					SUBSTR(numOrderAmount,1,POSITION(',' IN numOrderAmount || ',') -1) AS numOrderAmount,
					OVERLAY(numOrderAmount placing '' from 1 for POSITION(',' IN numOrderAmount || ',')) AS OrderAmount,
					SUBSTR(fltDiscountValue,1,POSITION(',' IN fltDiscountValue || ',') -1) AS numfltDiscountValue,
					OVERLAY(fltDiscountValue placing '' from 1 for POSITION(',' IN fltDiscountValue || ',')) AS fltDiscountValue
         FROM tt_TEMPDATA
         UNION all
         SELECT
         numPromotionID AS numPromotionID,
					numDomainID AS numDomainID,
					SUBSTR(OrderAmount,1,POSITION(',' IN OrderAmount || ',') -1) AS numOrderAmount,
					OVERLAY(OrderAmount placing '' from 1 for POSITION(',' IN OrderAmount || ',')) AS OrderAmount,
					SUBSTR(fltDiscountValue,1,POSITION(',' IN fltDiscountValue || ',') -1) AS numfltDiscountValue,
					OVERLAY(fltDiscountValue placing '' from 1 for POSITION(',' IN fltDiscountValue || ',')) AS fltDiscountValue
         FROM tmp
         WHERE
         OrderAmount > '' AND fltDiscountValue > '') SELECT
         numPromotionID,
				numOrderAmount::double precision,
				numfltDiscountValue::double precision,
				numDomainID
         FROM tmp;
      end if;
      IF v_bitRequireCouponCode = true then
         DROP TABLE IF EXISTS tt_DISCOUNTTEMPDATA CASCADE;
         CREATE TEMPORARY TABLE tt_DISCOUNTTEMPDATA
         (
            numPromotionID NUMERIC,
            CodeUsageLimit SMALLINT,
            vcDiscountCode TEXT
         );
         INSERT INTO tt_DISCOUNTTEMPDATA(numPromotionID,
				CodeUsageLimit,
				vcDiscountCode)
         SELECT
         v_numProId
				,v_tintUsageLimit
				,OutParam
         FROM
         SplitString(v_txtCouponCode,',');
         DELETE FROM
         DiscountCodes
         WHERE
         numPromotionID = v_numProId
         AND vcDiscountCode NOT IN(SELECT vcDiscountCode FROM tt_DISCOUNTTEMPDATA)
         AND coalesce((SELECT COUNT(*) FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId = DiscountCodes.numDiscountId),0) = 0;
         INSERT INTO DiscountCodes(numPromotionID
				,vcDiscountCode
				,CodeUsageLimit)
         SELECT
         tmp1.numPromotionID,
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
         FROM
         tt_DISCOUNTTEMPDATA tmp1
         WHERE
         tmp1.vcDiscountCode NOT IN(SELECT vcDiscountCode FROM DiscountCodes WHERE numPromotionID = v_numProId);
         UPDATE DiscountCodes SET CodeUsageLimit = v_tintUsageLimit WHERE numPromotionID = v_numProId;
      end if;
      open SWV_RefCur for
      SELECT  v_numProId;
   end if;
END; $$;




