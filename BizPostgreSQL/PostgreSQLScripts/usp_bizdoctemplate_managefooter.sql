-- Stored procedure definition script USP_BizDocTemplate_ManageFooter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================    
-- Author:  <Author,,Sachin Sadhu>    
-- Create date: <Create Date,,23rdDec2013>    
-- Description: <Description,,To Save Footer in BizDocTemplate Table>    
-- =============================================    
CREATE OR REPLACE FUNCTION USP_BizDocTemplate_ManageFooter(v_numBizDocTempID NUMERIC(18,0) ,  
      v_numDomainId NUMERIC(9,0) DEFAULT 0 ,  
      v_byteMode SMALLINT DEFAULT 0 ,  
      v_vcBizDocFooter VARCHAR(100) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql     
 -- Add the parameters for the stored procedure here    
   AS $$
BEGIN
    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
    
            
      
   IF v_byteMode = 1 then
            
      UPDATE  BizDocTemplate
      SET     vcBizDocFooter = v_vcBizDocFooter
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   ELSEIF v_byteMode = 2
   then
                
      UPDATE  BizDocTemplate
      SET     vcBizDocFooter = NULL
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   ELSEIF v_byteMode = 3
   then
                    
      UPDATE  BizDocTemplate
      SET     vcPurBizDocFooter = v_vcBizDocFooter
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   ELSEIF v_byteMode = 4
   then
                        
      UPDATE  BizDocTemplate
      SET     vcPurBizDocFooter = NULL
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   end if;
   RETURN;
END; $$;    





