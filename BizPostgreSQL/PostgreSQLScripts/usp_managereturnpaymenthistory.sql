-- Stored procedure definition script USP_ManageReturnPaymentHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageReturnPaymentHistory(v_numReferenceID NUMERIC(18,0) DEFAULT 0,
    v_tintMode SMALLINT DEFAULT NULL,
    v_numUserCntID NUMERIC(18,0) DEFAULT NULL ,
    v_numDomainID NUMERIC(18,0) DEFAULT NULL ,
    v_strItems TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then

      DROP TABLE IF EXISTS tt_TEMPCREDITS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCREDITS AS
         SELECT  numReturnHeaderID,monAmount,tintRefType 
         FROM
		 XMLTABLE
			(
				'NewDataSet/Credits'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numReturnHeaderID NUMERIC PATH 'numReturnHeaderID',
					monAmount DECIMAL(20,5) PATH 'monAmount',
					tintRefType SMALLINT PATH 'tintRefType'
			) AS X;

      IF v_tintMode = 1 then --Deposit (Sales Credit Memo,Credit Memo)
				
         DELETE FROM ReturnPaymentHistory WHERE numReferenceID = v_numReferenceID AND tintReferenceType IN(1,3);
      ELSEIF v_tintMode = 2
      then --Bill Payment (Purchase Credit Memo)
				
         DELETE FROM ReturnPaymentHistory WHERE numReferenceID = v_numReferenceID AND tintReferenceType = 2;
      end if;
      INSERT INTO ReturnPaymentHistory(numReturnHeaderID,
					numReferenceID,
					monAmount,
					tintReferenceType)  	SELECT  numReturnHeaderID,v_numReferenceID,monAmount,tintRefType
      FROM tt_TEMPCREDITS WHERE tintRefType != 0;
      UPDATE ReturnHeader SET monBizDocUsedAmount =(SELECT coalesce(SUM(coalesce(monAmount,0)),0) FROM ReturnPaymentHistory WHERE numReturnHeaderID = ReturnHeader.numReturnHeaderID)
      WHERE numReturnHeaderID IN(SELECT  numReturnHeaderID FROM tt_TEMPCREDITS WHERE tintRefType != 0);
      DROP TABLE IF EXISTS tt_TEMPCREDITS CASCADE;
   end if;
   RETURN;
END; $$;
			


