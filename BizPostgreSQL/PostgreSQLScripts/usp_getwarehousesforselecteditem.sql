DROP FUNCTION IF EXISTS USP_GetWarehousesForSelectedItem;
CREATE OR REPLACE FUNCTION USP_GetWarehousesForSelectedItem(v_numItemCode NUMERIC(18,0) DEFAULT 0,
	v_numWareHouseID NUMERIC(18,0) DEFAULT 0,
	v_numQty DOUBLE PRECISION DEFAULT NULL,
	v_numOrderSource NUMERIC(18,0) DEFAULT NULL,
	v_tintSourceType SMALLINT DEFAULT NULL,
	v_numShipToCountry NUMERIC(18,0) DEFAULT 0,
	v_numShipToState NUMERIC(18,0) DEFAULT 0,
	v_vcSelectedKitChildItems TEXT DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numBaseUnit  NUMERIC(18,0);
   v_vcUnitName  VARCHAR(100); 
   v_numSaleUnit  NUMERIC(18,0);
   v_vcSaleUnitName  VARCHAR(100); 
   v_numPurchaseUnit  NUMERIC(18,0);
   v_vcPurchaseUnitName  VARCHAR(100); 
   v_bitSerialize  BOOLEAN;     
   v_bitKitParent  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_numSaleUOMFactor  DECIMAL(28,14);
   v_numPurchaseUOMFactor  DECIMAL(28,14);
BEGIN
   select   numDomainID, coalesce(numBaseUnit,0), coalesce(numSaleUnit,0), coalesce(numPurchaseUnit,0), bitSerialized, coalesce(bitAssembly,false), (CASE WHEN coalesce(bitKitParent,false) = true
   AND coalesce(bitAssembly,false) = true THEN 0
   WHEN coalesce(bitKitParent,false) = true THEN 1
   ELSE 0
   END) INTO v_numDomainID,v_numBaseUnit,v_numSaleUnit,v_numPurchaseUnit,v_bitSerialize,
   v_bitAssembly,v_bitKitParent FROM
   Item WHERE
   numItemCode = v_numItemCode;    

   DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
   (
      numWarehouseItemID NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numOnHand DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPWAREHOUSE(numWarehouseItemID
		,numWarehouseID
		,numOnHand)
   SELECT
   WareHouseItems.numWareHouseItemID
		,WareHouseItems.numWareHouseID
		,coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0)
   FROM
   WareHouseItems
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   WareHouseItems.numItemID = v_numItemCode
   AND WarehouseLocation.numWLocationID IS NULL;


   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseItemID NUMERIC(18,0),
      bitWarehouseMapped BOOLEAN
   );

   IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND coalesce(bitAutoWarehouseSelection,false) = true) then
	
      INSERT INTO tt_TEMP(numWarehouseItemID
			,bitWarehouseMapped)
      SELECT
      numWareHouseItemID
			,CAST(1 AS BOOLEAN)
      FROM(SELECT
         ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) AS numRowIndex
				,* FROM(SELECT
            TW.numWarehouseItemID
					,(CASE
            WHEN v_bitKitParent = true
            THEN cast(fn_IsKitCanBeBuild(v_numDomainID,v_numItemCode::NUMERIC,v_numQty,TW.numWarehouseID,v_vcSelectedKitChildItems) as INTEGER)
            ELSE(CASE WHEN TW.numOnHand >= v_numQty THEN 1 ELSE 0 END)
            END) AS titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
            FROM
            MassSalesFulfillmentWM
            INNER JOIN
            MassSalesFulfillmentWMState
            ON
            MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
            INNER JOIN
            MassSalesFulfillmentWP
            ON
            MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
            INNER JOIN
            tt_TEMPWAREHOUSE TW
            ON
            MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
            WHERE
            MassSalesFulfillmentWM.numDomainID = v_numDomainID
            AND MassSalesFulfillmentWM.numOrderSource = v_numOrderSource
            AND MassSalesFulfillmentWM.tintSourceType = v_tintSourceType
            AND MassSalesFulfillmentWM.numCountryID = v_numShipToCountry
            AND MassSalesFulfillmentWMState.numStateID = v_numShipToState) TEMP) T1
      ORDER BY
      titOnHandOrder DESC,intOrder ASC;
   end if;

   select   coalesce(vcUnitName,'-') INTO v_vcUnitName FROM UOM u WHERE u.numUOMId = v_numBaseUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcSaleUnitName FROM UOM u WHERE u.numUOMId = v_numSaleUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcPurchaseUnitName FROM UOM u WHERE u.numUOMId = v_numPurchaseUnit;

   v_numSaleUOMFactor := fn_UOMConversion(v_numSaleUnit,v_numItemCode::NUMERIC,v_numDomainID,v_numBaseUnit);
   v_numPurchaseUOMFactor := fn_UOMConversion(v_numPurchaseUnit,v_numItemCode::NUMERIC,v_numDomainID,v_numBaseUnit);

	open SWV_RefCur for 
	SELECT
		WareHouseItems.numWareHouseItemID,
		WareHouseItems.numItemID,
		coalesce(WareHouseItems.numWLocationID,0) AS numWLocationID,
		coalesce(vcWareHouse,'') AS vcWareHouse,
		fn_GetAttributes(WareHouseItems.numWareHouseItemID,v_bitSerialize) as Attr,
		cast(WareHouseItems.monWListPrice as VARCHAR(255)),
		COALESCE(CASE
			WHEN v_bitKitParent = true
			THEN COALESCE(fn_GetKitInventory(v_numItemCode::NUMERIC,WareHouseItems.numWareHouseID),0)
			ELSE coalesce((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID = WareHouseItems.numItemID AND WI.numWareHouseID = WareHouseItems.numWareHouseID),0)+coalesce(numOnHand,0)
		END,0) AS numOnHand,
		CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numOnOrder,0) END AS numOnOrder,
		CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numReorder,0) END AS numReorder,
		CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numAllocation,0) END AS numAllocation,
		CASE WHEN v_bitKitParent = true THEN 0 ELSE coalesce(numBackOrder,0) END AS numBackOrder,
		cast(Warehouses.numWareHouseID as VARCHAR(255)),
		v_vcUnitName AS vcUnitName,
		v_vcSaleUnitName AS vcSaleUnitName,
		v_vcPurchaseUnitName AS vcPurchaseUnitName,
		CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN CAST(CAST(fn_GetKitInventory(v_numItemCode::NUMERIC,WareHouseItems.numWareHouseID)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) else CAST(CAST(coalesce((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID = WareHouseItems.numItemID AND WI.numWareHouseID = WareHouseItems.numWareHouseID),0)+coalesce(numOnHand,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numOnHandUOM,
		CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numOnOrder,0)/v_numPurchaseUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numOnOrderUOM,
		CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numReorder,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numReorderUOM,
		CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numAllocation,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numAllocationUOM,
		CASE WHEN v_numBaseUnit > 0 AND v_numSaleUnit > 0
   THEN
      CASE WHEN v_bitKitParent = true THEN '0' else CAST(CAST(coalesce(numBackOrder,0)/v_numSaleUOMFactor AS INTEGER) AS VARCHAR(30)) END
   ELSE
      '-'
   END AS numBackOrderUOM
		,T.bitWarehouseMapped
   FROM
   WareHouseItems
   JOIN
   Warehouses
   ON
   Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   LEFT JOIN
   tt_TEMP T
   ON
   WareHouseItems.numWareHouseItemID = T.numWarehouseItemID
   WHERE
   numItemID = v_numItemCode
   AND coalesce(WareHouseItems.numWLocationID,0) = 0
   ORDER BY(CASE
   WHEN T.ID IS NULL
   THEN(CASE
      WHEN WareHouseItems.numWareHouseID = v_numWareHouseID then 99991
      ELSE 99992
      END)
   ELSE T.ID
   END) ASC;
END; $$;












