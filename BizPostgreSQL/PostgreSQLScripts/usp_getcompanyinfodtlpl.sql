-- Stored procedure definition script usp_GetCompanyInfoDtlPl for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyInfoDtlPl(v_numDivisionID NUMERIC(9,0) ,                      
v_numDomainID NUMERIC(9,0) ,        
v_ClientTimeZoneOffset INTEGER                              
    --                                                                           
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CMP.numCompanyId,
CMP.vcCompanyName,
 (select vcData from Listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,
 (select vcData from Listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName,
 CMP.numCompanyRating,
 (select vcData from Listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,
 CMP.numCompanyType as numCmptype,
 DM.bitPublicFlag,
 DM.numDivisionID,
 DM.vcDivisionName,
getCompanyAddress(DM.numDivisionID,1::SMALLINT,DM.numDomainID) as Address,
getCompanyAddress(DM.numDivisionID,2::SMALLINT,DM.numDomainID) as ShippingAddress,
  coalesce(AD2.vcStreet,'') as vcShipStreet,
  coalesce(AD2.vcCity,'') as vcShipCity,
  coalesce(fn_GetState(AD2.numState),'')  as vcShipState,
  coalesce((SELECT numShippingZone FROM State WHERE (numDomainID = v_numDomainID OR ConstFlag = true) AND numStateID = AD2.numState),
   0) AS numShippingZone,
 (select vcData from Listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,
 CMP.vcWebLabel1,
 CMP.vcWebLink1,
 CMP.vcWebLabel2,
 CMP.vcWebLink2,
 CMP.vcWebLabel3,
 CMP.vcWebLink3,
 CMP.vcWeblabel4,
 CMP.vcWebLink4,
 DM.tintBillingTerms,
 DM.numBillingDays,
 DM.tintInterestType,
 DM.fltInterest,
 coalesce(AD2.vcPostalCode,'') as vcShipPostCode,
 AD2.numCountry AS vcShipCountry,
 DM.bitPublicFlag,
 (select vcData from Listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,
 (select vcData from Listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName,
 CMP.numCompanyIndustry,
 (select vcData from Listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,
 coalesce(CMP.txtComments,'') as txtComments,
 coalesce(CMP.vcWebSite,'') AS vcWebSite,
 (select vcData from Listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,
 (select vcData from Listdetails where numListItemID = CMP.numNoOfEmployeesId) as numNoOfEmployeesIDName,
	CMP.numNoOfEmployeesId,
    (select vcData from Listdetails where numListItemID = CMP.vcProfile) as vcProfileName,
 CMP.vcProfile,
 DM.numCreatedBy,
 fn_GetContactName(DM.numRecOwner) as RecOwner,
 fn_GetContactName(DM.numCreatedBy) || ' ' || CAST(DM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcCreatedBy,
 DM.numRecOwner,
 fn_GetContactName(DM.numModifiedBy) || ' ' || CAST(DM.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcModifiedBy,
 coalesce(DM.vcComPhone,'') as vcComPhone,
 DM.vcComFax,
 DM.numGrpId,
 (select vcGrpName from Groups where numGrpId = DM.numGrpId) as numGrpIDName,
 (select vcData from Listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,
 DM.tintCRMType,
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID = DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,
 DM.numAssignedBy, coalesce(DM.bitNoTax,false) AS bitNoTax,
 (select A.vcFirstName || ' ' || A.vcLastname
      from AdditionalContactsInformation A
      join DivisionMaster D
      on D.numDivisionID = A.numDivisionId
      where A.numContactId = DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from GenericDocuments where numRecID = v_numDivisionID and vcDocumentSection = 'A' AND tintDocumentType = 2) as DocumentCount ,
 (SELECT count(*) from CompanyAssociations where numDivisionID = v_numDivisionID and bitDeleted = false) as AssociateCountFrom,
 (SELECT count(*) from CompanyAssociations where numAssociateFromDivisionID = v_numDivisionID and bitDeleted = false) as AssociateCountTo,
 (select vcData from Listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,
 DM.numCompanyDiff,DM.vcCompanyDiff,coalesce(DM.bitActiveInActive,true) as bitActiveInActive,
(SELECT SUBSTR((SELECT ',' ||  A.vcFirstName || ' ' || A.vcLastname || CASE WHEN coalesce(SR.numContactType,0) > 0 THEN '(' || fn_GetListItemName(SR.numContactType) || ')' ELSE '' END
         FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
         JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
         WHERE SR.numDomainID = DM.numDomainID AND SR.numModuleID = 1 AND SR.numRecordID = DM.numDivisionID
         AND UM.numDomainID = DM.numDomainID and UM.numDomainID = A.numDomainID),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  coalesce(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode || '-' || C3.vcCompanyName as numPartenerSource,
 coalesce(DM.numPartenerContact,0) AS numPartenerContactId,
A2.vcFirstName || ' ' || A2.vcLastname AS numPartenerContact,
coalesce(DM.bitEmailToCase,false) AS bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	FormatedDateFromDate(DM.dtLastFollowUp,v_numDomainID) AS vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(A2.numContactId,2,v_numDomainID) ELSE '' END) AS vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN(SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID = DM.numDivisionID) > 0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,
coalesce(DM.numDefaultShippingServiceID,0) AS numShippingService,
coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS vcShippingService,
		CASE
   WHEN CAST(vcSignatureType AS INT) = 0 THEN 'Service Default'
   WHEN CAST(vcSignatureType AS INT) = 1 THEN 'Adult Signature Required'
   WHEN CAST(vcSignatureType AS INT) = 2 THEN 'Direct Signature'
   WHEN CAST(vcSignatureType AS INT) = 3 THEN 'InDirect Signature'
   WHEN CAST(vcSignatureType AS INT) = 4 THEN 'No Signature Required'
   ELSE ''
   END AS vcSignatureType,
		coalesce(DM.intShippingCompany,0) AS intShippingCompany,
		CASE WHEN coalesce(DM.intShippingCompany,0) = 0 THEN '' ELSE coalesce((SELECT vcData FROM Listdetails WHERE numListID = 82 AND numListItemID = coalesce(DM.intShippingCompany,0)),'') END AS vcShipVia,
        A2.vcFirstName AS vcFirstName,
		A2.vcLastname  AS vcLastName,
		A2.vcFirstName || ' ' || A2.vcLastname AS vcGivenName,
		A2.numPhone AS numPhone,
		A2.numPhoneExtension As numPhoneExtension,
		A2.vcEmail As vcEmail	,
 coalesce(DM.bitAutoCheckCustomerPart,false) AS  bitAutoCheckCustomerPart	    ,
COALESCE((SELECT string_agg(CONCAT(' <b>Buy</b> ',CASE WHEN u.intType = 1 THEN CONCAT('$',u.vcBuyingQty) ELSE CAST(u.vcBuyingQty AS TEXT) END,' ',CASE WHEN u.intType = 1 THEN 'Amount' WHEN u.intType = 2 THEN 'Quantity' WHEN u.intType = 3 THEN 'Lbs' ELSE '' END,' <b>Get</b> ',(CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END)),'<br/>' order by u.numPurchaseIncentiveId) 
      from PurchaseIncentives u
      where u.numPurchaseIncentiveId = numPurchaseIncentiveId AND numDomainID = v_numDomainID AND numDivisionId = v_numDivisionID
      ),'') AS Purchaseincentives,
coalesce(DM.intdropship,0) as intDropShip,
CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
   WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
   WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
   WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
   ELSE 'Select One'
   END AS intDropShipName,
CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
   WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
   WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
   WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
   ELSE 'Select One'
   END AS vcFropShip
   FROM  CompanyInfo CMP
   join DivisionMaster DM
   on DM.numCompanyID = CMP.numCompanyId
   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
   LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
   AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
   LEFT JOIN DivisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
   LEFT JOIN CompanyInfo C3 ON C3.numCompanyId = D3.numCompanyID
   LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = true
   LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
   where DM.numDivisionID = v_numDivisionID   and DM.numDomainID = v_numDomainID;
END; $$;













