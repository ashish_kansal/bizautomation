CREATE OR REPLACE FUNCTION USP_CaseComments(v_byteMode SMALLINT DEFAULT 0,    
v_numCaseId NUMERIC(9,0) DEFAULT 0,             
v_vcHeading VARCHAR(1000) DEFAULT '',    
v_txtComments TEXT DEFAULT '',    
v_numContactID NUMERIC(9,0) DEFAULT 0,    
v_numCommentID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select numCommentID,vcHeading || ' (' || CAST(bintCreatedDate AS VARCHAR(20)) || ') - ' || fn_GetContactName(numCreatedby) as vcHeading,txtComments from CaseComments where numCaseId = v_numCaseId  order by    numCommentID desc;
   end if;    
    
   if v_byteMode = 1 then

      if v_numCommentID = 0 then

         insert into CaseComments(numCaseId,vcHeading,txtComments,numCreatedby,bintCreatedDate)
values(v_numCaseId,v_vcHeading,v_txtComments,v_numContactID,TIMEZONE('UTC',now()));

         open SWV_RefCur for
         select CURRVAL('CaseComments_seq');
      else
         update CaseComments set vcHeading = v_vcHeading,txtComments = v_txtComments where numCommentID = v_numCommentID;
         open SWV_RefCur for
         select v_numCommentID;
      end if;
   end if;       
    
   if v_byteMode = 2 then

      delete from CaseComments where numCommentID = v_numCommentID;
      open SWV_RefCur for
      select 1;
   end if;
   RETURN;
END; $$;
