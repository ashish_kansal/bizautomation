-- Stored procedure definition script USP_MassPurchaseFulfillmentConfiguration_GetByUser for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillmentConfiguration_GetByUser(v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID;
END; $$;












