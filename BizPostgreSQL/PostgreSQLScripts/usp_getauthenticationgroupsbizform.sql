CREATE OR REPLACE FUNCTION usp_GetAuthenticationGroupsBizForm(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		numGroupID as "numGroupID"
		,vcGroupName as "vcGroupName"
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = v_numDomainID;
END; $$;












