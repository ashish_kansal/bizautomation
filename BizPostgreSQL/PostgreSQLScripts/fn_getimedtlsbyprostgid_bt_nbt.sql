CREATE OR REPLACE FUNCTION fn_GeTimeDtlsbyProStgID_BT_NBT
(
	p_numProID numeric
	,p_numProStageID numeric
	,p_tintType smallint
)
 RETURNS varchar(100)  LANGUAGE plpgsql            
AS $$
	DECLARE 
		v_BT varchar (250);
		v_NBT varchar (250);
BEGIN            
	--Now show sumtoal of amount and toal hours for selected stage
	SELECT 
		'Billable Time (' ||  CAST(CAST(SUM(DATEDIFF('minute',dtFromDate,dtToDate))/ 60 AS DECIMAL(10, 2)) AS VARCHAR(100)) || ')'  INTO v_BT			              
	FROM 
		timeandexpense    
	WHERE 
		numProID=p_numProID 
		AND (numStageID = p_numProStageID OR p_numProStageID = 0)
		AND numCategory = 1 
		AND tintTEType = 2
		AND numType=1
	GROUP BY 
		numType;

	IF v_BT IS NULL THEN
		v_BT := 'Billable Time (0)';
	END IF;

	SELECT 
		' Non Billable Time (' || CAST(CAST(SUM(DATEDIFF('minute',dtFromDate,dtToDate))/60 AS DECIMAL(10,2)) AS VARCHAR(100))  || ')' INTO v_NBT  
	FROM 
		timeandexpense    
	WHERE 
		numProID=p_numProID 
		AND (numStageID = p_numProStageID OR p_numProStageID =0)
		AND numCategory = 1 and tintTEType = 2
		AND numType=2
	GROUP BY 
		numType;
     
	IF v_NBT IS NULL THEN
		v_NBT :=' Non Billable Time (0)';
	END IF;
       
	RETURN v_BT || v_NBT;
END; $$;
