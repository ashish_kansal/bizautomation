-- Stored procedure definition script USP_GetCustomFieldsTab for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCustomFieldsTab(v_LocID       NUMERIC,
           v_numDomainID NUMERIC(9,0),
           v_Type        NUMERIC DEFAULT -1,
           v_numGroupID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		G.*,
		CASE G.tintType
			WHEN 0 THEN 'Tab For Normal Fields'
			WHEN 1 THEN 'Tab For Frame'
			WHEN 2 THEN 'Default Sub-tabs'
			ELSE 'Tab For Normal Fields'
		END AS TabType,
		L.*,
		coalesce((SELECT bitallowed FROM GroupTabDetails GTD WHERE coalesce(GTD.tintType,0) = 1 AND GTD.numGroupID = v_numGroupID AND GTD.numTabId = G.Grp_id LIMIT 1),false) AS bitAllowed
--  GTD.tintType,
   FROM   CFw_Grp_Master G
   JOIN CFW_Loc_Master L
   ON G.loc_Id = L.Loc_id
--         LEFT OUTER JOIN [GroupTabDetails] GTD ON GTD.numTabID = G.[Grp_id]
   WHERE  (G.loc_Id = v_LocID OR 1 =(CASE WHEN v_LocID IN(12,13,14) THEN CASE WHEN G.loc_Id = 1 THEN 1 ELSE 0 END ELSE 0 END))
   AND numDomainID = v_numDomainID
   AND (G.tintType = v_Type OR v_Type = -1);

   RETURN;
END; $$;












