CREATE OR REPLACE FUNCTION USP_GetWarehouseLocationPaging
(
	v_numDomainID NUMERIC(9,0) ,
    v_numWarehouseID NUMERIC(8,0),
    v_numWLocationID NUMERIC(9,0) DEFAULT 0,
	v_vcSortChar VARCHAR(100) DEFAULT '',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
	v_vcAisle VARCHAR(500) DEFAULT '',
	v_vcRack VARCHAR(500) DEFAULT '',
	v_vcShelf VARCHAR(500) DEFAULT '',
	v_vcBin VARCHAR(500) DEFAULT '',
	v_vcLocation VARCHAR(500) DEFAULT '',
    INOUT v_intTotalRecordCount INTEGER DEFAULT 0,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_firstRec  INTEGER;                                        
	v_lastRec  INTEGER;                                                                               
	v_dynamicQuery  TEXT DEFAULT '';
	v_dynamicQueryCount  TEXT DEFAULT '';
	v_dycFilterQuery  TEXT DEFAULT '';
BEGIN
   v_dynamicQueryCount := ' SELECT COUNT(numWLocationID) FROM WarehouseLocation WHERE numDomainID = ' || COALESCE(v_numDomainID,0);

   IF(COALESCE(v_numWarehouseID,0) > 0) then
      v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND numWarehouseID=' || v_numWarehouseID;
   end if;

	IF(COALESCE(v_numWLocationID,0) > 0) then
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND numWLocationID=' || v_numWLocationID;
	end if;

	IF(COALESCE(v_vcAisle,'') <> '') then
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND vcAisle=''' || v_vcAisle || ''' ';
	end if;

	IF(COALESCE(v_vcRack,'') <> '') then	
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND vcRack=''' || v_vcRack || ''' ';
	end if;

	IF(COALESCE(v_vcShelf,'') <> '') then
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND vcShelf=''' || v_vcShelf || ''' ';
	end if;

	IF(COALESCE(v_vcBin,'') <> '') then
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND vcBin=''' || v_vcBin || ''' ';
	end if;

	IF LENGTH(coalesce(v_vcLocation,'')) > 0 then
		v_dycFilterQuery := coalesce(v_dycFilterQuery,'') || ' AND vcLocation LIKE ''%' || v_vcLocation || '%'' ';
	end if;

	v_dynamicQueryCount := coalesce(v_dynamicQueryCount,'') || coalesce(v_dycFilterQuery,'');

	EXECUTE v_dynamicQueryCount INTO v_intTotalRecordCount;

	v_dynamicQuery := 'SELECT  
							numWLocationID ,
							numWarehouseID ,
							vcAisle ,
							vcRack ,
							vcShelf ,
							vcBin ,
							vcLocation ,
							bitDefault ,
							bitSystem,
							intQty 
						FROM 
							WarehouseLocation
						WHERE 
							numDomainID = ' || COALESCE(v_numDomainID,0);
		
	v_dynamicQuery := coalesce(v_dynamicQuery,'') || coalesce(v_dycFilterQuery,'');
	v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' ORDER BY vcLocation ';
	v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' LIMIT ' || COALESCE(v_PageSize,0) || ' OFFSET ' || ((COALESCE(v_CurrentPage,0) - 1) * COALESCE(v_PageSize,0));
   
	RAISE NOTICE '%',v_dynamicQuery;
		
	OPEN SWV_RefCur FOR EXECUTE v_dynamicQuery;
   RETURN;
END; $$;
 


