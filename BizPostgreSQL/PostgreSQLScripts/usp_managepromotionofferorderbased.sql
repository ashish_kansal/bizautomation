-- Stored procedure definition script USP_ManagePromotionOfferOrderBased for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePromotionOfferOrderBased(
	--@numProOrderBasedID AS NUMERIC(9) = 0
v_numProId NUMERIC(9,0) DEFAULT 0,
	v_vcProName VARCHAR(100) DEFAULT NULL,
	v_numDomainId NUMERIC(18,0) DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
	v_fltSubTotal DOUBLE PRECISION DEFAULT NULL,
	v_cSaleType CHAR DEFAULT NULL,
	v_byteMode SMALLINT DEFAULT 0,
	v_fltOldSubTotal DOUBLE PRECISION DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numProId = 0 then
		
      INSERT INTO PromotionOffer(vcProName, numDomainId, numCreatedBy, dtCreated, bitEnabled, IsOrderBasedPromotion)
			VALUES(v_vcProName, v_numDomainId, v_numUserCntID, TIMEZONE('UTC',now()), true, true);
			
      open SWV_RefCur for
      SELECT CURRVAL('PromotionOffer_seq');
   ELSEIF v_numProId > 0
   then
		
      IF v_byteMode = 0 then
			
         IF v_cSaleType = 'I' then
				
            IF(SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND fltSubTotal = v_fltSubTotal) > 0 then
					
               RAISE EXCEPTION 'DUPLICATE_SUBTOTAL_AMOUNT';
               RETURN;
            end if;
            IF(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5) > 0 then
					
               IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND cSaleType = 'I') > 0) then
						
                  IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5) > 1) then
							
                     DELETE FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND cSaleType = 'C';
                     INSERT INTO PromotionOfferOrderBased(numProID, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled)
                     SELECT numProId, numValue, v_numDomainId, v_fltSubTotal, v_cSaleType, 'M', true FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5
                     AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND fltSubTotal = v_fltSubTotal);
                  ELSEIF ((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5) = 1)
                  then
							
                     DELETE FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND cSaleType = 'C';
                     INSERT INTO PromotionOfferOrderBased(numProID, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled)
                     SELECT numProId, numValue, v_numDomainId, v_fltSubTotal, v_cSaleType, 'S', true FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5
                     AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND fltSubTotal = v_fltSubTotal);
                  end if;
               ELSE
                  IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5) > 1) then
							
                     DELETE FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND cSaleType = 'C';
                     INSERT INTO PromotionOfferOrderBased(numProID, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled)
                     SELECT numProId, numValue, v_numDomainId, v_fltSubTotal, v_cSaleType, 'M', true FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5;
                  ELSE
                     DELETE FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId AND cSaleType = 'C';
                     INSERT INTO PromotionOfferOrderBased(numProID, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled)
                     SELECT numProId, numValue, v_numDomainId, v_fltSubTotal, v_cSaleType, 'S', true FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5;
                  end if;
               end if;
            ELSE
               IF(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5) = 0 then
						
                  RAISE EXCEPTION 'SELECT_ITEMS_FOR_PROMOTIONS';
                  RETURN;
               end if;
            end if;
         ELSEIF v_cSaleType = 'C'
         then
				
            DELETE FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND numDomainId = v_numDomainId;
            DELETE FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 5;
            INSERT INTO PromotionOfferOrderBased(numProID, cSaleType, numDomainId, bitEnabled)
					VALUES(v_numProId, v_cSaleType, v_numDomainId, true);
         end if;
         open SWV_RefCur for
         SELECT v_numProId;
      ELSEIF v_byteMode = 1
      then
			
         IF(SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND fltSubTotal = v_fltSubTotal) > 0 then
				
            RAISE EXCEPTION 'DUPLICATE_SUBTOTAL_AMOUNT';
            RETURN;
         end if;
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
            numProOrderBasedID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMP(numProOrderBasedID)
         SELECT numProOrderBasedID FROM PromotionOfferOrderBased WHERE numProID = v_numProId AND fltSubTotal = v_fltOldSubTotal;
         UPDATE PromotionOfferOrderBased
         SET fltSubTotal = v_fltSubTotal
         WHERE numProOrderBasedID IN(SELECT numProOrderBasedID FROM tt_TEMP);
      end if;
   end if;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/



