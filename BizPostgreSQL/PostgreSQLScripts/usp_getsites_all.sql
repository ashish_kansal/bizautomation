-- Stored procedure definition script USP_GetSites_All for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSites_All(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numSiteID,
        vcSiteName,
		vcDescription,
        vcHostName,
        numDomainID,
        numCreatedBy,
        vcLiveURL
	FROM 
		Sites
	WHERE 
		numDomainID > 0;
END; $$;














