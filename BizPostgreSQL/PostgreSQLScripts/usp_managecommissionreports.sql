-- Stored procedure definition script USP_ManageCommissionReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCommissionReports(v_numItemCode NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_tintAssignTo SMALLINT DEFAULT NULL,
v_bitCommContact BOOLEAN DEFAULT NULL,
v_numDomainID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numComReports  NUMERIC(9,0);
BEGIN
   if not exists(select * from CommissionReports where numItemCode = v_numItemCode and numContactID = v_numContactID AND tintAssignTo = v_tintAssignTo AND bitCommContact = v_bitCommContact AND numDomainID = v_numDomainID) then
      insert into CommissionReports(numItemCode,numContactID,tintAssignTo,bitCommContact,numDomainID)
	values(v_numItemCode,v_numContactID,v_tintAssignTo,v_bitCommContact,v_numDomainID);
	
      v_numComReports := CURRVAL('CommissionReports_seq');
      IF v_bitCommContact = false then
	
         insert into Dashboard(numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup,tintReportCategory)
		values(v_numComReports, v_numContactID, 1, 1, 2, '','', 2, false,2);
      end if;
   end if;
   RETURN;
END; $$;
	


