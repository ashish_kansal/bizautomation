DROP FUNCTION IF EXISTS USP_GetPartNo;

CREATE OR REPLACE FUNCTION USP_GetPartNo(v_numItemCode NUMERIC(9,0),    
v_numDivisionID NUMERIC(9,0),  
INOUT v_vcPartNo VARCHAR(100) ,  
INOUT v_monCost DECIMAL(20,5))
LANGUAGE plpgsql
   AS $$
BEGIN
   v_vcPartNo := '';
   v_monCost := 0;    
    
   select   coalesce(vcPartNo,''), coalesce(monCost,0) INTO v_vcPartNo,v_monCost from Vendor where numVendorID = v_numDivisionID and numItemCode = v_numItemCode;

   v_vcPartNo := COALESCE(v_vcPartNo,'');
   v_monCost := COALESCE(v_monCost,0);

   RETURN;
END; $$;


