CREATE OR REPLACE FUNCTION usp_GetSourceConclusion(                                   
v_numDomainID NUMERIC,                  
 v_dtFromDate TIMESTAMP,                  
 v_dtToDate TIMESTAMP,                  
 v_numUserCntID NUMERIC DEFAULT 0,                                
 v_tintType NUMERIC DEFAULT NULL,                  
 v_tintRights NUMERIC DEFAULT 0,                  
 v_intDealStatus NUMERIC DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 0 then                  
 -- ALL RECORDS RIGHTS                  
 
      open SWV_RefCur for
      SELECT  coalesce(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) AS Total,
   (COUNT(DM.numDivisionID)*100.00)/(select count(*) from DivisionMaster DM
         join CompanyInfo CI
         on DM.numCompanyID = CI.numCompanyId
         left join Listdetails LD
         on LD.numListItemID = CI.vcHow
         WHERE DM.numDomainID = v_numDomainID
         and DM.numCompanyID = CI.numCompanyId
         AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
         and DM.numRecOwner = v_numUserCntID -- Added By Anoop              
    ) as Percentage
      FROM DivisionMaster DM
      join CompanyInfo CI
      on DM.numCompanyID = CI.numCompanyId
      left join Listdetails LD
      on LD.numListItemID = CI.vcHow
      WHERE DM.numDomainID = v_numDomainID
      AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      and DM.numRecOwner = v_numUserCntID -- Added By Anoop            
     -- and CI.vcHow is not null -- Commented by Siva  
      GROUP BY  LD.vcData;
   end if;                  
                  
   If v_tintRights = 1 then                  
 -- OWNER RECORDS RIGHTS                  
 
      open SWV_RefCur for
      SELECT  coalesce(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) AS Total,
   (COUNT(DM.numDivisionID)*100.00)/(select count(*) from DivisionMaster DM
         join CompanyInfo CI
         on DM.numCompanyID = CI.numCompanyId
         left join Listdetails LD
         on LD.numListItemID = CI.vcHow
         join AdditionalContactsInformation  ADC
         on ADC.numContactId = DM.numRecOwner
         WHERE DM.numDomainID = v_numDomainID
         AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
         and  ADC.numTeam  in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)) as Percentage
      FROM DivisionMaster DM
      join CompanyInfo CI
      on DM.numCompanyID = CI.numCompanyId
      left join Listdetails LD
      on LD.numListItemID = CI.vcHow
      join AdditionalContactsInformation  ADC
      on ADC.numContactId = DM.numRecOwner
      WHERE DM.numDomainID = v_numDomainID            
     --AND CI.vcHow is not null       
      AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      and ADC.numTeam in (select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
      GROUP BY  LD.vcData;
   end if;                  
                  
   If v_tintRights = 2 then                  
 -- TERRITORY RECORDS RIGHTS                  
 
      open SWV_RefCur for
      SELECT  coalesce(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) AS Total,
   (COUNT(DM.numDivisionID)*100.00)/(select count(*) from DivisionMaster DM
         join CompanyInfo CI
         on DM.numCompanyID = CI.numCompanyId
         left join Listdetails LD
         on LD.numListItemID = CI.vcHow
         join AdditionalContactsInformation  ADC
         on ADC.numContactId = DM.numRecOwner
         WHERE DM.numDomainID = v_numDomainID            
     --and DM.numCompanyID=CI.numCompanyID             
         AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate                
     --and DM.numRecOwner in(select distinct(numUserCntID) from userterritory where numTerritoryID in(select F.numTerritory from ForReportsByTerritory F      
         and DM.numTerID  in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)) as Percentage
      FROM DivisionMaster DM
      join CompanyInfo CI
      on DM.numCompanyID = CI.numCompanyId
      left join Listdetails LD
      on LD.numListItemID = CI.vcHow
      join AdditionalContactsInformation  ADC
      on ADC.numContactId = DM.numRecOwner
      WHERE DM.numDomainID = v_numDomainID            
     --and CI.vcHow is not null                   
      AND DM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      and DM.numTerID in (select F.numTerritory from ForReportsByTerritory F
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
      GROUP BY LD.vcData;
   end if;
   RETURN;
END; $$;


