CREATE OR REPLACE FUNCTION USP_PromotionOffer_GetItemSelections(v_numProId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_tintRecordType SMALLINT,
	v_tintItemSelectionType SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintItemSelectionType = 1 then
	
      open SWV_RefCur for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID
      FROM
      Item
      JOIN
      PromotionOfferItems
      ON
      numValue = numItemCode
      WHERE
      numProId = v_numProId
      AND tintType = 1
      AND tintRecordType = v_tintRecordType;
   ELSEIF v_tintItemSelectionType = 4
   then
	
      open SWV_RefCur for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID
      FROM
      Item
      JOIN
      PromotionOfferItems
      ON
      numValue = numItemCode
      WHERE
      numProId = v_numProId
      AND tintType = 4
      AND tintRecordType = v_tintRecordType;
   ELSEIF v_tintItemSelectionType = 2
   then
	
      open SWV_RefCur for
      SELECT
      numProItemId
			,numProId
			,numValue
			,GetListIemName(numValue) AS ItemClassification,
			(SELECT COUNT(*) FROM Item I1 WHERE I1.numItemClassification = numValue) AS ItemsCount
      FROM
      PromotionOfferItems
      WHERE
      numProId = v_numProId
      AND tintType = 2
      AND tintRecordType = v_tintRecordType;
   end if;
   RETURN;
END; $$; 


