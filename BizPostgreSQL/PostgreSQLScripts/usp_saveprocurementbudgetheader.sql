-- Stored procedure definition script USP_SaveProcurementBudgetHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveProcurementBudgetHeader(v_intFiscalYear NUMERIC(9,0) DEFAULT 0,                  
v_bitPurchaseDeal BOOLEAN DEFAULT NULL,                  
v_numDomainId NUMERIC(9,0) DEFAULT 0,                  
v_numUserCntId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProcurementBudgetId  NUMERIC(9,0);
BEGIN
   Select coalesce(numProcurementBudgetId,0) INTO v_numProcurementBudgetId From ProcurementBudgetMaster Where numDomainID = v_numDomainId;       
               
   If v_numProcurementBudgetId is null then
 
      Insert into ProcurementBudgetMaster(intFiscalYear,bitPurchaseDeal,numDomainID,numCreatedBy,dtCreationDate)
  Values(v_intFiscalYear,v_bitPurchaseDeal,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
  
      v_numProcurementBudgetId := CURRVAL('ProcurementBudgetMaster_seq');
      open SWV_RefCur for
      Select v_numProcurementBudgetId;
   Else
      Update ProcurementBudgetMaster Set bitPurchaseDeal = v_bitPurchaseDeal,numModifiedBy = v_numUserCntId,dtModifiedDate = TIMEZONE('UTC',now())
      Where numProcurementBudgetId = v_numProcurementBudgetId  And numDomainID = v_numDomainId;
      open SWV_RefCur for
      Select v_numProcurementBudgetId;
   end if;
   RETURN;
END; $$;


