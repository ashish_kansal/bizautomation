-- Stored procedure definition script usp_GetPageDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetPageDetails(v_numPageID NUMERIC(9,0) DEFAULT 0,
	v_numModuleID  NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT      numModuleID, numPageID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable,
                      bitIsExportApplicable
   FROM         PageMaster
   WHERE     numModuleID = v_numModuleID AND numPageID = v_numPageID LIMIT 1;
END; $$;












