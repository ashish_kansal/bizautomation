-- Stored procedure definition script USP_OpportunityBizDocs_GetPickListWithNoFulfillment for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetPickListWithNoFulfillment(v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   OBDPickList.numOppBizDocsId
		,OBDPickList.vcBizDocID
   FROM
   OpportunityBizDocs OBDPickList
   LEFT JOIN
   OpportunityBizDocs OBDFilfillment
   ON
   OBDFilfillment.numoppid = v_numOppID
   AND OBDFilfillment.numBizDocId = 296 --Fulfillment BizDoc
   AND OBDFilfillment.numSourceBizDocId = OBDPickList.numOppBizDocsId
   WHERE
   OBDPickList.numoppid = v_numOppID
   AND OBDPickList.numBizDocId = 29397
   AND OBDFilfillment.numOppBizDocsId IS NULL;
END; $$;












