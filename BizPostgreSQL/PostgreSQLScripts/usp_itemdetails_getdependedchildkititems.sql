DROP FUNCTION IF EXISTS USP_ItemDetails_GetDependedChildKitItems;

CREATE OR REPLACE FUNCTION USP_ItemDetails_GetDependedChildKitItems(v_numDomainID NUMERIC(18,0)
	,v_numMainKitItemCode NUMERIC(18,0)
	,v_numChildKitItemCode NUMERIC(18,0)
	,v_numChildKitSelectedItem TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      numPrimaryListID NUMERIC(18,0),
      numChildKitItemCode NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(300),
      vcDisplayText VARCHAR(500),
      UnitName VARCHAR(100),
      Qty DOUBLE PRECISION,
      vcSaleUOMName VARCHAR(100),
      fltUOMConversion DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPITEMS(numPrimaryListID
		,numChildKitItemCode
		,numItemCode
		,vcItemName
		,vcDisplayText
		,UnitName
		,Qty
		,vcSaleUOMName
		,fltUOMConversion)
   SELECT
		FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		Item.numItemCode,
		coalesce(Item.vcItemName,'') AS vcItemName,
		CAST(GetKitChildItemsDisplay(v_numMainKitItemCode,FR.numSecondaryListID,Item.numItemCode) AS VARCHAR(500)) AS vcDisplayText,
		coalesce(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty,
		fn_GetUOMName(Item.numSaleUnit) AS vcSaleUOMName,
		coalesce(fn_UOMConversion(Item.numSaleUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit),1) AS fltUOMConversion
   FROM
   FieldRelationshipDTL FRD
   INNER JOIN
   FieldRelationship FR
   ON
   FRD.numFieldRelID = FR.numFieldRelID
   INNER JOIN
   (
	SELECT
		CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC) AS  numPrimaryListID
		,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS NUMERIC) AS numSelectedValue
	FROM
		SplitString(v_numChildKitSelectedItem,',')
   ) T
   ON
   T.numPrimaryListID = FR.numPrimaryListID
   AND FRD.numPrimaryListItemID = T.numSelectedValue
   INNER JOIN
   ItemDetails
   ON
   FR.numSecondaryListID = ItemDetails.numItemKitID
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   AND Item.numItemCode = FRD.numSecondaryListItemID
   LEFT JOIN
   UOM
   ON
   ItemDetails.numUOMID = UOM.numUOMId
   WHERE
   FR.numDomainID = v_numDomainID
   AND FR.numModuleID = 14
   AND FRD.numSecondaryListItemID <> -1
   AND EXISTS (SELECT
      FRInner.numSecondaryListID
      FROM
      FieldRelationship FRInner
      WHERE
      FRInner.numDomainID = v_numDomainID
      AND FRInner.numModuleID = 14
      AND FRInner.numPrimaryListID = v_numChildKitItemCode
	  AND FRInner.numSecondaryListID = FR.numSecondaryListID
      AND EXISTS (SELECT FRDInner.numFieldRelID FROM FieldRelationshipDTL FRDInner WHERE FRDInner.numFieldRelID = FRInner.numFieldRelID))
   UNION
   SELECT
   FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		-1,
		CAST('' AS VARCHAR(300)) AS vcItemName,
		CAST('' AS VARCHAR(500)) AS vcDisplayText,
		CAST('' AS VARCHAR(100)) AS UnitName,
		0 as Qty,
		CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)),
		CAST(1 AS DOUBLE PRECISION)
   FROM
   FieldRelationshipDTL FRD
   INNER JOIN
   FieldRelationship FR
   ON
   FRD.numFieldRelID = FR.numFieldRelID
   INNER JOIN
   (
	SELECT
		CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC) AS  numPrimaryListID
		,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS NUMERIC) AS numSelectedValue
	FROM
		SplitString(v_numChildKitSelectedItem,',')
   ) T
   ON
   T.numPrimaryListID = FR.numPrimaryListID
   AND FRD.numPrimaryListItemID = T.numSelectedValue
   INNER JOIN
   ItemDetails
   ON
   FR.numSecondaryListID = ItemDetails.numItemKitID
   WHERE
   FR.numDomainID = v_numDomainID
   AND FR.numModuleID = 14
   AND FRD.numSecondaryListItemID = -1
   AND EXISTS (SELECT
      FRInner.numSecondaryListID
      FROM
      FieldRelationship FRInner
      WHERE
      FRInner.numDomainID = v_numDomainID
      AND FRInner.numModuleID = 14
      AND FRInner.numPrimaryListID = v_numChildKitItemCode
	  AND FRInner.numSecondaryListID = FR.numSecondaryListID
      AND EXISTS (SELECT FRDInner.numFieldRelID FROM FieldRelationshipDTL FRDInner WHERE FRDInner.numFieldRelID = FRInner.numFieldRelID));

	ANALYZE tt_TEMPITEMS;

	open SWV_RefCur for
	SELECT
		numSecondaryListID AS "numChildKitItemCode"
		,coalesce(I.bitKitSingleSelect,true) AS "bitKitSingleSelect"
		,coalesce(ID.tintView,1) AS "tintView"
		,coalesce(ID.bitOrderEditable,true) AS "bitOrderEditable"
		,COALESCE((CASE WHEN EXISTS (SELECT TInner.numChildKitItemCode FROM tt_TEMPITEMS TInner WHERE TInner.numChildKitItemCode=FR.numSecondaryListID AND numItemCode=-1) THEN true ELSE false END),false) AS "bitDoNotShow"
	FROM
		FieldRelationship FR
	INNER JOIN
		Item I
	ON
		I.numItemCode = FR.numSecondaryListID
	LEFT JOIN
		ItemDetails ID 
	ON 
		ID.numItemKitID = v_numMainKitItemCode 
		AND ID.numChildItemID = numSecondaryListID
	WHERE
		FR.numDomainID = v_numDomainID
	   AND FR.numModuleID = 14
	   AND FR.numPrimaryListID = v_numChildKitItemCode
	   AND EXISTS (SELECT FRD.numFieldRelID FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID = FR.numFieldRelID)
	ORDER BY
		FR.numSecondaryListID;
	
   open SWV_RefCur2 for
   SELECT 
		numChildKitItemCode AS "numChildKitItemCode"
		,numItemCode AS "numItemCode"
		,vcItemName AS "vcItemName"
		,vcDisplayText AS "vcDisplayText"
		,UnitName AS "UnitName"
		,Qty AS "Qty"
		,vcSaleUOMName AS vcSaleUOMName
		,fltUOMConversion AS "fltUOMConversion"
   FROM(SELECT
      numChildKitItemCode
			,numItemCode
			,vcItemName
			,vcDisplayText
			,UnitName
			,Qty
			,vcSaleUOMName
			,fltUOMConversion
			,COALESCE(ID.sintOrder,0) sintOrder
      FROM
      tt_TEMPITEMS TI
		LEFT JOIN
			ItemDetails ID 
		ON 
			ID.numItemKitID = numChildKitItemCode 
			AND ID.numChildItemID = numItemCode
      GROUP BY
      numChildKitItemCode,numItemCode,vcItemName,vcDisplayText,UnitName,Qty,
      vcSaleUOMName,fltUOMConversion,sintOrder
      HAVING
      1 =(CASE
      WHEN coalesce((SELECT COUNT(DISTINCT TIInner.numPrimaryListID) FROM tt_TEMPITEMS TIInner WHERE TIInner.numChildKitItemCode = TI.numChildKitItemCode),
      0) > 1
      THEN(CASE WHEN COUNT(*) =(SELECT COUNT(*) FROM(SELECT numPrimaryListID FROM tt_TEMPITEMS TI1 WHERE TI1.numChildKitItemCode = TI.numChildKitItemCode GROUP BY TI1.numPrimaryListID,TI1.numChildKitItemCode) TEMP2) THEN 1 ELSE 0 END)
      ELSE 1
      END)) T
   WHERE
   numItemCode <> -1 ORDER BY sintOrder,vcItemName;
   RETURN;
END; $$;


