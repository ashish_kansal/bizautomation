-- Stored procedure definition script usp_GetCommissionRuleContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCommissionRuleContacts(v_numComRuleID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintAssignTo  SMALLINT;
BEGIN
   select   tintAssignTo INTO v_tintAssignTo FROM CommissionRules WHERE numComRuleID = v_numComRuleID;

   IF v_tintAssignTo = 3 then
	
      open SWV_RefCur for
      SELECT
      CONCAT(CC.numValue,'~',CC.bitCommContact,'~1') AS numContactID,
			CONCAT(D.vcPartnerCode,'-',C.vcCompanyName) as vcUserName
      FROM
      CommissionRuleContacts CC
      INNER JOIN
      DivisionMaster D
      ON
      CC.numValue = D.numDivisionID
      INNER JOIN
      CompanyInfo C
      ON
      D.numCompanyID = C.numCompanyId
      WHERE
      CC.numComRuleId = v_numComRuleID;
   ELSE
      open SWV_RefCur for
      SELECT
      CONCAT(CC.numValue,'~',CC.bitCommContact,'~0') AS numContactID,
			CONCAT(fn_GetContactName(numValue),CASE CC.bitCommContact WHEN true THEN '(' || coalesce((SELECT CompanyInfo.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo ON DM.numCompanyID = CompanyInfo.numCompanyId WHERE ACI.numContactId = CC.numValue),'') || ')' ELSE '' END) as vcUserName
      FROM
      CommissionRuleContacts CC
      WHERE
      CC.numComRuleId = v_numComRuleID;
   end if;
   RETURN;
END; $$;	


