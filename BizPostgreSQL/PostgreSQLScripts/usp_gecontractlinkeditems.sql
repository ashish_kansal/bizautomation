-- Stored procedure definition script USP_GeContractLinkedItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GeContractLinkedItems(v_numContractId NUMERIC(9,0),
v_numDomainId NUMERIC,
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select case when tintTEType = 0 then 'T & E'
   when tintTEType = 1 then
      Case when numCategory = 1 then
         case when(select tintopptype from OpportunityMaster  where numOppId = TE.numOppId) = 1 then 'Sales Time' else 'Purch Time' end
      else
         case when(select tintopptype from OpportunityMaster  where numOppId = TE.numOppId) = 1 then 'Sales Exp' else 'Purch Exp' end
      end
   when tintTEType = 2 then
      Case when numCategory = 1 then 'Project Time' else 'Project Exp' end
   when tintTEType = 3 then
      Case when numCategory = 1 then 'Case Time' else 'Case Exp' end
   end as chrFrom,
  Case when numCategory = 1 then 'Time (' ||(CAST(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30)) || '-' || CAST(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30))) || ')'
   when numCategory = 2 then 'Expense'
   when numCategory = 3 then 'Leave' end as Category ,
  Case when numCategory = 1 then CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30)) || ' (R/H ' || CAST(monAmount AS VARCHAR(30)) || ' )'
   when numCategory = 2  then CAST(monAmount AS VARCHAR(30))
   when numCategory = 3 then(Case when bitFromFullDay = false  then  'HDL'
      when bitFromFullDay = true then  'FDL' end)
   end as Detail
   from
   timeandexpense TE
   where numcontractId = v_numContractId and numDomainID = v_numDomainId;
END; $$;












