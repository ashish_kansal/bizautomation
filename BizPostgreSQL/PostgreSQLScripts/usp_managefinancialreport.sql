-- Stored procedure definition script USP_ManageFinancialReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFinancialReport(INOUT v_numFRID NUMERIC(18,0) DEFAULT 0 ,
    v_vcReportName VARCHAR(50) DEFAULT NULL,
    v_numFinancialViewID NUMERIC(18,0) DEFAULT NULL,
    v_numFinancialDimensionID NUMERIC(18,0) DEFAULT NULL,
    v_intDateRange NUMERIC(18,0) DEFAULT NULL,
    v_numDomainID NUMERIC(18,0) DEFAULT NULL,
    v_numUserCntID NUMERIC DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldDimension  NUMERIC;
BEGIN
   IF v_numFRID = 0 then
    
      INSERT  INTO FinancialReport(vcReportName,
                                               numFinancialViewID,
                                               numFinancialDimensionID,
                                               intDateRange,
                                               numDomainID,
                                               numCreatedBy,
                                               dtCreateDate,
                                               numModifiedBy,
                                               dtModifiedDate)
      SELECT  v_vcReportName,
                        v_numFinancialViewID,
                        v_numFinancialDimensionID,
                        v_intDateRange,
                        v_numDomainID,
                        v_numUserCntID,
                        TIMEZONE('UTC',now()),
                        v_numUserCntID,
                        TIMEZONE('UTC',now());
      v_numFRID := CURRVAL('FinancialReport_seq');
   ELSE
      select   numFinancialDimensionID INTO v_numOldDimension FROM FinancialReport WHERE numFRID = v_numFRID;
      IF v_numOldDimension <> v_numFinancialDimensionID then
		
         DELETE FROM FinancialReportDetail WHERE numFRID = v_numFRID;
      end if;
      UPDATE  FinancialReport
      SET     vcReportName = v_vcReportName,numFinancialViewID = v_numFinancialViewID,
      numFinancialDimensionID = v_numFinancialDimensionID,intDateRange = v_intDateRange,
      numDomainID = v_numDomainID,numModifiedBy = v_numUserCntID,
      dtModifiedDate = TIMEZONE('UTC',now())
      WHERE   numFRID = v_numFRID;
   end if;
   RETURN;
END; $$;
    


