-- Stored procedure definition script USP_ProMilestone for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProMilestone(v_ProcessID NUMERIC(9,0) DEFAULT NULL,
                                           v_ContactID NUMERIC(9,0) DEFAULT NULL,
                                           v_DomianID NUMERIC(9,0) DEFAULT NULL,
                                           v_numProId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_ProcessID != 0 then
    
      open SWV_RefCur for
      SELECT  mst.numstagepercentage,
                dtl.numStageDetailsId,
                dtl.vcStageName,
                0 AS StageID,
                v_DomianID AS numDomainId,
                v_ContactID AS numCreatedBy,
                TIMEZONE('UTC',now()) AS bintCreatedDate,
                v_ContactID AS numModifiedBy,
                TIMEZONE('UTC',now()) AS bintModifiedDate,
                TIMEZONE('UTC',now()) AS bintDueDate,
                TIMEZONE('UTC',now()) AS bintStageComDate,
                '' AS vcComments,
                numAssignTo,
                coalesce(fn_GetContactName(numAssignTo),'-') AS vcAssignTo,
                false AS bitAlert,
                false AS bitStageCompleted,
                0 AS Op_Flag,
                coalesce(fn_GetContactName(v_ContactID),'-') AS numModifiedByName,
                0 AS numOppStageID,
                0 AS Depend,
                0 AS numTCategoryHDRID,
                0 AS numECategoryHDRID,
                0 AS Expense,
                '0' AS Time,
                0 AS SubStg,
                0 AS numStage,
                '' AS vcStage,
                coalesce(vcMileStoneName,'') AS vcMileStoneName,
                coalesce(tintPercentage,0) AS tintPercentage,
                coalesce(bitClose,false) AS bitClose,
                0 AS numCommActId
      FROM    stagepercentagemaster mst
      JOIN StagePercentageDetails dtl ON dtl.numStagePercentageId = mst.numstagepercentageid
      WHERE   dtl.slp_id = v_ProcessID
      ORDER BY numStagePercentage,numOppstageid;
   ELSE
      open SWV_RefCur for
      SELECT  numstagepercentage,
                numProStageId AS numstagedetailsID,
                vcstagedetail,
                numProStageId AS StageID,
                v_DomianID AS numDomainId,
                numCreatedBy,
                bintCreatedDate,
                numModifiedBy,
                bintModifiedDate,
                bintDueDate,
                bintStageComDate,
                vcComments,
                coalesce(numAssignTo,0) AS numAssignTo,
                coalesce(fn_GetContactName(numAssignTo),'-') AS vcAssignTo,
                bitAlert,
                bitStageCompleted,
                0 AS Op_Flag,
                coalesce(fn_GetContactName(numModifiedBy),'-') AS numModifiedByName,
                numProStageId,
                fn_GetDepDtlsbyProStgID(v_numProId,numProStageId,0::SMALLINT) AS Depend,
                0 AS numTCategoryHDRID,
                0 AS numECategoryHDRID,
                fn_GetExpenseDtlsbyProStgID(v_numProId,numProStageId,0::SMALLINT) AS Expense,
                fn_GeTimeDtlsbyProStgID(v_numProId,numProStageId,0::SMALLINT) AS Time,
                fn_GetSubStgDtlsbyProStgID(v_numProId,numProStageId,0::SMALLINT) AS SubStg,
                coalesce(numStage,0) AS numStage,
                (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = numStage) AS vcStage,
                coalesce(vcStagePercentageDtl,'') AS vcStagePercentageDtl,
                coalesce(numTemplateId,0) AS numTemplateId,
                coalesce(tintPercentage,0) AS tintPercentage,
                coalesce(numEvent,0) AS numEvent,
                coalesce(numReminder,0) AS numReminder,
                coalesce(numET,0) AS numET,
                coalesce(numActivity,0) AS numActivity,
                coalesce(numStartDate,0) AS numStartDate,
                coalesce(numStartTime,0) AS numStartTime,
                coalesce(numStartTimePeriod,15) AS numStartTimePeriod,
                coalesce(numEndTime,0) AS numEndTime,
                coalesce(numEndTimePeriod,15) AS numEndTimePeriod,
                coalesce(txtCom,'') AS txtCom,
                coalesce(numChgStatus,0) AS numChgStatus,
                coalesce(bitChgStatus,false) AS bitChgStatus,
                coalesce(bitClose,false) AS bitClose,
                coalesce(numType,0) AS numType,
                coalesce(numCommActId,0) AS numCommActId
      FROM    ProjectsStageDetails PSD
      WHERE   numProId = v_numProId
      ORDER BY numstagepercentage,numProStageId;
   end if;
   RETURN;
END; $$;


