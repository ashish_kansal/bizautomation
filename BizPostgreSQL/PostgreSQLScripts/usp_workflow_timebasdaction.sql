Create or replace FUNCTION USP_WorkFlow_TimeBasdAction
(
	v_numRecordID NUMERIC(18,0) DEFAULT 0,      
	v_TableName VARCHAR(50) DEFAULT NULL,    
	v_columnname VARCHAR(50) DEFAULT NULL,    
	v_intDays INTEGER DEFAULT NULL,    
	v_intActionOn INTEGER DEFAULT NULL,    
	v_numDomainID NUMERIC(18,0) DEFAULT 0    ,
	v_intCustom INTEGER DEFAULT 0,
	v_numFormID NUMERIC(18,0) DEFAULT 0,
	v_Fld_ID NUMERIC(18,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql    
AS $$
	DECLARE
	v_MatchingDate  TIMESTAMP;    
	v_CurrentDate  TIMESTAMP;
	v_SQL TEXT;    
	v_result VARCHAR(50);
BEGIN
	v_CurrentDate := LOCALTIMESTAMP:: date;    
    
	IF (v_intCustom = 0) then
		IF (v_TableName = 'OpportunityMaster') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numOppID=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'StagePercentageDetails') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numStageDetailsId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'DivisionMaster') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numDivisionID=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'ProjectsMaster') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numProId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'AdditionalContactsInformation') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numContactId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'Cases') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numCaseId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_TableName = 'Communication') then
			v_SQL := 'SELECT ' || replace(v_columnname,' ','') || ' FROM ' || coalesce(v_TableName,'') || ' WHERE numCommId=' || COALESCE(v_numRecordID,0);
		ELSE			
			RAISE NOTICE 'Nothing';
		end if;
	ELSE
		IF (v_numFormID = 68) then --organization	 
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_FLD_Values  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_numFormID = 69) then    --Contacts
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_FLD_Values_Cont  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_numFormID = 70) then    --Opportunities and Orders
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_Fld_Values_Opp  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_numFormID = 73) then --Projects    
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_FLD_Values_Pro  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		ELSEIF (v_numFormID = 72) then --Cases    
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_FLD_Values_Case  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		ELSE
			v_SQL := 'SELECT FormatedDateFromDate(Fld_Value,' || COALESCE(v_numDomainID,0) || ') FROM CFW_FLD_Values  WHERE  Fld_ID=' || COALESCE(v_Fld_ID,0) || ' and RecId=' || COALESCE(v_numRecordID,0);
		end if;
	end if;

	EXECUTE v_SQL INTO v_MatchingDate;    
	
	v_result := CAST(0 AS VARCHAR(50));    
   
	IF (v_intActionOn = 1) then
		IF (v_CurrentDate =(v_MatchingDate:: date)) then
			v_result := CAST(1 AS VARCHAR(50));
		ELSE
			v_result := CAST(0 AS VARCHAR(50));
		end if;
	ELSE
		IF (v_CurrentDate =(v_MatchingDate:: date)) then
			v_result := CAST(1 AS VARCHAR(50));
		ELSE
			v_result := CAST(0 AS VARCHAR(50));
		end if;
	end if;    
       
	open SWV_RefCur for SELECT v_result AS TimeMatchStatus;
END; $$; 