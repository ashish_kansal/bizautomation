-- Stored procedure definition script USP_GetTimeAndMaterials for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeAndMaterials(v_numProId NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ProjName  VARCHAR(100);
BEGIN
   select   vcProjectName INTO v_ProjName FROM    ProjectsMaster WHERE   numProId = v_numProId;
        
   open SWV_RefCur for SELECT  TE.numCategoryHDRID,
                CASE numType
   WHEN 1 THEN 'Billable Time'
   ELSE 'Non-Billable Time'
   END AS TimeType,
                TE.numOppItemID,
                I.vcItemName,
                OB.vcBizDocID,
                CASE numType
   WHEN 1 THEN coalesce(numUnitHour,0)
   ELSE coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
   END AS numUnitHour,
                cast(coalesce(monPrice,0) as NUMERIC(18,0)) AS monPrice,
                OB.numOppBizDocsId,
                FormatedDateFromDate(TE.dtTCreatedOn,v_numDomainID) AS dtDateEntered,
                fn_GetContactName(TE.numUserCntID) AS ContactName,
                v_ProjName AS vcProjectName
   FROM    timeandexpense TE
   LEFT OUTER JOIN OpportunityBizDocs OB ON TE.numOppBizDocsId = OB.numOppBizDocsId
   LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = TE.numOppBizDocsId
   LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
   WHERE   numProId = v_numProId
   AND TE.numDomainID = v_numDomainID
   UNION
   SELECT  0,
                CASE OM.tintopptype
   WHEN 1 THEN 'S.O'
   WHEN 2 THEN 'P.O'
   END AS TimeType,
                TE.numOppItemID,
                I.vcItemName,
                OB.vcBizDocID,
                coalesce(OBI.numUnitHour,0) AS numUnitHour,
                coalesce(OBI.monPrice,0) AS monPrice,
                OB.numOppBizDocsId,
                FormatedDateFromDate(OB.dtCreatedDate,v_numDomainID) AS dtDateEntered,
                fn_GetContactName(OB.numCreatedBy) AS ContactName,
                v_ProjName AS vcProjectName
   FROM    ProjectsOpportunities PO
   LEFT OUTER JOIN OpportunityMaster OM ON PO.numOppId = OM.numOppId
   LEFT OUTER JOIN OpportunityBizDocs OB ON PO.numOppBizDocID = OB.numOppBizDocsId
   LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
   LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
   LEFT OUTER JOIN timeandexpense TE ON TE.numProId = PO.numProId
   AND TE.numOppBizDocsId = PO.numOppBizDocID
   WHERE   PO.numProId = v_numProId
   AND PO.numDomainId = v_numDomainID
   AND OBI.numOppItemID > 0
   AND TE.numCategoryHDRID IS NULL; --added because qury was getting duplicate of billable time .. as it is also linked with project

--                AND OM.[tintOppType] = 2


        
END; $$;	
    
    
--    exec USP_ProMilestone @ProcessID=0,@ContactID=17,@DomianID=72,@numProId=191












