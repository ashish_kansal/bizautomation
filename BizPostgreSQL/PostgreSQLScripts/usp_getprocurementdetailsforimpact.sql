-- Stored procedure definition script USP_GetProcurementDetailsForImpact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProcurementDetailsForImpact(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_intFiscalYear INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select distinct CAST(IG.numItemGroupID AS VARCHAR) || '~' || CAST(PBM.numProcurementBudgetId AS VARCHAR) as  numProcurementDetId, IG.vcItemGroup as vcItemGroup From ProcurementBudgetMaster PBM
   inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId
   inner join ItemGroups IG on PBD.numItemGroupId = IG.numItemGroupID
   Where PBM.numDomainID = v_numDomainId;     -- PBM.intFiscalYear =@intFiscalYear And      
END; $$;
