-- FUNCTION: public.usp_getimportconfiguration(numeric, numeric, smallint, refcursor, refcursor)

-- DROP FUNCTION public.usp_getimportconfiguration(numeric, numeric, smallint, refcursor, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getimportconfiguration(
	v_numrelation numeric,
	v_numdomain numeric,
	v_importtype smallint,
	INOUT swv_refcur refcursor,
	INOUT swv_refcur2 refcursor)
    RETURNS record
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF v_ImportType = 1 then
--Available fields               
      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10)) || '/0' || '/R' as numFormFieldId from DynamicFormFieldMaster where numFormId = 9
      and numFormFieldId not in(select numFormFieldId from RecImportConfg where numDomainID = v_numDomain and bitCustomFld = false and cCtype = 'R' and numRelationShip = v_numRelation and Importtype = 1)
      union
      select fld_label as vcFormFieldName ,CAST(fld_id AS VARCHAR(10)) || '/1' || '/L' as numFormFieldId  from CFW_Fld_Master
      join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on CAST(subgrp As Numeric) = CFw_Grp_Master.Grp_id
      where CFW_Fld_Master.Grp_id IN(1,12,13,14) /*Leads/Prospects/Accounts*/ and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomain  and fld_id not in(select DTL.numFormFieldId from
         RecImportConfg DTL where  DTL.numDomainID = v_numDomain and bitCustomFld = true and cCtype = 'L' and numRelationShip = v_numRelation  and Importtype = 1)
      union
      select fld_label as vcFormFieldName ,CAST(fld_id AS VARCHAR(10)) || '/1' || '/C' as numFormFieldId  from CFW_Fld_Master
      join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on CAST(subgrp As Numeric) = CFw_Grp_Master.Grp_id
      where CFW_Fld_Master.Grp_id = 4 /*Contact Details*/and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomain
      and fld_id not in(select DTL.numFormFieldId from
         RecImportConfg DTL where  DTL.numDomainID = v_numDomain and bitCustomFld = true and cCtype = 'C' and numRelationShip = v_numRelation and Importtype = 1) order by 1;       
              
              
             
                
--Added fields          
      open SWV_RefCur2 for
      select HDR.vcFormFieldName,CAST(DTL.numFormFieldId AS VARCHAR(10)) || '/0' || '/' || cCtype as numFormFieldId,intcolumn
      from RecImportConfg DTL
      join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId
      where DTL.numDomainID = v_numDomain and DTL.numRelationShip = v_numRelation and bitCustomFld = false and Importtype = 1
      union
      select CFW.FLd_label as vcFormFieldName,CAST(DTL.numFormFieldId AS VARCHAR(10)) || '/1' || '/' || cCtype as numFormFieldId,intcolumn
      from RecImportConfg DTL
      join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id
      where DTL.numDomainID = v_numDomain and DTL.numRelationShip = v_numRelation and bitCustomFld = true   and Importtype = 1
      order by 3;
   ELSEIF v_ImportType = 2 OR v_ImportType = 4
   then

      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10)) || '/0' || '/R' as numFormFieldId from DynamicFormFieldMaster where bitDeleted = false AND numFormId in(20,27) --,26,27,28
      and numFormFieldId not in(select numFormFieldId from RecImportConfg where numDomainID = v_numDomain and bitCustomFld = false and cCtype = 'R' and Importtype = v_ImportType)
      AND (v_ImportType = 2 OR (vcDbColumnName <>(CASE WHEN v_ImportType = 4 THEN   'numWareHouseID' ELSE vcDbColumnName END)
			AND vcDbColumnName <> (CASE WHEN v_ImportType = 4 THEN 'numOnHand' ELSE vcDbColumnName END)
			AND vcDbColumnName <> (CASE WHEN v_ImportType = 4 THEN 'vcLocation' ELSE vcDbColumnName END)))
      union
      select fld_label as vcFormFieldName ,CAST(fld_id AS VARCHAR(10)) || '/1' || '/I' as numFormFieldId  from CFW_Fld_Master
      left join CFw_Grp_Master
      on CAST(subgrp As Numeric) = CFw_Grp_Master.Grp_id
      where CFW_Fld_Master.Grp_id = 5  and CFW_Fld_Master.numDomainID = v_numDomain  and fld_id not in(select DTL.numFormFieldId from
         RecImportConfg DTL where  DTL.numDomainID = v_numDomain and bitCustomFld = true and cCtype = 'I'   and Importtype = v_ImportType) order by 1;

      open SWV_RefCur2 for
      select HDR.vcFormFieldName,CAST(DTL.numFormFieldId AS VARCHAR(10)) || '/0' || '/' || cCtype as numFormFieldId,intcolumn
      from RecImportConfg DTL
      join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId
      where bitDeleted = false AND DTL.numDomainID = v_numDomain  and bitCustomFld = false and Importtype = v_ImportType
      AND
(v_ImportType = 2 OR
	(vcDbColumnName <>(CASE WHEN v_ImportType = 4 THEN   'numWareHouseID' ELSE vcDbColumnName END)
      AND  vcDbColumnName <>(CASE WHEN v_ImportType = 4 THEN   'numOnHand' ELSE vcDbColumnName END)
      AND  vcDbColumnName <>(CASE WHEN v_ImportType = 4 THEN   'vcLocation' ELSE vcDbColumnName END)))
      union
      select CFW.FLd_label as vcFormFieldName,CAST(DTL.numFormFieldId AS VARCHAR(10)) || '/1' || '/' || cCtype as numFormFieldId,intcolumn
      from RecImportConfg DTL
      join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id
      where DTL.numDomainID = v_numDomain and bitCustomFld = true   and Importtype = v_ImportType
      order by 3;
   ELSEIF v_ImportType = 3
   then

      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10))  as numFormFieldId,0 as intcolumn,false as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormId = 25;
   ELSEIF v_ImportType = 5
   then --Import Assembly Items

      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10)) as numFormFieldId,0 as intcolumn,false as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormId = 31
      order by numFormFieldId;
   ELSEIF v_ImportType = 6
   then --Import Tax Details

      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10)) as numFormFieldId,0 as intcolumn,false as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormId = 45
      order by numFormFieldId;
   ELSEIF v_ImportType = 7
   then --Import Update Item Warehouse Information/Matrix Item

      open SWV_RefCur for
      select vcFormFieldName,CAST(numFormFieldId AS VARCHAR(10)) as numFormFieldId,"Order",false as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName,numListID from DynamicFormFieldMaster where numFormId = 48
      UNION
      select Fld_label as vcFormFieldName,Fld_id as numFormFieldId,100 as "order",false as bitCustomFld,'C' as cCtype,fld_type as vcAssociatedControlType,'' as vcDbColumnName,CFW_Fld_Master.numlistid
      from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID = Fld_id where numItemGroupID = v_numRelation and tintType = 2
      order by 3;
   ELSEIF v_ImportType IN(136,137,140)
   then

      open SWV_RefCur for
      SELECT
      numFieldID
		,vcFieldName
      FROM
      DycFormField_Mapping
      WHERE
      numFormID = v_ImportType
      AND numFieldID NOT IN(SELECT numFieldID FROM View_DynamicColumns WHERE numDomainID = v_numDomain AND numFormId = v_ImportType);
                         
	--Added fields          
      open SWV_RefCur2 for
      SELECT
      numFieldID
		,vcFieldName
      FROM
      View_DynamicColumns
      WHERE
      numDomainID = v_numDomain
      AND numFormId = v_ImportType;
   end if;

   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getimportconfiguration(numeric, numeric, smallint, refcursor, refcursor)
    OWNER TO postgres;
