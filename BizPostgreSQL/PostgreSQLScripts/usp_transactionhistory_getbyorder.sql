-- Stored procedure definition script USP_TransactionHistory_GetByOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TransactionHistory_GetByOrder(v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT
   TH.numTransHistoryID,
		TH.numDivisionID,
		TH.numContactID,
		TH.numOppID,
		TH.numOppBizDocsID,
		vcTransactionID,
		tintTransactionStatus,
		(CASE WHEN tintTransactionStatus = 1 THEN 'Authorized/Pending Capture'
   WHEN tintTransactionStatus = 2 THEN 'Captured'
   WHEN tintTransactionStatus = 3 THEN 'Void'
   WHEN tintTransactionStatus = 4 THEN 'Failed'
   WHEN tintTransactionStatus = 5 THEN 'Credited'
   ELSE 'Not Authorized'
   END) AS vcTransactionStatus,
		vcPGResponse,
		tintType,
		monAuthorizedAmt,
		monCapturedAmt,
		monRefundAmt,
		vcCardHolder,
		vcCreditCardNo,
		coalesce((SELECT vcData FROM Listdetails WHERE numListItemID = coalesce(numCardType,0)),'-') as vcCardType,
		vcCVV2,
		tintValidMonth,
		intValidYear,
		TH.dtCreatedDate,
		coalesce(OBD.vcBizDocID,coalesce(OM.vcpOppName,'')) AS vcBizOrderID,
		vcSignatureFile,
		coalesce(tintopptype,0) AS tintOppType,
		coalesce(vcResponseCode,'') AS vcResponseCode
		,coalesce(DD.monAmountPaid,0) AS monAmountPaid
		,coalesce(TH.intPaymentGateWay,0) AS intPaymentGateWay
   FROM
   TransactionHistory TH
   INNER JOIN
   OpportunityMaster OM
   ON
   TH.numOppID = OM.numOppId
   LEFT JOIN
   DepositMaster DM
   ON
   TH.numTransHistoryID = DM.numTransHistoryID
   LEFT JOIN
   DepositeDetails DD
   ON
   DM.numDepositId = DD.numDepositID
   LEFT JOIN
   OpportunityBizDocs OBD
   ON
   OBD.numoppid = OM.numOppId
   AND OBD.numOppBizDocsId = DD.numOppBizDocsID
   WHERE
   TH.numDomainID = v_numDomainID
   AND TH.numOppID = v_numOppID;
END; $$;
	












