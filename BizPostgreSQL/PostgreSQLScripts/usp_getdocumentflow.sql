-- Stored procedure definition script USP_GetDocumentFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDocumentFlow(v_numRecID NUMERIC(9,0) DEFAULT 0,        
v_strType VARCHAR(10) DEFAULT '',          
v_numCategory NUMERIC(9,0) DEFAULT 0,
v_tintUserRightType SMALLINT DEFAULT 0, 
v_numDomainID NUMERIC(9,0) DEFAULT 0 ,
v_tintSortOrder SMALLINT DEFAULT 4,                         
v_SortChar CHAR(1) DEFAULT '0',
v_KeyWord VARCHAR(100) DEFAULT '', 
v_CurrentPage INTEGER DEFAULT NULL,  
v_PageSize INTEGER DEFAULT NULL ,
v_columnName VARCHAR(50) DEFAULT NULL,                    
v_columnSortOrder VARCHAR(10) DEFAULT NULL,
v_numDocStatus NUMERIC(9,0) DEFAULT 0,
INOUT v_TotRecs INTEGER  DEFAULT NULL,
v_numFilter INTEGER DEFAULT 0,
v_numUserId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDocumentSection  VARCHAR(1);
   v_strSql  VARCHAR(8000);                    
   v_intCustomPageRow  NUMERIC(8,0);
BEGIN
   drop table IF EXISTS tt_TEMPDOCFLOW CASCADE;
   create TEMPORARY TABLE tt_TEMPDOCFLOW
   (
      Organization VARCHAR(200),
      "Document Name"  VARCHAR(200),
      "Document Category" VARCHAR(200),
      "File Type" VARCHAR(50),
      "Document Status" VARCHAR(150),
      Pending NUMERIC(8,0),
      Approved NUMERIC(8,0),
      Declined  NUMERIC(8,0),
      CreatedBy VARCHAR(150),
      ModifiedOn TIMESTAMP,
      numGenericDocID NUMERIC(8,0),
      VcFileName VARCHAR(150),
      cUrlType  VARCHAR(150),
      numDivisionId NUMERIC(8,0),
      tintCRMType NUMERIC(8,0),
      numOppId NUMERIC(8,0),
      BizDocTypeId VARCHAR(1)
   );

   v_vcDocumentSection := 'D';

	
   select   coalesce(tintCustomPagingRows,20) INTO v_intCustomPageRow from Domain where numDomainId = v_numDomainID;

   v_strSql := 'select ';  
  
   if (v_tintSortOrder = 5 or v_tintSortOrder = 6) then 
      v_strSql := coalesce(v_strSql,'') || ' DISTINCT top  ' || SUBSTR(cast(v_intCustomPageRow  as VARCHAR(4)),1,4);
   ELSE 
      v_strSql := coalesce(v_strSql,'') || ' Distinct ';
   end if;

   v_strSql := coalesce(v_strSql,'') ||
   ' G.vcCompanyName,
"Order Id",
OrderType || cast(BizDocID as varchar(50)),NULL AS FILETYPE ,
H.vcData ,
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=0), 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=1) , 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=2) ,
fn_GetContactName(B.BizDocCreatedBy) ,
BizDocModifiedDate ,
A.numOppBizDocsId ,
'''' as VcFileName,
'''' as cUrlType,
f.numDivisionId,
f.tintCRMType,
numOppId,
''B'' as BizDocTypeId
from BizActionDetails A, 
	View_BizDoc B, 
	BizDocAction D,
	AdditionalContactsInformation E,
	DivisionMaster F,
	CompanyInfo G,
	ListDetails H
where A.numOppBizDocsId=b.numOppBizDocsId and 
	A.numBizActionID=D.numBizActionID and 
	D.numDomainID=B.numDomainID and 
	D.numDomainID=' || SUBSTR(cast(v_numDomainID as VARCHAR(10)),1,10) || ' and 
	D.numAssign=' || SUBSTR(cast(v_numRecID as VARCHAR(10)),1,10) || ' and 
	A.btDocType=1  and 
	E.numContactId=BizDocCreatedBy and 
	F.numDivisionID=b.DivisionID and
	G.numCompanyId=F.numCompanyId	and 
	btDocType=1 and 
	H.numListItemid=B.numBizDocId';
   if v_numDocStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and b.numBizDocId=' || SUBSTR(cast(v_numDocStatus as VARCHAR(10)),1,10);
   end if; 
   if v_numCategory <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and numBizDocStatus=' || SUBSTR(cast(v_numCategory as VARCHAR(10)),1,10);
   end if; 
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocId like ''' || coalesce(v_SortChar,'') || '%''';
   end if; 
   if v_KeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocId like ''' || coalesce(v_KeyWord,'') || '%'''
      || ' order by BizDocModifiedDate desc';
   end if;
	
   RAISE NOTICE '%',v_strSql;

   EXECUTE 'insert into tt_TEMPDOCFLOW ' || v_strSql;
   v_strSql := 'select ';  
  

   v_strSql := 'select distinct 
E.vcCompanyName,
"Order Id",
OrderType,
NULL AS FILETYPE,
BizDocId,
(select count(f.tintApprove) from DocumentWorkflow f where f.cDocType = ''B'' and f.numDocID = c.numDocID and f.tintApprove = 0),
(select count(f.tintApprove) from DocumentWorkflow f where f.cDocType = ''B'' and f.numDocID = c.numDocID and f.tintApprove = 1),
(select count(f.tintApprove) from DocumentWorkflow f where f.cDocType = ''B'' and f.numDocID = c.numDocID and f.tintApprove = 2),
fn_GetContactName(A.BizDocCreatedBy) ,
BizDocModifiedDate ,
A.numOppBizDocsId ,
'''' as VcFileName,
'''' as cUrlType,
D.numDivisionID,
D.tintCRMType,
A.numOppId,
''B'' as BizDocTypeId
from 
View_BizDoc A inner join OpportunityMaster B on A.numOppId = B.numOppId 
inner join(select distinct numDocID from DocumentWorkflow where cDocType = ''B'' and tintApprove = 0) C
on C.numDocID = A.numOppBizDocsId inner join  DivisionMaster D 
on D.numDivisionID = B.numDivisionId inner join CompanyInfo E
on E.numCompanyId = D.numCompanyID and A.numOppBizDocsId not in(select numGenericDocID from tt_TEMPDOCFLOW)';
   if v_numDocStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and A.numBizDocId=' || SUBSTR(cast(v_numDocStatus as VARCHAR(10)),1,10);
   end if; 
   if v_numCategory <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and numBizDocStatus=' || SUBSTR(cast(v_numCategory as VARCHAR(10)),1,10);
   end if; 
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocId ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if; 
   if v_KeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocId like ''' || coalesce(v_KeyWord,'') || '%''';
   end if; 
   if v_numFilter = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocCreatedBy =' || SUBSTR(cast(v_numUserId as VARCHAR(10)),1,10);
   end if;
   if v_numFilter = 2 then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocCreatedBy not in(' || SUBSTR(cast(v_numUserId as VARCHAR(10)),1,10) || ')';
   end if; 
   if v_numFilter = 4 then 
      v_strSql := coalesce(v_strSql,'') || ' and dtCreatedDate >=dateadd(day,-7,getutcdate()) ';
   end if;  
   if v_numFilter = 5 then 
      v_strSql := coalesce(v_strSql,'') || ' and dtCreatedDate >= dateadd(day,-20,getutcdate()) and BizDocCreatedBy =' || SUBSTR(cast(v_numUserId as VARCHAR(10)),1,10);
   end if; 
   if v_numFilter = 6 then 
      v_strSql := coalesce(v_strSql,'') || ' and BizDocModifiedDate >= TIMEZONE(''UTC'',now())+INTERVAL ''-20 day'' and BizDocCreatedBy =' || SUBSTR(cast(v_numUserId as VARCHAR(10)),1,10);
   end if; 
   v_strSql := coalesce(v_strSql,'') || ' WHERE B.numDomainID=' || SUBSTR(cast(v_numDomainID as VARCHAR(10)),1,10) || ' order by BizDocModifiedDate desc';




   RAISE NOTICE '%',v_strSql;

   EXECUTE 'insert into tt_TEMPDOCFLOW ' || v_strSql;





   if (v_tintSortOrder = 5 or v_tintSortOrder = 6) then 
      v_strSql := coalesce(v_strSql,'') || ' top  ' || SUBSTR(cast(v_intCustomPageRow  as VARCHAR(4)),1,4);
   end if;



   v_strSql := 'select
vcCompanyName AS Organization,
vcDocName as "Document Name",
fn_GetListItemName(numDocCategory) as "Document Category" ,
vcfiletype as "File Type",
fn_GetListItemName(numDocStatus) as "Document Status",
(select count(*) from DocumentWorkflow             
 where numDocID = numGenericDocId and cDocType = ''' || coalesce(v_vcDocumentSection,'')  || ''' and tintApprove = 0) as Pending,
(select count(*) from DocumentWorkflow             
 where numDocID = numGenericDocId and cDocType = ''' || coalesce(v_vcDocumentSection,'')  || ''' and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID = numGenericDocId and cDocType = ''' || coalesce(v_vcDocumentSection,'')  || ''' and tintApprove = 2) as Declined,
  fn_GetContactName(GP.numModifiedBy) as CreatedBy,
	GP.bintModifiedDate as ModifiedOn,
  numGenericDocId,
  VcFileName ,  
  cUrlType,
DV.numDivisionID,
DV.tintCRMType,
0,
''D'' as BizDocTypeId
  from GenericDocuments GP,AdditionalContactsInformation AC, DivisionMaster DV,CompanyInfo CI,
(select distinct numDocID from DocumentWorkflow where cDocType <> ''B'' and tintApprove = 0) WF    
  where GP.numCreatedBy =' || SUBSTR(cast(v_numRecID as VARCHAR(20)),1,20);  
   if v_strType <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and vcDocumentSection=' || coalesce(v_strType,'');
   end if;
   v_strSql := coalesce(v_strSql,'') || '  and GP.numDomainID =' || SUBSTR(cast(v_numDomainID as VARCHAR(20)),1,20) || ' and 
	GP.numCreatedBy = AC.numContactId and 
	AC.numDivisionId = DV.numDivisionID and 
	DV.numCompanyID = CI.numCompanyId	 and 
WF.numDocID = GP.numGenericDocId';



   if   v_KeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and  (vcDocName like ''%' || coalesce(v_KeyWord,'') || '%''                     
 or vcfiletype like  ''%' || coalesce(v_KeyWord,'') || '%''                     
 or vcDocDesc like  ''%' || coalesce(v_KeyWord,'') || '%'')';
   end if; 
   if v_numDocStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') ||  '   and  numDocStatus   =' || SUBSTR(CAST(v_numDocStatus AS VARCHAR(15)),1,15);
   end if;       
   if v_numCategory <> 0 then 
      v_strSql := coalesce(v_strSql,'') ||  ' and numDocCategory=' || SUBSTR(CAST(v_numCategory AS VARCHAR(15)),1,15);
   end if;                
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And UPPER(vcDocName) ilike upper(''' || coalesce(v_SortChar,'') || '%'')';
   end if;                     
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || '  AND (GP.numCreatedBy = ' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15) || ')';
   end if;                            
                    
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' AND GP.numCreatedBy = ' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND GP.numCreatedBy in (select numContactID from AdditionalContactsInformation A                               
where A.numManagerID=' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND GP.numCreatedBy = ' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15);
   end if; 

               
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND GP.numCreatedBy= ' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15) || ' and GP.numCreatedBy >= dateadd(day,-7,getutcdate())  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 5
   then  
      v_strSql := coalesce(v_strSql,'') || '  AND GP.numCreatedBy= ' || SUBSTR(CAST(v_numRecID AS VARCHAR(15)),1,15) || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || '  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                    
 


   RAISE NOTICE '%',(v_strSql);

   EXECUTE 'insert into tt_TEMPDOCFLOW
' || v_strSql;   


   select   coalesce(count(*),0) INTO v_TotRecs from  tt_TEMPDOCFLOW;
   open SWV_RefCur for
   select * from tt_TEMPDOCFLOW;	




   open SWV_RefCur2 for
   select SD.numGenericDocID, DW.dtCreatedDate as "Approval Request Date",
vcCompanyName as Company,
vcFirstName || ' ' || vcLastname as Name,
cast(numPhone as VARCHAR(100)) || ',' || cast(numPhoneExtension as VARCHAR(100)) as Phone,
vcEmail as Email,
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,
AC.numContactId
   from DocumentWorkflow DW JOIN GenericDocuments SD
   on DW.numDocID = SD.numGenericDocID join AdditionalContactsInformation AC
   on DW.numContactID = AC.numContactId join DivisionMaster DM
   on DM.numDivisionID = AC.numDivisionId join CompanyInfo CI
   on CI.numCompanyId = DM.numCompanyID
   where SD.numCreatedBy = v_numRecID and
   SD.numGenericDocID IN(SELECT numGenericDocID FROM tt_TEMPDOCFLOW)
   union
   select numOppBizDocsId,dtCreatedDate,
e.vcCompanyName,
vcFirstName || ' ' || vcLastname as Name,
cast(numPhone as VARCHAR(100)) || ',' || cast(numPhoneExtension as VARCHAR(100)) as Phone,
vcEmail as Email,
(case btStatus when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn,
A.numContactId
   from
   BizDocAction A
   INNER JOIN
   BizActionDetails B
   ON
   A.numBizActionId = B.numBizActionId
   INNER JOIN
   AdditionalContactsInformation C
   ON
   A.numAssign = C.numContactId
   INNER JOIN
   DivisionMaster D
   ON
   C.numDivisionId = D.numDivisionID
   INNER JOIN
   CompanyInfo e
   ON
   e.numCompanyId = D.numCompanyID
   where
   btDocType = 1 and
   numOppBizDocsId in(SELECT numGenericDocID FROM tt_TEMPDOCFLOW)
   union
   select numOppBizDocsId,
		A.dtCreatedDate,
		vcCompanyName,
		vcFirstName || ' ' || vcLastname as Name,
cast(numPhone as VARCHAR(100)) || ',' || cast(numPhoneExtension as VARCHAR(100)) as Phone,
vcEmail as Email,
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,A.numContactID
   from
   DocumentWorkflow A
   INNER JOIN
   View_BizDoc B
   ON
   A.numDocID = B.numOppBizDocsId
   INNER JOIN
   AdditionalContactsInformation D
   ON
   A.numContactID = D.numContactId
   INNER JOIN
   DivisionMaster e
   ON
   D.numDivisionId = e.numDivisionID
   INNER JOIN
   CompanyInfo f
   ON
   e.numCompanyID = f.numCompanyId
   INNER JOIN
   Listdetails h
   ON
   h.numListItemID = B.numBizDocId
   where
   cDocType = 'B' and
   B.numDomainID = v_numDomainID and 
--b.numBizDocStatus=@numCategory and 
   B.numBizDocId = v_numDocStatus and
   B.numOppBizDocsId in(SELECT numGenericDocID FROM tt_TEMPDOCFLOW) and
   B.numOppBizDocsId not in(select numOppBizDocsId from BizActionDetails where numOppBizDocsId in(SELECT numGenericDocID FROM tt_TEMPDOCFLOW))
   UNION
   select numGenericDocId,
		A.dtCreatedDate,
		vcCompanyName,
		vcFirstName || ' ' || vcLastname as Name,
cast(numPhone as VARCHAR(100)) || ',' || cast(numPhoneExtension as VARCHAR(100)) as Phone,
vcEmail as Email,
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,A.numContactID
   from
   DocumentWorkflow A
   INNER JOIN
   GenericDocuments B
   ON
   A.numDocID = B.numGenericDocID
   INNER JOIN
   AdditionalContactsInformation D
   ON
   A.numContactID = D.numContactId
   INNER JOIN
   DivisionMaster e
   ON
   D.numDivisionId = e.numDivisionID
   INNER JOIN
   CompanyInfo f
   ON
   e.numCompanyID = f.numCompanyId
   where
   cDocType = 'D' and
   B.numDomainId = v_numDomainID and
   B.numGenericDocID in(SELECT numGenericDocID FROM tt_TEMPDOCFLOW);


   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_OppAddItem]    Script Date: 07/26/2008 16:20:12 ******/



