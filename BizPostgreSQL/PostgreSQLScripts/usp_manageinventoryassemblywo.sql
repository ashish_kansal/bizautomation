-- Stored procedure definition script USP_ManageInventoryAssemblyWO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageInventoryAssemblyWO(v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numOppChildItemID NUMERIC(18,0),
	v_numOppKitChildItemID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_QtyShipped DOUBLE PRECISION,
	v_tintMode SMALLINT DEFAULT 0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
)
RETURNS VOID LANGUAGE plpgsql
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
   AS $$
   DECLARE
   v_numWOID  NUMERIC(18,0);
   v_numParentWOID  NUMERIC(18,0);
   v_numDomainID  NUMERIC(18,0);
   v_vcInstruction  VARCHAR(1000);
   v_bintCompliationDate  TIMESTAMP;
   v_numAssemblyQty  DOUBLE PRECISION; 
   v_numWOStatus  NUMERIC(18,0);
   v_numWOAssignedTo  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;
   v_Description  VARCHAR(500);
   v_numChildItemCode  INTEGER;
   v_numChildItemQty  DOUBLE PRECISION;
   v_numChildOrgQtyRequired  DOUBLE PRECISION;
   v_bitChildIsAssembly  BOOLEAN;
   v_numQuantityToBuild  DOUBLE PRECISION;
   v_numChildItemWarehouseID  NUMERIC(18,0);
   v_numChildItemWarehouseItemID  NUMERIC(18,0);
   v_numOnHand  DOUBLE PRECISION;
   v_numOnOrder  DOUBLE PRECISION;
   v_numOnAllocation  DOUBLE PRECISION;
   v_numOnBackOrder  DOUBLE PRECISION;
   v_numNewOppID  NUMERIC(18,0) DEFAULT 0;
   v_QtyToBuild  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_bitReOrderPoint  BOOLEAN DEFAULT 0;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_tintOppStautsForAutoPOBackOrder  SMALLINT DEFAULT 0;
   v_numReOrderPointOrderStatus  NUMERIC(18,0) DEFAULT 0;
   v_tintOppStatus  SMALLINT;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN


   BEGIN
      -- BEGIN TRAN
select   numWOId, numQtyItemsReq, numDomainId, coalesce(numWOStatus,0), vcInstruction, bintCompliationDate, numAssignedTo, coalesce(numParentWOID,0) INTO v_numWOID,v_numAssemblyQty,v_numDomainID,v_numWOStatus,v_vcInstruction,
      v_bintCompliationDate,v_numWOAssignedTo,v_numParentWOID FROM
      WorkOrder WHERE
      numOppId = v_numOppID
      AND numOppItemID = v_numOppItemID
      AND coalesce(numOppChildItemID,0) = v_numOppChildItemID
      AND coalesce(numOppKitChildItemID,0) = v_numOppKitChildItemID
      AND numItemCode = v_numItemCode
      AND numWareHouseItemId = v_numWarehouseItemID;

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
      IF coalesce(v_numWOID,0) > 0 AND coalesce(v_numAssemblyQty,0) > 0 then
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPITEM_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEM
         (
            RowNo INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
            numItemCode INTEGER,
            numQty DOUBLE PRECISION,
            numOrgQtyRequired DOUBLE PRECISION,
            numWarehouseItemID INTEGER,
            bitAssembly BOOLEAN
         );
         INSERT INTO
         tt_TEMPITEM(numItemCode, numQty, numOrgQtyRequired, numWarehouseItemID, bitAssembly)
         SELECT
         numChildItemID,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numWareHouseItemId,
			Item.bitAssembly
         FROM
         WorkOrderDetails
         INNER JOIN
         Item
         ON
         WorkOrderDetails.numChildItemID = Item.numItemCode
         WHERE
         numWOId = v_numWOID
         AND coalesce(numWareHouseItemId,0) > 0;
         select   COUNT(RowNo) INTO v_Count FROM tt_TEMPITEM;

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
         IF coalesce(v_Count,0) > 0 then
		

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
            IF v_numParentWOID = 0 then
			
				--GET ASSEMBY ITEM INVENTORY
               select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
               WareHouseItems WHERE
               numWareHouseItemID = v_numWarehouseItemID;
               IF v_tintMode = 1 then --WO INSERT/EDIT
				
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
                  IF v_numOnHand >= v_numAssemblyQty then
					
                     v_numOnHand := v_numOnHand -v_numAssemblyQty;
                     v_numOnOrder := v_numOnOrder+v_numAssemblyQty;
                     v_numOnAllocation := v_numOnAllocation+v_numAssemblyQty;
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand = v_numOnHand,numonOrder = v_numOnOrder,numAllocation = v_numOnAllocation,
                     dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
                  ELSEIF v_numOnHand < v_numAssemblyQty
                  then
					
                     v_numOnAllocation := v_numOnAllocation+v_numOnHand;
                     v_numOnBackOrder := v_numOnBackOrder+(v_numAssemblyQty -v_numOnHand);
                     v_numOnOrder := v_numOnOrder+v_numAssemblyQty;
                     v_numOnHand := 0;
                     UPDATE
                     WareHouseItems
                     SET
                     numAllocation = v_numOnAllocation,numBackOrder = v_numOnBackOrder,numOnHand = v_numOnHand,
                     numonOrder = v_numOnOrder,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  end if;
               ELSEIF v_tintMode = 2
               then --WO CLOSE
				
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
                  v_numAssemblyQty := coalesce(v_numAssemblyQty,0) -coalesce(v_QtyShipped,0);

					--RELASE ALLOCATION
                  v_numOnAllocation := v_numOnAllocation -v_numAssemblyQty;
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_numOnAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
               ELSEIF v_tintMode = 3
               then --WO REOPEN
				
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
                  v_numAssemblyQty := v_numAssemblyQty -v_QtyShipped;
  
					--ADD ITEM TO ALLOCATION
                  v_numOnAllocation := v_numOnAllocation+v_numAssemblyQty;
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_numOnAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
               ELSEIF v_tintMode = 4
               then --WO DELETE
				
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
                  IF v_QtyShipped > 0 then
					
                     v_numAssemblyQty := v_numAssemblyQty -v_QtyShipped;
                  end if; 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
                  IF v_numAssemblyQty < v_numOnBackOrder then
					
                     v_numOnBackOrder := v_numOnBackOrder -v_numAssemblyQty; 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
                  ELSEIF v_numAssemblyQty >= v_numOnBackOrder
                  then
					
                     v_numAssemblyQty := v_numAssemblyQty -v_numOnBackOrder;
                     v_numOnBackOrder := 0;
                        
						--REMOVE ITEM FROM ALLOCATION 
                     IF(v_numOnAllocation -v_numAssemblyQty) >= 0 then
                        v_numOnAllocation := v_numOnAllocation -v_numAssemblyQty;
                     end if;
						
						--ADD QTY TO ONHAND
                     v_numOnHand := v_numOnHand+v_numAssemblyQty;
                  end if;

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
                  IF v_numWOStatus <> 23184 then
					
						--DECREASE ONORDER BY ASSEMBLY QTY
                     v_numOnOrder := v_numOnOrder -v_numAssemblyQty;
                  ELSE
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
                     v_numOnHand := v_numOnHand -v_numAssemblyQty;
                  end if;
                  UPDATE
                  WareHouseItems
                  SET
                  numOnHand = v_numOnHand,numAllocation = v_numOnAllocation,numBackOrder = v_numOnBackOrder,
                  numonOrder = v_numOnOrder,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
               end if;
            end if;

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED, EDITED (NON COMPLETE WORK ORDER) OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
            IF v_tintMode = 1 then
			
				

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
               WHILE v_i <= v_Count LOOP
                  select   numItemCode, numWarehouseItemID, numQty, numOrgQtyRequired, bitAssembly INTO v_numChildItemCode,v_numChildItemWarehouseItemID,v_numChildItemQty,v_numChildOrgQtyRequired,
                  v_bitChildIsAssembly FROM tt_TEMPITEM WHERE RowNo = v_i;
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
                  select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
                  WareHouseItems WHERE
                  numWareHouseItemID = v_numChildItemWarehouseItemID;
                  IF coalesce(v_numParentWOID,0) = 0 then
					
                     IF v_numOppKitChildItemID > 0 then
						
                        v_Description := CONCAT('Items Allocated For SO KIT-CHILD-WO (Qty:',v_numChildItemQty,' Shipped:',
                        (v_QtyShipped*v_numChildOrgQtyRequired),')');
                     ELSEIF v_numOppChildItemID > 0
                     then
						
                        v_Description := CONCAT('Items Allocated For SO KIT-WO (Qty:',v_numChildItemQty,' Shipped:',(v_QtyShipped*v_numChildOrgQtyRequired),
                        ')');
                     ELSE
                        v_Description := CONCAT('Items Allocated For SO-WO (Qty:',v_numChildItemQty,' Shipped:',(v_QtyShipped*v_numChildOrgQtyRequired),
                        ')');
                     end if;
                  ELSE
                     IF v_numOppKitChildItemID > 0 then
						
                        v_Description := CONCAT('Items Allocated For SO KIT-CHILD-WO Work Order (Qty:',v_numChildItemQty,
                        ' Shipped:',(v_QtyShipped*v_numChildOrgQtyRequired),')');
                     ELSEIF v_numOppChildItemID > 0
                     then
						
                        v_Description := CONCAT('Items Allocated For SO KIT-WO Work Order (Qty:',v_numChildItemQty,' Shipped:',
                        (v_QtyShipped*v_numChildOrgQtyRequired),')');
                     ELSE
                        v_Description := CONCAT('Items Allocated For SO-WO Work Order (Qty:',v_numChildItemQty,' Shipped:',
                        (v_QtyShipped*v_numChildOrgQtyRequired),')');
                     end if;
                  end if;
                  v_numChildItemQty := v_numChildItemQty -(v_QtyShipped*v_numChildOrgQtyRequired);
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
                  IF coalesce(v_bitChildIsAssembly,false) = true then
					
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
                     IF v_numOnHand >= v_numChildItemQty then
						
                        v_numOnHand := v_numOnHand -v_numChildItemQty;
                        v_numOnAllocation := v_numOnAllocation+v_numChildItemQty;  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                        PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                        v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                        v_numReferenceID := v_numOppID,
                        v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                        v_Description := v_Description);    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
                     ELSEIF v_numOnHand < v_numChildItemQty
                     then
						
                        v_numOnAllocation := v_numOnAllocation+v_numOnHand;
                        v_numOnBackOrder := v_numOnBackOrder+(v_numChildItemQty -v_numOnHand);
                        v_QtyToBuild :=(v_numChildItemQty -v_numOnHand);
                        v_numQtyShipped :=(v_QtyShipped*v_numChildOrgQtyRequired);
                        v_numOnHand := 0;   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                        PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                        v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                        v_numReferenceID := v_numOppID,
                        v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                        v_Description := v_Description);
                        select   numWareHouseID INTO v_numChildItemWarehouseID FROM
                        WareHouseItems WHERE
                        numWareHouseItemID = v_numChildItemWarehouseItemID;
					
							-- CHEKC IF INTERNAL LOCATION OF WAREHOUSE HAVE QUANTITY AVAILABLE
                        IF coalesce((SELECT
                        SUM(numOnHand)
                        FROM
                        WareHouseItems
                        WHERE
                        numItemID = v_numChildItemCode
                        AND numWareHouseID = v_numChildItemWarehouseID
                        AND numWareHouseItemID <> v_numChildItemWarehouseItemID),0) > 0 then
							
                           v_QtyToBuild := v_QtyToBuild -coalesce((SELECT
                           SUM(numOnHand)
                           FROM
                           WareHouseItems
                           WHERE
                           numItemID = v_numChildItemCode
                           AND numWareHouseID = v_numChildItemWarehouseID
                           AND numWareHouseItemID <> v_numChildItemWarehouseItemID),0);
                        end if;
                        IF v_QtyToBuild > 0 then
							
                           PERFORM USP_WorkOrder_InsertRecursive(v_numOppID,v_numOppItemID,v_numOppChildItemID,v_numOppKitChildItemID,v_numChildItemCode,
                           v_QtyToBuild,v_numChildItemWarehouseItemID,v_numDomainID,
                           v_numUserCntID,v_numQtyShipped,v_numWOID,false);
                        end if;
                     end if;
                  ELSE
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
                     IF v_numOnHand >= v_numChildItemQty then
						
                        v_numOnHand := v_numOnHand -v_numChildItemQty;
                        v_numOnAllocation := v_numOnAllocation+v_numChildItemQty; 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                        PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                        v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                        v_numReferenceID := v_numOppID,
                        v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                        v_Description := v_Description);    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
                     ELSEIF v_numOnHand < v_numChildItemQty
                     then
                        v_numOnAllocation := v_numOnAllocation+v_numOnHand;
                        v_numOnBackOrder := v_numOnBackOrder+(v_numChildItemQty -v_numOnHand);
                        v_QtyToBuild :=(v_numChildItemQty -v_numOnHand);
                        v_numOnHand := 0; 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
                        PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                        v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numChildItemWarehouseItemID,
                        v_numReferenceID := v_numOppID,
                        v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                        v_Description := v_Description);


							-- AUTO CREATE BACK ORDER PO IF GLOBAL SETTINGS IS CONFIGURED FOR IT
                        select   coalesce(bitReOrderPoint,false), coalesce(numReOrderPointOrderStatus,0), coalesce(tintOppStautsForAutoPOBackOrder,0), coalesce(tintUnitsRecommendationForAutoPOBackOrder,0) INTO v_bitReOrderPoint,v_numReOrderPointOrderStatus,v_tintOppStautsForAutoPOBackOrder,
                        v_tintUnitsRecommendationForAutoPOBackOrder FROM
                        Domain WHERE
                        numDomainId = v_numDomainID;
                        IF v_bitReOrderPoint = true AND (v_tintOppStautsForAutoPOBackOrder = 2 OR v_tintOppStautsForAutoPOBackOrder = 3) then --2:Purchase Opportunity, 3: Purchase Order
							
                           v_tintOppStatus :=(CASE WHEN v_tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END);

								--CREATE PURCHASE ORDER/OPPORTUNITY FOR QUANTITY WHICH GOES ON BACKORDER
                           SELECT * INTO v_numNewOppID FROM USP_OpportunityMaster_CreatePO(v_numNewOppID,v_numDomainID,v_numUserCntID,v_numChildItemCode,v_QtyToBuild,
                           v_numChildItemWarehouseItemID,true,v_tintOppStatus::SMALLINT,0);
                           UPDATE WorkOrderDetails SET numPOID = v_numNewOppID WHERE numWOId = v_numWOID AND numChildItemID = v_numChildItemCode AND numWareHouseItemId = v_numChildItemWarehouseItemID;
                        end if;
                     end if;
                  end if;
                  v_i := v_i::bigint+1;
               END LOOP;
            end if;
         end if;
         UPDATE WorkOrder SET bitInventoryImpacted = true WHERE numWOId = v_numWOID;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;	

   RETURN;
END; $$;


