-- Stored procedure definition script USP_DeleteActItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteActItem(v_numCommId NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql        
    
--- to remover the calendar items when an task is deleted    
   AS $$
   DECLARE
   v_Activity  NUMERIC;     
   v_maxROWNUMBER  NUMERIC(9,0);
   v_minROWNUMBER  NUMERIC(9,0);
BEGIN
   v_Activity := 0;    
   select   numActivityId INTO v_Activity from Communication where numCommId = v_numCommId;    
   PERFORM Activity_Rem(v_DataKey := v_Activity);    
    
    
   Delete  FROM timeandexpense where numCaseId =(select CaseId from Communication where numCommId = v_numCommId AND coalesce(numCaseId,0) > 0) AND coalesce(numCaseId,0) > 0; -- deleting the linked casetime entries      
   delete FROM RecentItems where numRecordID = v_numCommId and chrRecordType = 'A';  
   Delete from Communication where numCommId = v_numCommId;

--Delete Communication Attendees Activity
   DROP TABLE IF EXISTS tt_TEMPATTENDEES CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPATTENDEES AS
      SELECT ActivityID,ROW_NUMBER() OVER(ORDER BY numAttendeeId) AS ROWNUMBER 
      FROM CommunicationAttendees where numCommId = v_numCommId AND ActivityID > 0;
 
   select   max(ROWNUMBER), min(ROWNUMBER) INTO v_maxROWNUMBER,v_minROWNUMBER FROM tt_TEMPATTENDEES;

   WHILE v_minROWNUMBER <= v_maxROWNUMBER LOOP
      v_Activity := 0;
      select   ActivityID INTO v_Activity FROM tt_TEMPATTENDEES WHERE ROWNUMBER = v_minROWNUMBER;
      PERFORM Activity_Rem(v_DataKey := v_Activity);
      v_minROWNUMBER := v_minROWNUMBER+1;
   END LOOP;

   Delete from CommunicationAttendees where numCommId = v_numCommId;
   RETURN;
END; $$;



