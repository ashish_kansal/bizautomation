-- Stored procedure definition script USP_ManageBizDocFilter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBizDocFilter(v_strAOI TEXT,                  
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   DELETE FROM  BizDocFilter WHERE numDomainID = v_numDomainID;
    
   insert into BizDocFilter(numBizDoc, tintBizocType,numDomainID)
   SELECT numBizDoc,tintBizocType,v_numDomainID
   FROM 
    XMLTABLE
		(
			'NewDataSet/TableBizDoc'
			PASSING 
				CAST(v_strAOI AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numBizDoc NUMERIC(9,0) PATH 'numBizDoc',
				tintBizocType SMALLINT PATH 'tintBizocType'
		) AS X;
    
   RETURN;
END; $$;  
  


