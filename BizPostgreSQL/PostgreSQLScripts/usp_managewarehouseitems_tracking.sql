DROP FUNCTION IF EXISTS USP_ManageWareHouseItems_Tracking;

CREATE OR REPLACE FUNCTION USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID NUMERIC(9,0) DEFAULT 0,
    v_numReferenceID NUMERIC(9,0) DEFAULT 0,
    v_tintRefType SMALLINT DEFAULT 0,
    v_vcDescription VARCHAR(100) DEFAULT '',
    v_tintMode SMALLINT DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT 0,
    v_PageSize INTEGER DEFAULT 0,
    INOUT v_TotRecs INTEGER DEFAULT 0  ,
    v_numModifiedBy NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT 0,
    v_dtRecordDate TIMESTAMP DEFAULT NULL,
	v_numDomainID NUMERIC(18,0) DEFAULT 0, 
	v_dtFilterFromDate TIMESTAMP DEFAULT NULL,
	v_dtFilterToDate TIMESTAMP DEFAULT NULL,
	v_vcFilterPOppName VARCHAR(100) DEFAULT NULL,
	v_vcFilterModifiedBy VARCHAR(500) DEFAULT NULL,
	v_vcFilterItemIDs VARCHAR(500) DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
	DECLARE
		v_i INTEGER DEFAULT 1;
		v_iCount INT;
		v_resultPV NUMERIC;
		v_resultNV NUMERIC;
		v_firstRec  INTEGER;
		v_lastRec  INTEGER;
BEGIN
	IF v_tintMode = 1 OR v_tintMode = 2 then --Select based on WareHouseItemID
		IF CAST(v_dtFilterFromDate AS DATE) = '0001-01-01' THEN
			v_dtFilterFromDate := null;
		END IF;
		IF CAST(v_dtFilterToDate AS DATE) = '0001-01-01' THEN
			v_dtFilterToDate := null;
		END IF;

		DROP TABLE IF EXISTS tt_TEMPTABLEManageWareHouseItemsTracking CASCADE;
		CREATE TEMPORARY TABLE tt_TEMPTABLEManageWareHouseItemsTracking ON COMMIT DROP AS
        SELECT 
			COUNT(*) OVER() AS TotalRows,
			ROW_NUMBER() OVER(ORDER BY numWareHouseItemTrackingID DESC) AS RowNumber,
			WHIT.numWareHouseItemID,
			'' vcResult,
			WHIT.numOnHand,
			WHIT.numOnOrder,
			WHIT.numReorder,
			WHIT.numAllocation,
			WHIT.numBackOrder,
			WHIT.numDomainID,
			WHIT.vcDescription,
			WHIT.tintRefType,
			coalesce(WHIT.numReferenceID,0) numReferenceID,
			(CASE 					
				WHEN WHIT.tintRefType IN (3,4,6) THEN COALESCE(COALESCE(OM.vcpOppName,''),'Order Deleted - ' || CAST(coalesce(WHIT.numReferenceID,0) AS VARCHAR(10)))
				WHEN WHIT.tintRefType = 2 THEN coalesce(WO.vcWorkOrderName,'')
				WHEN WHIT.tintRefType = 5 THEN coalesce(coalesce(RH.vcBizDocName,RH.vcRMA),'RMA Deleted - ' || CAST(coalesce(WHIT.numReferenceID,0) AS VARCHAR(10)))
				ELSE ''
			END) as vcPOppName,
			coalesce(OM.tintopptype,0) as tintOppType,
			CASE 
				WHEN CAST(WHIT.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
				THEN '<b><font color=red>Today</font></b>' 
				WHEN CAST(WHIT.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
				THEN '<b><font color=purple>YesterDay</font></b>' 
				WHEN CAST(WHIT.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
				THEN '<b><font color=orange>Tommorow</font></b>' 
				ELSE FormatedDateFromDate(WHIT.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),WHIT.numDomainID) 
			END AS dtCreatedDate,
			WHIT.dtCreatedDate AS dtCreatedDate1
			,WHIT.numModifiedBy
			,coalesce(WHIT.monAverageCost,0) AS monAverageCost
			,coalesce(WHIT.numTotalOnHand,0) AS numTotalOnHand
			,FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate
			,WHIT.numWareHouseItemTrackingID
			,IMain.vcItemName
			,CONCAT(W.vcWarehouse,(CASE WHEN WL.numWLocationID IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)) vcWarehouse
		FROM 
			WareHouseItems_Tracking WHIT		
		INNER JOIN
			WarehouseItems WI
		ON
			WHIT.numWareHouseItemID = WI.numWareHouseItemID
		INNER JOIN 
			Item IMain
		ON
			WI.numItemID = IMain.numItemCode
		INNER JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		LEFT JOIN
			WarehouseLocation WL
		ON
			WI.numWLocationID = WL.numWLocationID
		LEFT JOIN 
			Item I 
		ON 
			I.numItemCode = WHIT.numReferenceID
			AND WHIT.tintRefType = 1
		LEFT JOIN 
			OpportunityMaster OM  
		ON 
			WHIT.numReferenceID = OM.numOppId
			AND WHIT.tintRefType IN (3,4)
		LEFT JOIN 
			WorkOrder WO 
		ON 
			WHIT.numReferenceID = WO.numWOId
			AND WHIT.tintRefType = 2
		LEFT join 
			ReturnHeader RH 
		ON
			WHIT.numReferenceID = RH.numReturnHeaderID
			AND WHIT.tintRefType = 5
		WHERE 
			WHIT.numDomainID = v_numDomainID
			AND 1 = (CASE 
						WHEN COALESCE(v_vcFilterItemIDs,'') <> '' 
						THEN (CASE WHEN WI.numItemID IN (SELECT Id FROM SplitIDs(v_vcFilterItemIDs,',')) THEN 1 ELSE 0 END) 
						ELSE (CASE WHEN WHIT.numWareHouseItemID = v_numWareHouseItemID THEN 1 ELSE 0 END)
					END)
			AND 1 = (CASE WHEN v_tintMode = 2 THEN (CASE WHEN WHIT.tintRefType = 6 THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN v_dtFilterFromDate IS NOT NULL THEN (CASE WHEN CAST(WHIT.dtCreatedDate AS DATE) >= CAST(v_dtFilterFromDate AS DATE) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN v_dtFilterToDate IS NOT NULL THEN (CASE WHEN CAST(WHIT.dtCreatedDate AS DATE) <= CAST(v_dtFilterToDate AS DATE) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN COALESCE(v_vcFilterPOppName,'') <> '' THEN (CASE 					
																		WHEN WHIT.tintRefType IN (3,4) AND COALESCE(OM.vcpOppName,'') ILIKE CONCAT('%',v_vcFilterPOppName,'%') THEN 1
																		WHEN WHIT.tintRefType = 2 AND COALESCE(WO.vcWorkOrderName,'') ILIKE CONCAT('%',v_vcFilterPOppName,'%') THEN 1
																		WHEN WHIT.tintRefType = 5 AND COALESCE(RH.vcBizDocName,RH.vcRMA) ILIKE CONCAT('%',v_vcFilterPOppName,'%') THEN 1
																		ELSE 0
																	END) ELSE 1 END)
			AND 1 = (CASE WHEN COALESCE(v_vcFilterModifiedBy,'') <> '' THEN (CASE WHEN WHIT.numModifiedBy IN (SELECT Id FROM SplitIDs(v_vcFilterModifiedBy,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		ORDER BY
			numWareHouseItemTrackingID DESC
		OFFSET
			(v_CurrentPage-1) ROWS
		FETCH NEXT (v_PageSize + 1) ROWS ONLY;

		v_i := 1;
		SELECT COUNT(*) INTO v_iCount FROM tt_TEMPTABLEManageWareHouseItemsTracking;
		v_iCount := COALESCE(v_iCount,0);

		IF v_i = v_iCount THEN
			UPDATE tt_TEMPTABLEManageWareHouseItemsTracking SET vcResult = (CASE 
																				WHEN COALESCE(numOnHand,0) > 0 THEN CONCAT('<label>+',numOnHand,'</label>') 
																				WHEN COALESCE(numOnHand,0) < 0 THEN CONCAT('<label>-',numOnHand,'</label>') 
																				ELSE '' 
																			END);
		ELSE
			WHILE v_i <= (v_iCount-1) LOOP
				SELECT COALESCE(numOnHand,0) INTO v_resultNV FROM tt_TEMPTABLEManageWareHouseItemsTracking WHERE RowNumber = v_i;
				SELECT COALESCE(numOnHand,0) INTO v_resultPV FROM tt_TEMPTABLEManageWareHouseItemsTracking WHERE RowNumber = (v_i + 1);

				IF v_resultPV <> v_resultNV THEN
					UPDATE 
						tt_TEMPTABLEManageWareHouseItemsTracking 
					SET 
						vcResult = (CASE 
										WHEN v_resultNV - v_resultPV > 0  THEN CONCAT('<label>+',(v_resultNV - v_resultPV),'</label>') 
										WHEN v_resultNV - v_resultPV < 0 THEN CONCAT('<label>-',(v_resultNV - v_resultPV),'</label>') 
										ELSE '' 
									END)  
					WHERE 
						RowNumber = v_i;
				END IF;

				v_i := v_i + 1;
			END LOOP;
		END IF;

      v_firstRec := (v_CurrentPage::bigint -1) * v_PageSize::bigint;
      v_lastRec := (v_CurrentPage::bigint*v_PageSize::bigint + 1);
      
		IF (SELECT COUNT(*) FROM tt_TEMPTABLEManageWareHouseItemsTracking) > 0 THEN
			SELECT TotalRows INTO v_TotRecs FROM tt_TEMPTABLEManageWareHouseItemsTracking LIMIT 1;
		ELSE
			v_TotRecs := 0;
		END IF;

		OPEN SWV_REFCUR FOR SELECT *,fn_GetContactName(coalesce(numModifiedBy,0)) AS vcUserName FROM tt_TEMPTABLEManageWareHouseItemsTracking WHERE ROWNUMBER > v_firstRec and ROWNUMBER < v_lastRec order by ROWNUMBER;
	ELSEIF v_tintMode = 0
	then --Insert new record

      INSERT  INTO WareHouseItems_Tracking(numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate)
      SELECT  WHI.numWareHouseItemID,
                    coalesce(WHI.numOnHand,0),
                    coalesce(WHI.numonOrder,0),
                    coalesce(WHI.numReorder,0),
                    coalesce(WHI.numAllocation,0),
                    coalesce(WHI.numBackOrder,0),
                    WHI.numDomainID,
                    v_vcDescription,
                    v_tintRefType,
                    v_numReferenceID,
                    TIMEZONE('UTC',now()),v_numModifiedBy,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END),
                    (SELECT SUM(coalesce(numOnHand,0)) FROM WareHouseItems WHERE numItemID = I.numItemCode),v_dtRecordDate
      FROM    Item I JOIN WareHouseItems WHI ON I.numItemCode = WHI.numItemID
      WHERE WHI.numWareHouseItemID = v_numWareHouseItemID
      AND WHI.numDomainID = v_numDomainID;
      UPDATE Item I SET numModifiedBy = v_numModifiedBy,bintModifiedDate = TIMEZONE('UTC',now()) FROM WareHouseItems WI WHERE WI.numItemID = I.numItemCode AND(WI.numDomainID = v_numDomainID AND WI.numWareHouseItemID = v_numWareHouseItemID);
   end if;
   RETURN;
END; $$;


