-- Stored procedure definition script USP_InsertChartAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
                                                 
-- Created by Sojan
Create or replace FUNCTION USP_InsertChartAccount()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numUserCntId  NUMERIC(9,0);                                              
   v_intCount  NUMERIC(9,0);
   v_numDomainId  NUMERIC(9,0);
   v_numVendor  NUMERIC(9,0);
   v_numCustomer  NUMERIC(9,0);
   v_AccountTypeIdPrimary  NUMERIC(9,0);
   v_AccountTypeIdAsset  NUMERIC(9,0);
   v_AccountTypeIdCurrentAsset  NUMERIC(9,0);
   v_AccountTypeIdFixedAsset  NUMERIC(9,0);
   v_AccountTypeIdLiability  NUMERIC(9,0);
   v_AccountTypeIdCurrentLiability  NUMERIC(9,0);
   v_AccountTypeIdIncome  NUMERIC(9,0);
   v_AccountTypeIdExpense  NUMERIC(9,0);
   v_AccountTypeIdDirectExpense  NUMERIC(9,0);
   v_AccountTypeIdInDirectExpense  NUMERIC(9,0);
   v_AccountTypeIdInEquity  NUMERIC(9,0);
   v_AccountTypeIdShareHolderFunds  NUMERIC(9,0);
                                      
   BizDoc_Cursor CURSOR FOR

   select numDomainId from Domain where numDomainId not in(72);
BEGIN
   v_numUserCntId := 1;
 
   v_intCount := 0;                                  


   OPEN BizDoc_Cursor;

   FETCH NEXT FROM BizDoc_Cursor into v_numDomainId;
   WHILE FOUND LOOP
      update Item set numAssetChartAcntId = null,numIncomeChartAcntId = null,numCOGsChartAcntId = null where numDomainID = v_numDomainId;
      delete from AccountTypeDetail where numDomainID = v_numDomainId;
      delete from Chart_Of_Accounts  where numDomainId = v_numDomainId;
      delete from AccountingCharges where numDomainID = v_numDomainId;
      delete from COARelationships where  numDomainID = v_numDomainId;
		

		-- INSERT THE ACCOUNT TYPE DETAILS
      PERFORM USP_AccountTypeDefaultValue(v_numDomainId);
  
 --- INSERT THE COA

	
      insert into Chart_Of_Accounts(numParntAcntTypeID,
vcAccountName,
vcAccountDescription,
vcAccountCode,
numAcntTypeId,
bitProfitLoss,
numDomainId,
bitActive,
bitFixed,
bitDepreciation,
bitOpeningBalanceEquity,
monEndingOpeningBal,
monEndingBal,
numOpeningBal,
numOriginalOpeningBal)
      select numAccountTypeID,'Purchase','Purchase','0104010102',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01040101'
      UNION
      select numAccountTypeID,'Billable Time & Expenses','Billable Time & Expenses','0104010101',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01040101'
      UNION
      select numAccountTypeID,'Sales Tax Payable','Sales Tax Payable','0102010101',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01020101'
      UNION
      select numAccountTypeID,'Sales','Sales','01030102',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '010301'
      UNION
      select numAccountTypeID,'Discount Given','Discount Given','01030101',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '010301'
      UNION
      select numAccountTypeID,'Shipping Income','Shipping Income','01030201',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '010302'
      UNION
      select numAccountTypeID,'Late Charges','Late Charges','01030202',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '010302'
      UNION
      select numAccountTypeID,'UnDepositedFunds','UnDepositedFunds','0101010101',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01010101'
      UNION
      select numAccountTypeID,'COGS','COGS','0104010401',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01040104'
      UNION
      select numAccountTypeID,'Accounts Receivable','Accounts Receivable','0101010501',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01010105'
      UNION
      select numAccountTypeID,'Accounts Payable','Accounts Payable','0102010201',numAccountTypeID,false,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01020102'
      UNION
      select numAccountTypeID,'Profit & Loss','Profit & Loss','0105010101',numAccountTypeID,true,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01050101'
      UNION
      select numAccountTypeID,'Stock On Hand','Stock On Hand','0101010401',numAccountTypeID,true,v_numDomainId,true,false,false,false,0,0,0,0  from AccountTypeDetail where numDomainID = v_numDomainId and vcAccountCode = '01010104';


--- MAPPING DEFAULT CHARGE WITH COA

      UPDATE Item set numAssetChartAcntId =(select numAccountId from Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0101010401') where numDomainID = v_numDomainId;
      UPDATE Item set numIncomeChartAcntId =(select numAccountId from Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '01030102') where numDomainID = v_numDomainId;
      update Item set numCOGsChartAcntId =(select numAccountId from Chart_Of_Accounts where  numDomainId = v_numDomainId and vcAccountCode = '0104010102') where numDomainID = v_numDomainId;
      INSERT INTO AccountingCharges(numChargeTypeId,
numAccountID,
numDomainID)
      SELECT 10,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0104010101'
      UNION
      SELECT 3,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0102010101'
      UNION
      SELECT 5,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '01030101'
      UNION
      SELECT 6,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '01030201'
      UNION
      SELECT 7,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '01030202'
      UNION
      SELECT 9,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0101010101'
      UNION
      SELECT 11,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0104010401'
      UNION
      SELECT 4,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0101010501'
      UNION
      SELECT 8,numAccountId,numDomainId  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0102010201';


--- MAPPING CUSTOMER AND VENDOR RELATION WITH ITS DEFAULT COA.


      v_numCustomer := 0;
      v_numVendor := 0;
      select   count(numListItemID) INTO v_numCustomer FROM Listdetails WHERE numListID = 5 AND (constFlag = true or numDomainid = v_numDomainId) and vcData = 'Customer';
      select   count(numListItemID) INTO v_numVendor FROM Listdetails WHERE numListID = 5 AND (constFlag = true or numDomainid = v_numDomainId) and vcData = 'Vendor';
      if v_numCustomer > 0 then
	
         INSERT INTO COARelationships
		 ( numRelationshipID
      ,numARParentAcntTypeID
      ,numAPParentAcntTypeID
      ,numARAccountId
      ,numAPAccountId
      ,numDomainID
      ,dtCreateDate
      ,dtModifiedDate
      ,numCreatedBy
      ,numModifiedBy)
         SELECT(SELECT numListItemID  FROM Listdetails WHERE numListID = 5 AND (constFlag = true or numDomainid = v_numDomainId) and vcData = 'Customer')
,numAcntTypeId,Null,numAccountId,Null,v_numDomainId,TIMEZONE('UTC',now()),Null::TIMESTAMP,v_numUserCntId,Null  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0101010501';
      end if;
      if v_numVendor > 0 then
	
         INSERT INTO COARelationships
		 ( numRelationshipID
      ,numARParentAcntTypeID
      ,numAPParentAcntTypeID
      ,numARAccountId
      ,numAPAccountId
      ,numDomainID
      ,dtCreateDate
      ,dtModifiedDate
      ,numCreatedBy
      ,numModifiedBy)
         SELECT(SELECT numListItemID  FROM Listdetails WHERE numListID = 5 AND (constFlag = true or numDomainid = v_numDomainId) and vcData = 'Vendor')
,Null,numAcntTypeId,Null,numAccountId,v_numDomainId,TIMEZONE('UTC',now()),Null::TIMESTAMP,v_numUserCntId,Null  FROM Chart_Of_Accounts where numDomainId = v_numDomainId and vcAccountCode = '0102010201';
      end if;
      FETCH NEXT FROM BizDoc_Cursor into v_numDomainId;
   END LOOP;

   CLOSE BizDoc_Cursor;
   RETURN;
END; $$;

