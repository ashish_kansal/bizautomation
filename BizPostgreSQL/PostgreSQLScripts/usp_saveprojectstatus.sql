-- Stored procedure definition script usp_SaveProjectStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveProjectStatus(v_numDomainID NUMERIC(9,0),                             
v_numProId NUMERIC(9,0) DEFAULT null  ,    
v_ProjectStatus NUMERIC(9,0) DEFAULT NULL,
v_numContactId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE ProjectsMaster SET numProjectStatus = v_ProjectStatus,numCompletedBy = v_numContactId WHERE numdomainId = v_numDomainID AND numProId = v_numProId;
   RETURN;
END; $$;


