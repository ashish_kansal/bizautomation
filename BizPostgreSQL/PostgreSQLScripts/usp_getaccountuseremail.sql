CREATE OR REPLACE FUNCTION usp_getaccountuseremail(
	v_bytemode smallint,
	v_numuserid numeric DEFAULT 0,
	v_numcontactid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 14:55:55 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   if v_byteMode = 0 then --- Based on UserID

      open SWV_RefCur for
      select coalesce(vcEmail,'') from UserMaster
      join AdditionalContactsInformation
      on numContactId = numUserDetailId
      where numUserID = v_numUserID;
   end if;
   if v_byteMode = 1 then  --- Based on COntactID

      open SWV_RefCur for
      select coalesce(vcEmail,'') from UserMaster
      join AdditionalContactsInformation
      on numContactId = numUserDetailId
      where numContactId = v_numContactID;
   end if;


/****** Object:  StoredProcedure [dbo].[usp_GetACInfoForMail]    Script Date: 07/26/2008 16:16:07 ******/
   RETURN;
END;
$BODY$;

