-- Stored procedure definition script USP_DeleteReview for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteReview(v_numReviewId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM ReviewDetail WHERE numReviewId = v_numReviewId) then
	
      DELETE FROM ReviewDetail WHERE numReviewId = v_numReviewId;
   end if;
	
   DELETE FROM Review WHERE numReviewId = v_numReviewId;
   RETURN;
END; $$;

--SELECT * FROM Review
--SELECT * FROM ReviewDetail
--UPDATE ReviewDetail SET numContactID = 18 WHERE numReviewDetailId = 4
--INSERT INTO ReviewDetail
--VALUES(12,0,1,20)
--
--INSERT INTO Review
--values(2,19017,'Sample Delete' ,'sample Comment' ,1 ,'255.125.12.1',20,91,1,NULL,0,1)
--
--



