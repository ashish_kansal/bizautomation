-- Function definition script GetItemCostOfGoodsSoldAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemCostOfGoodsSoldAccount(v_numItemCode NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCOGSChartAcntId  NUMERIC(9,0);
BEGIN
   select   coalesce(numCOGsChartAcntId,'0') INTO v_numCOGSChartAcntId from Item where numItemCode = v_numItemCode;         
      
      
   return coalesce(v_numCOGSChartAcntId,cast('0' as NUMERIC));
END; $$;

