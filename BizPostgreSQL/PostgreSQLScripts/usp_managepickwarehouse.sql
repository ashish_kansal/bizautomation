-- Stored procedure definition script USP_ManagePickWareHouse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePickWareHouse(v_numDomainId NUMERIC(9,0),    
v_strWarehouse VARCHAR(4000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;  
   v_strPosition  VARCHAR(1000);
BEGIN
   delete from WareHouseForSalesFulfillment where numDomainID = v_numDomainId;   
   
  
   while coalesce(POSITION(substring(v_strWarehouse from E'\\,') IN v_strWarehouse),
   0) <> 0 LOOP -- Begin for While Loop  
      v_separator_position := coalesce(POSITION(substring(v_strWarehouse from E'\\,') IN v_strWarehouse),
      0);
      v_strPosition := SUBSTR(v_strWarehouse,1,v_separator_position -1);
      v_strWarehouse := OVERLAY(v_strWarehouse placing '' from 1 for v_separator_position);
      insert into WareHouseForSalesFulfillment(numWareHouseID,numDomainID)
     values(CAST(v_strPosition AS NUMERIC),v_numDomainId);
   END LOOP;

/****** Object:  StoredProcedure [dbo].[USP_ManagePortalBizDocs]    Script Date: 07/26/2008 16:19:56 ******/
   RETURN;
END; $$;


