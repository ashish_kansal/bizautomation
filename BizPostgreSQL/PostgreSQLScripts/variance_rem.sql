-- Stored procedure definition script Variance_Rem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Variance_Rem(v_DeletedStatusCode	INTEGER,	-- status code for updating the Variance
	v_ActivityID		INTEGER		-- primary key number of the Variance row
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Activity
   SET
   Status = v_DeletedStatusCode
   WHERE
   Activity.ActivityID = v_ActivityID;
   RETURN;
END; $$;


