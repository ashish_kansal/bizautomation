-- Stored procedure definition script USP_GetVendorShipmentMethod for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetVendorShipmentMethod(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numVendorid NUMERIC(9,0) DEFAULT 0,
v_numAddressID NUMERIC(9,0) DEFAULT 0,
v_numWarehouseID NUMERIC(18,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
	
      IF EXISTS(SELECT numShipmentMethod FROM VendorShipmentMethod VM WHERE VM.numDomainId = v_numDomainID AND VM.numVendorID = v_numVendorid AND coalesce(numWarehouseID,0) = v_numWarehouseID) then
		
         open SWV_RefCur for
         SELECT
         LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,coalesce(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,coalesce(VM.bitPreferredMethod,false) AS bitPreferredMethod
         FROM
         Listdetails LD
         LEFT JOIN
         VendorShipmentMethod VM
         ON
         VM.numDomainId = v_numDomainID
         AND VM.numVendorID = v_numVendorid
         AND VM.numAddressID = v_numAddressID
         AND VM.numListItemID = LD.numListItemID
         AND VM.numWarehouseID = v_numWarehouseID
         WHERE
         LD.numListID = 338
         AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true);
      ELSE
         open SWV_RefCur for
         SELECT
         LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,coalesce(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,coalesce(VM.bitPreferredMethod,false) AS bitPreferredMethod
         FROM
         Listdetails LD
         LEFT JOIN
         VendorShipmentMethod VM
         ON
         VM.numDomainId = v_numDomainID
         AND VM.numVendorID = v_numVendorid
         AND VM.numAddressID = v_numAddressID
         AND VM.numListItemID = LD.numListItemID
         AND coalesce(VM.numWarehouseID,0) = 0
         WHERE
         LD.numListID = 338
         AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true);
      end if;
   ELSEIF v_tintMode = 2
   then
	
      IF coalesce(v_numAddressID,0) = -1 then
		
         v_numAddressID := coalesce((SELECT 
         AD.numAddressID
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numRecordID = v_numVendorid
         AND AD.tintAddressOf = 2
         AND AD.tintAddressType = 2
         ORDER BY coalesce(AD.bitIsPrimary,false) DESC LIMIT 1),0);
      end if;
      open SWV_RefCur for
      SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,coalesce(VM.numListValue,0) AS numListValue,VM.bitPrimary,
		cast(LD.numListItemID AS VARCHAR(10)) || '~' || cast(coalesce(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
      from Listdetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID = LD.numListItemID
      AND VM.numVendorID = v_numVendorid AND VM.numAddressID = v_numAddressID AND VM.numDomainId = v_numDomainID
      WHERE LD.numListID = 338 AND (LD.numDomainid = v_numDomainID or LD.constFlag = true) ORDER BY coalesce(bitPrimary,false) DESC,coalesce(bitPreferredMethod,false) DESC;
   end if;
   RETURN;
END; $$;


