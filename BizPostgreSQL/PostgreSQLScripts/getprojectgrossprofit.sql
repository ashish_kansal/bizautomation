-- Function definition script GetProjectGrossProfit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectGrossProfit(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalIncome  DECIMAL(20,5) DEFAULT 0;
   v_TotalExpense  DECIMAL(20,5) DEFAULT 0;
BEGIN
   v_TotalIncome := v_TotalIncome+coalesce((SELECT
   SUM(coalesce(OPP.monPrice,0)*(CASE numType
   WHEN 1 THEN coalesce(OPP.numUnitHour::NUMERIC,0)
   ELSE coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
   END) -(CASE WHEN I.charItemType = 'P' THEN CASE WHEN coalesce(OPP.monAvgCost,0) = 0 THEN coalesce(OPP.monVendorCost,0) ELSE coalesce(OPP.monAvgCost,0) END ELSE 0 END))
   FROM
   timeandexpense TE
   INNER JOIN
   OpportunityMaster OM
   ON
   TE.numOppId = OM.numOppId
   INNER JOIN
   OpportunityItems OPP
   ON
   OPP.numOppId = TE.numOppId
   AND OPP.numoppitemtCode = TE.numOppItemID
   AND OPP.numProjectID = TE.numProId
   INNER JOIN
   Item I
   ON
   OPP.numItemCode = I.numItemCode
   WHERE
   numProId = v_numProId
   AND TE.numDomainID = v_numDomainID
   AND numType = 1 --Billable 
   AND OM.tintopptype = 1),0);

   v_TotalIncome := v_TotalIncome+coalesce((SELECT
   SUM(coalesce(OPP.monPrice,0)*coalesce(OPP.numUnitHour::NUMERIC,0) -(CASE WHEN I.charItemType = 'P' THEN CASE WHEN coalesce(OPP.monAvgCost,0) = 0 THEN coalesce(OPP.monVendorCost,0) ELSE coalesce(OPP.monAvgCost,0) END ELSE 0 END))
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OPP
   ON
   OPP.numOppId = OM.numOppId
   INNER JOIN
   Item I
   ON
   OPP.numItemCode = I.numItemCode
   WHERE
   OM.numDomainId = v_numDomainID
   AND OPP.numProjectID = v_numProId
   AND OPP.numProjectStageID = 0
   AND OM.tintopptype = 1),
   0);

   v_TotalIncome := v_TotalIncome+coalesce((SELECT
   SUM(DED.monAmountPaid)
   FROM
   DepositMaster DEM
   JOIN
   DepositeDetails DED
   ON
   DEM.numDepositId = DED.numDepositID
   WHERE
   DEM.tintDepositePage = 1
   AND DEM.numDomainId = v_numDomainID
   AND DED.numProjectID = v_numProId),
   0);
				
   v_TotalIncome := v_TotalIncome+coalesce((SELECT
   SUM(GD.numCreditAmt)
   FROM
   General_Journal_Header GH
   JOIN
   General_Journal_Details GD
   ON
   GH.numJOurnal_Id = GD.numJournalId
   JOIN
   Chart_Of_Accounts COA
   ON
   COA.numAccountId = GD.numChartAcntId
   WHERE
   coalesce(GH.numCheckHeaderID,0) = 0
   AND coalesce(GH.numBillPaymentID,0) = 0
   AND coalesce(GH.numBillID,0) = 0
   AND coalesce(GH.numDepositId,0) = 0
   AND coalesce(GH.numOppId,0) = 0
   AND coalesce(GH.numOppBizDocsId,0) = 0
   AND coalesce(GH.numReturnID,0) = 0
   AND GH.numDomainId = v_numDomainID
   AND GD.numProjectID = v_numProId
   AND coalesce(GD.numCreditAmt,0) > 0
   AND (COA.vcAccountCode ilike '0103%' OR  COA.vcAccountCode ilike '0104%' OR  COA.vcAccountCode ilike '0106%')),0);
		
   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM((OPP.monPrice*OPP.numUnitHour))
   FROM
   OpportunityMaster OM
   JOIN
   OpportunityItems OPP
   ON
   OM.numOppId = OPP.numOppId
   LEFT OUTER JOIN
   Item I
   ON
   OPP.numItemCode = I.numItemCode
   LEFT OUTER JOIN
   Chart_Of_Accounts COA
   ON
   I.numCOGsChartAcntId = COA.numAccountId
   WHERE
   OM.numDomainId = v_numDomainID
   AND OPP.numProjectID = v_numProId
   AND OM.tintopptype = 2),0);

   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM(BD.monAmount)
   FROM
   ProjectsOpportunities PO
   JOIN
   BillHeader BH
   ON
   BH.numBillID = PO.numBillId
   JOIN
   BillDetails BD
   ON
   BH.numBillID = BD.numBillID
   AND PO.numProId = BD.numProjectID
   WHERE
   PO.numDomainId = v_numDomainID
   AND PO.numProId = v_numProId
   AND coalesce(PO.numBillId,0) > 0),
   0);

   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM(BD.monAmount)
   FROM
   BillHeader BH
   JOIN
   BillDetails BD
   ON
   BH.numBillID = BD.numBillID
   WHERE
   BH.numDomainId = v_numDomainID
   AND BD.numProjectID = v_numProId),0);

   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM(coalesce(CAST((EXTRACT(DAY FROM dttodate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM dttodate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM dttodate -TE.dtFromDate)) AS DOUBLE PRECISION)/Cast(60 AS DOUBLE PRECISION),0)*coalesce(UM.monHourlyRate,0))
   FROM
   timeandexpense TE
   JOIN
   UserMaster UM
   ON
   UM.numUserDetailId = TE.numUserCntID
   WHERE
   numProId = v_numProId
   AND TE.numDomainID = v_numDomainID
   AND numType in(1,2) --Non Billable & Billable
   AND TE.numUserCntID > 0),0);

   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM(CD.monAmount)
   FROM
   CheckHeader CH
   JOIN
   CheckDetails CD
   ON
   CH.numCheckHeaderID = CD.numCheckHeaderID
   WHERE
   CH.tintReferenceType = 1
   AND CH.numDomainID = v_numDomainID
   AND CD.numProjectID = v_numProId),
   0);

   v_TotalExpense := v_TotalExpense+coalesce((SELECT
   SUM(GD.numDebitAmt)
   FROM
   General_Journal_Header GH
   JOIN
   General_Journal_Details GD
   ON
   GH.numJOurnal_Id = GD.numJournalId
   JOIN
   Chart_Of_Accounts COA
   ON
   COA.numAccountId = GD.numChartAcntId
   WHERE
   coalesce(GH.numCheckHeaderID,0) = 0
   AND coalesce(GH.numBillPaymentID,0) = 0
   AND coalesce(GH.numBillID,0) = 0
   AND coalesce(GH.numDepositId,0) = 0
   AND coalesce(GH.numOppId,0) = 0
   AND coalesce(GH.numOppBizDocsId,0) = 0
   AND coalesce(GH.numReturnID,0) = 0
   AND GH.numDomainId = v_numDomainID
   AND GD.numProjectID = v_numProId
   AND coalesce(GD.numDebitAmt,0) > 0
   AND (COA.vcAccountCode ilike '0103%' OR  COA.vcAccountCode ilike '0104%' OR  COA.vcAccountCode ilike '0106%')),0);

   RETURN coalesce(v_TotalIncome,0) -coalesce(v_TotalExpense,0); -- Set Accuracy of Two precision
END; $$;

