-- Stored procedure definition script USP_ManageItemImages for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemImages(INOUT v_numItemImageId  NUMERIC(18,0) DEFAULT 0 ,
	v_numItemCode	 NUMERIC(18,0) DEFAULT NULL,
	v_numDomainId     NUMERIC(18,0) DEFAULT NULL,
	v_vcPathForImage	 VARCHAR(300) DEFAULT NULL,
	v_vcPathForTImage VARCHAR(300) DEFAULT NULL,
	v_bitDefault	     BOOLEAN DEFAULT NULL,
	v_intDisplayOrder INTEGER DEFAULT NULL,
	v_bitIsImage	     BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_bitDefault,false) = true then
	
      UPDATE ItemImages SET bitDefault = false WHERE numItemCode = v_numItemCode AND numDomainId = v_numDomainId AND bitIsImage = true;
   end if;
  		
   IF coalesce(v_numItemImageId,0) = 0 then
	
      IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = v_numItemCode AND bitIsImage = true AND numDomainId = v_numDomainId) then
		
         v_bitDefault := true;
      ELSE
         v_bitDefault := false;
      end if;
      IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = v_numItemCode  AND numDomainId = v_numDomainId) then
		
         v_intDisplayOrder := 1;
      ELSE
         select   MAX(coalesce(intDisplayOrder,0))+1 INTO v_intDisplayOrder FROM
         ItemImages WHERE
         numItemCode = v_numItemCode
         AND numDomainId = v_numDomainId;
      end if;
	       
		-- PRINT(@intDisplayOrder)
		
      IF((SELECT numItemCode FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainId) > 0) then
		
         INSERT INTO ItemImages(numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId ,bitIsImage)
			VALUES(v_numItemCode,v_vcPathForImage,coalesce(v_vcPathForTImage, ''),v_bitDefault,v_intDisplayOrder,v_numDomainId ,v_bitIsImage);
			
         v_numItemImageId := CURRVAL('ItemImages_seq');
      end if;
   ELSE
      IF coalesce(v_vcPathForImage,'') = '' then
		
         select   coalesce(vcPathForImage,'') INTO v_vcPathForImage FROM ItemImages WHERE numItemImageId = v_numItemImageId;
      end if;
      IF coalesce(v_vcPathForTImage,'') = '' then
		
         select   coalesce(vcPathForTImage,'') INTO v_vcPathForTImage FROM ItemImages WHERE numItemImageId = v_numItemImageId;
      end if;
      UPDATE ItemImages SET bitDefault = v_bitDefault,intDisplayOrder = v_intDisplayOrder,vcPathForImage = coalesce(v_vcPathForImage,''),
      vcPathForTImage = coalesce(v_vcPathForTImage,'') WHERE numItemImageId = v_numItemImageId;
   end if;

   update Item set  bintModifiedDate = TIMEZONE('UTC',now()) where numItemCode = v_numItemCode  AND numDomainID = v_numDomainId;
   RETURN;
END; $$;

