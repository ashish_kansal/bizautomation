-- Stored procedure definition script USP_GET_PACKAGES for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_PACKAGES(v_DomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		0 AS "numCustomPackageID",
		0 AS "numPackageTypeID",
		'None' AS "vcPackageName",
		0.00 AS "fltWidth",
		0.00 AS "fltHeight",
		0.00 AS "fltLength",
		0.00 AS "fltTotalWeight",
		0 AS "numShippingCompanyID",
		CAST('' AS CHAR(1)) AS "vcShippingCompany",
		cast(CONCAT(0,',',0,',',0,',',0,',',0,',',0) as VARCHAR(255)) AS "ValueDimension"
	UNION
	SELECT
		numItemCode AS "numCustomPackageID",
		0 AS "numPackageTypeID",
		vcItemName AS "vcPackageName",
		fltWidth AS "fltWidth",
		fltHeight AS "fltHeight",
		fltLength AS "fltLength",
		fltWeight AS "fltTotalWeight",
		0 AS "numShippingCompanyID",
		'Custom Box' AS "vcShippingCompany",
		CONCAT(0,',',fltWidth,',',fltHeight,',',fltLength,',',fltWeight,',',numItemCode)  AS "ValueDimension"
	FROM
		Item 
	where
		numDomainID = v_DomainId
		AND coalesce(bitContainer,false) = true;
END; $$;












