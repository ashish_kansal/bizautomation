-- Stored procedure definition script USP_GetTaxPercentage for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTaxPercentage(v_DivisionID NUMERIC(9,0) DEFAULT 0,
v_numBillCountry NUMERIC(9,0) DEFAULT 0,
v_numBillState  NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_numTaxItemID NUMERIC(9,0) DEFAULT NULL,
v_tintBaseTaxCalcOn SMALLINT DEFAULT 2, -- Base tax calculation on Shipping Address(2) or Billing Address(1)
v_tintBaseTaxOnArea SMALLINT DEFAULT 0,
v_vcCity VARCHAR(100) DEFAULT NULL,
v_vcZipPostal VARCHAR(20) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TaxPercentage  DOUBLE PRECISION; 
   v_tintTaxType  SMALLINT;

   v_bitTaxApplicable  BOOLEAN;
BEGIN
   v_TaxPercentage := 0; 
 
   if v_DivisionID > 0 then --Existing Customer

      IF v_tintBaseTaxCalcOn = 1 then --Billing Address
         select   coalesce(numState,0), coalesce(numCountry,0), coalesce(vcCity,''), coalesce(vcPostalCode,'') INTO v_numBillState,v_numBillCountry,v_vcCity,v_vcZipPostal from DivisionMaster DM LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
         AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 where numDivisionID = v_DivisionID;
      ELSE --Shipping Address
         select   coalesce(numState,0), coalesce(numCountry,0), coalesce(vcCity,''), coalesce(vcPostalCode,'') INTO v_numBillState,v_numBillCountry,v_vcCity,v_vcZipPostal from DivisionMaster DM  LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
         AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true where numDivisionID = v_DivisionID;
      end if;
      IF v_numTaxItemID = 0 AND NOT EXISTS(SELECT numTaxItemID FROM DivisionTaxTypes WHERE numDivisionID =  v_DivisionID and numTaxItemID = v_numTaxItemID) then
	
         select(CASE WHEN coalesce(bitNoTax,false) = true THEN 0 ELSE 1 END) INTO v_bitTaxApplicable FROM DivisionMaster WHERE numDivisionID = v_DivisionID;
      ELSE
         select   bitApplicable INTO v_bitTaxApplicable FROM DivisionTaxTypes WHERE numDivisionID =  v_DivisionID and numTaxItemID = v_numTaxItemID;
      end if;
   else
      v_bitTaxApplicable := true;
   end if;


   select   tintBaseTaxOnArea INTO v_tintBaseTaxOnArea FROM Domain WHERE numDomainId = v_numDomainID;

   IF EXISTS(SELECT tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID = v_numDomainID AND numCountry = v_numBillCountry) then

	--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
      select   tintBaseTaxOnArea INTO v_tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID = v_numDomainID and numCountry = v_numBillCountry;
   end if;

   if  v_bitTaxApplicable = true then --If Tax Applicable
	
      if v_numBillCountry > 0 and v_numBillState > 0 then
		
         if v_numBillState > 0 then
			
            if exists(select * from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState and
            1 =(Case
            when v_tintBaseTaxOnArea = 0 then 1 --State
            when v_tintBaseTaxOnArea = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
            when v_tintBaseTaxOnArea = 2 then Case When vcZipPostal = v_vcZipPostal then 1 else 0 end --Zip/Postal
            else 0 end) and numDomainId = v_numDomainID and numTaxItemID = v_numTaxItemID) then
						
               select   decTaxPercentage, tintTaxType INTO v_TaxPercentage,v_tintTaxType from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState and
               1 =(Case
               when v_tintBaseTaxOnArea = 0 then 1 --State
               when v_tintBaseTaxOnArea = 1 then Case When vcCity ilike '%' || coalesce(v_vcCity,'') || '%' then 1 else 0 end --City
               when v_tintBaseTaxOnArea = 2 then Case When vcZipPostal = v_vcZipPostal then 1 else 0 end --Zip/Postal
               else 0 end) and numDomainId = v_numDomainID and numTaxItemID = v_numTaxItemID    LIMIT 1;
            ELSEIF exists(select * from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState
            and numDomainId = v_numDomainID and numTaxItemID = v_numTaxItemID and coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = '')
            then
						
               select   decTaxPercentage, tintTaxType INTO v_TaxPercentage,v_tintTaxType from TaxDetails where numCountryID = v_numBillCountry and numStateID = v_numBillState
               and numDomainId = v_numDomainID and numTaxItemID = v_numTaxItemID  and coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = ''    LIMIT 1;
            else
               select   decTaxPercentage, tintTaxType INTO v_TaxPercentage,v_tintTaxType from TaxDetails where numCountryID = v_numBillCountry and numStateID = 0 and coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = '' and numDomainId = v_numDomainID   and numTaxItemID = v_numTaxItemID    LIMIT 1;
            end if;
         end if;
      ELSEIF v_numBillCountry > 0
      then
         select   decTaxPercentage, tintTaxType INTO v_TaxPercentage,v_tintTaxType from TaxDetails where numCountryID = v_numBillCountry and numStateID = 0 and coalesce(vcCity,'') = '' and coalesce(vcZipPostal,'') = '' and numDomainId = v_numDomainID  and numTaxItemID = v_numTaxItemID    LIMIT 1;
      end if;
   end if;

   open SWV_RefCur for select cast(CONCAT(coalesce(v_TaxPercentage,0),'#',coalesce(v_tintTaxType,1)) as VARCHAR(255));
END; $$;












