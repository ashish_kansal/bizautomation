-- Stored procedure definition script USP_OpportunityBizDocs_GetARAccountID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetARAccountID(v_numOppBizDocsId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numARAccountID  NUMERIC(18,0);
BEGIN
   select   COA.numAccountId INTO v_numARAccountID FROM
   OpportunityBizDocs OBD
   INNER JOIN
   Chart_Of_Accounts COA
   ON
   OBD.numARAccountID = COA.numAccountId WHERE
   numOppBizDocsId = v_numOppBizDocsId;

   open SWV_RefCur for SELECT coalesce(v_numARAccountID,0) AS numARAccountID;
END; $$;












