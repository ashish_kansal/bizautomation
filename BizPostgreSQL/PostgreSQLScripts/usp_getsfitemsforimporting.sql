CREATE OR REPLACE FUNCTION USP_GEtSFItemsForImporting
(
	v_numDomainID NUMERIC(9,0),
	v_numBizDocId BIGINT,
	v_vcBizDocsIds TEXT,
	v_tintMode SMALLINT, 
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
   DECLARE
	v_strSql TEXT;
	v_strFrom TEXT;
	v_numFldID INTEGER;    
	v_intRowNum INTEGER;    	
	v_vcFldname TEXT;	
	v_strSQLUpdate TEXT;
	v_i  INTEGER DEFAULT 1;
	v_Count  INTEGER; 
	v_numOppId  BIGINT;
	v_numOppItemCode  BIGINT;
	v_tintTaxOperator  INTEGER;
	v_vcTaxName TEXT;
	v_numTaxItemID  NUMERIC(9,0);
	SWV_ExecDyn TEXT;
	SWV_RowCount INTEGER;
BEGIN
   v_vcBizDocsIds := OVERLAY((SELECT
   CONCAT(',',numOppBizDocsId)
   FROM
   OpportunityBizDocs
   WHERE
   numoppid IN(SELECT Id FROM SplitIDs(v_vcBizDocsIds,','))
   AND numBizDocId = coalesce(v_numBizDocId,0)) placing '' from 1 for 1);

   IF v_tintMode = 1 then --PrintPickList

      open SWV_RefCur for
      SELECT
      coalesce(vcItemName,'') AS vcItemName,
		coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = 187 AND bitDefault = true AND bitIsImage = true LIMIT 1),'') AS vcImage,
		coalesce(vcModelID,'') AS vcModelID,
		coalesce(txtItemDesc,'') AS txtItemDesc,
		coalesce(vcWareHouse,'') AS vcWareHouse,
		coalesce(vcLocation,'') AS vcLocation,
		(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
      FROM
      OpportunityBizDocItems
      INNER JOIN
      WareHouseItems
      ON
      OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
      INNER JOIN
      Warehouses
      ON
      WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocID IN(SELECT * FROM SplitIDs(v_vcBizDocsIds,','))
      GROUP BY
      vcItemName,vcModelID,txtItemDesc,vcWareHouse,vcLocation,(CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
      (CASE WHEN coalesce(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
      ORDER BY
      vcWareHouse ASC,vcLocation ASC,vcItemName ASC;
   ELSEIF v_tintMode = 2
   then --PrintPackingSlip
      DROP TABLE IF EXISTS tt_TEMP_PACKING_LIST CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP_PACKING_LIST AS
         SELECT
         ROW_NUMBER() OVER(ORDER BY OM.numOppId ASC,OI.numSortOrder ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainId,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcpOppName,
				cast((OBDI.monTotAmtBefDiscount -OBDI.monTotAmount) as VARCHAR(20)) || Case When coalesce(OM.fltDiscount,0) > 0 then  '(' || SUBSTR(cast(OM.fltDiscount as VARCHAR(10)),1,10) || '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE
         WHEN charItemType = 'P'
         THEN 'Inventory Item'
         WHEN charItemType = 'S'
         THEN 'Service'
         WHEN charItemType = 'A'
         THEN 'Accessory'
         WHEN charItemType = 'N'
         THEN 'Non-Inventory Item'
         END AS charItemType,charItemType AS Type,
				OBDI.vcAttributes,
				OI.numoppitemtCode ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.numQtyShipped,
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS Price,
				OBDI.monTotAmount AS Amount,
				CASE
         WHEN coalesce(OBD.numShipVia,0) = 0 THEN(CASE WHEN OM.intUsedShippingCompany IS NULL THEN '-' WHEN OM.intUsedShippingCompany = -1 THEN 'Will-call' ELSE fn_GetListItemName(OM.intUsedShippingCompany::NUMERIC) END)
         WHEN OBD.numShipVia = -1 THEN 'Will-call'
         ELSE fn_GetListItemName(OBD.numShipVia)
         END AS ShipVia,
				coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = OM.numShippingService),'') AS vcShippingService,
				(CASE
         WHEN coalesce(OBD.numSourceBizDocId,0) > 0
         THEN coalesce((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = OBD.numSourceBizDocId),'')
         ELSE ''
         END) AS vcPackingSlip,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS vcState,
				fn_GetListItemName(W.numWCountry) AS vcWCountry,
				vcWHSKU,
				vcBarCode,
				SUBSTR((SELECT ',' || vcSerialNo || CASE
         WHEN coalesce(I.bitLotNo,false) = true
         THEN ' (' || CAST(SERIALIZED_ITEM.numQty AS VARCHAR(15)) || ')'
         ELSE ''
         END
         FROM OppWarehouseSerializedItem SERIALIZED_ITEM
         JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWarehouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID
         WHERE SERIALIZED_ITEM.numOppID = OI.numOppId
         AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode
         and SERIALIZED_ITEM.numOppBizDocsId = OBD.numOppBizDocsId
         ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
				coalesce(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				coalesce(OM.bitBillingTerms,false) AS tintBillingTerms,
				coalesce(intBillingDays,0) AS numBillingDays,
				CASE WHEN SWF_IsNumeric(CAST((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)) AS TEXT)) = true
         THEN(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0))
         ELSE 0
         END AS numBillingDaysName,
				coalesce(bitInterestType,false) AS tintInterestType,
				tintopptype,
				fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate AS bintShippedDate,
				OM.numDivisionId,
				coalesce(numShipVia,0) AS numShipVia,
				coalesce(vcTrackingURL,'') AS vcTrackingURL,
				coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
				coalesce(bitPartialFulfilment,false) AS bitPartialFulfilment,
				fn_GetContactName(OM.numrecowner)  AS OrderRecOwner,
				fn_GetContactName(DM.numRecOwner) AS AccountRecOwner,
				fn_GetContactName(OM.numassignedto) AS AssigneeName,
				coalesce((SELECT vcEmail FROM AdditionalContactsInformation WHERE numContactId = OM.numassignedto),'') AS AssigneeEmail,
				coalesce((SELECT numPhone FROM AdditionalContactsInformation WHERE numContactId = OM.numassignedto),'') AS AssigneePhone,
				fn_GetComapnyName(OM.numDivisionId) AS OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				fn_GetContactName(OM.numContactId)  AS OrgContactName,
				getCompanyAddress(OM.numDivisionId,1::SMALLINT,OM.numDomainId) AS CompanyBillingAddress,
				CASE
         WHEN ACI.numPhone <> ''
         THEN ACI.numPhone || CASE
            WHEN ACI.numPhoneExtension <> ''
            THEN ' - ' || ACI.numPhoneExtension
            ELSE ''
            END
         ELSE ''
         END AS OrgContactPhone,
				coalesce(ACI.vcEmail,'') AS OrgContactEmail,
				fn_GetContactName(OM.numrecowner) AS OnlyOrderRecOwner,
				(SELECT  SD.vcSignatureFile FROM SignatureDetail SD
         JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID
         WHERE SD.numDomainID = v_numDomainID
         AND OBD.numDomainId = v_numDomainID
         AND OBD.numSignID IS not null
         ORDER BY OBD.numBizDocsPaymentDetId DESC LIMIT 1) AS vcSignatureFile,
				coalesce(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS "P.O.No",
				OM.txtComments AS Comments,
				D.vcBizDocImagePath
				,FormatedDateFromDate(OM.dtReleaseDate::VARCHAR(50),v_numDomainID) AS vcReleaseDate
				,FormatedDateFromDate(OM.intPEstimatedCloseDate::VARCHAR(50),v_numDomainID) AS vcRequiredDate
				,(CASE WHEN coalesce(OM.numPartner,0) > 0 THEN coalesce((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = OM.numPartner),'') ELSE '' END) AS vcPartner,
				coalesce("vcCustomerPO#",'') AS "vcCustomerPO#",
				coalesce(OM.txtComments,'') AS vcSOComments,
				GetOrderAssemblyKitInclusionDetails(OI.numOppId,OI.numoppitemtCode,OBDI.numUnitHour,CAST(coalesce(D.tintCommitAllocation,1) AS smallint),true) AS vcInclusionDetails,
				coalesce(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
				FormatedDateFromDate(OM.bintCreatedDate::VARCHAR(50),v_numDomainID) As OrderCreatedDate,
				(CASE WHEN coalesce(OBD.numSourceBizDocId,0) > 0 THEN coalesce((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = OBD.numSourceBizDocId),'') ELSE coalesce(vcVendorInvoice,'') END) AS vcVendorInvoice
				,coalesce(CheckOrderInventoryStatus(OM.numOppId,OM.numDomainId,CAST(2 AS SMALLINT)),'') AS vcInventoryStatus
	
         FROM OpportunityMaster AS OM
         join OpportunityBizDocs OBD on OM.numOppId = OBD.numoppid
         JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID = OBD.numOppBizDocsId
         JOIN OpportunityItems AS OI on OI.numoppitemtCode = CAST(OBDI.numOppItemID AS numeric) and OI.numOppId = OI.numOppId
         JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = v_numDomainID
         LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
         LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID
         JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainID
         JOIN AdditionalContactsInformation ACI ON ACI.numContactId = OM.numContactId
         JOIN Domain D ON D.numDomainId = OM.numDomainId
         LEFT OUTER JOIN Currency C ON C.numCurrencyID = OM.numCurrencyID
         WHERE OM.numDomainId = v_numDomainID
         AND OBD.numOppBizDocsId IN(SELECT * FROM SplitIDs(v_vcBizDocsIds,','));
      v_strSql := '';
	

	/**** START: Added Sales Tax Column ****/

      EXECUTE 'ALTER TABLE tt_TEMP_PACKING_LIST add TotalTax DECIMAL(20,5)';
      EXECUTE 'ALTER TABLE tt_TEMP_PACKING_LIST add CRV DECIMAL(20,5)';
      select   COUNT(*) INTO v_Count FROM tt_TEMP_PACKING_LIST;
      WHILE v_i <= v_Count LOOP
         v_strSQLUpdate := '';
         select   numDomainID, numOppId, tintTaxOperator, numoppitemtCode INTO v_numDomainID,v_numOppId,v_tintTaxOperator,v_numOppItemCode FROM
         tt_TEMP_PACKING_LIST WHERE
         SRNO = v_i;
         IF v_tintTaxOperator = 1 then
		
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
            || ',TotalTax = fn_CalBizDocTaxAmt('
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
            || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numOppItemCode AS VARCHAR(20)),1,20) || ',CAST(1 AS SMALLINT),Amount,numUnitHour)';
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
            || ',CRV = fn_CalBizDocTaxAmt('
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
            || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numOppItemCode AS VARCHAR(20)),1,20) || ',CAST(1 AS SMALLINT),Amount,numUnitHour)';
         ELSEIF v_tintTaxOperator = 2
         then -- remove sales tax
		
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', TotalTax = 0';
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
         ELSE
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
            || ',TotalTax = fn_CalBizDocTaxAmt('
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
            || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numOppItemCode AS VARCHAR(20)),1,20) || ',CAST(1 AS SMALLINT),Amount,numUnitHour)';
            v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
            || ', CRV = fn_CalBizDocTaxAmt('
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
            || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ','
            || SUBSTR(CAST(v_numOppItemCode AS VARCHAR(20)),1,20) || ',CAST(1 AS SMALLINT),Amount,numUnitHour)';
         end if;
         IF v_strSQLUpdate <> '' then
		
            v_strSQLUpdate := 'UPDATE tt_TEMP_PACKING_LIST SET numItemCode = numItemCode' || coalesce(v_strSQLUpdate,'') || ' WHERE numItemCode > 0 AND numOppId =' || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ' AND numoppitemtCode =' || SUBSTR(CAST(v_numOppItemCode AS VARCHAR(20)),1,20);
            RAISE NOTICE '%',v_strSQLUpdate;
            EXECUTE v_strSQLUpdate;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      v_strSQLUpdate := '';
      v_numTaxItemID := 0;
      select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM TaxItems WHERE numDomainID = v_numDomainID    LIMIT 1;
      WHILE v_numTaxItemID > 0 LOOP
         SWV_ExecDyn := 'ALTER TABLE tt_TEMP_PACKING_LIST add "' || coalesce(v_vcTaxName,'') || '" DECIMAL(20,5)';
         EXECUTE SWV_ExecDyn;
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',"' || coalesce(v_vcTaxName,'')  || '"= fn_CalBizDocTaxAmt(numDomainID,'
         || SUBSTR(CAST(v_numTaxItemID AS VARCHAR(20)),1,20) || ',numOppId,numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
         select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM TaxItems WHERE numDomainID = v_numDomainID AND numTaxItemID > v_numTaxItemID    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numTaxItemID := 0;
         end if;
      END LOOP;
      IF v_strSQLUpdate <> '' then
	
         v_strSQLUpdate := 'UPDATE tt_TEMP_PACKING_LIST SET numItemCode=numItemCode' || coalesce(v_strSQLUpdate,'') || ' WHERE numItemCode > 0';
         RAISE NOTICE '%',v_strSQLUpdate;
         EXECUTE v_strSQLUpdate;
      end if;

	/**** END: Added Sales Tax Column ****/
	
      RAISE NOTICE 'START';
      RAISE NOTICE '%','QUERY : ' || coalesce(v_strSql,'');
      select   numFieldId, Fld_label, (intRowNum+1) INTO v_numFldID,v_vcFldname,v_intRowNum FROM DycFormConfigurationDetails DTL
      JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFieldID WHERE DTL.numFormID = 7
      AND FIELD_MASTER.Grp_id = 5
      AND DTL.numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId   ORDER BY intRowNum LIMIT 1;
      RAISE NOTICE '%',v_intRowNum;
      WHILE v_intRowNum > 0 LOOP
         RAISE NOTICE 'FROM LOOP';
         RAISE NOTICE '%',v_vcFldname;
         RAISE NOTICE '%',v_numFldID;
         SWV_ExecDyn := 'ALTER TABLE tt_TEMP_PACKING_LIST add "' || coalesce(v_vcFldname,'') || '" VARCHAR(100)';
         EXECUTE SWV_ExecDyn;
         v_strSql := 'UPDATE tt_TEMP_PACKING_LIST SET "' || coalesce(v_vcFldname,'') || '" = GetCustFldValueItem(' || SUBSTR(CAST(v_numFldID AS VARCHAR(10)),1,10) || ',numItemCode) 
						   WHERE numItemCode > 0';
         RAISE NOTICE '%','QUERY : ' || coalesce(v_strSql,'');
         EXECUTE v_strSql;
         select   numFieldId, Fld_label, (intRowNum+1) INTO v_numFldID,v_vcFldname,v_intRowNum FROM DycFormConfigurationDetails DETAIL
         JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFieldID WHERE DETAIL.numFormID = 7
         AND MASTER.Grp_id = 5
         AND DETAIL.numDomainID = v_numDomainID
         AND numAuthGroupID = v_numBizDocId
         AND intRowNum >= v_intRowNum   ORDER BY intRowNum LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then 
            v_intRowNum := 0;
         end if;
      END LOOP;
      RAISE NOTICE 'END';
      open SWV_RefCur for
      SELECT * FROM tt_TEMP_PACKING_LIST ORDER BY SRNO;
   end if;
   RETURN;
END; $$;
--Created By                                                          


