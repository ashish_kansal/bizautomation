-- Stored procedure definition script USP_SaveJournalDetailsForMakeDeposit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveJournalDetailsForMakeDeposit(v_numJournalId NUMERIC(9,0) DEFAULT 0,                                                                  
v_strRow TEXT DEFAULT '',                                                  
v_Mode SMALLINT DEFAULT 0,                    
v_RecurringMode SMALLINT DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMaxJournalId  NUMERIC(9,0);
   v_hDoc3  INTEGER;
BEGIN
   if v_RecurringMode = 0 then

      if SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then
         if v_Mode = 0 then
            Insert Into General_Journal_Details
			(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile)
            Select 
			numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,vcMemo,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile 
			from
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					id FOR ORDINALITY,
					TransactionId NUMERIC(18,0) PATH 'TransactionId',
					numJournalId NUMERIC(9,0) PATH 'numJournalId',
					numDebitAmt DECIMAL(20,5) PATH 'numDebitAmt',
					numCreditAmt DECIMAL(20,5) PATH 'numCreditAmt',
					numChartAcntId NUMERIC(9,0) PATH 'numChartAcntId',
					vcMemo TEXT PATH 'vcMemo',
					numCustomerId NUMERIC(9,0) PATH 'numCustomerId',
					numDomainId NUMERIC(9,0) PATH 'numDomainId',
					bitMainDeposit BOOLEAN PATH 'bitMainDeposit',
					vcReference VARCHAR(50) PATH 'vcReference',
					numPaymentMethod NUMERIC(9,0) PATH 'numPaymentMethod',
					bitReconcile BOOLEAN PATH 'bitReconcile'
			) AS X
			WHERE
				COALESCE(TransactionId,0) = 0;
         end if;
         If v_Mode = 1 then                                                                           
            Update 
				General_Journal_Details  
			Set 
				numDebitAmt = X.numDebitAmt
				,numCreditAmt = X.numCreditAmt
				,numChartAcntId = X.numChartAcntId
				,varDescription = X.vcMemo
				,numCustomerId = X.numCustomerId
				,bitMainDeposit = X.bitMainDeposit
				,vcReference = X.vcReference
				,numPaymentMethod = X.numPaymentMethod
            From
			(
				SELECT 
					* 
				FROM 
				XMLTABLE
				(
					'NewDataSet/Table1'
					PASSING 
						CAST(v_strRow AS XML)
					COLUMNS
						id FOR ORDINALITY,
						TransactionId NUMERIC(18,0) PATH 'TransactionId',
						numJournalId NUMERIC(9,0) PATH 'numJournalId',
						numDebitAmt DECIMAL(20,5) PATH 'numDebitAmt',
						numCreditAmt DECIMAL(20,5) PATH 'numCreditAmt',
						numChartAcntId NUMERIC(9,0) PATH 'numChartAcntId',
						vcMemo TEXT PATH 'vcMemo',
						numCustomerId NUMERIC(9,0) PATH 'numCustomerId',
						numDomainId NUMERIC(9,0) PATH 'numDomainId',
						bitMainDeposit BOOLEAN PATH 'bitMainDeposit',
						vcReference VARCHAR(50) PATH 'vcReference',
						numPaymentMethod NUMERIC(9,0) PATH 'numPaymentMethod',
						bitReconcile BOOLEAN PATH 'bitReconcile'
				) AS TEMP
				WHERE
					COALESCE(TransactionId,0) > 0
			) X
            Where numTransactionId = X.TransactionId;

            Insert Into General_Journal_Details
			(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile)
            Select 
			numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,vcMemo,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile 
			from
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					id FOR ORDINALITY,
					TransactionId NUMERIC(18,0) PATH 'TransactionId',
					numJournalId NUMERIC(9,0) PATH 'numJournalId',
					numDebitAmt DECIMAL(20,5) PATH 'numDebitAmt',
					numCreditAmt DECIMAL(20,5) PATH 'numCreditAmt',
					numChartAcntId NUMERIC(9,0) PATH 'numChartAcntId',
					vcMemo TEXT PATH 'vcMemo',
					numCustomerId NUMERIC(9,0) PATH 'numCustomerId',
					numDomainId NUMERIC(9,0) PATH 'numDomainId',
					bitMainDeposit BOOLEAN PATH 'bitMainDeposit',
					vcReference VARCHAR(50) PATH 'vcReference',
					numPaymentMethod NUMERIC(9,0) PATH 'numPaymentMethod',
					bitReconcile BOOLEAN PATH 'bitReconcile'
			) AS X
			WHERE
				COALESCE(TransactionId,0) = 0;
         end if;
      end if;
   end if;        
                    
  ---For Recurring Transaction                            
                            
   if v_RecurringMode = 1 then
           
                            
--Exec USP_UpdateChartAcntOpnBalance @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                            
      select   max(numJOurnal_Id) INTO v_numMaxJournalId From General_Journal_Header Where numDomainId = v_numDomainId;
      insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod)
      Select v_numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod from General_Journal_Details Where numJournalId = v_numJournalId And numDomainId = v_numDomainId;
   end if;
   RETURN;
END; $$;


