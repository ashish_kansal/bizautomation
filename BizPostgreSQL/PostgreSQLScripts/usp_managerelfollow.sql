-- Stored procedure definition script USP_ManageRelFollow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRelFollow(v_numRelationship NUMERIC(9,0),  
v_numFollow NUMERIC(9,0),  
v_numRelFolID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numRelFolID > 0 then

      update  RelFollowupStatus set numRelationshipID = v_numRelationship,numFollowID = v_numFollow where numRelFolID = v_numRelFolID;
   else
      insert into RelFollowupStatus(numRelationshipID,numFollowID) values(v_numRelationship,v_numFollow);
   end if;
   RETURN;
END; $$;


