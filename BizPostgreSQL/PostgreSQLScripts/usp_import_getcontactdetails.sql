-- Stored procedure definition script USP_Import_GetContactDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Import_GetContactDetails(v_numDomainID NUMERIC(18,0)
	,v_numContactID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numDivisionId
		,numContactId
		,vcFirstName
		,vcLastname
		,vcEmail
		,numContactType
		,numPhone
		,numPhoneExtension
		,numCell
		,numHomePhone
		,vcFax
		,vcCategory
		,numTeam
		,vcPosition
		,vcTitle
		,vcDepartment
		,txtNotes
		,coalesce((SELECT numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID = v_numDomainID AND numContactId = v_numContactID LIMIT 1),'0') AS vcNonBizContactID
		,bitPrimaryContact
   FROM
   AdditionalContactsInformation
   WHERE
   numDomainID = v_numDomainID
   AND numContactId = v_numContactID;
   RETURN;
END; $$;













