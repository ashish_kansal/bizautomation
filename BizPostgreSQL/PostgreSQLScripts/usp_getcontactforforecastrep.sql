-- Stored procedure definition script usp_GetContactForForecastRep for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactForForecastRep(v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     numContactId, vcFirstName, vcLastname, numPhone, numPhoneExtension, vcPosition
   FROM         AdditionalContactsInformation
   WHERE numCreatedBy = v_numCreatedBy
   AND numDomainID = v_numDomainID;
END; $$;












