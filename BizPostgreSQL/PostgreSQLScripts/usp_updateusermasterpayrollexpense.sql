-- Stored procedure definition script USP_UpdateUserMasterPayrollExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateUserMasterPayrollExpense(v_numUserID NUMERIC(9,0),                                    
v_numUserCntID NUMERIC(9,0),                              
v_numDomainID NUMERIC(9,0),                              
v_bitHourlyRate BOOLEAN DEFAULT false,                          
v_monHourlyRate DECIMAL(20,5) DEFAULT NULL,                          
v_bitSalary BOOLEAN DEFAULT false,                  
v_numDailyHours DOUBLE PRECISION DEFAULT NULL,                
v_bitPaidLeave BOOLEAN DEFAULT false,              
v_fltPaidLeaveHrs DOUBLE PRECISION DEFAULT 0,              
v_fltPaidLeaveEmpHrs DOUBLE PRECISION DEFAULT 0,              
v_bitOverTime BOOLEAN DEFAULT false,  
v_bitOverTimeHrsDailyOrWeekly BOOLEAN DEFAULT false,                        
v_numLimDailHrs DOUBLE PRECISION DEFAULT NULL,                          
v_monOverTimeRate DECIMAL(20,5) DEFAULT NULL,             
v_bitManagerOfOwner BOOLEAN DEFAULT false,
v_fltManagerOfOwner DOUBLE PRECISION DEFAULT 0,
v_bitCommOwner BOOLEAN DEFAULT false,              
v_fltCommOwner DOUBLE PRECISION DEFAULT 0,              
v_bitCommAssignee BOOLEAN DEFAULT false,              
v_fltCommAssignee DOUBLE PRECISION DEFAULT 0,              
v_bitRoleComm BOOLEAN DEFAULT false,              
v_bitEmpNetAccount BOOLEAN DEFAULT false,              
v_dtHired TIMESTAMP DEFAULT NULL,              
v_dtReleased TIMESTAMP DEFAULT NULL,        
v_vcEmployeeId VARCHAR(150) DEFAULT NULL,        
v_vcEmergencyContactInfo VARCHAR(250) DEFAULT NULL,      
v_dtSalaryDateTime TIMESTAMP DEFAULT NULL,    
v_bitPayroll BOOLEAN DEFAULT false,  
v_fltEmpRating DOUBLE PRECISION DEFAULT 0,  
v_dtEmpRating TIMESTAMP DEFAULT NULL,  
v_monNetPay DECIMAL(20,5) DEFAULT NULL,  
v_monAmtWithheld DECIMAL(20,5) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_dtEmpRating = '1753-01-01 12:00:00 AM' then 
      v_dtEmpRating := null;
   end if;              
   UPDATE UserMaster SET
   numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
   bitHourlyRate = v_bitHourlyRate,monHourlyRate = v_monHourlyRate,bitSalary = v_bitSalary,
   numDailyHours = v_numDailyHours,bitPaidLeave = v_bitPaidLeave,
   fltPaidLeaveHrs = v_fltPaidLeaveHrs,fltPaidLeaveEmpHrs = v_fltPaidLeaveEmpHrs,
   bitOverTime = v_bitOverTime,bitOverTimeHrsDailyOrWeekly = v_bitOverTimeHrsDailyOrWeekly,
   numLimDailHrs = v_numLimDailHrs,monOverTimeRate = v_monOverTimeRate,
   bitManagerOfOwner = v_bitManagerOfOwner,fltManagerOfOwner = v_fltManagerOfOwner,
   bitCommOwner = v_bitCommOwner,fltCommOwner = v_fltCommOwner,
   bitCommAssignee = v_bitCommAssignee,fltCommAssignee = v_fltCommAssignee,
   bitRoleComm = v_bitRoleComm,bitEmpNetAccount = v_bitEmpNetAccount,dtHired = v_dtHired,
   dtReleased = v_dtReleased,vcEmployeeId = v_vcEmployeeId,vcEmergencyContact = v_vcEmergencyContactInfo,
   dtSalaryDateTime = v_dtSalaryDateTime,
   bitpayroll = v_bitPayroll,fltEmpRating = v_fltEmpRating,dtEmpRating = v_dtEmpRating,
   monNetPay = v_monNetPay,monAmtWithheld = v_monAmtWithheld
   WHERE numUserId = v_numUserID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_UpdateVendors]    Script Date: 07/26/2008 16:21:55 ******/



