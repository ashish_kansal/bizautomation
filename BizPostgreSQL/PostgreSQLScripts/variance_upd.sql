CREATE OR REPLACE FUNCTION Variance_Upd (
	p_AllDayEvent boolean,
	p_ActivityDescription text,
	p_Duration integer,
	p_Location TEXT,
	p_StartDateTimeUtc TIMESTAMP,
	p_Subject varchar(255),
	p_EnableReminder boolean,
	p_ReminderInterval integer,
	p_ShowTimeAs	integer,
	p_Importance	integer,
	p_Status	integer,
	p_OriginalStartDateTimeUtc TIMESTAMP,
	p_RecurrenceKey integer,
	p_DataKey integer,
	p_Color SMALLINT DEFAULT 0
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE
		Activity 
	SET 
		AllDayEvent = p_AllDayEvent,
		ActivityDescription = p_ActivityDescription,
		Duration = p_Duration, 
		Location = p_Location,
		StartDateTimeUtc = p_StartDateTimeUtc,
		Subject = p_Subject, 
		EnableReminder = p_EnableReminder,
		ReminderInterval = p_ReminderInterval,
		ShowTimeAs = p_ShowTimeAs,
		Importance = p_Importance,
		Status = p_Status,
		RecurrenceID = p_RecurrenceKey,
		OriginalStartDateTimeUtc = p_OriginalStartDateTimeUtc,
		Color=p_Color
	WHERE 
		ActivityID = p_DataKey;
RETURN;
END; $$;


