-- Stored procedure definition script GetCategories for PostgreSQL
CREATE OR REPLACE FUNCTION GetCategories(v_ParentID   INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numAccountId, cast(vcAccountName as VARCHAR(255)), CountChildren(numAccountId::INTEGER,0)
   As CulCount
   FROM
   Chart_Of_Accounts
   WHERE
   numparentaccid = v_ParentID;
END; $$;













