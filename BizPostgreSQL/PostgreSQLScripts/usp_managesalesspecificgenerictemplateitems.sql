-- Stored procedure definition script USP_ManageSalesSpecificGenericTemplateItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSalesSpecificGenericTemplateItems(v_numSalesTemplateItemID NUMERIC(18,0),
	v_numSalesTemplateID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0),
	v_numItemCode NUMERIC(18,0),
	v_numWarehouseItmsID NUMERIC(18,0) ,
	v_numUOM NUMERIC(18,0)  ,
	v_vcUOMName VARCHAR(50) ,
	v_Attributes VARCHAR(1000) ,
	v_AttrValues VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monListPrice  DECIMAL(20,5);
   v_numWareHouseID  NUMERIC(18,0);
   v_ItemType  VARCHAR(100);
   v_numVendorId  NUMERIC(18,0);
BEGIN
 
		--, @numUOM NUMERIC(18), @vcUOMName VARCHAR(100),@numWarehouseItmsID NUMERIC(18)
		

   IF EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numCurrencyID,0) = 0) then
		
      select   decDiscount INTO v_monListPrice FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numCurrencyID,0) = 0    LIMIT 1;
   ELSE
      select   monListPrice INTO v_monListPrice FROM Item WHERE numItemCode = v_numItemCode;
   end if;
   IF v_numSalesTemplateItemID > 0 then
	
      UPDATE SalesTemplateItems
      SET numSalesTemplateID = v_numSalesTemplateID,numDomainID = v_numDomainID,
      numItemCode = v_numItemCode,numWarehouseItmsID = v_numWarehouseItmsID,numUOM = v_numUOM,
      vcUOMName = v_vcUOMName,Attributes = v_Attributes,AttrValues = v_AttrValues
      WHERE numSalesTemplateItemID = v_numSalesTemplateItemID;
   ELSE
      INSERT INTO SalesTemplateItems(numSalesTemplateID,
			numDomainID,
			numItemCode,
			numWarehouseItmsID,
			numUOM,
			vcUOMName,
			Attributes,
			AttrValues,
			numUnitHour,
			monPrice,
			numWarehouseID,
			ItemType,
			monTotAmount,
			monTotAmtBefDiscount,
			numSOVendorId)
		VALUES(v_numSalesTemplateID,
			v_numDomainID,
			v_numItemCode,
			v_numWarehouseItmsID,
			v_numUOM,
			v_vcUOMName,
			v_Attributes,
			v_AttrValues,
			1,
			v_monListPrice,
			v_numWareHouseID,
			v_ItemType,
			v_monListPrice,
			v_monListPrice,
			v_numVendorId);
   end if;
   RETURN;
END; $$;

