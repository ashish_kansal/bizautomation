-- Stored procedure definition script USP_GetPageElementMaster for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPageElementMaster(v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numElementID,
		vcElementName,
		vcUserControlPath,
		vcTagName,
		coalesce(bitCustomization,false) AS bitCustomization
		,coalesce(bitAdd,false) AS bitAdd
		,coalesce(bitDelete,false) AS bitDelete
   FROM
   PageElementMaster
   WHERE
		(coalesce(numSiteID,0) = 0 OR coalesce(numSiteID,0) = v_numSiteID)
   ORDER BY
   vcElementName;
END; $$;

--Created by anoop jayaraj













