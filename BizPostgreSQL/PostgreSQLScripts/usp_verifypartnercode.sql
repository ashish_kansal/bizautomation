-- Stored procedure definition script usp_VerifyPartnerCode for PostgreSQL
CREATE OR REPLACE FUNCTION usp_VerifyPartnerCode(v_vcPartnerCode  VARCHAR(200) DEFAULT NULL,                                           
 v_numDomainID  NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   SELECT  numDomainID INTO v_numDomainID FROM Sites WHERE numSiteID = v_numDomainID     LIMIT 1;                                       
   open SWV_RefCur for SELECT  numDivisionID FROM DivisionMaster WHERE numDomainID = v_numDomainID AND vcPartnerCode = v_vcPartnerCode LIMIT 1;
END; $$;












