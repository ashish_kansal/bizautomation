DROP FUNCTION IF EXISTS usp_ManageAddlContInfo;

CREATE OR REPLACE FUNCTION usp_ManageAddlContInfo(v_numcontactId NUMERIC DEFAULT 0,                                    
 v_numContactType NUMERIC DEFAULT 0,                                    
 v_vcDepartment NUMERIC(9,0) DEFAULT 0,                                    
 v_vcCategory NUMERIC(9,0) DEFAULT 0,                                    
 v_vcGivenName VARCHAR(100) DEFAULT '',                                    
 v_vcFirstName VARCHAR(50) DEFAULT '',                                    
 v_vcLastName VARCHAR(50) DEFAULT '',                                    
 v_numDivisionId NUMERIC DEFAULT NULL,                                    
 v_numPhone VARCHAR(100) DEFAULT '',                                    
 v_numPhoneExtension VARCHAR(7) DEFAULT '',                                    
 v_numCell VARCHAR(100) DEFAULT '',                                    
 v_NumHomePhone VARCHAR(100) DEFAULT '',                                    
 v_vcFax VARCHAR(100) DEFAULT '',                                    
 v_vcEmail VARCHAR(50) DEFAULT '',                                    
 v_VcAsstFirstName VARCHAR(50) DEFAULT '',                                    
 v_vcAsstLastName VARCHAR(50) DEFAULT '',                                    
 v_numAsstPhone VARCHAR(100) DEFAULT '',                                    
 v_numAsstExtn VARCHAR(6) DEFAULT '',                                    
 v_vcAsstEmail VARCHAR(50) DEFAULT '',                                                      
 v_charSex CHAR(1) DEFAULT '',                                    
 v_bintDOB TIMESTAMP DEFAULT NULL,                                    
 v_vcPosition NUMERIC(9,0) DEFAULT 0,                                                     
 v_txtNotes TEXT DEFAULT '',                                                     
 v_numUserCntID NUMERIC DEFAULT NULL,                                                                                              
 v_numDomainID NUMERIC DEFAULT 1,                                                   
 v_vcPStreet VARCHAR(100) DEFAULT '',                                    
 v_vcPCity VARCHAR(50) DEFAULT '',                                    
 v_vcPPostalCode VARCHAR(15) DEFAULT '',                                    
 v_vcPState NUMERIC(9,0) DEFAULT 0,                                    
 v_vcPCountry NUMERIC(9,0) DEFAULT 0,                  
 v_numManagerID NUMERIC DEFAULT 0,        
 v_numTeam NUMERIC(9,0) DEFAULT 0,        
 v_numEmpStatus NUMERIC(9,0) DEFAULT 0,
 v_vcTitle VARCHAR(100) DEFAULT '',
 v_bitPrimaryContact BOOLEAN DEFAULT false,
 v_bitOptOut BOOLEAN DEFAULT false,
 v_vcLinkedinId VARCHAR(30) DEFAULT NULL,
 v_vcLinkedinUrl VARCHAR(300) DEFAULT NULL,
 v_numECampaignID NUMERIC(18,0) DEFAULT 0,
 v_vcPassword VARCHAR(500) DEFAULT '',
 v_vcTaxID VARCHAR(100) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCount  NUMERIC(18,0);
   v_bitAutoPopulateAddress  BOOLEAN; 
   v_tintPoulateAddressTo  SMALLINT;    
  ------Check the Email id Exist into the Email Master Table

   v_numID  NUMERIC(9,0);            
   v_bitPrimaryContactCheck  BOOLEAN;                 
    
    
   v_Date  TIMESTAMP;             
	
   v_vcName  VARCHAR(200) DEFAULT CONCAT(v_vcFirstName,' ',v_vcLastName);
   v_numExtranetID  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   If LENGTH(coalesce(v_vcFirstName,'')) = 0 then
	
      v_vcFirstName := '-';
   end if;

   If LENGTH(coalesce(v_vcLastName,'')) = 0 then
	
      v_vcLastName := '-';
   end if;

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

   v_vcGivenName := coalesce(v_vcLastName,'') || ', ' || coalesce(v_vcFirstName,'')  || '.eml';                                 
   if v_bintDOB = 'Jan  1 1753 12:00AM' then 
      v_bintDOB := null;
   end if;                      
   IF v_numcontactId = 0 then
      IF v_bitPrimaryContact = true then
         select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM    AdditionalContactsInformation WHERE   numDivisionId = v_numDivisionId    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numID := 0;
         end if;
         WHILE v_numID > 0 LOOP
            IF v_bitPrimaryContactCheck = true then
               UPDATE  AdditionalContactsInformation
               SET     bitPrimaryContact = false
               WHERE   numContactId = v_numID;
            end if;
            select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM    AdditionalContactsInformation WHERE   numDivisionId = v_numDivisionId
            AND numContactId > v_numID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numID := 0;
            end if;
         END LOOP;
      ELSEIF v_bitPrimaryContact = false
      then
    
         IF(SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionId AND numContactId != v_numcontactId AND coalesce(bitPrimaryContact,false) = true) = 0 then
            v_bitPrimaryContact := true;
         end if;
      end if;
      select   coalesce(bitAutoPopulateAddress,false), coalesce(tintPoulateAddressTo,'0') INTO v_bitAutoPopulateAddress,v_tintPoulateAddressTo from Domain where numDomainId = v_numDomainID;
      if (v_bitAutoPopulateAddress = true) then
         if(v_tintPoulateAddressTo = 1) then --if primary address is not specified then getting billing address    
	
            select   AD1.vcStreet, AD1.vcCity, AD1.numState, AD1.vcPostalCode, AD1.numCountry INTO v_vcPStreet,v_vcPCity,v_vcPState,v_vcPPostalCode,v_vcPCountry from DivisionMaster  DM
            LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
            AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true where numDivisionID = v_numDivisionId;
         ELSEIF (v_tintPoulateAddressTo = 2)
         then-- Primary Address is Shipping Address
	
            select   AD2.vcStreet, AD2.vcCity, AD2.numState, AD2.vcPostalCode, AD2.numCountry INTO v_vcPStreet,v_vcPCity,v_vcPState,v_vcPPostalCode,v_vcPCountry from DivisionMaster DM
            LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
            AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true where numDivisionID = v_numDivisionId;
         end if;
      end if;
      if (v_numEmpStatus is null or v_numEmpStatus = 0) then 
         v_numEmpStatus := 658;
      end if;
      INSERT into AdditionalContactsInformation(numContactType,
   vcDepartment,
   vcCategory,
   vcGivenName,
   vcFirstName,
   vcLastname,
   numDivisionId ,
   numPhone ,
   numPhoneExtension,
   numCell ,
   numHomePhone ,
   vcFax ,
   vcEmail ,
   VcAsstFirstName ,
   vcAsstLastName ,
   numAsstPhone ,
   numAsstExtn ,
   vcAsstEmail  ,
   charSex ,
   bintDOB ,
   vcPosition ,
   txtNotes ,
   numCreatedBy ,
   bintCreatedDate,
   numModifiedBy,
   bintModifiedDate,
   numDomainID,
   numManagerID,
   numRecOwner,
   numTeam,
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID)
    VALUES(v_numContactType,
   v_vcDepartment,
   v_vcCategory,
   v_vcGivenName ,
   v_vcFirstName ,
   v_vcLastName,
   v_numDivisionId ,
   v_numPhone ,
   v_numPhoneExtension,
   v_numCell ,
   v_NumHomePhone ,
   v_vcFax ,
   v_vcEmail ,
   v_VcAsstFirstName,
   v_vcAsstLastName,
   v_numAsstPhone,
   v_numAsstExtn,
   v_vcAsstEmail,
   v_charSex,
   v_bintDOB ,
   v_vcPosition,
   v_txtNotes,
   v_numUserCntID,
   TIMEZONE('UTC',now()),
   v_numUserCntID,
   TIMEZONE('UTC',now()),
   v_numDomainID,
   v_numManagerID,
   v_numUserCntID,
   v_numTeam,
   v_numEmpStatus,
   v_vcTitle,v_bitPrimaryContact,v_bitOptOut,
   v_vcLinkedinId,v_vcLinkedinUrl,v_numECampaignID,v_vcTaxID) RETURNING numcontactId INTO v_numcontactId;
 
      open SWV_RefCur for
      SELECT v_numcontactId;
      INSERT INTO AddressDetails(vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID) VALUES('Primary', v_vcPStreet,v_vcPCity,v_vcPPostalCode,v_vcPState,v_vcPCountry,true,1,0,v_numcontactId,v_numDomainID);
	 
      IF coalesce(v_numECampaignID,0) > 0 then
	 
		  --Manage ECampiagn When Campaign is changed from inline edit
		 --  numeric(9, 0)     
	 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  datetime
		 --  bit
         v_Date := GetUTCDateWithoutTime();
         PERFORM USP_ManageConEmailCampaign(v_numConEmailCampID := 0,v_numcontactId := v_numcontactId,v_numECampaignID := v_numECampaignID,
         v_intStartDate := v_Date,v_bitEngaged := true,
         v_numUserCntID := v_numUserCntID);
      end if;
      IF v_numDivisionId = coalesce((SELECT numDivisionId FROM Domain WHERE numDomainId = v_numDomainID),
      0) then
         PERFORM usp_SetUsersWithDomains(v_numUserID := 0,v_vcUserName := v_vcName,v_vcUserDesc := '',v_numGroupId := 0,
         v_numUserDetailID := v_numcontactId,v_numUserCntID := v_numUserCntID,
         v_strTerritory := '',v_numDomainID := v_numDomainID,v_strTeam := '',
         v_vcEmail := v_vcEmail,v_vcPassword := '',v_Active := false,v_numDefaultClass := 0,
         v_numDefaultWarehouse := 0,v_vcLinkedinId := null,v_intAssociate := 0,
         v_ProfilePic := null,v_bitPayroll := false,v_monHourlyRate := 0);
      end if;
   ELSEIF v_numcontactId > 0
   then
    
      UPDATE AdditionalContactsInformation SET
      numContactType = v_numContactType,vcGivenName = v_vcGivenName,vcFirstName = v_vcFirstName,
      vcLastname = v_vcLastName,numDivisionId = v_numDivisionId,
      numPhone = v_numPhone,numPhoneExtension = v_numPhoneExtension,numCell = v_numCell,
      numHomePhone = v_NumHomePhone,vcFax = v_vcFax,vcEmail = v_vcEmail,VcAsstFirstName = v_VcAsstFirstName,
      vcAsstLastName = v_vcAsstLastName,numAsstPhone = v_numAsstPhone,
      numAsstExtn = v_numAsstExtn,vcAsstEmail = v_vcAsstEmail,
      charSex = v_charSex,bintDOB = v_bintDOB,vcPosition = v_vcPosition,txtNotes = v_txtNotes,
      numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      numManagerID = v_numManagerID,bitPrimaryContact = v_bitPrimaryContact
      WHERE numContactId = v_numcontactId;
      Update AddressDetails set
      vcStreet = v_vcPStreet,vcCity = v_vcPCity,vcPostalCode = v_vcPPostalCode,numState = v_vcPState,
      numCountry = v_vcPCountry
      where numRecordID = v_numcontactId AND bitIsPrimary = true AND tintAddressOf = 1 AND tintAddressType = 0;
      open SWV_RefCur for
      SELECT v_numcontactId;
   end if;    
  ------Check the Email id Exist into the Email Master Table

   IF v_vcPassword IS NOT NULL AND  LENGTH(v_vcPassword) > 0 then
	
      IF EXISTS(SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivisionId) then
		
         IF EXISTS(SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID = v_numcontactId) then
			
            UPDATE
            ExtranetAccountsDtl
            SET
            vcPassword = v_vcPassword,tintAccessAllowed = 1
            WHERE
            numContactID = v_numcontactId;
         ELSE
            select   numExtranetID INTO v_numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivisionId;
            INSERT INTO ExtranetAccountsDtl(numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES(v_numExtranetID,v_numcontactId,v_vcPassword,1,v_numDomainID);
         end if;
      ELSE
         RAISE EXCEPTION 'Organization e-commerce access is not enabled';
      end if;
   end if;



   v_numCount := 0;
   IF v_vcEmail <> '' then

      select   COUNT(*) INTO v_numCount FROM EmailMaster WHERE vcEmailId = v_vcEmail and numDomainID = v_numDomainID;
      IF v_numCount = 0 then

         INSERT INTO EmailMaster(vcEmailId,vcName,numDomainID,numContactID)
	VALUES(v_vcEmail,v_vcFirstName,v_numDomainID,v_numcontactId);
      else
         update EmailMaster set numContactID = v_numcontactId where vcEmailId = v_vcEmail and numDomainID = v_numDomainID;
      end if;
   end if;
   RETURN;
END; $$; 
