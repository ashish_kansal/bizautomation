-- Stored procedure definition script usp_GetDtlsForEmailComposeWindow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDtlsForEmailComposeWindow(v_strContactID VARCHAR(7000) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);
BEGIN
   v_strSQL := 'select D.numDivisionID,A.numContactId,vcFirstName,vcLastName,vcEmail as Email,vcCompanyName,vcCompanyName || '' ,<br>'' ||                                                                                           
 VcShipStreet || '' <br>'' ||                                                                                           
 VcShipCity || '' ,'' ||                                                                                           
 coalesce(fn_GetState(vcShipState),'''') || '' '' ||                                                                                          
 vcShipPostCode || '' <br>'' ||                                                   
 coalesce(fn_GetListItemName(vcShipCountry),'''') as Ship,
 '''' as "Opt-Out"
 from CompanyInfo C
join DivisionMaster D
on D.numCompanyID = C.numCompanyId
join AdditionalContactsInformation A
on A.numDivisionId = D.numDivisionID
where numContactId in(' || coalesce(v_strContactID,'') || ')'; 
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


