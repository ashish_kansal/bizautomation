-- Stored procedure definition script USP_ManageBankStatementTransactions for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageBankStatementTransactions(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT BST.* FROM BankDetails BD JOIN BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
   JOIN BankStatementTransactions BST ON BSH.numStatementID = BST.numStatementId
   WHERE BD.numDomainID = v_numDomainID;
END; $$;












