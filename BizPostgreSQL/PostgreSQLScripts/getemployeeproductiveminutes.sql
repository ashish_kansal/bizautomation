CREATE OR REPLACE FUNCTION GetEmployeeProductiveMinutes(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProductiveMinutes  NUMERIC(18,0) DEFAULT 0;
BEGIN
   select  
	COALESCE(SUM(DATEDIFF('minute',TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate)),0) INTO v_numProductiveMinutes 
	FROM
   timeandexpense
   LEFT JOIN
   OpportunityMaster
   ON
   timeandexpense.numOppId = OpportunityMaster.numOppId WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND timeandexpense.numUserCntID = v_numUserCntID
   AND coalesce(timeandexpense.numCategory,0) = 1
   AND numType NOT IN(4) -- DO NOT INCLUDE UNPAID LEAVE
   AND dtFromDate BETWEEN v_dtFromDate AND v_dtToDate;

   select   coalesce(v_numProductiveMinutes,0) + COALESCE(SUM(DATEDIFF('minute',Communication.dtStartTime,Communication.dtEndTime)),0) INTO v_numProductiveMinutes FROM
   Communication
   LEFT JOIN
   DivisionMaster DM
   ON
   Communication.numDivisionID = DM.numDivisionID
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId WHERE
   Communication.numDomainID = v_numDomainID
   AND Communication.numAssign = v_numUserCntID
   AND Communication.dtStartTime BETWEEN v_dtFromDate AND v_dtToDate
   AND coalesce(bitClosedFlag,false) = true;

   select coalesce(v_numProductiveMinutes,0) + COALESCE(SUM(DATEDIFF('minute',Activity.StartDateTimeUtc,Activity.StartDateTimeUtc + make_interval(secs => Activity.Duration))),0) INTO v_numProductiveMinutes FROM
   Resource
   INNER JOIN
   ActivityResource
   ON
   Resource.ResourceID = ActivityResource.ResourceID
   INNER JOIN
   Activity
   ON
   ActivityResource.ActivityID = Activity.ActivityID WHERE
   Resource.numUserCntId = v_numUserCntID
   AND Activity.StartDateTimeUtc BETWEEN v_dtFromDate AND v_dtToDate;

   select   coalesce(v_numProductiveMinutes,0) + COALESCE(SUM(T.numMinutes),0)  INTO v_numProductiveMinutes FROM (SELECT COALESCE(GetTimeSpendOnTaskInMinutesDateRange(v_numDomainID,v_numUserCntID,SPDTTL.numTaskId,v_dtFromDate,v_dtToDate),0) numMinutes FROM
   StagePercentageDetailsTaskTimeLog SPDTTL WHERE
   SPDTTL.numUserCntID = v_numUserCntID
   AND SPDTTL.dtActionTime BETWEEN v_dtFromDate AND v_dtToDate GROUP BY
   SPDTTL.numTaskId) T;

   RETURN v_numProductiveMinutes;
END; $$;

