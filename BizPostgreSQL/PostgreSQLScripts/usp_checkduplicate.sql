-- Stored procedure definition script USP_CheckDuplicate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckDuplicate(v_WhereCondition VARCHAR(8000), v_numDomainID NUMERIC(9,0),
 INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Sql  VARCHAR(1000);
BEGIN
   v_Sql := 'SELECT  vcCompanyName FROM AdditionalContactsInformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId 
		   WHERE C.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and ';
		
   v_Sql := coalesce(v_Sql,'') || coalesce(v_WhereCondition,'');
   RAISE NOTICE '%',v_Sql;
   OPEN SWV_RefCur FOR EXECUTE v_Sql;
   RETURN;
END; $$;


