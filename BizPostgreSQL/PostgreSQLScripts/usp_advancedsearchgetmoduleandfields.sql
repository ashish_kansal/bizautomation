DROP FUNCTION IF EXISTS USP_AdvancedSearchGetModuleAndFields;

CREATE OR REPLACE FUNCTION USP_AdvancedSearchGetModuleAndFields(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAuthGroupID  NUMERIC(18,0);
BEGIN
   select   numGroupID INTO v_numAuthGroupID FROM UserMaster WHERE numUserDetailId = v_numUserCntID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numFormID NUMERIC(18,0),
      vcFormName VARCHAR(300)
   );

   INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(1,'Organizations & Contacts');

	
   IF EXISTS(SELECT numGroupID FROM GroupAuthorization WHERE numModuleID = 9 AND numPageID = 7 AND numGroupID = v_numAuthGroupID AND coalesce(intViewAllowed,0) <> 0) then
	
      INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(15,'Opportunities & Orders (with items)');
   end if;

   IF EXISTS(SELECT numGroupID FROM GroupAuthorization WHERE numModuleID = 9 AND numPageID = 11 AND numGroupID = v_numAuthGroupID AND coalesce(intViewAllowed,0) <> 0) then
	
      INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(29,'Items');
   end if;

   IF EXISTS(SELECT numGroupID FROM GroupAuthorization WHERE numModuleID = 9 AND numPageID = 8 AND numGroupID = v_numAuthGroupID AND coalesce(intViewAllowed,0) <> 0) then
	
      INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(17,'Cases');
   end if;

   IF EXISTS(SELECT numGroupID FROM GroupAuthorization WHERE numModuleID = 9 AND numPageID = 8 AND numGroupID = v_numAuthGroupID AND coalesce(intViewAllowed,0) <> 0) then
	
      INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(18,'Projects');
   end if;

   IF EXISTS(SELECT numGroupID FROM GroupAuthorization WHERE numModuleID = 9 AND numPageID = 12 AND numGroupID = v_numAuthGroupID AND coalesce(intViewAllowed,0) <> 0) then
	
      INSERT INTO tt_TEMP(numFormID,vcFormName) VALUES(59,'Financial Transactions');
   end if;

   open SWV_RefCur for
   SELECT  numFormID AS "numFormID",
      vcFormName AS "vcFormName" FROM tt_TEMP;
	
   open SWV_RefCur2 for
   SELECT * FROM(SELECT
      DFCD.numFormID AS "numFormId"
			,DFCD.numFieldID AS "numFieldId"
			,DFFm.vcFieldName AS "vcFieldName"
			,DFFm.vcAssociatedControlType AS "vcAssociatedControlType"
			,0 AS "bitCustomField" 
			,DFM.numListID AS "numListID"
			,DFM.vcListItemType AS "vcListItemType"
			,DFM.vcLookBackTableName AS "vcLookBackTableName"
			,DFM.vcDbColumnName AS "vcDbColumnName"
			,CAST('R' AS VARCHAR(1)) AS "vcFieldType"
			,DFM.vcFieldDataType AS "vcFieldDataType"
      FROM
      DycFormConfigurationDetails DFCD
      INNER JOIN
      DycFormField_Mapping DFFm
      ON
      DFCD.numFieldID = DFFm.numFieldID
      AND DFCD.numFormID = DFFm.numFormID
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFCD.numFieldID = DFM.numFieldID
      WHERE
      DFCD.numDomainID = v_numDomainID
      AND numAuthGroupID = v_numAuthGroupID
      AND DFCD.numFormID IN(SELECT numFormID FROm tt_TEMP)
      UNION
      SELECT
      DFCD.numFormID AS "numFormId"
			,DFCD.numFieldID AS "numFieldId"
			,CFW.FLd_label AS "vcFieldName"
			,CFW.fld_type AS "vcAssociatedControlType"
			,1 AS "bitCustomField"
			,CFW.numlistid AS "numlistid"
			,(CASE WHEN coalesce(CFW.numlistid,0) > 0 THEN 'LI' ELSE '' END) AS "vcListItemType"
			,'' AS "vcLookBackTableName"
			,CAST(DFCD.numFieldID AS VARCHAR(15)) || '_C' AS "vcDbColumnName"
			,CASE CLM.Loc_id WHEN 3 THEN 'CA' WHEN 5 THEN 'I' WHEN 9 THEN 'IA' WHEN 11 THEN 'P' ELSE CLM.vcFieldType END AS "vcFieldType"
			,'V' AS "vcFieldDataType"
      FROM
      DycFormConfigurationDetails DFCD
      INNER JOIN
      CFW_Fld_Master CFW
      ON
      DFCD.numFieldID = CFW.Fld_id
      INNER JOIN
      CFW_Loc_Master CLM
      ON
      CFW.Grp_id = CLM.Loc_id
      WHERE
      DFCD.numDomainID = v_numDomainID
      AND numAuthGroupID = v_numAuthGroupID
      AND DFCD.bitCustom = true
      AND DFCD.numFormID IN(SELECT numFormID FROm tt_TEMP)) TEMP
   ORDER BY
   TEMP."numFormId",TEMP."vcFieldName";
   RETURN;
END; $$;


