DROP FUNCTION IF EXISTS USP_GetVendors;
CREATE OR REPLACE FUNCTION USP_GetVendors(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemCode NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select Vendor.numVendorTCode,div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,
vcFirstName || ' ' || vcLastname as ConName,
numPhone || ', ' || numPhoneExtension as Phone,coalesce(monCost,0) AS monCost,vcItemName,coalesce(intMinQty,0) AS intMinQty
,numContactId, vcNotes,
(CASE WHEN(SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON numItemKitID = Item.numItemCode WHERE Item.bitAssembly = true AND numChildItemID = I.numItemCode) > 0 THEN 1 ELSE 0 END) AS bitUsedInAssembly,
coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) AS monCostEach
   from Vendor
   join DivisionMaster div
   on div.numDivisionID = numVendorID
   join CompanyInfo com
   on com.numCompanyId = div.numCompanyID
   left join AdditionalContactsInformation ADC
   on ADC.numDivisionId = div.numDivisionID
   join Item I
   on I.numItemCode = Vendor.numItemCode
   WHERE coalesce(ADC.bitPrimaryContact,false) = true and Vendor.numDomainID = v_numDomainID and Vendor.numItemCode = v_numItemCode;
END; $$;
