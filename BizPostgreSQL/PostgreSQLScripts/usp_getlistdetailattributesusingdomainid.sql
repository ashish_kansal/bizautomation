CREATE OR REPLACE FUNCTION USP_GetListDetailAttributesUsingDomainID(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		LD.numListItemID AS "numListItemID"
		,LD.numListID AS "numListID"
		,LD.vcData AS "vcData"
		,LD.numCreatedBY AS "numCreatedBY"
		,LD.bintCreatedDate AS "bintCreatedDate"
		,LD.numModifiedBy AS "numModifiedBy"
		,LD.bintModifiedDate AS "bintModifiedDate"
		,LD.bitDelete AS "bitDelete"
		,LD.numDomainID AS "numDomainID"
		,LD.constFlag AS "constFlag"
		,LD.sintOrder AS "sintOrder"
		,LD.numListType AS "numListType"
		,LD.tintOppOrOrder AS "tintOppOrOrder"
		,LD.numListItemGroupId AS "numListItemGroupId" 
		,CFM.Fld_id AS "Fld_id"
		,CFM.Fld_type AS "Fld_type"
		,CFM.Fld_label AS "Fld_label"
		,CFM.Fld_tab_ord AS "Fld_tab_ord"
		,CFM.Grp_id AS "Grp_id"
		,CFM.subgrp AS "subgrp"
		,CFM.numlistid AS "numlistid"
		,CFM.vcURL AS "vcURL"
		,CFM.vcToolTip AS "vcToolTip"
		,CFM.bitAutocomplete AS "bitAutocomplete"
		,CFM.vcItemsLocation AS "bitAutocomplete"
	FROM Listdetails LD
	JOIN CFW_Fld_Master CFM ON CFM.numlistid = LD.numListID
	WHERE Grp_id = 9 AND LD.numDomainid = v_numDomainID;
   RETURN;
END; $$;