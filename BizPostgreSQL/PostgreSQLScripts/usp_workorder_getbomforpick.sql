-- Stored procedure definition script USP_WorkOrder_GetBOMForPick for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_GetBOMForPick(v_numDomainID NUMERIC(18,0)
	,v_vcWorkOrders TEXT
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   WorkOrder.numWOId AS "numWOId"
		,WorkOrderDetails.numWODetailId AS "numWODetailId"
		,coalesce(WorkOrder.vcWorkOrderName,'-') AS "vcWorkOrderName"
		,CONCAT(Item.vcItemName,' (',WorkOrder.numQtyItemsReq,')') AS "vcAssembly"
		,(CASE
   WHEN OpportunityItems.ItemReleaseDate IS NOT NULL
   THEN FormatedDateFromDate(OpportunityItems.ItemReleaseDate,v_numDomainID)
   ELSE FormatedDateFromDate(WorkOrder.dtmEndDate,v_numDomainID)
   END) AS "vcRequestedDate"
		,ItemChild.numItemCode AS "numItemCode"
		,CONCAT(ItemChild.vcItemName,' (',WorkOrderDetails.numQtyItemsReq,')') AS "vcBOM"
		,WorkOrderDetails.numQtyItemsReq AS "numRequiredQty"
		,coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId = WorkOrderDetails.numWODetailId),
   0) AS "numPickedQty"
		,WareHouseItems.numWareHouseID AS "numWareHouseID"
		,WareHouseItems.numWareHouseItemID AS "numWareHouseItemID"
		,coalesce(Warehouses.vcWareHouse,'') AS "vcWarehouse"
		,coalesce(WareHouseItems.numAllocation,0) AS "numAllocation"
		,(CASE
   WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID = WL.numWLocationID WHERE WIInner.numDomainID = v_numDomainID AND WIInner.numItemID = ItemChild.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
   THEN CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":"',
         WL.vcLocation,'"}'),', ' ORDER BY WL.vcLocation)
        
         FROM
         WareHouseItems WIInner
         INNER JOIN
         WarehouseLocation WL
         ON
         WIInner.numWLocationID = WL.numWLocationID
         WHERE
         WIInner.numDomainID = v_numDomainID
         AND WIInner.numItemID = ItemChild.numItemCode
         AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),''),']')
   ELSE CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":""}'),', ')
         FROM
         WareHouseItems WIInner
         WHERE
         WIInner.numDomainID = v_numDomainID
         AND WIInner.numItemID = ItemChild.numItemCode
         AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),''),']')
   END)  AS "vcWarehouseLocations"
   FROM
   WorkOrder
   INNER JOIN
   Item
   ON
   WorkOrder.numItemCode = Item.numItemCode
   LEFT JOIN
   OpportunityItems
   ON
   WorkOrder.numOppId = OpportunityItems.numOppId
   AND WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
   INNER JOIN
   WorkOrderDetails
   ON
   WorkOrder.numWOId = WorkOrderDetails.numWOId
   INNER JOIN
   Item ItemChild
   ON
   WorkOrderDetails.numChildItemID = ItemChild.numItemCode
   INNER JOIN
   WareHouseItems
   ON
   WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId IN (SELECT Id FROM SplitIDs(v_vcWorkOrders,','))
   AND(WorkOrderDetails.numQtyItemsReq -coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId = WorkOrderDetails.numWODetailId),
   0)) > 0
   ORDER BY
   WorkOrder.numWOId;
END; $$;












