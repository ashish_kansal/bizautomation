-- Stored procedure definition script usp_GetAsItsType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAsItsType(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numListItemID as VARCHAR(255)), cast(vcData as VARCHAR(255))
   FROM Listdetails
   WHERE numListID = 5;
END; $$;












