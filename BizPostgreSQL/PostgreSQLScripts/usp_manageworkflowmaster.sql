DROP FUNCTION IF EXISTS USP_ManageWorkFlowMaster;

Create or replace FUNCTION USP_ManageWorkFlowMaster(INOUT v_numWFID NUMERIC(18,0) ,
    v_vcWFName VARCHAR(200),
    v_vcWFDescription VARCHAR(500),   
    v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
    v_bitActive BOOLEAN,
    v_numFormID NUMERIC(18,0),
    v_tintWFTriggerOn NUMERIC(18,0),
    v_vcWFAction TEXT,
    v_vcDateField TEXT,
    v_intDays INTEGER,
    v_intActionOn INTEGER,
	v_bitCustom BOOLEAN,
	v_numFieldID NUMERIC(18,0),
    v_strText XML DEFAULT '',
	v_vcWebsitePage TEXT DEFAULT '',
	v_intWebsiteDays INTEGER DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
	                                                                                                                                                             
   v_bitDiplicate  BOOLEAN DEFAULT false;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempWFID  NUMERIC(18,0);
BEGIN

BEGIN
	DROP TABLE IF EXISTS tt_TEMP CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP
	(
		tintActionType SMALLINT,
		numBizDocTypeID NUMERIC(18,0)
	);
    INSERT INTO tt_TEMP
	(
		tintActionType
		,numBizDocTypeID
	)
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
	XMLTABLE
	(
		'NewDataSet/WorkFlowConditionList'
        PASSING 
			v_strText 
		COLUMNS
			id FOR ORDINALITY,
			tintActionType SMALLINT PATH 'tintActionType',
			numBizDocTypeID NUMERIC PATH 'numBizDocTypeID'
	) AS Tabx;
  
      IF(SELECT COUNT(*) FROM tt_TEMP WHERE tintActionType = 6 AND (numBizDocTypeID = 287 OR numBizDocTypeID = 29397)) > 0 then
		
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
         IF(SELECT
         COUNT(*)
         FROM
         OpportunityAutomationRules
         WHERE
         numDomainID = v_numDomainID AND
					(numRuleID = 1 OR numRuleID = 2)) > 0 then
			
            RAISE EXCEPTION 'MASS SALES FULLFILLMENT RULE EXISTS';
            RETURN;
         end if;
      end if;
	

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
      IF EXISTS(SELECT numWFID FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numWFID <> v_numWFID AND numFormID = v_numFormID AND tintWFTriggerOn = v_tintWFTriggerOn AND(SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) =(SELECT COUNT(*) FROM 
         XMLTABLE('NewDataSet/WorkFlowConditionList'
         PASSING v_strText
				 COLUMNS
                    id FOR ORDINALITY,
                    numFieldID NUMERIC PATH 'numFieldID',
                    bitCustom BOOLEAN PATH 'bitCustom',	
                    vcFilterValue VARCHAR PATH 'vcFilterValue',
                    vcFilterOperator VARCHAR PATH 'vcFilterOperator',
				  	vcFilterANDOR VARCHAR PATH 'vcFilterANDOR',
				  	vcFilterData TEXT PATH 'vcFilterData'
				 ) AS Tabx)) then
	
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPWF_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPWF CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPWF
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numWFID NUMERIC(18,0)
         );
         INSERT INTO
         tt_TEMPWF(numWFID)
         SELECT
         numWFID
         FROM
         WorkFlowMaster
         WHERE
         numDomainID = v_numDomainID
         AND numWFID <> v_numWFID
         AND numFormID = v_numFormID
         AND tintWFTriggerOn = v_tintWFTriggerOn;
         select   COUNT(*) INTO v_iCount FROM tt_TEMPWF;
         WHILE v_i <= v_iCount LOOP
            select   numWFID INTO v_numTempWFID FROM tt_TEMPWF WHERE ID = v_i;
            IF NOT EXISTS(SELECT
            numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData
            FROM(SELECT
					numFieldID
					,bitCustom
					,vcFilterValue
					,vcFilterOperator
					,TabAl.a AS vcFilterData
				FROM
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,CAST('<NewDataSet><M>' || REPLACE(vcFilterData,',','</M><M>') || '</M></NewDataSet>' AS XML) AS vcFilterData 
					FROM 
						XMLTABLE('NewDataSet/WorkFlowConditionList'
						PASSING v_strText
						COLUMNS
							id FOR ORDINALITY,
							numFieldID NUMERIC PATH 'numFieldID',
							bitCustom BOOLEAN PATH 'bitCustom',	
							vcFilterValue VARCHAR PATH 'vcFilterValue',
							vcFilterOperator VARCHAR PATH 'vcFilterOperator',
							vcFilterANDOR VARCHAR PATH 'vcFilterANDOR',
							vcFilterData TEXT PATH 'vcFilterData'
						) AS Tabx
				) AS A
				CROSS JOIN LATERAL(select * from XMLTABLE('/M' PASSING vcFilterData COLUMNS a VARCHAR(100) PATH '.') AS Split) AS TabAl) T1
            EXCEPT
            SELECT
            numFieldId,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData
            FROM(SELECT
               numFieldId
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,TabAl.a AS vcFilterData
               FROM(SELECT
                  numFieldId
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST('<NewDataSet><M>' || REPLACE(vcFilterData,',','</M><M>') || '</M></NewDataSet>' AS XML) AS vcFilterData
                  FROM
                  WorkFlowConditionList
                  WHERE
                  numWFID = v_numTempWFID) AS A
               CROSS JOIN LATERAL(select * from XMLTABLE('/M'
                  PASSING vcFilterData
                  COLUMNS a VARCHAR(100) PATH '.') AS Split) AS TabAl) T2) then
               v_bitDiplicate := true; 
            ELSE 
               v_bitDiplicate := false;
            end if;
            IF NOT EXISTS(SELECT
            numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData
            FROM(SELECT
               numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,TabAl.a AS vcFilterData
               FROM(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,CAST('<NewDataSet><M>' || REPLACE(vcFilterData,',','</M><M>') || '</M></NewDataSet>' AS XML) AS vcFilterData
					FROM 
						XMLTABLE('NewDataSet/WorkFlowConditionList'
						PASSING v_strText
						COLUMNS
							id FOR ORDINALITY,
							numFieldID NUMERIC PATH 'numFieldID',
							bitCustom BOOLEAN PATH 'bitCustom',	
							vcFilterValue VARCHAR PATH 'vcFilterValue',
							vcFilterOperator VARCHAR PATH 'vcFilterOperator',
							vcFilterANDOR VARCHAR PATH 'vcFilterANDOR',
							vcFilterData TEXT PATH 'vcFilterData'
						) AS Tabx
			  ) AS A
               CROSS JOIN LATERAL(select * from XMLTABLE('/M'
                  PASSING vcFilterData
                  COLUMNS a VARCHAR(100) PATH '.') AS Split) AS TabAl) T1
            EXCEPT
            SELECT
            numFieldId,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData
            FROM(SELECT
               numFieldId
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,TabAl.a AS vcFilterData
               FROM(SELECT
                  numFieldId
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST('<NewDataSet><M>' || REPLACE(vcFilterData,',','</M><M>') || '</M></NewDataSet>' AS XML) AS vcFilterData
                  FROM
                  WorkFlowConditionList
                  WHERE
                  numWFID = v_numTempWFID) AS A
               CROSS JOIN LATERAL(select * from XMLTABLE('/M'
                  PASSING vcFilterData
                  COLUMNS a VARCHAR(100) PATH '.') AS Split) AS TabAl) T2) then
               v_bitDiplicate := true; 
            ELSE 
               v_bitDiplicate := false;
            end if;
            IF v_bitDiplicate = true then
			
               EXIT;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
	  --TODO: COMMENTED BECAUSE IF THERE IS OTHER WORKFLOW WITH SAME CONDITIONS BUT WITH SOME EXTRA CONDITION THEN IF THROWS ERROR
      --IF v_bitDiplicate = true then
	
      --   RAISE EXCEPTION 'DUPLICATE_WORKFLOW';
      --   RETURN;
      --end if;
      IF v_numWFID = 0 then
	
         INSERT INTO WorkFlowMaster(numDomainID, vcWFName, vcWFDescription, numCreatedBy, numModifiedBy, dtCreatedDate, dtModifiedDate, bitActive, numFormID, tintWFTriggerOn,vcWFAction,vcDateField,intDays,intActionOn,bitCustom,numFieldID,vcWebsitePage,intWebsiteDays)
         SELECT v_numDomainID, v_vcWFName, v_vcWFDescription, v_numUserCntID, v_numUserCntID, TIMEZONE('UTC',now()), TIMEZONE('UTC',now()), v_bitActive, v_numFormID, v_tintWFTriggerOn,v_vcWFAction,v_vcDateField,v_intDays,v_intActionOn,v_bitCustom,v_numFieldID,v_vcWebsitePage,v_intWebsiteDays
		 RETURNING numWFID INTO v_numWFID;
    
		--start of my
         IF LENGTH(CAST(v_strText AS TEXT)) > 2 then
		                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
            DELETE FROM WorkFlowTriggerFieldList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowTriggerFieldList(numWFID,numFieldID,bitCustom)
			SELECT 
				v_numWFID,numFieldID,bitCustom
			FROM 
				XMLTABLE('NewDataSet/WorkFlowTriggerFieldList'
				PASSING v_strText
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom'
				) AS Tabx;
            
	
	
			--Delete records into WorkFlowConditionList and Insert
            DELETE FROM WorkFlowConditionList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowConditionList(numWFID,numFieldId,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare)
			SELECT 
				v_numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare 
			FROM 
				XMLTABLE('NewDataSet/WorkFlowConditionList'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                numFieldID NUMERIC PATH 'numFieldID',
                bitCustom BOOLEAN PATH 'bitCustom',	
                vcFilterValue VARCHAR PATH 'vcFilterValue',
                vcFilterOperator VARCHAR PATH 'vcFilterOperator',
				vcFilterANDOR VARCHAR PATH 'vcFilterANDOR',
				vcFilterData TEXT PATH 'vcFilterData',
				intCompare INTEGER PATH 'intCompare') AS Tabx;


			--Delete records into WorkFlowActionUpdateFields and Insert
            DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID = v_numWFID);

			--Delete records into WorkFlowActionList and Insert
            DELETE FROM WorkFlowActionList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowActionList(numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject)
			SELECT 
				v_numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
			FROM 
				XMLTABLE('NewDataSet/WorkFlowActionList'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                tintActionType SMALLINT PATH 'tintActionType'
				,numTemplateID NUMERIC PATH 'numTemplateID'
				,vcEmailToType VARCHAR(50) PATH 'vcEmailToType'
				,tintTicklerActionAssignedTo SMALLINT PATH 'tintTicklerActionAssignedTo'
				,vcAlertMessage TEXT PATH 'vcAlertMessage'
				,tintActionOrder SMALLINT PATH 'tintActionOrder'
				,tintIsAttachment SMALLINT PATH 'tintIsAttachment'
				,vcApproval VARCHAR(100) PATH 'vcApproval'
				,tintSendMail SMALLINT PATH 'tintSendMail'
				,numBizDocTypeID NUMERIC(18,0) PATH 'numBizDocTypeID'
				,numBizDocTemplateID NUMERIC(18,0) PATH 'numBizDocTemplateID'
				,vcSMSText TEXT PATH 'vcSMSText'
				,vcEmailSendTo VARCHAR(500) PATH 'vcEmailSendTo'
				,vcMailBody TEXT PATH 'vcMailBody'
				,vcMailSubject VARCHAR(500) PATH 'vcMailSubject') AS Tabx;
           
            INSERT INTO WorkFlowActionUpdateFields(numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData)
			SELECT 
				WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
			FROM 
				XMLTABLE('NewDataSet/WorkFlowActionUpdateFields'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                numWFActionID NUMERIC PATH 'numWFActionID'
				,tintModuleType NUMERIC PATH 'tintModuleType'
				,numFieldID NUMERIC PATH 'numFieldID'
				,bitCustom BOOLEAN PATH 'bitCustom'
				,vcValue VARCHAR(500) PATH 'vcValue'
				,vcData VARCHAR(500) PATH 'vcData') AS WFAUF
            JOIN WorkFlowActionList WFA ON WFA.tintActionOrder = WFAUF.numWFActionID WHERE WFA.numWFID = v_numWFID;
         end if;
      ELSE
         UPDATE WorkFlowMaster
         SET    vcWFName = v_vcWFName,vcWFDescription = v_vcWFDescription,numModifiedBy = v_numUserCntID, 
         dtModifiedDate = TIMEZONE('UTC',now()),bitActive = v_bitActive, 
         numFormID = v_numFormID,tintWFTriggerOn = v_tintWFTriggerOn,
         vcWFAction = v_vcWFAction,vcDateField = v_vcDateField,intDays = v_intDays,
         intActionOn = v_intActionOn,bitCustom = v_bitCustom,numFieldID = v_numFieldID
		 ,vcWebsitePage=v_vcWebsitePage,intWebsiteDays=v_intWebsiteDays
         WHERE  numDomainID = v_numDomainID AND numWFID = v_numWFID;
         IF LENGTH(CAST(v_strText AS TEXT)) > 2 then
			--Delete records into WorkFlowTriggerFieldList and Insert
            DELETE FROM WorkFlowTriggerFieldList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowTriggerFieldList(numWFID,numFieldID,bitCustom)
			SELECT 
				v_numWFID,numFieldID,bitCustom
			FROM 
				XMLTABLE('NewDataSet/WorkFlowTriggerFieldList'
				PASSING v_strText
				COLUMNS
					id FOR ORDINALITY,
					numFieldID NUMERIC PATH 'numFieldID',
					bitCustom BOOLEAN PATH 'bitCustom'
				) AS Tabx;
 
			--Delete records into WorkFlowConditionList and Insert
            DELETE FROM WorkFlowConditionList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowConditionList(numWFID,numFieldId,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare)
			SELECT 
				v_numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare 
			FROM 
				XMLTABLE('NewDataSet/WorkFlowConditionList'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                numFieldID NUMERIC PATH 'numFieldID',
                bitCustom BOOLEAN PATH 'bitCustom',	
                vcFilterValue VARCHAR PATH 'vcFilterValue',
                vcFilterOperator VARCHAR PATH 'vcFilterOperator',
				vcFilterANDOR VARCHAR PATH 'vcFilterANDOR',
				vcFilterData TEXT PATH 'vcFilterData',
				intCompare INTEGER PATH 'intCompare') AS Tabx;

			--Delete records into WorkFlowActionUpdateFields and Insert
            DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID = v_numWFID);

			--Delete records into WorkFlowActionList and Insert
            DELETE FROM WorkFlowActionList WHERE  numWFID = v_numWFID;
            INSERT INTO WorkFlowActionList(numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject,tintEmailFrom)
			SELECT 
				v_numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject,(CASE WHEN tintEmailFrom='' THEN '0' ELSE tintEmailFrom END)::SMALLINT
			FROM 
				XMLTABLE('NewDataSet/WorkFlowActionList'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                tintActionType SMALLINT PATH 'tintActionType'
				,numTemplateID NUMERIC PATH 'numTemplateID'
				,vcEmailToType VARCHAR(50) PATH 'vcEmailToType'
				,tintEmailFrom VARCHAR PATH 'tintEmailFrom'
				,tintTicklerActionAssignedTo SMALLINT PATH 'tintTicklerActionAssignedTo'
				,vcAlertMessage TEXT PATH 'vcAlertMessage'
				,tintActionOrder SMALLINT PATH 'tintActionOrder'
				,tintIsAttachment SMALLINT PATH 'tintIsAttachment'
				,vcApproval VARCHAR(100) PATH 'vcApproval'
				,tintSendMail SMALLINT PATH 'tintSendMail'
				,numBizDocTypeID NUMERIC(18,0) PATH 'numBizDocTypeID'
				,numBizDocTemplateID NUMERIC(18,0) PATH 'numBizDocTemplateID'
				,vcSMSText TEXT PATH 'vcSMSText'
				,vcEmailSendTo VARCHAR(500) PATH 'vcEmailSendTo'
				,vcMailBody TEXT PATH 'vcMailBody'
				,vcMailSubject VARCHAR(500) PATH 'vcMailSubject') AS Tabx;
            
            INSERT INTO WorkFlowActionUpdateFields(numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData)
			SELECT 
				WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
			FROM 
				XMLTABLE('NewDataSet/WorkFlowActionUpdateFields'
			PASSING 
				v_strText
			COLUMNS
                id FOR ORDINALITY,
                numWFActionID NUMERIC PATH 'numWFActionID'
				,tintModuleType NUMERIC PATH 'tintModuleType'
				,numFieldID NUMERIC PATH 'numFieldID'
				,bitCustom BOOLEAN PATH 'bitCustom'
				,vcValue VARCHAR(500) PATH 'vcValue'
				,vcData VARCHAR(500) PATH 'vcData') AS WFAUF
            JOIN WorkFlowActionList WFA ON WFA.tintActionOrder = WFAUF.numWFActionID WHERE WFA.numWFID = v_numWFID;
         end if;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
END; $$;


