-- Stored procedure definition script usp_DemoteAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DemoteAccount(v_numDivisionID NUMERIC DEFAULT 0,      
 v_byteMode SMALLINT DEFAULT 0,
  v_numDomainID NUMERIC(9,0) DEFAULT 0,
  v_numUserCntID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCurrentCRMType  SMALLINT;
   v_numGRPID  NUMERIC;
BEGIN
   if v_byteMode = 0 then
      select   coalesce(numGrpId,0), tintCRMType INTO v_numGRPID,v_tintCurrentCRMType from DivisionMaster where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
      if v_numGRPID = 0 then
		
         UPDATE DivisionMaster SET numGrpId = 2,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
         WHERE numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID;
      end if;
      INSERT INTO DivisionMasterPromotionHistory(numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate)
		VALUES(v_numDomainID
			,v_numDivisionID
			,v_tintCurrentCRMType
			,0
			,v_numUserCntID
			,TIMEZONE('UTC',now()));
		 
      UPDATE DivisionMaster SET tintCRMType = 0,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numDivisionID = v_numDivisionID   and numDomainID = v_numDomainID;
   end if;     
   if v_byteMode = 1 then
	
      select   tintCRMType INTO v_tintCurrentCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      INSERT INTO DivisionMasterPromotionHistory(numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate)
		VALUES(v_numDomainID
			,v_numDivisionID
			,v_tintCurrentCRMType
			,1
			,v_numUserCntID
			,TIMEZONE('UTC',now()));
		 
      UPDATE DivisionMaster SET tintCRMType = 1,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numDivisionID = v_numDivisionID    and numDomainID = v_numDomainID;
   end if;
   if v_byteMode = 2 then
	
      select   tintCRMType INTO v_tintCurrentCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      INSERT INTO DivisionMasterPromotionHistory(numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate)
		VALUES(v_numDomainID
			,v_numDivisionID
			,v_tintCurrentCRMType
			,2
			,v_numUserCntID
			,TIMEZONE('UTC',now()));
		 
      UPDATE
      DivisionMaster
      SET
      tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID,
      numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE
      numDivisionID = v_numDivisionID
      AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


