-- Stored procedure definition script USP_InsertRecurringTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertRecurringTemplate(v_numRecurringId NUMERIC(9,0) DEFAULT 0,    
v_varRecurringTemplateName VARCHAR(50) DEFAULT NULL,      
v_chrIntervalType CHAR(1) DEFAULT NULL,      
v_tintIntervalDays SMALLINT DEFAULT NULL,      
v_tintMonthlyType SMALLINT DEFAULT NULL,      
v_tintFirstDet SMALLINT DEFAULT NULL,      
v_tintWeekDays SMALLINT DEFAULT NULL,      
v_tintMonths SMALLINT DEFAULT NULL,      
v_dtStartDate TIMESTAMP DEFAULT NULL,      
v_dtEndDate TIMESTAMP DEFAULT NULL,      
v_bitEndType BOOLEAN DEFAULT NULL,  
v_bitEndTransactionType BOOLEAN DEFAULT NULL,  
v_numNoTransaction NUMERIC(9,0) DEFAULT NULL,
v_numDomainId NUMERIC(9,0) DEFAULT NULL,
v_intBillingBreakup INTEGER DEFAULT 0,
v_vcBreakupPercentage VARCHAR(2000) DEFAULT NULL ,
v_bitBillingTerms BOOLEAN DEFAULT false,
v_numBillingDays NUMERIC(9,0) DEFAULT NULL,
v_bitBizDocBreakup BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_dtEndDate = 'Jan  1 1753 12:00AM' then 
      v_dtEndDate := null;
   end if;     
    
--Insert    
   if v_numRecurringId = 0 then

      Insert into RecurringTemplate(varRecurringTemplateName,chrIntervalType,tintIntervalDays,tintMonthlyType,tintFirstDet,tintWeekDays,tintMonths,dtStartDate,dtEndDate,bitEndType,bitEndTransactionType,numNoTransaction,numDomainID, intBillingBreakup,vcBreakupPercentage,bitBillingTerms,numBillingDays,bitBizDocBreakup)
Values(v_varRecurringTemplateName,v_chrIntervalType,v_tintIntervalDays,v_tintMonthlyType,v_tintFirstDet,v_tintWeekDays,v_tintMonths,v_dtStartDate,v_dtEndDate,v_bitEndType,v_bitEndTransactionType,v_numNoTransaction,v_numDomainId, v_intBillingBreakup,v_vcBreakupPercentage,v_bitBillingTerms,v_numBillingDays,v_bitBizDocBreakup);
   end if;    
    
---Update    
   if v_numRecurringId <> 0 then

      IF(SELECT COUNT(*) FROM OpportunityRecurring WHERE numRecurringId = v_numRecurringId) > 0 then

         RAISE NOTICE 'INUSE';
         RETURN;
      end if;
      Update RecurringTemplate Set varRecurringTemplateName = v_varRecurringTemplateName,chrIntervalType = v_chrIntervalType,
      tintIntervalDays = v_tintIntervalDays,tintMonthlyType = v_tintMonthlyType,
      tintFirstDet = v_tintFirstDet,tintWeekDays = v_tintWeekDays,
      tintMonths = v_tintMonths,dtStartDate = v_dtStartDate,dtEndDate = v_dtEndDate,
      bitEndType = v_bitEndType,bitEndTransactionType = v_bitEndTransactionType,
      numNoTransaction = v_numNoTransaction,numDomainID = v_numDomainId,intBillingBreakup = v_intBillingBreakup,
      vcBreakupPercentage = v_vcBreakupPercentage,
      bitBillingTerms = v_bitBillingTerms,numBillingDays = v_numBillingDays,
      bitBizDocBreakup = v_bitBizDocBreakup Where numRecurringId = v_numRecurringId;
   end if;
END; $$;
--Created By Anoop Jayaraj          
        
--exec USP_ItemDetailsForEcomm 173028,63,72


