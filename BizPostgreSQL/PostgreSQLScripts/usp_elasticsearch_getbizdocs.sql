DROP FUNCTION IF EXISTS USP_ElasticSearch_GetBizDocs;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetBizDocs(v_numDomainID NUMERIC(18,0)
	,v_vcOppBizDocIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numOppBizDocsId AS id
		,OpportunityMaster.numOppId AS "numOppId"
		,'bizdoc' AS module
		,'' AS url
		,coalesce(tintopptype,0) AS "tintOppType"
		,coalesce(tintoppstatus,0) AS "tintOppStatus"
		,coalesce(OpportunityMaster.numassignedto,0) AS "numAssignedTo"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,coalesce(OpportunityMaster.numrecowner,0) AS "numRecOwner"
		,CONCAT(CASE WHEN tintopptype = 2 THEN '<b style="color:#ff0000">' ELSE '<b style="color:#00aa50">' END,vcData,' (BizDoc):</b> ',vcBizDocID,', ',vcCompanyName,
   ', ',OpportunityBizDocs.monDealAmount) AS displaytext
		,vcBizDocID AS text
		,coalesce(vcBizDocID,'') AS "Search_vcBizDocID"
   FROM
   OpportunityBizDocs 
   INNER JOIN
   Listdetails 
   ON
   OpportunityBizDocs.numBizDocId = Listdetails.numListItemID
   AND Listdetails.numListID = 27
   INNER JOIN
   OpportunityMaster 
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   INNER JOIN
   DivisionMaster 
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo 
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND (NULLIF(v_vcOppBizDocIds,'') IS NULL OR numOppBizDocsId IN(SELECT id FROM SplitIDs(coalesce(v_vcOppBizDocIds,'0'),',')));
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/













