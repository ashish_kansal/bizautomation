-- Stored procedure definition script USP_ForecastDelete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ForecastDelete(v_tintForecastType SMALLINT DEFAULT null,  
v_numUserCntID NUMERIC(9,0) DEFAULT null,  
v_numItemCode NUMERIC(9,0) DEFAULT null,  
v_sintYear SMALLINT DEFAULT null,  
v_tintquarter SMALLINT DEFAULT null,  
v_numDomainId NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintForecastType = 1 then

      delete from Forecast where numCreatedBy = v_numUserCntID
      and sintYear = v_sintYear and tintQuarter = v_tintquarter
      and tintForecastType = v_tintForecastType
      and numDomainID = v_numDomainId;
   else
      delete from Forecast where numCreatedBy = v_numUserCntID
      and sintYear = v_sintYear and tintQuarter = v_tintquarter
      and tintForecastType = v_tintForecastType
      and numItemCode = v_numItemCode and numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;


