-- Function definition script GetDealTaxAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDealTaxAmount(v_numOppID NUMERIC(18,0),
	v_numOppBizDocsId NUMERIC(18,0) DEFAULT 0,
	v_numDomainId NUMERIC(18,0) DEFAULT NULL)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalTaxAmt  DECIMAL(20,5) DEFAULT 0;		
   v_numTaxItemID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   select   numTaxItemID INTO v_numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId = v_numOppID   ORDER BY numTaxItemID LIMIT 1; 
 
   WHILE v_numTaxItemID > -1 LOOP
      v_TotalTaxAmt := v_TotalTaxAmt+coalesce(fn_CalOppItemTotalTaxAmt(v_numDomainId,v_numTaxItemID,v_numOppID,v_numOppBizDocsId),0);
      select   numTaxItemID INTO v_numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId = v_numOppID AND numTaxItemID > v_numTaxItemID   ORDER BY numTaxItemID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numTaxItemID := -1;
      end if;
   END LOOP;   
	    
   RETURN CAST(coalesce(v_TotalTaxAmt,0) AS DECIMAL(20,5)); -- Set Accuracy of Two precision
END; $$;

