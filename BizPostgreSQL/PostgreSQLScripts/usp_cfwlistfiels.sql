-- Stored procedure definition script usp_cfwListFiels for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_cfwListFiels(v_TabId INTEGER DEFAULT null,  
v_loc_id SMALLINT DEFAULT null,
v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_TabId = -1 then

      open SWV_RefCur for
      Select Fld_id,FLd_label from CFW_Fld_Master
      where Grp_id = v_loc_id  and numDomainID = v_numDomainID
      order by Fld_tab_ord;
   end if;  
  
   if v_TabId != -1 then

      open SWV_RefCur for
      Select Fld_id,FLd_label from CFW_Fld_Master
      where Grp_id = v_loc_id  and subgrp = CAST(v_TabId AS VARCHAR)  and numDomainID = v_numDomainID
      order by Fld_tab_ord;
   end if;
   RETURN;
END; $$;


