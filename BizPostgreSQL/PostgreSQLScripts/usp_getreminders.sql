-- Stored procedure definition script USP_GetReminders for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReminders(v_OrganizerName VARCHAR(64), -- resource name (will be used to lookup ID)  
v_StartDateTimeUtc TIMESTAMP, -- the start date before which no activities are retrieved  
v_EndDateTimeUtc TIMESTAMP, -- the end date, any activities starting after this date are excluded  
INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
   AS $$
   DECLARE
   v_numDomaindID  NUMERIC(18,0) DEFAULT 0;
   v_numUserCntID  NUMERIC(18,0) DEFAULT 0;
   v_ResourceID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_OrganizerName) = true then
	
      v_ResourceID := CAST(v_OrganizerName AS NUMERIC(9,0));
      select   numDomainId, numUserCntId INTO v_numDomaindID,v_numUserCntID FROM Resource WHERE coalesce(ResourceID,0) = v_ResourceID;
   else
      v_ResourceID := -999;
   end if; 

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ActivityID INTEGER,
      numType INTEGER, --1-Calendar 2-Action Item
      RecurrenceID NUMERIC(18,0),
      Subject VARCHAR(500),
      Location VARCHAR(500),
      EnableReminder BOOLEAN,
      AllDayEvent BOOLEAN,
      StartDateTimeUtc TIMESTAMP,
      VarianceID UUID,
      LastSnoozDateTimeUtc TIMESTAMP,
      ReminderInterval INTEGER,
      Organization VARCHAR(500)
   );


	--FETCH CALENDAR REMINDER
   INSERT INTO
   tt_TEMP
   SELECT
   Activity.ActivityID,
		1,
		Activity.RecurrenceID,
		CAST(Activity.Subject AS VARCHAR(500)),
		CAST(Activity.Location AS VARCHAR(500)),
		Activity.EnableReminder,
	    Activity.AllDayEvent,
		Activity.StartDateTimeUtc,
		Activity.VarianceID,
		coalesce(LastSnoozDateTimeUtc,'1900-01-01 00:00:00.000') AS LastSnoozDateTimeUtc,
		Activity.ReminderInterval,
		CAST('' AS VARCHAR(500))
   FROM
   Activity
   INNER JOIN
   ActivityResource
   ON
   Activity.ActivityID = ActivityResource.ActivityID
   WHERE
   coalesce(Activity.EnableReminder,false) = true
   AND (Activity.ActivityID = ActivityResource.ActivityID AND ActivityResource.ResourceID = v_ResourceID)
   AND (((Activity.StartDateTimeUtc >= v_StartDateTimeUtc AND Activity.StartDateTimeUtc < v_EndDateTimeUtc) OR (Activity.RecurrenceID <> -999 AND CAST(Activity.StartDateTimeUtc AS TIME) >= CAST(v_StartDateTimeUtc AS TIME))) AND Activity.OriginalStartDateTimeUtc IS NULL);  
		

	--FETCH ACTION ITEMS
   INSERT INTO
   tt_TEMP
   SELECT
   Communication.numCommId,
		2,
		0,
		CAST('' AS VARCHAR(500)),
		coalesce(Listdetails.vcData,''),
		CAST(1 AS BOOLEAN),
		bitFollowUpAnyTime,
		Communication.dtStartTime,
		NULL,
		coalesce(LastSnoozDateTimeUtc,'1900-01-01 00:00:00.000') AS LastSnoozDateTimeUtc,
		coalesce(Communication.intRemainderMins,0)*60 AS ReminderInterval,
		CompanyInfo.vcCompanyName
   FROM
   Communication
   INNER JOIN
   DivisionMaster
   ON
   Communication.numDivisionID = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN
   Listdetails
   ON
   Listdetails.numListItemID = Communication.bitTask
   WHERE
   Communication.numDomainID = v_numDomaindID
   AND numAssign = v_numUserCntID
   AND coalesce(tintRemStatus,0) > 0
   AND bitClosedFlag = false
   AND coalesce(intRemainderMins,0) <> 0
   AND CAST(Communication.dtStartTime AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE)
   AND Communication.dtStartTime > v_StartDateTimeUtc;

	
open SWV_RefCur for SELECT * FROM tt_TEMP ORDER BY StartDateTimeUtc ASC;
END; $$;

