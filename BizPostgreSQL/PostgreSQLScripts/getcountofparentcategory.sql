-- Stored procedure definition script GetCountOfParentCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCountOfParentCategory(v_numParntAcntId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN; 
--Select Count(numParntAcntId) From Chart_Of_Accounts Where numParntAcntId=@numParntAcntId
END; $$;


