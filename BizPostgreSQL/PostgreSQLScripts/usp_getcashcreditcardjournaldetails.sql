-- Stored procedure definition script USP_GetCashCreditCardJournalDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCashCreditCardJournalDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                
v_numJournalId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJD.numTransactionId as TransactionId, CAST(GJD.numChartAcntId AS VARCHAR(10)) || '~' || CAST(CA.numAcntTypeId AS VARCHAR(10)) as numChartAcntId,
 CA.numAcntTypeId as numAcntType,
 case when GJD.numDebitAmt  <> 0 or GJD.numDebitAmt IS NOT NULL then GJD.numDebitAmt   else GJD.numCreditAmt end as numDebitAmt,
 GJD.varDescription as  varDescription,GJD.numCustomerId as numCustomerId,GJD.numJournalId,
 fn_GetRelationship_DivisionId(CI.numCompanyType,CI.numDomainID) || ' - ' || CAST(CI.numCompanyId AS VARCHAR(400)) as varRelation
   From General_Journal_Header GJH
   Inner join General_Journal_Details GJD  on GJH.numJOurnal_Id = GJD.numJournalId
   inner join Chart_Of_Accounts CA on  GJD.numChartAcntId = CA.numAccountId
   Left outer join CompanyInfo CI on GJD.numCustomerId = CI.numCompanyId
   Where GJD.bitMainCashCredit = false And GJH.numDomainId = v_numDomainId And GJH.numJOurnal_Id = v_numJournalId;
END; $$;












