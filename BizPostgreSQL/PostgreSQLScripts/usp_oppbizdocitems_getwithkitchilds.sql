DROP FUNCTION IF EXISTS USP_OPPBizDocItems_GetWithKitChilds;

CREATE OR REPLACE FUNCTION USP_OPPBizDocItems_GetWithKitChilds(v_numOppId NUMERIC(9,0) DEFAULT NULL,
    v_numOppBizDocsId NUMERIC(9,0) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);      
   v_tintTaxOperator  SMALLINT;
   v_tintCommitAllocation  SMALLINT;
   v_strSQL  TEXT;                                                                                          
   v_strSQL1  TEXT;                                       
   v_strSQLCusFields  TEXT DEFAULT '';
   v_strSQLEmpFlds  TEXT DEFAULT '';                                                                         
                                                                                              
   v_tintType  SMALLINT;   
   v_tintOppType  SMALLINT;                                                      
   v_intRowNum  INTEGER;                                                  
   v_numFldID  VARCHAR(15);                                          
   v_vcFldname  VARCHAR(200);                                            
   v_numBizDocId  NUMERIC(9,0);
   v_numBizDocTempID  NUMERIC(9,0) DEFAULT 0;
   v_bitDisplayKitChild  BOOLEAN DEFAULT false;
        
                                           
   v_strSQLUpdate  TEXT;
   
   v_vcTaxName  VARCHAR(200);
   v_numTaxItemID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(100);
   SWV_ExecDyn  VARCHAR(5000);
   SWV_RowCount INTEGER;
BEGIN
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
                                                                                                                                                                                                                                                                                                                                          
   select   numDivisionId, tintTaxOperator INTO v_DivisionID,v_tintTaxOperator FROM
   OpportunityMaster WHERE
   numOppId = v_numOppId;                                                                                                                                                                          

    
                                                                                          
   select   coalesce(numBizDocTempID,0), numBizDocId INTO v_numBizDocTempID,v_numBizDocId FROM
   OpportunityBizDocs WHERE
   numOppBizDocsId = v_numOppBizDocsId;

	
   select   coalesce(bitDisplayKitChild,false) INTO v_bitDisplayKitChild FROM
   BizDocTemplate WHERE
   numDomainID = v_numDomainID
   AND numBizDocTempID = v_numBizDocTempID;


   select   tintopptype, tintopptype INTO v_tintType,v_tintOppType FROM
   OpportunityMaster WHERE
   numOppId = v_numOppId;                                                                                          
                                                                                   
   IF v_tintType = 1 then
      v_tintType := 7;
   ELSE
      v_tintType := 8;
   end if;     
		
		
   IF(SELECT
   COUNT(*)
   FROM
   View_DynamicColumns
   WHERE
   numFormId = v_tintType
   AND numDomainID = v_numDomainID
   AND numAuthGroupId = v_numBizDocId
   AND vcFieldType = 'R'
   AND coalesce(numRelCntType,0) = v_numBizDocTempID) = 0 then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
      SELECT
      numFormId,numFieldID,1,tintRow,v_numDomainID,v_numBizDocId,v_numBizDocTempID
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_tintType and bitDefault = true and numDomainID = v_numDomainID order by tintRow ASC;
      IF(SELECT
      COUNT(*)
      FROM
      DycFrmConfigBizDocsSumm Ds
      WHERE
      numBizDocID = v_numBizDocId
      AND Ds.numFormID = v_tintType
      AND coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID
      AND Ds.numDomainID = v_numDomainID) = 0 then
		
         INSERT INTO DycFrmConfigBizDocsSumm(numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
         SELECT
         v_tintType,v_numBizDocId,DFM.numFieldID,v_numDomainID,v_numBizDocTempID
         FROM
         DycFormField_Mapping DFFM
         JOIN
         DycFieldMaster DFM
         ON
         DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
         WHERE
         numFormID = 16
         AND DFFM.numDomainID IS NULL;
      end if;
   end if;      

   DROP TABLE IF EXISTS tt_TEMPBIZDOCPRODUCTGRIDCOLUMNS CASCADE;
   DROP TABLE IF EXISTS tt_TEMPBIZDOCPRODUCTGRIDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPBIZDOCPRODUCTGRIDCOLUMNS AS
      SELECT * FROM(SELECT
      vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_tintType
      AND numDomainID = v_numDomainID
      AND numAuthGroupId = v_numBizDocId
      AND vcFieldType = 'R'
      AND coalesce(numRelCntType,0) = v_numBizDocTempID
      UNION
      SELECT
      vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' AS vcFieldDataType,
        tintRow AS intRowNum
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND coalesce(numRelCntType,0) = v_numBizDocTempID) t1
      ORDER BY
      intRowNum;                                                                              
                                                                                
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS
      SELECT * FROM(SELECT
      Opp.vcItemName AS vcItemName,
            coalesce(OBD.numOppBizDocItemID,0) AS OppBizDocItemID,
			(CASE
      WHEN OM.tintopptype = 1 AND OM.tintoppstatus = 1
      THEN(CASE
         WHEN (I.charItemType = 'P' AND coalesce(Opp.bitDropShip,false) = false AND coalesce(I.bitAsset,false) = false)
         THEN
            CAST(CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,
            (CASE WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true THEN false  WHEN coalesce(bitKitParent,false) = true THEN true ELSE false END)) AS VARCHAR(30))
         ELSE ''
         END)
      ELSE ''
      END) AS vcBackordered,
			coalesce(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
      WHEN charitemType = 'S' THEN 'Service'
      END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
      WHEN charitemType = 'N' THEN 'Non-Inventory'
      WHEN charitemType = 'S' THEN 'Service'
      END AS vcItemType,
			OBD.vcItemDesc AS txtItemDesc,
			Opp.numUnitHour AS numOrgTotalUnitHour,
			fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(Opp.numUOMId,0))*Opp.numUnitHour AS numTotalUnitHour,
            fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(Opp.numUOMId,0))*OBD.numUnitHour AS numUnitHour,
            CAST((fn_UOMConversion(coalesce(Opp.numUOMId,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0))*OBD.monPrice) AS DECIMAL(20,5)) AS monPrice,
            CAST(OBD.monTotAmount AS DECIMAL(20,5)) AS Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            coalesce(I.monListPrice,0) AS monListPrice,
            I.numItemCode AS ItemCode,
            Opp.numoppitemtCode,
            L.vcData AS numItemClassification,
            CASE WHEN bitTaxable = false THEN 'No'
      ELSE 'Yes'
      END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            coalesce(GJH.numJOurnal_Id,0) AS numJournalId,
            (SELECT 
         coalesce(GJD.numTransactionId,0)
         FROM      General_Journal_Details GJD
         WHERE     GJH.numJOurnal_Id = GJD.numJournalId
         AND GJD.chBizDocItems = 'NI'
         AND GJD.numoppitemtCode = Opp.numoppitemtCode LIMIT 1) AS numTransactionId,
            coalesce(Opp.vcModelID,'') AS vcModelID,
            (CASE WHEN charitemType = 'P' THEN USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE coalesce(OBD.vcAttributes,'') END) AS vcAttributes,
            coalesce(vcPartNo,'') AS vcPartNo,
           (CASE WHEN coalesce(Opp.bitMarkupDiscount,false) = true
      THEN 0
      ELSE(coalesce(OBD.monTotAmtBefDiscount,OBD.monTotAmount) -OBD.monTotAmount)
      END)
      AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CAST((fn_UOMConversion(coalesce(Opp.numUOMId,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0))*(coalesce(OBD.monTotAmount,0)/OBD.numUnitHour)) AS DECIMAL(20,5)) END) As monUnitSalePrice,
            coalesce(OBD.vcNotes,coalesce(Opp.vcNotes,'')) AS vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            FormatedDateFromDate(OBD.dtDeliveryDate,v_numDomainID) AS dtDeliveryDate,
            Opp.numWarehouseItmsID,
            coalesce(u.vcUnitName,'') AS numUOMId,
            coalesce(Opp.vcManufacturer,'') AS vcManufacturer,
            coalesce(V.monCost,0) AS VendorCost,
            Opp.vcPathForTImage,
            I.fltLength,
            I.fltWidth,
            I.fltHeight,
            I.fltWeight,
            coalesce(I.numVendorID,0) AS numVendorID,
            Opp.bitDropShip AS DropShip,
            COALESCE((SELECT string_agg(vcSerialNo || (CASE 
														WHEN coalesce(I.bitLotNo,false) = true
														THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')'
														ELSE ''
													END),' ,' ORDER BY vcSerialNo)
         FROM    OppWarehouseSerializedItem oppI
         JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
         WHERE   oppI.numOppID = Opp.numOppId
         AND oppI.numOppItemID = Opp.numoppitemtCode
         ),'') AS SerialLotNo,
            coalesce(Opp.numUOMId,0) AS numUOM,
            coalesce(u.vcUnitName,'') AS vcUOMName,
            fn_UOMConversion(Opp.numUOMId,Opp.numItemCode,v_numDomainID,NULL::NUMERIC) AS UOMConversionFactor,
            coalesce(Opp.numSOVendorId,0) AS numSOVendorId,
            FormatedDateTimeFromDate(OBD.dtRentalStartDate,v_numDomainID) AS dtRentalStartDate,
            FormatedDateTimeFromDate(OBD.dtRentalReturnDate,v_numDomainID) AS dtRentalReturnDate,
            coalesce(W.vcWareHouse,'') AS Warehouse,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END AS vcSKU,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS vcBarcode,
			CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS numBarCodeId,
            coalesce(Opp.numClassID,0) AS numClassID,
            coalesce(Opp.numProjectID,0) AS numProjectID,
            coalesce(I.numShipClass,0) AS numShipClass,OBD.numOppBizDocID
			,(CASE
      WHEN OB.numBizDocId IN(29397)
      THEN  COALESCE((SELECT
            string_agg(WLInner.vcLocation || ' (' || coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0) || ')',',')
            FROM
            WareHouseItems WIInner
            INNER JOIN
            WarehouseLocation WLInner
            ON
            WIInner.numWLocationID = WLInner.numWLocationID
            WHERE
            WIInner.numItemID = Opp.numItemCode
            AND WIInner.numWareHouseID = WI.numWareHouseID
            AND(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)) > 0),'')
      ELSE WL.vcLocation
      END) AS vcLocation
			,coalesce(bitSerialized,false) AS bitSerialized,coalesce(bitLotNo,false) AS bitLotNo,
            Opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,coalesce(I.numItemCode,0) AS numItemCode,
			coalesce(I.bitKitParent,false) AS bitKitParent,
			0 AS numOppChildItemID,
			0 AS numOppKitChildItemID,
			false AS bitChildItem,
			0 AS tintLevel,
			coalesce(Opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) AS vcWarehouse,
			(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPBIZDOCPRODUCTGRIDCOLUMNS WHERE vcDbColumnName = 'vcInclusionDetails') > 0 THEN GetOrderAssemblyKitInclusionDetails(Opp.numOppId,Opp.numoppitemtCode,OBD.numUnitHour,v_tintCommitAllocation,true) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(coalesce(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost,
			coalesce((SELECT SUM(numOnHand) FROM WareHouseItems WHERE WareHouseItems.numDomainID = v_numDomainID AND WareHouseItems.numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = W.numWareHouseID),0) AS numOnHand
			,FormatedDateFromDate(Opp.ItemReleaseDate,v_numDomainID) AS dtReleaseDate
			,(CASE WHEN I.bitKitParent = true THEN coalesce(fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),
         0) ELSE(coalesce(numOnHand,0)+coalesce(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,coalesce(Opp.bitWinningBid,false) AS bitWinningBid
			,coalesce(Opp.numParentOppItemID,0) AS numParentOppItemID
			,coalesce(Opp.numUnitHour,0) -coalesce(TEMPBizDocQty.numBizDocQty,0) AS numRemainingQty
			,FormatedDateFromDate(Opp.ItemRequiredDate,v_numDomainID) AS ItemRequiredDate
			,OB.dtFromDate AS dtFromDate
      FROM
      OpportunityItems Opp
      JOIN
      OpportunityBizDocItems OBD
      ON
      OBD.numOppItemID = Opp.numoppitemtCode
      JOIN
      OpportunityBizDocs OB
      ON
      OBD.numOppBizDocID = OB.numOppBizDocsId
      LEFT JOIN
      Item I
      ON
      Opp.numItemCode = I.numItemCode
      LEFT JOIN
      Listdetails L
      ON
      I.numItemClassification = L.numListItemID
      LEFT JOIN
      Vendor V
      ON
      V.numVendorID = v_DivisionID
      AND V.numItemCode = Opp.numItemCode
      LEFT JOIN
      General_Journal_Header GJH
      ON
      Opp.numOppId = GJH.numOppId
      AND GJH.numBizDocsPaymentDetId IS NULL
      AND GJH.numOppBizDocsId = v_numOppBizDocsId
      LEFT JOIN
      UOM u
      ON
      u.numUOMId = Opp.numUOMId
      LEFT JOIN
      WareHouseItems WI
      ON
      Opp.numWarehouseItmsID = WI.numWareHouseItemID
      AND WI.numDomainID = v_numDomainID
      LEFT JOIN
      Warehouses W
      ON
      WI.numDomainID = W.numDomainID
      AND WI.numWareHouseID = W.numWareHouseID
      LEFT JOIN
      WarehouseLocation WL
      ON
      WL.numWLocationID = WI.numWLocationID
      LEFT JOIN
      OpportunityMaster OM
      ON
      OM.numOppId = Opp.numOppId
      LEFT JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      LEFT JOIN
      CustomerPartNumber CPN
      ON
      Opp.numItemCode = CPN.numItemCode AND CPN.numDomainID = v_numDomainID AND CPN.numCompanyId = DM.numCompanyID
      LEFT JOIN LATERAL(SELECT
         SUM(OBDIInner.numUnitHour) AS numBizDocQty
         FROM
         OpportunityBizDocs OBDInner
         INNER JOIN
         OpportunityBizDocItems OBDIInner
         ON
         OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
         WHERE
         OBDInner.numoppid = v_numOppId
         AND OBDInner.numBizDocId = OB.numBizDocId
         AND OBDIInner.numOppItemID = OBD.numOppItemID) TEMPBizDocQty on TRUE
      WHERE
      Opp.numOppId = v_numOppId
      AND (bitShowDeptItem IS NULL OR bitShowDeptItem = false)
      AND OBD.numOppBizDocID = v_numOppBizDocsId) X
      ORDER BY
      numSortOrder;

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
   IF (v_bitDisplayKitChild = true AND(SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = v_numOppId) > 0) then
	

		--FIRST LEVEL CHILD
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         numOppItemCode NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         numOppKitChildItemID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numWareHouseItemID NUMERIC(18,0),
         numTotalQty DOUBLE PRECISION,
         numQty DOUBLE PRECISION,
         numUOMID NUMERIC(18,0),
         numQtyShipped DOUBLE PRECISION,
         tintLevel SMALLINT,
         numSortOrder INTEGER
      );
      INSERT INTO tt_TEMPTABLE(numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWareHouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder)
      SELECT
      OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			coalesce(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
			CAST((t1.numUnitHour*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
			coalesce(OKI.numUOMId,0),
			coalesce(numQtyShipped,0)
			,1
			,t1.numSortOrder
      FROM
      tt_TEMP1 t1
      INNER JOIN
      OpportunityKitItems OKI
      ON
      t1.numoppitemtCode = OKI.numOppItemID
      WHERE
      t1.bitKitParent = true;

		--SECOND LEVEL CHILD
      INSERT INTO tt_TEMPTABLE(numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWareHouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder)
      SELECT
      OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			coalesce(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty*OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			coalesce(OKCI.numUOMId,0),
			coalesce(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      tt_TEMPTABLE c
      ON
      OKCI.numOppId = v_numOppId
      AND OKCI.numOppItemID = c.numOppItemCode
      AND OKCI.numOppChildItemID = c.numOppChildItemID;
      INSERT INTO
      tt_TEMP1
      SELECT(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 AS OppBizDocItemID
			,(CASE WHEN(t2.numTotalQty -coalesce(t2.numQtyShipped,0)) > coalesce(WI.numAllocation,0) THEN((t2.numTotalQty -coalesce(t2.numQtyShipped,0)) -coalesce(WI.numAllocation,0))*fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(t2.numUOMID,0))  ELSE 0 END) AS vcBackordered
			,coalesce(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charItemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
      WHEN charitemType = 'N' THEN 'Non-Inventory'
      WHEN charitemType = 'S' THEN 'Service'
      END) AS vcItemType,
			CAST(I.txtItemDesc AS VARCHAR(2500)) AS txtItemDesc,
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (coalesce(fn_UOMConversion(coalesce(t2.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)*(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)) AS monPrice,
            CAST(((t2.numQty*coalesce(fn_UOMConversion(coalesce(t2.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1))*(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)) AS DECIMAL(20,5)) AS Amount,
            ((t2.numQty*coalesce(fn_UOMConversion(coalesce(t2.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1))*(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            coalesce((CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END),0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcData AS numItemClassification,
            CASE WHEN bitTaxable = false THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            coalesce(I.vcModelID,'') AS vcModelID,
            USP_GetAttributes(t2.numWareHouseItemID,bitSerialized) AS vcAttributes,
            coalesce(vcPartNo,'') AS vcPartNo,
            0 AS DiscAmt,
			(coalesce(fn_UOMConversion(coalesce(t2.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)*(CASE WHEN I.charItemType = 'P' THEN coalesce(WI.monWListPrice,0) ELSE coalesce(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWareHouseItemID,
            coalesce(u.vcUnitName,'') AS numUOMId,coalesce(I.vcManufacturer,'') AS vcManufacturer,coalesce(V.monCost,0) AS VendorCost,
            coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),'') AS vcPathForTImage,
            I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,coalesce(I.numVendorID,0) AS numVendorID,false AS DropShip,
            '' AS SerialLotNo,
            coalesce(t2.numUOMID,0) AS numUOM,
            coalesce(u.vcUnitName,'') AS vcUOMName,
            fn_UOMConversion(t2.numUOMID,t2.numItemCode,v_numDomainID,NULL::NUMERIC) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL AS dtRentalReturnDate,
            coalesce(W.vcWareHouse,'') AS Warehouse,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END AS vcSKU,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS vcBarcode,
			CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS numBarCodeId,
            coalesce(I.numItemClass,0) AS numClassID,
            0 AS numProjectID,
            coalesce(I.numShipClass,0) AS numShipClass,
			v_numOppBizDocsId AS numOppBizDocID,WL.vcLocation,coalesce(bitSerialized,false) AS bitSerialized,coalesce(bitLotNo,false) AS bitLotNo,
            v_numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			coalesce(I.numItemCode,0) AS numItemCode,
			coalesce(I.bitKitParent,false) AS bitKitParent,
			coalesce(t2.numOppChildItemID,0) AS numOppChildItemID,
			coalesce(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			true AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) AS vcWarehouse,
			'' AS vcInclusionDetails
			,0
			,0
			,coalesce((SELECT SUM(numOnHand) FROM WareHouseItems WHERE WareHouseItems.numDomainID = v_numDomainID AND WareHouseItems.numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = W.numWareHouseID),0) AS numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = true THEN coalesce(fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),
         0) ELSE(coalesce(numOnHand,0)+coalesce(numAllocation,0)) END) AS numAvailable
			,''
			,false
			,0
			,0
			,NULL
      FROM
      tt_TEMPTABLE t2
      INNER JOIN
      Item I
      ON
      t2.numItemCode = I.numItemCode
      LEFT JOIN
      WareHouseItems WI
      ON
      WI.numWareHouseItemID = t2.numWareHouseItemID
      LEFT JOIN
      Warehouses W
      ON
      WI.numDomainID = W.numDomainID
      AND WI.numWareHouseID = W.numWareHouseID
      LEFT JOIN
      WarehouseLocation WL
      ON
      WL.numWLocationID = WI.numWLocationID
      LEFT JOIN
      Listdetails L
      ON
      I.numItemClassification = L.numListItemID
      LEFT JOIN
      Vendor V
      ON
      V.numVendorID = v_DivisionID
      AND V.numItemCode = t2.numItemCode
      LEFT JOIN
      UOM u
      ON
      u.numUOMId = t2.numUOMID;
   end if;


   v_strSQLUpdate := '';
   
   EXECUTE 'ALTER TABLE tt_TEMP1 add TotalTax DECIMAL(20,5)';

   IF v_tintTaxOperator = 1 then --add Sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax =(CASE WHEN coalesce(bitChildItem,false) = false THEN fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,1::SMALLINT,Amount,numUnitHour) ELSE 0 END)';
   ELSEIF v_tintTaxOperator = 2
   then -- remove sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',TotalTax = 0';
   ELSE
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax =(CASE WHEN coalesce(bitChildItem,false) = false THEN fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,1::SMALLINT,Amount,numUnitHour) ELSE 0 END)';
   end if;

	-- CRV TAX TYPE
   EXECUTE 'ALTER TABLE tt_TEMP1 add CRV DECIMAL(20,5)';

   IF v_tintTaxOperator = 2 then
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ', CRV =(CASE WHEN coalesce(bitChildItem,false) = false THEN fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,1::SMALLINT,Amount,numUnitHour) ELSE 0 END)';
   ELSE
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
   end if;

   v_numTaxItemID := 0;
    
   select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM
   TaxItems WHERE
   numDomainID = v_numDomainID    LIMIT 1;

   WHILE v_numTaxItemID > 0 LOOP
      SWV_ExecDyn := 'ALTER TABLE tt_TEMP1 add "' || coalesce(v_vcTaxName,'') || '" DECIMAL(20,5)';
      EXECUTE SWV_ExecDyn;
      IF v_tintTaxOperator = 2 then -- remove sales tax
		
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', "' || coalesce(v_vcTaxName,'') || '" = 0';
      ELSE
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',"' || coalesce(v_vcTaxName,'')
         || '" =(CASE WHEN coalesce(bitChildItem,false) = false THEN fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ','
         || SUBSTR(CAST(v_numTaxItemID AS VARCHAR(20)),1,20) || ','
         || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
         'numoppitemtCode,1::SMALLINT,Amount,numUnitHour) ELSE 0 END)';
      end if;
      select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM
      TaxItems WHERE
      numDomainID = v_numDomainID
      AND numTaxItemID > v_numTaxItemID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numTaxItemID := 0;
      end if;
   END LOOP;

   IF v_strSQLUpdate <> '' then
    
      v_strSQLUpdate := 'UPDATE tt_TEMP1 SET ItemCode = ItemCode' || coalesce(v_strSQLUpdate,'') || ' WHERE ItemCode > 0 AND bitChildItem = false';
      EXECUTE v_strSQLUpdate;
   end if;

   select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
   AND Grp_id = 5
   AND numDomainID = v_numDomainID
   AND numAuthGroupID = v_numBizDocId
   AND bitCustom = true
   AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;     
	                                                                                     
   WHILE v_intRowNum > 0 LOOP
      SWV_ExecDyn := 'ALTER TABLE tt_TEMP1 add "' || coalesce(v_vcDbColumnName,'') || '" TEXT';
      EXECUTE SWV_ExecDyn;
      v_strSQLUpdate := 'UPDATE tt_TEMP1 set ItemCode = ItemCode,"' || coalesce(v_vcDbColumnName,'') || '" = GetCustFldValueOppItems(' || coalesce(v_numFldID,'')  || ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem = false';
      EXECUTE v_strSQLUpdate;
      select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM
      View_DynamicCustomColumns WHERE
      numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND tintRow >= v_intRowNum
      AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY
      tintRow LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_intRowNum := 0;
      end if;
   END LOOP;

   open SWV_RefCur for
   SELECT
   ROW_NUMBER() OVER(ORDER BY numSortOrder) AS RowNumber,
        * FROM
   tt_TEMP1
   ORDER BY
   numSortOrder ASC,tintLevel ASC,numoppitemtCode ASC,numOppChildItemID ASC, 
   numOppKitChildItemID ASC;

    

   open SWV_RefCur2 for
   SELECT
   coalesce(tintoppstatus,0)
   FROM
   OpportunityMaster
   WHERE
   numOppId = v_numOppId;                                                                                                 

   open SWV_RefCur3 for
   SELECT * FROM tt_TEMPBIZDOCPRODUCTGRIDCOLUMNS ORDER BY intRowNum;

   RETURN;
END; $$;


