CREATE OR REPLACE FUNCTION USP_GetItemDetailsFor850(v_Inbound850PickItem NUMERIC(18,0)
	,v_vcItemIdentification VARCHAR(100)
	,v_numDivisionId NUMERIC(18,0)
	,v_numCompanyId NUMERIC(18,0)
	,v_numDomainId NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   If EXISTS(SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID = v_numDomainId AND numMarketplaceID = v_numOrderSource AND LOWER(coalesce(vcMarketplaceUniqueID,'')) = LOWER(coalesce(v_vcItemIdentification,''))) then
	
      open SWV_RefCur for
      SELECT
      Item.* FROM
      ItemMarketplaceMapping
      INNER JOIN
      Item
      ON
      ItemMarketplaceMapping.numItemCode = Item.numItemCode
      WHERE
      ItemMarketplaceMapping.numDomainID = v_numDomainId
      AND ItemMarketplaceMapping.numMarketplaceID = v_numOrderSource
      AND LOWER(coalesce(ItemMarketplaceMapping.vcMarketplaceUniqueID,'')) = LOWER(coalesce(v_vcItemIdentification,''));
   ELSE
      IF v_Inbound850PickItem = 1 then --SKU
		
         IF v_numDomainId = 209 AND EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainId AND LOWER(vcItemName) = LOWER(v_vcItemIdentification)) then
			
            open SWV_RefCur for
            SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(vcItemName) = LOWER(v_vcItemIdentification);
         ELSE
            open SWV_RefCur for
            SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(vcSKU) = LOWER(v_vcItemIdentification);
         end if;
      ELSEIF v_Inbound850PickItem = 2
      then  -- UPC
		
         open SWV_RefCur for
         SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(numBarCodeId) = LOWER(v_vcItemIdentification);
      ELSEIF v_Inbound850PickItem = 3
      then  -- ItemName
		
         open SWV_RefCur for
         SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(vcItemName) = LOWER(v_vcItemIdentification);
      ELSEIF v_Inbound850PickItem = 4
      then   -- BizItemID
		
         open SWV_RefCur for
         SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(numItemCode::VARCHAR) = LOWER(v_vcItemIdentification);
      ELSEIF v_Inbound850PickItem = 5
      then   -- ASIN
		
         open SWV_RefCur for
         SELECT * FROM Item WHERE numDomainID = v_numDomainId AND LOWER(vcASIN) = LOWER(v_vcItemIdentification);
      ELSEIF v_Inbound850PickItem = 6
      then  -- CustomerPart#
		
         open SWV_RefCur for
         SELECT * FROM Item I
         INNER JOIN CustomerPartNumber CPN ON I.numItemCode = CPN.numItemCode
         WHERE CPN.numDomainID = v_numDomainId AND CPN.numCompanyId = v_numCompanyId AND LOWER(CPN.CustomerPartNo) = LOWER(v_vcItemIdentification);
      ELSEIF v_Inbound850PickItem = 7
      then   -- VendorPart#
		
         open SWV_RefCur for
         SELECT * FROM Item I
         INNER JOIN Vendor V ON I.numItemCode = V.numItemCode
         WHERE V.numDomainID = v_numDomainId AND V.numVendorID = v_numDivisionId AND LOWER(V.vcPartNo) = LOWER(v_vcItemIdentification);
      end if;
   end if;
   RETURN;
END; $$;
