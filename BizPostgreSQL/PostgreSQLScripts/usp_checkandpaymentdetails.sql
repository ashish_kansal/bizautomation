-- Stored procedure definition script USP_CheckAndPaymentDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckAndPaymentDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,        
 v_numChartAcntId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
--Select GJD.numChartAcntId as numChartAcntId,datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJD.numCreditAmt as CreditAmt,
--(Select numAcntType From Chart_of_accounts Where numAccountId=GJD.numChartAcntId And numDomainId=@numDomainId) as numAcntType From General_Journal_Header GJH         
--inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId         
--Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
--Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
--Where GJD.bitReconcile=0 And GJD.numCreditAmt<>0  And GJD.numChartAcntId=@numChartAcntId    
-- And GJD.numDomainId=@numDomainId  
END; $$;


