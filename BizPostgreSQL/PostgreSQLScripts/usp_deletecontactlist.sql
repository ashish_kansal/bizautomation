CREATE OR REPLACE FUNCTION usp_DeleteContactList(v_numDivisionID NUMERIC(9,0) DEFAULT 0,      
 v_numContactID NUMERIC(9,0) DEFAULT NULL,          
 v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitPrimaryContact  BOOLEAN;                  
   v_intCount  INTEGER;                           
   v_numComDivisionID  NUMERIC(9,0);
BEGIN
   if not exists(select * from  UserMaster where numUserDetailId = v_numContactID and numDomainID = v_numDomainID
   and bitactivateflag = true) then

      if exists(select * from AdditionalContactsInformation where numContactId = v_numContactID and numDomainID = v_numDomainID) then
         delete FROM RecentItems where numRecordID =  v_numContactID and chrRecordType = 'U';
         select   numDivisionId, coalesce(bitPrimaryContact,false) INTO v_numDivisionID,v_bitPrimaryContact from AdditionalContactsInformation where numContactId = v_numContactID;
         select   count(*) INTO v_intCount from AdditionalContactsInformation where numDivisionId = v_numDivisionID;
         select   numDivisionId INTO v_numComDivisionID from Domain where numDomainId = v_numDomainID;
         if (v_numComDivisionID = v_numDivisionID and v_bitPrimaryContact = true) then
            open SWV_RefCur for
            select 0;
         ELSEIF (v_bitPrimaryContact = true and v_intCount > 1)
         then 
            open SWV_RefCur for
            select 0;
         else                    
            open SWV_RefCur for
            SELECT 1;
         end if;
      end if;
   else
      open SWV_RefCur for
      Select 2;
   end if;
   RETURN;
END; $$;


