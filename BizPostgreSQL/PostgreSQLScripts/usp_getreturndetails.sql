-- Stored procedure definition script USP_GetReturnDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReturnDetails(v_numOppId NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT I.numItemCode,
           I.vcItemName AS Item,
           OI.vcItemDesc AS "Desc",
           I.vcModelID,
           OI.monPrice,
           (OI.monPrice*R.numQtyToReturn) AS ReturnAmount,
           R.numQtyToReturn AS Unit
   FROM   Returns R
   INNER JOIN OpportunityItems OI
   ON R.numOppItemCode = OI.numoppitemtCode
   INNER JOIN Item I
   ON I.numItemCode = OI.numItemCode
   WHERE  R.numOppID = v_numOppId;
    
   open SWV_RefCur2 for
   SELECT OM.txtComments,
           D.vcBizDocImagePath,
           OM.vcpOppName,
           OM.numContactId,
           coalesce(ACI.vcEmail,'') AS vcEmail
   FROM   OpportunityMaster OM
   INNER JOIN Domain D
   ON D.numDomainId = OM.numDomainId
   INNER JOIN AdditionalContactsInformation ACI
   ON ACI.numContactId = OM.numContactId
   WHERE  OM.numOppId = v_numOppId;
   RETURN;
END; $$;

  
/****** Object:  StoredProcedure [dbo].[USP_GetReturns]    Script Date: 01/22/2009 01:39:05 ******/



