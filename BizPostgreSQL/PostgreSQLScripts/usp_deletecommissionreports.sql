-- Stored procedure definition script USP_DeleteCommissionReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCommissionReports(v_numComReports NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM Dashboard WHERE numReportID = v_numComReports AND tintReportCategory = 2;

   DELETE FROM CommissionReports WHERE numComReports = v_numComReports;
   RETURN;
END; $$;




