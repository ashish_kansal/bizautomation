CREATE OR REPLACE FUNCTION USP_ManageHTMLFormURL(v_numFormId NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numGroupID NUMERIC(9,0) DEFAULT 0,
    v_vcURL VARCHAR(200) DEFAULT '',
    v_numURLId NUMERIC(9,0) DEFAULT 0,
    v_tintType SMALLINT DEFAULT NULL,
    v_vcSuccessURL VARCHAR(200) DEFAULT '',
    v_vcFailURL VARCHAR(200) DEFAULT '',
	v_numSubFormId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintType = 1 then --Insert/Edit

      IF v_numURLId > 0 then
    
         Update HTMLFormURL SET vcURL = v_vcURL,vcSuccessURL = v_vcSuccessURL,vcFailURL = v_vcFailURL WHERE numURLId = v_numURLId;
         open SWV_RefCur for
         SELECT v_numURLId;
      ELSE
         INSERT INTO HTMLFormURL(numFormId,numDomainID,numGroupID,vcURL,vcSuccessURL,vcFailURL,numSubFormId) VALUES(v_numFormId,v_numDomainId,v_numGroupID,v_vcURL,v_vcSuccessURL,v_vcFailURL,v_numSubFormId);
	    
         open SWV_RefCur for
         SELECT CURRVAL('HTMLFormURL_seq');
      end if;
   ELSEIF v_tintType = 2
   then --Select

      open SWV_RefCur for
      SELECT * FROM HTMLFormURL WHERE numFormId = v_numFormId AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0) AND numDomainID = v_numDomainId AND numGroupID = v_numGroupID;
   ELSEIF v_tintType = 3
   then --Delete

      DELETE FROM HTMLFormURL WHERE numURLId = v_numURLId;
      open SWV_RefCur for
      SELECT v_numURLId;
   ELSEIF v_tintType = 4
   then --Select Single Record

      open SWV_RefCur for
      SELECT * FROM HTMLFormURL WHERE numURLId = v_numURLId;
   end if;
   RETURN;
END; $$;  


