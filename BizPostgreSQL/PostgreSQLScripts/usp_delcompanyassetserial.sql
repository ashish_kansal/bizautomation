-- Stored procedure definition script USP_DelCompanyAssetSerial for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DelCompanyAssetSerial(v_numAssetItemDTLID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from CompanyAssetSerial where numAssetItemDTLID = v_numAssetItemDTLID;
   RETURN;
END; $$;


