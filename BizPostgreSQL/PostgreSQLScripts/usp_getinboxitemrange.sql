-- FUNCTION: public.usp_getinboxitemrange(integer, numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getinboxitemrange(integer, numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getinboxitemrange(
	v_pagesize integer,
	v_numdomainid numeric,
	v_numusercntid numeric,
	v_numnodeid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 17:03:03 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).

  
  --Added filter for not to fetch sent items
   open SWV_RefCur for
   with  Emails
   AS(SELECT cast(ROW_NUMBER()
   OVER(ORDER BY EH.numEmailHstrID DESC) as VARCHAR(255)) AS RowNumber,
                    cast(EH.numEmailHstrID as VARCHAR(255)),
                    cast(EH.numUid as VARCHAR(255))
   FROM   EmailHistory EH
   WHERE  numUserCntId = v_numUserCntId
   AND numDomainID = v_numDomainId
   AND tintType <> 2
   AND numUid > 0
   AND numNodeId = v_numNodeId)
   SELECT * FROM   Emails
   WHERE  CAST(RowNumber As Numeric) > 0
   AND CAST(RowNumber As Numeric) <= v_PageSize;

   IF((SELECT COUNT(EH.numEmailHstrID) FROM EmailHistory AS EH LEFT JOIN EmailMaster AS EM ON
   EH.numEMailId = EM.numEmailId
   WHERE numUserCntId = v_numUserCntId AND EH.numDomainID = v_numDomainId AND numNodeId = v_numNodeId AND coalesce(EM.numContactID,0) = 0) > 1000) then
 
      DELETE FROM EmailHistory WHERE numEmailHstrID IN(SELECT  EH.numEmailHstrID FROM EmailHistory AS EH LEFT JOIN EmailMaster AS EM ON
      EH.numEMailId = EM.numEmailId
      WHERE numUserCntId = v_numUserCntId AND EH.numDomainID = v_numDomainId AND numNodeId = v_numNodeId AND coalesce(EM.numContactID,0) = 0 ORDER BY numEmailHstrID ASC LIMIT 1000);
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getinboxitemrange(integer, numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
