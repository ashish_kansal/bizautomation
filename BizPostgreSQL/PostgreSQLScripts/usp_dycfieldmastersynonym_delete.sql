-- Stored procedure definition script USP_DycFieldMasterSynonym_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DycFieldMasterSynonym_Delete(v_numDomainID NUMERIC(18,0)
	,v_vcIDs TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM DycFieldMasterSynonym WHERE numDomainID = v_numDomainID AND ID IN(SELECT Id FROM SplitIDs(v_vcIDs,',')) AND coalesce(bitDefault,false) = false;
   RETURN;
END; $$;


