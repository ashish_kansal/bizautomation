-- Stored procedure definition script USP_GetOppKitItemsList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOppKitItemsList(v_numOppID NUMERIC(9,0),
	v_numOppItemID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		I.vcItemName AS "vcItemName"
		,OKI.numQtyItemsReq AS "numQtyItemsReq"
		,OKI.numQtyShipped AS "numQtyShipped"
		,W.vcWareHouse AS "vcWareHouse"
		,coalesce(numOnHand,0) AS "numOnHand"
		,coalesce(numOnOrder,0) AS "numOnOrder"
		,coalesce(numReorder,0) AS "numReorder"
		,coalesce(numAllocation,0) AS "numAllocation"
		,coalesce(numBackOrder,0) AS "numBackOrder"
		,case when I.charItemType = 'P' then 'Inventory Item' when I.charItemType = 'S' then 'Service' when I.charItemType = 'A' then 'Accessory' when I.charItemType = 'N' then 'Non-Inventory Item' end as "vcItemType"
   from OpportunityKitItems OKI join Item I on OKI.numChildItemID = I.numItemCode
   LEFT OUTER JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode
   AND WI.numWareHouseItemID = OKI.numWareHouseItemId
   LEFT OUTER JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
   where OKI.numOppId = v_numOppID AND OKI.numOppItemID = v_numOppItemID;
END; $$;
  












