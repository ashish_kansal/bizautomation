-- Stored procedure definition script usp_ManageCaseSolutions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageCaseSolutions(v_numCaseID NUMERIC,
	v_numSolnID NUMERIC
--
)
RETURNS VOID LANGUAGE plpgsql
-- Created : 19 April 2004
-- This Procedure will take the Case ID and teh Solution ID and check internally if the record exists. If no, it is inserted.
   AS $$
   DECLARE
   v_bitRetVal  BOOLEAN;
BEGIN
   v_bitRetVal := false;

   select   Count(numCaseId) INTO v_bitRetVal FROM CaseSolutions WHERE numCaseId = v_numCaseID AND numSolnID = v_numSolnID;

   IF v_bitRetVal = true then
	
      INSERT INTO CaseSolutions  values(v_numCaseID, v_numSolnID);
   end if;
   RETURN;
END; $$;


