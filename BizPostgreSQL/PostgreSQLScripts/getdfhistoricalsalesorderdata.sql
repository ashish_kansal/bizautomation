-- Function definition script GetDFHistoricalSalesOrderData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDFHistoricalSalesOrderData(v_numDFID NUMERIC(18,0),
      v_numDomainID NUMERIC(9,0),
	  v_dtFromDate TIMESTAMP,
	  v_dtToDate TIMESTAMP)
RETURNS TABLE 
(
   numItemCode NUMERIC(18,0), 
   numWarehouseID NUMERIC(18,0),
   numWarehouseItemID NUMERIC(18,0),
   numQtySold INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitWarehouseFilter  BOOLEAN DEFAULT false;
   v_bitItemClassificationFilter  BOOLEAN DEFAULT false;
   v_bitItemGroupFilter  BOOLEAN DEFAULT false;
BEGIN
   DROP TABLE IF EXISTS tt_GETDFHISTORICALSALESORDERDATA_TEMP;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETDFHISTORICALSALESORDERDATA_TEMP
   (
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numQtySold INTEGER
   );
   IF(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) > 0 then
		
      v_bitWarehouseFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) > 0 then
		
      v_bitItemClassificationFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) > 0 then
		
      v_bitItemGroupFilter := true;
   end if;


   DROP TABLE IF EXISTS tt_TEMPORDER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPORDER
   (
      numOppId NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      bitKitParent BOOLEAN,
      bitAssembly BOOLEAN
   );

   INSERT INTO
   tt_TEMPORDER
   SELECT
   OpportunityMaster.numOppId,
			OpportunityItems.numoppitemtCode,
			Item.numItemCode,
			WareHouseItems.numWareHouseID,
			(CASE
   WHEN coalesce(Item.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WareHouseItems.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WareHouseItems.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT  WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WareHouseItems.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WareHouseItems.numWareHouseItemID
      END)
   ELSE(SELECT WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND WareHouseItems.numWareHouseID = WareHouseItems.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END),
			OpportunityItems.numUnitHour,
			Item.bitKitParent,
			Item.bitAssembly
   FROM
   OpportunityMaster
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   INNER JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID AND
   CAST(OpportunityMaster.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE) AND
   OpportunityMaster.tintopptype = 1 AND
   OpportunityMaster.tintoppstatus = 1
   AND coalesce(OpportunityItems.bitDropShip,false) = false
   AND coalesce(OpportunityItems.bitWorkOrder,false) = false
   AND
			((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WareHouseItems.numWareHouseID IN(SELECT DemandForecastWarehouse.numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
			((SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) = 0 OR
   Item.numItemClassification IN(SELECT DemandForecastItemClassification.numitemclassificationid FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
			((SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) = 0 OR
   Item.numItemGroup IN(SELECT DemandForecastItemGroup.numitemgroupid FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

		
		-- GET KIT ITEMS CHILD ITEMS
   INSERT INTO
   tt_TEMPORDER   with recursive CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,numItemGroup,bitKitParent,
   numWarehouseID,numWarehouseItemID,numQty) AS(SELECT
   numOppChildItemID AS numOppChildItemID
				,OKI.numOppId AS numOppID
				,OKI.numOppItemID AS numOppItemID
				,numChildItemID AS numItemCode
				,I.numItemGroup AS numItemGroup
				,I.bitKitParent AS bitKitParent
				,WI.numWareHouseID AS numWarehouseID
				,WI.numWareHouseItemID AS numWarehouseItemID
				,t1.numUnitHour*numQtyItemsReq_Orig AS numQty
   FROM
   tt_TEMPORDER AS t1
   INNER JOIN
   OpportunityKitItems OKI
   ON
   t1.numOppId = OKI.numOppId
   AND t1.numOppItemID = OKI.numOppItemID
   INNER JOIN
   Item I
   ON
   OKI.numChildItemID = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKI.numWareHouseItemId = WI.numWareHouseItemID
   AND coalesce(t1.bitKitParent,false) = true
   AND coalesce(t1.bitAssembly,false) = false
   UNION ALL
   SELECT
   OKCI.numOppKitChildItemID AS numOppChildItemID
				,OKCI.numOppId AS numOppID
				,OKCI.numOppItemID AS numOppItemID
				,OKCI.numItemID AS numItemCode
				,I.numItemGroup AS numItemGroup
				,I.bitKitParent AS bitKitParent
				,WI.numWareHouseID AS numWarehouseID
				,WI.numWareHouseItemID AS numWarehouseItemID
				,c.numQty*numQtyItemsReq_Orig AS numQty
   FROM
   OpportunityKitChildItems OKCI
   INNER JOIN
   CTE c
   ON
   OKCI.numOppChildItemID = c.numOppChildItemID
   INNER JOIN
   Item I
   ON
   OKCI.numItemID = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKCI.numWareHouseItemId = WI.numWareHouseItemID) SELECT
   c.numOppID,
			c.numOppItemID,
			c.numItemCode,
			c.numWarehouseID,
			(CASE
   WHEN coalesce(c.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         c.numWareHouseItemID
      END)
   ELSE(SELECT  WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = c.numItemCode AND WareHouseItems.numWareHouseID = c.numWareHouseID AND WareHouseItems.numWLocationID = -1 LIMIT 1)
   END),
			numQty,
			bitKitParent,
			CAST(0 AS BOOLEAN)
   FROM
   CTE c
   WHERE
   coalesce(bitKitParent,false) = false;

   INSERT INTO
   tt_TEMPORDER
   SELECT
   0,
			0,
			I.numItemCode,
			WI.numWareHouseID,
			(CASE
   WHEN coalesce(I.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WI.numWareHouseItemID
      END)
   ELSE(SELECT  WareHouseItems.numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = I.numItemCode AND WareHouseItems.numWareHouseID = WI.numWareHouseID AND WareHouseItems.numWLocationID = -1 LIMIT 1)
   END),
			WOD.numQtyItemsReq,
			CAST(0 AS BOOLEAN),
			CAST(0 AS BOOLEAN)
   FROM
   WorkOrder WO
   INNER JOIN
   WorkOrderDetails WOD
   ON
   WO.numWOId = WOD.numWOId
   INNER JOIN
   Item I
   ON
   WOD.numChildItemID = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   WOD.numWarehouseItemID = WI.numWareHouseItemID
   WHERE
   WO.numDomainId = v_numDomainID
   AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE)
   AND
			((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WI.numWareHouseID IN(SELECT DemandForecastWarehouse.numWareHouseID FROM DemandForecastWarehouse WHERE DemandForecastWarehouse.numDFID = v_numDFID))
   AND
			((SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) = 0 OR
   I.numItemClassification IN(SELECT DemandForecastItemClassification.numitemclassificationid FROM DemandForecastItemClassification WHERE DemandForecastItemClassification.numDFID = v_numDFID))
   AND
			((SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) = 0 OR
   I.numItemGroup IN(SELECT DemandForecastItemGroup.numitemgroupid FROM DemandForecastItemGroup WHERE DemandForecastItemGroup.numDFID = v_numDFID));

		-- Get total quantity of inventoty or assembly item in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
   INSERT INTO
   tt_GETDFHISTORICALSALESORDERDATA_TEMP
   SELECT
   t1.numItemCode,
			t1.numWarehouseID,
			t1.numWarehouseItemID,
			SUM(t1.numUnitHour) AS numQtySold
   FROM
   tt_TEMPORDER AS t1
   WHERE
   coalesce(t1.bitKitParent,false) = false
   AND coalesce(t1.bitAssembly,false) = false
   AND coalesce(t1.numWarehouseItemID,0) > 0
   GROUP BY
   t1.numItemCode,t1.numWarehouseID,t1.numWarehouseItemID;

		RETURN QUERY (SELECT * FROM tt_GETDFHISTORICALSALESORDERDATA_TEMP);
END; $$;

