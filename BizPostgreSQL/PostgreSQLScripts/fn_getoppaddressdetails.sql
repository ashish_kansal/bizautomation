DROP FUNCTION IF EXISTS fn_getOPPAddressDetails;
CREATE OR REPLACE FUNCTION fn_getOPPAddressDetails(v_numOppId NUMERIC,v_numDomainID  NUMERIC,v_tintMode SMALLINT)
RETURNS table
(
   numCountry	NUMERIC(18,0),
   numState	NUMERIC(18,0),
   vcCountry	VARCHAR(50),
   vcState		VARCHAR(50),
   vcCity		VARCHAR(50),
   vcStreet	VARCHAR(100),
   vcPostalCode VARCHAR(20),
   vcAddressName VARCHAR(50),
   vcCompanyName TEXT,
   vcContact TEXT
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strAddress  VARCHAR(1000);

   v_tintOppType  SMALLINT;
   v_tintBillType  SMALLINT;
   v_tintShipType  SMALLINT;
   v_numBillToAddressID  NUMERIC(18,0);
   v_numShipToAddressID  NUMERIC(18,0);
 
   v_numParentOppID  NUMERIC;
   v_numDivisionID  NUMERIC;
BEGIN
	SELECT 
		tintopptype
		,tintBillToType
		,tintShipToType
		,numDivisionId
		,coalesce(numBillToAddressID,0)
		,coalesce(numShipToAddressID,0) 
	INTO 
		v_tintOppType
		,v_tintBillType
		,v_tintShipType
		,v_numDivisionID
		,v_numBillToAddressID,
		v_numShipToAddressID 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId = v_numOppId;
   
	SELECT 
		COALESCE(numParentOppID,0) 
	INTO 
		v_numParentOppID 
	FROM 
		OpportunityLinking 
	WHERE 
		numChildOppID = v_numOppId;
            
	IF v_tintMode = 1 then --Billing Address
		IF (v_tintBillType IS NULL AND v_tintOppType = 1) OR (v_tintBillType = 1 AND v_tintOppType = 1) THEN --Primary Bill Address or When Sales order and bill to is set to customer	 
			RETURN QUERY
			SELECT 
				coalesce(AD.numCountry,0),
				coalesce(AD.numState,0),
				coalesce(fn_GetListItemName(AD.numCountry),''),
				coalesce(fn_GetState(AD.numState),''),
				coalesce(AD.vcCity,'') ,
			    coalesce(AD.vcStreet,''),
			    coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,fn_GetComapnyName(v_numDivisionID) AS vcCompanyName
				,(CASE
					WHEN coalesce(bitAltContact,false) = true
					THEN coalesce(vcAltContact,'')
					ELSE(CASE
							WHEN coalesce(numContact,0) > 0
							THEN
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							ELSE
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						END)
				END) AS vcContact
			FROM 
				AddressDetails AD
			WHERE 
				AD.numDomainID = v_numDomainID
				AND AD.numRecordID = v_numDivisionID
				AND AD.tintAddressOf = 2
				AND AD.tintAddressType = 1
				AND AD.bitIsPrimary = true;
		ELSEIF v_tintBillType = 1 AND v_tintOppType = 2 THEN -- When Create PO from SO and Bill to is set to Customer
			RETURN QUERY
			SELECT AD.numCountry,AD.numState,AD.vcCountry,AD.vcState,AD.vcCity,AD.vcStreet,AD.vcPostalCode,AD.vcAddressName,AD.vcCompanyName::TEXT,AD.vcContact
			FROM fn_getOPPAddressDetails(v_numParentOppID,v_numDomainID,v_tintMode) AD;
		ELSEIF coalesce(v_tintBillType,0) = 0 THEN
			RETURN QUERY
			SELECT  
				coalesce(AD.numCountry,0),
				coalesce(AD.numState,0),
				coalesce(fn_GetListItemName(AD.numCountry),''),
				coalesce(fn_GetState(AD.numState),''),
				coalesce(AD.vcCity,'') ,
			    coalesce(AD.vcStreet,''),
			    coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName::TEXT
				,(CASE
					WHEN coalesce(bitAltContact,false) = true
					THEN coalesce(vcAltContact,'')
					ELSE(CASE
							WHEN coalesce(numContact,0) > 0
							THEN
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							ELSE
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						END)
				END) AS vcContact
			FROM 
				CompanyInfo Com1 
			JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
			JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
			JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true
			WHERE 
				D1.numDomainId = v_numDomainID;
		ELSEIF v_tintBillType = 2 THEN
			IF coalesce(v_numBillToAddressID,0) > 0 then
				RETURN QUERY
				SELECT 
					coalesce(AD.numCountry,0),
					coalesce(AD.numState,0),
					coalesce(fn_GetListItemName(AD.numCountry),''),
					coalesce(fn_GetState(AD.numState),''),
					coalesce(AD.vcCity,'') ,
					coalesce(AD.vcStreet,''),
					coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,fn_GetComapnyName(numRecordID) AS vcCompanyName
					,(CASE
						WHEN coalesce(bitAltContact,false) = true
						THEN coalesce(vcAltContact,'')
						ELSE(CASE
							   WHEN coalesce(numContact,0) > 0
							   THEN
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							   ELSE
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
							END)
					END) AS vcContact
				FROM 
					AddressDetails AD 
				WHERE 
					numAddressID = v_numBillToAddressID;
			ELSE
				RETURN QUERY
				SELECT 
					OpportunityAddress.numBillCountry,
					OpportunityAddress.numBillState,
					coalesce(fn_GetListItemName(OpportunityAddress.numBillCountry),''),
					coalesce(fn_GetState(OpportunityAddress.numBillState),''),
					OpportunityAddress.vcBillCity,
					OpportunityAddress.vcBillStreet,
					OpportunityAddress.vcBillPostCode,coalesce(OpportunityAddress.vcAddressName,'') AS vcAddressName,CASE WHEN LENGTH(OpportunityAddress.vcBillCompanyName) > 0 THEN OpportunityAddress.vcBillCompanyName ELSE fn_GetComapnyName(v_numDivisionID) END AS vcCompanyName
					,(CASE
						WHEN coalesce(bitAltBillingContact,false) = true
						THEN coalesce(vcAltBillingContact,'')
						ELSE(CASE
							   WHEN coalesce(numBillingContact,0) > 0
							   THEN
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numBillingContact LIMIT 1),'')
							   ELSE
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID = v_numDomainID AND DivisionMaster.numCompanyID = numBillCompanyId AND coalesce(bitPrimaryContact,false) = true LIMIT 1),
								  '')
							END)
					END) AS vcContact
				FROM 
					OpportunityAddress 
				WHERE 
					numOppID = v_numOppId;
			END IF;
		ELSEIF v_tintBillType = 3 THEN
			RETURN QUERY
			SELECT 
				OpportunityAddress.numBillCountry,
				OpportunityAddress.numBillState,
				coalesce(fn_GetListItemName(OpportunityAddress.numBillCountry),''),
				coalesce(fn_GetState(OpportunityAddress.numBillState),''),
				OpportunityAddress.vcBillCity,
				OpportunityAddress.vcBillStreet,
				OpportunityAddress.vcBillPostCode,coalesce(OpportunityAddress.vcAddressName,'') AS vcAddressName,fn_GetComapnyName(v_numDivisionID) AS vcCompanyName
				,(CASE
					 WHEN coalesce(OpportunityAddress.bitAltBillingContact,false) = true
					 THEN coalesce(OpportunityAddress.vcAltBillingContact,'')
					 ELSE(CASE
							WHEN coalesce(numBillingContact,0) > 0
							THEN
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = OpportunityAddress.numBillingContact LIMIT 1),'')
							ELSE
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = v_numDivisionID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						END)
				END) AS vcContact
			FROM 
				OpportunityAddress 
			WHERE 
				numOppID = v_numOppId;
		END IF;
	ELSEIF v_tintMode = 2 THEN --Shipping Address
		IF v_tintShipType IS NULL OR (v_tintShipType = 1 AND v_tintOppType = 1) THEN
			RETURN QUERY
			SELECT 
				coalesce(AD.numCountry,0),
				coalesce(AD.numState,0),
				coalesce(fn_GetListItemName(AD.numCountry),''),
				coalesce(fn_GetState(AD.numState),''),
				coalesce(AD.vcCity,'') ,
				coalesce(AD.vcStreet,''),
				coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,fn_GetComapnyName(v_numDivisionID) AS vcCompanyName
				,(CASE
					 WHEN coalesce(bitAltContact,false) = true
					 THEN coalesce(vcAltContact,'')
					 ELSE(CASE
							WHEN coalesce(numContact,0) > 0
							THEN
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							ELSE
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						END)
				END) AS vcContact
			FROM 
				AddressDetails AD
			WHERE 
				AD.numDomainID = v_numDomainID 
				AND AD.numRecordID = v_numDivisionID
				AND AD.tintAddressOf = 2 
				AND AD.tintAddressType = 2 
				AND AD.bitIsPrimary = true;
		ELSEIF v_tintShipType = 1 AND v_tintOppType = 2 THEN -- When Create PO from SO and Ship to is set to Customer 
			RETURN QUERY
			SELECT AD.numCountry,AD.numState,AD.vcCountry,AD.vcState,AD.vcCity,AD.vcStreet,AD.vcPostalCode,AD.vcAddressName,AD.vcCompanyName::TEXT,AD.vcContact
			FROM fn_getOPPAddressDetails(v_numParentOppID,v_numDomainID,v_tintMode) AD;
		ELSEIF v_tintShipType = 0 THEN
			RETURN QUERY
			SELECT  
				coalesce(AD.numCountry,0),
				coalesce(AD.numState,0),
				coalesce(fn_GetListItemName(AD.numCountry),''),
				coalesce(fn_GetState(AD.numState),''),
				coalesce(AD.vcCity,'') ,
				coalesce(AD.vcStreet,''),
				coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName::TEXT
				,(CASE
					 WHEN coalesce(bitAltContact,false) = true
					 THEN coalesce(vcAltContact,'')
					 ELSE(CASE
							WHEN coalesce(numContact,0) > 0
							THEN
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							ELSE
							   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						END)
				END) AS vcContact
			FROM 
				CompanyInfo Com1 
			JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
			JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
			JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true
			WHERE  D1.numDomainId = v_numDomainID;
		ELSEIF v_tintShipType = 2 THEN
			IF coalesce(v_numShipToAddressID,0) > 0 then
				RETURN QUERY
				SELECT 
					coalesce(AD.numCountry,0),
					coalesce(AD.numState,0),
					coalesce(fn_GetListItemName(AD.numCountry),''),
					coalesce(fn_GetState(AD.numState),''),
					coalesce(AD.vcCity,'') ,
					coalesce(AD.vcStreet,''),
					coalesce(AD.vcPostalCode,''),coalesce(AD.vcAddressName,'') AS vcAddressName,fn_GetComapnyName(numRecordID) AS vcCompanyName
					,(CASE
						WHEN coalesce(bitAltContact,false) = true
						THEN coalesce(vcAltContact,'')
						ELSE(CASE
							   WHEN coalesce(numContact,0) > 0
							   THEN
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
							   ELSE
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						   END)
					END) AS vcContact
				FROM 
					AddressDetails AD 
				WHERE 
					numAddressID = v_numShipToAddressID;
			ELSE
				RETURN QUERY
				SELECT 
					OpportunityAddress.numShipCountry
					,OpportunityAddress.numShipState
					,coalesce(fn_GetListItemName(OpportunityAddress.numShipCountry),'')
					,coalesce(fn_GetState(OpportunityAddress.numShipState),'')
					,OpportunityAddress.vcShipCity
					,OpportunityAddress.vcShipStreet
					,OpportunityAddress.vcShipPostCode
					,coalesce(OpportunityAddress.vcshipaddressname,'') AS vcAddressName
					,(CASE WHEN LENGTH(OpportunityAddress.vcShipCompanyName) > 0 THEN OpportunityAddress.vcShipCompanyName ELSE fn_GetComapnyName(v_numDivisionID) END) AS vcCompanyName
					,(CASE
						WHEN coalesce(OpportunityAddress.bitAltShippingContact,false) = true
						THEN coalesce(OpportunityAddress.vcAltShippingContact,'')
						ELSE(CASE
							   WHEN coalesce(OpportunityAddress.numShippingContact,0) > 0
							   THEN
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = OpportunityAddress.numShippingContact LIMIT 1),'')
							   ELSE
								  coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = v_numDivisionID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
						   END)
					END) AS vcContact
				FROM 
					OpportunityAddress 
				WHERE 
					numOppID = v_numOppId;
			END IF;
		ELSEIF v_tintShipType = 3 THEN
			RETURN QUERY
			SELECT OpportunityAddress.numShipCountry,OpportunityAddress.numShipState,coalesce(fn_GetListItemName(OpportunityAddress.numShipCountry),''),coalesce(fn_GetState(OpportunityAddress.numShipState),''),OpportunityAddress.vcShipCity,OpportunityAddress.vcShipStreet,OpportunityAddress.vcShipPostCode,coalesce(OpportunityAddress.vcshipaddressname,'') AS vcAddressName
				,fn_GetComapnyName(v_numDivisionID) AS vcCompanyName
				,(CASE
				 WHEN coalesce(OpportunityAddress.bitAltShippingContact,false) = true
				 THEN coalesce(OpportunityAddress.vcAltShippingContact,'')
				 ELSE(CASE
					WHEN coalesce(OpportunityAddress.numShippingContact,0) > 0
					THEN
					   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = OpportunityAddress.numShippingContact LIMIT 1),'')
					ELSE
					   coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = v_numDivisionID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
					END)
				 END) AS vcContact
			FROM 
				OpportunityAddress 
			WHERE 
				numOppID = v_numOppId;
		end if;
	end if;
END; $$;

