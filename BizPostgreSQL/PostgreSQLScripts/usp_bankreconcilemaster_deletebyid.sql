-- Stored procedure definition script USP_BankReconcileMaster_DeleteByID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BankReconcileMaster_DeleteByID(v_numDomainID NUMERIC(18,0)
	,v_numReconcileID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
UPDATE General_Journal_Details SET bitReconcile = false,bitCleared = false,numReconcileID = 0 WHERE numDomainId = v_numDomainID AND numReconcileID = v_numReconcileID;
      DELETE FROM BankReconcileFileData WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
      DELETE FROM BankReconcileMaster WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


