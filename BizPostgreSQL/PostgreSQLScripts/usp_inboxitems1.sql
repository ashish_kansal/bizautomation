-- Stored procedure definition script USP_InboxItems1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InboxItems1(v_PageSize INTEGER DEFAULT -1,    
    v_CurrentPage INTEGER DEFAULT -1,    
v_srchSubjectBody VARCHAR(100) DEFAULT '',    
v_srchEmail VARCHAR(100) DEFAULT '',    
v_srchAttachmenttype VARCHAR(100) DEFAULT '',   
v_columnName VARCHAR(50) DEFAULT NULL,                                                                      
v_columnSortOrder VARCHAR(10) DEFAULT 'desc' ,  
INOUT v_TotRecs INTEGER  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                    
   v_lastRec  INTEGER;                                                    
   v_strSql  VARCHAR(8000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                    
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                     


       
   v_strSql := ' select  FromName,FromEmail , ToEmail ,  CCEmail ,BCCEmail , numEmailHstrID  ,[count],Subject,Body,created,ChangeKey,IsRead,Size,HasAttachments
,AttachmentType,sent,type,Source,Category from (
select  isnull(vcMessageFrom,'''') as FromName,      
isnull(vcFromEmail,'''') as FromEmail,              
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,         
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,        
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,             
emailHistory.numEmailHstrID,      
ROW_NUMBER() OVER (ORDER BY emailHistory.numEmailHstrID ASC) AS  [count],      
isnull(vcSubject,'''') as Subject,              
isnull(vcBody,'''') as Body,      
isnull(bintCreatedOn,getutcdate()) as created,      
isnull(vcItemId,'''') as ItemId,      
isnull(vcChangeKey,'''') as ChangeKey,      
isnull(bitIsRead,false) as IsRead,      
isnull(vcSize,0) as Size,      
isnull(bitHasAttachments,false) as HasAttachments,      
isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,    
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,    
isnull(dtReceivedOn,getutcdate()) as sent,      
isnull(tintType,1) as type,      
isnull(chrSource,''B'') as Source,  
isnull(vcCategory,''white'') as Category    
      

    
from emailHistory        
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID    
where tinttype=1 
 )   as i
 WHERE [count] > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(200)),1,200) || ' and [count] <' || SUBSTR(CAST(v_lastRec AS VARCHAR(200)),1,200) ||
   ' order by ' || SUBSTR(CAST(v_columnName AS VARCHAR(100)),1,100) || ' ' || coalesce(v_columnSortOrder,'');

   RAISE NOTICE '%',(v_strSql);
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


