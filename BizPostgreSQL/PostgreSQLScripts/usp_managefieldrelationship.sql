-- Stored procedure definition script USP_ManageFieldRelationship for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageFieldRelationship(v_byteMode SMALLINT,
v_numFieldRelID NUMERIC(9,0),
v_numModuleID NUMERIC(18,0),
v_numPrimaryListID NUMERIC(9,0),
v_numSecondaryListID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),
v_numFieldRelDTLID NUMERIC(9,0),
v_numPrimaryListItemID NUMERIC(9,0),
v_numSecondaryListItemID NUMERIC(9,0),
v_bitTaskRelation BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      IF(v_bitTaskRelation = true) then
	
         IF NOT EXISTS(SELECT * FROM FieldRelationship WHERE numDomainID = v_numDomainID AND numPrimaryListID = v_numPrimaryListID AND bitTaskRelation = true) then
		
            insert into FieldRelationship(numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation,numModuleID)
			values(v_numPrimaryListID,v_numSecondaryListID,v_numDomainID,v_bitTaskRelation,v_numModuleID);
			
            v_numFieldRelID := CURRVAL('FieldRelationship_seq');
         ELSE
            v_numFieldRelID := 0;
         end if;
      ELSE
         IF NOT EXISTS(SELECT * FROM FieldRelationship WHERE numDomainID = v_numDomainID AND numPrimaryListID = v_numPrimaryListID AND numSecondaryListID = v_numSecondaryListID AND (numModuleID IS NULL OR numModuleID = v_numModuleID)) then
	
            insert into FieldRelationship(numPrimaryListID,numSecondaryListID,numDomainID,numModuleID,bitTaskRelation)
		values(v_numPrimaryListID,v_numSecondaryListID,v_numDomainID,v_numModuleID,v_bitTaskRelation);
		
            v_numFieldRelID := CURRVAL('FieldRelationship_seq');
         ELSE
            v_numFieldRelID := 0;
         end if;
      end if;
   ELSEIF v_byteMode = 2
   then

      if not exists(select * from FieldRelationshipDTL where numFieldRelID = v_numFieldRelID
      and numPrimaryListItemID = v_numPrimaryListItemID and numSecondaryListItemID = v_numSecondaryListItemID) then
         insert into FieldRelationshipDTL(numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values(v_numFieldRelID,v_numPrimaryListItemID,v_numSecondaryListItemID);
      end if;
   ELSEIF v_byteMode = 3
   then
	
      Delete from FieldRelationshipDTL where numFieldRelDTLID = v_numFieldRelDTLID;
   ELSEIF v_byteMode = 4
   then
	
      Delete from FieldRelationshipDTL where numFieldRelID = v_numFieldRelID;
      Delete from FieldRelationship where numFieldRelID = v_numFieldRelID;
   end if;

   open SWV_RefCur for select v_numFieldRelID;
END; $$;












