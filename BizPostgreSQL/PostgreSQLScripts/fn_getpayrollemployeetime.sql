-- Function definition script fn_GetPayrollEmployeeTime for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetPayrollEmployeeTime(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID INTEGER
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_ClientTimeZoneOffset INTEGER)
RETURNS TABLE
(	
   numRecordID NUMERIC(18,0),
   tintRecordType SMALLINT,
   vcSource VARCHAR(200),
   dtDate DATE,
   vcDate VARCHAR(50),
   numType SMALLINT,
   vcType VARCHAR(500),
   vcHours VARCHAR(20),
   numMinutes INTEGER,
   vcDetails TEXT,
   numApprovalComplete INTEGER
) LANGUAGE plpgsql
   AS $$
BEGIN


	-- Time & Expense
   DROP TABLE IF EXISTS tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME
   (
      numRecordID NUMERIC(18,0),
      tintRecordType SMALLINT,
      vcSource VARCHAR(500),
      dtDate DATE,
      vcDate VARCHAR(50),
      numType SMALLINT,
      vcType VARCHAR(500),
      vcHours VARCHAR(20),
      numMinutes INTEGER,
      vcDetails TEXT,
      numApprovalComplete INTEGER
   );
   INSERT INTO tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME(numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete)
   SELECT
   timeandexpense.numCategoryHDRID
		,1
		,'Time & Expense' AS vcSource
		,dtFromDate
		,FormatedDateFromDate(dtFromDate+ make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) AS vcDate
		,timeandexpense.numtype
		,(CASE
   WHEN timeandexpense.numtype = 4 THEN 'Absence (Un-Paid)'
   WHEN timeandexpense.numtype = 3 THEN 'Absence (Paid)'
   WHEN timeandexpense.numtype = 2 Then 'Non-Billable Time'
   WHEN timeandexpense.numtype = 1 Then 'Billable'
   ELSE ''
   END) AS vcType
		,SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS vcHours
		,(EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) AS numMinutes
		,coalesce(timeandexpense.txtDesc,'') AS vcDetails
		,coalesce(timeandexpense.numApprovalComplete,0) AS numApprovalComplete
   FROM
   timeandexpense
   LEFT JOIN
   OpportunityMaster
   ON
   timeandexpense.numOppId = OpportunityMaster.numOppId
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND timeandexpense.numUserCntID = v_numUserCntID
   AND coalesce(numCategory,0) = 1
   AND timeandexpense.numType NOT IN(4) -- DO NOT INCLUDE UNPAID LEAVE
   AND CAST(dtFromDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   ORDER BY
   dtFromDate;

	-- Action Item
   INSERT INTO tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME(numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete)
   SELECT
   Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,
   '&lngType=0">',GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),
      DM.numDivisionID,'">',coalesce(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)) AS vcSource
		,dtStartTime
		,FormatedDateFromDate(Communication.dtStartTime + make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) AS vcDate
		,0
		,GetListIemName(bitTask) AS vcType
		,SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM Communication.dtEndTime -Communication.dtStartTime)*60*24+EXTRACT(HOUR FROM Communication.dtEndTime -Communication.dtStartTime)*60+EXTRACT(MINUTE FROM Communication.dtEndTime -Communication.dtStartTime)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS vcHours
		,(EXTRACT(DAY FROM Communication.dtEndTime -Communication.dtStartTime)*60*24+EXTRACT(HOUR FROM Communication.dtEndTime -Communication.dtStartTime)*60+EXTRACT(MINUTE FROM Communication.dtEndTime -Communication.dtStartTime)) AS numMinutes
		,coalesce(Communication.textDetails,'') AS vcDetails
		,0
   FROM
   Communication
   LEFT JOIN
   DivisionMaster DM
   ON
   Communication.numDivisionID = DM.numDivisionID
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   Communication.numDomainID = v_numDomainID
   AND Communication.numAssign = v_numUserCntID
   AND CAST(Communication.dtStartTime + make_interval(mins => -v_ClientTimeZoneOffset) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   AND coalesce(bitClosedFlag,false) = true
   ORDER BY
   dtStartTime;

	-- Calendar
   INSERT INTO tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME(numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete)
   SELECT
   Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,
   '&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,FormatedDateFromDate(Activity.StartDateTimeUtc + make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) AS vcDate
		,0
		,'Calendar' AS vcType
		,SUBSTR(TO_CHAR('1900-01-01':: date+CAST((EXTRACT(DAY FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60*24+EXTRACT(HOUR FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60+EXTRACT(MINUTE FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)) || 'minute' as interval),'hh24:mi:ss'),1,5) AS vcHours
		,(EXTRACT(DAY FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60*24+EXTRACT(HOUR FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)*60+EXTRACT(MINUTE FROM Activity.StartDateTimeUtc+CAST(Activity.Duration || 'second' as interval) -Activity.StartDateTimeUtc)) AS numMinutes
		,coalesce(Activity.ActivityDescription,'') AS vcDetails
		,0
   FROM
   Resource
   INNER JOIN
   ActivityResource
   ON
   Resource.ResourceID = ActivityResource.ResourceID
   INNER JOIN
   Activity
   ON
   ActivityResource.ActivityID = Activity.ActivityID
   WHERE
   Resource.numUserCntId = v_numUserCntID
   AND CAST(Activity.StartDateTimeUtc + make_interval(mins => -v_ClientTimeZoneOffset) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   ORDER BY
   Activity.StartDateTimeUtc;

	-- Work Order Tasks
   INSERT INTO tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME(numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete)
   SELECT
   WO.numWOId
		,4
		,CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,
   '">',coalesce(WO.vcWorkOrderName,'-'),'</a>',(CASE WHEN OM.numOppId > 0 THEN CONCAT(' (<a target="_blank" href="../opportunity/frmOpportunities.aspx?opId=',OM.numOppId,'">',coalesce(OM.vcpOppName,'-'),'</a>)') ELSE '' END),
   (CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),
      DM.numDivisionID,'">',coalesce(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)) AS vcWorkOrderName
		,SPDTTLStart.dtActionTime
		,FormatedDateFromDate(SPDTTLStart.dtActionTime + make_interval(mins => -v_ClientTimeZoneOffset),v_numDomainID) AS vcDate
		,0
		,'Work Order' AS vcType
		,CONCAT(TO_CHAR(coalesce((EXTRACT(DAY FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60*24+EXTRACT(HOUR FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60+EXTRACT(MINUTE FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)),
   0)/60,'00'),':',TO_CHAR(MOD(CAST(coalesce((EXTRACT(DAY FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60*24+EXTRACT(HOUR FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60+EXTRACT(MINUTE FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)),
   0) AS NUMERIC),CAST(60 AS NUMERIC)),'')) AS vcHours
		,coalesce((EXTRACT(DAY FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60*24+EXTRACT(HOUR FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*60+EXTRACT(MINUTE FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)),
   0) AS numMinutes
		,coalesce(SPDT.vcTaskName,'') AS vcDetails
		,0
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   WorkOrder WO
   ON
   SPDT.numWorkOrderId = WO.numWOId
   LEFT JOIN
   OpportunityMaster OM
   ON
   WO.numOppId = OM.numOppId
   LEFT JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   LEFT JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   INNER JOIN
   StagePercentageDetailsTaskTimeLog  SPDTTLStart
   ON
   SPDT.numTaskId = SPDTTLStart.numTaskId
   CROSS JOIN LATERAL(SELECT
      TEMP.dtActionTime
      FROM(SELECT 
         SPDTTLEnd.dtActionTime
         FROM
         StagePercentageDetailsTaskTimeLog  SPDTTLEnd
         WHERE
         SPDTTLEnd.numDomainID = v_numDomainID
         AND SPDTTLEnd.numUserCntID = v_numUserCntID
         AND SPDTTLStart.numTaskID = SPDTTLEnd.numTaskId
         AND SPDTTLEnd.tintAction IN(2,4)
         AND SPDTTLEnd.dtActionTime >= SPDTTLStart.dtActionTime
         ORDER BY
         dtActionTime LIMIT 1) TEMP
      WHERE(EXTRACT(DAY FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)*24+EXTRACT(HOUR FROM TEMP.dtActionTime -SPDTTLStart.dtActionTime)) < 24) TEMP
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND coalesce(SPDT.numWorkOrderId,0) > 0
   AND SPDTTLStart.numDomainID = v_numDomainID
   AND SPDTTLStart.numUserCntID = v_numUserCntID
   AND SPDTTLStart.tintAction IN(1,3)
   AND CAST(SPDTTLStart.dtActionTime + make_interval(mins => -v_ClientTimeZoneOffset) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate;

	RETURN QUERY (SELECT * FROM tt_FN_GETPAYROLLEMPLOYEETIME_EMPLOYEETIME);
END; $$;

