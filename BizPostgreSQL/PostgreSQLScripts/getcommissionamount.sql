-- Function definition script GetCommissionAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCommissionAmount(v_numUserId NUMERIC,v_numUserCntID NUMERIC,v_numDomainId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AssignedAmount  DECIMAL(20,5);                  
   v_OwnerAmount  DECIMAL(20,5);                  
   v_bitCommOwner  BOOLEAN;                  
   v_bitCommAssignee  BOOLEAN;                  
   v_decCommOwner  DECIMAL(10,2);                  
   v_decCommAssignee  DECIMAL(10,2);                  
   v_CommAssignedAmt  DECIMAL(20,5);                  
   v_CommOwnerAmt  DECIMAL(20,5);                  
   v_TotalCommAmt  DECIMAL(20,5);             
   v_bitRoleComm  BOOLEAN;            
   v_CommRoleAmt  DECIMAL(20,5);
BEGIN
   select   Sum(coalesce(monPAmount,0)) INTO v_AssignedAmount From OpportunityMaster Where tintopptype = 1 And tintoppstatus = 1 And numassignedto = v_numUserCntID And numDomainId = v_numDomainId;                
   select   Sum(coalesce(monPAmount,0)) INTO v_OwnerAmount From OpportunityMaster Where tintopptype = 1 And tintoppstatus = 1 And  numrecowner = v_numUserCntID And numDomainId = v_numDomainId;                       
     
   select   bitCommOwner, bitCommAssignee, coalesce(fltCommOwner,0), coalesce(fltCommAssignee,0), bitRoleComm INTO v_bitCommOwner,v_bitCommAssignee,v_decCommOwner,v_decCommAssignee,v_bitRoleComm from UserMaster Where numUserId = v_numUserId;                  
    
    
    
   if v_bitCommOwner = true And v_decCommOwner <> 0 then
      v_CommOwnerAmt := v_OwnerAmount*v_decCommOwner/100;
   end if;                  
                   
   if v_bitCommAssignee = true And v_decCommAssignee <> 0 then
      v_CommAssignedAmt := v_AssignedAmount*v_decCommAssignee/100;
   end if;                  
   if v_bitRoleComm = true then
     
      select   sum(coalesce(monAmountPaid,0)*fltPercentage/100) INTO v_CommRoleAmt from OpportunityMaster Opp
      join OpportunityContact OppCont on Opp.numOppId = OppCont.numOppId
      left join OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId and oppBiz.bitAuthoritativeBizDocs = true
      join UserRoles UR on OppCont.numRole = UR.numRole
      join UserMaster Um on UR.numUserCntID = Um.numUserId and OppCont.numContactID = Um.numUserDetailId Where tintopptype = 1 And Opp.tintoppstatus = 1 And Opp.numDomainId = v_numDomainId And OppCont.numContactID = v_numUserCntID;
   end if;                 
   v_TotalCommAmt := coalesce(v_CommOwnerAmt,0)+coalesce(v_CommAssignedAmt,0)+coalesce(v_CommRoleAmt,0);            
   Return v_TotalCommAmt;
END; $$;

