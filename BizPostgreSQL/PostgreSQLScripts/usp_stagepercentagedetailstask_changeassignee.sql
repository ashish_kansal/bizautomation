-- Stored procedure definition script USP_StagePercentageDetailsTask_ChangeAssignee for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTask_ChangeAssignee(v_numDomainID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_numAssignedTo NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND tintAction = 4) then
	
      RAISE EXCEPTION 'TASK_IS_MARKED_AS_FINISHED';
   ELSEIF coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID ORDER BY tintAction LIMIT 1),0) IN(1,3)
   then
	
      RAISE EXCEPTION 'TASK_IS_ALREADY_STARTED';
   ELSE
      UPDATE StagePercentageDetailsTask SET numAssignTo = v_numAssignedTo WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
   end if;
   RETURN;
END; $$;


