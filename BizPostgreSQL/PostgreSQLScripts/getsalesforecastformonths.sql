-- Function definition script GetSalesForeCastForMonths for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetSalesForeCastForMonths(v_month INTEGER,v_year INTEGER,v_numDomainId NUMERIC(9,0),v_numUserCntID NUMERIC(9,0),v_sintForecast INTEGER)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monOppAmt  DECIMAL(20,5);                
   v_TotalPercentage  DOUBLE PRECISION;                
   v_TotalAmt  DECIMAL(20,5);                
   v_UnclosedDealCount  INTEGER;                
   v_Percentage  DOUBLE PRECISION;
BEGIN
   if v_sintForecast = 1 then

      select   coalesce(monForecastAmount,0) INTO v_TotalAmt from Forecast FC Where tintForecastType = 1 and tintMonth = v_month and  sintYear = v_year and numDomainID = v_numDomainId;
   Else               
---- Select  @monOppAmt=Sum(monPAmount*(Select tintPercentage from OpportunityMaster Opp                
---- inner join  OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId  Where Opp.tintOppType=1 And  OSD.bitstagecompleted=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  Opp.numRecOwner=@numUserCntID 
 --And Opp.tintoppStatus=1                           
---- And Opp.numDomainID=@numDomainId) From OpportunityMaster                 
----    Where tintOppType=1 And month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  numRecOwner=@numUserCntID And tintoppStatus=1 and tintshipped=0    
----    and numDomainID=@numDomainId          
----         
---- Select @TotalPercentage =sum(tintPercentage) from OpportunityMaster Opp                
---- inner join  OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId                
---- Where Opp.tintOppType=1 And  OSD.bitstagecompleted=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  Opp.numRecOwner=@numUserCntID  And Opp.tintoppStatus=1                           
---- And Opp.numDomainID=@numDomainId                
----                
----              
---- Select @UnclosedDealCount=Count(*) From OpportunityMaster where tintOppType=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  numRecOwner=@numUserCntID And tintoppStatus=0                           
----    and numDomainID=@numDomainId                  
----                
---- Set @Percentage=(Case When @TotalPercentage=0 then 1 Else @TotalPercentage End) /(Case When @UnclosedDealCount=0 then 1 Else @UnclosedDealCount End)                
---- ----Print @UnclosedDealCount                
---- ----Print @monOppAmt                
---- ----Print @Percentage                
 --Print @Percentage         
   
      select   Sum(X.Amount) INTO v_monOppAmt From(Select(monPAmount*(Select sum(coalesce(tintPercentage,0))  from OpportunityStageDetails OSD
            Where OSD.numoppid = OpportunityMaster.numOppId And OSD.bitstagecompleted = true))/100 as Amount  From OpportunityMaster
         Where tintopptype = 1 And EXTRACT(month FROM intPEstimatedCloseDate) = v_month and EXTRACT(year FROM intPEstimatedCloseDate) = v_year and  numrecowner = v_numUserCntID And tintoppstatus = 1 and tintshipped = 0
         and numDomainId = v_numDomainId) X;
      v_TotalAmt := coalesce(v_monOppAmt,0);
   end if;            
             
--Print @TotalAmt                
   Return v_TotalAmt;
END; $$;

