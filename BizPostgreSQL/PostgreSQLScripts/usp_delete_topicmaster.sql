-- Stored procedure definition script USP_DELETE_TOPICMASTER for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DELETE_TOPICMASTER(v_numTopicId	NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numTopicId != 0) then
	
      DELETE FROM MessageMaster WHERE numTopicId = v_numTopicId AND numDomainID = v_numDomainID;
      DELETE FROM TOPICMASTER WHERE numTopicId = v_numTopicId 	AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;

	


