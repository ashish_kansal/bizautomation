-- Stored procedure definition script USP_Import_SaveOrganizationDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Import_SaveOrganizationDetails(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_vcCompanyName VARCHAR(100)
	,v_vcProfile NUMERIC(18,0)
	,v_numCompanyType NUMERIC(18,0)
	,v_numNoOfEmployeesId NUMERIC(18,0)
	,v_numAnnualRevID NUMERIC(18,0)
	,v_vcWebSite VARCHAR(255)
	,v_vcHow NUMERIC(18,0)
	,v_numCompanyIndustry NUMERIC(18,0)
	,v_numCompanyRating NUMERIC(18,0)
	,v_numCompanyCredit NUMERIC(18,0)
	,v_txtComments TEXT
	,v_vcWebLink1 VARCHAR(255)
	,v_vcWebLink2 VARCHAR(255)
	,v_vcWebLink3 VARCHAR(255)
	,v_vcWebLink4 VARCHAR(255)
	,v_numCompanyDiff NUMERIC(18,0)
	,v_vcCompanyDiff VARCHAR(100)
	,v_numCampaignID NUMERIC(18,0)
	,v_vcComPhone VARCHAR(50)
	,v_numAssignedTo NUMERIC(18,0)
	,v_numFollowUpStatus NUMERIC(18,0)
	,v_numTerID NUMERIC(18,0)
	,v_numStatusID NUMERIC(18,0)
	,v_vcComFax VARCHAR(50)
	,v_bitPublicFlag BOOLEAN
	,v_bitActiveInActive BOOLEAN
	,v_numGrpID NUMERIC(18,0)
	,v_tintCRMType SMALLINT
	,v_numCurrencyID NUMERIC(18,0)
	,v_bitNoTax BOOLEAN
	,v_numBillingDays NUMERIC(18,0)
	,v_bitOnCreditHold BOOLEAN
	,v_numDefaultExpenseAccountID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ErrorMessage  VARCHAR(4000);
   v_ErrorNumber  TEXT;
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_ErrorLine  INTEGER;
   v_ErrorProcedure  VARCHAR(200);
   v_numCompanyID  NUMERIC(18,0);
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numFollow  NUMERIC(18,0);                     

   v_tempAssignedTo  NUMERIC(18,0);
BEGIN
   BEGIN
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetOrganizationDetails

      IF coalesce(v_numDivisionID,0) > 0 AND EXISTS(SELECT numDivisionID FROM DivisionMaster WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID) then
         -- BEGIN TRANSACTION
select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
         IF coalesce(v_numCompanyID,0) > 0 then
			
            UPDATE
            CompanyInfo
            SET
            vcCompanyName = v_vcCompanyName,vcProfile = v_vcProfile,numCompanyType = v_numCompanyType,
            numNoOfEmployeesId = v_numNoOfEmployeesId,numAnnualRevID = v_numAnnualRevID,
            vcWebSite = v_vcWebSite,vcHow = v_vcHow,numCompanyIndustry = v_numCompanyIndustry,
            numCompanyRating = v_numCompanyRating,numCompanyCredit = v_numCompanyCredit,
            txtComments = v_txtComments,vcWebLink1 = v_vcWebLink1,
            vcWebLink2 = v_vcWebLink2,vcWebLink3 = v_vcWebLink3,vcWebLink4 = v_vcWebLink4,
            numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
            WHERE
            numDomainID = v_numDomainID
            AND numCompanyId = v_numCompanyID;
         end if;
         select   coalesce(numFollowUpStatus,0) INTO v_numFollow FROM
         DivisionMaster WHERE
         numDivisionID = v_numDivisionID
         AND numDomainID = v_numDomainID;
         IF v_numFollow <> v_numFollowUpStatus then
			
            open SWV_RefCur for SELECT   numFollowUpstatus from FollowUpHistory where numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID ORDER BY numFollowUpStatusID DESC LIMIT 1;
            INSERT INTO FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainID)
				VALUES(v_numFollow,v_numDivisionID,TIMEZONE('UTC',now()),v_numDomainID);
         end if;                          
			
			---Updating if organization is assigned to someone            
         select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo FROM DivisionMaster WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
         IF v_tempAssignedTo <> v_numAssignedTo AND  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(18,0)) then
			
            UPDATE DivisionMaster SET numAssignedTo = v_numAssignedTo,numAssignedBy = v_numUserCntID WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
         ELSEIF coalesce(v_numAssignedTo,0) = 0
         then
			
            UPDATE DivisionMaster SET numAssignedTo = 0,numAssignedBy = 0 WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
         end if;
         UPDATE
         DivisionMaster
         SET
         numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
         numCompanyDiff = v_numCompanyDiff,vcCompanyDiff = v_vcCompanyDiff,numCampaignID = v_numCampaignID,
         vcComPhone = v_vcComPhone,numFollowUpStatus = v_numFollowUpStatus,
         numTerID = v_numTerID,numStatusID = v_numStatusID,vcComFax = v_vcComFax,
         bitPublicFlag = v_bitPublicFlag,bitActiveInActive = v_bitActiveInActive,
         numGrpId = v_numGrpID,tintCRMType = v_tintCRMType,numCurrencyID = v_numCurrencyID,
         bitNoTax = v_bitNoTax,numBillingDays = v_numBillingDays,bitOnCreditHold = v_bitOnCreditHold,
         numDefaultExpenseAccountID = v_numDefaultExpenseAccountID
         WHERE
         numDivisionID = v_numDivisionID
         AND numDomainID = v_numDomainID;
         -- COMMIT
end if;
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;












