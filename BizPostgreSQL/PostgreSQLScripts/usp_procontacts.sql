-- Stored procedure definition script USP_ProContacts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProContacts(v_numProId NUMERIC(9,0) DEFAULT 0 ,
v_numDomainId NUMERIC(9,0) DEFAULT 0       
--      
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  D.numDivisionID,a.numContactId,vcCompanyName || ', ' || coalesce(Lst.vcData,'-') as Company,
  COALESCE(a.vcFirstName,'') || ' ' || COALESCE(a.vcLastname,'') as Name,
  COALESCE(a.numPhone,'') || ', ' || COALESCE(a.numPhoneExtension,'') as Phone,a.vcEmail as Email,
  b.vcData as ContactRole,
  pro.numRole as ContRoleId,
  coalesce(bitPartner,false) as bitPartner,
  a.numContactType,CASE WHEN pro.bitSubscribedEmailAlert = true THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM ProjectsContacts pro
   join AdditionalContactsInformation a on
   a.numContactId = pro.numContactID
   join DivisionMaster D
   on a.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   left join Listdetails b
   on b.numListItemID = pro.numRole
   left join Listdetails Lst
   on Lst.numListItemID = C.numCompanyType
   WHERE pro.numProId = v_numProId and a.numDomainID = v_numDomainId;
END; $$;












