-- Stored procedure definition script USP_UpdateSiteStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateSiteStatus(v_numSiteID NUMERIC,
v_bitIsActive BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Sites SET bitIsActive = v_bitIsActive WHERE numSiteID = v_numSiteID;
   RETURN;
END; $$;


