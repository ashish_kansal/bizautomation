-- Stored procedure definition script usp_GetCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCompany(                          
--                        
v_numDomainId NUMERIC DEFAULT 0,                                            
 v_vcCmpFilter VARCHAR(100) DEFAULT '%',                    
 v_numUserCntID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                             
-- checking the company rights                    
                    
   AS $$
   DECLARE
   v_ProspectsRights  SMALLINT;                    
   v_accountsRights  SMALLINT;                   
   v_leadsRights  SMALLINT; 

   v_strSQL  VARCHAR(8000);
BEGIN
   RAISE NOTICE '%',v_vcCmpFilter;
   v_vcCmpFilter := replace(v_vcCmpFilter,'''','|');
   RAISE NOTICE '%',v_vcCmpFilter;
               
   SELECT  MAX(GA.intViewAllowed) As intViewAllowed INTO v_ProspectsRights FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 2 and PM.numModuleID = 3 and UM.numUserId =(select numUserId from UserMaster where numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                        
                    
   SELECT  MAX(GA.intViewAllowed) As intViewAllowed INTO v_accountsRights FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 2 and PM.numModuleID = 4 and UM.numUserId =(select numUserId from UserMaster where numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                
              
   SELECT  MAX(GA.intViewAllowed) As intViewAllowed INTO v_leadsRights FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 2 and PM.numModuleID = 2 and UM.numUserId =(select numUserId from UserMaster where numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;                   
                    
   v_strSQL := 'SELECT a.vcCompanyname || Case when COALESCE(d.numCompanyDiff,0)>0 then  ''  '' || fn_getlistitemname(d.numCompanyDiff) || '':'' || COALESCE(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d                      
   on  a.numCompanyid=d.numCompanyid 
   join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true                  
   WHERE a.numdomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType<>0 and d.tintCRMType=1                       
   AND replace(a.vcCompanyname,'''''''',' || '''|'')ILIKE ''' ||  coalesce(v_vcCmpFilter,'') || '''';     
   if v_ProspectsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=0 ';
   end if;                    
   if v_ProspectsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                     
   if v_ProspectsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)';
   end if;                    
   v_strSQL := coalesce(v_strSQL,'') || ' union SELECT a.vcCompanyname || Case when COALESCE(d.numCompanyDiff,0)>0 then  ''  '' || fn_getlistitemname(d.numCompanyDiff) || '':'' || COALESCE(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d                      
   on  a.numCompanyid=d.numCompanyid
	join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true                     
   WHERE a.numdomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType<>0 and d.tintCRMType=2                      
   AND replace(a.vcCompanyname,'''''''',' || '''|'')ILIKE ''' ||  coalesce(v_vcCmpFilter,'') || '''';     
   if v_accountsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=0 ';
   end if;                    
   if v_accountsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                     
   if v_accountsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)';
   end if;                 
   v_strSQL := coalesce(v_strSQL,'') || ' union SELECT a.vcCompanyname || Case when COALESCE(d.numCompanyDiff,0)>0 then ''  '' || fn_getlistitemname(d.numCompanyDiff) || '':'' || COALESCE(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d            
   on  a.numCompanyid=d.numCompanyid   
	join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND COALESCE(AC.bitPrimaryContact,false)=true                 
   WHERE a.numdomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and d.tintCRMType=0                       
   AND replace(a.vcCompanyname,'''''''',' || '''|'') ILIKE ''' ||  coalesce(v_vcCmpFilter,'') || '''';                     
   if v_leadsRights = 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=0 ';
   end if;                    
   if v_leadsRights = 1 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and d.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                     
   if v_leadsRights = 2 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0)';
   end if;                   
   v_strSQL := coalesce(v_strSQL,'') || ' ORDER BY vcCompanyname';    

   RAISE NOTICE '%',(v_strSQL);
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


