-- Stored procedure definition script USP_UserMaster_GetFieldValue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_UserMaster_GetFieldValue(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_vcFieldName VARCHAR(200),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'tintPayrollType' then
	
      open SWV_RefCur for SELECT coalesce(tintPayrollType,1) FROM UserMaster	WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID;
   end if;
END; $$;












