-- Stored procedure definition script USP_DeleteAlertDtlforPastDueBills for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteAlertDtlforPastDueBills(v_numAlertDTLid NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from AlertDTL where  numAlertDTLid = v_numAlertDTLid;
   RETURN;
END; $$;


