-- Function definition script GetUnitPriceForKitOrAssembly for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetUnitPriceForKitOrAssembly(v_numDomainID NUMERIC(18,0), v_numItemCode NUMERIC(18,0), v_numSelectedWarehouseID NUMERIC(18,0), v_numQuantity INTEGER)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_finalUnitPrice  DOUBLE PRECISION DEFAULT 0;
   v_Count  INTEGER DEFAULT 0;
   v_I  INTEGER DEFAULT 1;
   v_TempItemCode  NUMERIC(18,0);
   v_TempReqQuantity  NUMERIC(18,0);
   v_numWareHouseID  NUMERIC(18,0);
BEGIN
   IF(SELECT coalesce(bitCalAmtBasedonDepItems,false) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode) = false then -- Calculate from kit or assembly warehouse

      select   coalesce(monWListPrice,0) INTO v_finalUnitPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numSelectedWarehouseID;
   ELSE -- Calculate based on child items of kit or assembly
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         ID INTEGER, 
         numItemCode NUMERIC(18,0), 
         numReqQuantity INTEGER, 
         numWareHouseItemID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPTABLE  select ROW_NUMBER() OVER(ORDER BY numItemDetailID DESC), numChildItemID, numQtyItemsReq,(SELECT numWareHouseItemID FROM WareHouseItems WHERE WareHouseItems.numItemID= ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID=(SELECT WI.numWarehouseID FROM WareHouseItems WI WHERE WI.numWareHouseItemID=v_numSelectedWarehouseID) LIMIT 1) numWareHouseItemId from ItemDetails where numItemKitID = v_numItemCode;
      select   COUNT(*) INTO v_Count FROM tt_TEMPTABLE;
      IF v_Count > 0 then
         WHILE v_I <= v_Count LOOP
            select   numItemCode, numReqQuantity, numWareHouseItemID INTO v_TempItemCode,v_TempReqQuantity,v_numWareHouseID FROM tt_TEMPTABLE WHERE ID = v_I;
            IF(SELECT coalesce(charItemType,'') AS charItemType FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_TempItemCode) = 'P' then -- Inventory Item
			
               select   v_finalUnitPrice+(coalesce(monWListPrice,0)*v_TempReqQuantity) INTO v_finalUnitPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_TempItemCode AND numWareHouseItemID = v_numWareHouseID;
            ELSE
               select   v_finalUnitPrice+(coalesce(monListPrice,0)*v_TempReqQuantity) INTO v_finalUnitPrice FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_TempItemCode;
            end if;
            v_I := v_I::bigint+1;
         END LOOP;
      end if;
   end if;

   return v_finalUnitPrice;
END; $$;

