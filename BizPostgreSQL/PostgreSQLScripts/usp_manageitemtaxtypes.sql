-- Stored procedure definition script USP_ManageItemTaxTypes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemTaxTypes(v_numItemCode NUMERIC(9,0),
v_strItemDetails VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   DELETE FROM ItemTax WHERE numItemCode = v_numItemCode;    

   INSERT INTO ItemTax(numItemCode,
		numTaxItemID,
		numTaxID,
		bitApplicable)
   SELECT
   v_numItemCode,
		X.numTaxItemID,
		X.numTaxID,
		X.bitApplicable
   FROM
    XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strItemDetails AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numTaxItemID NUMERIC(18,0) PATH 'numTaxItemID',
				numTaxID NUMERIC(18,0) PATH 'numTaxID',
				bitApplicable BOOLEAN PATH 'bitApplicable'
		) X;  
    
   RETURN;
END; $$;


