-- Stored procedure definition script usp_GetSurveyRespondants for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyRespondants(v_numSurId NUMERIC(9,0) DEFAULT 0,  
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select srm.numSurID, sm.vcSurName, srm.numRespondantID, vcFirstName || ' ' || vcLastname as vcRespondentName,
vcCompanyName,
CASE WHEN coalesce(srm.numRegisteredRespondentContactId,0) = '0' Then 'No' Else 'Yes' End as
   bitAddedToBz,
srm.numSurRating, srm.numRegisteredRespondentContactId ,dtDateofResponse,vcEmail,A.bintCreatedDate
   from SurveyRespondentsMaster srm
   join SurveyMaster sm
   on   srm.numSurID = sm.numSurID
   left join  AdditionalContactsInformation A
   on    numContactId = numRegisteredRespondentContactId  and A.numDomainID = v_numDomainID
   left join DivisionMaster D
   on D.numDivisionID = A.numDivisionId
   left join CompanyInfo C
   on C.numCompanyId = D.numCompanyID
   where  sm.numSurID = srm.numSurID
   and sm.numSurID = v_numSurId  and srm.numDomainId = v_numDomainID;
END; $$;

