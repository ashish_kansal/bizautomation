CREATE OR REPLACE FUNCTION USP_LandedCostVendorsSave(v_numDomainId NUMERIC(18,0),
    v_numVendorId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strMsg  VARCHAR(200);
BEGIN
	IF EXISTS(SELECT 1 FROM LandedCostVendors AS LCV WHERE LCV.numDomainID = v_numDomainId AND LCV.numVendorId = v_numVendorId) then
      RAISE EXCEPTION 'Vendor_Exists';
   ELSE
      INSERT INTO LandedCostVendors(numDomainID, numVendorId)
      SELECT v_numDomainId, v_numVendorId;
   END IF;
END; $$;
