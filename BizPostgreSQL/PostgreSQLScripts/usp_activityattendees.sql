-- Stored procedure definition script USP_ActivityAttendees for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ActivityAttendees(v_strRow TEXT DEFAULT '' ,
	v_numActivityID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc3  INTEGER;
   v_numDomainID  NUMERIC(18,0);
BEGIN
   BEGIN
      IF coalesce(SUBSTR(CAST(v_strRow AS VARCHAR(30)),1,30),'') <> '' AND POSITION('<?xml' IN v_strRow) = 0 then
	
         v_strRow := CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',v_strRow);
      end if;
      IF SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            EmailId VARCHAR(50),
            ResponseStatus VARCHAR(20)
         );
         INSERT INTO tt_TEMP(EmailId,ResponseStatus)
         SELECT
         EmailId,
			ResponseStatus
         FROM
		 XMLTABLE
			(
				'ArrayOfAttendee/Attendee'
				PASSING 
					CAST(v_strRow AS XML)
				COLUMNS
					EmailId VARCHAR(50) PATH 'EmailId',
					ResponseStatus VARCHAR(20) PATH 'ResponseStatus'
			) AS X;

/*-----------------------Validation of balancing entry*/
         Delete From ActivityAttendees where ActivityID = v_numActivityID;
         IF EXISTS(SELECT * FROM tt_TEMP) then
            select   res.numDomainId INTO v_numDomainID FROM
            Activity ac
            INNER JOIN
            ActivityResource ar
            ON
            ac.ActivityID = ar.ActivityID
            INNER JOIN
            Resource res
            ON
            ar.ResourceID = res.ResourceID WHERE
            ac.ActivityID = v_numActivityID;
            INSERT INTO ActivityAttendees(ActivityID,
				ResponseStatus,
				AttendeeEmail,
				AttendeeFirstName,
				AttendeeLastName,
				AttendeePhone,
				AttendeePhoneExtension,
				CompanyNameinBiz,
				DivisionID,
				tintCRMType,
				ContactID)
            SELECT
            v_numActivityID
				,T.ResponseStatus
				,T.EmailId
				,Ac.vcFirstName
				,Ac.vcLastname
				,Ac.numPhone
				,Ac.numPhoneExtension
				,CI.numCompanyId
				,Ac.numDivisionId
				,Div.tintCRMType
				,Ac.numContactId
            From
            tt_TEMP T
            left Join
            AdditionalContactsInformation Ac
            on
            numContactId =(select MAX(numContactId) From AdditionalContactsInformation where vcEmail = T.EmailId AND numDomainID = v_numDomainID)
            left join
            DivisionMaster Div
            on
            Ac.numDivisionId = Div.numDivisionID
            left join
            CompanyInfo CI
            on
            Div.numCompanyID = CI.numCompanyId;
         end if;
      end if;
      EXCEPTION WHEN OTHERS THEN
         NULL;
         -- RollBack
END;
   RETURN;
END; $$;


