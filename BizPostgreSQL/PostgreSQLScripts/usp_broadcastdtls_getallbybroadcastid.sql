-- Stored procedure definition script USP_BroadCastDtls_GetAllByBroadcastID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BroadCastDtls_GetAllByBroadcastID(v_numDomainID NUMERIC(18,0),
 v_numBroadCastID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
  

   open SWV_RefCur for SELECT
		BroadCastDtls.numBroadcastID,
		BroadCastDtls.numBroadCastDtlId,
		ACI.numContactId,
        GetListIemName(ACI.vcDepartment) AS ContactDepartment,
        GetListIemName(ACI.vcCategory) AS ContactCategory,
        ACI.vcGivenName,
        ACI.vcFirstName AS ContactFirstName,
        ACI.vcLastname AS ContactLastName,
        ACI.numDivisionId,
        GetListIemName(ACI.numContactType) AS ContactType,
        GetListIemName(ACI.numTeam) AS ContactTeam,
        ACI.numPhone AS ContactPhone,
        ACI.numPhoneExtension AS ContactPhoneExt,
        ACI.numCell AS ContactCell,
        ACI.numHomePhone AS ContactHomePhone,
        ACI.vcFax AS ContactFax,
        ACI.vcEmail AS ContactEmail,
        ACI.VcAsstFirstName AS AssistantFirstName,
        ACI.vcAsstLastName AS AssistantLastName,
        ACI.numAsstPhone AS ContactAssistantPhone,
        ACI.numAsstExtn AS ContactAssistantPhoneExt,
        ACI.vcAsstEmail AS ContactAssistantEmail,
        CASE WHEN ACI.charSex = 'M' THEN 'Male'
   WHEN ACI.charSex = 'F' THEN 'Female'
   ELSE '-'
   END AS ContactGender,
        ACI.bintDOB,
        GetAge(ACI.bintDOB,TIMEZONE('UTC',now())) AS ContactAge,
        GetListIemName(ACI.vcPosition) AS ContactPosition,
        ACI.txtNotes,
        ACI.numCreatedBy,
        ACI.bintCreatedDate,
        ACI.numModifiedBy,
        ACI.bintModifiedDate,
        ACI.numDomainID,
        ACI.bitOptOut,
        fn_GetContactName(ACI.numManagerID) AS ContactManager,
        ACI.numRecOwner,
        GetListIemName(ACI.numEmpStatus::NUMERIC(9,0)) AS ContactStatus,
        ACI.vcTitle AS ContactTitle,
        ACI.vcAltEmail,
        ACI.vcitemid,
        ACI.vcChangeKey,
        coalesce((SELECT vcECampName
      FROM   ECampaign
      WHERE  numECampaignID = ACI.numECampaignID),'') AS ContactDripCampaign,
        getContactAddress(ACI.numContactId) AS ContactPrimaryAddress,
        C.numCompanyId,
        C.vcCompanyName AS OrganizationName,
        C.numCompanyType,
        C.numCompanyRating,
        C.numCompanyIndustry,
        C.numCompanyCredit,
        C.txtComments,
        C.vcWebSite,
        C.vcWebLabel1,
        C.vcWebLink1,
        C.vcWebLabel2,
        C.vcWebLink2,
        C.vcWebLabel3,
        C.vcWebLink3,
        C.vcWeblabel4,
        C.vcWebLink4,
        C.numAnnualRevID,
        GetListIemName(C.numNoOfEmployeesId) AS OrgNoOfEmployee,
        C.vcHow,
        C.vcProfile,
        C.numCreatedBy,
        C.bintCreatedDate,
        C.numModifiedBy,
        C.bintModifiedDate,
        C.bitPublicFlag,
        C.numDomainID,
        DM.numDivisionID,
        DM.numCompanyID,
        DM.vcDivisionName,
        DM.numGrpId,
        DM.numFollowUpStatus,
        DM.bitPublicFlag,
        DM.numCreatedBy,
        DM.bintCreatedDate,
        DM.numModifiedBy,
        DM.bintModifiedDate,
        DM.tintCRMType,
        DM.numDomainID,
        DM.bitLeadBoxFlg,
        DM.numTerID,
        DM.numStatusID,
        DM.bintLeadProm,
        DM.bintLeadPromBy,
        DM.bintProsProm,
        DM.bintProsPromBy,
        DM.numRecOwner,
        DM.decTaxPercentage,
        DM.tintBillingTerms,
        DM.numBillingDays,
        DM.tintInterestType,
        DM.fltInterest,
        DM.vcComPhone,
        DM.vcComFax,
        DM.numCampaignID,
        DM.numAssignedBy,
        DM.numAssignedTo,
        DM.bitNoTax,
        vcCompanyName || ' ,<br>' || coalesce(AD2.vcStreet,'') || ' <br>'
   || coalesce(AD2.vcCity,'') || ' ,' || coalesce(fn_GetState(AD2.numState),'') || ' ' || coalesce(AD2.vcPostalCode,'')
   || ' <br>' || coalesce(fn_GetListItemName(AD2.numCountry),'') AS OrgShippingAddress ,
		cast((select cast(U.txtSignature as VARCHAR(255)) from UserMaster U where U.numUserDetailId = ACI.numRecOwner) as VARCHAR(255)) as Signature
   FROM
   BroadCastDtls
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   ACI.numContactId = BroadCastDtls.numContactID
   INNER JOIN
   DivisionMaster DM
   ON
   ACI.numDivisionId = DM.numDivisionID
   INNER JOIN
   CompanyInfo C
   ON
   DM.numCompanyID = C.numCompanyId
   LEFT JOIN
   AddressDetails AD2
   ON
   AD2.numDomainID = DM.numDomainID
   AND AD2.numRecordID = DM.numDivisionID
   AND AD2.tintAddressOf = 2
   AND AD2.tintAddressType = 2
   AND AD2.bitIsPrimary = true
   WHERE
   DM.numDomainID = v_numDomainID AND
   BroadCastDtls.numBroadcastID = v_numBroadCastID AND
   ACI.bitOptOut = false;
END; $$;  














