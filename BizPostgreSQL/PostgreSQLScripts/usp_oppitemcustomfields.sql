-- Stored procedure definition script usp_OppItemCustomFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_OppItemCustomFields(v_numRefreshTimeInterval NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tempOppItemTable  VARCHAR(100);                  
   v_tempOrgContactTable  VARCHAR(100);
   v_vcColumnListNew  VARCHAR(4000);                      
   v_vcColumnListNewOpp  VARCHAR(4000);                      
   v_vcColumnListNewItem  VARCHAR(4000);
   v_OppItemTempCustomTableColumns  VARCHAR(4000);                      
   v_OppTempCustomTableQuery  VARCHAR(4000);                      
   v_ItemTempCustomTableQuery  VARCHAR(4000);
   v_vwOppItemTableColumns  VARCHAR(4000);                       
                    
   v_vwOppItemTableColumnsAndDataType  VARCHAR(4000);                       
       
   v_OrgAndContCustomFields  VARCHAR(1000);                    
   v_OrgAndContCustomFieldsAndDataTypes  VARCHAR(1000);                    
   SWV_ExecDyn  VARCHAR(5000);
   v_OppTempCustomTableCreateQuery1st  VARCHAR(4000);                    
   v_OppTempCustomTableCreateQuery2nd  VARCHAR(4000);                    
   v_OppTempCustomTableCreateQuery3rd  VARCHAR(4000);                    
   v_OppItemTempCustomTableDataEntryQuery1st  VARCHAR(4000);                     
   v_OppItemTempCustomTableDataEntryQuery2nd  VARCHAR(4000);                      
   v_OppItemTempCustomTableDataEntryQuery3rd  VARCHAR(4000);                       
   v_OppItemTempCustomTableDataEntryQuery4th  VARCHAR(4000);
BEGIN
   v_tempOppItemTable := 'tt_OPPITEMCUSTOMFIELD CASCADE';                                
   v_tempOrgContactTable := 'tt_ORGCONTACTCUSTOMFIELD CASCADE';                      
                      
   v_vcColumnListNewOpp := fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,1::SMALLINT);                            
 --PRINT 'ColumnListNewOpp: ' + @vcColumnListNewOpp                            
   v_vcColumnListNewItem := fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,1::SMALLINT);                            
 --PRINT 'ColumnListNewItem: ' + @vcColumnListNewItem                            
   IF v_vcColumnListNewOpp Is Not NULL AND v_vcColumnListNewItem Is Not NULL then
      v_vcColumnListNew := coalesce(v_vcColumnListNewOpp,'') ||  ', ' || coalesce(v_vcColumnListNewItem,'');
   ELSEIF v_vcColumnListNewOpp Is NULL AND v_vcColumnListNewItem Is Not NULL
   then
      v_vcColumnListNew := v_vcColumnListNewItem;
   ELSEIF v_vcColumnListNewItem Is NULL AND v_vcColumnListNewOpp Is Not NULL
   then
      v_vcColumnListNew := v_vcColumnListNewOpp;
   end if;                            
                            
 --PRINT @vcColumnListNew                            
                            
   IF NOT EXISTS(SELECT 'x' FROM sysobjects WHERE   table_name = v_tempOppItemTable) then
                              
   --Print 'Table To Be Created'                     
                       
    --EXEC ('SELECT * FROM ' + @tempOppItemTable)              
      v_OppTempCustomTableQuery := fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,0::SMALLINT);
      v_ItemTempCustomTableQuery := fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,0::SMALLINT);
      IF v_OppTempCustomTableQuery Is Not NULL AND v_ItemTempCustomTableQuery Is Not NULL then
         v_OppItemTempCustomTableColumns := coalesce(v_OppTempCustomTableQuery,'') ||  ', ' || coalesce(v_ItemTempCustomTableQuery,'');
      ELSEIF v_OppTempCustomTableQuery Is NULL AND v_ItemTempCustomTableQuery Is Not NULL
      then
         v_OppItemTempCustomTableColumns := v_ItemTempCustomTableQuery;
      ELSEIF v_ItemTempCustomTableQuery Is NULL AND v_OppTempCustomTableQuery Is Not NULL
      then
         v_OppItemTempCustomTableColumns := v_OppTempCustomTableQuery;
      end if;                      
   --PRINT '@OppItemTempCustomTableColumns: ' + @OppItemTempCustomTableColumns                    
                    
      select   Coalesce(coalesce(v_vwOppItemTableColumns,'') || ', ','') || column_name INTO v_vwOppItemTableColumns FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name =(SELECT table_name FROM sysobjects
         WHERE routine_name  = 'vw_OppItemsCustomReport'
         AND xType = 'V')
      AND colid > 4   ORDER BY colid;                     
    --PRINT @vwOppItemTableColumns                 
                    
      select   Coalesce(coalesce(v_vwOppItemTableColumnsAndDataType,'') || ', ','') || c.column_name || ' ' || t.name ||  CASE WHEN t.name <> 'tinyInt' AND t.name <> 'DECIMAL(20,5)' AND t.name <> 'int' Then
         CASE WHEN (c.prec IS NULL OR t.name = 'bit') THEN ''
         ELSE
            '(' || SUBSTR(Cast(c.prec As VARCHAR(30)),1,30) || CASE WHEN c.scale IS NULL THEN ')' ELSE ',' || SUBSTR(Cast(c.scale AS VARCHAR(30)),1,30) || ')' END
         END
      ELSE
         ''
      END INTO v_vwOppItemTableColumnsAndDataType FROM INFORMATION_SCHEMA.COLUMNS c, (SELECT name, type, xtype, xusertype, usertype FROM systypes) t WHERE c.table_name =(SELECT table_name FROM sysobjects
         WHERE routine_name  = 'vw_OppItemsCustomReport'
         AND xType = 'V')                     
   --AND t.type = c.type                    
      AND t.xusertype = c.xusertype
      AND t.usertype = c.usertype
      AND colid > 4   ORDER BY colid;                     
   --PRINT @vwOppItemTableColumnsAndDataType                    
                    
      SWV_ExecDyn := 'usp_OrganizationContactsCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
      select   Coalesce(coalesce(v_OrgAndContCustomFields,'') || ', ','') || column_name, Coalesce(coalesce(v_OrgAndContCustomFieldsAndDataTypes,'') || ', ','') || column_name || ' NVarchar(50)' INTO v_OrgAndContCustomFields,v_OrgAndContCustomFieldsAndDataTypes FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name =(SELECT table_name FROM sysobjects
         WHERE routine_name  = v_tempOrgContactTable
         AND table_type = 'BASE TABLE')
      AND column_name ilike 'CFld%'   ORDER BY colid;
      RAISE NOTICE '%',v_OrgAndContCustomFields;
      v_OppTempCustomTableCreateQuery1st := 'CREATE TABLE ' || coalesce(v_tempOppItemTable,'') || '(numOppId Numeric, vcColumnList NVarchar(2000), CreatedDateTime DateTime, ';
      v_OppTempCustomTableCreateQuery2nd := v_vwOppItemTableColumnsAndDataType;
      v_OppTempCustomTableCreateQuery3rd := CASE WHEN NULLIF(v_OppItemTempCustomTableColumns,'') IS Not NULL THEN ', ' ||  coalesce(v_OppItemTempCustomTableColumns,'') ELSE '' END || CASE WHEN NULLIF(v_OrgAndContCustomFieldsAndDataTypes,'') IS Not NULL THEN ',       
                                              ' || coalesce(v_OrgAndContCustomFieldsAndDataTypes,'') ELSE '' END || ')';
      RAISE NOTICE '%',v_OppTempCustomTableCreateQuery1st;
      RAISE NOTICE '%',v_OppTempCustomTableCreateQuery2nd;
      RAISE NOTICE '%',v_OppTempCustomTableCreateQuery3rd;
      EXECUTE coalesce(v_OppTempCustomTableCreateQuery1st,'') || coalesce(v_OppTempCustomTableCreateQuery2nd,'') || coalesce(v_OppTempCustomTableCreateQuery3rd,'');
      v_OppItemTempCustomTableDataEntryQuery1st := 'INSERT INTO ' || coalesce(v_tempOppItemTable,'') || ' (numOppId, vcColumnList, CreatedDateTime, ' || coalesce(v_vwOppItemTableColumns,'') ||
      CASE WHEN (NULLIF(v_OppTempCustomTableQuery,'') Is Not NULL)  AND (NULLIF(v_ItemTempCustomTableQuery,'') Is Not NULL) THEN
         ', ' || fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,1::SMALLINT) || ', ' || fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,1::SMALLINT)
      WHEN (NULLIF(v_OppTempCustomTableQuery,'') Is NULL AND NULLIF(v_ItemTempCustomTableQuery,'') Is Not NULL) THEN
         ', ' || fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,1::SMALLINT)
      WHEN (NULLIF(v_ItemTempCustomTableQuery,'') Is NULL AND NULLIF(v_OppTempCustomTableQuery,'') Is NOT NULL) THEN
         ', ' || fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,1::SMALLINT)
      ELSE
         ''
      END
      ||
      CASE WHEN (NULLIF(v_OrgAndContCustomFields,'') IS NULL) THEN
         ') '
      ELSE
         ', ' || coalesce(v_OrgAndContCustomFields,'') || ') '
      END;
      v_OppItemTempCustomTableDataEntryQuery2nd := ' SELECT numOppId, ''' || CASE WHEN NULLIF(v_vcColumnListNew,'') IS NOT NULL THEN v_vcColumnListNew ELSE '' END || ''', TIMEZONE(''UTC'',now()), ' || coalesce(v_vwOppItemTableColumns,'');
      IF (v_OppTempCustomTableQuery Is Not NULL)  AND (v_ItemTempCustomTableQuery Is Not NULL) then
         v_OppItemTempCustomTableDataEntryQuery3rd := ', ' || fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,2::SMALLINT)  || ', ' || fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,2::SMALLINT);
      ELSEIF (v_OppTempCustomTableQuery Is NULL AND v_ItemTempCustomTableQuery Is NOT NULL)
      then
         v_OppItemTempCustomTableDataEntryQuery3rd := ', ' || fn_OppAndItemsCustomFieldsColumns(5::SMALLINT,2::SMALLINT);
      ELSEIF (v_ItemTempCustomTableQuery Is NULL AND v_OppTempCustomTableQuery Is Not NULL)
      then
         v_OppItemTempCustomTableDataEntryQuery3rd := ', ' || fn_OppAndItemsCustomFieldsColumns(2::SMALLINT,2::SMALLINT);
      ELSE
         v_OppItemTempCustomTableDataEntryQuery3rd := '';
      end if;
      v_OppItemTempCustomTableDataEntryQuery4th := CASE WHEN (NULLIF(v_OrgAndContCustomFields,'') IS NULL) THEN '' ELSE ', ' || coalesce(v_OrgAndContCustomFields,'') END || '       
                                                      FROM vw_OppItemsCustomReport opp, (SELECT Distinct numDivisionId, numContactId' || CASE WHEN NULLIF(v_OrgAndContCustomFields,'') IS NOT NULL THEN ', ' || coalesce(v_OrgAndContCustomFields,'') ELSE '' END || '       
                                                      FROM  ##OrgContactCustomField) org WHERE opp.numDivisionId = org.numDivisionId AND opp.numContactId = org.numContactId  ORDER BY numOppId, vcItemCode';
      RAISE NOTICE '%',v_OppItemTempCustomTableDataEntryQuery1st;
      RAISE NOTICE '%',v_OppItemTempCustomTableDataEntryQuery2nd;
      RAISE NOTICE '%',v_OppItemTempCustomTableDataEntryQuery3rd;
      RAISE NOTICE '%',v_OppItemTempCustomTableDataEntryQuery4th;
      EXECUTE coalesce(v_OppItemTempCustomTableDataEntryQuery1st,'') || coalesce(v_OppItemTempCustomTableDataEntryQuery2nd,'') || coalesce(v_OppItemTempCustomTableDataEntryQuery3rd,'') || coalesce(v_OppItemTempCustomTableDataEntryQuery4th,'');
   ELSE
      IF (NOT EXISTS(SELECT  vcColumnList FROM tt_OPPITEMCUSTOMFIELD WHERE vcColumnList = v_vcColumnListNew LIMIT 1)) OR ((SELECT  numOppID FROM tt_OPPITEMCUSTOMFIELD WHERE(EXTRACT(DAY FROM TIMEZONE('UTC',now()) -CreatedDateTime)*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -CreatedDateTime)*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -CreatedDateTime)) > v_numRefreshTimeInterval LIMIT 1) > 0) then
   
         SWV_ExecDyn := 'DROP TABLE IF EXISTS ' ||  coalesce(v_tempOppItemTable,'');
         EXECUTE SWV_ExecDyn;
         IF EXISTS(SELECT 'x' FROM sysobjects WHERE   table_name = v_tempOrgContactTable) then
            SWV_ExecDyn := 'DROP TABLE IF EXISTS ' ||  coalesce(v_tempOrgContactTable,'') ||  '/** EDynId 9 1 **/  ';
            EXECUTE SWV_ExecDyn;
         end if;
         SWV_ExecDyn := 'usp_OppItemCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
         EXECUTE SWV_ExecDyn;
      end if;
   end if;
   RETURN;
END; $$;


