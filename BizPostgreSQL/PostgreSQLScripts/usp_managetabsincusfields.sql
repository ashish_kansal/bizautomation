-- Stored procedure definition script USP_ManageTabsInCuSFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTabsInCuSFields(v_byteMode SMALLINT DEFAULT 0,      
v_LocID NUMERIC(9,0) DEFAULT 0,      
v_TabName VARCHAR(50) DEFAULT '',      
v_TabID NUMERIC(9,0) DEFAULT 0  ,    
--@vcURL as varchar(1000)='',  
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_vcURL VARCHAR(100) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  NUMERIC(9,0);
   v_minGroupID  NUMERIC(9,0);
   v_maxGroupID  NUMERIC(9,0);
   v_minTabID  NUMERIC(9,0);
   v_maxTabID  NUMERIC(9,0);
BEGIN
   if v_byteMode = 1 then

      insert into  CFw_Grp_Master(Grp_Name,loc_Id,numDomainID,tintType)
values(v_TabName,v_LocID,v_numDomainID,0);
   end if;      
      
   if v_byteMode = 2 then

      update  CFw_Grp_Master set Grp_Name = v_TabName
      where Grp_id = v_TabID;
   end if;      
      
   if v_byteMode = 3 then
      
--Check if any custom fields associated with subtab 
      IF(SELECT COUNT(*) FROM CFW_Fld_Master WHERE subgrp = v_TabID::VARCHAR) > 0 then

         RAISE EXCEPTION 'CHILD_RECORD_FOUND';
         RETURN;
      end if;
--delete permission
--[tintType]=1=subtab 
      DELETE FROM GroupTabDetails WHERE numTabId = v_TabID AND tintType = 1;
--delete subtabs
      delete from  CFw_Grp_Master
      where Grp_id = v_TabID;
   end if;     
   
   if v_byteMode = 4 then

      insert into  CFw_Grp_Master(Grp_Name,loc_Id,numDomainID,tintType,vcURLF)
values(v_TabName,v_LocID,v_numDomainID,1,v_vcURL);
   end if;
--Added by chintan, this mode will add existing subtabs with tintType=2 which can not be deleted  
   if v_byteMode = 5 then

      insert into  CFw_Grp_Master(Grp_Name,loc_Id,numDomainID,tintType,vcURLF)
values(v_TabName,v_LocID,v_numDomainID,2,v_vcURL);
   end if;
--Added by chintan, this mode will add Default subtabs and give permission to all Roles by default
-- Only required Parameter is DomainID
   IF v_byteMode = 6 then
    
      IF(SELECT COUNT(*) FROM   Domain WHERE  numDomainId = v_numDomainID) > 0 then
        	

			--ContactDetails
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 4,v_TabName := 'Contact Details',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 4,v_TabName := 'Areas of Interest',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 4,v_TabName := 'Opportunities',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 4,v_TabName := 'Survey History',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 4,v_TabName := 'Correspondence',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			--SO
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Deal Details',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Milestones & Stages',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Associated Contacts',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Product/Service',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'BizDocs',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Correspondence',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			--Added By Manish || To add Packaging/Shipping in Orders
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Shipping & Packaging',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			
			--PO
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Deal Details',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Milestones & Stages',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Associated Contacts',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Product/Service',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'BizDocs',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Correspondence',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			--Added By Manish || To add Packaging/Shipping in Orders
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Shipping & Packaging',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			
			-- Case details
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 3,v_TabName := 'Case Details',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 3,v_TabName := 'Associated Contacts',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 3,v_TabName := 'Correspondence',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			-- Project details
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 11,v_TabName := 'Project Details',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 11,v_TabName := 'Milestones And Stages',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 11,v_TabName := 'Associated Contacts',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 11,v_TabName := 'Project Income & Expense',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 11,v_TabName := 'Correspondence',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			-- Leads locid has been changed from 1 to 14
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Lead Detail',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Areas of Interest',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Web Analysis',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Correspondence',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Assets',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Associations',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 14,v_TabName := 'Survey History',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			-- Prospects
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Prospect Details',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Contacts',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Transactions',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Projects',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Default Settings',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Web Analysis',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Correspondence',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Assets',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 12,v_TabName := 'Associations',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			-- Accounts
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Account Details',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Contacts',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Transactions',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Projects',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Cases',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Default Settings',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Web Analysis',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Correspondence',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Assets',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Contracts',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Associations',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			
			--Added By Sachin ||Issue raised by Customer:EasternBikes
			--Reason:To Make default entry for any new subscription
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 13,v_TabName := 'Items',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 15,v_TabName := 'Bought & Sold',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 15,v_TabName := 'Vended',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
			--end of code		
			--Added By Sachin||Issue raised by Customer:Coloroda State Uni
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 2,v_TabName := 'Shipping & Packaging',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 6,v_TabName := 'Shipping & Packaging',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			--end of Code

         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 17,v_TabName := 'Action Items & Meetings',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 17,v_TabName := 'Opportunities & Projects',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 17,v_TabName := 'Cases',v_TabID := 0,
         v_numDomainID := v_numDomainID,v_vcURL := '');
         PERFORM USP_ManageTabsInCuSFields(v_byteMode := 5::SMALLINT,v_LocID := 17,v_TabName := 'Approval Requests',
         v_TabID := 0,v_numDomainID := v_numDomainID,v_vcURL := '');
			/*********Get Auth groups */
			---- Give permission by default
         select   MIN(numGroupID) INTO v_minGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID;
         select   MAX(numGroupID) INTO v_maxGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID;
         WHILE  v_minGroupID <= v_maxGroupID LOOP
            RAISE NOTICE 'GroupID=%',SUBSTR(CAST(v_minGroupID AS VARCHAR(20)),1,20);
            v_i := 0;
            select   MIN(Grp_id) INTO v_minTabID FROM CFw_Grp_Master WHERE numDomainID = v_numDomainID;
            select   max(Grp_id) INTO v_maxTabID FROM CFw_Grp_Master WHERE numDomainID = v_numDomainID;
            WHILE v_minTabID <= v_maxTabID LOOP
               RAISE NOTICE 'TabID=%',SUBSTR(CAST(v_minTabID AS VARCHAR(20)),1,20);
								---------------
               insert into GroupTabDetails(numGroupID,numTabId,bitallowed,numOrder,numRelationShip,tintType)
								VALUES(v_minGroupID,v_minTabID,true,v_i+1,0,1);

								---------------
								
               select   MIN(Grp_id) INTO v_minTabID FROM CFw_Grp_Master WHERE numDomainID = v_numDomainID AND Grp_id > v_minTabID;
            END LOOP;
            select   min(numGroupID) INTO v_minGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID AND numGroupID > v_minGroupID;
         END LOOP;
      end if;
   end if;
END; $$;


