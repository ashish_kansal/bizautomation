-- Stored procedure definition script usp_SaveCustomReportFilterList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveCustomReportFilterList(v_ReportId NUMERIC(9,0) DEFAULT 0,    
v_strFilterList TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete FROM CustReportFilterlist where numCustomReportId = v_ReportId;    

	insert into CustReportFilterlist(numCustomReportId,vcFieldsText,vcFieldsValue,vcFieldsOperator,vcFilterValue,vcFilterValued)
	select v_ReportId,
     vcFilterFieldsText,vcFilterFieldsValue,vcFilterFieldsOperator,vcFilterValue,vcFilterValued
	 FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFilterList AS XML)
		COLUMNS
			id FOR ORDINALITY,
			vcFilterFieldsText VARCHAR(200) PATH 'vcFilterFieldsText',
			vcFilterFieldsValue VARCHAR(200) PATH 'vcFilterFieldsValue',
			vcFilterFieldsOperator VARCHAR(200) PATH 'vcFilterFieldsOperator',
			vcFilterValue VARCHAR(200) PATH 'vcFilterValue',
			vcFilterValued VARCHAR(200) PATH 'vcFilterValued'
	) AS X;  
			

   RETURN;
END; $$;


