-- Stored procedure definition script USP_PromoteLead for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromoteLead(v_numDivisionID NUMERIC,          
 v_numContactID NUMERIC,          
 v_numUserCntID NUMERIC        
         
--          
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_tintCurrentCRMType  SMALLINT;
   v_numCompanyID  NUMERIC(9,0);        
   v_numGroupID  NUMERIC;
BEGIN
   select   numDomainID, tintCRMType INTO v_numDomainID,v_tintCurrentCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;     
	
   INSERT INTO DivisionMasterPromotionHistory(numDomainID
		,numDivisionID
		,tintPreviousCRMType
		,tintNewCRMType
		,dtPromotedBy
		,dtPromotionDate)
	VALUES(v_numDomainID
		,v_numDivisionID
		,v_tintCurrentCRMType
		,1
		,v_numUserCntID
		,TIMEZONE('UTC',now()));


   
   UPDATE DivisionMaster
   SET tintCRMType = 1,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
   bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID
   WHERE numDivisionID = v_numDivisionID;          
   select   numCompanyID INTO v_numCompanyID from DivisionMaster where numDivisionID = v_numDivisionID;        
   UPDATE AdditionalContactsInformation
   set numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE numContactId = v_numContactID;       
   
  
   select   numGroupID INTO v_numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID AND tintGroupType = 2    LIMIT 1;
   IF v_numGroupID IS NULL then 
      v_numGroupID := 0;
   end if;    
	  
   insert into ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)
   values(v_numCompanyID,v_numDivisionID,v_numGroupID,v_numDomainID);
RETURN;
END; $$;


