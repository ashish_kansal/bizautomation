-- Stored procedure definition script USP_ReportListMaster_PrebuildScoreCard for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_PrebuildScoreCard(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_FinancialYearStartDate  TIMESTAMP;
   v_dtCYFromDate  TIMESTAMP;                                       
   v_dtCYToDate  TIMESTAMP;
   v_dtMonthFromDate  TIMESTAMP;                                       
   v_dtMonthToDate  TIMESTAMP;
BEGIN
   v_FinancialYearStartDate := GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP)::INTEGER,v_numDomainID);
   v_dtCYFromDate := v_FinancialYearStartDate;
   v_dtCYToDate := '1900-01-01':: date + make_interval(days => datediff('day', '1900-01-01':: TIMESTAMP, now():: TIMESTAMP)+1) + make_interval(secs => -0.003);

   v_dtMonthFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))*12+DATE_PART('month',AGE(TIMEZONE('UTC',now()),'1900-01-01':: DATE))) || 'month' as interval);
   v_dtMonthToDate := TIMEZONE('UTC',now());
	
	

   DROP TABLE IF EXISTS tt_TABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TABLE
   (
      ID INTEGER,
      vcReport VARCHAR(100),
      vcObjectsOfMeasure VARCHAR(100),
      vcPeriod VARCHAR(100),
      numChange NUMERIC(18,2)
   );

   INSERT INTO tt_TABLE(ID) VALUES(1),(2),(3),(4),(5),(6),(7);

	
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInLeadToAccountConversion(v_numDomainID,v_dtCYFromDate::DATE,v_dtCYToDate::DATE,1::SMALLINT) WHERE ID = 1;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInLeadToAccountConversion(v_numDomainID,v_dtMonthFromDate::DATE,v_dtMonthToDate::DATE,2::SMALLINT) WHERE ID = 2;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInGrossProfit(v_numDomainID,v_dtCYFromDate::DATE,v_dtCYToDate::DATE,1::SMALLINT) WHERE ID = 3;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInGrossProfit(v_numDomainID,v_dtMonthFromDate::DATE,v_dtMonthToDate::DATE,2::SMALLINT) WHERE ID = 4;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInSalesOppToOrderConversion(v_numDomainID,v_dtCYFromDate::DATE,v_dtCYToDate::DATE,1::SMALLINT) WHERE ID = 5;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInSalesOppToOrderConversion(v_numDomainID,v_dtCYFromDate::DATE,v_dtCYToDate::DATE,2::SMALLINT) WHERE ID = 6;
   UPDATE tt_TABLE SET numChange = GetPercentageChangeInProjectAverageGrossProfit(v_numDomainID,v_dtCYFromDate::DATE,v_dtCYToDate::DATE) WHERE ID = 7;

   open SWV_RefCur for SELECT * FROM tt_TABLE;
END; $$;












