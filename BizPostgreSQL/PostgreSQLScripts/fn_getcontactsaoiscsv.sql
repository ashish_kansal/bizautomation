CREATE OR REPLACE FUNCTION fn_GetContactsAOIsCSV(v_numContactID NUMERIC)
RETURNS VARCHAR(250) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_abc  VARCHAR(250);
BEGIN
   v_abc := '';
   select STRING_AGG(CAST(numAOIID AS VARCHAR),',') INTO v_abc FROM AOIContactLink WHERE numContactID = v_numContactID;
   RETURN v_abc;
END; $$;

