-- Stored procedure definition script USP_AddCompetitor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddCompetitor(v_numDivisionId NUMERIC(9,0),      
v_numDomainID NUMERIC(9,0),    
v_numItemCode NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if not exists(select * from  Competitor where numCompetitorID = v_numDivisionId and numItemCode = v_numItemCode and numDomainID = v_numDomainID) then

      insert into Competitor(numCompetitorID,vcPrice,numDomainID,dtDateEntered,numItemCode)
values(v_numDivisionId,'',v_numDomainID,TIMEZONE('UTC',now()),v_numItemCode);
   end if;
   RETURN;
END; $$;


