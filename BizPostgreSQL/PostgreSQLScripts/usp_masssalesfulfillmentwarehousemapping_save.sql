-- Stored procedure definition script USP_MassSalesFulfillmentWarehouseMapping_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWarehouseMapping_Save(v_numDomainID NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0)
	,v_numCountryID NUMERIC(18,0)
	,v_vcStateIDs TEXT
	,v_vcWarehousePriorities TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ID  NUMERIC(18,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numStateID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP(numStateID)
   SELECT
   Id
   FROM
   SplitIDs(v_vcStateIDs,',');


   IF EXISTS(SELECT
   MassSalesFulfillmentWMState.ID
   FROM
   MassSalesFulfillmentWM
   INNER JOIN
   MassSalesFulfillmentWMState
   ON
   MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
   WHERE
   numDomainID = v_numDomainID
   AND numOrderSource = v_numOrderSource
   AND numStateID IN(SELECT numStateID FROM tt_TEMP)) then
	
      RAISE EXCEPTION 'DUPLICATE_STATE';
      RETURN;
   ELSE
      INSERT INTO MassSalesFulfillmentWM(numDomainID
			,numOrderSource
			,numCountryID
			,vcStateIDs)
		VALUES(v_numDomainID
			,v_numOrderSource
			,v_numCountryID
			,v_vcStateIDs);
		
		v_ID := CURRVAL('MassSalesFulfillmentWM_seq');

      IF coalesce(v_vcWarehousePriorities,'') <> '' then
         INSERT INTO MassSalesFulfillmentWP(numMSFWMID
				,numWarehouseID
				,intOrder)
         SELECT
         numMSFWMID
				,numWarehouseID
				,intOrder
         FROM
		 XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcWarehousePriorities AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numMSFWMID NUMERIC(18,0) PATH 'numMSFWMID',
				numWarehouseID NUMERIC(18,0) PATH 'numWarehouseID',
				intOrder INTEGER PATH 'intOrder'
		) AS X;
   
      end if;
   end if;
END; $$;


