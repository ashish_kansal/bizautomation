-- Stored procedure definition script USP_WorkflowAlertList_SelectAll for PostgreSQL
Create or replace FUNCTION USP_WorkflowAlertList_SelectAll(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
 -- Add the parameters for the stored procedure here  
   AS $$
BEGIN
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   
  
    -- Insert statements for procedure here  
   open SWV_RefCur for SELECT  Row_Number() over(order by a.numWFAlertID) as "Number", a.numWFAlertID as "numWFAlertID", a.vcAlertMessage as "vcAlertMessage",a.numDomainID as "numDomainID",a.intAlertStatus as "intAlertStatus",a.numWFID as "numWFID"  FROM WorkFlowAlertList AS a   WHERE a.numDomainID = v_numDomainId AND a.intAlertStatus = 1;
END; $$;  
  
  












