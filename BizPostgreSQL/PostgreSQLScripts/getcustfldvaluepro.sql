-- Function definition script GetCustFldValuePro for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldValuePro(v_numFldId NUMERIC(9,0),
	v_numRecordId NUMERIC(9,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT;
   v_fld_Type  VARCHAR(50);
   v_numListID  NUMERIC(9,0);
BEGIN
   v_vcValue := '';

   select   numlistid, fld_type INTO v_numListID,v_fld_Type FROM
   CFW_Fld_Master WHERE
   Fld_id = v_numFldId;

   select   coalesce(Fld_Value,'') INTO v_vcValue FROM
   CFW_Fld_Values_Pro WHERE
   Fld_ID = v_numFldId
   AND RecId = v_numRecordId;
    
   IF (v_fld_Type = 'TextBox' or v_fld_Type = 'TextArea') then
    
      IF v_vcValue = '' then 
         v_vcValue := '-';
      end if;
   ELSEIF v_fld_Type = 'SelectBox'
   then
    
      IF v_vcValue = '' then
         v_vcValue := '-';
      ELSEIF SWF_IsNumeric(v_vcValue) THEN
         v_vcValue := GetListIemName(CAST(v_vcValue AS NUMERIC));
      end if;
   ELSEIF v_fld_Type = 'CheckBox'
   then
	
      v_vcValue :=(CASE WHEN cast(NULLIF(v_vcValue,'') as INTEGER) = 0 THEN 'No' ELSE 'Yes' END);
   ELSEIF v_fld_Type = 'CheckBoxList'
   then
	
      v_vcValue := COALESCE((SELECT string_agg(vcData,',')
      FROM
      Listdetails
      WHERE
      numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(v_vcValue,','))),'');
   end if;

   IF v_vcValue IS NULL then 
      v_vcValue := '';
   end if;

   RETURN v_vcValue;
END; $$;

