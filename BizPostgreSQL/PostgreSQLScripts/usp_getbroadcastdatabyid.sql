-- Stored procedure definition script USP_GetBroadcastDataByID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBroadcastDataByID(v_numDomainId NUMERIC(18,0),
v_numBroadCastId NUMERIC(18,0) DEFAULT 0,
v_numSearchID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numBroadCastId > 0 then
    
      open SWV_RefCur for
      select B.*,coalesce(T.numCategoryId,0) AS numCategoryId,coalesce(T.bitLastFollowupUpdate,false) AS bitLastFollowupUpdate
      from Broadcast AS B LEFT JOIN GenericDocuments AS T ON B.numEmailTemplateID = T.numGenericDocID where numBroadCastId = v_numBroadCastId and B.numDomainID = v_numDomainId;
   ELSE
      open SWV_RefCur for
      select B.*,coalesce(T.numCategoryId,0) AS numCategoryId,coalesce(T.bitLastFollowupUpdate,false) AS bitLastFollowupUpdate
      from Broadcast AS B LEFT JOIN GenericDocuments AS T ON B.numEmailTemplateID = T.numGenericDocID where numSearchID = v_numSearchID and B.numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;






