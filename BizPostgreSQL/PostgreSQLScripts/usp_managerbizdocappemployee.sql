-- Stored procedure definition script USP_ManagerBizDocAppEMployee for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManagerBizDocAppEMployee(v_numBizDocAppId NUMERIC(18,0),
v_numEmployeeId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO BizDocApprovalRuleEmployee  SELECT v_numBizDocAppId,v_numEmployeeId;

open SWV_RefCur for SELECT cast(CURRVAL('BizDocApprovalRuleEmployee_seq') as VARCHAR(255));
END; $$;












