-- Stored procedure definition script USP_InsertAttachment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertAttachment(v_vcFileName VARCHAR(2000) DEFAULT '',
v_vcAttachmentItemId VARCHAR(2000) DEFAULT '',
v_vcAttachmentType VARCHAR(500) DEFAULT '',
v_numEmailHSTRID NUMERIC(9,0) DEFAULT NULL,
v_NewAttachmentName VARCHAR(500) DEFAULT NULL,
v_vcSize VARCHAR(1000) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_NewvcFileName  VARCHAR(2000);
   v_NewvcAttachmentItemId  VARCHAR(2000);
   v_NewvcAttachmentType  VARCHAR(500);
--DECLARE @numEmailHSTRID numeric(9)
   v_SNewAttachmentName  VARCHAR(500);
--DECLARE @NewExtensionType VARCHAR(100)
   v_NewIcon  VARCHAR(1000);
   v_NewvcSize  VARCHAR(1000);
BEGIN
   v_vcFileName := coalesce(v_vcFileName,'') || '|';
   v_vcAttachmentType := coalesce(v_vcAttachmentType,'') || '|';
   v_NewAttachmentName := coalesce(v_NewAttachmentName,'') || '|';
   v_vcSize := coalesce(v_vcSize,'') || '|';
   WHILE POSITION('|' IN v_vcFileName) > 0 LOOP
      v_NewvcFileName := LTRIM(RTRIM(SUBSTR(v_vcFileName,1,POSITION('|' IN v_vcFileName))));
      v_NewvcAttachmentType := LTRIM(RTRIM(SUBSTR(v_vcAttachmentType,1,POSITION('|' IN v_vcAttachmentType))));
      v_SNewAttachmentName := LTRIM(RTRIM(SUBSTR(v_NewAttachmentName,1,POSITION('|' IN v_NewAttachmentName))));
--        SET @NewExtensionType = LTRIM(RTRIM(SUBSTRING(@ExtensionType, 1,
--                                                      CHARINDEX('|', @ExtensionType, 0))))
      v_NewvcSize := LTRIM(RTRIM(SUBSTR(v_vcSize,1,POSITION('|' IN v_vcSize))));
      v_NewvcFileName := REPLACE(v_NewvcFileName,'|','');
      v_NewvcAttachmentType := REPLACE(v_NewvcAttachmentType,'|','');
      v_SNewAttachmentName := REPLACE(v_SNewAttachmentName,'|','');
        --SET @NewExtensionType = REPLACE(@NewExtensionType, '|', '')
      v_NewvcSize := REPLACE(v_NewvcSize,'|','');
        
--        PRINT 'NewvcFileName :' + @NewvcFileName 
--        PRINT 'NewvcAttachmentType :' + @NewvcAttachmentType 
--        PRINT 'SNewAttachmentName  :' + @SNewAttachmentName 
--        PRINT 'NewExtensionType :' + @NewExtensionType 
--        PRINT 'NewvcSize:' + @NewvcSize 

      insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType,vcLocation,vcSize)
values(v_numEmailHSTRID,v_NewvcFileName,v_vcAttachmentItemId,v_NewvcAttachmentType,v_SNewAttachmentName,v_NewvcSize);
        
      v_vcFileName := LTRIM(RTRIM(SUBSTR(v_vcFileName,POSITION('|' IN v_vcFileName)+1,LENGTH(v_vcFileName))));
      v_vcAttachmentType := LTRIM(RTRIM(SUBSTR(v_vcAttachmentType,POSITION('|' IN v_vcAttachmentType)+1,LENGTH(v_vcAttachmentType))));
      v_NewAttachmentName := LTRIM(RTRIM(SUBSTR(v_NewAttachmentName,POSITION('|' IN v_NewAttachmentName)+1,LENGTH(v_NewAttachmentName))));
--        SET @ExtensionType = LTRIM(RTRIM(SUBSTRING(@ExtensionType,
--                                                   CHARINDEX('|',
--                                                             @ExtensionType)
--                                                   + 1, LEN(@ExtensionType))))
      v_vcSize := LTRIM(RTRIM(SUBSTR(v_vcSize,POSITION('|' IN v_vcSize)+1,LENGTH(v_vcSize))));
   END LOOP;
   RETURN;
END; $$;
--insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType,[vcLocation])
--values(@numEmailHSTRID,@vcFileName,@vcAttachmentItemId,@vcAttachmentType,@NewAttachmentName)


