-- Stored procedure definition script USP_EmailDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EmailDetails(v_numEmailHstrID NUMERIC(9,0),              
v_bintCreatedOn TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select GetEmaillAdd(numEmailHstrID,4::SMALLINT) as vcMessageFrom,
GetEmaillAdd(numEmailHstrID,1::SMALLINT) as vcMessageTo,
GetEmaillAdd(numEmailHstrID,2::SMALLINT) as vcCC,
GetEmaillAdd(numEmailHstrID,3::SMALLINT) as vcBCC,
coalesce(vcSubject,'') as vcSubject,
coalesce(vcBody,'') as vcBody,chrSource,tintType,numUid,numUserCntId from EmailHistory
   where numEmailHstrID = v_numEmailHstrID;
END; $$;












