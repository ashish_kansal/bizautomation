CREATE OR REPLACE FUNCTION USP_AdvancedSearch_GetListDetails(v_numListID NUMERIC(18,0)                      
	,v_vcItemType CHAR(3)                      
	,v_numDomainID NUMERIC(18,0)                    
	,v_vcSearchText VARCHAR(300)
	,v_intOffset INTEGER
	,v_intPageSize INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcItemType = 'SYS' then
	
      open SWV_RefCur for
      SELECT
      numItemID as "id"
			,vcItemName as "text"
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT 0 AS numItemID,'Lead' AS vcItemName
         UNION ALL
         SELECT 1 AS numItemID,'Prospect' AS vcItemName
         UNION ALL
         SELECT 2 AS numItemID,'Account' AS vcItemName) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   end if;
	
   IF v_vcItemType = 'LI' AND v_numListID = 9 then --Opportunity Source
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numItemID VARCHAR(50),
         vcItemName VARCHAR(100), 
         constFlag BOOLEAN, 
         bitDelete BOOLEAN,
         intSortOrder INTEGER, 
         numListItemID NUMERIC(9,0),
         vcData VARCHAR(100), 
         vcListType VARCHAR(1000), 
         vcOrderType VARCHAR(50), 
         vcOppOrOrder VARCHAR(20), 
         numListType NUMERIC(18,0), 
         tintOppOrOrder SMALLINT
      );
      INSERT INTO tt_TEMP
      SELECT CAST(CAST('0~1' AS VARCHAR(250)) AS VARCHAR(50)) AS numItemID,CAST('Internal Order' AS VARCHAR(100)) AS vcItemName, true AS  constFlag, true AS  bitDelete, CAST(0 AS INTEGER) AS intSortOrder, -1 AS numListItemID,CAST('Internal Order' AS VARCHAR(100)),CAST('Internal Order' AS VARCHAR(1000)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20)),CAST(0 AS NUMERIC(18,0)),CAST(0 AS SMALLINT)
      UNION
      SELECT CAST(CAST(numSiteID AS VARCHAR(250)) || '~2' AS VARCHAR(50)) AS numItemID,vcSiteName AS vcItemName, true AS  constFlag, true AS  bitDelete, CAST(0 AS INTEGER) AS intSortOrder, CAST(CAST(numSiteID AS VARCHAR(18)) AS INTEGER) AS numListItemID,vcSiteName,Sites.vcSiteName,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20)),CAST(0 AS NUMERIC(18,0)),CAST(0 AS SMALLINT)
      FROM Sites WHERE numDomainID = v_numDomainID
      UNION
      SELECT DISTINCT CAST(CAST(WebApiId  AS VARCHAR(250)) || '~3' AS VARCHAR(50)) AS numItemID,vcProviderName AS vcItemName,true AS constFlag, true AS bitDelete, CAST(0 AS INTEGER) AS intSortOrder, CAST(CAST(WebApiId  AS VARCHAR(18)) AS INTEGER) AS numListItemID,vcProviderName,WebAPI.vcProviderName,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20)),CAST(0 AS NUMERIC(18,0)),CAST(0 AS SMALLINT)
      FROM WebAPI
      UNION
      SELECT CAST(CAST(Ld.numListItemID  AS VARCHAR(250)) || '~1' AS VARCHAR(50)) AS numItemID, coalesce(vcRenamedListName,vcData)  AS vcItemName, Ld.constFlag,bitDelete,coalesce(intSortOrder,0) AS intSortOrder,CAST(CAST(Ld.numListItemID  AS VARCHAR(18)) AS INTEGER),coalesce(vcRenamedListName,vcData) as vcData,
		CAST((SELECT vcData FROM Listdetails WHERE numListID = 9 and numListItemId = Ld.numListType AND Listdetails.numDomainid = v_numDomainID) AS VARCHAR(1000)),CAST((CASE when Ld.numListType = 1 then 'Sales' when Ld.numListType = 2 then 'Purchase' else 'All' END) AS VARCHAR(50))
		,CAST((CASE WHEN Ld.tintOppOrOrder = 1 THEN 'Opportunity' WHEN Ld.tintOppOrOrder = 2 THEN 'Order' ELSE 'All' END) AS VARCHAR(20)),numListType,tintOppOrOrder
      FROM Listdetails Ld  left join listorder LO
      on Ld.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = 9 and (constFlag = true or Ld.numDomainid = v_numDomainID);
      open SWV_RefCur for
      SELECT
      numItemID as "id"
			,vcItemName as "text"
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      tt_TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'LI'
   then    --Master List                    
	
      open SWV_RefCur for
      SELECT
      LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Listdetails LD
      LEFT JOIN
      listorder LO
      ON
      LD.numListItemID = LO.numListItemID
      AND LO.numDomainId = v_numDomainID
      WHERE
      LD.numListID = v_numListID
      AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true)
      AND (LD.vcData ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      coalesce(intSortOrder,LD.sintOrder);
   ELSEIF v_vcItemType = 'L' AND coalesce(v_numListID,0) > 0
   then    --Master List                    
	
      open SWV_RefCur for
      SELECT
      LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Listdetails LD
      LEFT JOIN
      listorder LO
      ON
      LD.numListItemID = LO.numListItemID
      AND LO.numDomainId = v_numDomainID
      WHERE
      LD.numListID = v_numListID
      AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true)
      AND (LD.vcData ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      coalesce(intSortOrder,LD.sintOrder);
   ELSEIF v_vcItemType = 'T'
   then    --Territories                    
	
      open SWV_RefCur for
      SELECT
      vcData AS text
			,numListItemID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Listdetails
      WHERE
      numListID = 78
      AND numDomainid = v_numDomainID
      AND (vcData ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcData;
   ELSEIF v_vcItemType = 'AG'
   then   --Lead Groups                     
	
      open SWV_RefCur for
      SELECT
      vcGrpName AS text
			,numGrpId AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Groups
      WHERE
			(vcGrpName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcGrpName;
   ELSEIF v_vcItemType = 'S'
   then    --States                    
	
      open SWV_RefCur for
      SELECT
      vcState AS text
			,numStateID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      State
      WHERE
      numDomainID = v_numDomainID
      AND (vcState ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcState;
   ELSEIF v_vcItemType = 'U'
   then    --Users                    
	
      open SWV_RefCur for
      SELECT
      numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT
         A.numContactId AS numItemID
				,A.vcFirstName || ' ' || A.vcLastname AS vcItemName
         FROM
         UserMaster UM
         JOIN
         AdditionalContactsInformation A
         ON
         UM.numUserDetailId = A.numContactId
         WHERE
         UM.numDomainID = v_numDomainID
         AND ((vcFirstName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR vcLastname ilike '%' || coalesce(v_vcSearchText,'') || '%') OR coalesce(v_vcSearchText,'') = '')
         UNION
         SELECT
         A.numContactId
				, vcCompanyName || ' - ' || A.vcFirstName || ' ' || A.vcLastname
         FROM AdditionalContactsInformation A
         join DivisionMaster D
         on D.numDivisionID = A.numDivisionId
         join ExtarnetAccounts E
         on E.numDivisionID = D.numDivisionID
         join ExtranetAccountsDtl DTL
         on DTL.numExtranetID = E.numExtranetID
         join CompanyInfo C
         on C.numCompanyId = D.numCompanyID
         where A.numDomainID = v_numDomainID and bitPartnerAccess = true
         and D.numDivisionID <>(select numDivisionId from Domain where numDomainId = v_numDomainID)) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'C'
   then    --Organization Campaign                    
	
      open SWV_RefCur for
      SELECT
      numCampaignID AS id
			,vcCampaignName || CASE bitIsOnline WHEN true THEN ' (Online)' ELSE ' (Offline)' END AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      CampaignMaster
      WHERE
      numDomainID = v_numDomainID
      AND (vcCampaignName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcCampaignName;
   ELSEIF v_vcItemType = 'IG'
   then
	
      open SWV_RefCur for
      SELECT
      vcItemGroup AS text
			,numItemGroupID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      ItemGroups
      WHERE
      numDomainID = v_numDomainID
      AND (vcItemGroup ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemGroup;
   ELSEIF v_vcItemType = 'PP'
   then
	
      open SWV_RefCur for
      SELECT
      numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst
         UNION
         SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst
         UNION
         SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'V'
   then
	
      open SWV_RefCur for
      SELECT
      coalesce(C.vcCompanyName,'') AS text
			,numVendorID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Vendor V
      INNER JOIN
      DivisionMaster DM
      ON
      DM.numDivisionID = V.numVendorID
      INNER JOIN
      CompanyInfo C
      ON
      C.numCompanyId = DM.numCompanyID
      WHERE
      V.numDomainID = v_numDomainID
      AND (vcCompanyName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      C.vcCompanyName;
   ELSEIF v_vcItemType = 'OC'
   then
	
      open SWV_RefCur for
      SELECT DISTINCT
      C.vcCurrencyDesc AS text
			,C.numCurrencyID AS id
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Currency C
      WHERE
      C.numDomainId = v_numDomainID
      AND (vcCurrencyDesc ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      AND C.numCurrencyID IN(SELECT coalesce(numCurrencyID,0) FROM OpportunityMaster WHERE numDomainId = v_numDomainID)
      ORDER BY
      vcCurrencyDesc;
   ELSEIF v_vcItemType = 'UOM'
   then    --Organization Campaign                    
	
      open SWV_RefCur for
      SELECT
      u.numUOMId as id
			,u.vcUnitName as text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      UOM u
      INNER JOIN
      Domain d
      ON
      u.numDomainID = d.numDomainId
      WHERE
      u.numDomainID = v_numDomainID
      AND d.numDomainId = v_numDomainID
      AND u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END)
      AND (vcUnitName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcUnitName;
   ELSEIF v_vcItemType = 'O'
   then    --Opp Type                   
	
      open SWV_RefCur for
      SELECT
      numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT  1 AS numItemID,'Sales' AS vcItemName
         UNion ALL
         SELECT  2 AS numItemID,'Purchase' AS vcItemName) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'OT'
   then    --Opp Type                   
	
      open SWV_RefCur for
      SELECT
      numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT  1 AS numItemID,'Sales Opportunity' AS vcItemName
         UNION ALL
         SELECT  2 AS numItemID,'Purchase Opportunity' AS vcItemName
         UNION ALL
         SELECT  3 AS numItemID,'Sales Order' AS vcItemName
         UNION ALL
         SELECT  4 AS numItemID,'Purchase Order' AS vcItemName) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'CHK'
   then    --Opp Type                   
	
      open SWV_RefCur for
      SELECT
      numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM(SELECT  1 AS numItemID,'Yes' AS vcItemName
         UNION ALL
         SELECT  0 AS numItemID,'No' AS vcItemName) TEMP
      WHERE
			(vcItemName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcItemName;
   ELSEIF v_vcItemType = 'COA'
   then
	
      open SWV_RefCur for
      SELECT
      C.numAccountId AS id
			,coalesce(C.vcAccountName,'') AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM    Chart_Of_Accounts C
      INNER JOIN AccountTypeDetail ATD
      ON C.numParntAcntTypeID = ATD.numAccountTypeID
      WHERE   C.numDomainId = v_numDomainID
      AND (vcAccountName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY C.vcAccountCode;
   ELSEIF v_vcItemType = 'IC'
   then
	
      open SWV_RefCur for
      SELECT
      numCategoryID AS id
			,coalesce(vcCategoryName,'') AS text
			,COUNT(*) OVER() AS "numTotalRecords"
      FROM
      Category
      WHERE
      numDomainID = v_numDomainID
      AND (vcCategoryName ilike '%' || coalesce(v_vcSearchText,'') || '%' OR coalesce(v_vcSearchText,'') = '')
      ORDER BY
      vcCategoryName;
   end if;
   RETURN;
END; $$;       


