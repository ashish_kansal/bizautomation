-- FUNCTION: public.usp_getbizdocrule(integer, refcursor)

-- DROP FUNCTION public.usp_getbizdocrule(integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbizdocrule(
	v_numdomainid integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:10:10 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT cast(BZT.vcData as VARCHAR(255)) as BizDocType,cast(numRangeFrom as VARCHAR(255)),cast(numRangeTo as VARCHAR(255)),
		cast(numRangeFrom  as VARCHAR(50)) || ' to '  || cast(numRangeTo  as VARCHAR(50)) as Range,
		case btBizDocCreated when TRUE then 'The moment BizDoc is created ' else '' end  ||
   case btBizDocPaidFull when TRUE then 'When the BizDoc has been paid in full ' else '' end ||
   ' AND create an action item with the following "type" value' || ACT.vcData ||
   'for each approver (requesting approval or declination).' ||
   case numApproveDocStatusID when 0 then '' else
      ' And  WHEN ALL the approvers have approved the BizDoc (and none have declined) change the BiizDoc Status value to "' ||
      ADS.vcData || '".' end ||
   case numDeclainDocStatusID when 0 then '' else ' If any approver decline the BizDoc change the status value to "'
      || DDS.vcData  || '".' end		as Condition,
		fn_GetEmployeeName(numBizDocAppID,v_numDomainID) AS Employee,
		cast(btBizDocCreated as VARCHAR(255)),cast(btBizDocPaidFull as VARCHAR(255)),
		cast(ACT.vcData as VARCHAR(255)) As "Action Type",
		cast(ADS.vcData as VARCHAR(255)) AS "Approve Status",
		cast(DDS.vcData as VARCHAR(255)) as "Decline Status",
		cast(numBizDocTypeID as VARCHAR(255)),
		cast(numActionTypeID as VARCHAR(255)),
		cast(numApproveDocStatusID as VARCHAR(255)),
		cast(numDeclainDocStatusID as VARCHAR(255)),
		fn_GetEmployeeID(numBizDocAppID,v_numDomainID) as EmployeeID,
		cast(numBizDocAppID as VARCHAR(255))
   FROM(SELECT * FROM BizDocApprovalRule where numDomainID = v_numDomainID) BAR
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) BZT
   ON
   BZT.numListItemID = BAR.numBizDocTypeID
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) ACT
   ON
   ACT.numListItemID = BAR.numActionTypeID
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) ADS
   ON
   ADS.numListItemID = BAR.numApproveDocStatusID
   INNER JOIN(SELECT * FROM View_GetMasterListItems WHERE (constFlag = TRUE or  numDomainID = v_numDomainID)) DDS
   ON DDS.numListItemID = numDeclainDocStatusID;
END;
$BODY$;

ALTER FUNCTION public.usp_getbizdocrule(integer, refcursor)
    OWNER TO postgres;
