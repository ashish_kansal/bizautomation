-- Stored procedure definition script USP_GetEmailCampaignMails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmailCampaignMails(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
-- Organization Fields Which Needs To Be Replaced
   AS $$
   DECLARE
   v_vcCompanyName  VARCHAR(200);
   v_OrgNoOfEmployee  VARCHAR(12);
   v_OrgShippingAddress  VARCHAR(1000);
   v_Signature  VARCHAR(1000);


-- Contact Fields Which Needs To Be Replaced.
   v_ContactAge  VARCHAR(3);
   v_ContactAssistantEmail  VARCHAR(100);
   v_AssistantFirstName  VARCHAR(100);
   v_AssistantLastName  VARCHAR(100);
   v_ContactAssistantPhone  VARCHAR(20);
   v_ContactAssistantPhoneExt  VARCHAR(10);
   v_ContactCategory  VARCHAR(100);
   v_ContactCell  VARCHAR(20);
   v_ContactDepartment  VARCHAR(100);
   v_ContactDripCampaign  VARCHAR(500);
   v_ContactEmail  VARCHAR(100);
   v_ContactFax  VARCHAR(100);
   v_ContactFirstName  VARCHAR(100);
   v_ContactGender  VARCHAR(10);
   v_ContactHomePhone  VARCHAR(20);
   v_ContactLastName  VARCHAR(100);
   v_ContactManager  VARCHAR(100);
   v_ContactPhone  VARCHAR(20);	
   v_ContactPhoneExt  VARCHAR(10);
   v_ContactPosition  VARCHAR(100);
   v_ContactPrimaryAddress  VARCHAR(1000);
   v_ContactStatus  VARCHAR(100);
   v_ContactTeam  VARCHAR(100);
   v_ContactTitle  VARCHAR(100);
   v_ContactType  VARCHAR(100);
      
   v_numConEmailCampID  NUMERIC(9,0);
   v_intStartDate  TIMESTAMP;


   v_numConECampDTLID  NUMERIC(9,0);
   v_numContactID  NUMERIC(9,0);   
   v_numFromContactID  NUMERIC(9,0);   
   v_numEmailTemplate  VARCHAR(15);
   v_numDomainId  NUMERIC(9,0);
   v_tintFromField  SMALLINT;
   v_numCampaignOwner  NUMERIC(18,0);

   v_vcSubject  VARCHAR(1000);
   v_vcBody  VARCHAR(8000);              
   v_vcFormattedBody  VARCHAR(8000);

   v_LastDateTime  TIMESTAMP;
   v_CurrentDateTime  TIMESTAMP;
   SWV_RowCount INTEGER;
BEGIN
   select   dtLastDateTimeECampaign INTO v_LastDateTime FROM WindowsServiceHistory;
   v_CurrentDateTime := GetUTCDateWithoutTime()+CAST(EXTRACT(HOUR FROM TIMEZONE('UTC',now())) || 'hour' as interval)+CAST(EXTRACT(MINUTE FROM TIMEZONE('UTC',now())) || 'minute' as interval);
   RAISE NOTICE '%',v_LastDateTime;
   RAISE NOTICE '%',v_CurrentDateTime;


   DROP TABLE IF EXISTS tt_TEMPTABLESENDAPPLICATIONMAIL CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLESENDAPPLICATIONMAIL
   (  
      vcTo VARCHAR(2000),
      vcSubject VARCHAR(2000),
      vcBody TEXT,
      vcCC VARCHAR(2000),
      numDomainID NUMERIC(9,0),
      numConECampDTLID NUMERIC(9,0),
      tintFromField SMALLINT,
      numContactID NUMERIC,
      numFromContactID NUMERIC,
      numCampaignOwner NUMERIC(18,0)
   );                

-- EC.fltTimeZone-2 is used to fetch all email which are not sent from past 2 housrs of last execution date 
-- in case of amazon ses service is down and not able to send email in first try
   select   C.numContactID, numConECampDTLID, numConEmailCampID, intStartDate, numEmailTemplate, EC.numDomainID, EC.tintFromField, numFromContactID, C.numRecOwner INTO v_numContactID,v_numConECampDTLID,v_numConEmailCampID,v_intStartDate,v_numEmailTemplate,
   v_numDomainId,v_tintFromField,v_numFromContactID,v_numCampaignOwner FROM     ConECampaign C
   JOIN ConECampaignDTL DTL
   ON numConEmailCampID = numConECampID
   JOIN ECampaignDTLs E
   ON DTL.numECampDTLID = E.numECampDTLId
   JOIN ECampaign EC ON E.numECampID = EC.numECampaignID WHERE    bitSend IS NULL
   AND bitengaged = true
   AND numEmailTemplate > 0
   AND coalesce(EC.bitDeleted,false) = false
		 -- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
   AND (DTL.dtExecutionDate BETWEEN v_LastDateTime+INTERVAL '-2 hour' AND v_CurrentDateTime OR(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND coalesce(bitSend,false) = true) = 0)   ORDER BY numConEmailCampID LIMIT 1;
   RAISE NOTICE '@numConEmailCampID=';
   RAISE NOTICE '%',v_numConEmailCampID;



   WHILE v_numConEmailCampID > 0 LOOP
      RAISE NOTICE 'inside @numConEmailCampID=';
      RAISE NOTICE '%',v_numConEmailCampID;
      select   coalesce(A.vcFirstName,''), coalesce(A.vcLastname,''), coalesce(C.vcCompanyName,''), coalesce(GetListIemName(C.numNoOfEmployeesId),''), vcCompanyName || ' ,<br>' || coalesce(AD2.vcStreet,'') || ' <br>' || coalesce(AD2.vcCity,'') || ' ,'
      || coalesce(fn_GetState(AD2.numState),'') || ' ' || coalesce(AD2.vcPostalCode,'') || ' <br>' ||
      coalesce(fn_GetListItemName(AD2.numCountry),''), coalesce((select U.txtSignature from UserMaster U where U.numUserDetailId = A.numRecOwner),''), coalesce(A.numPhone,''), coalesce(A.vcEmail,''), coalesce(GetAge(A.bintDOB,TIMEZONE('UTC',now())),0), coalesce(A.vcAsstEmail,''), coalesce(A.VcAsstFirstName,''), coalesce(A.vcAsstLastName,''), coalesce(A.numAsstPhone,''), coalesce(A.numAsstExtn,''), coalesce(GetListIemName(A.vcCategory),''), coalesce(A.numCell,''), coalesce(GetListIemName(A.vcDepartment),''), coalesce((SELECT vcECampName FROM ECampaign WHERE numECampaignID = A.numECampaignID),''), coalesce(A.vcFax,''), (CASE WHEN A.charSex = 'M' THEN 'Male' WHEN A.charSex = 'F' THEN 'Female' ELSE '-' END), coalesce(A.numHomePhone,''), coalesce(fn_GetContactName(A.numManagerID),''), coalesce(A.numPhoneExtension,''), coalesce(GetListIemName(A.vcPosition),''), coalesce(getContactAddress(A.numContactId),''), coalesce(GetListIemName(A.numEmpStatus::NUMERIC(9,0)),''), coalesce(GetListIemName(A.numTeam),''), coalesce(A.vcTitle,''), coalesce(GetListIemName(A.numContactType),'') INTO v_ContactFirstName,v_ContactLastName,v_vcCompanyName,v_OrgNoOfEmployee,
      v_OrgShippingAddress,v_Signature,v_ContactPhone,v_ContactEmail,v_ContactAge,
      v_ContactAssistantEmail,v_AssistantFirstName,v_AssistantLastName,
      v_ContactAssistantPhone,v_ContactAssistantPhoneExt,v_ContactCategory,
      v_ContactCell,v_ContactDepartment,v_ContactDripCampaign,v_ContactFax,
      v_ContactGender,v_ContactHomePhone,v_ContactManager,v_ContactPhoneExt,
      v_ContactPosition,v_ContactPrimaryAddress,v_ContactStatus,v_ContactTeam,
      v_ContactTitle,v_ContactType FROM   AdditionalContactsInformation A
      JOIN DivisionMaster D
      ON A.numDivisionId = D.numDivisionID
      JOIN CompanyInfo C
      ON D.numCompanyID = C.numCompanyId
      LEFT JOIN
      AddressDetails AD2
      ON
      AD2.numDomainID = D.numDomainID
      AND AD2.numRecordID = D.numDivisionID
      AND AD2.tintAddressOf = 2
      AND AD2.tintAddressType = 2
      AND AD2.bitIsPrimary = true WHERE  A.numContactId = v_numContactID;
      select   vcSubject, vcDocdesc INTO v_vcSubject,v_vcBody FROM   GenericDocuments WHERE  numGenericDocID = v_numEmailTemplate;
    
	--Replace organization fields from body
      v_vcFormattedBody := REPLACE(v_vcBody,'##OrganizationName##',v_vcCompanyName);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##OrgNoOfEmployee##',v_OrgNoOfEmployee);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##OrgShippingAddress##',v_OrgShippingAddress);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##Signature##',v_Signature);

	--Replace contact fields from body
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactFirstName##',v_ContactFirstName);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactLastName##',v_ContactLastName);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactPhone##',v_ContactPhone);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactEmail##',v_ContactEmail);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactAge##',v_ContactAge);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactAssistantEmail##',v_ContactAssistantEmail);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##AssistantFirstName##',v_AssistantFirstName);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##AssistantLastName##',v_AssistantLastName);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactAssistantPhone##',v_ContactAssistantPhone);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactAssistantPhoneExt##',v_ContactAssistantPhoneExt);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactCategory##',v_ContactCategory);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactCell##',v_ContactCell);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactDepartment##',v_ContactDepartment);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactDripCampaign##',v_ContactDripCampaign);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactFax##',v_ContactFax);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactGender##',v_ContactGender);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactHomePhone##',v_ContactHomePhone);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactManager##',v_ContactManager);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactPhoneExt##',v_ContactPhoneExt);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactPosition##',v_ContactPosition);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactPrimaryAddress##',v_ContactPrimaryAddress);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactStatus##',v_ContactStatus);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactTeam##',v_ContactTeam);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactTitle##',v_ContactTitle);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactType##',v_ContactType);
      v_vcFormattedBody := REPLACE(v_vcFormattedBody,'##ContactOpt-OutLink##','');
      INSERT INTO tt_TEMPTABLESENDAPPLICATIONMAIL
    VALUES(CAST(v_ContactEmail AS VARCHAR(2000)),
                CAST(v_vcSubject AS VARCHAR(2000)),
                v_vcFormattedBody,
                CAST('' AS VARCHAR(2000)),
                v_numDomainId,
                v_numConECampDTLID,
                v_tintFromField,
                v_numContactID,
                v_numFromContactID,v_numCampaignOwner);
     
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
    
      select   C.numContactID, numConECampDTLID, numConEmailCampID, intStartDate, numEmailTemplate, EC.numDomainID, EC.tintFromField, numFromContactID, C.numRecOwner INTO v_numContactID,v_numConECampDTLID,v_numConEmailCampID,v_intStartDate,v_numEmailTemplate,
      v_numDomainId,v_tintFromField,v_numFromContactID,v_numCampaignOwner FROM     ConECampaign C
      JOIN ConECampaignDTL DTL
      ON numConEmailCampID = numConECampID
      JOIN ECampaignDTLs E
      ON DTL.numECampDTLID = E.numECampDTLId
      JOIN ECampaign EC ON E.numECampID = EC.numECampaignID WHERE    bitSend IS NULL
      AND bitengaged = true
      AND numConEmailCampID > v_numConEmailCampID
      AND numEmailTemplate > 0
      AND coalesce(EC.bitDeleted,false) = false
			-- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
      AND (DTL.dtExecutionDate BETWEEN v_LastDateTime+INTERVAL '-2 hour' AND v_CurrentDateTime OR(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND coalesce(bitSend,false) = true) = 0)   ORDER BY numConEmailCampID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numConEmailCampID := 0;
      end if;
   END LOOP;
  
  
   open SWV_RefCur for SELECT    X.*,
			cast(coalesce(EmailBroadcastConfiguration.vcAWSDomain,'') as VARCHAR(2000)) AS vcAWSDomain,
            cast(coalesce(EmailBroadcastConfiguration.vcAWSAccessKey,'') as TEXT) AS vcAWSAccessKey,
            cast(coalesce(EmailBroadcastConfiguration.vcAWSSecretKey,'') as VARCHAR(2000)) AS vcAWSSecretKey,
            cast(CASE WHEN LENGTH(vcCampaignOwnerEmail) > 0 THEN vcCampaignOwnerEmail ELSE coalesce(EmailBroadcastConfiguration.vcFrom,'') END as NUMERIC(9,0)) AS vcFrom
   FROM(SELECT    A.vcTo,
                        A.vcSubject,
                        A.vcBody,
                        A.vcCC,
                        A.numDomainID,
                        A.numConECampDTLID,
                        A.tintFromField,
                        A.numContactID,
                        CASE tintFromField
      WHEN 1 THEN --Record Owner of Company
         DM.numRecOwner
      WHEN 2 THEN --Assignee of Company
         DM.numAssignedTo
      WHEN 3 THEN numFromContactID
      ELSE DM.numRecOwner
      END AS numFromContactID,
						cast(coalesce(UM.vcEmailID,'') as VARCHAR(255)) AS vcCampaignOwnerEmail
      FROM      tt_TEMPTABLESENDAPPLICATIONMAIL A
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = A.numContactID
      LEFT JOIN UserMaster UM ON A.numCampaignOwner = UM.numUserDetailId
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = ACI.numDivisionId) X
   LEFT JOIN
   EmailBroadcastConfiguration
   ON
   X.numDomainID = EmailBroadcastConfiguration.numDomainID;
  

   UPDATE WindowsServiceHistory SET dtLastDateTimeECampaign = TIMEZONE('UTC',now());
   RETURN;
END; $$;













