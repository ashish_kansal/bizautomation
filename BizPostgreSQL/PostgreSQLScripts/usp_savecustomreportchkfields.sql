CREATE OR REPLACE FUNCTION usp_SaveCustomReportChkFields(v_ReportId NUMERIC(9,0) DEFAULT 0,
v_strCheckedFields TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM CustReportFields where numCustomReportId = v_ReportId;
           
   insert into CustReportFields(numCustomReportId,vcFieldName,vcValue)
   select v_ReportId,
     X.vcField,X.vcValue from
	 XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strCheckedFields AS XML)
				COLUMNS
					id FOR ORDINALITY,
					vcField TEXT PATH 'vcField',
					vcValue BOOLEAN PATH 'vcValue'
			) AS X; 
   RETURN;
END; $$;


