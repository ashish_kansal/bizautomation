-- Stored procedure definition script USP_GetDepositJournalDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDepositJournalDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                
v_numJournalId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJD.numTransactionId as TransactionId , CAST(GJD.numChartAcntId AS VARCHAR(10)) || '~' || CAST(CA.numParntAcntTypeID AS VARCHAR(10)) as numChartAcntId,
 CA.numParntAcntTypeID as numAcntType,GJD.numDebitAmt as numDebitAmt,
GJD.numCreditAmt as numCreditAmt,GJD.numCustomerId as numCustomerId,GJD.numJournalId,
 GJD.varDescription,GJD.vcReference as vcReference,GJD.numPaymentMethod as numPaymentMethod      ,GJD.numClassID,GJD.numProjectID
   From General_Journal_Header GJH
   Inner join General_Journal_Details GJD  on GJH.numJOurnal_Id = GJD.numJournalId
   inner join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId
   Left outer join CompanyInfo CI on GJD.numCustomerId = CI.numCompanyId
   Where coalesce(GJD.bitMainDeposit,false) = false And GJH.numDomainId = v_numDomainId And GJH.numJOurnal_Id = v_numJournalId;
END; $$;












