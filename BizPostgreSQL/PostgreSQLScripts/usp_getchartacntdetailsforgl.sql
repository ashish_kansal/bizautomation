-- Stored procedure definition script USP_GetChartAcntDetailsForGL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForGL(v_numDomainId NUMERIC(9,0),                            
v_dtFromDate TIMESTAMP,                          
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);                          
   v_i  INTEGER;
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   v_strSQL := '';                         
 ----  Select @i=count(*) From General_Journal_Header GJH Where GJH.numDomainId= @numDomainId And GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''                  
 ----    And  GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''           
        
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1       
                      
   select   count(*) INTO v_i From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                 
   GJH.numDomainId = v_numDomainId And GJH.datEntry_Date <= CAST('' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '' AS timestamp);               
   If v_i = 0 then
  
      v_strSQL := ' Select distinct COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,vcCatgyName as CategoryName,COA.numOpeningBal as OpeningBalance                              
       From Chart_Of_Accounts COA                              
       Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                              
       Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <>' || SUBSTR(CAST(v_numParntAcntId AS VARCHAR(10)),1,10) || ' And COA.numOpeningBal is not null ';--And CoA.numParntAcntId ='+convert(varchar(10),@numParntAcntId)                         
 
      v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''' And  COA.dtOpeningDate<=''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '''';
   Else
      v_strSQL := ' Select distinct COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as CategoryName,COA.numOpeningBal as OpeningBalance                              
        From Chart_Of_Accounts COA                              
        Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                              
        Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <>' || SUBSTR(CAST(v_numParntAcntId AS VARCHAR(10)),1,10) || ' And COA.numOpeningBal is not null ';
   end if;                    
   RAISE NOTICE '%',v_strSQL;                          
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


