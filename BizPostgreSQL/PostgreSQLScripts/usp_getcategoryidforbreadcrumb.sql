DROP FUNCTION IF EXISTS USP_GetCategoryIDForBreadCrumb;

CREATE OR REPLACE FUNCTION USP_GetCategoryIDForBreadCrumb(v_numItemCode NUMERIC(9,0),
	v_vcCategoryName VARCHAR(1000),
	v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	
   open SWV_RefCur for SELECT  cast(C.numCategoryID as VARCHAR(255))
   FROM Item I
   INNER JOIN ItemCategory IC
   ON I.numItemCode = IC.numItemID
   INNER JOIN Category C
   ON IC.numCategoryID = C.numCategoryID
   INNER JOIN SiteCategories SC
   ON SC.numCategoryID = C.numCategoryID
   WHERE I.numItemCode = v_numItemCode
   AND SC.numSiteID = v_numSiteID
   AND fn_GenerateSEOFriendlyURL(C.vcCategoryName) ilike v_vcCategoryName LIMIT 1;
   RETURN;
END; $$;














