-- Stored procedure definition script usp_ValidateeWorkOrderInventory for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ValidateeWorkOrderInventory(v_numWOId NUMERIC(9,0) DEFAULT 0,
	  v_numWOStatus NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppID  NUMERIC(18,0);
   v_tintCommitAllocation  SMALLINT;
BEGIN
   IF v_numWOStatus = 23184 then
      select   coalesce(tintCommitAllocation,1), coalesce(numOppId,0) INTO v_tintCommitAllocation,v_numOppID FROM WorkOrder INNER JOIN Domain ON WorkOrder.numDomainId = Domain.numDomainId WHERE numWOId = v_numWOId;
      open SWV_RefCur for SELECT
			WD.numQtyItemsReq
			,i.vcItemName
			,coalesce(WHI.numOnHand,0) AS numOnHand
			,coalesce(WHI.numAllocation,0) AS numAllocation
      FROM
      WorkOrderDetails WD
      JOIN
      Item i
      ON
      WD.numChildItemID = i.numItemCode
      JOIN
      WareHouseItems WHI
      ON
      WHI.numWareHouseItemID = WD.numWarehouseItemID
      WHERE
      numWOId = v_numWOId
      AND i.charItemType IN('P')
      AND 1 =(CASE
      WHEN v_numOppID > 0 AND v_tintCommitAllocation = 2
      THEN(CASE WHEN WD.numQtyItemsReq > coalesce(WHI.numOnHand,0) THEN 1 ELSE 0 END)
      ELSE(CASE WHEN WD.numQtyItemsReq > coalesce(WHI.numAllocation,0) THEN 1 ELSE 0 END)
      END);
   end if;
END; $$;












