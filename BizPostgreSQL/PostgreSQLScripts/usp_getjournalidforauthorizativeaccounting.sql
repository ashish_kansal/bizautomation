-- Stored procedure definition script USP_GetJournalIdForAuthorizativeAccounting for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetJournalIdForAuthorizativeAccounting(v_numOppId NUMERIC(9,0) DEFAULT 0,
v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numJOurnal_Id  From General_Journal_Header Where numOppId = v_numOppId and numOppBizDocsId = v_numOppBizDocsId;
END; $$;












