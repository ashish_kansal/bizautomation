-- Stored procedure definition script USP_DeleteTerritory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteTerritory(v_numTerID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from TerritoryMaster where numTerID = v_numTerID and numDomainid = v_numDomainID;
   RETURN;
END; $$;


