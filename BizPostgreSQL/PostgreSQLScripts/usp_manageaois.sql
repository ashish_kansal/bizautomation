-- Stored procedure definition script USP_ManageAOIs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAOIs(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numAOIId NUMERIC(9,0) DEFAULT 0,
v_vcAOIName VARCHAR(50) DEFAULT '',
v_bitDeleted BOOLEAN DEFAULT NULL,
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_bitDeleted = false then

      if v_numAOIId = 0 then
	
         insert into AOIMaster(vcAOIName,numDomainID,numCreatedBy,bintCreatedDate,numModifiedBy,bintmodifiedDate)
		values(v_vcAOIName,v_numDomainID,v_numUserCntID,TIMEZONE('UTC',now()),v_numUserCntID,TIMEZONE('UTC',now()));
      else
         update AOIMaster set vcAOIName = v_vcAOIName,numModifiedBy = v_numUserCntID,bintmodifiedDate = TIMEZONE('UTC',now())
         where numAOIID = v_numAOIId;
      end if;
   end if;
   if v_bitDeleted = true then

      delete from AOIContactLink where numAOIID = v_numAOIId;
      delete from AOIMaster where numAOIID = v_numAOIId;
   end if;
   RETURN;
END; $$;


