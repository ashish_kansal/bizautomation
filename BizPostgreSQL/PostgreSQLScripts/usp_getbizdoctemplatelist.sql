-- Stored procedure definition script USP_GetBizDocTemplateList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBizDocTemplateList(v_numDomainID NUMERIC,
	v_numBizDocID NUMERIC,
	v_numOppType NUMERIC DEFAULT 0,
	v_byteMode SMALLINT DEFAULT NULL,
	v_numRelationship NUMERIC(18,0) DEFAULT 0,
	v_numProfile NUMERIC(18,0) DEFAULT 0,
	v_numAccountClass NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAuthoritativePurchase  NUMERIC(18,0) DEFAULT 0;
   v_numAuthoritativeSales  NUMERIC(18,0) DEFAULT 0;
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
   v_numBizDocTempID  NUMERIC(18,0) DEFAULT 0;
BEGIN


   IF v_byteMode = 0 then
      select   numAuthoritativePurchase, numAuthoritativeSales INTO v_numAuthoritativePurchase,v_numAuthoritativeSales FROM
      AuthoritativeBizDocs WHERE
      numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT
      numDomainID,
	numBizDocID,
	numBizDocTempID,
	vcTemplateName,
	coalesce(bitEnabled,false) as bitEnabled,
	coalesce(bitDefault,false) as bitDefault,
	numOppType,
	numRelationship,
	numProfile,
	fn_GetListName(numRelationship,0::BOOLEAN) AS Relationship,
	fn_GetListName(numProfile,0::BOOLEAN) AS Profile,
	(CASE WHEN numOppType = 1 THEN 'Sales' ELSE 'Purchase' END) AS vcOppType,
	fn_GetListItemName(numBizDocID) ||(CASE
      WHEN numOppType = 1
      THEN(CASE WHEN coalesce(v_numAuthoritativeSales,0) = numBizDocID THEN ' - Authoritative' ELSE '' END)
      WHEN numOppType = 2
      THEN(CASE WHEN coalesce(v_numAuthoritativePurchase,0) = numBizDocID THEN ' - Authoritative' ELSE '' END)
      END) AS vcBizDocType
      FROM
      BizDocTemplate
      WHERE
      numDomainID = v_numDomainID
      AND tintTemplateType = 0
      AND	(numBizDocID = v_numBizDocID OR v_numBizDocID = 0)
      AND (numOppType = v_numOppType OR v_numOppType = 0);
   ELSEIF v_byteMode = 1
   then
      If coalesce(v_numRelationship,0) > 0 OR coalesce(v_numProfile,0) > 0 OR coalesce(v_numAccountClass,0) > 0 then
	
         select   numBizDocTempID INTO v_numBizDocTempID FROM
         BizDocTemplate WHERE
         numDomainID = v_numDomainID
         AND tintTemplateType = 0
         AND numBizDocID = v_numBizDocID
         AND numOppType = v_numOppType
         AND coalesce(bitEnabled,false) = true
         AND 1 =(CASE
         WHEN coalesce(numRelationship,0) > 0 AND coalesce(numProfile,0) = 0 AND coalesce(numAccountClass,0) = 0
         THEN(CASE WHEN numRelationship = v_numRelationship THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) > 0 AND coalesce(numProfile,0) > 0 AND coalesce(numAccountClass,0) = 0
         THEN(CASE WHEN numRelationship = v_numRelationship AND numProfile = v_numProfile THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) > 0 AND coalesce(numProfile,0) = 0 AND coalesce(numAccountClass,0) > 0
         THEN(CASE WHEN numRelationship = v_numRelationship AND numAccountClass = v_numAccountClass THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) = 0 AND coalesce(numProfile,0) > 0 AND coalesce(numAccountClass,0) = 0
         THEN(CASE WHEN numProfile = v_numProfile THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) = 0 AND coalesce(numProfile,0) > 0 AND coalesce(numAccountClass,0) > 0
         THEN(CASE WHEN numProfile = v_numProfile AND numAccountClass = v_numAccountClass THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) = 0 AND coalesce(numProfile,0) = 0 AND coalesce(numAccountClass,0) > 0
         THEN(CASE WHEN numAccountClass = v_numAccountClass THEN 1 ELSE 0 END)
         WHEN coalesce(numRelationship,0) > 0 AND coalesce(numProfile,0) > 0 AND coalesce(numAccountClass,0) > 0
         THEN(CASE WHEN numRelationship = v_numRelationship AND numProfile = v_numProfile AND numAccountClass = v_numAccountClass THEN 1 ELSE 0 END)
         ELSE
            0
         END);
      end if;
      IF coalesce(v_numBizDocTempID,0) = 0 then
	
         select   numBizDocTempID INTO v_numBizDocTempID FROM
         BizDocTemplate WHERE
         numDomainID = v_numDomainID
         AND tintTemplateType = 0
         AND	numBizDocID = v_numBizDocID
         AND numOppType = v_numOppType
         AND coalesce(bitDefault,false) = true
         AND coalesce(bitEnabled,false) = true;
      end if;
      open SWV_RefCur for
      SELECT v_numBizDocTempID as numBizDocTempID;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
   RETURN;
END; $$;


