-- Stored procedure definition script USP_SaveAuthoritativeBizDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveAuthoritativeBizDocs(v_numDomainID NUMERIC(9,0) DEFAULT 0,          
v_numAuthoritativeMode SMALLINT DEFAULT 0,  
v_numAuthoritativePurchase NUMERIC(9,0) DEFAULT 0,  
v_numAuthoritativeSales NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numAuthoritativeMode = 0 then

      Insert into AuthoritativeBizDocs(numAuthoritativePurchase,numAuthoritativeSales,numDomainId)  values(v_numAuthoritativePurchase,v_numAuthoritativeSales,v_numDomainID);
   Else
      Update AuthoritativeBizDocs Set numAuthoritativePurchase = v_numAuthoritativePurchase,numAuthoritativeSales = v_numAuthoritativeSales where numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


