-- Stored procedure definition script USP_CompanyInfo_AlertNotification for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CompanyInfo_AlertNotification(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numCompanyId,
		vcCompanyName
   FROM
   CompanyInfo
   WHERE
   bintModifiedDate > LOCALTIMESTAMP+INTERVAL '-1 day' OR bintCreatedDate > LOCALTIMESTAMP+INTERVAL '-1 day';
END; $$;












