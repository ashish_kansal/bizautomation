-- Stored procedure definition script USP_AddDelShippingExceptions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddDelShippingExceptions(v_numDomainID NUMERIC(9,0)
	,v_numClassificationID NUMERIC(18,0) 
	,v_PercentAbove DOUBLE PRECISION 
	,v_FlatAmt DOUBLE PRECISION 
	,v_tintMode	SMALLINT
	,v_numShippingExceptionID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      IF EXISTS(SELECT * FROM ShippingExceptions SE
      WHERE SE.numClassificationID = v_numClassificationID AND SE.numDomainID = v_numDomainID) then
                
         RAISE EXCEPTION 'DUPLICATE';
         RETURN;
      end if;
      INSERT INTO ShippingExceptions(numDomainID
				,numClassificationID
				,PercentAbove
				,FlatAmt)
			VALUES(v_numDomainID
				,v_numClassificationID
				,v_PercentAbove
				,v_FlatAmt);
   end if;
   IF v_tintMode = 1 then
    
      DELETE FROM ShippingExceptions
      WHERE numShippingExceptionID = v_numShippingExceptionID
      AND numDomainID = v_numDomainID;
   end if;
END; $$;



