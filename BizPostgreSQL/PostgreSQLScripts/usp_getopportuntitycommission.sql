CREATE OR REPLACE FUNCTION USP_GetOpportuntityCommission(v_numUserId NUMERIC(18,0)  DEFAULT 0,
	v_numUserCntID NUMERIC(18,0)  DEFAULT 0,
	v_numDomainId NUMERIC(18,0)  DEFAULT 0,
	v_numComPayPeriodID NUMERIC(18,0) DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,
	v_tintMode SMALLINT DEFAULT NULL,
	v_numComRuleID NUMERIC(18,0) DEFAULT NULL,
	v_numDivisionID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitIncludeShippingItem  BOOLEAN;
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_tintCommissionType  SMALLINT;
BEGIN
   select   bitIncludeTaxAndShippingInCommission, numShippingServiceItemID, coalesce(numDiscountServiceItemID,0), tintCommissionType INTO v_bitIncludeShippingItem,v_numShippingItemID,v_numDiscountItemID,v_tintCommissionType FROM
   Domain WHERE
   numDomainId = v_numDomainId;


   IF v_tintMode = 0 then  -- Paid Invoice
	
      open SWV_RefCur for
      SELECT
      CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocId,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			coalesce((SELECT SUM(monOrderSubTotal) FROM(SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID = OpportunityMaster.numOppId AND BDCInner.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			coalesce(OpportunityBizDocs.monAmountPaid,0) AS monAmountPaid,
			TEMPDEPOSIT.dtDepositDate AS bintCreatedDate,
			OpportunityMaster.vcpOppName AS Name,
			Case when OpportunityMaster.tintoppstatus = 0 then 'Open' Else 'Close' End as OppStatus,
			(CASE WHEN coalesce(OpportunityMaster.fltExchangeRate,0) = 0 OR coalesce(OpportunityMaster.fltExchangeRate,0) = 1 THEN coalesce(OpportunityBizDocs.monDealAmount,0) ELSE ROUND(CAST(coalesce(OpportunityBizDocs.monDealAmount,0)*OpportunityMaster.fltExchangeRate AS NUMERIC),2) END) as DealAmount,
			Case When (OpportunityMaster.numrecowner = v_numUserCntID and OpportunityMaster.numassignedto = v_numUserCntID) then  'Deal Owner/Assigned'
      When OpportunityMaster.numrecowner = v_numUserCntID then 'Deal Owner'
      When OpportunityMaster.numassignedto = v_numUserCntID then 'Deal Assigned'
      end as EmpRole
			,SUM(coalesce(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			coalesce((SELECT SUM(monInvoiceSubTotal) FROM(SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID = OpportunityMaster.numOppId AND BDCInner.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
      FROM
      BizDocComission
      INNER JOIN
      OpportunityBizDocs
      ON
      BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
      INNER JOIN
      OpportunityMaster
      ON
      BizDocComission.numOppID = OpportunityMaster.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      LEFT JOIN
      Listdetails LD
      ON
      LD.numListItemID = OpportunityBizDocs.numBizDocId
      LEFT JOIN LATERAL(SELECT
         MAX(DM.dtDepositDate) AS dtDepositDate
         FROM
         DepositMaster DM
         JOIN DepositeDetails DD	ON DM.numDepositId = DD.numDepositID
         WHERE
         DM.tintDepositePage = 2
         AND DM.numDomainId = v_numDomainId
         AND DD.numOppID = OpportunityBizDocs.numOppID
         AND DD.numOppBizDocsID = OpportunityBizDocs.numOppBizDocsID) TEMPDEPOSIT on TRUE
      WHERE
      numComPayPeriodID = v_numComPayPeriodID
      AND ((BizDocComission.numUserCntID = v_numUserCntID AND coalesce(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = v_numDivisionID AND tintAssignTo = 3))
      AND coalesce(OpportunityBizDocs.monAmountPaid,0) >= OpportunityBizDocs.monDealAmount
      AND (coalesce(v_numComRuleID,0) = 0 OR BizDocComission.numComRuleID = v_numComRuleID)
      GROUP BY
      CompanyInfo.vcCompanyName,LD.vcData,OpportunityBizDocs.numBizDocId,OpportunityMaster.numOppId,
      OpportunityBizDocs.numOppBizDocsId,OpportunityMaster.fltExchangeRate,
      OpportunityMaster.monDealAmount,OpportunityMaster.vcpOppName,
      OpportunityBizDocs.monAmountPaid,OpportunityMaster.tintoppstatus,
      OpportunityBizDocs.monDealAmount,OpportunityMaster.numrecowner,
      OpportunityMaster.numassignedto,OpportunityBizDocs.vcBizDocID,OpportunityBizDocs.dtCreatedDate,
      TEMPDEPOSIT.dtDepositDate
      ORDER BY
      OpportunityBizDocs.dtCreatedDate;
   ELSEIF v_tintMode = 1
   then -- Non Paid Invoice
	
      open SWV_RefCur for
      SELECT
      CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocId,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			coalesce((SELECT SUM(monOrderSubTotal) FROM(SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID = OpportunityMaster.numOppId AND BDCInner.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			coalesce(OpportunityBizDocs.monAmountPaid,0) AS monAmountPaid,
			OpportunityBizDocs.dtCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS bintCreatedDate,
			OpportunityMaster.vcpOppName AS Name,
			Case when OpportunityMaster.tintoppstatus = 0 then 'Open' Else 'Close' End as OppStatus,
			(CASE WHEN coalesce(OpportunityMaster.fltExchangeRate,0) = 0 OR coalesce(OpportunityMaster.fltExchangeRate,0) = 1 THEN coalesce(OpportunityBizDocs.monDealAmount,0) ELSE ROUND(CAST(coalesce(OpportunityBizDocs.monDealAmount,0)*OpportunityMaster.fltExchangeRate AS NUMERIC),2) END) as DealAmount,
			Case When (OpportunityMaster.numrecowner = v_numUserCntID and OpportunityMaster.numassignedto = v_numUserCntID) then  'Deal Owner/Assigned'
      When OpportunityMaster.numrecowner = v_numUserCntID then 'Deal Owner'
      When OpportunityMaster.numassignedto = v_numUserCntID then 'Deal Assigned'
      end as EmpRole
			,SUM(coalesce(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			coalesce((SELECT SUM(monInvoiceSubTotal) FROM(SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID = OpportunityMaster.numOppId AND BDCInner.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
      FROM
      BizDocComission
      INNER JOIN
      OpportunityBizDocs
      ON
      BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
      INNER JOIN
      OpportunityMaster
      ON
      BizDocComission.numOppID = OpportunityMaster.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      LEFT JOIN
      Listdetails LD
      ON
      LD.numListItemID = OpportunityBizDocs.numBizDocId
      WHERE
      numComPayPeriodID = v_numComPayPeriodID
      AND ((BizDocComission.numUserCntID = v_numUserCntID AND coalesce(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = v_numDivisionID AND tintAssignTo = 3))
      AND coalesce(OpportunityBizDocs.monAmountPaid,0) < OpportunityBizDocs.monDealAmount
      AND (coalesce(v_numComRuleID,0) = 0 OR BizDocComission.numComRuleID = v_numComRuleID)
      GROUP BY
      CompanyInfo.vcCompanyName,LD.vcData,OpportunityBizDocs.numBizDocId,OpportunityMaster.numOppId,
      OpportunityBizDocs.numOppBizDocsId,OpportunityMaster.fltExchangeRate,
      OpportunityMaster.monDealAmount,OpportunityMaster.vcpOppName,
      OpportunityBizDocs.monAmountPaid,OpportunityMaster.tintoppstatus,
      OpportunityBizDocs.monDealAmount,OpportunityMaster.numrecowner,
      OpportunityMaster.numassignedto,OpportunityBizDocs.vcBizDocID,OpportunityBizDocs.dtCreatedDate
      ORDER BY
      OpportunityBizDocs.dtCreatedDate;
   ELSEIF v_tintMode = 2
   then  -- Paid Invoice Detail
	
      open SWV_RefCur for
      SELECT * FROM(Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType,
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour
						,(CASE WHEN coalesce(Opp.fltExchangeRate,0) = 0 OR coalesce(Opp.fltExchangeRate,0) = 1 THEN coalesce(BDI.monPrice,0) ELSE ROUND(CAST(coalesce(BDI.monPrice,0)*Opp.fltExchangeRate AS NUMERIC),
            2) END) AS monPrice
						,(CASE WHEN coalesce(Opp.fltExchangeRate,0) = 0 OR coalesce(Opp.fltExchangeRate,0) = 1 THEN coalesce(BDI.monTotAmount,0) ELSE ROUND(CAST(coalesce(BDI.monTotAmount,0)*Opp.fltExchangeRate AS NUMERIC),
            2) END) AS monTotAmount
						,coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0) AS VendorCost,
						coalesce(OT.monAvgCost,0)*coalesce(BDI.numUnitHour,0) AS monAvgCost
         From OpportunityMaster Opp
         INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
         INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
         INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
         INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId and ((BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = v_numDivisionID AND BC.tintAssignTo = 3))
         INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId = BDI.numOppBizDocID AND BC.numOppBizDocItemID = BDI.numOppBizDocItemID
         INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
         INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
         Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintoppstatus = 1 And Opp.tintopptype = 1 And (Opp.numrecowner = v_numUserCntID OR Opp.numassignedto = v_numUserCntID OR Opp.numPartner = v_numDivisionID)
         And Opp.numDomainId = v_numDomainId
         AND BC.numComPayPeriodID = v_numComPayPeriodID
         AND oppBiz.numOppBizDocsId = v_numOppBizDocsId
         AND (coalesce(v_numComRuleID,0) = 0 OR BC.numComRuleID = v_numComRuleID)
         AND oppBiz.bitAuthoritativeBizDocs = 1 AND
         1 =(CASE WHEN v_tintMode = 2 THEN CASE WHEN coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount THEN 1 ELSE 0 END
         WHEN v_tintMode = 3 THEN CASE WHEN coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A;
   ELSEIF v_tintMode = 3
   then -- Non Paid Invoice Detail
	
      open SWV_RefCur for
      Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType,
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour
						,(CASE WHEN coalesce(Opp.fltExchangeRate,0) = 0 OR coalesce(Opp.fltExchangeRate,0) = 1 THEN coalesce(BDI.monPrice,0) ELSE ROUND(CAST(coalesce(BDI.monPrice,0)*Opp.fltExchangeRate AS NUMERIC),
         2) END) AS monPrice
						,(CASE WHEN coalesce(Opp.fltExchangeRate,0) = 0 OR coalesce(Opp.fltExchangeRate,0) = 1 THEN coalesce(BDI.monTotAmount,0) ELSE ROUND(CAST(coalesce(BDI.monTotAmount,0)*Opp.fltExchangeRate AS NUMERIC),
         2) END) AS monTotAmount
						,coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0) AS VendorCost,
						coalesce(OT.monAvgCost,0)*coalesce(BDI.numUnitHour,0) AS monAvgCost
      From OpportunityMaster Opp
      INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
      INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId and ((BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = v_numDivisionID AND BC.tintAssignTo = 3))
      INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId = BDI.numOppBizDocID AND BC.numOppBizDocItemID = BDI.numOppBizDocItemID
      INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
      INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
      Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintoppstatus = 1 And Opp.tintopptype = 1 And (Opp.numrecowner = v_numUserCntID OR Opp.numassignedto = v_numUserCntID OR Opp.numPartner = v_numDivisionID)
      And Opp.numDomainId = v_numDomainId
      AND BC.numComPayPeriodID = v_numComPayPeriodID
      AND oppBiz.numOppBizDocsId = v_numOppBizDocsId
      AND (coalesce(v_numComRuleID,0) = 0 OR BC.numComRuleID = v_numComRuleID)
      AND oppBiz.bitAuthoritativeBizDocs = 1 AND
      1 =(CASE WHEN v_tintMode = 2 THEN CASE WHEN coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount THEN 1 ELSE 0 END
      WHEN v_tintMode = 3 THEN CASE WHEN coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount THEN 1 ELSE 0 END END);
   end if;
   RETURN;
END; $$;


