-- Stored procedure definition script USP_CreateCommissionsContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateCommissionsContacts(v_numDivisionID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if not exists(select * from CommissionContacts where numDivisionID = v_numDivisionID and numContactID = v_numContactID AND numDomainID = v_numDomainID) then

      insert into CommissionContacts(numDivisionID,numContactID,numDomainID)
	values(v_numDivisionID,v_numContactID,v_numDomainID);
   end if;
   RETURN;
END; $$;
	


