-- Stored procedure definition script USP_OpportunityMaster_SearchSalesOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_SearchSalesOrder(v_numDomainID NUMERIC(18,0),
	 v_vcOppName VARCHAR(300),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numOppId AS "numOppId"
		,vcpOppName AS "vcpOppName"
   FROM
   OpportunityMaster
   WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND vcpOppName ilike CONCAT('%',v_vcOppName,'%');
END; $$;












