-- Stored procedure definition script USP_SaveBackOrderAuthGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveBackOrderAuthGroup(v_numDomainID NUMERIC(18,0),
	v_strAuthGroupID TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE from AuthenticationGroupBackOrder WHERE numDomainID = v_numDomainID; 

   IF LENGTH(coalesce(v_strAuthGroupID,'')) > 0 then
      INSERT INTO AuthenticationGroupBackOrder 
	  (
		numDomainID
		,numGroupID
	  )
	  SELECT v_numDomainID,Id FROM SplitIDS(v_strAuthGroupID,',');
   end if;
   RETURN;
END; $$;                        


