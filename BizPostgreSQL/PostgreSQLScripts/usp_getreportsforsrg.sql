CREATE OR REPLACE FUNCTION USP_GetReportsForSRG(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_tintReportType SMALLINT
	,v_numDashboardTemplateID NUMERIC(18,0)
	,v_vcSearchText VARCHAR(300)
	,v_numCurrentPage INTEGER
	,v_numPageSize INTEGER
	,v_numSRGID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
	
      IF v_tintReportType = 2 then -- Pre defined reports
		
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords"
				,ReportListMaster.numReportID AS "numReportID" 
				,(CASE WHEN  coalesce(bitDefault,false) = true THEN 2 ELSE 1 END) AS "tintReportType"
				,coalesce(vcReportName,'') AS "vcReportName"
				,'' AS "vcReportType"
				,'' AS "vcModuleName"
				,'' AS "vcPerspective"
				,'' AS "vcTimeline"
         FROM
         ReportListMaster
         LEFT JOIN
         ReportModuleMaster RMM
         ON
         ReportListMaster.numReportModuleID = RMM.numReportModuleID
         WHERE
         coalesce(ReportListMaster.bitDefault,false) = true
         AND (coalesce(v_vcSearchText,'') = '' OR vcReportName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcReportName;
      ELSEIF v_tintReportType = 3
      then -- My Reports
		
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords"
				,URL.numReportID AS "numReportID"
				,1 AS "tintReportType"
				,coalesce(vcReportName,'') AS "vcReportName"
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS "vcReportType"
				,coalesce(RMM.vcModuleName,'') AS "vcModuleName"
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS "vcPerspective"
				,(CASE
         WHEN coalesce(RLM.numDateFieldID,0) > 0 AND LENGTH(coalesce(RLM.vcDateFieldValue,'')) > 0
         THEN
            CONCAT((CASE WHEN bitDateFieldColumnCustom = true THEN(SELECT FLd_label FROM CFW_Fld_Master WHERE Fld_id = numDateFieldID) ELSE(SELECT vcFieldName FROM DycFieldMaster WHERE numFieldID = numDateFieldID) END),' (',(CASE RLM.vcDateFieldValue
            WHEN 'AllTime' THEN 'All Time'
            WHEN 'Custom' THEN 'Custom'
            WHEN 'CurYear' THEN 'Current CY'
            WHEN 'PreYear' THEN 'Previous CY'
            WHEN 'Pre2Year' THEN 'Previous 2 CY'
            WHEN 'Ago2Year' THEN '2 CY Ago'
            WHEN 'NextYear' THEN 'Next CY'
            WHEN 'CurPreYear' THEN 'Current and Previous CY'
            WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
            WHEN 'CurNextYear' THEN 'Current and Next CY'
            WHEN 'CuQur' THEN 'Current CQ'
            WHEN 'CurNextQur' THEN 'Current and Next CQ'
            WHEN 'CurPreQur' THEN 'Current and Previous CQ'
            WHEN 'NextQur' THEN 'Next CQ'
            WHEN 'PreQur' THEN 'Previous CQ'
            WHEN 'LastMonth' THEN 'Last Month'
            WHEN 'ThisMonth' THEN 'This Month'
            WHEN 'NextMonth' THEN 'Next Month'
            WHEN 'CurPreMonth' THEN 'Current and Previous Month'
            WHEN 'CurNextMonth' THEN 'Current and Next Month'
            WHEN 'LastWeek' THEN 'Last Week'
            WHEN 'ThisWeek' THEN 'This Week'
            WHEN 'NextWeek' THEN 'Next Week'
            WHEN 'Yesterday' THEN 'Yesterday'
            WHEN 'Today' THEN 'Today'
            WHEN 'Tomorrow' THEN 'Tomorrow'
            WHEN 'Last7Day' THEN 'Last 7 Days'
            WHEN 'Last30Day' THEN 'Last 30 Days'
            WHEN 'Last60Day' THEN 'Last 60 Days'
            WHEN 'Last90Day' THEN 'Last 90 Days'
            WHEN 'Last120Day' THEN 'Last 120 Days'
            WHEN 'Next7Day' THEN 'Next 7 Days'
            WHEN 'Next30Day' THEN 'Next 30 Days'
            WHEN 'Next60Day' THEN 'Next 60 Days'
            WHEN 'Next90Day' THEN 'Next 90 Days'
            WHEN 'Next120Day' THEN 'Next 120 Days'
            ELSE '-'
            END),
            ')')
         ELSE
            ''
         END) AS "vcTimeline"
         FROM
         UserReportList URL
         INNER JOIN
         ReportListMaster RLM
         ON
         URL.numReportID = RLM.numReportID
         INNER JOIN
         ReportModuleMaster RMM
         ON
         RLM.numReportModuleID = RMM.numReportModuleID
         WHERE
         URL.numDomainID = v_numDomainID
         AND URL.numUserCntID = v_numUserCntID
         AND URL.tintReportType = 1
         AND RLM.numDomainID = v_numDomainID
         AND (coalesce(v_vcSearchText,'') = '' OR vcReportName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcReportName;
      ELSEIF v_tintReportType = 4
      then -- Saved searches
		
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords",
				numSearchID AS "numReportID",
				4 AS "tintReportType",
				coalesce(vcSearchName,'') AS "vcReportName",
				'' AS "vcReportType",
				(CASE DFM.numFormID WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS "vcModuleName",
				'' AS "vcPerspective",
				'' AS "vcTimeline"
         FROM
         SavedSearch S
         INNER JOIN
         dynamicFormMaster DFM
         ON
         DFM.numFormID = S.numFormId
         WHERE
         numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID
         AND coalesce(numSharedFromSearchID,0) = 0
         AND (coalesce(v_vcSearchText,'') = '' OR vcSearchName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcSearchName;
      ELSEIF v_tintReportType = 5
      then -- Shared saved searched
		
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords",
				numSearchID AS "numReportID",
				4 AS "tintReportType",
				coalesce(vcSearchName,'') AS "vcReportName",
				'' AS "vcReportType",
				(CASE DFM.numFormID WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS "vcModuleName",
				'' AS "vcPerspective",
				'' AS "vcTimeline"
         FROM
         SavedSearch S
         INNER JOIN
         dynamicFormMaster DFM
         ON
         DFM.numFormID = S.numFormId
         WHERE
         numDomainID = v_numDomainID
         AND numUserCntID = v_numUserCntID
         AND coalesce(numSharedFromSearchID,0) > 0
         AND (coalesce(v_vcSearchText,'') = '' OR vcSearchName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcSearchName;
      ELSEIF v_tintReportType = 6
      then -- Dashboar templates
		
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords"
				,ReportDashboard.numDashBoardID AS "numReportID"
				,6 AS "tintReportType"
				,coalesce(vcReportName,'') AS "vcReportName"
				,'' AS "vcReportType"
				,'' AS "vcModuleName"
				,'' AS "vcPerspective"
				,'' AS "vcTimeline"
         FROM
         DashboardTemplate
         INNER JOIN
         ReportDashboard
         ON
         DashboardTemplate.numTemplateID = ReportDashboard.numDashboardTemplateID
         INNER JOIN
         ReportListMaster
         ON
         ReportDashboard.numReportID = ReportListMaster.numReportID
         LEFT JOIN
         ReportModuleMaster RMM
         ON
         ReportListMaster.numReportModuleID = RMM.numReportModuleID
         WHERE
         DashboardTemplate.numDomainID = v_numDomainID
         AND DashboardTemplate.numTemplateID = v_numDashboardTemplateID
         AND ReportDashboard.numDomainID = v_numDomainID
         AND ReportDashboard.numUserCntID = v_numUserCntID
         AND (ReportListMaster.numDomainID = v_numDomainID OR ReportListMaster.bitDefault = true)
         AND (coalesce(v_vcSearchText,'') = '' OR vcReportName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcReportName;
      ELSE
         open SWV_RefCur for
         SELECT
         COUNT(*) OVER() AS "TotalRecords"
				,RLM.numReportID AS "numReportID"
				,1 AS "tintReportType"
				,coalesce(vcReportName,'') AS "vcReportName"
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS "vcReportType"
				,coalesce(RMM.vcModuleName,'') AS "vcModuleName"
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS "vcPerspective"
				,(CASE
         WHEN coalesce(RLM.numDateFieldID,0) > 0 AND LENGTH(coalesce(RLM.vcDateFieldValue,'')) > 0
         THEN
            CONCAT((CASE WHEN bitDateFieldColumnCustom = true THEN(SELECT FLd_label FROM CFW_Fld_Master WHERE Fld_id = numDateFieldID) ELSE(SELECT vcFieldName FROM DycFieldMaster WHERE numFieldID = numDateFieldID) END),' (',(CASE RLM.vcDateFieldValue
            WHEN 'AllTime' THEN 'All Time'
            WHEN 'Custom' THEN 'Custom'
            WHEN 'CurYear' THEN 'Current CY'
            WHEN 'PreYear' THEN 'Previous CY'
            WHEN 'Pre2Year' THEN 'Previous 2 CY'
            WHEN 'Ago2Year' THEN '2 CY Ago'
            WHEN 'NextYear' THEN 'Next CY'
            WHEN 'CurPreYear' THEN 'Current and Previous CY'
            WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
            WHEN 'CurNextYear' THEN 'Current and Next CY'
            WHEN 'CuQur' THEN 'Current CQ'
            WHEN 'CurNextQur' THEN 'Current and Next CQ'
            WHEN 'CurPreQur' THEN 'Current and Previous CQ'
            WHEN 'NextQur' THEN 'Next CQ'
            WHEN 'PreQur' THEN 'Previous CQ'
            WHEN 'LastMonth' THEN 'Last Month'
            WHEN 'ThisMonth' THEN 'This Month'
            WHEN 'NextMonth' THEN 'Next Month'
            WHEN 'CurPreMonth' THEN 'Current and Previous Month'
            WHEN 'CurNextMonth' THEN 'Current and Next Month'
            WHEN 'LastWeek' THEN 'Last Week'
            WHEN 'ThisWeek' THEN 'This Week'
            WHEN 'NextWeek' THEN 'Next Week'
            WHEN 'Yesterday' THEN 'Yesterday'
            WHEN 'Today' THEN 'Today'
            WHEN 'Tomorrow' THEN 'Tomorrow'
            WHEN 'Last7Day' THEN 'Last 7 Days'
            WHEN 'Last30Day' THEN 'Last 30 Days'
            WHEN 'Last60Day' THEN 'Last 60 Days'
            WHEN 'Last90Day' THEN 'Last 90 Days'
            WHEN 'Last120Day' THEN 'Last 120 Days'
            WHEN 'Next7Day' THEN 'Next 7 Days'
            WHEN 'Next30Day' THEN 'Next 30 Days'
            WHEN 'Next60Day' THEN 'Next 60 Days'
            WHEN 'Next90Day' THEN 'Next 90 Days'
            WHEN 'Next120Day' THEN 'Next 120 Days'
            ELSE '-'
            END),
            ')')
         ELSE
            ''
         END) AS "vcTimeline"
         FROM
         ReportListMaster RLM
         INNER JOIN
         ReportModuleMaster RMM
         ON
         RLM.numReportModuleID = RMM.numReportModuleID
         WHERE
         RLM.numDomainID = v_numDomainID
         AND (coalesce(v_vcSearchText,'') = '' OR vcReportName ilike CONCAT('%',v_vcSearchText,'%'))
         ORDER BY
         vcReportName;
      end if;
   ELSEIF v_tintMode = 2
   then
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID NUMERIC(18,0),
         numReportID NUMERIC(18,0),
         tintReportType SMALLINT,
         vcReportName TEXT,
         vcReportType VARCHAR(100),
         vcModuleName VARCHAR(100),
         vcPerspective VARCHAR(100),
         vcTimeline VARCHAR(100),
         intSortOrder INTEGER
      );
      INSERT INTO tt_TEMP(ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder)
      SELECT
      SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN coalesce(bitDefault,false) = true THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN coalesce(bitDefault,false) = true THEN coalesce(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',coalesce(vcReportName,''),'</a>') END) AS vcReportName
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE(CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS VARCHAR(100)) AS vcReportType
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE coalesce(RMM.vcModuleName,'') END) AS VARCHAR(100)) AS vcModuleName
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE(CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS VARCHAR(100)) AS vcPerspective
			,CAST((CASE
      WHEN coalesce(ReportListMaster.bitDefault,false) = true
      THEN ''
      ELSE(CASE
         WHEN coalesce(ReportListMaster.numDateFieldID,0) > 0 AND LENGTH(coalesce(ReportListMaster.vcDateFieldValue,'')) > 0
         THEN
            CONCAT((CASE WHEN bitDateFieldColumnCustom = true THEN(SELECT FLd_label FROM CFW_Fld_Master WHERE Fld_id = numDateFieldID) ELSE(SELECT vcFieldName FROM DycFieldMaster WHERE numFieldID = numDateFieldID) END),' (',(CASE ReportListMaster.vcDateFieldValue
            WHEN 'AllTime' THEN 'All Time'
            WHEN 'Custom' THEN 'Custom'
            WHEN 'CurYear' THEN 'Current CY'
            WHEN 'PreYear' THEN 'Previous CY'
            WHEN 'Pre2Year' THEN 'Previous 2 CY'
            WHEN 'Ago2Year' THEN '2 CY Ago'
            WHEN 'NextYear' THEN 'Next CY'
            WHEN 'CurPreYear' THEN 'Current and Previous CY'
            WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
            WHEN 'CurNextYear' THEN 'Current and Next CY'
            WHEN 'CuQur' THEN 'Current CQ'
            WHEN 'CurNextQur' THEN 'Current and Next CQ'
            WHEN 'CurPreQur' THEN 'Current and Previous CQ'
            WHEN 'NextQur' THEN 'Next CQ'
            WHEN 'PreQur' THEN 'Previous CQ'
            WHEN 'LastMonth' THEN 'Last Month'
            WHEN 'ThisMonth' THEN 'This Month'
            WHEN 'NextMonth' THEN 'Next Month'
            WHEN 'CurPreMonth' THEN 'Current and Previous Month'
            WHEN 'CurNextMonth' THEN 'Current and Next Month'
            WHEN 'LastWeek' THEN 'Last Week'
            WHEN 'ThisWeek' THEN 'This Week'
            WHEN 'NextWeek' THEN 'Next Week'
            WHEN 'Yesterday' THEN 'Yesterday'
            WHEN 'Today' THEN 'Today'
            WHEN 'Tomorrow' THEN 'Tomorrow'
            WHEN 'Last7Day' THEN 'Last 7 Days'
            WHEN 'Last30Day' THEN 'Last 30 Days'
            WHEN 'Last60Day' THEN 'Last 60 Days'
            WHEN 'Last90Day' THEN 'Last 90 Days'
            WHEN 'Last120Day' THEN 'Last 120 Days'
            WHEN 'Next7Day' THEN 'Next 7 Days'
            WHEN 'Next30Day' THEN 'Next 30 Days'
            WHEN 'Next60Day' THEN 'Next 60 Days'
            WHEN 'Next90Day' THEN 'Next 90 Days'
            WHEN 'Next120Day' THEN 'Next 120 Days'
            ELSE '-'
            END),')')
         ELSE
            ''
         END)
      END) AS VARCHAR(100)) AS vcTimeline
			,SRGR.intSortOrder
      FROM
      ScheduledReportsGroupReports SRGR
      INNER JOIN
      ReportListMaster
      ON
      SRGR.numReportID = ReportListMaster.numReportID
      AND SRGR.tintReportType IN(1,2)
      LEFT JOIN
      ReportModuleMaster RMM
      ON
      ReportListMaster.numReportModuleID = RMM.numReportModuleID
      WHERE
      SRGR.numSRGID = v_numSRGID
      AND (ReportListMaster.numDomainID = v_numDomainID OR coalesce(ReportListMaster.bitDefault,false) = true)
      AND SRGR.tintReportType IN(1,2);
      INSERT INTO tt_TEMP(ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder)
      SELECT
      SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN coalesce(bitDefault,false) = true THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN coalesce(bitDefault,false) = true THEN coalesce(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',coalesce(vcReportName,''),'</a>') END) AS vcReportName
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE(CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS VARCHAR(100)) AS vcReportType
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE coalesce(RMM.vcModuleName,'') END) AS VARCHAR(100)) AS vcModuleName
			,CAST((CASE WHEN coalesce(ReportListMaster.bitDefault,false) = true THEN '' ELSE(CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS VARCHAR(100)) AS vcPerspective
			,CAST((CASE
      WHEN coalesce(ReportListMaster.bitDefault,false) = true
      THEN ''
      ELSE(CASE
         WHEN coalesce(ReportListMaster.numDateFieldID,0) > 0 AND LENGTH(coalesce(ReportListMaster.vcDateFieldValue,'')) > 0
         THEN
            CONCAT((CASE WHEN bitDateFieldColumnCustom = true THEN(SELECT FLd_label FROM CFW_Fld_Master WHERE Fld_id = numDateFieldID) ELSE(SELECT vcFieldName FROM DycFieldMaster WHERE numFieldID = numDateFieldID) END),' (',(CASE ReportListMaster.vcDateFieldValue
            WHEN 'AllTime' THEN 'All Time'
            WHEN 'Custom' THEN 'Custom'
            WHEN 'CurYear' THEN 'Current CY'
            WHEN 'PreYear' THEN 'Previous CY'
            WHEN 'Pre2Year' THEN 'Previous 2 CY'
            WHEN 'Ago2Year' THEN '2 CY Ago'
            WHEN 'NextYear' THEN 'Next CY'
            WHEN 'CurPreYear' THEN 'Current and Previous CY'
            WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
            WHEN 'CurNextYear' THEN 'Current and Next CY'
            WHEN 'CuQur' THEN 'Current CQ'
            WHEN 'CurNextQur' THEN 'Current and Next CQ'
            WHEN 'CurPreQur' THEN 'Current and Previous CQ'
            WHEN 'NextQur' THEN 'Next CQ'
            WHEN 'PreQur' THEN 'Previous CQ'
            WHEN 'LastMonth' THEN 'Last Month'
            WHEN 'ThisMonth' THEN 'This Month'
            WHEN 'NextMonth' THEN 'Next Month'
            WHEN 'CurPreMonth' THEN 'Current and Previous Month'
            WHEN 'CurNextMonth' THEN 'Current and Next Month'
            WHEN 'LastWeek' THEN 'Last Week'
            WHEN 'ThisWeek' THEN 'This Week'
            WHEN 'NextWeek' THEN 'Next Week'
            WHEN 'Yesterday' THEN 'Yesterday'
            WHEN 'Today' THEN 'Today'
            WHEN 'Tomorrow' THEN 'Tomorrow'
            WHEN 'Last7Day' THEN 'Last 7 Days'
            WHEN 'Last30Day' THEN 'Last 30 Days'
            WHEN 'Last60Day' THEN 'Last 60 Days'
            WHEN 'Last90Day' THEN 'Last 90 Days'
            WHEN 'Last120Day' THEN 'Last 120 Days'
            WHEN 'Next7Day' THEN 'Next 7 Days'
            WHEN 'Next30Day' THEN 'Next 30 Days'
            WHEN 'Next60Day' THEN 'Next 60 Days'
            WHEN 'Next90Day' THEN 'Next 90 Days'
            WHEN 'Next120Day' THEN 'Next 120 Days'
            ELSE '-'
            END),')')
         ELSE
            ''
         END)
      END) AS VARCHAR(100)) AS vcTimeline
			,SRGR.intSortOrder
      FROM
      ScheduledReportsGroupReports SRGR
      INNER JOIN
      ReportDashboard RD
      ON
      SRGR.numReportID = RD.numDashBoardID
      INNER JOIN
      ReportListMaster
      ON
      RD.numReportID = ReportListMaster.numReportID
      LEFT JOIN
      ReportModuleMaster RMM
      ON
      ReportListMaster.numReportModuleID = RMM.numReportModuleID
      WHERE
      SRGR.numSRGID = v_numSRGID
      AND (ReportListMaster.numDomainID = v_numDomainID OR coalesce(ReportListMaster.bitDefault,false) = true)
      AND SRGR.tintReportType = 6;
      INSERT INTO tt_TEMP(ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder)
      SELECT
      SRGR.ID
			,numSearchID AS numReportID,
			(CASE WHEN coalesce(numSharedFromSearchID,0) > 0 THEN 4 ELSE 3 END) AS tintReportType,
			(CASE WHEN coalesce(S.vcSearchConditionJson,'') = '' THEN CONCAT('<a href="javascript:OpenSearchResult(',S.numSearchID,',',coalesce(S.numFormId,0),
         ')">',coalesce(vcSearchName,''),'</a>') ELSE coalesce(vcSearchName,'') END) AS vcReportName,
			CAST('' AS VARCHAR(100)) AS vcReportType,
			CAST((CASE DFM.numFormID WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS VARCHAR(100)) AS vcModuleName,
			CAST('' AS VARCHAR(100)) AS vcPerspective,
			CAST('' AS VARCHAR(100)) AS vcTimeline,
			SRGR.intSortOrder
      FROM
      ScheduledReportsGroupReports SRGR
      INNER JOIN
      SavedSearch S
      ON
      SRGR.numReportID = S.numSearchID
      LEFT JOIN
      dynamicFormMaster DFM
      ON
      DFM.numFormID = S.numFormId
      WHERE
      numDomainID = v_numDomainID
      AND SRGR.numSRGID = v_numSRGID
      AND SRGR.tintReportType IN(4,5);
      open SWV_RefCur for
      SELECT 0 AS "TotalRecords",ID AS "ID",
         numReportID AS "numReportID",
         tintReportType AS "tintReportType",
         vcReportName AS "vcReportName",
         vcReportType AS "vcReportType",
         vcModuleName AS "vcModuleName",
         vcPerspective AS "vcPerspective",
         vcTimeline AS "vcTimeline",
         intSortOrder AS "intSortOrder" FROM tt_TEMP ORDER BY intSortOrder;
   ELSEIF v_tintMode = 3
   then
	
      UPDATE ScheduledReportsGroup SET tintDataCacheStatus = 1 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      open SWV_RefCur for
      SELECT
      SRGR.ID AS "ID"
			,SRGR.numSRGID AS "numSRGID"
			,(CASE
      WHEN SRGR.tintReportType = 6 THEN RLMDashboard.numReportID
      ELSE SRGR.numReportID
      END) AS "numReportID"
			,(CASE
      WHEN SRGR.tintReportType = 6 THEN(CASE WHEN coalesce(RLMDashboard.bitDefault,false) = true THEN 2 ELSE 1 END)
      ELSE SRGR.tintReportType
      END) AS "tintReportType"
			,SRGR.intSortOrder AS "intSortOrder"
			,(CASE
      WHEN SRGR.tintReportType IN(1,2) THEN coalesce(RLM.vcReportName,'')
      WHEN SRGR.tintReportType IN(4,5) THEN coalesce(SS.vcSearchName,'')
      WHEN SRGR.tintReportType = 6 THEN(CASE WHEN coalesce(RD.vcHeaderText,'') <> '' THEN coalesce(RD.vcHeaderText,'') ELSE coalesce(RLMDashboard.vcReportName,'') END)
      ELSE ''
      END) AS "vcReportName"
			,(CASE
      WHEN SRGR.tintReportType = 6 THEN(CASE WHEN coalesce(RLMDashboard.bitDefault,false) = true THEN coalesce(RLMDashboard.intDefaultReportID,0) ELSE 0 END)
      ELSE coalesce(RLM.intDefaultReportID,0)
      END) AS "intDefaultReportID"
			,coalesce(SS.numFormId,0) AS "numFormID"
			,coalesce(SS.numUserCntID,0) AS "numSearchUserCntID"
			,coalesce(SS.vcSearchConditionJson,'') AS "vcSearchConditionJson"
			,coalesce(SS.vcDisplayColumns,'') AS "vcDisplayColumns"
			,coalesce(RD.numDashBoardID,0) AS "numDashBoardID"
      FROM
      ScheduledReportsGroup SRG
      INNER JOIN
      ScheduledReportsGroupReports SRGR
      ON
      SRG.ID = SRGR.numSRGID
      LEFT JOIN
      ReportListMaster RLM
      ON
      SRGR.numReportID = RLM.numReportID
      AND SRGR.tintReportType IN(1,2)
      LEFT JOIN
      ReportDashboard RD
      ON
      SRGR.numReportID = RD.numDashBoardID
      AND SRGR.tintReportType = 6
      LEFT JOIN
      ReportListMaster RLMDashboard
      ON
      RD.numReportID = RLMDashboard.numReportID
      LEFT JOIN
      SavedSearch SS
      ON
      SRGR.numReportID = SS.numSearchID
      AND SRGR.tintReportType IN(4,5)
      WHERE
      SRG.numDomainID = v_numDomainID
      AND SRG.ID = v_numSRGID
      ORDER BY
      intSortOrder;
   end if;
   RETURN;
END; $$;


