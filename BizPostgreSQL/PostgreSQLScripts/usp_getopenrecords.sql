CREATE OR REPLACE FUNCTION USP_GetOpenRecords(v_DomainID NUMERIC,
	v_numDivisionID NUMERIC,
    v_byteMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 1 then
            
      open SWV_RefCur for
      SELECT numOppId AS "numOppId",
                        vcpOppName AS "vcpOppName"
      FROM    OpportunityMaster
      WHERE   tintActive = 1
      AND tintshipped = 0
      AND tintopptype = 1
      AND tintoppstatus = 0
      AND numDomainId = v_DomainID
      AND numDivisionId = v_numDivisionID
      ORDER BY numOppId DESC;
   end if;
            --Purchase Opp 
   IF v_byteMode = 2 then
            
      open SWV_RefCur for
      SELECT  numOppId AS "numOppId",
                        vcpOppName AS "vcpOppName"
      FROM    OpportunityMaster
      WHERE   tintActive = 1
      AND tintshipped = 0
      AND tintopptype = 2
      AND tintoppstatus = 0
      AND numDomainId = v_DomainID
      AND numDivisionId = v_numDivisionID
      ORDER BY numOppId DESC;
   end if; 
            --sales Order
   IF v_byteMode = 3 then
            
      open SWV_RefCur for
      SELECT  numOppId AS "numOppId",
                        vcpOppName AS "vcpOppName"
      FROM    OpportunityMaster
      WHERE   tintActive = 1
      AND tintshipped = 0
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND numDomainId = v_DomainID
      AND numDivisionId = v_numDivisionID
      ORDER BY numOppId DESC;
   end if; 
            --Purchase Order
   IF v_byteMode = 4 then
            
      open SWV_RefCur for
      SELECT  numOppId AS "numOppId",
                        vcpOppName AS "vcpOppName"
      FROM    OpportunityMaster
      WHERE   tintActive = 1
      AND tintshipped = 0
      AND tintopptype = 2
      AND tintoppstatus = 1
      AND numDomainId = v_DomainID
      AND numDivisionId = v_numDivisionID
      ORDER BY numOppId DESC;
   end if;
   RETURN;
END; $$;
--created by anoop jayaraj


