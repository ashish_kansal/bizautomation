-- Stored procedure definition script USP_Item_SelectedChildItemDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_SelectedChildItemDetails(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numChildItemCode NUMERIC(18,0),
	v_vcKitChilds TEXT,
	v_bitEdit BOOLEAN, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitCalAmtBasedonDepItems  BOOLEAN;
   v_tintKitAssemblyPriceBasedOn  SMALLINT;
   v_monListPrice  DECIMAL(20,5);
   v_bitKit  BOOLEAN;
   v_bitHasChildKits  BOOLEAN;
BEGIN
   select   coalesce(bitCalAmtBasedonDepItems,false), coalesce(tintKitAssemblyPriceBasedOn,1), coalesce(monListPrice,0), coalesce(bitKitParent,false), (CASE
   WHEN coalesce(bitKitParent,false) = true
   THEN(CASE
      WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = Item.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
      THEN 1
      ELSE 0
      END)
   ELSE 0
   END) INTO v_bitCalAmtBasedonDepItems,v_tintKitAssemblyPriceBasedOn,v_monListPrice,
   v_bitKit,v_bitHasChildKits FROM
   Item WHERE
   numItemCode = v_numItemCode;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(300),
      vcSKU VARCHAR(100),
      vcType VARCHAR(20),
      numQty DOUBLE PRECISION,
      monListPrice DECIMAL(20,5),
      monVendorCost DECIMAL(20,5),
      monAverageCost DECIMAL(20,5),
      monPrice DECIMAL(20,5),
      numSequence INTEGER
   );

   IF coalesce(v_bitKit,false) = true then
	
      IF LENGTH(coalesce(v_vcKitChilds,'')) = 0 then
		
         select   COALESCE(coalesce(v_vcKitChilds,'') || ', ','') || CONCAT('0-',numChildItemID,'-',numQtyItemsReq*fn_UOMConversion(numUOMID,I.numItemCode,I.numDomainID,I.numPurchaseUnit),'-',coalesce(sintOrder,1)) INTO v_vcKitChilds FROM
         ItemDetails
         INNER JOIN
         Item I
         ON
         numChildItemID = numItemCode WHERE
         numItemKitID = v_numItemCode;
      end if;
      IF LENGTH(coalesce(v_vcKitChilds,'')) > 0 then
         DROP TABLE IF EXISTS tt_TEMPEXISTINGITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPEXISTINGITEMS
         (
            vcItem VARCHAR(100)
         );
         INSERT INTO tt_TEMPEXISTINGITEMS(vcItem)
         SELECT
         OutParam
         FROM
         SplitString(v_vcKitChilds,',');
         INSERT INTO tt_TEMP(numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence)
         SELECT
         I.numItemCode
				,coalesce(I.vcItemName,'') AS vcItemName
				,coalesce(I.vcSKU,'') AS vcSKU
				,CAST((CASE
         WHEN charItemType = 'P'
         THEN CONCAT('Inventory',CASE
            WHEN coalesce(I.bitKitParent,false) = true THEN ' (Kit)'
            WHEN coalesce(I.bitAssembly,false) = true THEN ' (Assembly)'
            WHEN coalesce(I.numItemGroup,0) > 0 AND (coalesce(I.bitMatrix,false) = true OR EXISTS(SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID = I.numItemGroup AND tintType = 2)) THEN ' (Matrix)'
            ELSE ''
            END)
         WHEN charItemType = 'S' THEN 'Service'
         WHEN charItemType = 'A' THEN 'Accessory'
         WHEN charItemType = 'N' THEN 'Non-Inventory'
         END) AS VARCHAR(20)) AS vcType
				,T1.numQty AS numQty
				,coalesce(monListPrice,0) AS monListPrice
				,coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) AS monVendorCost
				,coalesce(monAverageCost,0) AS monAverageCost
				,(CASE
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 4
         THEN coalesce(T1.numQty,0)*coalesce(monListPrice,0)
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 3
         THEN coalesce(T1.numQty,0)*coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 2
         THEN coalesce(T1.numQty,0)*coalesce(monAverageCost,0)
         ELSE coalesce(T1.numQty,0)*coalesce(monListPrice,0)
         END)
				,T1.numSequence
         FROM(SELECT
					split_part(vcItem,'-',1)::NUMERIC As numKitItemID
					,split_part(vcItem,'-',2)::NUMERIC As numChildItemID
					,split_part(vcItem,'-',3)::NUMERIC As numQty
					,(CASE WHEN COALESCE(split_part(vcItem,'-',4),'') = '' THEN '0' ELSE split_part(vcItem,'-',4) END)::NUMERIC As numSequence
            FROM(SELECT
               vcItem
               FROM
               tt_TEMPEXISTINGITEMS) As x) T1
         INNER JOIN
         Item I
         ON
         T1.numChildItemID = I.numItemCode
         LEFT JOIN
         Vendor V
         ON
         I.numVendorID = V.numVendorID
         AND I.numItemCode = V.numItemCode;
      end if;
      IF(SELECT COUNT(*) FROM tt_TEMP WHERE numItemCode = v_numChildItemCode) = 0 then
		
         INSERT INTO tt_TEMP(numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence)
         SELECT
         I.numItemCode
				,coalesce(I.vcItemName,'') AS vcItemName
				,coalesce(I.vcSKU,'') AS vcSKU
				,CAST((CASE
         WHEN charItemType = 'P'
         THEN CONCAT('Inventory',CASE
            WHEN coalesce(I.bitKitParent,false) = true THEN ' (Kit)'
            WHEN coalesce(I.bitAssembly,false) = true THEN ' (Assembly)'
            WHEN coalesce(I.numItemGroup,0) > 0 AND (coalesce(I.bitMatrix,false) = true OR EXISTS(SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID = I.numItemGroup AND tintType = 2)) THEN ' (Matrix)'
            ELSE ''
            END)
         WHEN charItemType = 'S' THEN 'Service'
         WHEN charItemType = 'A' THEN 'Accessory'
         WHEN charItemType = 'N' THEN 'Non-Inventory'
         END) AS VARCHAR(20)) AS vcType
				,1 AS numQty
				,coalesce(monListPrice,0) AS monListPrice
				,coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) AS monVendorCost
				,coalesce(monAverageCost,0) AS monAverageCost
				,(CASE
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 4
         THEN 1*coalesce(monListPrice,0)
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 3
         THEN 1*coalesce(monCost,0)*fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
         WHEN v_bitCalAmtBasedonDepItems = true AND v_tintKitAssemblyPriceBasedOn = 2
         THEN 1*coalesce(monAverageCost,0)
         WHEN v_bitCalAmtBasedonDepItems = true
         THEN 1*coalesce(monListPrice,0)
         ELSE 0
         END)
				,coalesce((SELECT MAX(numSequence) FROM tt_TEMP),0)+1
         FROM
         Item I
         LEFT JOIN
         Vendor V
         ON
         I.numVendorID = V.numVendorID
         AND I.numItemCode = V.numItemCode
         WHERE
         I.numItemCode = v_numChildItemCode;
      end if;
   end if;
	
	

   open SWV_RefCur for
   SELECT
   v_bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems
		 ,v_tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
		 ,v_monListPrice AS monListPrice
		 ,v_bitKit AS bitKit
		 ,v_bitHasChildKits AS bitHasChildKits
		 ,(CASE WHEN v_tintKitAssemblyPriceBasedOn = 4 AND v_bitCalAmtBasedonDepItems = true THEN v_monListPrice ELSE 0 END)+coalesce((SELECT SUM(monPrice) FROM tt_TEMP),0) AS monPrice;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMP;
   RETURN;
END; $$;


