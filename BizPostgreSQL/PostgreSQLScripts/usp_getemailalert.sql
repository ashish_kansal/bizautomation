CREATE OR REPLACE FUNCTION public.usp_getemailalert(
	v_numdomainid numeric,
	v_numcontactid numeric,
	v_numusercontactid numeric,
	INOUT SWV_RefCur refcursor)
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
   v_bitOpenActionItem  BOOLEAN;
   v_vcOpenActionMsg  VARCHAR(50);
   v_bitOpencases  BOOLEAN;
   v_vcOpenCasesMsg  VARCHAR(50);
   v_bitOpenProject  BOOLEAN;
   v_vcOpenProjectMsg  VARCHAR(50);
   v_bitOpenSalesOpp  BOOLEAN;
   v_vcOpenSalesOppMsg  VARCHAR(50);
   v_bitOpenPurchaseOpp  BOOLEAN;
   v_vcOpenPurchaseOppMsg  VARCHAR(50);
   v_bitBalancedue  BOOLEAN;
   v_vcBalancedueMsg  VARCHAR(50);
   v_monBalancedue  DECIMAL(20,5); 
   v_bitARAccount  BOOLEAN;
   v_vcARAccountMsg  VARCHAR(50);
   v_bitAPAccount  BOOLEAN;
   v_vcAPAccountMsg  VARCHAR(50);
   v_bitUnreadEMail  BOOLEAN;
   v_intUnreadEmail  INTEGER;
   v_vcUnreadEmailMsg  VARCHAR(50);
   v_bitPurchasedPast  BOOLEAN;
   v_monPurchasedPast  DECIMAL(20,5); 
   v_vcPurchasedPastMsg  VARCHAR(50);
   v_bitSoldPast  BOOLEAN;
   v_monSoldPast  DECIMAL(20,5); 
   v_vcSoldPastMsg  VARCHAR(50);
   v_bitCustomField  BOOLEAN;
   v_numCustomFieldId  NUMERIC; 
   v_vcCustomFieldValue  VARCHAR(50);
   v_vcCustomfieldMsg  VARCHAR(50);
   v_numCount  NUMERIC; 
   v_strMsg  VARCHAR(50);
   v_numDivisionID  NUMERIC;
BEGIN
-- This function was converted on Sat Apr 10 16:41:41 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   v_numCount := 0;
   select   bitOpenActionItem, vcOpenActionMsg, bitOpenCases, vcOpenCasesMsg, bitOpenProject, vcOpenProjectMsg, bitOpenSalesOpp, vcOpenSalesOppMsg, bitOpenPurchaseOpp, vcOpenPurchaseOppMsg, bitBalancedue, monBalancedue, vcBalancedueMsg, bitARAccount, vcARAccountMsg, bitAPAccount, vcAPAccountMsg, bitUnreadEmail, intUnreadEmail, vcUnreadEmailMsg, bitPurchasedPast, monPurchasedPast, vcPurchasedPastMsg, bitSoldPast, monSoldPast, vcSoldPastMsg, bitCustomField, numCustomFieldId, vcCustomFieldValue, vcCustomFieldMsg INTO v_bitOpenActionItem,v_vcOpenActionMsg,v_bitOpencases,v_vcOpenCasesMsg,
   v_bitOpenProject,v_vcOpenProjectMsg,v_bitOpenSalesOpp,v_vcOpenSalesOppMsg,
   v_bitOpenPurchaseOpp,v_vcOpenPurchaseOppMsg,v_bitBalancedue,v_monBalancedue,
   v_vcBalancedueMsg,v_bitARAccount,v_vcARAccountMsg,v_bitAPAccount,
   v_vcAPAccountMsg,v_bitUnreadEMail,v_intUnreadEmail,v_vcUnreadEmailMsg,
   v_bitPurchasedPast,v_monPurchasedPast,v_vcPurchasedPastMsg,v_bitSoldPast,
   v_monSoldPast,v_vcSoldPastMsg,v_bitCustomField,v_numCustomFieldId,
   v_vcCustomFieldValue,v_vcCustomfieldMsg FROM    AlertConfig WHERE   AlertConfig.numDomainID = v_numDomainId
   AND AlertConfig.numContactId = v_numUserContactId;
      
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      bitOpenActionItem BOOLEAN,
      numOpenActionCount NUMERIC(18,0),
      bitOpenCases BOOLEAN,
      numOpenCasesCount NUMERIC(18,0),
      bitOpenProject BOOLEAN,
      numOpenProjectCount NUMERIC(18,0),
      bitOpenSalesOpp BOOLEAN,
      numOpenSalesOppCount NUMERIC(15,0),
      bitOpenPurchaseOpp BOOLEAN,
      numOpenPurchaseOppCount NUMERIC(15,0),
      bitBalancedue BOOLEAN,
      monBalancedue DECIMAL(20,5),
      monBalancedueCount DECIMAL(20,5),
      bitUnreadEmail BOOLEAN,
      intUnreadEmail INTEGER,
      intUnreadEmailCount INTEGER,
      bitPurchasedPast BOOLEAN,
      monPurchasedPast DECIMAL(20,5),
      monPurchasedPastCount DECIMAL(20,5),
      bitSoldPast BOOLEAN,
      monSoldPast DECIMAL(20,5),
      monSoldPastCount DECIMAL(20,5),
      numDivisionID NUMERIC(9,0) 
   );
      --TAKE divisionID
   INSERT INTO tt_TEMP  VALUES(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

   SELECT   numDivisionId INTO v_numDivisionID FROM     AdditionalContactsInformation WHERE    numContactId = v_numContactId;
   RAISE NOTICE 'DivisionId :%',SUBSTR(CAST(v_numDivisionID AS VARCHAR(30)),1,30);
   RAISE NOTICE 'bitOpenActionItem :%',SUBSTR(CAST(v_bitOpenActionItem AS VARCHAR(30)),1,30);
   IF v_bitOpenActionItem = true
   AND(SELECT    SUM(OpenActionItemCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID and v.numContactId = v_numContactId) > 0 then
    
      UPDATE tt_TEMP SET bitOpenActionItem = true,numOpenActionCount =(SELECT SUM(OpenActionItemCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID and v.numContactId = v_numContactId);
   ELSE
      UPDATE tt_TEMP SET bitOpenActionItem = false,numOpenActionCount = 0;
   end if;
   IF v_bitOpencases = true
   AND(SELECT    SUM(OpenCaseCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) > 0 then
    
      UPDATE tt_TEMP SET bitOpenCases = true,numOpenCasesCount =(SELECT SUM(OpenCaseCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID);
   ELSE
      UPDATE tt_TEMP SET bitOpenCases = false,numOpenCasesCount = 0;
   end if;
   IF v_bitOpenProject = true
   AND(SELECT    SUM(OpenProjectCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) > 0 then
    
      UPDATE tt_TEMP SET bitOpenProject = true,numOpenProjectCount =(SELECT SUM(OpenProjectCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID);
   ELSE
      UPDATE tt_TEMP SET bitOpenProject = false,numOpenProjectCount = 0;
   end if; 
   IF v_bitOpenSalesOpp = true
   AND(SELECT    SUM(OpenSalesOppCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) > 0 then
    
      UPDATE tt_TEMP SET bitOpenSalesOpp = true,numOpenSalesOppCount =(SELECT SUM(OpenSalesOppCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID);
   ELSE
      UPDATE tt_TEMP SET bitOpenSalesOpp = false,numOpenSalesOppCount = 0;
   end if;
   IF v_bitOpenPurchaseOpp = true
   AND(SELECT    SUM(OpenPurchaseOppCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) > 0 then
    
      UPDATE tt_TEMP SET bitOpenPurchaseOpp = true,numOpenPurchaseOppCount =(SELECT SUM(OpenPurchaseOppCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID);
   ELSE
      UPDATE tt_TEMP SET bitOpenPurchaseOpp = false,numOpenPurchaseOppCount = 0;
   end if;
   IF v_bitBalancedue = true
   AND(SELECT    SUM(TotalBalanceDue)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) > v_monBalancedue then
    
      UPDATE tt_TEMP SET bitBalancedue = true,monBalancedueCount =(SELECT SUM(TotalBalanceDue)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID),
      monBalancedue = v_monBalancedue;
   ELSE
      UPDATE tt_TEMP SET bitBalancedue = false,monBalancedueCount = 0,monBalancedue = v_monBalancedue;
   end if;
   IF v_bitUnreadEMail = true
   AND(SELECT    SUM(UnreadEmailCount)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) >= v_intUnreadEmail then
    
      UPDATE tt_TEMP SET bitUnreadEmail = true,intUnreadEmailCount =(SELECT SUM(UnreadEmailCount)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID),
      intUnreadEmail = v_intUnreadEmail;
   ELSE
      UPDATE tt_TEMP SET bitUnreadEmail = false,intUnreadEmailCount = 0,intUnreadEmail = v_intUnreadEmail;
   end if;
   IF v_bitPurchasedPast = true
   AND(SELECT    SUM(PastPurchaseOrderValue)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) >= v_monPurchasedPast then
    
      UPDATE tt_TEMP SET bitPurchasedPast = true,monPurchasedPastCount =(SELECT SUM(PastPurchaseOrderValue)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID),monPurchasedPast = v_monPurchasedPast;
   ELSE
      UPDATE tt_TEMP SET bitPurchasedPast = false,monPurchasedPastCount = 0,monPurchasedPast = v_monPurchasedPast;
   end if;
   IF v_bitSoldPast = true
   AND(SELECT    SUM(PastSalesOrderValue)
   FROM      VIEW_Email_Alert_Config v
   WHERE     v.numDomainid = v_numDomainId
   AND v.numDivisionID = v_numDivisionID) >= v_monSoldPast then
    
      UPDATE tt_TEMP SET bitSoldPast = true,monSoldPastCount =(SELECT SUM(PastSalesOrderValue)
      FROM      VIEW_Email_Alert_Config v
      WHERE     v.numDomainid = v_numDomainId
      AND v.numDivisionID = v_numDivisionID),
      monSoldPast = v_monSoldPast;
   ELSE
      UPDATE tt_TEMP SET bitSoldPast = false,monSoldPastCount = 0,monSoldPast = v_monSoldPast;
   end if;

   UPDATE tt_TEMP SET numDivisionID = v_numDivisionID;

   OPEN SWV_RefCur FOR SELECT * FROM tt_TEMP; 

   RETURN;
END;
$BODY$;

