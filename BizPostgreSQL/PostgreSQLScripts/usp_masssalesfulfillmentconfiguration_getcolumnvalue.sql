CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentConfiguration_GetColumnValue(v_numDomainID NUMERIC(18,0),
	v_vcDbColumnName VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcDbColumnName = 'bitAutoWarehouseSelection' then
	
      open SWV_RefCur for
      SELECT coalesce(bitAutoWarehouseSelection,false) AS bitAutoWarehouseSelection FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'bitBOOrderStatus'
   then
	
      open SWV_RefCur for
      SELECT coalesce(bitBOOrderStatus,false) AS bitBOOrderStatus FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


