-- Stored procedure definition script USP_UpdateCompetitor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCompetitor(v_strFieldList TEXT DEFAULT '' ,    
v_numItemCode NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN       
	UPDATE 
		Competitor
	SET 
		vcPrice = X.Price
		,dtDateEntered = X.Date
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFieldList AS XML)
		COLUMNS
			id FOR ORDINALITY,
			CompetitorID NUMERIC(9,0) PATH 'CompetitorID',
			Price VARCHAR(100) PATH 'Price',
			Date TIMESTAMP PATH 'Date'
	) AS X 
	WHERE 
		numCompetitorID = X.CompetitorID 
		AND numItemCode = v_numItemCode;      

   RETURN;
END; $$;


