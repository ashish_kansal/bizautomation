-- Stored procedure definition script USP_ManageOppAssociatedContact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageOppAssociatedContact(v_numOppId NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numRole NUMERIC(9,0) DEFAULT 0,
v_bitPartner BOOLEAN DEFAULT NULL,
v_byteMode SMALLINT DEFAULT NULL,
v_bitSubscribedEmailAlert BOOLEAN DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_byteMode = 0 then

      insert into OpportunityContact(numOppId, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
 values(v_numOppId,v_numContactID,v_numRole,v_bitPartner,v_bitSubscribedEmailAlert);
   ELSEIF v_byteMode = 1
   then

      delete from OpportunityContact where numOppId = v_numOppId and numContactID = v_numContactID;
   end if;






   open SWV_RefCur for SELECT  a.numContactId,vcCompanyName || ', ' || coalesce(Lst.vcData,'-') as Company,
  a.vcFirstName || ' ' || a.vcLastname as Name,
  case when a.numPhone <> '' then a.numPhone || case when a.numPhoneExtension <> '' then ' - ' || a.numPhoneExtension else '' end  else '' end as Phone,
 a.vcEmail as Email,
  cast(b.vcData as VARCHAR(255)) as ContactRole,
  cast(opp.numRole as VARCHAR(255)) as ContRoleId,
  coalesce(bitPartner,false) as bitPartner,
  a.numContactType,CASE WHEN opp.bitSubscribedEmailAlert = true THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert,D.numDivisionID AS numDivisionID FROM OpportunityContact opp
   join AdditionalContactsInformation a on
   a.numContactId = opp.numContactID
   join DivisionMaster D
   on a.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   left join Listdetails b
   on b.numListItemID = opp.numRole
   left join Listdetails Lst
   on Lst.numListItemID = C.numCompanyType
   WHERE opp.numOppId = v_numOppId and a.numDomainID = v_numDomainID;
END; $$;












