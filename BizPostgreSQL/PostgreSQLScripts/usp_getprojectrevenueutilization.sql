-- Stored procedure definition script USP_GetProjectRevenueUtilization for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProjectRevenueUtilization(v_numDomainId NUMERIC,
    v_numUserCntID NUMERIC,
    v_dtStartDate TIMESTAMP,
    v_dtEndDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPPROJECT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPROJECT
   (
      numProId NUMERIC(9,0),
      vcProjectID VARCHAR(100),
      vcProjectName VARCHAR(100),
      vcCompanyName VARCHAR(100),
      vcRecordOwner VARCHAR(100),
      vcInternalProjectManager VARCHAR(100),
      vcCustomerProjectManager VARCHAR(100),
      vcReviewer VARCHAR(500),
      monIncome DECIMAL(20,5),
      monExpense DECIMAL(20,5),
      monBalance DECIMAL(20,5),
      numSO INTEGER,
      numPO INTEGER,
      numBills INTEGER
   );
   
   INSERT INTO tt_TEMPPROJECT
   SELECT numProId,vcProjectID,vcProjectName,CAST(fn_GetComapnyName(Pro.numDivisionId) AS VARCHAR(100)) AS vcCompanyName,fn_GetContactName(Pro.numRecOwner) AS vcRecordOwner,fn_GetContactName(Pro.numIntPrjMgr) AS vcInternalProjectManager,
   fn_GetContactName(Pro.numCustPrjMgr) AS vcCustomerProjectManager,
   COALESCE((SELECT string_agg(CONCAT(A.vcFirstName,' ',A.vcLastname,CASE WHEN coalesce(SR.numContactType,0) > 0 THEN CONCAT('(',fn_GetListItemName(SR.numContactType),')') ELSE '' END),',') 
         FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
         JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
         WHERE SR.numDomainID = Pro.numdomainId AND SR.numModuleID = 5 AND SR.numRecordID = Pro.numProId
         AND UM.numDomainID = Pro.numdomainId and UM.numDomainID = A.numDomainID),'') AS vcReviewer,
   0 AS monIncome,0 AS monExpense,0 AS monBalance,0 AS numSO,0 AS numPO,0 AS numBills
   FROM ProjectsMaster Pro WHERE Pro.numdomainId = v_numDomainId;


			--SO
   UPDATE tt_TEMPPROJECT SET monIncome = OPP.monIncome+OPP.monIncome,numSO = OPP.numSO+OPP.numSO
   FROM(SELECT temp.numProId,SUM(coalesce(OPP.monPrice,0)*CASE numType
   WHEN 1 THEN coalesce(OPP.numUnitHour,0)
   ELSE coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
   END:: bigint) AS monIncome ,COUNT(DISTINCT OM.numOppId) AS numSO
   FROM    timeandexpense TE
   INNER JOIN tt_TEMPPROJECT temp ON TE.numProId = temp.numProId
   INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
   INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
   AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectID = TE.numProId
   WHERE   TE.numDomainID = v_numDomainId
   AND numType = 1 --Billable 
   AND OM.tintopptype = 1
   AND (TE.dtTCreatedOn >= v_dtStartDate AND TE.dtTCreatedOn <= v_dtEndDate) GROUP BY temp.numProId) AS OPP WHERE OPP.numProId=tt_TEMPPROJECT.numProId;

			--SO 
   UPDATE tt_TEMPPROJECT SET monIncome = OPP.monIncome+OPP.monIncome,numSO = OPP.numSO+OPP.numSO
   FROM(SELECT temp.numProId,SUM(coalesce(OPP.monPrice,0)*coalesce(OPP.numUnitHour,0)) AS monIncome,COUNT(DISTINCT OM.numOppId) AS numSO
   FROM    OpportunityMaster OM
   INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
   INNER JOIN tt_TEMPPROJECT temp ON OPP.numProjectID = temp.numProId
   WHERE  OM.numDomainId = v_numDomainId AND OPP.numProjectStageID = 0 AND OM.tintopptype = 1
   AND (OM.bintCreatedDate >= v_dtStartDate AND OM.bintCreatedDate <= v_dtEndDate)
   GROUP BY temp.numProId) AS OPP WHERE OPP.numProId=tt_TEMPPROJECT.numProId;

			--PO
   UPDATE tt_TEMPPROJECT SET monExpense = OPP.monExpense+OPP.monExpense,numPO = OPP.numPO+OPP.numPO
   FROM(SELECT temp.numProId,SUM((OPP.monPrice*OPP.numUnitHour)) AS monExpense,COUNT(DISTINCT OM.numOppId) AS numPO
   from  OpportunityMaster OM
   JOIN OpportunityItems OPP ON OM.numOppId = OPP.numOppId
   INNER JOIN tt_TEMPPROJECT temp ON OPP.numProjectID = temp.numProId
   where  OM.numDomainId = v_numDomainId
   AND OM.tintopptype = 2
   AND (OM.bintCreatedDate >= v_dtStartDate AND OM.bintCreatedDate <= v_dtEndDate) GROUP BY temp.numProId) AS OPP WHERE OPP.numProId=tt_TEMPPROJECT.numProId	;	

			--Bill
   UPDATE tt_TEMPPROJECT SET monExpense = OPP.monExpense+Opp.monExpense,numBills = OPP.numBills::bigint+Opp.numBills::bigint
   FROM(SELECT temp.numProId,SUM(OBD.monAmount) AS monExpense,COUNT(DISTINCT PO.numBillId) AS numBills
   from  ProjectsOpportunities PO
   INNER JOIN tt_TEMPPROJECT temp ON PO.numProId = temp.numProId
   LEFT OUTER JOIN OpportunityBizDocsDetails OBD ON OBD.numBizDocsPaymentDetId = PO.numBillId
   LEFT OUTER JOIN Chart_Of_Accounts COA ON OBD.numExpenseAccount = COA.numAccountId
   where  PO.numDomainId = v_numDomainId
   and coalesce(PO.numBillId,0) > 0
   AND (OBD.dtCreationDate >= v_dtStartDate AND OBD.dtCreationDate <= v_dtEndDate)
   GROUP BY temp.numProId) AS OPP WHERE OPP.numProId=tt_TEMPPROJECT.numProId;	
			
			--Employee Expense
   UPDATE tt_TEMPPROJECT SET monExpense = OPP.monExpense+Opp.monExpense
   FROM(SELECT temp.numProId,SUM(coalesce(Cast((EXTRACT(DAY FROM dttodate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM dttodate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM dttodate -TE.dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION),0)*coalesce(UM.monHourlyRate,0)) AS monExpense
   FROM    timeandexpense TE
   INNER JOIN tt_TEMPPROJECT temp ON TE.numProId = temp.numProId
   join UserMaster UM on UM.numUserDetailId = TE.numUserCntID
   WHERE   TE.numDomainID = v_numDomainId
   AND numType in(1,2) --Non Billable & Billable
   AND TE.numUserCntID > 0
   AND (TE.dtTCreatedOn >= v_dtStartDate AND TE.dtTCreatedOn <= v_dtEndDate)
   GROUP BY temp.numProId) AS OPP WHERE OPP.numProId=tt_TEMPPROJECT.numProId;	
					
		
   UPDATE tt_TEMPPROJECT SET monBalance = monIncome -monExpense; 
   open SWV_RefCur for SELECT * FROM tt_TEMPPROJECT WHERE numSO > 0 OR numPO > 0 OR numBills > 0 OR monExpense != 0 OR monIncome != 0;

   RETURN;
END; $$;    













