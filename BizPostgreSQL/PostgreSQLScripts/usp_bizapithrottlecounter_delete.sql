-- Stored procedure definition script USP_BizAPIThrottleCounter_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 April 2014
-- Description:	Deletes Throttle counter for id
-- =============================================
CREATE OR REPLACE FUNCTION USP_BizAPIThrottleCounter_Delete(v_ID VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
   IF v_ID = 'DeleteAll' then
      DELETE FROM BizAPIThrottleCounter;
   ELSE
      DELETE FROM BizAPIThrottleCounter WHERE vcID = v_ID;
   end if;
   RETURN;
END; $$;


