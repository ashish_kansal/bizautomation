-- Stored procedure definition script USP_GetAcntTypeId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAcntTypeId(v_numAccountId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numParntAcntTypeID,0) as NUMERIC(18,0)) from Chart_Of_Accounts Where numAccountId = v_numAccountId And numDomainId = v_numDomainId;
END; $$;












