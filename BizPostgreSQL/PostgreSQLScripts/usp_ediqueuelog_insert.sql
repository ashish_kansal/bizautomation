-- Stored procedure definition script USP_EDIQueueLog_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EDIQueueLog_Insert(v_numEDIQueueID NUMERIC(18,0),
	v_EDIType INTEGER,
	v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_vcLog TEXT,
	v_bitSuccess BOOLEAN,
	v_vcExceptionMessage TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
   v_tintOppStatus  SMALLINT;
BEGIN
   INSERT INTO EDIQueueLog(numEDIQueueID
		,numDomainID
		,numOppID
		,vcLog
		,bitSuccess
		,vcExceptionMessage
		,dtDate)
	VALUES(v_numEDIQueueID
		,v_numDomainID
		,v_numOppID
		,v_vcLog
		,v_bitSuccess
		,v_vcExceptionMessage
		,TIMEZONE('UTC',now()));

	
   IF coalesce(v_numEDIQueueID,0) > 0 then
	
      UPDATE
      EDIQueue
      SET
      bitExecuted = true,bitSuccess = v_bitSuccess,dtExecutionDate = TIMEZONE('UTC',now())
      WHERE
      ID = v_numEDIQueueID;
   end if;

   IF v_EDIType = 940 then
	
      IF coalesce(v_bitSuccess,false) = false then
		
         select   tintoppstatus, tintopptype INTO v_tintOppStatus,v_tintOppType from OpportunityMaster where numOppId = v_numOppID;
         UPDATE OpportunityMaster SET numStatus = 15446,bintModifiedDate = TIMEZONE('UTC',now()),tintEDIStatus = 8 WHERE numOppId = v_numOppID; --15446: Shipment Request (940) Failed

         IF v_tintOppType = 1 AND v_tintOppStatus = 1 then
			
            PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,0,v_numOppID,15446);
         end if;
         IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppID AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1 AND coalesce(numStatus,0) != 15446) then
			 
	   				 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- tinyint
            PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,
            v_numOppBizDocsId := 0,v_numOrderStatus := 15446,v_numUserCntID := 0,
            v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
         end if;
      ELSE
         IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 3) then
			
            UPDATE OpportunityMaster SET tintEDIStatus = 3,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --3: 940 Sent
			
            INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
				VALUES(v_numDomainID,v_numOppID,3,TIMEZONE('UTC',now()));
         end if;
      end if;
   ELSEIF v_EDIType = 940997
   then -- 940 Acknowledge
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 4) then
		
         UPDATE OpportunityMaster SET tintEDIStatus = 4,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --3: 940 Acknowledged
			
         INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,4,TIMEZONE('UTC',now()));
      end if;
   ELSEIF v_EDIType = 8565 AND coalesce(v_bitSuccess,false) = true
   then -- 856 Received from 3PL
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 5) then
		
         UPDATE OpportunityMaster SET tintEDIStatus = 5,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --5: 856 Received
			
         INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,5,TIMEZONE('UTC',now()));
      end if;
   ELSEIF v_EDIType = 856997 AND coalesce(v_bitSuccess,false) = true
   then -- 856 Acknowledged
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 6) then
		
         UPDATE OpportunityMaster SET tintEDIStatus = 6,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --6: 856 Acknowledged
			
         INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,6,TIMEZONE('UTC',now()));
      end if;
   ELSEIF v_EDIType = 856810997 AND coalesce(v_bitSuccess,false) = true
   then -- 856/810 Acknowledged
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 10) then
		
         UPDATE OpportunityMaster SET tintEDIStatus = 10,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --10: 856/810 Acknowledged
			
         INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,10,TIMEZONE('UTC',now()));
      end if;
   ELSEIF v_EDIType = 8566
   then -- 856 Sent to EDI
	
      IF coalesce(v_bitSuccess,false) = false then
		
         select   tintoppstatus, tintopptype INTO v_tintOppStatus,v_tintOppType from OpportunityMaster where numOppId = v_numOppID;
         UPDATE OpportunityMaster SET numStatus = 15448,bintModifiedDate = TIMEZONE('UTC',now()),tintEDIStatus = 9 WHERE numOppId = v_numOppID; --15448: Send 856 & 810 Failed

         IF v_tintOppType = 1 AND v_tintOppStatus = 1 then
			
            PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,0,v_numOppID,15448);
         end if;
         IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppID AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1 AND coalesce(numStatus,0) != 15448) then
			 
	   				 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- tinyint
            PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,
            v_numOppBizDocsId := 0,v_numOrderStatus := 15448,v_numUserCntID := 0,
            v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
         end if;
      ELSE
         IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 7) then
			
            UPDATE OpportunityMaster SET tintEDIStatus = 7,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID; --7: 856 Sent
			
            INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
				VALUES(v_numDomainID,v_numOppID,7,TIMEZONE('UTC',now()));
         end if;
      end if;
   ELSEIF v_EDIType = 85011 AND coalesce(v_bitSuccess,false) = true
   then   --- 850 SO Partially Created
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 11) then
         UPDATE OpportunityMaster SET tintEDIStatus = 11,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID;
      end if; --11: 850 SO Partially Created
			
      INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,11,TIMEZONE('UTC',now()));
   ELSEIF v_EDIType = 85012 AND coalesce(v_bitSuccess,false) = true
   then   --- 850 SO Created
	
      IF NOT EXISTS(SELECT ID FROM EDIHistory WHERE numOppID = v_numOppID AND tintEDIStatus = 12) then
         UPDATE OpportunityMaster SET tintEDIStatus = 12,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID;
      end if; --12: 850 SO Created
			
      INSERT INTO EDIHistory(numDomainID,numOppID,tintEDIStatus,dtDate)
			VALUES(v_numDomainID,v_numOppID,12,TIMEZONE('UTC',now()));
   end if;
   RETURN;
END; $$;


