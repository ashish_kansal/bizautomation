-- Stored procedure definition script USP_SaveDomainWiseSort for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveDomainWiseSort(v_vcModuleType VARCHAR(50),
v_vcSortOn VARCHAR(50),
v_numCreatedAndModifiedBY NUMERIC(18,0),
v_bintCreatedAndModifiedDate TIMESTAMP,
v_numDomainId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecExist  NUMERIC(9,0);
BEGIN
   BEGIN
      select   count(*) INTO v_numRecExist from DomainWiseSort where vcModuleType = v_vcModuleType and numDomainId = v_numDomainId;
      if(v_numRecExist <> 0) then
		
         update DomainWiseSort set vcSortOn = v_vcSortOn,numModifiedBy = v_numCreatedAndModifiedBY,bintModifiedDate = v_bintCreatedAndModifiedDate
         where vcModuleType = v_vcModuleType and numDomainId = v_numDomainId;
      else
         insert into DomainWiseSort(vcModuleType , vcSortOn, numCreatedBY, bintCreatedDate, numDomainId)
			values(v_vcModuleType , v_vcSortOn, v_numCreatedAndModifiedBY, v_bintCreatedAndModifiedDate, v_numDomainId);
      end if;
   END;
   RETURN;
END; $$;


