CREATE OR REPLACE FUNCTION USP_GetCartItemFilters
(
	v_numFormID	NUMERIC(18,0),
	v_numSiteID	NUMERIC(18,0),
    v_numDomainId NUMERIC(18,0),
	INOUT SWV_RefCur refcursor, 
	INOUT SWV_RefCur2 refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSQL  TEXT;
	v_strWhere  TEXT;
	v_JOIN  TEXT;
	v_Alias  TEXT;
	v_intRowCnt  BIGINT;
	v_intTableRowCnt  BIGINT;
	v_numFieldID  NUMERIC(18,0);
	v_numListID  NUMERIC(18,0);
	v_vcAssociatedControlType  VARCHAR(100);
	v_vcLookBackTableName  VARCHAR(100);
	v_bitCustom  BOOLEAN;
BEGIN
	v_strSQL := '';
   v_strWhere := ' WHERE 1=1 ';
	
	BEGIN
		CREATE TEMP SEQUENCE tt_tempFieldValues_seq INCREMENT BY 1 START WITH 1;
		EXCEPTION WHEN OTHERS THEN
			NULL;
	END;
	DROP TABLE IF EXISTS tt_TEMPFIELDVALUES CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFIELDVALUES
	(
		RowID BIGINT GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
		numFieldID NUMERIC(18,0),
		numListID NUMERIC(18,0),
		vcText TEXT,
		vcValue NUMERIC(18,0)
	);
	
	DROP TABLE IF EXISTS tt_TEMPFIELDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFIELDS
	(
		RowID BIGINT,
		numFieldID NUMERIC(18,0),
		vcFieldName VARCHAR(100),
		vcAssociatedControlType VARCHAR(20),
		bitCustomField BOOLEAN,
		vcLookBackTableName VARCHAR(50),
		numListID	NUMERIC(18,0),
		vcOrigDbColumnName VARCHAR(30)
	);
							 
   INSERT INTO tt_TEMPFIELDS(RowID,numFieldID,vcFieldName,vcAssociatedControlType,bitCustomField,vcLookBackTableName,numListID,vcOrigDbColumnName)
   SELECT ROW_NUMBER() OVER(ORDER BY DCF.bitCustomField,DCF.numFieldID) AS RowID,
		   DCF.numFieldID,
		   CASE WHEN DCF.bitCustomField = true THEN CFM.FLd_label ELSE DFM.vcFieldName END AS vcFieldName,
		   CASE WHEN DCF.bitCustomField = true THEN CFM.fld_type ELSE DFM.vcAssociatedControlType END AS vcAssociatedControlType,
		   DCF.bitCustomField,
		   CAST(CASE WHEN DCF.bitCustomField = true THEN '' ELSE DFM.vcLookBackTableName END AS VARCHAR(50)) AS vcFieldName,
		   coalesce(DFM.numListID,0) AS numListID,
		   CAST(CASE WHEN DCF.bitCustomField = true THEN CAST(CFM.Fld_id AS VARCHAR(10)) || 'C' ELSE DFM.vcOrigDbCOlumnName END AS VARCHAR(30)) AS vcOrigDbColumnName
   FROM DycCartFilters DCF
   LEFT JOIN DycFieldMaster DFM ON DCF.numFieldID = DFM.numFieldID
   LEFT JOIN CFW_Fld_Master CFM ON DCF.numFieldID = CFM.Fld_id
   WHERE DCF.numFormID = v_numFormID
   AND DCF.numSiteID = v_numSiteID
   AND DCF.numDomainID = v_numDomainId;
	
   v_intRowCnt := 0;
   SELECT COUNT(*) INTO v_intTableRowCnt FROM tt_TEMPFIELDS;
   RAISE NOTICE '%',v_intTableRowCnt; 
	
	--SELECT * FROM #tempFields
   IF v_intTableRowCnt > 0 then
	
      WHILE(v_intTableRowCnt > v_intRowCnt) LOOP
         v_intRowCnt := v_intRowCnt+1;
         select   numFieldID, vcAssociatedControlType, bitCustomField, vcLookBackTableName, numListID INTO v_numFieldID,v_vcAssociatedControlType,v_bitCustom,v_vcLookBackTableName,
         v_numListID FROM tt_TEMPFIELDS WHERE RowID = v_intRowCnt;
         IF v_vcAssociatedControlType = 'SelectBox' then
			
            IF v_bitCustom = true then
				
               v_strSQL := coalesce(v_strSQL,'') || ' INSERT INTO tt_TEMPFIELDVALUES(numFieldID,numListID,vcText,vcValue)
											  SELECT ' || COALESCE(v_numFieldID,0) || ',numListID,vcData,numListItemID FROM ListDetails 
											  WHERE numListID =(SELECT numListID FROM CFW_Fld_Master WHERE Fld_ID = '
               || COALESCE(v_numFieldID,0) || ') AND numDomainID = ' || COALESCE(v_numDomainId,0);
               RAISE NOTICE '%',v_strSQL;
               EXECUTE v_strSQL;
               v_strSQL := '';
            ELSE
               RAISE NOTICE '@numListID%',SUBSTR(CAST(v_numListID AS VARCHAR(10)),1,10);
               IF v_numListID > 0 then
					
                  v_strSQL := coalesce(v_strSQL,'') || ' INSERT INTO tt_TEMPFIELDVALUES(numFieldID,numListID,vcText,vcValue)
												  SELECT ' || COALESCE(v_numFieldID,0) || ',numListID,vcData,numListItemID FROM ListDetails 
												  WHERE numListID =(SELECT numListID FROM DycFieldMaster WHERE numFieldID = '
                  || COALESCE(v_numFieldID,0) || ') AND numDomainID = ' || COALESCE(v_numDomainId,0);
                  RAISE NOTICE '%',v_strSQL;
                  EXECUTE v_strSQL;
                  v_strSQL := '';


               ELSEIF v_vcLookBackTableName = 'category' AND v_numListID = 0
               then
					
                  v_strSQL := coalesce(v_strSQL,'') || ' INSERT INTO tt_TEMPFIELDVALUES(numFieldID,numListID,vcText,vcValue)
										  SELECT ' || COALESCE(v_numFieldID,0) || ',0,vcCategoryName,numCategoryID FROM Category 
										  WHERE numDomainID = ' || COALESCE(v_numDomainId,0);
                  RAISE NOTICE '%',v_strSQL;
                  EXECUTE v_strSQL;
                  v_strSQL := '';
               end if;
            end if;
         end if;
      END LOOP;
   end if;
	
   open SWV_RefCur for
   SELECT * FROM tt_TEMPFIELDS;
   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFIELDVALUES;
	
	
   RETURN;
END; $$;



