-- Stored procedure definition script USP_ForRepByTerritory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ForRepByTerritory(v_numUserCntId NUMERIC(9,0) DEFAULT null,              
v_numDomainID NUMERIC(9,0) DEFAULT null,              
v_tintType SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select D.numDivisionID AS "numDivisionID",C.vcCompanyName as "vcCompanyName",vcData as "vcTerName",L.numListItemID as "numTerID" from ExtarnetAccounts E
   join ExtranetAccountsDtl DTL
   on DTL.numExtranetID = E.numExtranetID
   join DivisionMaster D
   on D.numDivisionID = E.numDivisionID
   join CompanyInfo C
   on C.numCompanyId = D.numCompanyID
   join Listdetails L
   on L.numListItemID = D.numTerID
   where bitPartnerAccess = true and  D.numTerID in(select cast(F.numTerritory as NUMERIC(18,0)) from ForReportsByTerritory F
      where F.numUserCntId = v_numUserCntId and F.numDomainID = v_numDomainID and F.tintType = v_tintType);
END; $$;












