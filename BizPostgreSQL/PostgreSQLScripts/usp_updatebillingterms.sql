-- Stored procedure definition script USP_UpdateBillingTerms for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_UpdateBillingTerms(v_numDomainID  NUMERIC DEFAULT 0,                                          
     v_numDivisionID  NUMERIC DEFAULT 0, 
	 v_numCompanyID NUMERIC DEFAULT 0,
	 v_numCompanyCredit NUMERIC DEFAULT 0,                                   
	 v_vcDivisionName  VARCHAR(100) DEFAULT '',
	 v_numUserCntID  NUMERIC DEFAULT 0,                                                                                                                                     
	 v_tintCRMType  SMALLINT DEFAULT 0,                                          
	 v_tintBillingTerms SMALLINT DEFAULT NULL,                                          
	 v_numBillingDays NUMERIC(9,0) DEFAULT NULL,                                         
	 v_tintInterestType SMALLINT DEFAULT NULL,                                          
	 v_fltInterest DECIMAL DEFAULT NULL,                          
	 v_bitNoTax BOOLEAN DEFAULT NULL,
	 v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
	 v_numDefaultPaymentMethod NUMERIC(9,0) DEFAULT 0,         
	 v_numDefaultCreditCard NUMERIC(9,0) DEFAULT 0,         
	 v_bitOnCreditHold BOOLEAN DEFAULT false,
	 v_vcShipperAccountNo VARCHAR(100) DEFAULT '',
	 v_intShippingCompany INTEGER DEFAULT 0,
	 v_bitEmailToCase BOOLEAN DEFAULT false,
	 v_numDefaultExpenseAccountID NUMERIC(18,0) DEFAULT 0,
	 v_numDefaultShippingServiceID NUMERIC(18,0) DEFAULT NULL,
	 v_numAccountClass NUMERIC(18,0) DEFAULT 0,
	 v_tintPriceLevel INTEGER DEFAULT 0,
	 v_bitShippingLabelRequired BOOLEAN DEFAULT NULL,
	 v_tintInbound850PickItem INTEGER DEFAULT 0,
	 v_bitAllocateInventoryOnPickList BOOLEAN DEFAULT false,
	 v_bitAutoCheckCustomerPart BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitAllocateInventoryOnPickListOld  BOOLEAN;
BEGIN
   select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickListOld FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;


   IF v_bitAllocateInventoryOnPickList <> v_bitAllocateInventoryOnPickListOld AND(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND tintopptype = 1 AND tintoppstatus = 1 AND coalesce(tintshipped,0) = 0) > 0 then
	
      RAISE EXCEPTION 'NOT_ALLOWED_CHANGE_ALLOCATE_INVENTORY_FROM_PACKING_SLIP_OPEN_SALES_ORDER';
      RETURN;
   end if;

   UPDATE DivisionMaster
   SET numCompanyID = v_numCompanyID,vcDivisionName = v_vcDivisionName,numModifiedBy = v_numUserCntID,
   bintModifiedDate = TIMEZONE('UTC',now()),tintCRMType = v_tintCRMType,
   tintBillingTerms = v_tintBillingTerms,numBillingDays = v_numBillingDays,
   tintInterestType = v_tintInterestType,fltInterest = v_fltInterest,
   bitNoTax = v_bitNoTax,numCurrencyID = v_numCurrencyID,
   numDefaultPaymentMethod = v_numDefaultPaymentMethod,numDefaultCreditCard = v_numDefaultCreditCard,
   bitOnCreditHold = v_bitOnCreditHold,vcShippersAccountNo = v_vcShipperAccountNo,
   intShippingCompany = v_intShippingCompany,
   numDefaultExpenseAccountID = v_numDefaultExpenseAccountID,numDefaultShippingServiceID = v_numDefaultShippingServiceID,
   numAccountClassID = coalesce(v_numAccountClass,0),
   tintPriceLevel = coalesce(v_tintPriceLevel,0),
   bitShippingLabelRequired = v_bitShippingLabelRequired,tintInbound850PickItem = coalesce(v_tintInbound850PickItem,0),
   bitAllocateInventoryOnPickList = coalesce(v_bitAllocateInventoryOnPickList,false),bitAutoCheckCustomerPart = coalesce(v_bitAutoCheckCustomerPart,false)
   WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;     

   UPDATE CompanyInfo
   SET numCompanyCredit = v_numCompanyCredit,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE
   numCompanyId = v_numCompanyID and numDomainID = v_numDomainID;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/



