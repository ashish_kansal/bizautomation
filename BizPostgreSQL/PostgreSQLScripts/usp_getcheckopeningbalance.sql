-- Stored procedure definition script USP_GetCheckOpeningBalance for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckOpeningBalance(v_numAccountId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numOpeningBal,0) as DECIMAL(20,5)) from Chart_Of_Accounts Where v_numAccountId = numAccountId And numDomainId = v_numDomainId;
END; $$;












