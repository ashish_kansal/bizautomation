-- Stored procedure definition script USP_HierachyGridForBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_HierachyGridForBudget(v_numDomainId NUMERIC(9,0) DEFAULT 0,                                              
v_strRow TEXT DEFAULT '',        
v_tintType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE            
   v_strSQL  VARCHAR(8000);                 
   v_strMonSQL  VARCHAR(8000);
   v_numMonth  SMALLINT;              
   v_Date  TIMESTAMP;
   v_dtFiscalStDate  TIMESTAMP;                  
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN
	v_strMonSQL := '';                
	v_numMonth := 1;                          
	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	Create TEMPORARY TABLE tt_TEMPTABLE
	(
		numAcntId NUMERIC(9,0),
		numParentAcntId NUMERIC(9,0)
	);                  
                    
	Insert Into tt_TEMPTABLE(numAcntId,numParentAcntId)
	Select 
		numAcntId,numParentAcntId
	from 
	XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_strRow AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numAcntId NUMERIC(18,0) PATH 'numAcntId',
			numParentAcntId NUMERIC(18,0) PATH 'numParentAcntId'
	) AS X;
                
	v_Date := TIMEZONE('UTC',now())+CAST(v_tintType || 'year' as interval);             
          
         
----  Declare @dtFiscalDate as datetime                    
----  Set @dtFiscalDate ='01/01/'+Convert(varchar(4),year(getutcdate()))                    
----  set @dtFiscalDate=dateadd(month,@numMonth-1,@dtFiscalDate)                    
----  Set @strMonSQL=@strMonSQL+' '+  ' '''' as ' +Convert(varchar(3),DateName(month,convert(varchar(100),@dtFiscalDate)))+','                      
----  Set @numMonth=@numMonth+1              
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      v_strMonSQL := coalesce(v_strMonSQL,'') || ' ' ||  '''''' || ' as "' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || '",';
      v_numMonth := v_numMonth+1;
   END LOOP;                     
   v_strMonSQL := coalesce(v_strMonSQL,'') ||   '''''' || ' as Total' || ',''''' || ' as  Comments';                   
   RAISE NOTICE '%',v_strMonSQL;                             
   v_strSQL := '  with recursive RecursionCTE(RecordID,ParentRecordID,vcCatgyName,TOC,T)                
 as                
 (                
 select numAccountId AS RecordID,Chart_Of_Accounts.numparentaccid AS ParentRecordID,'''' AS vcCatgyName,CAST('''' AS VARCHAR(1000)) AS TOC,CAST('''' AS VARCHAR(1000)) AS T                
    from Chart_Of_Accounts                
    where chart_of_accounts.numparentaccid is null and numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
   || ' union all                
   select R1.numAccountId AS RecordID,                
          r1.numparentaccid AS ParentRecordID,                
    '''' AS vcCatgyName,                
          case when OCTET_LENGTH(R2.TOC) > 0                
                    then CAST(case when POSITION(''.'' IN R2.TOC) > 0 then R2.TOC else R2.TOC || ''.'' end                
                                 || cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                 
                    else CAST(cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                 
                    end AS TOC,case when OCTET_LENGTH(R2.TOC) > 0                
                    then  CAST(''&nbsp&nbsp'' || R2.T AS VARCHAR(1000))                
else CAST('''' AS VARCHAR(1000))                
                    end AS T                
      from Chart_Of_Accounts as R1                      
      join RecursionCTE as R2 on r1.numparentaccid = R2.RecordID and R1.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and R1.numAccountId in(Select numAcntId From  tt_TEMPTABLE)                
  )                 
select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T || vcCatgyName as vcCategoryName,TOC,' || SUBSTR(CAST(v_strMonSQL AS VARCHAR(8000)),1,8000) || ' from RecursionCTE Where ParentRecordID is not null order by TOC asc';                
   RAISE NOTICE '%',v_strSQL;                             
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


