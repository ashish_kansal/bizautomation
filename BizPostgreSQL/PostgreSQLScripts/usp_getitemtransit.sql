DROP FUNCTION IF EXISTS USP_GetItemTransit;

CREATE OR REPLACE FUNCTION USP_GetItemTransit(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemcode NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT NULL,
v_numVendorID NUMERIC(9,0) DEFAULT 0,
v_numAddressID NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, 
v_numWarehouseItemID NUMERIC(18,0) DEFAULT 0,
INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF v_tintMode = 1 then
		OPEN SWV_RefCur FOR SELECT fn_GetItemTransitCount(v_numItemcode,v_numDomainID) AS Total;
	ELSEIF v_tintMode = 2 THEN
		OPEN SWV_RefCur FOR
		SELECT 
			opp.numOppId
			,opp.vcpOppName
			,CASE 
				WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
				THEN '<b><font color=red>Today</font></b>' 
				WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
				THEN '<b><font color=purple>YesterDay</font></b>' 
				WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
				THEN '<b><font color=orange>Tommorow</font></b>' else FormatedDateFromDate(opp.bintCreatedDate,v_numDomainID) 
			END  AS bintCreatedDate
			,fn_GetContactName(opp.numContactId) AS vcOrderedBy
			,OppI.vcItemName
			,CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OppI.numItemCode,opp.numDomainId,coalesce(OppI.numUOMId,0))*(coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0))) as NUMERIC(18,0)) as numUnitHour
			,coalesce(u.vcUnitName,'') AS vcUOMName
			,OppI.vcManufacturer
			,coalesce(OppI.vcItemDesc,'') as vcItemDesc
			,OppI.vcModelID
			,fn_GetListItemName(opp.intUsedShippingCompany::NUMERIC) AS vcShipmentMethod
			,COALESCE(ShippingService.vcShipmentService,'') vcShipmentService
			,CONCAT(ADShipFrom.vcAddressName,' (',coalesce(fn_GetState(ADShipFrom.numState),'-'),', ',coalesce(fn_GetListItemName(ADShipFrom.numCountry),'-'),')') AS vcShipFrom
			,(CASE 
				WHEN COALESCE(OppI.bitDropship,false) = true THEN CONCAT(ADDropship.vcAddressName,' (',coalesce(fn_GetState(ADDropship.numState),'-'),', ',coalesce(fn_GetListItemName(ADDropship.numCountry),'-'),')')
				WHEN EXISTS (select coalesce(numParentOppID,0) FROM OpportunityLinking WHERE numChildOppID = opp.numOppId) THEN CONCAT(TEMPShipTo.vcAddressName,' (',coalesce(TEMPShipTo.vcState,'-'),', ',coalesce(TEMPShipTo.vcCountry,'-'),')') 
				ELSE CONCAT(ADWarehouse.vcAddressName,' (',coalesce(fn_GetState(ADWarehouse.numState),'-'),', ',coalesce(fn_GetListItemName(ADWarehouse.numCountry),'-'),')')
			END) AS vcShipTo
			,FormatedDateFromDate(opp.dtExpectedDate,v_numDomainID) AS dtExpectedDate
			,CASE 
				WHEN TEMPVendorLeadTimeDays.vcDaysToArrive IS NOT NULL
				THEN
					CONCAT(CASE 
						WHEN CAST(opp.bintCreatedDate + make_interval(days => (CASE WHEN opp.bintCreatedDate::DATE > timezone('utc', now())::DATE THEN DATEDIFF('day',timezone('utc', now())::TIMESTAMP,opp.bintCreatedDate) ELSE 0 END)) +CAST(-v_ClientTimeZoneOffset || 'minute' as interval) + make_interval(days => TEMPVendorLeadTimeDays.vcDaysToArrive) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
						THEN '<b><font color=red>Today</font></b>'
						WHEN CAST(opp.bintCreatedDate + make_interval(days => (CASE WHEN opp.bintCreatedDate::DATE > timezone('utc', now())::DATE THEN DATEDIFF('day',timezone('utc', now())::TIMESTAMP,opp.bintCreatedDate) ELSE 0 END)) +CAST(-v_ClientTimeZoneOffset || 'minute' as interval) + make_interval(days => TEMPVendorLeadTimeDays.vcDaysToArrive) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
						THEN '<b><font color=purple>YesterDay</font></b>'
						WHEN CAST(opp.bintCreatedDate + make_interval(days => (CASE WHEN opp.bintCreatedDate::DATE > timezone('utc', now())::DATE THEN DATEDIFF('day',timezone('utc', now())::TIMESTAMP,opp.bintCreatedDate) ELSE 0 END)) +CAST(-v_ClientTimeZoneOffset || 'minute' as interval) + make_interval(days => TEMPVendorLeadTimeDays.vcDaysToArrive) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
						THEN '<b><font color=orange>Tommorow</font></b>'
						ELSE FormatedDateFromDate(opp.bintCreatedDate + make_interval(days => (CASE WHEN opp.bintCreatedDate::DATE > timezone('utc', now())::DATE THEN DATEDIFF('day',timezone('utc', now())::TIMESTAMP,opp.bintCreatedDate) ELSE 0 END)) +CAST(-v_ClientTimeZoneOffset || 'minute' as interval) + make_interval(days => TEMPVendorLeadTimeDays.vcDaysToArrive),v_numDomainID) 
					END,' (',
					CASE 
						WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(TEMPVendorLeadTimeDays.vcDaysToArrive || 'day' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
						THEN '<b><font color=red>Today</font></b>'
						WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(TEMPVendorLeadTimeDays.vcDaysToArrive || 'day' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
						THEN '<b><font color=purple>YesterDay</font></b>'
						WHEN CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(TEMPVendorLeadTimeDays.vcDaysToArrive || 'day' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
						THEN '<b><font color=orange>Tommorow</font></b>'
						ELSE FormatedDateFromDate(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(TEMPVendorLeadTimeDays.vcDaysToArrive || 'day' as interval),v_numDomainID) 
					END,')')
				ELSE '' 
			END AS bintExpectedDelivery
		FROM 
			OpportunityMaster opp 
		INNER JOIN OpportunityItems OppI ON opp.numOppId = OppI.numOppId  
		LEFT JOIN AddressDetails ADShipFrom ON opp.numVendorAddressID = ADShipFrom.numAddressID
		LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID = WI.numWareHouseItemID
		LEFT JOIN Warehouses W ON WI.numWarehouseID=W.numWarehouseID
		LEFT JOIN AddressDetails ADWarehouse ON W.numAddressID = ADWarehouse.numAddressID
		LEFT JOIN AddressDetails ADDropship ON ADDropship.numDomainID = v_numDomainID AND ADDropship.numRecordID = opp.numDivisionID AND ADDropship.tintAddressOf = 2 AND ADDropship.tintAddressType = 2 AND ADDropship.bitIsPrimary = true
		INNER JOIN Item I on OppI.numItemCode = I.numItemCode
		LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId
		LEFT JOIN ShippingService ON opp.numShippingService = ShippingService.numShippingServiceID
		LEFT JOIN LATERAL (SELECT * FROM fn_getOPPAddressDetails(opp.numOppId,v_numDomainID,2::SMALLINT)) TEMPShipTo ON TRUE
		LEFT JOIN LATERAL (SELECT 
								fn_GetPOItemVendorLeadTimesDays AS vcDaysToArrive 
							FROM 
								fn_GetPOItemVendorLeadTimesDays(v_numDomainID
																,opp.numDivisionID
																,OppI.numUnitHour::NUMERIC
																,COALESCE(I.numItemClassification,0)
																,COALESCE(opp.intUsedShippingCompany,0)
																,COALESCE(opp.numShippingService,0)
																,COALESCE(ADShipFrom.numAddressID,0)
																,(CASE 
																	WHEN COALESCE(OppI.bitDropship,false) = true 
																	THEN COALESCE(ADDropship.numCountry,0) 
																	WHEN EXISTS (select coalesce(numParentOppID,0) FROM OpportunityLinking WHERE numChildOppID = opp.numOppId) 
																	THEN COALESCE(TEMPShipTo.numCountry,0)
																	ELSE COALESCE(ADWarehouse.numCountry,0) 
																END)
																,(CASE 
																	WHEN COALESCE(OppI.bitDropship,false) = true 
																	THEN COALESCE(ADDropship.numState,0)
																	WHEN EXISTS (select coalesce(numParentOppID,0) FROM OpportunityLinking WHERE numChildOppID = opp.numOppId) 
																	THEN COALESCE(TEMPShipTo.numState,0)
																	ELSE COALESCE(ADWarehouse.numState,0) 
																END))) TEMPVendorLeadTimeDays ON TRUE
		WHERE 
			opp.numDomainId = v_numDomainID 
			AND opp.tintopptype = 2 
			and opp.tintoppstatus = 1 
			and opp.tintshipped = 0
			AND OppI.numItemCode = v_numItemcode
			AND (coalesce(v_numWarehouseItemID,0) = 0 OR OppI.numWarehouseItmsID=v_numWarehouseItemID)
			and coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0) > 0;
   ELSEIF v_tintMode = 3
   then

      open SWV_RefCur for
      select opp.numOppId,opp.vcpOppName,case when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>' when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>' when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>' else FormatedDateFromDate(opp.bintCreatedDate,1::NUMERIC) end  AS bintCreatedDate,
fn_GetContactName(opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((fn_UOMConversion(coalesce(I.numBaseUnit,0),OppI.numItemCode,opp.numDomainId,coalesce(OppI.numUOMId,0))*(coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0))) as NUMERIC(18,0)) as numUnitHour
,coalesce(u.vcUnitName,'') AS vcUOMName,OppI.vcManufacturer,coalesce(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
fn_GetListItemName(OppI.numShipmentMethod) AS vcShipmentMethod,
CASE WHEN coalesce(VSM.numListValue,0) > 0 THEN
         case when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) then '<b><font color=red>Today</font></b>'
         when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11)) then '<b><font color=purple>YesterDay</font></b>'
         when CAST(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11)) then '<b><font color=orange>Tommorow</font></b>'
         else FormatedDateFromDate(opp.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+CAST(VSM.numListValue || 'day' as interval),v_numDomainID) end
      ELSE '' end AS bintExpectedDelivery
      FROM OpportunityMaster opp INNER JOIN OpportunityItems OppI ON opp.numOppId = OppI.numOppId LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID = WI.numWareHouseItemID
      INNER JOIN Item I on OppI.numItemCode = I.numItemCode
      LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId
      LEFT JOIN LATERAL(SELECT * FROM VendorShipmentMethod VSM WHERE VSM.numAddressID = opp.numVendorAddressID AND VSM.numVendorID = opp.numDivisionId ORDER BY(CASE WHEN VSM.numWarehouseID = WI.numWareHouseID THEN 1 ELSE 0 END) DESC,coalesce(bitPrimary,false) DESC,coalesce(bitPreferredMethod,false) DESC LIMIT 1) VSM on TRUE
      WHERE opp.tintopptype = 2 and opp.tintoppstatus = 1 and opp.tintshipped = 0
      and opp.numDomainId = v_numDomainID AND opp.numDivisionId = v_numVendorID AND OppI.numVendorWareHouse = v_numAddressID
      and coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0) > 0;
   end if;
   RETURN;
END; $$;


