-- Stored procedure definition script USP_GetShippingAbbreviation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetShippingAbbreviation(v_numShipFromState NUMERIC(9,0),
v_numShipFromCountry  NUMERIC(9,0),
v_numShipToState  NUMERIC(9,0),
v_numShipToCountry  NUMERIC(9,0),
INOUT v_vcFromState  VARCHAR(5) DEFAULT '' ,
INOUT v_vcFromCountry  VARCHAR(5) DEFAULT '' ,
INOUT v_vcToState  VARCHAR(5) DEFAULT '' ,
INOUT v_vcToCountry  VARCHAR(5) DEFAULT '' ,
v_numShipCompany  NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   vcStateCode INTO v_vcFromState from ShippingStateMaster where vcStateName =(select  vcState from State where numStateID = v_numShipFromState LIMIT 1) and  numShipCompany = v_numShipCompany;

   select   vcCountryCode INTO v_vcFromCountry from ShippingCountryMaster where vcCountryName =(select  vcData from Listdetails where numListItemID = v_numShipFromCountry LIMIT 1)  and  numShipCompany = v_numShipCompany;

   select   vcStateCode INTO v_vcToState from ShippingStateMaster where vcStateName =(select  vcState from State where numStateID = v_numShipToState LIMIT 1)  and  numShipCompany = v_numShipCompany;

   select   vcCountryCode INTO v_vcToCountry from ShippingCountryMaster where vcCountryName =(select  vcData from Listdetails where numListItemID = v_numShipToCountry LIMIT 1)  and  numShipCompany = v_numShipCompany;
   RETURN;
END; $$;




