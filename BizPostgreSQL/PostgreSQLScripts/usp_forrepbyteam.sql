-- Stored procedure definition script USP_ForRepByTeam for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ForRepByTeam(v_numUserCntId NUMERIC(9,0) DEFAULT null,          
v_numDomainID NUMERIC(9,0) DEFAULT null,          
v_tintType SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select A.numContactId AS "numContactId",COALESCE(A.vcFirstName,'') || ' ' || COALESCE(A.vcLastname,'') as "vcGivenName",A.numTeam AS "numTeam",lst.vcData as "vcData" from UserMaster U
   join AdditionalContactsInformation A
   on U.numUserDetailId = A.numContactId
   left Join Listdetails lst
   on lst.numListItemID = A.numTeam
   where A.numTeam is not null
   and A.numTeam in(select cast(F.numTeam as NUMERIC(18,0)) from ForReportsByTeam F
      where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
   order by A.numTeam;
END; $$;












