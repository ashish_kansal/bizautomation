-- Stored procedure definition script USP_OrgOppAddressModificationHistory_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OrgOppAddressModificationHistory_Get(v_numDomainID NUMERIC(18,0)
	,v_numRecordID NUMERIC(18,0)
	,v_numAddressID NUMERIC(18,0)
	,v_tintAddressOf SMALLINT
	,v_tintAddressType SMALLINT
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   fn_GetContactName(OrgOppAddressModificationHistory.numModifiedBy) AS vcModifiedBy
		,FormatedDateTimeFromDate(OrgOppAddressModificationHistory.dtModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS dtModifiedDate
   FROM
   OrgOppAddressModificationHistory
   INNER JOIN
		AdditionalContactsInformation
	ON
		OrgOppAddressModificationHistory.numModifiedBy = AdditionalContactsInformation.numContactId
   WHERE
   numDomainID = v_numDomainID
	AND numRecordID = v_numRecordID
   AND tintAddressOf = v_tintAddressOf
   AND tintAddressType = v_tintAddressType
   AND coalesce(v_numAddressID,0) = 0 LIMIT 1;
END; $$;













