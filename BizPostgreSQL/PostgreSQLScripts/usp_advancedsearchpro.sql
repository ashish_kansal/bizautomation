-- Stored procedure definition script USP_AdvancedSearchPro for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AdvancedSearchPro(v_WhereCondition VARCHAR(1000) DEFAULT '',                                                        
v_numDomainID NUMERIC(9,0) DEFAULT 0,                                      
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                     
v_CurrentPage INTEGER DEFAULT NULL,                                                                            
v_PageSize INTEGER DEFAULT NULL,                                                                            
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                                                     
v_columnSortOrder VARCHAR(10) DEFAULT NULL,        
v_ColumnName VARCHAR(20) DEFAULT '',        
v_SortCharacter CHAR(1) DEFAULT NULL,                                
v_SortColumnName VARCHAR(20) DEFAULT '',        
v_LookTable VARCHAR(10) DEFAULT '' ,
v_vcDisplayColumns TEXT DEFAULT NULL,
v_GetAll BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  SMALLINT;                                      
   v_vcFormFieldName  VARCHAR(50);                                      
   v_vcListItemType  VARCHAR(3);                                 
   v_vcListItemType1  VARCHAR(3);                                     
   v_vcAssociatedControlType  VARCHAR(20);                                      
   v_numListID  NUMERIC(9,0);                                      
   v_vcDbColumnName  VARCHAR(30);                                       
   v_ColumnSearch  VARCHAR(10);                              
   v_strSql  VARCHAR(8000);                                                                      
   v_vcLocationID  VARCHAR(100) DEFAULT '0';
   v_Table  VARCHAR(200);
   v_bitCustom  BOOLEAN;
   v_numFieldGroupID  INTEGER;
   v_vcColumnName  VARCHAR(200);
   v_numFieldID  NUMERIC(18,0);
   v_Prefix  VARCHAR(10);

   v_firstRec  INTEGER;                                                                            
   v_lastRec  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   if (v_SortCharacter <> '0' and v_SortCharacter <> '') then 
      v_ColumnSearch := v_SortCharacter;
   end if;       
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTableAdvancedSearchPro_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLEAdvancedSearchPro CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLEAdvancedSearchPro 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numProID NUMERIC(18,0)
   );      
      
   v_strSql := 'select numProID from       
ProjectsMaster ProMas
join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr
';      
      
                                 
   if (v_SortColumnName <> '') then
  
      select   vcListItemType INTO v_vcListItemType1 from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 1  and numDomainID = v_numDomainID;
      if v_vcListItemType1 = 'L' then
  
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      end if;
   end if;                                  
   v_strSql := coalesce(v_strSql,'') || ' where ProMas.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||   coalesce(v_WhereCondition,'');                                          
         
      
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then

      if v_vcListItemType = 'L' then
    
         v_strSql := coalesce(v_strSql,'') || ' and L1.vcData like ''' || coalesce(v_ColumnSearch,'') || '%''';
      else 
         v_strSql := coalesce(v_strSql,'') || ' and ' ||  coalesce(v_ColumnName,'') || ' like ''' || coalesce(v_ColumnSearch,'')  || '%''';
      end if;
   end if;                                  
   if (v_SortColumnName <> '') then
  
      if v_vcListItemType1 = 'L' then
    
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      else  
         v_strSql := coalesce(v_strSql,'') || ' order by ' || case  when v_SortColumnName = 'txtComments' then 'convert(varchar(500),txtComments)'
         else v_SortColumnName end  || ' ' || coalesce(v_columnSortOrder,'');
      end if;
   end if;       
      
      
   EXECUTE 'insert into tt_TEMPTABLEAdvancedSearchPro(numProID)                       
      
' || v_strSql;        
   RAISE NOTICE '%',v_strSql;     
   RAISE NOTICE '====================================================';      
                            
   v_strSql := '';                                      
                                      
                                     
   v_tintOrder := 0;                                      
   v_WhereCondition := '';                                   
   v_strSql := 'select ProMas.numProID ';

   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN,
      tintOrder INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 18;  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempIDs_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPIDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIDS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         vcFieldID VARCHAR(300)
      );
      INSERT INTO tt_TEMPIDS(vcFieldID)
      SELECT
      OutParam
      FROM
      SplitString(v_vcDisplayColumns,',');
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,(SELECT(ID::bigint -1) FROM tt_TEMPIDS T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
      FROM(SELECT
         numFieldID as numFormFieldID,
				false AS bitCustomField,
				CONCAT(numFieldID,'~0') AS vcFieldID
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 18
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,true AS bitCustomField
				,CONCAT(Fld_id,'~1') AS vcFieldID
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT vcFieldID FROM tt_TEMPIDS);
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
      FROM(SELECT
         A.numFormFieldID,
				false AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = 18
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = 18
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT
         A.numFormFieldID,
				true AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = 18
         AND C.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFieldID,
			CAST(0 AS BOOLEAN),
			1,
			0
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 18
      AND numDomainID = v_numDomainID
      AND numFieldID = 148;
   end if;                                      


   select   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFieldGroupID FROM(SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 18
      AND coalesce(T1.bitCustomField,false) = false
      UNION
      SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			C.fld_type,
			'',
			C.numlistid,
			'',
			true AS bitCustom,
			Grp_id
      FROM
      CFW_Fld_Master C
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      C.Fld_id = T1.numFieldID
      WHERE
      C.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1    ORDER BY
   tintOrder asc LIMIT 1;    
                                    
   while v_tintOrder > 0 LOOP
      IF v_Table = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      IF v_Table = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      IF v_Table = 'CompanyInfo' then
         v_Prefix := 'C.';
      end if;
      IF v_Table = 'ProjectsMaster' then
         v_Prefix := 'ProMas.';
      end if;
      IF v_bitCustom = false then
   
         if v_vcAssociatedControlType = 'SelectBox' then
        
            if v_vcListItemType = 'LI' then
			
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' ||(coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,''));
            end if;
            if v_vcListItemType = 'U' then
			
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            end if;
         else 
            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
            when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
            when v_vcDbColumnName = 'vcProgress' then  ' fn_ProTotalProgress(ProMas.numProId)'
            when v_vcDbColumnName = 'intTargetResolveDate' or v_vcDbColumnName = 'dtDateEntered' or v_vcDbColumnName = 'bintShippedDate'  then
               'FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')'
            else coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') end || ' AS "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then
	
         v_vcColumnName := CONCAT(v_vcFormFieldName,'~','Cust',v_numFieldID);
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
		
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=ProMas.numProID ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ProMas.numProID ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ProMas.numProID ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
		
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ProMas.numProID ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'','')), ''''))  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=ProMas.numProID ';
         end if;
      end if;
      select   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFieldGroupID FROM(SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
         FROM
         View_DynamicDefaultColumns D
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         D.numFieldID = T1.numFieldID
         WHERE
         D.numDomainID = v_numDomainID
         AND D.numFormId = 18
         AND coalesce(T1.bitCustomField,false) = false
         AND T1.tintOrder > v_tintOrder -1
         UNION
         SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			C.fld_type,
			'',
			C.numlistid,
			'',
			true AS bitCustom,
			Grp_id
         FROM
         CFW_Fld_Master C
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         C.Fld_id = T1.numFieldID
         WHERE
         C.numDomainID = v_numDomainID
         AND coalesce(T1.bitCustomField,false) = true
         AND T1.tintOrder > v_tintOrder -1) T1    ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;       
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                             
   select count(*) INTO v_TotRecs from tt_TEMPTABLEAdvancedSearchPro;       
   v_strSql := coalesce(v_strSql,'') || 'from ProjectsMaster ProMas join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr     
 ' || coalesce(v_WhereCondition,'') || '      
      
 join tt_TEMPTABLEAdvancedSearchPro T on T.numProID=ProMas.numProID';    

   IF coalesce(v_GetAll,false) = false then
	
      v_strSql := coalesce(v_strSql,'') || ' WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
   end if;

                        
   RAISE NOTICE '%',v_strSql;                                                              
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                                                                         
  
   RETURN;
END; $$;


