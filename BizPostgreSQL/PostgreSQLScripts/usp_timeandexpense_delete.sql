-- Stored procedure definition script USP_TimeAndExpense_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TimeAndExpense_Delete(v_numDomainID NUMERIC(18,0)
    ,v_numCategoryHDRID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT
   OpportunityItems.numoppitemtCode
   FROM
   timeandexpense
   INNER JOIN
   OpportunityItems
   ON
   timeandexpense.numOppId = OpportunityItems.numOppId
   AND timeandexpense.numOppItemID = OpportunityItems.numoppitemtCode
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND timeandexpense.numCategoryHDRID = v_numCategoryHDRID) then
	
      RAISE EXCEPTION 'ORDER_EXISTS';
      RETURN;
   end if;

   IF EXISTS(SELECT
   OpportunityBizDocItems.numOppBizDocItemID
   FROM
   timeandexpense
   INNER JOIN
   OpportunityBizDocItems
   ON
   timeandexpense.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
   AND timeandexpense.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND timeandexpense.numCategoryHDRID = v_numCategoryHDRID) then
	
      RAISE EXCEPTION 'BIZDOC_EXISTS';
      RETURN;
   end if;
	
   IF EXISTS(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND numCategoryHDRID = v_numCategoryHDRID) then
	
      RAISE EXCEPTION 'JOURNAL_ENTRIES_EXISTS';
      RETURN;
   end if;

   DELETE FROM timeandexpense WHERE numDomainID = v_numDomainID AND numCategoryHDRID = v_numCategoryHDRID;
END; $$;


