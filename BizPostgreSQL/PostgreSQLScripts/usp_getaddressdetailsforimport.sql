CREATE OR REPLACE FUNCTION Usp_GetAddressDetailsForImport(
    v_numDomainID NUMERIC(9,0),
    v_tintUpdateMode SMALLINT, 
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintUpdateMode = 2 then
            
      open SWV_RefCur for
      SELECT DISTINCT
      CI.vcCompanyName ,
                        'Organization' AS AddressOf ,
                        CASE AD.tintAddressType
      WHEN 1 THEN 'BILL TO'
      ELSE 'SHIP TO'
      END AS AddressType ,
                        CASE AD.bitIsPrimary
      WHEN true THEN 'YES'
      ELSE 'NO'
      END AS "Is Primary" ,
                        AD.vcAddressName AS "Address Name" ,
                        AD.vcStreet AS Street ,
                        AD.vcCity AS City ,
                        (SELECT    S.vcState
         FROM      State AS S
         WHERE     S.numStateID = AD.numState) AS State ,
                        (SELECT    LD.vcData
         FROM      Listdetails AS LD
         WHERE     LD.numListID = 40
         AND LD.numListItemID = AD.numCountry) AS Country ,
                        AD.vcPostalCode AS "Postal Code" ,
                        AD.numAddressID AS AddressID ,
                        DM.numDivisionID AS RecordID
      FROM    AddressDetails AS AD
      LEFT JOIN DivisionMaster AS DM ON AD.numDomainID = DM.numDomainID
      AND AD.numRecordID = DM.numDivisionID
      AND AD.tintAddressOf = 2
      JOIN CompanyInfo AS CI ON CI.numCompanyId = DM.numCompanyID
      WHERE   AD.numDomainID = v_numDomainID
      ORDER BY CI.vcCompanyName,AD.numAddressID;
   ELSEIF v_tintUpdateMode = 1
   then
                
      open SWV_RefCur for
      SELECT  ACI.vcFirstName AS "First Name",
                            ACI.vcLastname AS "Last Name",
                            'Contact' AS AddressOf ,
                            CASE AD.tintAddressType
      WHEN 1 THEN 'BILL TO'
      ELSE 'SHIP TO'
      END AS AddressType ,
                            CASE AD.bitIsPrimary
      WHEN true THEN 'YES'
      ELSE 'NO'
      END AS IsPrimary ,
                            AD.vcAddressName AS "Address Name" ,
                            AD.vcStreet AS Street ,
                            AD.vcCity AS City ,
                            (SELECT    S.vcState
         FROM      State AS S
         WHERE     S.numStateID = AD.numState) AS State ,
                            (SELECT    LD.vcData
         FROM      Listdetails AS LD
         WHERE     LD.numListID = 40
         AND LD.numListItemID = AD.numCountry) AS Country ,
                            AD.vcPostalCode AS "Postal Code" ,
                            AD.numAddressID AS AddressID ,
                            ACI.numContactId AS RecordID
      FROM    AddressDetails AS AD
      JOIN AdditionalContactsInformation AS ACI ON AD.numRecordID = ACI.numContactId
      AND ACI.numDomainID = AD.numDomainID
      AND AD.tintAddressOf = 1
      WHERE   AD.numDomainID = v_numDomainID
      AND (coalesce(ACI.vcFirstName,'') <> ''
      OR coalesce(ACI.vcLastname,'') <> '')
      ORDER BY ACI.vcFirstName,ACI.vcLastname,AD.numAddressID;
   end if;
   RETURN;
END; $$;

