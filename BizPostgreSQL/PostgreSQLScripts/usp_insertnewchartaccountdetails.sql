-- Stored procedure definition script usp_InsertNewChartAccountDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertNewChartAccountDetails(v_numAcntTypeId NUMERIC(9,0) DEFAULT 0,
    v_numParntAcntTypeID NUMERIC(9,0) DEFAULT 0,
    v_vcAccountName VARCHAR(100) DEFAULT NULL,
    v_vcAccountDescription VARCHAR(100) DEFAULT NULL,
    v_monOriginalOpeningBal DECIMAL(20,5) DEFAULT NULL,
    v_monOpeningBal DECIMAL(20,5) DEFAULT NULL,
    v_dtOpeningDate TIMESTAMP DEFAULT NULL,
    v_bitActive BOOLEAN DEFAULT NULL,
    INOUT v_numAccountId NUMERIC(9,0) DEFAULT 0 ,
    v_bitFixed BOOLEAN DEFAULT NULL,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numListItemID NUMERIC(9,0) DEFAULT 0,
    v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_bitProfitLoss BOOLEAN DEFAULT NULL,
    v_bitDepreciation BOOLEAN DEFAULT NULL,
    v_dtDepreciationCostDate TIMESTAMP DEFAULT NULL,
    v_monDepreciationCost DECIMAL(20,5) DEFAULT NULL,
    v_vcNumber VARCHAR(50) DEFAULT NULL,
    v_bitIsSubAccount BOOLEAN DEFAULT NULL,
    v_numParentAccId NUMERIC(18,0) DEFAULT NULL,
    v_IsBankAccount			BOOLEAN	DEFAULT false,
	v_vcStartingCheckNumber	VARCHAR(50) DEFAULT NULL ,
	v_vcBankRountingNumber	VARCHAR(50) DEFAULT NULL ,
	v_bitIsConnected				BOOLEAN DEFAULT false,
	v_numBankDetailID			NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountId1  NUMERIC(9,0);                              
   v_strSQl  VARCHAR(1000);                             
   v_strSQLBalance  VARCHAR(1000);                           
   v_numJournalId  NUMERIC(9,0); 
   v_numDepositId  NUMERIC(9,0);     
   v_numDepositJournalId  NUMERIC(9,0);  
   v_numCashCreditJournalId  NUMERIC(9,0);  
   v_numCashCreditId  NUMERIC(9,0);   
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
   v_vcAccountCode  VARCHAR(50);
   v_intLevel  INTEGER;
   v_intFlag  INTEGER;
   v_vcAccountNameWithNumber  VARCHAR(100);
        
    
  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_AccountTypeCode  VARCHAR(4);
	                             
		--Check for Current Fiancial Year
   v_OldOriginalOpeningBalance  DECIMAL(20,5); 
   v_OldOpeningBalance  DECIMAL(20,5); 
   v_NewOpeningBalance  DECIMAL(20,5);
   v_OldParentAcntTypeID  NUMERIC;
   v_OldIsSubAccount  NUMERIC;
   v_numTempAccountID  NUMERIC;
   v_bitTempIsSubAccount  BOOLEAN;
   v_numTempParentAccId  NUMERIC;
   v_vcTempAccountCode  VARCHAR(100);


   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
BEGIN
   -- BEGIN TRANSACTION
v_strSQl := '';
   v_strSQLBalance := '';
   v_intFlag := 1;
   IF coalesce(v_vcNumber,'') = '-1' then --@vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		
      v_intFlag := 0;
      v_vcNumber := '';
      RAISE NOTICE '%',v_intFlag;
   end if;
   IF coalesce(v_vcNumber,'') = '' then
      v_vcAccountNameWithNumber := '';
   ELSE
      v_vcAccountNameWithNumber := coalesce(v_vcNumber,'') || ' ';
   end if;
   IF v_numAcntTypeId = 0 then
      select   SUBSTR(vcAccountCode,1,4) INTO v_AccountTypeCode FROM AccountTypeDetail WHERE numAccountTypeID = v_numParntAcntTypeID;
      select   numAccountTypeID INTO v_numAcntTypeId FROM AccountTypeDetail WHERE numDomainID = v_numDomainId AND vcAccountCode =  v_AccountTypeCode;
   end if;
	                             
		--Check for Current Fiancial Year
   IF(SELECT COUNT(numFinYearId) FROM FinancialYear WHERE numDomainId = v_numDomainId AND bitCurrentYear = true) = 0 then
        
      RAISE EXCEPTION 'NoCurrentFinancialYearSet';
      RETURN;
   end if;
   IF v_numAccountId = 0 then
        
      IF coalesce(v_vcNumber,'') <> '' then
			
         IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = v_vcNumber AND numDomainId =  v_numDomainId) then
				
            RAISE EXCEPTION 'DuplicateNumber';
            RETURN;
         end if;
      end if;
      IF v_numParntAcntTypeID = 0 then
			
         SELECT  numAccountId INTO v_numParntAcntTypeID FROM Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = v_numDomainId     LIMIT 1;
      end if;
      SELECT  numAccountId INTO v_numAccountIdOpeningEquity FROM Chart_Of_Accounts WHERE bitProfitLoss = true AND Chart_Of_Accounts.numDomainId = v_numDomainId     LIMIT 1;
      IF (coalesce(v_numAccountIdOpeningEquity,0) = 0 AND v_bitProfitLoss = false) then
            
         RAISE EXCEPTION 'NoProfitLossACC';
         RETURN;
      end if;
      v_vcAccountCode := GetAccountTypeCode(v_numDomainId,v_numParntAcntTypeID,1::SMALLINT);
      IF (v_bitIsSubAccount = true) then
			
         v_vcAccountCode := GetAccountTypeCode(v_numDomainId,v_numParentAccId,3::SMALLINT);
         select   numParntAcntTypeID INTO v_numParntAcntTypeID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND numAccountId = v_numParentAccId;
      end if;
      select   CASE WHEN v_numParentAccId = 0 AND v_bitIsSubAccount = false THEN 0 ELSE coalesce(MAX(intLevel),0)+1 END INTO v_intLevel FROM Chart_Of_Accounts WHERE numAccountId = v_numParentAccId;
      RAISE NOTICE '%',v_intLevel;
									   
--			PRINT @vcAccountCode
      INSERT  INTO Chart_Of_Accounts(numAcntTypeId,
                numParntAcntTypeID,
                vcAccountCode,
                vcAccountName,
                vcAccountDescription,
                numOriginalOpeningBal,
                numOpeningBal,
                dtOpeningDate,
                bitActive,
                bitFixed,
                numDomainId,
                numListItemID,
                bitProfitLoss,
                bitDepreciation,
                dtDepreciationCostDate,
                monDepreciationCost,
                vcNumber,
                bitIsSubAccount,
                numParentAccId,
                intLevel,
                IsBankAccount,
                vcStartingCheckNumber,
                vcBankRountingNumber,
                IsConnected,
                numBankDetailID)
            VALUES(v_numAcntTypeId,
                v_numParntAcntTypeID,
                v_vcAccountCode,
                coalesce(v_vcAccountNameWithNumber, '') || coalesce(v_vcAccountName, ''),
                v_vcAccountDescription,
                v_monOriginalOpeningBal,
                v_monOpeningBal,
                v_dtOpeningDate,
                v_bitActive,
                v_bitFixed,
                v_numDomainId,
                v_numListItemID,
                v_bitProfitLoss,
                v_bitDepreciation,
                v_dtDepreciationCostDate,
                v_monDepreciationCost,
                v_vcNumber,
                v_bitIsSubAccount,
                v_numParentAccId,
                v_intLevel,
                v_IsBankAccount,
                v_vcStartingCheckNumber,
                v_vcBankRountingNumber,
                v_bitIsConnected,
                v_numBankDetailID) RETURNING numAccountId INTO v_numAccountId1;
			
      v_numAccountId := v_numAccountId1; --used at bottom
      open SWV_RefCur for
      SELECT v_numAccountId1;                            
				
			
			
			--Added By Chintan
      PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAccountId1,v_monOriginalOpeningBal,v_dtOpeningDate);
   ELSEIF v_numAccountId <> 0
   then
      IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numAccountId = v_numAccountId AND numDomainId = v_numDomainId AND (numAcntTypeId != v_numAcntTypeId OR numParntAcntTypeID != v_numParntAcntTypeID)) then
			
         IF NOT ((SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID = v_numParntAcntTypeID AND vcAccountCode ilike '0106%' AND numDomainID = v_numDomainId) = 1
         AND(SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID =(SELECT numParntAcntTypeID FROM Chart_Of_Accounts WHERE numAccountId = v_numAccountId AND numDomainId = v_numDomainId) AND vcAccountCode ilike '0104%' AND numDomainID = v_numDomainId) = 1) then
				
            IF EXISTS(SELECT GJD.numJournalId FROM General_Journal_Details GJD WHERE numDomainId = v_numDomainId AND numChartAcntId = v_numAccountId) then
					
               RAISE EXCEPTION 'JournalEntryExists';
               RETURN;
            end if;
         end if;
      end if;
      IF coalesce(v_vcNumber,'') <> '' then
			
         IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = v_vcNumber AND numDomainId =  v_numDomainId AND numAccountId <> v_numAccountId) then
				
            RAISE EXCEPTION 'DuplicateNumber';
            RETURN;
         end if;
      end if;
      IF v_numParntAcntTypeID = 0 then
			
         SELECT Chart_Of_Accounts.numAccountId INTO v_numParntAcntTypeID FROM Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = v_numDomainId;
      end if;
      select   numOriginalOpeningBal, numOpeningBal, numParntAcntTypeID, vcAccountCode, coalesce(bitIsSubAccount,false) INTO v_OldOriginalOpeningBalance,v_OldOpeningBalance,v_OldParentAcntTypeID,
      v_vcAccountCode,v_OldIsSubAccount FROM    Chart_Of_Accounts WHERE   numAccountId = v_numAccountId
      AND numDomainId = v_numDomainId;
      IF (v_OldParentAcntTypeID != v_numParntAcntTypeID) then --bug fix 1439
            
         v_vcAccountCode := GetAccountTypeCode(v_numDomainId,v_numParntAcntTypeID,1::SMALLINT);
         v_numParentAccId := 0;
         v_bitIsSubAccount := false;
      end if;
      IF (v_bitIsSubAccount = true) then
			
         v_vcAccountCode := GetAccountTypeCode(v_numDomainId,v_numParentAccId,3::SMALLINT);
         select   numParntAcntTypeID INTO v_numParntAcntTypeID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND numAccountId = v_numParentAccId;
      end if;
      UPDATE
      Chart_Of_Accounts
      SET
      numAcntTypeId = v_numAcntTypeId,numParntAcntTypeID = v_numParntAcntTypeID,
      vcAccountName = coalesce(v_vcAccountNameWithNumber,'')
      || coalesce(v_vcAccountName,''),vcAccountDescription = v_vcAccountDescription,
      bitActive = v_bitActive,bitProfitLoss = v_bitProfitLoss,
      numOriginalOpeningBal = v_monOriginalOpeningBal,dtOpeningDate = v_dtOpeningDate,
      bitDepreciation = v_bitDepreciation,dtDepreciationCostDate = v_dtDepreciationCostDate,
      monDepreciationCost = v_monDepreciationCost,
      vcAccountCode = v_vcAccountCode,bitIsSubAccount = v_bitIsSubAccount,vcNumber = v_vcNumber,
      numParentAccId = v_numParentAccId,IsBankAccount = v_IsBankAccount,
      vcStartingCheckNumber = v_vcStartingCheckNumber,vcBankRountingNumber = v_vcBankRountingNumber,
      IsConnected = v_bitIsConnected,
      numBankDetailID = v_numBankDetailID
      WHERE
      numAccountId = v_numAccountId
      AND  numDomainId = v_numDomainId;
			
		--UPDATE dbo.Chart_Of_Accounts SET intLevel = 0 WHERE numAccountId = @numAccountId
      UPDATE
      Chart_Of_Accounts
      SET
      intLevel = CASE
      WHEN numParentAccId = 0 AND bitIsSubAccount = false THEN 0
      ELSE(SELECT coalesce(intLevel,0)+1 FROM Chart_Of_Accounts WHERE numAccountId = v_numParentAccId)
      END
      WHERE (bitIsSubAccount = true OR bitIsSubAccount = false) AND numAccountId = v_numAccountId;
      PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAccountId,v_monOriginalOpeningBal,v_dtOpeningDate);
   end if;
    
    
		--Maintain Sorting by Account code when adding new account with sub account  or  updating Account numeber
   RAISE NOTICE '%',v_intFlag;
   if (v_intFlag = 1) then -- @vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
      UPDATE Chart_Of_Accounts SET vcAccountCode = '' WHERE numParntAcntTypeID = v_numParntAcntTypeID  AND numDomainId = v_numDomainId;
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numAccountID NUMERIC(18,0),
         bitIsSubAccount BOOLEAN,
         numParentAccId NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numAccountID
				,bitIsSubAccount
				,numParentAccId) with recursive CTE(tintLevel,numAccountId,vcAccountName,bitIsSubAccount,numParentAccId) AS(SELECT
      CAST(0 AS INTEGER) AS tintLevel
					,numAccountId AS numAccountId
					,vcAccountName AS vcAccountName
					,bitIsSubAccount AS bitIsSubAccount
					,numParentAccId AS numParentAccId
      FROM
      Chart_Of_Accounts
      WHERE
      numParntAcntTypeID = v_numParntAcntTypeID
      AND coalesce(numParentAccId,0) = 0
      UNION ALL
      SELECT
      c.tintLevel+1 AS tintLevel
					,COA.numAccountId AS numAccountId
					,COA.vcAccountName AS vcAccountName
					,COA.bitIsSubAccount AS bitIsSubAccount
					,COA.numParentAccId AS numParentAccId
      FROM
      Chart_Of_Accounts  COA
      INNER JOIN
      CTE c
      ON
      COA.numParentAccId = c.numAccountID
      WHERE
      numParntAcntTypeID = v_numParntAcntTypeID) SELECT
      numAccountId
				,bitIsSubAccount
				,numParentAccId
      FROM
      CTE
      ORDER BY
      tintLevel,vcAccountName;
      select   COUNT(*) INTO v_iCount FROM tt_TEMP;
      WHILE v_i <= v_iCount LOOP
         select   numAccountID, bitIsSubAccount, numParentAccId INTO v_numTempAccountID,v_bitTempIsSubAccount,v_numTempParentAccId FROM tt_TEMP WHERE ID = v_i;
         IF v_bitTempIsSubAccount = true then
            v_vcTempAccountCode := GetAccountTypeCode(v_numDomainId,v_numTempParentAccId,3::SMALLINT);
         ELSE
			RAISE NOTICE'1:%',v_numDomainId;
			RAISE NOTICE'1:%',v_numParntAcntTypeID;
            v_vcTempAccountCode := GetAccountTypeCode(v_numDomainId,v_numParntAcntTypeID,1::SMALLINT);
         end if;
         UPDATE Chart_Of_Accounts SET vcAccountCode = v_vcTempAccountCode WHERE numAccountId = v_numTempAccountID;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;

		--Maintain only one profit and loss account through out system
   IF v_bitProfitLoss = true then
        
      UPDATE  Chart_Of_Accounts
      SET     bitProfitLoss = false
      WHERE   numAccountId <> v_numAccountId
      AND numDomainId = v_numDomainId;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;
--exec USP_INVENTORYREPORT @numDomainID=72,@numUserCntID=1,@numDateRange=19,@numBasedOn=11,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' OppDate between ''01/01/2008'' and ''11/23/2008'''
-- select * from financialyear
-- exec USP_InventoryReport @numDomainID=72,@numUserCntID=17,@numDateRange=19,@numBasedOn=9,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' oppDate between ''01/01/2009 '' And '' 01/18/2010''',@numWareHouseId=0


