DROP FUNCTION IF EXISTS usp_GetTableInfoDefault;

CREATE OR REPLACE FUNCTION usp_GetTableInfoDefault(v_numUserCntID NUMERIC DEFAULT 0,                        
v_numRecordID NUMERIC DEFAULT 0,                                    
v_numDomainId NUMERIC DEFAULT 0,                                    
v_charCoType CHAR(1) DEFAULT 'a',                        
v_pageId NUMERIC DEFAULT NULL,                        
v_numRelCntType NUMERIC DEFAULT NULL,                        
v_numFormID NUMERIC DEFAULT NULL,                        
v_tintPageType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
BEGIN
   IF(SELECT
   coalesce(sum(TotalRow),0)
   FROM(Select count(*) AS TotalRow FROM View_DynamicColumns WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainId AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainId AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType) TotalRows) <> 0 then

      IF v_numFormID = 88 AND v_pageId = 5 AND v_numRelCntType = 0 AND v_tintPageType = 2 then
	
         IF NOT EXISTS(SELECT numFieldID FROM View_DynamicColumns WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType AND numFieldID = 212) then
		
            INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,numViewId,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom)
			VALUES(v_numFormID
				,212
				,0
				,1
				,coalesce((SELECT MAX(tintRow)+1 FROM View_DynamicColumns WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType), 0)
				,v_numDomainId
				,v_numUserCntID
				,v_numRelCntType
				,v_tintPageType
				,0);
         end if;
         IF NOT EXISTS(SELECT numFieldID FROM View_DynamicColumns WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType AND numFieldID = 216) then
		
            INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,numViewId,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom)
			VALUES(v_numFormID
				,216
				,0
				,1
				,coalesce((SELECT MAX(tintRow)+1 FROM View_DynamicColumns WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntID AND numFormId = v_numFormID AND numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType), 0)
				,v_numDomainId
				,v_numUserCntID
				,v_numRelCntType
				,v_tintPageType
				,0);
         end if;
      end if;
      IF v_pageId = 1 or v_pageId = 4 or v_pageId = 12 or  v_pageId = 13 or  v_pageId = 14 then

         open SWV_RefCur for
         SELECT
         numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintColumn as intcoulmn,CAST('' AS VARCHAR(1000)) as vcValue,bitCustom AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated
		,numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName
		,CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN coalesce((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = numListID AND numDomainID = v_numDomainId),0)
         ELSE 0
         END AS ListRelID
		,CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN(SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID = numListID AND numDomainID = v_numDomainId)
         ELSE 0
         END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
		,(SELECT  FldDTLID from CFW_FLD_Values where  RecId = v_numRecordID and Fld_ID = numFieldId LIMIT 1) as FldDTLID
		,CAST('' AS VARCHAR(1000)) as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
         FROM
         View_DynamicColumns
         WHERE
         numFormId = v_numFormID
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainId
         AND coalesce(bitCustom,false) = false
         AND numRelCntType = v_numRelCntType
         AND tintPageType = v_tintPageType
         AND 1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
         WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
         ELSE 0 END)
         UNION
         SELECT
         numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,
		CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN(SELECT vcData FROM Listdetails WHERE numListItemID = TEMPCfw.vcValue::NUMERIC)
         WHEN vcAssociatedControlType = 'CheckBoxList'
         THEN COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
         ELSE
            TEMPCfw.vcValue
         END AS vcValue,
		true AS bitCustomField,'' AS vcPropertyName, true AS bitCanBeUpdated,numListID,
		CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN TEMPCfw.vcValue::NUMERIC
         ELSE 0
         END AS numListItemID,'' AS PopupFunctionName,
		CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN coalesce((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = numListID AND numDomainID = v_numDomainId),0)
         ELSE 0
         END AS ListRelID ,
		CASE
         WHEN vcAssociatedControlType = 'SelectBox'
         THEN(SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID = numListID AND numDomainID = v_numDomainId)
         ELSE 0
         END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,coalesce((SELECT  FldDTLID from CFW_FLD_Values where  RecId = v_numRecordID and Fld_ID = numFieldId LIMIT 1),0) as FldDTLID
		,TEMPCfw.vcValue as Value,'' AS vcDbColumnName,vcToolTip,0 AS intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
         FROM
         View_DynamicCustomColumns_RelationShip
		 LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(numFieldId,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE 
         WHERE
         grp_id = v_pageId
         AND numDomainID = v_numDomainId
         AND CAST(subgrp AS NUMERIC) = 0
         AND numUserCntID = v_numUserCntID
         AND numFormId = v_numFormID
         AND numRelCntType = v_numRelCntType
         AND tintPageType = v_tintPageType
         ORDER BY
         5,intcoulmn;
      end if;
      if v_pageId = 2 or  v_pageId = 3 or v_pageId = 5 or v_pageId = 6 or v_pageId = 7 or v_pageId = 8   or v_pageId = 11 then
 
         IF v_numFormID = 123 then -- Add/Edit Order - Item Grid Column Settings
		
            open SWV_RefCur for
            SELECT numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,
					CAST('' AS VARCHAR(1000)) as vcValue,coalesce(bitCustom,false) AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
					numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
		Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM View_DynamicColumns
            where numFormId = v_numFormID and numUserCntID = v_numUserCntID and  numDomainID = v_numDomainId
            and coalesce(bitCustom,false) = false and numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType
            union
            select numFieldId,vcFieldName ,vcURL,vcAssociatedControlType as fld_type,
			tintRow,tintColumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            when vcAssociatedControlType = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            else TEMPCfw.vcValue end as vcValue,
			true AS bitCustomField,'' AS vcPropertyName,
			true as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
		Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 AS intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            from View_DynamicCustomColumns
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(numFieldId,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE 
            where Grp_id = v_pageId and numDomainID = v_numDomainId and CAST(subgrp AS NUMERIC) = 0
            and numUserCntID = v_numUserCntID
            AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType
            order by 5,intcoulmn;
         ELSE
            open SWV_RefCur for
            SELECT numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,
					CAST('' AS VARCHAR(1000)) as vcValue,coalesce(bitCustom,false) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE
            WHEN v_charCoType = 'O' AND numFieldID = 100
            THEN
               CASE
               WHEN(SELECT COUNT(*) FROM BizDocComission WHERE numOppID = v_numRecordID AND coalesce(bitCommisionPaid,false) = true) > 0
               THEN
                  false
               ELSE
                  coalesce(bitAllowEdit,false)
               END
            ELSE
               coalesce(bitAllowEdit,false)
            END) AS BOOLEAN) AS bitCanBeUpdated,
					numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
		Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM View_DynamicColumns
            where numFormId = v_numFormID and numUserCntID = v_numUserCntID and  numDomainID = v_numDomainId
            and coalesce(bitCustom,false) = false and numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType
            AND  1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
            WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
            ELSE 0 END)
            union
            select numFieldId,vcFieldName ,vcURL,vcAssociatedControlType as fld_type,
			tintRow,tintColumn as intcoulmn,
			case 
			when vcAssociatedControlType = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            WHEN vcAssociatedControlType = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            ELSE TEMPCfw.vcValue end as vcValue,
			true AS bitCustomField,'' AS vcPropertyName,
			true as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
		Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 AS intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            from View_DynamicCustomColumns
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(numFieldId,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE
            where Grp_id = v_pageId and numDomainID = v_numDomainId and CAST(subgrp AS NUMERIC) = 0
            and numUserCntID = v_numUserCntID
            AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelCntType AND tintPageType = v_tintPageType
            order by 5,intcoulmn;
         end if;
      end if;
      IF v_pageId = 153 then
	
         open SWV_RefCur for
         SELECT
         numFieldID
			,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL
			,vcAssociatedControlType as fld_type
			,tintRow
			,tintColumn as intcoulmn
			,'' as vcValue
			,coalesce(bitCustom,false) AS bitCustomField
			,vcPropertyName
			,coalesce(bitAllowEdit,false) AS bitCanBeUpdated
			,numListID
			,0 AS numListItemID
			,PopupFunctionName
			,Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID
			,Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
         FROM
         View_DynamicColumns
         WHERE
         numFormId = v_numFormID
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainId
         AND coalesce(bitCustom,false) = false
         AND numRelCntType = v_numRelCntType
         AND tintPageType = v_tintPageType
         ORDER BY
         tintRow,intcoulmn;
      end if;
      if v_pageId = 0 then
  
         open SWV_RefCur for
         SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intCoulmn,
  HDR.vcDBColumnName,DTL.bitCustomField as bitCustomField,
  false as bitIsRequired,false AS bitIsEmail,false AS bitIsAlphaNumeric,false AS bitIsNumeric,false AS bitIsLengthValidation,false AS bitFieldMessage,0 AS intMaxLength,0 AS intMinLength, false AS bitFieldMessage,'' AS vcFieldMessage
         from PageLayoutDTL DTL
         join PageLayout HDR on DTL.numFieldId = HDR.numFieldId
         where HDR.Ctype = v_charCoType and numUserCntId = v_numUserCntID  and bitCustomField = false and
         numDomainId = v_numDomainId and DTL.numRelCntType = v_numRelCntType   order by DTL.tintRow,intcoulmn;
      end if;
   ELSEIF v_tintPageType = 2 and v_numRelCntType > 3 and
((select coalesce(sum(TotalRow),0) from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainId AND tintPageType = v_tintPageType AND numFormId IN(36) AND numRelCntType = 2
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainId AND tintPageType = v_tintPageType AND numFormId IN(36) AND numRelCntType = 2) TotalRows) <> 0)
   then

      SELECT * INTO SWV_RefCur FROM usp_GetTableInfoDefault(v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecordID,v_numDomainId := v_numDomainId,
      v_charCoType := v_charCoType,v_pageId := v_pageId,
      v_numRelCntType := 2,v_numFormID := v_numFormID,v_tintPageType := v_tintPageType::SMALLINT,SWV_RefCur := SWV_RCur);
   ELSEIF COALESCE(v_charCoType,'') <> 'b'
   then/*added by kamal to prevent showing of all  fields on checkout by default*/                  
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF v_pageId = 1 or v_pageId = 4 or v_pageId = 12 or  v_pageId = 13 or  v_pageId = 14 then
	   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID = v_numFormID AND numRelCntType = v_numRelCntType AND bitGridConfiguration = false AND numGroupID = v_numUserGroup) > 0 then
	
            v_IsMasterConfAvailable := true;
         end if;

		--If MasterConfiguration is available then load it otherwise load default columns
         IF v_IsMasterConfAvailable = true then
		
            open SWV_RefCur for
            SELECT
            numFieldId,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
				 CAST('' AS VARCHAR(1000)) as vcValue,false AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
				 0 AS Tabletype,numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
				Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId = v_numFormID AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.numRelCntType = v_numRelCntType AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = false AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
            UNION
            SELECT
            numFieldId as numFieldId,vcFieldName as vcFieldName ,vcURL,vcAssociatedControlType,
				tintRow,tintColumn,
				case 
				when vcAssociatedControlType = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            WHEN vcAssociatedControlType = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            else TEMPCfw.vcValue end as vcValue,
				 false AS bitCustomField,'' AS vcPropertyName,
				 false as bitCanBeUpdated,1 AS Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
				Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
				coalesce(bitIsRequired,false) AS bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 AS intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM
            View_DynamicCustomColumnsMasterConfig
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(numFieldId,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE
            WHERE
            View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormID AND
            View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numRelCntType AND
            View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = false AND
            coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
            ORDER BY
            5,intcoulmn;
         ELSE
            open SWV_RefCur for
            SELECT numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
         CAST('' AS VARCHAR(1000)) as vcValue,CAST(0 AS BOOLEAN) AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
         0 AS Tabletype,numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
            FROM View_DynamicDefaultColumns
            where numFormId = v_numFormID AND numDomainID = v_numDomainId AND
            1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
            WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
            ELSE 0 END)
            union
            select fld_id as numFieldId,fld_label as vcFieldName ,vcURL,fld_type,
   CAST(0 AS SMALLINT) as tintrow,CAST(0 AS INTEGER) as intcoulmn,
 case 
 when fld_type = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            WHEN fld_type = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            else TEMPCfw.vcValue end as vcValue,
 true AS bitCustomField,'' AS vcPropertyName,
 true as bitCanBeUpdated,1 AS Tabletype,CFM.numlistid,case when fld_type = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
Case when fld_type = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = CFM.numlistid and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
Case when fld_type = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = CFM.numlistid and numDomainID = v_numDomainId) else 0 end as DependentFields,
coalesce(V.bitIsRequired,false) AS bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,coalesce(V.vcFieldMessage,'') AS vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 AS intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
            from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id = numFieldId
            left join CFw_Grp_Master CFG on CAST(subgrp AS NUMERIC) = CFG.Grp_id
            LEFT JOIN CFW_Validation V ON V.numFieldId = CFM.Fld_id
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE
            where CFM.Grp_id = v_pageId and CFD.numRelation = v_numRelCntType and CFM.numDomainID = v_numDomainId and CAST(subgrp AS NUMERIC) = 0
            order by 5,intcoulmn;
         end if;
      end if;
      IF v_pageId = 2 or  v_pageId = 3 or v_pageId = 5 or v_pageId = 6 or v_pageId = 7 or v_pageId = 8  or v_pageId = 11 then
	  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID = v_numFormID AND numRelCntType = v_numRelCntType AND bitGridConfiguration = false AND numGroupID = v_numUserGroup) > 0 then
		
            v_IsMasterConfAvailable := true;
         end if;

		--If MasterConfiguration is available then load it otherwise load default columns
         IF v_IsMasterConfAvailable = true then
		
            open SWV_RefCur for
            SELECT
            numFieldId,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
				 CAST('' AS VARCHAR(1000)) as vcValue,false AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
				 0 AS Tabletype,numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
				Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId = v_numFormID AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.numRelCntType = v_numRelCntType AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = false AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
            UNION
            SELECT
            numFieldId as numFieldId,vcFieldName as vcFieldName ,vcURL,vcAssociatedControlType,
				tintRow,tintColumn,
				case 
				when vcAssociatedControlType = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            WHEN vcAssociatedControlType = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            else TEMPCfw.vcValue end as vcValue,
				 true AS bitCustomField,'' AS vcPropertyName,
				 true as bitCanBeUpdated,1 AS Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
				Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
				coalesce(bitIsRequired,false) AS bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 AS intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName,coalesce(numOrder,0) AS numOrder
            FROM
            View_DynamicCustomColumnsMasterConfig
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(numFieldId,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE
            WHERE
            View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormID AND
            View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numRelCntType AND
            View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = false AND
            coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
            ORDER BY
            5,intcoulmn;
         ELSE
            open SWV_RefCur for
            SELECT numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
			 CAST('' AS VARCHAR(1000)) as vcValue,false AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
			 0 AS Tabletype,numListID,CAST(0 AS INTEGER) AS numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
			Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
            FROM View_DynamicDefaultColumns DFV
            where numFormId = v_numFormID AND numDomainID = v_numDomainId AND
            1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
            WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
            ELSE 0 END)
            UNION
            select fld_id as numFieldId,fld_label as vcFieldName ,vcURL,fld_type,
			CAST(0 AS SMALLINT) as tintRow,CAST(0 AS INTEGER) as intcoulmn,
			case 
			when fld_type = 'SelectBox' then (select vcData from Listdetails where numListItemID = TEMPCfw.vcValue::NUMERIC)
            WHEN fld_type = 'CheckBoxList' then COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(TEMPCfw.vcValue,','))),'')
            else TEMPCfw.vcValue end as vcValue,
			true AS bitCustomField,'' AS vcPropertyName,
			true as bitCanBeUpdated,1 AS Tabletype,CFM.numlistid,case when fld_type = 'SelectBox' THEN TEMPCfw.vcValue::NUMERIC ELSE 0 END AS numListItemID,'' AS PopupFunctionName,
			Case when fld_type = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = CFM.numlistid and numDomainID = v_numDomainId),0) else 0 end as ListRelID ,
			Case when fld_type = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = CFM.numlistid and numDomainID = v_numDomainId) else 0 end as DependentFields,
			coalesce(V.bitIsRequired,false) AS bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,coalesce(V.vcFieldMessage,'') AS vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 AS intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
            FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id = numFieldId
            left join CFw_Grp_Master CFG on CAST(subgrp AS NUMERIC) = CFG.Grp_id
            LEFT JOIN CFW_Validation V ON V.numFieldId = CFM.Fld_id
			LEFT JOIN LATERAL (SELECT GetCustFldValue AS vcValue FROM GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID)) TEMPCfw ON TRUE
            WHERE CFM.Grp_id = v_pageId and CFM.numDomainID = v_numDomainId   and CAST(subgrp AS NUMERIC) = 0
            ORDER BY 5,intcoulmn;
         end if;
      end if;
      IF v_pageId = 153 then
	  
		--Check if Master Form Configuration is created by administrator if NoColumns=0
         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainId AND numFormID = v_numFormID AND numRelCntType = v_numRelCntType AND bitGridConfiguration = false AND numGroupID = v_numUserGroup) > 0 then
		
            v_IsMasterConfAvailable := true;
         end if;

		--If MasterConfiguration is available then load it otherwise load default columns
         IF v_IsMasterConfAvailable = true then
		
            open SWV_RefCur for
            SELECT
            numFieldId,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
				 '' as vcValue,false AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
				 0 AS Tabletype,numListID,0 AS numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
				Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,coalesce(vcFieldMessage,'') AS vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId,coalesce(vcGroupName,'') AS vcGroupName
            FROM
            View_DynamicColumnsMasterConfig
            WHERE
            View_DynamicColumnsMasterConfig.numFormId = v_numFormID AND
            View_DynamicColumnsMasterConfig.numDomainID = v_numDomainId AND
            View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
            View_DynamicColumnsMasterConfig.numRelCntType = v_numRelCntType AND
            View_DynamicColumnsMasterConfig.bitGridConfiguration = false AND
            coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false;
         ELSE
            open SWV_RefCur for
            SELECT numFieldID,coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
			 '' as vcValue,false AS bitCustomField,vcPropertyName,coalesce(bitAllowEdit,false) AS bitCanBeUpdated,
			 0 AS Tabletype,numListID,0 AS numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType = 'SelectBox' then coalesce((Select numPrimaryListID from FieldRelationship where numSecondaryListID = numListID and numDomainID = v_numDomainId),0) else 0 end as ListRelID,
			Case when vcAssociatedControlType = 'SelectBox' then(Select count(*) from FieldRelationship where numPrimaryListID = numListID and numDomainID = v_numDomainId) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
            FROM View_DynamicDefaultColumns DFV
            where numFormId = v_numFormID AND numDomainID = v_numDomainId AND bitDefault = true AND
            1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
            WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
            ELSE 0 END)
            ORDER BY tintRow,intcoulmn;
         end if;
      end if;
      IF v_pageId = 0 then
	
         open SWV_RefCur for
         SELECT numFieldId,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintrow,intColumn as intcoulmn,
		false as bitCustomField,Ctype,'0' as tabletype,
		false as bitIsRequired,false AS bitIsEmail,false AS bitIsAlphaNumeric,false AS bitIsNumeric, false AS bitIsLengthValidation,false AS bitFieldMessage,0 AS intMaxLength,0 AS intMinLength, false AS bitFieldMessage,'' AS vcFieldMessage
         from PageLayout where Ctype = v_charCoType    order by tintrow,intcoulmn;
      end if;
   end if;
   RETURN;
END; $$;


