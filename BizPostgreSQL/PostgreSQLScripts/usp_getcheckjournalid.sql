-- Stored procedure definition script USP_GetCheckJournalId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckJournalId(v_numCheckId NUMERIC(9,0) DEFAULT 0,            
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);
BEGIN
   open SWV_RefCur for Select numJOurnal_Id From General_Journal_Header Where numCheckId = v_numCheckId  and numDomainId = v_numDomainId;
END; $$;












