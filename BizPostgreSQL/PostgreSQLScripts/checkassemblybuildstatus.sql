DROP FUNCTION IF EXISTS CheckAssemblyBuildStatus;

CREATE OR REPLACE FUNCTION CheckAssemblyBuildStatus(v_numItemKitID NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_numQty NUMERIC(18,0)
	,v_bitSalesOrder BOOLEAN
	,v_tintCommitAllocation SMALLINT)
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWarehouseID  NUMERIC(18,0);
   v_IsReadyToBuild  BOOLEAN DEFAULT 1;
BEGIN
   select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;

   DROP TABLE IF EXISTS tt_TEMPCheckAssemblyBuildStatus CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCheckAssemblyBuildStatus
   (
      numItemCode NUMERIC(18,0),
      numQtyRequired INTEGER,
      numOnAllocation DOUBLE PRECISION,
      bitReadyToBuild BOOLEAN
   );

   INSERT INTO
   tt_TEMPCheckAssemblyBuildStatus
   SELECT
   numChildItemID,
		CAST(coalesce(numQtyItemsReq*v_numQty,0) AS INTEGER),
		WareHouseItems.numAllocation,
		CAST(CASE
   WHEN v_bitSalesOrder = true AND v_tintCommitAllocation = 2
   THEN(CASE WHEN coalesce(numQtyItemsReq*v_numQty,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
   ELSE(CASE WHEN coalesce(numQtyItemsReq*v_numQty,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END)
   END AS BOOLEAN) AS bitReadyToBuild
   FROM
   ItemDetails
   CROSS JOIN LATERAL(SELECT * FROM
      WareHouseItems
      WHERE
      numItemID = ItemDetails.numChildItemID
      AND numWareHouseID = v_numWarehouseID
      ORDER BY
      numWareHouseItemID LIMIT 1) WareHouseItems
   WHERE
   numItemKitID = v_numItemKitID;

   IF(SELECT COUNT(*) FROM tt_TEMPCheckAssemblyBuildStatus WHERE  bitReadyToBuild = false) > 0 then
	
      v_IsReadyToBuild := false;
   end if;

   RETURN v_IsReadyToBuild;
END; $$;

