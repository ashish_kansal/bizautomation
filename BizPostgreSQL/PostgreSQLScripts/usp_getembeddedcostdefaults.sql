-- Stored procedure definition script USP_GetEmbeddedCostDefaults for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmbeddedCostDefaults(v_numEmbeddedCostID NUMERIC DEFAULT 0,
    v_numCostCatID NUMERIC DEFAULT 0,
    v_numDomainID NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(numEmbeddedCostID as VARCHAR(255)),
				cast(numCostCatID as VARCHAR(255)),
				cast(numCostCenterID as VARCHAR(255)),
                cast(numAccountID as VARCHAR(255)),
                cast(numDivisionID as VARCHAR(255)),
                fn_GetComapnyName(numDivisionID) AS vcCompanyName,
                cast(numPaymentMethod as VARCHAR(255)),
                cast(numDomainID as VARCHAR(255))
   FROM    EmbeddedCostDefaults
   WHERE   
				--[numEmbeddedCostID] = @numEmbeddedCostID
				--AND
   numCostCatID = v_numCostCatID
   AND numDomainID = v_numDomainID;
END; $$;

-- EXEC USP_GetEmbeddedCostItems 156,110












