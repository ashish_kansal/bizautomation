-- Function definition script fn_GetBudgetMonthDet for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetBudgetMonthDet(v_numBudgetId NUMERIC(9,0),v_tintMonth SMALLINT,v_numYear NUMERIC(9,0),v_numDomainId NUMERIC(9,0),v_numChartAcntId NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  VARCHAR(10);
BEGIN
   select   OBD.monAmount INTO v_monAmount From OperationBudgetMaster OBM
   inner join OperationBudgetDetails OBD on OBM.numBudgetId = OBD.numBudgetID Where OBD.numBudgetID = v_numBudgetId And OBD.numChartAcntId = v_numChartAcntId and OBD.tintMonth = v_tintMonth And OBD.intYear = v_numYear
   And OBM.numDomainId = v_numDomainId;        
----       
---- Set @vcmonth = Case @tintMonth when 1 then 'Jan' when 2 then 'Feb' when 3 then 'Mar' when 4 then 'Apr' when 5 then 'May' when 6 then 'Jun' when 7 then 'Jul'        
---- when 8 then 'Aug' when 9 then 'Sep' when 10 then 'Oct' when 11 then 'Nov' when 12 then 'Dec' End      
   if v_monAmount = '0.00' then 
      v_monAmount := '';
   end if;      
   Return v_monAmount;
END; $$;

