-- Stored procedure definition script USP_GetTotalReceivedItemByBizDocType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTotalReceivedItemByBizDocType(v_numBizDocTypeID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(coalesce(SUM(numUnitHour),0) as VARCHAR(255)) AS totalUnit
   FROM
   OpportunityBizDocs OB
   INNER JOIN
   OpportunityBizDocItems OBDI
   ON
   OB.numOppBizDocsId = OBDI.numOppBizDocID
   WHERE
   numoppid = v_numOppID
   AND numBizDocId = v_numBizDocTypeID
   AND numOppItemID = v_numOppItemID;
END; $$;
















