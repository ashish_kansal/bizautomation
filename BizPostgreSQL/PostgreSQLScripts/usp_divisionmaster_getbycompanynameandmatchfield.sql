CREATE OR REPLACE FUNCTION USP_DivisionMaster_GetByCompanyNameAndMatchField(v_numDomainID NUMERIC(18,0)
	,v_vcCompanyName VARCHAR(300)
	,v_numMatchField NUMERIC(18,0)
	,v_vcMatchValue VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numMatchField = 14 then -- Assigned To
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND numAssignedTo = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 28
   then -- Industry
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND numCompanyIndustry = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 539
   then -- Organization ID
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND DM.numDivisionID = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 3 OR v_numMatchField = 535
   then -- Organization Name OR Customer PO#
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName) LIMIT 1;
   ELSEIF v_numMatchField = 10
   then -- Organization phone
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND vcComPhone = v_vcMatchValue LIMIT 1;
   ELSEIF v_numMatchField = 5
   then -- Organization profile
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND vcProfile = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 30
   then --Organization Status
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND numStatusID = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 6
   then -- Relationship
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND numCompanyType = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 451
   then -- Relationship Type
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND tintCRMType = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   ELSEIF v_numMatchField = 21
   then -- Territory
	
      open SWV_RefCur for
      SELECT 
      DM.numDivisionID
			,ADC.numContactId
      FROM
      CompanyInfo CI
      INNER JOIN
      DivisionMaster DM
      ON
      CI.numCompanyId = DM.numCompanyID
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numDivisionId = DM.numDivisionID
      AND coalesce(bitPrimaryContact,false) = true
      WHERE
      CI.numDomainID = v_numDomainID
      AND LOWER(vcCompanyName) = LOWER(v_vcCompanyName)
      AND numTerID = CAST(v_vcMatchValue AS NUMERIC) LIMIT 1;
   end if;
   RETURN;
END; $$;


