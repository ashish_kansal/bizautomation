-- Stored procedure definition script usp_GetOpportunityStageComments for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetOpportunityStageComments(v_numOppId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM OpptStageComments WHERE numoppid = v_numOppId;
END; $$;












