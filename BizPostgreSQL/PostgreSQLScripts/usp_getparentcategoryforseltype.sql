-- Stored procedure definition script USP_GetParentCategoryForSelType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetParentCategoryForSelType(v_numAcntType NUMERIC(9,0) DEFAULT 0,      
v_numAccountId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From Chart_Of_Accounts Where   ((numAccountId <> v_numAccountId And numAcntTypeId = v_numAcntType) or numParntAcntTypeID is null) and  numDomainId = v_numDomainId;
END; $$;












