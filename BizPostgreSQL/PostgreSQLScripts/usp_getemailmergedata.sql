DROP FUNCTION IF EXISTS usp_getemailmergedata;

CREATE OR REPLACE FUNCTION public.usp_getemailmergedata(
	v_nummoduleid numeric,
	v_vcrecordids character varying DEFAULT ''::character varying,
	v_numdomainid numeric DEFAULT NULL::numeric,
	v_tintmode smallint DEFAULT 0,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	v_numoppbizdocid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	IF (v_numModuleID = 1  or v_numModuleID = 0) then    
		OPEN SWV_RefCur FOR
		SELECT 
			ACI.numContactId AS "numContactId",
            GetListIemName(ACI.vcDepartment) AS "ContactDepartment",
            GetListIemName(ACI.vcCategory) AS "ContactCategory",
            ACI.vcGivenName AS "vcGivenName",
            ACI.vcFirstName AS "ContactFirstName",
            ACI.vcLastname AS "ContactLastName",
            ACI.numDivisionId AS "numDivisionId",
            GetListIemName(ACI.numContactType) AS "ContactType",
            GetListIemName(ACI.numTeam) AS "ContactTeam",
            ACI.numPhone AS "ContactPhone",
            ACI.numPhoneExtension AS "ContactPhoneExt",
            ACI.numCell AS "ContactCell",
            ACI.numHomePhone AS "ContactHomePhone",
            ACI.vcFax AS "ContactFax",
            ACI.vcEmail AS "ContactEmail",
            ACI.VcAsstFirstName AS "AssistantFirstName",
            ACI.vcAsstLastName AS "AssistantLastName",
            ACI.numAsstPhone AS "ContactAssistantPhone",
            ACI.numAsstExtn AS "ContactAssistantPhoneExt",
            ACI.vcAsstEmail AS "ContactAssistantEmail",
            CASE 
				WHEN ACI.charSex = 'M' THEN 'Male'
				WHEN ACI.charSex = 'F' THEN 'Female'
				ELSE '-'
			END AS "ContactGender",
            ACI.bintDOB AS "bintDOB",
            GetAge(ACI.bintDOB,TIMEZONE('UTC',now())) AS "ContactAge",
            GetListIemName(ACI.vcPosition) AS "ContactPosition",
            ACI.txtNotes AS "txtNotes",
            ACI.numCreatedBy AS "numCreatedBy",
            ACI.bintCreatedDate AS "bintCreatedDate",
            ACI.numModifiedBy AS "numModifiedBy",
            ACI.bintModifiedDate AS "bintModifiedDate",
            ACI.numDomainID AS "numDomainID",
            ACI.bitOptOut AS "bitOptOut",
            fn_GetContactName(ACI.numManagerID) AS "ContactManager",
            ACI.numRecOwner AS "numRecOwner",
            GetListIemName(ACI.numEmpStatus::NUMERIC(9,0)) AS "ContactStatus",
            ACI.vcTitle AS "ContactTitle",
            ACI.vcAltEmail AS "vcAltEmail",
            ACI.vcitemid AS "vcitemid",
            ACI.vcChangeKey AS "vcChangeKey",
            coalesce((SELECT vcECampName FROM ECampaign WHERE numECampaignID = ACI.numECampaignID LIMIT 1),'') AS "ContactDripCampaign",
			getContactAddress(ACI.numContactId) AS "ContactPrimaryAddress",
            C.numCompanyId AS "numCompanyId",
            C.vcCompanyName AS "OrganizationName",
            C.numCompanyType AS "numCompanyType",
            C.numCompanyRating AS "numCompanyRating",
            C.numCompanyIndustry AS "numCompanyIndustry",
            C.numCompanyCredit AS "numCompanyCredit",
            C.txtComments AS "txtComments",
            C.vcWebSite AS "vcWebSite",
            C.vcWebLabel1 AS "vcWebLabel1",
            C.vcWebLink1 AS "vcWebLink1",
            C.vcWebLabel2 AS "vcWebLabel2",
            C.vcWebLink2 AS "vcWebLink2",
            C.vcWebLabel3 AS "vcWebLabel3",
            C.vcWebLink3 AS "vcWebLink3",
            C.vcWeblabel4 AS "vcWeblabel4",
            C.vcWebLink4 AS "vcWebLink4",
            C.numAnnualRevID AS "numAnnualRevID",
            GetListIemName(C.numNoOfEmployeesId) AS "OrgNoOfEmployee",
            C.vcHow AS "vcHow",
            C.numCreatedBy AS "numCreatedBy",
            C.bintCreatedDate AS "bintCreatedDate",
            C.numModifiedBy AS "numModifiedBy",
            C.bintModifiedDate AS "bintModifiedDate",
            C.bitPublicFlag AS "bitPublicFlag",
            C.numDomainID AS "numDomainID",
            DM.numDivisionID AS "numDivisionID",
            DM.numCompanyID AS "numCompanyID",
            DM.vcDivisionName AS "vcDivisionName",
            DM.numGrpId AS "numGrpId",
            DM.numFollowUpStatus AS "numFollowUpStatus",
            DM.bitPublicFlag AS "bitPublicFlag",
            DM.numCreatedBy AS "numCreatedBy",
            DM.bintCreatedDate AS "bintCreatedDate",
            DM.numModifiedBy AS "numModifiedBy",
            DM.bintModifiedDate AS "bintModifiedDate",
            DM.tintCRMType AS "tintCRMType",
            DM.numDomainID AS "numDomainID",
            DM.bitLeadBoxFlg AS "bitLeadBoxFlg",
            DM.numTerID AS "numTerID",
            DM.numStatusID AS "numStatusID",
            DM.bintLeadProm AS "bintLeadProm",
            DM.bintLeadPromBy AS "bintLeadPromBy",
            DM.bintProsProm AS "bintProsProm",
            DM.bintProsPromBy AS "bintProsPromBy",
            DM.numRecOwner AS "numRecOwner",
            DM.decTaxPercentage AS "decTaxPercentage",
            DM.tintBillingTerms AS "tintBillingTerms",
            DM.numBillingDays AS "numBillingDays",
            DM.tintInterestType AS "tintInterestType",
            DM.fltInterest AS "fltInterest",
            DM.vcComPhone AS "vcComPhone",
            DM.vcComFax AS "vcComFax",
            DM.numCampaignID AS "numCampaignID",
            DM.numAssignedBy AS "numAssignedBy",
            DM.numAssignedTo AS "numAssignedTo",
            DM.bitNoTax AS "bitNoTax",
			COALESCE((SELECT 
						vcCreditCardNo 
					FROM 
						CustomerCreditCardInfo CC
					LEFT JOIN 
						Listdetails 
					ON 
						Listdetails.numListItemID = CC.numCardTypeID
					WHERE 
						numContactId = ACI.numContactId 
						AND bitIsDefault = true
					ORDER BY CC.bitIsDefault DESC LIMIT 1),'') AS "PrimaryCreditCardNo",
			(select  U.txtSignature from UserMaster U where U.numUserDetailId = ACI.numRecOwner LIMIT 1) as "Signature",
			(CASE WHEN v_numDomainID = 1 THEN coalesce((SELECT  LD.vcData FROM CFW_FLD_Values CFV INNER JOIN Listdetails LD ON CAST(CFV.Fld_Value As Numeric) = LD.numListItemID WHERE CFV.RecId = DM.numDivisionID AND Fld_ID = 12882 LIMIT 1),'') ELSE '' END) AS "Cust12882"
      FROM    CompanyInfo C
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = C.numCompanyId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
      AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
					 ---- Added by Priya(9 Feb 2018)----
      LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
      AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
      AND coalesce(BT.bitActive,false) = true
      WHERE   DM.numDomainID = v_numDomainID
      AND numContactId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if;
   IF v_numModuleID = 2 then
        
		open SWV_RefCur for
		SELECT
			fn_GetContactName(OM.numassignedto) AS "OpportunityAssigneeName",
			ACI.vcGivenName AS "vcGivenName",
			ACI.vcFirstName AS "ContactFirstName",
			ACI.vcLastname AS "ContactLastName",
			(coalesce(C.varCurrSymbol,'') || ' ' ||(CAST((GetDealAmount(OM.numOppId,TIMEZONE('UTC',now()),0::NUMERIC)) AS VARCHAR(30)))) AS "OppOrderSubTotal",
			vcpOppName AS "OpportunityName",
			fn_GetContactName(OM.numContactId) AS "OppOrderContact",
			coalesce(OM.vcOppRefOrderNo,'') AS "OppOrderCustomerPO",
			(coalesce(C.varCurrSymbol,'') || ' ' ||(CAST(coalesce((SELECT SUM(coalesce(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) AS VARCHAR(30)))) AS "OppOrderInvoiceGrandtotal",
			(coalesce(C.varCurrSymbol,'') || ' ' ||(CAST(coalesce((SELECT SUM(coalesce(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numoppid = OM.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) AS VARCHAR(30)))) AS "OppOrderTotalAmountPaid",
			coalesce((SELECT  vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingAddressName",
			coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingStreet",
			coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingCity",
			coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingState",
			coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingPostal",
			coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,1::SMALLINT) LIMIT 1),'') AS "OppOrderBillingCountry",
			coalesce((SELECT  vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingAddressName",
			coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingStreet",
			coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingCity",
			coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingState",
			coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingPostal",
			coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainId,2::SMALLINT) LIMIT 1),'') AS "OppOrderShippingCountry",
			(CASE 
				WHEN coalesce(OM.intUsedShippingCompany,0) = 0 THEN ''
				ELSE coalesce((SELECT vcData FROM Listdetails WHERE numListID = 82 AND numListItemID = coalesce(OM.intUsedShippingCompany,0)),'') 
			END) AS "OppOrderShipVia",
			coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = OM.numShippingService),'') AS "OppOrderShippingService",
			coalesce(OM.txtComments,'') AS "OppOrderComments",
			coalesce(OM.dtReleaseDate,'1900-01-01 00:00:00.000') AS "OppOrderReleaseDate",
			coalesce(OM.intPEstimatedCloseDate,'1900-01-01 00:00:00.000') AS "OppOrderEstimatedCloseDate",
			fn_GetOpportunitySourceValue(coalesce(OM.tintSource,0),coalesce(OM.tintSourceType,0)::SMALLINT,OM.numDomainId) AS "OppOrderSource",
			COALESCE((SELECT string_agg((SELECT vcShipFieldValue FROM ShippingFieldValues
				WHERE numDomainID = OM.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = OBD.numShipVia  AND vcFieldName = 'Tracking URL')) || '#^#' || RTRIM(LTRIM(vcTrackingNo)),'$^$')
			FROM OpportunityBizDocs OBD WHERE OBD.numoppid = OM.numOppId and coalesce(OBD.numShipVia,0) <> 0),
			'') AS  "OppOrderTrackingNo",
			(SELECT U.txtSignature FROM UserMaster U where U.numUserDetailId = OM.numContactId) as "Signature",
			(CASE 
				WHEN v_numOppBizDocId > 0 AND EXISTS (SELECT numTransHistoryID FROM TransactionHistory TH WHERE TH.numOppBizDocsID = v_numOppBizDocId AND TH.numOppID = OM.numOppId) 
				THEN (SELECT vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppBizDocsID = v_numOppBizDocId AND TH.numOppID = OM.numOppId)
				ELSE coalesce((SELECT vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppID = OM.numOppId LIMIT 1),'')
			END) AS "PrimaryCreditCardNo",
			coalesce(CMP.vcCompanyName,'') AS "OppOrderAccountName"
		FROM    OpportunityMaster OM
	  LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
      LEFT OUTER JOIN Currency C
      ON C.numCurrencyID = OM.numCurrencyID
      LEFT JOIN DivisionMaster DM
      ON  OM.numDivisionId = DM.numDivisionID
      LEFT JOIN CompanyInfo CMP
      ON DM.numCompanyID = CMP.numCompanyId
      WHERE   OM.numDomainId = v_numDomainID
      AND OM.numOppId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if;
   IF v_numModuleID = 4 then
        
      open SWV_RefCur for
      SELECT  vcProjectID AS "ProjectID",
                    vcProjectName AS "ProjectName",
                    fn_GetContactName(numAssignedTo) AS "ProjectAssigneeName",
                    FormatedDateFromDate(intDueDate,numdomainId) AS "ProjectDueDate"
      FROM    ProjectsMaster
      WHERE   numdomainId = v_numDomainID
      AND numProId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if; 
   IF v_numModuleID = 5 then
        
      open SWV_RefCur for
      SELECT  vcCaseNumber AS "CaseNo",
                    textSubject AS "CaseSubject",
                    fn_GetContactName(numAssignedTo) AS "CaseAssigneeName",
                    FormatedDateFromDate(intTargetResolveDate,numDomainID) AS "CaseResoveDate"
      FROM    Cases
      WHERE   numDomainID = v_numDomainID
      AND numCaseId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if; 
   IF v_numModuleID = 6 then
        
      open SWV_RefCur for
      SELECT  textDetails AS "TicklerComment",
                    GetListIemName(bitTask) AS "TicklerType",
                    fn_GetContactName(numAssign) AS "TicklerAssigneeName",
                    FormatedDateFromDate(dtStartTime,C.numDomainID) AS "TicklerDueDate",
                    GetListIemName(numStatus) AS "TicklerPriority",
                    FormatedDateTimeFromDate(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),C.numDomainID) AS "TicklerStartTime",
                    FormatedDateTimeFromDate(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),C.numDomainID) AS "TicklerEndTime",
                    GetListIemName(bitTask) || ' - ' || GetListIemName(numStatus) AS "TicklerTitle",
                    fn_GetContactName(C.numCreatedBy) AS "TicklerCreatedBy",
					CMP.vcCompanyName AS "OrganizationName"
      FROM    Communication C
      LEFT JOIN DivisionMaster DM
      ON C.numDivisionID = DM.numDivisionID
      LEFT JOIN CompanyInfo CMP
      ON DM.numCompanyID = CMP.numCompanyId
      WHERE   C.numDomainID = v_numDomainID
      AND numCommId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if; 
   IF v_numModuleID = 8 then
        
      open SWV_RefCur for
      SELECT  ACI.vcFirstName AS "ContactFirstName",
                    ACI.vcLastname AS "ContactLastName",
					C.vcCompanyName AS "OrganizationName",
					OB.vcBizDocID AS "BizDocID",
					GetListIemName(OB.numBizDocId) AS "BizDoc",
                   OB.monDealAmount AS "BizDocAmount",
                    OB.monAmountPaid AS "BizDocAmountPaid",
					OB.monAmountPaid AS "AmountPaid",
                    (OB.monDealAmount -OB.monAmountPaid) AS "BizDocBalanceDue",
                    GetListIemName(coalesce(OB.numBizDocStatus,0)) AS "BizDocStatus",
                    C.vcCompanyName AS "OrderOrganizationName",
                    ACI.vcFirstName AS "OrderContactFirstName",
                    ACI.vcLastname AS "OrderContactLastName",
                    coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(om.intBillingDays,0)),0)  AS "BizDocBillingTermsNetDays",
                    FormatedDateFromDate(OB.dtFromDate,om.numDomainId) AS "BizDocBillingTermsFromDate",
                    CASE coalesce(om.bitInterestType,false)
      WHEN true THEN '+   OB.vcBizDocID BizDocID,
                  '
      ELSE '-'
      END || CAST(coalesce(om.fltInterest,0) AS VARCHAR(10))
      || ' %' AS "BizDocBillingTermsInterest",
                    om.vcpOppName AS "OrderName",
                    CASE WHEN   coalesce(om.bitBillingTerms,false) <> false
      THEN 'Net ' ||
         CASE WHEN
         SWF_IsNumeric(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0)::VARCHAR) = true
         THEN
            CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) AS VARCHAR(20))
         ELSE
            CAST(0 AS VARCHAR(20))
         END || ',' ||
         CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' || CAST(om.fltInterest AS VARCHAR(20))   || ' %' END ELSE '-'
      END
      AS "BizDocBillingTerms",
						  OB.dtCreatedDate+CAST(CASE WHEN SWF_IsNumeric(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0)::VARCHAR) = true
      THEN coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0)
      ELSE 0
      END || 'day' as interval) AS "BizDocDueDate",
					
					ACI.vcEmail AS "ContactEmail",OB.numOppBizDocsId AS "numOppBizDocsId",
					coalesce(OB.vcTrackingNo,'') AS  "vcTrackingNo",
					coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = om.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = OB.numShipVia  AND vcFieldName = 'Tracking URL')),'') AS "vcTrackingURL",
					'' As "BizDocTemplateFromGlobalSettings"

					
     --               dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName
      FROM    OpportunityBizDocs OB
      INNER JOIN OpportunityMaster om ON om.numOppId = OB.numoppid
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = om.numDivisionId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId
      INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID
      WHERE   om.numDomainId = v_numDomainID
      AND OB.numOppBizDocsId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if; 	
   IF v_numModuleID = 9 then
        
      open SWV_RefCur for
      SELECT
      coalesce(TempBizDoc.vcBizDocID,'') AS "BizDocID",
                   coalesce(TempBizDoc.monDealAmount,0) AS "BizDocAmount",
                   coalesce(TempBizDoc.monAmountPaid,0) AS "BizDocAmountPaid",
                   coalesce(TempBizDoc.BizDocBalanceDue,0) AS "BizDocBalanceDue",
				   coalesce(TempBizDoc.BizDocStatus,'') AS "BizDocStatus",
                    C.vcCompanyName AS "OrderOrganizationName",
                    ACI.vcFirstName AS "OrderContactFirstName",
                    ACI.vcLastname AS "OrderContactLastName",
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(om.intBillingDays,0)),0) AS "BizDocBillingTermsNetDays",
                   -- dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE coalesce(om.bitInterestType,false)
      WHEN true THEN '+'
      ELSE '-'
      END || CAST(coalesce(om.fltInterest,0) AS VARCHAR(10))
      || ' %' AS BizDocBillingTermsInterest,
                    om.vcpOppName AS OrderName,
                    CASE WHEN   coalesce(om.bitBillingTerms,false) <> false
      THEN 'Net ' ||
         CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1
         SWF_IsNumeric(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0)::VARCHAR) = true
         THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
            CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) AS VARCHAR(20))
         ELSE
            CAST(0 AS VARCHAR(20))
         END || ',' ||
         CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' || CAST(om.fltInterest AS VARCHAR(20))   || ' %' END ELSE '-'
      END
      AS "BizDocBillingTerms"
                                    ,
                        --  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) ELSE 0 END,OB.dtCreatedDate) AS BizDocDueDate,
					ACI.vcFirstName AS "ContactFirstName",
                    ACI.vcLastname AS "ContactLastName",
					ACI.vcEmail AS "ContactEmail"
      FROM    OpportunityMaster om
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = om.numDivisionId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId
      INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID
      LEFT JOIN LATERAL(SELECT 
         vcBizDocID,
							monDealAmount,
							monAmountPaid,
							(monDealAmount -monAmountPaid) AS BizDocBalanceDue,
							GetListIemName(coalesce(numBizDocStatus,0)) AS BizDocStatus
         FROM
         OpportunityBizDocs
         WHERE
         numoppid = om.numOppID ANd coalesce(bitAuthoritativeBizDocs,0) = 1 LIMIT 1) AS TempBizDoc on TRUE
      WHERE   om.numDomainId = v_numDomainID
      AND om.numOppId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if; 
        
   IF v_numModuleID = 11 then
        
      open SWV_RefCur for
      SELECT  ACI.numContactId AS "numContactId",
                    GetListIemName(ACI.vcDepartment) AS "ContactDepartment",
                    GetListIemName(ACI.vcCategory) AS "ContactCategory",
                    ACI.vcGivenName AS "vcGivenName",
                    ACI.vcFirstName AS "ContactFirstName",
                    ACI.vcLastname AS "ContactLastName",
                    ACI.numDivisionId AS "numDivisionId",
                    GetListIemName(ACI.numContactType) AS "ContactType",
                    GetListIemName(ACI.numTeam) AS "ContactTeam",
                    ACI.numPhone AS "ContactPhone",
                    ACI.numPhoneExtension AS "ContactPhoneExt",
                    ACI.numCell AS "ContactCell",
                    ACI.numHomePhone AS "ContactHomePhone",
                    ACI.vcFax AS "ContactFax",
                    ACI.vcEmail AS "ContactEmail",
                    ACI.VcAsstFirstName AS "AssistantFirstName",
                    ACI.vcAsstLastName AS "AssistantLastName",
                    ACI.numAsstPhone AS "ContactAssistantPhone",
                    ACI.numAsstExtn AS "ContactAssistantPhoneExt",
                    ACI.vcAsstEmail AS "ContactAssistantEmail",
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
      WHEN ACI.charSex = 'F' THEN 'Female'
      ELSE '-'
      END AS "ContactGender",
                    ACI.bintDOB AS "bintDOB",
                    GetAge(ACI.bintDOB,TIMEZONE('UTC',now())) AS "ContactAge",
                    GetListIemName(ACI.vcPosition) AS "ContactPosition",
                    ACI.txtNotes AS "txtNotes",
                    ACI.numCreatedBy AS "numCreatedBy",
                    ACI.bintCreatedDate AS "bintCreatedDate",
                    ACI.numModifiedBy AS "numModifiedBy",
                    ACI.bintModifiedDate AS "bintModifiedDate",
                    ACI.numDomainID AS "numDomainID",
                    ACI.bitOptOut AS "bitOptOut",
                    fn_GetContactName(ACI.numManagerID) AS "ContactManager",
                    ACI.numRecOwner AS "numRecOwner",
                    GetListIemName(ACI.numEmpStatus::NUMERIC(9,0)) AS "ContactStatus",
                    ACI.vcTitle AS "ContactTitle",
                    ACI.vcAltEmail AS "vcAltEmail",
                    ACI.vcitemid AS "vcitemid",
                    ACI.vcChangeKey AS "vcChangeKey",
                    coalesce((SELECT vcECampName
					 FROM ECampaign
					 WHERE numECampaignID = ACI.numECampaignID),'') AS "ContactDripCampaign",
                    getContactAddress(ACI.numContactId) AS "ContactPrimaryAddress",
                    C.numCompanyId AS "numCompanyId",
                    C.vcCompanyName AS "OrganizationName",
                    C.numCompanyType AS "numCompanyType",
                    C.numCompanyRating AS "numCompanyRating",
                    C.numCompanyIndustry AS "numCompanyIndustry",
                    C.numCompanyCredit AS "numCompanyCredit",
                    C.txtComments AS "txtComments",
                    C.vcWebSite AS "vcWebSite",
                    C.vcWebLabel1 AS "vcWebLabel1",
                    C.vcWebLink1 AS "vcWebLink1",
                    C.vcWebLabel2 AS "vcWebLabel2",
                    C.vcWebLink2 AS "vcWebLink2",
                    C.vcWebLabel3 AS "vcWebLabel3",
                    C.vcWebLink3 AS "vcWebLink3",
                    C.vcWeblabel4 AS "vcWeblabel4",
                    C.vcWebLink4 AS "vcWebLink4",
                    C.numAnnualRevID AS "numAnnualRevID",
                    GetListIemName(C.numNoOfEmployeesId) AS "OrgNoOfEmployee",
                    C.vcHow AS "vcHow",
                    C.vcProfile AS "vcProfile",
                    C.numCreatedBy AS "numCreatedBy",
                    C.bintCreatedDate AS "bintCreatedDate",
                    C.numModifiedBy AS "numModifiedBy",
                    C.bintModifiedDate AS "bintModifiedDate",
                    C.bitPublicFlag AS "bitPublicFlag",
                    C.numDomainID AS "numDomainID",
                    DM.numDivisionID AS "numDivisionID",
                    DM.numCompanyID AS "numCompanyID",
                    DM.vcDivisionName AS "vcDivisionName",
                    DM.numGrpId AS "numGrpId",
                    DM.numFollowUpStatus AS "numFollowUpStatus",
                    DM.bitPublicFlag AS "bitPublicFlag",
                    DM.numCreatedBy AS "numCreatedBy",
                    DM.bintCreatedDate AS "bintCreatedDate",
                    DM.numModifiedBy AS "numModifiedBy",
                    DM.bintModifiedDate AS "bintModifiedDate",
                    DM.tintCRMType AS "tintCRMType",
                    DM.numDomainID AS "numDomainID",
                    DM.bitLeadBoxFlg AS "bitLeadBoxFlg",
                    DM.numTerID AS "numTerID",
                    DM.numStatusID AS "numStatusID",
                    DM.bintLeadProm AS "bintLeadProm",
                    DM.bintLeadPromBy AS "bintLeadPromBy",
                    DM.bintProsProm AS "bintProsProm",
                    DM.bintProsPromBy AS "bintProsPromBy",
                    DM.numRecOwner AS "numRecOwner",
                    DM.decTaxPercentage AS "decTaxPercentage",
                    DM.tintBillingTerms AS "tintBillingTerms",
                    DM.numBillingDays AS "numBillingDays",
                    DM.tintInterestType AS "tintInterestType",
                    DM.fltInterest AS "fltInterest",
                    DM.vcComPhone AS "vcComPhone",
                    DM.vcComFax AS "vcComFax",
                    DM.numCampaignID AS "numCampaignID",
                    DM.numAssignedBy AS "numAssignedBy",
                    DM.numAssignedTo AS "numAssignedTo",
                    DM.bitNoTax AS "bitNoTax",
                    vcCompanyName || ' ,<br>' || coalesce(AD2.vcStreet,'') || ' <br>'
      || coalesce(AD2.vcCity,'') || ' ,' || coalesce(fn_GetState(AD2.numState),'') || ' ' || coalesce(AD2.vcPostalCode,'')
      || ' <br>' || coalesce(fn_GetListItemName(AD2.numCountry),'') AS "OrgShippingAddress"
      FROM    CompanyInfo C
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = C.numCompanyId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
      AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 1 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      WHERE   DM.numDomainID = v_numDomainID
      AND numContactId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if;

   IF v_numModuleID = 45 then
        
      open SWV_RefCur for
      SELECT  C.vcCompanyName AS "OrganizationName",
					(select vcData from Listdetails where numListItemID = C.vcProfile) AS "OrgProfile",
					(select A.vcFirstName || ' ' || A.vcLastname
         from AdditionalContactsInformation A
         join DivisionMaster D
         on D.numDivisionID = A.numDivisionId
         where A.numContactId = DM.numAssignedTo) AS "OrgAssignto",
					 coalesce(AD1.vcAddressName,'') AS "OrgBillingName",
					 coalesce(AD1.vcStreet,'') AS "OrgBillingStreet",
					 coalesce(AD1.vcCity,'') AS "OrgBillingCity",
					 coalesce(fn_GetState(AD1.numState),'') AS "OrgBillingState",
					 coalesce(AD1.vcPostalCode,'') AS "OrgBillingPostal",
					 coalesce(fn_GetListItemName(AD1.numCountry),'') AS "OrgBillingCountry",
					 coalesce(AD2.vcAddressName,'') AS "OrgShippingName",
					 coalesce(AD2.vcStreet,'') AS "OrgShippingStreet",
					 coalesce(AD2.vcCity,'') AS "OrgShippingCity",
					 coalesce(fn_GetState(AD2.numState),'') AS "OrgShippingState",
					 coalesce(AD2.vcPostalCode,'') AS "OrgShippingPostal",
					 coalesce(fn_GetListItemName(AD2.numCountry),'') AS "OrgShippingCountry",
					 (select vcData from Listdetails where numListItemID = C.numCompanyCredit) AS  "OrgCreditLimit",
					 (select(CAST(BT.vcTerms AS VARCHAR(100)) || ' (' || CAST(BT.numNetDueInDays AS VARCHAR(10)) || ',' || CAST(BT.numDiscount AS VARCHAR(10)) || ',' ||  CAST(BT.numDiscountPaidInDays AS VARCHAR(10)) || ')')) AS "OrgNetTerms",
					 (select vcData from Listdetails where numListItemID = DM.numDefaultPaymentMethod) AS  "OrgPaymentMethod",
					 (select vcData from Listdetails where numListItemID = DM.intShippingCompany) AS  "OrgPreferredShipVia",
					 coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS "OrgPreferredParcelShippingService",
					(SELECT  vcCreditCardNo FROM CustomerCreditCardInfo CC
         LEFT JOIN Listdetails ON Listdetails.numListItemID = CC.numCardTypeID
         WHERE numContactId = ACI.numContactId AND bitIsDefault = true
         ORDER BY CC.bitIsDefault DESC LIMIT 1) AS "PrimaryCreditCardNo",
					ACI.vcFirstName AS "ContactFirstName",
                    ACI.vcLastname AS "ContactLastName",
					ACI.vcEmail AS "ContactEmail",
					ACI.numPhone AS "ContactPhone",
					ACI.numPhoneExtension AS "ContactPhoneExt",
					ACI.numCell AS "ContactCellPhone",
					 coalesce((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactID = ACI.numContactId::VARCHAR),'') AS "ContactEcommercepassword",
					(select U.txtSignature from UserMaster U where U.numUserDetailId = ACI.numRecOwner) AS "Signature",
					GetListIemName(C.numNoOfEmployeesId) AS "OrgNoOfEmployee",
					(CASE WHEN v_numDomainID = 1 THEN coalesce((SELECT  LD.vcData FROM CFW_FLD_Values CFV INNER JOIN Listdetails LD ON CFV.Fld_Value = LD.numListItemID::VARCHAR WHERE CFV.RecId = DM.numDivisionID AND Fld_ID = 12882 LIMIT 1),'') ELSE '' END) AS "Cust12882"
      FROM    CompanyInfo C
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = C.numCompanyId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DM.numDomainID
      AND AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
					 ---- Added by Priya(9 Feb 2018)----
      LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
      AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
      AND coalesce(BT.bitActive,false) = true
      WHERE   DM.numDomainID = v_numDomainID
      AND numContactId IN(SELECT  Id
         FROM    SplitIDs(v_vcRecordIDs,','));
   end if;
   RETURN; 
        	
--select * from dbo.OpportunityBizDocs	
--    SELECT TOP 1 bitInterestType,
--            *
--    FROM    dbo.OpportunityBizDocs ORDER BY numoppbizdocsid DESC 
END;
$BODY$;

