-- Stored procedure definition script USP_SendPassword for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SendPassword(v_vcEmail VARCHAR(100),
INOUT v_vcPassword VARCHAR(500) ,
INOUT v_NoOfRows SMALLINT ,
v_bitPortal BOOLEAN DEFAULT true,
INOUT v_numContactID NUMERIC DEFAULT 0 ,
v_numDomainID NUMERIC DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ResetLinkID  UUID;
BEGIN
   IF v_bitPortal = true then
	
      select   COUNT(*) INTO v_NoOfRows FROM
      ExtranetAccountsDtl E
      JOIN
      AdditionalContactsInformation A
      ON
      E.numContactID = A.numContactId WHERE
      vcEmail = v_vcEmail
      AND A.numDomainID = v_numDomainID;
      IF v_NoOfRows = 1 then
         v_ResetLinkID := uuid_generate_v4();
         UPDATE
         ExtranetAccountsDtl E
         SET
         vcResetLinkID = SUBSTR(CAST(v_ResetLinkID AS VARCHAR(255)),1,255),vcResetLinkCreatedTime = TIMEZONE('UTC',now())
         FROM
         AdditionalContactsInformation A
         WHERE
         E.numContactID = A.numContactId AND(vcEmail = v_vcEmail
         AND A.numDomainID = v_numDomainID);
      end if;
      select   coalesce(vcResetLinkID,''), A.numContactId INTO v_vcPassword,v_numContactID FROM
      ExtranetAccountsDtl E
      JOIN
      AdditionalContactsInformation A
      ON
      E.numContactID = A.numContactId WHERE
      vcEmail = v_vcEmail
      AND A.numDomainID = v_numDomainID    LIMIT 1;
   ELSE
      select   coalesce(vcPassword,''), UM.numUserDetailId INTO v_vcPassword,v_numContactID from UserMaster UM where vcEmailID = v_vcEmail    LIMIT 1;
	--	AND UM.numDomainID=@numDomainID 

      select   count(*) INTO v_NoOfRows from UserMaster UM where vcEmailID = v_vcEmail;
   end if;
   RETURN;
END; $$;


