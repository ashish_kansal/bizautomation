-- Stored procedure definition script USP_GetChartAcntDetailsForBalanceSheet_New for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForBalanceSheet_New(v_numDomainId NUMERIC(9,0),                                                  
v_dtFromDate TIMESTAMP,                                                
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, 
v_numAccountClass NUMERIC(9,0) DEFAULT 0,
v_DateFilter VARCHAR(20) DEFAULT NULL,
v_ReportColumn VARCHAR(20) DEFAULT NULL,
v_bitAddTotalRow BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PLAccountStruc  VARCHAR(500);
   v_FinancialYearStartDate  TIMESTAMP;
   v_PLCHARTID  NUMERIC(8,0);
   v_PLOPENING  DECIMAL(20,5);
   v_columns  VARCHAR(8000);--SET @columns = '';
   v_SUMColumns  VARCHAR(8000);--SET @SUMColumns = '';
   v_PivotColumns  VARCHAR(8000);--SET @PivotColumns = '';
   v_Select  VARCHAR(8000);
   v_Where  VARCHAR(8000);
   v_sql  TEXT DEFAULT '';
   v_ProifitLossAmount  DECIMAL(20,5) DEFAULT 0;
BEGIN
   v_dtFromDate := v_dtFromDate+INTERVAL '.000 ';
   v_dtToDate := v_dtToDate+INTERVAL '.997 ';
                                                    
   v_FinancialYearStartDate := GetFiscalStartDate(CAST(EXTRACT(YEAR FROM LOCALTIMESTAMP) AS INT),v_numDomainId);

   DROP TABLE IF EXISTS tt_TEMPQUARTER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPQUARTER
   (
      ID INTEGER,
      StartDate TIMESTAMP,
      EndDate TIMESTAMP
   );

   INSERT INTO tt_TEMPQUARTER  VALUES(1,v_FinancialYearStartDate,v_FinancialYearStartDate + make_interval(months => 3) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTER  VALUES(2,v_FinancialYearStartDate + make_interval(months => 3),v_FinancialYearStartDate + make_interval(months => 6) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTER  VALUES(3,v_FinancialYearStartDate + make_interval(months => 6),v_FinancialYearStartDate + make_interval(months => 9) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTER  VALUES(4,v_FinancialYearStartDate + make_interval(months => 9),v_FinancialYearStartDate + make_interval(months => 12) + make_interval(secs => -0.003));

	
   IF v_DateFilter = 'CurYear' then
		v_dtFromDate := v_FinancialYearStartDate;
		v_dtToDate := v_FinancialYearStartDate + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'PreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => -1) + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CuQur' then
		select StartDate, EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'CurPreQur' then
		select StartDate + make_interval(months => -3), EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'PreQur' then
		select StartDate + make_interval(months => -3), EndDate + make_interval(months => -3) INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'LastMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'ThisMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	end if;

   DROP TABLE IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNALBS
   (
      numDomainID NUMERIC(18,0),
      numAccountID NUMERIC(18,0),
      numAccountClass NUMERIC(18,0),
      datEntry_Date TIMESTAMP,
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

	-- OLD CODE COMMENTED DUE TO PERFORMANCE ISSUE
	--INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

   INSERT INTO
   tt_VIEW_JOURNALBS
   SELECT
   v_numDomainId,
		General_Journal_Details.numChartAcntId AS numAccountId,
		coalesce(General_Journal_Details.numClassID,0) AS numAccountClass,
		General_Journal_Header.datEntry_Date,
		SUM(coalesce(General_Journal_Details.numDebitAmt,0)) AS Debit,
		SUM(coalesce(General_Journal_Details.numCreditAmt,0)) AS Credit
   FROM
   General_Journal_Header
   INNER JOIN
   General_Journal_Details
   ON
   General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId
   WHERE
   General_Journal_Header.numDomainId = v_numDomainId
   AND General_Journal_Details.numDomainId = v_numDomainId
   AND (v_numAccountClass = 0 OR General_Journal_Details.numClassID = v_numAccountClass)
   GROUP BY
   General_Journal_Details.numChartAcntId,General_Journal_Header.datEntry_Date,
   General_Journal_Details.numClassID;

   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL
   (
      numAccountId NUMERIC(18,0),
      vcAccountCode VARCHAR(50),
      datEntry_Date TIMESTAMP,
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

	-- OLD CODE COMMENTED DUE TO PERFORMANCE ISSUE
	-- INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
   INSERT INTO tt_VIEW_JOURNAL
   SELECT
   Chart_Of_Accounts.numAccountId
		,AccountTypeDetail.vcAccountCode
		,General_Journal_Header.datEntry_Date
		,SUM(coalesce(General_Journal_Details.numDebitAmt,0)) AS Debit
		,SUM(coalesce(General_Journal_Details.numCreditAmt,0)) AS Credit
   FROM
   General_Journal_Header
   INNER JOIN General_Journal_Details ON General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId
   INNER JOIN Chart_Of_Accounts ON General_Journal_Details.numChartAcntId = Chart_Of_Accounts.numAccountId
   INNER JOIN AccountTypeDetail ON Chart_Of_Accounts.numParntAcntTypeID = AccountTypeDetail.numAccountTypeID
   WHERE
   General_Journal_Header.numDomainId = v_numDomainId
   AND General_Journal_Details.numDomainId = v_numDomainId
   AND (v_numAccountClass = 0 OR General_Journal_Details.numClassID = v_numAccountClass)
   GROUP BY
   Chart_Of_Accounts.numAccountId,AccountTypeDetail.vcAccountCode,General_Journal_Header.datEntry_Date;

   select   numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND bitProfitLoss = true;

   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numaccountid = v_PLCHARTID AND datentry_date <=  v_dtFromDate+INTERVAL '-0.003 ';

   DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTBalanceSheet CASCADE;

   CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTBalanceSheet AS
   WITH RECURSIVE DirectReport AS
	(  
		WITH RECURSIVE DirectReportInternal AS
		(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID, '#', 0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(ATD.vcAccountType AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), '000'), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
			FALSE AS bitTotal
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0),'000'),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0101','0102','0105')
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
            CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
            ATD.numAccountTypeID,
            CAST(0 AS NUMERIC) AS numAccountID,
            CAST(ATD.vcAccountType AS VARCHAR(300)) AS vcAccountType,
            ATD.vcAccountCode,
            LEVEL + 1 AS LEVEL, 
            CAST(d.Struc || '#' || CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), '000'), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
            FALSE AS bitTotal
		FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReportInternal D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
		WHERE 
			ATD.numDomainID = v_numdomainid 
		)	
		SELECT * FROM DirectReportInternal
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
			ATD.numAccountTypeID,
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)) AS vcAccountType,
			ATD.vcAccountCode,
			LEVEL + 1 AS LEVEL, 
			CAST(CONCAT(d.Struc,'#',TO_CHAR(COALESCE(ATD.tintSortOrder,0),'000'),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal
        FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReport D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
        WHERE 
			ATD.numDomainID = v_numdomainid 
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
	), DirectReport1  AS
	(
		WITH RECURSIVE DirectReport1INTERNAL AS 
		(
			SELECT 
				ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,false as bitProfitLoss, bitTotal
			FROM
				DirectReport
			UNION ALL
			SELECT 
				CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
				CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
				CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
				CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
				COA.vcAccountCode,
				LEVEL + 1 AS LEVEL,
				COA.numAccountId, 
				CAST(CONCAT(d.Struc ,'#',COA.numAccountId) AS VARCHAR(300)) AS Struc,
				COA.bitProfitLoss,
				FALSE AS bitTotal
			FROM 
				Chart_of_Accounts AS COA 
			JOIN 
				DirectReport1INTERNAL D 
			ON 
				D.numAccountTypeID = COA.numParntAcntTypeId 
				AND D.bitTotal = FALSE
			WHERE 
				COA.numDomainID = v_numdomainid 
				AND COALESCE(COA.bitIsSubAccount, FALSE) = FALSE 
		)
		SELECT 
			ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,bitProfitLoss,bitTotal
		FROM
			DirectReport1INTERNAL
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
			CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
			CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
			COA.vcAccountCode,
			LEVEL + 1 AS LEVEL,
			COA.numAccountId, 
			CAST(d.Struc || '#' || CAST(COA.numAccountId AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			FALSE AS bitTotal
		FROM 
			Chart_of_Accounts AS COA 
		JOIN 
			DirectReport1 D 
		ON 
			D.numAccountId = COA.numParentAccId 
			AND D.bitTotal = FALSE
        WHERE 
			COA.numDomainID = v_numdomainid 
			AND COALESCE(COA.bitIsSubAccount, FALSE) = TRUE
	)
	SELECT 
		ParentId
		,vcCompundParentKey
		,numAccountTypeID
		,vcAccountType
		,LEVEL
		,vcAccountCode
		,numAccountId
		,Struc
		,bitProfitLoss
		,bitTotal
	FROM 
		DirectReport1;  

   IF v_bitAddTotalRow = true then
	
      INSERT INTO tt_TEMPDIRECTREPORTBalanceSheet(ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			LEVEL,
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal)
      SELECT
      ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			CONCAT('Total ',vcAccountType),
			LEVEL,
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			true
      FROM
      tt_TEMPDIRECTREPORTBalanceSheet COA
      WHERE
      coalesce(COA.numAccountId,0) > 0
      AND coalesce(COA.bitTotal,false) = false
      AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTBalanceSheet COAInner WHERE COAInner.Struc ilike COA.Struc || '%') > 1;

      UPDATE tt_TEMPDIRECTREPORTBalanceSheet DRMain SET vcAccountType = SUBSTR(vcAccountType,7,LENGTH(vcAccountType) -6)  WHERE coalesce(DRMain.bitTotal,false) = true AND coalesce(numAccountId,0) = 0 AND DRMain.Struc IN(SELECT CONCAT(DRMain1.Struc,'#Total') FROm tt_TEMPDIRECTREPORTBalanceSheet DRMain1 WHERE coalesce(DRMain1.bitTotal,false) = false AND coalesce(DRMain1.numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTBalanceSheet DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain1.Struc || '%') = 0);
      DELETE FROM tt_TEMPDIRECTREPORTBalanceSheet DRMain  WHERE coalesce(DRMain.bitTotal,false) = false AND coalesce(numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTBalanceSheet DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain.Struc || '%') = 0;
   end if;

	
	
   v_Select := 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc';


   IF v_ReportColumn = 'Year' then
	
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet
      (
         intYear INTEGER,
         intMonth INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         ProfitLossAmount DECIMAL(20,5)
      );
      INSERT INTO tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet 
	  WITH RECURSIVE CTE AS
	  (
		SELECT
				EXTRACT(YEAR FROM v_dtFromDate) AS yr,
				EXTRACT(MONTH FROM v_dtFromDate) AS mm,
				TO_CHAR(v_dtFromDate,'FMMonth') AS mon,
				(TO_CHAR(v_dtFromDate,'FMMon') || ' ' || CAST(EXTRACT(YEAR FROM v_dtFromDate) AS VARCHAR(30))) AS MonthYear,
				CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,1) AS TIMESTAMP) AS dtStartDate,
				CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.002' AS dtEndDate,
				v_dtFromDate AS new_date
			UNION ALL
			SELECT
				EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS yr,
				EXTRACT(MONTH FROM new_date+INTERVAL '1 day') AS mm,
				TO_CHAR(new_date+INTERVAL '1 day','FMMonth') AS mon,
				(TO_CHAR(new_date+INTERVAL '1 day','FMMon') || ' ' || CAST(EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS VARCHAR(30))) AS MonthYear,
				CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,1) AS TIMESTAMP) AS dtStartDate,
				CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.002' AS dtEndDate,
					new_date+INTERVAL '1 day' AS new_date
			FROM 
				CTE
			WHERE 
				new_date+INTERVAL '1 day' < v_dtToDate
		) 
		SELECT
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM
			CTE
		GROUP BY
			yr,mm,MonthYear,dtStartDate,dtEndDate
		ORDER BY
			yr,mm;

		INSERT INTO tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0);
		
		UPDATE
			tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet
		SET
			EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);


		UPDATE
			tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet
		SET
			ProfitLossAmount =(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0104%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0106%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode ilike '0106%') AND datentry_date <=  StartDate+INTERVAL '-0.003 ')+v_PLOPENING;
			
		v_columns := COALESCE((SELECT string_agg('COALESCE("' || MonthYear  || '",0) AS "' || REPLACE(MonthYear,'_',' ') || '"',', ' ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet),'');
		v_PivotColumns := COALESCE((SELECT string_agg('"' || MonthYear || '"',',' ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet),'');


      DROP TABLE IF EXISTS tt_TEMPVIEWDATAYEAR CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATAYEAR AS
         SELECT
         COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE
         WHEN bitProfitLoss = true
         THEN Period.ProfitLossAmount
         WHEN COA.vcAccountCode ilike '0105%' AND COA.numAccountId <> v_PLCHARTID THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         WHEN COA.vcAccountCode ilike '0102%' THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         ELSE
            coalesce(Debit,0) -coalesce(Credit,0)
         END) AS Amount
		
         FROM
         tt_TEMPDIRECTREPORTBalanceSheet COA
         LEFT JOIN LATERAL(SELECT
         MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
         FROM
         tt_TEMPYEARMONTHGetChartAcntDetailsForBalanceSheet) AS Period
         LEFT JOIN LATERAL(SELECT
         SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
         FROM
         tt_VIEW_JOURNALBS V
         WHERE
         V.numAccountID = COA.numAccountID
         AND datEntry_Date <= Period.EndDate) AS t1 on TRUE on TRUE
         WHERE
         coalesce(COA.bitTotal,false) =false
         AND coalesce(COA.bitProfitLoss,false) = false;
      v_sql := CONCAT('SELECT
						ParentId AS "ParentId",
						vcCompundParentKey AS "vcCompundParentKey",
						numAccountId AS "numAccountId",
						numAccountTypeID AS "numAccountTypeID",
						vcAccountType AS "vcAccountType",
						LEVEL AS "LEVEL",
						vcAccountCode AS "vcAccountCode",
						Struc AS "Struc",
						"Type",
						bitTotal AS "bitTotal",',v_columns,'
					FROM crosstab(
					''SELECT 
							(''''#'''' || COA.Struc || ''''#'''') AS row_id,
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode,
							(''''#'''' || COA.Struc || ''''#'''') AS Struc,
							(CASE WHEN COALESCE(COA.numAccountId,0) > 0 AND COALESCE(COA.bitTotal,false) = false THEN 1 ELSE 2 END) "Type",
							COA.bitTotal,
							V.MonthYear,
							SUM(Amount) AS Amount
						FROM 
							tt_TEMPDIRECTREPORTBalanceSheet COA 
						LEFT OUTER JOIN 
							tt_TEMPVIEWDATAYEAR V 
						ON  
							V.Struc iLIKE CONCAT(REPLACE(COA.Struc,''''#Total'''','''''''') || (CASE WHEN ',CASE WHEN coalesce(v_bitAddTotalRow,false) THEN 'true' ELSE 'false' END,'=true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '''''''' ELSE ''''%'''' END))
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
						 order by 1'',  $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
					) AS final_result_pivot
					( 
						row_id TEXT
						,ParentId TEXT
						,vcCompundParentKey TEXT
						,numAccountId NUMERIC
						,numAccountTypeID NUMERIC
						,vcAccountType TEXT
						,LEVEL INT
						,vcAccountCode TEXT
						,Struc TEXT
						,"Type" SMALLINT
						,bitTotal BOOLEAN, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' NUMERIC(20,5))
					   ORDER BY Struc, "Type" desc');

		--RAISE NOTICE '%',v_sql;

      OPEN SWV_RefCur FOR EXECUTE v_sql;
   ELSEIF v_ReportColumn = 'Quarter'
   then
	
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHQUARTERBalanceSheetReport CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTHQUARTERBalanceSheetReport
      (
         intYear INTEGER,
         intQuarter INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         ProfitLossAmount DECIMAL(20,5)
      );
      INSERT INTO
      tt_TEMPYEARMONTHQUARTERBalanceSheetReport   with recursive CTE AS(SELECT
      GetFiscalyear(v_dtFromDate,v_numDomainId) AS yr,
				GetFiscalQuarter(v_dtFromDate,v_numDomainId) AS qq,
				('Q' || cast(GetFiscalQuarter(v_dtFromDate,v_numDomainId) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(v_dtFromDate,v_numDomainId) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-0.003 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-0.003 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-0.003 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-0.003 '
      END AS dtEndDate,
				v_dtFromDate AS new_date
      UNION ALL
      SELECT
      GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId) AS yr,
				GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId) AS qq,
				('Q' || cast(GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-0.003 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-0.003 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-0.003 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-0.003 '
      END AS dtEndDate,
				new_date+INTERVAL '1 day' AS new_date
      FROM CTE
      WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
      yr,qq,MonthYear,dtStartDate,dtEndDate,0
      FROM
      CTE
      GROUP BY
      yr,qq,MonthYear,dtStartDate,dtEndDate
      ORDER BY
      yr,qq;
      INSERT INTO tt_TEMPYEARMONTHQUARTERBalanceSheetReport  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0);
		
      UPDATE
      tt_TEMPYEARMONTHQUARTERBalanceSheetReport
      SET
      EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      UPDATE
      tt_TEMPYEARMONTHQUARTERBalanceSheetReport
      SET
      ProfitLossAmount =(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0104%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode ilike '0106%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode ilike '0106%') AND datentry_date <=  StartDate+INTERVAL '-0.003 ')+v_PLOPENING;
      
	  v_columns := COALESCE((SELECT string_agg('COALESCE("' || MonthYear  || '",0) AS "' || REPLACE(MonthYear,'_',' ') || '"',', ' ORDER BY intYear,intQuarter) FROM tt_TEMPYEARMONTHQUARTERBalanceSheetReport),'');
      v_PivotColumns := COALESCE((SELECT string_agg('"' || MonthYear || '"',',' ORDER BY intYear,intQuarter) FROM tt_TEMPYEARMONTHQUARTERBalanceSheetReport),'');
      
	  DROP TABLE IF EXISTS tt_TEMPVIEWDATAQUARTER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATAQUARTER AS
         SELECT
         COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE
         WHEN bitProfitLoss = true
         THEN Period.ProfitLossAmount
         WHEN COA.vcAccountCode ilike '0105%' AND COA.numAccountId <> v_PLCHARTID THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         WHEN COA.vcAccountCode ilike '0102%' THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         ELSE
            coalesce(Debit,0) -coalesce(Credit,0)
         END) AS Amount
		
         FROM
         tt_TEMPDIRECTREPORTBalanceSheet COA
         LEFT JOIN LATERAL(SELECT
         MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
         FROM
         tt_TEMPYEARMONTHQUARTERBalanceSheetReport) AS Period
         LEFT JOIN LATERAL(SELECT
         SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
         FROM
         tt_VIEW_JOURNALBS V
         WHERE
         V.numAccountID = COA.numAccountID
         AND datEntry_Date <= Period.EndDate) AS t1 on TRUE on TRUE
         WHERE
         coalesce(COA.bitTotal,false) = false
         AND coalesce(COA.bitProfitLoss,false) = false;

		 v_sql := CONCAT('SELECT
						ParentId AS "ParentId",
						vcCompundParentKey AS "vcCompundParentKey",
						numAccountId AS "numAccountId",
						numAccountTypeID AS "numAccountTypeID",
						vcAccountType AS "vcAccountType",
						LEVEL AS "LEVEL",
						vcAccountCode AS "vcAccountCode",
						Struc AS "Struc",
						"Type",
						bitTotal AS "bitTotal",',v_columns,'
					FROM crosstab(
					''SELECT 
							(''''#'''' || COA.Struc || ''''#'''') AS row_id,
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode,
							(''''#'''' || COA.Struc || ''''#'''') AS Struc,
							(CASE WHEN COALESCE(COA.numAccountId,0) > 0 AND COALESCE(COA.bitTotal,false) = false THEN 1 ELSE 2 END) "Type",
							COA.bitTotal,
							V.MonthYear,
							SUM(Amount) AS Amount
						FROM 
							tt_TEMPDIRECTREPORTBalanceSheet COA 
						LEFT OUTER JOIN 
							tt_TEMPVIEWDATAQUARTER V 
						ON  
							V.Struc iLIKE CONCAT(REPLACE(COA.Struc,''''#Total'''','''''''') || (CASE WHEN ',CASE WHEN coalesce(v_bitAddTotalRow,false) THEN 'true' ELSE 'false' END,'=true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '''''''' ELSE ''''%'''' END))
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
						 order by 1'',  $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
					) AS final_result_pivot
					( 
						row_id TEXT
						,ParentId TEXT
						,vcCompundParentKey TEXT
						,numAccountId NUMERIC
						,numAccountTypeID NUMERIC
						,vcAccountType TEXT
						,LEVEL INT
						,vcAccountCode TEXT
						,Struc TEXT
						,"Type" SMALLINT
						,bitTotal BOOLEAN, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' NUMERIC(20,5))
					   ORDER BY Struc, "Type" desc');

     
      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
     
   ELSE
      v_ProifitLossAmount :=(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0103%' AND datentry_date between v_dtFromDate and v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0104%' AND datentry_date between v_dtFromDate and v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE vcAccountCode  ilike '0106%' AND datentry_date between v_dtFromDate and v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNAL VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode ilike '0106%') AND datentry_date <=  v_dtFromDate+INTERVAL '-0.003 ')+v_PLOPENING;
      select   '#' || Struc || '#' INTO v_PLAccountStruc FROM tt_TEMPDIRECTREPORTBalanceSheet WHERe bitProfitLoss = true    LIMIT 1;
      DROP TABLE IF EXISTS tt_TEMPVIEWDATA CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATA ON COMMIT DROP AS
         SELECT
         COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			(CASE
         WHEN COA.vcAccountCode ilike '0105%' AND COA.numAccountId <> v_PLCHARTID THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         WHEN COA.vcAccountCode ilike '0102%' THEN(coalesce(Debit,0)*(-1) -coalesce(Credit,0)*(-1))
         ELSE coalesce(Debit,0) -coalesce(Credit,0)
         END) AS Amount,
			V.datEntry_Date
		
         FROM
         tt_TEMPDIRECTREPORTBalanceSheet COA
         LEFT JOIN
         tt_VIEW_JOURNALBS V
         ON
         COA.numAccountId = V.numAccountID
         AND datEntry_Date <= v_dtToDate
         WHERE
         COA.numAccountId IS NOT NULL
         AND coalesce(COA.bitTotal,false) = false
         AND coalesce(COA.bitProfitLoss,false) = false;
      open SWV_RefCur for
      SELECT
      COA.ParentId AS "ParentId",
			COA.vcCompundParentKey AS "vcCompundParentKey",
			COA.numAccountId AS "numAccountId",
			COA.numAccountTypeID AS "numAccountTypeID",
			COA.vcAccountType AS "vcAccountType",
			COA.LEVEL AS "LEVEL",
			COA.vcAccountCode AS "vcAccountCode",
			('#' || COA.Struc || '#') AS "Struc",
			(CASE WHEN coalesce(COA.numAccountId,0) > 0 AND coalesce(COA.bitTotal,false) = false THEN 1 ELSE 2 END) AS "Type",
			COA.bitTotal AS "bitTotal",
			(Case
      When COA.bitProfitLoss = true
      THEN v_ProifitLossAmount
      ELSE((CASE WHEN POSITION(('#' || COA.Struc || '#') IN v_PLAccountStruc) > 0 THEN v_ProifitLossAmount ELSE 0 END)+SUM(coalesce(Amount,0)))
      END) AS "Total"
      FROM
      tt_TEMPDIRECTREPORTBalanceSheet COA
      LEFT OUTER JOIN
      tt_TEMPVIEWDATA V
      ON
      V.Struc ilike REPLACE(COA.Struc,'#Total','') ||(CASE WHEN coalesce(v_bitAddTotalRow,false) = true AND coalesce(COA.numAccountId,0) > 0 AND COA.bitTotal = false THEN '' ELSE '%' END)
      GROUP BY
      COA.ParentId,COA.vcCompundParentKey,COA.numAccountTypeID,COA.vcAccountType, 
      COA.LEVEL,COA.vcAccountCode,COA.numAccountId,COA.Struc,COA.bitTotal,
      COA.bitProfitLoss
      ORDER BY
      "Struc","Type" desc;
   end if;

	
   DROP TABLE IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   RETURN;
END; $$;

--created by anoop jayaraj                                          


