-- Stored procedure definition script USP_MassSalesFulfillmentBatch_GetAll for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentBatch_GetAll(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		ID AS "ID"
		,vcName AS "vcName"
	FROM
		MassSalesFulfillmentBatch
	WHERE
		numDomainID = v_numDomainID
		AND coalesce(bitEnabled,false) = true
	ORDER BY
		vcName;
END; $$;












