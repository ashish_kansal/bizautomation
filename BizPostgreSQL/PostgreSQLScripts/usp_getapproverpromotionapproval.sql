-- Stored procedure definition script usp_getApproverPromotionApproval for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getApproverPromotionApproval(v_numDomainId NUMERIC(18,2) DEFAULT 0,
  v_numoppId NUMERIC(18,2) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ApprovalStaus  INTEGER;
   v_CreatedBy  INTEGER;
   v_listIds  TEXT;
BEGIN
   select   intPromotionApprovalStatus, numCreatedBy INTO v_ApprovalStaus,v_CreatedBy FROM OpportunityMaster WHERE numOppId = v_numoppId    LIMIT 1;
   IF(v_ApprovalStaus = 1) then

      SELECT  vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel1Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1)     LIMIT 1;
   end if;
   IF(v_ApprovalStaus = 2) then

      SELECT  vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel2Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1)     LIMIT 1;
   end if;
   IF(v_ApprovalStaus = 3) then

      SELECT  vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel3Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1)     LIMIT 1;
   end if;
   IF(v_ApprovalStaus = 4) then

      SELECT  vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel4Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1)     LIMIT 1;
   end if;
   IF(v_ApprovalStaus = 5) then

      SELECT  vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel5Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1)     LIMIT 1;
   end if;
   IF(v_ApprovalStaus = 6) then

      SELECT(SELECT  vcUserName FROM UserMaster where numUserDetailId =(SELECT  numLevel1Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1) LIMIT 1)
      || ',' ||(SELECT  vcUserName FROM UserMaster where numUserDetailId =(SELECT  numLevel2Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1) LIMIT 1)
      || ',' ||(SELECT  vcUserName FROM UserMaster where numUserDetailId =(SELECT  numLevel3Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1) LIMIT 1)
      || ',' ||(SELECT  vcUserName FROM UserMaster where numUserDetailId =(SELECT  numLevel4Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1) LIMIT 1)
      || ',' || vcUserName INTO v_listIds FROM UserMaster where numUserDetailId =(SELECT  numLevel5Authority FROM ApprovalConfig WHERE numModule = 2 AND numUserId = v_CreatedBy LIMIT 1);
   end if;
   open SWV_RefCur for SELECT v_listIds AS UserNames;
   RETURN;
END; $$;













