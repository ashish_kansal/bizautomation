-- Stored procedure definition script USP_GetBizDocDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBizDocDetail(v_numOppBizDocsId NUMERIC(9,0),
               v_numDomainId     NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT BD.vcBizDocID,
           cast(AD1.vcCity as VARCHAR(255)) AS vcBillCity,
           cast(coalesce(fn_GetListName(AD1.numCountry,0::BOOLEAN),'') as VARCHAR(100))  as vcBillCountry,
           cast(coalesce(fn_GetState(AD1.numState),'') as VARCHAR(100)) as vcBilState,
		   cast(coalesce(AD1.vcPostalCode,'') as VARCHAR(255)) as vcBillPostCode,
           cast(AD1.vcStreet as VARCHAR(255)) AS vcBillStreet,
           AC.vcFirstName,
           AC.vcLastname,
           AC.vcEmail,
           AC.numPhone,
           AC.numContactId,
           cast(coalesce(OM.bitBillingTerms,false) as BOOLEAN) AS bitBillingTerms,
           cast(coalesce(OM.intBillingDays,0) as INTEGER) AS intBillingDays,
           OM.numDivisionId,
           OM.tintopptype,
           cast(coalesce(BD.monAmountPaid,0) as DECIMAL(20,5)) AS monAmountPaid,
           cast(coalesce(BD.monDealAmount,0) as DECIMAL(20,5)) AS monDealAmount,
           fn_GetComapnyName(DM.numDivisionID) AS vcCompanyName ,--added by chintan, to be used in Receive payment
           BD.numoppid,
           BD.numBizDocTempID,
           BD.numBizDocId,
		   BD.dtFromDate
   FROM   OpportunityBizDocs BD
   INNER JOIN OpportunityMaster OM
   ON BD.numoppid = OM.numOppId
   INNER JOIN AdditionalContactsInformation AC
   ON OM.numContactId = AC.numContactId
   INNER JOIN DivisionMaster DM
   ON AC.numDivisionId = DM.numDivisionID
   LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = DM.numDomainID
   AND AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
   WHERE  BD.numOppBizDocsId = v_numOppBizDocsId
   AND OM.numDomainId = v_numDomainId;
END; $$;











