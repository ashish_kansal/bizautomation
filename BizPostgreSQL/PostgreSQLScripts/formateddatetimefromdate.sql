-- Function definition script FormatedDateTimeFromDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION FormatedDateTimeFromDate(v_date TIMESTAMP WITHOUT TIMEZONE,v_numDomainID NUMERIC)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strDateFormat  VARCHAR(30);
   v_strDate  VARCHAR(50);
   v_str4Yr  VARCHAR(4);
   v_str2Yr  VARCHAR(2);
   v_strIntMonth  VARCHAR(2);
   v_strDay  VARCHAR(2);
   v_str3LetMon  VARCHAR(3);
   v_strFullMonth  VARCHAR(15);
BEGIN
   select   vcDateFormat INTO v_strDateFormat from Domain where numDomainId = v_numDomainID;
   v_str4Yr := CAST(EXTRACT(Year FROM v_date) AS VARCHAR(4));
   v_str2Yr := SUBSTR(CAST(EXTRACT(Year FROM v_date) AS VARCHAR(4)),3,4);
   v_strFullMonth := TO_CHAR(v_date,'Month');
   v_str3LetMon := TO_CHAR(CAST(v_date AS TIMESTAMP),'Mon');
   v_strIntMonth := CAST(EXTRACT(month FROM v_date) AS VARCHAR(2)); 
   v_strDay := CAST(EXTRACT(day FROM v_date) AS VARCHAR(2));
   if cast(NULLIF(v_strIntMonth,'') as INTEGER) <= 9 then 
      v_strIntMonth := '0' || coalesce(v_strIntMonth,'');
   end if;
   if cast(NULLIF(v_strDay,'') as INTEGER) <= 9 then 
      v_strDay := '0' || coalesce(v_strDay,'');
   end if;   

   v_strDate := Replace(v_strDateFormat,'DD',v_strDay);
   v_strDate := Replace(v_strDate,'YYYY',v_str4Yr);
   v_strDate := Replace(v_strDate,'MM',v_strIntMonth);
   v_strDate := Replace(v_strDate,'YY',v_str2Yr);
   v_strDate := Replace(v_strDate,'MONTH',v_strFullMonth);
   v_strDate := Replace(v_strDate,'MON',v_str3LetMon);

    

        
   v_strDate := coalesce(v_strDate,'') || ' ' || SUBSTR(TO_CHAR(v_date,'Mon dd yyyy hh:miAM'),length(TO_CHAR(v_date,'Mon dd yyyy hh:miAM')) -7+1); 


   Return v_strDate;
END; $$;

