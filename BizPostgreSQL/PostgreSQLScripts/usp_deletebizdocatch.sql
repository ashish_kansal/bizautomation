-- Stored procedure definition script USP_DeleteBizDocAtch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteBizDocAtch(v_numBizDocAtchID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from BizDocAttachments where numAttachmntID = v_numBizDocAtchID;
   RETURN;
END; $$;


