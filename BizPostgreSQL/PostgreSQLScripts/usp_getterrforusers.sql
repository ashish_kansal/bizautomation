DROP FUNCTION IF EXISTS USP_GetTerrForUsers;

CREATE OR REPLACE FUNCTION USP_GetTerrForUsers(v_numUserCntID NUMERIC(9,0),        
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT distinct T.numTerritoryID as "numTerritoryID",Lst.vcData AS "vcTerName"
   FROM  UserTerritory T
   join Listdetails Lst
   on Lst.numListItemID = T.numTerritoryID
   WHERE  T.numUserCntID = v_numUserCntID and Lst.numListID = 78
   and T.numDomainID = v_numDomainId;
END; $$;












