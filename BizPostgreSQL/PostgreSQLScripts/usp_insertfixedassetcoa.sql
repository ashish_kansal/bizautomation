-- Stored procedure definition script usp_InsertFixedAssetCOA for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertFixedAssetCOA(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,                                                                      
v_numAcntType NUMERIC(9,0) DEFAULT 0,                                                                      
v_vcCatgyName VARCHAR(100) DEFAULT NULL,                                                                      
v_vcCatgyDescription VARCHAR(100) DEFAULT NULL,                                                 
v_monOriginalOpeningBal DECIMAL(20,5) DEFAULT NULL,                                                                      
v_monOpeningBal DECIMAL(20,5) DEFAULT NULL,                                                                      
v_dtOpeningDate TIMESTAMP DEFAULT NULL,                                                                      
v_bitActive BOOLEAN DEFAULT NULL,                                                                    
INOUT v_numAccountId NUMERIC(9,0) DEFAULT 0 ,                                                                    
v_bitFixed BOOLEAN DEFAULT NULL,                                                          
v_bitDepreciation NUMERIC(9,0) DEFAULT 0,                  
v_dtDepreciationCostDate TIMESTAMP DEFAULT NULL,                  
v_monDepreciationCost DECIMAL(20,5) DEFAULT NULL,                  
v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_numUserCntId NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountId1  NUMERIC(9,0);                                              
   v_numJournalId  NUMERIC(9,0);                   
   v_numAcntIdOriginal  NUMERIC(9,0);                      
   v_numAcntIdDepreciation  NUMERIC(9,0);                         
   v_numJournalId1  NUMERIC(9,0);          
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
BEGIN
   If v_numAccountId = 0 then
  
      if v_numParntAcntId = 0 then
         Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numDomainId = v_numDomainId;
      end if; --and numAccountId = 1         
                                                                        
      Insert Into Chart_Of_Accounts(numParntAcntTypeID,numAcntTypeId,vcAccountName,
   vcAccountDescription,numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId) Values(v_numParntAcntId,
   v_numAcntType,v_vcCatgyName,v_vcCatgyDescription,0,0,v_dtOpeningDate,v_bitActive,v_bitFixed,v_numDomainId);
   
      v_numAccountId1 := CURRVAL('Chart_Of_Accounts_seq');
      v_numAccountId := v_numAccountId1;
   --Added By Chintan
      PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAccountId1,v_monOpeningBal,v_dtOpeningDate);
      Select numAccountId INTO v_numAccountIdOpeningEquity From Chart_Of_Accounts Where bitOpeningBalanceEquity = true And numDomainId = v_numDomainId;
      If v_bitDepreciation = false then
                      
     -- For Original Cost                  
     -- Insert into Chart_Of_Accounts Table              
         Insert Into Chart_Of_Accounts(numParntAcntTypeID,numAcntTypeId,vcAccountName,
     vcAccountDescription,numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId) Values(v_numAccountId1,
     v_numAcntType,coalesce(v_vcCatgyName, '') || ' - Original',coalesce(v_vcCatgyDescription, '') || ' - Original',0,0,v_dtOpeningDate,v_bitActive,v_bitFixed,v_numDomainId);
        
         v_numAcntIdOriginal := CURRVAL('Chart_Of_Accounts_seq');
         v_numAccountId := v_numAcntIdOriginal;
		--Added By Chintan
         PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAcntIdOriginal,v_monOpeningBal,v_dtOpeningDate);
			               
     -- Insert into General_Journal_Header Table              
         Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)
     Values(v_dtOpeningDate,'Opening Balance Equity',v_monOpeningBal,null,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
     
         v_numJournalId := CURRVAL('General_Journal_Header_seq');
         RAISE NOTICE '@numJournalId==%',SUBSTR(CAST(v_numJournalId AS VARCHAR(10)),1,10);               
             
     -- Insert into General_Journal_Details Table               
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
     Values(v_numJournalId,v_monOpeningBal,0,v_numAcntIdOriginal,0,v_numDomainId);
     
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
     Values(v_numJournalId,0,v_monOpeningBal,v_numAccountIdOpeningEquity,0,v_numDomainId);                     
--Commented by chintan 
--     Exec USP_--Commented by chintan  @JournalId=@numJournalId,@numDomainId=@numDomainId                
                  
     --For Depreciation                  
     -- Insert into Chart_Of_Accounts Table              
     
         Insert Into Chart_Of_Accounts(numParntAcntTypeID,numAcntTypeId,vcAccountName,vcAccountDescription,numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId)
     Values(v_numAccountId1,v_numAcntType,coalesce(v_vcCatgyName, '') || ' - Depreciation',coalesce(v_vcCatgyDescription, '') || ' - Depreciation',0,0,v_dtOpeningDate,v_bitActive,v_bitFixed,v_numDomainId);
     
         v_numAcntIdDepreciation := CURRVAL('Chart_Of_Accounts_seq');
         v_numAccountId := v_numAcntIdDepreciation;
	--Added By Chintan
         PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAcntIdDepreciation,v_monOpeningBal,v_dtOpeningDate);
			
     -- Insert into General_Journal_Header Table              
         Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)
     Values(v_dtDepreciationCostDate,'Opening Balance Equity',v_monDepreciationCost,null,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
        
         v_numJournalId1 := CURRVAL('General_Journal_Header_seq');                 
                             
     -- Insert into General_Journal_Details Table               
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
     Values(v_numJournalId1,0,v_monDepreciationCost,v_numAcntIdDepreciation,0,v_numDomainId);
     
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
     Values(v_numJournalId1,v_monDepreciationCost,0,v_numAccountIdOpeningEquity,0,v_numDomainId);
     
         RAISE NOTICE '@numJournalId1==%',SUBSTR(CAST(v_numJournalId1 AS VARCHAR(10)),1,10);
      Else              
      --Insert into General_Journal_Header Table              
         Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)
      Values(v_dtOpeningDate,'Opening Balance Equity',v_monOpeningBal,null,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
      
         v_numJournalId := CURRVAL('General_Journal_Header_seq');
         RAISE NOTICE '@numJournalId==%',SUBSTR(CAST(v_numJournalId AS VARCHAR(10)),1,10);               
                  
      -- Insert into General_Journal_Details Table               
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
      Values(v_numJournalId,v_monOpeningBal,0,v_numAccountId1,0,v_numDomainId);
      
         Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)
      Values(v_numJournalId,0,v_monOpeningBal,v_numAccountIdOpeningEquity,0,v_numDomainId);
      end if;
   ELSEIF v_numAccountId <> 0
   then
                                           
  --Following updates new opening balance of newly assigned accountTypeID
  -- I think this part is not required as we just have to update account code.. rest will automatically come into parent account type
  --Commented by chintan      
--      Declare @strSQl as varchar(8000)          
--      set @strSQl= dbo.fn_ParentCategory(@numAccountId,@numDomainId)                                       
--   Declare @OpeningBal as DECIMAL(20,5)                                      
--   Select @OpeningBal=numOpeningBal From chart_of_accounts Where numAccountId=@numAccountId And numDomainId = @numDomainId         
--   set @strSQl='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@OpeningBal) + ' where  numAccountId in ('+@strSQl+') And numDomainId ='+convert(varchar(10),@numDomainId)    
--   print @strSQl                                      
--   exec (@strSQl)                                                             
                        
      Update Chart_Of_Accounts Set numParntAcntTypeID = v_numParntAcntId,numAcntTypeId = v_numAcntType,vcAccountName = v_vcCatgyName,
      vcAccountDescription = v_vcCatgyDescription,bitActive = v_bitActive Where numAccountId = v_numAccountId And numDomainId = v_numDomainId;                 
              
    --Added By Chintan
      PERFORM USP_ManageChartAccountOpening(v_numDomainId,v_numAccountId,v_monOpeningBal,v_dtOpeningDate);
   end if;
   RETURN;
END; $$;


