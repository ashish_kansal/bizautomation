-- Stored procedure definition script dbo.xmlOrderInsert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--Warning: object conversion was canceled because of license limitations. The target script equals to the source script.
----

CREATE PROCEDURE [dbo].[xmlOrderInsert]
@order ntext AS
DECLARE @docHandle int, @OID int
EXEC sp_xml_preparedocument @docHandle OUTPUT, @order
BEGIN TRANSACTION
INSERT INTO Orders( CustomerID, EmployeeID, OrderDate, RequiredDate ) 
  SELECT CustomerID, EmployeeID, OrderDate, RequiredDate 
  FROM Openxml( @docHandle, '/Order', 3) WITH ( CustomerID nchar(5), 
  EmployeeID int,   OrderDate datetime, RequiredDate datetime   )
IF @@ERROR<>0 BEGIN ROLLBACK TRANSACTION RETURN -100 END
SET @OID = SCOPE_IDENTITY()
INSERT INTO [Order Details] ( OrderID, ProductID, UnitPrice, Quantity, Discount ) 
SELECT @OID AS [PO ID], ProductID, UnitPrice, Quantity, Discount 
 FROM OpenXml( @docHandle, '/Order/OrderDetails', 1)   WITH 
  ( ProductID int, UnitPrice DECIMAL(20,5), Quantity smallint, Discount real   ) 
  IF @@ERROR<>0 BEGIN ROLLBACK TRANSACTION RETURN -101 END
COMMIT TRANSACTION
EXEC sp_xml_removedocument @docHandle SELECT @OID AS [Order ID]


