-- FUNCTION: public.usp_getcustomfieldlist(numeric, numeric, numeric, refcursor, refcursor)

-- DROP FUNCTION public.usp_getcustomfieldlist(numeric, numeric, numeric, refcursor, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcustomfieldlist(
	v_numdomainid numeric DEFAULT 0,
	v_locid numeric DEFAULT NULL::numeric,
	v_numlistid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor,
	INOUT swv_refcur2 refcursor DEFAULT NULL::refcursor)
    RETURNS record
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for
   SELECT  fld_id ,
            LOWER(Fld_label) AS Fld_label ,
            fld_type ,
            CASE WHEN subgrp = '0' THEN 'Detail Section'
   ELSE grp_name
   END AS TabName ,
            loc_name ,
            coalesce(CFV.bitIsRequired,false) AS bitIsRequired ,
            coalesce(CFV.bitIsEmail,false) AS bitIsEmail ,
            coalesce(CFV.bitIsAlphaNumeric,false) AS bitIsAlphaNumeric ,
            coalesce(CFV.bitIsNumeric,false) AS bitIsNumeric ,
            coalesce(CFV.bitIsLengthValidation,false) AS bitIsLengthValidation ,
            coalesce(fmst.numlistid,0) AS ListID
   FROM    CFW_Fld_Master fmst
   LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = CAST(fmst.subgrp AS Numeric)
   JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = fmst.Grp_id
   LEFT JOIN CFW_Validation CFV ON CFV.numFieldId = fld_id
   WHERE   fmst.numDomainID = v_numDomainID
   AND Lmst.Loc_id =(CASE WHEN v_locId = 0 THEN Lmst.Loc_id
   ELSE v_locId
   END);

   open SWV_RefCur2 for
   SELECT  Ld.numListItemID ,
            LOWER(vcData) AS vcData ,
            Ld.constFlag ,
            bitDelete ,
            coalesce(intSortOrder,0) AS intSortOrder,
            Ld.numListID AS ListID
   FROM    Listdetails Ld
   LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   WHERE   (Ld.numDomainid = v_numDomainID
   OR Ld.constFlag = TRUE)
   AND Ld.numListID IN(SELECT numListID FROM CFW_Fld_Master CFM
      LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = CAST(CFM.subgrp As Numeric)
      JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = CFM.Grp_id
								--LEFT JOIN CFW_Validation CFV ON CFV.numFieldID = fld_id
      WHERE CFM.numDomainID =  v_numDomainID)
   ORDER BY intSortOrder;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getcustomfieldlist(numeric, numeric, numeric, refcursor, refcursor)
    OWNER TO postgres;
