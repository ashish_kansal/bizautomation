-- Stored procedure definition script usp_InsertStagePercentageDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_InsertStagePercentageDetails(v_numStagePercentageId NUMERIC,
    v_tintConfiguration SMALLINT,
    v_vcStageName VARCHAR(1000),
    v_slpid NUMERIC,
    v_numDomainId NUMERIC,
    v_numUserCntID NUMERIC,
    v_numAssignTo NUMERIC DEFAULT 0,
    v_vcMileStoneName VARCHAR(1000) DEFAULT NULL,
    v_tintPercentage SMALLINT DEFAULT NULL,
    v_vcDescription VARCHAR(2000) DEFAULT NULL,
	v_dtStartDate TIMESTAMP DEFAULT NULL,
	v_dtEndDate TIMESTAMP DEFAULT NULL, 
    v_numParentStageID NUMERIC DEFAULT NULL,
	v_numProjectID NUMERIC DEFAULT NULL,
	v_numOppID NUMERIC DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numStageDetailId  NUMERIC;               

   v_sortOrder  NUMERIC(18,0) DEFAULT 0;
   v_numTempStagePercentageId  NUMERIC(18,0);
BEGIN
   SELECT coalesce(MAX(numStageOrder),0) INTO v_sortOrder FROM StagePercentageDetails WHERE numStagePercentageId = v_numStagePercentageId;
   v_sortOrder := v_sortOrder+1;
   INSERT  INTO StagePercentageDetails(numStagePercentageId,
                                        tintConfiguration,
                                        vcStageName,
                                        numdomainid,
                                        numCreatedBy,
                                        bintCreatedDate,
                                        numModifiedBy,
                                        bintModifiedDate,
                                        slp_id,
                                        numAssignTo,
                                        vcMileStoneName,
                                        tintPercentage,
                                        vcDescription,dtStartDate,dtEndDate,numParentStageID,numProjectid,numOppid,bitClose,numStageOrder)
VALUES(v_numStagePercentageId,
          v_tintConfiguration,
          v_vcStageName,
          v_numDomainId,
          v_numUserCntID,
          TIMEZONE('UTC',now()),
          v_numUserCntID,
          TIMEZONE('UTC',now()),
          v_slpid,
          v_numAssignTo,
          v_vcMileStoneName,
          v_tintPercentage,
          v_vcDescription,v_dtStartDate,v_dtEndDate,v_numParentStageID,v_numProjectID,v_numOppID,false,v_sortOrder);

  
   v_numTempStagePercentageId := CURRVAL('StagePercentageDetails_seq');

   if((select bitClose from StagePercentageDetails where numStageDetailsId = v_numParentStageID) = true) then
      update StagePercentageDetails set bitClose = true,tinProgressPercentage = 100 where v_numParentStageID = numParentStageID and numStageDetailsId = v_numTempStagePercentageId;
   end if;

   PERFORM usp_ManageStageAccessDetail(v_numProjectID,v_numOppID,v_numTempStagePercentageId,v_numUserCntID,0::SMALLINT);

   IF coalesce((SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numStageDetailsId = v_numTempStagePercentageId),0) = 0 then
	
      PERFORM USP_ManageStageTask(v_numDomainId,v_numTempStagePercentageId,'Place Holder',0,0,v_numUserCntID,
      v_numUserCntID,v_numOppID,v_numProjectID,0,false,0,false,false,0,
      0);
   end if;

--Update total progress
   IF v_numProjectID > 0 then
	 --  numeric(9, 0)
	 --  numeric(9, 0)
      PERFORM USP_GetProjectTotalProgress(v_numDomainId := v_numDomainId,v_numProId := v_numProjectID,v_tintMode := 1::SMALLINT);
   end if; --  tinyint

   IF v_numOppID > 0 then
	 --  numeric(9, 0)
	 --  numeric(9, 0)
      PERFORM USP_GetProjectTotalProgress(v_numDomainId := v_numDomainId,v_numProId := v_numOppID,v_tintMode := 0::SMALLINT);
   end if; --  tinyint

  
   open SWV_RefCur for SELECT  v_numTempStagePercentageId;
END; $$;












