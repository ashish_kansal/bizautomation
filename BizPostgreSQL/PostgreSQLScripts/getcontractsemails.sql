-- Function definition script GetContractsEmails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetContractsEmails(v_numContractid NUMERIC(9,0))
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcToEmail  VARCHAR(1000);  
   v_vcEmail  VARCHAR(100);  
   v_numContactId  NUMERIC;
   v_i  INTEGER;
BEGIN
   select   min(CRS.numContactId) INTO v_numContactId from ContractsContact CRS
   join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId
   and numContractid = v_numContractid;  
  
   v_i := 0;   
   v_vcToEmail := '';  
  
   while v_numContactId > 0 LOOP
      select   vcEmail INTO v_vcEmail from AdditionalContactsInformation Where     numContactId = v_numContactId;
      v_vcToEmail := case when v_i = 0 then v_vcEmail else coalesce(v_vcToEmail,'') || ';' || coalesce(v_vcEmail,'') end;
      select   min(CRS.numContactId) INTO v_numContactId from ContractsContact CRS
      join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId
      and numContractid = v_numContractid   and  CRS.numContactId > v_numContactId;
      v_i := v_i::bigint+1;
   END LOOP;    
  
   return v_vcToEmail;
END; $$;  
  
  
-- where        
-- numScheduleid =

