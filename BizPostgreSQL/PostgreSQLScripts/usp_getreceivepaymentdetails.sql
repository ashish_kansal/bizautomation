-- Stored procedure definition script USP_GetReceivePaymentDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReceivePaymentDetails(v_numDomainId          NUMERIC(9,0)  DEFAULT 0,
               v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT OppBizDocsPayDet.dtDueDate,
           OppBizDocsPayDet.numbizdocspaymentdetailsid AS numBizDocsPaymentDetailsId,
           OppBizDocsDet.numBizDocsPaymentDetId AS numBizDocsPaymentDetId,
           Opp.numOppId AS numOppId,
           Opp.numDivisionId AS numDivisionId,
           OppBizDocsDet.numBizDocsId AS numBizDocsId,
           Opp.vcpOppName AS Name,
           OppBizDocsPayDet.monAmount AS Amount,
           OppBizDocsDet.numPaymentMethod,
		   fn_GetListItemName(OppBizDocsDet.numPaymentMethod) AS PaymentMethod,
           fn_GetContactName(OppBizDocsDet.numCreatedBy)
   || ' - '
   || FormatedDateTimeFromDate(OppBizDocsDet.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId) AS CreatedBy,
           coalesce(OppBizDocsDet.vcMemo,'') AS Memo,
           coalesce(OppBizDocsDet.vcReference,'') AS Reference,
           coalesce(OppBizDocsDet.numCurrencyID,coalesce(Opp.numCurrencyID,0)) AS numCurrencyID,
           coalesce(OppBizDocsDet.fltExchangeRate,Opp.fltExchangeRate) AS fltExchangeRate,
           coalesce(OppBizDocsPayDet.monAmount*OppBizDocsDet.fltExchangeRate,0) AS ConAmt,
           coalesce(OppBizDocsDet.numCardTypeID,0) AS numCardTypeID,OppBizDocs.vcBizDocID
--           (SELECT fltTransactionCharge FROM [COACreditCardCharge] numDomainID=@numDomainId AND numCreditCardTypeId =OppBizDocsDet.[numCardTypeID]) AS 
   FROM   OpportunityBizDocsDetails OppBizDocsDet
   INNER JOIN OpportunityBizDocsPaymentDetails OppBizDocsPayDet
   ON OppBizDocsDet.numBizDocsPaymentDetId = OppBizDocsPayDet.numBizDocsPaymentDetId
   INNER JOIN OpportunityBizDocs OppBizDocs
   ON OppBizDocsDet.numBizDocsId = OppBizDocs.numOppBizDocsId
   INNER JOIN OpportunityMaster Opp
   ON OppBizDocs.numoppid = Opp.numOppId
   INNER JOIN AdditionalContactsInformation ADC
   ON Opp.numContactId = ADC.numContactId
   INNER JOIN DivisionMaster Div
   ON Opp.numDivisionId = Div.numDivisionID
   AND ADC.numDivisionId = Div.numDivisionID
   INNER JOIN CompanyInfo C
   ON Div.numCompanyID = C.numCompanyId
   WHERE  OppBizDocsDet.numDomainId = v_numDomainId
   AND OppBizDocsDet.bitAuthoritativeBizDocs = true
   AND OppBizDocsPayDet.bitIntegrated = false
   AND Opp.tintopptype = 1
   AND OppBizDocsDet.tintPaymentType <> 5;
   RETURN; --Do not show commission entries
--           AND dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod) <> 'Credit Card'
  --And OppBizDocsPayDet.dtDueDate<=dateadd(minute,-@ClientTimeZoneOffset,getutcdate())
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/













