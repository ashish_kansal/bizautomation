-- Stored procedure definition script USP_GetChartAcntDetailsForBalanceSheet for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForBalanceSheet(v_numDomainId NUMERIC(9,0),                                                  
v_dtFromDate TIMESTAMP,                                                
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine                                                   
v_numFRID NUMERIC DEFAULT 0,
v_numAccountClass NUMERIC(9,0) DEFAULT 0

--@tintByteMode as tinyint                                                            
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  DECIMAL(20,5);
   v_PLCHARTID  NUMERIC(8,0);
   v_PLOPENING  DECIMAL(20,5);
--DECLARE @numFinYear INT;
--DECLARE @dtFinYearFrom datetime;
   v_TotalIncome  DECIMAL(20,5);
   v_TotalExpense  DECIMAL(20,5);
   v_TotalCOGS  DECIMAL(20,5);
BEGIN
   DROP TABLE IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNALBS AS
      select * from VIEW_JOURNALBS where numdomainid = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0); 
   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL AS
      SELECT * FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

--Select Financial Year and From Date
--SELECT @numFinYear=numFinYearId,@dtFinYearFrom=dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId=@numDomainId

--SELECT @dtFromDate=MIN(datEntry_Date) FROM dbo.VIEW_JOURNAL WHERE numDomainID=@numDomainId
--Reflact timezone difference in From and To date
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);

  /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
   RAISE NOTICE '@dtToDate :'; 
   RAISE NOTICE '%',v_dtToDate;
--PRINT @numFinYear
--PRINT @dtFinYearFrom
   RAISE NOTICE '%',v_dtFromDate;
--RETURN
--select * from VIEW_JOURNALPL where numDomainid=72

--------------------------------------------------------
-- GETTING P&L VALUE
   v_CURRENTPL := 0;	
   v_PLOPENING := 0;

--SELECT @CURRENTPL = /*ISNULL(SUM(Opening),0)+*/
--+
--ISNULL(sum(Credit),0)- ISNULL(sum(Debit),0) FROM
--VIEW_DAILY_INCOME_EXPENSE P WHERE 
--numDomainID=@numDomainId AND datEntry_Date between @dtFromDate and @dtToDate ;


   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalIncome FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId AND vcAccountCode  ilike '0103%' AND datentry_date between v_dtFromDate and v_dtToDate;
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalExpense FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId AND vcAccountCode  ilike '0104%' AND datentry_date between v_dtFromDate and v_dtToDate;
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalCOGS FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId AND vcAccountCode  ilike '0106%' AND datentry_date between v_dtFromDate and v_dtToDate;
   RAISE NOTICE 'TotalIncome=									%',SUBSTR(CAST(v_TotalIncome AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalExpense=									%',SUBSTR(CAST(v_TotalExpense AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalCOGS=										%',SUBSTR(CAST(v_TotalCOGS AS VARCHAR(20)),1,20);
   v_CURRENTPL :=(v_TotalIncome+v_TotalExpense+v_TotalCOGS);
   RAISE NOTICE 'Current Profit/Loss = (Income - expense - cogs)= %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);



   RAISE NOTICE '%',v_CURRENTPL;


   select   numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND bitProfitLoss = true;
   RAISE NOTICE 'PL Account ID=%',SUBSTR(CAST(v_PLCHARTID AS VARCHAR(20)),1,20);
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId 
/* AND numAccountId=@PLCHARTID*/
   AND (vcAccountCode  ilike '0103%' or  vcAccountCode  ilike '0104%' or  vcAccountCode  ilike '0106%')
   AND datentry_date <=  v_dtFromDate+INTERVAL '-1 minute';

   select   coalesce(v_PLOPENING,0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND numaccountid = v_PLCHARTID; /*AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);*/


   RAISE NOTICE 'PL Account Opening=%',SUBSTR(CAST(v_PLOPENING AS VARCHAR(20)),1,20);
   v_CURRENTPL := v_PLOPENING+v_CURRENTPL;
   RAISE NOTICE 'PL = %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);






   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      bitIsSubAccount BOOLEAN
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
   0 AS OPENING,
coalesce((SELECT sum(coalesce(Debit,0)) FROM tt_VIEW_JOURNALBS VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datEntry_Date <= v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(coalesce(Credit,0)) FROM tt_VIEW_JOURNALBS VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datEntry_Date <= v_dtToDate),0) as CREDIT
,coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId  AND COA.bitActive = true  AND
   coalesce(bitProfitLoss,false) = false AND
      (COA.vcAccountCode ilike '0101%' OR
   COA.vcAccountCode ilike '0102%' OR
   COA.vcAccountCode ilike '0105%')
   UNION


/*(* -1) added by chintan to fix bug 2148 #3  */
   SELECT v_PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
v_CURRENTPL,CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId AND COA.bitActive = true and
   bitProfitLoss = true AND COA.numAccountId = v_PLCHARTID;


/*Added by chintan bugid 962 #3, For presentation do follwoing Show Credit amount in Posite and Debit Amount in Negative (only for accounts under quaity except PL account)*/
   UPDATE tt_PLSUMMARY SET
   Opening = Opening*(-1),--CASE WHEN Opening <0 THEN Opening * (-1) ELSE Opening END ,
Debit = Debit*(-1),--CASE WHEN Debit >0 THEN Debit * (-1) ELSE Debit END,
Credit = Credit*(-1)--CASE WHEN Credit <0 THEN Credit * (-1) ELSE Credit END 
   WHERE vcAccountCode ilike '0105%' AND numAccountId <> v_PLCHARTID;



   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );
--insert account types and balances
   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
 coalesce(SUM(coalesce(Opening,0)),0) as Opening,
coalesce(Sum(coalesce(Debit,0)),0) as Debit,coalesce(Sum(coalesce(Credit,0)),0) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0101%' OR
   ATD.vcAccountCode ilike '0102%' OR
   ATD.vcAccountCode ilike '0105%')
   WHERE PL.bitIsSubAccount = false
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;

   ALTER TABLE tt_PLSUMMARY
   DROP COLUMN bitIsSubAccount;

   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance DECIMAL(20,5),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );





   INSERT INTO tt_PLSHOW(numAccountId,
					vcAccountName,
					numParntAcntTypeID,
					vcAccountDescription,
					vcAccountCode,
					Opening,
					Debit,
					Credit,
					Balance,
					AccountCode1,
					vcAccountName1,
					Type)
   SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end)+(Debit*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end)
   -(Credit*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end) as Balance,
CAST(CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE  P.vcAccountCode
   END AS VARCHAR(100)) AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   UNION
   SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end)+(Debit*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end) -(Credit*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end) as Balance,
CAST(CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE  O.vcAccountCode
   END AS VARCHAR(100)) AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, 2 as Type
   FROM tt_PLOUTPUT O;

   UPDATE tt_PLSHOW SET TYPE = 3 WHERE numAccountId = v_PLCHARTID;




   open SWV_RefCur for select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LENGTH(A.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(A.vcAccountCode) -4) || A.vcAccountName
   ELSE A.vcAccountName
   END AS vcAccountName1, A.vcAccountCode ,
        A.TYPE	from tt_PLSHOW A ORDER BY A.vcAccountCode;

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   DROP TABLE IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   RETURN;
END; $$;














