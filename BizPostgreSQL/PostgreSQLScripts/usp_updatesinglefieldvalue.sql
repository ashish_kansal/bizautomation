-- Stored procedure definition script USP_UpdateSingleFieldValue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_UpdateSingleFieldValue(v_tintMode SMALLINT,
	v_numUpdateRecordID NUMERIC,
	v_numUpdateValueID NUMERIC DEFAULT 0,
	v_vcText TEXT DEFAULT NULL,
	v_numDomainID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	 -- For 26 and 37
   AS $$
   DECLARE
   v_Status  NUMERIC(9,0);
   v_SiteID  NUMERIC(9,0);
   v_OrderStatus  NUMERIC(9,0);
   v_BizdocStatus  NUMERIC(9,0);  
   v_numDomain  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
   SWV_RowCount INTEGER;
BEGIN
   v_numDomain := v_numDomainID; 

   IF v_tintMode = 1 then
	
      UPDATE  OpportunityBizDocsDetails
      SET     bitAmountCaptured = v_numUpdateValueID
      WHERE   numBizDocsPaymentDetId = v_numUpdateRecordID;
   end if;
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
   IF v_tintMode = 3 then
		
      UPDATE AdditionalContactsInformation SET txtNotes = coalesce(SUBSTR(CAST(txtNotes AS VARCHAR(8000)),1,8000),'') || ' ' ||  coalesce(SUBSTR(CAST(v_vcText AS VARCHAR(8000)),1,8000),'') WHERE numContactId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 4 then
		
      UPDATE OpportunityBizDocsDetails SET numBillPaymentJournalID = v_numUpdateValueID WHERE numBizDocsPaymentDetId = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 5 then
		
      UPDATE BizDocComission SET bitCommisionPaid = CAST(v_numUpdateValueID AS BOOLEAN) WHERE numOppBizDocId = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 6 then
		
      UPDATE OpportunityRecurring SET numOppBizDocID = v_numUpdateValueID WHERE numOppRecID = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 7 then
		
      UPDATE OpportunityRecurring SET dtRecurringDate = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(30)),1,30) AS TIMESTAMP) WHERE numOppRecID = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 8 then
		
      UPDATE OpportunityRecurring SET fltBreakupPercentage = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(30)),1,30) AS DOUBLE PRECISION) WHERE numOppRecID = v_numUpdateRecordID;
   end if;
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
   IF v_tintMode = 11 then
		
      DELETE FROM LuceneItemsIndex WHERE numItemCode = v_numUpdateRecordID;
   end if;    
   IF v_tintMode = 12 then
		
      UPDATE Domain SET vcPortalName = SUBSTR(CAST(v_vcText AS VARCHAR(50)),1,50)
      WHERE numDomainId = v_numUpdateRecordID;
   end if; 
   IF v_tintMode = 13 then
      UPDATE WareHouseItems SET numOnHand = numOnHand+CAST(SUBSTR(CAST(v_vcText AS VARCHAR(50)),1,50) AS NUMERIC(18,0)),dtModified = LOCALTIMESTAMP
      WHERE numWareHouseItemID = v_numUpdateRecordID;
			 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
      select   numItemID INTO v_numItemCode from WareHouseItems where numWareHouseItemID = v_numUpdateRecordID;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numUpdateRecordID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := 'Inventory Adjustment',
      v_numModifiedBy := v_numUpdateValueID,v_numDomainID := v_numDomain,
      SWV_RefCur := null);
   end if;
   IF v_tintMode = 14 then
		
      UPDATE Item SET monAverageCost = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(50)),1,50) AS NUMERIC(20,5))
      WHERE numItemCode = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 15 then
		
      UPDATE EmailHistory  SET numListItemId = v_numUpdateValueID  WHERE numEmailHstrID = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 16 then
		
      UPDATE EmailHistory SET bitIsRead = v_numUpdateValueID WHERE numEmailHstrID IN(SELECT Id FROM SplitIDs(CAST(v_vcText AS VARCHAR(200)),','));
   end if;
   IF v_tintMode = 17 then
		
      UPDATE CartItems SET numUserCntId = v_numUpdateValueID WHERE vcCookieId = CAST(v_vcText AS TEXT) AND numUserCntId = 0;
   end if;
   IF v_tintMode = 18 then
		
      UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 19 then
		
      UPDATE OpportunityMaster SET vcpOppName = SUBSTR(CAST(v_vcText AS VARCHAR(100)),1,100) WHERE numOppId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 20 then
		
      UPDATE UserMaster SET tintTabEvent = v_numUpdateValueID WHERE numUserId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 21 then
		
      UPDATE OpportunityBizDocs SET numBizDocTempID = v_numUpdateValueID WHERE numOppBizDocsId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 22 then
		
      UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID = v_numUpdateRecordID;
   end if;
   IF	 v_tintMode = 23 then
		
      UPDATE Item SET bintModifiedDate = TIMEZONE('UTC',now()) WHERE numItemCode = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 24 then
		
      UPDATE OpportunityMaster SET numStatus = v_numUpdateValueID WHERE numOppId in(select Items from Split(v_vcText,','));
   end if;
		
   IF v_tintMode = 25 then
		
      UPDATE WebAPIDetail SET vcEighthFldValue = v_vcText WHERE WebApiId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 26 then
		
      select   coalesce(numStatus,0) INTO v_Status FROM OpportunityMaster WHERE numOppId = v_numUpdateRecordID;
      IF v_Status = 0 then
		      
         select   CAST(tintSource AS NUMERIC(9,0)) INTO v_SiteID FROM	OpportunityMaster WHERE numOppId = v_numUpdateRecordID;
         select   coalesce(numOrderStatus,0), coalesce(numBizDocStatus,0) INTO v_OrderStatus,v_BizdocStatus FROM eCommercePaymentConfig WHERE numSiteId = v_SiteID AND numPaymentMethodId = 31488;
         UPDATE OpportunityMaster SET numStatus = coalesce(v_OrderStatus,0) WHERE numOppId = v_numUpdateRecordID AND numDomainId = v_numDomainID;
         Update OpportunityBizDocs set monAmountPaid = coalesce(monDealAmount,0),numBizDocStatus = v_BizdocStatus  WHERE  numoppid = v_numUpdateRecordID and bitAuthoritativeBizDocs = true;
      end if;
   end if;
   IF v_tintMode = 27 then
		
      UPDATE OpportunityBizDocs SET numBizDocStatusOLD = numBizDocStatus,numBizDocStatus = v_numUpdateValueID WHERE numOppBizDocsId = v_numUpdateRecordID;
   end if;
   IF v_tintMode = 28 then
		
      UPDATE General_Journal_Details SET bitReconcile = CAST((CASE WHEN v_vcText = 'R' THEN true ELSE false END) AS BOOLEAN),bitCleared = CAST((CASE WHEN v_vcText = 'C' THEN true ELSE false END) AS BOOLEAN),
      numReconcileID = 0
      WHERE numDomainId = v_numDomainID AND numTransactionId = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 29 then
		
      UPDATE ReturnHeader SET numRMATempID = v_numUpdateValueID WHERE numReturnHeaderID = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 30 then
		
      UPDATE ReturnHeader SET numBizdocTempID = v_numUpdateValueID WHERE numReturnHeaderID = v_numUpdateRecordID;
   end if;
		
   IF v_tintMode = 31 then
		
      UPDATE OpportunityMaster SET numOppBizDocTempID = v_numUpdateValueID WHERE numOppId = v_numUpdateRecordID;
   end if;
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
   IF v_tintMode = 33 then
		
      UPDATE ReturnHeader SET numBizdocTempID = v_numUpdateValueID,numRMATempID = v_numUpdateValueID WHERE numReturnHeaderID = v_numUpdateRecordID;
   end if;
		
			
   IF v_tintMode = 34 then
		
      UPDATE WebAPIDetail SET vcFourteenthFldValue = v_numUpdateValueID WHERE WebApiId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;
		
   IF v_tintMode = 35 then
		
      UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(10)),1,10) AS DOUBLE PRECISION)
      WHERE numOppId = v_numUpdateRecordID AND numTaxItemID = 0;
   end if;
		
   IF v_tintMode = 36 then
		
      UPDATE Import_File_Master SET numProcessedCSVRowNumber = v_numUpdateValueID
      WHERE intImportFileID = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;
   IF v_tintMode = 37 then
		
      select   coalesce(numStatus,0) INTO v_Status FROM OpportunityMaster WHERE numOppId = v_numUpdateRecordID;
      IF v_Status = 0 then
		      
         select   CAST(tintSource AS NUMERIC(9,0)) INTO v_SiteID FROM	OpportunityMaster WHERE numOppId = v_numUpdateRecordID;
         select   coalesce(numFailedOrderStatus,0) INTO v_OrderStatus FROM eCommercePaymentConfig WHERE numSiteId = v_SiteID AND numPaymentMethodId = 31488;
         UPDATE OpportunityMaster SET numStatus = coalesce(v_OrderStatus,0) WHERE numOppId = v_numUpdateRecordID AND numDomainId = v_numDomainID;
      end if;
   end if;
   IF v_tintMode = 38 then
		
      UPDATE WebAPIDetail SET    vcNinthFldValue = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(30)),1,30) AS TIMESTAMP)
      WHERE  WebApiId =  v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;
		
   IF v_tintMode = 39 then
		
      UPDATE WebAPIDetail SET    vcSixteenthFldValue = CAST(SUBSTR(CAST(v_vcText AS VARCHAR(30)),1,30) AS TIMESTAMP)
      WHERE  WebApiId =  v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;

   IF v_tintMode = 40 then
		
      UPDATE WebAPIDetail SET bitEnableAPI = v_numUpdateValueID WHERE WebApiId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;

   IF v_tintMode = 41 then
		
      UPDATE Domain SET numShippingServiceItemID = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;
		
   IF v_tintMode = 42 then
		
      UPDATE Domain SET numDiscountServiceItemID = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;
		
   IF v_tintMode = 43 then
		
      UPDATE Cases SET numStatus = v_numUpdateValueID WHERE numCaseId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;
		
   IF v_tintMode = 44 then
		
      UPDATE UserMaster SET numDefaultClass = v_numUpdateValueID WHERE numUserId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;

   IF v_tintMode = 46 then
		
      UPDATE Communication SET bitClosedFlag = CAST(v_numUpdateValueID AS BOOLEAN) WHERE numCommId = v_numUpdateRecordID AND numDomainID = v_numDomainID;
   end if;

   IF v_tintMode = 47 then
		
      UPDATE Domain SET bitCloneLocationWithItem = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 48 then
		
      IF coalesce(SUBSTR(CAST(v_vcText AS VARCHAR(30)),1,30),'') = '' then
			
         DELETE FROM DomainMarketplace WHERE numDomainID = v_numDomainID;
      ELSE
         DELETE FROM DomainMarketplace WHERE numDomainID = v_numDomainID AND numMarketplaceID NOT IN(SELECT Id FROM SplitIDs(v_vcText,','));
         INSERT INTO DomainMarketplace(numDomainID,numMarketplaceID) SELECT v_numDomainID,Id FROM SplitIDs(v_vcText,',') WHERE Id NOT IN(SELECT numMarketplaceID FROM DomainMarketplace WHERE numDomainID = v_numDomainID);
      end if;
   end if;

   IF v_tintMode = 49 then
		
      UPDATE OpportunityMaster SET intUsedShippingCompany = v_numUpdateValueID,numShippingService = -1 WHERE numDomainId = v_numDomainID AND numOppId = v_numUpdateRecordID;
   end if;

   IF v_tintMode = 50 then
		
      UPDATE StagePercentageDetailsTask SET vcTaskName = v_vcText WHERE numDomainID = v_numDomainID AND numTaskId = v_numUpdateRecordID;
   end if;

   IF v_tintMode = 51 then
		
      UPDATE Domain SET bitReceiveOrderWithNonMappedItem = CAST(v_numUpdateValueID AS BOOLEAN) WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 52 then
		
      UPDATE Domain SET numItemToUseForNonMappedItem = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 53 then
		
      UPDATE Domain SET bitUsePredefinedCustomer = CAST(v_numUpdateValueID AS BOOLEAN) WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 54 then
		
      UPDATE Domain SET tintReorderPointBasedOn = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 55 then
		
      UPDATE Domain SET intReorderPointDays = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 56 then
		
      UPDATE Domain SET intReorderPointPercent = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;

   IF v_tintMode = 57 then
		
      UPDATE Domain SET bitPurchaseTaxCredit = v_numUpdateValueID WHERE numDomainId = v_numDomainID;
   end if;
		
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   open SWV_RefCur for SELECT SWV_RowCount;
END; $$;
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203











