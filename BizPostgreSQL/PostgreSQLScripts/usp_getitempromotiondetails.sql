-- Stored procedure definition script USP_GetItemPromotionDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemPromotionDetails(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numPromotionID NUMERIC(18,0),
	v_numUnitHour DOUBLE PRECISION,
	v_monTotalAmount DECIMAL(20,5), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCurrency  VARCHAR(10);
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_numItemClassification  NUMERIC(18,0);
BEGIN
   IF coalesce(v_numPromotionID,0) > 0 then
      select   coalesce(vcCurrency,'$') INTO v_vcCurrency FROm Domain WHERE numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT 
      numProId
			,coalesce(vcLongDesc,'-') AS vcLongDesc
			,CONCAT(coalesce(vcShortDesc,'-'),(CASE WHEN coalesce(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcShortDesc
			,coalesce(tintOfferTriggerValueTypeRange,1) AS tintOfferTriggerValueTypeRange
			,coalesce(tintOfferTriggerValueType,1) AS tintOfferTriggerValueType
			,coalesce(fltOfferTriggerValue,0) AS fltOfferTriggerValue
			,coalesce(fltOfferTriggerValueRange,0) AS fltOfferTriggerValueRange
			,(CASE
      WHEN coalesce(tintOfferTriggerValueType,1) = 1 --Based on quantity
      THEN
         CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE WHEN coalesce(v_numUnitHour,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DOUBLE PRECISION AND coalesce(fltOfferTriggerValueRange,0):: DOUBLE PRECISION THEN 1 ELSE 0 END)
         ELSE(CASE WHEN coalesce(v_numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         END
      WHEN coalesce(tintOfferTriggerValueType,1) = 2 --Based on amount
      THEN
         CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE WHEN coalesce(v_monTotalAmount,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DECIMAL(20,5) AND coalesce(fltOfferTriggerValueRange,0):: DECIMAL(20,5) THEN 1 ELSE 0 END)
         ELSE(CASE WHEN coalesce(v_monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         END
      END) AS canUsePromotion
			,CONCAT((CASE
      WHEN coalesce(tintOfferTriggerValueType,1) = 1 --Based on quantity
      THEN(CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE
            WHEN coalesce(v_numUnitHour,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DOUBLE PRECISION AND coalesce(fltOfferTriggerValueRange,0):: DOUBLE PRECISION
            THEN GetPromotionDiscountDescription(numProId,v_vcCurrency)
            ELSE CONCAT('Increase qty to between ',coalesce(fltOfferTriggerValue,0),' & ',coalesce(fltOfferTriggerValueRange,0),
               ' to use promotion')
            END)
         ELSE(CASE
            WHEN coalesce(v_numUnitHour,0) >= fltOfferTriggerValue
            THEN GetPromotionDiscountDescription(numProId,v_vcCurrency)
            ELSE CONCAT('Increase qty to ',coalesce(fltOfferTriggerValue,0),' to use promotion')
            END)
         END)
      WHEN coalesce(tintOfferTriggerValueType,1) = 2 --Based on amount
      THEN(CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE
            WHEN coalesce(v_monTotalAmount,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DECIMAL(20,5) AND coalesce(fltOfferTriggerValueRange,0):: DECIMAL(20,5)
            THEN GetPromotionDiscountDescription(numProId,v_vcCurrency)
            ELSE CONCAT('Increase amount to between ',v_vcCurrency,coalesce(fltOfferTriggerValue,0),
               ' & ',v_vcCurrency,coalesce(fltOfferTriggerValueRange,0),' to use promotion')
            END)
         ELSE(CASE
            WHEN coalesce(v_monTotalAmount,0) >= fltOfferTriggerValue
            THEN GetPromotionDiscountDescription(numProId,v_vcCurrency)
            ELSE CONCAT('Increase amount to ',v_vcCurrency,coalesce(fltOfferTriggerValue,0),' to use promotion')
            END)
         END)
      END),(CASE WHEN coalesce(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcPromotionStatus
			,tintOfferBasedOn
			,(CASE WHEN coalesce(numOrderPromotionID,0) > 0 THEN true ELSE false END) AS bitRequireCouponCode
      FROM
      PromotionOffer PO
      WHERE
      numDomainId = v_numDomainID
      AND numProId = v_numPromotionID LIMIT 1;
   ELSE
      select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile FROM
      DivisionMaster D
      JOIN
      CompanyInfo C
      ON
      C.numCompanyId = D.numCompanyID WHERE
      numDivisionID = v_numDivisionID;
      select   numItemClassification INTO v_numItemClassification FROM Item WHERE Item.numItemCode = v_numItemCode;
      open SWV_RefCur for
      SELECT 
      numProId
			,coalesce(vcShortDesc,'-') AS vcShortDesc
			,(CASE
      WHEN coalesce(tintOfferTriggerValueType,1) = 1 --Based on quantity
      THEN
         CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE WHEN coalesce(v_numUnitHour,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DOUBLE PRECISION AND coalesce(fltOfferTriggerValueRange,0):: DOUBLE PRECISION THEN 1 ELSE 0 END)
         ELSE(CASE WHEN coalesce(v_numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         END
      WHEN coalesce(tintOfferTriggerValueType,1) = 2 --Based on amount
      THEN
         CASE
         WHEN coalesce(tintOfferTriggerValueTypeRange,1) = 2
         THEN(CASE WHEN coalesce(v_monTotalAmount,0) BETWEEN coalesce(fltOfferTriggerValue,0):: DECIMAL(20,5) AND coalesce(fltOfferTriggerValueRange,0):: DECIMAL(20,5) THEN 1 ELSE 0 END)
         ELSE(CASE WHEN coalesce(v_monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
         END
      END) AS canUsePromotion
			,coalesce(bitRequireCouponCode,false) AS bitRequireCouponCode
      FROM
      PromotionOffer PO
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitEnabled,false) = true
      AND coalesce(IsOrderBasedPromotion,false) = false
      AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= dtValidFrom AND TIMEZONE('UTC',now()) <= dtValidTo THEN 1 ELSE 0 END) END)
      AND 1 =(CASE
      WHEN coalesce(numOrderPromotionID,0) > 0
      THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
      ELSE(CASE tintCustomersBasedOn
         WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionID) > 0 THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
         WHEN 3 THEN 1
         ELSE 0
         END)
      END)
      AND 1 =(CASE
      WHEN tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
      WHEN tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
      WHEN tintOfferBasedOn = 4 THEN 1
      ELSE 0
      END)
      ORDER BY(CASE
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
      END) ASC,coalesce(PO.fltOfferTriggerValue,0) DESC LIMIT 1;
   end if;
   RETURN;
END; $$; 


