-- Stored procedure definition script usp_GetUsersWithDomains for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetUsersWithDomains(v_numUserID NUMERIC(18,0) DEFAULT 0,
 v_numDomainID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSMTPServer  VARCHAR(100);
   v_numSMTPPort  NUMERIC(18,0);
   v_bitSMTPSSL  BOOLEAN;
   v_bitSMTPAuth  BOOLEAN;
   v_vcIMAPServer  VARCHAR(100);
   v_numIMAPPort  NUMERIC(18,0);
   v_bitIMAPSSL  BOOLEAN;
   v_bitIMAPAuth  BOOLEAN;
BEGIN
   IF v_numUserID = 0 AND v_numDomainID > 0 then --Check if request is made while session is on
	
      open SWV_RefCur for
      SELECT
      numUserID
			,vcUserName
			,UserMaster.numGroupID
			,coalesce(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,coalesce(ADC.vcFirstName,'') || ' ' || coalesce(ADC.vcLastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,coalesce(UserMaster.SubscriptionId,'') AS SubscriptionId
			,coalesce(UserMaster.tintDefaultRemStatus,0) AS tintDefaultRemStatus
			,coalesce(UserMaster.numDefaultTaskType,0) AS numDefaultTaskType
			,coalesce(UserMaster.bitOutlook,false) AS bitOutlook
			,UserMaster.vcLinkedinId
			,coalesce(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
			,UserMaster.vcDashboardTemplateIDs
			,UserMaster.vcEmailAlias
      FROM
      UserMaster
      JOIN
      Domain
      ON
      UserMaster.numDomainID =  Domain.numDomainId
      LEFT JOIN
      AdditionalContactsInformation ADC
      ON
      ADC.numContactId = UserMaster.numUserDetailId
      LEFT JOIN
      AuthenticationGroupMaster GM
      ON
      GM.numGroupID = UserMaster.numGroupID
      WHERE
      UserMaster.numDomainID = v_numDomainID
      AND tintGroupType IN(1,4)
      ORDER BY
      Domain.numDomainId,vcUserDesc;
   ELSEIF v_numUserID = -1 AND v_numDomainID > 0
   then --Support Email
	
      open SWV_RefCur for
      SELECT
      '' AS vcEmailId
			,vcImapUserName
			,coalesce(bitUseUserName,false) AS bitUseUserName
			,coalesce(ImapUserDTL.bitImap,false) AS bitImap
			,ImapUserDTL.vcImapServerUrl
			,coalesce(ImapUserDTL.vcImapPassword,'') AS vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort
			,'' AS vcEmailAlias
      FROM
      ImapUserDetails ImapUserDTL
      WHERE
      ImapUserDTL.numUserCntID = v_numUserID
      AND ImapUserDTL.numDomainID = v_numDomainID;
   ELSE
      select   vcSMTPServer, numSMTPPort, bitSMTPSSL, bitSMTPAuth, vcIMAPServer, numIMAPPort, bitIMAPSSL, bitIMAPAuth INTO v_vcSMTPServer,v_numSMTPPort,v_bitSMTPSSL,v_bitSMTPAuth,v_vcIMAPServer,
      v_numIMAPPort,v_bitIMAPSSL,v_bitIMAPAuth FROM
      UniversalSMTPIMAP WHERE
      numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT
      U.numUserId
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,coalesce(monOverTimeRate,0) AS monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID = U.numUserDetailId) AS Roles
			,vcEmailid
			,coalesce(vcEmailAlias,'') AS vcEmailAlias
			,vcPassword
			,coalesce(ExcUserDTL.bitExchangeIntegration,false) AS bitExchangeIntegration
			,coalesce(ExcUserDTL.bitAccessExchange,false) AS bitAccessExchange
			,coalesce(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,coalesce(ExcUserDTL.vcExchPath,'') as vcExchPath
			,coalesce(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,coalesce(U.SubscriptionId,'') as SubscriptionId
			,coalesce(ImapUserDTL.bitImap,false) AS bitImap
			,coalesce(ImapUserDTL.vcImapServerUrl,coalesce(v_vcIMAPServer,'')) AS vcImapServerUrl
			,coalesce(ImapUserDTL.vcImapPassword,'') AS vcImapPassword
			,coalesce(ImapUserDTL.bitSSl,coalesce(v_bitIMAPSSL,false)) AS bitSSl
			,coalesce(ImapUserDTL.numPort,coalesce(v_numIMAPPort,0)) AS numPort
			,coalesce(ImapUserDTL.bitUseUserName,coalesce(v_bitIMAPAuth,false)) AS bitUseUserName
			,(CASE WHEN coalesce(vcSMTPServer,'') <> '' AND coalesce(numSMTPPort,0) <> 0 THEN coalesce(bitSMTPAuth,false) ELSE coalesce(v_bitSMTPAuth,false) END) AS bitSMTPAuth
			,coalesce(vcSmtpPassword,'') AS vcSmtpPassword
			,(CASE WHEN coalesce(vcSMTPServer,'') = '' THEN coalesce(v_vcSMTPServer,'') ELSE vcSMTPServer END) AS vcSMTPServer
			,(CASE WHEN coalesce(numSMTPPort,0) = 0 THEN coalesce(v_numSMTPPort,0) ELSE numSMTPPort END) AS numSMTPPort
			,(CASE WHEN coalesce(vcSMTPServer,'') <> '' AND coalesce(numSMTPPort,0) <> 0 THEN coalesce(bitSMTPSSL,false) ELSE coalesce(v_bitSMTPSSL,false) END) AS bitSMTPSSL
			,coalesce(bitSMTPServer,false) AS bitSMTPServer
			,coalesce(U.tintDefaultRemStatus,0) AS tintDefaultRemStatus
			,coalesce(U.numDefaultTaskType,0) AS numDefaultTaskType
			,coalesce(U.bitOutlook,false) AS bitOutlook
			,coalesce(numDefaultClass,0) AS numDefaultClass
			,coalesce(numDefaultWarehouse,0) AS numDefaultWarehouse
			,U.vcLinkedinId
			,coalesce(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
			,U.vcDashboardTemplateIDs
			,coalesce(bitPayroll,false) AS bitPayroll
			,coalesce(monHourlyRate,0) AS monHourlyRate
			,coalesce(tintPayrollType,1) AS tintPayrollType
			,coalesce(tintHourType,1) AS tintHourType
			,coalesce(U.tintMailProvider,1) AS tintMailProvider
			,coalesce(U.vcMailAccessToken,'') AS vcMailAccessToken
			,coalesce(U.vcMailRefreshToken,'') AS vcMailRefreshToken
			,coalesce(U.bitOauthImap,false) AS bitOauthImap
      FROM
      UserMaster U
      JOIN
      Domain D
      ON
      U.numDomainID =  D.numDomainId
      LEFT JOIN
      ExchangeUserDetails ExcUserDTL
      ON
      ExcUserDTL.numUserID = U.numUserId
      LEFT JOIN
      ImapUserDetails ImapUserDTL
      ON
      ImapUserDTL.numUserCntID = U.numUserDetailId
      AND ImapUserDTL.numDomainID = D.numDomainId
      WHERE
      U.numUserId = v_numUserID;
   end if;
   RETURN;
END; $$;



