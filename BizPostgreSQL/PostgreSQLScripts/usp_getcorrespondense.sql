-- Stored procedure definition script USP_GetCorrespondense for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCorrespondense(v_numCorrespondenseID NUMERIC,
	v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   open SWV_RefCur for SELECT cast(numCorrespondenseID as VARCHAR(255)),
	cast(numCommID as VARCHAR(255)),
	cast(numEmailHistoryID as VARCHAR(255)),
	cast(tintCorrType as VARCHAR(255)),
	cast(numDomainID as VARCHAR(255))
   FROM Correspondense
   WHERE numCorrespondenseID = v_numCorrespondenseID;
END; $$;












