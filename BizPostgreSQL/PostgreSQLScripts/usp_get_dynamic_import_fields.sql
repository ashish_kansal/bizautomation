-- Stored procedure definition script USP_GET_DYNAMIC_IMPORT_FIELDS for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE OR REPLACE FUNCTION USP_GET_DYNAMIC_IMPORT_FIELDS(v_numFormID BIGINT,
      v_intMode INTEGER,
      v_numImportFileID NUMERIC DEFAULT 0,
      v_numDomainID NUMERIC DEFAULT 0,
	  v_tintInsertUpdateType SMALLINT DEFAULT 0,
	  v_tintItemLinkingID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intMode = 1 then -- TO BIND CATEGORY DROP DOWN
    
      open SWV_RefCur for
      SELECT 20 AS intImportMasterID,'Item' AS vcImportName
      UNION ALL
      SELECT  48 AS intImportMasterID,'Item WareHouse' AS vcImportName
      UNION ALL
      SELECT  31 AS intImportMasterID,'Kit sub-items' AS vcImportName
      UNION ALL
      SELECT  54 AS intImportMasterID,'Item Images' AS vcImportName
      UNION ALL
      SELECT  55 AS intImportMasterID,'Item Vendor' AS vcImportName
      UNION ALL
      SELECT  133 AS intImportMasterID,'Organization' AS vcImportName
      UNION ALL
      SELECT  134 AS intImportMasterID,'Contact' AS vcImportName
      UNION ALL
      SELECT  43 AS intImportMasterID,'Organization Correspondence' AS vcImportName
      UNION ALL
      SELECT  98 AS intImportMasterID,'Address Details' AS vcImportName
      UNION ALL
      SELECT  136 AS intImportMasterID,'Import Sales Order(s)' AS vcImportName
      UNION ALL
      SELECT  137 AS intImportMasterID,'Import Purchase Order(s)' AS vcImportName
      UNION ALL
      SELECT  140 AS intImportMasterID,'Import Accounting Entries' AS vcImportName;
   end if;
	
   IF v_intMode = 2 then -- TO GET DETAILS OF DYNAMIC FORM FIELDS
    
      open SWV_RefCur for
      SELECT DISTINCT
      intSectionID AS Id,
            vcSectionName AS Data
      FROM
      DycFormSectionDetail
      WHERE
      numFormID = v_numFormID;

      open SWV_RefCur2 for
      SELECT
      ROW_NUMBER() OVER(ORDER BY X.intSectionID,X.numFormFieldID) AS SrNo
			,X.* FROM(SELECT
         coalesce((SELECT
            intImportTransactionID
            FROM
            Import_File_Field_Mapping FFM
            INNER JOIN
            Import_File_Master IFM
            ON
            IFM.intImportFileID = FFM.intImportFileID
            AND IFM.intImportFileID = v_numImportFileID
            WHERE
            FFM.numFormFieldID = DCOL.numFieldID),0) AS intImportFieldID
				,DCOL.numFieldID AS numFormFieldID
				,'' AS vcSectionNames
				,DCOL.vcFieldName AS vcFormFieldName
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS intSectionID
				,0 AS IsCustomField
				,coalesce((SELECT
            intMapColumnNo
            FROM
            Import_File_Field_Mapping FFM
            WHERE
            1 = 1
            AND FFM.numFormFieldID = DCOL.numFieldID
            AND FFM.intImportFileID = v_numImportFileID),
         0) AS intMapColumnNo
				,DCOL.vcPropertyName
				,CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END AS vcReqString
				,DCOL.bitRequired AS bitRequired
				,CAST(DCOL.numFieldID AS VARCHAR(30)) || '~' || CAST(0 AS VARCHAR(10)) AS keyFieldID
				,DCOL.tintOrder
         FROM
         View_DynamicDefaultColumns DCOL
         WHERE
         DCOL.numDomainID = v_numDomainID
         AND DCOL.numFormId = v_numFormID
         AND coalesce(DCOL.bitImport,false) = true
         AND coalesce(DCOL.bitDeleted,false) = false
         AND 1 =(CASE
         WHEN v_numFormID = 20 THEN(CASE WHEN DCOL.numFieldID IN(189,294,271,272,270) THEN 0 ELSE 1 END)
         ELSE 1
         END)
         UNION ALL
         SELECT
         -1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,CFm.FLd_label
				,CFm.FLd_label
				,DFSD.intSectionID
				,1
				,coalesce((SELECT
            intMapColumnNo
            FROM
            Import_File_Field_Mapping IIFM
            LEFT JOIN
            DycFormField_Mapping DMAP
            ON
            IIFM.numFormFieldID = DMAP.numFieldID
            WHERE
            IIFM.intImportFileID = v_numImportFileID
            AND IIFM.numFormFieldID = CFm.Fld_id
            AND bitCustomField = true
            AND coalesce(DMAP.bitImport,false) = true),0) AS intMapColumnNo
				,'' AS vcPropertyName
				,'' AS vcReqString
				,false AS bitRequired
				,CAST(CFm.Fld_id AS VARCHAR(30)) || '~' || CAST(1 AS VARCHAR(10)) AS keyFieldID
				,100 AS tintOrder
         FROM
         CFW_Fld_Master CFm
         LEFT JOIN CFW_Loc_Master LOC ON CFm.Grp_id = LOC.Loc_id
         LEFT JOIN DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
         WHERE
         Grp_id = DFSD.Loc_id
         AND numDomainID = v_numDomainID
         AND (DFSD.numFormID = v_numFormID OR DFSD.numFormID =(CASE WHEN v_numFormID = 20 THEN 48 ELSE v_numFormID END))) X
      ORDER BY
      X.tintOrder,X.intSectionID;

      open SWV_RefCur3 for
      SELECT
      ROW_NUMBER() OVER(ORDER BY X.intSectionID,X.numFormFieldID) AS SrNo
			,X.* FROM(SELECT
         coalesce((SELECT
            intImportTransactionID
            FROM
            Import_File_Field_Mapping FFM
            INNER JOIN
            Import_File_Master IFM
            ON
            IFM.intImportFileID = FFM.intImportFileID
            AND IFM.intImportFileID = v_numImportFileID
            WHERE
            FFM.numFormFieldID = DCOL.numFieldID),0) AS intImportFieldID
				,DCOL.numFieldID AS numFormFieldID
				,'' AS vcSectionNames
				,DCOL.vcFieldName AS vcFormFieldName
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS intSectionID
				,0 AS IsCustomField
				,coalesce((SELECT
            intMapColumnNo
            FROM
            Import_File_Field_Mapping FFM
            WHERE
            1 = 1
            AND FFM.numFormFieldID = DCOL.numFieldID
            AND FFM.intImportFileID = v_numImportFileID),
         0) AS intMapColumnNo
				,DCOL.vcPropertyName
				,CASE
         WHEN v_numFormID = 20
         THEN CASE
            WHEN DCOL.numFieldID IN(189,294,271,272,270,(CASE v_tintItemLinkingID
            WHEN 1 THEN 211
            WHEN 2 THEN 281
            WHEN 3 THEN 203
            WHEN 4 THEN 193
            WHEN 7 THEN 189
            ELSE 0
            END)) THEN '*' ELSE(CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END) END
         WHEN v_numFormID = 133
         THEN CASE
            WHEN DCOL.numFieldID IN(3,6,380,451,(CASE v_tintItemLinkingID
            WHEN 1 THEN 380
            WHEN 2 THEN 539
            ELSE 0
            END)) THEN '*' ELSE(CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END) END
         WHEN v_numFormID = 134
         THEN CASE
            WHEN DCOL.numFieldID IN(51,52,386,(CASE v_tintItemLinkingID
            WHEN 3 THEN 380
            WHEN 4 THEN 539
            ELSE 0
            END)) THEN '*' ELSE(CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END) END
         WHEN v_numFormID IN(136,137)
         THEN CASE WHEN DCOL.numFieldID IN(258,259,293,coalesce(v_tintItemLinkingID,0)) THEN '*' ELSE(CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END) END
         ELSE(CASE WHEN coalesce(DCOL.bitRequired,false) = false THEN '' ELSE '*' END)
         END AS vcReqString
				,CASE
         WHEN v_numFormID = 20
         THEN CASE
            WHEN DCOL.numFieldID IN(189,294,271,272,270,(CASE v_tintItemLinkingID
            WHEN 1 THEN 211
            WHEN 2 THEN 281
            WHEN 3 THEN 203
            WHEN 4 THEN 193
            WHEN 7 THEN 189
            ELSE 0
            END)) THEN true ELSE coalesce(DCOL.bitRequired,false) END
         WHEN v_numFormID = 133
         THEN CASE
            WHEN DCOL.numFieldID IN(3,6,380,451,(CASE v_tintItemLinkingID
            WHEN 1 THEN 380
            WHEN 2 THEN 539
            ELSE 0
            END)) THEN true ELSE coalesce(DCOL.bitRequired,false) END
         WHEN v_numFormID = 134
         THEN CASE
            WHEN DCOL.numFieldID IN(51,52,386,(CASE v_tintItemLinkingID
            WHEN 3 THEN 380
            WHEN 4 THEN 539
            ELSE 0
            END)) THEN true ELSE coalesce(DCOL.bitRequired,false) END
         WHEN v_numFormID IN(136,137)
         THEN CASE WHEN DCOL.numFieldID IN(258,259,293,coalesce(v_tintItemLinkingID,0)) THEN true ELSE coalesce(DCOL.bitRequired,false) END
         ELSE DCOL.bitRequired
         END AS bitRequired
				,CAST(DCOL.numFieldID AS VARCHAR(30)) || '~' || CAST(0 AS VARCHAR(10)) AS keyFieldID
				,coalesce(DCOL.tintColumn,0) AS SortOrder
				,coalesce(DCOL.tintRow,0) AS tintRow
         FROM
         View_DynamicColumns DCOL
         WHERE
         DCOL.numDomainID = v_numDomainID
         AND DCOL.numFormId = v_numFormID
         AND coalesce(DCOL.bitImport,false) = true
         AND coalesce(tintPageType,0) = 4
         UNION ALL
         SELECT
         coalesce((SELECT
            intImportTransactionID
            FROM
            Import_File_Field_Mapping FFM
            INNER JOIN
            Import_File_Master IFM
            ON
            IFM.intImportFileID = FFM.intImportFileID
            AND IFM.intImportFileID = v_numImportFileID
            WHERE
            FFM.numFormFieldID = DCOL.numFieldID),0) AS intImportFieldID
				,DCOL.numFieldID AS numFormFieldID
				,'' AS vcSectionNames
				,DCOL.vcFieldName AS vcFormFieldName
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS intSectionID
				,0 AS IsCustomField
				,coalesce((SELECT
            intMapColumnNo
            FROM
            Import_File_Field_Mapping FFM
            WHERE
            1 = 1
            AND FFM.numFormFieldID = DCOL.numFieldID
            AND FFM.intImportFileID = v_numImportFileID),
         0) AS intMapColumnNo
				,DCOL.vcPropertyName
				,CAST('*' AS VARCHAR(1)) AS vcReqString
				,true AS bitRequired
				,CAST(DCOL.numFieldID AS VARCHAR(30)) || '~' || CAST(0 AS VARCHAR(10)) AS keyFieldID
				,0 AS SortOrder
				,DCOL.tintOrder
         FROM
         View_DynamicDefaultColumns DCOL
         WHERE
         DCOL.numDomainID = v_numDomainID
         AND DCOL.numFormId = v_numFormID
         AND coalesce(DCOL.bitImport,false) = true
         AND coalesce(DCOL.bitDeleted,false) = false
         AND DCOL.numFieldID NOT IN(SELECT
            VDC.numFieldID
            FROM
            View_DynamicColumns VDC
            WHERE
            VDC.numDomainID = v_numDomainID
            AND VDC.numFormId = v_numFormID
            AND coalesce(VDC.bitImport,false) = true
            AND coalesce(VDC.tintPageType,0) = 4)
         AND v_tintInsertUpdateType IN(2,3)
         AND 1 =(CASE
         WHEN v_numFormID = 20
         THEN(CASE
            WHEN DCOL.numFieldID IN(189,294,271,272,270,(CASE v_tintItemLinkingID
            WHEN 1 THEN 211
            WHEN 2 THEN 281
            WHEN 3 THEN 203
            WHEN 4 THEN 193
            WHEN 7 THEN 189
            ELSE 0
            END))
            THEN 1
            ELSE 0
            END)
         WHEN v_numFormID = 133
         THEN(CASE
            WHEN DCOL.numFieldID IN(3,6,380,451,(CASE v_tintItemLinkingID
            WHEN 1 THEN 380
            WHEN 2 THEN 539
            ELSE 0
            END))
            THEN 1
            ELSE 0
            END)
         WHEN v_numFormID = 134
         THEN(CASE
            WHEN DCOL.numFieldID IN(51,52,386,(CASE v_tintItemLinkingID
            WHEN 3 THEN 380
            WHEN 4 THEN 539
            ELSE 0
            END))
            THEN 1
            ELSE 0
            END)
         WHEN v_numFormID IN(136,137)
         THEN(CASE WHEN DCOL.numFieldID IN(258,259,293,coalesce(v_tintItemLinkingID,0)) THEN 1 ELSE 0 END)
         WHEN v_numFormID IN(140)
         THEN(CASE WHEN DCOL.numFieldID IN(931,932,933,934) THEN 1 ELSE 0 END)
         ELSE 0
         END)
         UNION ALL
         SELECT
         -1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,CFm.FLd_label
				,CFm.FLd_label
				,DFSD.intSectionID
				,1
				,coalesce((SELECT
            intMapColumnNo
            FROM
            Import_File_Field_Mapping IIFM
            LEFT JOIN
            DycFormField_Mapping DMAP
            ON
            IIFM.numFormFieldID = DMAP.numFieldID
            WHERE
            IIFM.intImportFileID = v_numImportFileID
            AND IIFM.numFormFieldID = CFm.Fld_id
            AND bitCustomField = true
            AND coalesce(DMAP.bitImport,false) = true),0) AS intMapColumnNo
				,'' AS vcPropertyName
				,CAST('' AS VARCHAR(1)) AS vcReqString
				,false AS bitRequired
				,CAST(CFm.Fld_id AS VARCHAR(30)) || '~' || CAST(1 AS VARCHAR(10)) AS keyFieldID
				,DFCD.intColumnNum AS SortOrder
				,coalesce(DFCD.intRowNum,0) AS tintRow
         FROM
         CFW_Fld_Master CFm
         LEFT JOIN CFW_Loc_Master LOC ON CFm.Grp_id = LOC.Loc_id
         LEFT JOIN DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
         JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFm.Fld_id AND DFCD.numFormID = v_numFormID AND DFCD.bitCustom = true
         WHERE
         Grp_id = DFSD.Loc_id
         AND DFCD.numDomainID = v_numDomainID
         AND (DFSD.numFormID = v_numFormID OR DFSD.numFormID =(CASE WHEN v_numFormID = 20 THEN 48 ELSE v_numFormID END))
         AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)) X
      ORDER BY
      X.SortOrder,X.intSectionID;
   end if;
   RETURN;
END; $$;


