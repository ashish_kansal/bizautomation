-- Stored procedure definition script Usp_GetDuplicateFieldName for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetDuplicateFieldName(v_numFormId   NUMERIC(9,0)  DEFAULT 24,
                v_numDomainId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(vcFieldName as VARCHAR(255)) as vcformfieldname,
           cast(vcDbColumnName as VARCHAR(255)),
           cast(vcLookBackTableName as VARCHAR(255)),
           cast(numFieldID as VARCHAR(255)) as numformfieldid
   from View_DynamicColumns
   where numFormId = v_numFormId and numDomainID = v_numDomainId AND tintPageType = 1
   AND coalesce(bitSettingField,false) = true;
END; $$;












