-- Stored procedure definition script USP_OpportunityItemsReleaseDates_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItemsReleaseDates_Save(v_ID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_dtReleaseDate DATE,
	v_numQuantity NUMERIC(18,0),
	v_tintReleaseStatus SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   OpportunityItems
   SET
   ItemReleaseDate = v_dtReleaseDate
   WHERE
   numOppId = v_numOppID
   AND numoppitemtCode = v_numOppItemID;
   RETURN;
END; $$;



