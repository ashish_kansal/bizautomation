-- Stored procedure definition script USP_GetWebTrafficDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetWebTrafficDetails(v_numDivisionID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
		
   coalesce(vcSearchTerm,'-') AS vcSearchTerm
		,(CASE WHEN vcOrginalRef = '-' THEN coalesce(vcOrginalURL,'-') ELSE coalesce(vcOrginalRef,'-') END) AS vcOrginalRef
		,vcUserHostAddress
		,CAST(dtcreated AS VARCHAR(20)) AS StartDate
		,CAST((SELECT  dtcreated FROM TrackingVisitorsHDR WHERE numDivisionID = v_numDivisionID ORDER BY numTrackingID DESC LIMIT 1) AS VARCHAR(20)) AS EndDate
		,coalesce(vcCountry,'') AS vcLocation
		,coalesce((SELECT  vcPageName FROM TrackingVisitorsDTL WHERE  numTracVisitorsHDRID IN(SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDivisionID = v_numDivisionID) ORDER by numTracVisitorsDTLID DESC LIMIT 1),
   '') AS vcLastPageVisited
   FROM
   TrackingVisitorsHDR TVHDR
   WHERE
   numDivisionID = v_numDivisionID
   ORDER BY
   numTrackingID ASC LIMIT 1;  
  
  
   open SWV_RefCur2 for
   SELECT 
   numTrackingID
		,(SELECT COUNT(*) FROM TrackingVisitorsDTL WHERE numTracVisitorsHDRID = TrackingVisitorsHDR.numTrackingID) AS NoOfTimes
		,CAST(vcTotalTime AS VARCHAR(8)) AS vcTotalTime
		,CAST(dtcreated AS VARCHAR(20)) AS dtCreated
   FROM
   TrackingVisitorsHDR
   WHERE
   numDivisionID = v_numDivisionID
   ORDER BY
   dtcreated ASC LIMIT 10;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/



