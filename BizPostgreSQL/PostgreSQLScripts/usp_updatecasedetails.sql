-- Stored procedure definition script usp_UpdateCaseDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateCaseDetails(v_intTargetResolveDate INTEGER DEFAULT 0,  
 v_textSubject TEXT DEFAULT '',  
 v_vcStatus NUMERIC DEFAULT NULL,  
 v_vcPriority VARCHAR(25) DEFAULT '',  
 v_textDesc TEXT DEFAULT '',  
 v_textInternalComments TEXT DEFAULT '',  
 v_vcReason VARCHAR(250) DEFAULT '',  
 v_vcOrigin VARCHAR(50) DEFAULT '',  
 v_vcType VARCHAR(50) DEFAULT '',  
 v_vcAssignTo VARCHAR(50) DEFAULT '',  
 v_numCaseID NUMERIC(9,0) DEFAULT 0,  
 v_numModifiedBy NUMERIC(9,0) DEFAULT 0,  
 v_bintModifiedDate BIGINT DEFAULT 0,  
 v_vcClosedDate VARCHAR(10) DEFAULT '',  
 v_numSolnID NUMERIC DEFAULT NULL  
 --@txtSolution varchar(150)=''    
--
)
RETURNS VOID LANGUAGE plpgsql  
--Insertion into Table    
	
   AS $$
   DECLARE
   v_numID  NUMERIC;
BEGIN
   if (v_vcClosedDate = '') then
      Update Cases set intTargetResolveDate = v_intTargetResolveDate,textSubject = v_textSubject,
      numStatus = v_vcStatus,numPriority = v_vcPriority,textDesc = v_textDesc,textInternalComments = v_textInternalComments,
      numReason = v_vcReason,numOrigin = v_vcOrigin,
      numType = v_vcType,numAssignedTo = v_vcAssignTo,numModifiedBy = v_numModifiedBy,
      bintModifiedDate = v_bintModifiedDate
      WHERE numCaseId = v_numCaseID;
   else
      Update Cases set intTargetResolveDate = v_intTargetResolveDate,textSubject = v_textSubject,
      numStatus = v_vcStatus,numPriority = v_vcPriority,textDesc = v_textDesc,textInternalComments = v_textInternalComments,
      numReason = v_vcReason,numOrigin = v_vcOrigin,
      numType = v_vcType,numAssignedTo = v_vcAssignTo,numModifiedBy = v_numModifiedBy,
      bintModifiedDate = v_bintModifiedDate
      WHERE numCaseId = v_numCaseID;
   end if;  
		
   if v_numSolnID = 0 then
      Delete from CaseSolutions where numCaseId = v_numCaseID;
   else
      NULL;
   end if;  
   v_numID := 0;  
   select   numSolnID INTO v_numID from CaseSolutions where numCaseId = v_numCaseID;  
   RAISE NOTICE '%',v_numID;  
   If v_numID = 0 then
      Insert into CaseSolutions(numCaseId,numSolnID) values(v_numCaseID,v_numSolnID);
   Else
      Update CaseSolutions set numSolnID = v_numSolnID where numCaseId = v_numCaseID;
   end if;
   RETURN;
END; $$;


