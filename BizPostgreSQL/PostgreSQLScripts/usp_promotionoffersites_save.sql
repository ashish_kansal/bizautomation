-- Stored procedure definition script USP_PromotionOfferSites_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromotionOfferSites_Save(v_numProId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_vcSelectedSites TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numSiteID NUMERIC(18,0)
   );
   INSERT INTO
   tt_TEMP
   SELECT
   Id
   FROM
   SplitIDs(v_vcSelectedSites,',');

	-- FIRST DELETE THE SITES WHICH ARE REMOVED FROM SELECTION
   DELETE FROM PromotionOfferSites WHERE numPromotionID = v_numProId AND numSiteID NOT IN(SELECT ID FROM tt_TEMP);

	-- ADD NEW SITES
   INSERT INTO PromotionOfferSites(numPromotionID,numSiteID) SELECT v_numProId,numSiteID FROM tt_TEMP WHERE numSiteID NOT IN(SELECT coalesce(numSiteID,0) FROM PromotionOfferSites WHERE numPromotionID = v_numProId);
RETURN;
END; $$; 


