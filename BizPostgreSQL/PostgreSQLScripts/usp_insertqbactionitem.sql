-- Stored procedure definition script usp_InsertQBActionItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertQBActionItem(v_vcSubject VARCHAR(500),
	v_vcStartDate VARCHAR(10),
	v_vcCompanyName VARCHAR(255),
	v_numUserID NUMERIC(8,0),
	v_numDomainID NUMERIC(8,0),
	v_bitStatus BOOLEAN   
--

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into QBActionItem  values(v_vcSubject,v_vcStartDate,v_vcCompanyName,v_numUserID,v_numDomainID,v_bitStatus);
RETURN;
END; $$;


