-- Stored procedure definition script USP_MassPurchaseFulfillment_ReceiveOnlyItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_ReceiveOnlyItems(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_vcSelectedRecords TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numOppID  NUMERIC(18,0);
   v_numWOID  NUMERIC(18,0);
   v_numOppItemID  NUMERIC(18,0);
   v_numQtyToReceive  DOUBLE PRECISION;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         OppID NUMERIC(18,0),
         WOID NUMERIC(18,0),
         OppItemID NUMERIC(18,0),
         numQtyToReceive DOUBLE PRECISION,
         IsSuccess BOOLEAN,
         ErrorMessage VARCHAR(300)
      );

      INSERT INTO tt_TEMP(OppID
		,WOID
		,OppItemID
		,numQtyToReceive
		,IsSuccess)
      SELECT
      coalesce(OppID,0)
		,coalesce(WOID,0)
		,coalesce(OppItemID,0)
		,QtyToReceive
		,IsSuccess
      FROM
	  XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcSelectedRecords AS XML)
				COLUMNS
					id FOR ORDINALITY,
					OppID NUMERIC(18,0) PATH 'OppID',
					WOID NUMERIC(18,0) PATH 'WOID',
					OppItemID NUMERIC(18,0) PATH 'OppItemID',
					QtyToReceive DOUBLE PRECISION PATH 'QtyToReceive',
					IsSuccess BOOLEAN PATH 'IsSuccess'
			) AS X;

      select   COUNT(*) INTO v_iCount FROM tt_TEMP;
      WHILE v_i <= v_iCount LOOP
         select   OppID, WOID, coalesce(OppItemID,0), coalesce(numQtyToReceive,0) INTO v_numOppID,v_numWOID,v_numOppItemID,v_numQtyToReceive FROM
         tt_TEMP WHERE
         ID = v_i;
         IF v_numWOID	> 0 then
		
            IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID AND coalesce(numQtyBuilt,0) >=(coalesce(numQtyReceived,0)+v_numQtyToReceive)) then
			
               UPDATE
               WorkOrder
               SET
               numQtyReceived = coalesce(numQtyReceived,0)+v_numQtyToReceive
               WHERE
               numDomainId = v_numDomainID
               AND numWOId = v_numWOID;
               UPDATE
               tt_TEMP
               SET
               IsSuccess = true
               WHERE
               ID = v_i;
            ELSE
               UPDATE
               tt_TEMP
               SET
               IsSuccess = false,ErrorMessage = 'Received quantity can not be greater then quantity built'
               WHERE
               ID = v_i;
            end if;
         ELSEIF v_numOppItemID > 0
         then
		
            IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID AND numoppitemtCode = v_numOppItemID AND coalesce(numUnitHour,0) >=(coalesce(numQtyReceived,0)+v_numQtyToReceive)) then
			
               UPDATE
               OpportunityItems
               SET
               numQtyReceived = coalesce(numQtyReceived,0)+v_numQtyToReceive
               WHERE
               numOppId = v_numOppID
               AND numoppitemtCode = v_numOppItemID;
               UPDATE
               tt_TEMP
               SET
               IsSuccess = true
               WHERE
               ID = v_i;
            ELSE
               UPDATE
               tt_TEMP
               SET
               IsSuccess = false,ErrorMessage = 'Received quantity can not be greater than ordered quantity'
               WHERE
               ID = v_i;
            end if;
         ELSE
            UPDATE
            OpportunityItems
            SET
            numQtyReceived = numUnitHour
            WHERE
            numOppId = v_numOppID
            AND coalesce(OpportunityItems.bitDropShip,false) = false
            AND coalesce(numUnitHour,0) <> coalesce(numQtyReceived,0);
            UPDATE
            tt_TEMP
            SET
            IsSuccess = true
            WHERE
            ID = v_i;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      UPDATE
      OpportunityMaster OM
      SET
      bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID
      FROM
      tt_TEMP T1
      WHERE(OM.numOppId = T1.OppID
      AND T1.IsSuccess = true) AND OM.numDomainId = v_numDomainID;
      open SWV_RefCur for SELECT ID AS "ID",
         OppID AS "OppID",
         WOID AS "WOID",
         OppItemID AS "OppItemID",
         numQtyToReceive AS "numQtyToReceive",
         IsSuccess AS "IsSuccess",
         ErrorMessage AS "ErrorMessage" FROM tt_TEMP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;












