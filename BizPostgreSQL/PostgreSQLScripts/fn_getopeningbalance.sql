-- Function definition script fn_GetOpeningBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetOpeningBalance(v_vcChartAcntId VARCHAR(500),
v_numDomainId NUMERIC(9,0),
v_dtFromDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER  --Added by Chintan to enable calculation of date according to client machine
)
RETURNS table
(
   numAccountId INTEGER,
   vcAccountName VARCHAR(100),
   numParntAcntTypeID INTEGER,
   vcAccountDescription VARCHAR(100),
   vcAccountCode VARCHAR(50),
   dtAsOnDate TIMESTAMP,
   mnOpeningBalance DECIMAL(20,5)
) LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETOPENINGBALANCE_COAOPENINGBALANCE CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETOPENINGBALANCE_COAOPENINGBALANCE
   (
      numAccountId INTEGER,
      vcAccountName VARCHAR(100),
      numParntAcntTypeID INTEGER,
      vcAccountDescription VARCHAR(100),
      vcAccountCode VARCHAR(50),
      dtAsOnDate TIMESTAMP,
      mnOpeningBalance DECIMAL(20,5)
   );
   v_dtFromDate := v_dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);

   INSERT INTO
   tt_FN_GETOPENINGBALANCE_COAOPENINGBALANCE
   SELECT
   COA.numAccountId
		,COA.vcAccountName
		,COA.numParntAcntTypeID
		,COA.vcAccountDescription
		,COA.vcAccountCode
		,v_dtFromDate
		,(SELECT
      coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD
      JOIN
      Domain D
      ON
      D.numDomainId = GJD.numDomainId
      AND
      COA.numDomainId = D.numDomainId
      INNER JOIN
      General_Journal_Header GJH
      ON
      GJD.numJournalId = GJH.numJOurnal_Id
      AND GJD.numDomainId = COA.numDomainId
      AND GJD.numChartAcntId = COA.numAccountId
      AND GJH.numDomainId = D.numDomainId
      AND GJH.datEntry_Date <= v_dtFromDate)
   FROM
   Chart_Of_Accounts COA
   WHERE
   COA.numDomainId = v_numDomainId
   AND COA.numAccountId in(select CAST(ID AS NUMERIC(9,0)) from SplitIDs(v_vcChartAcntId,','));

	RETURN QUERY (SELECT * FROM tt_FN_GETOPENINGBALANCE_COAOPENINGBALANCE);
END; $$;

