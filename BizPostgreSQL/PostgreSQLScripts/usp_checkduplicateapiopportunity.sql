DROP FUNCTION IF EXISTS USP_CheckDuplicateAPIOpportunity;

CREATE OR REPLACE FUNCTION USP_CheckDuplicateAPIOpportunity(v_WebApiId    INTEGER,
                v_numDomainId NUMERIC(18,0),
                v_vcAPIOppId  VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COALESCE((SELECT COUNT(*)
   FROM   OpportunityMaster OM
   WHERE  OM.tintSourceType = 3
   AND OM.tintSource = v_WebApiId
   AND OM.numDomainId = v_numDomainId
   AND OM.vcMarketplaceOrderID = v_vcAPIOppId),0);
END; $$;
--Created by anoop jayaraj                                                          












