CREATE OR REPLACE FUNCTION Usp_GetScheduledContacts(v_numDomainId NUMERIC(9,0),  
v_numScheduleid NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select CRS.numcontactId as "numcontactId",vcEmail AS "vcEmail" from CustRptSchContacts CRS
   join AdditionalContactsInformation AC on   CRS.numcontactId =  AC.numContactId
   where
   numScheduleid = v_numScheduleid and
   AC.numDomainID = v_numDomainId;
END; $$;












