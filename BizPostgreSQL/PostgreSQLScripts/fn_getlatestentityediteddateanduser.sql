-- Function definition script fn_GetLatestEntityEditedDateAndUser for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetLatestEntityEditedDateAndUser(v_numDivisionId NUMERIC, v_numContactId NUMERIC, v_vcEntity CHAR(3))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intCreationDate  BIGINT;          
   v_intModificationDate  BIGINT;          
   v_numCreatedBy  NUMERIC;          
   v_numModifiedBy  NUMERIC;          
          
   v_dtCreationDate  TIMESTAMP;                          
   v_vcCreatedBy  VARCHAR(100);                          
   v_dtModificationDate  TIMESTAMP;                          
   v_vcModifiedBy  VARCHAR(100);                          
   v_vcLatestEntityEditedDateAndUser  VARCHAR(100);            
   v_numLastContactId  NUMERIC;
BEGIN
   IF v_numContactId = 0 then
 
      IF v_vcEntity = 'Org' then
                        
                --Organization Created Date (Actually the Division Creation Date is considered here)                          
         select   d.bintCreatedDate, fn_GetUserName(d.numCreatedBy), d.numModifiedBy INTO v_dtCreationDate,v_vcCreatedBy,v_vcModifiedBy FROM DivisionMaster d WHERE d.numDivisionID = v_numDivisionId;
         IF v_vcModifiedBy IS NOT NULL then
                   
            select   d.bintModifiedDate, fn_GetUserName(d.numModifiedBy) INTO v_dtModificationDate,v_vcModifiedBy FROM DivisionMaster d WHERE d.numDivisionID = v_numDivisionId;
            v_vcLatestEntityEditedDateAndUser := SUBSTR(Cast(v_dtModificationDate As VARCHAR(20)),1,20) || ' by ' || coalesce(v_vcModifiedBy,'');
         ELSE
            v_vcLatestEntityEditedDateAndUser := SUBSTR(Cast(v_dtCreationDate As VARCHAR(20)),1,20) || ' by ' || coalesce(v_vcCreatedBy,'');
         end if;
      end if;
   ELSEIF v_numDivisionId = 0
   then
 
      IF v_vcEntity = 'Cnt' then
                                 
            --Contact                         
         select   bintCreatedDate, fn_GetUserName(numCreatedBy), numModifiedBy INTO v_dtCreationDate,v_vcCreatedBy,v_vcModifiedBy FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
         IF v_vcModifiedBy IS NOT NULL then
            
            select   bintModifiedDate, fn_GetUserName(numModifiedBy) INTO v_dtModificationDate,v_vcModifiedBy FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
            v_vcLatestEntityEditedDateAndUser := SUBSTR(Cast(v_dtModificationDate As VARCHAR(20)),1,20) || ' by ' || coalesce(v_vcModifiedBy,'');
         ELSE
            v_vcLatestEntityEditedDateAndUser := SUBSTR(Cast(v_dtCreationDate As VARCHAR(20)),1,20) || ' by ' || coalesce(v_vcCreatedBy,'');
         end if;
      end if;
   end if;                      
   RETURN v_vcLatestEntityEditedDateAndUser;
END; $$;

