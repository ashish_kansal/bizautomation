-- Stored procedure definition script usp_getlistDataType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getlistDataType(v_numDomainID NUMERIC(9,0),
v_numListID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numListID as VARCHAR(255)),cast(vcListName as VARCHAR(255)),cast(vcDataType as VARCHAR(255)) from listmaster where (numDomainID = v_numDomainID or bitFlag = true) AND numListID = v_numListID;
END; $$;












