-- Function definition script fn_OppAndItemsCustomFieldsColumns for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_OppAndItemsCustomFieldsColumns(v_numLocationId SMALLINT, v_ReturnType SMALLINT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCustomOpportunityItemFieldList TEXT;
BEGIN
   IF v_numLocationId  = 2 then  --OPPORTUNITIES        
 
      IF v_ReturnType = 0 then --Column list along with their datatypes        
  
         select STRING_AGG('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' NVarchar(50)',',') INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 2;
      end if;
      IF v_ReturnType = 1 then --Column list        
  
         select STRING_AGG('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',') INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 2;
      end if;
      IF v_ReturnType = 2 then --Function list to populate the data        
  
         select STRING_AGG('(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN Opp' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(Opp' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value,''L'') ELSE Opp' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value   
     END FROM CFW_Fld_Master M, CFW_Fld_Values_Opp Opp' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' WHERE M.Fld_ID = ' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' AND recId = numOppId AND Opp' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Id = M.Fld_Id AND M.Grp_Id In (2,6))',',') INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 2;
      end if;
   ELSEIF v_numLocationId = 5
   then --ITEMS        
 
      IF v_ReturnType = 0 then --Column list along with their datatypes        
  
         select STRING_AGG('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' NVarchar(50)','') INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 5;
      end if;
      IF v_ReturnType = 1 then --Column list        
  
         select STRING_AGG('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',') INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 5;
      end if;
      IF v_ReturnType = 2 then --Function list to populate the data        
  
         select string_agg('(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN Item' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(Item' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value,''L'') ELSE Item' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value  
     END FROM CFW_Fld_Master M, CFW_Fld_Values_Item Item' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' WHERE M.Fld_ID = ' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' AND recId = vcItemCode AND Item' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Id = M.Fld_Id AND M.Grp_Id = 5)',',')
          INTO v_vcCustomOpportunityItemFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 5;
      end if;
   end if;        
   RETURN v_vcCustomOpportunityItemFieldList;
END; $$;

