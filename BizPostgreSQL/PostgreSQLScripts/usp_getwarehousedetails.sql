-- Stored procedure definition script usp_GetWarehouseDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetWarehouseDetails(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numWareHouseDetailID as "numWareHouseDetailID",
  fn_GetListName(numCountryID,false) as "country"
  ,case when numStateID = 0 then 'All' else fn_GetState(numStateID) end as "state"
      ,(select vcWareHouse  from Warehouses where numWareHouseID = Dtl.numWareHouse) as "WareHouse"
      ,numDomainID as "numDomainID",numFromPinCode as "numFromPinCode",  numToPinCode as "numToPinCode"
   FROM WareHouseDetails Dtl
   where numDomainID = v_numDomainId;
END; $$;

