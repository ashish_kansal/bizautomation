-- Function definition script fn_CheckIfItemUOMConversionExists for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CheckIfItemUOMConversionExists(v_numDomainID NUMERIC,
	v_numItemCode NUMERIC,
	v_numSourceUnit NUMERIC)
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitExists  BOOLEAN DEFAULT 0;
   v_numBaseUnit  NUMERIC(18,0) DEFAULT 0;
   v_intConversionsFount  INTEGER DEFAULT 0;
BEGIN
   select   coalesce(numBaseUnit,0) INTO v_numBaseUnit FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
	
   IF v_numSourceUnit = v_numBaseUnit then
      v_bitExists := true;
   ELSE
      with recursive CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS(SELECT
      IUOM.numSourceUOM AS FromUnit,
				CAST(0 AS INTEGER) AS LevelNum,
				IUOM.numSourceUOM AS numSourceUOM,
				IUOM.numTargetUOM AS numTargetUOM,
				IUOM.numTargetUnit AS numTargetUnit
      FROM
      ItemUOMConversion IUOM
      WHERE
      IUOM.numDomainID = v_numDomainID
      AND IUOM.numItemCode = v_numItemCode
      AND IUOM.numSourceUOM = v_numSourceUnit
      UNION ALL
      SELECT
      cte.FROMUnit AS FromUnit,
				LevelNum+1 AS LevelNum,
				IUOM.numSourceUOM AS numSourceUOM,
				IUOM.numTargetUOM AS numTargetUOM,
				(IUOM.numTargetUnit*cte.numTargetUnit) AS numTargetUnit
      FROM
      ItemUOMConversion IUOM
      INNER JOIN
      CTE cte
      ON
      IUOM.numSourceUOM = cte.numTargetUOM
      WHERE
      IUOM.numDomainID = v_numDomainID
      AND IUOM.numItemCode = v_numItemCode) select   COUNT(*) INTO v_intConversionsFount FROM CTE WHERE FROMUnit = v_numSourceUnit AND numTargetUOM = v_numBaseUnit;
      IF coalesce(v_intConversionsFount,0) > 0 then
		
         v_bitExists := true;
      end if;
   end if;

   RETURN v_bitExists;
END; $$;

