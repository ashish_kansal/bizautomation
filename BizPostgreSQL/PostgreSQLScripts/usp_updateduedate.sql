-- Stored procedure definition script USP_UpdateDueDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateDueDate(v_dtDueDate TIMESTAMP,
    v_numOppBizDocsId NUMERIC(9,0),
    v_numOppId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitBillingTerms  BOOLEAN;
   v_intBillingDays  INTEGER;
BEGIN

                --@intBillingDays = ISNULL(dbo.fn_GetListItemName(isnull(om.intBillingDays,0)),0)
   select   om.bitBillingTerms, coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(om.intBillingDays,0)),0) INTO v_bitBillingTerms,v_intBillingDays FROM    OpportunityBizDocs BD JOIN OpportunityMaster om ON BD.numoppid = om.numOppId WHERE   numOppBizDocsId = v_numOppBizDocsId AND om.numOppId = v_numOppId;
        
        
   UPDATE  OpportunityBizDocs
   SET     dtFromDate = v_dtDueDate+CAST(-v_intBillingDays || 'day' as interval)
   WHERE   numOppBizDocsId = v_numOppBizDocsId AND numoppid = v_numOppId;
        
        
   UPDATE General_Journal_Header GJH SET datEntry_Date = CAST(TO_CHAR(OBD.dtFromDate,'yyyy/mm/dd') AS TIMESTAMP)
   FROM OpportunityBizDocs OBD WHERE(OBD.numoppid = GJH.numOppId
   AND OBD.numOppBizDocsId = GJH.numOppBizDocsId) AND(OBD.numoppid = v_numOppId AND OBD.numOppBizDocsId = v_numOppBizDocsId
   AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numPayrollDetailID,0) = 0
   AND coalesce(GJH.numCheckHeaderID,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
   AND coalesce(GJH.numDepositId,0) = 0);
   RETURN;
END; $$;
---Created by anoop Jayaraj



