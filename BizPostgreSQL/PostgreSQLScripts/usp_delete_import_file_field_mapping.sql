-- Stored procedure definition script USP_DELETE_IMPORT_FILE_FIELD_MAPPING for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DELETE_IMPORT_FILE_FIELD_MAPPING(v_numImportFileID			BIGINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM Import_File_Field_Mapping WHERE intImportFileID = v_numImportFileID;
   RETURN;
END; $$;


