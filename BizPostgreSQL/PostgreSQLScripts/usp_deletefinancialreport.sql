-- Stored procedure definition script USP_DeleteFinancialReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteFinancialReport(v_numFRID NUMERIC(18,0),
v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM FinancialReportDetail WHERE numFRID = v_numFRID; 
   DELETE  FROM FinancialReport
   WHERE   numFRID = v_numFRID AND numDomainID = v_numDomainID;
   RETURN;
END; $$; 


