-- Stored procedure definition script USP_GET_SHIPPING_LABEL_RULE_CHILD for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_SHIPPING_LABEL_RULE_CHILD
(
	v_numShippingRuleID NUMERIC(18,0),
    v_numDomainID NUMERIC(18,0),
	INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
	SELECT DISTINCT 
		numChildShipID
		,SLRM.numShippingRuleID
		,numFromValue AS FromValue
		,numToValue AS ToValue
		,numDomesticShipID AS Domestic
		,numInternationalShipID AS International
		,SLRC.numPackageTypeID AS PackageType
		,CP.numShippingCompanyID
		,CP.vcPackageName
	FROM ShippingLabelRuleMaster SLRM
	INNER JOIN ShippingLabelRuleChild SLRC ON SLRM.numShippingRuleID = SLRC.numShippingRuleID
	INNER JOIN ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SLRC.numDomesticShipID AND SLRM.numDomainID = SST1.numDomainID
	INNER JOIN ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SLRC.numInternationalShipID AND SLRM.numDomainID = SST2.numDomainID
	INNER JOIN CustomPackages CP ON SLRC.numPackageTypeID = CP.numCustomPackageID
	WHERE 
		SLRM.numDomainID =  v_numDomainID
		AND SLRM.numShippingRuleID = v_numShippingRuleID;
END; $$;
			













