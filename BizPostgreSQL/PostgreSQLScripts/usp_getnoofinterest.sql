-- Stored procedure definition script usp_GetNoOfInterest for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetNoOfInterest(v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		vcInterested1,
		vcInterested2,
		vcInterested3,
		vcInterested4,
		vcInterested5,
		vcInterested6,
		vcInterested7,
		vcInterested8
	FROM
		LeadBox
	WHERE 
		numUserID = v_numCreatedBy
		and numDomainID = v_numDomainID;
END; $$;












