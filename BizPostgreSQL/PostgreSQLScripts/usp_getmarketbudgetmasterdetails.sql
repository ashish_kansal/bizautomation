-- Stored procedure definition script USP_GetMarketBudgetMasterDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMarketBudgetMasterDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,          
v_intFiscalYear NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numMarketBudgetId,bitMarketCampaign From MarketBudgetMaster Where  numDomainID = v_numDomainId;  -- intFiscalYear=@intFiscalYear And     
END; $$;
