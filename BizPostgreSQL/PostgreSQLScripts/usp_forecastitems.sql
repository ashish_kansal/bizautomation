-- Stored procedure definition script USP_ForeCastItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ForeCastItems(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                        
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                      
 v_tintType SMALLINT DEFAULT 1 ,                
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,        
 v_tintFirstMonth SMALLINT DEFAULT 0,        
 v_tintSecondMonth SMALLINT DEFAULT 0,        
 v_tintThirdMonth SMALLINT DEFAULT 0,        
 v_intYear INTEGER DEFAULT 0 ,       
 v_byteMode SMALLINT DEFAULT NULL,    
 v_numItemCode NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      Select M.Mon,coalesce(monForecastAmount,0) as ForeCastAmount,coalesce(monQuota,0) as QuotaAmt,
 coalesce((SELECT sum(numUnitHour)
         FROM   OpportunityMaster
         join OpportunityItems
         on OpportunityMaster.numOppId = OpportunityItems.numOppId
         WHERE tintopptype = 1 and numItemCode = v_numItemCode and  tintoppstatus = 1  and  numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID
                  = v_numUserCntID) end) when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end)
         and numDomainId = v_numDomainID And EXTRACT(month FROM intPEstimatedCloseDate) = M.Mon And EXTRACT(year FROM intPEstimatedCloseDate) = v_intYear),0) AS sold,
 coalesce((SELECT
         sum(numUnitHour)
         FROM
         OpportunityMaster
         join OpportunityItems
         on OpportunityMaster.numOppId = OpportunityItems.numOppId
         WHERE tintopptype = 1 And  tintoppstatus = 0  And  numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end) when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end)
         and numDomainId = v_numDomainID and numItemCode = v_numItemCode  And  EXTRACT(month FROM intPEstimatedCloseDate) = M.Mon And EXTRACT(year FROM intPEstimatedCloseDate) = v_intYear),0) As Pipeline,
 coalesce((Select
         Avg(coalesce(tintPercentage,0))
         From
         OpportunityMaster Opp
         inner join OpportunityStageDetails OSD on Opp.numOppId = OSD.numoppid
         join OpportunityItems
         on Opp.numOppId = OpportunityItems.numOppId
         Where Opp.tintopptype = 1 and numItemCode = v_numItemCode  And   Opp.tintoppstatus = 0  And  numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end) when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end)  And OSD.bitstagecompleted = true
         and Opp.numDomainId = v_numDomainID And  EXTRACT(month FROM intPEstimatedCloseDate) = M.Mon And EXTRACT(year FROM intPEstimatedCloseDate) = v_intYear),0) As Probability,
 coalesce((Select Sum(X.Units)   From(Select(numUnitHour*(Select sum(coalesce(tintPercentage,0))  from OpportunityStageDetails OSD
               Where OSD.numoppid = OpportunityMaster.numOppId And OSD.bitstagecompleted = true))/100 as Units  From OpportunityMaster
            join OpportunityItems
            on OpportunityMaster.numOppId = OpportunityItems.numOppId
            Where tintopptype = 1 and numItemCode = v_numItemCode  And EXTRACT(month FROM intPEstimatedCloseDate) = M.Mon and EXTRACT(year FROM intPEstimatedCloseDate) = v_intYear
            and  numrecowner in(case when v_numUserCntID > 0 then(case when v_tintType = 1 then(select v_numUserCntID) else(select numContactId from AdditionalContactsInformation where numManagerID = v_numUserCntID) end) when v_numDivisionID > 0 then(select numContactId from AdditionalContactsInformation where numDivisionId = v_numDivisionID) else(select 0) end)  And tintoppstatus = 0 and tintshipped = 0 and numDomainId = v_numDomainID) X),
      0) As ProbableAmt
      From Forecast F
      right join(select v_tintFirstMonth as Mon union select v_tintSecondMonth union select v_tintThirdMonth) M
      on F.tintMonth = M.Mon and sintYear = v_intYear and numDomainID = v_numDomainID and tintForecastType = 2 and numItemCode = v_numItemCode  and numCreatedBy = v_numUserCntID;
   ELSEIF v_byteMode = 1
   then

      open SWV_RefCur for
      Select M.Mon,coalesce(monForecastAmount,0) as ForeCastAmount,coalesce(monQuota,0) as QuotaAmt
      From Forecast F
      right join(select v_tintFirstMonth as Mon union select v_tintSecondMonth union select v_tintThirdMonth) M
      on F.tintMonth = M.Mon and sintYear = v_intYear and numDomainID = v_numDomainID and tintForecastType = 2 and numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$;


