-- Stored procedure definition script Usp_DelContactScheduled for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_DelContactScheduled(v_numContactID NUMERIC(9,0),
v_numScheduleId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM CustRptSchContacts where numScheduleId = v_numScheduleId and numcontactId = v_numContactID;
   RETURN;
END; $$;


