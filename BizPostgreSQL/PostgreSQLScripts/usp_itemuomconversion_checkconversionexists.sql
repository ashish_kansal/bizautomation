-- Stored procedure definition script USP_ItemUOMConversion_CheckConversionExists for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemUOMConversion_CheckConversionExists(v_numDomainID NUMERIC,
	v_numItemCode NUMERIC,
	v_numSourceUnit NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseUnit  NUMERIC(18,0) DEFAULT 0;
BEGIN
   select   coalesce(numBaseUnit,0) INTO v_numBaseUnit FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;

   IF coalesce(v_numBaseUnit,0) = 0 then
	
      RAISE EXCEPTION 'ITEM_BASE_UOM_MISSING';
   ELSE
      IF v_numBaseUnit = v_numSourceUnit then
		
         open SWV_RefCur for
         SELECT 1;
      ELSE
         open SWV_RefCur for
         SELECT fn_CheckIfItemUOMConversionExists(v_numDomainID,v_numItemCode,v_numSourceUnit);
      end if;
   end if;
   RETURN;
END; $$;

