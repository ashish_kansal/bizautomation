-- Function definition script fn_GetTerritoryName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetTerritoryName(v_numTerID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TerrName  VARCHAR(100);
BEGIN
   select   coalesce(vcData,'') INTO v_TerrName from Listdetails where numListItemID = v_numTerID;  
  
   return v_TerrName;
END; $$;  
/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalHrs]    Script Date: 07/26/2008 18:12:47 ******/

