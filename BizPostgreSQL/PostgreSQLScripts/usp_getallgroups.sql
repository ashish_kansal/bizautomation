CREATE OR REPLACE FUNCTION usp_GetAllGroups(v_numUserID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all Groups for the selected Domain.    
--If UserId is not 0 then turn all groups for the user.    
   AS $$
BEGIN
   open SWV_RefCur for SELECT numGrpId as "numGrpId",vcGrpName as "vcGrpName"
   FROM Groups     
   --WHERE bitPublic=1    
   ORDER BY vcGrpName;
END; $$;