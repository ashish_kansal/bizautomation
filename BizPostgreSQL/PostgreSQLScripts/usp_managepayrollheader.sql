-- Stored procedure definition script USP_ManagePayrollHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePayrollHeader(v_numDomainId NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
    INOUT v_numPayrollHeaderID NUMERIC(18,0) ,
    v_numPayrolllReferenceNo NUMERIC(18,0),
    v_dtFromDate TIMESTAMP,
    v_dtToDate TIMESTAMP,
    v_dtPaydate TIMESTAMP,
    v_strItems TEXT,
    v_tintMode SMALLINT DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_numPayrollStatus INTEGER DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF(SELECT COUNT(*) FROM PayrollHeader WHERE numPayrolllReferenceNo = v_numPayrolllReferenceNo AND numDomainID = v_numDomainId AND numPayrollHeaderID <> v_numPayrollHeaderID) > 0 then
	
      RAISE EXCEPTION 'DUPLICATE';
      RETURN;
   end if;
	 
   IF v_numPayrollHeaderID = 0 then
	
      INSERT INTO PayrollHeader(numPayrolllReferenceNo, numDomainID, dtFromDate, dtToDate, dtPaydate, numCreatedBy, dtCreatedDate, numPayrollStatus)
      SELECT v_numPayrolllReferenceNo, v_numDomainId, v_dtFromDate, v_dtToDate, v_dtPaydate, v_numUserCntID, TIMEZONE('UTC',now()),v_numPayrollStatus;
      v_numPayrollHeaderID := CURRVAL('PayrollHeader_seq');
   ELSE
      UPDATE PayrollHeader
      SET    numPayrolllReferenceNo = v_numPayrolllReferenceNo,dtFromDate = v_dtFromDate, 
      dtToDate = v_dtToDate,dtPaydate = v_dtPaydate,numModifiedBy = v_numUserCntID, 
      dtModifiedDate = TIMEZONE('UTC',now()),numPayrollStatus = v_numPayrollStatus
      WHERE  numPayrollHeaderID = v_numPayrollHeaderID AND numDomainID = v_numDomainId;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
				
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
            SELECT  numUserCntID,decRegularHrs,decOvertimeHrs,decPaidLeaveHrs,monCommissionAmt,
					 monExpenses,monReimbursableExpenses,monDeductions,monTotalAmt 
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numUserCntID NUMERIC(18,0) PATH 'numUserCntID',
					decRegularHrs DOUBLE PRECISION PATH 'decRegularHrs',
					decOvertimeHrs DOUBLE PRECISION PATH 'decOvertimeHrs',
					decPaidLeaveHrs DOUBLE PRECISION PATH 'decPaidLeaveHrs',
					monCommissionAmt DECIMAL(20,5) PATH 'monCommissionAmt',
					monExpenses DECIMAL(20,5) PATH 'monExpenses',
					monReimbursableExpenses DECIMAL(20,5) PATH 'monReimbursableExpenses',
					monDeductions DECIMAL(20,5) PATH 'monDeductions',
					monTotalAmt DECIMAL(20,5) PATH 'monTotalAmt'
			) AS X;

         UPDATE PayrollDetail PD SET decRegularHrs = T.decRegularHrs,decOvertimeHrs = T.decOvertimeHrs,decPaidLeaveHrs = T.decPaidLeaveHrs,
         monCommissionAmt = T.monCommissionAmt,monExpenses = T.monExpenses,
         monReimbursableExpenses = T.monReimbursableExpenses,monDeductions = T.monDeductions,
         monTotalAmt = T.monTotalAmt
         FROM tt_TEMP T
         WHERE PD.numUserCntID = T.numUserCntID AND(PD.numDomainID = v_numDomainId AND PD.numPayrollHeaderID = v_numPayrollHeaderID);
         INSERT INTO PayrollDetail(numPayrollHeaderID,numDomainID,numUserCntID
							,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs
							,monOverTimeRate,decRegularHrs,decOvertimeHrs,decPaidLeaveHrs
							,monCommissionAmt,monExpenses,monReimbursableExpenses,monDeductions,monTotalAmt,numCheckStatus)
         SELECT v_numPayrollHeaderID,v_numDomainId,T.numUserCntID,
							monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,
							T.decRegularHrs,T.decOvertimeHrs,T.decPaidLeaveHrs,T.monCommissionAmt,
							T.monExpenses,T.monReimbursableExpenses,T.monDeductions,T.monTotalAmt,0
         FROM tt_TEMP T JOIN UserMaster UM ON T.numUserCntID = UM.numUserDetailId
         WHERE UM.numDomainID = v_numDomainId AND T.numUserCntID NOT IN(SELECT numUserCntID FROM PayrollDetail WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID);
					 
					--LOGIC OF COMMISSION TYPE IS REMOVED BECAUSE WE hAVE REMOVES PROJECT COMMISSION(tintCommissionType=3) OPTION FROM GLOBAL SETTINGS
         IF v_tintMode != 4 then
					
            DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID
            AND coalesce(numComissionID,0) > 0
            AND numPayrollDetailID IN(SELECT numPayrollDetailID FROM PayrollDetail PD JOIN tt_TEMP T ON PD.numUserCntID = T.numUserCntID
            WHERE PD.numDomainID = v_numDomainId AND PD.numPayrollHeaderID = v_numPayrollHeaderID);
            IF v_tintMode = 1 then --Only Paid
						
               INSERT INTO PayrollTracking(numDomainID,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
               SELECT
               v_numDomainId,v_numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
               FROM(SELECT
                  PD.numPayrollDetailID,numComissionID,BC.numComissionAmount -coalesce((SELECT SUM(coalesce(monCommissionAmt,0)) FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monCommissionAmt
                  FROM
                  PayrollDetail PD
                  JOIN tt_TEMP T ON PD.numUserCntID = T.numUserCntID
                  INNER JOIN BizDocComission BC ON BC.numUserCntID = PD.numUserCntID
                  INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId = oppBiz.numOppBizDocsId
                  INNER JOIN OpportunityMaster Opp ON oppBiz.numoppid = Opp.numOppId
                  LEFT JOIN LATERAL(SELECT
                     MAX(DM.dtDepositDate) AS dtDepositDate
                     FROM
                     DepositMaster DM
                     JOIN
                     DepositeDetails DD
                     ON
                     DM.numDepositId = DD.numDepositID
                     WHERE
                     DM.tintDepositePage = 2
                     AND DM.numDomainId = v_numDomainId
                     AND DD.numOppID = OppBiz.numOppID
                     AND DD.numOppBizDocsID = OppBiz.numOppBizDocsID) TempDepositMaster on TRUE
                  WHERE PD.numDomainID = v_numDomainId AND PD.numPayrollHeaderID = v_numPayrollHeaderID AND
                  Opp.numDomainId = v_numDomainId AND BC.bitCommisionPaid = false
                  AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount
                  AND (TempDepositMaster.dtDepositDate IS NOT NULL AND TempDepositMaster.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)) T
               WHERE monCommissionAmt > 0;
            ELSEIF v_tintMode = 2
            then --Only UNPaid
						
               INSERT INTO PayrollTracking(numDomainID,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
               SELECT v_numDomainId,v_numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
               FROM(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount -coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
                     FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monCommissionAmt
                  FROM PayrollDetail PD JOIN tt_TEMP T ON PD.numUserCntID = T.numUserCntID
                  INNER JOIN BizDocComission BC ON BC.numUserCntID = PD.numUserCntID
                  INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId = oppBiz.numOppBizDocsId
                  INNER JOIN OpportunityMaster Opp ON oppBiz.numoppid = Opp.numOppId
                  WHERE PD.numDomainID = v_numDomainId AND PD.numPayrollHeaderID = v_numPayrollHeaderID AND
                  Opp.numDomainId = v_numDomainId AND BC.bitCommisionPaid = false
                  AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount
                  AND (oppBiz.dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate)) T
               WHERE monCommissionAmt > 0;
            ELSEIF v_tintMode = 3
            then --Both Paid & UNPaid
						
               INSERT INTO PayrollTracking(numDomainID,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
               SELECT v_numDomainId,v_numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
               FROM(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount -coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
                     FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monCommissionAmt
                  FROM PayrollDetail PD JOIN tt_TEMP T ON PD.numUserCntID = T.numUserCntID
                  INNER JOIN BizDocComission BC ON BC.numUserCntID = PD.numUserCntID
                  INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId = oppBiz.numOppBizDocsId
                  INNER JOIN OpportunityMaster Opp ON oppBiz.numoppid = Opp.numOppId
                  LEFT JOIN LATERAL(SELECT
                     MAX(DM.dtDepositDate) AS dtDepositDate
                     FROM
                     DepositMaster DM
                     JOIN
                     DepositeDetails DD
                     ON
                     DM.numDepositId = DD.numDepositID
                     WHERE
                     DM.tintDepositePage = 2
                     AND DM.numDomainId = v_numDomainId
                     AND DD.numOppID = OppBiz.numOppID
                     AND DD.numOppBizDocsID = OppBiz.numOppBizDocsID) TempDepositMaster on TRUE
                  WHERE PD.numDomainID = v_numDomainId AND PD.numPayrollHeaderID = v_numPayrollHeaderID AND
                  Opp.numDomainId = v_numDomainId AND BC.bitCommisionPaid = false
                  AND oppBiz.bitAuthoritativeBizDocs = 1
                  AND 1 =(CASE
                  WHEN coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount
                  THEN CASE WHEN oppBiz.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate THEN 1 ELSE 0 END
                  ELSE CASE WHEN TempDepositMaster.dtDepositDate IS NOT NULL AND TempDepositMaster.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtFromDate And v_dtToDate THEN 1 ELSE 0 END
                  END)) T
               WHERE monCommissionAmt > 0;
            end if;
         end if;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
      end if;
   end if;
END; $$;



