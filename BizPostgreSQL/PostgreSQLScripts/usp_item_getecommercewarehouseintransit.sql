-- Stored procedure definition script USP_Item_GetEcommerceWarehouseInTransit for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetEcommerceWarehouseInTransit(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   TO_CHAR(coalesce(SUM(numOnOrder),0),'FM999,999,999,999,990')
   FROM
   WareHouseItems
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   WHERE
   WareHouseItems.numDomainID = v_numDomainID
   AND WareHouseItems.numItemID = v_numItemCode;
END; $$;












