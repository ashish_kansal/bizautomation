-- Stored procedure definition script USP_UpdateShippingItemPrice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateShippingItemPrice(v_numOppId NUMERIC(9,0) DEFAULT 0,
    v_numItemCode NUMERIC(9,0) DEFAULT 0,
	v_numItemPrice NUMERIC(18,2) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   OpportunityItems
   SET
   monPrice = v_numItemPrice,monTotAmount = v_numItemPrice,monTotAmtBefDiscount = v_numItemPrice
   WHERE
   numOppId = v_numOppId and numItemCode = v_numItemCode;
   RETURN;
END; $$;


