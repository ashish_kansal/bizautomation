-- Stored procedure definition script USP_ManageLeadSubform for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageLeadSubform(v_numDomainId NUMERIC DEFAULT 0,
 v_numSubFormId NUMERIC DEFAULT 0,
 v_vcFormName VARCHAR(300) DEFAULT '',
 v_bitByPassRoutingRules BOOLEAN DEFAULT false,
 v_numAssignTo NUMERIC DEFAULT 0,
 v_bitDripCampaign BOOLEAN DEFAULT false,
 v_numDripCampaign NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numSubFormId > 0) then
	
      DELETE FROM LeadsubForms WHERE numDomainID = v_numDomainId AND numSubFormId = v_numSubFormId;
      open SWV_RefCur for
      SELECT 1;
   ELSE
      IF EXISTS(SELECT numSubFormId FROM LeadsubForms WHERE numDomainID = v_numDomainId AND vcFormName = v_vcFormName) then
		
         open SWV_RefCur for
         SELECT 0;
      ELSE
         INSERT INTO LeadsubForms(numDomainID,
				vcFormName,
				bitByPassRoutingRules,
				numAssignTo,
				bitDripCampaign,
				numDripCampaign)
		VALUES(v_numDomainId,
				v_vcFormName,
				v_bitByPassRoutingRules,
				v_numAssignTo,
				v_bitDripCampaign,
				v_numDripCampaign);

		open SWV_RefCur for
		SELECT 1;
      end if;
   end if;
   RETURN;
END; $$;


