-- Stored procedure definition script USP_PromotionOffer_ClearCouponCodeECommerce for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromotionOffer_ClearCouponCodeECommerce(v_numDomainID NUMERIC(18,0),
	v_numUserCntId NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),
	v_vcCookieId VARCHAR(100),
	v_numPromotionID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   CartItems
   SET
   PromotionID = 0,bitPromotionTrigerred = false,PromotionDesc = '',bitPromotionDiscount = false,
   monPrice = coalesce(monInsertPrice,0),fltDiscount = coalesce(fltInsertDiscount,0),
   bitDiscountType = coalesce(bitInsertDiscountType,false),monTotAmtBefDiscount = coalesce(numUnitHour,0)*coalesce(monInsertPrice,0),
   monTotAmount =(CASE
   WHEN coalesce(fltInsertDiscount,0) > 0
   THEN(CASE
      WHEN coalesce(bitInsertDiscountType,false) = true
      THEN(CASE
         WHEN(coalesce(numUnitHour,0)*coalesce(monInsertPrice,0)) -fltInsertDiscount >= 0
         THEN(coalesce(numUnitHour,0)*coalesce(monInsertPrice,0)) -fltInsertDiscount
         ELSE 0
         END)
      ELSE coalesce(numUnitHour,0)*(coalesce(monInsertPrice,0) -(coalesce(monInsertPrice,0)*(fltInsertDiscount/100)))
      END)
   ELSE coalesce(numUnitHour,0)*coalesce(monInsertPrice,0)
   END)
   WHERE
   numDomainId = v_numDomainID
   AND vcCookieId = v_vcCookieId
   AND numUserCntId = v_numUserCntId
   AND coalesce(PromotionID,0) = v_numPromotionID;
   RETURN;
END; $$;


