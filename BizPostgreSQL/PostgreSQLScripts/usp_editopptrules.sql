-- Stored procedure definition script usp_EditOpptRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_EditOpptRules(   
--
v_numRuleId NUMERIC DEFAULT 0,
v_numEmpDets NUMERIC DEFAULT 0,
v_numDomainID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
--Means it is for fetching the details of the rules	  
   AS $$
BEGIN
   IF v_numRuleId = 0 then
	
      open SWV_RefCur for
      SELECT OM.numruleid, OM.numStagePercentageID, OM.monAmt, OM.vcCondn, OM.bintCreatedDate,
			OM.numNoOfDays, OM.vcRuleName, MIN(ACI.vcFirstName || ' ' || ACI.vcLastname) As vcUserNames
      FROM OpptRuleMaster OM INNER JOIN OpptRuleDetails OD ON OD.numruleid = OM.numruleid INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = OD.numEmpID
      WHERE OM.numDomainID = v_numDomainID
      GROUP BY OM.numruleid,OM.numStagePercentageID,OM.monAmt,OM.vcCondn,OM.numNoOfDays, 
      OM.vcRuleName,OM.bintCreatedDate;
   ELSE
		--SELECT numstagepercentage FROM stagepercentagemaster where 
		--numstagepercentageid=@numstagepercentageId
      IF v_numEmpDets = 0 then
			
         open SWV_RefCur for
         SELECT * FROM OpptRuleMaster WHERE numruleid = v_numRuleId;
      ELSE
         open SWV_RefCur for
         SELECT * FROM OpptRuleDetails WHERE numruleid = v_numRuleId;
      end if;
   end if;
   RETURN;
END; $$;


