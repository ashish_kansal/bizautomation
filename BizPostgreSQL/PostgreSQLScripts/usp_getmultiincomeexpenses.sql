-- Stored procedure definition script USP_GetMultiIncomeExpenses for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMultiIncomeExpenses(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
 v_numSubscriberID INTEGER,
 INOUT SWV_RefCur refcursor default null,
 v_Rollup BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPINCOMEEXPENSE CASCADE;
   create TEMPORARY TABLE tt_TEMPINCOMEEXPENSE
   (
      numDomainID INTEGER,
      vcDomainName VARCHAR(200),
      vcDomainCode VARCHAR(50),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Expense DECIMAL(20,5),
      Income DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPINCOMEEXPENSE
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode;

------------------------
--getting income
-------------------------

   INSERT INTO tt_TEMPINCOMEEXPENSE
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, 0,Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode;

   if v_Rollup = false then
	
      open SWV_RefCur for
      select
      vcDomainName,
		coalesce(Sum(coalesce(Expense,0)),0) as Expense ,
		coalesce(sum(coalesce(Income,0)),0)*(-1)  as Income
      from tt_TEMPINCOMEEXPENSE
      group by
      vcDomainName;
   ELSEIF v_Rollup = true
   then
	
      open SWV_RefCur for
      select
      'Income & Expenses Roll-Up' as vcDomainName,
		coalesce(Sum(coalesce(Expense,0)),0) as Expense ,
		coalesce(sum(coalesce(Income,0)),0)*(-1)  as Income
      from tt_TEMPINCOMEEXPENSE;
   end if;
   RETURN;
END; $$;
-- Created by Sojan        
--exec USP_GetMultiPL @numParentDomainID=0,@dtFromDate='01/Jan/2008',@dtToDate='31/Dec/2008',@numSubscriberId=103,@Rollup=1


