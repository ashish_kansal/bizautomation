DROP FUNCTION IF EXISTS usp_getaccountreceivableaging;

CREATE OR REPLACE FUNCTION public.usp_getaccountreceivableaging(
	v_numdomainid numeric DEFAULT 0,
	v_numdivisionid numeric DEFAULT NULL::numeric,
	v_numusercntid numeric DEFAULT NULL::numeric,
	v_numaccountclass numeric DEFAULT 0,
	v_dtfromdate timestamp without time zone DEFAULT NULL::timestamp without time zone,
	v_dttodate timestamp without time zone DEFAULT NULL::timestamp without time zone,
	v_clienttimezoneoffset integer DEFAULT 0,
	v_bitcustomerstatement boolean DEFAULT false,
	v_tintbasedon smallint DEFAULT 1,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_AuthoritativeSalesBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
BEGIN
-- This function was converted on Sat Apr 10 14:54:54 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF v_dtFromDate IS NULL then
      v_dtFromDate := CAST('1990-01-01 00:00:00.000' AS TIMESTAMP);
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

   RAISE NOTICE '%',v_dtFromDate;
   RAISE NOTICE '%',v_dtToDate;

   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId = v_numDomainId;
  
   INSERT  INTO TempARRecord(numUserCntID,
          numOppId,
          numDivisionId,
          numOppBizDocsId,
          DealAmount,
          numBizDocId,
          numCurrencyID,
          dtDueDate,AmountPaid)
   SELECT  v_numUserCntID,
                OB.numoppid,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount,
                OB.numBizDocId,
                coalesce(Opp.numCurrencyID,0) AS numCurrencyID,
				dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) AS dtDueDate,
                coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0) as AmountPaid
   FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = Opp.numDivisionId
   LEFT JOIN LATERAL(SELECT
      SUM(monAmountPaid) AS monPaidAmount
      FROM
      DepositeDetails DD
      INNER JOIN
      DepositMaster DM
      ON
      DD.numDepositID = DM.numDepositId
      WHERE
      numOppID = Opp.numOppID
      AND numOppBizDocsId = OB.numOppBizDocsID
      AND 1 =(CASE WHEN coalesce(DM.numReturnHeaderID,0) > 0 THEN(CASE WHEN CAST(DD.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) <= CAST(v_dtToDate AS DATE) THEN 1 ELSE 0 END) ELSE(CASE WHEN CAST(DM.dtDepositDate AS DATE) <= CAST(v_dtToDate AS DATE) THEN 1 ELSE 0 END) END)) TablePayments on TRUE
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
   AND Opp.numDomainId = v_numDomainId
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND (Opp.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND (Opp.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND(CASE
   WHEN coalesce(v_bitCustomerStatement,false) = true
   THEN CAST(OB.dtCreatedDate AS DATE)
   ELSE CAST(dtFromDate+CAST(CASE
      WHEN Opp.bitBillingTerms = true AND v_tintBasedOn = 1
      THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
      ELSE 0
      END || 'day' as interval) AS DATE)
   END) <= v_dtToDate
   GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
   OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
   OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate,TablePayments.monPaidAmount
   HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0)
   -coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0)) != 0
                           
	--Show Impact of AR journal entries in report as well 
   UNION
   SELECT v_numUserCntID,
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCustomerId,
        CAST(0 AS NUMERIC(18,0)),
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCurrencyID,
        GJH.datEntry_Date ,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = GJD.numCustomerId
   WHERE  GJH.numDomainId = v_numDomainId
   AND COA.vcAccountCode ilike '01010105%'
   AND coalesce(numOppId,0) = 0 AND coalesce(numOppBizDocsId,0) = 0
   AND (GJD.numCustomerId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND coalesce(GJH.numDepositId,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
   AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(GJH.datEntry_Date AS DATE) <= v_dtToDate
   UNION ALL --Standalone Refund against Account Receivable
   SELECT v_numUserCntID,
        CAST(0 AS NUMERIC(18,0)),
        RH.numDivisionId,
        CAST(0 AS NUMERIC(18,0)),
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCurrencyID,
        GJH.datEntry_Date ,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   ReturnHeader RH JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = RH.numDivisionId
   JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode ilike '0101010501'
   WHERE RH.tintReturnType = 4 AND RH.numDomainID = v_numDomainId
   AND coalesce(RH.numParentID,0) = 0
   AND coalesce(RH.IsUnappliedPayment,false) = false
   AND (RH.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND monBizDocAmount > 0 AND coalesce(RH.numBillPaymentIDRef,0) = 0 --AND ISNULL(RH.numDepositIDRef,0) > 0
   AND (RH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(GJH.datEntry_Date AS DATE) <= v_dtToDate;
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

	INSERT  INTO TempARRecord
	(
		numUserCntID,
        numOppId,
        numDivisionId,
        numOppBizDocsId,
        DealAmount,
        numBizDocId,
        numCurrencyID,
        dtDueDate,
		AmountPaid,
		monUnAppliedAmount
	)
	SELECT
		v_numUserCntID
		,0
		,numDivisionID
		,0
		,0
		,0
		,0
		,NULL
		,0
		,SUM(monDepositAmount)
	FROM
	(
		SELECT
			DM.numDivisionID
			,coalesce(DM.monDepositAmount,0) -coalesce(DM.monAppliedAmount,0) AS monDepositAmount
		FROM
			DepositMaster DM
		JOIN
			DivisionMaster AS DIV
		ON
			DIV.numDivisionID = DM.numDivisionID
		WHERE
			DM.numDomainId = v_numDomainId 
			AND tintDepositePage IN (2,3)
			AND (DM.numDivisionID = v_numDivisionId OR v_numDivisionId IS NULL)
			AND CAST((coalesce(monDepositAmount,0) -coalesce(DM.monAppliedAmount,0)) AS DECIMAL(18,2)) <> 0
			AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
			AND CAST(DM.dtDepositDate AS DATE) <= v_dtToDate
		--	AND coalesce(DM.numReturnHeaderID,0) = 0
		--UNION ALL
		--SELECT
		--	DM.numDivisionID
		--	,coalesce(DM.monDepositAmount,0) -(CASE
		--										  WHEN RH.tintReturnType = 4
		--										  THEN coalesce(monAppliedAmount,0)
		--										  ELSE coalesce((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID = DM.numDepositId AND DD.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) <= v_dtToDate),0)
		--										  END) AS monDepositAmount
		--FROM
		--	DepositMaster DM
		--JOIN
		--	DivisionMaster AS DIV
		--ON
		--	DIV.numDivisionID = DM.numDivisionID
		--LEFT JOIN
		--	ReturnHeader RH
		--ON
		--	DM.numReturnHeaderID = RH.numReturnHeaderID
		--WHERE
		--	DM.numDomainId = v_numDomainId AND tintDepositePage IN(2,3)
		--	AND (DM.numDivisionID = v_numDivisionId OR v_numDivisionId IS NULL)
		--	AND CAST(coalesce(monDepositAmount,0) -(CASE
		--												WHEN RH.tintReturnType = 4
		--												THEN coalesce(monAppliedAmount,0)
		--												ELSE coalesce((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID = DM.numDepositId AND DD.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) <= v_dtToDate),0)
		--											END) AS DECIMAL(18,2)) <> 0
		--	AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
		--	AND CAST(DM.dtDepositDate AS DATE) <= v_dtToDate
		--	AND coalesce(DM.numReturnHeaderID,0) > 0
	) TEMP
	GROUP BY
		numDivisionID;

   INSERT  INTO TempARRecord1(numUserCntID,
	numDivisionId,
	tintCRMType,
	vcCompanyName,
	vcCustPhone,
	numContactId,
	vcEmail,
	numPhone,
	vcContactName,
	numCurrentDays,
	numThirtyDays,
	numSixtyDays,
	numNinetyDays,
	numOverNinetyDays,
	numThirtyDaysOverDue,
	numSixtyDaysOverDue,
	numNinetyDaysOverDue,
	numOverNinetyDaysOverDue,
	numCompanyID,
	numDomainID,
	intCurrentDaysCount,
	intThirtyDaysCount,
	intThirtyDaysOverDueCount,
	intSixtyDaysCount,
	intSixtyDaysOverDueCount,
	intNinetyDaysCount,
	intOverNinetyDaysCount,
	intNinetyDaysOverDueCount,
	intOverNinetyDaysOverDueCount,
	numCurrentDaysPaid,
	numThirtyDaysPaid,
	numSixtyDaysPaid,
	numNinetyDaysPaid,
	numOverNinetyDaysPaid,
	numThirtyDaysOverDuePaid,
	numSixtyDaysOverDuePaid,
	numNinetyDaysOverDuePaid,
	numOverNinetyDaysOverDuePaid,monUnAppliedAmount)
   SELECT DISTINCT
   v_numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                0,
                '',
                '',
                '',
				0 AS numCurrentDays,
                0 AS numThirtyDays,
                0 AS numSixtyDays,
                0 AS numNinetyDays,
                0 AS numOverNinetyDays,
                0 AS numThirtyDaysOverDue,
                0 AS numSixtyDaysOverDue,
                0 AS numNinetyDaysOverDue,
                0 AS numOverNinetyDaysOverDue,
                C.numCompanyId,
				Div.numDomainID,
				0 AS intCurrentDaysCount,
                0 AS intThirtyDaysCount,
				0 AS intThirtyDaysOverDueCount,
				0 AS intSixtyDaysCount,
				0 AS intSixtyDaysOverDueCount,
				0 AS intNinetyDaysCount,
				0 AS intOverNinetyDaysCount,
				0 AS intNinetyDaysOverDueCount,
				0 AS intOverNinetyDaysOverDueCount,
				0 AS numCurrentDaysPaid,
				0 AS numThirtyDaysPaid,
                0 AS numSixtyDaysPaid,
                0 AS numNinetyDaysPaid,
                0 AS numOverNinetyDaysPaid,
                0 AS numThirtyDaysOverDuePaid,
                0 AS numSixtyDaysOverDuePaid,
                0 AS numNinetyDaysOverDuePaid,
                0 AS numOverNinetyDaysOverDuePaid,0 AS monUnAppliedAmount
   FROM    DivisionMaster Div
   INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
   WHERE   Div.numDomainID = v_numDomainId
   AND Div.numDivisionID IN(SELECT DISTINCT
      numDivisionId
      FROM   TempARRecord);

--Update Custome Phone 
   UPDATE  TempARRecord1
   SET     vcCustPhone = X.vcCustPhone
   FROM(SELECT
   CASE WHEN NULLIF(numPhone,'') IS NULL THEN ''
   WHEN numPhone = '' THEN ''
   ELSE ', ' || numPhone
   END AS vcCustPhone,
                            numDivisionId
   FROM      AdditionalContactsInformation
   WHERE     bitPrimaryContact = true
   AND numDivisionId  IN(SELECT DISTINCT numDivisionId FROM TempARRecord1 WHERE numUserCntID = v_numUserCntID)) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

--Update Customer info 
   UPDATE  TempARRecord1
   SET     numContactId = X.numContactID,vcEmail = X.vcEmail,vcContactName = X.vcContactName,
   numPhone = X.numPhone
   FROM(SELECT numContactId AS numContactID,
                     coalesce(vcEmail,'') AS vcEmail,
                     coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS vcContactName,
                     CAST(coalesce(CASE WHEN NULLIF(numPhone,'') IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' || numPhone END,'') AS VARCHAR(30)) AS numPhone,
                     numDivisionId
   FROM      AdditionalContactsInformation
   WHERE     (vcPosition = 844 or coalesce(bitPrimaryContact,false) = true)
   AND numDivisionId  IN(SELECT DISTINCT numDivisionId FROM TempARRecord1 WHERE numUserCntID = v_numUserCntID)) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

                    
--SELECT  GETDATE()
------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM Domain WHERE numDomainId = v_numDomainId;
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

   UPDATE  TempARRecord1
   SET     numCurrentDays = X.numCurrentDays
   FROM(SELECT    SUM(DealAmount) AS numCurrentDays,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
   GROUP BY numDivisionId) X WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numCurrentDaysPaid = X.numCurrentDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) >= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
   GROUP BY numDivisionId) X WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
   AND
   numCurrencyID <> v_baseCurrency);

---------------------------------------------------------------------------------------------

-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
   UPDATE  TempARRecord1
   SET     numThirtyDays = X.numThirtyDays
   FROM(SELECT    SUM(DealAmount) AS numThirtyDays,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 0 AND 30
   GROUP BY numDivisionId) X WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numThirtyDaysPaid = X.numThirtyDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 0 AND 30
   GROUP BY numDivisionId) X WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

                
   UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 0 AND 30
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  TempARRecord1
   SET     numSixtyDays = X.numSixtyDays
   FROM(SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 31 AND 60
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numSixtyDaysPaid = X.numSixtyDaysPaid
   FROM(SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND
   DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 31 AND 60
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 31 AND 60
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  TempARRecord1
   SET     numNinetyDays = X.numNinetyDays
   FROM(SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 61 AND 90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numNinetyDaysPaid = X.numNinetyDaysPaid
   FROM(SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 61 AND 90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) BETWEEN 61 AND 90
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  TempARRecord1
   SET     numOverNinetyDays = X.numOverNinetyDays
   FROM(SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) > 90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
   FROM(SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND DATE_PART('day',dtDueDate:: timestamp -GetUTCDateWithoutTime()) > 90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND dtDueDate >= GetUTCDateWithoutTime()+INTERVAL '91 day'
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
   UPDATE  TempARRecord1
   SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
   FROM(SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 1 AND 30
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
   FROM(SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 1 AND 30
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 1 AND 30
   AND numCurrencyID <> v_baseCurrency);

----------------------------------
   UPDATE  TempARRecord1
   SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
   FROM(SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 31
   AND     60
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
   FROM(SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 31
   AND     60
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 31
   AND     60
   AND numCurrencyID <> v_baseCurrency);

------------------------------------------
   UPDATE  TempARRecord1
   SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
   FROM(SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 61 AND 90
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
   FROM(SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 61 AND 90
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) BETWEEN 61 AND 90
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  TempARRecord1
   SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
   FROM(SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) > 90
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1
   SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
   FROM(SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) > 90
   GROUP BY numDivisionId) X
   WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN(SELECT DISTINCT numDivisionId
   FROM      TempARRecord
   WHERE     numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) > 90
   AND numCurrencyID <> v_baseCurrency);

   UPDATE  TempARRecord1
   SET     monUnAppliedAmount = coalesce(X.monUnAppliedAmount,0)
   FROM(SELECT  SUM(coalesce(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID
   GROUP BY numDivisionId) X WHERE   TempARRecord1.numDivisionId = X.numDivisionID;

   UPDATE  TempARRecord1 SET numTotal =  coalesce(numCurrentDays,0)+coalesce(numThirtyDaysOverDue,0)+coalesce(numSixtyDaysOverDue,0)+coalesce(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)1810
      +coalesce(numOverNinetyDaysOverDue,0) -coalesce(monUnAppliedAmount,0);
           
   open SWV_RefCur for SELECT
   numDivisionId,
		numContactId,
		tintCRMType,
		vcCompanyName,
		vcCustPhone,
		vcContactName,
		vcEmail,
		numPhone,
		numCurrentDays,
		numThirtyDays,
		numSixtyDays,
		numNinetyDays,
		numOverNinetyDays,
		numThirtyDaysOverDue,
		numSixtyDaysOverDue,
		numNinetyDaysOverDue,
		numOverNinetyDaysOverDue,
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		coalesce(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		intCurrentDaysCount,
		intThirtyDaysCount,
		intThirtyDaysOverDueCount,
		intSixtyDaysCount,
		intSixtyDaysOverDueCount,
		intNinetyDaysCount,
		intOverNinetyDaysCount,
		intNinetyDaysOverDueCount,
		intOverNinetyDaysOverDueCount,
		numCurrentDaysPaid,
		numThirtyDaysPaid,
		numSixtyDaysPaid,
		numNinetyDaysPaid,
		numOverNinetyDaysPaid,
		numThirtyDaysOverDuePaid,
		numSixtyDaysOverDuePaid,
		numNinetyDaysOverDuePaid,
		numOverNinetyDaysOverDuePaid,coalesce(monUnAppliedAmount,0) AS monUnAppliedAmount
   FROM    TempARRecord1 WHERE numUserCntID = v_numUserCntID AND coalesce(numTotal,0) != 0
   ORDER BY vcCompanyName; 
   DELETE  FROM TempARRecord1 WHERE   numUserCntID = v_numUserCntID;
   DELETE  FROM TempARRecord WHERE   numUserCntID = v_numUserCntID;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getaccountreceivableaging(numeric, numeric, numeric, numeric, timestamp without time zone, timestamp without time zone, integer, boolean, smallint, refcursor)
    OWNER TO postgres;
