DROP FUNCTION IF EXISTS USP_UpdateDealStatus;

CREATE OR REPLACE FUNCTION USP_UpdateDealStatus(v_numOppID NUMERIC(9,0) DEFAULT 0,                    
v_byteMode SMALLINT DEFAULT 0,                    
v_Amount DECIMAL(20,5) DEFAULT 0,  
v_ShippingCost DECIMAL(20,5) DEFAULT NULL,
v_numUserContactID NUMERIC(9,0) DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_BizDocID NUMERIC(9,0) DEFAULT 0,
v_numSiteID NUMERIC(9,0) DEFAULT NULL,
v_numPaymentMethodId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	v_numDivid  NUMERIC(9,0);
	v_tintOppType  SMALLINT;
	v_status  SMALLINT;                       
	v_numOrderStatus  NUMERIC;
	v_numFailedOrderStatus  NUMERIC;
	v_numRecOwner  NUMERIC;
	v_numAssignTo  NUMERIC;

	v_numStatus  NUMERIC(9,0);
	v_numUserCntID  NUMERIC(9,0);
	v_numDivisionID  NUMERIC;           
	v_numAuthoritativeeCommerce  NUMERIC(9,0);
	v_fltDiscount  DOUBLE PRECISION;
	v_bitDiscountType  BOOLEAN;
BEGIN
	SELECT 
		numDivisionId, tintopptype INTO v_numDivid,v_tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId = v_numOppID;             
   
	UPDATE DivisionMaster SET tintCRMType = 2 WHERE numDivisionID = v_numDivid;               

	UPDATE OpportunityMaster SET tintoppstatus = 1,bintAccountClosingDate = TIMEZONE('UTC',now()) where numOppId = v_numOppID;

	IF  v_byteMode = 1 THEN
		v_numAuthoritativeeCommerce := 0;

		SELECT 
			numDivisionId, coalesce(fltDiscountTotal,0), coalesce(bitDiscountTypeTotal,false) INTO v_numDivisionID,v_fltDiscount,v_bitDiscountType 
		FROM 
			OpportunityMaster where numOppId = v_numOppID;                            
		
		/*When first order is placed any lead or prospect should be converted to Account automatically-By chintan*/
		UPDATE DivisionMaster set tintCRMType = 2 where numDivisionID = v_numDivisionID;
	END IF;
 
	/*From e-commerce setting one can customize type of bizdoc to create based on payment mechanism used--By chintan 13/12/2011*/
	SELECT 
		coalesce(numOrderStatus,0), coalesce(numFailedOrderStatus,0), coalesce(numRecordOwner,0), coalesce(numAssignTo,0) INTO v_numOrderStatus,v_numFailedOrderStatus,v_numRecOwner,v_numAssignTo 
	FROM 
		eCommercePaymentConfig 
	WHERE 
		numDomainID = v_numDomainID 
		AND bitEnable = 1 
		AND numSiteId = v_numSiteID 
		AND numPaymentMethodId = v_numPaymentMethodId;         	

	IF v_numDomainID = 209 THEN
		SELECT coalesce(numRecOwner,0), coalesce(numAssignedTo,0) INTO v_numRecOwner,v_numAssignTo FROM DivisionMaster WHERE numDivisionID = v_numDivid;
	END IF;

                
   IF v_numPaymentMethodId = 1 then --Credit Card 
	
      IF EXISTS(SELECT * FROM eCommerceDTL WHERE numDomainID = v_numDomainID  AND bitAuthOnlyCreditCard = true) then
          
         select   numAuthrizedOrderStatus INTO v_numOrderStatus FROM eCommerceDTL WHERE numDomainID = v_numDomainID  AND bitAuthOnlyCreditCard = true;
      end if;
      UPDATE OpportunityMaster SET numStatus = coalesce(v_numOrderStatus,0),numrecowner = coalesce(v_numRecOwner,0),
      numassignedto = coalesce(v_numAssignTo,0)
      WHERE numOppId = v_numOppID AND numDomainId = v_numDomainID;
   ELSEIF v_numPaymentMethodId = 27261
   then --Bill Me
	
				 --PRINT(@numOppBizDocID)
      UPDATE OpportunityMaster SET numStatus = coalesce(v_numOrderStatus,0),numrecowner = coalesce(v_numRecOwner,0),
      numassignedto = coalesce(v_numAssignTo,0)
      WHERE numOppId = v_numOppID AND numDomainId = v_numDomainID;
   ELSEIF v_numPaymentMethodId = 31488
   then --Google Checkout By Default numStatus pass 0 .When actual order placed from Google checkout its status updated.
    
      UPDATE OpportunityMaster SET numStatus = v_numFailedOrderStatus,numrecowner = coalesce(v_numRecOwner,0),
      numassignedto = coalesce(v_numAssignTo,0)
      WHERE numOppId = v_numOppID AND numDomainId = v_numDomainID;
   end if;     
    

   v_numStatus := 0;
   v_numUserCntID := 0;
   select   coalesce(numStatus,0), coalesce(numCreatedBy,0) INTO v_numStatus,v_numUserCntID FROM OpportunityMaster WHERE numOppId = v_numOppID AND numDomainId = v_numDomainID; 

   IF coalesce(v_numStatus,0) > 0 then
	   
	   		 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- tinyint
      PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,
      v_numOppBizDocsId := 0,v_numOrderStatus := v_numStatus,v_numUserCntID := v_numUserCntID,
      v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
   end if;
   RETURN;
END; $$;
	   
/*below used to insert journal entries . which is now done from frontend.- chintan */
 --exec USP_UpdateAuthorizativeBizDocsForEcommerce  @numOppID,'',@numDomainId,@numOppBizDocID,@numUserContactID


/*below code is obsolete as its being replaced by updateDealamount function from front endt, in checkout.ascx.cs- chintan*/
--if @Amount>0
--begin 
-- -- To Insert into OpportunityBizDocsDetails table 
-- declare @numCurrencyID as numeric(9)
-- declare @fltExchangeRate as FLOAT
-- set @numCurrencyID=0
-- select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId
-- if @numCurrencyID=0 set   @fltExchangeRate=1
-- else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
--
--
-- Declare @numBizDocsPaymentDetId as numeric(9)  
-- Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDomainId,bitAuthoritativeBizDocs,bitIntegratedToAcnt,numCreatedBy,dtCreationDate,[numCurrencyID],[fltExchangeRate])  
-- Values(@numOppBizDocID,1,@Amount,@numDomainId,@bitAuthoritativeBizDocs,0,@numUserContactID,getutcdate(),@numCurrencyID,@fltExchangeRate)  
--    Set @numBizDocsPaymentDetId=@@Identity  
--   --To insert into OpportunityBizDocsPaymentDetails table  
--    Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)   
-- Values(@numBizDocsPaymentDetId,@Amount,getutcdate(),0)  
--
-- end                
--end


