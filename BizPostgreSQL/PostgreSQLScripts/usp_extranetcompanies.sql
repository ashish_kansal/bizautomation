-- Stored procedure definition script USP_ExtranetCompanies for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtranetCompanies(v_numDomainID NUMERIC(18,0)
	,v_CurrentPage INTEGER
	,v_PageSize INTEGER       
	,v_columnName VARCHAR(100)        
	,v_columnSortOrder VARCHAR(100)
	,v_vcCustomSearch TEXT
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  TEXT;
BEGIN
   v_strSql := CONCAT('SELECT 
							COUNT(*) OVER() numTotalRecords
							,Ext.numExtranetID
							,ExtDTL.numExtranetDtlID
							,CONCAT(''<a href='',(CASE WHEN tintCRMType=2 THEN CONCAT(''../account/frmAccounts.aspx?DivID='',Div.numDivisionID,'''') WHEN tintCRMType=1 THEN CONCAT(''../prospects/frmProspects.aspx?DivID='',Div.numDivisionID,'''') ELSE CONCAT(''../Leads/frmLeads.aspx?DivID='',Div.numDivisionID,'''') END),''>'',COALESCE(vcCompanyName,''-''),''</a>'') vcCompanyName
							,COALESCE(A.vcFirstName,'''') vcFirstName
							,COALESCE(A.vcLastName,'''') vcLastName
							,COALESCE(A.vcEmail,'''') vcEmail
							,COALESCE(ExtDTL.vcPassword,'''') vcPassword
							,COALESCE(ExtDTL.tintAccessAllowed,0) tintAccessAllowed
							,COALESCE(ExtDTL.bitPartnerAccess,false) bitPartnerAccess
							,COALESCE((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',v_numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID),0) intLoginCount
							,CONCAT(''Contact~'',(SELECT numFieldID FROM DycFieldMaster WHERE vcLookBackTableName=''ExtranetAccountsDtl'' AND vcOrigDbColumnName=''vcPassword'' LIMIT 1),''~False~'',A.numContactID,''~'',Div.numDivisionID,''~0~0'') vcPasswordInlineEditID
						FROM 
							ExtarnetAccounts Ext      
						INNER JOIN
							ExtranetAccountsDtl ExtDTL 
						ON
							ExtDTL.numExtranetID=Ext.numExtranetID    
						INNER JOIN 
							DivisionMaster Div 
						ON 
							Div.numDivisionID=Ext.numDivisionID            
						INNER JOIN 
							CompanyInfo Com 
						ON 
							Com.numCompanyID=Ext.numCompanyID
						INNER JOIN 
							AdditionalContactsInformation A
						ON 
							A.numDivisionID = Ext.numDivisionID 
							AND CAST(A.numContactID AS VARCHAR) = ExtDTL.numContactID            
						WHERE 
							Ext.numDomainID=',v_numDomainID);
		
   IF LENGTH(coalesce(v_vcCustomSearch,'')) > 0 then
	
      v_strSql := CONCAT(v_strSql,' AND ',v_vcCustomSearch);
   end if;	
						
   IF coalesce(v_columnName,'') <> '' then
	
      IF v_columnName = 'Organization' then
		
         v_columnName := 'Com.vcCompanyName';
      ELSEIF v_columnName = 'FirstName'
      then
		
         v_columnName := 'A.vcFirstName';
      ELSEIF v_columnName = 'LastName'
      then
		
         v_columnName := 'A.vcLastName';
      ELSEIF v_columnName = 'EmailAddress'
      then
		
         v_columnName := 'A.vcEmail';
      ELSEIF v_columnName = 'LoginActivity'
      then
		
         v_columnName := CONCAT('COALESCE((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',v_numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID),0)');
      end if;
      v_strSql := CONCAT(v_strSql,' ORDER BY ',v_columnName,' ',v_columnSortOrder);
   ELSE
      v_strSql := CONCAT(v_strSql,' ORDER BY vcCompanyName ASC');
   end if;

   v_strSql := CONCAT(v_strSql,' OFFSET ',((v_CurrentPage::bigint -1)*v_PageSize::bigint),' ROWS FETCH NEXT ',
   v_PageSize,' ROWS ONLY');

   RAISE NOTICE '%',CAST(v_strSql AS TEXT);

   open SWV_RefCur for EXECUTE v_strSql;
   RETURN;
END; $$;


