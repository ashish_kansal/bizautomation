-- Stored procedure definition script usp_GetCustRptOrgContactCustomFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCustRptOrgContactCustomFields(v_numContactTypeId NUMERIC,  
 v_numRelationTypeId NUMERIC ,  
 v_numDomainId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numContactTypeId <> 0 AND  v_numRelationTypeId <> 0 then
 
      open SWV_RefCur for
      SELECT 1 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
      WHERE
      m.Grp_id = 1 --Prospect/ Accounts  
      AND m.numDomainID = v_numDomainId
      AND d.numRelation = v_numRelationTypeId
      UNION
      SELECT 2 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
      WHERE m.Grp_id = 4 --Contact Details 
      AND m.numDomainID = v_numDomainId
      AND d.numRelation = v_numContactTypeId
      ORDER BY 4;
   ELSEIF v_numContactTypeId = 0 AND  v_numRelationTypeId = 0
   then
 
      open SWV_RefCur for
      SELECT 1 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
      WHERE m.Grp_id = 1 --Prospect/ Accounts
      AND m.numDomainID = v_numDomainId
      UNION
      SELECT 2 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d  ON m.Fld_id = d.numFieldId
      WHERE m.Grp_id = 4 --Contact Details
      AND m.numDomainID = v_numDomainId
      ORDER BY 4;
   ELSEIF v_numContactTypeId <> 0
   then
      open SWV_RefCur for
      SELECT 2 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
      WHERE
      m.Grp_id = 4 --Contact Details  
      AND m.numDomainID = v_numDomainId
      ORDER BY d.numOrder;
   ELSEIF v_numRelationTypeId <> 0
   then
      open SWV_RefCur for
      SELECT 1 AS numFieldGroupID, 'CFld' || CAST(m.Fld_id AS VARCHAR(30)) AS vcDbFieldName, m.FLd_label AS vcScrFieldName, d.numOrder
      AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
      FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
      WHERE
      m.Grp_id = 1 --Prospect/ Accounts  
      AND d.numRelation = v_numRelationTypeId
      AND m.numDomainID = v_numDomainId
      ORDER BY d.numOrder;
   end if;
   RETURN;
END; $$;


