-- Stored procedure definition script USP_GetWarehouseLocation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWarehouseLocation(v_numDomainID NUMERIC(9,0) ,
    v_numWarehouseID NUMERIC(8,0),
    v_numWLocationID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  numWLocationID ,
                numWarehouseID ,
                vcAisle ,
                vcRack ,
                vcShelf ,
                vcBin ,
                vcLocation ,
                bitDefault ,
                bitSystem,
                intQty
   FROM    WarehouseLocation
   WHERE   (numWarehouseID = v_numWarehouseID   OR coalesce(v_numWarehouseID,0) = 0)
   AND numDomainID = v_numDomainID
   AND (numWLocationID = v_numWLocationID OR coalesce(v_numWLocationID,0) = 0);
END; $$;
 