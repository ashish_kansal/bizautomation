-- Stored procedure definition script usp_GetLeadOwnerFromCurcularDistList for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetLeadOwnerFromCurcularDistList(v_numRoutId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmpId  INTEGER;
   v_count  SMALLINT;  
   v_intAssignOrder  INTEGER;
BEGIN
   IF(Select bitDefault from RoutingLeads where numRoutID = v_numRoutId) = true then
 
      select   numEmpId INTO v_numEmpId from RoutingLeadDetails where  numRoutID = v_numRoutId;
   ELSE
      select   count(*) INTO v_count from CircularDistList where numRoutID = v_numRoutId;
      if v_count = 0 then

         select   numEmpId INTO v_numEmpId FROM RoutingLeadDetails WHERE numRoutID = v_numRoutId and intAssignOrder = 1;
         insert into CircularDistList(numRoutID,numLastRecOwner) values(v_numRoutId,1);
      else
         select   numLastRecOwner INTO v_intAssignOrder from CircularDistList where numRoutID = v_numRoutId;
         delete from CircularDistList where numRoutID = v_numRoutId;
         select   count(*) INTO v_count from RoutingLeadDetails where numRoutID = v_numRoutId and intAssignOrder =(v_intAssignOrder::bigint+1);
         if v_count = 0 then

            select   numEmpId INTO v_numEmpId FROM RoutingLeadDetails WHERE numRoutID = v_numRoutId and intAssignOrder = 1;
            insert into CircularDistList(numRoutID,numLastRecOwner) values(v_numRoutId,1);
         else
            select   numEmpId INTO v_numEmpId FROM RoutingLeadDetails WHERE numRoutID = v_numRoutId and intAssignOrder =(v_intAssignOrder::bigint+1);
            insert into CircularDistList(numRoutID,numLastRecOwner) values(v_numRoutId,(v_intAssignOrder::bigint+1));
         end if;
      end if;
   end if;      
   open SWV_RefCur for SELECT  v_numEmpId;
END; $$;












