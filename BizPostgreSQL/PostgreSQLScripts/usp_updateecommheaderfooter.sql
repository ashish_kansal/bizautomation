-- Stored procedure definition script USP_UpdateEcommHeaderFooter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateEcommHeaderFooter(v_numDomainId NUMERIC(9,0) DEFAULT 0,    
v_strText TEXT DEFAULT NULL,    
v_byteMode SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
    
    
    
    
-- if @byteMode=0    
--  update eCommerceDTL set txtHeader=@strText where numDomainID=@numDomainId    
-- if @byteMode=1    
--  update eCommerceDTL set txtFooter=@strText where numDomainID=@numDomainId    
   if v_byteMode = 2 then
      update DynamicFormMasterParam set ntxtHeader = v_strText where numDomainID = v_numDomainId and   numFormID = 3;
   end if;  
   if v_byteMode = 3 then
      update DynamicFormMasterParam set ntxtFooter = v_strText where numDomainID = v_numDomainId   and   numFormID = 3;
   end if;  
   if v_byteMode = 4 then
      update DynamicFormMasterParam set ntxtLeft = v_strText where numDomainID = v_numDomainId   and   numFormID = 3;
   end if;  
   if v_byteMode = 5 then
      update DynamicFormMasterParam set ntxtRight = v_strText where numDomainID = v_numDomainId   and   numFormID = 3;
   end if;
   if v_byteMode = 6 then
      update DynamicFormMasterParam set ntxtStyle = v_strText where numDomainID = v_numDomainId   and   numFormID = 3;
   end if;
   RETURN;
END; $$;


