-- Stored procedure definition script USP_Project_IsTaskStarted for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Project_IsTaskStarted(v_numDomainID NUMERIC(18,0)                            
	,v_numProId NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT
   SPDT.numTaskId
   FROM
   StagePercentageDetailsTask SPDT
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND SPDT.numProjectId = v_numProId
   AND coalesce((SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0) > 0) then
	
      open SWV_RefCur for
      SELECT 1;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;





