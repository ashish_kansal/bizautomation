-- Stored procedure definition script USP_GetJournalIdForAuthorizativeBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetJournalIdForAuthorizativeBizDocs(v_numOppId NUMERIC(9,0) DEFAULT 0,  
  v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJH.numJOurnal_Id as numJournalId from General_Journal_Header GJH
   Where GJH.numOppId = v_numOppId And GJH.numOppBizDocsId = v_numOppBizDocsId;
--	and numBizDocsPaymentDetId is null
-- Commented by chintan 
END; $$;












