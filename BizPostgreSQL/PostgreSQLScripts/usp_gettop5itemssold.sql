CREATE OR REPLACE FUNCTION usp_GetTop5ItemsSold(               
v_numDomainID NUMERIC,          
 v_dtDateFrom TIMESTAMP,          
 v_dtDateTo TIMESTAMP,          
 v_numTerID NUMERIC DEFAULT 0,          
 v_numUserCntID NUMERIC DEFAULT 0,          
 v_tintRights SMALLINT DEFAULT 3,          
 v_intType NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 3 then --All Records          
 
      IF v_intType = 0 then
  
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode = OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
         WHERE
         IT.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM
         AdditionalContactsInformation AI
         INNER JOIN
         OpportunityMaster OM
         ON
         AI.numContactId = OM.numrecowner
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         INNER JOIN
         Item IT
         ON
         IT.numItemCode = OI.numItemCode
         WHERE
         IT.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND AI.numTeam is not null
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;          
          
   IF v_tintRights = 2 then --Territory Records          
 
      IF v_intType = 0 then
  
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode = OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         WHERE IT.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM
         AdditionalContactsInformation AI
         INNER JOIN
         OpportunityMaster OM
         ON
         AI.numContactId = OM.numCreatedBy
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         INNER JOIN
         Item IT
         ON
         IT.numItemCode = OI.numItemCode
         INNER JOIN
         DivisionMaster DM
         ON
         DM.numDivisionID = OM.numDivisionId
         WHERE IT.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numOppId = OI.numOppId
         AND AI.numTeam is not null
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;          
          
   IF v_tintRights = 1 then --Owner Records          
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode = OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
         WHERE IT.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numrecowner = v_numUserCntID
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  IT.vcItemName, SUM(OI.monTotAmount) AS Amount
         FROM AdditionalContactsInformation AI
         INNER JOIN
         OpportunityMaster OM
         ON
         AI.numContactId = OM.numrecowner
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         INNER JOIN
         Item IT
         ON
         IT.numItemCode = OI.numItemCode
         WHERE IT.numDomainID = v_numDomainID
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numrecowner = v_numUserCntID
         AND AI.numTeam is not null
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY IT.vcItemName
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;
   RETURN;
END; $$;


