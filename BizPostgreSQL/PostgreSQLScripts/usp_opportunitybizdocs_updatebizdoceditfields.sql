-- Stored procedure definition script USP_OpportunityBizDocs_UpdateBizDocEditFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_UpdateBizDocEditFields(v_numOppBizDocsId NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numBizDocTempID NUMERIC(18,0)
	,v_dtFromDate TIMESTAMP
	,v_numShipVia NUMERIC(18,0)
	,v_numSequenceId VARCHAR(50)
	,v_vcTrackingNo VARCHAR(500)	
	,v_vcRefOrderNo VARCHAR(300)
	,v_numBizDocStatus NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   OpportunityBizDocs
   SET
   numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
   numBizDocTempID = v_numBizDocTempID,dtFromDate = v_dtFromDate,numShipVia = v_numShipVia,
   numSequenceId = v_numSequenceId,vcTrackingNo = v_vcTrackingNo,
   vcRefOrderNo = v_vcRefOrderNo,numBizDocStatus = v_numBizDocStatus
   WHERE
   numOppBizDocsId = v_numOppBizDocsId;
   RETURN;
END; $$;


