-- Stored procedure definition script usp_GetAllSelectedSystemModulesPagesAndAccessesReport for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAllSelectedSystemModulesPagesAndAccessesReport(v_numModuleID NUMERIC(9,0) DEFAULT 0,              
 v_numGroupID NUMERIC(9,0) DEFAULT 0                 
--              
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from ReportList where rptGroup = 0 AND bitEnable = true ORDER BY rptSequence;
END; $$;












