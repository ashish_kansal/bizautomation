-- Stored procedure definition script USP_GetRatings for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRatings(v_tintTypeId INTEGER DEFAULT 0,       
v_vcReferenceId	 VARCHAR(100) DEFAULT '', 
v_numContactId NUMERIC(18,0) DEFAULT NULL ,
v_numSiteId NUMERIC(18,0) DEFAULT NULL ,
v_numDomainId NUMERIC(18,0) DEFAULT NULL ,
v_Mode INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_Mode = 1 then
		
      open SWV_RefCur for
      SELECT intRatingCount FROM Ratings WHERE tintTypeId = v_tintTypeId AND vcReferenceId = v_vcReferenceId AND numContactId = v_numContactId AND numSiteID = v_numSiteId AND numDomainID = v_numDomainId;
   ELSEIF v_Mode = 2
   then
        
      open SWV_RefCur for
      SELECT AVG(CAST(intRatingCount AS NUMERIC(9,0))) AS AvgRatings FROM Ratings WHERE tintTypeId = v_tintTypeId AND vcReferenceId = v_vcReferenceId AND  numSiteID = v_numSiteId  AND numDomainID = v_numDomainId;
   ELSEIF v_Mode = 3
   then --To get Rating Count ...Display on Product Page ...
	    
      open SWV_RefCur for
      SELECT COUNT(*) AS RatingCount FROM Ratings WHERE tintTypeId = v_tintTypeId AND vcReferenceId = v_vcReferenceId AND  numSiteID = v_numSiteId AND numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;



