-- Stored procedure definition script USP_PlanningProcurement for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PlanningProcurement(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numVendorID NUMERIC(9,0) DEFAULT 0,
    v_ItemClassification NUMERIC(9,0) DEFAULT 0,
    v_OrderStatus NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
           
   open SWV_RefCur for SELECT  cast(txtItemDesc as VARCHAR(255)),
 cast(vcPathForTImage as VARCHAR(255)),
 charItemType,
 cast(numWarehouseItemID as VARCHAR(255)),
 cast(numItemCode as VARCHAR(255)),
 vcItemName,
 cast(vcWareHouse as VARCHAR(255)),
 cast(vcModelID as VARCHAR(255)),
 cast(numOnHand as VARCHAR(255)),
 cast(numOnOrder as VARCHAR(255)),
 cast(numReorder as VARCHAR(255)),
 cast(numAllocation as VARCHAR(255)),
 cast(numBackOrder as VARCHAR(255)),
 cast(numVendorID as VARCHAR(255)),cast(SUM(A.Units) as VARCHAR(255)) AS Units,bitAssembly FROM(SELECT
      cast(txtItemDesc as VARCHAR(255)),
            --kishan
            cast((SELECT  cast(vcPathForTImage as VARCHAR(255))
         FROM  ItemImages
         WHERE numItemCode = I.numItemCode AND bitDefault = true
         AND numDomainId = v_numDomainID LIMIT 1) as VARCHAR(255))
      AS vcPathForTImage,
			CAST('Inventory Item' AS CHAR(14)) AS charItemType,
            cast(WI.numWareHouseItemID as VARCHAR(255)),
            cast(I.numItemCode as VARCHAR(255)),
            vcItemName || ' - '
      || coalesce(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN),'''') AS vcItemName,
            cast(vcWareHouse as VARCHAR(255)),
            cast(vcModelID as VARCHAR(255)),
            cast(coalesce(numOnHand,0) as VARCHAR(255)) AS numOnHand,
            cast(coalesce(numOnOrder,0) as VARCHAR(255)) AS numOnOrder,
            cast(coalesce(numReorder,0) as VARCHAR(255)) AS numReorder,
            cast(coalesce(numAllocation,0) as VARCHAR(255)) AS numAllocation,
            cast(coalesce(numBackOrder,0) as VARCHAR(255)) AS numBackOrder,
            cast(I.numVendorID as VARCHAR(255)),
            cast(SUM(coalesce(numAllocation,0)+coalesce(numBackOrder,0)) as VARCHAR(255)) AS Units,CASE WHEN (I.bitKitParent = true AND I.bitAssembly = true) THEN 1 ELSE 0 END as bitAssembly
      FROM    Item I
      INNER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID
      LEFT JOIN Vendor V ON V.numItemCode = I.numItemCode
      JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
      WHERE    ((bitKitParent = false
      OR bitKitParent IS NULL) OR (coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true))
      AND charItemType = 'P'
      AND ( ((coalesce(numBackOrder,0) -coalesce(numOnOrder,0)) > 0 AND v_OrderStatus IN(0,1))
      OR
                  (coalesce(numOnHand,0)+coalesce(numOnOrder,0) <= coalesce(numReorder,0)
      AND coalesce(numReorder,0) > 0 AND v_OrderStatus IN(0,2))
--                  OR numOnhand <= ISNULL(numReorder, 0) + ISNULL(numOnOrder, 0)
                )
      AND I.numDomainID = v_numDomainID
      AND (v_numVendorID = 0
      OR V.numItemCode IN(SELECT    cast(numItemCode as VARCHAR(255))
         FROM      Vendor
         WHERE     numVendorID = v_numVendorID)) AND (I.numItemClassification = v_ItemClassification OR v_ItemClassification = 0)
      GROUP BY WI.numWareHouseItemID,I.numItemCode,I.numVendorID,I.bitAssembly,I.bitKitParent) A
   GROUP BY
 txtItemDesc,
 vcPathForTImage,
 charItemType,
 numWarehouseItemID,
 numItemCode,
 vcItemName,
 vcWareHouse,
 vcModelID,
 numOnHand,
 numOnOrder,
 numReorder,
 numAllocation,
 numBackOrder,
 numVendorID,bitAssembly
   ORDER BY vcWareHouse,vcItemName;

/****** Object:  StoredProcedure [dbo].[USP_PopulateCustomFlds]    Script Date: 07/26/2008 16:20:31 ******/
   RETURN;
END; $$;












