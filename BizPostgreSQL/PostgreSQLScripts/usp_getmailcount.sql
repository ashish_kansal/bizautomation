-- Stored procedure definition script Usp_GetMailCount for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetMailCount(v_numDomainID	NUMERIC(10,0),
	v_numUserCntID	NUMERIC(10,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COUNT(*) FROM EmailHistory
   WHERE coalesce(bitIsRead,false) = false
   AND numDomainID = v_numDomainID
   AND numUserCntId = v_numUserCntID
   AND tintType = 1;
   RETURN;
END; $$;    













