-- Stored procedure definition script usp_GetContactsParComp for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactsParComp(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numContactId, vcFirstName || ' '  || vcLastname as vcGivenName From AdditionalContactsInformation
   join  UserMaster
   on   numuserdetailid = numContactId
   where UserMaster.numDomainID = v_numDomainID;
END; $$;












