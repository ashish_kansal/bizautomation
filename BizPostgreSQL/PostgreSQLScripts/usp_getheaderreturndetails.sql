-- Stored procedure definition script USP_GetHeaderReturnDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetHeaderReturnDetails(v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,
      v_numDomainId NUMERIC(9,0) DEFAULT NULL,
      v_tintReceiveType NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintType  SMALLINT;
   v_tintReturnType  SMALLINT;
   v_monEstimatedBizDocAmount  DECIMAL(20,5);
BEGIN
   v_tintType := 0;
        
   select   tintReturnType INTO v_tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = v_numReturnHeaderID;    
   
   v_tintType := CASE When v_tintReturnType = 1 AND v_tintReceiveType = 1 THEN 9
   When v_tintReturnType = 1 AND v_tintReceiveType = 2 THEN 7
   When v_tintReturnType = 1 AND v_tintReceiveType = 3 THEN 9
   When v_tintReturnType = 2 AND v_tintReceiveType = 2 THEN 8
   When v_tintReturnType = 3 THEN 10
   When v_tintReturnType = 4 THEN 9 END; 
				       
   select   monAmount+monTotalTax -monTotalDiscount INTO v_monEstimatedBizDocAmount FROM GetReturnDealAmount(v_numReturnHeaderID,v_tintType);
	
   open SWV_RefCur for SELECT  RH.numReturnHeaderID,
                vcRMA,
                coalesce(vcBizDocName,'') AS vcBizDocName,
                RH.numDomainID,
                coalesce(RH.numDivisionId,0) AS numDivisionId,
                coalesce(RH.numContactID,0) AS numContactId,
                coalesce(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                coalesce(monBizDocAmount,0) AS monBizDocAmount,coalesce(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				coalesce(I.vcModelID,'') AS vcModelID,
				coalesce(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                TO_CHAR(monPrice,'FM9,999,999,999,990.99999') AS monPrice,
				CAST((coalesce(monTotAmount,0)/RI.numUnitHour) AS DECIMAL(18,4)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWareHouseID,
                coalesce(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				coalesce(fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOM,
				coalesce(fn_GetUOMName(numUOMId),'') AS vcUOM,
                fn_GetListItemName(numReturnStatus) AS vcStatus,
                fn_GetContactName(RH.numCreatedBy) AS CreatedBy,
                fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                coalesce(C2.vcCompanyName,'')
   || CASE WHEN coalesce(D.numCompanyDiff,0) > 0
   THEN '  '
      || coalesce(fn_GetListItemName(D.numCompanyDiff),'') || ':' || coalesce(D.vcCompanyDiff,'')
   ELSE ''
   END AS vcCompanyname,
                coalesce(D.tintCRMType,0) AS tintCRMType,
                coalesce(numAccountID,0) AS numAccountID,
                coalesce(vcCheckNumber,'') AS vcCheckNumber,
                coalesce(IsCreateRefundReceipt,false) AS IsCreateRefundReceipt,
                (SELECT    vcData
      FROM      Listdetails
      WHERE     numListItemID = numReturnStatus) AS ReturnStatusName,
                (SELECT    vcData
      FROM      Listdetails
      WHERE     numListItemID = numReturnReason) AS ReturnReasonName,
                coalesce(con.vcEmail,'') AS vcEmail,
                coalesce(RH.numBizdocTempID,0) AS numBizDocTempID,
                case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as ItemType,
                charItemType,coalesce(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,coalesce(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,coalesce(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID = RI.numOppBizDocItemID),
      coalesce(I.monAverageCost,'0')) END) as AverageCost
                ,coalesce(RH.numItemCode,0) AS numItemCodeForAccount
				,coalesce(I.bitKitParent,false) AS bitKitParent
				,coalesce(I.bitAssembly,false) AS bitAssembly,
                CASE WHEN coalesce(RH.numOppId,0) <> 0 THEN vcpOppName
   ELSE ''
   END AS Source,coalesce(I.bitSerialized,false) AS bitSerialized,coalesce(I.bitLotNo,false) AS bitLotNo,
				SUBSTR((SELECT ',' || vcSerialNo || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || oppI.numQty || ')' ELSE '' end
      FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID = RH.numReturnHeaderID and oppI.numReturnItemID = RI.numReturnItemID
      ORDER BY vcSerialNo),2,200000) AS SerialLotNo
				,v_monEstimatedBizDocAmount AS monEstimatedBizDocAmount
				,coalesce(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef
				,coalesce(RH.numDepositIDRef,0) AS numDepositIDRef
				,coalesce(numOppBizDocID,0) AS numOppBizDocID
				, coalesce(((SELECT  CASE WHEN coalesce(DATE_PART('day',LOCALTIMESTAMP  -O.bintClosedDate),0) > 0 AND coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainId AND IC.numItemClassification::VARCHAR = C.vcItemClassification),0) > 0 THEN coalesce((SELECT vcNotes FROM Contracts AS C WHERE  numDomainId = v_numDomainId AND IC.numItemClassification::VARCHAR = C.vcItemClassification),'-') ELSE '-' END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item IC ON OI.numItemCode = IC.numItemCode WHERE
         O.numDivisionId = RH.numDivisionId AND O.numDomainId = v_numDomainId AND IC.numItemCode = I.numItemCode AND
         IC.bitTimeContractFromSalesOrder = true AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1)),
   '') AS vcWarrantyNotes
				, coalesce(((SELECT  CASE WHEN coalesce(DATE_PART('day',LOCALTIMESTAMP  -O.bintClosedDate),0) > 0 AND coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainId AND IC.numItemClassification::VARCHAR = C.vcItemClassification),0) > 0 THEN  coalesce((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId = v_numDomainId AND IC.numItemClassification::VARCHAR = C.vcItemClassification),0) -coalesce(DATE_PART('day',LOCALTIMESTAMP  -O.bintClosedDate),0) ELSE 0 END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId = OI.numOppId LEFT JOIN Item IC ON OI.numItemCode = IC.numItemCode WHERE
         O.numDivisionId = RH.numDivisionId AND O.numDomainId = v_numDomainId AND IC.numItemCode = I.numItemCode AND
         IC.bitTimeContractFromSalesOrder = true AND O.tintopptype = 1 AND O.tintoppstatus = 1 AND O.tintshipped = 1 ORDER BY O.bintCreatedDate DESC LIMIT 1)),
   0) AS numWarrantyDaysLeft
   FROM    ReturnHeader RH
   LEFT JOIN ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
   LEFT JOIN OpportunityMaster OM ON OM.numOppId = RH.numOppId AND RH.numDomainID = OM.numDomainId
   LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
   LEFT JOIN DivisionMaster D ON RH.numDivisionId = D.numDivisionID
   LEFT JOIN CompanyInfo C2 ON C2.numCompanyId = D.numCompanyID
   LEFT JOIN OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
   LEFT JOIN AdditionalContactsInformation con ON con.numDivisionId = RH.numDivisionId
   AND con.numContactId = RH.numContactID
   LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WItems.numWareHouseID
   WHERE  RH.numDomainID = v_numDomainId
   AND RH.numReturnHeaderID = v_numReturnHeaderID;
END; $$;
