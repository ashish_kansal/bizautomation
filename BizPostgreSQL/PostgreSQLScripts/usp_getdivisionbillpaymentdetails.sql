-- FUNCTION: public.usp_getdivisionbillpaymentdetails(numeric, numeric, integer, refcursor)

-- DROP FUNCTION public.usp_getdivisionbillpaymentdetails(numeric, numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getdivisionbillpaymentdetails(
	v_numdomainid numeric DEFAULT 0,
	v_numdivisionid numeric DEFAULT NULL::numeric,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:30:30 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT cast(oppbizdocspaydet.numbizdocspaymentdetailsid as VARCHAR(255)) AS numbizdocspaymentdetailsid,
           cast(oppbizdocsdet.numBizDocsPaymentDetId as VARCHAR(255)) AS numbizdocspaymentdetid,
           0 AS numoppid,
           cast(oppbizdocsdet.numDivisionID as VARCHAR(255)),
           cast(oppbizdocsdet.numBizDocsId as VARCHAR(255)) AS numbizdocsid,
           CASE WHEN LENGTH(oppbizdocsdet.vcReference) = 0 THEN 'Open bill' ELSE oppbizdocsdet.vcReference END AS NAME  ,
           cast(oppbizdocspaydet.monAmount as VARCHAR(255)) AS amount,
           fn_GetListItemName(oppbizdocsdet.numPaymentMethod) AS paymentmethod,
           fn_GetContactName(oppbizdocsdet.numCreatedBy)
   || ' - '
   || FormatedDateTimeFromDate(oppbizdocsdet.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId) AS createdby,
           cast(coalesce(oppbizdocsdet.vcMemo,'') as VARCHAR(255)) AS memo,
           cast(coalesce(oppbizdocsdet.vcReference,'') as VARCHAR(255)) AS reference,
           cast(c.numCompanyId as VARCHAR(255)) AS numcompanyid,
           cast(c.vcCompanyName as VARCHAR(255)) AS vccompanyname,
           CASE oppbizdocsdet.tintPaymentType
   WHEN 1 THEN 'Purchase Order'
   WHEN 2 THEN 'Bill'
   WHEN 4 THEN 'Bill (liability)'
   WHEN 3 THEN 'Sales Returns'
   WHEN 5 THEN 'Commission Expense'
   ELSE '-'
   END AS PaymentType,
           cast(coalesce(oppbizdocsdet.numReturnID,0) as VARCHAR(255)) AS numReturnID,
			cast(oppbizdocsdet.numCurrencyID as VARCHAR(255)),
			cast(oppbizdocsdet.fltExchangeRate as VARCHAR(255)),
			oppbizdocspaydet.monAmount*oppbizdocsdet.fltExchangeRate as ConAmt,
			cast(tintPaymentType as VARCHAR(255)),
			0 AS fltTransactionCharge,
			0 AS TransactionChargeAccountID,
			FormatedDateFromDate(oppbizdocspaydet.dtDueDate,v_numDomainId) AS DueDate,
			CAST('' AS CHAR(1)) AS vcRefOrderNo,
			CAST('' AS CHAR(1)) AS EmployerName,
			cast(coalesce(numCommissionID,0) as VARCHAR(255)) AS numCommissionID
   FROM   OpportunityBizDocsDetails oppbizdocsdet
   INNER JOIN OpportunityBizDocsPaymentDetails oppbizdocspaydet
   ON oppbizdocsdet.numBizDocsPaymentDetId = oppbizdocspaydet.numBizDocsPaymentDetId
   INNER JOIN DivisionMaster Div
   ON oppbizdocsdet.numDivisionID = Div.numDivisionID
   INNER JOIN CompanyInfo c
   ON Div.numCompanyID = c.numCompanyId
   WHERE  oppbizdocsdet.numDomainId = v_numDomainId
   AND oppbizdocsdet.numDivisionID =	v_numDivisionId
   AND oppbizdocsdet.bitAuthoritativeBizDocs = TRUE
           --AND OppBizDocsPayDet.bitIntegrated = 0
   AND oppbizdocsdet.numBizDocsId = 0
   AND oppbizdocsdet.tintPaymentType = 2;
END;
$BODY$;

ALTER FUNCTION public.usp_getdivisionbillpaymentdetails(numeric, numeric, integer, refcursor)
    OWNER TO postgres;
