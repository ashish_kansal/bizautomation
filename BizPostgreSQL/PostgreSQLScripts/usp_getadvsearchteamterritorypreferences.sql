-- Stored procedure definition script usp_getAdvSearchTeamTerritoryPreferences for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getAdvSearchTeamTerritoryPreferences(v_numDomainID NUMERIC,          
 v_numUserCntID NUMERIC,        
 v_vcTerritoryTeamFlag VARCHAR(5), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcTerritoriesSelected  VARCHAR(2000);        
   v_vcTeamsSelected  VARCHAR(2000);
   v_iTeamDoc  INTEGER;
   v_iTerrDoc  INTEGER;
BEGIN
   select   vcTeamsSelected, vcTerritoriesSelected INTO v_vcTeamsSelected,v_vcTerritoriesSelected FROM AdvSearchTeamTerritoryPreferences Where numUserCntId = v_numUserCntID And numDomainID = v_numDomainID;        
   IF v_vcTerritoryTeamFlag = 'Teams' then
 
      If LTrim(v_vcTeamsSelected) = '' then
         open SWV_RefCur for
         select numListItemId as numTeamId, vcData as vcTeamName from UserTeams, Listdetails
         where 1 = 2;
      Else
         SELECT * INTO v_iTeamDoc FROM SWF_Xml_PrepareDocument(v_vcTeamsSelected);
         open SWV_RefCur for
         SELECT numTeamId as numListItemId, vcData FROM SWF_OpenXml(v_iTeamDoc,'/Teams/Team','numTeamId |vcTeamName') st(numTeamId NUMERIC,vcTeamName VARCHAR(50)), UserTeams ut, Listdetails ld
         WHERE st.numTeamId = ut.numTeam
         AND ld.numListItemID = ut.numTeam
         AND ut.numUserCntID = v_numUserCntID
         AND ut.numDomainID = v_numDomainID
         AND ld.numDomainid = ut.numDomainID;
         PERFORM SWF_Xml_RemoveDocument(v_iTeamDoc);
      end if;
   ELSE
      If LTrim(v_vcTerritoriesSelected) = '' then
         open SWV_RefCur for
         select numTerID as numTerritoryID, vcTerName from TerritoryMaster
         where 1 = 2;
      Else
         SELECT * INTO v_iTerrDoc FROM SWF_Xml_PrepareDocument(v_vcTerritoriesSelected);
         open SWV_RefCur for
         SELECT st.numTerId as numTerritoryID, tm.vcTerName FROM SWF_OpenXml(v_iTerrDoc,'/Territories/Territory','numTerId |vcTerName') st(numTerId NUMERIC,vcTerName VARCHAR(50)), TerritoryMaster tm
         WHERE st.numTerId = tm.numTerID
         AND tm.numDomainid = v_numDomainID;
         PERFORM SWF_Xml_RemoveDocument(v_iTerrDoc);
      end if;
   end if;
   RETURN;
END; $$;


