DROP FUNCTION IF EXISTS USP_AdvancedSearchFinancialSearch;

CREATE OR REPLACE FUNCTION USP_AdvancedSearchFinancialSearch(v_WhereCondition VARCHAR(4000) DEFAULT '',                              
v_ViewID SMALLINT DEFAULT NULL,                              
v_numDomainID NUMERIC(9,0) DEFAULT 0,                              
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                              
v_numGroupID NUMERIC(9,0) DEFAULT 0,                              
v_CurrentPage INTEGER DEFAULT NULL,                                                                    
v_PageSize INTEGER DEFAULT NULL,
INOUT v_TotRecs INTEGER  DEFAULT NULL, 
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                              
v_ColumnName VARCHAR(20) DEFAULT '', 
v_SortCharacter CHAR(1) DEFAULT NULL,                        
v_SortColumnName VARCHAR(20) DEFAULT '',  
v_LookTable VARCHAR(20) DEFAULT '' ,        
v_GetAll BOOLEAN DEFAULT NULL, 
INOUT SWV_RefCur refcursor DEFAULT NULL 
)
LANGUAGE plpgsql
AS $$
   DECLARE
   v_tintOrder  SMALLINT;
   v_vcFormFieldName  VARCHAR(100);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(3);
   v_vcAssociatedControlType  VARCHAR(10);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(20);
   v_ColumnSearch  VARCHAR(10);
   v_vcLookBackTableName  VARCHAR(100);
   v_WCondition  VARCHAR(1000);

   v_strSql TEXT;                                                              
   v_firstRec  INTEGER;                                                                    
   v_lastRec  INTEGER;
BEGIN
	v_WhereCondition := regexp_replace(v_WhereCondition,'CH.numCheckNo ILIKE','CH.numCheckNo::TEXT ILIKE','gi');
	v_WhereCondition := regexp_replace(v_WhereCondition,'CH.numCheckNo NOT ILIKE','CH.numCheckNo::TEXT NOT ILIKE','gi');
	v_WhereCondition := regexp_replace(v_WhereCondition,'GJD.numCreditAmt ILIKE','GJD.numCreditAmt::TEXT ILIKE','gi');
	v_WhereCondition := regexp_replace(v_WhereCondition,'GJD.numCreditAmt NOT ILIKE','GJD.numCreditAmt::TEXT NOT ILIKE','gi');
	v_WhereCondition := regexp_replace(v_WhereCondition,'GJD.numDebitAmt ILIKE','GJD.numDebitAmt::TEXT ILIKE','gi');
	v_WhereCondition := regexp_replace(v_WhereCondition,'GJD.numDebitAmt NOT ILIKE','GJD.numDebitAmt::TEXT NOT ILIKE','gi');

   if (v_SortCharacter <> '0' and v_SortCharacter <> '') then 
      v_ColumnSearch := v_SortCharacter;
   end if;   
   
   DROP TABLE IF EXISTS tt_TEMPTABLEFS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLEFS 
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)   PRIMARY KEY,
      numJournalId NUMERIC(18,0),
      numTransactionId NUMERIC(18,0)
   );

   v_strSql := 'Select GJD.numJournalId,GJD.numTransactionId                    
  FROM General_Journal_Header GJH
  JOIN General_Journal_Details GJD on GJH.numJournal_Id = GJD.numJournalId
   LEFT OUTER JOIN(DivisionMaster DM                                               
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId) ON GJD.numCustomerId = DM.numDivisionID    
  LEFT OUTER JOIN CheckHeader CH ON (GJH.numCheckHeaderID = CH.numCheckHeaderID or (GJH.numBillPaymentID = CH.numReferenceID and CH.tintReferenceType = 8))'; 

   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then
  
      select vcListItemType, numListID INTO v_vcListItemType,v_numListID from View_DynamicDefaultColumns where vcDbColumnName = v_ColumnName and numFormId = 1 and numDomainID = v_numDomainID;
      if v_vcListItemType = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
      end if;
   end if;       
           
                      
   if (v_SortColumnName <> '') then
  
      select   vcListItemType, numListID INTO v_vcListItemType1,v_numListID from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 1 and numDomainID = v_numDomainID;
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      end if;
   end if;    
                              
   v_strSql := coalesce(v_strSql,'') || ' where GJH.numDomainID  = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(15)),1,15) ||   coalesce(v_WhereCondition,'');                                  
                 
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then

      if v_vcListItemType = 'LI' then
         v_strSql := coalesce(v_strSql,'') || ' and L1.vcData ilike ''' || coalesce(v_ColumnSearch,'') || '%''';
      else
         v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_ColumnName,'') || ' ilike ''' || coalesce(v_ColumnSearch,'')  || '%''';
      end if;
   end if;
                          
   if (v_SortColumnName <> '') then
  
      if v_vcListItemType1 = 'LI' then
		
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      else
         v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' order by  GJH.datCreatedDate asc ';
   end if;
	  
	  
   RAISE NOTICE '%',v_strSql;

   EXECUTE 'INSERT INTO tt_TEMPTABLEFS (numJournalId,numTransactionId)
	' || v_strSql;
	
   v_strSql := '';


   v_tintOrder := 0;

   v_strSql := 'select numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                FormatedDateFromDate(datEntry_Date,numDomainID) AS Date,
                varDescription,
                coalesce(BizPayment,'''') || '' '' || coalesce(CheqNo,'''') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CAST(coalesce(numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt,
                CAST(coalesce(numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt,
                vcAccountName,
                '''' AS balance,
                numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,fn_GetListItemName(numClassID) as vcClass,
				numLandedCostOppId ';                              
                                                                           
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);     
                                          
   SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLEFS;
   
   if v_GetAll = false then

      v_strSql := coalesce(v_strSql,'') || 'FROM (SELECT general_journal_header.numjournal_id,
    general_journal_header.datentry_date,
    general_journal_header.vardescription,
    COALESCE(fn_GetPaymentBizDoc(OpportunityBizDocsDetails.numBizDocsPaymentDetId, OpportunityBizDocsDetails.numDomainId::INT), general_journal_header.vardescription) AS bizpayment,
    general_journal_details.numtransactionid,
        CASE
            WHEN COALESCE(general_journal_header.numreturnid, 0::numeric) <> 0::numeric THEN '' Chek No: ''::text || COALESCE(ch.numcheckno::character varying(50), ''No-Cheq''::character varying)::text
            ELSE view_journalcheq.narration
        END AS cheqno,
    general_journal_header.numdomainid,
    ''Biz Doc Id: ''::text || opportunitybizdocs.vcbizdocid::text AS bizdocid,
    general_journal_header.numoppbizdocsid,
    general_journal_details.vcreference AS tranref,
    general_journal_details.vardescription AS trandesc,
    general_journal_details.numdebitamt,
    general_journal_details.numcreditamt,
    chart_of_accounts.numaccountid,
    chart_of_accounts.vcaccountname,
        CASE
            WHEN COALESCE(general_journal_header.numcheckheaderid, 0::numeric) <> 0::numeric THEN ''Checks''::text
            WHEN COALESCE(general_journal_header.numcashcreditcardid, 0::numeric) <> 0::numeric AND cashcreditcarddetails.bitmoneyout = false THEN ''Cash''::text
            WHEN COALESCE(general_journal_header.numcashcreditcardid, 0::numeric) <> 0::numeric AND cashcreditcarddetails.bitmoneyout = true AND cashcreditcarddetails.bitchargeback = true THEN ''Charge''::text
            WHEN COALESCE(general_journal_header.numdepositid, 0::numeric) <> 0::numeric AND dm.tintdepositepage = 1 THEN ''Deposit''::text
            WHEN COALESCE(general_journal_header.numdepositid, 0::numeric) <> 0::numeric AND dm.tintdepositepage = 2 THEN ''Receved Pmt''::text
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numbizdocspaymentdetid, 0::numeric) = 0::numeric AND opportunitymaster.tintopptype = 1 THEN ''BizDocs Invoice''::text
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numbizdocspaymentdetid, 0::numeric) = 0::numeric AND opportunitymaster.tintopptype = 2 THEN ''BizDocs Purchase''::text
            WHEN COALESCE(general_journal_header.numcategoryhdrid, 0::numeric) <> 0::numeric THEN ''Time And Expenses''::text
            WHEN COALESCE(general_journal_header.numbillid, 0::numeric) <> 0::numeric AND COALESCE(bh.bitlandedcost, false) = true THEN ''Landed Cost''::text
            WHEN COALESCE(general_journal_header.numbillid, 0::numeric) <> 0::numeric THEN ''Bill''::text
            WHEN COALESCE(general_journal_header.numbillpaymentid, 0::numeric) <> 0::numeric THEN ''Pay Bill''::text
            WHEN COALESCE(general_journal_header.numreturnid, 0::numeric) <> 0::numeric THEN
            CASE
                WHEN rh.tintreturntype = 1 AND rh.tintreceivetype = 1 THEN ''Sales Return Refund''::text
                WHEN rh.tintreturntype = 1 AND rh.tintreceivetype = 2 THEN ''Sales Return Credit Memo''::text
                WHEN rh.tintreturntype = 2 AND rh.tintreceivetype = 2 THEN ''Purchase Return Credit Memo''::text
                WHEN rh.tintreturntype = 3 AND rh.tintreceivetype = 2 THEN ''Credit Memo''::text
                WHEN rh.tintreturntype = 4 AND rh.tintreceivetype = 1 THEN ''Refund Against Credit Memo''::text
                ELSE NULL::text
            END
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) = 0::numeric THEN ''PO Fulfillment''::text
            WHEN general_journal_header.numjournal_id <> 0::numeric THEN ''Journal''::text
            ELSE NULL::text
        END AS transactiontype,
    companyinfo.vccompanyname AS companyname,
    general_journal_header.numcheckheaderid,
    general_journal_header.numcashcreditcardid,
    general_journal_header.numoppid,
    general_journal_header.numdepositid,
    general_journal_header.numcategoryhdrid,
    timeandexpense.tinttetype,
    timeandexpense.numcategory,
    timeandexpense.dtfromdate,
    timeandexpense.numusercntid,
    divisionmaster.numdivisionid,
    COALESCE(general_journal_details.bitcleared, false) AS bitcleared,
    COALESCE(general_journal_details.bitreconcile, false) AS bitreconcile,
    COALESCE(general_journal_header.numbillid, 0::numeric) AS numbillid,
    COALESCE(general_journal_header.numbillpaymentid, 0::numeric) AS numbillpaymentid,
    COALESCE(general_journal_details.numclassid, 0::numeric) AS numclassid,
    COALESCE(general_journal_details.numprojectid, 0::numeric) AS numprojectid,
    COALESCE(general_journal_header.numreturnid, 0::numeric) AS numreturnid,
    general_journal_details.numcurrencyid,
    COALESCE(general_journal_details.numitemid, 0::numeric) AS numitemid,
        CASE
            WHEN COALESCE(bh.bitlandedcost, false) = true THEN COALESCE(bh.numoppid, 0::numeric)
            ELSE 0::numeric
        END AS numlandedcostoppid,
	T.ID
   FROM general_journal_header
     JOIN general_journal_details ON general_journal_header.numjournal_id = general_journal_details.numjournalid
     JOIN chart_of_accounts ON general_journal_details.numchartacntid = chart_of_accounts.numaccountid
	 JOIN tt_TEMPTABLEFS T ON general_journal_header.numjournal_id = T.numjournalid AND general_journal_details.numtransactionid = T.numtransactionid
	LEFT JOIN OpportunityBizDocsDetails ON OpportunityBizDocsDetails.numbizdocspaymentdetid = general_journal_header.numbizdocspaymentdetid
     LEFT JOIN timeandexpense ON general_journal_header.numcategoryhdrid = timeandexpense.numcategoryhdrid
     LEFT JOIN (divisionmaster
     LEFT JOIN companyinfo ON divisionmaster.numcompanyid = companyinfo.numcompanyid) ON general_journal_details.numcustomerid = divisionmaster.numdivisionid
     LEFT JOIN opportunitymaster ON general_journal_header.numoppid = opportunitymaster.numoppid
     LEFT JOIN cashcreditcarddetails ON general_journal_header.numcashcreditcardid = cashcreditcarddetails.numcashcreditid
     LEFT JOIN opportunitybizdocs ON general_journal_header.numoppbizdocsid = opportunitybizdocs.numoppbizdocsid
     LEFT JOIN view_journalcheq ON general_journal_header.numcheckheaderid = view_journalcheq.numcheckheaderid OR general_journal_header.numbillpaymentid = view_journalcheq.numreferenceid AND view_journalcheq.tintreferencetype = 8
     LEFT JOIN depositmaster dm ON dm.numdepositid = general_journal_header.numdepositid
     LEFT JOIN returnheader rh ON rh.numreturnheaderid = general_journal_header.numreturnid
     LEFT JOIN checkheader ch ON ch.numreferenceid = rh.numreturnheaderid AND ch.tintreferencetype = 10
     LEFT JOIN billheader bh ON COALESCE(general_journal_header.numbillid, 0::numeric) = bh.numbillid WHERE T.ID > ' || SUBSTR(CAST(COALESCE(v_firstRec, 0) AS VARCHAR(10)),1,10) || ' AND T.ID <' || SUBSTR(CAST(COALESCE(v_lastRec, 0) AS VARCHAR(10)),1,10) || ') TEMP ORDER BY ID';
   else
      v_strSql := coalesce(v_strSql,'') || 'FROM (SELECT general_journal_header.numjournal_id,
    general_journal_header.datentry_date,
    general_journal_header.vardescription,
    COALESCE(fn_GetPaymentBizDoc(OpportunityBizDocsDetails.numBizDocsPaymentDetId, OpportunityBizDocsDetails.numDomainId::INT), general_journal_header.vardescription) AS bizpayment,
    general_journal_details.numtransactionid,
        CASE
            WHEN COALESCE(general_journal_header.numreturnid, 0::numeric) <> 0::numeric THEN '' Chek No: ''::text || COALESCE(ch.numcheckno::character varying(50), ''No-Cheq''::character varying)::text
            ELSE view_journalcheq.narration
        END AS cheqno,
    general_journal_header.numdomainid,
    ''Biz Doc Id: ''::text || opportunitybizdocs.vcbizdocid::text AS bizdocid,
    general_journal_header.numoppbizdocsid,
    general_journal_details.vcreference AS tranref,
    general_journal_details.vardescription AS trandesc,
    general_journal_details.numdebitamt,
    general_journal_details.numcreditamt,
    chart_of_accounts.numaccountid,
    chart_of_accounts.vcaccountname,
        CASE
            WHEN COALESCE(general_journal_header.numcheckheaderid, 0::numeric) <> 0::numeric THEN ''Checks''::text
            WHEN COALESCE(general_journal_header.numcashcreditcardid, 0::numeric) <> 0::numeric AND cashcreditcarddetails.bitmoneyout = false THEN ''Cash''::text
            WHEN COALESCE(general_journal_header.numcashcreditcardid, 0::numeric) <> 0::numeric AND cashcreditcarddetails.bitmoneyout = true AND cashcreditcarddetails.bitchargeback = true THEN ''Charge''::text
            WHEN COALESCE(general_journal_header.numdepositid, 0::numeric) <> 0::numeric AND dm.tintdepositepage = 1 THEN ''Deposit''::text
            WHEN COALESCE(general_journal_header.numdepositid, 0::numeric) <> 0::numeric AND dm.tintdepositepage = 2 THEN ''Receved Pmt''::text
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numbizdocspaymentdetid, 0::numeric) = 0::numeric AND opportunitymaster.tintopptype = 1 THEN ''BizDocs Invoice''::text
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numbizdocspaymentdetid, 0::numeric) = 0::numeric AND opportunitymaster.tintopptype = 2 THEN ''BizDocs Purchase''::text
            WHEN COALESCE(general_journal_header.numcategoryhdrid, 0::numeric) <> 0::numeric THEN ''Time And Expenses''::text
            WHEN COALESCE(general_journal_header.numbillid, 0::numeric) <> 0::numeric AND COALESCE(bh.bitlandedcost, false) = true THEN ''Landed Cost''::text
            WHEN COALESCE(general_journal_header.numbillid, 0::numeric) <> 0::numeric THEN ''Bill''::text
            WHEN COALESCE(general_journal_header.numbillpaymentid, 0::numeric) <> 0::numeric THEN ''Pay Bill''::text
            WHEN COALESCE(general_journal_header.numreturnid, 0::numeric) <> 0::numeric THEN
            CASE
                WHEN rh.tintreturntype = 1 AND rh.tintreceivetype = 1 THEN ''Sales Return Refund''::text
                WHEN rh.tintreturntype = 1 AND rh.tintreceivetype = 2 THEN ''Sales Return Credit Memo''::text
                WHEN rh.tintreturntype = 2 AND rh.tintreceivetype = 2 THEN ''Purchase Return Credit Memo''::text
                WHEN rh.tintreturntype = 3 AND rh.tintreceivetype = 2 THEN ''Credit Memo''::text
                WHEN rh.tintreturntype = 4 AND rh.tintreceivetype = 1 THEN ''Refund Against Credit Memo''::text
                ELSE NULL::text
            END
            WHEN COALESCE(general_journal_header.numoppid, 0::numeric) <> 0::numeric AND COALESCE(general_journal_header.numoppbizdocsid, 0::numeric) = 0::numeric THEN ''PO Fulfillment''::text
            WHEN general_journal_header.numjournal_id <> 0::numeric THEN ''Journal''::text
            ELSE NULL::text
        END AS transactiontype,
    companyinfo.vccompanyname AS companyname,
    general_journal_header.numcheckheaderid,
    general_journal_header.numcashcreditcardid,
    general_journal_header.numoppid,
    general_journal_header.numdepositid,
    general_journal_header.numcategoryhdrid,
    timeandexpense.tinttetype,
    timeandexpense.numcategory,
    timeandexpense.dtfromdate,
    timeandexpense.numusercntid,
    divisionmaster.numdivisionid,
    COALESCE(general_journal_details.bitcleared, false) AS bitcleared,
    COALESCE(general_journal_details.bitreconcile, false) AS bitreconcile,
    COALESCE(general_journal_header.numbillid, 0::numeric) AS numbillid,
    COALESCE(general_journal_header.numbillpaymentid, 0::numeric) AS numbillpaymentid,
    COALESCE(general_journal_details.numclassid, 0::numeric) AS numclassid,
    COALESCE(general_journal_details.numprojectid, 0::numeric) AS numprojectid,
    COALESCE(general_journal_header.numreturnid, 0::numeric) AS numreturnid,
    general_journal_details.numcurrencyid,
    COALESCE(general_journal_details.numitemid, 0::numeric) AS numitemid,
        CASE
            WHEN COALESCE(bh.bitlandedcost, false) = true THEN COALESCE(bh.numoppid, 0::numeric)
            ELSE 0::numeric
        END AS numlandedcostoppid
	,T.ID
   FROM general_journal_header
     JOIN general_journal_details ON general_journal_header.numjournal_id = general_journal_details.numjournalid
     JOIN chart_of_accounts ON general_journal_details.numchartacntid = chart_of_accounts.numaccountid
	 JOIN tt_TEMPTABLEFS T ON general_journal_header.numjournal_id = T.numjournalid AND general_journal_details.numtransactionid = T.numtransactionid
	LEFT JOIN OpportunityBizDocsDetails ON OpportunityBizDocsDetails.numbizdocspaymentdetid = general_journal_header.numbizdocspaymentdetid
     LEFT JOIN timeandexpense ON general_journal_header.numcategoryhdrid = timeandexpense.numcategoryhdrid
     LEFT JOIN (divisionmaster
     LEFT JOIN companyinfo ON divisionmaster.numcompanyid = companyinfo.numcompanyid) ON general_journal_details.numcustomerid = divisionmaster.numdivisionid
     LEFT JOIN opportunitymaster ON general_journal_header.numoppid = opportunitymaster.numoppid
     LEFT JOIN cashcreditcarddetails ON general_journal_header.numcashcreditcardid = cashcreditcarddetails.numcashcreditid
     LEFT JOIN opportunitybizdocs ON general_journal_header.numoppbizdocsid = opportunitybizdocs.numoppbizdocsid
     LEFT JOIN view_journalcheq ON general_journal_header.numcheckheaderid = view_journalcheq.numcheckheaderid OR general_journal_header.numbillpaymentid = view_journalcheq.numreferenceid AND view_journalcheq.tintreferencetype = 8
     LEFT JOIN depositmaster dm ON dm.numdepositid = general_journal_header.numdepositid
     LEFT JOIN returnheader rh ON rh.numreturnheaderid = general_journal_header.numreturnid
     LEFT JOIN checkheader ch ON ch.numreferenceid = rh.numreturnheaderid AND ch.tintreferencetype = 10
     LEFT JOIN billheader bh ON COALESCE(general_journal_header.numbillid, 0::numeric) = bh.numbillid) TEMP ORDER BY ID';
   end if;            
      
   RAISE NOTICE '%',v_strSql;                                                      
   
   OPEN SWV_RefCur
   FOR EXECUTE v_strSql;    
END; 
$$;


