-- Stored procedure definition script USP_GetAdministrationNavigation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAdministrationNavigation(v_numDomainID NUMERIC(18,0),
	v_numGroupID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numModuleID  NUMERIC(18,0);
BEGIN
   select   numModuleID INTO v_numModuleID FROM PageNavigationDTL WHERE numTabID = -1    LIMIT 1; -- -1:Administration

	/********  ADMINISTRATION MENU  ********/
	
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPADMIN_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPADMIN CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPADMIN
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numParentID NUMERIC(18,0),
      numTabID NUMERIC(18,0),
      numPageNavID NUMERIC(18,0),
      vcNodeName VARCHAR(500),
      vcURL VARCHAR(1000),
      vcAddURL VARCHAR(1000),
      bitAddIsPopUp BOOLEAN,
      "order" INTEGER,
      vcImageURL VARCHAR(1000)
   );

   INSERT INTO
   tt_TEMPADMIN(numParentID, numTabID, numPageNavID, vcNodeName, vcURL, vcAddURL, bitAddIsPopUp, "order", vcImageURL)
   SELECT
   numParentID,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		coalesce(vcNavURL,'') as vcNavURL,
		CAST('' AS VARCHAR(1000)),
		false,
		coalesce(PND.intSortOrder,0) AS sintOrder,
		coalesce(vcImageURL,'')
   FROM
   PageNavigationDTL PND
   JOIN
   TreeNavigationAuthorization TNA
   ON
   PND.numPageNavID = TNA.numPageNavID
   AND PND.numTabID = TNA.numTabID
   WHERE
   coalesce(TNA.bitVisible,false) = true
   AND coalesce(PND.bitVisible,false) = true
   AND PND.numModuleID = 13  
   AND TNA.numDomainID = v_numDomainID
   AND TNA.numGroupID = v_numGroupID
   ORDER BY
   numParentID,sintOrder,(CASE WHEN PND.numPageNavID = 79 THEN 2000 ELSE 0 END),
   numPageNavID;   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
   UPDATE
   tt_TEMPADMIN
   SET
   numParentID = 0
   WHERE
   numPageNavID =(SELECT numPageNavID FROM tt_TEMPADMIN WHERE numParentID = 0);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMPADMIN WHERE coalesce(numPageNavID,0) <> 0 AND numParentID NOT IN(SELECT numPageNavID FROM tt_TEMPADMIN);

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
   DELETE FROM tt_TEMPADMIN WHERE numParentID = 0;

	-- UPDATE PARENT DETAIL
   UPDATE
   tt_TEMPADMIN t1
   SET
   numParentID = coalesce((SELECT ID FROM tt_TEMPADMIN t2 WHERE t2.numPageNavID = t1.numParentID),0);

   UPDATE
   tt_TEMPADMIN t1
   SET
   numParentID = coalesce((SELECT ID FROM tt_TEMPADMIN t2 WHERE t2.numPageNavID = t1.numParentID),0);
	
   open SWV_RefCur for SELECT * FROM tt_TEMPADMIN;
END; $$;












