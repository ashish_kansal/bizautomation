-- Stored procedure definition script USP_DeleteDocStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DeleteDocStatus(v_numDomainId NUMERIC(18,0),
v_numBizDocTypeId NUMERIC(18,0),
v_numEmployeeId NUMERIC(18,0),
v_numActionTypeId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   delete from BizDocStatusApprove where numDomainID = v_numDomainId and
   numBizDocTypeId = v_numBizDocTypeId and numEmployeeId = v_numEmployeeId and
   numActionTypeId = v_numActionTypeId;

   open SWV_RefCur for select 1;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_DeleteEmailGroup]    Script Date: 07/26/2008 16:15:32 ******/













