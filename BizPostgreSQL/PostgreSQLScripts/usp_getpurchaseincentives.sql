-- Stored procedure definition script usp_GetPurchaseIncentives for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetPurchaseIncentives(v_numDivisonID NUMERIC,
v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		true AS IsIncentives
		,vcBuyingQty
		,intType
		,vcIncentives
		,tintDiscountType 
	FROM 
		PurchaseIncentives 
	WHERE 
		numDomainID = v_numDomainID 
		AND numDivisionId = v_numDivisonID;
END; $$;











