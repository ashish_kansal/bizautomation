-- Stored procedure definition script usp_CopyStagePercentageDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_CopyStagePercentageDetails(v_numDomainid NUMERIC DEFAULT 0,
    v_numProjectID NUMERIC DEFAULT 0 ,
    v_slp_id NUMERIC DEFAULT 0,
    v_numContactId NUMERIC DEFAULT 0 ,
    v_numOppID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_id  INTEGER;
BEGIN
   if(select count(*) from StagePercentageDetails where numOppid = v_numOppID and numProjectid = v_numProjectID and numdomainid = v_numDomainid) = 0 then
      v_id := IDENT_CURRENT('StagePercentageDetails');
      open SWV_RefCur for
      with tNewRow AS(SELECT numStageDetailsId,numParentStageID,numStagePercentageId,tintConfiguration,vcStageName
      ,numdomainid,slp_id,numAssignTo,vcMileStoneName,tintPercentage,bitClose,intDueDays,
	   vcDescription,(v_id+ROW_NUMBER() OVER(ORDER BY numStageDetailsId)) AS numNewParentStageID
      FROM StagePercentageDetails where slp_id = v_slp_id and coalesce(numProjectid,0) = 0 and coalesce(numOppid,0) = 0 and numdomainid = v_numDomainid)
      SELECT * INTO TEMPORARY TABLE tt_TEMPTABLE
      FROM tNewRow order by numStagePercentageId,numStageDetailsId;

      INSERT INTO StagePercentageDetails(numParentStageID,numStagePercentageId,tintConfiguration,vcStageName
      ,numdomainid,slp_id,numAssignTo,vcMileStoneName,tintPercentage,bitClose,
		intDueDays,numOppid,vcDescription,numProjectid,numCreatedBy,bintCreatedDate,numModifiedBy,
		bintModifiedDate,dtStartDate,dtEndDate,bitFromTemplate) with recursive tParent as(SELECT t1.numNewParentStageID AS numStageDetailsId,coalesce(t2.numNewParentStageID,0) AS numParentStageID,t1.numStagePercentageId,
	   t1.tintConfiguration,t1.vcStageName,t1.numDomainId,t1.slp_id,t1.numAssignTo,t1.vcMileStoneName,t1.tintPercentage,t1.bitClose,t1.intDueDays,
	   t1.vcDescription FROM tt_TEMPTABLE t1 LEFT JOIN tt_TEMPTABLE t2 ON t1.numParentStageID = t2.numStageDetailsId),tStageOrder AS(SELECT CAST(0 AS INTEGER) AS Lvl, numStageDetailsId,numParentStageID,numStagePercentageId,tintConfiguration,vcStageName
      ,numDomainId,slp_id,numAssignTo,vcMileStoneName,tintPercentage,bitClose,intDueDays,vcDescription,
	CAST(ROW_NUMBER() OVER(ORDER BY numStagePercentageId,numStageDetailsId) AS TEXT) as stage
      FROM tParent where numParentStageID = 0
      UNION ALL
      SELECT p.lvl+1, s.numStageDetailsId,s.numParentStageID,s.numStagePercentageId,s.tintConfiguration,s.vcStageName
      ,s.numDomainId,s.slp_id,s.numAssignTo,s.vcMileStoneName,s.tintPercentage,s.bitClose,s.intDueDays,s.vcDescription,
	p.stage || '.' || CAST(ROW_NUMBER() OVER(ORDER BY s.numStagePercentageId,s.numStageDetailsId) AS TEXT)
      FROM tParent s INNER JOIN tStageOrder p ON p.numStageDetailsId = s.numParentStageID) select numParentStageID,numStagePercentageId,tintConfiguration,vcStageName
      ,numDomainId,slp_id,numAssignTo,vcMileStoneName,tintPercentage,bitClose,intDueDays,
		v_numOppID,vcDescription,v_numProjectID,v_numContactId,LOCALTIMESTAMP,v_numContactId,LOCALTIMESTAMP,LOCALTIMESTAMP  as dtStartDate,LOCALTIMESTAMP+intDueDays+CAST(-CASE intDueDays WHEN 0 THEN 0 ELSE 1 END || 'day' as interval)  as dtEndDate,1
      from tStageOrder order by numStageDetailsId;
      INSERT INTO StageDependency(numStageDetailId,numDependantOnID) with tDependency as(SELECT numStageDependancyID
      ,SD.numStageDetailId
      ,SD.numDependantOnID,S.numNewParentStageID as StageDetailID,D.numNewParentStageID as DependantOnID
      FROM StageDependency SD join tt_TEMPTABLE S on SD.numStageDetailId = S.numStageDetailsId
      join tt_TEMPTABLE D on SD.numDependantOnID = D.numStageDetailsId) select StageDetailID,DependantOnID from tDependency;
      drop table IF EXISTS tt_TEMPTABLE CASCADE;
      PERFORM usp_AssignProjectStageDate(v_numDomainid,v_numProjectID,v_numOppID);


/*Added by chintan, Bug ID*/
      IF v_numOppID > 0 AND v_slp_id > 0 then

         UPDATE OpportunityMaster SET numBusinessProcessID = v_slp_id WHERE numOppId = v_numOppID;
      end if;
   end if;

   RETURN;
END; $$;
