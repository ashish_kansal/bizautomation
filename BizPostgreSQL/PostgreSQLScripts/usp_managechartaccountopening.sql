-- Stored procedure definition script USP_ManageChartAccountOpening for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageChartAccountOpening(v_numDomainId NUMERIC(9,0),
    v_numAccountId NUMERIC(9,0),
    --@numFinYearId numeric(9),
    v_monOpening DECIMAL(20,5),
    v_dtOpenDate TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFinYearId  NUMERIC(9,0);
BEGIN
   select   numFinYearId INTO v_numFinYearId FROM    FinancialYear WHERE   numDomainId = v_numDomainId
   AND bitCurrentYear = true;
--    PRINT @numFinYearId
   IF v_numFinYearId > 0 then
        
      IF(SELECT COUNT(*)
      FROM   ChartAccountOpening
      WHERE  numDomainID = v_numDomainId
      AND numAccountId = v_numAccountId AND numFinYearId = v_numFinYearId) > 0 then
                
         UPDATE  ChartAccountOpening
         SET     numFinYearId = v_numFinYearId,monOpening = v_monOpening,dtOpenDate = v_dtOpenDate
         WHERE   numDomainID = v_numDomainId
         AND numAccountId = v_numAccountId;
      ELSE
         INSERT  INTO ChartAccountOpening(numDomainID,
                              numAccountId,
                              numFinYearId,
                              monOpening,
                              dtOpenDate)
                    VALUES(v_numDomainId,
                              v_numAccountId,
                              v_numFinYearId,
                              v_monOpening,
                              v_dtOpenDate);
      end if;
   end if;
   RETURN;
END; $$;


