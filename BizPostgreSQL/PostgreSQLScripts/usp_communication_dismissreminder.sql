-- Stored procedure definition script USP_Communication_DismissReminder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Communication_DismissReminder(v_numCommID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Communication SET tintRemStatus = 0,intRemainderMins = 0 WHERE numCommId = v_numCommID;
   RETURN;
END; $$;


