-- Stored procedure definition script usp_GetItemNameFromID for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetItemNameFromID(v_numListItemID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcData as VARCHAR(255)) from Listdetails where numListItemID = v_numListItemID;
END; $$;












