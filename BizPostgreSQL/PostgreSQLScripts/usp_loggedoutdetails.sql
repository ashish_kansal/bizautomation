-- Stored procedure definition script USP_LoggedOutDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LoggedOutDetails(v_numAccessID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update UserAccessedDTL set dtLoggedOutTime = TIMEZONE('UTC',now()) where  numAppAccessID = v_numAccessID;
   RETURN;
END; $$;


