DROP FUNCTION IF EXISTS USP_ElasticSearch_GetDomainsForReindex;

CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetDomainsForReindex(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 1) AS DATE) <> CAST(LOCALTIMESTAMP  AS DATE) AND EXTRACT(HOUR FROM LOCALTIMESTAMP) >= 1 then
	
      open SWV_RefCur for SELECT
      numDomainId
      FROM
      Domain
      WHERE
      coalesce(bitElasticSearch,false) = true;
      UPDATE ElasticSearchLastReindex SET LastReindexDate = LOCALTIMESTAMP  WHERE ID = 1;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/













