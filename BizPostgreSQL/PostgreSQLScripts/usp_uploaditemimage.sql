-- Stored procedure definition script USP_UploadItemImage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UploadItemImage(v_numItemCode NUMERIC(9,0),  
v_vcPathForImage VARCHAR(300)  ,
v_vcPathForTImage VARCHAR(300) ,
v_numOppItemCode NUMERIC(9,0) DEFAULT 0 ,
v_numCatergoryId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  

--IF @numOppItemCode = 0 AND @numCatergoryId=0
--Note: it needs to be checked if there is a case when @numOppItemCode = 0 AND @numCatergoryId=0 
--BEGIN
   -- kishan  This is not used .
   -- update dbo.ItemImages set vcPathForImage=@vcPathForImage  ,vcPathForTImage=@vcPathForTImage  
   -- where numItemCode=@numItemCode
--END

--ELSE
   IF v_numOppItemCode = 0 AND v_numItemCode = 0 then

      update Category set vcPathForCategoryImage = v_vcPathForImage where numCategoryID = v_numCatergoryId;
   ELSE
      UPDATE OpportunityItems SET vcPathForTImage = v_vcPathForTImage WHERE numoppitemtCode = v_numOppItemCode;
   end if;
   RETURN;
END; $$;


