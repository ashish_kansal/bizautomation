-- Function definition script fn_GetParentOrgName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetParentOrgName(v_numDivisionId NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCompanyName  VARCHAR(100);
BEGIN
   select   vcCompanyName INTO v_vcCompanyName FROM(SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci, DivisionMaster dm
      WHERE ca.numDivisionID = v_numDivisionId AND
      ca.numAssociateFromDivisionID = dm.numDivisionID AND
      dm.numCompanyID = ci.numCompanyId AND
      ca.bitChildOrg = true AND
      ca.bitDeleted  = false
      UNION
      SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci
      WHERE ca.numAssociateFromDivisionID = v_numDivisionId AND
      ca.numCompanyID = ci.numCompanyId AND
      ca.bitParentOrg = true AND
      ca.bitDeleted  = false) As tmpTable;  
  
   RETURN v_vcCompanyName;
END; $$;

