-- Stored procedure definition script USP_GetDuplicateCouponCode for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDuplicateCouponCode(v_numProId NUMERIC(9,0) DEFAULT 0, 
    v_numDomainId NUMERIC(18,0) DEFAULT NULL,   
	v_txtCouponCode VARCHAR(100) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT   cast(numDiscountID as VARCHAR(255))
				,cast(vcDiscountCode as VARCHAR(255))
				,cast(numPromotionID as VARCHAR(255))
				,cast(vcProName as VARCHAR(255))
   FROM PromotionOffer PO
   INNER JOIN DiscountCodes D ON D.numPromotionID = PO.numProId
   WHERE vcDiscountCode IN(SELECT cast(Id as VARCHAR(255))
      FROM SplitIDs(v_txtCouponCode,','))
   AND PO.numDomainId = v_numDomainId
   AND	D.numPromotionID <> v_numProId
   AND coalesce(PO.bitEnabled,false) = true;
   RETURN;
END; $$;













