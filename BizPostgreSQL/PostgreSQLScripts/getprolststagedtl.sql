-- Function definition script GetProLstStageDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProLstStageDTL(v_numProid NUMERIC(9,0))
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StageDetail  VARCHAR(500);
   v_DueDate  VARCHAR(100);
   v_ComDate  VARCHAR(100);
   v_numProStageId NUMERIC;
BEGIN
   select  numProStageId INTO v_numProStageId from ProjectsStageDetails where numProId = v_numProid and numstagepercentage != 100 and bitStageCompleted = true
   and bintStageComDate =(select  max(bintStageComDate) from ProjectsStageDetails
      where numProId = v_numProid and bitStageCompleted = true LIMIT 1)     LIMIT 1;
   if COALESCE(v_numProStageId,0) <> 0 then

      select vcstagedetail INTO v_StageDetail from ProjectsStageDetails where numProStageId = v_numProStageId;
   end if;

   return v_StageDetail;
END; $$;

