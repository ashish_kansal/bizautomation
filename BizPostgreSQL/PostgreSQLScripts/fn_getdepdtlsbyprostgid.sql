-- Function definition script fn_GetDepDtlsbyProStgID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetDepDtlsbyProStgID(v_numProID NUMERIC, v_numProStageID NUMERIC,v_tintType SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
   DECLARE
   v_vcRetValue VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_vcRetValue := '';

   select COUNT(*) INTO SWV_RowCount from  ProjectsDependency where numProId = v_numProID and numProStageId = v_numProStageID;
	
   if SWV_RowCount > 0 then 
      v_vcRetValue := '1';
   else 
      v_vcRetValue := '0';
   end if;
   RETURN v_vcRetValue;
END; $$;

