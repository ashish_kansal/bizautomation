DROP FUNCTION IF EXISTS USP_Item_SearchForSalesOrder;

CREATE OR REPLACE FUNCTION USP_Item_SearchForSalesOrder(v_tintOppType SMALLINT,
    v_numDomainID NUMERIC(18,0),
    v_numDivisionID NUMERIC(18,0) DEFAULT 0,
    v_str VARCHAR(1000) DEFAULT NULL,
    v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
    v_tintSearchOrderCustomerHistory SMALLINT DEFAULT 0,
    v_numPageIndex INTEGER DEFAULT NULL,
    v_numPageSize INTEGER DEFAULT NULL,
    v_WarehouseID NUMERIC(18,0) DEFAULT NULL,
    INOUT v_TotalCount INTEGER  DEFAULT NULL,
	v_bitTransferTo BOOLEAN DEFAULT NULL,
	v_bitCustomerPartSearch BOOLEAN DEFAULT NULL,
	p_vcSearchType VARCHAR(10) DEFAULT '1',
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strNewQuery  TEXT;
   v_strSQL  TEXT;
   v_strSQLCount  TEXT;
   v_bitRemoveVendorPOValidation  BOOLEAN;
   v_tintDecimalPoints  SMALLINT;
   v_filtereredCustomerPartCondition  VARCHAR(500) DEFAULT ' ';
   v_itemSearchText  VARCHAR(100);
   v_vcAttribureSearch  TEXT;
   v_vcAttribureSearchCondition  TEXT;
   v_attributeSearchText  TEXT;
   v_vcWareHouseSearch  VARCHAR(1000);
   v_vcVendorSearch  VARCHAR(1000);
   v_tintOrder  INTEGER;
   v_Fld_id  VARCHAR(20);
   v_Fld_Name  VARCHAR(20);
   v_strSearch  VARCHAR(8000);
   v_CustomSearch  VARCHAR(4000);
   v_numFieldId  NUMERIC(9,0);
   v_numCompany  NUMERIC(18,0);
   SWV_RowCount INTEGER;
   v_vclookbacktablename VARCHAR(100);
   v_PreselectItemCode NUMERIC(18,0);
BEGIN
   v_str := REPLACE(v_str,'''','''''');

	IF v_str ilike 'preselectitemcode:%' THEN
		v_PreselectItemCode := REPLACE(v_str,'preselectitemcode:','')::NUMERIC;
		v_str := '';
	END IF;

   DROP TABLE IF EXISTS tt_TABLEROWCOUNT CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEROWCOUNT
   ( 
      Value INTEGER 
   );
	
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1 AS
      SELECT * FROM(SELECT    numFieldID ,
                    vcDbColumnName ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
      FROM      View_DynamicColumns
      WHERE     numFormId = 22
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND bitCustom = false
      AND coalesce(bitSettingField,false) = true
      AND numRelCntType = 0
      UNION
      SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
      FROM      View_DynamicCustomColumns
      WHERE     Grp_id = 5
      AND numFormId = 22
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND bitCustom = true
      AND numRelCntType = 0) X; 
  
   IF NOT EXISTS(SELECT * FROM    tt_TEMP1) then
    
      INSERT  INTO tt_TEMP1
      SELECT  numFieldID ,
                        vcDbColumnName ,
                        coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                        tintOrder ,
                        0
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = 22
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID;
   end if;

   DROP TABLE IF EXISTS tt_TEMPITEMCODE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMCODE 
   ( 
      numItemCode NUMERIC(9,0) 
   );
   select   coalesce(bitRemoveVendorPOValidation,false), coalesce(tintDecimalPoints,4) INTO v_bitRemoveVendorPOValidation,v_tintDecimalPoints FROM
   Domain WHERE
   numDomainId = v_numDomainID;
   IF (v_bitCustomerPartSearch = true) then

      v_filtereredCustomerPartCondition := ' OR CPN.CustomerPartNo ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
   end if;
   RAISE NOTICE '%',v_filtereredCustomerPartCondition;
   IF v_tintOppType > 0 then
    
      IF POSITION(',' IN v_str) > 0 then
         v_itemSearchText := LTRIM(RTRIM(SUBSTR(v_str,0,POSITION(',' IN v_str))));

			--GENERATES ATTRIBUTE SEARCH CONDITION
         v_vcAttribureSearch := SUBSTR(v_str,POSITION(',' IN v_str)+1,LENGTH(v_str));
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE
         (
            vcValue TEXT
         );
         WHILE LENGTH(v_vcAttribureSearch) > 0 LOOP
            v_attributeSearchText := SUBSTR(v_vcAttribureSearch,1,coalesce(NULLIF(POSITION(',' IN v_vcAttribureSearch) -1,-1),LENGTH(v_vcAttribureSearch)));
            v_vcAttribureSearch := SUBSTR(v_vcAttribureSearch,coalesce(NULLIF(POSITION(',' IN v_vcAttribureSearch),0),LENGTH(v_vcAttribureSearch))+1,LENGTH(v_vcAttribureSearch));
            INSERT INTO tt_TEMPTABLE(vcValue) VALUES(v_attributeSearchText);
         END LOOP;
		
			--REMOVES WHITE SPACES
         UPDATE
         tt_TEMPTABLE
         SET
         vcValue = LTRIM(RTRIM(vcValue));
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
         select   COALESCE(v_vcAttribureSearch,'') ||  CAST(vcValue AS TEXT) || ',' INTO v_vcAttribureSearch FROM tt_TEMPTABLE;
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
         v_vcAttribureSearch := LTRIM(RTRIM(v_vcAttribureSearch));
         IF OCTET_LENGTH(v_vcAttribureSearch) > 0 then
            v_vcAttribureSearch := SUBSTR(v_vcAttribureSearch,1,LENGTH(v_vcAttribureSearch) -1);
         end if;
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
         v_vcAttribureSearchCondition := '(TEMPMATRIX.Attributes LIKE ''%' || REPLACE(v_vcAttribureSearch,',','%'' AND TEMPMATRIX.Attributes LIKE ''%') || '%'')'; 

			

			--LOGIC FOR GENERATING FINAL SQL STRING
         IF v_tintOppType = 1 OR (v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 AND EXISTS(SELECT numUserDetailId FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID)) then
            
            v_strSQL := 'SELECT
									I.numItemCode AS "numItemCode",
									I.numVendorID AS "numVendorID",
									MIN(WHT.numWareHouseItemID) as "numWareHouseItemID",
									fn_GetAttributes(WHT.numWareHouseItemID,I.bitSerialized) AS "Attributes"
								FROM
									Item I 
								LEFT JOIN
									WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((coalesce(I.numItemGroup,0) > 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_itemSearchText,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_itemSearchText,'') || '''' ELSE '''%' || COALESCE(v_itemSearchText,'') || '%''' END) || ') OR (coalesce(I.numItemGroup,0) = 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END) || '))
									AND coalesce(I.IsArchieve,false) <> true
									AND I.charItemType NOT IN(''A'')
									AND I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || '
									AND (WHT.numWareHouseID = ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)';

				--- added Asset validation  by sojan
            IF v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 then
               v_strSQL := coalesce(v_strSQL,'') || ' and coalesce(I.bitKitParent,false) = false AND coalesce(I.bitAssembly,false) = false ';
            end if;

            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1) AND v_numDivisionID > 0 then
                
               v_strSQL := coalesce(v_strSQL,'')
               || ' and I.numItemCode in(select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId = oppI.numOppId where oppM.numDomainId ='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId ='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;
            v_strSQL := coalesce(v_strSQL,'') || ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										fn_GetAttributes(numWareHouseItemID,I.bitSerialized)';
            v_strSQL :=
            '
				SELECT    
					ROW_NUMBER() OVER( ORDER BY  I.vcItemName) AS "SRNO",
					I.numItemCode AS "numItemCode",
					(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForImage" ,
					(SELECT vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForTImage" ,
					COALESCE(vcItemName, '''') "vcItemName",
					CAST(CASE WHEN I.charItemType = ''P''
					THEN COALESCE((SELECT monWListPrice FROM WareHouseItems WHERE numItemID = I.numItemCode LIMIT 1), 0)
					ELSE monListPrice
					END AS DECIMAL(20,' || SUBSTR(CAST(v_tintDecimalPoints AS VARCHAR(30)),1,30) || ')) AS "monListPrice",
					COALESCE(vcSKU, '''') AS "vcSKU" ,
					COALESCE(numBarCodeId, 0) AS "numBarCodeId" ,
					COALESCE(vcModelID, '''') AS "vcModelID" ,
					COALESCE(txtItemDesc, '''') AS "txtItemDesc",
					COALESCE(C.vcCompanyName, '''') AS "vcCompanyName",
					TEMPMATRIX.numWareHouseItemID AS "numWareHouseItemID",
					TEMPMATRIX.Attributes AS "vcAttributes",
					(CASE 
						WHEN COALESCE(I.bitKitParent,false) = true 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) AS "bitHasKitAsChild"
				FROM  
					(' || coalesce(v_strSQL,'') || ') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((COALESCE(I.numItemGroup,0) = 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END) || ') OR ' || coalesce(v_vcAttribureSearchCondition,'') || ')';

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQL := coalesce(v_strSQL,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            EXECUTE 'INSERT  INTO tt_TABLEROWCOUNT
                        ' || 'SELECT COUNT(*) FROM ( ' || coalesce(v_strSQL,'')
            || ') as t2';
            select   Value INTO v_TotalCount FROM    tt_TABLEROWCOUNT;
            v_strNewQuery := 'select * from (' || coalesce(v_strSQL,'')
            || ') as t where "SRNO"> '
            || CAST(((v_numPageIndex::bigint -1)*v_numPageSize::bigint) AS VARCHAR(10))
            || ' and "SRNO" < '
            || CAST(((v_numPageIndex::bigint*v_numPageSize::bigint) || 1) AS VARCHAR(10))
            || ' order by  vcItemName';

				--PRINT @strNewQuery
            OPEN SWV_RefCur FOR EXECUTE v_strNewQuery;
         ELSE
            v_strSQL := 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									fn_GetAttributes(WHT.numWareHouseItemID,I.bitSerialized) AS Attributes
								FROM
									Item I 
								INNER JOIN
									Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((coalesce(I.numItemGroup,0) > 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_itemSearchText,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_itemSearchText,'') || '''' ELSE '''%' || COALESCE(v_itemSearchText,'') || '%''' END) || ') OR (coalesce(I.numItemGroup,0) = 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END) || '))
									AND coalesce(I.IsArchieve,false) <> true
									AND I.charItemType NOT IN(''A'')
									AND I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || '
									AND (WHT.numWareHouseID = ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)';

				--- added Asset validation  by sojan
            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1) AND v_numDivisionID > 0 then
                
               v_strSQL := coalesce(v_strSQL,'')
               || ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;

            v_strSQL := coalesce(v_strSQL,'') || ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										fn_GetAttributes(numWareHouseItemID,I.bitSerialized)';
            v_strSQL :=
            '
				SELECT    
					ROW_NUMBER() OVER( ORDER BY  I.vcItemName) AS "SRNO",
					I.numItemCode AS "numItemCode",
					(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForImage" ,
					(SELECT vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) AS "vcPathForTImage" ,
					COALESCE(vcItemName, '''') AS "vcItemName" ,
					CAST(CAST(CASE WHEN I.charItemType = ''P''
					THEN COALESCE((SELECT ROUND(monWListPrice,2) FROM WareHouseItems WHERE numItemID = I.numItemCode LIMIT 1), 0)
					ELSE monListPrice
					END AS DEIMCAL(20,' || SUBSTR(CAST(v_tintDecimalPoints AS VARCHAR(30)),1,30) || ')) AS VARCHAR) AS "monListPrice",
					COALESCE(vcSKU, '''') AS "vcSKU" ,
					COALESCE(numBarCodeId, 0) AS "numBarCodeId" ,
					COALESCE(vcModelID, '''') AS "vcModelID" ,
					COALESCE(txtItemDesc, '''') AS "txtItemDesc" ,
					COALESCE(C.vcCompanyName, '''') AS "vcCompanyName",
					TEMPMATRIX.numWareHouseItemID AS "numWareHouseItemID",
					TEMPMATRIX.Attributes AS "vcAttributes",
					(CASE 
						WHEN COALESCE(I.bitKitParent,false) = true
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) AS "bitHasKitAsChild"
				FROM  
					(' || coalesce(v_strSQL,'') || ') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((COALESCE(I.numItemGroup,0) = 0 AND I.vcItemName ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END) || ') OR ' || coalesce(v_vcAttribureSearchCondition,'') || ')';

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQL := coalesce(v_strSQL,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            EXECUTE 'INSERT  INTO tt_TABLEROWCOUNT
                        ' || 'SELECT COUNT(*) FROM ( ' || coalesce(v_strSQL,'')
            || ') as t2';
            select   Value INTO v_TotalCount FROM    tt_TABLEROWCOUNT;
            v_strNewQuery := 'select * from (' || coalesce(v_strSQL,'')
            || ') as t where "SRNO"> '
            || CAST(((v_numPageIndex::bigint -1)*v_numPageSize::bigint) AS VARCHAR(10))
            || ' and "SRNO" < '
            || CAST(((v_numPageIndex::bigint*v_numPageSize::bigint) || 1) AS VARCHAR(10))
            || ' order by  vcItemName';

				--PRINT @strNewQuery
            OPEN SWV_RefCur FOR EXECUTE v_strNewQuery;
         end if;
      ELSE
         v_vcWareHouseSearch := '';
         v_vcVendorSearch := '';
         v_strSQL := '';
         select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name FROM    tt_TEMP1 WHERE   Custom = 1   ORDER BY tintOrder LIMIT 1;
         WHILE v_tintOrder > 0 LOOP
            v_strSQL := coalesce(v_strSQL,'') || ', GetCustFldValueItem(' || coalesce(v_Fld_id,'') || ', I.numItemCode) as "' || coalesce(v_Fld_Name,'') || '"';
            select   tintOrder+1, numFieldId, vcFieldName INTO v_tintOrder,v_Fld_id,v_Fld_Name FROM    tt_TEMP1 WHERE   Custom = 1
            AND tintOrder >= v_tintOrder   ORDER BY tintOrder LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_tintOrder := 0;
            end if;
         END LOOP;


				--Temp table for Item Search Configuration

         DROP TABLE IF EXISTS tt_TEMPSEARCH CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPSEARCH AS
            SELECT * FROM(SELECT    numFieldID ,
                                    vcDbColumnName ,
                                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom,
									vclookbacktablename
            FROM      View_DynamicColumns
            WHERE     numFormId = 22
            AND numDomainID = v_numDomainID
            AND tintPageType = 1
            AND bitCustom = false
            AND coalesce(bitSettingField,false) = true
            AND numRelCntType = 1
            UNION
            SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom,
									''
            FROM      View_DynamicCustomColumns
            WHERE     Grp_id = 5
            AND numFormId = 22
            AND numDomainID = v_numDomainID
            AND tintPageType = 1
            AND bitCustom = true
            AND numRelCntType = 1) X;
         IF NOT EXISTS(SELECT * FROM    tt_TEMPSEARCH) then
                    
            INSERT  INTO tt_TEMPSEARCH
            SELECT  numFieldID ,
                                        vcDbColumnName ,
                                        coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                                        tintOrder ,
                                        0
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 22
            AND bitDefault = true
            AND coalesce(bitSettingField,false) = true
            AND numDomainID = v_numDomainID;
         end if;

				--Regular Search
         v_strSearch := '';
         select   tintOrder+1, vcDbColumnName, numFieldId,vclookbacktablename INTO v_tintOrder,v_Fld_Name,v_numFieldId,v_vclookbacktablename FROM    tt_TEMPSEARCH WHERE   Custom = 0   ORDER BY tintOrder LIMIT 1;
         WHILE v_tintOrder > -1 LOOP
            IF v_Fld_Name = 'vcPartNo' then
						
               INSERT  INTO tt_TEMPITEMCODE
               SELECT DISTINCT
               Item.numItemCode
               FROM    Vendor
               JOIN Item ON Item.numItemCode = Vendor.numItemCode
               WHERE   Item.numDomainID = v_numDomainID
               AND Vendor.numDomainID = v_numDomainID
               AND Vendor.vcPartNo IS NOT NULL
               AND LENGTH(Vendor.vcPartNo) > 0
               AND vcPartNo ilike (CASE WHEN p_vcSearchType = '2'  THEN COALESCE(v_str,'') || '%' WHEN p_vcSearchType = '3' THEN '%' || COALESCE(v_str,'') ELSE '%' || COALESCE(v_str,'') || '%' END);
               v_vcVendorSearch := 'Vendor.vcPartNo ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
            ELSEIF v_Fld_Name = 'vcBarCode'
            then
						
               INSERT  INTO tt_TEMPITEMCODE
               SELECT DISTINCT
               Item.numItemCode
               FROM    Item
               JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
               AND Item.numItemCode = WI.numItemID
               JOIN Warehouses W ON W.numDomainID = v_numDomainID
               AND W.numWareHouseID = WI.numWareHouseID
               WHERE   Item.numDomainID = v_numDomainID
               AND coalesce(Item.numItemGroup,0) > 0
               AND WI.vcBarCode IS NOT NULL
               AND LENGTH(WI.vcBarCode) > 0
               AND WI.vcBarCode ilike (CASE WHEN p_vcSearchType = '2'  THEN COALESCE(v_str,'') || '%' WHEN p_vcSearchType = '3' THEN '%' || COALESCE(v_str,'') ELSE '%' || COALESCE(v_str,'') || '%' END);
               v_vcWareHouseSearch := 'WareHouseItems.vcBarCode ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
            ELSEIF v_Fld_Name = 'vcWHSKU'
            then
						
               INSERT  INTO tt_TEMPITEMCODE
               SELECT DISTINCT
               Item.numItemCode
               FROM    Item
               JOIN WareHouseItems WI ON WI.numDomainID = v_numDomainID
               AND Item.numItemCode = WI.numItemID
               JOIN Warehouses W ON W.numDomainID = v_numDomainID
               AND W.numWareHouseID = WI.numWareHouseID
               WHERE   Item.numDomainID = v_numDomainID
               AND coalesce(Item.numItemGroup,0) > 0
               AND WI.vcWHSKU IS NOT NULL
               AND LENGTH(WI.vcWHSKU) > 0
               AND WI.vcWHSKU ilike (CASE WHEN p_vcSearchType = '2'  THEN COALESCE(v_str,'') || '%' WHEN p_vcSearchType = '3' THEN '%' || COALESCE(v_str,'') ELSE '%' || COALESCE(v_str,'') || '%' END);
               IF LENGTH(v_vcWareHouseSearch) > 0 then
                  v_vcWareHouseSearch := coalesce(v_vcWareHouseSearch,'') || ' OR WareHouseItems.vcWHSKU ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
               ELSE
                  v_vcWareHouseSearch := 'WareHouseItems.vcWHSKU ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
               end if;
            ELSE
               v_strSearch := coalesce(v_strSearch,'')
               ||(CASE v_vclookbacktablename 
						WHEN 'Item' THEN ' I.' || coalesce(v_Fld_Name,'') 
						WHEN 'WareHouseItems' THEN ' WHT.' || coalesce(v_Fld_Name,'') 
						WHEN 'CompanyInfo' THEN ' C.' || coalesce(v_Fld_Name,'') 
						ELSE ' ' || coalesce(v_Fld_Name,'') 
						END)  || '::TEXT ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
            end if;
            select   tintOrder+1, vcDbColumnName, numFieldId,vclookbacktablename INTO v_tintOrder,v_Fld_Name,v_numFieldId,v_vclookbacktablename  FROM  tt_TEMPSEARCH WHERE   tintOrder >= v_tintOrder
            AND Custom = 0   ORDER BY tintOrder LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
                            
               v_tintOrder := -1;
            ELSEIF v_Fld_Name != 'vcPartNo'
            AND v_Fld_Name != 'vcBarCode'
            AND v_Fld_Name != 'vcWHSKU'
            then
                                
               v_strSearch := coalesce(v_strSearch,'') || ' or ';
            end if;
         END LOOP;
         IF(SELECT COUNT(*)
         FROM   tt_TEMPITEMCODE) > 0 then
            v_strSearch := coalesce(v_strSearch,'')
            || ' or  I.numItemCode in (select numItemCode from tt_TEMPITEMCODE)';
         end if; 

				--Custom Search
         
         select   tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId FROM    tt_TEMPSEARCH WHERE   Custom = 1   ORDER BY tintOrder LIMIT 1;
         WHILE v_tintOrder > -1 LOOP
            IF LENGTH(v_strSearch) > 0 then
				v_strSearch := coalesce(v_strSearch,'') || ' OR ' || 'GetCustFldValueItem(' || CAST(v_numFieldId AS VARCHAR) || ',I.numItemCode) ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
			ELSE
				v_strSearch := 'GetCustFldValueItem(' || CAST(v_numFieldId AS VARCHAR) || ',I.numItemCode) ilike ' || (CASE WHEN p_vcSearchType = '2'  THEN '''' || COALESCE(v_str,'') || '%''' WHEN p_vcSearchType = '3' THEN '''%' || COALESCE(v_str,'') || '''' ELSE '''%' || COALESCE(v_str,'') || '%''' END);
			end if;

            SELECT 
				tintOrder+1, vcDbColumnName, numFieldId INTO v_tintOrder,v_Fld_Name,v_numFieldId 
			FROM 
				tt_TEMPSEARCH 
			WHERE 
				tintOrder >= v_tintOrder
				AND Custom = 1  
			ORDER BY 
				tintOrder LIMIT 1;

            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then     
               v_tintOrder := -1;
            end if;
         END LOOP;

         v_strSearch := coalesce(v_strSearch,'') || coalesce(v_filtereredCustomerPartCondition,'');
         select   numCompanyID INTO v_numCompany FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
         IF v_tintOppType = 1 OR (v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 AND EXISTS(SELECT numUserDetailId FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID)) then
                
            v_strSQLCount := 'SELECT COALESCE((SELECT 
											COUNT(I.numItemCode) 
										FROM 
											Item  I 
										LEFT JOIN 
											DivisionMaster D              
										ON 
											I.numVendorID=D.numDivisionID  
										LEFT JOIN 
											CompanyInfo C  
										ON 
											C.numCompanyID=D.numCompanyID 
										LEFT JOIN 
											CustomerPartNumber  CPN  
										ON 
											I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' || CAST(coalesce(v_numCompany,0) AS VARCHAR(30)) ||  '
										LEFT JOIN
											WareHouseItems WHT
										ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' ||
            CASE LENGTH(v_vcWareHouseSearch)
            WHEN 0
            THEN
               'SELECT 
																					numWareHouseItemID 
																				FROM 
																					WareHouseItems 
																				WHERE 
																					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																					AND WareHouseItems.numItemID = I.numItemCode 
																					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1'
            ELSE
               'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								WareHouseItems 
																							WHERE 
																								WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																								AND WareHouseItems.numItemID = I.numItemCode 
																								AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)' ||
               ' AND (' || coalesce(v_vcWareHouseSearch,'') || ')) > 0 
																					THEN
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) AND (' || coalesce(v_vcWareHouseSearch,'') || ') LIMIT 1)
																					ELSE
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1)
																					END'
            END
            ||
            ') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
            || ' AND (numWareHouseID =  ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)) > 0)) AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID='
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and ('
            || coalesce(v_strSearch,'') || ') ';   
						     
						--- added Asset validation  by sojan
            IF v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 then
               v_strSQLCount := coalesce(v_strSQLCount,'') || ' AND COALESCE(I.bitKitParent,false) = false AND COALESCE(I.bitAssembly,false) = false ';
            end if;

					--DECLARE @vcCustomerPartSearch VARCHAR(MAX)=''
					--DECLARE @vcCustomerPartSearchFilter VARCHAR(MAX)=''
					--IF(@bitCustomerPartSearch=1)
					--BEGIN
					--	SET @vcCustomerPartSearch = ' LEFT JOIN CustomerPartNumber AS CPNF ON CPNF.numItemCode=OPPI.numItemCode  '
					--	SET @vcCustomerPartSearchFilter = ' AND  '
					--END
            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1) AND v_numDivisionID > 0 then
                    
               v_strSQLCount := coalesce(v_strSQLCount,'')
               || ' AND I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId 
							where oppM.numDomainID='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQLCount := coalesce(v_strSQLCount,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            v_strSQLCount := coalesce(v_strSQLCount,'') || '),0);';
            v_strSQL := 'DROP TABLE IF EXISTS TEMPTableItems; CREATE TEMP TABLE TEMPTableItems AS select I.numItemCode AS "numItemCode", COALESCE(CPN.CustomerPartNo,'''') AS "CustomerPartNo",
									  (SELECT vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForImage",
									  (SELECT vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForTImage",
									  COALESCE(vcItemName,'''') AS "vcItemName",
									  CAST(CAST(CASE 
									  WHEN I.charItemType=''P''
									  THEN COALESCE((SELECT monWListPrice FROM WareHouseItems WHERE WareHouseItems.numItemID=I.numItemCode LIMIT 1),0) 
									  ELSE 
										monListPrice 
									  END AS DECIMAL(20,' || SUBSTR(CAST(v_tintDecimalPoints AS VARCHAR(30)),1,30) || ')) AS VARCHAR) AS "monListPrice",
									  COALESCE(vcSKU,'''') AS "vcSKU",
									  COALESCE(numBarCodeId,'''') AS "numBarCodeId",
									  COALESCE(vcModelID,'''') AS "vcModelID",
									  COALESCE(txtItemDesc,'''') as "txtItemDesc",
									  COALESCE(C.vcCompanyName,'''') as "vcCompanyName",
									  WHT.numWareHouseItemID AS "numWareHouseItemID",
									  fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS "vcAttributes",
									  (CASE 
											WHEN COALESCE(I.bitKitParent,false) = true 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) AS "bitHasKitAsChild",COALESCE(I.bitMatrix,false) AS "bitMatrix",COALESCE(I.numItemGroup,0) AS "numItemGroup" '
            || coalesce(v_strSQL,'')
            || ' FROM Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									 LEFT JOIN CustomerPartNumber  CPN  
									 ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' || CAST(coalesce(v_numCompany,0) AS VARCHAR(30)) ||  '
									  LEFT JOIN
											WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' ||
            CASE LENGTH(v_vcWareHouseSearch)
            WHEN 0
            THEN
               'SELECT 
					numWareHouseItemID 
				FROM 
					WareHouseItems 
				WHERE 
					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
					AND WareHouseItems.numItemID = I.numItemCode 
					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1'
            ELSE
               'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								WareHouseItems 
																							WHERE 
																								WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																								AND WareHouseItems.numItemID = I.numItemCode 
																								AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)' ||
               ' AND (' || coalesce(v_vcWareHouseSearch,'') || ')) > 0 
																					THEN
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) AND (' || coalesce(v_vcWareHouseSearch,'') || ') LIMIT 1)
																					ELSE
																						(SELECT 
																							numWareHouseItemID 
																						FROM 
																							WareHouseItems 
																						WHERE 
																							WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																							AND WareHouseItems.numItemID = I.numItemCode 
																							AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1)
																					END'
            END
            ||
            ') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
            || ' AND (numWareHouseID =  ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)) > 0)) AND COALESCE(I.Isarchieve,false) <> true And I.charItemType NOT IN(''A'') AND  I.numDomainID='
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' and ('
            || coalesce(v_strSearch,'') || ') ';   
						     
						--- added Asset validation  by sojan
            IF v_bitRemoveVendorPOValidation = true AND v_tintOppType = 2 then
						
               v_strSQL := coalesce(v_strSQL,'') || ' and COALESCE(I.bitKitParent,false) = false AND COALESCE(I.bitAssembly,false) = false ';
               IF coalesce(v_bitTransferTo,false) = true then
							
                  v_strSQL := coalesce(v_strSQL,'') || ' and COALESCE(I.bitLotNo,false) = false AND COALESCE(I.bitSerialized,false) = false AND I.charItemType = ''P'' ';
               end if;
            end if;
            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1)
            AND v_numDivisionID > 0 then
                            
               v_strSQL := coalesce(v_strSQL,'')
               || ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQL := coalesce(v_strSQL,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            IF v_numDomainID <> 204 then
						
               v_strSQL := coalesce(v_strSQL,'') || CONCAT(' ORDER BY vcItemName OFFSET ',(v_numPageIndex::bigint -1)*v_numPageSize::bigint,
               ' ROWS FETCH NEXT ',v_numPageSize,' ROWS ONLY ');
            ELSE
               v_strSQL := coalesce(v_strSQL,'') || 'DELETE FROM 
														TEMPTableItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				TEMPTableItems AS F
																			WHERE 
																				COALESCE(F.bitMatrix,false) = true AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								TEMPTableItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = true
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									TEMPTableItems AS F
																								WHERE
																									COALESCE(F.bitMatrix,false) = true AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													TEMPTableItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = true
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);';
            end if;
            v_strSQL := coalesce(v_strSQL,'') || '; DROP TABLE IF EXISTS TEMPTableItemsFinal; CREATE TEMP TABLE TEMPTableItemsFinal AS SELECT ROW_NUMBER() OVER( ORDER BY "vcItemName") AS "SRNO",(CASE WHEN (lower("vcSKU")=''' || LOWER(coalesce(v_str,'')) || ''' OR ' || CAST(coalesce(v_numDomainID,0) AS VARCHAR(30)) || '=209) THEN 1 ELSE 0 END) "bitSKUMatch",* FROM TEMPTableItems;';
            IF v_numDomainID <> 204 then
				EXECUTE coalesce(v_strSQL,'');
				EXECUTE v_strSQLCount INTO v_TotalCount;
				v_strSQL := 'SELECT * FROM TEMPTableItemsFinal ORDER BY "SRNO";';
			    OPEN SWV_RefCur FOR EXECUTE v_strSQL;
				
            ELSE
			   EXECUTE coalesce(v_strSQL,'');
			   EXECUTE 'SELECT COUNT(*) FROM TEMPTableItemsFinal;' INTO v_TotalCount;
               v_strSQL := ' SELECT * FROM TEMPTableItemsFinal where "SRNO"> '
               || CAST(((v_numPageIndex::bigint -1)*v_numPageSize::bigint) AS VARCHAR(10))
               || ' and "SRNO" < '
               || CAST(((v_numPageIndex::bigint*v_numPageSize::bigint) || 1) AS VARCHAR(10))
               || ' order by  "SRNO";';

			   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
            end if;
						
						--PRINT @strSQL
           
         ELSEIF v_tintOppType = 2
         then
                
            v_strSQLCount := 'SELECT COALESCE((SELECT 
																	COUNT(I.numItemCode) 
																FROM 
																	Item I  
																INNER JOIN
																	Vendor V
																ON
																	V.numItemCode = I.numItemCode
																LEFT JOIN 
																	DivisionMaster D              
																ON 
																	V.numVendorID=D.numDivisionID  
																LEFT join 
																	CompanyInfo C  
																ON 
																	C.numCompanyID=D.numCompanyID
																LEFT JOIN 
																	CustomerPartNumber  CPN  
																ON 
																	I.numItemCode=CPN.numItemCode 
																	AND CPN.numCompanyID=' || CAST(coalesce(v_numCompany,0) AS VARCHAR(30)) ||  ' 
																LEFT JOIN
																	WareHouseItems WHT
																ON
																	WHT.numItemID = I.numItemCode AND
																	WHT.numWareHouseItemID = (' ||
            CASE LENGTH(v_vcWareHouseSearch)
            WHEN 0
            THEN
				'SELECT 
					numWareHouseItemID 
				FROM 
					WareHouseItems 
				WHERE 
					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
					AND WareHouseItems.numItemID = I.numItemCode 
					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1'
            ELSE
               'CASE 
																										WHEN (SELECT 
																													COUNT(*) 
																												FROM 
																													WareHouseItems 
																												WHERE 
																													WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																													AND WareHouseItems.numItemID = I.numItemCode 
																													AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)' ||
               ' AND (' || coalesce(v_vcWareHouseSearch,'') || ')) > 0 
																										THEN
																											(SELECT 
																												numWareHouseItemID 
																											FROM 
																												WareHouseItems 
																											WHERE 
																												WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																												AND WareHouseItems.numItemID = I.numItemCode 
																												AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) AND (' || coalesce(v_vcWareHouseSearch,'') || ') LIMIT 1)
																										ELSE
																											(SELECT 
																												numWareHouseItemID 
																											FROM 
																												WareHouseItems 
																											WHERE 
																												WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																												AND WareHouseItems.numItemID = I.numItemCode 
																												AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1)
																										END'
            END
            ||
            ')
										       
																		WHERE (
																				(I.charItemType <> ''P'') OR 
																				((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
            || ' AND (numWareHouseID =  ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)) > 0)) AND COALESCE(I.Isarchieve,false) <> true and COALESCE(I.bitKitParent,false) = false  AND COALESCE(I.bitAssembly,false) = false And I.charItemType NOT IN(''A'') AND V.numVendorID='
            || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
            || ' and (' || CASE LENGTH(v_vcVendorSearch)
            WHEN 0 THEN ''
            ELSE REPLACE(v_vcVendorSearch,'Vendor','V') || ' OR '
            END || coalesce(v_strSearch,'') || ') '; 

					--- added Asset validation  by sojan
            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1) AND v_numDivisionID > 0 then
                    
               v_strSQLCount := coalesce(v_strSQLCount,'')
               || ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQLCount := coalesce(v_strSQLCount,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            v_strSQLCount := coalesce(v_strSQLCount,'') || '),0);';
            v_strSQL := 'DROP TABLE IF EXISTS TEMPTableItems; CREATE TEMP TABLE TEMPTableItems AS SELECT I.numItemCode AS "numItemCode",
									COALESCE(CPN.CustomerPartNo,'''') AS "CustomerPartNo",
									(SELECT vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForImage",
									(SELECT vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode LIMIT 1) as "vcPathForTImage",
									COALESCE(vcItemName,'''') "vcItemName",
									CAST(CAST(monListPrice AS DECIMAL(20,' || SUBSTR(CAST(v_tintDecimalPoints AS VARCHAR(30)),1,30) || ')) AS VARCHAR) as "monListPrice",
									COALESCE(vcSKU,'''') AS "vcSKU",
									COALESCE(numBarCodeId,'''') AS "numBarCodeId",
									COALESCE(vcModelID,'''') AS "vcModelID",
									COALESCE(txtItemDesc,'''') as "txtItemDesc",
									COALESCE(C.vcCompanyName,'''') as "vcCompanyName",
									WHT.numWareHouseItemID AS "numWareHouseItemID",
									fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS "vcAttributes",
									(CASE 
										WHEN COALESCE(I.bitKitParent,false) = true 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND COALESCE(Item.bitKitParent,false) = true) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) AS "bitHasKitAsChild",COALESCE(I.bitMatrix,false) AS "bitMatrix",COALESCE(I.numItemGroup,0) AS "numItemGroup" '
            || coalesce(v_strSQL,'')
            || '  from item I  
									INNER JOIN
									Vendor V
									ON
									V.numItemCode = I.numItemCode
									Left join 
									DivisionMaster D              
									ON 
									V.numVendorID=D.numDivisionID  
									LEFT join 
									CompanyInfo C  
									ON 
									C.numCompanyID=D.numCompanyID
									LEFT JOIN 
									CustomerPartNumber  CPN  
									ON 
									I.numItemCode=CPN.numItemCode 
									AND CPN.numCompanyID=' || CAST(coalesce(v_numCompany,0) AS VARCHAR(30)) ||  ' 
									LEFT JOIN
									WareHouseItems WHT
									ON
									WHT.numItemID = I.numItemCode AND
									WHT.numWareHouseItemID = (' ||
            CASE LENGTH(v_vcWareHouseSearch)
            WHEN 0
            THEN
               'SELECT 
					numWareHouseItemID 
				FROM 
					WareHouseItems 
				WHERE 
					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
					AND WareHouseItems.numItemID = I.numItemCode 
					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1'
            ELSE
               'CASE 
																			WHEN (SELECT 
																						COUNT(*) 
																					FROM 
																						WareHouseItems 
																					WHERE 
																						WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																						AND WareHouseItems.numItemID = I.numItemCode 
																						AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)' ||
               ' AND (' || coalesce(v_vcWareHouseSearch,'') || ')) > 0 
																			THEN
																				(SELECT 
																					numWareHouseItemID 
																				FROM 
																					WareHouseItems 
																				WHERE 
																					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																					AND WareHouseItems.numItemID = I.numItemCode 
																					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) AND (' || coalesce(v_vcWareHouseSearch,'') || ') LIMIT 1)
																			ELSE
																				(SELECT 
																					numWareHouseItemID 
																				FROM 
																					WareHouseItems 
																				WHERE 
																					WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' 
																					AND WareHouseItems.numItemID = I.numItemCode 
																					AND (WareHouseItems.numWareHouseID = ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0) LIMIT 1)
																			END'
            END
            ||
            ')
										       
									WHERE (
											(I.charItemType <> ''P'') OR 
											((SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
            || ' AND (numWareHouseID =  ' ||  CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20))  || ' OR ' || CAST(coalesce(v_WarehouseID,0) AS VARCHAR(20)) || ' = 0)) > 0)) AND COALESCE(I.Isarchieve,false) <> true and COALESCE(I.bitKitParent,false) = false  AND COALESCE(I.bitAssembly,false) = false And I.charItemType NOT IN(''A'') AND V.numVendorID='
            || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
            || ' and (' || CASE LENGTH(v_vcVendorSearch)
            WHEN 0 THEN ''
            ELSE REPLACE(v_vcVendorSearch,'Vendor','V') || ' OR '
            END || coalesce(v_strSearch,'') || ') '; 

							--- added Asset validation  by sojan
            IF (v_bitCustomerPartSearch = true OR v_tintSearchOrderCustomerHistory = 1)
            AND v_numDivisionID > 0 then
                                
               v_strSQL := coalesce(v_strSQL,'')
               || ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)
               || ' and oppM.numDivisionId='
               || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20)
               || ')';
            end if;
            IF coalesce(v_bitTransferTo,false) = true then
							
               v_strSQL := coalesce(v_strSQL,'') || ' and COALESCE(I.bitLotNo,0) = 0 AND COALESCE(I.bitSerialized,0) = 0 AND I.charItemType = ''P'' ';
            end if;

			IF COALESCE(v_PreselectItemCode,0) > 0 THEN
				v_strSQL := coalesce(v_strSQL,'') || CONCAT(' AND I.numItemCode=',v_PreselectItemCode);
			END IF;

            IF v_numDomainID <> 204 then
							
               v_strSQL := coalesce(v_strSQL,'') || CONCAT(' ORDER BY vcItemName OFFSET ',(v_numPageIndex::bigint -1)*v_numPageSize::bigint,
               ' ROWS FETCH NEXT ',v_numPageSize,' ROWS ONLY ');
            ELSE
               v_strSQL := coalesce(v_strSQL,'') || 'DELETE FROM 
															TEMPTableItems
														WHERE 
															numItemCode IN (
																				SELECT 
																					F.numItemCode
																				FROM 
																					TEMPTableItems AS F
																				WHERE
																					COALESCE(F.bitMatrix,false) = true AND
																					EXISTS (
																								SELECT 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																								FROM 
																									TEMPTableItems t1
																								WHERE 
																									t1.vcItemName = F.vcItemName
																									AND t1.bitMatrix = true
																									AND t1.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																								HAVING 
																									Count(t1.numItemCode) > 1
																							)
																			)
															AND numItemCode NOT IN (
																									SELECT 
																										Min(numItemCode)
																									FROM 
																										TEMPTableItems AS F
																									WHERE
																										COALESCE(F.bitMatrix,false) = true AND
																										Exists (
																													SELECT 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																													FROM 
																														TEMPTableItems t2
																													WHERE 
																														t2.vcItemName = F.vcItemName
																													   AND t2.bitMatrix = true
																													   AND t2.numItemGroup = F.numItemGroup
																													GROUP BY 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																													HAVING 
																														Count(t2.numItemCode) > 1
																												)
																									GROUP BY 
																										vcItemName, bitMatrix, numItemGroup
																								);';
            end if;
            v_strSQL := coalesce(v_strSQL,'') || '; DROP TABLE IF EXISTS TEMPTableItemsFinal; CREATE TEMP TABLE TEMPTableItemsFinal AS SELECT ROW_NUMBER() OVER( ORDER BY "vcItemName") AS "SRNO",(CASE WHEN (lower("vcSKU")=''' || LOWER(coalesce(v_str,'')) || ''' OR ' || CAST(coalesce(v_numDomainID,0) AS VARCHAR(30)) || '=209) THEN 1 ELSE 0 END) "bitSKUMatch",* FROM TEMPTableItems;';
            IF v_numDomainID <> 204 then
				EXECUTE coalesce(v_strSQL,'');
				EXECUTE v_strSQLCount INTO v_TotalCount;
				v_strSQL := ' SELECT * FROM TEMPTableItemsFinal ORDER BY "SRNO";';
				OPEN SWV_RefCur FOR EXECUTE v_strSQL;
            ELSE
			   EXECUTE coalesce(v_strSQL,'');
			   EXECUTE 'SELECT COUNT(*) FROM TEMPTableItemsFinal;' INTO v_TotalCount;
               v_strSQL := ' SELECT * FROM TEMPTableItemsFinal where "SRNO" > '
               || CAST(((v_numPageIndex::bigint -1)*v_numPageSize::bigint) AS VARCHAR(10))
               || ' and "SRNO" < '
               || CAST(((v_numPageIndex::bigint*v_numPageSize::bigint) || 1) AS VARCHAR(10))
               || ' order by  vcItemName;';
			   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
            end if;
            
         ELSE
            open SWV_RefCur for
            SELECT  0;
         end if;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT  0;
   end if;  

   open SWV_RefCur2 for
   SELECT  numFieldID AS "numFieldID",
                    vcDbColumnName AS "vcDbColumnName",
                    vcFieldName AS "vcFieldName" ,
                    tintOrder AS "tintOrder" ,
                    Custom AS "Custom" FROM    tt_TEMP1
   WHERE   vcDbColumnName NOT IN('vcPartNo','vcBarCode','vcWHSKU') ORDER BY tintOrder; 
   
   RAISE NOTICE '%',(CAST(v_strSQL AS TEXT));

   DROP TABLE IF EXISTS Scores CASCADE;

   IF EXISTS(SELECT * FROM pg_tables WHERE tablename = ' "tempdb..#Temp1"') then

      DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   end if;

   IF EXISTS(SELECT * FROM pg_tables WHERE tablename = ' "tempdb..#tempSearch"') then

      DROP TABLE IF EXISTS tt_TEMPSEARCH CASCADE;
   end if;

   IF EXISTS(SELECT * FROM pg_tables WHERE tablename = ' "tempdb..#tempItemCode"') then

      DROP TABLE IF EXISTS tt_TEMPITEMCODE CASCADE;
   end if;
   RETURN;
END; $$;


