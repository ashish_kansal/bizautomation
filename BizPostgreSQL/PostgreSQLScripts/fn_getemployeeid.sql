-- Function definition script fn_GetEmployeeID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetEmployeeID(v_numBizDocAppID NUMERIC(8,0),
 v_numDomainId INTEGER)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcEmployee  VARCHAR(2000);
   v_vcUserName  VARCHAR(50);


   Employee_Cursor CURSOR FOR
	
   select cast(numEmployeeID as VARCHAR(50)) as vcUserName
   from AdditionalContactsInformation ACI
   INNER JOIN BizDocApprovalRuleEmployee BAR
   ON BAR.numEmployeeID = ACI.numContactId AND 
   BAR.numBizDocAppID = v_numBizDocAppID;
BEGIN
   v_vcEmployee := '';

   OPEN Employee_Cursor;

   FETCH NEXT FROM Employee_Cursor into v_vcUserName;
   WHILE FOUND LOOP
      v_vcEmployee := coalesce(v_vcEmployee,'') || ',' || coalesce(v_vcUserName,'');
      FETCH NEXT FROM Employee_Cursor into v_vcUserName;
   END LOOP;

   CLOSE Employee_Cursor;


   RETURN SUBSTR(v_vcEmployee,2,LENGTH(v_vcEmployee));
END; $$;

