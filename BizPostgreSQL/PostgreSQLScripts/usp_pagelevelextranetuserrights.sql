-- Stored procedure definition script usp_PageLevelExtranetUserRights for PostgreSQL
CREATE OR REPLACE FUNCTION usp_PageLevelExtranetUserRights(v_numContactID NUMERIC,      
 v_vcFileName VARCHAR(100),      
 v_numModuleID NUMERIC DEFAULT 0,      
 v_numPageID NUMERIC DEFAULT 0         
--      
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		GA.numModuleID AS "numModuleID"
		,ExtDtl.numContactID AS "numContactID"
		,PM.vcFileName AS "vcFileName"
		,GA.numModuleID AS "numModuleID"
		,GA.numPageID AS "numPageID"
		,MAX(GA.intExportAllowed) As "intExportAllowed"
		,MAX(GA.intPrintAllowed) As "intPrintAllowed"
		,MAX(GA.intViewAllowed) As "intViewAllowed"
		,MAX(GA.intAddAllowed) As "intAddAllowed"
		,MAX(GA.intUpdateAllowed) As "intUpdateAllowed"
		,MAX(GA.intDeleteAllowed) As "intDeleteAllowed"
   FROM ExtarnetAccounts Ext
   INNER JOIN GroupAuthorization GA ON (GA.numGroupID = Ext.numGroupID  or GA.numGroupID = Ext.numPartnerGroupID)
   INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
   INNER JOIN ExtranetAccountsDtl ExtDtl ON Ext.numExtranetID = ExtDtl.numExtranetID AND ExtDtl.numContactID = v_numContactID::VARCHAR
   WHERE
   PM.vcFileName = v_vcFileName
   AND GA.numModuleID = v_numModuleID
   AND GA.numPageID = v_numPageID
   GROUP BY GA.numModuleID,ExtDtl.numContactID,PM.vcFileName,GA.numModuleID,GA.numPageID;
END; $$;












