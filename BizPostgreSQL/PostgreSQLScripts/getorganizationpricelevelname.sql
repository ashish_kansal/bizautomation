-- Function definition script GetOrganizationPriceLevelName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOrganizationPriceLevelName(v_numDomainID NUMERIC(18,0)
	,v_tintPriceLevel SMALLINT)
RETURNS VARCHAR(300) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcPriceLevelName  VARCHAR(300) DEFAULT '';
BEGIN
   select   coalesce(NULLIF(pnt.vcPriceLevelName,''),CONCAT('Price Level ',pnt.tintPriceLevel)) INTO v_vcPriceLevelName FROM
   PricingNamesTable pnt WHERE
   pnt.numDomainID = v_numDomainID
   AND pnt.tintPriceLevel = v_tintPriceLevel;

   RETURN coalesce(v_vcPriceLevelName,'');
END; $$;

