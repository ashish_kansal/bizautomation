CREATE OR REPLACE FUNCTION USP_WorkOrder_GetFieldValue(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_vcFieldName VARCHAR(200), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'numQtyItemsReq' then
	
      open SWV_RefCur for
      SELECT coalesce(numQtyItemsReq,0) FROM WorkOrder	WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID;
   ELSEIF v_vcFieldName = 'numItemCode'
   then
	
      open SWV_RefCur for
      SELECT coalesce(numItemCode,0) FROM WorkOrder	WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID;
   ELSEIF v_vcFieldName = 'numQuantityBuilt'
   then
	
      IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID AND(coalesce(numQtyItemsReq,0) -coalesce(numQtyBuilt,0)) > 0) then
		
         open SWV_RefCur for
         SELECT coalesce(numQtyItemsReq,0) -coalesce(numQtyBuilt,0) FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID;
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   ELSE
      RAISE EXCEPTION 'FIELD_NOT_FOUND';
   end if;
   RETURN;
END; $$;


