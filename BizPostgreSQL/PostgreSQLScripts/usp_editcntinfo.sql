-- Stored procedure definition script USP_EditCntInfo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EditCntInfo(v_numContactID NUMERIC(9,0) ,    
v_numDomainID NUMERIC(9,0)  
 ,  
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId, A.vcGivenName, A.vcFirstName,
                      A.vcLastname, D.numDivisionID, C.numCompanyId,
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,
					  coalesce((select vcData from Listdetails where numListItemID = C.numCompanyType),'') as vcCompanyTypeName,
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,
                      A.vcPosition, A.txtNotes, A.numCreatedBy,
                      A.numCell,A.numHomePhone,A.VcAsstFirstName,
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,
                      A.vcAsstEmail,A.charSex, A.vcDepartment,
                      AD.vcStreet AS vcpStreet,
                      AD.vcCity AS vcPCity,
                      fn_GetState(AD.numState) as State, fn_GetListName(AD.numCountry,0::BOOLEAN) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,
        A.numManagerID, A.vcCategory  , fn_GetContactName(A.numCreatedBy) || ' ' || CAST(A.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy,
          fn_GetContactName(A.numModifiedBy) || ' ' || CAST(A.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,vcTitle,vcAltEmail,
  fn_GetContactName(A.numRecOwner) as RecordOwner,  A.numRecOwner, D.numAssignedTo,  D.numTerID,
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,
  coalesce((select  case when bitEngaged = false then 'Disengaged' when bitEngaged = true then 'Engaged' end as ECampStatus  from ConECampaign
      where  numContactID = v_numContactID order by numConEmailCampID desc LIMIT 1),'-') as CampStatus ,
GetECamLastAct(v_numContactID) as Activity ,
(select  count(*) from GenericDocuments   where numRecID = v_numContactID and  vcDocumentSection = 'C') as DocumentCount,
(SELECT count(*) from CompanyAssociations where numDivisionID = v_numContactID and bitDeleted = false) as AssociateCountFrom,
(SELECT count(*) from CompanyAssociations where numAssociateFromDivisionID = v_numContactID and bitDeleted = false) as AssociateCountTo    ,
vcitemid ,
vcChangeKey,
vcCompanyName,
tintCRMType ,coalesce((select  cast(numUserId as NUMERIC(18,0)) from  UserMaster where numUserDetailId = A.numContactId LIMIT 1),0) as numUserID,
fn_GetListName(C.numNoOfEmployeesId,false) AS NoofEmp,coalesce(vcImageName,'') AS vcImageName
   FROM         AdditionalContactsInformation A
   INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID
   INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId
   LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID = A.numContactId AND AD.tintAddressOf = 1 AND AD.tintAddressType = 0 AND AD.bitIsPrimary = true
   WHERE     A.numContactId = v_numContactID and A.numDomainID = v_numDomainID;
END; $$;












