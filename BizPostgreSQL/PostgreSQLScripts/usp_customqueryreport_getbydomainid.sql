-- Stored procedure definition script USP_CustomQueryReport_GetByDomainID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_GetByDomainID(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		tintEmailFrequency
   FROM
   CustomQueryReport
   WHERE
   numDomainID = v_numDomainID;
END; $$;











