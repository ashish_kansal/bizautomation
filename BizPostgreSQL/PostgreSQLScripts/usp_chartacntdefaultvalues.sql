CREATE OR REPLACE PROCEDURE USP_ChartAcntDefaultValues
(
	v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numUserCntId NUMERIC(9,0) DEFAULT 0
)
LANGUAGE plpgsql
AS $$
DECLARE
   v_intCount NUMERIC(9,0);   
   v_dtTodayDate  TIMESTAMP;
   v_numAssetAccountTypeID  NUMERIC(9,0);
   v_numLiabilityAccountTypeID  NUMERIC(9,0);
   v_numIncomeAccountTypeID  NUMERIC(9,0); 
   v_numExpenseAccountTypeID  NUMERIC(9,0);
   v_numEquityAccountTypeID  NUMERIC(9,0);
   v_numCOGSAccountTypeID  NUMERIC(9,0);

   v_numLoanAccountTypeID  NUMERIC(9,0);
   v_numParentForCOGSAccID  NUMERIC(9,0);
   v_numRSccountTypeID  NUMERIC(9,0);
   v_numBankAccountTypeID  NUMERIC(9,0);
   v_numPropAccountTypeID  NUMERIC(9,0);
   v_numARAccountTypeID  NUMERIC(9,0);
   v_numStockAccountTypeID  NUMERIC(9,0);
   v_numDutyTaxAccountTypeID  NUMERIC(9,0);
   v_numAPAccountTypeID  NUMERIC(9,0);
   v_numCLAccountTypeID  NUMERIC(9,0);
   v_numWPAccountTypeID  NUMERIC(9,0);
   v_numCurrentLiabilitiesAccountTypeID  NUMERIC(9,0);
   v_numEquityID  NUMERIC(9,0);
   v_numOtherDirectExpenseID  NUMERIC(9,0);
   v_numCOGSExpenseID  NUMERIC(9,0);
   v_numDirectExpenseID  NUMERIC(9,0);
   v_numOpeExpenseID  NUMERIC(9,0);
   v_numIndirectExpenseID  NUMERIC(9,0);
   v_numAdminExpenseID  NUMERIC(9,0);
   v_numStaffExpenseID  NUMERIC(9,0);
   v_numWSID  NUMERIC(9,0);
   v_numDirectIncomeAccountTypeID  NUMERIC(9,0);
   v_numIndirectIncomeAccountTypeID  NUMERIC(9,0);
   v_numSDccountTypeID  NUMERIC(9,0);
   v_numFCccountTypeID  NUMERIC(9,0);
   v_numSEccountTypeID  NUMERIC(9,0);
   v_numNCEccountTypeID  NUMERIC(9,0);
   v_numOCccountTypeID  NUMERIC(9,0);
   v_numSFccountTypeID  NUMERIC(9,0);
   v_numUnCategorizedExpenseID  NUMERIC(9,0);
   v_strSQL  TEXT;
   v_ST  VARCHAR(10);
   v_AR  VARCHAR(10);
   v_SC  VARCHAR(10);
   v_DG  VARCHAR(10);
   v_SI  VARCHAR(10);
   v_LC  VARCHAR(10);
   v_AP  VARCHAR(10);
   v_UF  VARCHAR(10);
   v_BE  VARCHAR(10);
   v_CG  VARCHAR(10);
   v_EP  VARCHAR(10);
   v_CP  VARCHAR(10);
   v_PL  VARCHAR(10);
   v_WP  VARCHAR(10);
   v_DP  VARCHAR(10);       
   v_UI  VARCHAR(10);       
   v_UE  VARCHAR(10);       
   v_RC  VARCHAR(10);       
   v_FE  VARCHAR(10);       
   v_IA  VARCHAR(10);       
   v_OE  VARCHAR(10);       
   v_PC  VARCHAR(10);       
   v_PV  VARCHAR(10);       
   v_DI  VARCHAR(10);
   v_AssetAccountID  VARCHAR(10);
   v_IncomeAccountID  VARCHAR(10);
   v_COGSAccountID  VARCHAR(10);
   v_numModuleID  INTEGER;

------- UPDATE COARelationships FOR DEFAULT ACCOUNT SETTING FOR Accounts Receivable & Accounts Payable --------
   v_numCustRelationshipID  NUMERIC(10,0);
   v_numEmpRelationshipID  NUMERIC(10,0);
   v_numVendorRelationShipID  NUMERIC(10,0);

   v_numListID  NUMERIC(10,0);
   v_numShipIncomeID  NUMERIC(10,0);
   v_numCustCount  INTEGER;
   v_dtPeriodFrom  TIMESTAMP;
   v_dtPeriodTo  TIMESTAMP;
BEGIN        
	v_dtTodayDate := LOCALTIMESTAMP;                                                 
	v_intCount := 0;                                  
                                           
	SELECT COUNT(*) INTO v_intCount FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId;   
                                                  

    IF v_intCount = 0 THEN
		PERFORM USP_AccountTypeDefaultValue(v_numDomainId := v_numDomainId);
		
		--Check for Current Fiancial Year
		IF(SELECT COUNT(numFinYearId) FROM  FinancialYear WHERE numDomainId = v_numDomainId AND bitCurrentYear = true) = 0 THEN
			 v_dtPeriodFrom := '1900-01-01':: date+CAST(extract(year from LOCALTIMESTAMP) -extract(year from 0:: date) || 'year' as interval);
			 v_dtPeriodTo := '1900-01-01':: date -INTERVAL '1 day'+CAST(extract(year from LOCALTIMESTAMP) -extract(year from -1:: date) || 'year' as interval);
         
			 PERFORM USP_ManageFinancialYear(v_numDomainId := v_numDomainId,v_dtPeriodFrom := v_dtPeriodFrom,v_dtPeriodTo := v_dtPeriodTo,
			 v_vcFinYearDesc := '',v_bitCloseStatus := false,
			 v_bitAuditStatus := false,v_bitCurrentYear := true,SWV_RefCur := null);
		END IF;

		SELECT numAccountTypeId INTO v_numAssetAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = v_numDomainId;
		SELECT numAccountTypeId INTO v_numLiabilityAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = v_numDomainId; 
		SELECT numAccountTypeId INTO v_numIncomeAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0103' AND numDomainID = v_numDomainId;
		SELECT numAccountTypeId INTO v_numExpenseAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = v_numDomainId; 
		SELECT numAccountTypeId INTO v_numEquityAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = v_numDomainId;  
		SELECT numAccountTypeID INTO v_numLoanAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01020201'  AND numDomainID = v_numDomainId;  
		SELECT numParentID INTO v_numParentForCOGSAccID FROM AccountTypeDetail WHERE numDomainID = v_numDomainId AND vcAccountCode = '0101';
		SELECT numAccountTypeId INTO v_numCOGSAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = v_numDomainId; 
		SELECT numAccountTypeID INTO v_numRSccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = v_numDomainId;
		

		PERFORM usp_InsertNewChartAccountDetails(v_numAcntTypeId = v_numEquityAccountTypeID,
		v_numParntAcntTypeID = v_numRSccountTypeID, v_vcAccountName = 'Profit & Loss',
		v_vcAccountDescription = 'Profit & Loss',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = true, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost =0,
		v_vcNumber = '-1', v_bitIsSubAccount = false, v_numParentAccId = 0);
   
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numEquityAccountTypeID,
		v_numParntAcntTypeID = v_numRSccountTypeID, v_vcAccountName = 'Retained Earnings',
		v_vcAccountDescription = 'Retained Earnings', v_monOriginalOpeningBal = $0,
		v_monOpeningBal = $0, v_dtOpeningDate = v_dtTodayDate, v_bitActive = true,
		v_numAccountId = 0, v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0,
		v_numUserCntId = 1, v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1', v_bitIsSubAccount = false, v_numParentAccId = 0);


		PERFORM usp_InsertNewChartAccountDetails(v_numAcntTypeId = v_numEquityAccountTypeID,
		v_numParntAcntTypeID = v_numRSccountTypeID, v_vcAccountName = 'Opening Balance Equity',
		v_vcAccountDescription = 'Opening Balance Equity', v_monOriginalOpeningBal = $0,
		v_monOpeningBal = $0, v_dtOpeningDate = v_dtTodayDate, v_bitActive = true,
		v_numAccountId = 0, v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0,
		v_numUserCntId = 1, v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

   
		------------------------------------ ASSETS ENTRY --------------------------------------------
		SELECT numAccountTypeID INTO v_numBankAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01010101'  AND numDomainID = v_numDomainID; 

		--select numAccountTypeID,'UnDepositedFunds','UnDepositedFunds','0101010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01010101'
		PERFORM usp_InsertNewChartAccountDetails(v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numBankAccountTypeID , v_vcAccountName = 'UnDeposited Funds',
		v_vcAccountDescription = 'UnDepositedFunds',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		-----------------
		SELECT numAccountTypeID INTO v_numPropAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01010201'  AND numDomainID = v_numDomainID; 
    
		PERFORM usp_InsertNewChartAccountDetails(v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numPropAccountTypeID, v_vcAccountName = 'Office Equipment',
		v_vcAccountDescription = 'Office equipment that is owned and controlled by the business',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails( v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numPropAccountTypeID,
		v_vcAccountName = 'Less Accumulated Depreciation on Office Equipment',
		v_vcAccountDescription = 'The total amount of office equipment cost that has been consumed by the entity (based on the useful life)',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails( v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numPropAccountTypeID, v_vcAccountName = 'Computer Equipment',
		v_vcAccountDescription = 'Computer equipment that is owned and controlled by the business',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
-------------------------
		SELECT numAccountTypeID INTO v_numARAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = v_numDomainID;

		PERFORM usp_InsertNewChartAccountDetails( v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numARAccountTypeID, v_vcAccountName = 'Account Receivable',
		v_vcAccountDescription = 'Account Receivable.',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		PERFORM usp_InsertNewChartAccountDetails( v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numARAccountTypeID, v_vcAccountName = 'Prepayments',
		v_vcAccountDescription = 'An expenditure that has been paid for in advance.',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails( v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numARAccountTypeID, v_vcAccountName = 'Sales Clearing',
		v_vcAccountDescription = 'Sales Clearing.',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numStockAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01010104'  AND numDomainID = v_numDomainID;

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numStockAccountTypeID, v_vcAccountName = 'Inventory',
		v_vcAccountDescription = 'Items available for sale including all costs of production.',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numStockAccountTypeID, v_vcAccountName = 'Work In Progress',
		v_vcAccountDescription = 'Work In Progress',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);    

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numAssetAccountTypeID,
		v_numParntAcntTypeID = v_numStockAccountTypeID, v_vcAccountName = 'Finished Goods',
		v_vcAccountDescription = 'Finished Goods',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);     
		---------------------------------------------------------------------------------------------------------
		------------------------------------ LIABILITIES ENTRY --------------------------------------------------
		---------------------------------------------------------------------------------------------------------

		SELECT numAccountTypeID INTO v_numDutyTaxAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01020101'  AND numDomainID = v_numDomainID;

		--select numAccountTypeID,'Sales Tax Payable','Sales Tax Payable','0102010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01020101'   
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID =  v_numDutyTaxAccountTypeID , v_vcAccountName = 'Sales Tax Payable',
		v_vcAccountDescription = 'Sales Tax Payable',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numAPAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = v_numDomainID;  

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID =  v_numAPAccountTypeID , v_vcAccountName = 'Deduction from Employee Payroll',
		v_vcAccountDescription = 'Deduction from Employee Payroll',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID =  v_numAPAccountTypeID , v_vcAccountName = 'Accounts Payable',
		v_vcAccountDescription = 'Accounts Payable',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numAPAccountTypeID, v_vcAccountName = 'Employee Tax Payable',
		v_vcAccountDescription = 'The amount of tax that has been deducted from wages or salaries paid to employes and is due to be paid',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numAPAccountTypeID, v_vcAccountName = 'Income Tax Payable',
		v_vcAccountDescription = 'The amount of income tax that is due to be paid, also resident withholding tax paid on interest received.',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numCLAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = v_numDomainID;  

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numCLAccountTypeID, v_vcAccountName = 'Unpaid Expense Claims',
		v_vcAccountDescription = 'Automatically updates this account for payroll entries created using Payroll and will store the payroll amount to be paid to the employee for the pay run. This account enables you to maintain separate accounts for employee Wages Payable amounts and Accounts Payable amounts',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numAPAccountTypeID, v_vcAccountName = 'Wages Payable',
		v_vcAccountDescription = 'Office equipment that is owned and controlled by the business',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numCLAccountTypeID, v_vcAccountName = 'Tracking Transfers',
		v_vcAccountDescription = 'Transfers between tracking categories',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountId INTO v_numWPAccountTypeID FROM Chart_Of_Accounts WHERE numDomainID = v_numDomainID  AND vcAccountName = 'Wages Payable';

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numCLAccountTypeID, v_vcAccountName = 'Wages & Salaries Payable',
		v_vcAccountDescription = 'Wages & Salaries Payable',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = true, v_numParentAccId = v_numWPAccountTypeID);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numLoanAccountTypeID , v_vcAccountName = 'Loan',
		v_vcAccountDescription = 'Money that has been borrowed from a creditor',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount =false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numCurrentLiabilitiesAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '010201'  AND numDomainID = v_numDomainID;  

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
		v_numParntAcntTypeID = v_numCurrentLiabilitiesAccountTypeID, v_vcAccountName = 'Deferred Income',
		v_vcAccountDescription = 'Deferred Income',
		v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
		v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
		v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
		v_bitProfitLoss = false, v_bitDepreciation = false,
		v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
		v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		---------------------------------------------------------------------------------------------------
		------------------------------------ EQUITY ENTRY --------------------------------------------
		---------------------------------------------------------------------------------------------------

		SELECT numAccountTypeID INTO v_numEquityID FROM AccountTypeDetail WHERE vcAccountCode = '0105'  AND numDomainID = v_numDomainID; 
		---------------------------------------------------------------------------------------------------

		------------------------------------ EXPENSE ENTRY --------------------------------------------

		SELECT numAccountTypeID INTO v_numOtherDirectExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '01040101';

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOtherDirectExpenseID, v_vcAccountName = 'Billable Time & Expenses',
			v_vcAccountDescription = 'Office equipment that is owned and controlled by the business',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--select numAccountTypeID,'Purchase Account','Purchase','0104010402',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOtherDirectExpenseID, v_vcAccountName = 'Purchase',
			v_vcAccountDescription = 'Purchase Account',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOtherDirectExpenseID, v_vcAccountName = 'Reconciliation Discrepancies',
			v_vcAccountDescription = 'Reconciliation Discrepancies',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOtherDirectExpenseID, v_vcAccountName = 'Foreign Exchange Gain/Loss',
			v_vcAccountDescription = 'Foreign Exchange Gain/Loss',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
     
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOtherDirectExpenseID, v_vcAccountName = 'Inventory Adjustment',
			v_vcAccountDescription = 'Inventory Adjustment',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);


		SELECT numAccountTypeID INTO v_numCOGSExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '010601';
       
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numCOGSAccountTypeID,
			v_numParntAcntTypeID = v_numCOGSExpenseID, v_vcAccountName = 'Cost of Goods Sold',
			v_vcAccountDescription = 'Costs of goods made by the business include material, labor, and other modification costs.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);


		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numCOGSAccountTypeID,
			v_numParntAcntTypeID = v_numCOGSExpenseID, v_vcAccountName = 'Purchase Price Variance',
			v_vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);


		SELECT numAccountTypeID INTO v_numDirectExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '010401';
		SELECT numAccountTypeID INTO v_numOpeExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '01040103'; 
  
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOpeExpenseID, v_vcAccountName = 'Utilities',
			v_vcAccountDescription = 'Expenses incurred for lighting, powering or heating the premises',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
     
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOpeExpenseID, v_vcAccountName = 'Automobile Expenses',
			v_vcAccountDescription = 'Expenses incurred on the running of company automobiles.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOpeExpenseID, v_vcAccountName = 'Rent',
			v_vcAccountDescription = 'The payment to lease a building or area.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numOpeExpenseID, v_vcAccountName = 'Bad Debts',
			v_vcAccountDescription = 'Noncollectable accounts receivable which have been written off.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numIndirectExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '010402';
		SELECT numAccountTypeID INTO v_numAdminExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '01040201'; 

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Postage & Delivery',
			v_vcAccountDescription = 'Expenses incurred on postage & delivery costs.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Janitorial Expenses',
			v_vcAccountDescription = 'Expenses incurred for cleaning business property.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Consulting & Accounting',
			v_vcAccountDescription = 'Expenses related to paying consultants',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'General Expenses',
			v_vcAccountDescription = 'General expenses related to the running of the business.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Office Expenses',
			v_vcAccountDescription = 'General expenses related to the running of the business office.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Repairs and Maintenance',
			v_vcAccountDescription = 'Expenses incurred on a damaged or run down asset that will bring the asset back to its original condition.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
  
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Dues & Subscriptions',
			v_vcAccountDescription = 'E.g. Magazines, professional bodies',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);


		SELECT numAccountTypeID INTO v_numStaffExpenseID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID   AND vcAccountCode = '01040202';

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numStaffExpenseID, v_vcAccountName = 'Wages and Salaries',
			v_vcAccountDescription = 'Payment to employees in exchange for their resources',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numStaffExpenseID, v_vcAccountName = 'Travel',
			v_vcAccountDescription = 'Expenses incurred from travel which has a business purpose',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    

		SELECT numAccountID INTO v_numWSID FROM Chart_Of_Accounts WHERE numDomainID = v_numDomainID   AND vcAccountName = 'Wages and Salaries';

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numStaffExpenseID , v_vcAccountName = 'Employee payroll expense',
			v_vcAccountDescription = 'Employee payroll expense',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = true, v_numParentAccId = v_numWSID);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numStaffExpenseID, v_vcAccountName = 'Contract Employee Payroll Expense',
			v_vcAccountDescription = 'Contract Employee Payroll Expense',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = true, v_numParentAccId = v_numWSID);
  
		---------------------------------------------------------------------------------------------------
		------------------------------------ INCOME ENTRY -------------------------------------------- 
		---------------------------------------------------------------------------------------------------

		SELECT numAccountTypeID INTO v_numDirectIncomeAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '010301'  AND numDomainID = v_numDomainID; 

		--select numAccountTypeID,'Sales Discounts','Sales Discounts','01030101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010301'
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID = v_numDirectIncomeAccountTypeID , v_vcAccountName = 'Sales Discounts',
			v_vcAccountDescription = 'Sales Discounts',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--select numAccountTypeID,'Shipping Income','Shipping Income','01030201',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numAssetAccountTypeID,
			v_numParntAcntTypeID = v_numDirectIncomeAccountTypeID, v_vcAccountName = 'Shipping Income',
			v_vcAccountDescription = 'Shipping Income',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--select numAccountTypeID,'Late Charges','Late Charges','01030202',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numAssetAccountTypeID,
			v_numParntAcntTypeID = v_numDirectIncomeAccountTypeID, v_vcAccountName = 'Late Charges',
			v_vcAccountDescription = 'Late Charges',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID =  v_numDirectIncomeAccountTypeID, v_vcAccountName = 'Other Revenue',
			v_vcAccountDescription = 'Any other income that does not relate to normal business activities and is not recurring',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID = v_numDirectIncomeAccountTypeID , v_vcAccountName = 'Sales',
			v_vcAccountDescription = 'Sales',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
		-- Create Credit Card as a new account under Current liability.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
			v_numParntAcntTypeID =  v_numCLAccountTypeID, v_vcAccountName = 'Credit Card',
			v_vcAccountDescription = 'Credit Card',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--- Adding new account "Purchase Clearing" under Current liability.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numLiabilityAccountTypeID,
			v_numParntAcntTypeID =  v_numCLAccountTypeID, v_vcAccountName = 'Purchase Clearing',
			v_vcAccountDescription = 'Purchase Clearing',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numIndirectIncomeAccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '010302'  AND numDomainID = v_numDomainID; 

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID =  v_numIndirectIncomeAccountTypeID, v_vcAccountName = 'Interest Income',
			v_vcAccountDescription = 'Interest income',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Account with the name of �Dividend income� should be there under indirect income.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID =  v_numIndirectIncomeAccountTypeID, v_vcAccountName = 'Dividend Income',
			v_vcAccountDescription = 'Dividend income',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--------------------------------------------------------------------------------------------------------------------------------------
		-- DONE 																															--																																		    
		--Under expense side COGS accounts should not be under any other category except COGS so remove the same from other direct expense. --
		--Remove �office equipment� from operating expense type.																			--
		--------------------------------------------------------------------------------------------------------------------------------------

		--Transfer following accounts to Administrative Expense from Operating Expense.
		--Printing & Stationary,   telephone & Internet, Income Tax expense, Entertainment, Insurance.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Printing & Stationery',
			v_vcAccountDescription = 'Expenses incurred by the entity as a result of printing and stationery',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Telephone & Internet',
			v_vcAccountDescription = 'Expenditure incurred from any business-related phone calls, phone lines, or internet connections',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Income Tax Expense',
			v_vcAccountDescription = 'A percentage of total earnings paid to the government',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false , v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Entertainment',
			v_vcAccountDescription = 'Expenses paid by company for the business but are not deductable for income tax purposes.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Insurance',
			v_vcAccountDescription = 'Expenses incurred for insuring the business'' assets',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
           
		--Advertising expense should be moved to Selling & Distribution from operating expense.
		SELECT numAccountTypeID INTO v_numSDccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01040102'  AND numDomainID = v_numDomainID;  

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numSDccountTypeID, v_vcAccountName = 'Advertising',
			v_vcAccountDescription = 'Expenses incurred for advertising while trying to increase sales',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--�Interest Expense� should be under Finance Cost rather than Operating Expense.
		SELECT numAccountTypeID INTO v_numFCccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01040203'  AND numDomainID = v_numDomainID;

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numFCccountTypeID, v_vcAccountName = 'Interest Expense',
			v_vcAccountDescription = 'Any interest expenses paid to your tax authority, business bank accounts or credit card accounts.',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Move bank Service charges under finance Costs.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numFCccountTypeID, v_vcAccountName = 'Bank Service Charges',
			v_vcAccountDescription = 'Fees charged by your bank for transactions regarding your bank account(s).',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Move �Payroll tax expense� to Staff expense from operating expense.

		SELECT numAccountTypeID INTO v_numSEccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01040202'  AND numDomainID = v_numDomainID;

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numSEccountTypeID, v_vcAccountName = 'Payroll Tax Expense',
			v_vcAccountDescription = '', v_monOriginalOpeningBal = $0,
			v_monOpeningBal = $0, v_dtOpeningDate = v_dtTodayDate, v_bitActive = true,
			v_numAccountId = 0, v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0,
			v_numUserCntId = 1, v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Move �Depreciation� from administrative expense to Non-cash Expense.
		SELECT numAccountTypeID INTO v_numNCEccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01040205'  AND numDomainID = v_numDomainID; 
 
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numNCEccountTypeID, v_vcAccountName = 'Depreciation',
			v_vcAccountDescription = 'The amount of the asset''s cost (based on the useful life) that was consumed during the period',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Create new account with the name �Amortization� under non-cash expense.   

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numNCEccountTypeID, v_vcAccountName = 'Amortization',
			v_vcAccountDescription = 'Amortization',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
    
		--Legal Expense should be under Administrative Expenses.
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numAdminExpenseID, v_vcAccountName = 'Legal Expenses',
			v_vcAccountDescription = 'Expenses incurred on any legal matters',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		--Owner�s contribution, owner�s draw, and common stock should be under Shareholder�s Fund.
		SELECT numAccountTypeID INTO v_numOCccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '010502'  AND numDomainID = v_numDomainID; 
        
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numEquityAccountTypeID,
			v_numParntAcntTypeID = v_numOCccountTypeID, v_vcAccountName = 'Owner''s Contribution',
			v_vcAccountDescription = 'Funds contributed by the owner',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numEquityAccountTypeID,
			v_numParntAcntTypeID = v_numOCccountTypeID, v_vcAccountName = 'Owner''s Draw',
			v_vcAccountDescription = 'Withdrawals by the owners',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		SELECT numAccountTypeID INTO v_numSFccountTypeID FROM AccountTypeDetail WHERE vcAccountCode = '01050102'  AND numDomainID = v_numDomainID; 

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numEquityAccountTypeID,
			v_numParntAcntTypeID = v_numSFccountTypeID, v_vcAccountName = 'Common Stock',
			v_vcAccountDescription = 'The value of shares purchased by the shareholders',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);
        
    
		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numIncomeAccountTypeID,
			v_numParntAcntTypeID = v_numIndirectIncomeAccountTypeID , v_vcAccountName = 'UnCategorized Income',
			v_vcAccountDescription = 'UnCategorized Income',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		------- Add new Account type UnCategorized Expense and move account "UnCategorized Expense" under it 
		SELECT numAccountTypeID INTO v_numUnCategorizedExpenseID FROM AccountTypeDetail WHERE vcAccountCode = '01040206'  AND numDomainID = v_numDomainID; 

		PERFORM usp_InsertNewChartAccountDetails (v_numAcntTypeId = v_numExpenseAccountTypeID,
			v_numParntAcntTypeID = v_numUnCategorizedExpenseID , v_vcAccountName = 'UnCategorized Expense',
			v_vcAccountDescription = 'UnCategorized Expense',
			v_monOriginalOpeningBal = $0, v_monOpeningBal = $0,
			v_dtOpeningDate = v_dtTodayDate, v_bitActive = true, v_numAccountId = 0,
			v_bitFixed = false, v_numDomainID = v_numDomainID   , v_numListItemID = 0, v_numUserCntId = 1,
			v_bitProfitLoss = false, v_bitDepreciation = false,
			v_dtDepreciationCostDate = '1753-01-01', v_monDepreciationCost = 0,
			v_vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, v_bitIsSubAccount = false, v_numParentAccId = 0);

		----------------
		DELETE FROM AccountingCharges WHERE numDomainID = v_numDomainID;

		SELECT coalesce(numAccountId,0) INTO v_ST FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Sales Tax Payable';
		SELECT coalesce(numAccountId,0) INTO v_AR FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Account Receivable';
		SELECT coalesce(numAccountId,0) INTO v_SC FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Sales Clearing';
		SELECT coalesce(numAccountId,0) INTO v_DG FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Sales Discounts';
		SELECT coalesce(numAccountId,0) INTO v_SI FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Shipping Income';
		SELECT coalesce(numAccountId,0) INTO v_LC FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Late Charges';
		SELECT coalesce(numAccountId,0) INTO v_AP FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Accounts Payable';
		SELECT coalesce(numAccountId,0) INTO v_UF FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'UnDeposited Funds';
		SELECT coalesce(numAccountId,0) INTO v_BE FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Billable Time & Expenses';
		SELECT coalesce(numAccountId,0) INTO v_CG FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Cost of Goods Sold';
		SELECT coalesce(numAccountId,0) INTO v_EP FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Employee Payroll Expense';
		SELECT coalesce(numAccountId,0) INTO v_CP FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Contract Employee Payroll Expense';
		SELECT coalesce(numAccountId,0) INTO v_PL FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Wages & Salaries Payable';
		SELECT coalesce(numAccountId,0) INTO v_WP FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Work in Progress';
		SELECT coalesce(numAccountId,0) INTO v_DP FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Deduction from Employee Payroll';
		SELECT coalesce(numAccountId,0) INTO v_UI FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'UnCategorized Income';
		SELECT coalesce(numAccountId,0) INTO v_UE FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND coalesce(vcAccountName,'') = 'UnCategorized Expense';
		SELECT coalesce(numAccountId,0) INTO v_RC FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Reconciliation Discrepancies';
		SELECT coalesce(numAccountId,0) INTO v_FE FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Foreign Exchange Gain/Loss';
		SELECT coalesce(numAccountId,0) INTO v_IA FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Inventory Adjustment';
		SELECT coalesce(numAccountId,0) INTO v_OE FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Opening Balance Equity';
		SELECT coalesce(numAccountId,0) INTO v_PC FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Purchase Clearing';
		SELECT coalesce(numAccountId,0) INTO v_PV FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Purchase Price Variance';
		SELECT coalesce(numAccountId,0) INTO v_DI FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND vcAccountName = 'Deferred Income';

		v_strSQL := ' PERFORM USP_ManageDefaultAccountsForDomain v_numDomainID = ' || CAST(v_numDomainID AS VARCHAR(10)) || ',@str=''<NewDataSet>
																			  <Table1>
																				<chChargeCode>EP</chChargeCode>
																				<numAccountID>' + coalesce(@EP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CP</chChargeCode>
																				<numAccountID>' + coalesce(@CP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>ST</chChargeCode>
																				<numAccountID>' + coalesce(@ST,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AR</chChargeCode>
																				<numAccountID>' + coalesce(@AR,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DG</chChargeCode>
																				<numAccountID>' + coalesce(@DG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SI</chChargeCode>
																				<numAccountID>' + coalesce(@SI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>LC</chChargeCode>
																				<numAccountID>' + coalesce(@LC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AP</chChargeCode>
																				<numAccountID>' + coalesce(@AP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UF</chChargeCode>
																				<numAccountID>' + coalesce(@UF,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>BE</chChargeCode>
																				<numAccountID>' + coalesce(@BE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CG</chChargeCode>
																				<numAccountID>' + coalesce(@CG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PL</chChargeCode>
																				<numAccountID>' + coalesce(@PL,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>WP</chChargeCode>
																				<numAccountID>' + coalesce(@WP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DP</chChargeCode>
																				<numAccountID>' + coalesce(@DP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UI</chChargeCode>
																				<numAccountID>' + coalesce(@UI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UE</chChargeCode>
																				<numAccountID>' + coalesce(@UE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>RC</chChargeCode>
																				<numAccountID>' + coalesce(@RC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>FE</chChargeCode>
																				<numAccountID>' + coalesce(@FE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>IA</chChargeCode>
																				<numAccountID>' + coalesce(@IA,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>OE</chChargeCode>
																				<numAccountID>' + coalesce(@OE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PC</chChargeCode>
																				<numAccountID>' + coalesce(@PC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PV</chChargeCode>
																				<numAccountID>' + coalesce(@PV,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SC</chChargeCode>
																				<numAccountID>' + coalesce(@SC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DI</chChargeCode>
																				<numAccountID>' + coalesce(@DI,'0') + '</numAccountID>
																			  </Table1>
																			</NewDataSet>''';

		EXECUTE v_strSQL;

		------- Setting up Default Accounts for Income, Asset & COGS of domain. ---------------------------------------
		SELECT numAccountId INTO v_AssetAccountID FROM Chart_Of_Accounts WHERE vcAccountName = 'Inventory' AND numDomainID = v_numDomainID;
		SELECT numAccountId INTO v_IncomeAccountID FROM Chart_Of_Accounts WHERE vcAccountName = 'Sales' AND numDomainID = v_numDomainID;
		SELECT numAccountId INTO v_COGSAccountID FROM Chart_Of_Accounts WHERE vcAccountName = 'Cost of Goods Sold' AND numDomainID = v_numDomainID;

		UPDATE Domain SET numAssetAccID = v_AssetAccountID, numIncomeAccID = v_IncomeAccountID, numCOGSAccID = v_COGSAccountID WHERE numDomainId = v_numDomainID;
		------------------------------------------------------------
		SELECT numListID INTO v_numListID FROM ListMaster WHERE vcListName = 'Relationship'  AND bitFlag = true;
		SELECT numListItemID INTO v_numCustRelationshipID FROM ListDetails WHERE numListID = v_numListID AND constFlag = true AND vcData = 'Customer';
		SELECT numListItemID INTO v_numEmpRelationshipID FROM ListDetails WHERE numListID = v_numListID AND constFlag = true AND vcData = 'Employer';
		SELECT numListItemID INTO v_numVendorRelationShipID FROM ListDetails WHERE numListID = v_numListID AND constFlag = true AND vcData = 'Vendor';


		DELETE FROM COARelationships WHERE numDomainID = v_numDomainID;

		INSERT INTO COARelationships (numRelationshipID,numARParentAcntTypeID,numAPParentAcntTypeID,numARAccountId,numAPAccountId,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy) 
		SELECT v_numCustRelationshipID,v_numARAccountTypeID,v_numAPAccountTypeID,v_AR,v_AP,v_numDomainID,GETDATE(),NULL,NULL,NULL
		UNION ALL
		SELECT v_numEmpRelationshipID,v_numARAccountTypeID,v_numAPAccountTypeID,v_AR,v_AP,v_numDomainID,GETDATE(),NULL,NULL,NULL
		UNION ALL
		SELECT v_numVendorRelationShipID,v_numARAccountTypeID,v_numAPAccountTypeID,v_AR,v_AP,v_numDomainID,GETDATE(),NULL,NULL,NULL;
		---------------------------------------------------------------------------------------------------------------------
		------- UPDATE COAShippingMapping FOR DEFAULT SHIPPING METHOD MAPPING SETTING ---------------------------------
		---------------------------------------------------------------------------------------------------------------------

		DELETE FROM COAShippingMapping WHERE numDomainID = v_numDomainID;

		SELECT numAccountId INTO v_numShipIncomeID FROM Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = v_numDomainID;


		INSERT INTO COAShippingMapping (numShippingMethodID,numIncomeAccountID,numDomainID) 
		SELECT 88,v_numShipIncomeID,v_numDomainID
		UNION ALL
		SELECT 91,v_numShipIncomeID,v_numDomainID
		UNION ALL
		SELECT 90,v_numShipIncomeID,v_numDomainID;

		DROP TABLE IF EXISTS TempAccounts;
		CREATE TEMP TABLE TempAccounts AS
        SELECT  *
        FROM    ( SELECT    numAccountId,
                            vcAccountName,
                            COA.vcAccountCode,
                            COA.numParntAcntTypeId,
                            COA.numAcntTypeId OLDnumAcntTypeId,
                            ( SELECT    ATD.numAccountTypeID
                                FROM      AccountTypeDetail ATD
                                WHERE     ATD.numDomainID = COA.numDomainID
                                        AND ATD.vcAccountCode = SUBSTRING(COA.vcAccountCode, 1, 4)
                            ) NEWnumAcntTypeId
                    FROM      Chart_Of_Accounts COA
                    WHERE     COA.numDomainId = v_numDomainID
                ) X
        WHERE   X.OLDnumAcntTypeId <> X.NEWnumAcntTypeId;



        UPDATE  Chart_Of_Accounts
        SET     numAcntTypeId = X.NEWnumAcntTypeId
        FROM    TempAccounts X
        WHERE   X.numAccountID = Chart_Of_Accounts.numAccountId;

        DROP TABLE TempAccounts;


    
        SELECT COUNT(*) INTO v_numCustCount FROM ListDetails WHERE numListID = 5 AND (constFlag = true OR numDomainID = v_numDomainID) AND vcData = 'Customer';

        IF v_numCustCount > 0 THEN
                INSERT  INTO COARelationships
                        SELECT  ( SELECT    numListItemId
                                    FROM      ListDetails
                                    WHERE     numListID = 5
                                            AND ( constFlag = 1
                                                    OR numDomainID = v_numDomainID
                                                )
                                            AND vcData = 'Customer'
                                ),
                                numAcntTypeID,
                                NULL,
                                numAccountId,
                                NULL,
                                v_numDomainID,
                                now(),
                                NULL,
                                v_numUserCntId,
                                NULL
                        FROM    chart_of_accounts
                        WHERE   numDomainID = v_numDomainID
                                AND vcAccountCode = '0101010501';
        END IF;

        SET v_numCustCount = 0;
        SELECT  COUNT(*) INTO v_numCustCount FROM ListDetails WHERE numListID = 5 AND (constFlag = true OR numDomainID = v_numDomainID) AND vcData = 'Vendor';

        IF v_numCustCount > 0 THEN
            INSERT INTO COARelationships
                        SELECT  ( SELECT    numListItemId
                                    FROM      ListDetails
                                    WHERE     numListID = 5
                                            AND ( constFlag = 1
                                                    OR numDomainID = v_numDomainID
                                                )
                                            AND vcData = 'Vendor'
                                ),
                                NULL,
                                numAcntTypeID,
                                NULL,
                                numAccountId,
                                v_numDomainID,
                                now(),
                                NULL,
                                v_numUserCntId,
                                NULL
                        FROM    chart_of_accounts
                        WHERE   numDomainID = v_numDomainID
                                AND vcAccountCode = '0102010201';
        END IF;
    END IF;
END; $$;
