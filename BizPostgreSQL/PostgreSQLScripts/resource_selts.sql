-- Stored procedure definition script Resource_SelTs for PostgreSQL
CREATE OR REPLACE FUNCTION Resource_SelTs(v_ResourceID		INTEGER		-- primary key number of the resource
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(_ts as VARCHAR(255))
   FROM
   Resource
   WHERE
   Resource.ResourceID = v_ResourceID;
END; $$;













