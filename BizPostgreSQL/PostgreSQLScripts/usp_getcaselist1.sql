DROP FUNCTION IF EXISTS USP_GetCaseList1;

CREATE OR REPLACE FUNCTION USP_GetCaseList1(v_numUserCntId NUMERIC(9,0) DEFAULT 0,                                            
	 v_tintSortOrder NUMERIC DEFAULT 0,                                                                                
	 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                            
	 v_tintUserRightType SMALLINT DEFAULT NULL,                                                            
	 v_SortChar CHAR(1) DEFAULT '0' ,                                                                                                                   
	 v_CurrentPage INTEGER DEFAULT NULL,                                                            
	 v_PageSize INTEGER DEFAULT NULL,                                                            
	 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                            
	 v_columnName VARCHAR(50) DEFAULT NULL,                                                            
	 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                          
	 v_numDivisionID NUMERIC(9,0) DEFAULT 0,                              
	 v_bitPartner BOOLEAN DEFAULT false,
	 v_vcRegularSearchCriteria VARCHAR(1000) DEFAULT '',
	 v_vcCustomSearchCriteria VARCHAR(1000) DEFAULT '' ,
	 v_ClientTimeZoneOffset INTEGER DEFAULT NULL,                                                          
	 v_numStatus BIGINT DEFAULT 0,
	 v_SearchText TEXT DEFAULT '',
	 INOUT SWV_RefCur refcursor default null,
	 INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Nocolumns  SMALLINT;                
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_strColumns  TEXT;

   v_tintOrder  SMALLINT;                                                  
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcListItemType1  VARCHAR(1);                                                 
   v_vcAssociatedControlType  VARCHAR(20);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_vcDbColumnName  VARCHAR(200);                      
   v_WhereCondition  VARCHAR(2000);                       
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;          
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;   
   v_bitAllowEdit BOOLEAN;                 
   v_vcColumnName  VARCHAR(500);                  
   v_SearchQuery  TEXT DEFAULT '';            
   v_ListRelID  NUMERIC(9,0); 

   v_strShareRedordWith  VARCHAR(300);
   v_strExternalUser  TEXT DEFAULT '';
   v_StrSql  TEXT DEFAULT '';
	                                                        
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_firstRec  INTEGER;                                                            
   v_lastRec  INTEGER;                                                            
   v_strTotal  TEXT;
   v_strFinal  TEXT;
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );


   v_Nocolumns := 0;                
 
   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns FROM(SELECT COUNT(*) AS TotalRow from View_DynamicColumns where numFormId = 12 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainID AND tintPageType = 1
      UNION
      SELECT count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 12 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainID AND tintPageType = 1) TotalRows;

	--Check if Master Form Configuration is created by administrator if NoColumns=0
   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntId;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 12 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;


	--If MasterConfiguration is available then load it otherwise load default columns
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      12,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntId,CAST(null as INTEGER),1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 12 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      12,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntId,CAST(null as INTEGER),1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 12 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 12 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,false,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 12 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      if v_Nocolumns = 0 then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         select 12,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntId,NULL,1,false,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = 12 and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
	 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
	,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM View_DynamicColumns
      where numFormId = 12 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainID AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,
	 vcAssociatedControlType,'' as vcListItemType,numListID,''
	,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,false,''
      from View_DynamicCustomColumns
      where numFormId = 12 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainID AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      order by tintOrder asc;
   end if;     

   v_strColumns := ' ADC.numContactId,DM.numDivisionID,COALESCE(DM.numTerID,0) numTerID,cs.numRecOwner,COALESCE(cs.numAssignedTo,0) AS numAssignedTo,DM.tintCRMType,cs.numCaseID,cs.vcCaseNumber,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail ';                

   v_tintOrder := 0;                                                  
   v_WhereCondition := '';    
	--set @DefaultNocolumns=  @Nocolumns              
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
   tt_TEMPFORM    ORDER BY
   tintOrder
   ASC LIMIT 1;            
                                        
   WHILE v_tintOrder > 0 LOOP
      if v_bitCustom = false then
         if v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         if v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         if v_vcLookBackTableName = 'Cases' then
            v_Prefix := 'Cs.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         if v_vcAssociatedControlType = 'SelectBox' then
     
            if v_vcListItemType = 'LI' then
     
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
		  
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
     
               v_strColumns := coalesce(v_strColumns,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
		  
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
		  
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
		
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
		  
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            IF v_vcDbColumnName = 'intTargetResolveDate' then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') ||' AS DATE) = CAST(now() + make_interval(days => -1)  AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE) = CAST(now() + make_interval(days => 1)  AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ','|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
			
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
            ELSE
				v_strColumns := coalesce(v_strColumns,'')
				|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => -1)  AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => 1)  AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
				v_strColumns := coalesce(v_strColumns,'')
				|| 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || '),'|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';

               IF LENGTH(v_SearchText) > 0 then
		
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
	   
            IF v_vcDbColumnName = 'vcCompactContactDetails' then
		
               v_strColumns := coalesce(v_strColumns,'') || ' ,'''' AS vcCompactContactDetails';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || case
               WHEN v_vcDbColumnName = 'numAge' then 'year(getutcdate())-year(bintDOB)'
               WHEN v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'textSubject' then 'textSubject'
               WHEN v_vcDbColumnName = 'textDesc' then    'textDesc'
               WHEN v_vcDbColumnName = 'textInternalComments' then 'textInternalComments'
               ELSE v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
            IF LENGTH(v_SearchText) > 0 then
		 
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) ||(case
               WHEN v_vcDbColumnName = 'numAge' then 'year(getutcdate())-year(bintDOB)'
               WHEN v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'textSubject' then 'textSubject'
               WHEN v_vcDbColumnName = 'textDesc' then    'textDesc'
               WHEN v_vcDbColumnName = 'textInternalComments' then 'textInternalComments'
               ELSE v_vcDbColumnName end) || ' ILIKE ''%' || coalesce(v_SearchText,'') || '%'' ';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
  
            v_strColumns := coalesce(v_strColumns,'') || ', ' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'SoultionName'
		 THEN v_strColumns := coalesce(v_strColumns,'') || ',CONCAT(''<a id="SolutionForCase" class="normal1Right" onclick="return openSolutionPopup('',cs.numCaseId,'')" href="#" style="font-weight:bold;"><img src="../images/solutions.png">Solutions</a>'') "' || coalesce(v_vcColumnName,'') || '"';
		 ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
         then
		
            v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(
	(SELECT string_agg(CONCAT(A.vcFirstName,'' '',A.vcLastName,CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('',fn_GetListItemName(SR.numContactType),'')'' ELSE '''' END),'','')
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Cs.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=Cs.numCaseID
				AND UM.numDomainID=Cs.numDomainID and UM.numDomainID=A.numDomainID),'''') "' || coalesce(v_vcColumnName,'') || '"';
		ELSE
			v_strColumns := coalesce(v_strColumns,'') || ', ' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      Else
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strColumns := coalesce(v_strColumns,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Case CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '         
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=cs.numCaseId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
   
            v_strColumns := coalesce(v_strColumns,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Case CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=cs.numCaseId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strColumns := coalesce(v_strColumns,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Case CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '         
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=cs.numCaseId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Case CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '         
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=cs.numCaseId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=(CASE WHEN isnumeric(' || 'CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value' || ') THEN ' || 'CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value' || ' ELSE ''0'' END)::NUMERIC';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;    

   v_strShareRedordWith := ' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=7 AND SR.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ') ';
          
   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=CAST(',v_numUserCntId,' AS VARCHAR) AND EA.numDivisionID=DM.numDivisionID))');
		                                                   
   v_StrSql := coalesce(v_StrSql,'') || '  FROM AdditionalContactsInformation ADC                                                             
	 JOIN Cases  Cs                                                              
	   ON ADC.numContactId =Cs.numContactId                                                             
	 JOIN DivisionMaster DM                                                              
	  ON ADC.numDivisionId = DM.numDivisionID                                                             
	  JOIN CompanyInfo CMP                                                             
	 ON DM.numCompanyID = CMP.numCompanyId                                                             
	 left join listdetails lst                                                            
	 on lst.numListItemID= cs.numPriority
	 left join listdetails lst1                                                            
	 on lst1.numListItemID= cs.numStatus                                                           
	 left join AdditionalContactsInformation ADC1                          
	 on Cs.numAssignedTo=ADC1.numContactId                                 
	  ' || coalesce(v_WhereCondition,'');  
	 
   IF v_bitPartner = true then
      v_StrSql := coalesce(v_StrSql,'') || ' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '';
   end if;                              

   if v_columnName ilike 'CFW.Cust%' then
	
      v_columnName := 'CFW.Fld_Value';
      v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= ' || replace(v_columnName,'CFW.Cust','');
   end if;                                 
   if v_columnName ilike 'DCust%' then
	
      v_columnName := 'LstCF.vcData';
      v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= ' || replace(v_columnName,'DCust','');
      v_StrSql := coalesce(v_StrSql,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=(CASE WHEN isnumeric(CFW.Fld_Value) THEN CFW.Fld_Value ELSE ''0'' END)::NUMERIC';
   end if; 

	       
-------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 12 AND DFCS.numFormID = 12 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
---------------------------- 
 
   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      v_strColumns := coalesce(v_strColumns,'') || ',tCS.vcColorScheme';
   end if;
       

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      if v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      if v_vcCSLookBackTableName = 'Cases' then
         v_Prefix := 'Cs.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
			v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
	
         v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
      end if;
   end if;

   v_StrSql := coalesce(v_StrSql,'') || ' Where cs.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';
 
 
   IF v_numStatus <> 0 then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND cs.numStatus = ' || SUBSTR(CAST(v_numStatus AS VARCHAR(10)),1,10);
   ELSEIF v_tintSortOrder <> 5
   then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND cs.numStatus <> 136 ';
   end if;
                                                 
   if v_numDivisionID <> 0 then 
      v_StrSql := coalesce(v_StrSql,'') || ' And div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   end if;                                                      
   if v_SortChar <> '0' then 
      v_StrSql := coalesce(v_StrSql,'') || ' And (ADC.vcFirstName ILIKE ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                              
   if v_SortChar <> '0' then 
      v_StrSql := coalesce(v_StrSql,'') || ' or ADC.vcLastName ILIKE ''' || coalesce(v_SortChar,'') || '%'')';
   end if;                                                             

   IF v_tintSortOrder != 6 then
	
      if v_tintUserRightType = 1 then 
         v_StrSql := coalesce(v_StrSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) ||
         case when v_tintSortOrder = 1 THEN ' OR (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)' ELSE '' end
         || case when v_bitPartner = true then ' or (CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')' else '' end  || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
      ELSEIF v_tintUserRightType = 2
      then 
         v_StrSql := coalesce(v_StrSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' ) or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
   ELSE
      v_StrSql := coalesce(v_StrSql,'') || ' AND (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)';
   end if;                 
                                                   
   if v_tintSortOrder = 0 then  
      v_StrSql := coalesce(v_StrSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
   ELSEIF v_tintSortOrder = 2
   then  
      v_StrSql := coalesce(v_StrSql,'') || 'And cs.numStatus<>136  ' || ' AND cs.bintCreatedDate> ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 3
   then  
      v_StrSql := coalesce(v_StrSql,'') || 'And cs.numStatus<>136  ' || ' and cs.numCreatedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 4
   then  
      v_StrSql := coalesce(v_StrSql,'') || 'And cs.numStatus<>136  ' || ' and cs.numModifiedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 5
   then  
      v_StrSql := coalesce(v_StrSql,'') || 'And cs.numStatus=136  ';
   end if; --+ ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
 
   IF v_vcRegularSearchCriteria <> '' then 
      v_StrSql := coalesce(v_StrSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if; 
   IF v_vcCustomSearchCriteria <> '' then 
      v_StrSql := coalesce(v_StrSql,'') || ' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' || coalesce(v_vcCustomSearchCriteria,'') || ')';
   end if;


   IF LENGTH(v_SearchQuery) > 0 then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
   end if;	
	
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);  

	-- EXECUTE FINAL QUERY
	drop table IF EXISTS tt_TempGetCaseList1 CASCADE;
   v_strFinal := CONCAT('CREATE TEMPORARY TABLE tt_TempGetCaseList1 AS SELECT ROW_NUMBER() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,') ID, ',v_strColumns,v_StrSql,';');
   EXECUTE v_strFinal;

   EXECUTE 'SELECT COUNT(*) FROM tt_TempGetCaseList1;' INTO v_TotRecs;

   open SWV_RefCur for EXECUTE CONCAT('SELECT * FROM tt_TempGetCaseList1 WHERE ID > ', v_firstRec,' and ID <',v_lastRec,';');
	
   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   RETURN;
END; $$;


