-- Stored procedure definition script USP_ManageOpportunitySalesTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageOpportunitySalesTemplate(INOUT v_numSalesTemplateID NUMERIC(9,0) ,
	v_vcTemplateName VARCHAR(50),
	v_tintType SMALLINT,
	v_numOppId NUMERIC(9,0),
	v_numDivisionID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0),
	v_numUserCntID NUMERIC(9,0),
	v_tintAppliesTo SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDivisionID = 0 then
      select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numOppId = v_numOppId;
   end if;
   IF v_numOppId = 0 then
      v_numOppId := NULL;
   end if;

   IF v_numSalesTemplateID = 0 then

      INSERT INTO OpportunitySalesTemplate(vcTemplateName,
		tintType,
		numOppId,
		numDivisionID,
		numDomainID,
		numCreatedby,
		numModifiedBy,
		tintAppliesTo)
	VALUES(v_vcTemplateName,
		v_tintType,
		v_numOppId,
		v_numDivisionID,
		v_numDomainID,
		v_numUserCntID,
		v_numUserCntID,
		v_tintAppliesTo);
	
      v_numSalesTemplateID := CURRVAL('OpportunitySalesTemplate_seq');
      open SWV_RefCur for
      SELECT v_numSalesTemplateID As InsertedID;
   ELSE
      UPDATE OpportunitySalesTemplate SET
      vcTemplateName = v_vcTemplateName,tintType = v_tintType,numOppId = v_numOppId,
      numDivisionID = v_numDivisionID,numDomainID = v_numDomainID,dtModifiedDate = TIMEZONE('UTC',now()),
      numModifiedBy = v_numUserCntID,tintAppliesTo = v_tintAppliesTo
      WHERE numSalesTemplateID = v_numSalesTemplateID;
   end if;
   RETURN;
END; $$;


