-- Stored procedure definition script USP_InboxTreeSort_NewSortOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InboxTreeSort_NewSortOrder(
	v_numUserCntID INTEGER,v_numDomainID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   	-- Add the parameters for the stored procedure here
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_numParentNodeID  NUMERIC(18,0);
   v_lastParentid  INTEGER;
BEGIN


	-- Insert statements for procedure here
   IF Not Exists(Select 'col1' from  InboxTreeSort where numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem)
		VALUES(NULL,NULL,v_numDomainID,v_numUserCntID,1,true) RETURNING numNodeID INTO v_lastParentid;

      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES(v_lastParentid,'Inbox',v_numDomainID,v_numUserCntID,2,true,1);
		
      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES(v_lastParentid,'Email Archive',v_numDomainID,v_numUserCntID,3,true,2);
		
      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES(v_lastParentid,'Sent Messages',v_numDomainID,v_numUserCntID,4,true,4);
		
      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES(v_lastParentid,'Calendar',v_numDomainID,v_numUserCntID,5,true,5);
		
      INSERT INTO InboxTreeSort(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES(v_lastParentid,'Custom Folders',v_numDomainID,v_numUserCntID,6,true,6);

		--ALL RECORDS ARE ALREADY REPLACED WITH MASTER SCRIPT TO APPROPROATE ID FOR USERCNTID BUT
		--THERE ARE SOME RECORDS IN EMAIL HISTORY TABLE FOR SOME USERS WHOSE CORRESPONDING TREE STRUCTURE IS NOT AVILABLE IN INBOXTREESORT TABLE
		--FOLLOWING SCRIPTS UPDATED NODEID IN EMAIL HISTORY TABLE FOR INBOX=0, SENT EMAILS=4 AND DELETED MAILS=2 INCASE EXISTS BEFORE TREE STRUCTURE CREATED
		
      UPDATE EmailHistory SET numNodeId = coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 4),
      4) WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = 4 AND numNodeId <>(SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 4);
      UPDATE EmailHistory SET numNodeId = coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 2),
      2) WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = 2 AND numNodeId <>(SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 2);
      UPDATE EmailHistory SET numNodeId = coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 1),
      0) WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntID AND numNodeId = 0 AND numNodeId <>(SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numFixID,0) = 1);
   end if;

   DROP TABLE IF EXISTS tt_TMEP CASCADE;
   CREATE TEMPORARY TABLE tt_TMEP
   (
      numNodeID INTEGER,
      numParentId INTEGER,
      vcNodeName VARCHAR(100),
      numSortOrder INTEGER,
      bitSystem BOOLEAN,
      numFixID NUMERIC(18,0),
      bitHidden BOOLEAN
   );
   INSERT INTO
   tt_TMEP
   SELECT
   numNodeID,
		CAST(CASE WHEN numParentId = 0 THEN NULL ELSE numParentId END AS INTEGER) AS numParentID,
		vcNodeName as vcNodeName,
		numSortOrder,
		bitSystem,
		numFixID,
		coalesce(bitHidden,false) AS bitHidden
   FROM
   InboxTreeSort
   WHERE
   numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID 
   ORDER BY
   numSortOrder asc;

   select   numNodeID INTO v_numParentNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND coalesce(numParentId,0) = 0 AND coalesce(vcNodeName,'') = '';

   UPDATE tt_TMEP SET numParentId = NULL WHERE numParentId = v_numParentNodeID;
   UPDATE tt_TMEP SET numParentId = v_numParentNodeID WHERE numParentId NOT IN(SELECT numNodeID FROM tt_TMEP);
   UPDATE tt_TMEP SET numParentId = v_numParentNodeID WHERE numParentId IS NULL AND numNodeID <> v_numParentNodeID;


	--DELETE FROM @TMEP WHERE numNodeID = @numParentNodeID

   open SWV_RefCur for SELECT * FROM tt_TMEP ORDER BY numSortOrder ASC;
   RETURN;
END; $$;













