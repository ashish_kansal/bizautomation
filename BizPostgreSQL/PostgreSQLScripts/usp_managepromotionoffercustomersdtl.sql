-- Stored procedure definition script USP_ManagePromotionOfferCustomersDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
create or replace FUNCTION USP_ManagePromotionOfferCustomersDTL(v_numProID			NUMERIC(9,0) DEFAULT 0,  
	v_numProOrgId		NUMERIC(9,0) DEFAULT 0,    
	v_tintRuleAppType	SMALLINT DEFAULT NULL,  
	v_numDivisionID		NUMERIC(9,0) DEFAULT 0,
	v_numRelationship	NUMERIC(9,0) DEFAULT 0,  
	v_numProfile			NUMERIC(9,0) DEFAULT 0,  
	v_byteMode			SMALLINT DEFAULT NULL,
	v_numDomainID		NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
	
      IF NOT EXISTS(SELECT * FROM PromotionOfferOrganizations
      WHERE numProId = v_numProID
      AND numDivisionID = v_numDivisionID
      AND numRelationship = v_numRelationship
      AND numProfile = v_numProfile
      AND tintType = v_tintRuleAppType) then
		
         IF v_tintRuleAppType = 1 then
			
            DELETE FROM PromotionOfferOrganizations
            WHERE tintType = 2
            AND numProId = v_numProID
            AND numDivisionID <> v_numDivisionID;
         ELSEIF v_tintRuleAppType = 2
         then
			
            DELETE FROM PromotionOfferOrganizations
            WHERE tintType = 1
            AND numProId = v_numProID
            AND numDivisionID <> v_numDivisionID;
         end if;

         INSERT INTO PromotionOfferOrganizations(numProId, numDivisionID, numRelationship, numProfile, tintType)
			VALUES(v_numProID, v_numDivisionID, v_numRelationship, v_numProfile, v_tintRuleAppType);
      ELSE
         RAISE EXCEPTION 'DUPLICATE';
         RETURN;
      end if;
   ELSEIF v_byteMode = 1
   then
	
      DELETE FROM PromotionOfferOrganizations
      WHERE numProOrgId = v_numProOrgId;
   end if;
END; $$;


