CREATE OR REPLACE FUNCTION usp_DeleteProspect(v_numCompanyID NUMERIC,
	v_numDivisionID NUMERIC,
	v_numUserID NUMERIC,
	v_CRMType SMALLINT   
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_RowCount  NUMERIC;
BEGIN
   v_RowCount := 0;
	BEGIN

	DELETE FROM Communication WHERE numDivisionID = v_numDivisionID AND numContactId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID);
	DELETE FROM timeandexpense WHERE numOppId IN(SELECT numOppId FROM OpportunityMaster WHERE numDivisionId = v_numDivisionID);
	DELETE FROM OpportunityItems WHERE numOppId IN(SELECT numOppId FROM OpportunityMaster WHERE numDivisionId = v_numDivisionID);
	DELETE FROM OpportunityMaster WHERE numDivisionId = v_numDivisionID;
	DELETE FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID;
	DELETE FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
	
   select   Count(*) INTO v_RowCount FROM DivisionMaster WHERE numCompanyID = v_numCompanyID;
   IF v_RowCount = 0 then
      DELETE FROM CompanyInfo WHERE numCompanyId = v_numCompanyID AND numCreatedBy = v_numUserID;
   end if;

   EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
RETURN;
END; $$;


