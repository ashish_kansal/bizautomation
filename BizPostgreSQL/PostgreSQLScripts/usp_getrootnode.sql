-- Stored procedure definition script USP_GetRootNode for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRootNode(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId;
END; $$;












