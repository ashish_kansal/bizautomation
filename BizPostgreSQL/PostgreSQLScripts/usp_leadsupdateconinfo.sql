-- Stored procedure definition script USP_LeadsUpdateConInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LeadsUpdateConInfo(v_vcFirstName VARCHAR(50),        
v_vcLastName VARCHAR(50),        
v_vcEmail VARCHAR(50),        
v_numFollowUpStatus NUMERIC(9,0),        
v_numPosition NUMERIC(9,0),        
v_numPhone VARCHAR(15),        
v_numPhoneExtension VARCHAR(7),        
v_Comments TEXT DEFAULT '',        
v_numContactId NUMERIC(9,0) DEFAULT NULL,        
--@vcStreet as varchar(50),        
--@vcCity as varchar(50),        
--@intPostalCode as varchar(12),        
--@vcState as numeric(9),        
--@vcCountry as numeric(9),     
v_vcTitle VARCHAR(100) DEFAULT NULL,        
v_vcAltEmail VARCHAR(100) DEFAULT NULL,
v_numECampaignID NUMERIC DEFAULT NULL,
v_numUserCntID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Date  TIMESTAMP;
BEGIN
   update AdditionalContactsInformation
   set  vcFirstName = v_vcFirstName,vcLastname = v_vcLastName,vcEmail = v_vcEmail,vcPosition = v_numPosition,
   numPhone = v_numPhone,numPhoneExtension = v_numPhoneExtension,
   txtNotes = v_Comments,vcTitle = v_vcTitle,vcAltEmail = v_vcAltEmail,
   numECampaignID = v_numECampaignID,numModifiedBy = v_numUserCntID
   where numContactId = v_numContactId;  

/*Added by chintan BugID-262*/
   v_Date := GetUTCDateWithoutTime();
	  --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  datetime
	 --  bit
   IF v_numECampaignID = -1 then
      v_numECampaignID := 0;
   end if;
	 
   PERFORM USP_ManageConEmailCampaign(v_numConEmailCampID := 0,v_numContactId := v_numContactId,v_numECampaignID := v_numECampaignID,
   v_intStartDate := v_Date,v_bitEngaged := true,
   v_numUserCntID := v_numUserCntID);
   RETURN;
END; $$; --  numeric(9, 0)
  
--  
--update ContactAddress set  vcStreet=@vcStreet,        
--  vcCity=@vcCity,        
--  intPostalCode =@intPostalCode,        
--  vcState=@vcState,        
--  vcCountry=@vcCountry where   numContactId=@numContactId


