DROP FUNCTION IF EXISTS USP_BizRecurrence_Order;
  
Create or replace FUNCTION USP_BizRecurrence_Order(v_numRecConfigID NUMERIC(18,0), 
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	INOUT v_numRecurOppID NUMERIC(18,0) ,
	v_numFrequency INTEGER,
	v_dtEndDate DATE,
	v_Date DATE)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(18,0);
   v_numOppType  SMALLINT;
   v_tintOppStatus  SMALLINT;
   v_DealStatus  SMALLINT;
   v_numStatus  NUMERIC(9,0);
   v_numCurrencyID  NUMERIC(9,0);

   v_intOppTcode  NUMERIC(18,0);
   v_numNewOppID  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;


--Get Existing Opportunity Detail
   v_tintPageID  SMALLINT;
   v_TotalAmount  DOUBLE PRECISION DEFAULT 0;       
   v_tintCommitAllocation  SMALLINT;                                                    
   
   v_tintShipped  SMALLINT DEFAULT 0;                

   v_tintCRMType  NUMERIC(9,0);
       
   v_AccountClosingDate  TIMESTAMP; 
   v_numAddressID NUMERIC(18,0);
   v_vcStreet  VARCHAR(100);
   v_vcCity  VARCHAR(50);
   v_vcPostalCode  VARCHAR(15);
   v_vcCompanyName  VARCHAR(100);
   v_vcAddressName  VARCHAR(50);
   v_vcAltContact  VARCHAR(200);      
   v_numState  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numCompanyId  NUMERIC(9,0);
   v_numContact  NUMERIC(18,0);
   v_bitIsPrimary  BOOLEAN;
   v_bitAltContact  BOOLEAN;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_dtNextRecurrenceDate  DATE DEFAULT NULL;
BEGIN
   -- BEGIN TRANSACTION
BEGIN

--DECLARE @Date AS DATE = GETDATE()

      select   numDivisionId, tintopptype, tintoppstatus, numCurrencyID, numStatus, tintoppstatus INTO v_numDivisionID,v_numOppType,v_tintOppStatus,v_numCurrencyID,v_numStatus,
      v_DealStatus FROM
      OpportunityMaster WHERE
      numOppId = v_numOppID;
      IF coalesce(v_numCurrencyID,0) = 0 then

         v_fltExchangeRate := 1;
         select   coalesce(numCurrencyID,0) INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainID;
      ELSE
         v_fltExchangeRate := GetExchangeRate(v_numDomainID,v_numCurrencyID);
      end if;
      INSERT INTO OpportunityMaster(numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcpOppName,
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numrecowner, lngPConclAnalysis, tintopptype,
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numassignedto, numAssignedBy, tintoppstatus, numStatus,
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode,
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate,
	intUsedShippingCompany, bintAccountClosingDate)
      SELECT
      numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcpOppName,
	v_Date, v_numUserCntID, v_Date, numDomainId, v_numUserCntID, lngPConclAnalysis, tintopptype,
	numSalesOrPurType, v_numCurrencyID, v_fltExchangeRate, numassignedto, numAssignedBy, tintoppstatus, numStatus, vcOppRefOrderNo,
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator,
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, v_Date
      FROM
      OpportunityMaster
      WHERE
      numOppId = v_numOppID;
      v_numNewOppID := CURRVAL('OpportunityMaster_seq');
      PERFORM USP_OpportunityMaster_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numNewOppID);                                             
  
--Update OppName as per Name Template
      PERFORM USP_UpdateNameTemplateValue(v_numOppType::SMALLINT,v_numDomainID,v_numNewOppID);

--Map Custom Field	
      v_tintPageID := CASE WHEN v_numOppType = 1 THEN 2 ELSE 6 END;
      PERFORM USP_AddParentChildCustomFieldMap(v_numDomainID := v_numDomainID,v_numRecordID := v_numNewOppID,v_numParentRecId := v_numDivisionID,
      v_tintPageID := v_tintPageID::SMALLINT);
      IF coalesce(v_numStatus,0) > 0 AND v_numOppType = 1 AND coalesce(v_DealStatus,0) = 1 then

	 -- numeric(18, 0)
			 -- numeric(18, 0)
			 -- numeric(18, 0)
			 -- numeric(18, 0)
			 -- numeric(18, 0)
			 -- numeric(18, 0)
			 -- tinyint
         PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppID := v_numNewOppID,
         v_numOppBizDocsId := 0,v_numOrderStatus := v_numStatus,v_numUserCntID := v_numUserCntID,
         v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
      end if;
      IF coalesce(v_numStatus,0) > 0 AND v_numOppType = 1 AND coalesce(v_DealStatus,0) = 1 then

         IF v_numOppType = 1 AND v_tintOppStatus = 1 then
	
            PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numStatus);
         end if;
      end if;
      IF(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = v_numOppID) > 0 then

	-- INSERT OPPORTUNITY ITEMS

         INSERT INTO OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID)
         SELECT
         v_numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
         FROM
         OpportunityItems
         WHERE
         numOppId = v_numOppID;
         PERFORM USP_BizRecurrence_WorkOrder(v_numDomainID,v_numUserCntID,v_numNewOppID,v_numOppID);
         INSERT INTO OpportunityKitItems(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost)
         SELECT
         v_numNewOppID,OI.numoppitemtCode,OKI.numChildItemID,OKI.numWareHouseItemId,OKI.numQtyItemsReq,OKI.numQtyItemsReq,OKI.numUOMId,0,coalesce(I.monAverageCost,0)
         FROM
         OpportunityKitItems OKI
         INNER JOIN
         OpportunityItems OI
         ON
         OKI.numOppItemID = OI.numRecurParentOppItemID
         INNER JOIN
         Item I
         ON
         OKI.numChildItemID = I.numItemCode
         WHERE
         OKI.numOppId = v_numOppID
         AND OI.numOppId = v_numNewOppID;
         INSERT INTO OpportunityKitChildItems(numOppId,numOppItemID,numOppChildItemID,numItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost)
         SELECT
         v_numNewOppID,OI.numoppitemtCode,OKI.numOppChildItemID,OKCI.numItemID,OKCI.numWareHouseItemId,OKCI.numQtyItemsReq,OKCI.numQtyItemsReq,OKCI.numUOMId,0,coalesce(I.monAverageCost,0)
         FROM
         OpportunityKitChildItems OKCI
         INNER JOIN
         OpportunityItems OI
         ON
         OKCI.numOppItemID = OI.numRecurParentOppItemID
         INNER JOIN
         OpportuntiyKitItems OKI
         ON
         OKI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         Item I
         ON
         OKCI.numItemID = I.numItemCode
         WHERE
         OKCI.numOppId = v_numOppID
         AND OI.numOppId = v_numNewOppID;
      end if;
      select   sum(monTotAmount) INTO v_TotalAmount FROM OpportunityItems WHERE numOppId = v_numNewOppID;
      UPDATE OpportunityMaster SET monPAmount = v_TotalAmount WHERE numOppId = v_numNewOppID;
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
--Updating the warehouse items              
      IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then

         PERFORM USP_UpdatingInventoryonCloseDeal(v_numNewOppID,0,v_numUserCntID);
      end if;
      select   numDivisionId, bintAccountClosingDate INTO v_numDivisionID,v_AccountClosingDate FROM OpportunityMaster WHERE numOppId = v_numNewOppID;
      select   tintCRMType INTO v_tintCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID; 

                 
--Add/Update Address Details
      select   vcCompanyName, div.numCompanyID INTO v_vcCompanyName,v_numCompanyId FROM
      CompanyInfo Com
      JOIN
      DivisionMaster div
      ON
      div.numCompanyID = Com.numCompanyId WHERE
      div.numDivisionID = v_numDivisionID;

--Bill Address
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) > 0 then

         select coalesce(numBillAddressID,0),coalesce(vcBillStreet,''), coalesce(vcBillCity,''), coalesce(vcBillPostCode,''), coalesce(numBillState,0), coalesce(numBillCountry,0), vcAddressName, numBillingContact, bitAltBillingContact, vcAltBillingContact 
		 INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_vcAddressName,
         v_numContact,v_bitAltContact,v_vcAltContact FROM
         OpportunityAddress WHERE
         numOppID = v_numOppID;
         PERFORM USP_UpdateOppAddress(v_numOppID := v_numNewOppID,v_byteMode := 0::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
         v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
         v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
         v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
         v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
         v_vcAltContact := v_vcAltContact);
      end if;
  
--Ship Address
      IF(SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = v_numOppID) > 0 then

         select coalesce(numShipAddressID,0),coalesce(vcShipStreet,''), coalesce(vcShipCity,''), coalesce(vcShipPostCode,''), coalesce(numShipState,0), coalesce(numShipCountry,0), vcAddressName, numShippingContact, bitAltShippingContact, vcAltShippingContact 
		 INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_vcAddressName,
         v_numContact,v_bitAltContact,v_vcAltContact FROM
         OpportunityAddress WHERE
         numOppID = v_numOppID;
         PERFORM USP_UpdateOppAddress(v_numOppID := v_numNewOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
         v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
         v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
         v_numCompanyId := v_numCompanyId,v_vcAddressName := v_vcAddressName,
         v_bitCalledFromProcedure := true,v_numContact := v_numContact,
         v_bitAltContact := v_bitAltContact,v_vcAltContact := v_vcAltContact);
      end if;


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
      IF v_numOppType = 1 OR (v_numOppType = 2 and(select coalesce(bitPurchaseTaxCredit,0) from Domain where numDomainId = v_numDomainID) = 1) then

         INSERT INTO OpportunityMasterTaxItems(numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID)
         SELECT
         v_numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
         FROM
         TaxItems TI
         JOIN
         DivisionTaxTypes DTT
         ON
         TI.numTaxItemID = DTT.numTaxItemID
         CROSS JOIN LATERAL(SELECT
            decTaxValue,
			tintTaxType
            FROM
            fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,TI.numTaxItemID,v_numNewOppID,0,NULL)) AS TEMPTax
         WHERE
         DTT.numDivisionID = v_numDivisionID
         AND DTT.bitApplicable = true
         UNION
         SELECT
         v_numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
         FROM
         DivisionMaster
         CROSS JOIN LATERAL(SELECT
            decTaxValue,
			tintTaxType
            FROM
            fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,0,v_numNewOppID,1,NULL)) AS TEMPTax
         WHERE
         bitNoTax = false
         AND numDivisionID = v_numDivisionID
         UNION
         SELECT
         v_numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
         FROM
         fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,1,v_numNewOppID,1,NULL);
      end if;

--Delete Tax for Opportunity Items if item deleted 
      DELETE FROM OpportunityItemsTaxItems WHERE numOppId = v_numNewOppID AND numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numNewOppID);

--Insert Tax for Opportunity Items
      INSERT INTO OpportunityItemsTaxItems(numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID)
      SELECT
      v_numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
      FROM
      OpportunityItems OI
      JOIN
      ItemTax IT
      ON
      OI.numItemCode = IT.numItemCode
      JOIN
      TaxItems TI
      ON
      TI.numTaxItemID = IT.numTaxItemID
      WHERE
      OI.numOppId = v_numNewOppID AND
      IT.bitApplicable = true AND
      OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numNewOppID)
      UNION
      SELECT
      v_numNewOppID,
	OI.numoppitemtCode,
	0,
	0
      FROM
      OpportunityItems OI
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OI.numOppId = v_numNewOppID AND
      I.bitTaxable = true AND
      OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numNewOppID)
      UNION
      SELECT
      v_numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
      FROM
      OpportunityItems OI
      INNER JOIN
      ItemTax IT
      ON
      IT.numItemCode = OI.numItemCode
      INNER JOIN
      TaxDetails TD
      ON
      TD.numTaxID = IT.numTaxID
      AND TD.numDomainId = v_numDomainID
      WHERE
      OI.numOppId = v_numNewOppID
      AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numNewOppID);
      UPDATE OpportunityMaster SET monDealAmount = GetDealAmount(v_numNewOppID,v_Date::TIMESTAMP,0::NUMERIC) WHERE numOppId = v_numNewOppID;
      IF v_numNewOppID > 0 then

	-- Insert newly created opportunity to transaction
         INSERT INTO RecurrenceTransaction(numRecConfigID,numRecurrOppID,dtCreatedDate) VALUES(v_numRecConfigID,v_numNewOppID,LOCALTIMESTAMP);

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	
         INSERT INTO RecurrenceTransactionHistory(numRecConfigID,numRecurrOppID,dtCreatedDate) VALUES(v_numRecConfigID,v_numNewOppID,LOCALTIMESTAMP);

	--Get next recurrence date for order 
	
         v_dtNextRecurrenceDate :=(CASE v_numFrequency
         WHEN 1 THEN v_Date+INTERVAL '1 day' --Daily
         WHEN 2 THEN v_Date+INTERVAL '7 day' --Weekly
         WHEN 3 THEN v_Date+INTERVAL '1 month' --Monthly
         WHEN 4 THEN v_Date+INTERVAL '4 month'  --Quarterly
         END);
	
	--Increase value of number of transaction completed by 1
         UPDATE RecurrenceConfiguration SET numTransaction = coalesce(numTransaction,0)+1 WHERE numRecConfigID = v_numRecConfigID;
         RAISE NOTICE '%',v_dtNextRecurrenceDate;
         RAISE NOTICE '%',v_dtEndDate;

	-- Set recurrence status as completed if next recurrence date is greater than end date
         If v_dtNextRecurrenceDate > v_dtEndDate then
	
            UPDATE RecurrenceConfiguration SET bitCompleted = true WHERE numRecConfigID = v_numRecConfigID;
         ELSE
            UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = v_dtNextRecurrenceDate WHERE numRecConfigID = v_numRecConfigID;
         end if;
      end if;
      UPDATE OpportunityMaster SET bitRecurred = true WHERE numOppId = v_numNewOppID;
      v_numRecurOppID := v_numNewOppID;
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END;

   -- COMMIT
RETURN;
END; $$;  
--modified by anoop jayaraj              


