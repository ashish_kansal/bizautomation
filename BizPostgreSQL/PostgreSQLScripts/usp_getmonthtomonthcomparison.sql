-- Stored procedure definition script usp_GetMonthToMonthComparison for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetMonthToMonthComparison(v_numDomainID NUMERIC,        
  v_numUserCntID NUMERIC,          
        v_FirstStartDate TIMESTAMP,          
        v_FirstEndDate TIMESTAMP,          
        v_SecondStartDate TIMESTAMP,        
        v_SecondEndDate TIMESTAMP,        
  v_numCompareFirst NUMERIC,        
  v_numCompareSecond NUMERIC,        
  v_tintTypeCompare SMALLINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintTypeCompare = 0 then

      open SWV_RefCur for
      SELECT coalesce(SUM(OM.monPAmount),0) as Amount
      FROM
      OpportunityMaster OM
      join AdditionalContactsInformation A
      on OM.numrecowner = A.numContactId
      where A.numTeam = v_numCompareFirst
      and (OM.bintAccountClosingDate >= v_FirstStartDate AND OM.bintAccountClosingDate <= v_SecondEndDate);

      open SWV_RefCur2 for
      SELECT coalesce(SUM(OM.monPAmount),0) as Amount
      FROM
      OpportunityMaster OM
      join AdditionalContactsInformation A
      on OM.numrecowner = A.numContactId
      where numTeam = v_numCompareSecond
      and (OM.bintAccountClosingDate >= v_FirstStartDate AND OM.bintAccountClosingDate <= v_SecondEndDate);
   end if;        
   if v_tintTypeCompare = 1 then

      open SWV_RefCur for
      SELECT coalesce(SUM(OM.monPAmount),0) as Amount
      FROM
      OpportunityMaster OM
      join DivisionMaster D
      on D.numDivisionID = OM.numDivisionId
      where numTerID = v_numCompareFirst
      and (OM.bintAccountClosingDate >= v_FirstStartDate AND OM.bintAccountClosingDate <= v_SecondEndDate);

      open SWV_RefCur2 for
      SELECT coalesce(SUM(OM.monPAmount),0) as Amount
      FROM
      OpportunityMaster OM
      join DivisionMaster D
      on D.numDivisionID = OM.numDivisionId
      where numTerID = v_numCompareSecond
      and (OM.bintAccountClosingDate >= v_FirstStartDate AND OM.bintAccountClosingDate <= v_SecondEndDate);
   end if;
   RETURN;
END; $$;


