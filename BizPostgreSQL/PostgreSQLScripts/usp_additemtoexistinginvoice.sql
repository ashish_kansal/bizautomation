-- Stored procedure definition script USP_AddItemToExistingInvoice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddItemToExistingInvoice(v_numOppID NUMERIC(9,0),
    v_numOppBizDocID NUMERIC(9,0),
    v_numItemCode NUMERIC(9,0),
    v_numUnitHour DOUBLE PRECISION,
    v_monPrice DECIMAL(20,5),
    v_vcItemDesc VARCHAR(1000),
--    @vcNotes VARCHAR(500),
    v_numCategoryHDRID NUMERIC(9,0),
    v_numProId NUMERIC(9,0),
	v_numStageId NUMERIC(9,0),
	v_numClassID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppItemID  NUMERIC(9,0);

   v_vcItemName  VARCHAR(300);
   v_vcModelID  VARCHAR(200);
   v_vcManufacturer  VARCHAR(250);
   v_vcPathForTImage  VARCHAR(300);
BEGIN
   select   vcItemName, vcModelID, vcManufacturer, (SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = numItemCode AND II.numDomainId = numDomainId AND II.bitDefault = true LIMIT 1) INTO v_vcItemName,v_vcModelID,v_vcManufacturer,v_vcPathForTImage FROM    Item WHERE   numItemCode = v_numItemCode;
			
   INSERT  INTO OpportunityItems(numOppId,
                                              numItemCode,
                                              numUnitHour,
                                              monPrice,
                                              monTotAmount,
                                              numSourceID,
                                              vcItemDesc,
                                              numWarehouseItmsID,
                                              vcType,
                                              vcAttributes,
                                              bitDropShip,
                                              numUnitHourReceived,
                                              bitDiscountType,
                                              fltDiscount,
                                              monTotAmtBefDiscount,
                                              vcItemName,
                                              vcModelID,
                                              vcManufacturer,
                                              vcPathForTImage,numProjectID,numClassID,numProjectStageID)
            VALUES(v_numOppID,
                      v_numItemCode,
                      v_numUnitHour,
                      v_monPrice,
                      (v_monPrice*v_numUnitHour),
                      NULL,
                      v_vcItemDesc,
                      NULL,
                      'Service',
                      NULL,
                      NULL,
                      NULL,
                      false,
                      0,
                      (v_monPrice*v_numUnitHour),
                      v_vcItemName,
                      v_vcModelID,
                      v_vcManufacturer,
                      v_vcPathForTImage,v_numProId,v_numClassID,v_numStageId);
           
   v_numOppItemID := CURRVAL('OpportunityItems_seq');

            
   IF v_numCategoryHDRID > 0 then
            
      UPDATE timeandexpense SET numOppItemID = v_numOppItemID,numOppId = v_numOppID,numStageId = v_numStageId WHERE numCategoryHDRID = v_numCategoryHDRID;
   end if;
   RETURN;
END; $$;

--created by anoop Jayaraj


