-- Stored procedure definition script usp_GetContactIDForCase for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactIDForCase(v_numDivisionID NUMERIC DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
   AdditionalContactsInformation.numContactId
   FROM         OpportunityMaster INNER JOIN
   AdditionalContactsInformation ON OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
   WHERE     OpportunityMaster.numDivisionId = v_numDivisionID;
END; $$;












