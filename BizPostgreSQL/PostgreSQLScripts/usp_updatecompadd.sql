-- Stored procedure definition script USP_UpdateCompAdd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCompAdd(v_vcBillStreet VARCHAR(100),            
v_vcBillCity VARCHAR(50),            
v_vcBilState NUMERIC(9,0),            
v_vcBillPostCode VARCHAR(15),            
v_vcBillCountry NUMERIC(9,0),            
v_bitSameAddr SMALLINT,            
v_vcShipStreet VARCHAR(100),            
v_vcShipCity VARCHAR(50),            
v_vcShipState NUMERIC(9,0),            
v_vcShipPostCode VARCHAR(15),         
v_vcShipCountry NUMERIC(9,0),            
v_numDivisionID NUMERIC(9,0),  
v_numContactId NUMERIC(9,0),  
v_vcFirstname VARCHAR(50),  
v_vcLastName VARCHAR(50),  
v_vcEmail VARCHAR(50),  
v_vcPhone VARCHAR(15))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitAutoPopulateAddress  BOOLEAN; 
   v_tintPoulateAddressTo  SMALLINT;
   v_PContact  NUMERIC;
   v_vcPStreet  VARCHAR(100);                      
   v_vcPCity  VARCHAR(50);
   v_vcPPostalCode  VARCHAR(15);
   v_vcPState  NUMERIC(9,0);
   v_vcPCountry  NUMERIC(9,0);
   v_PrimaryAddress  VARCHAR(100);
BEGIN
   update AddressDetails
   set
   vcStreet = v_vcBillStreet,vcCity = v_vcBillCity,numState = v_vcBilState,vcPostalCode = v_vcBillPostCode,
   numCountry = v_vcBillCountry
   where  numRecordID = v_numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true;
   update AddressDetails
   set
   vcStreet = v_vcShipStreet,vcCity = v_vcShipCity,numState = v_vcShipState,vcPostalCode = v_vcShipPostCode,
   numCountry = v_vcShipCountry
   where  numRecordID = v_numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true;
   if  v_numContactId > 0 then

      update AdditionalContactsInformation
      set vcFirstName = v_vcFirstname,vcLastname = v_vcLastName,numPhone = v_vcPhone,vcEmail = v_vcEmail
      where numContactId = v_numContactId;
   end if;


 
-- Auto Populate Primary Address if address not specified
   select   coalesce(bitAutoPopulateAddress,false), coalesce(tintPoulateAddressTo,'0') INTO v_bitAutoPopulateAddress,v_tintPoulateAddressTo from Domain where numDomainId IN(SELECT numDomainID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID);
   if (v_bitAutoPopulateAddress = true) then
      select   numContactId INTO v_PContact FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND coalesce(bitPrimaryContact,false) = true;
      if(v_tintPoulateAddressTo = 1) then --if primary address is not specified then getting billing address    
	
         select   AD1.vcStreet, AD1.vcCity, AD1.numState, AD1.vcPostalCode, AD1.numCountry INTO v_vcPStreet,v_vcPCity,v_vcPState,v_vcPPostalCode,v_vcPCountry from AddressDetails AD1 where  numRecordID = v_numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true;
      ELSEIF (v_tintPoulateAddressTo = 2)
      then-- Primary Address is Shipping Address
	
         select   AD2.vcStreet, AD2.vcCity, AD2.numState, AD2.vcPostalCode, AD2.numCountry INTO v_vcPStreet,v_vcPCity,v_vcPState,v_vcPPostalCode,v_vcPCountry from AddressDetails AD2 where  numRecordID = v_numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true;
      end if;  
	
-- ONLY UPDATE WHEN address lenght is 0 ,
      select   coalesce(vcStreet,'') || coalesce(vcCity,'') INTO v_PrimaryAddress FROM AddressDetails WHERE numRecordID = v_PContact AND bitIsPrimary = true AND tintAddressOf = 1 AND tintAddressType = 0;
      IF LENGTH(v_PrimaryAddress) = 0 then

         UPDATE AddressDetails
         SET vcStreet = v_vcPStreet,vcCity = v_vcPCity,numCountry = v_vcPCountry,vcPostalCode = v_vcPPostalCode,
         numState = v_vcPState
         WHERE  numRecordID = v_PContact	AND tintAddressOf = 1 AND tintAddressType = 0 AND bitIsPrimary = true;
      end if;
   end if;
   RETURN;
END; $$;


