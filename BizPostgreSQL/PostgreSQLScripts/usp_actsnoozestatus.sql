-- Stored procedure definition script USP_ActsnoozeStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ActsnoozeStatus(v_numCommID NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Communication set intSnoozeMins = 0,intRemainderMins = 0,tintSnoozeStatus = 0,tintRemStatus = 0
   where numCommId = v_numCommID;
   RETURN;
END; $$;


