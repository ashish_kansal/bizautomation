DROP FUNCTION IF EXISTS uspp_ExecteDynamicQuery;
CREATE OR REPLACE FUNCTION uspp_ExecteDynamicQuery(v_Sql TEXT  ,  
v_numUserCntId NUMERIC(18,0) DEFAULT NULL,  
v_numDomainId  NUMERIC(18,0) DEFAULT NULL,  
 v_ClientTimeZoneOffset NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sql1  TEXT;
BEGIN
	v_Sql := regexp_replace(v_Sql,'@numdomainid',v_numDomainID::VARCHAR,'gi');
	v_Sql := regexp_replace(v_Sql,'@numUserCntId',v_numUserCntId::VARCHAR,'gi');
	v_Sql := regexp_replace(v_Sql,'@ClientTimeZoneOffset',v_ClientTimeZoneOffset::VARCHAR,'gi');   
	v_Sql := regexp_replace(v_Sql,'##Items##','"##Items##"','gi');  
	v_Sql := regexp_replace(v_Sql,'PERFORM','SELECT','gi'); 

   RAISE NOTICE '%',(v_Sql);  
   execute v_Sql;
   RETURN;
END; $$;


