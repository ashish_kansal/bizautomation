-- Stored procedure definition script USP_WorkOrder_IsCompleted for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkOrder_IsCompleted(v_numDomainID NUMERIC(18,0)                            
	,v_numWOID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID AND numWOId = 23184) then
	
      open SWV_RefCur for
      SELECT 1;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


