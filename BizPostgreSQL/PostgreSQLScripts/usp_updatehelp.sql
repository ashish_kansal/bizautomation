-- Stored procedure definition script USP_UpdateHelp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateHelp(v_numHelpId NUMERIC(9,0),
    v_helpcategory NUMERIC(9,0),
    v_helpMetatag VARCHAR(400),
    v_helpheader VARCHAR(400),
    v_helpDescription TEXT,
    v_helpshortdesc VARCHAR(400),
    v_helpLinkingPageURL VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  HelpMaster
   SET     helpHeader = v_helpheader,helpCategory = v_helpcategory,helpMetatag = v_helpMetatag,
   helpDescription = v_helpDescription,helpshortdesc = v_helpshortdesc,
   helpLinkingPageURL = v_helpLinkingPageURL
   WHERE   numHelpID = v_numHelpId;
   RETURN;
END; $$;   
---Created by anoop Jayaraj



