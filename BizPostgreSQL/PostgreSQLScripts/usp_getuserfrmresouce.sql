-- Stored procedure definition script USP_GetUserFrmResouce for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUserFrmResouce(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select ResourceName as "ResourceName",ResourceID as "ResourceID" from Resource where numDomainId = v_numDomainId;
END; $$;












