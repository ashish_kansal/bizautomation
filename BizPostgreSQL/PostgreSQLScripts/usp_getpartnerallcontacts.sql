-- Stored procedure definition script USP_GetPartnerAllContacts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPartnerAllContacts(v_numDomainID  NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT A.numContactId,A.vcFirstName || ' ' || A.vcLastname AS vcGivenName FROM OpportunityMaster AS O
   LEFT JOIN AdditionalContactsInformation AS A
   ON O.numPartenerContact = A.numContactId
   WHERE A.numDomainID = v_numDomainID AND coalesce(O.numPartenerContact,0) > 0 AND A.vcGivenName <> '';
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_GetPartnerContacts]    Script Date: 05/07/2009 22:09:30 ******/













