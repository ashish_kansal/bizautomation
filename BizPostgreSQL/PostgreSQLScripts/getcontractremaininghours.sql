-- Function definition script GetContractRemainingHours for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetContractRemainingHours(v_numContractID NUMERIC,v_numDivisionId NUMERIC,v_numDomainId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue1  DECIMAL(10,2);
BEGIN
   select   coalesce(sum(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))/60),0) INTO v_vcRetValue1 from timeandexpense where numtype = 1 and numCategory = 1
   and numcontractId = v_numContractID
   and  numDomainID = v_numDomainId;      
          
  
   RETURN coalesce(v_vcRetValue1,0);
END; $$;

