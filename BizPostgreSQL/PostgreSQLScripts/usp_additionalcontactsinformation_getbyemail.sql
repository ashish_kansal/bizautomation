CREATE OR REPLACE FUNCTION USP_AdditionalContactsInformation_GetByEmail(v_numDomainID NUMERIC(18,0)
	,v_numContactID NUMERIC(18,0)
	,v_vcEmail VARCHAR(300)
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(coalesce(v_vcEmail,'')) > 0 AND EXISTS(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND vcEmail = coalesce(v_vcEmail,'') AND numContactId <> coalesce(v_numContactID,0)) then
	
      open SWV_RefCur for SELECT
      AdditionalContactsInformation.numContactId
			,AdditionalContactsInformation.numDivisionId
			,cast(CompanyInfo.vcCompanyName as VARCHAR(255))
			,cast(CONCAT(coalesce(vcFirstName,'-'),' ',coalesce(vcLastname,'-')) as VARCHAR(255)) AS vcContact
      FROM
      AdditionalContactsInformation
      INNER JOIN
      DivisionMaster
      ON
      AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      AdditionalContactsInformation.numDomainID = v_numDomainID
      AND LOWER(vcEmail) = LOWER(coalesce(v_vcEmail,''))
      AND AdditionalContactsInformation.numContactId <> coalesce(v_numContactID,0);
   end if;
END; $$;






