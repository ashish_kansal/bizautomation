-- Stored procedure definition script USP_ElasticSearchBizCart_DisableDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


CREATE OR REPLACE FUNCTION USP_ElasticSearchBizCart_DisableDomain(v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE eCommerceDTL
   SET dtElasticSearchDisableDatetime = NULL
   WHERE numDomainID = v_numDomainID;
   RETURN;
END; $$;


