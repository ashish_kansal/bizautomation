-- Stored procedure definition script USP_DeleteTimExpLeave for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteTimExpLeave(v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0,      
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCategory  NUMERIC(9,0);      
   v_numType  NUMERIC(9,0);      
   v_numJournalId  NUMERIC(9,0);      
   v_numProId  NUMERIC(9,0);
   v_numStageID  NUMERIC(9,0);
   v_numOppItemID  NUMERIC(9,0);
BEGIN
   select   numCategory, numtype, coalesce(numOppItemID,0), coalesce(numProId,0), coalesce(numStageId,0) INTO v_numCategory,v_numType,v_numOppItemID,v_numProId,v_numStageID From  timeandexpense where numCategoryHDRID = v_numCategoryHDRID;
--Delete Journal entries 
   If  v_numCategory = 1 OR v_numCategory = 2  OR v_numCategory = 3 then
   
      select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header WHERE (numProjectID = v_numProId OR v_numProId = 0) AND numCategoryHDRID = v_numCategoryHDRID And numDomainId = v_numDomainId;
      Delete from General_Journal_Details where numJournalId = v_numJournalId and numDomainId = v_numDomainId;
      Delete from General_Journal_Header Where numJOurnal_Id = v_numJournalId and numDomainId = v_numDomainId;
   end if;      

   IF (v_numProId > 0 AND v_numOppItemID > 0) then

--	--Delete bizdoc service item 
--	DELETE FROM dbo.OpportunityBizDocItems WHERE numOppBizDocItemID =@numOppBizDocItemID
--	--delete linking id between project stage and bizdoc 
--	DELETE FROM [ProjectsOpportunities] Where numProID=@numProId AND numStageID = @numStageID and numOppBizDocID = @numOppBizDocsId AND numDomainID = @numDomainId	

	--Update OpportunityItems and Reset Project and StageId
      Update OpportunityItems set numProjectID = null,numProjectStageID = null where numoppitemtCode = v_numOppItemID;
   end if;
--Delete PK
   delete from timeandexpense where numCategoryHDRID = v_numCategoryHDRID   and numDomainID = v_numDomainId;    
--remove reference of time 
   UPDATE General_Journal_Header SET numCategoryHDRID = NULL WHERE numCategoryHDRID = v_numCategoryHDRID;
   RETURN;
END; $$;


