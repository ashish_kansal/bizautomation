-- Stored procedure definition script USP_ReportListMaster_RemindersPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_RemindersPreBuildReport(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitReOrderPoint  BOOLEAN;
   v_numDefaultSalesShippingDoc  NUMERIC(18,0);

   v_TimeAndExpenseToReview  INTEGER;
   v_PriceMarginToReview  INTEGER;
   v_PromotionApproval  INTEGER;
   v_SalesOppStagesToComplete  INTEGER;
   v_PurchaseOppStagesToComplete  INTEGER;
   v_SalesOrderStagesToComplete  INTEGER;
   v_ProjectStagesToComplete  INTEGER;
   v_CasesToSolve  INTEGER;
   v_PendingSalesRMA  INTEGER;
   v_PaymentsToDeposite  INTEGER;
   v_BillsToPay  INTEGER;
   v_PurchaseOrderToBill  INTEGER;
   v_SalesOrderToPickAndPack  INTEGER;
   v_SalesOrderToShip  INTEGER;
   v_SalesOrderToInvoice  INTEGER;
   v_SalesOrderWithItemsToPurchase  INTEGER;
   v_SalesOrderPastTheirReleaseDate  INTEGER;
   v_ItemOnHandLessThenReorder  INTEGER;
BEGIN
   select   coalesce(bitReOrderPoint,false), coalesce(numDefaultSalesShippingDoc,0) INTO v_bitReOrderPoint,v_numDefaultSalesShippingDoc FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER,
      Value INTEGER
   );

	---------------------------- 1 Billable time & expense entries to review -----------------------------------

   select   COUNT(numCategoryHDRID) INTO v_TimeAndExpenseToReview FROM
   timeandexpense TE WHERE
   TE.numDomainID = v_numDomainID
   AND TE.numApprovalComplete NOT IN(0);

	---------------------------- 2 Minimum price margin violations to review -----------------------------------

   select   COUNT(M.numOppId) INTO v_PriceMarginToReview FROM
   OpportunityMaster AS M WHERE
   M.numDomainId = v_numDomainID
   AND(SELECT
      COUNT(OpportunityItems.numoppitemtCode)
      FROM
      OpportunityItems
      WHERE
      OpportunityItems.bitItemPriceApprovalRequired = true
      AND OpportunityItems.numOppId = M.numOppId) > 0;

	---------------------------- 3 Promotions in sales orders to approve -----------------------------------

   select   COUNT(numOppId) INTO v_PromotionApproval FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND coalesce(intPromotionApprovalStatus,0) NOT IN(0,-1); 

	---------------------------- 4 Sales OpportunityOrder Stages to complete -----------------------------------

   select   COUNT(*) INTO v_SalesOppStagesToComplete FROM
   StagePercentageDetails SPD
   INNER JOIN
   OpportunityMaster OM
   ON
   SPD.numOppid = OM.numOppId WHERE
   SPD.numdomainid = v_numDomainID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND coalesce(OM.tintoppstatus,0) = 0
   AND tinProgressPercentage <> 100;

	---------------------------- 18 Purchase Opportunities Stages to complete -----------------------------------

   select   COUNT(*) INTO v_PurchaseOppStagesToComplete FROM
   StagePercentageDetails SPD
   INNER JOIN
   OpportunityMaster OM
   ON
   SPD.numOppid = OM.numOppId WHERE
   SPD.numdomainid = v_numDomainID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 2
   AND coalesce(OM.tintoppstatus,0) = 0
   AND tinProgressPercentage <> 100;

	---------------------------- 19 Sales Order Stages to complete -----------------------------------

   select   COUNT(*) INTO v_SalesOrderStagesToComplete FROM
   StagePercentageDetails SPD
   INNER JOIN
   OpportunityMaster OM
   ON
   SPD.numOppid = OM.numOppId
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   OM.numContactId = ACI.numContactId WHERE
   SPD.numdomainid = v_numDomainID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND coalesce(OM.tintoppstatus,0) = 1
   AND tinProgressPercentage <> 100;

	---------------------------- 5 Project Stages to complete -----------------------------------

   select   COUNT(*) INTO v_ProjectStagesToComplete FROM
   StagePercentageDetails SPD
   INNER JOIN
   ProjectsMaster
   ON
   SPD.numProjectid = ProjectsMaster.numProId WHERE
   SPD.numdomainid = v_numDomainID
   AND ProjectsMaster.numdomainId = v_numDomainID
   AND tinProgressPercentage <> 100;

	---------------------------- 6 Cases to solve -----------------------------------

   select   COUNT(*) INTO v_CasesToSolve FROM
   Cases
   INNER JOIN
   AdditionalContactsInformation
   ON
   Cases.numContactId = AdditionalContactsInformation.numContactId WHERE
   Cases.numDomainID = v_numDomainID
   AND numStatus <> 136;
	
	---------------------------- 7 Pending Sales RMAs -----------------------------------

   select   COUNT(*) INTO v_PendingSalesRMA FROM
   ReturnHeader WHERE
   numDomainID = v_numDomainID
   AND tintReturnType = 1
   AND numReturnStatus = 301;

	---------------------------- 8 Payments to deposit -----------------------------------

   select   COUNT(numDepositId) INTO v_PaymentsToDeposite FROM
   DepositMaster DM WHERE
   DM.numDomainId = v_numDomainID
   AND tintDepositeToType = 2
   AND coalesce(bitDepositedToAcnt,false) = false; 

	---------------------------- 9 Bills to pay -----------------------------------

   select   COUNT(*) INTO v_BillsToPay FROM(SELECT
      OBD.numOppBizDocsId
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityMaster Opp
      ON
      OBD.numoppid = Opp.numOppId
      INNER JOIN
      AdditionalContactsInformation ADC
      ON
      Opp.numContactId = ADC.numContactId
      INNER JOIN
      DivisionMaster Div
      ON
      Opp.numDivisionId = Div.numDivisionID
      AND ADC.numDivisionId = Div.numDivisionID
      INNER JOIN
      CompanyInfo C
      ON
      Div.numCompanyID = C.numCompanyId
      WHERE
      Opp.numDomainId = v_numDomainID
      AND OBD.bitAuthoritativeBizDocs = 1
      AND Opp.tintopptype = 2
      AND(OBD.monDealAmount -OBD.monAmountPaid) > 0
      UNION       
			--Regular Bills For Add Bill Payment
      SELECT  BH.numBillID FROM BillHeader BH	WHERE BH.numDomainId = v_numDomainID AND BH.bitIsPaid = false) TEMP;

	---------------------------- 14 Sales Orders with items to purchase -----------------------------------

   DROP TABLE IF EXISTS tt_TEMPOPPID CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPID
   (
      numOppID NUMERIC(18,0)
   );

   INSERT INTO tt_TEMPOPPID(numOppID)
   SELECT DISTINCT
   OM.numOppId
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   LEFT JOIN
   OpportunityLinking OMInner
   ON
   OMInner.numParentOppID = OM.numOppId
   LEFT JOIN
   OpportunityItems OIInner
   ON
   OMInner.numChildOppID = OIInner.numOppId
   AND OIInner.numItemCode = OI.numItemCode
   AND coalesce(OIInner.numWarehouseItmsID,0) = coalesce(OI.numWarehouseItmsID,0)
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OI.bitDropShip,false) = true
   AND OIInner.numoppitemtCode IS NULL;


   If coalesce(v_bitReOrderPoint,false) = true then
	
      INSERT INTO tt_TEMPOPPID(numOppID)
      SELECT DISTINCT
      OM.numOppId
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      I.numItemCode = OI.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(v_bitReOrderPoint,false) = true
      AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
      AND OM.tintopptype = 1
      AND OM.tintoppstatus = 1
      AND coalesce(I.bitAssembly,false) = false
      AND coalesce(I.bitKitParent,false) = false
      AND coalesce(OI.bitDropShip,false) = false
      AND OM.numOppId NOT IN(SELECT numOppID FROM tt_TEMPOPPID);
   end if;
	
	---------------------------- 15 Sales Orders past their release date -----------------------------------

   select   COUNT(*) INTO v_SalesOrderPastTheirReleaseDate FROM
   OpportunityMaster OM WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND dtReleaseDate IS NOT NULL
   AND dtReleaseDate < TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
   AND(SELECT
      COUNT(*)
      FROM(SELECT
         OI.numoppitemtCode,
					coalesce(OI.numUnitHour,0) AS OrderedQty,
					coalesce(TempFulFilled.FulFilledQty,0) AS FulFilledQty
         FROM
         OpportunityItems OI
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         LEFT JOIN LATERAL(SELECT
            SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = OM.numOppId
            AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
            AND coalesce(OpportunityBizDocs.bitFulFilled,false) = true
            AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode) AS TempFulFilled on TRUE
         WHERE
         OI.numOppId = OM.numOppID
         AND UPPER(I.charItemType) = 'P'
         AND coalesce(OI.bitDropShip,false) = false) X
      WHERE
      X.OrderedQty <> X.FulFilledQty) > 0;

	---------------------------- 17 Items to buy where qty-on-hand is =< the reorder qty -----------------------------------
	 
   select   COUNT(numItemID) INTO v_ItemOnHandLessThenReorder FROM(SELECT DISTINCT
      numItemID
      FROM
      WareHouseItems
      INNER JOIN
      Item
      ON
      WareHouseItems.numItemID = Item.numItemCode
      WHERE
      WareHouseItems.numDomainID = v_numDomainID
      AND Item.numDomainID = v_numDomainID
      AND coalesce(Item.bitArchiveItem,false) = false
      AND coalesce(Item.IsArchieve,false) = false
      AND coalesce(numReorder,0) > 0
      AND coalesce(numOnHand,0) < coalesce(numReorder,0)) TEMP;


   INSERT INTO tt_TEMP(ID,Value) VALUES(1,coalesce(v_TimeAndExpenseToReview, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(2,coalesce(v_PriceMarginToReview, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(3,coalesce(v_PromotionApproval, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(4,coalesce(v_SalesOppStagesToComplete, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(5,coalesce(v_ProjectStagesToComplete, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(6,coalesce(v_CasesToSolve, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(7,coalesce(v_PendingSalesRMA, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(8,coalesce(v_PaymentsToDeposite, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(9,coalesce(v_BillsToPay, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(10,coalesce(v_PurchaseOrderToBill, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(11,coalesce(v_SalesOrderToPickAndPack, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(12,coalesce(v_SalesOrderToShip, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(13,coalesce(v_SalesOrderToInvoice, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(14,coalesce((SELECT COUNT(*) FROM tt_TEMPOPPID), 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(15,coalesce(v_SalesOrderPastTheirReleaseDate, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(16,0);
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(17,coalesce(v_ItemOnHandLessThenReorder, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(18,coalesce(v_PurchaseOppStagesToComplete, 0));
	
   INSERT INTO tt_TEMP(ID,Value) VALUES(19,coalesce(v_SalesOrderStagesToComplete, 0));

	
open SWV_RefCur for SELECT * FROM tt_TEMP;
END; $$;












