-- Stored procedure definition script USP_UpdateFollowUpStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateFollowUpStatus(v_numContactID NUMERIC,

v_numFollowUpID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyId  NUMERIC(9,0);          
   v_numDivisionId  NUMERIC(9,0);  
   v_numDomainID  NUMERIC(9,0);
BEGIN
   select   numDivisionId INTO v_numDivisionId FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
   select   numCompanyID, numDomainID INTO v_numCompanyId,v_numDomainID FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;
		
   UPDATE  DivisionMaster
   SET     numFollowUpStatus = v_numFollowUpID
   WHERE   numDivisionID = v_numDivisionId
   AND numCompanyID = v_numCompanyId;
                
   INSERT INTO FollowUpHistory(numFollowUpstatus,
			numDivisionID,
			bintAddedDate,
			numDomainID) VALUES( 
			/* numFollowUpstatus - numeric(18, 0) */ v_numFollowUpID,
			/* numDivisionID - numeric(18, 0) */ v_numDivisionId,
			/* bintAddedDate - datetime */ TIMEZONE('UTC',now()),
			/* numDomainID - numeric(18, 0) */v_numDomainID);
RETURN;
END; $$;


