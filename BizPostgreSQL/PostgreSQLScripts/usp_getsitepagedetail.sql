DROP FUNCTION IF EXISTS USP_GetSitePageDetail;

CREATE OR REPLACE FUNCTION USP_GetSitePageDetail(v_vcPageURL VARCHAR(1000),
          v_numSiteID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor, INOUT SWV_RefCur3 refcursor, INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPageID  NUMERIC(9,0);
   v_numTemplateID  NUMERIC(9,0);
   v_numDomainID  NUMERIC(9,0);
   SWV_RCur4 refcursor;
BEGIN
   select   numPageID, numTemplateID, numDomainID INTO v_numPageID,v_numTemplateID,v_numDomainID FROM   SitePages WHERE  LOWER(vcPageURL) = LOWER(v_vcPageURL)
   AND numSiteID = v_numSiteID;
   IF v_numPageID > 0 then
    
      open SWV_RefCur for
      SELECT numPageID,
             coalesce(SP.vcPageName,'') AS vcPageName,
             coalesce(SP.vcPageURL,'') AS vcPageURL,
             tintPageType,
             coalesce(SP.vcPageTitle,'') AS vcPageTitle,
--             [numTemplateID],
--             (SELECT vcTemplateName
--              FROM   [SiteTemplates] st
--              WHERE  st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
             coalesce(MT.vcMetaTag,'') AS vcMetaTag,
             coalesce(MT.vcMetaKeywords,'') AS vcMetaKeywords,
             coalesce(MT.vcMetaDescription,'') AS vcMetaDescription,
             coalesce(MT.numMetaID,0) AS numMetaID,
             numDomainID
      FROM   SitePages SP
      LEFT OUTER JOIN MetaTags MT
      ON MT.numReferenceID = SP.numPageID
      WHERE  (numPageID = v_numPageID
      OR v_numPageID = 0)
      AND numSiteID = v_numSiteID;
      --Select StyleSheets for Page
      open SWV_RefCur2 for
      SELECT S.numCssID,
             S.styleFileName
      FROM   StyleSheetDetails SD
      INNER JOIN StyleSheets S
      ON SD.numCssID = S.numCssID
      WHERE  numPageID = v_numPageID
      AND tintType = 0;
      --Select Javascripts for Page
      open SWV_RefCur3 for
      SELECT S.numCssID,
             S.styleFileName
      FROM   StyleSheetDetails SD
      INNER JOIN StyleSheets S
      ON SD.numCssID = S.numCssID
      WHERE  numPageID = v_numPageID
      AND tintType = 1
      ORDER BY intDisplayOrder; 
	 -- Select Templates
      SELECT * INTO SWV_RefCur4 FROM USP_GetSiteTemplates(v_numTemplateID,v_numSiteID,v_numDomainID,SWV_RCur4);
   end if;
   RETURN;
END; $$;
    




