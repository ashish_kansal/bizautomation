-- Stored procedure definition script USP_GetMutltiCashBank for PostgreSQL
Create or replace FUNCTION USP_GetMutltiCashBank(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
 v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPCASHBANK CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCASHBANK
   (
      numDomainID NUMERIC,
      vcDomainName VARCHAR(200),
      vcDomainCode VARCHAR(50),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Total DECIMAL(20,5)
   );


   INSERT INTO tt_TEMPCASHBANK
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_CASHBANKDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_CASHBANKDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_CASHBANKDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_CASHBANKDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode;



open SWV_RefCur for SELECT vcDomainName ,cast(coalesce(SUM(coalesce(TOTAL,0)),0) as DECIMAL(20,5)) as Total  FROM tt_TEMPCASHBANK GROUP BY vcDomainName;


   RETURN;
END; $$;













