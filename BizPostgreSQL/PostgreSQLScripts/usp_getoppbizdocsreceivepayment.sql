DROP FUNCTION IF EXISTS USP_GetOppBizDocsReceivePayment;

CREATE OR REPLACE FUNCTION USP_GetOppBizDocsReceivePayment(v_numDomainID NUMERIC(18,0) ,
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocsID NUMERIC(18,0),
	v_numBillID NUMERIC(18,0),
	v_numReturnHeaderID NUMERIC(18,0),
    v_tintClientTimeZoneOffset INTEGER,
	v_numLandedCostOppId NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
   v_tintReturnType  SMALLINT;
   v_tintReceiveType  SMALLINT;
BEGIN
   IF v_numBillID > 0 then
   
      open SWV_RefCur for
      SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(BPD.dtAppliedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainID) AS dtPaymentDate
   			,CASE WHEN coalesce(numReturnHeaderID,0) > 0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END AS vcReference
			,SUBSTR(TO_CHAR(BPH.dtPaymentDate,'hh:mi AM'),
      1,15) AS dtPaymentTime
			,'-' AS vcPaymentReceiver
      FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
      WHERE BH.numBillID = v_numBillID ORDER BY BPD.dtAppliedDate;

      open SWV_RefCur2 for
      SELECT coalesce(monAmountDue,0) AS monAmountTotal,coalesce(monAmtPaid,0) AS monAmtPaid FROM BillHeader
      WHERE numBillID = v_numBillID;
   ELSEIF v_numReturnHeaderID > 0
   then
      select   tintReturnType, coalesce(tintReceiveType,0) INTO v_tintReturnType,v_tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = v_numReturnHeaderID;
      IF (v_tintReturnType = 1 AND v_tintReceiveType = 2) OR v_tintReturnType = 3 then
		
         open SWV_RefCur for
         SELECT 2 AS tintRefType,DM.numDepositId AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtPaymentDate
   			,OM.vcpOppName AS vcReference
			,SUBSTR(TO_CHAR(DD.dtCreatedDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
			,coalesce(fn_GetContactName(DM.numCreatedBy),'-') AS vcPaymentReceiver
         FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
         JOIN OpportunityMaster OM ON DD.numOppID = OM.numOppId
         WHERE DM.numReturnHeaderID = v_numReturnHeaderID ORDER BY DD.dtCreatedDate;

         open SWV_RefCur2 for
         SELECT 2 AS tintRefType,coalesce(monDepositAmount,0) AS monAmountTotal,coalesce(monAppliedAmount,0) AS monAmtPaid FROM DepositMaster
         WHERE numReturnHeaderID = v_numReturnHeaderID;
      ELSEIF v_tintReturnType = 2 AND v_tintReceiveType = 2
      then
		
         open SWV_RefCur for
         SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(BPD.dtAppliedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainID) AS dtPaymentDate
			,SUBSTR(TO_CHAR(BPH.dtPaymentDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
   			,CASE WHEN(BPD.numBillID > 0) THEN  'Bill-' || CAST(BH.numBillID AS VARCHAR(10)) ELSE OM.vcpOppName END AS vcReference
			,'-' AS vcPaymentReceiver
         FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
         LEFT JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
         LEFT JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
         LEFT JOIN BillHeader BH ON BH.numBillID = BPD.numBillID
         WHERE BPH.numReturnHeaderID = v_numReturnHeaderID	ORDER BY dtAppliedDate;

         open SWV_RefCur2 for
         SELECT 1 AS tintRefType,coalesce(monPaymentAmount,0) AS monAmountTotal,coalesce(monAppliedAmount,0) AS monAmtPaid FROM BillPaymentHeader
         WHERE numReturnHeaderID = v_numReturnHeaderID;
      end if;
   ELSEIF v_numOppID > 0 AND v_numOppBizDocsID > 0
   then
   
      select   tintopptype INTO v_tintOppType FROM OpportunityMaster WHERE numOppId = v_numOppID;
      IF v_tintOppType = 1 then
		
         open SWV_RefCur for
         SELECT 2 AS tintRefType,DM.numDepositId AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtPaymentDate
				,SUBSTR(to_char(DD.dtCreatedDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
   			,CASE WHEN tintDepositePage = 3 THEN 'Credit Applied' ELSE 'Payment Received' END AS vcReference
			,coalesce(fn_GetContactName(DM.numCreatedBy),'-') AS vcPaymentReceiver
			,DD.numReceivedFrom
         FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
         WHERE DD.numOppID = v_numOppID AND DD.numOppBizDocsID = v_numOppBizDocsID ORDER BY DD.dtCreatedDate;
      ELSE
         open SWV_RefCur for
         SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(BPD.dtAppliedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainID) AS dtPaymentDate
			,SUBSTR(to_char(BPH.dtPaymentDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
   			,CASE WHEN coalesce(numReturnHeaderID,0) > 0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END ||
         CASE WHEN coalesce(CH.numCheckNo,0) > 0 THEN ' (Check # : ' || CAST(CH.numCheckNo AS VARCHAR(18)) || ')'  ELSE '' END AS vcReference
			,'-' AS vcPaymentReceiver
         FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
         LEFT JOIN CheckHeader CH ON BPH.numBillPaymentID = CH.numReferenceID AND CH.tintReferenceType = 8 AND CH.numDomainID = v_numDomainID
         WHERE BPD.numOppBizDocsID = v_numOppBizDocsID ORDER BY BPD.dtAppliedDate;
      end if;

      open SWV_RefCur2 for
      SELECT tintopptype AS tintRefType,coalesce(OBD.monDealAmount,0) AS monAmountTotal,coalesce(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId
      FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
      WHERE OM.numOppId = v_numOppID AND numOppBizDocsId = v_numOppBizDocsID;
   ELSEIF v_numOppID > 0
   then
    
      select   tintopptype INTO v_tintOppType FROM OpportunityMaster WHERE numOppId = v_numOppID;
      IF v_tintOppType = 1 then
		
         open SWV_RefCur for
         SELECT 2 AS tintRefType,DM.numDepositId AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(DD.dtCreatedDate,v_numDomainID) AS dtPaymentDate
				,SUBSTR(TO_CHAR(DD.dtCreatedDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
   			,CASE WHEN tintDepositePage = 3 THEN 'Credit Applied' ELSE 'Payment Received' END AS vcReference
			,coalesce(fn_GetContactName(DM.numCreatedBy),'-') AS vcPaymentReceiver
         FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositId = DD.numDepositID
         WHERE DD.numOppID = v_numOppID ORDER BY DD.dtCreatedDate;
      ELSE
         open SWV_RefCur for
         SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(BPD.dtAppliedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainID) AS dtPaymentDate
			,SUBSTR(TO_CHAR(BPH.dtPaymentDate,'hh:mi AM'),
         1,15) AS dtPaymentTime
			,CASE WHEN coalesce(numReturnHeaderID,0) > 0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END ||
         CASE WHEN coalesce(CH.numCheckNo,0) > 0 THEN ' (Check # : ' || CAST(CH.numCheckNo AS VARCHAR(18)) || ')'  ELSE '' END  AS vcReference
			,'-' AS vcPaymentReceiver
         FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
         LEFT JOIN CheckHeader CH ON BPH.numBillPaymentID = CH.numReferenceID AND CH.tintReferenceType = 8 AND CH.numDomainID = v_numDomainID
         WHERE BPD.numOppBizDocsID IN(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = v_numOppID) ORDER BY BPD.dtAppliedDate;
      end if;

      open SWV_RefCur2 for
      SELECT tintopptype AS tintRefType,coalesce(OBD.monDealAmount,0) AS monAmountTotal,coalesce(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId
      FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
      WHERE OM.numOppId = v_numOppID AND coalesce(OBD.bitAuthoritativeBizDocs,0) = 1;
   ELSEIF v_numLandedCostOppId > 0
   then
	
      open SWV_RefCur for
      SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BH.numDivisionID) AS vcCompanyName,*/
   			FormatedDateFromDate(BPD.dtAppliedDate,v_numDomainID) AS dtAppliedDate
			,FormatedDateFromDate(BPH.dtPaymentDate,v_numDomainID) AS dtPaymentDate
			,SUBSTR(TO_CHAR(BPH.dtPaymentDate,'hh:mi AM'),
      1,15) AS dtPaymentTime
   			,CASE WHEN coalesce(numReturnHeaderID,0) > 0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END AS vcReference
			,'-' AS vcPaymentReceiver
      FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
      WHERE BH.numOppId = v_numLandedCostOppId ORDER BY BPD.dtAppliedDate;

      open SWV_RefCur2 for
      SELECT SUM(coalesce(monAmountDue,0)) AS monAmountTotal,SUM(coalesce(monAmtPaid,0)) AS monAmtPaid FROM BillHeader
      WHERE BillHeader.numOppId = v_numLandedCostOppId;
   end if;
   RETURN;
END; $$;
				
				


