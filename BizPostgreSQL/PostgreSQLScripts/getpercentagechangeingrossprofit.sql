-- Function definition script GetPercentageChangeInGrossProfit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPercentageChangeInGrossProfit(v_numDomainID NUMERIC(18,0),
	 v_dtStartDate DATE,
	 v_dtEndDate DATE,
	 v_tintDateRange SMALLINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)
RETURNS NUMERIC(18,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_GrossProfitChange  NUMERIC(18,2) DEFAULT 0.00;
   v_GrossProfit1  INTEGER;
   v_GrossProfit2  INTEGER;
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_ProfitCost  INTEGER;
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID, numCost INTO v_numShippingItemID,v_numDiscountItemID,v_ProfitCost FROM
   Domain WHERE
   numDomainId = v_numDomainID;


   select   SUM(Profit) INTO v_GrossProfit1 FROM(SELECT
      coalesce(monTotAmount,0) -(coalesce(numUnitHour,0)*(CASE
      WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
      THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
      ELSE(CASE v_ProfitCost
         WHEN 3 THEN coalesce(V.monCost,0)*fn_UOMConversion(numBaseUnit,I.numItemCode,v_numDomainID,numPurchaseUnit)
         ELSE monAverageCost
         END)
      END)) AS Profit
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode 
		 AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 LIMIT 1) TEMPMatchedPO on TRUE
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OI.numItemCode
         AND OIInner.vcAttrValues = OI.vcAttrValues LIMIT 1) TEMPPO  on TRUE
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate BETWEEN  v_dtStartDate AND v_dtEndDate
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;

   select   SUM(Profit) INTO v_GrossProfit2 FROM(SELECT
      coalesce(monTotAmount,0) -(coalesce(numUnitHour,0)*(CASE
      WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
      THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
      ELSE(CASE v_ProfitCost
         WHEN 3 THEN coalesce(V.monCost,0)*fn_UOMConversion(numBaseUnit,I.numItemCode,v_numDomainID,numPurchaseUnit)
         ELSE coalesce(OI.monAvgCost,0)
         END)
      END)) AS Profit
      FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = OM.numOppId
         AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode 
		 AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 LIMIT 1) TEMPMatchedPO on TRUE
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OM.numOppId
         AND OIInner.numItemCode = OI.numItemCode
         AND OIInner.vcAttrValues = OI.vcAttrValues LIMIT 1) TEMPPO on TRUE
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND 1 =(CASE
      WHEN v_tintDateRange = 1
      THEN(CASE WHEN OM.bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 year' AND v_dtEndDate+INTERVAL '-1 year' THEN 1 ELSE 0 END)
      WHEN v_tintDateRange = 2
      THEN(CASE WHEN OM.bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 month' AND v_dtEndDate+INTERVAL '-1 month' THEN 1 ELSE 0 END)
      ELSE 0
      END)
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP;


   v_GrossProfitChange := 100.0*(coalesce(v_GrossProfit1,0) -coalesce(v_GrossProfit2,0))/(CASE WHEN coalesce(v_GrossProfit2,0) = 0 THEN 1 ELSE coalesce(v_GrossProfit2,0) END);


   RETURN coalesce(v_GrossProfitChange,0.00);
END; $$;

