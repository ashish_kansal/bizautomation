-- Stored procedure definition script usp_GetDealHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDealHistory(           
--        
v_numDomainID NUMERIC,        
 v_dtDateFrom TIMESTAMP,        
 v_dtDateTo TIMESTAMP,        
 v_numTerID NUMERIC DEFAULT 0,        
 v_numUserCntID NUMERIC DEFAULT 0,        
 v_tintRights SMALLINT DEFAULT 3,        
 v_intType NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 3 then  --ALL Records        
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         JOIN AdditionalContactsInformation ACI
         ON ACI.numCreatedBy = OM.numCreatedBy
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      end if;
  Else IF v_tintRights = 2 then --ALL Records related to teritory        
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         JOIN AdditionalContactsInformation ACI
         ON ACI.numCreatedBy = OM.numCreatedBy
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      end if;
  Else IF v_tintRights = 1 then --Owner Records        
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount ,fn_GetContactName(OM.numrecowner) as vcUserDesc
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      ELSE
         open SWV_RefCur for
         SELECT  OM.vcpOppName,
    CI.vcCompanyName || ' - (' || DM.vcDivisionName || ')' As CompanyName,
    OM.bintAccountClosingDate,
    (SELECT GetOppStatus(OM.numOppId)) As Stage,
    LD.vcData As Reason, OM.monPAmount,fn_GetContactName(OM.numrecowner) as vcUserDesc
         FROM OpportunityMaster OM
         join  DivisionMaster DM
         on DM.numDivisionID = OM.numDivisionId
         join CompanyInfo CI
         on CI.numCompanyId = DM.numCompanyID
         JOIN AdditionalContactsInformation ACI
         ON ACI.numCreatedBy = OM.numCreatedBy
         left join  Listdetails LD
         on LD.numListItemID = OM.lngPConclAnalysis
         WHERE OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         ORDER BY OM.bintAccountClosingDate DESC LIMIT 10;
      end if;
   end if;
   RETURN;
END; $$;


