-- Stored procedure definition script USP_MassPurchaseFulfillment_GetRecordsForPutAway for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_GetRecordsForPutAway(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_vcSelectedRecords TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainDivisionID  NUMERIC(18,0);
BEGIN
   select   coalesce(v_numDomainDivisionID,0) INTO v_numDomainDivisionID FROM Domain WHERE numDomainId = v_numDomainID;

   DROP TABLE IF EXISTS tt_TEMPFIELDSRIGHT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSRIGHT
   (
      ID VARCHAR(50),
      numFieldID NUMERIC(18,0),
      vcFieldName VARCHAR(100),
      vcOrigDbColumnName VARCHAR(100),
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      vcAssociatedControlType VARCHAR(20),
      bitCustomField BOOLEAN,
      intRowNum INTEGER,
      intColumnWidth DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPFIELDSRIGHT(ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth)
   SELECT
   CONCAT(143,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbCOlumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,CAST(0 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   DycFormField_Mapping DFFM
   ON
   DFCD.numFormID = DFFM.numFormID
   AND DFCD.numFieldID = DFFM.numFieldID
   INNER JOIN
   DycFieldMaster DFM
   ON
   DFFM.numFieldID = DFM.numFieldID
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 143
   AND coalesce(DFFM.bitDeleted,false) = false
   AND coalesce(DFFM.bitSettingField,false) = true
   AND coalesce(DFCD.bitCustom,false) = false;

   IF(SELECT COUNT(*) FROM tt_TEMPFIELDSRIGHT) = 0 then
	
      INSERT INTO tt_TEMPFIELDSRIGHT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 143
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND (coalesce(DFFM.bitDefault,false) = true OR coalesce(DFFM.bitRequired,false) = true);
   ELSE
      INSERT INTO tt_TEMPFIELDSRIGHT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 143
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND coalesce(DFFM.bitRequired,false) = true
      AND DFM.numFieldID NOT IN(SELECT T1.numFieldID FROM tt_TEMPFIELDSRIGHT T1);
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0),
      numWOID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0)
   );

   IF COALESCE(v_vcSelectedRecords,'') <> '' THEN
   INSERT INTO tt_TEMP(numOppID
		,numWOID
		,numOppItemID)
   SELECT
   coalesce(OppID,0)
		,coalesce(WOID,0)
		,coalesce(OppItemID,0)
   FROM
   XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcSelectedRecords AS XML)
				COLUMNS
					id FOR ORDINALITY,
					OppID NUMERIC(18,0) PATH 'OppID',
					WOID NUMERIC(18,0) PATH 'WOID',
					OppItemID NUMERIC(18,0) PATH 'OppItemID'
			) AS X ;
	END IF;

   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      ID NUMERIC(18,0),
      numSortOrder INTEGER,
      numOppID NUMERIC(18,0),
      numWOID NUMERIC(18,0),
      bitStockTransfer BOOLEAN,
      numDivisionID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      vcPoppName VARCHAR(300),
      numBarCodeId VARCHAR(300),
      vcItemName VARCHAR(300),
      vcItemDesc TEXT,
      vcSKU VARCHAR(300),
      vcPathForTImage TEXT,
      vcAttributes TEXT,
      numRemainingQty DOUBLE PRECISION,
      numQtyToShipReceive DOUBLE PRECISION,
      bitSerialized BOOLEAN,
      bitLotNo BOOLEAN,
      bitPPVariance BOOLEAN,
      numCurrencyID NUMERIC(18,0),
      fltExchangeRate DOUBLE PRECISION,
      charItemType VARCHAR(3),
      vcItemType VARCHAR(50),
      monPrice DECIMAL(20,5),
      bitDropShip BOOLEAN,
      numProjectID NUMERIC(18,0),
      numClassID NUMERIC(18,0),
      numAssetChartAcntId NUMERIC(18,0),
      numCOGsChartAcntId NUMERIC(18,0),
      numIncomeChartAcntId NUMERIC(18,0),
      SerialLotNo TEXT,
      vcWarehouses TEXT
   );

   INSERT INTO tt_TEMPITEMS(ID
		,numSortOrder
		,numOppID
		,numWOID
		,bitStockTransfer
		,numDivisionID
		,numOppItemID
		,numItemCode
		,numWarehouseItemID
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcAttributes
		,numRemainingQty
		,numQtyToShipReceive
		,bitSerialized
		,bitLotNo
		,bitPPVariance
		,numCurrencyID
		,fltExchangeRate
		,charItemType
		,vcItemType
		,monPrice
		,bitDropShip
		,numProjectID
		,numClassID
		,numAssetChartAcntId
		,numCOGsChartAcntId
		,numIncomeChartAcntId
		,SerialLotNo
		,vcWarehouses)
   SELECT
   T1.ID
		,coalesce(OpportunityItems.numSortOrder,0)
		,OpportunityMaster.numOppId
		,WorkOrder.numWOId
		,coalesce(OpportunityMaster.bitStockTransfer,false)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN v_numDomainDivisionID ELSE OpportunityMaster.numDivisionId END)
		,OpportunityItems.numoppitemtCode
		,Item.numItemCode
		,WareHouseItems.numWareHouseItemID
		,CAST((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN coalesce(WorkOrder.vcWorkOrderName,'-') ELSE OpportunityMaster.vcpOppName END) AS VARCHAR(300))
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,coalesce(ItemImages.vcPathForTImage,'') AS vcPathForTImage
		,coalesce(OpportunityItems.vcAttributes,'') AS vcAttributes
		,(CASE
   WHEN v_tintMode = 1 THEN coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) -coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
   WHEN v_tintMode = 3 THEN coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) -coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
   ELSE 0
   END) AS numRemainingQty
		,0 AS numQtyToShipReceive
		,coalesce(bitSerialized,false)
		,coalesce(bitLotNo,false)
		,coalesce(OpportunityMaster.bitPPVariance,false)
		,coalesce(OpportunityMaster.numCurrencyID,0)
		,coalesce(OpportunityMaster.fltExchangeRate,0)
		,Item.charItemType
		,OpportunityItems.vcType
		,coalesce(OpportunityItems.monPrice,0)
		,coalesce(OpportunityItems.bitDropShip,false)
		,coalesce(OpportunityItems.numProjectID,0)
		,coalesce(OpportunityItems.numClassID,0)
		,coalesce(Item.numAssetChartAcntId,0)
		,coalesce(Item.numCOGsChartAcntId,0)
		,coalesce(Item.numIncomeChartAcntId,0)
		,(CASE
   WHEN WorkOrder.numWOId IS NOT NULL THEN ''
   ELSE SUBSTR((SELECT
         CONCAT(',',vcSerialNo,(CASE WHEN coalesce(Item.bitLotNo,false) = true THEN ' (' || CAST(whi.numQty AS VARCHAR(15)) || ')' ELSE '' END))
         FROM
         OppWarehouseSerializedItem oppI
         JOIN
         WareHouseItmsDTL whi
         ON
         oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
         WHERE
         oppI.numOppID = OpportunityItems.numOppId
         and oppI.numOppItemID = OpportunityItems.numoppitemtCode
         ORDER BY vcSerialNo),2,200000) END)
		,(CASE
   WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID = WL.numWLocationID WHERE WIInner.numDomainID = v_numDomainID AND WIInner.numItemID = Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
   THEN CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',coalesce(WL.vcLocation,''),'"}'),',' ORDER BY WL.vcLocation ASC)
         FROM
         WareHouseItems WIInner
         LEFT JOIN
         WarehouseLocation WL
         ON
         WIInner.numWLocationID = WL.numWLocationID
         WHERE
         WIInner.numDomainID = v_numDomainID
         AND WIInner.numItemID = Item.numItemCode
         AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
         AND (coalesce(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID = WareHouseItems.numWareHouseItemID)),''),']')
   ELSE CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',coalesce(WL.vcLocation,''),'"}'),',' ORDER BY WL.vcLocation ASC)
         FROM
         WareHouseItems WIInner
         LEFT JOIN
         WarehouseLocation WL
         ON
         WIInner.numWLocationID = WL.numWLocationID
         WHERE
         WIInner.numDomainID = v_numDomainID
         AND WIInner.numItemID = Item.numItemCode
         AND WIInner.numWareHouseItemID = WareHouseItems.numWareHouseItemID),''),']')
   END)
   FROM
   tt_TEMP T1
   LEFT JOIN
   WorkOrder
   ON
   T1.numWOID = WorkOrder.numWOId
   LEFT JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numOppId = T1.numOppID
   LEFT JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityItems.numoppitemtCode = T1.numOppItemID OR coalesce(T1.numOppItemID,0) = 0)
   LEFT JOIN
   WareHouseItems
   ON
   WareHouseItems.numWareHouseItemID =(CASE
   WHEN WorkOrder.numWOId IS NOT NULL
   THEN WorkOrder.numWareHouseItemId
   ELSE(CASE WHEN coalesce(OpportunityMaster.bitStockTransfer,false) = true THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END)
   END)
   INNER JOIN
   Item
   ON(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
   LEFT JOIN
   ItemImages
   ON
   ItemImages.numDomainId = v_numDomainID
   AND ItemImages.numItemCode = Item.numItemCode
   AND ItemImages.bitDefault = true
   AND ItemImages.bitIsImage = true
   WHERE
   1 =(CASE
   WHEN WorkOrder.numWOId IS NOT NULL
   THEN(CASE WHEN WorkOrder.numDomainId = v_numDomainID THEN 1 ELSE 0 END)
   ELSE(CASE WHEN OpportunityMaster.numDomainId = v_numDomainID THEN 1 ELSE 0 END)
   END)
   AND 1 =(CASE
   WHEN WorkOrder.numWOId IS NOT NULL
   THEN 1
   ELSE(CASE WHEN coalesce(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
   END)
   AND (WorkOrder.numWOId IS NOT NULL OR (OpportunityMaster.numOppId IS NOT NULL AND OpportunityItems.numoppitemtCode IS NOT NULL))
   AND(CASE
   WHEN v_tintMode = 1 THEN coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) -coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
   WHEN v_tintMode = 3 THEN coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) -coalesce((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
   ELSE 0
   END) > 0
   ORDER BY
   T1.ID,OpportunityItems.numSortOrder;

   open SWV_RefCur for
   SELECT ID  AS "ID",
      numSortOrder AS "numSortOrder",
      numOppID AS "numOppID",
      numWOID AS "numWOID",
      bitStockTransfer AS "bitStockTransfer",
      numDivisionID AS "numDivisionID",
      numOppItemID AS "numOppItemID",
      numItemCode AS "numItemCode",
      numWarehouseItemID AS "numWarehouseItemID",
      vcPoppName AS "vcPoppName",
      numBarCodeId AS "numBarCodeId",
      vcItemName AS "vcItemName",
      vcItemDesc AS "vcItemDesc",
      vcSKU AS "vcSKU",
      vcPathForTImage AS "vcPathForTImage",
      vcAttributes AS "vcAttributes",
      numRemainingQty AS "numRemainingQty",
      numQtyToShipReceive AS "numQtyToShipReceive",
      bitSerialized AS "bitSerialized",
      bitLotNo AS "bitLotNo",
      bitPPVariance AS "bitPPVariance",
      numCurrencyID AS "numCurrencyID",
      fltExchangeRate AS "fltExchangeRate",
      charItemType AS "charItemType",
      vcItemType AS "vcItemType",
      monPrice AS "monPrice",
      bitDropShip AS "bitDropShip",
      numProjectID AS "numProjectID",
      numClassID AS "numClassID",
      numAssetChartAcntId AS "numAssetChartAcntId",
      numCOGsChartAcntId AS "numCOGsChartAcntId",
      numIncomeChartAcntId AS "numIncomeChartAcntId",
      SerialLotNo AS "SerialLotNo",
      vcWarehouses AS "vcWarehouses" FROM tt_TEMPITEMS ORDER BY ID;	
   open SWV_RefCur2 for
   SELECT ID AS "ID",
      numFieldID AS "numFieldID",
      vcFieldName AS "vcFieldName",
      vcOrigDbColumnName AS "vcOrigDbColumnName",
      bitAllowSorting AS "bitAllowSorting",
      bitAllowFiltering AS "bitAllowFiltering",
      vcAssociatedControlType AS "vcAssociatedControlType",
      bitCustomField AS "bitCustomField",
      intRowNum AS "intRowNum",
      intColumnWidth AS "intColumnWidth" FROM tt_TEMPFIELDSRIGHT ORDER BY intRowNum;
   RETURN;
END; $$;


