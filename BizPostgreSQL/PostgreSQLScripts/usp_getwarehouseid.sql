-- Stored procedure definition script USP_GetWareHouseID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWareHouseID(v_numWareHouseItemID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numWareHouseID AS a from WareHouseItems
   where numWareHouseItemID = v_numWareHouseItemID;
END; $$;












