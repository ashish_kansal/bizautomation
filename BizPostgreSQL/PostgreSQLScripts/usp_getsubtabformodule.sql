-- Stored procedure definition script USP_GetSubTabForModule for PostgreSQL
Create or replace FUNCTION USP_GetSubTabForModule(v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_tintTabType SMALLINT DEFAULT NULL, 
v_numModuleId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
    numTabId,
	numTabName,
	Remarks,
	tintTabType,
	vcURL,
	bitFixed,
	numDomainID,
fn_GetModuleName(numModuleId) as ModuleName
   from TabMaster   TM
   where numDomainID = v_numDomainID
   and tintTabType = v_tintTabType
   and numModuleId = v_numModuleId;
END; $$;












