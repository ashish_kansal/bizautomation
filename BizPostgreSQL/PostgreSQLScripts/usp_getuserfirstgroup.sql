-- Stored procedure definition script usp_GetUserFirstGroup for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetUserFirstGroup(v_numUserID NUMERIC,  
 v_vcGrpName VARCHAR(50)   --
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return the first GroupID for a user.  
   AS $$
BEGIN
   open SWV_RefCur for SELECT numGrpId as "numGrpId" FROM Groups
   WHERE vcGrpName = v_vcGrpName
   order by vcGrpName DESC;
END; $$;












