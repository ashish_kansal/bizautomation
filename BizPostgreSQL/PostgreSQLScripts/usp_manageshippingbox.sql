DROP FUNCTION IF EXISTS USP_ManageShippingBox;

CREATE OR REPLACE FUNCTION USP_ManageShippingBox(v_numShippingReportId NUMERIC(18,0),
    v_strItems TEXT,
    v_numUserCntId NUMERIC(18,0),
	v_tintMode SMALLINT,
	INOUT v_numBoxID NUMERIC(18,0)	,
	v_PageMode SMALLINT)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 1 then
    
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
         UPDATE
         ShippingBox
         SET
         dtDeliveryDate = X.dtDeliveryDate,monShippingRate = X.monShippingRate,
         fltTotalWeight = X.fltTotalWeight,fltHeight = X.fltHeight,fltWidth = X.fltWidth,
         fltLength = X.fltLength,numPackageTypeID = X.numPackageTypeID,
         numServiceTypeID = X.numServiceTypeID,fltDimensionalWeight = X.fltDimensionalWeight,
         numShipCompany = X.numShipCompany
         FROM
		 XMLTABLE
		(
			'NewDataSet/Box'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				dtDeliveryDate TIMESTAMP PATH 'dtDeliveryDate',
				monShippingRate DECIMAL(20,5) PATH 'monShippingRate',
				numBoxID NUMERIC(18,0) PATH 'numBoxID',
				fltTotalWeight DOUBLE PRECISION PATH 'fltTotalWeight',
				fltHeight DOUBLE PRECISION PATH 'fltHeight',
				fltWidth DOUBLE PRECISION PATH 'fltWidth',
				fltLength DOUBLE PRECISION PATH 'fltLength',
				numPackageTypeID BIGINT PATH 'numPackageTypeID',
				numServiceTypeID BIGINT PATH 'numServiceTypeID',
				fltDimensionalWeight DOUBLE PRECISION PATH 'fltDimensionalWeight',
				numShipCompany INTEGER PATH 'numShipCompany'
		) X
         WHERE
         X.numBoxID = ShippingBox.numBoxID
         AND numShippingReportID = v_numShippingReportId;
 
      end if;
   end if; 
  
   IF v_tintMode = 0 then
	
      DELETE FROM ShippingReportItems WHERE numShippingReportId = v_numShippingReportId;
      DELETE  FROM ShippingBox WHERE numShippingReportID = v_numShippingReportId;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
         INSERT  INTO ShippingBox(vcBoxName,
                numShippingReportID,
                fltTotalWeight,
                fltHeight,
                fltWidth,
                fltLength,
				numCreatedBy,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany)
         SELECT
         X.vcBoxName,
                v_numShippingReportId,
                X.fltTotalWeight,
                X.fltHeight,
                X.fltWidth,
                X.fltLength,
				v_numUserCntId,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany
         FROM
		 XMLTABLE
		(
			'NewDataSet/Box'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcBoxName VARCHAR(100) PATH 'vcBoxName',
				fltTotalWeight DOUBLE PRECISION PATH 'fltTotalWeight',
				fltHeight DOUBLE PRECISION PATH 'fltHeight',
				fltWidth DOUBLE PRECISION PATH 'fltWidth',
				fltLength DOUBLE PRECISION PATH 'fltLength',
				numPackageTypeID BIGINT PATH 'numPackageTypeID',
				numServiceTypeID BIGINT PATH 'numServiceTypeID',
				fltDimensionalWeight DOUBLE PRECISION PATH 'fltDimensionalWeight',
				numShipCompany INTEGER PATH 'numShipCompany'
		) X;

         INSERT  INTO ShippingReportItems(numShippingReportId,
                numItemCode,
                tintServiceType,
                dtDeliveryDate,
                monShippingRate,
                fltTotalWeight,
                intNoOfBox,
                fltHeight,
                fltWidth,
                fltLength,
                dtCreateDate,
                numCreatedBy,
                numBoxID,
                numOppBizDocItemID,
                intBoxQty)
         SELECT
         v_numShippingReportId,
                OBI.numItemCode,
                coalesce((SELECT  numServiceTypeID FROM ShippingBox WHERE numBoxID = SB.numBoxID LIMIT 1),0),--Unspecified
                NULL,
                0.0
                ,(CASE
         WHEN coalesce(bitKitParent,false) = true
         AND(CASE
         WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
         THEN 1
         ELSE 0
         END) = 0
         THEN
            GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
         ELSE
            coalesce(fltWeight,0)
         END),
                1,
                I.fltHeight,
                I.fltWidth,
                I.fltLength,
                TIMEZONE('UTC',now()),
                v_numUserCntId,
                SB.numBoxID,
                X.numOppBizDocItemID,
                X.intBoxQty
         FROM
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					vcBoxName VARCHAR(100) PATH 'vcBoxName',
					numOppBizDocItemID NUMERIC PATH 'numOppBizDocItemID',
					intBoxQty BIGINT PATH 'intBoxQty'
			) X
		 INNER JOIN ShippingBox SB ON SB.numShippingReportID = v_numShippingReportId AND X.vcBoxName = SB.vcBoxName
         INNER JOIN OpportunityBizDocItems OBI ON X.numOppBizDocItemID = OBI.numOppBizDocItemID
         INNER JOIN OpportunityItems OI ON OBI.numOppItemID = OI.numoppitemtCode
         INNER JOIN Item I ON OBI.numItemCode = I.numItemCode;

      end if;
   end if;
   RETURN;
END; $$;


