-- Stored procedure definition script USP_GetDiscountItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDiscountItem(v_numDomainID BIGINT DEFAULT 0,
v_str VARCHAR(20) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numItemCode as VARCHAR(255)),cast(vcItemName as VARCHAR(255)) from Item where vcItemName ilike coalesce(v_str,'') || '%' AND numDomainID =  v_numDomainID;
END; $$; 













