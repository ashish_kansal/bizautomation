-- Stored procedure definition script USP_TrueCommerceQueueLog_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


CREATE OR REPLACE FUNCTION USP_TrueCommerceQueueLog_Insert(v_FileName VARCHAR(50),
	v_numDomainID NUMERIC(18,0),
    v_PO VARCHAR(300),
	--@vcLog NTEXT,
	--@bitSuccess BIT,
	v_vcMessage TEXT)
RETURNS VOID LANGUAGE plpgsql
	--@numTCQueueID NUMERIC(18,0),
	--@TCType INT,
   AS $$
BEGIN
   INSERT INTO TrueCommerceLog(
		--numTCQueueID
		--,
		FileName
		,numDomainID
		,PurchaseOrder
		,vcMessage
		,dtDate)
	VALUES(
		--@numTCQueueID
		--,
		v_FileName
		,v_numDomainID
		,v_PO
		,v_vcMessage
		,TIMEZONE('UTC',now()));
RETURN;
END; $$;




