-- Stored procedure definition script Usp_UpdateScheduledOccurance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_UpdateScheduledOccurance(v_numScheduleId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Transactiontype  BOOLEAN;
   v_DtEndDate  TIMESTAMP;
   v_noTransaction  NUMERIC;
   v_noTracsactioncmp  NUMERIC;
   v_DateDiff  INTEGER;
BEGIN
   select   bitEndTransactionType, coalesce(dtEndDate,'Jan  1 1753 12:00AM'), coalesce(numNoTransaction,0), coalesce(numNoTransactionCmp,0) INTO v_Transactiontype,v_DtEndDate,v_noTransaction,v_noTracsactioncmp from CustRptScheduler where numScheduleid = v_numScheduleId;

   RAISE NOTICE '%',v_Transactiontype;
   RAISE NOTICE '%',v_DtEndDate;
   RAISE NOTICE '%',v_noTransaction;
   RAISE NOTICE '%',v_noTracsactioncmp+1;

   if v_Transactiontype = false then --indicates its enddate tyep
	
      v_DateDiff := DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_DtEndDate);
      if v_DateDiff >= 0 then
			
         RAISE NOTICE '%',v_DateDiff;
         RAISE NOTICE 'expired';
         update CustRptScheduler set
         bitrecurring = true,LastRecurringDate = TIMEZONE('UTC',now())
         where numScheduleid = v_numScheduleId;
      else
         RAISE NOTICE 'recurring';
         update CustRptScheduler set
         LastRecurringDate = TIMEZONE('UTC',now())
         where numScheduleid = v_numScheduleId;
      end if;
   else
      if v_noTracsactioncmp+1 >= v_noTransaction then
				
         RAISE NOTICE 'expired';
         update CustRptScheduler set
         bitrecurring = true,LastRecurringDate = TIMEZONE('UTC',now()),numNoTransactionCmp = v_noTracsactioncmp+1
         where numScheduleid = v_numScheduleId;
      else
         RAISE NOTICE 'recurring';
         update CustRptScheduler set
         LastRecurringDate = TIMEZONE('UTC',now()),numNoTransactionCmp = v_noTracsactioncmp+1
         where numScheduleid = v_numScheduleId;
      end if;
   end if;
   RETURN;
END; $$;


