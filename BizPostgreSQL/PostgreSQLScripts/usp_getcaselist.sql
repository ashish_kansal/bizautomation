-- Stored procedure definition script USP_GetCaseList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCaseList(v_numUserCntId NUMERIC(9,0) DEFAULT 0,                            
 v_tintSortOrder NUMERIC DEFAULT 0,                                                                
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                            
 v_tintUserRightType SMALLINT DEFAULT NULL,                                            
 v_SortChar CHAR(1) DEFAULT '0' ,                                            
 v_CustName VARCHAR(100) DEFAULT '',                                            
 v_FirstName VARCHAR(100) DEFAULT '',                                            
 v_LastName VARCHAR(100) DEFAULT '',                                            
 v_CurrentPage INTEGER DEFAULT NULL,                                            
 v_PageSize INTEGER DEFAULT NULL,                                            
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                            
 v_columnName VARCHAR(50) DEFAULT NULL,                                            
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                          
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,              
 v_bitPartner BOOLEAN DEFAULT false,
 v_numStatus NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(5000);                                            
   v_firstRec  INTEGER;                                            
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCaseID VARCHAR(15)
   );                                            
                                             
   v_strSql := 'SELECT  ';                               
   if v_tintSortOrder = 3 or v_tintSortOrder = 4 then  
      v_strSql := coalesce(v_strSql,'') || '  ';
   end if;                                             
   v_strSql := coalesce(v_strSql,'') || '                                             
   Cs.numCaseId                                                             
 FROM                                               
   AdditionalContactsInformation ADC                                             
 JOIN Cases  Cs                                              
   ON ADC.numContactId = Cs.numContactId                                             
 JOIN DivisionMaster Div                                               
  ON ADC.numDivisionId = Div.numDivisionID';              
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CaseContacts CCont on CCont.numCaseID = Cs.numCaseId and CCont.bitPartner = true and CCont.numContactID =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '';
   end if;              
                                               
   v_strSql := coalesce(v_strSql,'') || ' JOIN CompanyInfo CMP                                             
 ON Div.numCompanyID = CMP.numCompanyId                                             
 left join ListDetails lst                                            
 on lst.numListItemID = Cs.numPriority       
 left join ListDetails lst1                                            
 on lst1.numListItemID = Cs.numStatus                                                                
 left join AdditionalContactsInformation ADC1                                            
 on Cs.numAssignedTo = ADC1.numContactId                                            
 Where                                            
 Cs.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';                      
 
   IF v_numStatus > 0 then
      v_strSql := coalesce(v_strSql,'') || 'and Cs.numStatus = ' || SUBSTR(CAST(v_numStatus AS VARCHAR(15)),1,15);
   ELSE
      v_strSql := coalesce(v_strSql,'') || 'and Cs.numStatus <> 136 ';
   end if;
   
 
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                                 
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''';
   end if;                      
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;                                            
                                             
                                           
   if v_numDivisionID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And Div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   end if;                                      
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And (ADC.vcFirstName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                              
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' or ADC.vcLastName ilike ''' || coalesce(v_SortChar,'') || '%'') LIMIT 20';
   end if;                                             
                                             
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) ||
      case when v_bitPartner = true then ' or (CCont.bitPartner=true and CCont.numContactId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')' else '' end  || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' ) or Div.numTerID=0 or Cs.numAssignedTo=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || '
) ';
   end if;   
  
   if v_numDivisionID = 0 then

      if v_tintSortOrder = 0 then  
         v_strSql := coalesce(v_strSql,'') || ' AND (Cs.numRecOwner = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ' or Cs.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) || ')';
      end if;
   end if;                                            
   if v_tintSortOrder = 2 then  
      v_strSql := coalesce(v_strSql,'') || ' AND cs.bintCreatedDate> ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' and cs.numCreatedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' and cs.numModifiedBy =' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15);
   end if;                                            
                                       
   if v_tintSortOrder = 0 then 
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 1
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY cs.bintCreatedDate';
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY cs.bintModifiedDate';
   end if;                              

   RAISE NOTICE '%',v_strSql;                                       
   EXECUTE 'insert into tt_TEMPTABLE (numCaseID)                                            
' || v_strSql;                     
                    
                    
                                          
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                     
                    
                    
   open SWV_RefCur for
   SELECT   ADC.vcFirstName || ' ' || ADC.vcLastname as Name,
   CMP.vcCompanyName,
   Cs.numCaseId,
   Cs.vcCaseNumber,
   Cs.intTargetResolveDate,
   Cs.numCreatedby,
   Cs.textSubject,
   lst.vcData as Priority,
   CMP.numCompanyId,
   Div.numDivisionID,
   Div.numTerID,
   Div.tintCRMType,
   ADC.numContactId as ActCntID,
   ADC1.vcFirstName || ' ' || ADC1.vcLastname || '/ ' || fn_GetContactName(Cs.numAssignedBy)  as AssignedToBy,
   fn_GetListItemName(Cs.numStatus) as Status,
   Cs.numRecOwner
   FROM
   AdditionalContactsInformation ADC
   JOIN Cases  Cs
   ON ADC.numContactId = Cs.numContactId
   JOIN DivisionMaster Div
   ON ADC.numDivisionId = Div.numDivisionID
   JOIN CompanyInfo CMP
   ON Div.numCompanyID = CMP.numCompanyId
   left join Listdetails lst
   on lst.numListItemID = Cs.numPriority
   left join AdditionalContactsInformation ADC1
   on Cs.numAssignedTo = ADC1.numContactId
   join tt_TEMPTABLE T on T.numCaseID = Cs.numCaseId
   where ID > v_firstRec and ID < v_lastRec;               
                    
                                         
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                            
   RETURN;
END; $$;


