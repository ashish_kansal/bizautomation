-- Stored procedure definition script USP_ProSaveSubStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProSaveSubStage(v_byteMode SMALLINT DEFAULT null,
	v_numProID NUMERIC(9,0) DEFAULT null,
	v_stageDetailsId NUMERIC(9,0) DEFAULT null,
	v_strSubStage TEXT DEFAULT '',
	v_numSubStageHdrID NUMERIC(9,0) DEFAULT NULL,
	v_numSubStageID NUMERIC(9,0) DEFAULT null, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_byteMode = 0 then
      SELECT * INTO v_hDoc FROM SWF_Xml_PrepareDocument(v_strSubStage);
      delete from ProjectsSubStageDetails where numStageDetailsId = v_stageDetailsId and numProId = v_numProID;
      insert into ProjectsSubStageDetails(numProId,
				numStageDetailsId,
				numSubStageID,
				numProcessListId,
				vcSubStageDetail,
				bitStageCompleted)
      select v_numProID,v_stageDetailsId,v_numSubStageID,X.* from(SELECT *FROM SWF_OpenXml(v_hDoc,'/NewDataSet/Table','numSalesProsessList |vcSubStageDetail |bitStageCompleted') SWA_OpenXml(numSalesProsessList NUMERIC(9,0),vcSubStageDetail VARCHAR(1000),bitStageCompleted BOOLEAN)) X;
      PERFORM SWF_Xml_RemoveDocument(v_hDoc);
   end if;
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numSubStageElmtID,numProId,numStageDetailsId,
				numSubStageID ,
				numProcessListId as numSalesProsessList,
				vcSubStageDetail,
				bitStageCompleted
      from ProjectsSubStageDetails
      where numProId = v_numProID and numStageDetailsId = v_stageDetailsId;
   end if;

   if v_byteMode = 2 then

      open SWV_RefCur for
      select 	numSubstageDtlid,
		0 as numProId,
		0 as numStageDetailsId,
		0 as numSubStageID,
		numProcessListID as numSalesProsessList,
		vcSubStageElemt as vcSubStageDetail,
		false as bitStageCompleted
      from SubStageDetails
      where  numSubStageHdrID = v_numSubStageHdrID;
   end if;
   RETURN;
END; $$;


