-- Stored procedure definition script USP_RepBizDocsByCont for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RepBizDocsByCont(v_numContactID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcBizDocID AS "vcBizDocID",fn_GetListItemName(numBizDocId) as "BizDoc",'Viewed' as "ActivityType",dtViewedDate as "Date" from OpportunityBizDocs
   where numViewedBy = v_numContactID;
END; $$;












