CREATE OR REPLACE FUNCTION USP_GetDateFieldWorkFlowQueue
(
	v_numDomainID NUMERIC(18,0) 
	,v_numFormID NUMERIC(18,0)
	,v_numWFID NUMERIC(18,0)
	,v_textCondition TEXT
	,v_textConditionCustom TEXT
	,INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_SQL  TEXT;
BEGIN
	IF v_numFormID = 68 then  --Organization	
		v_SQL := 'SELECT 
					DMT.numDivisionID AS numRecordID 
				FROM 
					DivisionMaster_TempDateFields DMT
				WHERE 
					numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
					AND DMT.numDivisionID NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textCondition,'');
      
		IF LENGTH(coalesce(v_textConditionCustom,'')) > 0 then	
			v_SQL := coalesce(v_SQL,'') 
					|| ' UNION 
						SELECT 
							RecId AS numRecordID 
						FROM 
							CFW_FLD_Values 
						WHERE 
							is_date(Fld_Value) = true 
							AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textConditionCustom,'');
		end if;
	ELSEIF v_numFormID = 69 then  --Contacts
		v_SQL := 'SELECT 
						ACIT.numContactId AS numRecordID
						, * 
					FROM 
						AdditionalContactsInformation_TempDateFields ACIT
					WHERE 
						numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						AND ACIT.numContactId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textCondition,'');
      
		IF LENGTH(coalesce(v_textConditionCustom,'')) > 0 then
			v_SQL := coalesce(v_SQL,'') 
					|| ' UNION 
							SELECT 
								RecId  AS numRecordID 
							FRom 
								CFW_FLD_Values_Cont 
							WHERE 
								is_date(Fld_Value) = true 
								AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textConditionCustom,'');
		end if;
	ELSEIF v_numFormID = 70 then  --Opp/Order
		v_SQL := 'SELECT 
						OMT.numOppId AS numRecordID
						, *
					FROM 
						OpportunityMaster_TempDateFields OMT
					WHERE 
						numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						AND OMT.numOppId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
				|| coalesce(v_textCondition,'');

		IF LENGTH(coalesce(v_textConditionCustom,'')) > 0 then
			v_SQL := coalesce(v_SQL,'') 
					|| ' UNION 
							SELECT 
								RecId AS numRecordID 
							FROM 
								CFW_Fld_Values_Opp 
							WHERE 
								is_date(Fld_Value) = true 
								AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textConditionCustom,'');
		end if;
	ELSEIF v_numFormID = 72 then  --Cases
		v_SQL := 'SELECT 
						CT.numCaseId AS numRecordID
						,* 
					FROM 
						Cases_TempDateFields CT
					WHERE 
						numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						AND CT.numCaseId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
				|| coalesce(v_textCondition,'');

		IF LENGTH(coalesce(v_textConditionCustom,'')) > 0 then
			v_SQL := coalesce(v_SQL,'') 
					|| ' UNION 
						SELECT 
							RecId  AS numRecordID 
						FRom 
							CFW_FLD_Values_Case 
						WHERE 
							is_date(Fld_Value) = true 
							AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textConditionCustom,'');
		end if;
	ELSEIF v_numFormID = 73 then  --Projects
		v_SQL := 'SELECT 
						PMT.numProId AS numRecordID
						, * 
					FROM 
						ProjectsMaster_TempDateFields PMT
					WHERE 
						numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						AND PMT.numProId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
				|| coalesce(v_textCondition,'');
		
		IF LENGTH(coalesce(v_textConditionCustom,'')) > 0 then	
			v_SQL := coalesce(v_SQL,'') 
					|| ' UNION 
							SELECT 
								RecId  AS numRecordID 
							FRom 
								CFW_FLD_Values_Pro 
							WHERE 
								is_date(Fld_Value) = true 
								AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
					|| coalesce(v_textConditionCustom,'');
		end if;
	ELSEIF v_numFormID = 124 then  --Action Items
		v_SQL := 'SELECT 
						CT.numCommId AS numRecordID
						, * 
					FROM 
						Communication_TempDateFields CT
					WHERE 
						numDomainID = ' || COALESCE(v_numDomainID,0) || ' 
						AND CT.numCommId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(timezone(''utc'', now()) AS DATE ))) AND numWFID = ' || COALESCE(v_numWFID,0) || ')'
				|| coalesce(v_textCondition,'');
	end if;

	RAISE NOTICE '%',v_SQL;
		
	OPEN SWV_RefCur FOR EXECUTE v_SQL;
   RETURN;
END; $$;




