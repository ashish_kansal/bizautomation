-- Stored procedure definition script USP_ReportListMaster_WarehouseInventoryValue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_WarehouseInventoryValue(v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   with
   Cte
   as(Select --top 15 15 AS TotalParentRecords, 
--isnull(WareHouseItems.numOnHand,'0') as OnHand,
   coalesce(Item.monAverageCost,0) as AverageCost,
coalesce(Warehouses.vcWareHouse,'') as WareHouse,
--CONCAT('~/Items/frmItemList.aspx?Page=InventoryItems&ItemGroup=0&WID=',Warehouses.numWareHouseID) AS URL,
coalesce(Warehouses.numWareHouseID,0) as WareHouseID,

--isnull((WareHouseItems.numOnHand + WarehouseItems.numAllocation)*(Item.monAverageCost),0)	as InventoryValue	 
(coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0))*(coalesce(Item.monAverageCost,0)) as InventoryValue
   FROM Item
   Left Join Vendor on Vendor.numDomainID = v_numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode = Vendor.numItemCode
   join WareHouseItems  on WareHouseItems.numDomainID = v_numDomainID and Item.numItemCode = WareHouseItems.numItemID
   LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID
   join Warehouses Warehouses on Warehouses.numDomainID = v_numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   WHERE 1 = 1 AND Item.numDomainID = v_numDomainID AND coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0) > 0  and coalesce(Item.monAverageCost,0) > 0
   and  coalesce(Item.IsArchieve,false) = false)
   select cte.WareHouse,
cte.WareHouseID,
--Cte.URL,
cast(SUM(cte.InventoryValue) as DECIMAL(18,0)) as InventoryValue
   from  cte group by Cte.WareHouse,Cte.WareHouseID
--Cte.URL 
   order by InventoryValue desc;
END; $$;












