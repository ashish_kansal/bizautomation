-- Stored procedure definition script USP_MassSalesFulfillmentShippingConfig_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentShippingConfig_Get(v_numDomainID NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0)
	,v_tintSourceType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM
   MassSalesFulfillmentShippingConfig
   WHERE
   numDomainID = v_numDomainID
   AND numOrderSource = v_numOrderSource
   AND tintSourceType = v_tintSourceType;
END; $$;












