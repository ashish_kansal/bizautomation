-- Function definition script GetFiscalStartDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetFiscalStartDate(v_Year INTEGER,v_numDomainID NUMERIC)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtFiscalStartDate  TIMESTAMP;
   v_month INT;
BEGIN
   select   coalesce(tintFiscalStartMonth,1) INTO v_month from Domain where numDomainId = v_numDomainID;
   v_dtFiscalStartDate := CAST('01/01/' || SUBSTR(CAST(v_Year AS VARCHAR(4)),1,4) AS TIMESTAMP);
   v_dtFiscalStartDate := v_dtFiscalStartDate+CAST(v_month -1 || 'month' as interval);
   RETURN v_dtFiscalStartDate;
END; $$;

