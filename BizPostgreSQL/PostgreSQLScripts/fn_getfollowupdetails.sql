-- Function definition script fn_GetFollowUpDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Priya>
-- Create date: <15 Jan 2018>
-- Description:	<Fetch Last and Next Follow ups>
-- =============================================
CREATE OR REPLACE FUNCTION fn_GetFollowUpDetails(v_numContactID NUMERIC, v_numFollowup INTEGER, v_numDomainID NUMERIC)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetFollowup  TEXT;
BEGIN
   v_RetFollowup := '';

   IF(v_numFollowup = 1) then
  
      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = true) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL
            THEN CONCAT(GenericDocuments.VcDocName || ' ',coalesce(FormatedDateFromDate(ConECampaignDTL.bintSentON,v_numDomainID),
               ''),' <img alt="Emails Read" height="16px" width="16px" title="EmailsRead" src="../images/email_read.png"> (',
               (SELECT COUNT(*) FROM ConECampaignDTL WHERE coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true),')')
            WHEN tblActionItemData.RowID IS NOT NULL
            THEN CONCAT(tblActionItemData.Activity,' ',coalesce(FormatedDateFromDate(ConECampaignDTL.bintSentON,v_numDomainID),
               ''),' <img alt="Action Items" height="16px" width="16px" title="Action Items" src="../images/MasterList-16.gif">')
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   ELSEIF (v_numFollowup = 2)
   then

      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = false) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN CONCAT(GenericDocuments.VcDocName,' ' || coalesce(FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,v_numDomainID),
               ''))
            WHEN tblActionItemData.RowID IS NOT NULL THEN CONCAT(tblActionItemData.Activity,' ' || coalesce(FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,v_numDomainID),
               ''))
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MIN(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = false))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   ELSEIF (v_numFollowup = 3)
   then
  
      select   CASE
      WHEN(SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID = ConECampaign.numConEmailCampID ANd coalesce(bitSend,false) = true) > 0 THEN(SELECT(CASE
            WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN(SELECT vcEmailLog FROM ConECampaignDTL WHERE coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true)
            ELSE ''
            END)
            FROM
            ConECampaignDTL
            LEFT JOIN
            ECampaignDTLs
            ON
            ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
            LEFT JOIN
            GenericDocuments
            ON
            ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
            LEFT JOIN
            tblActionItemData
            ON
            ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
            WHERE
            ConECampaignDTL.numConECampDTLID =(SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true))
      ELSE ''
      END INTO v_RetFollowup FROM
      ConECampaign WHERE
      ConECampaign.numContactID = v_numContactID
      AND coalesce(bitEngaged,false) = true;
   end if;

   RETURN v_RetFollowup;
END; $$;

