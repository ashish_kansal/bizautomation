-- Function definition script GetEmailTemplateName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetEmailTemplateName(v_numConEmailCampID NUMERIC(9,0) -- Pass PK([numConEmailCampID]) of ConECampaign
	
	--@ECampDetailId numeric(9,0)
)
RETURNS VARCHAR(100) LANGUAGE plpgsql
	-- Declare the return variable here
   AS $$
   DECLARE
   v_Result  VARCHAR(100);

	-- Add the T-SQL statements to compute the return value here


BEGIN
   select   VcDocName INTO v_Result FROM   GenericDocuments WHERE  GenericDocuments.numDocCategory = 369
   AND numGenericDocID IN(SELECT  numEmailTemplate
      FROM   ECampaignDTLs
      WHERE  numECampDTLId IN(SELECT    numECampDTLID
         FROM     ConECampaignDTL
         WHERE    bitSend = true
         AND numConECampID = v_numConEmailCampID
         ORDER BY numConECampDTLID DESC LIMIT 1) LIMIT 1);

	-- Return the result of the function
   RETURN v_Result;
END; $$;

--Created By Anoop Jayaraj

