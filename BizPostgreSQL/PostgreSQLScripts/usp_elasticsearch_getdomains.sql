-- Stored procedure definition script USP_ElasticSearch_GetDomains for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetDomains(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numDomainId
   FROM
   Domain
   WHERE
   coalesce(bitElasticSearch,false) = true;
   RETURN;
END; $$;













