-- Stored procedure definition script USP_ManageCaseAssociatedContact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageCaseAssociatedContact(v_numCaseId NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numRole NUMERIC(9,0) DEFAULT 0,
v_bitPartner BOOLEAN DEFAULT NULL,
v_byteMode SMALLINT DEFAULT NULL,
v_bitSubscribedEmailAlert BOOLEAN DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_byteMode = 0 then

      insert into CaseContacts(numCaseID, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
 values(v_numCaseId,v_numContactID,v_numRole,v_bitPartner,v_bitSubscribedEmailAlert);
   ELSEIF v_byteMode = 1
   then

      delete from CaseContacts where numCaseID = v_numCaseId and numContactID = v_numContactID;
   end if;


       
   open SWV_RefCur for SELECT  a.numContactId,vcCompanyName || ', ' || coalesce(Lst.vcData,'-') as Company,
  a.vcFirstName || ' ' || a.vcLastname as Name,        
  --a.numPhone +', '+ a.numPhoneExtension as Phone,  
  case when a.numPhone <> '' then a.numPhone || case when a.numPhoneExtension <> '' then ' - ' || a.numPhoneExtension else '' end  else '' end as Phone,
  a.vcEmail as Email,
  cast(b.vcData as VARCHAR(255)) as ContactRole,
  cast(CCont.numRole as VARCHAR(255)) as ContRoleId,
  coalesce(bitPartner,false) as bitPartner,
  a.numContactType,CASE WHEN CCont.bitSubscribedEmailAlert = true THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM CaseContacts CCont
   join AdditionalContactsInformation a on
   a.numContactId = CCont.numContactID
   join DivisionMaster D
   on a.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   left join Listdetails b
   on b.numListItemID = CCont.numRole
   left join Listdetails Lst
   on Lst.numListItemID = C.numCompanyType
   WHERE CCont.numCaseID = v_numCaseId    and a.numDomainID = v_numDomainID;
END; $$;












