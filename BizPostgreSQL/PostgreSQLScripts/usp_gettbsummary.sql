-- Stored procedure definition script USP_GetTBSummary for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTBSummary(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT v_dtFromDate as FromDate,v_dtToDate as ToDate,v_numDomainId as numDomainId,numAccountTypeID,vcAccountCode,vcAccountType,numParentID,
coalesce((SELECT SUM(coalesce(COA.numOriginalOpeningBal,0))
      FROM Chart_Of_Accounts COA WHERE COA.numDomainId = v_numDomainId
      AND COA.vcAccountCode ilike ATD.vcAccountCode || '%'),0)+coalesce((SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      AND
      COA.numDomainId = v_numDomainId AND
      GJD.numDomainId = v_numDomainId AND
      COA.vcAccountCode ilike ATD.vcAccountCode || '%' AND
      GJH.datEntry_Date < v_dtFromDate),
   0) AS OPENING,
cast(coalesce((SELECT coalesce(SUM(GJD.numDebitAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      AND
      COA.numDomainId = v_numDomainId AND
      GJD.numDomainId = v_numDomainId AND
      COA.vcAccountCode ilike ATD.vcAccountCode || '%' AND
      GJH.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate),0) as DOUBLE PRECISION) as DEBIT,
cast(coalesce((SELECT coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
      INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
      AND
      COA.numDomainId = v_numDomainId AND
      GJD.numDomainId = v_numDomainId AND
      COA.vcAccountCode ilike ATD.vcAccountCode || '%' AND
      GJH.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate),0) as DOUBLE PRECISION) as CREDIT
   FROM AccountTypeDetail ATD
   WHERE numDomainID = v_numDomainId;
END; $$;












