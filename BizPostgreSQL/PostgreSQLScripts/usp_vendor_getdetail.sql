-- Stored procedure definition script USP_Vendor_GetDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Vendor_GetDetail(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_vcAttrValues VARCHAR(500), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT 
   coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0) AS "numQty"
		,FormatedDateFromDate(OI.ItemRequiredDate,v_numDomainID) AS "vcDueDate"
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numDivisionId = v_numDivisionID
   AND OM.tintopptype = 2
   AND OM.tintopptype = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND OI.numItemCode = v_numItemCode
   AND coalesce(OI.vcAttrValues,'') = coalesce(v_vcAttrValues,'')
   AND coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0) > 0 LIMIT 10;

   open SWV_RefCur2 for
   SELECT
   SUM(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0)*(monTotAmount/(CASE WHEN coalesce(OI.numUnitHour,0) = 0 THEN 1 ELSE OI.numUnitHour END))) AS "TotalPOAmount"
		,SUM(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0)) AS "TotalQty"
		,SUM(coalesce(I.fltWeight,0)*(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0))) AS "TotalWeight"
		,COALESCE((SELECT string_agg(' <b>Buy</b> ' || CASE WHEN u.intType = 1 THEN '$' || CAST(u.vcBuyingQty AS TEXT) ELSE CAST(u.vcBuyingQty AS TEXT) END || ' ' || CASE WHEN u.intType = 1 THEN 'Amount' WHEN u.intType = 2 THEN 'Quantity' WHEN u.intType = 3 THEN 'Lbs' ELSE '' END || ' <b>Get</b> ' ||(CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END) || '<br/>','' ORDER BY u.numPurchaseIncentiveId)
      
      FROM
      PurchaseIncentives u
      WHERE
      u.numPurchaseIncentiveId = numPurchaseIncentiveId
      AND numDivisionId = v_numDivisionID),'')  AS "Purchaseincentives"
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item AS I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numDivisionId = v_numDivisionID
   AND OM.tintopptype = 2
   AND OM.tintopptype = 1
   AND coalesce(OM.tintshipped,0) = 0;
   RETURN;
END; $$;


