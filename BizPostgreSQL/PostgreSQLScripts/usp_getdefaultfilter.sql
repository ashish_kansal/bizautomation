-- Stored procedure definition script USP_GetDefaultfilter for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDefaultfilter(v_numDomainId NUMERIC(9,0),
v_numUserCntId NUMERIC(9,0),
v_numRelation NUMERIC(9,0),
v_FormId NUMERIC(9,0),
v_GroupId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from RelationsDefaultFilter
   WHERE numUserCntId = v_numUserCntId and numDomainID = v_numDomainId
   and numRelationId = v_numRelation
   and numformId = v_FormId AND tintGroupId = v_GroupId;
END; $$;












