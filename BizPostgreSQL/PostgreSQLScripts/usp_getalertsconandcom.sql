CREATE OR REPLACE FUNCTION USP_GetAlertsConAndCom(v_numAlerDTLId NUMERIC(9,0) DEFAULT 0,
v_byteMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select Alt.numAlertConID,Com.vcCompanyName,Div.vcDivisionName,
	AddC.vcFirstName || ' ' || AddC.vcLastname as Name,coalesce(Lst.vcData,'-') as Rating
      from AlertContactsAndCompany Alt
      join AdditionalContactsInformation AddC
      on AddC.numContactId = Alt.numContactID
      join DivisionMaster Div
      on Div.numDivisionID = AddC.numDivisionId
      join CompanyInfo Com
      on Com.numCompanyId = Div.numCompanyID
      left join Listdetails Lst
      on Lst.numListItemID = Com.numCompanyRating
      where numAlerDTLId = v_numAlerDTLId;
   end if;
   if v_byteMode = 1 then

      open SWV_RefCur for
      select Alt.numAlertConID,Com.vcCompanyName,Div.vcDivisionName,coalesce(Lst.vcData,'-') as Rating from AlertContactsAndCompany Alt
      join DivisionMaster Div
      on Div.numDivisionID = Alt.numContactID
      join CompanyInfo Com
      on Com.numCompanyId = Div.numCompanyID
      left join Listdetails Lst
      on Lst.numListItemID = Com.numCompanyRating
      where numAlerDTLId = v_numAlerDTLId;
   end if;
   RETURN;
END; $$;


