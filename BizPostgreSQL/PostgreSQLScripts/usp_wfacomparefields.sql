-- Stored procedure definition script USP_WFACompareFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 14thAug2014>
-- Description:	<Description,, Bind Compare fields WFA>
-- =============================================
Create or replace FUNCTION USP_WFACompareFields(v_numDomainID NUMERIC(18,0),
	v_numFieldID  NUMERIC(18,0),
	v_numFormID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_Fields  VARCHAR(150);
BEGIN


    -- Insert statements for procedure here
   select   vcWFCompareField INTO v_Fields FROM View_DynamicDefaultColumns where  numDomainID = v_numDomainID  and numFieldID = v_numFieldID
   and numFormId = v_numFormID
   AND coalesce(bitWorkFlowField,false) = true AND coalesce(bitCustom,false) = false
   AND intWFCompare = 1;

   open SWV_RefCur for
   select v_Fields;

   open SWV_RefCur2 for
   SELECT numFieldID,vcFieldName,
		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
		   vcListItemType,numListID,false AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit,coalesce(vcGroup,'Custom Fields') as vcGroup,coalesce(intWFCompare,0) as intWFCompare
   FROM View_DynamicDefaultColumns
   where  numDomainID = v_numDomainID
   and numFormId = v_numFormID
   AND coalesce(bitWorkFlowField,false) = true AND coalesce(bitCustom,false) = false
		--AND intWFCompare=1
   AND vcOrigDbColumnName IN(v_Fields);
   RETURN;
END; $$;


