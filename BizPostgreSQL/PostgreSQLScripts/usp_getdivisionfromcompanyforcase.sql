-- Stored procedure definition script usp_GetDivisionFromCompanyForCase for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDivisionFromCompanyForCase(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_vcCompanyName VARCHAR(100) DEFAULT '' 
--  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     DivisionMaster.vcDivisionName,DivisionMaster.numDivisionID
   FROM         DivisionMaster INNER JOIN
   CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE CompanyInfo.vcCompanyName = v_vcCompanyName;
END; $$;












