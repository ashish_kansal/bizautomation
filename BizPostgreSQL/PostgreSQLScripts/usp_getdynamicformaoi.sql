-- Stored procedure definition script usp_GetDynamicFormAOI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetDynamicFormAOI(v_numDomainID NUMERIC(9,0) DEFAULT 0,                                     
 v_numFormId INTEGER DEFAULT NULL ,        
 v_numGroupID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select numListItemID as numAOIID,vcData as vcAOIName from Listdetails where numDomainid = v_numDomainID and numListID = 74;         
        
   open SWV_RefCur2 for
   select distinct(numListItemID) as numAOIID,vcData as vcAOIName from DynamicFormAOIConf D
   join  Listdetails L on L.numListItemID = D.numAOIID
   where L.numDomainid = v_numDomainID  and L.numListID = 74
   and numFormID = v_numFormId
   and numGroupID = v_numGroupID;
   RETURN;
END; $$;


