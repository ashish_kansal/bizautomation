-- Stored procedure definition script USP_ReportListMaster_Top10CampaignsByROI for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10CampaignsByROI(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
		vcCampaignName,
		CONCAT('~/Marketing/frmCampaignDetails.aspx?CampID=',numCampaignID) AS URL,
		GetIncomeforCampaign(numCampaignID,4::SMALLINT,NULL::TIMESTAMP,NULL::TIMESTAMP) as ROI
   FROM
   CampaignMaster
   WHERE
   numDomainID = v_numDomainID
   AND GetIncomeforCampaign(numCampaignID,4::SMALLINT,NULL::TIMESTAMP,NULL::TIMESTAMP) <> 0
   ORDER BY
   GetIncomeforCampaign(numCampaignID,4::SMALLINT,NULL::TIMESTAMP,NULL::TIMESTAMP) DESC LIMIT 10;
END; $$;












