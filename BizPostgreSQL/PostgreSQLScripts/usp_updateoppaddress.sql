DROP FUNCTION IF EXISTS USP_UpdateOppAddress;

CREATE OR REPLACE FUNCTION USP_UpdateOppAddress(v_numOppID NUMERIC(9,0) DEFAULT 0,      
v_byteMode SMALLINT DEFAULT NULL,
p_numAddressID NUMERIC(18,0) DEFAULT NULL,
v_vcStreet VARCHAR(100) DEFAULT NULL,      
v_vcCity VARCHAR(50) DEFAULT NULL,      
v_vcPostalCode VARCHAR(15) DEFAULT NULL,      
v_numState NUMERIC(9,0) DEFAULT NULL,      
v_numCountry NUMERIC(9,0) DEFAULT NULL,  
v_vcCompanyName VARCHAR(100) DEFAULT NULL, 
v_numCompanyId NUMERIC(9,0) DEFAULT NULL, -- This in Parameter is added by Ajit Singh on 01/10/2008,
v_vcAddressName VARCHAR(50) DEFAULT '',
v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,
v_bitCalledFromProcedure BOOLEAN DEFAULT false, -- Avoid Tax calculation when this procedure is called from another procedure because all calling procedure have tax calulation uptill now
v_numContact NUMERIC(18,0) DEFAULT 0,
v_bitAltContact BOOLEAN DEFAULT false,
v_vcAltContact VARCHAR(200) DEFAULT '',
v_numUserCntID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);   -- This local variable is added by Ajit Singh on 01/10/2008
	v_numAddressID NUMERIC(18,0);
   v_numDomainID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;
BEGIN
   v_numReturnHeaderID := NULLIF(v_numReturnHeaderID,0);
   v_numOppID := NULLIF(v_numOppID,0);
	
   v_numDomainID := 0;
	
   IF v_byteMode = 0 then
	
      IF coalesce(v_numOppID,0) > 0 then
		
         update OpportunityMaster set tintBillToType = 2 where numOppId = v_numOppID;
         IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numOppID = v_numOppID) then
			
            INSERT INTO OpportunityAddress(numOppID
					,numBillAddressID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyId
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact)
				VALUES(v_numOppID
					,p_numAddressID
					,v_vcCompanyName
					,v_vcStreet
					,v_vcCity
					,v_numState
					,v_vcPostalCode
					,v_numCountry
					,v_vcAddressName
					,v_numReturnHeaderID
					,v_numCompanyId
					,v_numContact
					,v_bitAltContact
					,v_vcAltContact);
         ELSE
            UPDATE
            OpportunityAddress
            SET
            numBillAddressID = p_numAddressID,vcBillCompanyName = v_vcCompanyName,vcBillStreet = v_vcStreet,vcBillCity = v_vcCity,
            numBillState = v_numState,vcBillPostCode = v_vcPostalCode,numBillCountry = v_numCountry,
            vcAddressName = v_vcAddressName,numBillCompanyId = v_numCompanyId,
            numBillingContact = v_numContact,bitAltBillingContact = v_bitAltContact,
            vcAltBillingContact = v_vcAltContact
            WHERE
            numOppID = v_numOppID;
         end if;
         IF coalesce(v_numUserCntID,0) > 0 then
			
            IF NOT EXISTS(SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID = v_numOppID AND tintAddressOf = 2 AND tintAddressType = 1) then
				
               INSERT INTO OrgOppAddressModificationHistory(tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate)
					VALUES(2
						,1
						,v_numOppID
						,v_numUserCntID
						,TIMEZONE('UTC',now()));
            ELSE
               UPDATE
               OrgOppAddressModificationHistory
               SET
               numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
               WHERE
               numRecordID = v_numOppID
               AND tintAddressOf = 2
               AND tintAddressType = 1;
            end if;
         end if;
      ELSEIF coalesce(v_numReturnHeaderID,0) > 0
      then
		
         IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numReturnHeaderID = v_numReturnHeaderID) then
			
            INSERT INTO OpportunityAddress(numOppID
					,numBillAddressID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyId
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact)
				VALUES(v_numOppID
					,p_numAddressID
					,v_vcCompanyName
					,v_vcStreet
					,v_vcCity
					,v_numState
					,v_vcPostalCode
					,v_numCountry
					,v_vcAddressName
					,v_numReturnHeaderID
					,v_numCompanyId
					,v_numContact
					,v_bitAltContact
					,v_vcAltContact);
         ELSE
            UPDATE
            OpportunityAddress
            SET
            numBillAddressID = p_numAddressID, vcBillCompanyName = v_vcCompanyName,vcBillStreet = v_vcStreet,vcBillCity = v_vcCity,
            numBillState = v_numState,vcBillPostCode = v_vcPostalCode,numBillCountry = v_numCountry,
            vcAddressName = v_vcAddressName,numBillCompanyId = v_numCompanyId,
            numBillingContact = v_numContact,bitAltBillingContact = v_bitAltContact,
            vcAltBillingContact = v_vcAltContact
            WHERE
            numReturnHeaderID = v_numReturnHeaderID;
         end if;
      end if;
   ELSEIF v_byteMode = 1
   then
	
      IF coalesce(v_numOppID,0) > 0 then
		
         IF v_numCompanyId > 0 then
			
            UPDATE OpportunityMaster set tintShipToType = 2 where numOppId = v_numOppID;
         end if;
         IF NOT EXISTS(SELECT * FROM OpportunityAddress where numOppID = v_numOppID) then
			
            INSERT INTO OpportunityAddress(numOppID
					,numShipAddressID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcShipAddressName
					,numShipCompanyId
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact)
				VALUES(v_numOppID
					,p_numAddressID
					,v_vcCompanyName
					,v_vcStreet
					,v_vcCity
					,v_numState
					,v_vcPostalCode
					,v_numCountry
					,v_vcAddressName
					,v_numCompanyId
					,v_numContact
					,v_bitAltContact
					,v_vcAltContact);
         ELSE
            UPDATE
            OpportunityAddress
            SET
            numShipAddressID = p_numAddressID,vcShipCompanyName = v_vcCompanyName,vcShipStreet = v_vcStreet,vcShipCity = v_vcCity,
            numShipState = v_numState,vcShipPostCode = v_vcPostalCode,numShipCountry = v_numCountry,
            vcShipAddressName = v_vcAddressName,numShipCompanyId = v_numCompanyId,
            numShippingContact = v_numContact,bitAltShippingContact = v_bitAltContact,
            vcAltShippingContact = v_vcAltContact
            WHERE
            numOppID = v_numOppID;
         end if;
         IF coalesce(v_numUserCntID,0) > 0 then
			
            IF NOT EXISTS(SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID = v_numOppID AND tintAddressOf = 2 AND tintAddressType = 2) then
				
               INSERT INTO OrgOppAddressModificationHistory(tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate)
					VALUES(2
						,2
						,v_numOppID
						,v_numUserCntID
						,TIMEZONE('UTC',now()));
            ELSE
               UPDATE
               OrgOppAddressModificationHistory
               SET
               numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
               WHERE
               numRecordID = v_numOppID
               AND tintAddressOf = 2
               AND tintAddressType = 2;
            end if;
         end if;
      ELSEIF coalesce(v_numReturnHeaderID,0) > 0
      then
		
         IF NOT EXISTS(SELECT * FROM OpportunityAddress where numReturnHeaderID = v_numReturnHeaderID) then
			
            INSERT INTO OpportunityAddress(numOppID
					,numShipAddressID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcShipAddressName
					,numShipCompanyId
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact)
				VALUES(v_numOppID
					,p_numAddressID
					,v_vcCompanyName
					,v_vcStreet
					,v_vcCity
					,v_numState
					,v_vcPostalCode
					,v_numCountry
					,v_vcAddressName
					,v_numCompanyId
					,v_numContact
					,v_bitAltContact
					,v_vcAltContact);
         ELSE
            UPDATE
            OpportunityAddress
            SET
            numShipAddressID = p_numAddressID,vcShipCompanyName = v_vcCompanyName,vcShipStreet = v_vcStreet,vcShipCity = v_vcCity,
            numShipState = v_numState,vcShipPostCode = v_vcPostalCode,numShipCountry = v_numCountry,
            vcShipAddressName = v_vcAddressName,numShipCompanyId = v_numCompanyId,
            numShippingContact = v_numContact,bitAltShippingContact = v_bitAltContact,
            vcAltShippingContact = v_vcAltContact
            WHERE
            numReturnHeaderID = v_numReturnHeaderID;
         end if;
      end if;
   ELSEIF v_byteMode = 2
   then
	
      update OpportunityMaster set tintShipToType = 3 where numOppId = v_numOppID;
      IF coalesce(v_numUserCntID,0) > 0 then
		
         IF NOT EXISTS(SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID = v_numOppID AND tintAddressOf = 2 AND tintAddressType = 2) then
			
            INSERT INTO OrgOppAddressModificationHistory(tintAddressOf
					,tintAddressType
					,numRecordID
					,numModifiedBy
					,dtModifiedDate)
				VALUES(2
					,2
					,v_numOppID
					,v_numUserCntID
					,TIMEZONE('UTC',now()));
         ELSE
            UPDATE
            OrgOppAddressModificationHistory
            SET
            numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
            WHERE
            numRecordID = v_numOppID
            AND tintAddressOf = 2
            AND tintAddressType = 2;
         end if;
      end if;
   ELSEIF v_byteMode = 3
   then
	
		--SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 
      IF NOT EXISTS(SELECT * FROM AddressDetails WHERE numRecordID = v_numOppID AND tintAddressOf = 4) then
			
         RAISE NOTICE 'ADD Mode';
         INSERT INTO AddressDetails(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID)
         SELECT v_vcAddressName,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,1,4,2,v_numOppID,numdomainId
         FROM ProjectsMaster WHERE numProId = v_numOppID RETURNING numAddressID INTO v_numAddressID;
         UPDATE ProjectsMaster SET numAddressID = v_numAddressID WHERE numProId = v_numOppID;
      ELSE
         RAISE NOTICE 'Update Mode';
         UPDATE AddressDetails SET
         vcStreet = v_vcStreet,vcCity = v_vcCity,numState = v_numState,vcPostalCode = v_vcPostalCode,
         numCountry = v_numCountry,vcAddressName = v_vcAddressName
         WHERE   numRecordID = v_numOppID;
      end if;
   end if;

   IF v_numOppID > 0 AND coalesce(v_bitCalledFromProcedure,false) = false then

      IF(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppID AND bitAuthoritativeBizDocs = 1) = 0 then
         select   numDomainId, numDivisionId, tintopptype INTO v_numDomainID,v_numDivisionID,v_tintOppType FROM
         OpportunityMaster WHERE
         numOppId = v_numOppID;
         DELETE FROM OpportunityMasterTaxItems WHERE numOppId = v_numOppID;

		--INSERT TAX FOR DIVISION   
         IF v_tintOppType = 1 OR (v_tintOppType = 2 and(select coalesce(bitPurchaseTaxCredit,false) from Domain where numDomainId = v_numDomainID) = true) then
		
            INSERT INTO OpportunityMasterTaxItems(numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID)
            SELECT
            v_numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
            FROM
            TaxItems TI
            JOIN
            DivisionTaxTypes DTT
            ON
            TI.numTaxItemID = DTT.numTaxItemID
            CROSS JOIN LATERAL(SELECT
               decTaxValue,
					tintTaxType
               FROM
               fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,TI.numTaxItemID,v_numOppID,false,NULL)) AS TEMPTax
            WHERE
            DTT.numDivisionID = v_numDivisionID
            AND DTT.bitApplicable = true
            UNION
            SELECT
            v_numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
            FROM
            DivisionMaster
            CROSS JOIN LATERAL(SELECT
               decTaxValue,
					tintTaxType
               FROM
               fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,0,v_numOppID,true,NULL)) AS TEMPTax
            WHERE
            bitNoTax = false
            AND numDivisionID = v_numDivisionID
            UNION
            SELECT
            v_numOppID
				,1
				,decTaxValue
				,tintTaxType
				,numTaxID
            FROM
            fn_CalItemTaxAmt(v_numDomainID,v_numDivisionID,0,1,v_numOppID,true,NULL);
         end if;
         UPDATE OpportunityBizDocs OBD SET monDealAmount = GetDealAmount(v_numOppID,TIMEZONE('UTC',now()),OBD.numOppBizDocsId)
         WHERE numoppid = v_numOppID;
      end if;
   end if;
   RETURN;
END; $$; 


--Created by Anoop Jayaraj



