-- Function definition script FlattenedJSON for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION FlattenedJSON(v_XMLResult XML)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_JSONVersion  TEXT;
   v_Rowcount  INTEGER;
BEGIN
   select   '', COUNT(*) INTO v_JSONVersion,v_Rowcount FROM XMLTABLE('/root/*'
   PASSING v_XMLResult
   COLUMNS) x;
   v_JSONVersion := coalesce(v_JSONVersion,'') ||
   OVERLAY((SELECT TheLine FROM(SELECT ',
		{' ||
      OVERLAY((SELECT ',"' || COALESCE(b.c.value('local-name(.)','NVARCHAR(255)'),'') || '":"' ||
         REPLACE( --escape tab properly within a value
			 REPLACE( --escape return properly
			   REPLACE( --linefeed must be escaped
				 REPLACE( --backslash too
				   REPLACE(COALESCE(b.c.value('text()[1]','NVARCHAR(MAX)'),''),--forwardslash
				   E'\\',E'\\\\'),'/',E'\\/'),CHR(10),E'\\n'),CHR(13),E'\\r'),
         CHR(09),E'\\t')
         || '"'
         FROM x.a.nodes('*') b(c)).(./text()) placing '' from 1 for 1) || '}' AS theLine
      FROM XMLTABLE('/root'
      PASSING v_XMLResult
      COLUMNS b.c VARCHAR(255) PATH '*',b.c TEXT PATH '*',TEXT PATH '*')) JSON(theLine)). placing '' from 1 for 1);
   IF v_Rowcount > 1 then 
      RETURN '[' || coalesce(v_JSONVersion,'') || '
	]';
   end if;
   RETURN v_JSONVersion;
END; $$;

