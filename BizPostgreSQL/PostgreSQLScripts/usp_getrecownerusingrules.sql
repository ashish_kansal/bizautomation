DROP FUNCTION IF EXISTS USP_GetRecOwnerUsingRules;

CREATE OR REPLACE FUNCTION USP_GetRecOwnerUsingRules(v_numDomainID NUMERIC(9,0) DEFAULT 0,        
v_strData TEXT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecOwner  NUMERIC(9,0);
   v_numRoutID  NUMERIC(9,0);        
   v_tintPriority  SMALLINT;        
   v_vcDBColumnName  VARCHAR(100);  
   v_rowcount  SMALLINT;        
   v_tintEqualTo  VARCHAR(4);
BEGIN
   v_numRecOwner := 0;        
        
        
        
        
        
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS  tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      vcDbColumnName VARCHAR(100),
      vcDbColumnText VARCHAR(100)  
   );          
   if SUBSTR(CAST(v_strData AS VARCHAR(10)),1,10) <> '' then
   
      insert into tt_TEMPTABLE(vcDbColumnName,vcDbColumnText)
		SELECT 
			vcDbColumnName
			,vcDbColumnText
		FROM 
			XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strData AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcDbColumnName VARCHAR(100) PATH 'vcDbColumnName',
				vcDbColumnText VARCHAR(100) PATH 'vcDbColumnText'
		) AS X;

      v_numRoutID := CAST(0 AS VARCHAR(15));
      select   numRoutID, tintPriority, vcDBColumnName, tintEqualTo INTO v_numRoutID,v_tintPriority,v_vcDBColumnName,v_tintEqualTo from RoutingLeads where numDomainID = v_numDomainID and bitDefault = false   order by tintPriority LIMIT 1;
      while COALESCE(v_numRoutID,0) > 0 LOOP
         if cast(NULLIF(v_tintEqualTo,'') as INTEGER) = 1  and   (v_vcDBColumnName ilike '%State%') then
  
            select   numEmpID INTO v_numRecOwner from RoutingLeads R
            Join RoutingLeadDetails DTL
            on DTL.numRoutID = R.numRoutID
            join RoutinLeadsValues V
            on V.numRoutID = R.numRoutID
            join tt_TEMPTABLE T
            on T.vcDbColumnName = R.vcDbColumnName and T.vcDbColumnText = V.vcValue where R.numRoutID = v_numRoutID and intAssignOrder = 1    LIMIT 1;
         ELSEIF cast(NULLIF(v_tintEqualTo,'') as INTEGER) = 1
         then
   
            select   numEmpID INTO v_numRecOwner from RoutingLeads R
            Join RoutingLeadDetails DTL
            on DTL.numRoutID = R.numRoutID
            join tt_TEMPTABLE T
            on T.vcDbColumnName = R.vcDbColumnName and T.vcDbColumnText = R.numValue where R.numRoutID = v_numRoutID  and intAssignOrder = 1    LIMIT 1;
         ELSEIF cast(NULLIF(v_tintEqualTo,'') as INTEGER) = 2
         then
  
            select   numEmpID INTO v_numRecOwner from RoutingLeads R
            Join RoutingLeadDetails DTL
            on DTL.numRoutID = R.numRoutID
            join RoutinLeadsValues V
            on V.numRoutID = R.numRoutID
            join tt_TEMPTABLE T
            on T.vcDbColumnName = R.vcDbColumnName where R.numRoutID = v_numRoutID  and T.vcDbColumnText ilike '%' || V.vcValue || '%' and intAssignOrder = 1    LIMIT 1;
         ELSEIF cast(NULLIF(v_tintEqualTo,'') as INTEGER) = 3
         then
  
            select   numEmpID INTO v_numRecOwner from RoutingLeads R
            Join RoutingLeadDetails DTL
            on DTL.numRoutID = R.numRoutID
            join RoutinLeadsValues V
            on V.numRoutID = R.numRoutID
            join tt_TEMPTABLE T
            on T.vcDbColumnName = R.vcDbColumnName and T.vcDbColumnText = V.vcValue where R.numRoutID = v_numRoutID and intAssignOrder = 1    LIMIT 1;
         end if;
         select   numRoutID, tintPriority, vcDBColumnName, tintEqualTo INTO v_numRoutID,v_tintPriority,v_vcDBColumnName,v_tintEqualTo from RoutingLeads where numDomainID = v_numDomainID and bitDefault = false  and tintPriority > v_tintPriority   order by tintPriority LIMIT 1;
         GET DIAGNOSTICS v_rowcount = ROW_COUNT;
         if v_numRecOwner > 0 then
  
            update RoutingLeadDetails set intAssignOrder = intAssignOrder -1 where numRoutID = v_numRoutID;
            update RoutingLeadDetails set intAssignOrder =((select max(intAssignOrder) from RoutingLeadDetails where numRoutID = v_numRoutID)+1) where intAssignOrder = 0;
            v_numRoutID := CAST(0 AS VARCHAR(15));
         end if;
         if v_rowcount = 0 then 
            v_numRoutID := CAST(0 AS VARCHAR(15));
         end if;
      END LOOP;
      if COALESCE(v_numRecOwner,0) = 0 then
 
         select   numEmpId INTO v_numRecOwner from RoutingLeadDetails DTL
         join RoutingLeads R
         on R.numRoutID = DTL.numRoutID where numDomainID = v_numDomainID and bitDefault = true;
      end if;
      drop table IF EXISTS  tt_TEMPTABLE CASCADE;
   ELSE
      select   numEmpId INTO v_numRecOwner from RoutingLeadDetails DTL
      join RoutingLeads R
      on R.numRoutID = DTL.numRoutID where numDomainID = v_numDomainID and bitDefault = true;
   end if;

   open SWV_RefCur for select COALESCE(v_numRecOwner,0);
END; $$;












