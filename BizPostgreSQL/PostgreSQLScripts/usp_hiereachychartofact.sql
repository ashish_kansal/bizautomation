-- Stored procedure definition script USP_HiereachyChartOfAct for PostgreSQL
CREATE OR REPLACE FUNCTION USP_HiereachyChartOfAct(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--@numParentAcntID as numeric(9)=0,        
   AS $$
BEGIN
    
--Declare @strSQL as varchar(8000)    
--SELECT * FROM [Chart_Of_Accounts]
   open SWV_RefCur for
   with recursive    RecursionCTE(RecordID,ParentRecordID,vcAccountName,TOC,T,numChartOfAcntID,numAcntTypeId)
   AS(SELECT   numAccountId AS RecordID,
                                numParntAcntTypeID AS ParentRecordID,
                                vcAccountName AS vcAccountName,
                                CAST('' AS VARCHAR(1000)) AS TOC,
                                CAST('' AS VARCHAR(1000)) AS T,
                                CAST(numAccountId AS VARCHAR(10)) || '~'
   || CAST(numAcntTypeId AS VARCHAR(10)) AS numChartOfAcntID,
                                numAcntTypeId AS numAcntTypeId
   FROM     Chart_Of_Accounts
   WHERE    numParntAcntTypeID IS NULL
   AND numDomainId = v_numDomainID
   UNION ALL
   SELECT   R1.numAccountId AS RecordID,
                                R1.numParntAcntTypeID AS ParentRecordID,
                                R1.vcAccountName AS vcAccountName,
                                CASE WHEN OCTET_LENGTH(R2.TOC) > 0
   THEN CAST(CASE WHEN POSITION('.' IN R2.TOC) > 0 THEN R2.TOC
      ELSE R2.TOC || '.'
      END
      || CAST(R1.numAccountId AS VARCHAR(10)) AS VARCHAR(1000))
   ELSE CAST(CAST(R1.numAccountId AS VARCHAR(10)) AS VARCHAR(1000))
   END AS TOC,
                                CASE WHEN OCTET_LENGTH(R2.TOC) > 0
   THEN CAST('..' || R2.T AS VARCHAR(1000))
   ELSE CAST('' AS VARCHAR(1000))
   END AS T,
                                CAST(R1.numAccountId AS VARCHAR(10)) || '~'
   || CAST(R1.numAcntTypeId AS VARCHAR(10)) AS numChartOfAcntID,
                                R1.numAcntTypeId AS numAcntTypeId
   FROM     Chart_Of_Accounts AS R1
   JOIN RecursionCTE AS R2 ON R1.numParntAcntTypeID = R2.RecordID
   AND R1.numDomainId = v_numDomainID)
   SELECT  cast(RecordID as VARCHAR(255)),
                    T || vcAccountName AS vcCategoryName,
                    cast(TOC as VARCHAR(255)),
                    cast(numChartOfAcntID as VARCHAR(255)),
                    numAcntTypeId,
                    cast(coalesce(vcData,'') as VARCHAR(255)) AS vcAcntType
   FROM    RecursionCTE
   LEFT OUTER JOIN Listdetails ON numAcntTypeId = numLIstItemId
   WHERE   ParentRecordID IS NOT NULL
   ORDER BY numAcntTypeId,TOC ASC;
   RETURN;

--Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcAccountName,TOC,T,numChartOfAcntID,numAcntTypeId)                  
-- as                  
-- (                  
-- select numAccountId,numParntAcntTypeId,vcAccountName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T,
--Convert(varchar(10),numAccountId)+''~''+Convert(varchar(10),numAcntTypeId) as numChartOfAcntID,numAcntTypeId                  
--    from Chart_Of_Accounts                  
--    where numParntAcntTypeId is null and numDomainID='+Convert(varchar(10),@numDomainId)               
--    +' union all                  
--   select R1.numAccountId,                  
--          R1.numParntAcntTypeId,                  
--    R1.vcAccountName,                  
--          case when DataLength(R2.TOC) > 0                  
--                    then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                  
--                                 + cast(R1.numAccountId as varchar(10)))                   
--       else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                   
--                    end as TOC,case when DataLength(R2.TOC) > 0                  
--                    then  convert(varchar(1000),''..''+ R2.T)                  
--else convert(varchar(1000),'''')                  
--                    end as T,Convert(varchar(10),R1.numAccountId)+''~''+Convert(varchar(10),R1.numAcntTypeId) as numChartOfAcntID,R1.numAcntTypeId
--      from Chart_Of_Accounts as R1                        
--      join RecursionCTE as R2 on R1.numParntAcntTypeId = R2.RecordID and R1.numDomainID='+Convert(varchar(10),@numDomainId) +'                   
--  )                   
--select RecordID ,T+vcAccountName as vcCategoryName,TOC,numChartOfAcntID,numAcntTypeId,isnull(vcData,'''') vcAcntType from RecursionCTE left outer join ListDetails on numAcntTypeId=numLIstItemId Where ParentRecordID is not null order by numAcntTypeId,TOC asc'
--  PRINT @strSQL
-- Exec (@strSQL) 
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_HiereachyChartOfActForAccountType]    Script Date: 07/26/2008 16:18:52 ******/













