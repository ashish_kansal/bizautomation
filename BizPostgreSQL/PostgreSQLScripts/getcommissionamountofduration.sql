-- Function definition script GetCommissionAmountOfDuration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCommissionAmountOfDuration(v_numUserId NUMERIC,
      v_numUserCntID NUMERIC,
      v_numDomainId NUMERIC,
      v_dtStartDate TIMESTAMP,
      v_dtEndDate TIMESTAMP,
      v_tintMode SMALLINT)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalCommAmt  DECIMAL(10,2);
BEGIN
   select   coalesce(sum(numComissionAmount),0) INTO v_TotalCommAmt FROM OpportunityMaster Opp
   INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
   INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId and BC.numUserCntID = v_numUserCntID where Opp.numDomainId = v_numDomainId and
   --OB.dtCreatedDate between @dtStartDate and @dtEndDate AND
   BC.numUserCntID = v_numUserCntID
   AND BC.bitCommisionPaid = false --added by chintan BugID 982
   AND oppBiz.bitAuthoritativeBizDocs = 1
   AND ((coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount AND v_tintMode = 0)
   OR (coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount AND v_tintMode = 1));
    
   RETURN v_TotalCommAmt;
END; $$;

