-- Stored procedure definition script USP_GetChartAcntChildDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntChildDetails(v_numDomainId NUMERIC(9,0),                              
v_dtFromDate TIMESTAMP,                            
v_dtToDate TIMESTAMP,                          
v_numParntAcntId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);        
   v_strCount  VARCHAR(4000);
   v_i INT DEFAULT 0;      
   v_strChild  VARCHAR(8000);      
   v_numParentAccountId  NUMERIC(9,0);
BEGIN
   v_strCount := '';                          
   v_strSQL := '';                           
   Select numAccountId INTO v_numParentAccountId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1         
         
         
   v_strChild := fn_ChildCategory(v_numParntAcntId::VARCHAR(100),v_numDomainId) || SUBSTR(CAST(v_numParntAcntId AS VARCHAR(4)),1,4);         
        
	IF v_strChild IS NOT NULL THEN
		 v_strCount := '    
     select  count(*) From General_Journal_Header as GJH inner join General_Journal_Details GJD on GJH.numJournal_Id = GJD.numJournalId Where GJD.numChartAcntId in(' || COALESCE(v_strChild,'') || ')      
     And GJH.datEntry_Date <= ''' || v_dtToDate || '''      
     ';            
   RAISE NOTICE '%',v_strCount;      
   EXECUTE v_strCount INTO v_i;   
	END IF;

     
   If v_i = 0 then
  
      v_strSQL := CONCAT(' Select distinct numAccountId as numAccountId,'''' as CategoryName,numOpeningBal as OpeningBalance,coalesce(bitOpeningBalanceEquity,false) as bitOpeningBalanceEquity                             
       From Chart_Of_Accounts COA Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                                
       Where  COA.numDomainId =',v_numDomainId,' And numParntAcntId =',COALESCE(v_numParntAcntId,0),' And COA.numAccountId <>' ,COALESCE(v_numParentAccountId,0), ' And COA.numOpeningBal is not null');
      v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || v_dtFromDate || ''' And  COA.dtOpeningDate<=''' || v_dtToDate || '''';
   Else
      v_strSQL := ' Select distinct numAccountId as numAccountId,'''' as CategoryName,numOpeningBal as OpeningBalance,coalesce(bitOpeningBalanceEquity,false) as bitOpeningBalanceEquity                                
       From Chart_Of_Accounts COA Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId                                
       Where  COA.numDomainId =' || v_numDomainId || ' And numParntAcntId =' || COALESCE(v_numParntAcntId,0) || ' And COA.numAccountId <>' || COALESCE(v_numParentAccountId,0) || ' And COA.numOpeningBal is not null';
   end if;                        
   RAISE NOTICE '%',v_strSQL;                            
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


