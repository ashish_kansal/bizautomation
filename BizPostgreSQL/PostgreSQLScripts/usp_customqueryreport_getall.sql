-- Stored procedure definition script USP_CustomQueryReport_GetAll for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_GetAll(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		CustomQueryReport.numReportID,
		CustomQueryReport.numDomainID,
		Domain.vcDomainName,
		CustomQueryReport.vcReportName,
		CustomQueryReport.vcReportDescription,
		CustomQueryReport.vcEmailTo,
		(CASE CustomQueryReport.tintEmailFrequency WHEN 1 THEN 'Daily' WHEN 2 THEN 'Weekly' WHEN 3 THEN 'Monthly' WHEN 4 THEN 'Yearly' END) As vcEmailFrequency
   FROM
   CustomQueryReport
   INNER JOIN
   Domain
   ON
   CustomQueryReport.numDomainID = Domain.numDomainId
   ORDER BY
   Domain.vcDomainName ASC;
END; $$;











