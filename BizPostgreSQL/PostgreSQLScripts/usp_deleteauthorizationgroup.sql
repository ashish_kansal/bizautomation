-- Stored procedure definition script USP_DeleteAuthorizationGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteAuthorizationGroup(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numGroupID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from AuthenticationGroupMaster where numGroupID = v_numGroupID and numDomainID = v_numDomainID;
   RETURN;
END; $$;


