-- Stored procedure definition script USP_GetRecordByLinkingID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRecordByLinkingID(v_numDomainID NUMERIC(18,0),
	v_intImportType NUMERIC(18,0),
    v_tintLinkingID SMALLINT,
	v_vcValue VARCHAR(500), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemCount  INTEGER;
BEGIN
   If v_intImportType = 20 then -- Item
	
      IF v_tintLinkingID = 1 then --ItemCode
		
         select   COUNT(*) INTO v_ItemCount FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = CAST(v_vcValue AS NUMERIC(18,0));
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE';
         ELSE
            open SWV_RefCur for
            SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = CAST(v_vcValue AS NUMERIC(18,0));
         end if;
      ELSEIF v_tintLinkingID = 2
      then --SKU
		
         select   COUNT(*) INTO v_ItemCount FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcSKU) = LOWER(v_vcValue);
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ITEM_NOT_FOUND_WITH_GIVEN_SKU';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU';
         ELSE
            open SWV_RefCur for
            SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcSKU) = LOWER(v_vcValue);
         end if;
      ELSEIF v_tintLinkingID = 3
      then --UPC
		
         select   COUNT(*) INTO v_ItemCount FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(numBarCodeId) = LOWER(v_vcValue);
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ITEM_NOT_FOUND_WITH_GIVEN_UPC';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ITEMS_FOUND_WITH_GIVEN_UPC';
         ELSE
            open SWV_RefCur for
            SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND LOWER(numBarCodeId) = LOWER(v_vcValue);
         end if;
      ELSEIF v_tintLinkingID = 4
      then --ModelID
		
         select   COUNT(*) INTO v_ItemCount FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcModelID) = LOWER(v_vcValue);
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ITEM_NOT_FOUND_WITH_GIVEN_ModelID';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ModelID';
         ELSE
            open SWV_RefCur for
            SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcModelID) = LOWER(v_vcValue);
         end if;
      ELSEIF v_tintLinkingID = 7
      then --Item Name
		
         select   COUNT(*) INTO v_ItemCount FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcItemName) = LOWER(v_vcValue);
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ITEM_NOT_FOUND_WITH_GIVEN_ItemName';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ItemName';
         ELSE
            open SWV_RefCur for
            SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND  LOWER(vcModelID) = LOWER(v_vcValue);
         end if;
      end if;
   ELSEIF v_intImportType = 133
   then -- Organization
	
      IF v_tintLinkingID = 1 then -- Non Biz Company ID
		
         select   COUNT(*) INTO v_ItemCount FROM ImportOrganizationContactReference WHERE numDomainID = v_numDomainID AND  LOWER(numNonBizCompanyID) = LOWER(v_vcValue);
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID';
         ELSE
            open SWV_RefCur for
            SELECT numDivisionID,numCompanyID FROM ImportOrganizationContactReference WHERE numDomainID = v_numDomainID AND  LOWER(numNonBizCompanyID) = LOWER(v_vcValue);
         end if;
      ELSEIF v_tintLinkingID = 2
      then -- Organization ID
		
         select   COUNT(*) INTO v_ItemCount FROM DivisionMaster WHERE numDomainID = v_numDomainID AND numDivisionID = CAST(v_vcValue AS NUMERIC(18,0));
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID';
         ELSE
            open SWV_RefCur for
            SELECT numDivisionID,numCompanyID  FROM DivisionMaster WHERE numDomainID = v_numDomainID AND numDivisionID = CAST(v_vcValue AS NUMERIC(18,0));
         end if;
      end if;
   ELSEIF v_intImportType = 134
   then -- Contact
	
      IF v_tintLinkingID = 1 then -- Non Biz Contact ID
		
         select   COUNT(*) INTO v_ItemCount FROM ImportActionItemReference WHERE numDomainID = v_numDomainID AND  LOWER(numNonBizContactID) = v_vcValue;
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID';
         ELSE
            open SWV_RefCur for
            SELECT numContactID,numDivisionID,numCompanyID  FROM ImportActionItemReference WHERE numDomainID = v_numDomainID AND  LOWER(numNonBizContactID) = v_vcValue;
         end if;
      ELSEIF v_tintLinkingID = 2
      then -- Contact ID
		
         select   COUNT(*) INTO v_ItemCount FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = CAST(v_vcValue AS NUMERIC(18,0));
         IF v_ItemCount = 0 then
			
            RAISE EXCEPTION 'CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID';
         ELSEIF v_ItemCount > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID';
         ELSE
            open SWV_RefCur for
            SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = CAST(v_vcValue AS NUMERIC(18,0));
         end if;
      end if;
   end if;
   RETURN;
END; $$;

