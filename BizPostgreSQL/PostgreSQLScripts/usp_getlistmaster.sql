-- Stored procedure definition script USP_GetListMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetListMaster(v_numDomainID NUMERIC(9,0),
    v_numModuleID NUMERIC(9,0) DEFAULT 0,
    v_bitCustom BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  TEXT;
BEGIN
   IF v_bitCustom = false then
    
      open SWV_RefCur for
      SELECT
      LM.numListID,
			(CASE WHEN (v_numModuleID = 8 AND coalesce(CLM.loc_name,'') <> '') THEN CONCAT(LM.vcListName,' (',CLM.loc_name,')') ELSE LM.vcListName END) AS vcListName
      FROM
      listmaster LM
      LEFT JOIN
      CFW_Fld_Master CFM
      ON
      CFM.numDomainID = LM.numDomainID
      AND CFM.numlistid = LM.numListID
      LEFT JOIN
      CFW_Loc_Master CLM
      ON
      CFM.Grp_id = CLM.Loc_id
      WHERE
			(LM.numDomainID = v_numDomainID OR LM.bitFlag = true)
      AND coalesce(LM.bitDeleted,false) = false
      AND (coalesce(LM.numModuleId,0) = v_numModuleID OR v_numModuleID = 0)
      ORDER BY
      vcListName;
   ELSE
      v_strSql := 'SELECT  L.numListID,L.vcListName FROM listmaster L';
      v_strSql := coalesce(v_strSql,'') || ' WHERE (L.numdomainID = ' || v_numDomainID || ' OR L.bitFlag = true) AND coalesce(L.bitDeleted, false) = false AND (coalesce(L.numModuleID, 0) = ' || v_numModuleID || '';
      v_strSql := coalesce(v_strSql,'') || ' OR  L.numModuleID = 0 )';
      v_strSql := coalesce(v_strSql,'') ||   ' UNION ';
      v_strSql := coalesce(v_strSql,'') ||   '(SELECT 
										CFM.numListID,
										(CASE WHEN ' || v_numModuleID || ' = 8 AND coalesce(CLM.Loc_name,'''') <> '''' THEN CONCAT(LM.vcListName,'' ('',CLM.Loc_name,'')'') ELSE LM.vcListName END) AS vcListName
									FROM 
										CFW_Fld_Master CFM 
									LEFT JOIN
										CFW_Loc_Master CLM    
									ON
										CFM.Grp_id = CLM.Loc_Id
									INNER JOIN 
										ListMaster LM 
									ON 
										CFM.numDomainID = LM.numDomainID 
										AND CFM.numlistid= LM.numListID 
										';
      IF v_numModuleID = 1 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (1,12,13,14)';
      end if;
      IF v_numModuleID = 2 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (2,6)';
      end if;
      IF v_numModuleID = 3 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (5)';
      end if;
      IF v_numModuleID = 4 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (11)';
      end if;
      IF v_numModuleID = 5 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (3)';
      end if;
      IF v_numModuleID = 6 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (12,13,14)';
      end if;
      IF v_numModuleID = 7 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (0)';
      end if;
      IF v_numModuleID = 8 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (0)';
      end if;
      IF v_numModuleID = 9 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (4)';
      end if;
      IF v_numModuleID = 10 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (0)';
      end if;
      IF v_numModuleID = 11 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (15)';
      end if;
      IF v_numModuleID = 12 then
			
         v_strSql := coalesce(v_strSql,'') || 'AND Grp_id IN (0)';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' WHERE CFM.numDomainID=' || v_numDomainID || ') ';
      v_strSql := coalesce(v_strSql,'') || ' order by vcListName ';
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   end if;
   RETURN;
END; $$;


