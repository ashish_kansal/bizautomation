-- Stored procedure definition script USP_ApplyCouponCodetoCart for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ApplyCouponCodetoCart(v_numDomainID NUMERIC(18,0),
	INOUT v_numItemCode NUMERIC(18,0) ,
	v_cookieId TEXT DEFAULT NULL,
	v_numUserCntId NUMERIC DEFAULT 0,
	v_vcSendCoupon VARCHAR(200) DEFAULT 0,
	v_numSiteID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltOfferTriggerValue  DOUBLE PRECISION;
   v_tintOfferTriggerValueType  SMALLINT;
   v_tintOfferBasedOn  SMALLINT;
   v_fltDiscountValue  DOUBLE PRECISION;
   v_tintDiscountType  SMALLINT;
   v_tintDiscoutBaseOn  SMALLINT;
   v_PromotionID  NUMERIC(18,0);
   v_PromotionDesc  VARCHAR(500);
		


   v_isPromotionTrigerred  BOOLEAN DEFAULT 0;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numTempCartID  NUMERIC(18,0);
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempItemClassification  NUMERIC(18,0);
   v_numTempUnitHour  NUMERIC(18,0);
   v_monTempPrice  NUMERIC(18,0);
   v_monTempTotalAmount  NUMERIC(18,0);
   v_remainingOfferValue  DOUBLE PRECISION DEFAULT v_fltOfferTriggerValue;
   v_remainingDiscountValue  DOUBLE PRECISION DEFAULT v_fltDiscountValue;

   v_k  INTEGER DEFAULT 1;
   v_kCount  INTEGER;
   v_usedDiscountAmount  DOUBLE PRECISION;
   v_usedDiscountQty  DOUBLE PRECISION;
BEGIN
   IF(SELECT COUNT(*) FROM PromotionOffer PO LEFT JOIN PromotionOfferSites POS ON PO.numProId = POS.numPromotionID WHERE numDomainId = v_numDomainID AND coalesce(POS.numSiteID,0) = v_numSiteID AND txtCouponCode = v_vcSendCoupon AND coalesce(bitEnabled,false) = true) = 0 then
	
      RAISE EXCEPTION 'INVALID_COUPON_CODE';
      RETURN;
   ELSEIF(SELECT
   COUNT(*)
   FROM
   PromotionOffer PO
   LEFT JOIN
   PromotionOfferSites POS
   ON
   PO.numProId = POS.numPromotionID
   WHERE
   numDomainId = v_numDomainID
   AND POS.numSiteID = v_numSiteID
   AND txtCouponCode = v_vcSendCoupon
   AND bitEnabled = true
   AND 1 =(CASE WHEN coalesce(tintUsageLimit,0) > 0 THEN(CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN coalesce(bitNeverExpires,false) = true THEN 1 ELSE(CASE WHEN LOCALTIMESTAMP  BETWEEN dtValidFrom:: BYTEA AND dtValidTo:: BYTEA THEN 1 ELSE 0 END) END)) = 0
   then
	
      RAISE EXCEPTION 'COUPON_EXPIRED';
      RETURN;
   end if;

   IF((SELECT COUNT(numCartId) FROM CartItems WHERE numDomainId = v_numDomainID AND vcCookieId = v_cookieId AND numUserCntId = v_numUserCntId AND vcCoupon = v_vcSendCoupon) = 0) then
      v_numItemCode := 1;
      select   PO.numProId, PO.fltOfferTriggerValue, PO.tintOfferTriggerValueType, PO.tintOfferBasedOn, fltDiscountValue, tintDiscountType, tintDiscoutBaseOn, (CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN SUBSTR(CAST(fltOfferTriggerValue AS VARCHAR(30)),1,30) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
      WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
      WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
         '"')
      END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
      CASE
      WHEN tintDiscoutBaseOn = 1 THEN
         CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 2 THEN
         CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
      ELSE ''
      END)) INTO v_PromotionID,v_fltOfferTriggerValue,v_tintOfferTriggerValueType,v_tintOfferBasedOn,
      v_fltDiscountValue,v_tintDiscountType,v_tintDiscoutBaseOn,
      v_PromotionDesc FROM
      PromotionOffer  AS PO WHERE
      PO.numDomainId = v_numDomainID
      AND PO.txtCouponCode = v_vcSendCoupon
      AND bitEnabled = true
      AND 1 =(CASE WHEN coalesce(tintUsageLimit,0) > 0 THEN(CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
      AND 1 =(CASE WHEN coalesce(bitNeverExpires,false) = true THEN 1 ELSE(CASE WHEN LOCALTIMESTAMP  BETWEEN dtValidFrom:: BYTEA AND dtValidTo:: BYTEA THEN 1 ELSE 0 END) END)    LIMIT 1;
      DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMS
      (
         ID INTEGER,
         numCartID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numItemClassification NUMERIC(18,0),
         numUnitHour NUMERIC(18,0),
         monPrice NUMERIC(18,0),
         monTotalAmount NUMERIC(18,0)
      );
      IF v_tintOfferBasedOn = 1 then --individual items
		
         INSERT INTO
         tt_TEMPITEMS
         SELECT
         ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour*CI.monPrice)
         FROM
         CartItems CI
         INNER JOIN
         Item I
         ON
         CI.numItemCode = I.numItemCode
         WHERE
         CI.numDomainId = v_numDomainID
         AND CI.numUserCntId = v_numUserCntId
         AND CI.vcCookieId = v_cookieId
         AND coalesce(CI.PromotionID,0) = 0
         AND CI.numItemCode IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 1)
         AND 1 =(CASE
         WHEN v_tintOfferTriggerValueType = 1 -- Quantity
         THEN(CASE WHEN numUnitHour >= v_fltOfferTriggerValue THEN 1 ELSE 0 END)
         WHEN v_tintOfferTriggerValueType = 2 -- Amount
         THEN(CASE WHEN(CI.numUnitHour*CI.monPrice) >= v_fltOfferTriggerValue THEN 1 ELSE 0 END)
         ELSE 0
         END);
      ELSEIF v_tintOfferBasedOn = 2
      then --item classifications
		
         INSERT INTO
         tt_TEMPITEMS   with CTE(numCartId,numItemCode,numItemClassification,numUnitHour,monPrice,monTotalAmount) AS(SELECT
         numCartId AS numCartId,
					CI.numItemCode AS numItemCode,
					I.numItemClassification AS numItemClassification,
					CI.numUnitHour AS numUnitHour,
					CI.monPrice AS monPrice,
					(CI.numUnitHour*CI.monPrice) AS monTotalAmount
         FROM
         CartItems CI
         INNER JOIN
         Item I
         ON
         CI.numItemCode = I.numItemCode
         WHERE
         CI.numDomainId = v_numDomainID
         AND CI.numUserCntId = v_numUserCntId
         AND CI.vcCookieId = v_cookieId
         AND coalesce(CI.PromotionID,0) = 0
         AND I.numItemClassification IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 2)) SELECT
         ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				numItemCode,
				numItemClassification,
				numUnitHour,
				monPrice,
				monTotalAmount
         FROM
         CTE
         WHERE
         1 =(CASE
         WHEN v_tintOfferTriggerValueType = 1 -- Quantity
         THEN(CASE WHEN(SELECT SUM(numUnitHour) FROM CTE) >= v_fltOfferTriggerValue THEN 1 ELSE 0 END)
         WHEN v_tintOfferTriggerValueType = 2 -- Amount
         THEN(CASE WHEN(SELECT SUM(monTotalAmount) FROM CTE) >= v_fltOfferTriggerValue THEN 1 ELSE 0 END)
         ELSE 0
         END);
      end if;
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPITEMS;
      IF v_COUNT > 0 then
		
         v_isPromotionTrigerred := true;
         WHILE v_i <= v_COUNT LOOP
            select   numCartID, numItemCode, coalesce(numItemClassification,0), numUnitHour, monPrice, (numUnitHour*monPrice) INTO v_numTempCartID,v_numTempItemCode,v_numTempItemClassification,v_numTempUnitHour,
            v_monTempPrice,v_monTempTotalAmount FROM
            tt_TEMPITEMS WHERE
            ID = v_i;
            IF v_tintOfferBasedOn = 1 -- Individual Items
            AND 1 =(CASE
            WHEN v_tintOfferTriggerValueType = 1 -- Quantity
            THEN CASE WHEN v_numTempUnitHour >= v_fltOfferTriggerValue THEN 1 ELSE 0 END
            WHEN v_tintOfferTriggerValueType = 2 -- Amount
            THEN CASE WHEN v_monTempTotalAmount >= v_fltOfferTriggerValue THEN 1 ELSE 0 END
            ELSE 0
            END) then
				
               UPDATE
               CartItems
               SET
               PromotionID = v_PromotionID,PromotionDesc = v_PromotionDesc,bitParentPromotion = true,
               vcCoupon = v_vcSendCoupon
               WHERE
               numCartId = v_numTempCartID;
               IF 1 =(CASE v_tintDiscoutBaseOn
               WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND POI.numValue = v_numTempItemCode AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND POI.numValue = v_numTempItemClassification AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
               WHEN 3 THEN(CASE v_tintOfferBasedOn
                  WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
                  WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
                  ELSE 0
                  END)
               ELSE 0
               END) then
					
                  IF v_tintDiscountType = 1  AND v_fltDiscountValue > 0 then --Percentage
						
                     UPDATE
                     CartItems
                     SET
                     fltDiscount = v_fltDiscountValue,bitDiscountType = 0,monTotAmount =(v_monTempTotalAmount -((v_monTempTotalAmount*v_fltDiscountValue)/100))
                     WHERE
                     numCartId = v_numTempCartID;
                  ELSEIF v_tintDiscountType = 2 AND v_fltDiscountValue > 0
                  then --Flat Amount
						
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
                     select   SUM(coalesce(fltDiscount,0)) INTO v_usedDiscountAmount FROM
                     CartItems WHERE
                     PromotionID = v_PromotionID;
                     IF v_usedDiscountAmount < v_fltDiscountValue then
							
                        UPDATE
                        CartItems
                        SET
                        monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN 0 ELSE(v_monTempTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN v_monTempTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                        WHERE
                        numCartId = v_numTempCartID;
                     end if;
                  ELSEIF v_tintDiscountType = 3  AND v_fltDiscountValue > 0
                  then --Quantity
						
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
                     select   SUM(coalesce(fltDiscount,0)/(CASE WHEN coalesce(monPrice,1) = 0 THEN 1 ELSE coalesce(monPrice,1) END)) INTO v_usedDiscountQty FROM
                     CartItems WHERE
                     PromotionID = v_PromotionID;
                     IF v_usedDiscountQty < v_fltDiscountValue then
							
                        UPDATE
                        CartItems
                        SET
                        monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN 0 ELSE(v_monTempTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN(v_numTempUnitHour*v_monTempPrice) ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice) END)
                        WHERE
                        numCartId = v_numTempCartID;
                     end if;
                  end if;
               end if;
               EXIT;
            ELSEIF v_tintOfferBasedOn = 2
            then -- Item Classification
				
               UPDATE
               CartItems
               SET
               PromotionID = v_PromotionID,PromotionDesc = v_PromotionDesc,bitParentPromotion = true,
               vcCoupon = v_vcSendCoupon
               WHERE
               numCartId = v_numTempCartID;

					-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
               IF 1 =(CASE v_tintDiscoutBaseOn
               WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND POI.numValue = v_numTempItemCode AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND POI.numValue = v_numTempItemClassification AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
               WHEN 3 THEN(CASE v_tintOfferBasedOn
                  WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
                  WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = v_numTempItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
                  ELSE 0
                  END)
               ELSE 0
               END) then
					
                  IF v_tintDiscountType = 1 then --Percentage
						
                     UPDATE
                     CartItems
                     SET
                     fltDiscount = v_fltDiscountValue,bitDiscountType = 0,monTotAmount =(v_monTempTotalAmount -((v_monTempTotalAmount*v_fltDiscountValue)/100))
                     WHERE
                     numCartId = v_numTempCartID;
                  ELSEIF v_tintDiscountType = 2
                  then --Flat Amount
						
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
                     select   SUM(coalesce(fltDiscount,0)) INTO v_usedDiscountAmount FROM
                     CartItems WHERE
                     PromotionID = v_PromotionID;
                     IF v_usedDiscountAmount < v_fltDiscountValue then
							
                        UPDATE
                        CartItems
                        SET
                        monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN 0 ELSE(v_monTempTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN v_monTempTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                        WHERE
                        numCartId = v_numTempCartID;
                     end if;
                  ELSEIF v_tintDiscountType = 3
                  then --Quantity
						
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
                     select   SUM(coalesce(fltDiscount,0)/(CASE WHEN coalesce(monPrice,1) = 0 THEN 1 ELSE coalesce(monPrice,1) END)) INTO v_usedDiscountQty FROM
                     CartItems WHERE
                     PromotionID = v_PromotionID;
                     IF v_usedDiscountQty < v_fltDiscountValue then
							
                        UPDATE
                        CartItems
                        SET
                        monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN 0 ELSE(v_monTempTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice)) END),bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= v_numTempUnitHour THEN(v_numTempUnitHour*v_monTempPrice) ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice) END)
                        WHERE
                        numCartId = v_numTempCartID;
                     end if;
                  end if;
               end if;
               v_remainingOfferValue := v_remainingOfferValue -(CASE WHEN v_tintOfferTriggerValueType = 1 THEN v_numTempUnitHour ELSE v_monTempTotalAmount END);
               IF v_remainingOfferValue <= 0 then
					
                  EXIT;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
      IF coalesce(v_isPromotionTrigerred,false) = true then
         RAISE NOTICE '%',CONCAT(v_isPromotionTrigerred,v_tintDiscoutBaseOn);
         DELETE FROM tt_TEMPITEMS;
         INSERT INTO
         tt_TEMPITEMS
         SELECT
         ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour*CI.monPrice)
         FROM
         CartItems CI
         INNER JOIN
         Item I
         ON
         CI.numItemCode = I.numItemCode
         WHERE
         CI.numDomainId = v_numDomainID
         AND CI.numUserCntId = v_numUserCntId
         AND CI.vcCookieId = v_cookieId
         AND coalesce(CI.PromotionID,0) = 0
         AND 1 =(CASE v_tintDiscoutBaseOn
         WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue = I.numItemCode AND numProId = v_PromotionID AND tintRecordType = 6 AND tintType = 1) > 0 THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue = I.numItemClassification AND numProId = v_PromotionID AND tintRecordType = 6 AND tintType = 2) > 0 THEN 1 ELSE 0 END)
         WHEN 3 THEN(CASE v_tintOfferBasedOn
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = I.numItemCode AND numParentItemCode IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 1)) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM SimilarItems WHERE numItemCode = I.numItemCode AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId = v_PromotionID AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
            ELSE 0
            END)
         ELSE 0
         END);
         select   COUNT(*) INTO v_kCount FROM tt_TEMPITEMS;
         IF v_kCount > 0 then
			
            WHILE v_k <= v_kCount LOOP
               select   numCartID, numItemCode, coalesce(numItemClassification,0), numUnitHour, monPrice, (numUnitHour*monPrice) INTO v_numTempCartID,v_numTempItemCode,v_numTempItemClassification,v_numTempUnitHour,
               v_monTempPrice,v_monTempTotalAmount FROM
               tt_TEMPITEMS WHERE
               ID = v_k;
               IF v_tintDiscountType = 1 then --Percentage
					
                  UPDATE
                  CartItems
                  SET
                  PromotionID = v_PromotionID,PromotionDesc = v_PromotionDesc,bitParentPromotion = false,
                  fltDiscount = v_fltDiscountValue,bitDiscountType = false,monTotAmount =(v_monTempTotalAmount -((v_monTempTotalAmount*v_fltDiscountValue)/100))
                  WHERE
                  numCartId = v_numTempCartID;
               ELSEIF v_tintDiscountType = 2
               then --Flat Amount
					
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
                  select   SUM(fltDiscount) INTO v_usedDiscountAmount FROM
                  CartItems WHERE
                  PromotionID = v_PromotionID;
                  IF v_usedDiscountAmount < v_fltDiscountValue then
						
                     UPDATE
                     CartItems
                     SET
                     monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount)  >= v_monTempTotalAmount THEN 0 ELSE(v_monTempTotalAmount -(v_fltDiscountValue -v_usedDiscountAmount)) END),PromotionID = v_PromotionID,PromotionDesc = v_PromotionDesc,
                     bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountAmount) >= v_monTempTotalAmount THEN v_monTempTotalAmount ELSE(v_fltDiscountValue -v_usedDiscountAmount) END)
                     WHERE
                     numCartId = v_numTempCartID;
                  ELSE
                     EXIT;
                  end if;
               ELSEIF v_tintDiscountType = 3
               then --Quantity
					
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
                  select   SUM(fltDiscount/monPrice) INTO v_usedDiscountQty FROM
                  CartItems WHERE
                  PromotionID = v_PromotionID;
                  IF v_usedDiscountQty < v_fltDiscountValue then
						
                     UPDATE
                     CartItems
                     SET
                     monTotAmount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty)  >= numUnitHour THEN 0 ELSE(v_monTempTotalAmount -((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice)) END),PromotionID = v_PromotionID,PromotionDesc = v_PromotionDesc,
                     bitDiscountType = true,fltDiscount =(CASE WHEN(v_fltDiscountValue -v_usedDiscountQty) >= numUnitHour THEN v_monTempTotalAmount ELSE((v_fltDiscountValue -v_usedDiscountQty)*v_monTempPrice) END)
                     WHERE
                     numCartId = v_numTempCartID;
                  ELSE
                     EXIT;
                  end if;
               end if;
               v_k := v_k::bigint+1;
            END LOOP;
         end if;
      end if;
   ELSE
      v_numItemCode := 2;
   end if;

   open SWV_RefCur for
   SELECT v_numItemCode AS ResOutput;
END; $$;


