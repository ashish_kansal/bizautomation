-- Stored procedure definition script USP_GetMaxRows for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMaxRows(v_numUserCntID NUMERIC(9,0) DEFAULT 0,      
v_numDomainId NUMERIC(9,0) DEFAULT 0 ,    
v_Ctype VARCHAR(1) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if((select count(*) from PageLayoutDTL DTL  where DTL.numUserCntID = v_numUserCntID and DTL.numDomainID = v_numDomainId and DTL.Ctype = v_Ctype) <> 0) then

      open SWV_RefCur for
      select max(DTL.intCoulmn) from PageLayoutDTL DTL
      where DTL.numUserCntID = v_numUserCntID and DTL.numDomainID = v_numDomainId and DTL.Ctype = v_Ctype;
   else
      open SWV_RefCur for
      select max(intColumn) from PageLayout  where Ctype = v_Ctype;
   end if;
   RETURN;
END; $$;


