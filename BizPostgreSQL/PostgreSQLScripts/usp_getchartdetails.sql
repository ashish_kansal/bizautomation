-- FUNCTION: public.usp_getchartdetails(numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getchartdetails(numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getchartdetails(
	v_numacntid numeric DEFAULT 0,
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:44:44 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT  CCC.numAccountId,
                CCC.numParntAcntTypeID,
                cast(coalesce(CCC.numOriginalOpeningBal,0) as DECIMAL(20,5)) AS numOriginalOpeningBal,
                cast(coalesce(CCC.numOpeningBal,0) as DECIMAL(20,5)) AS numOpeningBal,
                CCC.dtOpeningDate,
                CCC.bitActive,
                CCC.bitFixed,
                CCC.numDomainId,
                CCC.numListItemID,
                cast(coalesce(CCC.bitDepreciation,false) as BOOLEAN) AS bitDepreciation,
                CCC.monDepreciationCost,
                CCC.dtDepreciationCostDate,
                CCC.bitOpeningBalanceEquity,
                CCC.monEndingOpeningBal,
                CCC.monEndingBal,
                CCC.dtEndStatementDate,
                CCC.chBizDocItems,
                CCC.numAcntTypeId,
                CCC.vcAccountCode,
                CASE WHEN coalesce(CCC.vcNumber,'') <> ''
   THEN coalesce(REPLACE(coalesce(CCC.vcAccountName,''),coalesce(CCC.vcNumber,'') || ' ', 
      ''),'')
   ELSE coalesce(CCC.vcAccountName,'')
   END AS vcAccountName,
                CCC.vcAccountDescription,
                CCC.bitProfitLoss,
                GetAccountTypeCode(CCC.numDomainId,CCC.numAcntTypeId,2::SMALLINT) AS vcAccountTypeCode,
                (GetAccountTypeCode(CCC.numDomainId,CCC.numAcntTypeId,2::SMALLINT) || '~'
   || CAST(CCC.numAcntTypeId AS VARCHAR(10))) AS numAcntTypeId1,
                cast(coalesce(CCC.vcNumber,'') as VARCHAR(50)) AS vcNumber,
                cast(coalesce(CCC.numParentAccId,0) as NUMERIC(18,0)) AS numParentAccId,
                cast(coalesce(CCC.bitIsSubAccount,false) as BOOLEAN) AS bitIsSubAccount,
                (SELECT    COUNT(*)
      FROM      Chart_Of_Accounts COA
      WHERE     COA.numAccountId = C.numParentAccId) AS ChildCount,
                ATD.vcAccountCode || '~'
   || CAST(ATD.numAccountTypeID AS VARCHAR(20)) AS CandidateKey,
                cast(coalesce(CCC.numBankDetailID,0) as NUMERIC(18,0)) AS numBankDetailID,
                cast(coalesce(CCC.IsBankAccount,false) as BOOLEAN) AS IsBankAccount,
                cast(coalesce(CCC.vcStartingCheckNumber,'') as VARCHAR(50)) AS vcStartingCheckNumber,
                cast(coalesce(CCC.vcBankRountingNumber,'') as VARCHAR(50)) AS vcBankRountingNumber,
                cast(coalesce(CCC.IsConnected,false) as BOOLEAN) AS IsConnected,
                cast(BM.vcFIName as VARCHAR(255)) AS vcBankName,
                CCC.vcNumber AS vcBankAccountNumber,
--              CASE WHEN C.[numParntAcntTypeId] IS NULL THEN NULL
--							 ELSE CCC.numOpeningBal
--				END AS [monCompanyBalance],
                CCC.numOpeningBal AS monCompanyBalance,
                CASE WHEN (coalesce(CCC.IsBankAccount,false) = false) THEN 0
   ELSE(SELECT DISTINCT
         cast(coalesce((SELECT 
            cast(monAvailableBalance as NUMERIC(18,0))
            FROM   BankStatementHeader A
            WHERE  A.numBankDetailID = BD.numBankDetailID
            AND dtAvailableBalanceDate =(SELECT 
               cast(dtLedgerBalanceDate as date)
               FROM     BankStatementHeader
               WHERE    numBankDetailID = BD.numBankDetailID
               ORDER BY dtCreatedDate DESC LIMIT 1)
            ORDER BY dtCreatedDate DESC LIMIT 1),0) as NUMERIC(18,0)) AS monAvailableBalance
         FROM    BankDetails BD
         LEFT JOIN BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
         WHERE   BD.numBankDetailID = CCC.numBankDetailID)
   END AS monBankBalance
   FROM    Chart_Of_Accounts CCC
   LEFT JOIN Chart_Of_Accounts C ON CCC.numAccountId = C.numParentAccId
   LEFT JOIN AccountTypeDetail ATD ON CCC.numAcntTypeId = ATD.numAccountTypeID
   LEFT JOIN BankDetails BD ON BD.numBankDetailID = CCC.numBankDetailID
   LEFT JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
   WHERE   CCC.numAccountId = v_numAcntId
   AND CCC.numDomainId = v_numDomainId;
END;
$BODY$;

ALTER FUNCTION public.usp_getchartdetails(numeric, numeric, refcursor)
    OWNER TO postgres;
