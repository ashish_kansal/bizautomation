-- Stored procedure definition script USP_ManagePayrollTracking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePayrollTracking(v_numDomainId NUMERIC(18,0),
    v_numPayrollHeaderID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
    v_strItems TEXT,
    v_tintMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPayrollDetailID  NUMERIC(18,0);

   v_hDocItem  INTEGER;
BEGIN
   IF NOT EXISTS(SELECT numPayrollDetailID FROM PayrollDetail WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID
   AND numUserCntID = v_numUserCntID) then

      INSERT INTO PayrollDetail(numPayrollHeaderID,numDomainID,numUserCntID,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,numCheckStatus)
      SELECT v_numPayrollHeaderID,v_numDomainId,v_numUserCntID,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,0
      FROM UserMaster WHERE numDomainID = v_numDomainId AND numUserDetailId = v_numUserCntID;
   end if;

   select   numPayrollDetailID INTO v_numPayrollDetailID FROM PayrollDetail WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID
   AND numUserCntID = v_numUserCntID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP AS
      SELECT CASE WHEN coalesce(X.numCategoryHDRID,0) = 0 THEN NULL ELSE X.numCategoryHDRID END AS numCategoryHDRID,
		CASE WHEN coalesce(X.numComissionID,0) = 0 THEN NULL ELSE X.numComissionID END AS numComissionID,
		X.monCommissionAmt 
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numCategoryHDRID NUMERIC PATH 'numCategoryHDRID',
				numComissionID NUMERIC PATH 'numComissionID',
				monCommissionAmt DECIMAL(20,5) PATH 'monCommissionAmt'
		) AS X;
					

					
   IF v_tintMode = 1 then --RegularHrs

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM timeandexpense TE
      JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      WHERE TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND (numType = 1 Or numType = 2)  And numCategory = 1);
   ELSEIF v_tintMode = 3
   then --PaidLeaveHrs

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM timeandexpense TE
      JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      WHERE TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numType = 3 And numCategory = 3);
   ELSEIF v_tintMode = 4
   then --PaidInvoice

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numComissionID IN(SELECT  BC.numComissionID FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      JOIN PayrollTracking PT ON PT.numComissionID = BC.numComissionID
      where Opp.numDomainId = v_numDomainId AND BC.numUserCntID = v_numUserCntID
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND BC.bitCommisionPaid = false AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount);
   ELSEIF v_tintMode = 5
   then --UNPaidInvoice

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numComissionID IN(SELECT  BC.numComissionID FROM OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      JOIN PayrollTracking PT ON PT.numComissionID = BC.numComissionID
      where Opp.numDomainId = v_numDomainId AND BC.numUserCntID = v_numUserCntID
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND BC.bitCommisionPaid = false AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount);
   ELSEIF v_tintMode = 6
   then --Expense

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM timeandexpense TE
      JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      WHERE TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numCategory = 2 And numType in(1,2));
   ELSEIF v_tintMode = 7
   then --Reimburse

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM timeandexpense TE
      JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      WHERE TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND bitreimburse = true AND numcategory = 2);
   ELSEIF v_tintMode = 8
   then --Project Commission

      DELETE FROM PayrollTracking WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND numComissionID IN(SELECT  BC.numComissionID FROM ProjectsMaster PM
      INNER JOIN BizDocComission BC on BC.numProId = PM.numProId
      JOIN PayrollTracking PT ON PT.numComissionID = BC.numComissionID
      where PM.numdomainId = v_numDomainId /*AND PM.numProjectStatus=27492*/ AND BC.numUserCntID = v_numUserCntID
      And numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      AND BC.bitCommisionPaid = false);
   end if;		

   INSERT INTO PayrollTracking(numDomainID,numPayrollHeaderID,numPayrollDetailID,numCategoryHDRID,numComissionID,monCommissionAmt)
   SELECT v_numDomainId,v_numPayrollHeaderID,v_numPayrollDetailID,numCategoryHDRID,numComissionID,monCommissionAmt
   FROM tt_TEMP;
		
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   RETURN;
END; $$;


