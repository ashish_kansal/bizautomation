-- Stored procedure definition script USP_GetCashCreditCardMainDetailsId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCashCreditCardMainDetailsId(v_numCashCreditCardId NUMERIC(9,0) DEFAULT 0,          
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);
BEGIN
   v_numJournalId := 0;        
         
   select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numCashCreditCardId = v_numCashCreditCardId And numDomainId = v_numDomainId;           
         
   open SWV_RefCur for Select numTransactionId From General_Journal_Details
   Where numJournalId = v_numJournalId And bitMainCashCredit = true And numDomainId = v_numDomainId;
END; $$;












