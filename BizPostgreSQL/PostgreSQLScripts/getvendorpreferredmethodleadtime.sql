-- Function definition script GetVendorPreferredMethodLeadTime for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetVendorPreferredMethodLeadTime(v_numVendorID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LeadTime  INTEGER DEFAULT NULL;
BEGIN
   IF EXISTS(SELECT * FROM VendorShipmentMethod WHERE numVendorID = v_numVendorID AND coalesce(numWarehouseID,0) = v_numWarehouseID) then
	
      v_LeadTime := coalesce((SELECT  numListValue FROM VendorShipmentMethod WHERE numVendorID = v_numVendorID AND coalesce(numWarehouseID,0) = v_numWarehouseID ORDER BY bitPrimary DESC,bitPreferredMethod DESC LIMIT 1),0);
   ELSE
      v_LeadTime := coalesce((SELECT  numListValue FROM VendorShipmentMethod WHERE numVendorID = v_numVendorID ORDER BY bitPrimary DESC,bitPreferredMethod DESC LIMIT 1),
      0);
   end if;

   RETURN coalesce(v_LeadTime,0);
END; $$;

