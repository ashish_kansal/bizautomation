-- Stored procedure definition script usp_InsertCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertCases(v_numOppId NUMERIC(9,0) DEFAULT 0,
	v_numContactId NUMERIC(9,0) DEFAULT 0,
	v_vcCaseNumber VARCHAR(50) DEFAULT '',
	v_intTargetResolveDate INTEGER DEFAULT 0,
	v_textSubject TEXT DEFAULT '',
	v_vcStatus VARCHAR(50) DEFAULT '',
	v_vcPriority VARCHAR(25) DEFAULT '',
	v_textDesc TEXT DEFAULT '',
	v_textInternalComments TEXT DEFAULT '',
	v_vcReason VARCHAR(250) DEFAULT '',
	v_vcOrigin VARCHAR(50) DEFAULT '',
	v_vcType VARCHAR(50) DEFAULT '',
	v_intOpenResolveDate INTEGER DEFAULT 0,
	v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_bintCreatedDate BIGINT DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_vcAssignTo VARCHAR(50) DEFAULT '', 
	v_numSolnID NUMERIC DEFAULT NULL,
	v_bitSupportKey BOOLEAN DEFAULT NULL
--

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCaseID  NUMERIC;
BEGIN
	--Insertion into Table  
	
   INSERT INTO Cases(numOppId,numContactId,vcCaseNumber,intTargetResolveDate,textSubject,numStatus,numPriority,textDesc,textInternalComments,numReason,numOrigin,numType,numCreatedby,bintCreatedDate,numDomainID,numAssignedTo,tintSupportKeyType)
	VALUES(v_numOppId,v_numContactId,v_vcCaseNumber,v_intTargetResolveDate,v_textSubject,v_vcStatus,v_vcPriority,v_textDesc,v_textInternalComments,v_vcReason,v_vcOrigin,v_vcType,v_numCreatedBy,v_bintCreatedDate,v_numDomainID,v_vcAssignTo, v_bitSupportKey);
	
	
   v_numCaseID := CURRVAL('Cases_seq');
	
   INSERT INTO CaseSolutions(numCaseId,numSolnID) VALUES(v_numCaseID,v_numSolnID);
RETURN;
END; $$;


