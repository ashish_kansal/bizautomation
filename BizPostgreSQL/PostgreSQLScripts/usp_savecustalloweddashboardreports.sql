-- Stored procedure definition script Usp_saveCustAllowedDashBoardReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_saveCustAllowedDashBoardReports(v_strReportIds VARCHAR(2000),
v_GroupId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete  FROM DashboardAllowedReports where  numGrpId = v_GroupId;
   if v_strReportIds <> '' then

      insert into DashboardAllowedReports(numReportId,
numGrpId)
      select x.Id,v_GroupId from(select * from SplitIds(v_strReportIds,',')) x;
   end if;
   RETURN;
END; $$;


