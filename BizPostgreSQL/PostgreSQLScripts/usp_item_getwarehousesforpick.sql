-- Stored procedure definition script USP_Item_GetWareHousesForPick for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetWareHousesForPick(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_vcWarehouseLocation VARCHAR(100)
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS "numTotalRecords"
		,WareHouseItems.numWareHouseItemID AS "numWareHouseItemID"
		,Warehouses.vcWareHouse AS "vcWareHouse"
		,coalesce(WarehouseLocation.vcLocation,'') AS "vcLocation"
		,coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0) AS "numAvailable"
   FROM
   WareHouseItems
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   WareHouseItems.numDomainID = v_numDomainID
   AND WareHouseItems.numItemID = v_numItemCode
   AND (coalesce(v_numWarehouseItemID,0) = 0 OR WareHouseItems.numWareHouseItemID <> v_numWarehouseItemID)
   AND (coalesce(v_numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID = v_numWarehouseID)
   AND (coalesce(v_vcWarehouseLocation,'') = '' OR WarehouseLocation.vcLocation ilike CONCAT('%',v_vcWarehouseLocation,'%'))
   AND coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0) > 0
   ORDER BY
   coalesce(WarehouseLocation.vcLocation,'') ASC;
END; $$;












