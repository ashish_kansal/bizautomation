-- Stored procedure definition script USP_ReportListMaster_RunPrebuildReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportListMaster_RunPrebuildReport(v_numDomainID NUMERIC(18,0),                     
v_numUserCntID NUMERIC(18,0),      
v_ClientTimeZoneOffset INTEGER,      
v_intDefaultReportID INTEGER,      
v_numDashBoardID NUMERIC(18,0),
INOUT SWV_RefCur refcursor DEFAULT NULL,
INOUT SWV_RefCur2 refcursor DEFAULT NULL,
INOUT SWV_RefCur3 refcursor DEFAULT NULL,
INOUT SWV_RefCur4 refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcTimeLine  VARCHAR(50);
   v_vcGroupBy  VARCHAR(50);
   v_vcTeritorry  TEXT;
   v_vcFilterBy  SMALLINT;
   v_vcFilterValue  TEXT;
   v_dtFromDate  DATE;
   v_dtToDate  DATE;
   v_numRecordCount  INTEGER;
   v_tintControlField  INTEGER;
   v_vcDealAmount  TEXT;
   v_tintOppType  INTEGER;
   v_lngPConclAnalysis  TEXT;
   v_vcTeams  TEXT;
   v_vcEmploees  TEXT;
   v_bitTask  TEXT;
   v_tintTotalProgress  INTEGER;
   v_tintMinNumber  INTEGER;
   v_tintMaxNumber  INTEGER;
   v_tintQtyToDisplay  INTEGER;
   v_vcDueDate  TEXT;
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   SWV_RCur3 REFCURSOR;
   SWV_RCur4 REFCURSOR;
BEGIN
   IF v_intDefaultReportID = 1 then --1 - A/R      
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_ARPreBuildReport(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 2
   then -- A/P      
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_APPreBuildReport(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 3
   then -- MONEY IN THE BANK      
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_BankPreBuildReport(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 5
   then -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)      
 
      select   coalesce(vcTimeLine,'Last12Months'), vcGroupBy, vcTeritorry, vcFilterBy, vcFilterValue, dtFromDate, dtToDate, coalesce(numRecordCount,10), coalesce(tintControlField,1) INTO v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_dtFromDate,
      v_dtToDate,v_numRecordCount,v_tintControlField FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;

      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ItemByProfitPreBuildReport(v_numDomainID,v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy::SMALLINT,
      v_vcFilterValue,v_dtFromDate,v_dtToDate,v_numRecordCount,v_tintControlField,SWV_RCur);
   ELSEIF v_intDefaultReportID = 6
   then -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ItemByRevenuePreBuildReport(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 7
   then  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_ItemClassificationProfitPreBuildReport(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 8
   then  -- TOP 10 CUSTOMERS BY PROFIT MARGIN      
 
      select   coalesce(vcTimeLine,'Last12Months'), coalesce(tintControlField,1), coalesce(numRecordCount,10) INTO v_vcTimeLine,v_tintControlField,v_numRecordCount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_tintControlField,v_numRecordCount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 9
   then  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT      
 
      select   coalesce(vcTimeLine,'Last12Months'), coalesce(tintControlField,1), coalesce(numRecordCount,10) INTO v_vcTimeLine,v_tintControlField,v_numRecordCount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_tintControlField,v_numRecordCount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 10
   then  -- ACTION ITEMS & MEETINGS DUE TODAY      
 
      select   vcTimeLine, vcTeritorry, vcFilterBy, vcFilterValue, bitTask INTO v_vcTimeLine,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_bitTask FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_ActionItemPreBuildReport(v_numDomainID,0,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcTeritorry,v_vcFilterBy::SMALLINT,
      v_vcFilterValue,v_bitTask,SWV_RCur);
   ELSEIF v_intDefaultReportID = 11
   then -- YTD VS SAME PERIOD LAST YEAR      
 
  --added 
      select   vcFilterBy INTO v_vcFilterBy from
      ReportDashboard where
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_RPEYTDAndSamePeriodLastYear(v_numDomainID,v_vcFilterBy::SMALLINT,SWV_RCur);
   ELSEIF v_intDefaultReportID = 12
   then  -- ACTION ITEMS & MEETINGS DUE TODAY      
 
      select   vcFilterValue INTO v_vcFilterValue FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear(v_numDomainID,v_ClientTimeZoneOffset,v_vcFilterValue,SWV_RCur);
   ELSEIF v_intDefaultReportID = 13
   then  --  SALES VS EXPENSES (LAST 12 MONTHS)      
 
      select   vcTimeLine, vcGroupBy, vcTeritorry, vcFilterBy, vcFilterValue, dtFromDate, dtToDate INTO v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_dtFromDate,
      v_dtToDate FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_SalesVsExpenseLast12Months(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,
      v_vcFilterBy::SMALLINT,v_vcFilterValue,v_dtFromDate,v_dtToDate,SWV_RCur);
   ELSEIF v_intDefaultReportID = 14
   then  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)      
 
      select   vcDealAmount INTO v_vcDealAmount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_TopSourceOfSalesOrder(v_numDomainID,v_ClientTimeZoneOffset,v_vcDealAmount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 15
   then  --  TOP 10 ITEMS BY PROFIT MARGIN      
 
      PERFORM USP_ReportListMaster_Top10ItemByProfitMargin(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 16
   then  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)      
 
      select   coalesce(numRecordCount,10), coalesce(vcTimeLine,'Last12Months'), vcDealAmount INTO v_numRecordCount,v_vcTimeLine,v_vcDealAmount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10SalesOpportunityByRevenue(v_numDomainID,v_ClientTimeZoneOffset,v_numRecordCount,v_vcTimeLine,v_vcDealAmount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 17
   then  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10SalesOpportunityByTotalProgress(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 18
   then  --  TOP 10 ITEMS RETURNED VS QTY SOLD      
 
      select   coalesce(numRecordCount,10) INTO v_numRecordCount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ItemsByReturnedVsSold(v_numDomainID,v_numRecordCount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 19
   then  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE      
 
      select   vcTimeLine, vcDealAmount, vcTeritorry, tintOppType, vcFilterBy, vcFilterValue, tintTotalProgress, tintMinNumber, tintMaxNumber, tintQtyToDisplay, vcDueDate, dtFromDate, dtToDate INTO v_vcTimeLine,v_vcDealAmount,v_vcTeritorry,v_tintOppType,v_vcFilterBy,v_vcFilterValue,
      v_tintTotalProgress,v_tintMinNumber,v_tintMaxNumber,v_tintQtyToDisplay,
      v_vcDueDate,v_dtFromDate,v_dtToDate FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10SalesOpportunityByPastDue(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcTeritorry,v_tintOppType,
      v_vcDealAmount,v_vcFilterBy::SMALLINT,v_vcFilterValue,v_tintTotalProgress,
      v_tintMinNumber,v_tintMaxNumber,v_tintQtyToDisplay,v_vcDueDate,
      v_dtFromDate,v_dtToDate,SWV_RCur);
   ELSEIF v_intDefaultReportID = 20
   then  --  Top 10 Campaigns by ROI      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10CampaignsByROI(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 21
   then  --  MARKET FRAGMENTATION – TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_TopCustomersByPortionOfTotalSales(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 22
   then  --  SCORE CARD      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_PrebuildScoreCard(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 23
   then  --  TOP 10 LEAD SOURCES (NEW LEADS)      
 
      select   coalesce(vcTimeLine,'Last12Months'), vcGroupBy, vcTeritorry, vcFilterBy, vcFilterValue, dtFromDate, dtToDate, coalesce(numRecordCount,10) INTO v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_dtFromDate,
      v_dtToDate,v_numRecordCount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10LeadSource(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,
      v_vcFilterBy::SMALLINT,v_vcFilterValue,v_dtFromDate,v_dtToDate,v_numRecordCount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 24
   then  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10Email(v_numDomainID,v_numUserCntID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 25
   then  --  Reminders      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_RemindersPreBuildReport(v_numDomainID,v_ClientTimeZoneOffset,SWV_RCur);
   ELSEIF v_intDefaultReportID = 26
   then  --  TOP REASONS WHY WE’RE WINING DEALS      
 
      select   tintOppType, vcDealAmount INTO v_tintOppType,v_vcDealAmount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ReasonsDealWins(v_numDomainID,v_ClientTimeZoneOffset,v_tintOppType,v_vcDealAmount,SWV_RCur);
   ELSEIF v_intDefaultReportID = 27
   then  --  TOP REASONS WHY WE’RE LOOSING DEALS      
 
      select   tintOppType, vcDealAmount, lngPConclAnalysis INTO v_tintOppType,v_vcDealAmount,v_lngPConclAnalysis FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ReasonsDealLost(v_numDomainID,v_ClientTimeZoneOffset,v_tintOppType,v_vcDealAmount,v_lngPConclAnalysis,SWV_RCur);
   ELSEIF v_intDefaultReportID = 28
   then  --  TOP 10 REASONS FOR SALES RETURNS      
 
      select   coalesce(numRecordCount,10) INTO v_numRecordCount FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_Top10ReasonsForSalesReturns(v_numDomainID,v_numDashBoardID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 29
   then  --  EMPLOYEE SALES PERFORMANCE PANEL 1      
 
      select   vcTimeLine, vcFilterBy, vcFilterValue INTO v_vcTimeLine,v_vcFilterBy,v_vcFilterValue FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3,SWV_RefCur4 FROM USP_ReportListMaster_EmployeeSalesPerformancePanel1(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcFilterBy::SMALLINT,
      v_vcFilterValue,SWV_RCur,SWV_RCur2,SWV_RCur3,SWV_RCur4);
   ELSEIF v_intDefaultReportID = 30
   then  --  PARTNER REVENUES, MARGINS & PROFITS (LAST 12 MONTHS)      
 
      SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3 FROM USP_ReportListMaster_PartnerRMPLast12Months(v_numDomainID,SWV_RCur,SWV_RCur2,SWV_RCur3);
   ELSEIF v_intDefaultReportID = 31
   then  --  EMPLOYEE SALES PERFORMANCE PT 1 (LAST 12 MONTHS)      
 
      select   vcTimeLine, vcFilterBy, vcFilterValue INTO v_vcTimeLine,v_vcFilterBy,v_vcFilterValue FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3 FROM USP_ReportListMaster_EmployeeSalesPerformance1(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcFilterBy::SMALLINT,
      v_vcFilterValue,SWV_RCur,SWV_RCur2,SWV_RCur3);
   ELSEIF v_intDefaultReportID = 32
   then  --  EMPLOYEE BENEFIT TO COMPANY (LAST 12 MONTHS)      
 
      select   vcTimeLine, vcFilterBy, vcFilterValue INTO v_vcTimeLine,v_vcFilterBy,v_vcFilterValue FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3 FROM USP_ReportListMaster_EmployeeBenefitToCompany(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcFilterBy::SMALLINT,v_vcFilterValue,SWV_RCur,SWV_RCur2,SWV_RCur3);
   ELSEIF v_intDefaultReportID = 33
   then  --  FIRST 10 SAVED SEARCHES      
 
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_First10SavedSearch(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 34
   then  --  SALES OPPORTUNITY WON/LOST REPORT      
 
      select   vcTimeLine, vcGroupBy, vcTeritorry, vcFilterBy, vcFilterValue, dtFromDate, dtToDate INTO v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_dtFromDate,
      v_dtToDate FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_SalesOppWonLostPreBuildReport(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,
      v_vcFilterBy::SMALLINT,v_vcFilterValue,v_dtFromDate,v_dtToDate,SWV_RCur);
   ELSEIF v_intDefaultReportID = 35
   then  --  SALES OPPORTUNITY PIPELINE      
 
      select   vcTimeLine, vcGroupBy, vcTeritorry, vcFilterBy, vcFilterValue, dtFromDate, dtToDate INTO v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,v_vcFilterBy,v_vcFilterValue,v_dtFromDate,
      v_dtToDate FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_SalesOppPipeLine(v_numDomainID,v_ClientTimeZoneOffset,v_vcTimeLine,v_vcGroupBy,v_vcTeritorry,
      v_vcFilterBy::SMALLINT,v_vcFilterValue,v_dtFromDate,v_dtToDate,SWV_RCur);
   ELSEIF v_intDefaultReportID = 36
   then -- BackOrder Valuation    
  
      select   vcFilterBy, vcFilterValue INTO v_vcFilterBy,v_vcFilterValue FROM
      ReportDashboard WHERE
      numDashBoardID = v_numDashBoardID;
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_TotalBackOrderValue(v_numDomainID,v_vcFilterBy::SMALLINT,v_vcFilterValue,SWV_RCur);
   ELSEIF v_intDefaultReportID = 37
   then -- Inventory Value by the WareHouse    
  
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_WarehouseInventoryValue(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 38
   then -- Month To Date Revenue    
  
      SELECT * INTO SWV_RefCur FROM USP_ReportListMaster_MonthToDateRevenue(v_numDomainID,SWV_RCur);
   ELSEIF v_intDefaultReportID = 39
   then -- Pending Report Details   
  
      SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3 FROM USP_ReportListMaster_PendingOrdersStatus(v_numDomainID,SWV_RCur,SWV_RCur2,SWV_RCur3);
   end if;
   RETURN;
END; $$; 


