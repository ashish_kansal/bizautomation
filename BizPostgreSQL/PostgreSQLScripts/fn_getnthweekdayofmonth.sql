CREATE OR REPLACE FUNCTION fn_GetNthWeekdayOfMonth(v_theDate TIMESTAMP,
    v_theWeekday SMALLINT,
    v_theNth SMALLINT)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN(SELECT  theDate
                FROM    (
                            SELECT '1900-01-01'::TIMESTAMP + make_interval(months => DATEDIFF('month', ('1900-01-01'::TIMESTAMP + make_interval(days => v_theNth)), v_theDate)) + make_interval(days => CAST(7 * v_theNth - 7 * SIGN(SIGN(v_theNth) + 1) + (v_theWeekday + 6 - DATEDIFF('day', '1753-01-01'::TIMESTAMP, ('1900-01-01'::TIMESTAMP + make_interval(months => DATEDIFF('month', ('1900-01-01'::TIMESTAMP + make_interval(days => v_theNth)), v_theDate)))) % 7) % 7 AS INT)) AS theDate
                            WHERE   v_theWeekday BETWEEN 1 AND 7
                                    AND v_theNth IN (-5, -4, -3, -2, -1, 1, 2, 3, 4, 5)
                        ) AS d
                WHERE   DATEDIFF('month', theDate, v_theDate) = 0
);
END; $$;


