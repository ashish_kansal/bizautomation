-- Stored procedure definition script USP_GetSelectedTeadForUsrMst for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSelectedTeadForUsrMst(v_numUserCntId NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numlistItemId,vcdata  from UserTeams
   join Listdetails
   on numlistItemId = numTeam
   where numuserCntid = v_numUserCntId
   and UserTeams.numDomainID = v_numDomainID;
END; $$;
