DROP FUNCTION IF EXISTS USP_ElasticSearch_GetReturns;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetReturns(v_numDomainID NUMERIC(18,0)
	,v_vcReturnHeaderIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numReturnHeaderID AS id
		,'return' AS module
		,CONCAT(CASE WHEN tintReturnType = 1 THEN '<b style="color:#ff0000">RMA (Sales):</b> ' ELSE '<b style="color:#00aa50">RMA (Purchase):</b> ' END,vcRMA,', ',vcCompanyName) AS displaytext
		,vcRMA AS text
		,CONCAT('/opportunity/frmReturnDetail.aspx?ReturnID=',numReturnHeaderID) AS url
		,coalesce(vcRMA,'') AS "Search_vcRMA"
   FROM
   ReturnHeader
   INNER JOIN
   DivisionMaster
   ON
   ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   ReturnHeader.numDomainID = v_numDomainID
   AND ReturnHeader.tintReturnType IN(1,2) -- 1:Sales, 2:Purchase
   AND (NULLIF(v_vcReturnHeaderIds,'') IS NULL OR numReturnHeaderID IN(SELECT id FROM SplitIDs(coalesce(v_vcReturnHeaderIds,''),',')));
END; $$;












