-- Function definition script GetOrderShipReceiveStatusForIndividualItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOrderShipReceiveStatusForIndividualItem(v_numOppID NUMERIC(18,0),
	  v_tintOppType NUMERIC(18,0),
	  v_tintshipped SMALLINT,
	  v_numItemCode NUMERIC(18,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcShippedReceivedStatus  VARCHAR(100) DEFAULT '';

   v_TotalQty  DOUBLE PRECISION DEFAULT 0;
   v_numQtyShipped  DOUBLE PRECISION DEFAULT 0;
   v_numQtyReceived  DOUBLE PRECISION DEFAULT 0;
BEGIN
   IF v_tintOppType = 1 then --SALES ORDER
      v_TotalQty := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID AND I.numItemCode = v_numItemCode),
      0);

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
      IF coalesce(v_tintshipped,0) = 1 then
		
         v_numQtyShipped := v_TotalQty;
      ELSE
         v_numQtyShipped := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID  AND I.numItemCode = v_numItemCode),
         0);
      end if;
      v_vcShippedReceivedStatus :=(CASE WHEN  v_TotalQty = v_numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) || CONCAT(CAST(v_TotalQty AS DOUBLE PRECISION),' / ',CAST(v_numQtyShipped AS DOUBLE PRECISION)) || '</font></b>';
   ELSEIF v_tintOppType = 2
   then --PURCHASE ORDER
      v_TotalQty := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID  AND I.numItemCode = v_numItemCode),
      0);
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
      IF coalesce(v_tintshipped,0) = 1 then
		
         v_numQtyReceived := v_TotalQty;
      ELSE
         v_numQtyReceived := coalesce((SELECT SUM(coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),OI.numItemCode,I.numDomainID,coalesce(OI.numUOMId,0)),1)*numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND coalesce(bitDropship,false) = false AND numOppId = v_numOppID AND I.numItemCode = v_numItemCode),0);
      end if;
      v_vcShippedReceivedStatus :=(CASE WHEN  v_TotalQty = v_numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) || CONCAT(CAST(v_TotalQty AS DOUBLE PRECISION),' / ',CAST(v_numQtyReceived AS DOUBLE PRECISION)) || '</font></b>';
   end if;

   RETURN v_vcShippedReceivedStatus;
END; $$;

