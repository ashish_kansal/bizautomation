-- Stored procedure definition script USP_WareHouseItems_ManageInventoryKitWithinKit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WareHouseItems_ManageInventoryKitWithinKit(v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numOppChildItemID NUMERIC(18,0),
	v_Mode SMALLINT,
	v_tintMode SMALLINT DEFAULT 0, -- 0:Add/Edit 1:Delete
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
	v_tintOppType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_bitAssembly  BOOLEAN;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_onHand  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;
   v_onBackOrder  DOUBLE PRECISION;
   v_onReOrder  DOUBLE PRECISION;
   v_numUnits  DOUBLE PRECISION;
   v_QtyShipped  DOUBLE PRECISION;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_description  VARCHAR(100);
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   DROP TABLE IF EXISTS tt_TABLEKITCHILDITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEKITCHILDITEM
   (
      RowNo INTEGER,
      numOppKitChildItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      bitAssembly BOOLEAN,
      numWarehouseItemID NUMERIC(18,0),
      numUnits DOUBLE PRECISION,
      numQtyShipped DOUBLE PRECISION
   );
   INSERT INTO tt_TABLEKITCHILDITEM(RowNo,
		numOppKitChildItemID,
		numItemCode,
		bitAssembly,
		numWarehouseItemID,
		numUnits,
		numQtyShipped)
   SELECT
   ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
		numOppKitChildItemID,
		numItemID,
		coalesce(bitAssembly,false),
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped
   FROM
   OpportunityKitChildItems
   JOIN
   Item I
   ON
   OpportunityKitChildItems.numItemID = I.numItemCode
   WHERE
   charitemtype = 'P'
   AND coalesce(numWareHouseItemId,0) > 0
   AND numOppID = v_numOppID
   AND numOppItemID = v_numOppItemID
   AND numOppChildItemID = v_numOppChildItemID;
  
   select   COUNT(*) INTO v_COUNT FROM tt_TABLEKITCHILDITEM;

   WHILE v_i <= v_COUNT LOOP
      select   numOppKitChildItemID, numItemCode, coalesce(bitAssembly,false), numWarehouseItemID, coalesce(numUnits,0), coalesce(numQtyShipped,0) INTO v_numOppKitChildItemID,v_numItemCode,v_bitAssembly,v_numWareHouseItemID,
      v_numUnits,v_QtyShipped FROM
      tt_TABLEKITCHILDITEM WHERE
      RowNo = v_i;
      IF v_Mode = 0 then
         v_description := CONCAT('SO CHILD KIT insert/edit (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      ELSEIF v_Mode = 1
      then
         v_description := CONCAT('SO CHILD KIT Deleted (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      ELSEIF v_Mode = 2
      then
         v_description := CONCAT('SO CHILD KIT Close (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      ELSEIF v_Mode = 3
      then
         v_description := CONCAT('SO CHILD KIT Re-Open (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      end if;
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0), coalesce(numReorder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder,v_onReOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      IF v_Mode = 0 then --insert/edit
    	
         v_numUnits := v_numUnits -v_QtyShipped;
         IF v_onHand >= v_numUnits then
			
            v_onHand := v_onHand -v_numUnits;
            v_onAllocation := v_onAllocation+v_numUnits;
         ELSEIF v_onHand < v_numUnits
         then
			
            v_onAllocation := v_onAllocation+v_onHand;
            v_onBackOrder := v_onBackOrder+v_numUnits -v_onHand;
            v_onHand := 0;
         end if;
      ELSEIF v_Mode = 1
      then --Revert
		
         IF v_QtyShipped > 0 then
			
            IF v_tintMode = 0 then
               v_numUnits := v_numUnits -v_QtyShipped;
            ELSEIF v_tintMode = 1
            then
               v_onAllocation := v_onAllocation+v_QtyShipped;
            end if;
         end if;
         IF v_numUnits >= v_onBackOrder then
			
            v_numUnits := v_numUnits -v_onBackOrder;
            v_onBackOrder := 0;
            IF (v_onAllocation -v_numUnits >= 0) then
               v_onAllocation := v_onAllocation -v_numUnits;
            end if;
            v_onHand := v_onHand+v_numUnits;
         ELSEIF v_numUnits < v_onBackOrder
         then
			
            IF (v_onBackOrder -v_numUnits > 0) then
               v_onBackOrder := v_onBackOrder -v_numUnits;
            end if;
         end if;
      ELSEIF v_Mode = 2
      then --Close
		
         v_numUnits := v_numUnits -v_QtyShipped;
         v_onAllocation := v_onAllocation -v_numUnits;
      ELSEIF v_Mode = 3
      then --Re-Open
		
         v_numUnits := v_numUnits -v_QtyShipped;
         v_onAllocation := v_onAllocation+v_numUnits;
      end if;
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
      UPDATE
      WareHouseItems
      SET
      numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
      dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
      v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
      v_numDomainID := v_numDomainID,SWV_RefCur := null);
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


