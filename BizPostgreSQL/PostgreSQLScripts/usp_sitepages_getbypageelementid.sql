-- Stored procedure definition script USP_SitePages_GetByPageElementID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SitePages_GetByPageElementID(v_numDomainID NUMERIC(18,0),
	v_numPageElementID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcPageElement  VARCHAR(500) DEFAULT '';
BEGIN
   select   coalesce(vcTagName,'') INTO v_vcPageElement FROM PageElementMaster WHERE numElementID =  v_numPageElementID;

   open SWV_RefCur for SELECT * FROM
   SitePages
   WHERE
   numTemplateID IN(SELECT
      numTemplateID
      FROM
      SiteTemplates
      WHERE
      numDomainID = v_numDomainID AND
      numSiteID = v_numSiteID AND
      POSITION(LOWER(v_vcPageElement) in LOWER(txtTemplateHTML)) > 0);
END; $$;













