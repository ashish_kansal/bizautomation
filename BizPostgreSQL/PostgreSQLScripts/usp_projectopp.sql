-- Stored procedure definition script USP_ProjectOpp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectOpp(v_byteMode SMALLINT DEFAULT 0,      
	v_domainId NUMERIC(9,0) DEFAULT 0,    
	v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,       
	v_numProjectId  NUMERIC(9,0) DEFAULT 0  ,
	v_numCaseId  NUMERIC(9,0) DEFAULT 0 ,
	v_numStageID	NUMERIC(9,0) DEFAULT 0,
	v_tintOppType Integer DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
	
      open SWV_RefCur for
      SELECT
      numOppId
			,vcpOppName
      FROM
      OpportunityMaster
      WHERE
      tintActive = 1
      AND tintshipped = 0
      AND numDivisionId = v_numDivisionID
      AND numDomainId = v_domainId
      ORDER BY
      numOppId DESC;
   end if;           
      
   IF v_byteMode = 1 then
	
      If (v_numProjectId = 0 and v_numCaseId <> 0) then
		
         open SWV_RefCur for
         SELECT
         numOppId
				,vcpOppName
         FROM
         OpportunityMaster
         WHERE
         numDomainId = v_domainId
         AND numDivisionId = v_numDivisionID
         AND ((tintActive = 1 AND tintshipped = 0) OR numOppId IN(SELECT numoppid from CaseOpportunities where numCaseId = v_numCaseId and numDomainid = v_domainId))
         ORDER BY
         numOppId DESC;
      ELSE
         open SWV_RefCur for
         SELECT
         OM.numOppId
				,0 as numBillID
				,0 as numProjOppID
				,OM.vcpOppName
				,I.vcItemName
				,'Purchase' AS Type
				,(OPP.monPrice*OPP.numUnitHour) AS ExpenseAmount
				,false as bitIntegratedToAcnt
				,0 as numBizDocsPaymentDetailsId
				,COA.vcAccountName as ExpenseAccount
         FROM
         OpportunityMaster OM
         INNER JOIN OpportunityItems OPP ON OM.numOppId = OPP.numOppId
         LEFT OUTER JOIN Item I ON OPP.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId
         WHERE
         OM.numDomainId = v_domainId
         AND OPP.numProjectID = v_numProjectId
         AND (OPP.numProjectStageID = v_numStageID OR v_numStageID = 0)
         AND OM.tintopptype = 2
         UNION ALL
         SELECT
         CAST(0 AS NUMERIC(18,0)) as numOppId,PO.numBillId,PO.numProjOppID,'','','Bill' as Type,
			 BH.monAmountDue AS ExpenseAmount,
			coalesce((SELECT bitIntegrated FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId),false) AS bitIntegratedToAcnt,
			(SELECT numbizdocspaymentdetailsid FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId) AS numBizDocsPaymentDetailsId,
			'' as ExpenseAccount
         from  ProjectsOpportunities PO
         JOIN BillHeader BH ON BH.numBillID = PO.numBillId
         where  PO.numDomainId = v_domainId
         AND PO.numProId = v_numProjectId
         AND (PO.numStageID = v_numStageID OR v_numStageID = 0)
         and coalesce(PO.numBillId,0) > 0
         order by 1 desc;
      end if;
   end if;

   IF v_byteMode = 2 then
	
      open SWV_RefCur for
      select  OB.vcBizDocID,OB.numOppBizDocsId,CASE WHEN NULLIF(OB.vcBizDocName,'') IS NULL THEN OB.vcBizDocID WHEN LENGTH(OB.vcBizDocID) > 0 THEN OB.vcBizDocID ELSE OB.vcBizDocName END AS vcBizDocName
      from OpportunityMaster OM INNER JOIN OpportunityBizDocs OB ON OM.numOppId = OB.numoppid where
      OM.tintopptype = 1 AND
      OB.bitAuthoritativeBizDocs = 1 AND tintActive = 1 and  tintshipped = 0 and numDivisionId = v_numDivisionID and numDomainId = v_domainId order by OB.numoppid desc;
   end if; 

   IF v_byteMode = 3 then --Select only sales/purchase opportunity 
	
      open SWV_RefCur for
      select   numOppId,
				vcpOppName
      from     OpportunityMaster
      where    tintActive = 1
      and tintshipped = 0
      and numDomainId = v_domainId
      AND numDivisionId = v_numDivisionID
      AND tintopptype = v_tintOppType
      order by numOppId desc;
   end if; 

   IF v_byteMode = 4 then --Select only closed orders which are not returned yet.
	
      open SWV_RefCur for
      select   numOppId,
				vcpOppName
      from     OpportunityMaster
      where    tintActive = 1
      and tintshipped = 1
      and numDomainId = v_domainId
      AND numDivisionId = v_numDivisionID
      AND tintopptype = v_tintOppType
				--AND numOppId NOT IN (SELECT numOppId FROM dbo.ReturnHeader WHERE numDomainId=@domainId)
      order by numOppId desc;
   end if; 

   IF v_byteMode = 5 then --Select Fulfillment BizDocs FROM closed orders which are not returned yet.
	
      IF v_tintOppType = 2 then
		
         open SWV_RefCur for
         SELECT
         CONCAT(OpportunityMaster.numOppId,'-0') AS numOppBizDocsId
				,vcpOppName
         FROM
         OpportunityMaster
         WHERE
         tintActive = 1
         and tintshipped = 1
         and numDomainId = v_domainId
         AND numDivisionId = v_numDivisionID
         AND tintopptype = v_tintOppType
         ORDER BY
         OpportunityMaster.numOppId desc;
      ELSE
         open SWV_RefCur for
         SELECT
         CONCAT(OpportunityMaster.numOppId,'-',numOppBizDocsId) AS numOppBizDocsId
				,CONCAT(vcBizDocID,' (',vcpOppName,')') AS vcPOppName
         FROM
         OpportunityMaster
         INNER JOIN
         OpportunityBizDocs
         ON
         OpportunityMaster.numOppId = OpportunityBizDocs.numoppid
         WHERE
         tintActive = 1
         and tintshipped = 1
         and numDomainId = v_domainId
         AND numDivisionId = v_numDivisionID
         AND tintopptype = v_tintOppType
         AND numBizDocId = 296
         ORDER BY
         OpportunityMaster.numOppId desc;
      end if;
   end if;
   RETURN;
END; $$;


