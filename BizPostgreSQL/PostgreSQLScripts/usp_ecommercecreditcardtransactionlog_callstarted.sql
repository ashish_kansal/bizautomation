-- Stored procedure definition script USP_ECommerceCreditCardTransactionLog_CallStarted for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ECommerceCreditCardTransactionLog_CallStarted(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_monAmount DECIMAL(20,5),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO ECommerceCreditCardTransactionLog(numDomainID
		,numOppID
		,monAmount
		,bitNsoftwareCallStarted
		,bitNSoftwareCallCompleted)
	VALUES(v_numDomainID
		,v_numOppID
		,v_monAmount
		,true
		,false);

	
open SWV_RefCur for SELECT CURRVAL('ECommerceCreditCardTransactionLog_seq');
END; $$;












