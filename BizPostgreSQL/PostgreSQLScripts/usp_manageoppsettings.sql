DROP FUNCTION IF EXISTS USP_ManageOppSettings;

CREATE OR REPLACE FUNCTION USP_ManageOppSettings(v_numOppID NUMERIC,                                                                                                                                  
  v_numDomainId NUMERIC(9,0),                                                                                                                                                                                   
  v_bitBillingTerms BOOLEAN,
  v_intBillingDays INTEGER,
  v_bitInterestType BOOLEAN,
  v_fltInterest NUMERIC,
  v_tintTaxOperator SMALLINT,
  v_fltExchangeRate NUMERIC,
  v_dtItemReceivedDate TIMESTAMP,
  v_numUserCntId NUMERIC(9,0),
  v_bitRecur BOOLEAN,
  v_dtStartDate DATE,
  v_dtEndDate DATE,
  v_numFrequency SMALLINT,
  v_vcFrequency VARCHAR(20),
  INOUT v_numRecConfigID NUMERIC(18,0) ,
  v_bitDisable BOOLEAN)
LANGUAGE plpgsql
   AS $$
   DECLARE
  
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
	--SELECT numOppID,numDomainId,bitDiscountType,fltDiscount,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,tintTaxOperator,numDiscountAcntType,fltExchangeRate FROM dbo.OpportunityMaster
	
      UPDATE OpportunityMaster
      SET 
--		bitDiscountType = @bitDiscountType,
--	    fltDiscount = @fltDiscount,
      bitBillingTerms = v_bitBillingTerms,intBillingDays = v_intBillingDays,
	    --bitInterestType = @bitInterestType,
	    --fltInterest = @fltInterest,
      tintTaxOperator = v_tintTaxOperator,
	    --numDiscountAcntType = @numDiscountAcntType,
	    fltExchangeRate = v_fltExchangeRate,
      dtItemReceivedDate = v_dtItemReceivedDate
      WHERE numOppId = v_numOppID
      AND numDomainId = v_numDomainId;
      UPDATE OpportunityBizDocs OBD SET monDealAmount = GetDealAmount(v_numOppID,TIMEZONE('UTC',now()),OBD.numOppBizDocsId)
      WHERE numoppid = v_numOppID;

	-- 1 if user has selected for order recurrence
      IF coalesce(v_bitRecur,false) = true then
	
		-- First Check if receurrece is already created for any invoice in sales order because user should not 
		-- be able to create recurrence on both order and invoice
         IF(SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = v_numOppID AND numType = 2) > 0 then
		
            RAISE EXCEPTION 'RECURRENCE_ALREADY_CREATED_FOR_ORDER';
         ELSE
            PERFORM USP_RecurrenceConfiguration_Insert(v_numDomainId := v_numDomainId,v_numUserCntId := v_numUserCntId,v_numOppID := v_numOppID,
            v_numOppBizDocID := NULL,v_dtStartDate := v_dtStartDate,
            v_dtEndDate := v_dtEndDate,v_numType := 1::SMALLINT,v_vcType := 'Sales Order',
            v_vcFrequency := v_vcFrequency,v_numFrequency := v_numFrequency::SMALLINT);
            v_numRecConfigID := CURRVAL('RecurrenceConfiguration_seq');
            UPDATE OpportunityMaster SET vcRecurrenceType = 'Sales Order' WHERE numOppId = v_numOppID;
         end if;
      ELSEIF coalesce(v_numRecConfigID,0) > 0 AND coalesce(v_bitDisable,false) = true
      then
	
         UPDATE
         RecurrenceConfiguration
         SET
         bitDisabled = false,numDisabledBy = v_numUserCntId,dtDisabledDate = LOCALTIMESTAMP
         WHERE
         numRecConfigID = v_numRecConfigID;
      end if;
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;  
                     



