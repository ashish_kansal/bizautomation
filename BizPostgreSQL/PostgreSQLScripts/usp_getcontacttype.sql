-- Stored procedure definition script USP_GetContactType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactType(v_numUserCntId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(coalesce((SELECT numContactType FROM AdditionalContactsInformation WHERE numContactId = v_numUserCntId),0) as NUMERIC(18,0));
END; $$;












