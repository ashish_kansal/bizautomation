-- Stored procedure definition script USP_SaveGridColumnWidth for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveGridColumnWidth(v_numDomainID NUMERIC(18,0) DEFAULT 0,  
	v_numUserCntID NUMERIC(18,0) DEFAULT 0,
	v_str TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      numFormId NUMERIC(18,0),
      numFieldId NUMERIC(9,0),
      bitCustom BOOLEAN,
      intColumnWidth INTEGER
   );

   INSERT INTO tt_TEMPTABLE(numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth)
   SELECT
   numFormId
		,numFieldId
		,bitCustom
		,CAST(intColumnWidth AS INTEGER)
   FROM
   XMLTABLE
		(
			'NewDataSet/GridColumnWidth'
			PASSING 
				CAST(v_str AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFormId NUMERIC(18,0) PATH 'numFormId',
				numFieldId NUMERIC(18,0) PATH 'numFieldId',
				bitCustom BOOLEAN PATH 'bitCustom',
				intColumnWidth DOUBLE PRECISION PATH 'intColumnWidth'
		) AS X; 

   UPDATE
   DycFormConfigurationDetails DFCD
   SET
   intColumnWidth = temp.intColumnWidth
   FROM
   tt_TEMPTABLE temp
   WHERE(DFCD.numFormID = temp.numFormId
   AND DFCD.numFieldID = temp.numFieldId
   AND coalesce(DFCD.bitCustom,false) = temp.bitCustom) AND(DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.tintPageType = 1);                                                         

   INSERT INTO DycFormConfigurationDetails(numDomainID
		,numUserCntID
		,numFormID
		,numFieldID
		,bitCustom
		,intColumnWidth
		,tintPageType
		,intColumnNum
		,intRowNum)
   SELECT
   v_numDomainID
		,v_numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,1
		,1
		,1
   FROM
   tt_TEMPTABLE
   WHERE
   numFormId = 145
   AND numFieldId NOT IN(SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numFormId = 145);

   INSERT INTO DycFormConfigurationDetails(numDomainID
		,numUserCntID
		,numFormID
		,numFieldID
		,bitCustom
		,intColumnWidth
		,tintPageType
		,intColumnNum
		,intRowNum)
   SELECT
   v_numDomainID
		,v_numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,1
		,1
		,1
   FROM
   tt_TEMPTABLE
   WHERE
   numFormId = 43
   AND numFieldId NOT IN(SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numFormId = 43);

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


