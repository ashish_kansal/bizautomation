-- Function definition script swf_isdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
create or replace FUNCTION swf_isdate(in Str text)
RETURNS INTEGER LANGUAGE plpgsql
 AS $$
 declare vDate date;
BEGIN
 if Str is null then
 return 0;
 end if;
 vDate := cast(Str as date);
 return 1;
 exception when others
 then return 0;
END; $$;

