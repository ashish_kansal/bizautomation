-- Stored procedure definition script usp_GetUserAssign for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetUserAssign(v_numDomainID NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT A.numContactId as numUserId,A.vcFirstName || ' ' || A.vcLastname as vcGivenName
   from UserMaster UM
   join AdditionalContactsInformation A
   on UM.numUserDetailId = A.numContactId
   where UM.numDomainID = v_numDomainID
   union
   select  A.numContactId,A.vcFirstName || ' ' || A.vcLastname || ' - ' || vcCompanyName
   from AdditionalContactsInformation A
   join DivisionMaster D
   on D.numDivisionID = A.numDivisionId
   join ExtarnetAccounts E
   on E.numDivisionID = D.numDivisionID
   join ExtranetAccountsDtl DTL
   on DTL.numExtranetID = E.numExtranetID
   join CompanyInfo C
   on C.numCompanyId = D.numCompanyID
   where A.numDomainID = v_numDomainID
   and D.numDivisionID <>(select cast(numDivisionId as NUMERIC(18,0)) from Domain where numDomainId = v_numDomainID);
END; $$;












