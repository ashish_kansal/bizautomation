DROP FUNCTION IF EXISTS USP_PayrollHeader_Save;

CREATE OR REPLACE FUNCTION USP_PayrollHeader_Save(v_numDomainId NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),
	v_numComPayPeriodID NUMERIC(18,0),
	v_tintMode SMALLINT,
    v_strPayrollDetail TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE

   v_i  INTEGER;
   v_iCount  INTEGER;
   v_j  INTEGER;
   v_jCount  INTEGER;
   v_numUserID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numOppID  NUMERIC(18,0);
   v_numOppBizDocID  NUMERIC(18,0);
   v_numTimeAndExpenseCommissionID  NUMERIC(18,0);
   v_monTimeCommisionTotalAmount  DECIMAL(20,5);
   v_monAmountToPay  DECIMAL(20,5);
   v_numPayrollHeaderID  NUMERIC(18,0);
   v_numComissionID  NUMERIC(18,0);
   v_monCommissionAmount  DECIMAL(20,5);

    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numTempUserCntID  NUMERIC(18,0);
   v_numTempDivisionID  NUMERIC(18,0);
   v_monTempHoursWorked  DECIMAL(20,5);
   v_monTempAdditionalAmt  DECIMAL(20,5);
   v_monTempReimbursableExpense  DECIMAL(20,5);
   v_monTempSalesReturn  DECIMAL(20,5);
   v_monTempOverpayment  DECIMAL(20,5);
   v_monTempCommissionAmount  DECIMAL(20,5);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
DROP TABLE IF EXISTS tt_TEMPTIMEEXPENSECOMMISSION CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTIMEEXPENSECOMMISSION
      (
         ID INTEGER,
         numTimeAndExpenseCommissionID NUMERIC(18,0),
         monTotalAmount DECIMAL(20,5)
      );
      IF NOT EXISTS(SELECT numPayrollHeaderID FROM PayrollHeader WHERE numComPayPeriodID = v_numComPayPeriodID) then
	
         INSERT INTO PayrollHeader(numDomainID
			,numCreatedBy
			,dtCreatedDate
			,numComPayPeriodID)
		VALUES(v_numDomainId
			,v_numUserCntID
			,TIMEZONE('UTC',now())
			,v_numComPayPeriodID);
		
         v_numPayrollHeaderID := CURRVAL('PayrollHeader_seq');
      ELSE
         UPDATE PayrollHeader SET dtModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numComPayPeriodID = v_numComPayPeriodID;
         select   numPayrollHeaderID INTO v_numPayrollHeaderID FROM
         PayrollHeader WHERE
         numComPayPeriodID = v_numComPayPeriodID;
      end if;
      IF v_tintMode = 1 then -- Pay Hours Worked
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPEmployeeTime_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPEMPLOYEETIME CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPEMPLOYEETIME
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numUserCntID NUMERIC(18,0),
            monAmountToPay DECIMAL(20,5),
            monAdditionalAmtToPay DECIMAL(20,5)
         );
  
         INSERT INTO tt_TEMPEMPLOYEETIME(numUserCntID
			,monAmountToPay
			,monAdditionalAmtToPay)
         SELECT
         numUserCntID
			,coalesce(monTimeAmtToPay,0)
			,coalesce(monAdditionalAmtToPay,0)
         FROM
		 XMLTABLE
		(
			'NewDataSet/Employee'
			PASSING 
				CAST(v_strPayrollDetail AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numUserCntID NUMERIC(18,0) PATH 'numUserCntID',
				monTimeAmtToPay DECIMAL(20,5) PATH 'monTimeAmtToPay',
				monAdditionalAmtToPay DECIMAL(20,5) PATH 'monAdditionalAmtToPay'
		) X;

		-- FIRST UPDATE EIXSTING RECORDS
         UPDATE
         PayrollDetail PD
         SET
         monHourlyAmt = TET.monAmountToPay,monAdditionalAmt = TET.monAdditionalAmtToPay
         FROM
         tt_TEMPEMPLOYEETIME TET
         WHERE
         PD.numUserCntID = TET.numUserCntID AND PD.numPayrollHeaderID = v_numPayrollHeaderID;

		-- INSERT NEW RECORDS
         INSERT INTO PayrollDetail(numPayrollHeaderID
			,numUserCntID
			,monHourlyAmt
			,monAdditionalAmt)
         SELECT
         v_numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
			,TET.monAdditionalAmtToPay
         FROM
         tt_TEMPEMPLOYEETIME TET
         LEFT JOIN
         PayrollDetail PD
         ON
         PD.numPayrollHeaderID = v_numPayrollHeaderID
         AND TET.numUserCntID = PD.numUserCntID
         WHERE
         PD.numPayrollDetailID IS NULL;
         v_i := 1;
         SELECT COUNT(*) INTO v_iCount FROM tt_TEMPEMPLOYEETIME;
         WHILE v_i <= v_iCount LOOP
            select   numUserCntID, monAmountToPay INTO v_numUserID,v_monAmountToPay FROM
            tt_TEMPEMPLOYEETIME WHERE
            ID = v_i;

			-- FIRST SET COMMISSION AS NOT PAID
            UPDATE
            TimeAndExpenseCommission
            SET
            bitCommissionPaid = false,monAmountPaid = 0
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID
            AND (numType = 1 OR numType = 2)
            AND numCategory = 1;
            UPDATE
            StagePercentageDetailsTaskCommission
            SET
            bitCommissionPaid = false,monAmountPaid = 0
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID;
            DELETE FROM tt_TEMPTIMEEXPENSECOMMISSION;
            INSERT INTO tt_TEMPTIMEEXPENSECOMMISSION(ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount)
            SELECT
            ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,coalesce(monTotalAmount,0)
            FROM
            TimeAndExpenseCommission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID
            AND (numType = 1 OR numType = 2)
            AND numCategory = 1
            ORDER BY
            ID;
            v_j := 1;
            SELECT COUNT(*) INTO v_jCount FROM tt_TEMPTIMEEXPENSECOMMISSION;
            WHILE v_j <= v_jCount AND v_monAmountToPay > 0 LOOP
               select   numTimeAndExpenseCommissionID, monTotalAmount INTO v_numTimeAndExpenseCommissionID,v_monTimeCommisionTotalAmount FROM
               tt_TEMPTIMEEXPENSECOMMISSION WHERE
               ID = v_j;
               IF v_monTimeCommisionTotalAmount >= v_monAmountToPay then
				
                  UPDATE TimeAndExpenseCommission SET bitCommissionPaid = true,monAmountPaid = v_monAmountToPay WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := 0;
               ELSE
                  UPDATE TimeAndExpenseCommission SET bitCommissionPaid = true,monAmountPaid = v_monTimeCommisionTotalAmount WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := v_monAmountToPay -v_monTimeCommisionTotalAmount;
               end if;
               v_j := v_j::bigint+1;
            END LOOP;
            DELETE FROM tt_TEMPTIMEEXPENSECOMMISSION;
            INSERT INTO tt_TEMPTIMEEXPENSECOMMISSION(ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount)
            SELECT
            ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,coalesce(monTotalAmount,0)
            FROM
            StagePercentageDetailsTaskCommission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID
            ORDER BY
            ID;
            v_j := 1;
            SELECT COUNT(*) INTO v_jCount FROM tt_TEMPTIMEEXPENSECOMMISSION;
            WHILE v_j <= v_jCount AND v_monAmountToPay > 0 LOOP
               select   numTimeAndExpenseCommissionID, monTotalAmount INTO v_numTimeAndExpenseCommissionID,v_monTimeCommisionTotalAmount FROM
               tt_TEMPTIMEEXPENSECOMMISSION WHERE
               ID = v_j;
               IF v_monTimeCommisionTotalAmount >= v_monAmountToPay then
				
                  UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid = true,monAmountPaid = v_monAmountToPay WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := 0;
               ELSE
                  UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid = true,monAmountPaid = v_monTimeCommisionTotalAmount WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := v_monAmountToPay -v_monTimeCommisionTotalAmount;
               end if;
               v_j := v_j::bigint+1;
            END LOOP;
            IF v_monAmountToPay > 0 then
			
               RAISE EXCEPTION 'INVALID_HOURS_WORKED_PAYMENT';
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      ELSEIF v_tintMode = 2
      then -- Pay reimbursable expense
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPEmployeeReimburseExpense_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPEMPLOYEEREIMBURSEEXPENSE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPEMPLOYEEREIMBURSEEXPENSE
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numUserCntID NUMERIC(18,0),
            monAmountToPay DECIMAL(20,5)
         );

         INSERT INTO tt_TEMPEMPLOYEEREIMBURSEEXPENSE(numUserCntID
			,monAmountToPay)
         SELECT
         numUserCntID
			,coalesce(monReimbursableExpenseToPay,0)
         FROM
		XMLTABLE
		(
			'NewDataSet/Employee'
			PASSING 
				CAST(v_strPayrollDetail AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numUserCntID NUMERIC(18,0) PATH 'numUserCntID',
				monTimeAmtToPay DECIMAL(20,5) PATH 'monReimbursableExpenseToPay'
		) X;

		-- FIRST UPDATE EIXSTING RECORDS
         UPDATE
         PayrollDetail PD
         SET
         monReimbursableExpenses = TET.monAmountToPay
         FROM
         tt_TEMPEMPLOYEEREIMBURSEEXPENSE TET
         WHERE
         PD.numUserCntID = TET.numUserCntID AND PD.numPayrollHeaderID = v_numPayrollHeaderID;

		-- INSERT NEW RECORDS
         INSERT INTO PayrollDetail(numPayrollHeaderID
			,numUserCntID
			,monReimbursableExpenses)
         SELECT
         v_numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
         FROM
         tt_TEMPEMPLOYEEREIMBURSEEXPENSE TET
         LEFT JOIN
         PayrollDetail PD
         ON
         PD.numPayrollHeaderID = v_numPayrollHeaderID
         AND TET.numUserCntID = PD.numUserCntID
         WHERE
         PD.numPayrollDetailID IS NULL;
         v_i := 1;
         SELECT COUNT(*) INTO v_iCount FROM tt_TEMPEMPLOYEEREIMBURSEEXPENSE;
         WHILE v_i <= v_iCount LOOP
            select   numUserCntID, monAmountToPay INTO v_numUserID,v_monAmountToPay FROM
            tt_TEMPEMPLOYEEREIMBURSEEXPENSE WHERE
            ID = v_i;

			-- FIRST SET EXPENSE AS NOT PAID
            UPDATE
            TimeAndExpenseCommission
            SET
            bitCommissionPaid = false,monAmountPaid = 0
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID
            AND numCategory = 2
            AND bitReimburse = true;
            DELETE FROM tt_TEMPTIMEEXPENSECOMMISSION;
            INSERT INTO tt_TEMPTIMEEXPENSECOMMISSION(ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount)
            SELECT
            ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,coalesce(monTotalAmount,0)
            FROM
            TimeAndExpenseCommission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numUserID
            AND numCategory = 2
            AND bitReimburse = true
            ORDER BY
            ID;
            v_j := 1;
            SELECT COUNT(*) INTO v_jCount FROM tt_TEMPTIMEEXPENSECOMMISSION;
            WHILE v_j <= v_jCount AND v_monAmountToPay > 0 LOOP
               select   numTimeAndExpenseCommissionID, monTotalAmount INTO v_numTimeAndExpenseCommissionID,v_monTimeCommisionTotalAmount FROM
               tt_TEMPTIMEEXPENSECOMMISSION WHERE
               ID = v_j;
               IF v_monTimeCommisionTotalAmount >= v_monAmountToPay then
				
                  UPDATE TimeAndExpenseCommission SET bitCommissionPaid = true,monAmountPaid = v_monAmountToPay WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := 0;
               ELSE
                  UPDATE TimeAndExpenseCommission SET bitCommissionPaid = true,monAmountPaid = v_monTimeCommisionTotalAmount WHERE ID = v_numTimeAndExpenseCommissionID;
                  v_monAmountToPay := v_monAmountToPay -v_monTimeCommisionTotalAmount;
               end if;
               v_j := v_j::bigint+1;
            END LOOP;
            IF v_monAmountToPay > 0 then
			
               RAISE EXCEPTION 'INVALID_REIMBURSABLE_EXPENSE_PAYMENT';
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      ELSEIF v_tintMode = 3
      then -- pay remaining amount (commission)
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPCommission_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPCOMMISSION CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPCOMMISSION
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numUserCntID NUMERIC(18,0),
            numDivisionID NUMERIC(18,0),
            numOppID NUMERIC(18,0),
            numOppBizDocID NUMERIC(18,0),
            monCommissionPaid DECIMAL(20,5)
         );
         DROP TABLE IF EXISTS tt_TEMPBIZDOCCOMMISSION CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPBIZDOCCOMMISSION
         (
            ID INTEGER,
            numComissionID NUMERIC(18,0),
            numComissionAmount DECIMAL(20,5)
         );
     
         INSERT INTO tt_TEMPCOMMISSION(numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid)
         SELECT
         numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid
         FROM
		 XMLTABLE
		(
			'NewDataSet/Employee'
			PASSING 
				CAST(v_strPayrollDetail AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numUserCntID NUMERIC(18,0) PATH 'numUserCntID',
				numDivisionID NUMERIC(18,0) PATH 'numDivisionID',
				numOppID NUMERIC(18,0) PATH 'numOppID',
				numOppBizDocID NUMERIC(18,0) PATH 'numOppBizDocID',
				monCommissionPaid DECIMAL(20,5) PATH 'monCommissionPaid'
		) X;

		-- FIRST UPDATE EIXSTING RECORDS
         UPDATE
         PayrollDetail PD
         SET
         monCommissionAmt = coalesce(TEMP.monCommissionToPay,0)
         FROM(SELECT
         SUM(monCommissionPaid) AS monCommissionToPay
         FROM
         tt_TEMPCOMMISSION T1
         WHERE
         coalesce(T1.numUserCntID,0) = coalesce(PD.numUserCntID,0)
         AND coalesce(T1.numDivisionID,0) = coalesce(PD.numDivisionID,0)) TEMP
         WHERE
         PD.numPayrollHeaderID = v_numPayrollHeaderID;

		-- INSERT NEW RECORDS
         INSERT INTO PayrollDetail(numPayrollHeaderID
			,numUserCntID
			,numDivisionID
			,monCommissionAmt)
         SELECT
         v_numPayrollHeaderID
			,coalesce(TEMP.numUserCntID,0)
			,coalesce(TEMP.numDivisionID,0)
			,monCommissionToPay
         FROM(SELECT
            numUserCntID
				,numDivisionID
				,SUM(monCommissionPaid) AS monCommissionToPay
            FROM
            tt_TEMPCOMMISSION T1
            GROUP BY
            numUserCntID,numDivisionID) TEMP
         LEFT JOIN
         PayrollDetail PD
         ON
         PD.numPayrollHeaderID = v_numPayrollHeaderID
         AND coalesce(TEMP.numUserCntID,0) = coalesce(PD.numUserCntID,0)
         AND coalesce(TEMP.numDivisionID,0) = coalesce(PD.numDivisionID,0)
         WHERE
         PD.numPayrollDetailID IS NULL;
         v_i := 1;
         SELECT COUNT(*) INTO v_iCount FROM tt_TEMPCOMMISSION;
         WHILE v_i <= v_iCount LOOP
            select   coalesce(numUserCntID,0), coalesce(numDivisionID,0), coalesce(numOppID,0), coalesce(numOppBizDocID,0), coalesce(monCommissionPaid,0) INTO v_numUserID,v_numDivisionID,v_numOppID,v_numOppBizDocID,v_monAmountToPay FROM
            tt_TEMPCOMMISSION WHERE
            ID = v_i;

			-- FIRST SET COMMISSION AS NOT PAID
            UPDATE
            BizDocComission
            SET
            bitCommisionPaid = false,monCommissionPaid = 0
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1 =(CASE
            WHEN tintAssignTo = 3
            THEN(CASE WHEN coalesce(numUserCntID,0) = v_numDivisionID THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(numUserCntID,0) = v_numUserID THEN 1 ELSE 0 END)
            END)
            AND coalesce(numOppID,0) = v_numOppID
            AND coalesce(numOppBizDocId,0) = v_numOppBizDocID;
            DELETE FROM tt_TEMPBIZDOCCOMMISSION;
            INSERT INTO tt_TEMPBIZDOCCOMMISSION(ID
				,numComissionID
				,numComissionAmount)
            SELECT
            ROW_NUMBER() OVER(ORDER BY numComissionID)
				,numComissionID
				,coalesce(numComissionAmount,0)
            FROM
            BizDocComission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1 =(CASE
            WHEN tintAssignTo = 3
            THEN(CASE WHEN coalesce(numUserCntID,0) = v_numDivisionID THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(numUserCntID,0) = v_numUserID THEN 1 ELSE 0 END)
            END)
            AND coalesce(numOppID,0) = v_numOppID
            AND coalesce(numOppBizDocId,0) = v_numOppBizDocID
            ORDER BY
            numComissionID;
            v_j := 1;
            SELECT COUNT(*) INTO v_jCount FROM tt_TEMPBIZDOCCOMMISSION;
            WHILE v_j <= v_jCount AND v_monAmountToPay > 0 LOOP
               select   numComissionID, numComissionAmount INTO v_numComissionID,v_monCommissionAmount FROM
               tt_TEMPBIZDOCCOMMISSION WHERE
               ID = v_j;
               IF v_monCommissionAmount >= v_monAmountToPay then
				
                  UPDATE BizDocComission SET bitCommisionPaid = true,monCommissionPaid = v_monAmountToPay WHERE numComissionID = v_numComissionID;
                  v_monAmountToPay := 0;
               ELSE
                  UPDATE BizDocComission SET bitCommisionPaid = true,monCommissionPaid = v_monCommissionAmount WHERE numComissionID = v_numComissionID;
                  v_monAmountToPay := v_monAmountToPay -v_monCommissionAmount;
               end if;
               v_j := v_j::bigint+1;
            END LOOP;
            IF v_monAmountToPay > 0 then
			
               RAISE EXCEPTION 'INVALID_COMMISSION_PAYMENT';
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      ELSEIF v_tintMode = 4
      then -- pay remaining amount (All e.g Hours worked, reimbursable, overpayment, commission)
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPCommissionPay_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPCOMMISSIONPAY CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONPAY
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numUserCntID NUMERIC(18,0),
            numDivisionID NUMERIC(18,0),
            monHoursWorked DECIMAL(20,5),
            monAdditionalAmt DECIMAL(20,5),
            monReimbursableExpense DECIMAL(20,5),
            monSalesReturn DECIMAL(20,5),
            monOverpayment DECIMAL(20,5),
            monCommissionAmount DECIMAL(20,5)
         );
    
         INSERT INTO tt_TEMPCOMMISSIONPAY(numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount)
         SELECT
         numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount
         FROM
		  XMLTABLE
			(
				'NewDataSet/Employee'
				PASSING 
					CAST(v_strPayrollDetail AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numUserCntID NUMERIC(18,0) PATH 'numUserCntID',
					numDivisionID NUMERIC(18,0) PATH 'numDivisionID',
					monHoursWorked DECIMAL(20,5) PATH 'monHoursWorked',
					monAdditionalAmt DECIMAL(20,5) PATH 'monAdditionalAmt',
					monReimbursableExpense DECIMAL(20,5) PATH 'monReimbursableExpense',
					monSalesReturn DECIMAL(20,5) PATH 'monSalesReturn',
					monOverpayment DECIMAL(20,5) PATH 'monOverpayment',
					monCommissionAmount DECIMAL(20,5) PATH 'monCommissionAmount'
			) X;

         v_i := 1;
         SELECT COUNT(*) INTO v_iCount FROM tt_TEMPCOMMISSIONPAY;
         WHILE v_i <= v_iCount LOOP
            select   coalesce(numUserCntID,0), coalesce(numDivisionID,0), coalesce(monHoursWorked,0), coalesce(monAdditionalAmt,0), coalesce(monReimbursableExpense,0), coalesce(monSalesReturn,0), coalesce(monOverpayment,0), coalesce(monCommissionAmount,0) INTO v_numTempUserCntID,v_numTempDivisionID,v_monTempHoursWorked,v_monTempAdditionalAmt,
            v_monTempReimbursableExpense,v_monTempSalesReturn,v_monTempOverpayment,
            v_monTempCommissionAmount FROM
            tt_TEMPCOMMISSIONPAY WHERE
            ID = v_i;
            IF v_numTempUserCntID > 0 then
			
               IF coalesce((SELECT
               SUM(monTotalAmount)
               FROM
               TimeAndExpenseCommission
               WHERE
               numComPayPeriodID = v_numComPayPeriodID
               AND numUserCntID = v_numTempUserCntID
               AND (numType = 1 OR numType = 2)
               AND numCategory = 1),0) <> v_monTempHoursWorked then
				
                  RAISE EXCEPTION 'INVALID_HOURS_WORKED_PAYMENT';
               end if;
               IF coalesce((SELECT
               SUM(monTotalAmount)
               FROM
               TimeAndExpenseCommission
               WHERE
               numComPayPeriodID = v_numComPayPeriodID
               AND numUserCntID = v_numTempUserCntID
               AND numCategory = 2
               AND bitReimburse = true),0) <> v_monTempReimbursableExpense then
				
                  RAISE EXCEPTION 'INVALID_REIMBURSABLE_EXPENSE_PAYMENT';
               end if;
            end if;
            IF coalesce((SELECT
            SUM(monCommissionReversed)
            FROM
            SalesReturnCommission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1  =(CASE
            WHEN v_numTempDivisionID > 0
            THEN(CASE WHEN numUserCntID = v_numTempDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
            ELSE(CASE WHEN numUserCntID = v_numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
            END)),
            0) <> v_monTempSalesReturn then
			
               RAISE EXCEPTION 'INVALID_SALES_RETURN_PAYMENT';
            end if;
            IF coalesce((SELECT
            SUM(monDifference)
            FROM
            BizDocComissionPaymentDifference BDCPD
            INNER JOIN
            BizDocComission BDC
            ON
            BDCPD.numComissionID = BDC.numComissionID
            WHERE
            BDC.numDomainId = v_numDomainId
			AND  1  =(CASE
				WHEN v_numTempDivisionID > 0
				THEN(CASE WHEN numUserCntID = v_numTempDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
				ELSE(CASE WHEN numUserCntID = v_numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
				END)
            AND (coalesce(bitDifferencePaid,false) = false  OR coalesce(BDCPD.numComPayPeriodID,0) = v_numComPayPeriodID)),0) <> v_monTempOverpayment then
               RAISE EXCEPTION 'INVALID_OVER_PAYMENT';
            end if;
            
            IF coalesce((SELECT
            SUM(numComissionAmount)
            FROM
            BizDocComission
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1  =(CASE
            WHEN v_numTempDivisionID > 0
            THEN(CASE WHEN numUserCntID = v_numTempDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
            ELSE(CASE WHEN numUserCntID = v_numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
            END)),0) <> v_monTempCommissionAmount then
			
               RAISE EXCEPTION 'INVALID_COMMISSION_PAYMENT';
            end if;
            IF EXISTS(SELECT numPayrollDetailID FROM PayrollDetail WHERE numPayrollHeaderID = v_numPayrollHeaderID AND coalesce(numUserCntID,0) = v_numTempUserCntID AND coalesce(numDivisionID,0) = v_numTempDivisionID) then
			
               UPDATE
               PayrollDetail
               SET
               monHourlyAmt = v_monTempHoursWorked,monAdditionalAmt = v_monTempAdditionalAmt,
               monReimbursableExpenses = v_monTempReimbursableExpense,monSalesReturn = v_monTempSalesReturn,
               monOverPayment = v_monTempOverpayment,monCommissionAmt = v_monTempCommissionAmount
               WHERE
               numPayrollHeaderID = v_numPayrollHeaderID
               AND coalesce(numUserCntID,0) = v_numTempUserCntID
               AND coalesce(numDivisionID,0) = v_numTempDivisionID;
            ELSE
               INSERT INTO PayrollDetail(numDomainID
					,numPayrollHeaderID
					,numUserCntID
					,numDivisionID
					,monHourlyAmt
					,monAdditionalAmt
					,monReimbursableExpenses
					,monSalesReturn
					,monOverPayment
					,monCommissionAmt)
				VALUES(v_numDomainId
					,v_numPayrollHeaderID
					,v_numTempUserCntID
					,v_numTempDivisionID
					,v_monTempHoursWorked
					,v_monTempAdditionalAmt
					,v_monTempReimbursableExpense
					,v_monTempSalesReturn
					,v_monTempOverpayment
					,v_monTempCommissionAmount);
            end if;
            UPDATE
            TimeAndExpenseCommission
            SET
            bitCommissionPaid = true,monAmountPaid = monTotalAmount
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND numUserCntID = v_numTempUserCntID;
            UPDATE
            SalesReturnCommission
            SET
            bitCommissionReversed = true
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1  =(CASE
            WHEN v_numTempDivisionID > 0
            THEN(CASE WHEN numUserCntID = v_numTempDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
            ELSE(CASE WHEN numUserCntID = v_numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
            END);
            UPDATE
            BizDocComissionPaymentDifference
            SET
            bitDifferencePaid = true,numComPayPeriodID = v_numComPayPeriodID
            WHERE
            coalesce(bitDifferencePaid,false) = false;
            UPDATE
            BizDocComission
            SET
            bitCommisionPaid = true,monCommissionPaid = numComissionAmount
            WHERE
            numComPayPeriodID = v_numComPayPeriodID
            AND 1  =(CASE
            WHEN v_numTempDivisionID > 0
            THEN(CASE WHEN numUserCntID = v_numTempDivisionID AND tintAssignTo = 3 THEN 1 ELSE 0 END)
            ELSE(CASE WHEN numUserCntID = v_numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
            END);
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
      UPDATE
      PayrollDetail
      SET
      monTotalAmt = coalesce(monHourlyAmt,0)+coalesce(monAdditionalAmt,0)+coalesce(monReimbursableExpenses,0)+coalesce(monCommissionAmt,0)+coalesce(monOverPayment,0) -coalesce(monSalesReturn,0)
      WHERE
      numPayrollHeaderID = v_numPayrollHeaderID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


