DROP FUNCTION IF EXISTS USP_WFConditionQueryExecute;

CREATE OR REPLACE FUNCTION USP_WFConditionQueryExecute(v_numDomainID NUMERIC(18,0),
    v_numRecordID NUMERIC(18,0),
    v_textQuery TEXT,
    INOUT v_boolCondition BOOLEAN ,
    v_tintMode SMALLINT)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalRecords  INTEGER;
   v_numOppType  SMALLINT;
   v_tintOppStatus  SMALLINT;
   v_numStatus  NUMERIC(18,0);
BEGIN
	v_textQuery := REPLACE(v_textQuery,'$1',v_numDomainID::VARCHAR);
	v_textQuery := REPLACE(v_textQuery,'$2',v_numRecordID::VARCHAR);

	IF v_tintMode = 1 THEN --Condition Check
		INSERT INTO ExceptionLog
		(
			vcMessage
			,numDomainID
			,numUserContactID
			,dtCreatedDate
			,numCaseID
			,vcURL
			,vcReferrer
			,vcBrowser
			,vcBrowserAgent
		)
		VALUES
		(
			v_textQuery
		   ,v_numDomainID
		   ,0
		   ,TIMEZONE('UTC',now())
		   ,0
		   ,''
		   ,''
		   ,''
		   ,''
		);

		EXECUTE v_textQuery INTO v_TotalRecords;

		v_boolCondition := (CASE WHEN v_TotalRecords = 0 THEN false ELSE true END);
	ELSEIF v_tintMode = 2 THEN
		EXECUTE v_textQuery USING v_numDomainID v_numRecordID;

		If POSITION('OpportunityMaster' IN v_textQuery) > 0 AND POSITION('numStatus' IN v_textQuery) > 0 THEN
			SELECT 
				tintopptype,tintoppstatus,v_numStatus INTO v_numOppType,v_tintOppStatus,v_numStatus 
			FROM
				OpportunityMaster 
			WHERE
				numDomainId = v_numDomainID
				AND numOppId = v_numRecordID;

			IF v_numOppType = 1 AND v_tintOppStatus = 1 then
				PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,0,v_numRecordID,v_numStatus);
			end if;
      end if;
   end if;
   RETURN;
END; $$;


