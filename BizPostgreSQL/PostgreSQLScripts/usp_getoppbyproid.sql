-- Stored procedure definition script USP_GetOppByProId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOppByProId(v_numProId NUMERIC,  
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(P.numOppId as VARCHAR(255)) ,M.vcpOppName
   from ProjectsOpportunities P
   join OpportunityMaster M
   on P.numOppId = M.numOppId
   where P.numProId = v_numProId and P.numDomainId = v_numDomainId  and tintopptype = 1;
END; $$;












