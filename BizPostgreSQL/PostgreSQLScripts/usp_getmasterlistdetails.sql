CREATE OR REPLACE FUNCTION usp_GetMasterListDetails(     
	v_numListID NUMERIC(9,0) DEFAULT 0,  
	v_vcItemType CHAR(3) DEFAULT 'L',  
	v_numDomainID NUMERIC(9,0) DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF v_vcItemType = 'L' OR v_vcItemType = 'LI' then
		open SWV_RefCur for
		SELECT 
			vcData AS "vcItemName"
			,numListItemID AS "numItemID"
			,'L' As "vcItemType"
			,constFlag As flagConst 
		FROM
			Listdetails 
		WHERE 
			numListID = v_numListID 
			AND (numDomainid = v_numDomainID or constFlag = true) 
			AND vcData <> '0';
	end if;  
	IF v_vcItemType = 'T' then
		open SWV_RefCur for
		SELECT 
			vcTerName AS "vcItemName"
			,numTerID AS "numItemID"
			,'T' As "vcItemType"
			,false As "flagConst" 
		FROM 
			TerritoryMaster 
		WHERE 
			numDomainid = v_numDomainID;
	end if;

	RETURN;
END; $$;


