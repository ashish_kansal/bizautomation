CREATE OR REPLACE FUNCTION USP_TicklerActItemsButtonCount
(
	v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                                        
	v_numDomainID NUMERIC(9,0) DEFAULT null,                                                                        
	v_startDate TIMESTAMP DEFAULT NULL,                                                                        
	v_endDate TIMESTAMP DEFAULT NULL,                                                                    
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numViewID INTEGER DEFAULT NULL,
	INOUT v_numTotalRecords NUMERIC(18,0) DEFAULT 0 ,
	v_TypeId INTEGER DEFAULT 0
)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF(v_TypeId = 0) then
      SELECT * INTO v_numTotalRecords FROM USP_TicklerActItemsV2TopCount(v_numUserCntID := v_numUserCntID,v_numDomainID := v_numDomainID,v_startDate := v_startDate,
      v_endDate := v_endDate,v_ClientTimeZoneOffset := v_ClientTimeZoneOffset,
      v_numViewID := v_numViewID,v_numTotalRecords := v_numTotalRecords);
   end if;
   IF(v_TypeId = 1) then
      SELECT * INTO v_numTotalRecords FROM USP_TicklerActItemsV2TopPurchaseRecordCount(v_numUserCntID := v_numUserCntID,v_numDomainID := v_numDomainID,v_startDate := v_startDate,
      v_endDate := v_endDate,v_ClientTimeZoneOffset := v_ClientTimeZoneOffset,
      v_numViewID := v_numViewID,v_numTotalRecords := v_numTotalRecords);
   end if;
   RETURN;
END; $$;


