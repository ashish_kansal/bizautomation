-- Stored procedure definition script Select_Action_Template__UI_Data for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Select_Action_Template__UI_Data(INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN


-- get status or Priority list data
   open SWV_RefCur for
   SELECT  vcData , numListItemID FROM  Listdetails WHERE numListID = 33;

-- get activity data
   open SWV_RefCur2 for
   SELECT  vcData , numListItemID FROM  Listdetails WHERE numListID = 32;
   RETURN;
END; $$;


