-- Stored procedure definition script USP_ItemTaxTypes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemTaxTypes(v_numItemCode NUMERIC(9,0),
v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   TaxItems.numTaxItemID,
		vcTaxName,
		coalesce(bitApplicable,false) AS bitApplicable
   FROM
   TaxItems
   LEFT JOIN
   ItemTax
   ON
   ItemTax.numTaxItemID = TaxItems.numTaxItemID
   AND numItemCode = v_numItemCode
   WHERE
   numDomainID = v_numDomainID
   ORDER BY
   TaxItems.numTaxItemID;
END; $$;
