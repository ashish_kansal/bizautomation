CREATE OR REPLACE FUNCTION Usp_cflList(v_numDomainID NUMERIC(9,0) DEFAULT 0 ,   
v_locId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_locId = 45) then
      open SWV_RefCur for
      SELECT fld_id,Fld_label,fld_type,(CASE WHEN CAST(subgrp AS INT) = 0 THEN 'Detail Section' ELSE  grp_name END) AS TabName,
loc_name,
coalesce(CFV.bitIsRequired,false) AS bitIsRequired,
coalesce(CFV.bitIsEmail,false) AS bitIsEmail,
coalesce(CFV.bitIsAlphaNumeric,false) AS bitIsAlphaNumeric,
coalesce(CFV.bitIsNumeric,false) AS bitIsNumeric,
coalesce(CFV.bitIsLengthValidation,false) AS bitIsLengthValidation,
coalesce(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS VARCHAR(30)) || '-' || CAST(coalesce(fmst.numlistid,0) as VARCHAR(30)) AS MergeFldLstId,
coalesce(fmst.vcItemsLocation,'') AS vcItemsLocation
      FROM CFW_Fld_Master fmst
      LEFT JOIN CFw_Grp_Master Tmst
      on Tmst.Grp_id = CAST(fmst.subgrp AS INT)
      JOIN CFW_Loc_Master Lmst
      on Lmst.Loc_id = fmst.Grp_id
      LEFT JOIN CFW_Validation CFV ON CFV.numFieldId = fld_id
      WHERE fmst.numDomainID = v_numDomainID
      AND (Lmst.Loc_id = 1 or Lmst.Loc_id = 4);  -- for Leads/Prospects/Accounts and Contacts

   ELSEIF (v_locId = 26)
   then -- (Sales Opportunities / Orders and Purchase Opportunities / Orders) combined for Email Template screen

      open SWV_RefCur for
      SELECT fld_id,Fld_label,fld_type,(CASE WHEN CAST(subgrp AS INT) = 0 THEN 'Detail Section' ELSE  grp_name END) AS TabName,
loc_name,
coalesce(CFV.bitIsRequired,false) AS bitIsRequired,
coalesce(CFV.bitIsEmail,false) AS bitIsEmail,
coalesce(CFV.bitIsAlphaNumeric,false) AS bitIsAlphaNumeric,
coalesce(CFV.bitIsNumeric,false) AS bitIsNumeric,
coalesce(CFV.bitIsLengthValidation,false) AS bitIsLengthValidation,
coalesce(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS VARCHAR(30)) || '-' || CAST(coalesce(fmst.numlistid,0) as VARCHAR(30)) AS MergeFldLstId,
coalesce(fmst.vcItemsLocation,'') AS vcItemsLocation
      FROM CFW_Fld_Master fmst
      LEFT JOIN CFw_Grp_Master Tmst
      on Tmst.Grp_id = CAST(fmst.subgrp AS INT)
      JOIN CFW_Loc_Master Lmst
      on Lmst.Loc_id = fmst.Grp_id
      LEFT JOIN CFW_Validation CFV ON CFV.numFieldId = fld_id
      WHERE fmst.numDomainID = v_numDomainID
      AND (Lmst.Loc_id = 2 or Lmst.Loc_id = 6);
   ELSE
      open SWV_RefCur for
      select fld_id,Fld_label,fld_type,case when CAST(subgrp AS INT) = 0 then 'Detail Section' else  grp_name  end as TabName,loc_name,
coalesce(CFV.bitIsRequired,false) AS bitIsRequired,coalesce(CFV.bitIsEmail,false) AS bitIsEmail,coalesce(CFV.bitIsAlphaNumeric,false) AS bitIsAlphaNumeric,coalesce(CFV.bitIsNumeric,false) AS bitIsNumeric,coalesce(CFV.bitIsLengthValidation,false) AS bitIsLengthValidation
,coalesce(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS VARCHAR(30)) || '-' || CAST(coalesce(fmst.numlistid,0) as VARCHAR(30)) AS MergeFldLstId,
coalesce(fmst.vcItemsLocation,'') AS vcItemsLocation
      from CFW_Fld_Master fmst
      left join CFw_Grp_Master Tmst
      on Tmst.Grp_id = CAST(fmst.subgrp AS INT)
      join CFW_Loc_Master Lmst
      on Lmst.Loc_id = fmst.Grp_id
      LEFT JOIN CFW_Validation CFV ON CFV.numFieldId = fld_id
      where fmst.numDomainID = v_numDomainID
      and  Lmst.Loc_id =(case when v_locId = 0 then Lmst.Loc_id else v_locId end);
   end if;
   RETURN;
END; $$;
