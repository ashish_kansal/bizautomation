-- Stored procedure definition script USP_AdminProcess for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AdminProcess(v_byteMode SMALLINT DEFAULT null,
v_numProcessId NUMERIC(9,0) DEFAULT null,
v_numSubStageId NUMERIC(9,0) DEFAULT null, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select numSubStageHDRId ,vcSubStageName as SubStage from SubStageHDR where numProcessId = v_numProcessId;
   end if;

   if v_byteMode = 1 then

      delete from SubStageHDR where numSubStageHDRId = v_numSubStageId;
      delete from SubStageDetails where numSubStageHdrID = v_numSubStageId;
   end if;


   if v_byteMode = 2 then

      open SWV_RefCur for
      select numSubStageHDRId,vcSubStageName from SubStageHDR;
   end if;
   RETURN;
END; $$;


