DROP FUNCTION IF EXISTS USP_ManageItemsAndKits;

CREATE OR REPLACE FUNCTION USP_ManageItemsAndKits(INOUT v_numItemCode NUMERIC(9,0) ,                                                                
v_vcItemName VARCHAR(300),                                                                
v_txtItemDesc VARCHAR(2000),                                                                
v_charItemType CHAR(1),                                                                
v_monListPrice DECIMAL(20,5),                                                                
v_numItemClassification NUMERIC(9,0),                                                                
v_bitTaxable BOOLEAN,                                                                
v_vcSKU VARCHAR(50),                                                                                                                                      
v_bitKitParent BOOLEAN,                                                                                   
--@dtDateEntered as datetime,                                                                
v_numDomainID NUMERIC(9,0),                                                                
v_numUserCntID NUMERIC(9,0),  
v_bitSerialized BOOLEAN,   
v_numVendorID NUMERIC(9,0),                 
v_strFieldList TEXT,                                            
v_vcModelID VARCHAR(200) DEFAULT '' ,                                            
v_numItemGroup NUMERIC(9,0) DEFAULT 0,                                      
v_numCOGSChartAcntId NUMERIC(9,0) DEFAULT 0,                                      
v_numAssetChartAcntId NUMERIC(9,0) DEFAULT 0,                                      
v_numIncomeChartAcntId NUMERIC(9,0) DEFAULT 0,                            
v_monAverageCost DECIMAL(20,5) DEFAULT NULL,                          
v_monLabourCost DECIMAL(20,5) DEFAULT NULL,                  
v_fltWeight DOUBLE PRECISION DEFAULT NULL,                  
v_fltHeight DOUBLE PRECISION DEFAULT NULL,                  
v_fltLength DOUBLE PRECISION DEFAULT NULL,                  
v_fltWidth DOUBLE PRECISION DEFAULT NULL,                  
v_bitFreeshipping BOOLEAN DEFAULT NULL,                
v_bitAllowBackOrder BOOLEAN DEFAULT NULL,              
v_UnitofMeasure VARCHAR(30) DEFAULT '',              
v_strChildItems TEXT DEFAULT NULL,            
v_bitShowDeptItem BOOLEAN DEFAULT NULL,              
v_bitShowDeptItemDesc BOOLEAN DEFAULT NULL,        
v_bitCalAmtBasedonDepItems BOOLEAN DEFAULT NULL,    
v_bitAssembly BOOLEAN DEFAULT NULL,
v_intWebApiId INTEGER DEFAULT null,
v_vcApiItemId VARCHAR(50) DEFAULT null,
v_numBarCodeId VARCHAR(25) DEFAULT null,
v_vcManufacturer VARCHAR(250) DEFAULT NULL,
v_numBaseUnit NUMERIC(18,0) DEFAULT 0,
v_numPurchaseUnit NUMERIC(18,0) DEFAULT 0,
v_numSaleUnit NUMERIC(18,0) DEFAULT 0,
v_bitLotNo BOOLEAN DEFAULT NULL,
v_IsArchieve BOOLEAN DEFAULT NULL,
v_numItemClass NUMERIC(9,0) DEFAULT 0,
v_tintStandardProductIDType SMALLINT DEFAULT 0,
v_vcExportToAPI VARCHAR(50) DEFAULT NULL,
v_numShipClass NUMERIC DEFAULT NULL,
v_ProcedureCallFlag SMALLINT DEFAULT 0,
v_vcCategories VARCHAR(2000) DEFAULT '',
v_bitAllowDropShip BOOLEAN DEFAULT false,
v_bitArchiveItem BOOLEAN DEFAULT false,
v_bitAsset BOOLEAN DEFAULT false,
v_bitRental BOOLEAN DEFAULT false,
v_numCategoryProfileID NUMERIC(18,0) DEFAULT 0,
v_bitVirtualInventory BOOLEAN DEFAULT NULL,
v_bitContainer BOOLEAN DEFAULT NULL,
v_numContainer NUMERIC(18,0) DEFAULT 0,
v_numNoItemIntoContainer NUMERIC(18,0) DEFAULT NULL,
v_bitMatrix BOOLEAN DEFAULT false ,
v_vcItemAttributes VARCHAR(2000) DEFAULT '',
v_numManufacturer NUMERIC(18,0) DEFAULT NULL,
v_vcASIN VARCHAR(50) DEFAULT NULL,
v_tintKitAssemblyPriceBasedOn SMALLINT DEFAULT 1,
v_fltReorderQty DOUBLE PRECISION DEFAULT 0,
v_bitSOWorkOrder BOOLEAN DEFAULT false,
v_bitKitSingleSelect BOOLEAN DEFAULT false,
v_bitExpenseItem BOOLEAN DEFAULT false,
v_numExpenseChartAcntId NUMERIC(18,0) DEFAULT 0,
v_numBusinessProcessId NUMERIC(18,0) DEFAULT 0,
v_bitTimeContractFromSalesOrder BOOLEAN DEFAULT false,
v_numDepreciationPeriod NUMERIC(18,1) DEFAULT 0,
v_monResidualValue NUMERIC(20,5) DEFAULT 0,
v_tintDepreciationMethod SMALLINT DEFAULT 1,
p_PreventOrphanedParents BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitAttributesChanged  BOOLEAN DEFAULT true;
   v_bitOppOrderExists  BOOLEAN DEFAULT false;

   v_hDoc  INTEGER;     
	                                                                        	
   v_ParentSKU  VARCHAR(50) DEFAULT v_vcSKU;                        
   v_ItemID  NUMERIC(9,0);
   v_cnt  INTEGER;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_bitDuplicate  BOOLEAN DEFAULT false;
   v_strAttribute  VARCHAR(500) DEFAULT '';
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER; 
   v_numFldID  NUMERIC(18,0);
   v_numFldValue  NUMERIC(18,0);
   v_OldGroupID  NUMERIC;
   v_monOldAverageCost  DECIMAL(18,4);
   v_bitCartFreeShipping  BOOLEAN;
   v_numWareHouseItemID  NUMERIC(9,0);
   v_numTotal  INTEGER;
   v_vcDescription  VARCHAR(100);
 
   v_hDoc1  INTEGER;
   v_j  INTEGER DEFAULT 1;
   v_jCount  INTEGER;
   v_numTempWarehouseID  NUMERIC(18,0);
   v_USP_WarehouseItems_Save_numWareHouseItemID NUMERIC(18,0);
   v_ExportToAPIList  VARCHAR(30);
   v_numDomain  NUMERIC(18,0);
BEGIN
   v_vcManufacturer := CASE WHEN coalesce(v_numManufacturer,0) > 0 THEN coalesce((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = v_numManufacturer),'')  ELSE coalesce(v_vcManufacturer,'') END;

   IF coalesce(v_numItemCode,0) > 0 then
	
      IF(SELECT COUNT(OI.numOppId) FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode) > 0 then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitItems OI WHERE OI.numChildItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitChildItems OI WHERE OI.numItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      end if;
   ELSE
      IF (coalesce(v_vcItemName,'') = '') then
		
         RAISE EXCEPTION 'ITEM_NAME_NOT_SELECTED';
         RETURN;
      ELSEIF (coalesce(v_charItemType,'') = '0')
      then
		
         RAISE EXCEPTION 'ITEM_TYPE_NOT_SELECTED';
         RETURN;
      end if;
   end if;

   If (v_charItemType = 'P' AND coalesce(v_bitKitParent,false) = false) then
	
      IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numAssetChartAcntId) = 0) then
		
         RAISE EXCEPTION 'INVALID_ASSET_ACCOUNT';
         RETURN;
      end if;
   end if;

   If v_charItemType = 'P' OR coalesce((SELECT bitExpenseNonInventoryItem FROM Domain WHERE numDomainId = v_numDomainID),false) = true then
	
		-- This is common check for CharItemType P and Other
      IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numCOGSChartAcntId) = 0) then
		
         RAISE EXCEPTION 'INVALID_COGS_ACCOUNT';
         RETURN;
      end if;
   end if;

   IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numIncomeChartAcntId) = 0) then
	
      RAISE EXCEPTION 'INVALID_INCOME_ACCOUNT';
      RETURN;
   end if;

   IF coalesce(v_bitExpenseItem,false) = true then
	
      IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numExpenseChartAcntId) = 0) then
		
         RAISE EXCEPTION 'INVALID_EXPENSE_ACCOUNT';
         RETURN;
      end if;
   end if;

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      Fld_ID NUMERIC(18,0),
      Fld_Value NUMERIC(18,0)
   );   

   IF coalesce(v_bitMatrix,false) = true AND coalesce(v_numItemGroup,0) = 0 then
	
      RAISE EXCEPTION 'ITEM_GROUP_NOT_SELECTED';
      RETURN;
   ELSEIF coalesce(v_bitMatrix,false) = true AND LENGTH(coalesce(v_vcItemAttributes,'')) = 0
   then
	
      RAISE EXCEPTION 'ITEM_ATTRIBUTES_NOT_SELECTED';
      RETURN;
   ELSEIF coalesce(v_bitMatrix,false) = true
   then
      IF LENGTH(coalesce(v_vcItemAttributes,'')) > 2 then
         INSERT INTO tt_TEMPTABLE(Fld_ID,
				Fld_Value)
         SELECT Fld_ID,Fld_Value FROM
		 XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_vcItemAttributes AS XML)
				COLUMNS
					id FOR ORDINALITY,
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value NUMERIC(9,0) PATH 'Fld_Value'
			) AS X 
         ORDER BY
         Fld_ID;
         select   COUNT(*) INTO v_COUNT FROM tt_TEMPTABLE;
         WHILE v_i <= v_COUNT LOOP
            select   Fld_ID, Fld_Value INTO v_numFldID,v_numFldValue FROM tt_TEMPTABLE WHERE ID = v_i;
            IF SWF_IsNumeric(v_numFldValue::VARCHAR) = true then
				
               select   coalesce(v_strAttribute,'') || FLd_label INTO v_strAttribute FROM CFW_Fld_Master WHERE Fld_id = v_numFldID AND Grp_id = 9;
               IF LENGTH(v_strAttribute) > 0 then
					
                  select   coalesce(v_strAttribute,'') || ':' || vcData || ',' INTO v_strAttribute FROM Listdetails WHERE numListItemID = v_numFldValue;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         v_strAttribute := SUBSTR(v_strAttribute,0,LENGTH(v_strAttribute));
         IF(SELECT
         COUNT(*)
         FROM
         Item
         INNER JOIN
         ItemAttributes
         ON
         Item.numItemCode = ItemAttributes.numItemCode
         AND ItemAttributes.numDomainID = v_numDomainID
         WHERE
         Item.numItemCode <> v_numItemCode
         AND Item.vcItemName = v_vcItemName
         AND Item.numItemGroup = v_numItemGroup
         AND Item.numDomainID = v_numDomainID
         AND fn_GetItemAttributes(v_numDomainID,Item.numItemCode,0::BOOLEAN) = v_strAttribute) > 0 then
			
            RAISE EXCEPTION 'ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS';
            RETURN;
         end if;
         If coalesce(v_numItemCode,0) > 0 AND fn_GetItemAttributes(v_numDomainID,v_numItemCode,0::BOOLEAN) = v_strAttribute then
			
            v_bitAttributesChanged := false;
         end if;
         
      end if;
   ELSEIF coalesce(v_bitMatrix,false) = false
   then
	
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
      DELETE FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
   end if;

   IF coalesce(v_numItemGroup,0) > 0 AND coalesce(v_bitMatrix,false) = true then
	
      IF(SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = v_numItemGroup AND tintType = 2) = 0 then
		
         RAISE EXCEPTION 'ITEM_GROUP_NOT_CONFIGURED';
         RETURN;
      ELSEIF EXISTS(SELECT
      CFM.Fld_id
      FROM
      CFW_Fld_Master CFM
      LEFT JOIN
      Listdetails LD
      ON
      CFM.numlistid = LD.numListID
      WHERE
      CFM.numDomainID = v_numDomainID
      AND CFM.Fld_id IN(SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID = v_numItemGroup AND tintType = 2)
      GROUP BY
      CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
      then
		
         RAISE EXCEPTION 'ITEM_GROUP_NOT_CONFIGURED';
         RETURN;
      end if;
   end if;

   BEGIN
      -- BEGIN TRANSACTION
IF v_numCOGSChartAcntId = 0 then 
         v_numCOGSChartAcntId := NULL;
      end if;
      IF v_numAssetChartAcntId = 0 then 
         v_numAssetChartAcntId := NULL;
      end if;
      IF v_numIncomeChartAcntId = 0 then 
         v_numIncomeChartAcntId := NULL;
      end if;
      IF v_numExpenseChartAcntId = 0 then 
         v_numExpenseChartAcntId := NULL;
      end if;
      IF coalesce(v_charItemType,'') = '' OR coalesce(v_charItemType,'') = '0' then
         v_charItemType := 'N';
      end if;
      IF v_intWebApiId = 2 AND LENGTH(coalesce(v_vcApiItemId,'')) > 0 AND coalesce(v_numItemGroup,0) > 0 then
     
        -- check wether this id already Mapped in ITemAPI 
         select   COUNT(numItemID) INTO v_cnt FROM
         ItemAPI WHERE
         WebApiId = v_intWebApiId
         AND numDomainId = v_numDomainID
         AND vcAPIItemID = v_vcApiItemId;
         IF v_cnt > 0 then
        
            select   numItemID INTO v_ItemID FROM
            ItemAPI WHERE
            WebApiId = v_intWebApiId
            AND numDomainId = v_numDomainID
            AND vcAPIItemID = v_vcApiItemId;
        
			--Update Existing Product coming from API
            v_numItemCode := v_ItemID;
         end if;
      ELSEIF v_intWebApiId > 1 AND LENGTH(coalesce(v_vcSKU,'')) > 0
      then
    
         v_ParentSKU := v_vcSKU;
         select   COUNT(numItemCode) INTO v_cnt FROM
         Item WHERE
         numDomainID = v_numDomainID
         AND (vcSKU = v_vcSKU OR v_vcSKU IN(SELECT vcWHSKU FROM WareHouseItems WHERE numItemID = Item.numItemCode AND vcWHSKU = v_vcSKU));
         IF v_cnt > 0 then
        
            select   numItemCode, vcSKU INTO v_ItemID,v_ParentSKU FROM
            Item WHERE
            numDomainID = v_numDomainID
            AND (vcSKU = v_vcSKU OR v_vcSKU IN(SELECT vcWHSKU FROM WareHouseItems WHERE numItemID = Item.numItemCode AND vcWHSKU = v_vcSKU));
            v_numItemCode := v_ItemID;
         ELSE
			-- check wether this id already Mapped in ITemAPI 
            select   COUNT(numItemID) INTO v_cnt FROM
            ItemAPI WHERE
            WebApiId = v_intWebApiId
            AND numDomainId = v_numDomainID
            AND vcAPIItemID = v_vcApiItemId;
            IF v_cnt > 0 then
			
               select   numItemID INTO v_ItemID FROM
               ItemAPI WHERE
               WebApiId = v_intWebApiId
               AND numDomainId = v_numDomainID
               AND vcAPIItemID = v_vcApiItemId;
        
				--Update Existing Product coming from API
               v_numItemCode := v_ItemID;
            end if;
         end if;
      end if;
      IF v_numItemCode = 0 OR v_numItemCode IS NULL then
	
         INSERT INTO Item(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem
			,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder,bitPreventOrphanedParents)
		VALUES(v_vcItemName,v_txtItemDesc,v_charItemType,v_monListPrice,v_numItemClassification,v_bitTaxable,v_ParentSKU,v_bitKitParent,
			v_numVendorID,v_numDomainID,v_numUserCntID,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),v_numUserCntID,v_bitSerialized,v_vcModelID,v_numItemGroup,
			v_numCOGSChartAcntId,v_numAssetChartAcntId,v_numIncomeChartAcntId,v_monAverageCost,v_monLabourCost,v_fltWeight,v_fltHeight,v_fltWidth,
			v_fltLength,v_bitFreeshipping,v_bitAllowBackOrder,v_UnitofMeasure,v_bitShowDeptItem,v_bitShowDeptItemDesc,v_bitCalAmtBasedonDepItems,
			v_bitAssembly,v_numBarCodeId,v_vcManufacturer,v_numBaseUnit,v_numPurchaseUnit,v_numSaleUnit,v_bitLotNo,v_IsArchieve,v_numItemClass,
			v_tintStandardProductIDType,v_vcExportToAPI,v_numShipClass,v_bitAllowDropShip,v_bitArchiveItem ,v_bitAsset,v_bitRental,
			v_bitVirtualInventory,v_bitContainer,v_numContainer,v_numNoItemIntoContainer,v_bitMatrix,v_numManufacturer,v_vcASIN
			,v_tintKitAssemblyPriceBasedOn,v_fltReorderQty,v_bitSOWorkOrder,v_bitKitSingleSelect,v_bitExpenseItem,v_numExpenseChartAcntId
			,v_numBusinessProcessId,v_bitTimeContractFromSalesOrder,p_PreventOrphanedParents) RETURNING numItemCode INTO  v_numItemCode;
		
         IF  v_intWebApiId > 1 then
		
			 -- insert new product
			 --insert this id into linking table
            INSERT INTO ItemAPI(WebApiId,
	 			numDomainId,
	 			numItemID,
	 			vcAPIItemID,
	 			numCreatedby,
	 			dtCreated,
	 			numModifiedby,
	 			dtModified,vcSKU) VALUES(v_intWebApiId,
				v_numDomainID,
				v_numItemCode,
				v_vcApiItemId,
	 			v_numUserCntID,
	 			TIMEZONE('UTC',now()),
	 			v_numUserCntID,
	 			TIMEZONE('UTC',now()),v_vcSKU);
         end if;
         IF v_charItemType = 'P' then
            BEGIN
               CREATE TEMP SEQUENCE tt_TEMPWarehouse_seq INCREMENT BY 1 START WITH 1;
               EXCEPTION WHEN OTHERS THEN
                  NULL;
            END;
            DROP TABLE IF EXISTS tt_TEMPWAREHOUSE CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPWAREHOUSE
            (
               ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
               numWarehouseID NUMERIC(18,0)
            );
            INSERT INTO tt_TEMPWAREHOUSE(numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID = v_numDomainID;
            select   COUNT(*) INTO v_jCount FROM tt_TEMPWAREHOUSE;
            WHILE v_j <= v_jCount LOOP
               select   numWarehouseID INTO v_numTempWarehouseID FROM tt_TEMPWAREHOUSE WHERE ID = v_j;
               v_USP_WarehouseItems_Save_numWareHouseItemID := 0;
               PERFORM USP_WarehouseItems_Save(v_numDomainID,v_numUserCntID,v_USP_WarehouseItems_Save_numWareHouseItemID,
               v_numTempWarehouseID,0,v_numItemCode,'',v_monListPrice,0,0,'','','',
               '',false);
               v_j := v_j::bigint+1;
            END LOOP;
         end if;
      ELSEIF v_numItemCode > 0 AND v_intWebApiId > 0
      then
	
         IF v_ProcedureCallFlag = 1 then
            select   vcExportToAPI INTO v_ExportToAPIList FROM Item WHERE numItemCode = v_numItemCode;
            IF v_ExportToAPIList != '' then
			
               IF NOT EXISTS(select * from(SELECT * FROM Split(v_ExportToAPIList,',')) as A where items = v_vcExportToAPI) then
				
                  v_vcExportToAPI := coalesce(v_ExportToAPIList,'') || ',' || coalesce(v_vcExportToAPI,'');
               ELSE
                  v_vcExportToAPI := v_ExportToAPIList;
               end if;
            end if;

			--update ExportToAPI String value
            UPDATE Item SET vcExportToAPI = v_vcExportToAPI where numItemCode = v_numItemCode  AND numDomainID = v_numDomainID;
            select   COUNT(numItemID) INTO v_cnt FROM
            ItemAPI WHERE
            WebApiId = v_intWebApiId
            AND numDomainId = v_numDomainID
            AND vcAPIItemID = v_vcApiItemId
            AND numItemID = v_numItemCode;
            IF v_cnt > 0 then
			
				--update lastUpdated Date 
               UPDATE ItemAPI SET dtModified = TIMEZONE('UTC',now()) WHERE WebApiId = v_intWebApiId AND numDomainId = v_numDomainID;
            ELSE
               INSERT INTO ItemAPI(WebApiId,
					numDomainId,
					numItemID,
					vcAPIItemID,
					numCreatedby,
					dtCreated,
					numModifiedby,
					dtModified,vcSKU) VALUES(v_intWebApiId,
					v_numDomainID,
					v_numItemCode,
					v_vcApiItemId,
					v_numUserCntID,
					TIMEZONE('UTC',now()),
					v_numUserCntID,
					TIMEZONE('UTC',now()),v_vcSKU);
            end if;
         end if;
      ELSEIF v_numItemCode > 0 AND v_intWebApiId <= 0
      then
         select   coalesce(numItemGroup,0) INTO v_OldGroupID FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
         v_monOldAverageCost := 0;
         select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_monOldAverageCost FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
         UPDATE
         Item
         SET
         vcItemName = v_vcItemName,txtItemDesc = v_txtItemDesc,charItemType = v_charItemType,
         monListPrice = v_monListPrice,numItemClassification = v_numItemClassification,
         bitTaxable = v_bitTaxable,vcSKU =(CASE WHEN coalesce(v_ParentSKU,'') = '' THEN v_vcSKU ELSE v_ParentSKU END),bitKitParent = v_bitKitParent,
         numVendorID = v_numVendorID,numDomainID = v_numDomainID,bintModifiedDate = TIMEZONE('UTC',now()),
         numModifiedBy = v_numUserCntID,bitSerialized = v_bitSerialized,
         vcModelID = v_vcModelID,numItemGroup = v_numItemGroup,
         numCOGsChartAcntId = v_numCOGSChartAcntId,numAssetChartAcntId = v_numAssetChartAcntId,
         numIncomeChartAcntId = v_numIncomeChartAcntId,monCampaignLabourCost = v_monLabourCost,
         fltWeight = v_fltWeight,fltHeight = v_fltHeight,fltWidth = v_fltWidth,
         fltLength = v_fltLength,bitFreeShipping = v_bitFreeshipping,
         bitAllowBackOrder = v_bitAllowBackOrder,vcUnitofMeasure = v_UnitofMeasure,
         bitShowDeptItem = v_bitShowDeptItem,bitShowDeptItemDesc = v_bitShowDeptItemDesc,
         bitCalAmtBasedonDepItems = v_bitCalAmtBasedonDepItems,bitAssembly = v_bitAssembly,
         numBarCodeId = v_numBarCodeId,vcManufacturer = v_vcManufacturer,
         numBaseUnit = v_numBaseUnit,numPurchaseUnit = v_numPurchaseUnit,numSaleUnit = v_numSaleUnit,
         bitLotNo = v_bitLotNo,IsArchieve = v_IsArchieve,
         numItemClass = v_numItemClass,tintStandardProductIDType = v_tintStandardProductIDType,
         vcExportToAPI = v_vcExportToAPI,numShipClass = v_numShipClass,
         bitAllowDropShip = v_bitAllowDropShip,bitArchiveItem = v_bitArchiveItem,
         bitAsset = v_bitAsset,bitRental = v_bitRental,bitVirtualInventory = v_bitVirtualInventory,
         numContainer = v_numContainer,numNoItemIntoContainer = v_numNoItemIntoContainer,
         bitMatrix = v_bitMatrix,numManufacturer = v_numManufacturer,
         vcASIN = v_vcASIN,tintKitAssemblyPriceBasedOn = v_tintKitAssemblyPriceBasedOn,
         fltReorderQty = v_fltReorderQty,bitSOWorkOrder = v_bitSOWorkOrder,
         bitKitSingleSelect = v_bitKitSingleSelect,bitExpenseItem = v_bitExpenseItem,
         numExpenseChartAcntId = v_numExpenseChartAcntId,numBusinessProcessId = v_numBusinessProcessId,
         bitTimeContractFromSalesOrder = v_bitTimeContractFromSalesOrder,bitPreventOrphanedParents=p_PreventOrphanedParents
         WHERE
         numItemCode = v_numItemCode
         AND numDomainID = v_numDomainID;
         IF NOT EXISTS(SELECT numItemCode FROM OpportunityItems WHERE numItemCode = v_numItemCode) then
		
            IF (((SELECT coalesce(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = v_numItemCode AND numDomainID = v_numDomainID) = 0) AND ((SELECT coalesce(bitVirtualInventory,false) FROM Item  WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID) = false)) then
			
               UPDATE Item SET monAverageCost = v_monAverageCost  where numItemCode = v_numItemCode  AND numDomainID = v_numDomainID;
            end if;
         end if;
	
		--If Average Cost changed then add in Tracking
         IF coalesce(v_monAverageCost,0) != coalesce(v_monOldAverageCost,0) then
            v_numWareHouseItemID := 0;
            v_numTotal := 0;
            v_i := 0;
            v_vcDescription := '';
            select   COUNT(*) INTO v_numTotal FROM WareHouseItems where numItemID = v_numItemCode AND numDomainID = v_numDomainID;
            WHILE v_i < v_numTotal LOOP
               select   min(numWareHouseItemID) INTO v_numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numDomainID = v_numDomainID and numWareHouseItemID > v_numWareHouseItemID;
               IF v_numWareHouseItemID > 0 then
                  v_vcDescription := 'Average Cost Changed Manually OLD : ' || CAST(coalesce(v_monOldAverageCost,0) AS VARCHAR(20));

                  v_numDomain := v_numDomainID;
				  
				  PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
                  v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
                  v_tintMode := 0::SMALLINT,v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
				  
				END IF;  
				 v_i := v_i::bigint+1;
            END LOOP;
         end if;
         select   bitFreeShipping INTO v_bitCartFreeShipping FROM CartItems where numItemCode = v_numItemCode  AND numDomainID = v_numDomainID;
         IF v_bitCartFreeShipping <> v_bitFreeshipping then
		
            UPDATE CartItems SET bitFreeShipping = v_bitFreeshipping WHERE numItemCode = v_numItemCode  AND numDomainID = v_numDomainID;
         end if;
         IF v_intWebApiId > 1 then
		
            select   COUNT(numItemID) INTO v_cnt FROM
            ItemAPI WHERE
            WebApiId = v_intWebApiId
            AND numDomainId = v_numDomainID
            AND vcAPIItemID = v_vcApiItemId;
            IF v_cnt > 0 then
			
				--update lastUpdated Date 
               UPDATE ItemAPI SET dtModified = TIMEZONE('UTC',now()) WHERE WebApiId = v_intWebApiId AND numDomainId = v_numDomainID;
            ELSE
				
INSERT INTO ItemAPI(WebApiId,
	 				numDomainId,
	 				numItemID,
	 				vcAPIItemID,
	 				numCreatedby,
	 				dtCreated,
	 				numModifiedby,
	 				dtModified,vcSKU)
				 VALUES(v_intWebApiId,
					v_numDomainID,
					v_numItemCode,
					v_vcApiItemId,
	 				v_numUserCntID,
	 				TIMEZONE('UTC',now()),
	 				v_numUserCntID,
	 				TIMEZONE('UTC',now()),v_vcSKU);
            end if;
         end if;   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
         IF	v_bitTaxable = true then
		
            IF NOT EXISTS(SELECT * FROM ItemTax WHERE numItemCode = v_numItemCode AND numTaxItemID = 0) then
			
			     
INSERT INTO ItemTax(numItemCode,
					numTaxItemID,
					bitApplicable)
				VALUES(v_numItemCode,
					0,
					false);
            end if;
         end if;
         IF v_charItemType = 'S' or v_charItemType = 'N' then
		
            DELETE FROM WareHouseItmsDTL where numWareHouseItemID in(select numWareHouseItemID from WareHouseItems where numItemID = v_numItemCode AND WareHouseItems.numDomainId = v_numDomainID);
            DELETE FROM WareHouseItems where numItemID = v_numItemCode AND WareHouseItems.numDomainId = v_numDomainID AND numWareHouseItemID NOT IN(SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode);
         end if;
         IF v_bitKitParent = true OR v_bitAssembly = true then
            IF OCTET_LENGTH(coalesce(v_strChildItems,'')) > 0 then
			
               v_strChildItems := CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',v_strChildItems);
            end if;
           
            UPDATE
            ItemDetails
            SET
            numQtyItemsReq = X.QtyItemsReq,numUOMId = X.numUOMId,vcItemDesc = X.vcItemDesc,
            sintOrder = X.sintOrder,tintView = X.tintView,vcFields = X.vcFields,bitUseInDynamicSKU = X.bitUseInDynamicSKU,
            bitOrderEditable = X.bitOrderEditable,bitUseInNewItemConfiguration = X.bitUseInNewItemConfiguration
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strChildItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					ItemKitID NUMERIC(9,0) PATH 'ItemKitID',
					ChildItemID NUMERIC(9,0) PATH 'ChildItemID',
					QtyItemsReq DECIMAL(30,16) PATH 'QtyItemsReq',
					numUOMId NUMERIC(9,0) PATH 'numUOMId',
					vcItemDesc VARCHAR(1000) PATH 'vcItemDesc',
					sintOrder INTEGER PATH 'sintOrder',
					numItemDetailID NUMERIC(18,0) PATH 'numItemDetailID',
					tintView SMALLINT PATH 'tintView',
					vcFields VARCHAR(500) PATH 'vcFields',
					bitUseInDynamicSKU BOOLEAN PATH 'bitUseInDynamicSKU',
					bitOrderEditable BOOLEAN PATH 'bitOrderEditable',
					bitUseInNewItemConfiguration BOOLEAN PATH 'bitUseInNewItemConfiguration'
			) AS X 
            WHERE
            numItemKitID = X.ItemKitID
            AND numChildItemID = ChildItemID
            AND itemdetails.numitemdetailid = X.numItemDetailID;
          
         ELSE
            DELETE FROM ItemDetails WHERE numItemKitID = v_numItemCode;
         end if;
      end if;
      IF coalesce(v_numItemGroup,0) = 0 OR coalesce(v_bitMatrix,false) = true then
	
         UPDATE WareHouseItems SET monWListPrice = v_monListPrice,vcWHSKU = v_vcSKU,vcBarCode = v_numBarCodeId WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode;
      end if;

	-- SAVE MATRIX ITEM ATTRIBUTES
      IF v_numItemCode > 0 AND coalesce(v_bitAttributesChanged,false) = true AND coalesce(v_bitMatrix,false) = true AND LENGTH(coalesce(v_vcItemAttributes,'')) > 0 AND coalesce(v_bitOppOrderExists,false) <> true AND(SELECT COUNT(*) FROM tt_TEMPTABLE) > 0 then
	
		-- DELETE PREVIOUS ITEM ATTRIBUTES
         DELETE FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;

		-- INSERT NEW ITEM ATTRIBUTES
         INSERT INTO ItemAttributes(numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value)
         SELECT
         v_numDomainID
			,v_numItemCode
			,Fld_ID
			,Fld_Value
         FROM
         tt_TEMPTABLE;
         IF(SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode) > 0 then
		

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
            DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN(SELECT coalesce(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode);

			-- INSERT NEW WAREHOUSE ATTRIBUTES
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized)
            SELECT
            Fld_ID,
				coalesce(CAST(Fld_Value AS VARCHAR(30)),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				false
            FROM
            tt_TEMPTABLE
            LEFT JOIN LATERAL(SELECT
               numWarehouseItemID
               FROM
               WarehouseItems
               WHERE
               numDomainID = v_numDomainID
               AND numItemID = v_numItemCode) AS TEMPWarehouse on TRUE;
         end if;
      end if;
      IF coalesce(v_bitAsset,false) = true then
	
         IF EXISTS(SELECT ID FROM ItemDepreciationConfig WHERE numItemCode = v_numItemCode) then
		
            UPDATE
            ItemDepreciationConfig
            SET
            numDepreciationPeriod = v_numDepreciationPeriod,monResidualValue = v_monResidualValue,
            tintDepreciationMethod = v_tintDepreciationMethod
            WHERE
            numItemCode = v_numItemCode;
         ELSE
			
INSERT INTO ItemDepreciationConfig(numItemCode
				,numDepreciationPeriod
				,monResidualValue
				,tintDepreciationMethod)
			VALUES(v_numItemCode
				,v_numDepreciationPeriod
				,v_monResidualValue
				,v_tintDepreciationMethod);
         end if;
      ELSE
         DELETE FROM ItemDepreciationConfig WHERE numItemCode = v_numItemCode;
         DELETE FROM ItemDepreciation WHERE numItemCode = v_numItemCode;
      end if;
      -- COMMIT
IF v_numItemCode > 0 then
	
         DELETE FROM ItemCategory IC using Category where IC.numCategoryID = Category.numCategoryID AND numItemID = v_numItemCode
         AND numCategoryProfileID = v_numCategoryProfileID;
         IF v_vcCategories <> '' then
		
			 
INSERT INTO ItemCategory(numItemID , numCategoryID)  SELECT  v_numItemCode AS numItemID,ID from SplitIDs(v_vcCategories,',');
         end if;
      end if;
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;