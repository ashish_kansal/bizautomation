-- Stored procedure definition script USP_GetECampaignContactFollowUpList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetECampaignContactFollowUpList(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LastDateTime  TIMESTAMP;
   v_CurrentDateTime  TIMESTAMP;
BEGIN
   select   dtLastDateTimeECampaign INTO v_LastDateTime FROM WindowsServiceHistory;
   v_CurrentDateTime := GetUTCDateWithoutTime()+CAST(EXTRACT(HOUR FROM TIMEZONE('UTC',now())) || 'hour' as interval)+CAST(EXTRACT(MINUTE FROM TIMEZONE('UTC',now())) || 'minute' as interval);

   open SWV_RefCur for SELECT
   cast(E.numECampaignID as VARCHAR(255)),
			cast(ED.numECampDTLId as VARCHAR(255)),
            cast(C.numContactID as VARCHAR(255)),
            cast(ED.numFollowUpID as VARCHAR(255)),
            cast(DTL.numConECampDTLID as VARCHAR(255)),
            cast(C.numConEmailCampID as VARCHAR(255)),
                DTL.dtExecutionDate+CAST(coalesce(E.fltTimeZone,0)*60 || 'minute' as interval) AS StartDate,
                v_LastDateTime+CAST((coalesce(E.fltTimeZone,0) -2)*60 || 'minute' as interval) AS Between1,
                v_CurrentDateTime+CAST(coalesce(E.fltTimeZone,0)*60 || 'minute' as interval) AS Between2,
                cast(E.fltTimeZone as VARCHAR(255))
   FROM    ConECampaign C
   JOIN ConECampaignDTL DTL ON numConEmailCampID = numConECampID
   JOIN ECampaignDTLs ED ON ED.numECampDTLId = DTL.numECampDTLID
   JOIN ECampaign E ON ED.numECampID = E.numECampaignID
   JOIN AdditionalContactsInformation A ON A.numContactId = C.numContactID
   WHERE   coalesce(bitFollowUpStatus,false) = false
   AND
   bitEngaged = true
   AND numFollowUpID > 0
   AND DTL.dtExecutionDate+CAST(coalesce(E.fltTimeZone,0)*60 || 'minute' as interval)
   BETWEEN v_LastDateTime+CAST((coalesce(E.fltTimeZone,0) -2)*60 || 'minute' as interval) AND v_CurrentDateTime+CAST(coalesce(E.fltTimeZone,0)*60 || 'minute' as interval);
END; $$;












