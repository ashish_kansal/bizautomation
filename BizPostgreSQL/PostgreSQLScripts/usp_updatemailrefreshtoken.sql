-- Stored procedure definition script USP_UpdateMailRefreshToken for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateMailRefreshToken(v_numDomainId NUMERIC(18,0),
    v_numUserCntId NUMERIC(18,0),
	v_tintMailProvider SMALLINT,
	v_vcEmail VARCHAR(100),
	v_vcMailAccessToken TEXT,
    v_vcMailRefreshToken TEXT,
	v_intExpireInSeconds INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numDomainId,0) > 0 AND coalesce(v_numUserCntId,0) > 0 then
	
      UPDATE
      UserMaster
      SET
      vcPSMTPUserName =(CASE WHEN LENGTH(coalesce(v_vcEmail,'')) > 0 THEN v_vcEmail ELSE vcPSMTPUserName END),tintMailProvider = v_tintMailProvider,
      vcMailAccessToken = v_vcMailAccessToken,vcMailRefreshToken = v_vcMailRefreshToken,
      dtTokenExpiration = TIMEZONE('UTC',now())+CAST(coalesce(v_intExpireInSeconds,0) || 'second' as interval)
      WHERE
      numDomainID = v_numDomainId
      AND numUserDetailId = v_numUserCntId;
      UPDATE
      UserMaster
      SET
      tintMailProvider = v_tintMailProvider,vcMailAccessToken = v_vcMailAccessToken,
      vcMailRefreshToken = v_vcMailRefreshToken,dtTokenExpiration = TIMEZONE('UTC',now())+CAST(coalesce(v_intExpireInSeconds,0) || 'second' as interval)
      WHERE
      numUserDetailId <> v_numUserCntId
      AND vcPSMTPUserName = v_vcEmail;
   ELSEIF coalesce(v_numDomainId,0) > 0
   then
	
      UPDATE
      Domain
      SET
      vcPSMTPUserName =(CASE WHEN LENGTH(coalesce(v_vcEmail,'')) > 0 THEN v_vcEmail ELSE vcPSMTPUserName END),tintMailProvider = v_tintMailProvider,
      vcMailAccessToken = v_vcMailAccessToken,vcMailRefreshToken = v_vcMailRefreshToken,
      dtTokenExpiration = TIMEZONE('UTC',now())+CAST(coalesce(v_intExpireInSeconds,0) || 'second' as interval)
      WHERE
      numDomainId = v_numDomainId;
   end if;
   RETURN;
END; $$;


