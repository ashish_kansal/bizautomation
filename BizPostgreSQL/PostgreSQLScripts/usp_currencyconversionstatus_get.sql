-- Stored procedure definition script USP_CurrencyConversionStatus_GET for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CurrencyConversionStatus_GET(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		Id,
		dtLastExecuted,
		bitSucceed,
		intNoOfTimesTried,
		bitFailureNotificationSent
   FROM
   CurrencyConversionStatus;
END; $$;











