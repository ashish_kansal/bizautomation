-- Stored procedure definition script usp_SaveAssets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveAssets(v_numDivId NUMERIC,  
v_strItems VARCHAR(8000),  
v_numDomainId NUMERIC,
v_intType INTEGER DEFAULT 1,
v_numItemCode NUMERIC(18,0) DEFAULT NULL,
v_numDeptId NUMERIC(18,0) DEFAULT NULL,
v_btApp BOOLEAN DEFAULT NULL,
v_btDep BOOLEAN DEFAULT NULL,
v_numAppValue NUMERIC(18,2) DEFAULT NULL,
v_numDepValue NUMERIC(18,2) DEFAULT NULL,
v_dtPurchaseDate TIMESTAMP DEFAULT NULL,
v_dtWarrentyDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql   
-- delete  CompanyAssets where numDivId = @numDivId and numDomainId = @numDomainId       
   AS $$
   DECLARE
   v_hDocItem  INTEGER;    
   v_Count  INTEGER;
BEGIN
   if v_intType = 0 then
	
      if SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
		 
         SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
         insert into
         CompanyAssets(numDivID,numDomainId ,numWarehouseItmsDtlID,numItemCode,bitSerializable,numUnit,numOppId)
         select v_numDivId,v_numDomainId,coalesce(X.numWareHouseItmsDtlId,0),x.numItemCode,x.Type,X.Unit,X.numOppId from(SELECT *FROM SWF_OpenXml(v_hDocItem,'/NewDataSet/Table','numWareHouseItmsDtlId |numItemCode |type |unit |numOppId') SWA_OpenXml(numWareHouseItmsDtlId NUMERIC(9,0),numItemCode NUMERIC(9,0),type BOOLEAN,
            unit NUMERIC(9,0),numOppId NUMERIC(9,0))) X;
         PERFORM SWF_Xml_RemoveDocument(v_hDocItem);
      end if;
   else
      v_Count := 0;
      v_Count := coalesce((select count(*) from CompanyAssets where numItemCode = v_numItemCode),
      0);
      if v_Count = 0 then
		
         insert into CompanyAssets(numDivID,numDomainId ,numWarehouseItmsDtlID,numItemCode,bitSerializable,numUnit,numOppId,numDeptId,btAppreciation,btDepreciation,numAppvalue,numDepValue,dtPurchase,dtWarrentyTill)
         select v_numDivId,v_numDomainId,0,v_numItemCode,false,0,0,v_numDeptId,v_btApp,v_btDep,v_numAppValue,v_numDepValue,v_dtPurchaseDate,v_dtWarrentyDate;
      else
         update CompanyAssets
         set numDivID = v_numDivId,numDeptId = v_numDeptId,btAppreciation = v_btApp,btDepreciation = v_btDep,
         numAppvalue = v_numAppValue,numDepValue = v_numDepValue,
         dtPurchase = v_dtPurchaseDate,dtWarrentyTill = v_dtWarrentyDate
         where numItemCode = v_numItemCode;
      end if;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_SaveAuthoritativeBizDocs]    Script Date: 07/26/2008 16:20:54 ******/
   RETURN;
END; $$;


