DROP FUNCTION IF EXISTS USP_InsertCartItem;

CREATE OR REPLACE FUNCTION USP_InsertCartItem
(
	p_numUserCntId NUMERIC(18, 0),
    p_numDomainId NUMERIC(18, 0),
    p_vcCookieId VARCHAR(100),
    p_numOppItemCode NUMERIC(18, 0),
    p_numItemCode NUMERIC(18, 0),
    p_numUnitHour NUMERIC(18, 0),
    p_monPrice DECIMAL(20,5),
    p_numSourceId NUMERIC(18, 0),
    p_vcItemDesc VARCHAR(2000),
    p_numWarehouseId NUMERIC(18, 0),
    p_vcItemName VARCHAR(200),
    p_vcWarehouse VARCHAR(200),
    p_numWarehouseItmsID NUMERIC(18, 0),
    p_vcItemType VARCHAR(200),
    p_vcAttributes VARCHAR(100),
    p_vcAttrValues VARCHAR(100),
    p_bitFreeShipping BOOLEAN,
    p_numWeight NUMERIC(18, 2),
    p_tintOpFlag SMALLINT,
    p_bitDiscountType BOOLEAN,
    p_fltDiscount DECIMAL(18,2),
    p_monTotAmtBefDiscount DECIMAL(20,5),
    p_ItemURL VARCHAR(200),
    p_numUOM NUMERIC(18, 0),
    p_vcUOMName VARCHAR(200),
    p_decUOMConversionFactor DECIMAL(18, 5),
    p_numHeight NUMERIC(18, 0),
    p_numLength NUMERIC(18, 0),
    p_numWidth NUMERIC(18, 0),
    p_vcShippingMethod VARCHAR(200),
    p_numServiceTypeId NUMERIC(18, 0),
    p_decShippingCharge DECIMAL(18, 2),
    p_numShippingCompany NUMERIC(18, 0),
    p_tintServicetype SMALLINT,
    p_dtDeliveryDate TIMESTAMP,
    p_monTotAmount DECIMAL(20,5),
	p_numSiteID NUMERIC(18,0) = 0,
	p_numParentItemCode NUMERIC(18,0) = 0,
	p_vcChildKitItemSelection TEXT = '',
	p_vcPreferredPromotions TEXT='',
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
   AS $$
   DECLARE 
		v_numPreferredPromotionID NUMERIC(18,0);
		v_numCartId NUMERIC(18,0);
		my_ex_state TEXT;
		my_ex_message TEXT;
		my_ex_detail TEXT;
		my_ex_hint TEXT;
		my_ex_ctx TEXT;
BEGIN
	BEGIN
		DROP TABLE IF EXISTS tt_PreferredPromotion CASCADE;
		CREATE TEMPORARY TABLE tt_PreferredPromotion 
		(
			numItemCode NUMERIC(18,0)
			,numPromotionID NUMERIC(18,0)
		);

		IF COALESCE(p_vcPreferredPromotions,'') <> '' THEN
			INSERT INTO tt_PreferredPromotion
			(
				numItemCode
				,numPromotionID
			)
			SELECT
				numItemCode
				,numPromotionID
			FROM 
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(p_vcPreferredPromotions AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numItemCode NUMERIC(18,0) PATH 'numItemCode',
					numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
			) AS X;                                                                          


			v_numPreferredPromotionID := COALESCE((SELECT numPromotionID  FROM tt_PreferredPromotion WHERE numItemCode=p_numItemCode LIMIT 1),0);
		END IF;
	
		INSERT INTO CartItems
		(
			numUserCntId,
			numDomainId,
			vcCookieId,
			numOppItemCode,
			numItemCode,
			numUnitHour,
			monPrice,
			numSourceId,
			vcItemDesc,
			numWarehouseId,
			vcItemName,
			vcWarehouse,
			numWarehouseItmsID,
			vcItemType,
			vcAttributes,
			vcAttrValues,
			bitFreeShipping,
			numWeight,
			tintOpFlag,
			bitDiscountType,
			fltDiscount,
			monTotAmtBefDiscount,
			ItemURL,
			numUOM,
			vcUOMName,
			decUOMConversionFactor,
			numHeight,
			numLength,
			numWidth,
			vcShippingMethod,
			numServiceTypeId,
			decShippingCharge,
			numShippingCompany,
			tintServicetype,
			monTotAmount,
			numParentItem
			,vcChildKitItemSelection
			,monInsertPrice
			,fltInsertDiscount
			,bitInsertDiscountType
			,numPreferredPromotionID
		)
		VALUES  
		(
			p_numUserCntId,
			p_numDomainId,
			p_vcCookieId,
			p_numOppItemCode,
			p_numItemCode,
			p_numUnitHour,
			p_monPrice,
			p_numSourceId,
			p_vcItemDesc,
			p_numWarehouseId,
			p_vcItemName,
			p_vcWarehouse,
			p_numWarehouseItmsID,
			p_vcItemType,
			p_vcAttributes,
			p_vcAttrValues,
			p_bitFreeShipping,
			p_numWeight,
			p_tintOpFlag,
			p_bitDiscountType,
			p_fltDiscount,
			p_monTotAmtBefDiscount,
			p_ItemURL,
			p_numUOM,
			p_vcUOMName,
			p_decUOMConversionFactor,
			p_numHeight,
			p_numLength,
			p_numWidth,
			p_vcShippingMethod,
			p_numServiceTypeId,
			p_decShippingCharge,
			p_numShippingCompany,
			p_tintServicetype,
			p_monTotAmount,
			p_numParentItemCode,
			p_vcChildKitItemSelection,
			p_monPrice,
			p_fltDiscount,
			p_bitDiscountType,
			v_numPreferredPromotionID
		) RETURNING numCartId INTO v_numCartId;

		IF (SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId=p_numDomainId AND COALESCE(bitEnabled,false)=true) > 0 THEN
			PERFORM USP_PromotionOffer_ApplyItemPromotionToECommerce(p_numDomainID,p_numUserCntId,p_numSiteID,p_vcCookieId,false,'');
		END IF;

		OPEN SWV_RefCur FOR SELECT v_numCartId AS "numCartID";
  
	EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
	END;
RETURN;
END; $$;










