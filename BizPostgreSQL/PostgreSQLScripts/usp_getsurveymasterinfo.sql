-- Stored procedure definition script usp_GetSurveyMasterInfo for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyMasterInfo(v_numDomainID NUMERIC,       
 v_numSurId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all survey master information for the selected survey    
   AS $$
BEGIN
   open SWV_RefCur for SELECT numSurID, vcSurName, vcRedirectURL,
  TO_CHAR(bintCreatedOn,'mm/dd/yyyy') as dtCreatedOn, bitSkipRegistration, bitRandomAnswers,
  bitSinglePageSurvey, bitPostRegistration  ,bitRelationshipProfile,bitSurveyRedirect,bitEmailTemplateContact,bitEmailTemplateRecordOwner
      ,numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage,vcLandingPageYes,vcLandingPageNo ,vcSurveyRedirect,
      tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId
   FROM SurveyMaster
   WHERE numDomainID = v_numDomainID
   AND numSurID = v_numSurId;
END; $$;
