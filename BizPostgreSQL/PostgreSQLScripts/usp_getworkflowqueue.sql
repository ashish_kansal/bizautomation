-- Stored procedure definition script USP_GetWorkFlowQueue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWorkFlowQueue(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN

	--IF Workflow is not active or deleted
   UPDATE
   WorkFlowQueue WFQ
   SET
   tintProcessStatus = 5
   FROM
   WorkFlowMaster WF
   WHERE(WFQ.numDomainID = WF.numDomainID
   AND WFQ.numFormID = WF.numFormID
   AND WF.tintWFTriggerOn = WFQ.tintWFTriggerOn
   AND WF.bitActive = true) AND WF.numWFID IS NULL;

   DELETE FROM WorkFlowQueue WFQ using WorkFlowMaster WF where WFQ.numWFID = WF.numWFID AND coalesce(WF.vcDateField,'') <> '' AND intDays > 0;
			   
	--Select top 25 WorkFlow which are Pending Execution			  	
   open SWV_RefCur for SELECT 
   		WorkFlowQueue.numWFQueueID
		,WorkFlowQueue.numDomainID
		,WorkFlowQueue.numRecordID
		,WorkFlowQueue.numFormID
		,WorkFlowQueue.tintProcessStatus
		,WorkFlowQueue.tintWFTriggerOn
		,WorkFlowQueue.numWFID
   FROM
   WorkFlowQueue
   INNER JOIN
   WorkFlowMaster
   ON
   WorkFlowQueue.numWFID = WorkFlowMaster.numWFID and WorkFlowQueue.numFormID = WorkFlowMaster.numFormID
   AND WorkFlowQueue.numDomainID = WorkFlowMaster.numDomainID AND WorkFlowMaster.tintWFTriggerOn = WorkFlowQueue.tintWFTriggerOn
   WHERE
   WorkFlowQueue.tintProcessStatus IN(1)
   AND WorkFlowMaster.bitActive = true
   ORDER BY
   numWFQueueID LIMIT 25;
   RETURN;
END; $$;
