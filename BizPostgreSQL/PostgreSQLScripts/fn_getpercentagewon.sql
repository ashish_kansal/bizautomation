-- Function definition script fn_GetPercentageWon for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetPercentageWon(v_numUserID NUMERIC,v_numDomainID NUMERIC,v_dtDateFrom TIMESTAMP,v_dtDateTo TIMESTAMP)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retPer  VARCHAR(10);
   v_intOppWon  INTEGER;
   v_intOPP  INTEGER;
BEGIN
   select count(*) INTO v_intOppWon from OpportunityMaster where numrecowner = v_numUserID and tintoppstatus = 1 and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo;
   select count(*) INTO v_intOPP from OpportunityMaster  where numrecowner = v_numUserID and numDomainId = v_numDomainID
   and intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo;

   if v_intOPP = 0 then

      v_retPer := CAST(0 AS VARCHAR(10));
   else
      v_retPer := CAST(round(CAST((CAST(v_intOppWon::bigint AS decimal)/v_intOPP::bigint)*100 AS NUMERIC),2) AS VARCHAR(10));
   end if;
   return v_retPer;
END; $$;

