-- Stored procedure definition script USP_Domain_UpdateShipPhoneNumber for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,27thAugust2014>
-- Description:	<Description,, Update Shipping Phone Number in E-commerce API>
-- =============================================
Create or replace FUNCTION USP_Domain_UpdateShipPhoneNumber(v_numDomainID NUMERIC(9,0) DEFAULT 0,       
	v_vcShipToPhoneNumber VARCHAR(50) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   Update Domain
   SET
   vcShipToPhoneNo = v_vcShipToPhoneNumber
   WHERE   numDomainId = v_numDomainID;
   RETURN;
END; $$;


