DROP FUNCTION IF EXISTS GetCustFldItemsValue;
CREATE OR REPLACE FUNCTION GetCustFldItemsValue(v_numFldId NUMERIC(9,0)
	,v_pageId SMALLINT
	,v_numRecordId NUMERIC(9,0)
	,v_tintMode SMALLINT -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
	,v_fld_type VARCHAR(100))
RETURNS VARCHAR(300) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(100);
BEGIN
   IF v_pageId = 9 AND coalesce(v_tintMode,0) = 0 then
	
      select   CASE
      WHEN v_fld_type = 'SelectBox' THEN GetListIemName(CAST(Fld_Value AS NUMERIC))
      WHEN v_fld_type = 'CheckBox' THEN CASE CAST(COALESCE(Fld_Value,'') AS TEXT) WHEN '1' THEN 'Yes' WHEN '0' THEN 'No' ELSE '' END
      ELSE Fld_Value
      END INTO v_vcValue FROM
      CFW_Fld_Values_Serialized_Items WHERE
      Fld_ID = v_numFldId
      AND RecId = v_numRecordId;
   ELSEIF coalesce(v_tintMode,0) = 1
   then
	
      select   CASE
      WHEN v_fld_type = 'SelectBox' THEN GetListIemName(CAST(Fld_Value AS NUMERIC))
      WHEN v_fld_type = 'CheckBox' THEN CASE CAST(COALESCE(Fld_Value::TEXT,'') AS TEXT) WHEN '1' THEN 'Yes' WHEN '0' THEN 'No' ELSE '' END
      ELSE Fld_Value::TEXT
      END INTO v_vcValue FROM
      ItemAttributes WHERE
      Fld_ID = v_numFldId
      AND numItemCode = v_numRecordId;
   end if;

   IF v_vcValue IS NULL then
      v_vcValue := '0';
   end if;  

   RETURN v_vcValue;
END; $$;

