DROP FUNCTION IF EXISTS usp_getcorrespondance;

CREATE OR REPLACE FUNCTION usp_getcorrespondance(v_FromDate TIMESTAMP,                          
v_toDate TIMESTAMP,                          
v_numContactId NUMERIC,                          
v_vcMessageFrom VARCHAR(200) DEFAULT '' ,                        
v_tintSortOrder NUMERIC DEFAULT NULL,                        
v_SeachKeyword VARCHAR(100) DEFAULT NULL,                        
v_CurrentPage INTEGER DEFAULT NULL,                        
v_PageSize INTEGER DEFAULT NULL,                        
INOUT v_TotRecs NUMERIC  DEFAULT NULL,                        
v_columnName VARCHAR(100) DEFAULT NULL,                        
v_columnSortOrder VARCHAR(50) DEFAULT NULL,                        
v_filter NUMERIC DEFAULT NULL,                        
v_numdivisionId NUMERIC DEFAULT 0,              
v_numDomainID NUMERIC(9,0) DEFAULT 0,              
v_numCaseID NUMERIC(9,0) DEFAULT 0,              
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
v_numOpenRecordID NUMERIC DEFAULT 0,
v_tintMode SMALLINT DEFAULT 0,
v_bitOpenCommu BOOLEAN DEFAULT false,
v_numUserCntID NUMERIC DEFAULT 0,
v_vcTypes VARCHAR(200) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcUserLoginEmail  TEXT DEFAULT '';
   v_strSql  TEXT DEFAULT '';     
                                  
   v_firstRec  INTEGER;                                                        
   v_lastRec  INTEGER;
   v_strCondition  TEXT;
   v_vcContactEmail  TEXT DEFAULT '';                

 

   v_TnumContactID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SELECT
   distinct
   COALESCE((select string_agg(replace(vcEmailID,'''',''''''),',' order by U.vcEmailID) FROM UserMaster AS U where numDomainID = v_numDomainID
      ),'') as userlist INTO v_vcUserLoginEmail from UserMaster where numDomainID = v_numDomainID group by vcEmailID;
   v_vcMessageFrom := replace(v_vcMessageFrom,'''','|');
                         
   if v_filter > 0 then 
      v_tintSortOrder :=  v_filter;
   end if;
              
   if ((v_filter <= 2 and v_tintSortOrder <= 2) or (v_tintSortOrder > 2 and v_filter > 2)) then
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      v_strSql := '';
      if (v_filter <= 2 and v_tintSortOrder <= 2 and v_bitOpenCommu = false) then
         if v_numdivisionId = 0 and  v_vcMessageFrom <> '' then
 
            If v_filter = 1 then --ReceivedMessages
               v_strCondition := ' and (vcFrom iLike ''%' || REPLACE(v_vcMessageFrom,'''','''''') || '%'')  ';
            ELSEIF v_filter = 2
            then --SentMessages
               v_strCondition := ' and (vcTo iLike ''%' || REPLACE(v_vcMessageFrom,'''','''''') || '%'')  ';
            ELSE
               v_strCondition := ' and (vcFrom iLike ''%' || REPLACE(v_vcMessageFrom,'''','''''') || '%'' or vcTo iLike ''%' || REPLACE(v_vcMessageFrom,'''','''''') || '%'')  ';
            end if;
         ELSEIF v_numdivisionId <> 0
         then
            v_strCondition := '';
            select   numContactId, replace(vcEmail,'''','''''') INTO v_TnumContactID,v_vcContactEmail from AdditionalContactsInformation where numDivisionId = v_numdivisionId and LENGTH(vcEmail) > 2   ORDER BY numContactId ASC LIMIT 1;
            v_vcContactEmail := coalesce(v_vcContactEmail,'');
            WHILE v_TnumContactID > 0 LOOP
               if LENGTH(v_strCondition) > 0 then
                  v_strCondition := coalesce(v_strCondition,'') || ' or ';
               end if;
               If v_filter = 1 then --ReceivedMessages
                  v_strCondition := coalesce(v_strCondition,'') || ' (vcFrom iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')  ';
               ELSEIF v_filter = 2
               then --SentMessages
                  v_strCondition := coalesce(v_strCondition,'') || ' (vcTo iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')  ';
               ELSE
                  v_strCondition := coalesce(v_strCondition,'') || ' (vcFrom iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'' or vcTo iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')  ';
               end if;
               select   numContactId, vcEmail INTO v_TnumContactID,v_vcContactEmail from AdditionalContactsInformation where numDivisionId = v_numdivisionId and LENGTH(vcEmail) > 2 and numContactId > v_TnumContactID   ORDER BY numContactId ASC LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               IF SWV_RowCount = 0 then 
                  v_TnumContactID := 0;
               end if;
            END LOOP;
            if LENGTH(v_strCondition) > 0 then
               v_strCondition := 'and (' || coalesce(v_strCondition,'') || ')';
            ELSEIF v_tintMode <> 6 AND v_tintMode <> 5
            then
               v_strCondition := ' and (1=0)';
            end if;
         ELSEIF v_numContactId > 0
         then
 
            select   coalesce(replace(vcEmail,'''',''''''),'') INTO v_vcContactEmail from AdditionalContactsInformation where numContactId = v_numContactId and LENGTH(vcEmail) > 2;
            v_vcContactEmail := coalesce(v_vcContactEmail,'');
            if LENGTH(v_vcContactEmail) > 0 then
               If v_filter = 1 then --ReceivedMessages
                  v_strCondition := ' and (vcFrom iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')  ';
               ELSEIF v_filter = 2
               then --SentMessages
                  v_strCondition := ' and (vcTo iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')  ';
               ELSE
                  v_strCondition := ' and (vcFrom iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'' or vcTo iLike ''%' || REPLACE(v_vcContactEmail,'''','''''') || '%'')';
               end if;
            ELSEIF v_tintMode <> 6 AND v_tintMode <> 5
            then
               v_strCondition := ' and (1=0)';
            end if;
         else
            v_strCondition := '';
         end if;
         DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDATA
         (
            numTotalRecords INTEGER,
            tintCountType SMALLINT,
            numEmailHstrID NUMERIC(18,0),
            DelData VARCHAR(200),
            bintCreatedOn TIMESTAMP,
            date VARCHAR(100),
            Subject TEXT,
            type VARCHAR(100),
            phone VARCHAR(100),
            assignedto VARCHAR(100),
            caseid NUMERIC(18,0),
            vcCasenumber VARCHAR(200),
            caseTimeid NUMERIC(18,0),
            caseExpid NUMERIC(18,0),
            tinttype NUMERIC(18,0),
            dtCreatedDate TIMESTAMP,
            "From" TEXT,
            bitClosedflag BOOLEAN,
            bitHasAttachments BOOLEAN,
            vcBody TEXT,
            InlineEdit VARCHAR(1000),
            numCreatedBy NUMERIC(18,0),
            numOrgTerId NUMERIC(18,0),
            TypeClass VARCHAR(100)
         );
         v_strSql := 'INSERT INTO 
					tt_TEMPDATA
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,CONCAT(HDR.numEmailHstrID,''~1~0'',HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,FormatedDateTimeFromDate(dtReceivedOn + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') as date
					,vcSubject as Subject
					,CASE WHEN hdr.tinttype=2  THEN ''Email Out'' ELSE ''Email In'' END as type
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,HDR.tintType
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom || '','' || vcTo as "From"
					,false as bitClosedflag
					,coalesce(HDR.bitHasAttachments,false) AS bitHasAttachments
					,CASE WHEN LENGTH(Cast(vcBodyText AS VARCHAR(1000))) > 150 THEN SUBSTR(vcBodyText,0,150) || ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN hdr.tinttype=2 THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN Correspondence CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ';
         v_strSql := coalesce(v_strSql,'') || ' where COALESCE(HDR.bitInvisible,false) = false AND  (HDR.numNodeId IN (SELECT numNodeID FROM InboxTreeSort WHERE vcNodeName NOT IN (''All Mail'',''Important'',''Drafts'') AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ')  OR HDR.numNodeId=0) AND HDR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
         || ' AND (CR.numOpenRecordID = ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)'
         ||(CASE WHEN coalesce(v_vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END);
         IF(v_FromDate IS NOT NULL AND v_FromDate <> '1900-01-01 00:00:00.000') then
   
            v_strSql := coalesce(v_strSql,'') || ' and  (dtReceivedOn between ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''' and ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30) || ''') ';
         end if;
         v_strSql := coalesce(v_strSql,'') || coalesce(v_strCondition,'');
         IF v_tintMode = 1 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(1,3) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '= 0)';
         end if;
         IF v_tintMode = 2 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(2,4) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         IF v_tintMode = 5 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(5) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '= 0)';
         end if;
         IF v_tintMode = 6 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(6) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         if v_filter = 0 then 
            v_strSql := coalesce(v_strSql,'') || ' and (HDR.tintType = 1 or HDR.tintType = 2 or  HDR.tintType = 5)';
         ELSEIF v_filter = 1 OR v_filter = 2
         then 
            v_strSql := coalesce(v_strSql,'') || ' and (hdr.tinttype=1 OR hdr.tinttype=2)';
         end if;
         if v_filter > 2 then 
            v_strSql := coalesce(v_strSql,'') || ' and HDR.tintType = 0';
         end if;
         if v_tintSortOrder = 0 and v_SeachKeyword <> '' then
  
            v_strSql := coalesce(v_strSql,'') || ' and (vcFrom iLike ''%' || coalesce(v_SeachKeyword,'') || '%'' or vcTo iLike ''%' || coalesce(v_SeachKeyword,'') || '%'' or vcsubject iLike ''%' || coalesce(v_SeachKeyword,'') || '%'' or vcBody iLike ''%' || coalesce(v_SeachKeyword,'') || '%'')';
         end if;
         if v_tintSortOrder = 1 and v_SeachKeyword <> '' then
  
            v_strSql := coalesce(v_strSql,'') || ' and (vcFrom ilike ''%' || coalesce(v_SeachKeyword,'') || '%'' or vcSubject ilike ''%' || coalesce(v_SeachKeyword,'') || '%'' or  vcBody ilike ''%' || coalesce(v_SeachKeyword,'') || '%'')';
         end if;
         if v_tintSortOrder = 2 and v_SeachKeyword <> '' then
  
            v_strSql := coalesce(v_strSql,'') || ' and (vcTo iLike ''%' || coalesce(v_SeachKeyword,'') || '%'' or vcsubject iLike ''%' || coalesce(v_SeachKeyword,'') || '%'' or  vcBody iLike ''%' || coalesce(v_SeachKeyword,'') || '%'')';
         end if;

		  
      end if;
      if ((v_filter = 0 or v_filter > 2) and (v_tintSortOrder = 0 or v_tintSortOrder > 2)) then
 
         v_strSql := coalesce(v_strSql,'') || '; INSERT INTO tt_TEMPDATA SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,CONCAT(C.numCommId,''~2~'',C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then FormatedDateTimeFromDate(dtStartTime + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') 
  when c.bitTask=974 then FormatedDateTimeFromDate(dtStartTime  + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') 
  when c.bitTask=971 THEN FormatedDateFromDate(dtEndTime + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') || '' '' || CAST(dtStartTime + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS VARCHAR) || '' - '' ||  CAST(dtEndTime  + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS VARCHAR)
  else  FormatedDateFromDate(dtEndTime + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') || '' '' || CAST(dtStartTime + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS VARCHAR) || '' - '' || CAST(dtEndTime  + make_interval(mins =>  ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS VARCHAR)  END AS "date",
  textdetails as "Subject",                                                                         
 fn_GetListItemName(c.bitTask)  AS "Type",                                                                 
  CONCAT(A1.vcFirstName ,'' '',A1.vcLastName,'' '',                                     
  case when A1.numPhone<>'''' then A1.numPhone || case when A1.numPhoneExtension<>'''' then '' - '' || A1.numPhoneExtension else '''' end  else '''' end) as "Phone",                        
  CONCAT(A2.vcFirstName,'' '',A2.vcLastName)  as assignedto,                         
  c.caseid,                           
  (select vcCaseNumber from cases where cases.numcaseid= c.caseid LIMIT 1)as vcCasenumber,                               
  COALESCE(caseTimeid,0)as caseTimeid,                              
  COALESCE(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as "FROM"     ,COALESCE(C.bitClosedflag,false) as bitClosedflag,false as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''||CAST(c.bitTask AS VARCHAR)||''~''||CAST(c.numCommId AS VARCHAR)||''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and COALESCE(UM.bitActivateFlag,false)=true                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN Correspondence CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
         ||(CASE WHEN coalesce(v_vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM SplitIDs(''' || coalesce(v_vcTypes,'') || ''','',''))' ELSE '' END) ||
         ' AND (numOpenRecordID = ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         if (v_numdivisionId = 0 and v_numContactId <> 0) then 
            v_strSql := coalesce(v_strSql,'') || ' and C.numContactId=' || SUBSTR(CAST(v_numContactId AS VARCHAR(15)),1,15);
         end if;
         if v_numdivisionId <> 0 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (C.numDivisionId=' || SUBSTR(CAST(v_numdivisionId AS VARCHAR(15)),1,15) || ' OR clo.numDivisionID=' || SUBSTR(CAST(v_numdivisionId AS VARCHAR(15)),1,15) || ') ';
         end if;
         if v_numCaseID > 0 then  
            v_strSql := coalesce(v_strSql,'') || ' and  C.CaseId=' || SUBSTR(CAST(v_numCaseID AS VARCHAR(15)),1,15);
         end if;
         if v_filter = 0 then
  
            IF v_tintMode <> 9 then
	
               IF(v_FromDate IS NOT NULL AND v_FromDate <> '1900-01-01 00:00:00.000') then
		
                  v_strSql := coalesce(v_strSql,'') || ' and dtStartTime  >= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''' and dtStartTime  <= ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30)  || '''';
               end if;
            end if;
         else
            v_strSql := coalesce(v_strSql,'') || ' and c.bitTask=' || SUBSTR(CAST(v_filter AS VARCHAR(12)),1,12);
         end if;
         IF(v_FromDate IS NOT NULL AND v_FromDate <> '1900-01-01 00:00:00.000') then
		
            v_strSql := coalesce(v_strSql,'') || ' and ((dtStartTime  >= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''' and dtStartTime  <= ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30)  || ''') OR (dtEndTime  >= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || ''' and dtEndTime  <= ''' || SUBSTR(CAST(v_toDate AS VARCHAR(30)),1,30)  || '''))';
         end if;
         IF v_tintMode = 1 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(1,3) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         IF v_tintMode = 2 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(2,4) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         IF v_tintMode = 5 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(5) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         IF v_tintMode = 6 then 
            v_strSql := coalesce(v_strSql,'') || ' and  (CR.tintCorrType IN(6) OR ' || SUBSTR(CAST(v_numOpenRecordID AS VARCHAR(15)),1,15) || '=0)';
         end if;
         IF v_tintSortOrder = 0 and v_SeachKeyword <> '' then
            v_strSql := coalesce(v_strSql,'') || ' and textdetails iLike ''%' || coalesce(v_SeachKeyword,'') || '%''';
         ELSEIF coalesce(v_filter,0) = 0
         then
	
            IF v_SeachKeyword <> '' then
		
               v_strSql := coalesce(v_strSql,'') || ' and textdetails iLike ''%' || coalesce(v_SeachKeyword,'') || '%''';
            end if;
         ELSE
            v_strSql := coalesce(v_strSql,'') || ' and c.bitTask=' || SUBSTR(CAST(v_filter AS VARCHAR(12)),1,12);
            IF v_SeachKeyword <> '' then
		
               v_strSql := coalesce(v_strSql,'') || ' and textdetails iLike ''%' || coalesce(v_SeachKeyword,'') || '%''';
            end if;
         end if;
         IF v_bitOpenCommu = true then  
            v_strSql := coalesce(v_strSql,'') || ' and COALESCE(C.bitClosedflag,false)=false';
         end if;

		  RAISE NOTICE '%',CAST(v_strSql AS TEXT);

		 EXECUTE v_strSql;
      end if;                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

      v_strSql := CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM tt_TEMPDATA ORDER BY bintCreatedOn DESC OFFSET ',((v_CurrentPage::bigint -1)*v_PageSize::bigint),
      ' ROWS FETCH NEXT ',v_PageSize,
      ' ROWS ONLY;');
      RAISE NOTICE '%',CAST(v_strSql AS TEXT);
     
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

      v_TotRecs := COALESCE((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM tt_TEMPDATA GROUP BY tintCountType,numTotalRecords) X),0);
		
   end if;
   RETURN;
END; $$;


