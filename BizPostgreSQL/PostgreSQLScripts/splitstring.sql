-- Function definition script SplitString for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SplitString(v_List TEXT,
	v_Delimiter CHAR(1))
RETURNS TABLE 
(
   OutParam VARCHAR(500)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LeftSplit  VARCHAR(7998);
   v_SplitStart  INTEGER;
   v_SplitEnd  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_SPLITSTRING_RETURNTBL CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_SPLITSTRING_RETURNTBL
   (
      OutParam VARCHAR(500)
   );
   v_SplitStart := 0;
   v_SplitEnd := 7997;

   select   MAX(Number) INTO v_SplitEnd FROM Numbers WHERE
		(REPLACE(SUBSTR(v_List,Number,1),' ',CHR(255)) =
   REPLACE(v_Delimiter,' ',CHR(255))
   OR Number = OCTET_LENGTH(v_List)+1)
   AND Number BETWEEN v_SplitStart AND v_SplitEnd;

   WHILE v_SplitStart < v_SplitEnd LOOP
      v_LeftSplit :=
      coalesce(v_Delimiter,'') ||
      SUBSTR(v_List,v_SplitStart,v_SplitEnd -v_SplitStart) ||
      coalesce(v_Delimiter,'');
      INSERT INTO tt_SPLITSTRING_RETURNTBL(OutParam)
      SELECT
      LTRIM(RTRIM(SUBSTR(v_LeftSplit,Number+1,CASE WHEN POSITION(v_Delimiter IN SUBSTR(v_LeftSplit,Number+1)) > 0 THEN POSITION(v_Delimiter IN SUBSTR(v_LeftSplit,Number+1))+Number+1 -1 ELSE 0 END -Number -1))) AS Value
      FROM Numbers
      WHERE
      Number <= LENGTH(v_LeftSplit) -1
      AND REPLACE(SUBSTR(v_LeftSplit,Number,1),' ',CHR(255)) =
      REPLACE(v_Delimiter,' ',CHR(255))
      AND '' <>
      SUBSTR(v_LeftSplit,Number+1,CASE WHEN POSITION(v_Delimiter IN SUBSTR(v_LeftSplit,Number+1)) > 0 THEN POSITION(v_Delimiter IN SUBSTR(v_LeftSplit,Number+1))+Number+1 -1 ELSE 0 END -Number -1);
      v_SplitStart := v_SplitEnd::bigint+1;
      v_SplitEnd := v_SplitEnd::bigint+7997;
      select   MAX(Number)+v_SplitStart INTO v_SplitEnd FROM Numbers WHERE
			(REPLACE(SUBSTR(v_List,Number+v_SplitStart,1),' ',CHR(255)) =
      REPLACE(v_Delimiter,' ',CHR(255))
      OR Number+v_SplitStart = OCTET_LENGTH(v_List)+1)
      AND Number BETWEEN 1 AND v_SplitEnd::bigint -v_SplitStart::bigint;
   END LOOP;

   RETURN QUERY (SELECT * FROM tt_SPLITSTRING_RETURNTBL);
END; $$;

