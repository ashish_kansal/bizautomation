CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_GetOrderItems(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_vcSelectedRecords TEXT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintBillType  SMALLINT DEFAULT 1;
BEGIN
   IF EXISTS(SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
	
      select   coalesce(tintBillType,1) INTO v_tintBillType FROM
      MassPurchaseFulfillmentConfiguration WHERE
      numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntID;
   end if;

   IF coalesce(v_vcSelectedRecords,'') <> '' then
      DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMS
      (
         numOppItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPITEMS(numOppItemID,
			numUnitHour)
      SELECT
      CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS DOUBLE PRECISION)
      FROM
      SplitString(v_vcSelectedRecords,',');
      IF v_tintMode = 1 then -- For bill orders
		
         open SWV_RefCur for
         SELECT
         numoppitemtCode
				,(CASE WHEN coalesce(T1.numUnitHour,0) > 0
         THEN T1.numUnitHour
         ELSE(CASE
            WHEN v_tintBillType = 2
            THEN coalesce(OpportunityItems.numQtyReceived,0)
            WHEN v_tintBillType = 3 THEN coalesce(OpportunityItems.numUnitHourReceived,0)
            ELSE coalesce(OpportunityItems.numUnitHour,0)
            END) -coalesce((SELECT
               SUM(OpportunityBizDocItems.numUnitHour)
               FROM
               OpportunityBizDocs
               INNER JOIN
               OpportunityBizDocItems
               ON
               OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
               WHERE
               OpportunityBizDocs.numoppid = v_numOppID
               AND OpportunityBizDocs.numBizDocId = 644
               AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0)
         END) AS numUnitHour
				,monPrice
         FROM
         tt_TEMPITEMS T1
         INNER JOIN
         OpportunityItems
         ON
         T1.numOppItemID = OpportunityItems.numoppitemtCode
         INNER JOIN
         OpportunityMaster
         ON
         OpportunityItems.numOppId = OpportunityMaster.numOppId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND OpportunityMaster.numOppId = v_numOppID
         AND OpportunityMaster.tintopptype = 2
         AND OpportunityMaster.tintoppstatus = 1
         AND coalesce(OpportunityMaster.tintshipped,0) = 0
         AND(CASE
         WHEN v_tintBillType = 2
         THEN coalesce(OpportunityItems.numQtyReceived,0)
         WHEN v_tintBillType = 3 THEN coalesce(OpportunityItems.numUnitHourReceived,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 644
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) > 0;
      end if;
   ELSE
      IF v_tintMode = 1 then -- For bill orders
		
         open SWV_RefCur for
         SELECT
         numoppitemtCode
				,(CASE
         WHEN v_tintBillType = 2
         THEN coalesce(OpportunityItems.numQtyReceived,0)
         WHEN v_tintBillType = 3 THEN coalesce(OpportunityItems.numUnitHourReceived,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 644
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) AS numUnitHour
				,monPrice
         FROM
         OpportunityMaster
         INNER JOIN
         OpportunityItems
         ON
         OpportunityMaster.numOppId = OpportunityItems.numOppId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND OpportunityMaster.numOppId = v_numOppID
         AND OpportunityMaster.tintopptype = 2
         AND OpportunityMaster.tintoppstatus = 1
         AND coalesce(OpportunityMaster.tintshipped,0) = 0
         AND(CASE
         WHEN v_tintBillType = 2
         THEN coalesce(OpportunityItems.numQtyReceived,0)
         WHEN v_tintBillType = 3 THEN coalesce(OpportunityItems.numUnitHourReceived,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 644
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) > 0;
      ELSEIF v_tintMode = 2
      then -- For close order with auto receive and bill on
		
         open SWV_RefCur for
         SELECT
         OpportunityMaster.numOppId AS OppID
				,OpportunityMaster.vcpOppName AS OppName
				,OpportunityItems.numoppitemtCode AS OppItemID
				,(CASE WHEN coalesce(OpportunityMaster.bitStockTransfer,false) = true THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) AS WarehouseItemID
				,coalesce(OpportunityMaster.bitStockTransfer,false) AS IsStockTransfer
				,coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numUnitHourReceived,0) AS QtyToReceive
				,OpportunityItems.numItemCode AS ItemCode
				,Item.vcItemName AS ItemName
				,coalesce(bitSerialized,false) AS IsSerial
				,coalesce(bitLotNo,false) AS IsLot
				,'' AS SerialLot
				,coalesce(OpportunityMaster.bitPPVariance,false) AS IsPPVariance
				,OpportunityMaster.numDivisionId AS DivisionID
				,coalesce(OpportunityItems.monPrice,0) AS Price
				,coalesce(OpportunityMaster.numCurrencyID,0) AS CurrencyID
				,coalesce(OpportunityMaster.fltExchangeRate,0) AS ExchangeRate
				,Item.charItemType AS CharItemType
				,OpportunityItems.vcType AS ItemType
				,coalesce(OpportunityItems.bitDropShip,false) AS DropShip
				,coalesce(OpportunityItems.numProjectID,0) AS ProjectID
				,coalesce(OpportunityItems.numClassID,0) AS ClassID
				,coalesce(Item.numAssetChartAcntId,0) AS AssetChartAcntId
				,coalesce(Item.numCOGsChartAcntId,0) AS COGsChartAcntId
				,coalesce(Item.numIncomeChartAcntId,0) AS IncomeChartAcntId
         FROM
         OpportunityMaster
         INNER JOIN
         OpportunityItems
         ON
         OpportunityMaster.numOppId = OpportunityItems.numOppId
         INNER JOIN
         Item
         ON
         OpportunityItems.numItemCode = Item.numItemCode
         WHERE
         OpportunityItems.numOppId = v_numOppID
         AND(coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numUnitHourReceived,0)) > 0;
      end if;
   end if;
   RETURN;
END; $$;


