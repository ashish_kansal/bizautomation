-- Function definition script GetTotalAmountDue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalAmountDue(v_numUserId NUMERIC,v_dtStartDate TIMESTAMP,v_dtEndDate TIMESTAMP,v_numUserCntId NUMERIC,v_numDomainId NUMERIC,v_ClientTimeZoneOffset INTEGER)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltRegularWork  DOUBLE PRECISION;                        
   v_fltOverTimeWork  DOUBLE PRECISION;                        
   v_fltLimDailHrs  DOUBLE PRECISION;                        
   v_bitOverTime  BOOLEAN;                       
   v_bitOverTimeHrsDailyOrWeekly  BOOLEAN;                     
   v_monOverTimeRate  DECIMAL(20,5);                        
   v_monOverTimeAmt  DECIMAL(20,5);                        
   v_monCommissionAmt  DECIMAL(20,5);                        
   v_monTotalAmtDue  DECIMAL(20,5);                        
   v_monTotalExpenses  DECIMAL(20,5);                    
   v_monHourlyRate  DECIMAL(20,5);
BEGIN
   v_monHourlyRate := 0;                  
   v_monTotalExpenses := 0;    
   select   coalesce(monHourlyRate,0), coalesce(numLimDailHrs,0), coalesce(bitOverTime,false), coalesce(bitOverTimeHrsDailyOrWeekly,false), coalesce(monOverTimeRate,0) INTO v_monHourlyRate,v_fltLimDailHrs,v_bitOverTime,v_bitOverTimeHrsDailyOrWeekly,
   v_monOverTimeRate From UserMaster Where numUserId = v_numUserId;                        
----                   
---- Select  @fltRegularWork=Sum(Cast(datediff(minute,dtfromdate,dttodate) as float) *@monHourlyRate/Cast(60 as float))                   
----from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1  And numUserCntID=@numUserCntId And numDomainID=@numDomainId                        
   select   coalesce((Sum(Cast(monAmount as DOUBLE PRECISION))),0) INTO v_monTotalExpenses from timeandexpense where bitReimburse = false and  numCategory = 2
   And numtype in(1,2) And numUserCntID = v_numUserCntId And numDomainID = v_numDomainId
   and   dtFromDate between   v_dtStartDate and    v_dtEndDate;         
    
  
-- Set @monTotalExpenses = dbo.GetTotalExpense(@numUserCntId,@numDomainId,@dtStartDate,@dtEndDate)                  
   v_fltRegularWork := fn_GetTotalHrs(v_bitOverTime,v_bitOverTimeHrsDailyOrWeekly,v_fltLimDailHrs::DECIMAL,v_dtStartDate,
   v_dtEndDate,v_numUserCntId,v_numDomainId,0::BOOLEAN,v_ClientTimeZoneOffset);                
  --print @fltRegularWork                        
                          
   v_fltOverTimeWork := Cast(GetOverHrsWorked(v_bitOverTime,v_bitOverTimeHrsDailyOrWeekly,v_fltLimDailHrs::DECIMAL,v_dtStartDate,
   v_dtEndDate,v_numUserCntId,v_numDomainId,v_ClientTimeZoneOffset) as DOUBLE PRECISION);                         
   if v_bitOverTime = true and v_monOverTimeRate <> 0 then
      v_monOverTimeAmt := v_fltOverTimeWork*v_monOverTimeRate;
   end if;                        
   v_monCommissionAmt := GetCommissionAmountOfDuration(v_numUserId,v_numUserCntId,v_numDomainId,v_dtStartDate,v_dtEndDate,0::SMALLINT);
   v_monTotalAmtDue :=(v_fltRegularWork*v_monHourlyRate)+coalesce(v_monOverTimeAmt,0)+coalesce(v_monCommissionAmt,0)+coalesce(v_monTotalExpenses,0);                      
  --print @fltOverTimeWork                        
   return v_monTotalAmtDue;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[GetTotalBudgetBalanceForMonths]    Script Date: 07/26/2008 18:13:12 ******/

