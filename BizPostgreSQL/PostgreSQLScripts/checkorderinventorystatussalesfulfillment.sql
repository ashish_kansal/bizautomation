-- Function definition script CheckOrderInventoryStatusSalesFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckOrderInventoryStatusSalesFulfillment(v_numOppID NUMERIC(9,0),
      v_numDomainID NUMERIC(9,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcInventoryStatus  TEXT;
   v_ItemCount  INTEGER;
   v_BackOrderItemCount  INTEGER;
BEGIN
   v_vcInventoryStatus := '<font color="#000000">Not Applicable</font>';
	
   IF EXISTS(SELECT tintshipped FROM OpportunityMaster WHERE numDomainId = v_numDomainID
   AND numOppId = v_numOppID AND coalesce(tintshipped,0) = 1) OR(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
																			coalesce(OI.numUnitHour,0) AS OrderedQty,
																			coalesce(TempFulFilled.FulFilledQty,0) AS FulFilledQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND coalesce(bitFulFilled,false) = true) AS TempFulFilled on TRUE
      WHERE
      OI.numOppId = v_numOppID
      AND UPPER(I.charItemType) = 'P'
      AND coalesce(OI.bitDropShip,false) = false) X
   WHERE
   X.OrderedQty <> X.FulFilledQty) = 0 then
	
      v_vcInventoryStatus := '<font color="#008000">Allocation Cleared</font>';
   ELSE
      v_ItemCount := 0;
      v_BackOrderItemCount := 0;
      select   coalesce(COUNT(*),0), coalesce(SUM(CAST(bitBackOrder AS INTEGER)),0) INTO v_ItemCount,v_BackOrderItemCount FROM(SELECT(CASE
         WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true
         THEN 0
         WHEN coalesce(bitKitParent,false) = true THEN 1
         ELSE 0
         END) as bitKitParent,
			CASE WHEN CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,coalesce(Opp.numWarehouseItmsID,0),
         bitKitParent) > 0 THEN 1 ELSE 0 END AS bitBackOrder
         FROM OpportunityItems Opp
         JOIN Item I ON Opp.numItemCode = I.numItemCode
         JOIN WareHouseItems WItems ON Opp.numItemCode = WItems.numItemID
         AND WItems.numWareHouseItemID = Opp.numWarehouseItmsID
         WHERE numOppId = v_numOppID AND I.numDomainID = v_numDomainID AND charItemType = 'P'
         and (bitDropShip = false or bitDropShip is null)) AS TEMP;
      IF v_ItemCount > 0 then
		
         IF v_BackOrderItemCount > 0 then
            v_vcInventoryStatus := '<font color="#FF0000">Back Order (' || SUBSTR(CAST(v_BackOrderItemCount AS VARCHAR(18)),1,18) || '/' || SUBSTR(CAST(v_ItemCount AS VARCHAR(18)),1,18) || ')</font>';
         ELSE
            v_vcInventoryStatus := '<font color="#800080">Ready to Ship</font>';
         end if;
      end if;
   end if;
    
   RETURN coalesce(v_vcInventoryStatus,'');
END; $$;

