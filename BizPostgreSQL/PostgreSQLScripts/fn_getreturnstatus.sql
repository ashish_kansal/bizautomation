-- Function definition script fn_GetReturnStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetReturnStatus(v_numOppItemCode BIGINT)
RETURNS VARCHAR(20) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Status  VARCHAR(20);
BEGIN
   select   coalesce(GetListIemName(numreturnstatus),'') INTO v_Status FROM Returns WHERE numOppItemCode = v_numOppItemCode;



   RETURN v_Status;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_GetSlpId]    Script Date: 07/26/2008 18:12:44 ******/

