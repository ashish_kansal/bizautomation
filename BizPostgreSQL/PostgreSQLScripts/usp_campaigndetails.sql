-- Stored procedure definition script USP_CampaignDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CampaignDetails(v_numCampaignId NUMERIC(9,0) DEFAULT 0 ,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   vcCampaignName,
numCampaignStatus,
numCampaignType,
intLaunchDate,
intEndDate,
numRegion,
numNoSent,
monCampaignCost,
CAMP.numDomainID,
fn_GetContactName(CAMP.numCreatedBy) as CreatedBy,
CAST(CAMP.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as CreatedDate,
fn_GetContactName(CAMP.numModifiedBy) as ModifiedBy,
CAST(CAMP.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as ModifiedDate,
GetIncomeforCampaign(numCampaignID,1::SMALLINT,NULL,NULL) as TIP,
GetIncomeforCampaign(numCampaignID,2::SMALLINT,NULL,NULL) as TI,
GetIncomeforCampaign(numCampaignID,3::SMALLINT,NULL,NULL) as CostMFG,
GetIncomeforCampaign(numCampaignID,4::SMALLINT,NULL,NULL) as ROI,
GetIncomeforCampaign(numCampaignID,5::SMALLINT,NULL,NULL) as COGS,
GetIncomeforCampaign(numCampaignID,6::SMALLINT,NULL,NULL) as LaborCost,
coalesce(bitIsOnline,false) AS bitIsOnline,
coalesce(bitIsMonthly,false) AS bitIsMonthly,
coalesce(vcCampaignCode,'')  AS vcCampaignCode,
coalesce(bitActive,true) AS bitActive,
(SELECT SUM(monAmount) FROM BillDetails BD JOIN BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CAMP.numCampaignID GROUP BY numCampaignId) AS numTotalCampaignCost
   from CampaignMaster  CAMP 
--LEFT JOIN dbo.OpportunityMaster Opp ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainId        
   where numCampaignID = v_numCampaignId;

/****** Object:  StoredProcedure [dbo].[USP_CampaignList]    Script Date: 07/26/2008 16:14:59 ******/
   RETURN;
END; $$;
