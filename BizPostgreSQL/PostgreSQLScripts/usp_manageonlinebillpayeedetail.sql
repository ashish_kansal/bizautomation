-- Stored procedure definition script USP_ManageOnlineBillPayeeDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageOnlineBillPayeeDetail(v_numPayeeDetailId NUMERIC(18,0),
	v_vcPayeeFIId CHAR(10),
	v_vcPayeeFIListID VARCHAR(50),
	v_vcPayeeName VARCHAR(50),
	v_vcAccount VARCHAR(50),
	v_vcAddress1 VARCHAR(350),
	v_vcAddress2 VARCHAR(350),
	v_vcAddress3 VARCHAR(350),
	v_vcCity VARCHAR(50),
	v_vcState VARCHAR(50),
	v_vcPostalCode VARCHAR(50),
	v_vcPhone VARCHAR(50),
	v_vcCountry VARCHAR(50),
	v_numBankDetailId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN

   IF EXISTS(SELECT numPayeeDetailId FROM OnlineBillPayeeDetails WHERE numPayeeDetailId = v_numPayeeDetailId) then

      UPDATE OnlineBillPayeeDetails SET
      vcPayeeFIId = v_vcPayeeFIId,vcPayeeFIListID = v_vcPayeeFIListID,numBankDetailId = v_numBankDetailId,
      vcPayeeName = v_vcPayeeName,vcAccount = v_vcAccount,
      vcAddress1 = v_vcAddress1,vcAddress2 = v_vcAddress2,vcAddress3 = v_vcAddress3,
      vcCity = v_vcCity,vcState = v_vcState,vcPostalCode = v_vcPostalCode,
      vcPhone = v_vcPhone,vcCountry = v_vcCountry,dtModifiedDate = LOCALTIMESTAMP,
      bitIsActive = true
      WHERE
      numPayeeDetailId = v_numPayeeDetailId;
   ELSE
      select   COUNT(*) INTO v_Check FROM   OnlineBillPayeeDetails WHERE  vcPayeeFIListID = v_vcPayeeFIListID AND numBankDetailId = v_numBankDetailId;
      IF v_Check = 0 then

         INSERT INTO OnlineBillPayeeDetails(vcPayeeFIId,
		vcPayeeFIListID,
		numBankDetailId,
		vcPayeeName,
		vcAccount,
		vcAddress1,
		vcAddress2,
		vcAddress3,
		vcCity,
		vcState,
		vcPostalCode,
		vcPhone,
		vcCountry,
		dtCreatedDate,
		bitIsActive) VALUES(v_vcPayeeFIId,
		v_vcPayeeFIListID,
		v_numBankDetailId,
		v_vcPayeeName,
		v_vcAccount,
		v_vcAddress1,
		v_vcAddress2,
		v_vcAddress3,
		v_vcCity,
		v_vcState,
		v_vcPostalCode,
		v_vcPhone,
		v_vcCountry,
		LOCALTIMESTAMP ,
		true);
	
         v_numPayeeDetailId := CURRVAL('OnlineBillPayeeDetails_seq');
      end if;
   end if; 

   open SWV_RefCur for SELECT  v_numPayeeDetailId;
END; $$;













