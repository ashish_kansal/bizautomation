-- Stored procedure definition script USP_UpdateCheckNo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCheckNo(v_numDomainID NUMERIC(9,0),
v_numCheckNo NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Update Domain set numCheckNo = v_numCheckNo where numDomainId = v_numDomainID;
   RETURN;
END; $$;


