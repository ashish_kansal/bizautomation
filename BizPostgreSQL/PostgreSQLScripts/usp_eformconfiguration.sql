-- Stored procedure definition script USP_EFormConfiguration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EFormConfiguration(v_numSpecDocID NUMERIC(9,0) DEFAULT 0,
v_strSpecDocDetails TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc1  INTEGER;
BEGIN
   delete from EformConfiguration where numSpecDocID = v_numSpecDocID;
   insert into EformConfiguration(numSpecDocID,vcColumnName,vcEFormFld,VcFldType)
   select v_numSpecDocID,X.* 
   from
   XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strSpecDocDetails AS XML)
		COLUMNS
			ColumnName VARCHAR(100) PATH 'ColumnName',
			EformField VARCHAR(100) PATH 'EformField',
			FieldType VARCHAR(1) PATH 'FieldType'
	) X;  

   RETURN;
END; $$;


