-- Stored procedure definition script Usp_SaveProjectOpportunities for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_SaveProjectOpportunities(v_numProjectId NUMERIC,
    v_numDomainId NUMERIC,
    v_numOppID NUMERIC(9,0),
    v_numBillID NUMERIC(9,0) DEFAULT 0,
    v_numStageID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numStageID = 0 then
      v_numStageID := NULL;
   end if; 
    
   IF v_numBillID > 0 then
            
      INSERT  INTO ProjectsOpportunities(numProId,
                          numOppId,
                          numDomainId,
                          numOppBizDocID,
                          numBillId,
                          numStageID)
                VALUES(v_numProjectId,
                          NULL,
                          v_numDomainId,
                          NULL,
                          v_numBillID,
                          v_numStageID);
   ELSEIF v_numOppID > 0
   then
		
      INSERT  INTO ProjectsOpportunities(numProId,
                                          numOppId,
                                          numDomainId,
                                          numOppBizDocID,
                                          numBillId,
                                          numStageID)
                                VALUES(v_numProjectId,
                                          v_numOppID,
                                          v_numDomainId,
                                          NULL,
                                          NULL,
                                          v_numStageID);
   end if;
   RETURN;
END; $$;



