-- Stored procedure definition script USP_MasterList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MasterList(v_ListId NUMERIC(9,0) DEFAULT null,       
 v_bitDeleted BOOLEAN DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  Ld.numListItemID as "numListItemID", vcData as "vcData" FROM Listdetails Ld
   left join listorder LO on Ld.numListItemID = LO.numListItemID
   WHERE  Ld.numListID = v_ListId  order by intSortOrder,"numListItemID";
END; $$;












