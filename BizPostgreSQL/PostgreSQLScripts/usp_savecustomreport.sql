-- Stored procedure definition script usp_SaveCustomReport for PostgreSQL
CREATE OR REPLACE FUNCTION usp_SaveCustomReport(v_ReportId NUMERIC,                  
v_ReportName VARCHAR(200),                  
v_ReportDesc VARCHAR(200),                  
v_sql TEXT,                  
v_numDomainId NUMERIC(9,0),                  
v_numUserCntId NUMERIC(9,0) ,                 
v_ModuleId NUMERIC(9,0) ,                
v_GroupId NUMERIC(9,0),              
v_FilterRel BOOLEAN ,            
v_stdFieldId VARCHAR(200),            
v_custFilter VARCHAR(200),            
v_FromDate VARCHAR(200) DEFAULT '',            
v_ToDate VARCHAR(200) DEFAULT '',            
v_Rows NUMERIC(9,0) DEFAULT NULL  ,          
v_GridType BOOLEAN DEFAULT false,          
v_strGrpfld VARCHAR(200) DEFAULT '',           
v_strGrpOrd VARCHAR(200) DEFAULT '',           
v_strGrpflt VARCHAR(200) DEFAULT '' ,          
v_sqlGrp TEXT DEFAULT NULL  ,      
v_FilterType INTEGER DEFAULT NULL ,      
v_AdvFilterStr   VARCHAR(200) DEFAULT ''  ,    
v_OrderbyFld VARCHAR(200) DEFAULT NULL,    
v_Orderf VARCHAR(200) DEFAULT NULL  ,  
v_MttId INTEGER DEFAULT NULL ,    
v_MttValue VARCHAR(200) DEFAULT NULL  ,
v_CompFilter1  VARCHAR(200) DEFAULT NULL  ,
v_CompFilter2 VARCHAR(200) DEFAULT NULL  ,
v_CompFilterOpr VARCHAR(200) DEFAULT NULL,
v_bitDrillDown BOOLEAN DEFAULT NULL,
v_bitSortRecCount BOOLEAN DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
              
--bitGridType bit Unchecked          
--varGrpfld varchar(200) Checked          
--varGrpOrd varchar(200) Checked          
--varGrpflt varchar(200) Checked          
               
   if v_ReportId = 0 then
 
      insert into CustomReport(vcReportName,vcReportDescription,textQuery,numCreatedBy,numdomainId,numModifiedBy,numModuleId,numGroupId
    ,bitFilterRelAnd,varstdFieldId,custFilter,FromDate,ToDate,bintRows,bitGridType,varGrpfld,varGrpOrd,varGrpflt,textQueryGrp,
 FilterType,AdvFilterStr,OrderbyFld,Orderf,MttId,MttValue,CompFilter1,CompFilter2,CompFilterOpr,bitDrillDown,bitSortRecCount)
   values(v_ReportName,v_ReportDesc,v_sql,v_numUserCntId,v_numDomainId,v_numUserCntId,v_ModuleId,v_GroupId,v_FilterRel,v_stdFieldId,v_custFilter
	,CASE WHEN COALESCE(v_FromDate,'') = '' THEN NULL ELSE CAST(v_FromDate AS TIMESTAMP) END,CASE WHEN COALESCE(v_ToDate,'') = '' THEN NULL ELSE CAST(v_ToDate AS TIMESTAMP) END,v_Rows,v_GridType,v_strGrpfld,v_strGrpOrd,v_strGrpflt,v_sqlGrp,v_FilterType,v_AdvFilterStr,v_OrderbyFld,v_Orderf
	,v_MttId,v_MttValue,v_CompFilter1,v_CompFilter2,v_CompFilterOpr,v_bitDrillDown,v_bitSortRecCount);
    
      v_ReportId := CURRVAL('customreport_seq');
   else
      update CustomReport set
      vcReportName = v_ReportName,vcReportDescription = v_ReportDesc,textQuery = v_sql,
      numdomainId = v_numDomainId,numModifiedBy = v_numUserCntId,numModuleId =   v_ModuleId,
      numGroupId = v_GroupId,bitFilterRelAnd = v_FilterRel,
      varstdFieldId = v_stdFieldId,custFilter = v_custFilter,FromDate = CASE WHEN COALESCE(v_FromDate,'') = '' THEN NULL ELSE CAST(v_FromDate AS TIMESTAMP) END,
      ToDate = CASE WHEN COALESCE(v_ToDate,'') = '' THEN NULL ELSE CAST(v_ToDate AS TIMESTAMP) END,bintRows = v_Rows,
      bitGridType = v_GridType,varGrpfld = v_strGrpfld,varGrpOrd = v_strGrpOrd,
      varGrpflt = v_strGrpflt,textQueryGrp = v_sqlGrp,FilterType = v_FilterType,
      AdvFilterStr = v_AdvFilterStr,OrderbyFld = v_OrderbyFld,Orderf  = v_Orderf,
      MttId = v_MttId,MttValue = v_MttValue,CompFilter1 = v_CompFilter1,CompFilter2 = v_CompFilter2,
      CompFilterOpr = v_CompFilterOpr,bitDrillDown = v_bitDrillDown,
      bitSortRecCount = v_bitSortRecCount
      where numCustomReportId = v_ReportId;
   end if;                  
                  
   open SWV_RefCur for SELECT
   v_ReportId;
END; $$;












