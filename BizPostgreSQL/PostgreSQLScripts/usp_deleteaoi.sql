CREATE OR REPLACE FUNCTION usp_DeleteAOI(v_numAOIID NUMERIC,
	v_numUserID NUMERIC,
	v_bintTimeStamp BIGINT 
)
RETURNS VOID LANGUAGE plpgsql
	--This SP will update the Name changes of the AOIs.
   AS $$
BEGIN
   UPDATE AOIMaster
   SET bitDeleted = false,numModifiedBy = v_numUserID,bintmodifiedDate = v_bintTimeStamp
   WHERE numAOIID = v_numAOIID;
   RETURN;
END; $$;


