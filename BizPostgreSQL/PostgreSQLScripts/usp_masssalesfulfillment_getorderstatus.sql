-- Stored procedure definition script USP_MassSalesFulfillment_GetOrderStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetOrderStatus(v_numDomainID NUMERIC(18,0)
	,v_tintOppType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Listdetails.numListItemID AS "ListItemID"
		,Listdetails.vcData AS "ListItemValue"
		,COUNT(numOppId) AS "StatusCount"
   FROM
   Listdetails
   LEFT JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numStatus = Listdetails.numListItemID
   AND OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.tintopptype = v_tintOppType
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   WHERE
   Listdetails.numDomainid = v_numDomainID
   AND Listdetails.numListID = 176
   AND Listdetails.numListType = v_tintOppType
   AND (Listdetails.tintOppOrOrder = 2 OR coalesce(Listdetails.tintOppOrOrder,0) = 0)
   GROUP BY
   Listdetails.numListItemID,Listdetails.vcData,Listdetails.sintOrder
   ORDER BY
   Listdetails.sintOrder;
END; $$;












