-- Stored procedure definition script USP_WorkOrder_PickItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_PickItems(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_vcPickItems TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

   v_vcWorkOrder  VARCHAR(100);
   v_vcItemName  VARCHAR(300);
   v_numQtyToBuild  DOUBLE PRECISION;
   v_numBuildManager  NUMERIC(18,0);
   v_numWarehouseID  NUMERIC(18,0);
   v_numBuildPlan  NUMERIC(18,0);

   v_numOldPickedQty  DOUBLE PRECISION DEFAULT 0;
   v_numNewPickedQty  DOUBLE PRECISION DEFAULT 0;
   v_numWODetailID  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_numFromWarehouseItemID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;
   v_numQtyItemsReq_Orig  DOUBLE PRECISION;
   v_numQtyLeftToPicked  DOUBLE PRECISION;
   v_numQtyPicked  DOUBLE PRECISION;
   v_numOnHand  DOUBLE PRECISION;
   v_numOnOrder  DOUBLE PRECISION;
   v_numOnAllocation  DOUBLE PRECISION;
   v_numOnBackOrder  DOUBLE PRECISION;
   v_Description  VARCHAR(1000);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
BEGIN
   IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID AND numWOStatus = 23184) then
	
      RAISE EXCEPTION 'WORKORDER_COMPLETED';
      RETURN;
   end if;
	
   v_numOldPickedQty := coalesce((SELECT
   MIN(numPickedQty)
   FROM(SELECT
      WorkOrderDetails.numWODetailId
										,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig:: NUMERIC) AS numPickedQty
      FROM
      WorkOrderDetails
      LEFT JOIN
      WorkOrderPickedItems
      ON
      WorkOrderDetails.numWODetailId = WorkOrderPickedItems.numWODetailId
      WHERE
      WorkOrderDetails.numWOId = v_numWOID
      AND coalesce(WorkOrderDetails.numWarehouseItemID,0) > 0
      GROUP BY
      WorkOrderDetails.numWODetailId,WorkOrderDetails.numQtyItemsReq_Orig) TEMP),
   0);

   select   coalesce(vcWorkOrderName,'-'), vcItemName, numQtyItemsReq, coalesce(numAssignedTo,0), WareHouseItems.numWareHouseID, coalesce(numBuildPlan,0) INTO v_vcWorkOrder,v_vcItemName,v_numQtyToBuild,v_numBuildManager,v_numWarehouseID,
   v_numBuildPlan FROM
   WorkOrder
   INNER JOIN
   Item
   ON
   WorkOrder.numItemCode = Item.numItemCode
   INNER JOIN
   WareHouseItems
   ON
   WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID;

   BEGIN
      CREATE TEMP SEQUENCE tt_TableWOItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLEWOITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEWOITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0),
      numWODetailID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numQtyItemsReq_Orig DOUBLE PRECISION,
      numQtyLeftToPick DOUBLE PRECISION,
      numPickedQty DOUBLE PRECISION,
      numFromWarehouseItemID NUMERIC(18,0),
      bitSuccess BOOLEAN,
      vcErrorMessage VARCHAR(300)
   );

   IF LENGTH(coalesce(v_vcPickItems,'')) = 0 then
	
      INSERT INTO tt_TABLEWOITEMS(numWOID
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyLeftToPick)
      SELECT
      numWOID
			,numWODetailID
			,numChildItemID
			,numWarehouseItemID
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyItemsReq -coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId),0)
      FROM
      WorkOrderDetails
      WHERE
      numWOID = v_numWOID
      AND coalesce(numWarehouseItemID,0) > 0;
      select   COUNT(*) INTO v_iCount FROM tt_TABLEWOITEMS;
      BEGIN
         -- BEGIN TRANSACTION
WHILE v_i <= v_iCount LOOP
            select   numWODetailID, numItemCode, numWareHouseItemId, numQtyItemsReq, numQtyItemsReq_Orig, numQtyLeftToPick INTO v_numWODetailID,v_numItemCode,v_numWarehouseItemID,v_numQtyItemsReq,v_numQtyItemsReq_Orig,
            v_numQtyLeftToPicked FROM
            tt_TABLEWOITEMS WHERE
            ID = v_i;

				--GET CHILD ITEM CURRENT INVENTORY DETAIL
            select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
            WareHouseItems WHERE
            numWarehouseItemID = v_numWarehouseItemID;
            IF v_numOnAllocation > 0 AND v_numQtyLeftToPicked > 0 then
				
               IF v_numOnAllocation >= v_numQtyLeftToPicked then
					
                  v_Description := CONCAT('Items Picked For Work Order (Qty:',v_numQtyLeftToPicked,')');
                  v_numQtyPicked := v_numQtyLeftToPicked;
                  v_numOnAllocation := v_numOnAllocation -v_numQtyLeftToPicked;
               ELSE
                  v_numQtyPicked := FLOOR(v_numOnAllocation/v_numQtyItemsReq_Orig)*v_numQtyItemsReq_Orig;
                  v_Description := CONCAT('Items Picked For Work Order (Qty:',v_numQtyPicked,')');
                  v_numOnAllocation := v_numOnAllocation -(FLOOR(v_numOnAllocation/v_numQtyItemsReq_Orig)*v_numQtyItemsReq_Orig);
               end if;
               PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
               v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numWarehouseItemID,
               v_numReferenceID := v_numWOID,v_tintRefType := 2::SMALLINT,
               v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
               v_Description := v_Description);
               INSERT INTO WorkOrderPickedItems(numWODetailId
						,numPickedQty
						,numFromWarehouseItemID
						,numToWarehouseItemID
						,dtCreatedDate
						,numCreatedBy)
					VALUES(v_numWODetailID
						,v_numQtyPicked
						,v_numWarehouseItemID
						,v_numWarehouseItemID
						,TIMEZONE('UTC',now())
						,v_numUserCntID);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            -- ROLLBACK TRANSACTION
 GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   ELSE
      INSERT INTO tt_TABLEWOITEMS(numWOID
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numPickedQty
			,numFromWarehouseItemID
			,bitSuccess
			,vcErrorMessage)
      SELECT
      WorkOrderDetails.numWOId
			,TEMP.numWODetailID
			,TEMP.numItemCode
			,TEMP.numWareHouseItemId
			,WorkOrderDetails.numQtyItemsReq
			,WorkOrderDetails.numQtyItemsReq_Orig
			,TEMP.numPickedQty
			,TEMP.numFromWarehouseItemID
			,CAST(1 AS BOOLEAN)
			,CAST('' AS VARCHAR(300))
      FROM(SELECT
				numWODetailID
				,numItemCode
				,numWarehouseItemID
				,numPickedQty
				,numFromWarehouseItemID
			FROM  XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcPickItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numWODetailID NUMERIC(18,0) PATH 'numWODetailID'
					,numItemCode NUMERIC(18,0) PATH 'numItemCode'
					,numWarehouseItemID NUMERIC(18,0) PATH 'numWarehouseItemID'
					,numPickedQty DOUBLE PRECISION PATH 'numPickedQty'
					,numFromWarehouseItemID NUMERIC(18,0) PATH 'numFromWarehouseItemID'
			)) TEMP
      INNER JOIN
      WorkOrderDetails
      ON
      TEMP.numWODetailID = WorkOrderDetails.numWODetailId;

      select   COUNT(*) INTO v_iCount FROM tt_TABLEWOITEMS;
      BEGIN
         -- BEGIN TRANSACTION
WHILE v_i <= v_iCount LOOP
            select   numWOID, numWODetailID, numItemCode, numWareHouseItemId, numQtyItemsReq, numQtyItemsReq_Orig, numFromWarehouseItemID, numPickedQty INTO v_numWOID,v_numWODetailID,v_numItemCode,v_numWarehouseItemID,v_numQtyItemsReq,
            v_numQtyItemsReq_Orig,v_numFromWarehouseItemID,v_numQtyPicked FROM
            tt_TABLEWOITEMS WHERE
            ID = v_i;
            IF(v_numQtyPicked+coalesce((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId = v_numWODetailID),0)) > v_numQtyItemsReq then
				
               UPDATE tt_TABLEWOITEMS SET bitSuccess = false,vcErrorMessage = 'Picked quantity can not be greater then quantity left to be picked.' WHERE ID = v_i;
            ELSE
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
               select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
               WareHouseItems WHERE
               numWarehouseItemID = v_numFromWarehouseItemID;
               IF v_numFromWarehouseItemID = v_numWarehouseItemID then
					
                  IF v_numOnAllocation > 0 AND v_numOnAllocation >= v_numQtyPicked then
						
                     v_Description := CONCAT('Items Picked For Work Order (Qty:',v_numQtyPicked,')');
                     v_numOnAllocation := v_numOnAllocation -v_numQtyPicked;
                     PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                     v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numFromWarehouseItemID,
                     v_numReferenceID := v_numWOID,
                     v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                     v_Description := v_Description);
                     INSERT INTO WorkOrderPickedItems(numWODetailId
								,numPickedQty
								,numFromWarehouseItemID
								,numToWarehouseItemID
								,dtCreatedDate
								,numCreatedBy)
							VALUES(v_numWODetailID
								,v_numQtyPicked
								,v_numWarehouseItemID
								,v_numWarehouseItemID
								,TIMEZONE('UTC',now())
								,v_numUserCntID);
                  ELSE
                     UPDATE tt_TABLEWOITEMS SET bitSuccess = false,vcErrorMessage = 'Picked quantity can not be greater then quantity on allocation.' WHERE ID = v_i;
                  end if;
               ELSE
                  IF(v_numOnHand+v_numOnAllocation) > 0 AND(v_numOnHand+v_numOnAllocation) >= v_numQtyPicked then
						
                     v_Description := CONCAT('Items Transferred For Work Order (Qty:',v_numQtyPicked,')');
                     IF v_numOnHand >= v_numQtyPicked then
							
                        v_numOnHand := v_numOnHand -v_numQtyPicked;
                     ELSE
                        v_numOnBackOrder := v_numOnBackOrder+(v_numQtyPicked -v_numOnHand);
                        v_numOnAllocation := v_numOnAllocation -(v_numQtyPicked -v_numOnHand);
                        v_numOnHand := 0;
                     end if;
                     PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                     v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numFromWarehouseItemID,
                     v_numReferenceID := v_numWOID,
                     v_tintRefType := 2::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                     v_Description := v_Description);
                     select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_numOnHand,v_numOnAllocation,v_numOnOrder,v_numOnBackOrder FROM
                     WareHouseItems WHERE
                     numWarehouseItemID = v_numWarehouseItemID;
                     IF v_numOnBackOrder > 0 then
							
                        IF v_numOnBackOrder > v_numQtyPicked then
								
                           v_numOnBackOrder := v_numOnBackOrder -v_numQtyPicked;
                        ELSE
                           v_numOnBackOrder := 0;
                        end if;
                     end if;
                     v_numOnAllocation := v_numOnAllocation+v_numQtyPicked;
                     v_Description := CONCAT('Items Received For Work Order (Qty:',v_numQtyPicked,')');
                     PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                     v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numWarehouseItemID,
                     v_numReferenceID := v_numWOID,v_tintRefType := 2::SMALLINT,
                     v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                     v_Description := v_Description);
                     v_numOnAllocation := v_numOnAllocation -v_numQtyPicked;
                     v_Description := CONCAT('Items Picked For Work Order (Qty:',v_numQtyPicked,')');
                     PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numOnHand,v_numOnAllocation := v_numOnAllocation,v_numOnBackOrder := v_numOnBackOrder,
                     v_numOnOrder := v_numOnOrder,v_numWarehouseItemID := v_numWarehouseItemID,
                     v_numReferenceID := v_numWOID,v_tintRefType := 2::SMALLINT,
                     v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
                     v_Description := v_Description);
                     INSERT INTO WorkOrderPickedItems(numWODetailId
								,numPickedQty
								,numFromWarehouseItemID
								,numToWarehouseItemID
								,dtCreatedDate
								,numCreatedBy)
							VALUES(v_numWODetailID
								,v_numQtyPicked
								,v_numFromWarehouseItemID
								,v_numWarehouseItemID
								,TIMEZONE('UTC',now())
								,v_numUserCntID);
                  ELSE
                     UPDATE tt_TABLEWOITEMS SET bitSuccess = false,vcErrorMessage = 'Picked quantity can not be greater then available quantity.' WHERE ID = v_i;
                  end if;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            -- ROLLBACK TRANSACTION
   GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
   end if;

   open SWV_RefCur for SELECT 
		ID AS "ID",
      numWOID AS "numWOID",
      numWODetailID AS "numWODetailID",
      numItemCode AS "numItemCode",
      numWarehouseItemID AS "numWarehouseItemID",
      numQtyItemsReq AS "numQtyItemsReq",
      numQtyItemsReq_Orig AS "numQtyItemsReq_Orig",
      numQtyLeftToPick AS "numQtyLeftToPick",
      numPickedQty AS "numPickedQty",
      numFromWarehouseItemID AS "numFromWarehouseItemID",
      bitSuccess AS "bitSuccess",
      vcErrorMessage AS "vcErrorMessage" FROM tt_TABLEWOITEMS;
END; $$;
