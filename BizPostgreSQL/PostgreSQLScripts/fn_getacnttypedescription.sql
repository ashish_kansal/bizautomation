-- Function definition script fn_GetAcntTypeDescription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetAcntTypeDescription(v_numId VARCHAR(100),v_CharType CHAR(3))
RETURNS VARCHAR(5000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);    
   v_numTransactionId  NUMERIC(9,0);    
   v_varAcntType  VARCHAR(5000);    
   v_AcntTypeId  VARCHAR(40);    
   v_AcntTypeDescription  VARCHAR(500);    
   v_lenAcntType  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   v_varAcntType := '';    
   v_AcntTypeDescription := '';    
   if v_CharType = 'C' then
      select   numJOurnal_Id INTO v_numJournalId from General_Journal_Header Where numCheckId = cast(NULLIF(v_numId,'') as NUMERIC(18,0));
   ELSEIF v_CharType = 'CC'
   then
      select   numJOurnal_Id INTO v_numJournalId from General_Journal_Header Where numCashCreditCardId = cast(NULLIF(v_numId,'') as NUMERIC(18,0));
   ELSEIF v_CharType = 'JD'
   then
      select   numJOurnal_Id INTO v_numJournalId from General_Journal_Header Where numJOurnal_Id = cast(NULLIF(v_numId,'') as NUMERIC(18,0));
   ELSEIF v_CharType = 'DD'
   then
      select   numJOurnal_Id INTO v_numJournalId from General_Journal_Header Where numDepositId = cast(NULLIF(v_numId,'') as NUMERIC(18,0));
   ELSEIF v_CharType = 'Opp'
   then
      select   numJOurnal_Id INTO v_numJournalId from General_Journal_Header Where numOppId = cast(NULLIF(v_numId,'') as NUMERIC(18,0));
   end if;  
   select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Details Where numJournalId = v_numJournalId;     
   While v_numTransactionId > 0 LOOP
      select   numChartAcntId INTO v_AcntTypeId From General_Journal_Details Where numTransactionId = v_numTransactionId;
      select   vcAccountName INTO v_AcntTypeDescription From Chart_Of_Accounts Where numAccountId = cast(NULLIF(v_AcntTypeId,'') as NUMERIC(18,0));
      v_varAcntType := coalesce(v_varAcntType,'') || coalesce(v_AcntTypeDescription,'') || ',';
      select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Details Where numTransactionId > v_numTransactionId And numJournalId = v_numJournalId;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then
         v_numTransactionId := 0;
      end if;
   END LOOP;    
   v_lenAcntType := LENGTH(v_varAcntType);        
   if v_lenAcntType > 0 then

      v_varAcntType := SUBSTR(v_varAcntType,1,v_lenAcntType -1);
   end if;    
    
   return v_varAcntType;
END; $$;

