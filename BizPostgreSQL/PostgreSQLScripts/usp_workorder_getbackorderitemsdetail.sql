DROP FUNCTION IF EXISTS USP_WorkOrder_GetBackOrderItemsDetail;

CREATE OR REPLACE FUNCTION USP_WorkOrder_GetBackOrderItemsDetail(v_numDomainID NUMERIC(18,0)
	,v_vcItems TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintDefaultCost  NUMERIC(18,0);
   v_bitReOrderPoint  BOOLEAN;
   v_numReOrderPointOrderStatus  NUMERIC(18,0);
   v_tintOppStautsForAutoPOBackOrder  SMALLINT;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT;
   v_bitIncludeRequisitions  BOOLEAN;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGetBackOrderItemsDetail CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetBackOrderItemsDetail
   (
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMPGetBackOrderItemsDetail(numItemCode
		,numWarehouseItemID)
   SELECT
   CAST(SUBSTR(items,0,POSITION('-' IN items)) AS NUMERIC),
		CAST(SUBSTR(items,POSITION('-' IN items)+1,LENGTH(items) -POSITION('-' IN items)) AS NUMERIC)
   FROM
   Split(v_vcItems,',');


   select   coalesce(numCost,0), coalesce(bitReOrderPoint,false), coalesce(numReOrderPointOrderStatus,0), coalesce(tintOppStautsForAutoPOBackOrder,0), coalesce(tintUnitsRecommendationForAutoPOBackOrder,1), coalesce(bitIncludeRequisitions,false) INTO v_tintDefaultCost,v_bitReOrderPoint,v_numReOrderPointOrderStatus,v_tintOppStautsForAutoPOBackOrder,
   v_tintUnitsRecommendationForAutoPOBackOrder,
   v_bitIncludeRequisitions FROM
   Domain WHERE
   numDomainId = v_numDomainID;


   open SWV_RefCur for
   SELECT
   v_numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,v_tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,v_tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder;

   open SWV_RefCur2 for
   SELECT
   I.numItemCode
		,CONCAT(I.vcItemName,CASE WHEN fn_GetAttributes(T1.numWarehouseItemID,0::BOOLEAN) <> '' THEN CONCAT(' (',fn_GetAttributes(T1.numWarehouseItemID,0::BOOLEAN),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN coalesce(I.numItemGroup,0) > 0 AND bitMatrix = true THEN coalesce(WI.vcWHSKU,coalesce(I.vcSKU,''))  ELSE coalesce(I.vcSKU,'') END) AS vcSKU
		,V.vcNotes
		,I.charItemType
		,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),
   '') AS vcPathForTImage
		,T1.numWarehouseItemID
		,(CASE
   WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 1 --Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On Order + Requisitions”
   THEN(CASE WHEN coalesce(I.fltReorderQty,0) > coalesce(V.intMinQty,0) THEN coalesce(I.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)+coalesce(WI.numBackOrder,0) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
            SUM(numUnitHour)
            FROM
            OpportunityItems
            INNER JOIN
            OpportunityMaster
            ON
            OpportunityItems.numOppId = OpportunityMaster.numOppId
            WHERE
            OpportunityMaster.numDomainId = v_numDomainID
            AND OpportunityMaster.tintopptype = 2
            AND OpportunityMaster.tintoppstatus = 0
            AND OpportunityItems.numItemCode = T1.numItemCode
            AND OpportunityItems.numWarehouseItmsID = T1.numWarehouseItemID),0) ELSE 0 END))
   WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 2 --Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On Order + Requisitions”
   THEN(CASE
      WHEN coalesce(I.fltReorderQty,0) >= coalesce(V.intMinQty,0) AND coalesce(I.fltReorderQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(I.fltReorderQty,0)
      WHEN coalesce(V.intMinQty,0) >= coalesce(I.fltReorderQty,0) AND coalesce(V.intMinQty,0) >= coalesce(WI.numBackOrder,0) THEN coalesce(V.intMinQty,0)
      WHEN coalesce(WI.numBackOrder,0) >= coalesce(I.fltReorderQty,0) AND coalesce(WI.numBackOrder,0) >= coalesce(V.intMinQty,0) THEN coalesce(WI.numBackOrder,0)
      ELSE coalesce(I.fltReorderQty,0)
      END) -(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
            SUM(numUnitHour)
            FROM
            OpportunityItems
            INNER JOIN
            OpportunityMaster
            ON
            OpportunityItems.numOppId = OpportunityMaster.numOppId
            WHERE
            OpportunityMaster.numDomainId = v_numDomainID
            AND OpportunityMaster.tintopptype = 2
            AND OpportunityMaster.tintoppstatus = 0
            AND OpportunityItems.numItemCode = T1.numItemCode
            AND OpportunityItems.numWarehouseItmsID = T1.numWarehouseItemID),
         0) ELSE 0 END))
   WHEN v_tintUnitsRecommendationForAutoPOBackOrder = 3 --Add “On-Order Qty” “ + Requisitions” - “Back Order” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater)
   THEN(coalesce(numOnOrder,0)+(CASE WHEN v_bitIncludeRequisitions = true THEN coalesce((SELECT
            SUM(numUnitHour)
            FROM
            OpportunityItems
            INNER JOIN
            OpportunityMaster
            ON
            OpportunityItems.numOppId = OpportunityMaster.numOppId
            WHERE
            OpportunityMaster.numDomainId = v_numDomainID
            AND OpportunityMaster.tintopptype = 2
            AND OpportunityMaster.tintoppstatus = 0
            AND OpportunityItems.numItemCode = T1.numItemCode
            AND OpportunityItems.numWarehouseItmsID = T1.numWarehouseItemID),
         0) ELSE 0 END)) -coalesce(WI.numBackOrder,0)+(CASE
      WHEN coalesce(I.fltReorderQty,0) >= coalesce(V.intMinQty,0) THEN coalesce(I.fltReorderQty,0) ELSE coalesce(V.intMinQty,0) END)
   ELSE coalesce(WI.numBackOrder,0)+coalesce(V.intMinQty,0)
   END) AS numUnitHour
		,coalesce(WI.numBackOrder,0) AS numBackOrder
		,coalesce(I.numBaseUnit,0) AS numUOMID
		,1 AS fltUOMConversionFactor
		,coalesce(I.numVendorID,0) AS numVendorID
		,CASE WHEN v_tintDefaultCost = 3 THEN(coalesce(V.monCost,0)/fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) WHEN coalesce(v_tintDefaultCost,0) = 2 THEN(coalesce(V.monCost,0)/fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE coalesce(I.monAverageCost,0) END AS monCost
		,coalesce(V.intMinQty,0) AS intMinQty
		,coalesce(v_tintDefaultCost,0) AS tintDefaultCost
		,coalesce(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
   FROM
   tt_TEMPGetBackOrderItemsDetail T1
   INNER JOIN
   Item I
   ON
   T1.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   T1.numWarehouseItemID = WI.numWareHouseItemID
   LEFT JOIN
   Vendor V
   ON
   V.numVendorID = I.numVendorID
   AND I.numItemCode = V.numItemCode
   WHERE
   I.numDomainID = v_numDomainID
   AND coalesce(v_bitReOrderPoint,false) = true
   AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
   AND coalesce(I.bitAssembly,false) = false
   AND coalesce(I.bitKitParent,false) = false;
   RETURN;
END; $$;


