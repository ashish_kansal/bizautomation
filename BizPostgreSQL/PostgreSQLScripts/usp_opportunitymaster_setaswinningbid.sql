-- Stored procedure definition script USP_OpportunityMaster_SetAsWinningBid for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_SetAsWinningBid(v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
    v_numOppItemID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE OpportunityItems SET bitWinningBid = false WHERE numOppId = v_numOppID;
   UPDATE OpportunityItems SET bitWinningBid = true WHERE numOppId = v_numOppID AND numoppitemtCode = v_numOppItemID;
   RETURN;
END; $$;


