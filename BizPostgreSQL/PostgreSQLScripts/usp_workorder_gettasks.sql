CREATE OR REPLACE FUNCTION USP_WorkOrder_GetTasks(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_numStageDetailsId NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numQtyToBuild  DOUBLE PRECISION;
   v_dtPlannedStartDate  TIMESTAMP;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTaskID  NUMERIC(18,0);
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numWorkScheduleID  NUMERIC(18,0);
   v_numTempUserCntID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_bitParallelStartSet  BOOLEAN DEFAULT false;
BEGIN
	IF v_tintMode = 1 then
		select 
			numQtyItemsReq,coalesce(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) INTO v_numQtyToBuild,v_dtPlannedStartDate 
		FROM
			WorkOrder 
		WHERE
			WorkOrder.numDomainId = v_numDomainID
			AND WorkOrder.numWOId = v_numWOID;

		DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
		CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
		(
			numAssignedTo NUMERIC(18,0),
			dtLastTaskCompletionTime TIMESTAMP
		);
		BEGIN
			CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
			EXCEPTION WHEN OTHERS THEN
			NULL;
		END;

		DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
		CREATE TEMPORARY TABLE tt_TEMPTASKS
		(
			ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
			numTaskID NUMERIC(18,0),
			numTaskTimeInMinutes NUMERIC(18,0),
			numTaskAssignee NUMERIC(18,0),
			intTaskType INTEGER --1:Parallel, 2:Sequential
			,
			dtPlannedStartDate TIMESTAMP
		);

		INSERT INTO tt_TEMPTASKS
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT
			SPDT.numTaskId
			,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*v_numQtyToBuild
			,coalesce(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE
			SPDT.numDomainID = v_numDomainID
			AND SPDT.numWorkOrderId = v_numWOID;

		UPDATE 
			tt_TEMPTASKS 
		SET 
			dtPlannedStartDate = v_dtPlannedStartDate;

		INSERT INTO tt_TEMPTASKASSIGNEE
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			tt_TEMPTASKS;

		select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

		WHILE v_i <= v_iCount LOOP
			select  
				numTaskID, numTaskAssignee, numTaskTimeInMinutes, intTaskType INTO v_numTaskID,v_numTaskAssignee,v_numTotalTaskInMinutes,v_intTaskType 
			FROM
				tt_TEMPTASKS 
			WHERE
				ID = v_i;

			IF NOT EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = v_numTaskID AND tintAction = 4) then
				-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
				SELECT 
					WS.ID
					,(coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0)
					,tmStartOfDay
					,CONCAT(',',vcWorkDays,',')
					,CAST(TO_CHAR(v_dtPlannedStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval) 
				INTO 
					v_numWorkScheduleID
					,v_numProductiveTimeInMinutes
					,v_tmStartOfDay
					,v_vcWorkDays
					,v_dtStartDate 
				FROM
					WorkSchedule WS
				INNER JOIN
					UserMaster
				ON
					WS.numUserCntID = UserMaster.numUserDetailId WHERE
					WS.numUserCntID = v_numTaskAssignee;

				IF v_intTaskType = 1 AND EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
					select dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
				ELSEIF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
					select dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
				end if;

				UPDATE tt_TEMPTASKS SET dtPlannedStartDate = v_dtStartDate WHERE ID = v_i;
				
				IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 then
					WHILE v_numTotalTaskInMinutes > 0 LOOP
						-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
						IF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtStartDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
							IF CAST(v_dtStartDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE) then
								-- CHECK TIME LEFT FOR DAY BASED
								v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
								'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
								'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
								'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));
							ELSE
								v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
							end if;
							
							IF v_numTimeLeftForDay > 0 then
								IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
									v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
								ELSE
								   v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
								end if;

								v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
							 ELSE
								v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
							 end if;
						  ELSE
							 v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
						  end if;
					END LOOP;
				end if;
            
				UPDATE tt_TEMPTASKASSIGNEE SET dtLastTaskCompletionTime = v_dtStartDate WHERE numAssignedTo = v_numTaskAssignee;
			end if;
         
			v_i := v_i::bigint+1;
		END LOOP;

		open SWV_RefCur for
		SELECT
			SPDT.numTaskId AS "numTaskId"
			,SPDT.numAssignTo AS "numAssignTo"
			,SPDT.numReferenceTaskId AS "numReferenceTaskId"
			,SPDT.vcTaskName AS "vcTaskName"
			,fn_GetContactName(SPDT.numAssignTo) AS "vcAssignedTo" 
			,'Work Station 1' AS "vcWorkStation"
			,CASE
			  WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4) THEN coalesce(WO.numQtyItemsReq,0)
			  ELSE coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId),0)
			  END AS "numProcessedQty"
			,(CASE
			  WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4) THEN 0
			  ELSE coalesce(WO.numQtyItemsReq,0) -coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId),0)
			  END) AS numRemainingQty
			,CASE coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),0)
			  WHEN 4 THEN '<img src="../images/comflag.png" />'
			  WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,
				 ');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,',
				 SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
			  WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,
				 ');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,',SPDT.numTaskId,
				 ',1);">Resume</button></li></ul>')
			  WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,
				 ');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,',
				 SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,',
				 SPDT.numTaskId,',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
			  ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,
				 ');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,',
				 SPDT.numTaskId,',0);">Start</button></li></ul>')
			  END  AS "vcTaskControls"
			,TO_CHAR(FLOOR((((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*v_numQtyToBuild)/60),'00') || ':' || TO_CHAR(MOD((((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*CAST(v_numQtyToBuild AS DECIMAL)),60),'00')  AS "vcEstimatedTaskTime"
			,(((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*v_numQtyToBuild) AS "numTaskEstimationInMinutes"
			,(CASE coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1),0)
			  WHEN 4 THEN 0
			  WHEN 3 THEN cast(GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2::SMALLINT) as INTEGER)
			  WHEN 2 THEN cast(GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2::SMALLINT) as INTEGER)
			  WHEN 1 THEN 0
			  END) AS "numTimeSpentInMinutes"
			,(CASE coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),
			  0)
			  WHEN 4 THEN NULL
			  WHEN 3 THEN(SELECT  dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  WHEN 2 THEN NULL
			  WHEN 1 THEN(SELECT  dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  END) AS "dtLastStartDate"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)
			  THEN GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1::SMALLINT)
			  ELSE ''
			  END AS "vcActualTaskTimeHtml"
			,(CASE WHEN GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0::SMALLINT) = 'Invalid time sequence' THEN '00:00' ELSE GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0::SMALLINT) END) AS "vcActualTaskTime"
			,CASE
			  WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)
			  THEN CONCAT('<span>',FormatedDateTimeFromDate((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID),'</span>')
			  ELSE CONCAT('<i style="color:#a6a6a6">',FormatedDateTimeFromDate((CASE
				 WHEN TT.dtPlannedStartDate IS NULL
				 THEN coalesce(WO.dtmStartDate,WO.bintCreatedDate)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
				 ELSE TT.dtPlannedStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
				 END),v_numDomainID),
				 ' (planned)</i>')
			  END AS "dtPlannedStartDate"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)
			  THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  ELSE '1900-01-01'
			  END AS "dtStartDate"
					,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)
			  THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  ELSE '1900-01-01'
			  END AS "dtFinishDate"
					,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)
			  THEN FormatedDateTimeFromDate((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID)
			  ELSE ''
			  END AS "vcFinishDate"
					,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)
			  THEN true
			  ELSE false
			  END AS "bitTaskCompleted"
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			tt_TEMPTASKS TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = v_numStageDetailsId
			AND SPDT.numWorkOrderId = v_numWOID;
	ELSEIF v_tintMode = 2
	then
		open SWV_RefCur for
		SELECT
			SPDT.numTaskId AS "numTaskId"
			,SPDT.numReferenceTaskId AS "numReferenceTaskId"
			,SPDT.numStageDetailsId AS "numStageDetailsId"
			,SPDT.vcTaskName AS "vcTaskName"
			,coalesce(SPDT.numHours,0) AS "numHours"
			,coalesce(SPDT.numMinutes,0) AS "numMinutes"
			,SPDT.numAssignTo AS "numAssignTo"
			,ADC.numTeam AS "numTeam"
			,CONCAT(TO_CHAR(coalesce(SPDT.numHours,0),'00'),':',TO_CHAR(coalesce(SPDT.numMinutes,0),'00')) AS "vcEstimatedTaskTime"
			,(CASE WHEN GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0::SMALLINT) = 'Invalid time sequence' THEN '00:00' ELSE GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0::SMALLINT) END) AS "vcActualTaskTime"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)
			  THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 1)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  ELSE '1900-01-01'
			  END AS "dtStartDate"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)
			  THEN(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId AND tintAction = 4)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
			  ELSE '1900-01-01'
			  END AS "dtFinishDate"
			,CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID = SPDT.numTaskId)
			  THEN true
			  ELSE false
			  END AS "bitTaskStarted"
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		WHERE
			SPDT.numStageDetailsId = v_numStageDetailsId
			AND SPDT.numWorkOrderId = v_numWOID;
	end if;

	RETURN;
END; $$;


