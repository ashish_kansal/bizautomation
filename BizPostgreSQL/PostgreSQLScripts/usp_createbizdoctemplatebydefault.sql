-- Stored procedure definition script USP_CreateBizDocTemplateByDefault for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE OR REPLACE FUNCTION USP_CreateBizDocTemplateByDefault(v_numDomainID NUMERIC(9,0) ,
    v_txtBizDocTemplate TEXT,
    v_txtCss TEXT,
    v_tintMode SMALLINT DEFAULT 0, --1=create default template for all domain, 2 = create default template for selected domain, 3 = update template data for selected domain where its empty,4 = repeate same as 3 for all domains
    v_txtPackingSlipBizDocTemplate TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_maxDomainID  NUMERIC(9,0);
   v_minDomainID  NUMERIC(9,0);
BEGIN
   IF v_tintMode = 1 then
   	
--   		DECLARE @numDomainID NUMERIC

      select   max(numDomainId) INTO v_maxDomainID FROM   Domain WHERE numDomainId > 0;
      select   min(numDomainId) INTO v_minDomainID FROM   Domain WHERE numDomainId > 0;
      WHILE v_minDomainID <= v_maxDomainID LOOP
         RAISE NOTICE 'DomainID=%',SUBSTR(CAST(v_minDomainID AS VARCHAR(20)),1,20);
         RAISE NOTICE '--------------';
         v_numDomainID := v_minDomainID;
         INSERT INTO BizDocTemplate(numDomainID ,
					  numBizDocID ,
					  numOppType ,
					  txtBizDocTemplate ,
					  txtCSS ,
					  bitEnabled ,
					  tintTemplateType ,
					  vcTemplateName ,
					  bitDefault)
         SELECT v_numDomainID,numListItemID AS numBizDocID,
			CASE numListItemID
         WHEN 292 THEN 2
         WHEN 295 THEN 2
         WHEN 644 THEN 2
         WHEN 300 THEN 2
         ELSE 1 END AS numOppType,'' AS txtBizDocTemplate,'' AS txtCss,true AS bitEnabled,0 AS tintTemplateType,LD.vcData AS vcTemplateName,true AS bitDefault
         FROM Listdetails LD  WHERE numListID = 27 AND constFlag = true
         AND LD.numListItemID NOT IN(SELECT numBizDocID FROM BizDocTemplate WHERE numBizDocID > 0 AND numDomainID = v_numDomainID AND tintTemplateType = 0);
			 


--				SELECT * FROM dbo.BizDocTemplate WHERE numDomainID=@numDomainID AND tintTemplateType=0


				---------------------------------------------------
         select   min(numDomainId) INTO v_minDomainID FROM   Domain WHERE  numDomainId > v_minDomainID;
      END LOOP;
   ELSEIF v_tintMode = 2
   then
   	
      INSERT INTO BizDocTemplate(numDomainID ,
					  numBizDocID ,
					  numOppType ,
					  txtBizDocTemplate ,
					  txtCSS ,
					  bitEnabled ,
					  tintTemplateType ,
					  vcTemplateName ,
					  bitDefault)
      SELECT v_numDomainID,numListItemID AS numBizDocID,
			CASE numListItemID
      WHEN 292 THEN 2
      WHEN 295 THEN 2
      WHEN 644 THEN 2
      WHEN 300 THEN 2
      ELSE 1 END AS numOppType,'' AS txtBizDocTemplate,'' AS txtCss,true AS bitEnabled,0 AS tintTemplateType,LD.vcData AS vcTemplateName,true AS bitDefault
      FROM Listdetails LD  WHERE numListID = 27 AND constFlag = true
      AND LD.numListItemID NOT IN(SELECT numBizDocID FROM BizDocTemplate WHERE numBizDocID > 0 AND numDomainID = v_numDomainID AND tintTemplateType = 0);
   ELSEIF v_tintMode = 3
   then
   	
      UPDATE BizDocTemplate SET txtBizDocTemplate = v_txtBizDocTemplate,txtCSS = v_txtCss WHERE numDomainID = v_numDomainID AND tintTemplateType = 0
      AND LENGTH(SUBSTR(CAST(txtBizDocTemplate AS VARCHAR(8000)),1,8000)) = 0 AND LENGTH(SUBSTR(CAST(txtCSS AS VARCHAR(8000)),1,8000)) = 0 AND numBizDocID <> 29397;
      UPDATE BizDocTemplate SET txtBizDocTemplate = v_txtPackingSlipBizDocTemplate,txtCSS = v_txtCss WHERE numDomainID = v_numDomainID AND tintTemplateType = 0
      AND LENGTH(SUBSTR(CAST(txtBizDocTemplate AS VARCHAR(8000)),1,8000)) = 0 AND LENGTH(SUBSTR(CAST(txtCSS AS VARCHAR(8000)),1,8000)) = 0 AND numBizDocID = 29397;
   ELSEIF v_tintMode = 4
   then
   	
      UPDATE BizDocTemplate SET txtBizDocTemplate = v_txtBizDocTemplate,txtCSS = v_txtCss WHERE tintTemplateType = 0
      AND LENGTH(SUBSTR(CAST(txtBizDocTemplate AS VARCHAR(8000)),1,8000)) = 0 AND LENGTH(SUBSTR(CAST(txtCSS AS VARCHAR(8000)),1,8000)) = 0 AND numBizDocID <> 29397;
      UPDATE BizDocTemplate SET txtBizDocTemplate = v_txtPackingSlipBizDocTemplate,txtCSS = v_txtCss WHERE tintTemplateType = 0
      AND LENGTH(SUBSTR(CAST(txtBizDocTemplate AS VARCHAR(8000)),1,8000)) = 0 AND LENGTH(SUBSTR(CAST(txtCSS AS VARCHAR(8000)),1,8000)) = 0 AND numBizDocID = 29397;
   end if;
   RETURN;
END; $$;


