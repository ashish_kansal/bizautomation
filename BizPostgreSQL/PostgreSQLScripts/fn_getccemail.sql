-- Function definition script fn_GetCCEmail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetCCEmail(v_numAlertDTLID NUMERIC,v_numDomainId NUMERIC(9,0))
RETURNS VARCHAR(8000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Email  VARCHAR(8000);
   v_vcEmail  VARCHAR(100);
   v_numAlertEmailID  VARCHAR(15);
   SWV_RowCount INTEGER;
BEGIN
   v_Email := '';
   v_vcEmail := '';
   select   numAlertEmailID, coalesce(vcEmailID,'') INTO v_numAlertEmailID,v_vcEmail from AlertEmailDTL where numAlertDTLid = v_numAlertDTLID and numDomainID = v_numDomainId    LIMIT 1;

   while cast(NULLIF(v_numAlertEmailID,'') as INTEGER) > 0 LOOP
      v_Email := coalesce(v_Email,'') || coalesce(v_vcEmail,'');
      select   numAlertEmailID, coalesce(vcEmailID,'') INTO v_numAlertEmailID,v_vcEmail from AlertEmailDTL where numAlertDTLid = v_numAlertDTLID and numDomainID = v_numDomainId and numAlertEmailID > v_numAlertEmailID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numAlertEmailID := CAST(0 AS VARCHAR(15)); 
      else 
         v_Email := coalesce(v_Email,'') || ' ; ';
      end if;
   END LOOP;

   return coalesce(v_Email,'');
END; $$;

