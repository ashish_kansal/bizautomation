-- Stored procedure definition script USP_GetUOM for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUOM(v_numDomainID NUMERIC(9,0),
 v_tintUnitType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numUOMId,vcUnitName,tintUnitType,CASE tintUnitType WHEN 1
   THEN 'English' WHEN 2 THEN 'Matrix' ELSE '' END AS vcUnitType,
	CASE coalesce(bitEnabled,false) WHEN true THEN 'Yes' ELSE 'No' END AS vcEnable  FROM UOM WHERE numDomainID = v_numDomainID
   AND tintUnitType = v_tintUnitType;
END; $$;
