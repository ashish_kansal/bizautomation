-- Function definition script GetKitWeightBasedOnItemSelection for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetKitWeightBasedOnItemSelection(v_numItemCode NUMERIC(18,0),
	v_vcSelectedKitChildItems TEXT)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltWeight  DOUBLE PRECISION;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPEXISTINGITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEXISTINGITEMS
   (
      vcItem VARCHAR(100)
   );
   INSERT INTO tt_TEMPEXISTINGITEMS(vcItem)
   SELECT
   OutParam
   FROM
   SplitString(v_vcSelectedKitChildItems,',');

   IF(SELECT COUNT(*) FROM tt_TEMPEXISTINGITEMS) > 0 then
	
      v_fltWeight := coalesce((SELECT
      SUM(numQty*coalesce(I.fltWeight,0))
      FROM(SELECT
			 split_part(vcItem,'-',2)::NUMERIC As numItemCode
			,(CASE WHEN split_part(vcItem,'-',3) = '' THEN '0' ELSE split_part(vcItem,'-',3) END)::NUMERIC As numQty
         FROM(SELECT
            vcItem
            FROM
            tt_TEMPEXISTINGITEMS) As x) T2
      INNER JOIN
      Item I
      ON
      T2.numItemCode = I.numItemCode),0);
   ELSE
      v_fltWeight := coalesce((SELECT
      SUM(coalesce(ID.numQtyItemsReq,0)*fn_UOMConversion(numUOMID,I.numItemCode,I.numDomainID,I.numPurchaseUnit)*coalesce(I.fltWeight,0))
      FROM
      ItemDetails ID
      INNER JOIN
      Item I
      ON
      ID.numChildItemID = I.numItemCode
      WHERE
      numItemKitID = v_numItemCode),0);
   end if;
	
   RETURN v_fltWeight;
END; $$;

