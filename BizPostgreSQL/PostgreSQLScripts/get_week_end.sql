CREATE OR REPLACE FUNCTION get_week_end(v_date TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
BEGIN
	RETURN date_trunc('week', v_date::DATE + 1)::date - 1 + make_interval(days => 7) + make_interval(secs => -0.003);	
END; $$;

