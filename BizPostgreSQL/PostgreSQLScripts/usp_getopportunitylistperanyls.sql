-- Stored procedure definition script USP_GetOpportunityListPerAnyls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpportunityListPerAnyls(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                  
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                             
 v_OppStatus SMALLINT DEFAULT NULL,                            
 v_Type SMALLINT DEFAULT NULL,                                  
 v_CurrentPage INTEGER DEFAULT NULL,                                  
 v_PageSize INTEGER DEFAULT NULL,                                  
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                  
 v_columnName VARCHAR(50) DEFAULT NULL,                                  
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                            
 v_numStartDate TIMESTAMP DEFAULT NULL,                                
 v_numEndDate TIMESTAMP DEFAULT NULL,                    
 v_strUserIDs VARCHAR(1000) DEFAULT NULL,      
 v_TeamType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
   v_firstRec  INTEGER;                                   
   v_lastRec  INTEGER;
BEGIN
   if v_strUserIDs = '' then 
      v_strUserIDs := '0';
   end if;                                 
--Create a Temporary table to hold data                       
   if  v_Type <> 3 then

      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      drop table IF EXISTS tt_TEMPTABLE CASCADE;
      Create TEMPORARY TABLE tt_TEMPTABLE 
      ( 
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         numOppID VARCHAR(15),
         Name VARCHAR(100),
         STAGE VARCHAR(100),
         closeDate VARCHAR(50),
         numcreatedby VARCHAR(15),
         Contact VARCHAR(110),
         Company VARCHAR(100),
         numCompanyID VARCHAR(15),
         OppType  VARCHAR(50),
         tintCRMType VARCHAR(10),
         numcontactid VARCHAR(15),
         numDivisionID  VARCHAR(15),
         numTerID VARCHAR(15),
         monPAmount DECIMAL(20,5),
         vcusername VARCHAR(50)
      );
   end if;                                
   if   v_Type = 1 then

      v_strSql := CONCAT('SELECT  Opp.numOppId,                                   
  Opp.vcPOppName AS Name,                                   
  GetOppStatus(Opp.numOppId) as STAGE,                                   
  Opp.intPEstimatedCloseDate AS CloseDate,                                   
  COALESCE(Opp.numcreatedby,0) ,                                  
  CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS Contact,                                   
  C.vcCompanyName AS Company,                                  
                C.numCompanyID,                             
  case when tintOppType=1 then ''Sales opportunity'' else ''Purchase opportunity''  end as OppType,                                  
  Div.tintCRMType,                                  
  ADC.numContactID,                                  
  Div.numDivisionID,                                  
  Div.numTerID,                                  
  Opp.monPAmount,                                  
  fn_GetContactName(Opp.numRecOwner) as vcUserName                                  
  FROM OpportunityMaster Opp                                   
  INNER JOIN AdditionalContactsInformation ADC                                   
  ON Opp.numContactId = ADC.numContactId                                   
  INNER JOIN DivisionMaster Div                                   
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                   
  INNER JOIN CompanyInfo C                                   
  ON Div.numCompanyID = C.numCompanyId                                                        
 WHERE  tintopptype=1 and Opp.bintCreatedDate  between ''',SUBSTR(CAST(v_numStartDate AS VARCHAR(20)),1,20),'''                              
  and ''',SUBSTR(CAST(v_numEndDate AS VARCHAR(20)),1,20),'''  and Opp.numRecOwner in (select numUserCntID from UserTeams                                             
  where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID=',SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15),'                             
  and F.numDomainID=',COALESCE(v_numDomainID,0),' and F.tintType=',COALESCE(v_TeamType,0),'))                               
  and Opp.numDomainID=',COALESCE(v_numDomainID,0));

      if v_OppStatus > 0 then 
         v_strSql := coalesce(v_strSql,'') || '  and Opp.tintOppStatus= ' || SUBSTR(CAST(v_OppStatus AS VARCHAR(1)),1,1);
      end if;
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                             
   if   v_Type = 2 then
      v_strSql := 'SELECT  Opp.numOppId,                                   
  Opp.vcPOppName AS Name,                            
  GetOppStatus(Opp.numOppId) as STAGE,                                   
  Opp.intPEstimatedCloseDate AS CloseDate,                                   
  Opp.numRecOwner,                                  
  CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS Contact,                                   
  C.vcCompanyName AS Company,                                  
  C.numCompanyId,                            
  case when tintOppType = 1 then ''Sales opportunity'' else ''Purchase opportunity''  end as OppType,                              
  Div.tintCRMType,                                  
  ADC.numContactId,                                  
  Div.numDivisionID,                                  
  Div.numTerID,                                  
  Opp.monPAmount,                                  
  fn_GetContactName(Opp.numRecOwner) AS vcUserName                                  
  FROM OpportunityMaster Opp                                   
  INNER JOIN AdditionalContactsInformation ADC                         
   ON Opp.numContactId = ADC.numContactId                                   
  INNER JOIN DivisionMaster Div                                   
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                   
  INNER JOIN CompanyInfo C                                   
  ON Div.numCompanyID = C.numCompanyId                                                        
  WHERE Opp.bintCreatedDate  between ''' || SUBSTR(CAST(v_numStartDate AS VARCHAR(20)),1,20) || '''                              
  and ''' || SUBSTR(CAST(v_numEndDate AS VARCHAR(20)),1,20) || '''  and tintOppType = 1 and Opp.numRecOwner =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '                               
  and ADC.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
      if v_OppStatus > 0 then 
         v_strSql := coalesce(v_strSql,'') || 'and Opp.tintOppStatus= ' || SUBSTR(CAST(v_OppStatus AS VARCHAR(1)),1,1);
      end if;
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                 
   RAISE NOTICE '%',v_strSql;                  
   if  v_Type <> 3 then
      EXECUTE 'insert into tt_TEMPTABLE(numOppId,                                  
      Name,                                  
      STAGE,                                  
      CloseDate,                                  
 numcreatedby ,                                  
 Contact,                                  
 Company,                                  
 numCompanyID,                             
 OppType,                                 
 tintCRMType,                                  
 numContactID,                                  
 numDivisionID,                                  
 numTerID,                                  
 monPAmount,                                  
 vcusername)                                  
' || v_strSql;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      open SWV_RefCur for
      select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;
      select count(*) INTO v_TotRecs from tt_TEMPTABLE;
   end if;                    
                    
   if  v_Type = 3 then

      v_strSql := 'SELECT ADC1.numContactId,COALESCE(vcFirstName,''-'') as vcFirstName,COALESCE(vcLastName,''-'') as vcLastName,(select count(*) from OPPORTUNITYMASTER OM  WHERE                               
     (OM.bintCreatedDate  between ''' || SUBSTR(CAST(v_numStartDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_numEndDate AS VARCHAR(20)),1,20) || ''') and tintopptype=1 and OM.numRecOwner = ADC1.numContactId';
      if v_OppStatus > 0 then 
         v_strSql := coalesce(v_strSql,'') || ' and OM.tintOppStatus= ' || SUBSTR(CAST(v_OppStatus AS VARCHAR(1)),1,1);
      end if;
      v_strSql := coalesce(v_strSql,'') || ') as NoofOpp FROM  AdditionalContactsInformation ADC1                   
                       
  WHERE                  
   ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   end if;
   RETURN;
END; $$;


