DROP FUNCTION IF EXISTS USP_MassSalesFulfillment_GetRecordsForPacking;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetRecordsForPacking(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPackingMode  SMALLINT;
   v_numDefaultSalesShippingDoc  NUMERIC(18,0);
   v_numShippingServiceItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(numDefaultSalesShippingDoc,0), coalesce(numShippingServiceItemID,0) INTO v_numDefaultSalesShippingDoc,v_numShippingServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   select   tintPackingMode INTO v_tintPackingMode FROM
   MassSalesFulfillmentConfiguration WHERE
   numDomainID = v_numDomainID;

   open SWV_RefCur for SELECT
   OM.numOppId AS "numOppId"
		,coalesce(OM.intUsedShippingCompany,coalesce(DM.intShippingCompany,0)) AS "numShipVia"
		,coalesce(OM.numShippingService,coalesce(DM.numDefaultShippingServiceID,0)) AS "numShipService"
		,OM.vcpOppName AS "vcpOppName"
		,I.numContainer AS "numContainer"
		,coalesce(IContainer.vcItemName,'') AS "vcContainer"
		,(CASE WHEN coalesce(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS "numNoItemIntoContainer"
		,coalesce(IContainer.fltWeight,0) AS "fltContainerWeight"
		,coalesce(IContainer.fltHeight,0) AS "fltContainerHeight"
		,coalesce(IContainer.fltWidth,0) AS "fltContainerWidth"
		,coalesce(IContainer.fltLength,0) AS "fltContainerLength"
		,OI.numoppitemtCode AS "numoppitemtCode"
		,I.numItemCode AS "numItemCode"
		,I.vcItemName AS "vcItemName"
		,coalesce(I.fltWeight,0) AS "fltItemWeight"
		,coalesce(I.fltHeight,0) AS "fltItemHeight"
		,coalesce(I.fltWidth,0) AS "fltItemWidth"
		,coalesce(I.fltLength,0) AS "fltItemLength"
		,coalesce((SELECT  monPrice FROM OpportunityItems OIInner WHERE OIInner.numOppId = OM.numOppId AND numItemCode = v_numShippingServiceItemID LIMIT 1),-1) AS "monCurrentShipRate"
		,(CASE WHEN COALESCE(OI.numQtyPicked,0) >= COALESCE((SELECT 
																SUM(OBIInner.numUnitHour)
															FROM
																OpportunityBizDocs OBInner
															INNER JOIN
																OpportunityBizDocItems OBIInner
															ON
																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
															WHERE
																OBInner.numOppId = OM.numOppID
																AND OBInner.numBizDocId=29397
					 											AND OBIInner.numOppItemID = OI.numoppitemtcode),0) THEN COALESCE(OI.numQtyPicked,0) - COALESCE((SELECT 
																																									SUM(OBIInner.numUnitHour)
																																								FROM
																																									OpportunityBizDocs OBInner
																																								INNER JOIN
																																									OpportunityBizDocItems OBIInner
																																								ON
																																									OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																								WHERE
																																									OBInner.numOppId = OM.numOppID
																																									AND OBInner.numBizDocId=29397
					 																																				AND OBIInner.numOppItemID = OI.numoppitemtcode),0) ELSE 0 END) AS "numTotalQty"
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item AS I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   Item AS IContainer
   ON
   I.numContainer = IContainer.numItemCode
   WHERE
   OM.numOppId = v_numOppID
   AND I.charItemType = 'P'
   AND coalesce(I.bitContainer,false) = false
   AND (OI.numoppitemtCode = v_numOppItemID OR coalesce(v_numOppItemID,0) = 0)
   AND (CASE WHEN COALESCE(OI.numQtyPicked,0) >= COALESCE((SELECT 
																SUM(OBIInner.numUnitHour)
															FROM
																OpportunityBizDocs OBInner
															INNER JOIN
																OpportunityBizDocItems OBIInner
															ON
																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
															WHERE
																OBInner.numOppId = OM.numOppID
																AND OBInner.numBizDocId=29397
					 											AND OBIInner.numOppItemID = OI.numoppitemtcode),0) THEN COALESCE(OI.numQtyPicked,0) - COALESCE((SELECT 
																																									SUM(OBIInner.numUnitHour)
																																								FROM
																																									OpportunityBizDocs OBInner
																																								INNER JOIN
																																									OpportunityBizDocItems OBIInner
																																								ON
																																									OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																								WHERE
																																									OBInner.numOppId = OM.numOppID
																																									AND OBInner.numBizDocId=29397
					 																																				AND OBIInner.numOppItemID = OI.numoppitemtcode),0) ELSE 0 END) > 0;
END; $$;












