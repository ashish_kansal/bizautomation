-- Function definition script GetOppSuccesfullPercentage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppSuccesfullPercentage(v_numUserCntID NUMERIC(9,0),v_numDomainID NUMERIC(9,0),v_dtDateFrom TIMESTAMP,v_dtDateTo TIMESTAMP)
RETURNS DECIMAL LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SucessPer  DECIMAL;  
   v_OppTCount  DECIMAL;  
   v_OppCount  DECIMAL;
BEGIN
   select   count(*) INTO v_OppTCount from OpportunityMaster OM where  OM.tintopptype = 1  and OM.numDomainId = v_numDomainID and  OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo;  
   select   count(*) INTO v_OppCount from OpportunityMaster OM where   OM.tintopptype = 1 and tintoppstatus = 1  and OM.numDomainId = v_numDomainID and  OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo;  
   if v_OppTCount = 0 then

      v_SucessPer := 0;
   else
      v_SucessPer :=(v_OppCount/v_OppTCount)*100;
   end if;  
  
   RETURN v_SucessPer;
END; $$;

