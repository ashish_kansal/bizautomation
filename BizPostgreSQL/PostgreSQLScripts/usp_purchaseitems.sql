-- Stored procedure definition script USP_PurchaseItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PurchaseItems(v_numDomainID NUMERIC(9,0) DEFAULT 0,      
v_numDivisionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		I.numItemCode as "numItemCode"
		,vcItemName as "vcItemName"
		,monListPrice as "Price"
		,vcSKU as "SKU"
		,vcModelID as "Model ID" 
	from Item I
   join Vendor V
   on V.numItemCode = I.numItemCode
   where V.numVendorID = v_numDivisionID;
END; $$;












