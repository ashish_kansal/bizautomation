-- Stored procedure definition script usp_GetDivisionName for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDivisionName(v_numDivisionId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcDivisionName FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;
END; $$;












