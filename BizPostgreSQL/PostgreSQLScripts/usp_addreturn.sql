CREATE OR REPLACE PROCEDURE USP_AddReturn(v_numOppItemCode       NUMERIC(18,0),
               v_numOppId             NUMERIC(18,0),
               v_numItemCode          NUMERIC(18,0),
               v_numTotalQty          NUMERIC(18,0),
               v_numQtyToReturn       NUMERIC(18,0),
               v_numQtyReturned       NUMERIC(18,0)  DEFAULT 0,
               v_monPrice             DECIMAL(20,5) DEFAULT NULL,
               v_numReasonForReturnID NUMERIC(18,0) DEFAULT NULL,
               v_numReturnStatus      NUMERIC(18,0) DEFAULT NULL,
               v_numCreateBy          NUMERIC(18,0) DEFAULT NULL,
			v_vcReferencePO VARCHAR(50) DEFAULT '')
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numReturnID  NUMERIC(18,0);
   v_temp  NUMERIC(9,0);
   v_AllowedQty  NUMERIC(9,0);
BEGIN
   select   numReturnID INTO v_numReturnID FROM   Returns WHERE  numOppID = v_numOppId
   AND numOppItemCode = v_numOppItemCode; --Bug fix ID 2138
   IF v_numReturnID IS NULL then
      
        --insert
      RAISE NOTICE 'insert';
      INSERT INTO Returns(numOppID,
                    numOppItemCode,
                    numitemcode,
                    numtotalqty,
                    numQtyToReturn,
                    numqtyreturned,
                    monprice,
                    numreasonforreturnid,
                    numreturnstatus,
                    numcreateby,
                    numModifiedBy,
                    dtModifiedDate)
        VALUES(v_numOppId,
                    v_numOppItemCode,
                    v_numItemCode,
                    v_numTotalQty,
                    v_numQtyToReturn,
                    v_numQtyReturned,
                    v_monPrice,
                    v_numReasonForReturnID,
                    v_numReturnStatus,
                    v_numCreateBy,
                    v_numCreateBy,
                    LOCALTIMESTAMP);
        
      v_numReturnID := CURRVAL('returns_seq');
   ELSE
        --update
        
      RAISE NOTICE 'update';
      select   coalesce(numtotalqty,0), (coalesce(numQtyToReturn,0)+v_numQtyToReturn) INTO v_AllowedQty,v_temp FROM   Returns WHERE  numReturnID = v_numReturnID;
      IF (v_temp > v_AllowedQty) then
          
         RAISE EXCEPTION 'NOT_ALLOWED';
         RETURN;
      ELSE
         UPDATE Returns
         SET    numQtyToReturn = numQtyToReturn+v_numQtyToReturn,numModifiedBy = v_numCreateBy,dtModifiedDate = LOCALTIMESTAMP,
         vcReferencePO = v_vcReferencePO
         WHERE  numReturnID = v_numReturnID;
      end if;
   end if;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/



