-- Stored procedure definition script USP_Item_GetDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetDetails(v_numItemCode NUMERIC(9,0),
v_numWarehouseItemID NUMERIC(9,0),
v_ClientTimeZoneOffset INTEGER,
v_vcSelectedKitChildItems TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitItemIsUsedInOrder  BOOLEAN;
   
   v_numDomainId  NUMERIC;
BEGIN
   v_bitItemIsUsedInOrder := false;
   
   select   numDomainID INTO v_numDomainId FROM Item WHERE numItemCode = v_numItemCode;
   
   IF TIMEZONE('UTC',now()) < '2013-03-15 23:59:39' then
      v_bitItemIsUsedInOrder := false;
   ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode) > 0
   then
      v_bitItemIsUsedInOrder := true;
   ELSEIF EXISTS(SELECT numItemID FROM General_Journal_Details WHERE numItemID = v_numItemCode and numDomainId = v_numDomainId AND chBizDocItems = 'IA1')
   then
      v_bitItemIsUsedInOrder := true;
   end if;
                                               
   open SWV_RefCur for SELECT
		I.numItemCode,
		vcItemName,
		coalesce(txtItemDesc,'') AS txtItemDesc,
		coalesce(IED.txtDesc,'') AS vcExtendedDescToAPI,
		charItemType,
		(CASE WHEN charItemType = 'P' THEN coalesce(W.monWListPrice,0) ELSE coalesce(monListPrice,0) END) AS monListPrice,
		numItemClassification,
		coalesce(bitTaxable,false) as bitTaxable,
		vcSKU AS vcSKU,
		coalesce(bitKitParent,false) as bitKitParent,--, dtDateEntered,                  
		I.numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,
		numModifiedBy,
		(SELECT  vcPathForImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1) AS vcPathForImage ,
		(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1) AS  vcPathForTImage, coalesce(bitSerialized,false) as bitSerialized, vcModelID,
		CONCAT('<Images>',(SELECT string_agg(CONCAT('<ItemImages'
		, ' numItemImageId="',numItemImageId,'"'
		, ' vcPathForImage="',vcPathForImage,'"'
		, ' vcPathForTImage="',vcPathForTImage,'"'
		, ' bitDefault="',bitDefault,'"'
		, ' intDisplayOrder="',case when bitDefault = true then -1 else coalesce(intDisplayOrder,0) end ,'"','/>'),'' order by case when bitDefault = true then -1 else coalesce(intDisplayOrder,0) end asc) 
      FROM ItemImages
      WHERE numItemCode = v_numItemCode),'</Images>')  AS xmlItemImages,
		COALESCE((SELECT string_agg(CAST(numCategoryID AS VARCHAR),',') FROM ItemCategory WHERE numItemID = I.numItemCode),'') AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END) AS monAverageCost,
		monCampaignLabourCost,fn_GetContactName(numCreatedBy) || ' ,' || bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as CreatedBy ,
		fn_GetContactName(numModifiedBy) || ' ,' || bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as ModifiedBy,
		numOnHand as numOnHand,
		numOnOrder as numOnOrder,
		numReorder as numReorder,
		numAllocation as numAllocation,
		numBackOrder as numBackOrder,
		(CASE
   WHEN coalesce(bitKitParent,false) = true
   AND(CASE
   WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
   THEN 1
   ELSE 0
   END) = 0
   THEN
      GetKitWeightBasedOnItemSelection(v_numItemCode,v_vcSelectedKitChildItems)
   ELSE
      coalesce(fltWeight,0)
   END) as fltWeight,
		coalesce(fltHeight,0)  as fltHeight,
		coalesce(fltWidth,0)  as fltWidth,
		coalesce(fltLength,0)  as fltLength,
		coalesce(bitFreeShipping,false)  as bitFreeShipping,
		coalesce(bitAllowBackOrder,false)  as bitAllowBackOrder,
		coalesce(vcUnitofMeasure,'Units')  as vcUnitofMeasure,
		coalesce(bitShowDeptItem,false)  AS bitShowDeptItem,
		coalesce(bitShowDeptItemDesc,false)  AS bitShowDeptItemDesc,
		coalesce(bitCalAmtBasedonDepItems,false)  AS bitCalAmtBasedonDepItems,
		coalesce(bitAssembly,false)  AS bitAssembly ,
		coalesce(numBarCodeId,'') as  numBarCodeId ,
		coalesce(I.vcManufacturer,'')  as vcManufacturer,
		coalesce(I.numBaseUnit,0)  AS numBaseUnit
		,coalesce(I.numPurchaseUnit,0)  AS numPurchaseUnit
		,coalesce(I.numSaleUnit,0)  AS numSaleUnit,
		fn_GetUOMName(coalesce(I.numBaseUnit,0)) AS vcBaseUnit
		,fn_GetUOMName(coalesce(I.numPurchaseUnit,0)) AS vcPurchaseUnit
		,fn_GetUOMName(coalesce(I.numSaleUnit,0)) AS vcSaleUnit,
		coalesce(bitLotNo,false)  as bitLotNo,
		coalesce(IsArchieve,false)  AS IsArchieve,
		case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as ItemType,
		case when coalesce(bitAssembly,false) = true then fn_GetAssemblyPossibleWOQty(I.numItemCode,W.numWareHouseID) else 0 end AS numWOQty,
		coalesce(I.numItemClass,0)  as numItemClass,
		coalesce(tintStandardProductIDType,0)  AS tintStandardProductIDType,
		coalesce(vcExportToAPI,'')  AS vcExportToAPI,
		coalesce(numShipClass,0)  AS numShipClass,coalesce(I.bitAllowDropShip,false)  AS bitAllowDropShip,
		coalesce(v_bitItemIsUsedInOrder,false) AS bitItemIsUsedInOrder,
		coalesce((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		coalesce(I.bitArchiveItem,false)  AS bitArchiveItem,
		coalesce(W.numWareHouseID,0)  AS numWareHouseID,
		coalesce(W.numWareHouseItemID,0)  AS numWareHouseItemID,
		coalesce((SELECT COUNT(WI.numWareHouseItemID) FROM WareHouseItems WI WHERE WI.numItemID = I.numItemCode AND WI.numItemID = v_numItemCode),0) AS intTransCount,
		coalesce(I.bitAsset,false)  AS bitAsset,
		coalesce(I.bitRental,false)  AS bitRental,
		coalesce(I.numContainer,0)  AS numContainer,
		coalesce(I.numNoItemIntoContainer,0)  AS numNoItemIntoContainer,
		coalesce(Vendor.monCost,0)  AS monVendorCost, CPN.CustomerPartNo ,
		coalesce(WH.vcWareHouse,'')  AS vcWareHouse
   FROM
   Item I
   left join
   WareHouseItems W
   on
   W.numItemID = I.numItemCode AND
   W.numWareHouseItemID = v_numWarehouseItemID
   LEFT JOIN
   Warehouses WH
   ON
   W.numWareHouseID = WH.numWareHouseID
   LEFT JOIN
   ItemExtendedDetails IED
   ON
   I.numItemCode = IED.numItemCode
   LEFT JOIN
   Vendor
   ON
   I.numVendorID = Vendor.numVendorID
   AND Vendor.numItemCode = I.numItemCode
   LEFT JOIN
   CustomerPartNumber CPN
   ON
   I.numItemCode = CPN.numItemCode
   WHERE
   I.numItemCode = v_numItemCode;
END; $$; 













