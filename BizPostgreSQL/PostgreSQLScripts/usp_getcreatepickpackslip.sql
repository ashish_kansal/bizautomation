-- FUNCTION: public.usp_getcreatepickpackslip(numeric, numeric, numeric, numeric, boolean, refcursor)

-- DROP FUNCTION public.usp_getcreatepickpackslip(numeric, numeric, numeric, numeric, boolean, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcreatepickpackslip(
	v_numbizdoctypeid numeric,
	v_numoppid numeric,
	v_numwarehouseid numeric,
	v_numshiptoaddressid numeric,
	v_bitfirstiteration boolean,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:11:11 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT
   O.numOppBizDocsId,
		O.vcBizDocID || '      ' || B.vcTemplateName AS vcBizDocName
   FROM
   OpportunityBizDocs AS O
   LEFT JOIN
   BizDocTemplate AS B
   ON
   O.numBizDocTempID = B.numBizDocTempID
   WHERE
   O.numoppid = v_numOppID
   AND 1 =(CASE
   WHEN v_numBizDocTypeID = 296 THEN(CASE WHEN O.numBizDocId IN(296,29397) THEN 1 ELSE 0 END)
   ELSE(CASE WHEN O.numBizDocId = v_numBizDocTypeID THEN 1 ELSE 0 END)
   END)
   AND 1 =(CASE
   WHEN(SELECT
      COUNT(*)
      FROM
      OpportunityBizDocItems
      WHERE
      numOppBizDocID = O.numOppBizDocsId) =(SELECT
      COUNT(*)
      FROM
      OpportunityBizDocItems OBDI
      INNER JOIN
      OpportunityItems OI
      ON
      CAST(OBDI.numOppItemID AS Numeric)= OI.numoppitemtCode
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      numOppBizDocID = O.numOppBizDocsId
      AND (((coalesce(WI.numWareHouseID,0) = v_numWarehouseID OR (coalesce(WI.numWareHouseID,0) = 0 AND v_numShipToAddressID > 0 AND coalesce(OI.numShipToAddressID,0) = v_numShipToAddressID)) AND (coalesce(OI.numShipToAddressID,0) = v_numShipToAddressID OR coalesce(OI.numShipToAddressID,0) = 0)) OR (v_bitFirstIteration = true AND coalesce(OI.numWarehouseItmsID,0) = 0 AND coalesce(OI.numShipToAddressID,0) = 0)))
   THEN 1
   ELSE 0
   END);
END;
$BODY$;

ALTER FUNCTION public.usp_getcreatepickpackslip(numeric, numeric, numeric, numeric, boolean, refcursor)
    OWNER TO postgres;
