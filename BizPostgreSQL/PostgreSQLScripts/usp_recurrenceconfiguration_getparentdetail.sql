-- Stored procedure definition script USP_RecurrenceConfiguration_GetParentDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Gets parent detail of recurred order or bizdoc
-- =============================================  
Create or replace FUNCTION USP_RecurrenceConfiguration_GetParentDetail(v_numID NUMERIC(18,0),
	v_numType SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecConfigID  NUMERIC(18,0);
BEGIN
  

   IF v_numType = 1 then --Order
      select   numRecConfigID INTO v_numRecConfigID FROM RecurrenceTransaction WHERE numRecurrOppID = v_numID;
   ELSEIF v_numType = 2
   then --BizDoc
      select   numRecConfigID INTO v_numRecConfigID FROM RecurrenceTransaction WHERE numRecurrOppBizDocID = v_numID;
   end if;
	
   IF coalesce(v_numRecConfigID,0) > 0 then
	
      IF v_numType = 1 then --Order
         open SWV_RefCur for
         SELECT
         RowNo,
				numParentOppID,
				vcpOppName AS Name,
				numParentBizDocID
         FROM(SELECT
            ROW_NUMBER() OVER(ORDER BY RecurrenceTransaction.numRecTranID) AS RowNo,
						numRecurrOppID,
						RecurrenceConfiguration.numOppID AS numParentOppID,
						OpportunityMaster.vcpOppName,
						RecurrenceConfiguration.numOppBizDocID AS numParentBizDocID
            FROM
            RecurrenceTransaction
            INNER JOIN
            RecurrenceConfiguration
            ON
            RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
            INNER JOIN
            OpportunityMaster
            ON
            RecurrenceConfiguration.numOppID = OpportunityMaster.numOppId
            WHERE
            RecurrenceTransaction.numRecConfigID = v_numRecConfigID) AS TEMP
         WHERE
         TEMP.numRecurrOppID = v_numID;
      ELSEIF v_numType = 2
      then --BizDoc
         open SWV_RefCur for
         SELECT
         RowNo,
				numParentOppID,
				vcBizDocID AS Name,
				numParentBizDocID
         FROM(SELECT
            ROW_NUMBER() OVER(ORDER BY RecurrenceTransaction.numRecTranID ASC) AS RowNo,
						numRecurrOppBizDocID,
						RecurrenceConfiguration.numOppID AS numParentOppID,
						OpportunityBizDocs.vcBizDocID,
						RecurrenceConfiguration.numOppBizDocID AS numParentBizDocID
            FROM
            RecurrenceTransaction
            INNER JOIN
            RecurrenceConfiguration
            ON
            RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
            INNER JOIN
            OpportunityBizDocs
            ON
            RecurrenceConfiguration.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
            WHERE
            RecurrenceTransaction.numRecConfigID = v_numRecConfigID) AS TEMP
         WHERE
         TEMP.numRecurrOppBizDocID = v_numID;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 0 AS RowNo, NULL AS numParentOppID, NULL AS numParentBizDocID;
   end if;
   RETURN;
END; $$;  


