-- Stored procedure definition script USP_GetUOMConversionByfromtoconversion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetUOMConversionByfromtoconversion(v_chrAction CHAR(10),                                                               
 v_numDomainID NUMERIC(18,0),
 v_fromUOM NUMERIC(18,0),   
 v_toUOM NUMERIC(18,0),
 v_ItemCode NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_chrAction = 'G') then
   
      open SWV_RefCur for
      select * from UOMConversion where numDomainID = v_numDomainID AND numUOM1 = v_fromUOM AND numUOM2 = v_toUOM;
   end if;
   IF(v_chrAction = 'IL') then
   
      open SWV_RefCur for
      select * from ItemUOMConversion where numDomainID = v_numDomainID AND numItemCode = v_ItemCode AND numSourceUOM = v_fromUOM AND numTargetUOM = v_toUOM;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_GetTeamForUsrMst]    Script Date: 07/26/2008 16:18:39 ******/
   RETURN;
END; $$;


