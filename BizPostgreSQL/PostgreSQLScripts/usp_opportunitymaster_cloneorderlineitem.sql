CREATE OR REPLACE FUNCTION USP_OpportunityMaster_CloneOrderLineItem
(
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
    v_numOppItemID NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	v_tintCommitAllocation SMALLINT;
	v_tintOppType  SMALLINT;
	v_tintOppStatus  SMALLINT;
	v_numDivisionId  NUMERIC(18,0);
	v_bitAllocateInventoryOnPickList  BOOLEAN;

	v_vcOppItemsColumns TEXT DEFAULT COALESCE((SELECT string_agg(COLUMN_NAME,',')
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = 'opportunityitems' AND COLUMN_NAME NOT IN ('numoppitemtcode','bitmappingrequired','bitrequiredwarehousecorrection','numqtyreceived','numqtypicked','numqtyshipped','numunithourreceived')),'');

	v_vcOppKitItemsColumns TEXT DEFAULT COALESCE((SELECT string_agg(COLUMN_NAME,',')
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = 'opportunitykititems' AND COLUMN_NAME NOT IN ('numoppchilditemid','numoppitemid','numqtyshipped')),'');

	v_vcOppKitChildItemsColumns TEXT DEFAULT COALESCE((SELECT string_agg(COLUMN_NAME,',')
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = 'opportunitykitchilditems' AND COLUMN_NAME NOT IN ('numoppkitchilditemid','numoppitemid','numoppchilditemid','numqtyshipped')),'');

	v_numNewOppItemID NUMERIC(18,0);
	v_numNewOppChildItemID NUMERIC(18,0);
	v_numNewOppKitChildItemID NUMERIC(18,0);

	v_sqlText  TEXT DEFAULT CONCAT('INSERT INTO OpportunityItems (',v_vcOppItemsColumns,') SELECT ',v_vcOppItemsColumns,' FROM OpportunityItems WHERE numOppID=',v_numOppID,' AND numoppitemtCode=',v_numOppItemID,' RETURNING numoppitemtcode; ');

	v_i INTEGER DEFAULT 1;
	v_iCount INTEGER;
	v_numTempOppChildItemID NUMERIC(18,0);
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
	
	BEGIN
		IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId = v_numOppID AND coalesce(tintshipped,0) = 1) then
			RAISE EXCEPTION 'CLOSED_ORDER';
		ELSE
		BEGIN
			SELECT coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;

			SELECT numDivisionId, tintopptype, coalesce(tintoppstatus,0) INTO v_numDivisionId,v_tintOppType,v_tintOppStatus FROM OpportunityMaster WHERE numOppId = v_numOppID;

			SELECT 
				coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList 
			FROM
				DivisionMaster D
			JOIN
				CompanyInfo C
			ON
				C.numCompanyId = D.numCompanyID 
			WHERE
				D.numDivisionID = v_numDivisionId;

			--REVERTING BACK THE WAREHOUSE ITEMS                  
			IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
				PERFORM USP_RevertDetailsOpp(v_numOppID,0,0::SMALLINT,v_numUserCntID);
			end if;
			EXECUTE v_sqlText INTO v_numNewOppItemID;
			
			BEGIN
			CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
			EXCEPTION WHEN OTHERS THEN
				NULL;
			END;

			DROP TABLE IF EXISTS tt_TEMP CASCADE;
			CREATE TEMPORARY TABLE tt_TEMP
			(
				ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
				numOppChildItemID NUMERIC(18,0)
			);

			INSERT INTO tt_TEMP
			(
				numOppChildItemID
			)
			SELECT
				numOppChildItemID
			FROM
				OpportunityKitItems
			WHERE
				numOppId = v_numOppID
				AND numOppItemID = v_numOppItemID;

			select COUNT(*) INTO v_iCount FROM tt_TEMP;


			WHILE v_i <= v_iCount LOOP
				select   numOppChildItemID INTO v_numTempOppChildItemID FROM tt_TEMP WHERE ID = v_i;

				v_sqlText := CONCAT('INSERT INTO OpportunityKitItems (',v_vcOppKitItemsColumns,',numOppItemID) SELECT ',
				v_vcOppKitItemsColumns,',',v_numNewOppItemID,' FROM OpportunityKitItems WHERE numOppID=',
				v_numOppID,' AND numOppItemID=',v_numOppItemID,
				' AND numOppChildItemID=',v_numTempOppChildItemID,' RETURNING numOppChildItemID;');

				EXECUTE v_sqlText INTO v_numNewOppChildItemID;
				v_sqlText := CONCAT('INSERT INTO OpportunityKitChildItems (',v_vcOppKitChildItemsColumns,',numOppItemID,numOppChildItemID) SELECT ',
				v_vcOppKitChildItemsColumns,
				',',v_numNewOppItemID,',',v_numNewOppChildItemID,' FROM OpportunityKitChildItems WHERE numOppID=',
				v_numOppID,' AND numOppItemID=',v_numOppItemID,
				' AND numOppChildItemID=',v_numTempOppChildItemID,';');

				EXECUTE v_sqlText;
				v_i := v_i::bigint+1;
			END LOOP;

			IF NOT (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then
				PERFORM USP_ManageSOWorkOrder(v_numDomainID,v_numOppID,v_numUserCntID);
			end if;
			IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
				PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,v_numUserCntID);
			end if;

			IF v_tintOppStatus = 1 then
				IF(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = v_numOppID AND coalesce(numWarehouseItmsID,0) > 0 AND coalesce(bitDropShip,false) = false) <>(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode = WareHouseItems.numItemID WHERE numOppId = v_numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND coalesce(bitDropShip,false) = false) then
					RAISE EXCEPTION 'INVALID_ITEM_WAREHOUSES';
					RETURN;
				end if;
			end if;
         
			IF(SELECT
				COUNT(*)
				FROM
				OpportunityBizDocItems OBDI
				INNER JOIN
				OpportunityBizDocs OBD
				ON
				OBDI.numOppBizDocID = OBD.numOppBizDocsId
				WHERE
				OBD.numoppid = v_numOppID AND OBDI.numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID)) > 0 then
				RAISE EXCEPTION 'ITEM_USED_IN_BIZDOC';
				RETURN;
			end if;

			IF(SELECT
				COUNT(*)
				FROM
				OpportunityItems OI
				INNER JOIN
				(SELECT
					numOppItemID
					,SUM(numUnitHour) AS numInvoiceBillQty
				FROM
				OpportunityBizDocItems OBDI
				INNER JOIN
				OpportunityBizDocs OBD
				ON
				OBDI.numOppBizDocID = OBD.numOppBizDocsId
				WHERE
				OBD.numoppid = v_numOppID
				AND coalesce(OBD.bitAuthoritativeBizDocs,0) = 1
				GROUP BY
				numOppItemID) AS TEMP
				ON
				TEMP.numOppItemID = OI.numoppitemtCode
				WHERE
				OI.numOppId = v_numOppID
				AND OI.numUnitHour < Temp.numInvoiceBillQty) > 0 then
				RAISE EXCEPTION 'ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY';
				RETURN;
			end if;

			IF v_tintOppType = 1 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
				IF(SELECT
					COUNT(*)
					FROM(SELECT
					   numOppItemID
									,SUM(numUnitHour) AS PackingSlipUnits
					   FROM
					   OpportunityBizDocItems OBDI
					   INNER JOIN
					   OpportunityBizDocs OBD
					   ON
					   OBDI.numOppBizDocID = OBD.numOppBizDocsId
					   WHERE
					   OBD.numoppid = v_numOppID
					   AND OBD.numBizDocId = 29397 --Picking Slip
					   GROUP BY
					   numOppItemID) AS TEMP
					INNER JOIN
					OpportunityItems OI
					ON
					TEMP.numOppItemID = OI.numoppitemtCode
					WHERE
					OI.numUnitHour < PackingSlipUnits) > 0 then
					RAISE EXCEPTION 'ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY';
					RETURN;
				end if;
			end if;
         
			IF EXISTS(SELECT
					numoppitemtCode
					FROM
					OpportunityItems
					INNER JOIN
					Item
					ON
					OpportunityItems.numItemCode = Item.numItemCode
					WHERE
					numOppId = v_numOppID
					AND charItemType = 'P'
					AND coalesce(numWarehouseItmsID,0) = 0
					AND coalesce(bitDropShip,false) = false) then
				RAISE EXCEPTION 'WAREHOUSE_REQUIRED';
			end if;

			IF(SELECT COUNT(*) FROM WorkOrder WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID AND coalesce(numOppItemID,0) > 0 AND numOppItemID NOT IN(SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId = v_numOppID)) > 0 then
				RAISE EXCEPTION 'WORK_ORDER_EXISTS';
			end if;

			IF(SELECT
				COUNT(*)
				FROM
				WorkOrder
				INNER JOIN
				OpportunityItems
				ON
				OpportunityItems.numOppId = v_numOppID
				AND OpportunityItems.numoppitemtCode = WorkOrder.numOppItemID
				WHERE
				WorkOrder.numDomainId = v_numDomainID
				AND WorkOrder.numOppId = v_numOppID
				AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0 then
				RAISE EXCEPTION 'CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS';
			end if;

			--Delete Tax for Opportunity Items if item deleted 
			DELETE FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID AND numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID);

			--Insert Tax for Opportunity Items
			INSERT INTO OpportunityItemsTaxItems
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			)
			SELECT
				v_numOppID,
				OI.numoppitemtCode,
				TI.numTaxItemID,
				0
			FROM
			OpportunityItems OI
			JOIN
			ItemTax IT
			ON
			OI.numItemCode = IT.numItemCode
			JOIN
			TaxItems TI
			ON
			TI.numTaxItemID = IT.numTaxItemID
			WHERE
			OI.numOppId = v_numOppID
			AND IT.bitApplicable = true
			AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
			UNION
			SELECT
				v_numOppID,
				OI.numoppitemtCode,
				0,
				0
			FROM
			OpportunityItems OI
			JOIN
			Item I
			ON
			OI.numItemCode = I.numItemCode
			WHERE
			OI.numOppId = v_numOppID
			AND I.bitTaxable = true
			AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
			UNION
			SELECT
				v_numOppID,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
			OpportunityItems OI
			INNER JOIN
			ItemTax IT
			ON
			IT.numItemCode = OI.numItemCode
			INNER JOIN
			TaxDetails TD
			ON
			TD.numTaxID = IT.numTaxID
			AND TD.numDomainId = v_numDomainID
			WHERE
			OI.numOppId = v_numOppID
			AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID);

			UPDATE
			OpportunityItems OI
			SET
			vcInclusionDetail = GetOrderAssemblyKitInclusionDetails(OI.numOppId,OI.numoppitemtCode,OI.numUnitHour,1::SMALLINT,true)
			FROM
			Item I
			WHERE
			OI.numItemCode = I.numItemCode AND(numOppId = v_numOppID
			AND coalesce(I.bitKitParent,false) = true);
			UPDATE OpportunityMaster SET monDealAmount = GetDealAmount(v_numOppID,LOCALTIMESTAMP,0::NUMERIC),numModifiedBy = v_numUserCntID,
			bintModifiedDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID;

	EXCEPTION WHEN OTHERS THEN
      
			GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
	END;
   end if;
END; $$;


