-- Stored procedure definition script USP_TransferOwnerAssigneAdv for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TransferOwnerAssigneAdv(v_numUserCntID NUMERIC(9,0),
v_xmlStr VARCHAR(500),
v_byteMode SMALLINT,
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      update DivisionMaster set numRecOwner = v_numUserCntID where numDivisionID in(select distinct(numDivisionId) from AdditionalContactsInformation where numDomainID = v_numDomainID and
      numContactId in(select Id from SplitIds(v_xmlStr,',')));
   else
      update DivisionMaster set numAssignedTo = v_numUserCntID where numDivisionID in(select distinct(numDivisionId) from AdditionalContactsInformation where numDomainID = v_numDomainID and
      numContactId in(select Id from SplitIds(v_xmlStr,',')));
   end if;
   RETURN;
END; $$;


