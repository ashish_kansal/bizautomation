-- Stored procedure definition script USP_InsertImapNewMails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_InsertImapNewMails(v_numUserCntId NUMERIC(9,0),            
v_numDomainID NUMERIC(9,0),            
v_numUid NUMERIC(9,0),
v_vcSubject VARCHAR(500),
v_vcBody TEXT,
v_vcBodyText TEXT,
v_dtReceivedOn TIMESTAMP,
v_vcSize VARCHAR(50),
v_bitIsRead BOOLEAN,
v_bitHasAttachments BOOLEAN,
v_vcFromName VARCHAR(5000),
v_vcToName VARCHAR(5000),
v_vcCCName VARCHAR(5000),
v_vcBCCName VARCHAR(5000),
v_vcAttachmentName VARCHAR(500),
v_vcAttachmenttype VARCHAR(500),
v_vcNewAttachmentName VARCHAR(500),
v_vcAttachmentSize VARCHAR(500),
v_numNodeId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Identity  NUMERIC(9,0);
BEGIN








                                        
   if(select count(*) from EmailHistory where numUserCntId = v_numUserCntId
   and numDomainID = v_numDomainID  and numUid = v_numUid AND numNodeId = v_numNodeId) = 0 then
        
             --Inser into Email Master
      v_vcToName := coalesce(v_vcToName,'') || '#^#';
      v_vcFromName := coalesce(v_vcFromName,'') || '#^#';
      v_vcCCName := coalesce(v_vcCCName,'') || '#^#';
      v_vcBCCName := coalesce(v_vcBCCName,'') || '#^#';

            -- PRINT 'ToName' + @ToName 
      PERFORM USP_InsertEmailMaster(v_vcToName,v_numDomainID);
      PERFORM USP_InsertEmailMaster(v_vcFromName,v_numDomainID);
      PERFORM USP_InsertEmailMaster(v_vcCCName,v_numDomainID);
      PERFORM USP_InsertEmailMaster(v_vcBCCName,v_numDomainID); 

             -- Insert Into Email History
                
      INSERT  INTO EmailHistory(vcSubject,
                          vcBody,
                          vcBodyText,
                          bintCreatedOn,
                          bitHasAttachments,
                          vcItemId,
                          vcChangeKey,
                          bitIsRead,
                          vcSize,
                          chrSource,
                          tintType,
                          vcCategory,
                          dtReceivedOn,
                          numDomainID,
                          numUserCntId,
                          numUid,IsReplied,
						  numNodeId)
                VALUES(v_vcSubject,
                          v_vcBody,
                          fn_StripHTML(v_vcBodyText),
                          TIMEZONE('UTC',now()),
                          v_bitHasAttachments,
                          '',
                          '',
                          v_bitIsRead,
                          CAST(v_vcSize AS VARCHAR(200)),
                          'I',
                          1,
                          '',
                          v_dtReceivedOn,
                          v_numDomainID,
                          v_numUserCntId,
                          v_numUid,false,
						  v_numNodeId) RETURNING numEmailHstrID INTO v_Identity;
				
--				To increase performance of Select
      UPDATE  EmailHistory
      SET     vcFrom = fn_GetNewvcEmailString(v_vcFromName,v_numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 4),
                        vcTo = fn_GetNewvcEmailString(v_vcToName,v_numDomainID),    --dbo.GetEmaillName(numEmailHstrID, 1),
      vcCC = fn_GetNewvcEmailString(v_vcCCName,v_numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 3)
      vcBCC = fn_GetNewvcEmailString(v_vcBCCName,v_numDomainID)
      WHERE   numEmailHstrID = v_Identity;
      IF(SELECT vcFrom FROM EmailHistory WHERE numEmailHstrID = v_Identity) IS NOT NULL then
				
         update EmailHistory set numEMailId =(SELECT  Item::NUMERIC FROM DelimitedSplit8K(vcFrom,'$^$') LIMIT 1)
         where numEmailHstrID = v_Identity;
      end if;
      IF v_bitHasAttachments = true then
                    
         PERFORM USP_InsertAttachment(v_vcAttachmentName,'',v_vcAttachmenttype,v_Identity,v_vcNewAttachmentName,
         v_vcAttachmentSize);
      end if;
   end if;
   RETURN;
END; $$;            


