DROP FUNCTION IF EXISTS USP_GetCampaignReport;
CREATE OR REPLACE FUNCTION USP_GetCampaignReport
(
	v_numDomainID NUMERIC(18,0)
	,v_ClientTimezoneOffset INTEGER
	,v_numLeadSource NUMERIC(18,0)
	,v_tintReportView SMALLINT
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_columns  VARCHAR(8000) DEFAULT '';
   v_PivotColumns  VARCHAR(8000) DEFAULT '';
   v_sql  TEXT DEFAULT '';
BEGIN
   IF v_tintReportView IN(1,2) then
	
      DROP TABLE IF EXISTS tt_TEMPYEARMONTH CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTH
      (
         intYear INTEGER,
         intMonth INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         numConversionPercent NUMERIC(18,2),
         monGrossProfit DECIMAL(20,5)
      );
      IF v_tintReportView = 2 then
		
         INSERT INTO tt_TEMPYEARMONTH(intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate)
				with recursive CTE AS(SELECT
         EXTRACT(YEAR FROM v_dtFromDate) AS yr,
					EXTRACT(MONTH FROM v_dtFromDate) AS mm,
					TO_CHAR(v_dtFromDate,'FMMonth') AS mon,
					(TO_CHAR(v_dtFromDate,'FMMonth') || '_' || CAST(EXTRACT(YEAR FROM v_dtFromDate) AS VARCHAR(30))) AS MonthYear,
					CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,
         1) AS TIMESTAMP) AS dtStartDate,
					CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,
         1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.003 ' AS dtEndDate,
					v_dtFromDate AS new_date
         UNION ALL
         SELECT
         EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS yr,
					EXTRACT(MONTH FROM new_date+INTERVAL '1 day') AS mm,
					TO_CHAR(new_date+INTERVAL '1 day','FMMonth') AS mon,
					(TO_CHAR(new_date+INTERVAL '1 day','FMMonth') || '_' || CAST(EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS VARCHAR(30))) AS MonthYear,
					CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,
         1) AS TIMESTAMP) AS dtStartDate,
					CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,
         1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.003' AS dtEndDate,
					new_date+INTERVAL '1 day' AS new_date
         FROM CTE
         WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
         yr,mm,MonthYear,dtStartDate,dtEndDate
         FROM
         CTE
         GROUP BY
         yr,mm,MonthYear,dtStartDate,dtEndDate
         ORDER BY
         yr,mm;
         UPDATE
         tt_TEMPYEARMONTH
         SET
         EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      ELSE
         INSERT INTO tt_TEMPYEARMONTH(intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate) with recursive CTE AS(SELECT
         GetFiscalyear(v_dtFromDate,v_numDomainID) AS yr,
					GetFiscalQuarter(v_dtFromDate,v_numDomainID) AS qq,
					('Q' || cast(GetFiscalQuarter(v_dtFromDate,v_numDomainID) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(v_dtFromDate,v_numDomainID) AS VARCHAR(30))) AS MonthYear,
					CASE GetFiscalQuarter(v_dtFromDate,v_numDomainID)
         WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '0 month'
         WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '3 month'
         WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '6 month'
         WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '9 month'
         END AS dtStartDate,
					CASE GetFiscalQuarter(v_dtFromDate,v_numDomainID)
         WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '3 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '6 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '9 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '12 month'+INTERVAL '-3 MILLISECONDS '
         END AS dtEndDate,
					v_dtFromDate AS new_date
         UNION ALL
         SELECT
         GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID) AS yr,
					GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainID) AS qq,
					('Q' || cast(GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainID) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID) AS VARCHAR(30))) AS MonthYear,
					CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainID)
         WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '0 month'
         WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '3 month'
         WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '6 month'
         WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '9 month'
         END AS dtStartDate,
					CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainID)
         WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '3 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '6 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '9 month'+INTERVAL '-3 MILLISECONDS '
         WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainID)::INTEGER,v_numDomainID)+INTERVAL '12 month'+INTERVAL '-3 MILLISECONDS '
         END AS dtEndDate,
					new_date+INTERVAL '1 day' AS new_date
         FROM CTE
         WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
         yr,qq,MonthYear,dtStartDate,dtEndDate
         FROM
         CTE
         GROUP BY
         yr,qq,MonthYear,dtStartDate,dtEndDate
         ORDER BY
         yr,qq;
         UPDATE
         tt_TEMPYEARMONTH
         SET
         EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      end if;

		v_columns := COALESCE((SELECT string_agg( ', coalesce(' || MonthYear  || ','''''''') AS "' || REPLACE(MonthYear,'_',' ') || '"','' ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTH),''); 
		v_PivotColumns := COALESCE((SELECT string_agg(MonthYear,', ' ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTH),''); 

      DROP TABLE IF EXISTS tt_TEMPYEARFINAL CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARFINAL AS
         SELECT
         coalesce(vcData,'') AS "Campaign"
			,coalesce((SELECT
         SUM(monDealAmount)
         FROM
         CompanyInfo CI
         INNER JOIN
         DivisionMaster DM
         ON
         CI.numCompanyId = DM.numCompanyID
         INNER JOIN
         OpportunityMaster OM
         ON
         DM.numDivisionID = OM.numDivisionId
         WHERE
         CI.numDomainID = v_numDomainID
         AND DM.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND CI.vcHow = LD.numListItemID
         AND OM.tintopptype = 1
         AND OM.tintoppstatus = 1
         AND OM.bintCreatedDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate),0) AS "monTotalIncome"
			,coalesce((SELECT
         COUNT(DM.numDivisionID)
         FROM
         CompanyInfo CI
         INNER JOIN
         DivisionMaster DM
         ON
         CI.numCompanyId = DM.numCompanyID
         WHERE
         CI.numDomainID = v_numDomainID
         AND CI.vcHow = LD.numListItemID
         AND DM.bintCreatedDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate),0) AS "numLeadCount"
			,coalesce((SELECT
         COUNT(numDivisionId)
         FROM(SELECT
            DM.numDivisionID
							,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate),0) AS "numCustomerCount"
			,COALESCE((SELECT string_agg(CAST(numDivisionId AS TEXT),', ')
         FROM(SELECT
            DM.numDivisionID
							,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate),'') AS "DivisionIDs"
			,coalesce((SELECT
         AVG(DATE_PART('day',dtFirstSalesOrderDate -dtOrgCreatedDate))
         FROM(SELECT
            DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID,DM.bintCreatedDate) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate),0) AS "intAverageDaysToConvert"
			,coalesce((SELECT
         SUM(monDealAmount)
         FROM
         OpportunityMaster
         WHERE
         numDomainId = v_numDomainID
         AND tintopptype = 1
         AND tintoppstatus = 1
         AND numDivisionId IN(SELECT
            numDivisionId
            FROM(SELECT
               DM.numDivisionID
				,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
               FROM
               CompanyInfo CI
               INNER JOIN
               DivisionMaster DM
               ON
               CI.numCompanyId = DM.numCompanyID
               INNER JOIN
               OpportunityMaster OM
               ON
               DM.numDivisionID = OM.numDivisionId
               WHERE
               CI.numDomainID = v_numDomainID
               AND DM.numDomainID = v_numDomainID
               AND OM.numDomainId = v_numDomainID
               AND CI.vcHow = LD.numListItemID
               AND OM.tintopptype = 1
               AND OM.tintoppstatus = 1
               GROUP BY
               DM.numDivisionID) TEMP
            WHERE
            dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN TYM.StartDate AND TYM.EndDate)),0) AS "monCampaignIncome"
			,coalesce((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID = BD.numBillID WHERE BH.numDomainId = v_numDomainID AND coalesce(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN TYM.StartDate AND TYM.EndDate),0) AS "monCampaignExpense"
			,TYM.* FROM
         Listdetails LD
         LEFT JOIN LATERAL(select * from tt_TEMPYEARMONTH) AS TYM on TRUE
         WHERE
         LD.numDomainid = v_numDomainID
         AND LD.numListID = 18
         AND (coalesce(v_numLeadSource,0) = 0 OR LD.numListItemID = v_numLeadSource);
      UPDATE
      tt_TEMPYEARFINAL
      SET
      numConversionPercent =(CASE WHEN "numLeadCount" > 0 THEN CAST((coalesce("numCustomerCount",0)*100) AS decimal)/"numLeadCount"::bigint ELSE 0.00 END),monGrossProfit = coalesce("monCampaignIncome",0) -coalesce("monCampaignExpense",0);
      v_sql := 'SELECT 
					row_id AS "Campaign"' || coalesce(v_columns,'') || '
				FROM 
					fn_GetPivotQueryResult(''SELECT 
												row_id' || v_columns || ' 
											FROM 
											crosstab (
												''''SELECT
							CAST("Campaign" AS TEXT) As row_id
							,MonthYear
							,CONCAT(''''''''{''''''''
							,''''''''TotalIncome:'''''''',COALESCE("monTotalIncome",0),'''''''',''''''''
							,''''''''LeadCount:'''''''',COALESCE("numLeadCount",0),'''''''',''''''''
							,''''''''CostPerLead:'''''''',(CASE WHEN COALESCE("numLeadCount",0) > 0 THEN COALESCE("monCampaignExpense",0) / COALESCE("numLeadCount",0) ELSE 0 END),'''''''',''''''''
							,''''''''CustomerCount:'''''''',COALESCE("numCustomerCount",0),'''''''',''''''''
							,''''''''CostPerCustomer:'''''''',(CASE WHEN COALESCE("numCustomerCount",0) > 0 THEN COALESCE("monCampaignExpense",0) / COALESCE("numCustomerCount",0) ELSE 0 END),'''''''',''''''''
							,''''''''DivisionIDs:'''''''''''''''''''''''',COALESCE("DivisionIDs",''''''''''''''''),'''''''''''''''''''''''',''''''''
							,''''''''ConversionPercent:'''''''',(CASE WHEN "numLeadCount" > 0 THEN (COALESCE("numCustomerCount",0) * 100)/"numLeadCount" ELSE 0.00 END),'''''''',''''''''
							,''''''''AverageDaysToConvert:'''''''',COALESCE("intAverageDaysToConvert",0),'''''''',''''''''
							,''''''''CampaignIncome:'''''''',COALESCE("monCampaignIncome",0),'''''''',''''''''
							,''''''''CampaignExpense:'''''''',COALESCE("monCampaignExpense",0),'''''''',''''''''
							,''''''''GrossProfit:'''''''',COALESCE("monCampaignIncome",0) - COALESCE("monCampaignExpense",0),''''''''}'''''''') AS MonthTotal
						FROM
							tt_TEMPYEARFINAL'''') AS final_result(row_id TEXT, ' || REPLACE(v_PivotColumns,',',' TEXT,') || ' TEXT);'',0) as t(row_id TEXT,' || REPLACE(v_PivotColumns,',',' TEXT,') || ' TEXT)
					ORDER BY row_id';
      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
   ELSE
      DROP TABLE IF EXISTS tt_TEMPFINAL CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINAL ON COMMIT DROP AS
         SELECT
         LD.numListItemID
			,LD.vcData
			,coalesce((SELECT
         SUM(monDealAmount)
         FROM
         CompanyInfo CI
         INNER JOIN
         DivisionMaster DM
         ON
         CI.numCompanyId = DM.numCompanyID
         INNER JOIN
         OpportunityMaster OM
         ON
         DM.numDivisionID = OM.numDivisionId
         WHERE
         CI.numDomainID = v_numDomainID
         AND DM.numDomainID = v_numDomainID
         AND OM.numDomainId = v_numDomainID
         AND CI.vcHow = LD.numListItemID
         AND OM.tintopptype = 1
         AND OM.tintoppstatus = 1
         AND OM.bintCreatedDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate),0) AS monTotalIncome
			,coalesce((SELECT
         COUNT(DM.numDivisionID)
         FROM
         CompanyInfo CI
         INNER JOIN
         DivisionMaster DM
         ON
         CI.numCompanyId = DM.numCompanyID
         WHERE
         CI.numDomainID = v_numDomainID
         AND CI.vcHow = LD.numListItemID
         AND DM.bintCreatedDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate),0) AS numLeadCount
			,coalesce((SELECT
         COUNT(numDivisionId)
         FROM(SELECT
            DM.numDivisionID
							,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate),0) AS numCustomerCount
			,COALESCE((SELECT string_agg(CAST(numDivisionId AS TEXT),', ')
         FROM(SELECT
            DM.numDivisionID
			,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate),'') AS DivisionIDs
			,coalesce((SELECT
         AVG(DATE_PART('day',dtFirstSalesOrderDate -dtOrgCreatedDate))
         FROM(SELECT
            DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
            FROM
            CompanyInfo CI
            INNER JOIN
            DivisionMaster DM
            ON
            CI.numCompanyId = DM.numCompanyID
            INNER JOIN
            OpportunityMaster OM
            ON
            DM.numDivisionID = OM.numDivisionId
            WHERE
            CI.numDomainID = v_numDomainID
            AND DM.numDomainID = v_numDomainID
            AND OM.numDomainId = v_numDomainID
            AND CI.vcHow = LD.numListItemID
            AND OM.tintopptype = 1
            AND OM.tintoppstatus = 1
            GROUP BY
            DM.numDivisionID,DM.bintCreatedDate) TEMP
         WHERE
         dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate),0) AS intAverageDaysToConvert
			,coalesce((SELECT
         SUM(monDealAmount)
         FROM
         OpportunityMaster
         WHERE
         numDomainId = v_numDomainID
         AND tintopptype = 1
         AND tintoppstatus = 1
         AND numDivisionId IN(SELECT
            numDivisionId
            FROM(SELECT
               DM.numDivisionID
														,MIN(OM.bintCreatedDate) AS dtFirstSalesOrderDate
               FROM
               CompanyInfo CI
               INNER JOIN
               DivisionMaster DM
               ON
               CI.numCompanyId = DM.numCompanyID
               INNER JOIN
               OpportunityMaster OM
               ON
               DM.numDivisionID = OM.numDivisionId
               WHERE
               CI.numDomainID = v_numDomainID
               AND DM.numDomainID = v_numDomainID
               AND OM.numDomainId = v_numDomainID
               AND CI.vcHow = LD.numListItemID
               AND OM.tintopptype = 1
               AND OM.tintoppstatus = 1
               GROUP BY
               DM.numDivisionID) TEMP
            WHERE
            dtFirstSalesOrderDate + (-1*v_ClientTimezoneOffset::bigint || ' minute')::interval BETWEEN v_dtFromDate AND v_dtToDate)),0) AS monCampaignIncome
			,coalesce((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID = BD.numBillID WHERE BH.numDomainId = v_numDomainID AND coalesce(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN v_dtFromDate AND v_dtToDate),0) AS monCampaignExpense
		
         FROM
         Listdetails LD
         WHERE
         LD.numDomainid = v_numDomainID
         AND LD.numListID = 18
         AND (coalesce(v_numLeadSource,0) = 0 OR LD.numListItemID = v_numLeadSource);
      open SWV_RefCur for
      SELECT
      coalesce(vcData,'') AS "Campaign"
			,CONCAT('{','TotalIncome:',coalesce(monTotalIncome,0),',','LeadCount:',coalesce(numLeadCount,0),
      ',','CostPerLead:',(CASE WHEN coalesce(numLeadCount,0) > 0 THEN coalesce(monCampaignExpense,0)/coalesce(numLeadCount,0) ELSE 0 END),
      ',','CustomerCount:',coalesce(numCustomerCount,0),',',
      'CostPerCustomer:',(CASE WHEN coalesce(numCustomerCount,0) > 0 THEN coalesce(monCampaignExpense,0)/coalesce(numCustomerCount,0) ELSE 0 END),
      ',','DivisionIDs:''',coalesce(DivisionIDs,''),''',','ConversionPercent:',
      (CASE WHEN numLeadCount > 0 THEN CAST((coalesce(numCustomerCount,0)*100) AS decimal)/numLeadCount ELSE 0.00 END),',','AverageDaysToConvert:',
      coalesce(intAverageDaysToConvert,0),',','CampaignIncome:',
      coalesce(monCampaignIncome,0),',','CampaignExpense:',coalesce(monCampaignExpense,0),
      ',','GrossProfit:',coalesce(monCampaignIncome,0) -coalesce(monCampaignExpense,0),
      '}') AS "Totals"
      FROM
      tt_TEMPFINAL
      ORDER BY
      vcData;
   end if;
   RETURN;
END; $$;


