-- Stored procedure definition script USP_RemoveCouponCode for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RemoveCouponCode(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_cookieId TEXT DEFAULT NULL,
	v_numUserCntId NUMERIC DEFAULT 0,
	v_vcSendCoupon VARCHAR(200) DEFAULT '',
	v_numSiteID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   CartItems
   SET
   PromotionID = 0,PromotionDesc = '',monTotAmount = monTotAmtBefDiscount,fltDiscount = 0,
   vcCoupon = '',bitParentPromotion = false
   WHERE
   numDomainId = v_numDomainID AND
   vcCookieId = v_cookieId AND
   numUserCntId = v_numUserCntId AND
   PromotionID IN(coalesce((SELECT  PromotionID FROM CartItems WHERE numDomainId = v_numDomainID AND vcCookieId = v_cookieId AND vcCoupon = v_vcSendCoupon AND numUserCntId = v_numUserCntId LIMIT 1),0));

   PERFORM USP_ManageECommercePromotion(v_numUserCntId,v_numDomainID,v_cookieId,0,v_numSiteID);
   RETURN;
END; $$;


