-- Stored procedure definition script USP_ManagekitParentsForChildItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagekitParentsForChildItems(v_numItemDetailID NUMERIC(18,0),
v_numItemKitID NUMERIC(18,0),  
v_numChildItemID NUMERIC(18,0),  
v_numQtyItemsReq NUMERIC(18,0),  
v_byteMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then

      IF EXISTS(SELECT numItemCode FROM Item WHERE numItemCode = v_numItemKitID AND (coalesce(bitAssembly,false) = true OR coalesce(bitKitParent,false) = true)) then
	
         IF v_numItemKitID = v_numChildItemID then
		
            RAISE EXCEPTION 'CAN_NOT_ADD_PARENT_AS_CHILD';
         ELSEIF EXISTS(SELECT numItemDetailID FROM ItemDetails WHERE numItemKitID = v_numItemKitID AND numChildItemID = v_numChildItemID)
         then
		
            RAISE EXCEPTION 'CHILD_ITEM_ALREADY_ADDED';
         ELSEIF EXISTS(SELECT numItemCode FROM Item WHERE numItemCode = v_numItemKitID AND coalesce(bitAssembly,false) = true) AND EXISTS(SELECT numItemCode FROM Item WHERE numItemCode = v_numChildItemID AND coalesce(bitKitParent,false) = true)
         then
		
            RAISE EXCEPTION 'CAN_NOT_ADD_KIT_AS_ASSEMBLY_CHILD';
         ELSEIF EXISTS(SELECT numItemCode FROM Item WHERE numItemCode = v_numItemKitID AND coalesce(bitKitParent,false) = true) AND EXISTS(SELECT numItemCode FROM Item WHERE numItemCode = v_numChildItemID AND coalesce(bitKitParent,false) = true) AND EXISTS(SELECT numItemDetailID FROM ItemDetails WHERE numChildItemID = v_numItemKitID)
         then
		
            RAISE EXCEPTION 'CAN_NOT_ADD_KIT_AS_KIT_IS_ALREADY_CHILD_OTHER_ITEM';
         ELSE
            INSERT INTO ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,sintOrder)
			VALUES(v_numItemKitID,v_numChildItemID,v_numQtyItemsReq,coalesce((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID = v_numItemKitID), 0)+1);
         end if;
      end if;
   end if;  
  
   IF v_byteMode = 1 then

      DELETE FROM ItemDetails WHERE numItemKitID = v_numItemKitID and numChildItemID = v_numChildItemID;
   end if;
   IF v_byteMode = 2 then

      DELETE FROM ItemDetails WHERE numItemDetailID = v_numItemDetailID;
   end if;
   RETURN;
END; $$;


