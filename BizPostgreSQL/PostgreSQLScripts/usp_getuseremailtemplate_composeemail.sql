CREATE OR REPLACE FUNCTION USP_GetUserEmailTemplate_ComposeEmail(v_numUserID NUMERIC(9,0) DEFAULT 0,
                                                    v_numDomainID NUMERIC(9,0) DEFAULT 0,
                                                    v_isMarketing BOOLEAN DEFAULT false,
													v_Screen VARCHAR(20) DEFAULT NULL,
													v_numGroupId NUMERIC(18,0) DEFAULT 0,
													v_numCategoryId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dynamicQuery  TEXT DEFAULT '';
BEGIN
   IF v_isMarketing = true then
    
      v_dynamicQuery := 'SELECT  vcDocName,
                numGenericDocID
        FROM    GenericDocuments
        WHERE   numDocCategory = 369
                --AND vcDocumentSection = ''M''
                AND numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND COALESCE(VcFileName,'''') not iLIKE ''#SYS#--''	
				AND numModuleID <> 8  --Exclude BizDoc Templates';
   end if;
    
   IF v_isMarketing = false then

      IF v_Screen = 'Opp/Order' then
         v_dynamicQuery := 'SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType = 1 --1 =generic,2=specific
						--AND COALESCE(vcDocumentSection,'''') <> ''M''
						AND numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND COALESCE(VcFileName,'''') not iLIKE ''#SYS#--''
						AND numModuleID = 2 -- Opp/Order Templates';
      ELSEIF v_Screen = 'BizDoc'
      then
         v_dynamicQuery := 'SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType = 1 
						--AND COALESCE(vcDocumentSection,'''') <> ''M''
						AND numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND COALESCE(VcFileName,'''') not iLIKE ''#SYS#--''
						AND  (numModuleID = 1 OR numModuleID = 45 OR numModuleID = 2 OR numModuleID = 8) ';
      ELSE
		--AND  (numModuleID = 1 OR numModuleID = 45 or numModuleID = 0 or COALESCE(numModuleID, 0) =0)
         v_dynamicQuery := 'SELECT  vcDocName,
					numGenericDocID
			FROM    GenericDocuments
			WHERE   numDocCategory = 369
					AND tintDocumentType = 1 
					--AND COALESCE(vcDocumentSection,'''') <> ''M''
					AND numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND COALESCE(VcFileName,'''') not iLIKE ''#SYS#--''
					 ';
      end if;
   end if;
   IF(v_numCategoryId > 0) then

      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND numCategoryId=' || SUBSTR(CAST(v_numCategoryId AS VARCHAR(30)),1,30) || ' ';
   end if;
   IF(v_numGroupId > 0) then

      v_dynamicQuery := coalesce(v_dynamicQuery,'') || '  AND (' || SUBSTR(CAST(v_numGroupId AS VARCHAR(30)),1,30) || ' IN (SELECT Id FROM SplitIds(COALESCE(vcGroupsPermission,''''),'','')) OR COALESCE(vcGroupsPermission,''0'')=''0'') ';
   end if;
   RAISE NOTICE '%',v_dynamicQuery;
   OPEN SWV_RefCur FOR EXECUTE v_dynamicQuery;
   RETURN;
END; $$;



