-- Stored procedure definition script usp_DeleteSelectedSearchRecords for PostgreSQL
CREATE OR REPLACE FUNCTION usp_DeleteSelectedSearchRecords(v_vcEntityIdList VARCHAR(4000),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intFirstComma  INTEGER;                
   v_intFirstUnderScore  INTEGER;                
              
   v_numCompanyId  NUMERIC(9,0);              
   v_numContactId  NUMERIC(9,0);              
   v_numDivisionId  NUMERIC(9,0);              
              
   v_vcCompContDivValue  VARCHAR(30);              
             
   v_intDivCount  NUMERIC;                     
   v_intContCount  NUMERIC;         
          
   v_DeletionFlag  INTEGER;
   v_numExtranetID  NUMERIC;        
   v_numExtranetIDCount  NUMERIC;        
   v_numCampaignId  NUMERIC;        
   v_numCaseId  NUMERIC;        

   v_numProId  NUMERIC;        
   v_numOppId  NUMERIC;        
   v_numProjectForOppId  NUMERIC;        
   v_numOppBizDocId  NUMERIC;
   SWV_error INTEGER;
BEGIN
   SWV_error := 0;
   v_vcEntityIdList := coalesce(v_vcEntityIdList,'') || ',';              
   v_DeletionFlag := 1;        
           
   v_intFirstComma := POSITION(',' IN v_vcEntityIdList);                
              
   WHILE v_intFirstComma > 0 LOOP
      v_vcCompContDivValue := SUBSTR(v_vcEntityIdList,0,v_intFirstComma);
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numCompanyId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_vcCompContDivValue := SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue));
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numContactId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_numDivisionId := CAST(SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue)) AS NUMERIC(9,0));
      RAISE NOTICE '%',v_numCompanyId;
      RAISE NOTICE '%',v_numContactId;
      RAISE NOTICE '%',v_numDivisionId;          
     /*----------------DELETION STARTS HERE----------------------*/        
      DELETE FROM AOIContactLink WHERE numContactID = v_numContactId;
      RAISE NOTICE 'AOIContactLink Deleted';
      DELETE FROM Communication WHERE numContactId = v_numContactId and numDivisionID = v_numDivisionId;
      RAISE NOTICE 'Communication Deleted';       
    
  --Deleting ExtraNetAccounts        
      select   numExtranetID INTO v_numExtranetID FROM ExtranetAccountsDtl WHERE numContactID = CAST(v_numContactId AS VARCHAR);
      DELETE FROM ExtranetAccountsDtl WHERE numContactID = CAST(v_numContactId AS VARCHAR);
      RAISE NOTICE 'ExtranetAccountsDtl Deleted';
      select   COUNT(numExtranetID) INTO v_numExtranetIDCount FROM ExtranetAccountsDtl WHERE numExtranetID IN(SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivisionId AND numCompanyID = v_numCompanyId);
      IF(v_numExtranetIDCount = 0) then
 
         DELETE FROM ExtarnetAccounts WHERE  numDivisionID = v_numDivisionId AND  numCompanyID = v_numCompanyId;
         RAISE NOTICE 'ExtarnetAccounts Deleted';
      end if;        
        
 --Deleting Campaigns        
      select   numCampaignId INTO v_numCampaignId FROM CampaignDetails WHERE numContactId = v_numContactId AND  numCompanyId = v_numCompanyId;
      DELETE FROM CampaignDetails WHERE numContactId = v_numContactId AND  numCompanyId = v_numCompanyId;
      RAISE NOTICE 'CampaignDetails Deleted';        
 /*
 DECLARE @CampaignDivisionCount as Numeric        
 SELECT @CampaignDivisionCount = COUNT(numContactId) FROM CampaignDetails WHERE numContactId IN (SELECT DISTINCT numContactId FROM DivisionMaster WHERE numDivisionId = @numDivisionID)        
 IF(@CampaignDivisionCount=0)        
 BEGIN        
  DELETE FROM CampaignDivision WHERE numDivisionID=@numDivisionID AND numCampaignID = @numCampaignId        
  PRINT 'CampaignDivision Deleted'        
 END        
 */     
 --Deleting Cases        
      select   numCaseId INTO v_numCaseId FROM Cases WHERE numDivisionID = v_numDivisionId AND  numContactId = v_numContactId;
      DELETE FROM CaseSolutions WHERE numCaseId = v_numCaseId;
      RAISE NOTICE 'CaseSolutions Deleted';
      DELETE FROM Cases WHERE numDivisionID = v_numDivisionId AND  numContactId = v_numContactId AND numCaseId = v_numCaseId;
      RAISE NOTICE 'Cases Deleted';
      DELETE FROM FollowUpHistory WHERE numDivisionID = v_numDivisionId;
      RAISE NOTICE 'FollowUpHistory Deleted';
      DELETE FROM Leads WHERE  numCompanyId = v_numCompanyId;
      RAISE NOTICE 'Leads Deleted';
      DELETE FROM UniversalSupportKeyMaster WHERE numDivisionID = v_numDivisionId;
      RAISE NOTICE 'UniversalSupportKeyMaster Deleted';
      select   numProId INTO v_numProId FROM ProjectsContacts WHERE numContactID = v_numContactId;
      select   numOppId INTO v_numOppId FROM OpportunityMaster WHERE numDivisionId = v_numDivisionId AND  numContactId = v_numContactId;
      select   numProId INTO v_numProjectForOppId FROM ProjectsMaster WHERE numOppId = v_numOppId;
      DELETE FROM ProjectsContacts WHERE numProId = v_numProId and numContactID = v_numContactId;
      DELETE FROM ProjectsContacts WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsContacts Deleted';
      DELETE FROM ProjectsSubStageDetails WHERE numProId = v_numProId;
      DELETE FROM ProjectsSubStageDetails WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsSubStageDetails Deleted';
      DELETE FROM ProjectsStageDetails WHERE numProId = v_numProId;
      DELETE FROM ProjectsStageDetails WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsStageDetails Deleted';
      DELETE FROM ProjectsDependency WHERE numProId = v_numProId;
      DELETE FROM ProjectsDependency WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsDependency Deleted';
      DELETE FROM ProjectsTime WHERE numProId = v_numProId;
      DELETE FROM ProjectsTime WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsTime Deleted';
      DELETE FROM ProjectsExpense WHERE numProId = v_numProId;
      DELETE FROM ProjectsExpense WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsExpense Deleted';
      DELETE FROM ProjectsMaster WHERE numProId = v_numProId;
      DELETE FROM ProjectsMaster WHERE numProId = v_numProjectForOppId;
      RAISE NOTICE 'ProjectsMaster Deleted';
      DELETE FROM OpportunitySubStageDetails WHERE numOppId = v_numOppId;
      RAISE NOTICE 'OpportunitySubStageDetails Deleted';
      DELETE FROM OpportunityStageDetails WHERE numoppid = v_numOppId;
      RAISE NOTICE 'OpportunityStageDetails Deleted';
      DELETE FROM OpportunityDependency WHERE numOpportunityId = v_numOppId;
      RAISE NOTICE 'OpportunityDependency Deleted';
      DELETE FROM OpportunityExpense WHERE numOppId = v_numOppId;
      RAISE NOTICE 'OpportunityExpense Deleted';
      DELETE FROM OpportunityItems WHERE numOppId = v_numOppId;
      RAISE NOTICE 'OpportunityTime Deleted';
      DELETE FROM timeandexpense WHERE numOppId = v_numOppId;
      RAISE NOTICE 'Opportunity Time And Expense Deleted';
      select   numBizDocId INTO v_numOppBizDocId FROM OpportunityBizDocs WHERE numoppid = v_numOppId;
      DELETE FROM OpportunityTime WHERE numOppId = v_numOppId;
      RAISE NOTICE 'OpportunityTime Deleted';
      DELETE FROM OpportunityBizDocDtl WHERE numBizDocID = v_numOppBizDocId;
      RAISE NOTICE 'OpportunityBizDocDtl Deleted';
      DELETE FROM OpportunityBizDocs WHERE numoppid = v_numOppId;
      RAISE NOTICE 'OpportunityBizDocs Deleted';
      DELETE FROM OpportunityContact WHERE numOppId = v_numOppId; --Delete all opportunities for the contact (whatever the associated contacts in OpportunityContact is)    
      RAISE NOTICE 'Opportunity Contact Deleted';
      DELETE FROM ProjectsMaster WHERE numOppId = v_numOppId;
      RAISE NOTICE 'ProjectsMaster Deleted';
      DELETE FROM OpportunityMaster WHERE numOppId = v_numOppId AND numContactId = v_numContactId;
      RAISE NOTICE 'SurveyRespondentsMaster Deleted';
      UPDATE SurveyRespondentsMaster SET numRegisteredRespondentContactId = NULL WHERE numRegisteredRespondentContactId = v_numContactId;
      DELETE FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      RAISE NOTICE 'Contact Deleted';
      begin
         select   COUNT(*) INTO v_intContCount FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionId;
         EXCEPTION
         WHEN OTHERS
         THEN
            SWV_error := -1;
      end;
      IF v_intContCount = 0 then --If There are no more Contacts for the selected division, then delete division.                
      
         SWV_error := 0;
         begin
            DELETE FROM ExtarnetAccounts WHERE  numDivisionID = v_numDivisionId;
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         SWV_error := 0;
         begin
            DELETE FROM CompanyAssociations WHERE numDivisionID = v_numDivisionId OR numAssociateFromDivisionID = v_numDivisionId;
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         RAISE NOTICE 'CompanyAssociations Deleted';
         SWV_error := 0;
         begin
            DELETE FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         RAISE NOTICE 'Division Deleted';
         SWV_error := 0;
         begin
            select   COUNT(*) INTO v_intDivCount FROM  DivisionMaster WHERE numCompanyID = v_numCompanyId;
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         IF v_intDivCount = 0 then      -- If there are no more Divisions for the selected Company then Delete Company.                
          
            SWV_error := 0;
            begin
               DELETE FROM CompanyInfo WHERE numCompanyId = v_numCompanyId;
               EXCEPTION
               WHEN OTHERS
               THEN
                  SWV_error := -1;
            end;
            RAISE NOTICE 'Company Deleted';
         end if;
      end if;
      IF SWV_error <> 0 then
         v_DeletionFlag := 0;
      end if;
      SWV_error := 0;        
     /*----------------DELETION ENDS HERE----------------------*/        
              
      v_vcEntityIdList := SUBSTR(v_vcEntityIdList,v_intFirstComma+1,LENGTH(v_vcEntityIdList));
      v_intFirstComma := POSITION(',' IN v_vcEntityIdList);
   END LOOP;              
            
            
            
   open SWV_RefCur for SELECT v_DeletionFlag;            
   RETURN;
END; $$;












