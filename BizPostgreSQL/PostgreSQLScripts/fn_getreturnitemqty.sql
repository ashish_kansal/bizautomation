-- Function definition script fn_GetReturnItemQty for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetReturnItemQty(v_numOppItemCode BIGINT)
RETURNS BIGINT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Qty  NUMERIC(9,0);
   v_Qty1  NUMERIC(9,0);
   v_Status  VARCHAR(20);
BEGIN
   select   GetListIemName(numreturnstatus), coalesce(numqtyreturned,0), coalesce(numQtyToReturn,0) INTO v_Status,v_Qty,v_Qty1 FROM Returns WHERE numOppItemCode = v_numOppItemCode;

   IF v_Status = 'Returned' then

      v_Qty := v_Qty;
   ELSE
      v_Qty := v_Qty1;
   end if;

   RETURN coalesce(v_Qty,0);
END; $$;

