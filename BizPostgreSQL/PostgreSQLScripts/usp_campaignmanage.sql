-- Stored procedure definition script USP_CampaignManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CampaignManage(INOUT v_numCampaignId NUMERIC(9,0) DEFAULT 0 ,        
v_vcCampaignName VARCHAR(200) DEFAULT '',
v_numCampaignStatus NUMERIC(9,0) DEFAULT 0,        
v_numCampaignType NUMERIC(9,0) DEFAULT 0,        
v_intLaunchDate TIMESTAMP DEFAULT NULL,
v_intEndDate TIMESTAMP DEFAULT NULL,        
v_numRegion NUMERIC(9,0) DEFAULT 0,        
v_numNoSent NUMERIC(9,0) DEFAULT 0,        
v_monCampaignCost DECIMAL(20,5) DEFAULT 0,
v_bitIsOnline BOOLEAN DEFAULT NULL,
v_bitIsMonthly BOOLEAN DEFAULT NULL,
v_vcCampaignCode	VARCHAR(500) DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
INOUT v_IsDuplicate BOOLEAN DEFAULT false ,
v_bitActive BOOLEAN DEFAULT true)
LANGUAGE plpgsql
   AS $$
BEGIN
        

/*Landing page duplication validation*/
   IF v_bitIsOnline = true AND(SELECT COUNT(*) FROM CampaignMaster WHERE numCampaignID <> v_numCampaignId AND numDomainID = v_numDomainID AND bitIsOnline = true AND vcCampaignCode = v_vcCampaignCode) > 0 then

      v_IsDuplicate := true;
      RETURN;
   end if;

   if v_numCampaignId = 0 then

      insert into CampaignMaster(vcCampaignName,
numCampaignStatus,
numCampaignType,
intLaunchDate,
intEndDate,
numRegion,
numNoSent,
monCampaignCost,
bitIsOnline,
bitIsMonthly,
vcCampaignCode,
numDomainID,
numCreatedBy,
bintCreatedDate,
numModifiedBy,
bintModifiedDate,
bitActive)
values(v_vcCampaignName,
v_numCampaignStatus,
v_numCampaignType,
v_intLaunchDate,
v_intEndDate,
v_numRegion,
v_numNoSent,
v_monCampaignCost,
v_bitIsOnline,
v_bitIsMonthly,
v_vcCampaignCode,
v_numDomainID,
v_numUserCntID,
TIMEZONE('UTC',now()),
v_numUserCntID,
TIMEZONE('UTC',now()),
v_bitActive) RETURNING numCampaignId INTO v_numCampaignId;

   end if;        
   if v_numCampaignId > 0 then

      update CampaignMaster set
      vcCampaignName = v_vcCampaignName,numCampaignStatus = v_numCampaignStatus,
      numCampaignType = v_numCampaignType,intLaunchDate = v_intLaunchDate,intEndDate = v_intEndDate,
      numRegion = v_numRegion,numNoSent = v_numNoSent,monCampaignCost = v_monCampaignCost,
      bitIsOnline = v_bitIsOnline,bitIsMonthly = v_bitIsMonthly,
      vcCampaignCode = v_vcCampaignCode,numDomainID = v_numDomainID,numModifiedBy = v_numUserCntID,
      bintModifiedDate = TIMEZONE('UTC',now()),bitActive = v_bitActive
      where v_numCampaignId = numCampaignID;
   end if;
/****** Object:  StoredProcedure [dbo].[usp_caseTime1]    Script Date: 07/26/2008 16:15:05 ******/
END; $$;


