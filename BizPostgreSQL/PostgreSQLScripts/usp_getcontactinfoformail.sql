-- Stored procedure definition script usp_GetContactInfoForMail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactInfoForMail(v_vcEmail VARCHAR(50) DEFAULT '',
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   AC.vcFirstName,AC.vcLastname,cast(CI.vcWebSite as VARCHAR(255)),AC.vcEmail,cast(CI.vcCompanyName as VARCHAR(255))
   FROM
   CompanyInfo CI
   INNER JOIN
   DivisionMaster DM
   ON
   DM.numCompanyID = CI.numCompanyId
   INNER JOIN
   AdditionalContactsInformation AC
   ON
   AC.numDivisionId = DM.numDivisionID
   WHERE
   AC.vcEmail ilike v_vcEmail
   and AC.numDomainID = v_numDomainID;
END; $$;












