-- Stored procedure definition script USP_CheckCanbeReOpenOppertunity for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckCanbeReOpenOppertunity(v_intOpportunityId NUMERIC(18,0),
v_bitCheck BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_OppType SMALLINT;   
   v_Sel  INTEGER;   
   v_itemcode  NUMERIC;     
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numWLocationID  NUMERIC(18,0);                                
   v_numUnits  DOUBLE PRECISION;                                            
   v_numQtyShipped  DOUBLE PRECISION;
   v_onHand  DOUBLE PRECISION;                                                                                 
   v_numoppitemtCode  NUMERIC(9,0);
   v_Kit  BOOLEAN;                   
   v_LotSerial  BOOLEAN;                   
   v_numQtyReceived  DOUBLE PRECISION;
   SWV_RowCount INTEGER;
BEGIN
   v_Sel := 0;          
   select   tintopptype, numDomainId INTO v_OppType,v_numDomainID FROM
   OpportunityMaster WHERE
   numOppId = v_intOpportunityId;
 
   IF v_OppType = 2 then
	
      select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), numWarehouseItmsID, coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true and bitAssembly = true THEN 0 WHEN bitKitParent = true THEN 1 ELSE 0 END), (CASE WHEN I.bitLotNo = true OR I.bitSerialized = true THEN 1 ELSE 0 END) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
      v_numWareHouseItemID,v_numWLocationID,v_Kit,v_LotSerial FROM
      OpportunityItems OI
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      AND numOppId = v_intOpportunityId
      AND (bitDropShip = false OR bitDropShip IS NULL) WHERE
      charitemtype = 'P'   ORDER BY
      OI.numoppitemtCode LIMIT 1;
      WHILE v_numoppitemtCode > 0 LOOP  
			-- Selected Global Warehouse Location 
         IF EXISTS(SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomainID AND numOppID = v_intOpportunityId AND numOppItemID = v_numoppitemtCode) then
			
            IF(SELECT
            COUNT(*)
            FROM
            OpportunityItemsReceievedLocation OIRL
            INNER JOIN
            WareHouseItems WI
            ON
            OIRL.numWarehouseItemID = WI.numWareHouseItemID
            WHERE
            OIRL.numDomainId = v_numDomainID
            AND numOppID = v_intOpportunityId
            AND numOppItemID = v_numoppitemtCode
            GROUP BY
            OIRL.numWarehouseItemID,WI.numOnHand
            HAVING
            WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0 then
				
               IF v_Sel = 0 then 
                  v_Sel := 1;
               end if;
            ELSE
               v_numUnits := v_numUnits -(SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomainID AND numOppID = v_intOpportunityId AND numOppItemID = v_numoppitemtCode);
            end if;
         end if;
         IF v_Sel = 0 then
			
            select   coalesce(numOnHand,0) INTO v_onHand FROM
            WareHouseItems WHERE
            numWareHouseItemID = v_numWareHouseItemID;
            IF v_onHand < v_numUnits then
				
               IF v_Sel = 0 then 
                  v_Sel := 1;
               end if;
            end if;
         end if;
         select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), numWarehouseItmsID, coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true AND bitAssembly = true THEN 0 WHEN bitKitParent = true THEN 1 ELSE 0 END), (CASE WHEN I.bitLotNo = true OR I.bitSerialized = true THEN 1 ELSE 0 END) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
         v_numWareHouseItemID,v_numWLocationID,v_Kit,v_LotSerial FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode and numOppId = v_intOpportunityId WHERE
         charitemtype = 'P'
         AND OI.numoppitemtCode > v_numoppitemtCode
         AND (bitDropShip = false OR bitDropShip IS NULL)   ORDER BY
         OI.numoppitemtCode LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numoppitemtCode := 0;
         end if;
      END LOOP;
   end if;      


   IF EXISTS(SELECT * FROM ReturnHeader WHERE numOppId = v_intOpportunityId) then
	
      v_Sel := -1*v_OppType;
   end if;	
                                    
  
   open SWV_RefCur for SELECT v_Sel;
END; $$;












