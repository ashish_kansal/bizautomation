DROP FUNCTION IF EXISTS usp_vendorcosttable_updatedefaultrange;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_updatedefaultrange
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_vcCostRange JSONB
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	UPDATE Domain SET vcDefaultCostRange=p_vcCostRange WHERE numDomainID=p_numdomainid;
END; $$;