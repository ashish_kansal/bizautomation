-- Trigger definition script TRG_WarehouseItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION TRG_WarehouseItems_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBackOrder  NUMERIC(9,0);
   v_numOnHand  NUMERIC(9,0);
   v_numWareHouseItemID  NUMERIC(9,0);
BEGIN
   select   numBackOrder, numOnHand, numWareHouseItemID INTO v_numBackOrder,v_numOnHand,v_numWareHouseItemID FROM new_table;
   RETURN NULL;
   END; $$;
CREATE TRIGGER TRG_WarehouseItems
AFTER UPDATE
ON WareHouseItems
REFERENCING NEW TABLE AS new_table
FOR STATEMENT
   EXECUTE PROCEDURE TRG_WarehouseItems_TrFunc();
--if @numBackOrder>0 and @numOnHand>0
--begin
-- if @numBackOrder>=@numOnHand
--   update WareHouseItems set numAllocation=numAllocation+@numOnHand,numOnHand=0,numBackOrder=@numBackOrder-@numOnHand
--   where numWareHouseItemID=@numWareHouseItemID
-- else if @numBackOrder<@numOnHand
--	 update WareHouseItems set numAllocation=numAllocation+@numBackOrder,numOnHand=@numOnHand-@numBackOrder,numBackOrder=0
--   where numWareHouseItemID=@numWareHouseItemID
--end


