-- Stored procedure definition script USP_UpdateChartAcntReconcileDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateChartAcntReconcileDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numChartAcntId NUMERIC(9,0) DEFAULT 0,
v_dtEndingBal TIMESTAMP DEFAULT NULL,
v_monEndingBal DECIMAL(20,5) DEFAULT NULL,
v_numServiceChargeChartAcntId NUMERIC(9,0) DEFAULT 0,
v_dtServiceChargeDate TIMESTAMP DEFAULT NULL,
v_monServiceChargeAmt DECIMAL(20,5) DEFAULT NULL,
v_numInterestEarnedChartAcntId NUMERIC(9,0) DEFAULT 0,
v_dtInterestEarnedDate TIMESTAMP DEFAULT NULL,
v_monInterestEarnedAmt DECIMAL(20,5) DEFAULT NULL,
v_numFinanceChargeChartAcntId NUMERIC(9,0) DEFAULT 0,
v_dtFinanceChargeDate TIMESTAMP DEFAULT NULL,
v_monFinanceChargeAmt DECIMAL(20,5) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;
--Begin
--if @dtServiceChargeDate ='Jan  1 1753 12:00AM' set  @dtServiceChargeDate=null                                                                                                 
--if @dtInterestEarnedDate ='Jan  1 1753 12:00AM' set  @dtInterestEarnedDate=null     
--if @dtFinanceChargeDate ='Jan  1 1753 12:00AM' set  @dtFinanceChargeDate=null   
--if @numServiceChargeChartAcntId=0 set @numServiceChargeChartAcntId=null
--if @numInterestEarnedChartAcntId=0 set @numInterestEarnedChartAcntId=null
--if @numFinanceChargeChartAcntId=0 set @numFinanceChargeChartAcntId=null  
--Update Chart_of_Accounts  Set monEndingBal=@monEndingBal,
--							dtEndStatementDate=@dtEndingBal,
--							numServiceAcntId=@numServiceChargeChartAcntId,
--							dtServiceCharge=@dtServiceChargeDate,
--							monserviceCharges=@monServiceChargeAmt,
--						    numInterestEarnedAcntId=@numInterestEarnedChartAcntId,
--							dtInternestEarned=@dtInterestEarnedDate,
--							monInterestEarned=@monInterestEarnedAmt,
--							numFinancialAntId=@numFinanceChargeChartAcntId,
--							dtFinancialCharges=@dtFinanceChargeDate,
--							monFinancialCharges=@monFinanceChargeAmt 
--Where numAccountId = @numChartAcntId And numDomainID=@numDomainID
--								 
--End


