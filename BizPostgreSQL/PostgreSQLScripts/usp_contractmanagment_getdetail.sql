-- Stored procedure definition script USP_ContractManagment_GetDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ContractManagment_GetDetail(v_numDomainID NUMERIC(18,0),  
 v_numContractID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   
  
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      RecordID NUMERIC(18,0),
      RecordType VARCHAR(10),
      RecordName VARCHAR(500),
      Amount NUMERIC(18,2),
      HoursUsed NUMERIC(18,2)
   );

  --GET CASES WHERE CONTRACT IS ASSIGNED
   INSERT INTO tt_TEMP  SELECT numCaseId,CAST('Case' AS VARCHAR(10)),vcCaseNumber,0,0 FROM Cases WHERE coalesce(numContractID,0) = v_numContractID AND numDomainID = v_numDomainID;
  
  --GET PROJECTS WHERE CONTRACT IS ASSIGNED 

   INSERT INTO
   tt_TEMP
   SELECT
   ProjectsMaster.numProId,
		CAST('Project' AS VARCHAR(10)),
		ProjectsMaster.vcProjectName,
		coalesce(SUM(TE.monAmount),0),
		coalesce(SUM(TE.HoursUsed),0)
   FROM
   ProjectsMaster
   LEFT JOIN LATERAL(SELECT
      monAmount,
				coalesce(CAST(CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)) AS NUMERIC(18,2)),
      0) AS HoursUsed
      FROM
      timeandexpense
      WHERE
      coalesce(numProId,0) = ProjectsMaster.numProId) AS TE on TRUE
   WHERE
   ProjectsMaster.numcontractId = v_numContractID  AND numDomainID = v_numDomainID
   GROUP BY
   ProjectsMaster.numProId,ProjectsMaster.vcProjectName;

	
open SWV_RefCur for SELECT * FROM tt_TEMP;
   RETURN;
END; $$;  


/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/













