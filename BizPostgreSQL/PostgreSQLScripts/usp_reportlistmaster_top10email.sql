-- Stored procedure definition script USP_ReportListMaster_Top10Email for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10Email(v_numDomainID NUMERIC(18,0)
	,v_numUserCntId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numEmailHstrID NUMERIC(18,0),
      vcCompanyName VARCHAR(300)
   );
   INSERT INTO tt_TEMP(numEmailHstrID
		,vcCompanyName)
   SELECT 
   EH.numEmailHstrID
		,CI.vcCompanyName
   From
   EmailHistory EH
   INNER JOIN
   EmailMaster EM
   ON
   EM.numEmailId = EH.numEMailId
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   ACI.numContactId = EM.numContactID
   INNER JOIN
   DivisionMaster DM
   ON
   ACI.numDivisionId = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   EH.numDomainID = v_numDomainID
   AND EH.numUserCntId = v_numUserCntId
   ORDER BY
   bintCreatedOn DESC LIMIT 10;

	
open SWV_RefCur for SELECT
   T1.*
		,CASE
   WHEN LENGTH(Cast(EH.vcBodyText AS VARCHAR(1000))) > 150
   THEN SUBSTR(EH.vcBodyText,0,150) || '...'
   ELSE EH.vcBodyText END
   AS vcBodyText
		,EH.vcSubject
		,REVERSE(SUBSTR(REVERSE(EH.vcFrom),0,POSITION('$^$' IN REVERSE(EH.vcFrom)))) AS vcFrom
   FROM
   EmailHistory EH
   INNER JOIN
   tt_TEMP T1
   ON
   EH.numEmailHstrID = T1.numEmailHstrID
   WHERE
   EH.numDomainID = v_numDomainID
   AND EH.numUserCntId = v_numUserCntId;
END; $$;












