-- Stored procedure definition script USP_ManagerBizDocStatusApprove for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManagerBizDocStatusApprove(v_numBizDocStatusAppID NUMERIC(18,0),
v_numDomainID INTEGER,
v_numBizDocTypeID NUMERIC(18,0),
v_numBizDocStatusID NUMERIC(18,0),
v_numEmployeeID NUMERIC(18,0),
v_numActionTypeID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO BizDocStatusApprove  SELECT v_numDomainID,v_numBizDocTypeID,v_numBizDocStatusID,v_numEmployeeID,v_numActionTypeID;

open SWV_RefCur for select cast(CURRVAL('BizDocStatusApprove_seq') as VARCHAR(255));
END; $$;
        












