-- Stored procedure definition script USP_ScheduledReportsGroup_Save for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_Save(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_vcName VARCHAR(200)
	,v_tintFrequency SMALLINT
	,v_dtStartDate TIMESTAMP
	,v_numEmailTemplate NUMERIC(18,0)
	,v_vcSelectedTokens TEXT
	,v_vcRecipientsEmail TEXT
	,v_vcReceipientsContactID TEXT
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtStartDateExisting  TIMESTAMP;
BEGIN
   IF coalesce(v_numSRGID,0) > 0 then
      select   dtStartDate INTO v_dtStartDateExisting FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      UPDATE
      ScheduledReportsGroup
      SET
      vcName = v_vcName,tintFrequency = v_tintFrequency,dtStartDate = v_dtStartDate,
      numEmailTemplate = v_numEmailTemplate,vcSelectedTokens = v_vcSelectedTokens,
      vcRecipientsEmail = v_vcRecipientsEmail,vcReceipientsContactID = v_vcReceipientsContactID,
      dtNextDate =(CASE WHEN v_dtStartDateExisting <> v_dtStartDate THEN NULL ELSE dtNextDate END),intNotOfTimeTried =(CASE WHEN v_dtStartDateExisting <> v_dtStartDate THEN 0 ELSE intNotOfTimeTried END)
      WHERE
      numDomainID = v_numDomainID
      AND ID = v_numSRGID;
   ELSE
      IF(SELECT COUNT(*) FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID) >= 19 then
		
         RAISE EXCEPTION 'MAX_20_REPORT_GROUPS';
      ELSE
         INSERT INTO ScheduledReportsGroup(numDomainID
				,numCreatedBy
				,vcName
				,tintFrequency
				,dtStartDate
				,numEmailTemplate
				,vcSelectedTokens
				,vcRecipientsEmail
				,vcReceipientsContactID
				,intTimeZoneOffset)
			VALUES(v_numDomainID
				,v_numUserCntID
				,v_vcName
				,v_tintFrequency
				,v_dtStartDate
				,v_numEmailTemplate
				,v_vcSelectedTokens
				,v_vcRecipientsEmail
				,v_vcReceipientsContactID
				,v_ClientTimeZoneOffset);
			
         v_numSRGID := CURRVAL('ScheduledReportsGroup_seq');
      end if;
   end if;

   open SWV_RefCur for SELECT v_numSRGID;
END; $$;












