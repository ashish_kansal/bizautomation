-- Stored procedure definition script usp_ManageBankMaster for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ManageBankMaster(v_numBankMasterID NUMERIC(18,0),
	v_tintBankType SMALLINT,
	v_vcFIId VARCHAR(50),
	v_vcFIName VARCHAR(50),
	v_vcFIOrganization VARCHAR(50),
	v_vcFIOFXServerUrl VARCHAR(350),
	v_vcBankID VARCHAR(50),
	v_vcOFXAccessKey VARCHAR(50),
	v_dtCreatedDate TIMESTAMP,
	v_dtModifiedDate TIMESTAMP,
	v_vcBankPhone VARCHAR(50),
	v_vcBankWebsite VARCHAR(350),
	v_vcUserIdLabel VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT numBankMasterID FROM BankMaster WHERE numBankMasterID = v_numBankMasterID) then

      UPDATE BankMaster SET
      tintBankType = v_tintBankType,vcFIId = v_vcFIId,vcFIName = v_vcFIName,
      vcFIOrganization = v_vcFIOrganization,vcFIOFXServerUrl = v_vcFIOFXServerUrl,
      vcBankID = v_vcBankID,vcOFXAccessKey = v_vcOFXAccessKey,dtModifiedDate = LOCALTIMESTAMP,
      vcBankPhone = v_vcBankPhone,vcBankWebsite = v_vcBankWebsite,
      vcUserIdLabel = v_vcUserIdLabel
      WHERE
      numBankMasterID = v_numBankMasterID;
   ELSE
      INSERT INTO BankMaster(tintBankType,
		vcFIId,
		vcFIName,
		vcFIOrganization,
		vcFIOFXServerUrl,
		vcBankID,
		vcOFXAccessKey,
		dtCreatedDate,
		vcBankPhone,
		vcBankWebsite,
		vcUserIdLabel) VALUES(v_tintBankType,
		v_vcFIId,
		v_vcFIName,
		v_vcFIOrganization,
		v_vcFIOFXServerUrl,
		v_vcBankID,
		v_vcOFXAccessKey,
		LOCALTIMESTAMP ,
		v_vcBankPhone,
		v_vcBankWebsite,
		v_vcUserIdLabel) RETURNING numBankMasterID INTO v_numBankMasterID;
   end if;

   open SWV_RefCur for SELECT v_numBankMasterID;
END; $$;












