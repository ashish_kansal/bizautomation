-- Stored procedure definition script usp_UpdateTimeContract for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateTimeContract(v_numDomainID NUMERIC(9,0),
v_numDivisonId NUMERIC(9,0) ,    
v_numSecounds NUMERIC(9,0) ,  
v_intType INTEGER DEFAULT 0 ,
v_numTaskID NUMERIC(9,0) DEFAULT NULL,
INOUT v_outPut INTEGER  DEFAULT 0 ,
INOUT v_balanceContractTime BIGINT DEFAULT 0  ,
v_isProjectActivityEmail INTEGER DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1) then
	
		
		--@intType = 1
      IF(v_intType = 1) then
		
         v_outPut := 3;
         IF(v_isProjectActivityEmail = 1) then
				
            UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract = false WHERE numTaskId = v_numTaskID;
         end if;
         IF(v_isProjectActivityEmail = 2) then
				
            UPDATE Communication SET bitTimeAddedToContract = false WHERE numCommId = v_numTaskID;
         end if;
         IF(v_isProjectActivityEmail = 3) then
				
            UPDATE Activity SET bitTimeAddedToContract = false WHERE ActivityID = v_numTaskID;
         end if;
         IF v_isProjectActivityEmail <> 1 then --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				
            UPDATE Contracts SET timeUsed = timeUsed -v_numSecounds,dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID;
            DELETE FROM ContractsLog WHERE numDivisionID = v_numDivisonId  AND numReferenceId = v_numTaskID
            AND numContractId =(SELECT  numContractId FRoM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID LIMIT 1);
         end if;
      end if;
		--@intType = 2
      IF(v_intType = 2) then
		
         IF v_isProjectActivityEmail <> 1 AND ((SELECT coalesce(timeLeft,0) FROM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1) < v_numSecounds) then
			
            v_outPut := 1;
         ELSEIF v_isProjectActivityEmail = 1 AND ((SELECT coalesce(timeLeft,0) FROM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1) <= 0)
         then
			
            v_outPut := 1;
         ELSE
            v_outPut := 2; --Time is available
            IF(v_isProjectActivityEmail = 1) then
				
               UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract = true WHERE numTaskId = v_numTaskID;
            end if;
            IF(v_isProjectActivityEmail = 2) then
				
               UPDATE Communication SET bitTimeAddedToContract = true WHERE numCommId = v_numTaskID;
            end if;
            IF(v_isProjectActivityEmail = 3) then
				
               UPDATE Activity SET bitTimeAddedToContract = true WHERE ActivityID = v_numTaskID;
            end if;
            IF v_isProjectActivityEmail <> 1 then --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				
               UPDATE Contracts SET timeUsed = timeUsed+v_numSecounds,dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID;
               SELECT coalesce((coalesce(timeLeft,0) -coalesce(timeUsed,0)),0) INTO v_balanceContractTime FROM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1;
               INSERT INTO ContractsLog(intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType)
               SELECT 
               1,v_numDivisonId,v_numTaskID,TIMEZONE('UTC',now()),0,v_isProjectActivityEmail,v_numSecounds,v_balanceContractTime,numContractId,v_isProjectActivityEmail
               FROM Contracts WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1 ORDER BY numContractId DESC LIMIT 1;
            end if;
         end if;
      end if;
   ELSE
      v_outPut := 0;
   end if;

   SELECT((coalesce(C.numHours,0)*60*60)+(coalesce(C.numMinutes,0)*60)) -coalesce((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId = C.numContractId),0) INTO v_balanceContractTime FROM Contracts C WHERE numDivisonId = v_numDivisonId AND numDomainId = v_numDomainID AND intType = 1   ORDER BY numContractId DESC  LIMIT 1;
   RETURN;
END; $$;


