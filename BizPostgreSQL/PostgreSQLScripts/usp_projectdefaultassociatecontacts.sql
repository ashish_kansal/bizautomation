-- Stored procedure definition script usp_ProjectDefaultAssociateContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ProjectDefaultAssociateContacts(v_numProId NUMERIC(9,0) DEFAULT 0 ,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecordOwnerRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numRecordAssigneeRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numRecordOrganizationContactsRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numROwnerContactID  NUMERIC(18,0) DEFAULT 0;
   v_numRAssigneContactID  NUMERIC(18,0) DEFAULT 0;
   v_numROrgContactID  NUMERIC(18,0) DEFAULT 0;
--Record Owner
BEGIN
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Record Owner') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Record Owner');
	
      v_numRecordOwnerRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordOwnerRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Record Owner'    LIMIT 1;
   end if;
--Assignee
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Assignee') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Assignee');
	
      v_numRecordAssigneeRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordAssigneeRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Assignee'    LIMIT 1;
   end if;
--Organization Contact
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Organization Contact') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Organization Contact');
	
      v_numRecordOrganizationContactsRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordOrganizationContactsRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Organization Contact'    LIMIT 1;
   end if;

   IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId = v_numProId AND numRole = v_numRecordOwnerRoleId) = 0) then
      SELECT  numRecOwner INTO v_numROwnerContactID FROM ProjectsMaster WHERE numProId = v_numProId     LIMIT 1;
      if(coalesce(v_numROwnerContactID,0) > 0) then
	
         insert into ProjectsContacts(numProId, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numProId,v_numROwnerContactID,v_numRecordOwnerRoleId,false,false);
      end if;
   end if;
   IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId = v_numProId AND numRole = v_numRecordAssigneeRoleId) = 0) then
      SELECT  numAssignedby INTO v_numRAssigneContactID FROM ProjectsMaster WHERE numProId = v_numProId     LIMIT 1;
      if(coalesce(v_numRAssigneContactID,0) > 0) then
	
         insert into ProjectsContacts(numProId, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numProId,v_numRAssigneContactID,v_numRecordAssigneeRoleId,false,false);
      end if;
   end if;
   IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId = v_numProId AND numRole = v_numRecordOrganizationContactsRoleId) = 0) then
      SELECT  numCustPrjMgr INTO v_numROrgContactID FROM ProjectsMaster WHERE numProId = v_numProId     LIMIT 1;
      if(coalesce(v_numROrgContactID,0) > 0) then
	
         insert into ProjectsContacts(numProId, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numProId,v_numROrgContactID,v_numRecordOrganizationContactsRoleId,false,false);
      end if;
   end if;
   RETURN;
END; $$;



