-- Stored procedure definition script USP_DivisionMasterShippingConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DivisionMasterShippingConfiguration_Save(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_IsAdditionalHandling BOOLEAN
    ,v_IsCOD BOOLEAN
    ,v_IsHomeDelivery BOOLEAN
    ,v_IsInsideDelevery BOOLEAN
    ,v_IsInsidePickup BOOLEAN
    ,v_IsSaturdayDelivery BOOLEAN
    ,v_IsSaturdayPickup BOOLEAN
    ,v_IsLargePackage BOOLEAN
    ,v_vcDeliveryConfirmation VARCHAR(1000)
    ,v_vcDescription TEXT
    ,v_vcSignatureType VARCHAR(300)
    ,v_vcCODType VARCHAR(50))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM DivisionMasterShippingConfiguration WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID) then
	
      UPDATE
      DivisionMasterShippingConfiguration
      SET
      numDomainID = v_numDomainID,numDivisionID = v_numDivisionID,IsAdditionalHandling = v_IsAdditionalHandling,
      IsCOD = v_IsCOD,IsHomeDelivery = v_IsHomeDelivery,
      IsInsideDelevery = v_IsInsideDelevery,IsInsidePickup = v_IsInsidePickup,
      IsSaturdayDelivery = v_IsSaturdayDelivery,IsSaturdayPickup = v_IsSaturdayPickup,
      IsLargePackage = v_IsLargePackage,vcDeliveryConfirmation = v_vcDeliveryConfirmation,
      vcDescription = v_vcDescription,
      vcSignatureType = v_vcSignatureType,vcCODType = v_vcCODType
      WHERE
      numDomainID = v_numDomainID
      AND numDivisionID = v_numDivisionID;
   ELSE
      INSERT INTO DivisionMasterShippingConfiguration(numDomainID
           ,numDivisionID
           ,IsAdditionalHandling
           ,IsCOD
           ,IsHomeDelivery
           ,IsInsideDelevery
           ,IsInsidePickup
           ,IsSaturdayDelivery
           ,IsSaturdayPickup
           ,IsLargePackage
           ,vcDeliveryConfirmation
           ,vcDescription
           ,vcSignatureType
           ,vcCODType)
		VALUES(v_numDomainID
           ,v_numDivisionID
           ,v_IsAdditionalHandling
           ,v_IsCOD
           ,v_IsHomeDelivery
           ,v_IsInsideDelevery
           ,v_IsInsidePickup
           ,v_IsSaturdayDelivery
           ,v_IsSaturdayPickup
           ,v_IsLargePackage
           ,v_vcDeliveryConfirmation
           ,v_vcDescription
           ,v_vcSignatureType
           ,v_vcCODType);
   end if;
   RETURN;
END; $$;


