-- Function definition script GetCreditTerms for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCreditTerms(v_numOppId NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQl  VARCHAR(100);
   v_bitBillingTerms  BOOLEAN;
   v_intDays  VARCHAR(10);
   v_InterestType BOOLEAN;
   v_Interest  VARCHAR(10);
BEGIN
   select   fn_GetListItemName(OM.intBillingDays::NUMERIC), OM.bitBillingTerms, OM.bitInterestType, OM.fltInterest INTO v_intDays,v_bitBillingTerms,v_InterestType,v_Interest from OpportunityMaster OM where OM.numOppId = v_numOppId;

   if v_bitBillingTerms then

      v_strSQl := 'Net ' || coalesce(v_intDays,cast(0 as TEXT));
      if NULLIF(v_InterestType,false) = false then 
         v_strSQl := coalesce(v_strSQl,'') || ', -';
      else 
         v_strSQl := coalesce(v_strSQl,'') || ', +';
      end if;
      v_strSQl := coalesce(v_strSQl,'') || coalesce(v_Interest,cast(0 as TEXT));
   end if;


   return coalesce(v_strSQl,'-');
END; $$;

