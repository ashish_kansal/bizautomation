-- Stored procedure definition script USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID(v_numDivisionID NUMERIC(18,0)
	,v_numShipViaID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM DivisionMasterShippingAccount WHERE numDivisionID = v_numDivisionID AND numShipViaID = v_numShipViaID;
END; $$;












