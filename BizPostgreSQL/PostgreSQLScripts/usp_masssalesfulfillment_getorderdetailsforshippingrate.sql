-- Stored procedure definition script USP_MassSalesFulfillment_GetOrderDetailsForShippingRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetOrderDetailsForShippingRate(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numShipVia NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
	-- GET FROM ADDRESS
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numContainer  NUMERIC(18,0);
   v_numNoItemIntoContainer  DOUBLE PRECISION;
   v_fltItemWeight  DOUBLE PRECISION;
   v_fltItemHeight  DOUBLE PRECISION;
   v_fltItemWidth  DOUBLE PRECISION;
   v_fltItemLength  DOUBLE PRECISION;
   v_fltContainerWeight  DOUBLE PRECISION;
   v_fltContainerHeight  DOUBLE PRECISION;
   v_ltContainerWidth  DOUBLE PRECISION;
   v_fltContainerLength  DOUBLE PRECISION;
   v_numTotalQty  DOUBLE PRECISION;
BEGIN
   open SWV_RefCur for
   SELECT
   vcStreet
		,vcCity
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = v_numShipVia AND vcStateName = fn_GetState(vcState)),'') AS vcState
		,vcZipCode
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = v_numShipVia AND vcCountryName = fn_GetListItemName(vcCountry)),
   '') AS vcCountry
   FROM
   fn_GetShippingReportAddress(1,v_numDomainID,v_numOppID);
		  

	-- GET TO ADDRESS
   open SWV_RefCur2 for
   SELECT
   vcStreet
		,vcCity
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = v_numShipVia AND vcStateName = fn_GetState(vcState)),'') AS vcState
		,vcZipCode
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = v_numShipVia AND vcCountryName = fn_GetListItemName(vcCountry)),
   '') AS vcCountry
   FROM
   fn_GetShippingReportAddress(2,v_numDomainID,v_numOppID);

	-- GET CONTAINERS/PACKAGES
   BEGIN
      CREATE TEMP SEQUENCE tt_TempContiner_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPCONTINER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCONTINER
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      fltContainerWeight DOUBLE PRECISION,
      fltContainerHeight DOUBLE PRECISION,
      fltContainerWidth DOUBLE PRECISION,
      fltContainerLength DOUBLE PRECISION,
      numQty DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numContainer NUMERIC(18,0),
      numNoItemIntoContainer DOUBLE PRECISION,
      fltItemWeight DOUBLE PRECISION,
      fltItemHeight DOUBLE PRECISION,
      fltItemWidth DOUBLE PRECISION,
      fltItemLength DOUBLE PRECISION,
      fltContainerWeight DOUBLE PRECISION,
      fltContainerHeight DOUBLE PRECISION,
      ltContainerWidth DOUBLE PRECISION,
      fltContainerLength DOUBLE PRECISION,
      numTotalQty DOUBLE PRECISION
   );

   INSERT INTO
   tt_TEMPITEMS(numContainer, numNoItemIntoContainer, fltItemWeight, fltItemHeight, fltItemWidth, fltItemLength, fltContainerWeight, fltContainerHeight, ltContainerWidth, fltContainerLength, numTotalQty)
   SELECT
   T1.numContainer
		,T1.numNoItemIntoContainer
		,T1.fltItemWeight
		,T1.fltItemHeight
		,T1.fltItemWidth
		,T1.fltItemLength
		,coalesce(IContainer.fltWeight,0) AS fltContainerWeight
		,coalesce(IContainer.fltHeight,0) AS fltContainerHeight
		,coalesce(IContainer.fltWidth,0) AS fltContainerWidth
		,coalesce(IContainer.fltLength,0) AS fltContainerLength
		,T1.numTotalQty
   FROM(SELECT
      I.numContainer
			,(CASE WHEN coalesce(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE SUM(OI.numUnitHour) END) AS numNoItemIntoContainer
			,coalesce(I.fltWeight,0) AS fltItemWeight
			,coalesce(I.fltHeight,0) AS fltItemHeight
			,coalesce(I.fltWidth,0) AS fltItemWidth
			,coalesce(I.fltLength,0) AS fltItemLength
			,SUM(OI.numUnitHour) AS numTotalQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item AS I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      numOppId = v_numOppID
      AND I.charItemType = 'P'
      AND coalesce(I.bitContainer,false) = false
      GROUP BY
      I.numContainer,I.numNoItemIntoContainer,I.fltWeight,I.fltHeight,I.fltWidth,
      I.fltLength) AS T1
   LEFT JOIN
   Item IContainer
   ON
   T1.numContainer = IContainer.numItemCode;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPITEMS;

   WHILE v_i <= v_iCount LOOP
      select   numContainer, numNoItemIntoContainer, fltItemWeight, fltItemHeight, fltItemWidth, fltItemLength, fltContainerWeight, fltContainerHeight, ltContainerWidth, fltContainerLength, numTotalQty INTO v_numContainer,v_numNoItemIntoContainer,v_fltItemWeight,v_fltItemHeight,
      v_fltItemWidth,v_fltItemLength,v_fltContainerWeight,v_fltContainerHeight,
      v_ltContainerWidth,v_fltContainerLength,v_numTotalQty FROM
      tt_TEMPITEMS WHERE
      ID = v_i;
      IF v_numTotalQty <= v_numNoItemIntoContainer then
		
         INSERT INTO tt_TEMPCONTINER(fltContainerWeight
				,fltContainerHeight
				,fltContainerWidth
				,fltContainerLength
				,numQty)
			VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numTotalQty, 0)*coalesce(v_fltItemWeight, 0))
				,v_fltContainerHeight
				,v_ltContainerWidth
				,v_fltContainerLength
				,v_numTotalQty);
      ELSE
         WHILE v_numTotalQty > v_numNoItemIntoContainer LOOP
            INSERT INTO tt_TEMPCONTINER(fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty)
				VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numNoItemIntoContainer, 0)*coalesce(v_fltItemWeight, 0))
					,v_fltContainerHeight
					,v_ltContainerWidth
					,v_fltContainerLength
					,v_numNoItemIntoContainer);
				
            v_numTotalQty := v_numTotalQty -v_numNoItemIntoContainer;
         END LOOP;
         IF coalesce(v_numTotalQty,0) > 0 then
			
            INSERT INTO tt_TEMPCONTINER(fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty)
				VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numTotalQty, 0)*coalesce(v_fltItemWeight, 0))
					,v_fltContainerHeight
					,v_ltContainerWidth
					,v_fltContainerLength
					,v_numTotalQty);
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur3 for
   SELECT * FROm tt_TEMPCONTINER;
   RETURN;
END; $$;


