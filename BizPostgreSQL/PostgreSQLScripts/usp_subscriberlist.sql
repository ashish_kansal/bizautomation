-- Stored procedure definition script USP_SubScriberList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SubScriberList(v_numUserCntID NUMERIC,                                                                                           
v_tintSortOrder SMALLINT,                                                                            
v_SortChar CHAR(1) DEFAULT '0',                                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                        
v_SearchWord VARCHAR(100) DEFAULT '',                                                                                            
v_CurrentPage INTEGER DEFAULT NULL,                                                      
v_PageSize INTEGER DEFAULT NULL,                                                      
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                      
v_columnName VARCHAR(50) DEFAULT NULL,                                                      
v_columnSortOrder VARCHAR(10) DEFAULT NULL,        
v_ClientTimeZoneOffset INTEGER DEFAULT NULL ,
INOUT v_ActiveUsers DOUBLE PRECISION  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                      
   v_lastRec  INTEGER;                                                      
   v_strSql  VARCHAR(8000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                      
 
 
 
                                                     
                     
   v_strSql := 'With tblSubscriber AS (SELECT ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,numSubscriberID,  CASE WHEN Subscribers.numTargetDomainID IN (1,72) THEN 0 ELSE Subscribers.numTargetDomainID END AS numDomainID                 
 FROM Subscribers ';                  
   v_strSql := coalesce(v_strSql,'') || ' Join Divisionmaster D on D.numDivisionID= Subscribers.numDivisionID Join Companyinfo C                  
on C.numCompanyID=D.numCompanyID ';                  
                  
   v_strSql := coalesce(v_strSql,'') || ' Left Join AdditionalContactsInformation A on A.numContactID= Subscribers.numAdminContactID ';                  
                  
                  
   v_strSql := coalesce(v_strSql,'') || ' where bitDeleted=false ';                  
                  
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' and Subscribers.numDivisionID in (select D.numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName ilike ''' || coalesce(v_SortChar,'') || '%'')';
   end if;                   
                  
   if v_SearchWord <> '' then

      if v_tintSortOrder = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' and D.numDivisionID in (select numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName ilike ''%' || coalesce(v_SearchWord,'') || '%'')';
      ELSEIF v_tintSortOrder = 2
      then 
         v_strSql := coalesce(v_strSql,'') || ' and D.numDivisionID in (select numDivisionID from AdditionalContactsInformation A                  
 where vcEmail ilike ''%' || coalesce(v_SearchWord,'') || '%'')';
      end if;
   end if;
   if v_tintSortOrder = 3 then 
      v_strSql := coalesce(v_strSql,'') || ' and bitTrial =true ';
   ELSEIF v_tintSortOrder = 4
   then 
      v_strSql := coalesce(v_strSql,'') || ' and bitActive =1 ';
   ELSEIF v_tintSortOrder = 5
   then 
      v_strSql := coalesce(v_strSql,'') || ' and bitActive =0 ';
   ELSEIF v_tintSortOrder = 6
   then 
      v_strSql := coalesce(v_strSql,'') || ' and bitActive =0 and dtSuspendedDate <=''' || CAST(TIMEZONE('UTC',now())+INTERVAL '30 day' AS VARCHAR(30)) || '''';
   ELSEIF v_tintSortOrder = 7
   then 
      v_strSql := coalesce(v_strSql,'') || ' and bitActive =0 and dtSuspendedDate >=''' || CAST(TIMEZONE('UTC',now())+INTERVAL '30 day' AS VARCHAR(30)) || '''';
   end if;                  
                   
                  
   v_strSql := coalesce(v_strSql,'') || ')                     
 select RowNumber,T.numSubscriberID,S.numDivisionID,numContactID,vcCompanyName,                  
 coalesce(vcFirstName,'''')|| '' ''||coalesce(vcLastname,'''') AdminContact,coalesce(intNoofUsersSubscribed,0) + coalesce(intNoofPartialSubscribed,0)/2 + coalesce(intNoofMinimalSubscribed,0)/10 as intNoofUsersSubscribed,                  
 intNoOfPartners,FormatedDateTimeFromDate(dtSubStartDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') as dtSubStartDate,    
FormatedDateTimeFromDate(dtSubEndDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') as dtSubEndDate,case when bitTrial=false then ''No'' when bitTrial      
=true then ''Yes'' else ''No'' End as Trial,            
 case when bitActive=0 then ''Suspended'' when bitActive=1 then ''Active'' else ''Trial'' End as  Status,FormatedDateTimeFromDate(dtSuspendedDate + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||
   ') as dtSuspendedDate,              
 vcSuspendedReason,vcEmail, coalesce((SELECT SUM(coalesce(SpaceOccupied,0)) FROM View_InboxCount where numDomainID=S.numTargetDomainID),0) as TotalSize, T.numDomainID 
 from tblSubscriber T                  
 join Subscribers S                  
 on S.numSubscriberID=T.numSubscriberID           
 join DivisionMaster D                  
 on D.numDivisionID=S.numDivisionID                  
 join Companyinfo C                  
 on C.numCompanyID=D.numCompanyID                  
 Left join  AdditionalContactsInformation A                  
 on A.numContactID=S.numAdminContactID                  
 Where RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '                   
 union select 0,count(*),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null from tblSubscriber order by RowNumber';                  
   RAISE NOTICE '%',v_strSql;                  
   OPEN SWV_RefCur FOR EXECUTE v_strSql;

   select   coalesce(SUM(coalesce(intNoofUsersSubscribed,0)+coalesce(intNoofPartialSubscribed,0)/2+coalesce(intNoofMinimalSubscribed,0)/10),0) INTO v_ActiveUsers from Subscribers S
   JOIN DivisionMaster D ON D.numDivisionID = S.numDivisionid
   JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
   JOIN AdditionalContactsInformation A ON A.numContactId = S.numAdminContactID WHERE S.bitActive = 1 AND S.bitDeleted = false;
   RETURN;
END; $$;
 
 


