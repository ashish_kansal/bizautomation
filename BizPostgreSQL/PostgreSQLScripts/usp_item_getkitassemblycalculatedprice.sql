-- Stored procedure definition script USP_Item_GetKitAssemblyCalculatedPrice for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetKitAssemblyCalculatedPrice(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numQty NUMERIC,
	v_tintKitAssemblyPriceBasedOn SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   SELECT
		bitSuccess
		,monPrice
   FROM
   fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,0,v_tintKitAssemblyPriceBasedOn::SMALLINT,0,0,'',0,1);
END; $$;












