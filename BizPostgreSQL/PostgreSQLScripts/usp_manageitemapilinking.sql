-- Stored procedure definition script USP_ManageItemAPILinking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemAPILinking(v_numDomainID  NUMERIC(9,0),
                v_intWebApiId  INTEGER  DEFAULT NULL,
                v_numItemCode  NUMERIC(9,0)  DEFAULT 0,
                v_vcApiItemId  VARCHAR(50) DEFAULT NULL,
                v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
                v_vcSKU  VARCHAR(100) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM   ItemAPI
   WHERE  
--                   [vcAPIItemID] = @vcApiItemId
   numItemID = v_numItemCode
   AND WebApiId = v_intWebApiId
   AND numDomainId = v_numDomainID) then
      
      INSERT INTO ItemAPI(WebApiId,
                    numDomainId,
                    numItemID,
                    vcAPIItemID,
                    numCreatedby,
                    dtCreated,
                    numModifiedby,
                    dtModified,vcSKU)
        VALUES(v_intWebApiId,
                    v_numDomainID,
                    v_numItemCode,
                    v_vcApiItemId,
                    v_numUserCntID,
                    TIMEZONE('UTC',now()),
                    v_numUserCntID,
                    TIMEZONE('UTC',now()),v_vcSKU);
   ELSE
      UPDATE ItemAPI
      SET    vcAPIItemID = v_vcApiItemId,dtModified = TIMEZONE('UTC',now())
      WHERE  numItemID = v_numItemCode
      AND WebApiId = v_intWebApiId
      AND numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


