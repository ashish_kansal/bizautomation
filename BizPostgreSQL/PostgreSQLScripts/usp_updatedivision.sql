-- Stored procedure definition script usp_UpdateDivision for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateDivision(v_numCmpID NUMERIC,
v_numDivID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldCmpId  NUMERIC;
   v_numCntDivID  NUMERIC;
BEGIN
   v_numCntDivID := 0;

   select   numCompanyID INTO v_numOldCmpId from DivisionMaster where numCompanyID = v_numDivID;
   Update DivisionMaster set numCompanyID = v_numCmpID where numCompanyID = v_numDivID;

   select   count(numDivisionID) INTO v_numCntDivID from DivisionMaster where numCompanyID = v_numOldCmpId;
   IF v_numCntDivID = 0 then

      Delete from CompanyInfo where numCompanyId = v_numOldCmpId;
   end if;
   RETURN;
END; $$;


