-- FUNCTION: public.usp_getcontactslogged(numeric, refcursor)

-- DROP FUNCTION public.usp_getcontactslogged(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcontactslogged(
	v_numdivisionid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:06:06 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for select cast(Dtl.numContactID as VARCHAR(255)),
vcFirstName || ' ' || vcLastname || ', ' || vcEmail as Name,
cast(bintLastLoggedIn as VARCHAR(255)),
cast(numNoofTimes as VARCHAR(255)) from ExtarnetAccounts Hdr
   join ExtranetAccountsDtl Dtl
   on Hdr.numExtranetID = Dtl.numExtranetID
   join AdditionalContactsInformation AddC
   on CAST(Dtl.numContactID As Numeric) = AddC.numContactId
   where Hdr.numDivisionID = v_numDivisionID;
END;
$BODY$;

ALTER FUNCTION public.usp_getcontactslogged(numeric, refcursor)
    OWNER TO postgres;
