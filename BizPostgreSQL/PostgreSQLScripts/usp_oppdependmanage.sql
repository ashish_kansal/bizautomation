-- Stored procedure definition script USP_OPPDependManage for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPDependManage(v_byteMode SMALLINT DEFAULT null,
v_strDependency TEXT DEFAULT '',
v_OppID NUMERIC(9,0) DEFAULT null,
v_OppStageID NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_byteMode = 0 then
      delete from OpportunityDependency where numOpportunityId = v_OppID and numOppstageid = v_OppStageID;
      SELECT * INTO v_hDoc FROM SWF_Xml_PrepareDocument(v_strDependency);
      insert into  OpportunityDependency(numOpportunityId,
	numOppstageid,
	numProcessStageId,
	vcType)
      select v_OppID,v_OppStageID,X.* from(SELECT *FROM SWF_OpenXml(v_hDoc,'/NewDataSet/Table','numProcessStageId |vcType') SWA_OpenXml(numProcessStageId NUMERIC(9,0),vcType VARCHAR(1))) X;
      PERFORM SWF_Xml_RemoveDocument(v_hDoc);
   end if;

   if v_byteMode = 1 then

      open SWV_RefCur for select cast(dep.numProcessStageId as VARCHAR(255)),cast(dtl.vcstagedetail as VARCHAR(255)),vcType,
	'Milestone - ' || CAST(dtl.numStagePercentage AS VARCHAR(4)) || '%' as numstagepercentage ,
	mst.vcpOppName as vcPOppName,
	case when dtl.bitstagecompleted = false then '<font color=red>Pending</font>'  when dtl.bitstagecompleted = true then '<font color=blue>Completed</font>' end as Status
      from OpportunityDependency dep
      join OpportunityStageDetails dtl
      on dtl.numOppStageId = dep.numProcessStageId
      join OpportunityMaster mst on
      dtl.numoppid = mst.numOppId
      where  vcType = 'O' and dep.numOpportunityId = v_OppID and dep.numOppstageid = v_OppStageID
      union
      select cast(dep.numProcessStageId as VARCHAR(255)),cast(dtl.vcstagedetail as VARCHAR(255)),vcType,
	'Milestone - ' || CAST(dtl.numstagepercentage AS VARCHAR(4)) || '%' as numstagepercentage ,
	cast(mst.vcProjectName as VARCHAR(100)) as vcPOppName,
	case when dtl.bitStageCompleted = false then '<font color=red>Pending</font>'  when dtl.bitStageCompleted = true then '<font color=blue>Completed</font>' end as Status
      from OpportunityDependency dep
      join ProjectsStageDetails dtl
      on dtl.numProStageId = dep.numProcessStageId
      join ProjectsMaster mst on
      dtl.numProId = mst.numProId
      where  vcType = 'P' and dep.numOpportunityId = v_OppID and dep.numOppstageid = v_OppStageID;
   end if;
END; $$;












