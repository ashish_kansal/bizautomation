DROP FUNCTION IF EXISTS GetAccountTypeCode;
CREATE OR REPLACE FUNCTION GetAccountTypeCode(v_numDomainID NUMERIC(9,0),v_numParentID NUMERIC(9,0),v_tintMode SMALLINT DEFAULT 0)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountTypeID  NUMERIC(9,0);
   v_vcMaxAccountTypeCode  VARCHAR(50);
   v_vcMaxCharOfAccountCode  VARCHAR(50);
   v_vcNewAccountCode  VARCHAR(50);
   v_vcParentAccountCode  VARCHAR(50);
BEGIN
   IF v_tintMode = 0 OR v_tintMode = 1 then --0: Get Account code for Account Type, 1:Get new Account code for Chart of Account on change of parent account type 
	
      select   numAccountTypeID, vcAccountCode INTO v_numAccountTypeID,v_vcParentAccountCode FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND numAccountTypeID = v_numParentID;
      IF(SELECT COUNT(*) FROM  AccountTypeDetail WHERE numDomainID = v_numDomainID AND  numParentID = v_numAccountTypeID) > 0 OR(SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numParntAcntTypeID = v_numParentID) > 0 then
		
         select   coalesce(v_vcParentAccountCode,'') || REPLACE(LPAD((coalesce((CASE WHEN LENGTH(COALESCE(vcAccountCode,'')) > 0 THEN CAST(SUBSTR(vcAccountCode,LENGTH(vcAccountCode) -1,LENGTH(vcAccountCode)) AS INTEGER) ELSE 0 END),0)+1)::VARCHAR,2),' ','0') INTO v_vcMaxAccountTypeCode FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND numParentID = v_numAccountTypeID   ORDER BY numAccountTypeID DESC LIMIT 1;
         
		 IF COALESCE((select vcAccountCode FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numParntAcntTypeID = v_numParentID AND coalesce(bitIsSubAccount,false) = false ORDER BY vcAccountCode DESC LIMIT 1),'') <> '' THEN
			select   coalesce(v_vcParentAccountCode,'') || REPLACE(LPAD((coalesce((CASE WHEN LENGTH(COALESCE(vcAccountCode,'')) > 0 THEN CAST(SUBSTR(vcAccountCode,LENGTH(vcAccountCode) -1,LENGTH(vcAccountCode)) AS INTEGER) ELSE 0 END),0)+1)::VARCHAR,2),' ','0') INTO v_vcMaxCharOfAccountCode FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numParntAcntTypeID = v_numParentID AND coalesce(bitIsSubAccount,false) = false ORDER BY vcAccountCode DESC LIMIT 1;
		 END IF;

		 RAISE NOTICE '%',v_vcMaxCharOfAccountCode;
		 RAISE NOTICE '%',v_vcMaxAccountTypeCode;
         v_vcNewAccountCode :=(CASE WHEN coalesce(v_vcMaxCharOfAccountCode,'0') > coalesce(v_vcMaxAccountTypeCode,'0') THEN v_vcMaxCharOfAccountCode ELSE  v_vcMaxAccountTypeCode END);
      ELSE
         v_vcNewAccountCode := coalesce(v_vcParentAccountCode,'') || '01';
      end if;
   end if; 
   IF v_tintMode = 2 then --select code from AccounttypeDetail table

      select   vcAccountCode INTO v_vcNewAccountCode FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND numAccountTypeID = v_numParentID;
   end if; 

   IF v_tintMode = 3 then --Get new Account code for Chart of Account on change of IsSubAccount flag from 0 to 1 or 1 to 0

      select   vcAccountCode INTO v_vcParentAccountCode FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numAccountId = v_numParentID;
				
			--RETURN  @vcParentAccountCode
      IF(SELECT COUNT(*) FROM  Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numParentAccId = v_numParentID) > 0 then
			
         select   coalesce(v_vcParentAccountCode,'') || REPLACE(LPAD((coalesce((CASE WHEN LENGTH(COALESCE(vcAccountCode,'')) > 0 THEN CAST(SUBSTR(vcAccountCode,LENGTH(vcAccountCode) -1,LENGTH(vcAccountCode)) AS INTEGER) ELSE 0 END),0)+1)::VARCHAR,2),' ','0') INTO v_vcNewAccountCode FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND numParentAccId = v_numParentID   ORDER BY vcAccountCode DESC LIMIT 1;
      ELSE
         v_vcNewAccountCode := v_vcParentAccountCode || '01';
      end if;
   end if; 	

   RETURN coalesce(v_vcNewAccountCode,'');
END; $$;

