-- Stored procedure definition script USP_TicklerActItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TicklerActItem(v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                
v_numDomainID NUMERIC(9,0) DEFAULT null,                                                
v_startDate TIMESTAMP DEFAULT NULL,                                                
v_endDate TIMESTAMP DEFAULT NULL,                                            
v_ClientTimeZoneOffset INTEGER DEFAULT NULL  --Added by Debasish to enable calculation of date according to client machine                                              
                          
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * from(SELECT Comm.numCommId AS Id,bitTask,
 dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS CloseDate,
 Comm.textDetails AS itemDesc,
 AddC.vcFirstName || ' ' || AddC.vcLastname  as Name,
 case when AddC.numPhone <> '' then AddC.numPhone || case when AddC.numPhoneExtension <> '' then ' - ' || AddC.numPhoneExtension else '' end  else '' end as Phone,
 SUBSTR(Comp.vcCompanyName,0,12) || '..' As vcCompanyName ,
 AddC.vcEmail As vcEmail ,                                                 
-- case when Comm.bitTask=1 then 
-- 'Communication' when Comm.bitTask=2 then 
-- 'Opportunity' when Comm.bitTask = 3 then 
-- 'Task' when Comm.bitTask=4 then  'Notes' 
--When Comm.bitTask = 5 then 
-- 'Follow-up Anytime' else
-- 'Unknown' end  
fn_GetListName(Comm.bitTask,0::BOOLEAN) AS Task,
 cast(listdetailsActivity.vcData as NUMERIC(18,0)) As Activity ,
 cast(listdetailsStatus.vcData as SMALLINT) As Status   ,  -- Priority column                                                                     
 Comm.numCreatedBy,
 Div.numTerID,
 case When LENGTH(fn_GetContactName(numAssign)) > 12
      Then SUBSTR(fn_GetContactName(numAssign),0,12) || '..'
      Else fn_GetContactName(numAssign)
      end
      ||
      ' / '
      ||
      case
      When LENGTH(fn_GetContactName(Comm.numAssignedBy)) > 12
      Then SUBSTR(fn_GetContactName(Comm.numAssignedBy),0,12) || '..'
      Else fn_GetContactName(Comm.numAssignedBy)
      end  As AssignedTo ,
Comm.CaseId,
cast((select  cast(vcCaseNumber as NUMERIC(18,0)) from Cases where Cases.numCaseId = Comm.CaseId LIMIT 1) as NUMERIC(18,0)) as vcCasenumber,
Comm.caseTimeid,
Comm.caseExpid
      FROM  Communication Comm
      JOIN AdditionalContactsInformation AddC
      ON Comm.numContactId = AddC.numContactId
      JOIN DivisionMaster Div
      ON AddC.numDivisionId = Div.numDivisionID
      JOIN CompanyInfo  Comp
      ON Div.numCompanyID = Comp.numCompanyId
      Left Join Listdetails listdetailsActivity
      On Comm.numActivity = listdetailsActivity.numListItemID
      Left Join Listdetails listdetailsStatus
      On Comm.numStatus = listdetailsStatus.numListItemID
      WHERE Comm.numDomainID = v_numDomainID
      AND (Comm.numAssign = v_numUserCntID or Comm.numCreatedBy = v_numUserCntID)
      AND (dtStartTime >= v_startDate and dtStartTime <= v_endDate)
      AND Comm.bitClosedFlag = false
      and Comm.bitTask <> 4) As X
   Order by X.CloseDate;
END; $$;












