-- Stored procedure definition script USP_OpportunityBizDocs_GetVendorInvoice for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetVendorInvoice(v_numDomainID NUMERIC(18,0)
	,v_numRecordID NUMERIC(18,0)
	,v_tintMode SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
   numOppBizDocsId
		,CONCAT(vcBizDocID,(CASE WHEN LENGTH(coalesce(vcVendorInvoice,'')) > 0 THEN CONCAT(' (',vcVendorInvoice,')') ELSE '' END)) AS vcBizDocID
   FROM
   OpportunityBizDocs
   INNER JOIN
   OpportunityBizDocItems
   ON
   OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND tintopptype = 2
   AND tintoppstatus = 1
   AND coalesce(bitStockTransfer,false) = false
   AND OpportunityBizDocs.numBizDocId = 305 -- Vendor Invoice
   AND(coalesce(numUnitHour,0) -coalesce(numVendorInvoiceUnitReceived,0)) > 0
   AND 1 =(CASE v_tintMode
   WHEN 3 -- Purchase Order
   THEN(CASE WHEN OpportunityMaster.numOppId = v_numRecordID THEN 1 ELSE 0 END)
   WHEN 4 -- Vendor
   THEN(CASE WHEN OpportunityMaster.numDivisionId = v_numRecordID THEN 1 ELSE 0 END)
   END);
   RETURN;
END; $$;













