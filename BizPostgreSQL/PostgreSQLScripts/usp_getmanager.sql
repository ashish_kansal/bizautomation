-- Stored procedure definition script USP_GetManager for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetManager(v_numManagerID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select CONCAT(vcFirstName,' ',vcLastname) from AdditionalContactsInformation where numContactId = v_numManagerID;
END; $$;












