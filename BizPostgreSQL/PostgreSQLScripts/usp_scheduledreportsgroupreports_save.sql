DROP FUNCTION IF EXISTS USP_ScheduledReportsGroupReports_Save;

CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroupReports_Save(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_vcReports TEXT
	,v_tintMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE

   v_hDoc  INTEGER;
BEGIN
   IF v_tintMode = 1 then
      DROP TABLE IF EXISTS tt_TEMPSRGRSave CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSRGRSave
      (
         tintReportType SMALLINT,
         numReportID NUMERIC(18,0),
         intSortOrder INTEGER
      );

      INSERT INTO tt_TEMPSRGRSave(tintReportType
			,numReportID
			,intSortOrder)
      SELECT
      ReportType
			,ReportID
			,SortOrder
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcReports AS XML)
			COLUMNS
				id FOR ORDINALITY,
				ReportType SMALLINT PATH 'ReportType',
				ReportID NUMERIC(18,0) PATH 'ReportID',
				SortOrder INTEGER PATH 'SortOrder'
		) AS X;


      IF(SELECT COUNT(*) FROM tt_TEMPSRGRSave) > 5 then
		
         RAISE EXCEPTION 'MAX_5_REPORTS';
      ELSE
         DELETE FROM ScheduledReportsGroupReports SRGR using ScheduledReportsGroup SRG where SRG.ID = SRGR.numSRGID AND SRG.numDomainID = v_numDomainID AND numSRGID = v_numSRGID;
         INSERT INTO ScheduledReportsGroupReports(numSRGID
				,tintReportType
				,numReportID
				,intSortOrder)
         SELECT
         v_numSRGID
				,tintReportType
				,numReportID
				,intSortOrder
         FROM
         tt_TEMPSRGRSave;
      end if;
   ELSEIF v_tintMode = 2
   then
	
      IF EXISTS(SELECT numReportID FROM ReportListMaster WHERE numDomainID = v_numDomainID AND numReportID = CAST(v_vcReports AS NUMERIC)) then
		
         IF EXISTS(SELECT ID FROM ScheduledReportsGroupReports WHERE numSRGID = v_numSRGID AND tintReportType = 1 AND numReportID = CAST(v_vcReports AS NUMERIC)) then
			
            RAISE EXCEPTION 'REPORT_ALREADY_ADDED';
         ELSE
            IF(SELECT COUNT(*) FROM ScheduledReportsGroupReports WHERE numSRGID = v_numSRGID) >= 5 then
				
               RAISE NOTICE 'MAX_5_REPORTS';
            ELSE
               INSERT INTO ScheduledReportsGroupReports(numSRGID
						,tintReportType
						,numReportID
						,intSortOrder)
					VALUES(v_numSRGID
						,1
						,CAST(v_vcReports AS NUMERIC)
						,0);
            end if;
         end if;
      ELSE
         RAISE EXCEPTION 'REPORT_DOES_NOT_EXISTS';
      end if;
   end if;
   RETURN;
END; $$;


