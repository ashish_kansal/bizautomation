-- Function definition script GetEmaillName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetEmaillName(v_numEmailHstrID NUMERIC,v_tinttype INTEGER)
RETURNS VARCHAR(200) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Name  VARCHAR(200);
BEGIN
   select   coalesce(EHS.vcName,'') INTO v_Name from EmailHStrToBCCAndCC  EHS
   join EmailMaster EM on EM.numEmailId =  EHS.numEmailId where numEmailHstrID = v_numEmailHstrID and tintType = v_tinttype    LIMIT 1;
   return v_Name;
END; $$;

