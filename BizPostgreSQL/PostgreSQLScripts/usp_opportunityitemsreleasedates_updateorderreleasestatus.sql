-- Stored procedure definition script USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numOppID  NUMERIC(18,0);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0)
   );
   INSERT INTO
   tt_TEMP(numOppID)
   SELECT
   numOppId
   FROM
   OpportunityMaster
   WHERE
   numDomainId = v_numDomainID
   AND numOppId IN(SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE numDomainID = v_numDomainID)
   AND dtReleaseDate IS NOT NULL
   AND coalesce(numReleaseStatus,0) <> 2;

   select   COUNT(*) INTO v_COUNT FROM tt_TEMP;

   WHILE v_i <= v_COUNT LOOP
      select   numOppID INTO v_numOppID FROM tt_TEMP WHERE ID = v_i;
      IF NOT EXISTS(SELECT
      numOppID
      FROM(SELECT
         OIRD.numOppId,
					OIRD.numOppItemID,
					OI.numUnitHour*fn_UOMConversion(OI.numUOMId,I.numItemCode,OIRD.numDomainID,I.numBaseUnit) AS numOrderedQty,
					SUM(OIRD.numQty) AS numReleaseQty
         FROM
         OpportunityItemsReleaseDates OIRD
         INNER JOIN
         OpportunityItems OI
         ON
         OIRD.numOppId = OI.numOppId
         AND OIRD.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         INNER JOIN
         Item  I
         ON
         OI.numItemCode = I.numItemCode
         WHERE
         OIRD.numDomainID = v_numDomainID
         AND OIRD.numOppId = v_numOppID
         GROUP BY
         OIRD.numDomainID,OIRD.numOppId,OIRD.numOppItemID,OI.numUnitHour,OI.numUOMId,
         I.numItemCode,I.numBaseUnit) T1
      WHERE
      numOrderedQty > numReleaseQty) then
		
         UPDATE OpportunityMaster SET numReleaseStatus = 2 WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


