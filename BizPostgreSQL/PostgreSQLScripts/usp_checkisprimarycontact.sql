-- Stored procedure definition script usp_CheckIsPrimaryContact for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CheckIsPrimaryContact(   
	--@numCheckFlg numeric=0
--
v_numContactID NUMERIC DEFAULT 0,
	v_numDivId NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN

	--If the checkflag is passed then it means that we need to check whether a particular contact is of the type
	--Primary Contact or not.
   IF v_numContactID <> 0 then
		
      open SWV_RefCur for
      SELECT a.numContactType from AdditionalContactsInformation a INNER JOIN Listdetails b ON a.numContactType = b.numListItemID where
      coalesce(a.bitPrimaryContact,false) = true and a.numContactId = v_numContactID;
	--If not then we need to get the contactId for whom the mails are to be sent
   ELSE
      open SWV_RefCur for
      SELECT vcFirstName,vcLastname,numContactId FROM AdditionalContactsInformation WHERE coalesce(bitPrimaryContact,false) = true and numDivisionId = v_numDivId;
   end if;
   RETURN;
END; $$;


