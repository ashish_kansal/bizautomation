-- Stored procedure definition script USP_GetResourceId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetResourceId(v_numUserCntId NUMERIC(9,0),
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from Resource where numUserCntId = v_numUserCntId and numDomainId = v_numDomainId;
END; $$;












