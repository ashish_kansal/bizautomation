-- Stored procedure definition script USP_DycFieldMasterSynonym_GetByFieldID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DycFieldMasterSynonym_GetByFieldID(v_numDomainID NUMERIC(18,0)
	,v_numFieldID NUMERIC(18,0)
	,v_bitCustomField BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ID as "ID"
		,numFieldID as "numFieldID"
		,vcSynonym as "vcSynonym"
		,bitDefault as "bitDefault"
   FROM
   DycFieldMasterSynonym
   WHERE
		(numDomainID = v_numDomainID OR bitDefault = true)
   AND numFieldID = v_numFieldID
   AND coalesce(bitCustomField,false) = v_bitCustomField
   ORDER BY
   vcSynonym;
END; $$;












