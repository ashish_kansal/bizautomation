DROP FUNCTION IF EXISTS USP_AdvancedSearchItems;

CREATE OR REPLACE FUNCTION USP_AdvancedSearchItems(v_WhereCondition VARCHAR(4000) DEFAULT '',                            
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(18,0) DEFAULT NULL,                         
v_CurrentPage INTEGER DEFAULT NULL,
v_PageSize INTEGER DEFAULT NULL,        
INOUT v_TotRecs INTEGER  DEFAULT NULL,
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                              
v_ColumnSearch VARCHAR(100) DEFAULT '',                           
v_ColumnName VARCHAR(50) DEFAULT '',                             
v_GetAll BOOLEAN DEFAULT NULL,                              
v_SortCharacter CHAR(1) DEFAULT NULL,                        
v_SortColumnName VARCHAR(50) DEFAULT '',        
v_bitMassUpdate BOOLEAN DEFAULT NULL,        
v_LookTable VARCHAR(10) DEFAULT '' ,        
v_strMassUpdate VARCHAR(2000) DEFAULT '',
v_vcDisplayColumns TEXT DEFAULT NULL, 
INOUT SWV_RefCur refcursor default null,
INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_strSql  VARCHAR(8000);
   v_numMaxFieldId  NUMERIC(9,0);
   v_from  VARCHAR(1000);
   v_Where  VARCHAR(4000);
   v_OrderBy  VARCHAR(1000);
   v_GroupBy  VARCHAR(1000);
   v_SelectFields  VARCHAR(4000);
   v_InneJoinOn  VARCHAR(1000);
         
   v_tintOrder  SMALLINT;
   v_vcFormFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(3);
   v_vcAssociatedControlType  VARCHAR(20);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(30);
   v_vcLookBackTableName  VARCHAR(50);
   v_WCondition  VARCHAR(1000);
   v_vcLocationID  VARCHAR(100) DEFAULT '0';
   v_Table  VARCHAR(200);
   v_bitCustom  BOOLEAN;
   v_numFieldGroupID  INTEGER;
   v_vcColumnName  VARCHAR(200);
   v_numFieldID  NUMERIC(18,0);

   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_strTemp  TEXT;
   v_vcItemCodes  TEXT;
   SWV_RowCount INTEGER;
   
BEGIN
	v_WhereCondition := REPLACE(v_WhereCondition,' like ',' ilike ');
	v_WhereCondition := REPLACE(v_WhereCondition,'dbo.','');
	v_WhereCondition := REPLACE(v_WhereCondition,'ISNULL','COALESCE');
	v_WhereCondition := REPLACE(v_WhereCondition,'I.numItemCode IN (SELECT OutParam FROM SplitString(','I.numItemCode IN (SELECT Id FROM SplitIDs(');

   IF (v_SortCharacter <> '0' AND v_SortCharacter <> '') then
      v_ColumnSearch := v_SortCharacter;
   end if;
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   
------------Declaration---------------------  
   v_InneJoinOn := '';
   v_OrderBy := '';
   v_Where := '';
   v_from := ' FROM 
				Item I 
			LEFT JOIN Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode AND I.numVendorID = V.numVendorID
			LEFT JOIN DivisionMaster DM ON V.numVendorID = DM.numDivisionID 
			LEFt JOIN AdditionalContactsinformation ADC on ADC.numDivisionID = DM.numDomainID AND COALESCE(ADC.bitPrimaryContact,false)=true
			LEFT JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyID
			LEFT JOIN LATERAL
			(
				SELECT
					SUM(numOnHand) AS numOnHand,
					SUM(numBackOrder) AS numBackOrder,
					SUM(numOnOrder) AS numOnOrder,
					SUM(numAllocation) AS numAllocation,
					SUM(numReorder) AS numReorder
				FROM
					WareHouseItems
				WHERE
					numItemID = I.numItemCode
			) WI ON true';

   v_GroupBy := '';
   v_SelectFields := '';  
   v_WCondition := '';

--	SELECT I.numItemCode,* FROM item I 
--	LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID
--	LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode --AND I.numVendorID = V.numVendorID
	
	
   v_Where := coalesce(v_Where,'') || ' WHERE I.charItemType <>''A'' and I.numDomainID=' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(15)),1,15);
   if (v_ColumnSearch <> '') then 
      v_Where := coalesce(v_Where,'') || ' and I.vcItemName ilike ''' || COALESCE(v_ColumnSearch,'') || '%''';
   end if;
   
   IF POSITION('SplitString' IN v_WhereCondition) > 0 then
      v_WhereCondition	:= REPLACE(v_WhereCondition,' AND I.numItemCode IN ','');
      v_WhereCondition := SUBSTR(v_WhereCondition,length(v_WhereCondition) -(LENGTH(v_WhereCondition) -1)+1);
      v_WhereCondition := SUBSTR(v_WhereCondition,1,LENGTH(v_WhereCondition) -1);
      
	  v_strTemp := '
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
	  
      CREATE TEMPORARY TABLE tt_TEMP
	  (
	  	ItemID INTEGER PRIMARY KEY
	  );
																								   
	  INSERT INTO tt_TEMP ' || coalesce(v_WhereCondition,'');
      
	  RAISE NOTICE '%',v_strTemp;
	  
      EXECUTE v_strTemp;
	  
	  v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' JOIN tt_TEMP SP ON SP.ItemID = I.numItemCode ';
   ELSE
      v_Where := coalesce(v_Where,'') || coalesce(v_WhereCondition,'');
   END IF;
	
   v_tintOrder := 0;
   v_WhereCondition := '';
   v_strSql := 'SELECT  I.numItemCode ';

   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN,
      tintOrder INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 29;   

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempIDs_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPIDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIDS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         vcFieldID VARCHAR(300)
      );
      
      INSERT INTO tt_TEMPIDS(vcFieldID)
      SELECT OutParam
      FROM SplitString(v_vcDisplayColumns,',');
																								   
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,(SELECT(ID::bigint -1) FROM tt_TEMPIDS T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
      FROM(SELECT
         numFieldID as numFormFieldID,
				false AS bitCustomField,
				CONCAT(numFieldID,'~0') AS vcFieldID
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 29
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,true AS bitCustomField
				,CONCAT(Fld_id,'~1') AS vcFieldID
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT vcFieldID FROM tt_TEMPIDS);
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
      FROM(SELECT
         A.numFormFieldID,
				false AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = 29
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = 29
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT
         A.numFormFieldID,
				true AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = 29
         AND C.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFieldID,
			CAST(0 AS BOOLEAN),
			1,
			0
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 29
      AND numDomainID = v_numDomainID
      AND numFieldID = 189;
   END IF;

   SELECT   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   			v_vcListItemType, v_numListID, v_vcLookBackTableName, v_bitCustom, v_numFieldGroupID 
   FROM	(	SELECT T1.tintOrder::bigint+1 AS tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 29
      AND coalesce(T1.bitCustomField,false) = false
      UNION
      SELECT T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			Grp_id
      FROM
      CFW_Fld_Master c
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      c.Fld_id = T1.numFieldID
      WHERE
      c.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1    
   ORDER BY tintOrder asc LIMIT 1;
   
   while v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
	
         if v_vcAssociatedControlType = 'SelectBox' then
        
            if v_vcListItemType = 'IG' then --Item group
		  
               v_SelectFields := coalesce(v_SelectFields,'') || ',IG.vcItemGroup "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID ';
               v_GroupBy := coalesce(v_GroupBy,'') || 'IG.vcItemGroup,';
            ELSEIF v_vcListItemType = 'PP'
            then--Item Type
		  
               v_SelectFields := coalesce(v_SelectFields,'') || ', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN  ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_GroupBy := coalesce(v_GroupBy,'') || 'I.charItemType,';
            ELSEIF v_vcListItemType = 'UOM'
            then--Unit of Measure
		  
               v_SelectFields := coalesce(v_SelectFields,'') || ' , fn_GetUOMName(I.' || coalesce(v_vcDbColumnName,'') || ') "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_GroupBy := coalesce(v_GroupBy,'') || 'I.' || coalesce(v_vcDbColumnName,'') || ',';
            ELSEIF v_vcListItemType = 'LI'
            then--Any List Item
		  
               IF v_vcDbColumnName = 'vcUnitofMeasure' then
			
                  v_SelectFields := coalesce(v_SelectFields,'') || ' ,I.' || coalesce(v_vcDbColumnName,'') || ' "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
                  v_GroupBy := coalesce(v_GroupBy,'') || 'I.' || coalesce(v_vcDbColumnName,'') || ',';
               ELSE
                  v_SelectFields := coalesce(v_SelectFields,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
                  v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
                  v_GroupBy := coalesce(v_GroupBy,'') || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData,';
                  IF v_SortColumnName = 'numItemClassification' then
                     v_SortColumnName := 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData';
                  end if;
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'ListBox'
         then
		
            if v_vcListItemType = 'V' then--Vendor
			
               v_SelectFields := coalesce(v_SelectFields,'') || ', fn_GetComapnyName(I.numVendorID)  "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_GroupBy := coalesce(v_GroupBy,'') || 'I.' || coalesce(v_vcDbColumnName,'') || ',';
            end if;
         ELSE
            IF v_vcLookBackTableName = 'WareHouseItems' then
			
               IF v_vcDbColumnName = 'vcBarCode' then
				
                  v_SelectFields := coalesce(v_SelectFields,'') || ', STRING_AGG((SELECT vcBarCode FROM WareHouseItems WHERE numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(15)),1,15) || ' AND numItemID = I.numItemCode), N'', '' ) "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               ELSEIF v_vcDbColumnName = 'vcWHSKU'
               then
				
                  v_SelectFields := coalesce(v_SelectFields,'') || ', STRING_AGG((SELECT vcWHSKU FROM WareHouseItems WHERE numDomainID = ' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(15)),1,15) || ' AND numItemID = I.numItemCode), N'', '' ) "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               ELSE
                  v_SelectFields := coalesce(v_SelectFields,'') || ' ,WI.' || coalesce(v_vcDbColumnName,'') || ' "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               end if;
            ELSEIF v_vcDbColumnName = 'vcSerialNo'
            then
			
               v_SelectFields := coalesce(v_SelectFields,'') || ' ,0 "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'vcPartNo'
            then
			
               v_SelectFields := coalesce(v_SelectFields,'') || ' ,(SELECT COALESCE(vcPartNo,'''') FROM Vendor V1 WHERE V1.numVendorID =I.numVendorID LIMIT 1) AS  "' ||  coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            ELSE
               v_SelectFields := coalesce(v_SelectFields,'') || ' ,I.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_GroupBy := coalesce(v_GroupBy,'') || 'I.' || coalesce(v_vcDbColumnName,'') || ',';
            end if;
         end if;
      ELSEIF v_bitCustom = true
      then
	
         v_vcColumnName := CONCAT(v_vcFormFieldName,'~','Cust',v_numFieldID);
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
		
            v_SelectFields := coalesce(v_SelectFields,'') || ',CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_InneJoinOn := coalesce(v_InneJoinOn,'')
            || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldID, 0) AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || '.RecId=I.numItemCode ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
		
            v_SelectFields := coalesce(v_SelectFields,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Value,0)='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '             
			on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldID, 0) AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.RecId=I.numItemCode ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
		
            v_SelectFields := coalesce(v_SelectFields,'') || ', FormatedDateFromDate(CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '                 
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.RecId=I.numItemCode ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
		
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10);
            v_SelectFields := coalesce(v_SelectFields,'') || ',L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '                 
				on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldID, 0) AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.RecId=I.numItemCode ';
            v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' left Join ListDetails L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
		
            v_SelectFields := coalesce(v_SelectFields,'') || ', STRING_AGG((SELECT vcData FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM dbo.SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'',''))), '' ,'')  "' || coalesce(v_vcColumnName,'') || '"';
            v_InneJoinOn := coalesce(v_InneJoinOn,'')
            || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldID, 0) AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3)
            || '.RecId=I.numItemCode ';
         end if;
      end if;
	  
      select   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldGroupID 
	  FROM(SELECT T1.tintOrder::bigint+1 AS tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
         FROM
         View_DynamicDefaultColumns D
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         D.numFieldID = T1.numFieldID
         WHERE
         D.numDomainID = v_numDomainID
         AND D.numFormId = 29
         AND coalesce(T1.bitCustomField,false) = false
         AND T1.tintOrder > v_tintOrder -1
         UNION
         SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			Grp_id
         FROM
         CFW_Fld_Master c
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         c.Fld_id = T1.numFieldID
         WHERE
         c.numDomainID = v_numDomainID
         AND COALESCE(T1.bitCustomField,false) = true
         AND T1.tintOrder > v_tintOrder -1) T1    
		 ORDER BY tintOrder ASC LIMIT 1;
      
	  GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      
	  if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   
   END LOOP;                              

/*--------------Sorting logic-----------*/
	
   IF v_SortColumnName = '' then
      v_SortColumnName := 'vcItemName';
   ELSEIF v_SortColumnName = 'numItemGroup'
   then
      v_SortColumnName := 'IG.vcItemGroup';
   ELSEIF v_SortColumnName = 'charItemType'
   then
      v_SortColumnName := 'I.charItemType';
   ELSEIF v_SortColumnName = 'vcPartNo'
   then
      v_SortColumnName := 'vcItemName';
   end if;
	 
   v_OrderBy := ' ORDER BY ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');	
	
/*--------------Sorting logic ends-----------*/
	
   v_strSql :=  coalesce(v_strSql,'') /*+ @SelectFields */|| coalesce(v_from,'') || coalesce(v_InneJoinOn,'') || coalesce(v_Where,'') || coalesce(v_OrderBy,'');
   
   v_strSql := '
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numItemID NUMERIC(18, 0)
   );
  
   INSERT INTO tt_TEMPTABLE (numItemID) 
   ' || COALESCE(v_strSql, '');
   
   RAISE NOTICE '%',v_strSql;
   
   EXECUTE v_strSql;

/*-----------------------------Mass Update-----------------------------*/
   if v_bitMassUpdate = true then

      IF POSITION('USP_ItemCategory_MassUpdate' IN v_strMassUpdate) > 0 then
         v_vcItemCodes := '';
         select   coalesce(v_vcItemCodes,'') || numItemID || ', ' INTO v_vcItemCodes from tt_TEMPTABLE;
         v_strMassUpdate := REPLACE(v_strMassUpdate,'##ItemCodes##',SUBSTR(v_vcItemCodes,0,LENGTH(v_vcItemCodes)));
         RAISE NOTICE '%',v_strMassUpdate;
         EXECUTE v_strMassUpdate;
      ELSE
         RAISE NOTICE '%',v_strMassUpdate;
         EXECUTE v_strMassUpdate;
      end if;
   end if;

	
/*-----------------------------Paging Logic-----------------------------*/
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;
	
   v_InneJoinOn := coalesce(v_InneJoinOn,'') || ' join tt_TEMPTABLE T on T.numItemID = I.numItemCode ';
   v_strSql := 'SELECT DISTINCT I.numItemCode  "numItemCode", T.ID "ID~ID",COALESCE(I.bitSerialized,false) as "bitSerialized",COALESCE(I.bitLotNo,false) as "bitLotNo" ' ||  coalesce(v_SelectFields,'') || coalesce(v_from,'') || coalesce(v_InneJoinOn,'') || coalesce(v_Where,''); 
   IF v_GetAll = false then
	
      v_strSql := coalesce(v_strSql,'') || ' and T.ID > ' || SUBSTR(CAST(COALESCE(v_firstRec, 0) AS VARCHAR(10)),1,10) || ' and T.ID <' || SUBSTR(CAST(COALESCE(v_lastRec, 0) AS VARCHAR(10)),1,10);
   end if;
	
	-- enable or disble group by based on search result view
   IF POSITION('WareHouseItems WI' IN v_InneJoinOn) = 0 then
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY T.ID; ';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' GROUP BY I.numItemCode,T.ID,I.bitSerialized,I.bitLotNo,I.numVendorID,' || SUBSTR(v_GroupBy,1,LENGTH(v_GroupBy) -1);
   end if;
	
	
   
   OPEN SWV_RefCur FOR 
   EXECUTE v_strSql;
   
   RAISE NOTICE '%',v_strSql;
	
   --DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
-------------------------------------------------	


   OPEN SWV_RefCur2 FOR
   SELECT vcDbColumnName,
		vcFormFieldName,
		tintOrder AS tintOrder,
		bitAllowSorting
   FROM(SELECT D.vcDbColumnName,
			D.vcFieldName AS vcFormFieldName,
			T1.tintOrder AS tintOrder,
			coalesce(D.bitAllowSorting,false) AS bitAllowSorting
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 29
      AND coalesce(T1.bitCustomField,false) = false
      AND D.vcDbColumnName not in('numBillShipCountry','numBillShipState')
      UNION
      SELECT
      CONCAT('Cust',Fld_id),
			Fld_label,
			T1.tintOrder AS tintOrder,
			true AS bitAllowSorting
      FROM
      CFW_Fld_Master C
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      C.Fld_id = T1.numFieldID
      WHERE
      C.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1
   UNION
   SELECT 'ID','numItemCode',CAST(0 AS INTEGER) AS tintOrder,false
   ORDER BY tintOrder ASC;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   --RETURN;
END;
$$;






