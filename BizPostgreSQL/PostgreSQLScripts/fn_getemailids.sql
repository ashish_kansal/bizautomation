-- Function definition script fn_GetEmailIDs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetEmailIDs(v_vcEmailIds VARCHAR(250))
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
--This function will take the AOI IDs in comma separated string and return the AOI names with <BR> separating each AOI.
   DECLARE
   v_intFirstComma  INTEGER;
   v_intNextComma  INTEGER;
   v_vcTempValue  VARCHAR(255);
   v_vcRetVal  VARCHAR(1000);
   v_vcTempVal  NUMERIC;
BEGIN
   v_vcRetVal := '';

   IF v_vcEmailIds = '' then
	
      RETURN '&nbsp;';
   end if;
   v_intFirstComma := POSITION(';' IN v_vcEmailIds);
   v_intNextComma := CASE WHEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intFirstComma+1)) > 0 THEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   IF v_intNextComma = 0 then
	
      v_vcTempValue := SUBSTR(v_vcEmailIds,v_intFirstComma+1,LENGTH(v_vcEmailIds) -v_intFirstComma);
      IF v_vcTempValue <> '' then
		
         v_vcTempVal := 0;
         select   numContactId INTO v_vcTempVal FROM AdditionalContactsInformation WHERE LOWER(vcEmail) = LOWER(v_vcTempValue) ORDER BY numContactId;
         IF v_vcTempVal > 0 then
			
            v_vcRetVal := coalesce(v_vcRetVal,'') || ',' || SUBSTR(CAST(v_vcTempVal AS VARCHAR(30)),1,30);
         end if;
      end if;
      v_intFirstComma := CASE WHEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intNextComma)) > 0 THEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
   end if;
   WHILE v_intFirstComma > 0 LOOP
      IF v_intNextComma = 0 then
		
         v_vcTempValue := SUBSTR(v_vcEmailIds,v_intFirstComma+1,LENGTH(v_vcEmailIds) -v_intFirstComma);
         IF v_vcTempValue <> '' then
			
            v_vcTempVal := 0;
            select   numContactId INTO v_vcTempVal FROM AdditionalContactsInformation WHERE LOWER(vcEmail) = LOWER(v_vcTempValue) ORDER BY numContactId;
            IF v_vcTempVal > 0 then
				
               v_vcRetVal := coalesce(v_vcRetVal,'') || ',' || SUBSTR(CAST(v_vcTempVal AS VARCHAR(30)),1,30);
            end if;
         end if;
         EXIT;
      ELSE
         v_vcTempValue := SUBSTR(v_vcEmailIds,v_intFirstComma+1,v_intNextComma -v_intFirstComma -1);
         IF v_vcTempValue <> '' then
			
            v_vcTempVal := 0;
            select   numContactId INTO v_vcTempVal FROM AdditionalContactsInformation WHERE LOWER(vcEmail) = LOWER(v_vcTempValue) ORDER BY numContactId;
            IF v_vcTempVal > 0 then
				
               v_vcRetVal := coalesce(v_vcRetVal,'') || ',' || SUBSTR(CAST(v_vcTempVal As VARCHAR(30)),1,30);
            end if;
         end if;
      end if;
      v_intFirstComma := CASE WHEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intNextComma)) > 0 THEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intNextComma))+v_intNextComma -1 ELSE 0 END;
      v_intNextComma := CASE WHEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intFirstComma+1)) > 0 THEN POSITION(';' IN SUBSTR(v_vcEmailIds,v_intFirstComma+1))+v_intFirstComma::bigint+1 -1 ELSE 0 END;
   END LOOP;

   RETURN v_vcRetVal;
END; $$;

