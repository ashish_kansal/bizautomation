-- Stored procedure definition script USP_GetAutomatedFollowups for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAutomatedFollowups(v_numContactId NUMERIC,
	v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
 
--DECLARE FollowUpCampaign as  VARCHAR(MAX)
--DECLARE	LastFollowUp VARCHAR(MAX)
--DECLARE	NextFollowUp VARCHAR(MAX)

   open SWV_RefCur for SELECT
   (CASE WHEN
   coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
      WHERE ConECampaign.numContactID = A.numContactId AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0)
   = coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
      WHERE ConECampaign.numContactID = A.numContactId AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged = true),0)
   THEN
      coalesce((SELECT cast(CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',vcECampName,' (' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0) AS VARCHAR(30)) || ' Of ' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true),0) AS VARCHAR(30)) || ') ') as VARCHAR(255)) FROM ECampaign WHERE numECampaignID = A.numECampaignID),'')
   ELSE
      coalesce((SELECT cast(CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/Flag_Green.png"> ',vcECampName,' (' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0) AS VARCHAR(30)) || ' Of ' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true),0) AS VARCHAR(30)) || ') ') as VARCHAR(255)) FROM ECampaign WHERE numECampaignID = A.numECampaignID),'')
   END)  AS "FollowUpCampaign",
	fn_FollowupDetailsInOrgList(v_numContactId,1,v_numDomainId) AS "LastFollowUp",
	fn_FollowupDetailsInOrgList(v_numContactId,2,v_numDomainId) AS "NextFollowUp"
	--dbo.fn_GetFollowUpDetails(@numContactId,3,@numDomainId)LastEmailStatus


   FROM AdditionalContactsInformation A
   WHERE A.numContactId = v_numContactId AND A.numDomainID = v_numDomainId;
END; $$;














