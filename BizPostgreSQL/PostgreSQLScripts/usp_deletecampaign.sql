-- Stored procedure definition script USP_DeleteCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCampaign(v_numCampaignId NUMERIC(9,0) DEFAULT 0  ,
v_numDomainID  NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from CampaignMaster where numCampaignID = v_numCampaignId  and numDomainID = v_numDomainID;
   RETURN;
END; $$;


