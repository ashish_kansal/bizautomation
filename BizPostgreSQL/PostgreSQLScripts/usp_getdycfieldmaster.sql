-- Stored procedure definition script usp_getDycFieldMaster for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDycFieldMaster(v_numDomainID NUMERIC(9,0),
 v_numModuleID NUMERIC(9,0),
v_numCultureID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(DFM.numFieldID as VARCHAR(255)),cast(DFM.numModuleID as VARCHAR(255)),cast(DFM.vcFieldName as VARCHAR(255)),cast(coalesce(DFG.vcNewFieldName,'') as VARCHAR(255)) as vcNewFieldName,
cast(DFM.vcAssociatedControlType as VARCHAR(255)),
cast(coalesce(DFG.vcToolTip,DFM.vcToolTip) as VARCHAR(255)) as vcToolTip
   FROM DycFieldMaster DFM
   left join DycField_Globalization DFG on DFG.numDomainID = v_numDomainID AND DFG.numCultureID = v_numCultureID AND DFM.numFieldID = DFG.numFieldID AND DFM.numModuleID = DFG.numModuleID
   where DFM.numModuleID = v_numModuleID ORDER BY DFM.vcFieldName;
END; $$;












