-- Stored procedure definition script USP_OppWarehouseSerializedItem_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppWarehouseSerializedItem_Insert(v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_vcSelectedItems TEXT,
	v_tintItemType SMALLINT -- 1 = Serial, 2 = Lot
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numWareHouseItmsDTLID  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_numQty  INTEGER;
BEGIN

   BEGIN
      -- BEGIN TRANSACTION
IF v_tintItemType = 1 OR v_tintItemType = 2 then
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPSERIALLOT_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numWareHouseItmsDTLID NUMERIC(18,0),
            numWarehouseItemID NUMERIC(18,0),
            numQty INTEGER
         );
         UPDATE
         WareHouseItmsDTL WIDL
         SET
         numQty =(CASE WHEN v_tintItemType = 2 THEN(coalesce(WIDL.numQty,0)+coalesce(OWSI.numQty,0)) ELSE 1 END)
         FROM
         OppWarehouseSerializedItem OWSI
         WHERE(WIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
         AND WIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND(OWSI.numOppID = v_numOppID
         AND OWSI.numOppItemID = v_numOppItemID);
         DELETE FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numOppItemID = v_numOppItemID;
         IF LENGTH(v_vcSelectedItems) > 0 then
            INSERT INTO tt_TEMPSERIALLOT(numWareHouseItmsDTLID
				,numQty)
            SELECT
             split_part(items,'.',1)::NUMERIC,
				split_part(items,'.',2)::DOUBLE PRECISION
            FROM
            Split(Replace(v_vcSelectedItems,'-','.'),',');
            UPDATE
            tt_TEMPSERIALLOT TSL
            SET
            numWarehouseItemID = WID.numWareHouseItemID
            FROM
            WareHouseItmsDTL WID WHERE TSL.numWareHouseItmsDTLID = WID.numWareHouseItmsDTLID;
            select   COUNT(*) INTO v_COUNT FROM tt_TEMPSERIALLOT;
            WHILE v_i <= v_COUNT LOOP
               select   numWareHouseItmsDTLID, numQty, numWarehouseItemID INTO v_numWareHouseItmsDTLID,v_numQty,v_numWarehouseItemID FROM tt_TEMPSERIALLOT WHERE ID = v_i;
               UPDATE WareHouseItmsDTL SET numQty =(CASE WHEN v_tintItemType = 2 THEN(coalesce(numQty,0) -coalesce(v_numQty,0)) ELSE 0 END) WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
               INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,
					numOppID,
					numOppItemID,
					numWarehouseItmsID,
					numQty)
               SELECT
               v_numWareHouseItmsDTLID,
					v_numOppID,
					v_numOppItemID,
					v_numWarehouseItemID,
					v_numQty;
               v_i := v_i::bigint+1;
            END LOOP;
         end if;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;









