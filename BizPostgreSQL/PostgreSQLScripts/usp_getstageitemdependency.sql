-- Stored procedure definition script usp_GetStageItemDependency for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetStageItemDependency(v_numDomainid NUMERIC DEFAULT 0,
    v_numProjectID NUMERIC DEFAULT 0 ,
    v_numStageDetailID NUMERIC DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0,
    v_tintModeType SMALLINT DEFAULT -1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      select SP.vcStageName,SD.numStageDetailId,SD.numStageDependancyID,SD.numDependantOnID from
      StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID = SP.numStageDetailsId
      where SD.numStageDetailId = v_numStageDetailID and SP.numdomainid = v_numDomainid and
      v_numProjectID =(case v_tintModeType when 0 then SP.numOppid
      when 1 then SP.numProjectid end);
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      SELECT numStageDetailsId,vcStageName
      FROM StagePercentageDetails where v_numProjectID =(case v_tintModeType when 0 then numOppid
      when 1 then numProjectid end) and numdomainid = v_numDomainid and numStageDetailsId
      not in(select numDependantOnID from StageDependency where numStageDetailId = v_numStageDetailID)
      and numStagePercentageId =(select numStagePercentageId from StagePercentageDetails where numStageDetailsId = v_numStageDetailID)
      and numStageDetailsId != v_numStageDetailID;
   ELSEIF v_tintMode = 2
   then

      open SWV_RefCur for
      select ST.numStageDetailId,ST.numDependantOnID,
case when ST.numStageDetailId = v_numStageDetailID then SP2.vcStageName else SP1.vcStageName end AS vcStageName,
case when ST.numStageDetailId = v_numStageDetailID then 'UP' else 'DOWN' end
      AS DependType  from StageDependency ST left join StagePercentageDetails SP1 on
      ST.numStageDetailId = SP1.numStageDetailsId  left join StagePercentageDetails SP2 on
      ST.numDependantOnID = SP2.numStageDetailsId
      where ST.numStageDetailId = v_numStageDetailID or ST.numDependantOnID = v_numStageDetailID;
   ELSEIF v_tintMode = 3
   then

      open SWV_RefCur for
      SELECT numStageDetailsId,vcStageName
      FROM StagePercentageDetails where slp_id = v_numProjectID and coalesce(numProjectid,0) = 0 and coalesce(numOppid,0) = 0 and numdomainid = v_numDomainid	and
      numStageDetailsId not in(select numDependantOnID from StageDependency where numStageDetailId = v_numStageDetailID)
      and numStagePercentageId =(select numStagePercentageId from StagePercentageDetails where numStageDetailsId = v_numStageDetailID)
      and numStageDetailsId != v_numStageDetailID;
   ELSEIF v_tintMode = 4
   then

      open SWV_RefCur for
      select SP.vcStageName,SD.numStageDetailId,SD.numStageDependancyID,SD.numDependantOnID from
      StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID = SP.numStageDetailsId
      where SD.numStageDetailId = v_numStageDetailID and SP.numdomainid = v_numDomainid and
      slp_id = v_numProjectID;
   end if;
--delete from StageDependency where numStageDetailID=611 and numDependantOnID=611
/****** Object:  StoredProcedure [dbo].[USP_GetStateAndCountryID]    Script Date: 07/26/2008 16:18:32 ******/
   RETURN;
END; $$;


