-- Stored procedure definition script USP_ManageItemDiscProfile for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemDiscProfile(v_byteMode SMALLINT DEFAULT 0,
v_numDiscProfileID NUMERIC(9,0) DEFAULT NULL,
v_decDisc DECIMAL DEFAULT NULL,
v_bitApplicable BOOLEAN DEFAULT NULL,
v_numItemID NUMERIC(9,0) DEFAULT 0,
v_numDivisionID NUMERIC(9,0) DEFAULT 0,
v_numRelID NUMERIC(9,0) DEFAULT 0,
v_numProID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      if v_numDiscProfileID > 0 then

         update ItemDiscountProfile set decDisc = v_decDisc,bitApplicable = v_bitApplicable
         where numDiscProfileID = v_numDiscProfileID;
      else
         insert INTO ItemDiscountProfile(numItemID,numDivisionID,decDisc,bitApplicable)
values(v_numItemID,v_numDivisionID,v_decDisc,v_bitApplicable);
      end if;
   else
      if v_numDiscProfileID > 0 then

         update ItemDiscountProfile set decDisc = v_decDisc,bitApplicable = v_bitApplicable
         where numDiscProfileID = v_numDiscProfileID;
      else
         insert INTO ItemDiscountProfile(numItemID,numRelID,numProID,decDisc,bitApplicable)
values(v_numItemID,v_numRelID,v_numProID,v_decDisc,v_bitApplicable);
      end if;
   end if;
   RETURN;
END; $$;


