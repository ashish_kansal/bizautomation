CREATE OR REPLACE FUNCTION USP_OPPGetINItemsForAuthorizativeAccountingEdit(v_numOppId NUMERIC(9,0) DEFAULT 0,
 v_numDomainId NUMERIC(9,0) DEFAULT 0,
 v_numOppBizDocsId NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);
   v_strSQL  VARCHAR(8000);                                                                                
   v_OppType  NUMERIC(9,0);
BEGIN
   v_DivisionID := 0;                                                                           
   v_OppType := 0;                                                                                                              
   v_strSQL := '';                          
                                  
   select   numDivisionId, tintopptype INTO v_DivisionID,v_OppType From OpportunityMaster Where numOppId = v_numOppId And numDomainId = v_numDomainId;                                                                                                                               
                                                           
                                
   if v_OppType = 1 then

      open SWV_RefCur for
      select 0 as numDebitAmt, OBI.monTotAmount as numCreditAmt,
case when bitTaxable = false then 0 when bitTaxable = true then(select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID = v_numOppBizDocsId and numTaxItemID = 0) end as Tax,
i.numItemCode as ItemCode,opp.numoppitemtCode as numoppitemtCode,
numIncomeChartAcntId as itemIncomeAccount,'NI' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile
      from OpportunityItems opp
      join OpportunityBizDocItems OBI on OBI.numOppItemID = opp.numoppitemtCode
      left join Item i on opp.numItemCode = i.numItemCode
      left join Listdetails L on i.numItemClassification = L.numListItemID
      left join General_Journal_Header GJH on opp.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId = v_numOppBizDocsId
      left join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode
      where opp.numOppId = v_numOppId  and OBI.numOppBizDocID = v_numOppBizDocsId
      union
      select 0 as numDebitAmt, CAST(OBI.numUnitHour*i.monAverageCost AS DECIMAL(20,5)) as numCreditAmt,
case when bitTaxable = false then 0 when bitTaxable = true then(select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID = v_numOppBizDocsId and numTaxItemID = 0)  end as Tax,
i.numItemCode as ItemCode,opp.numoppitemtCode as numoppitemtCode,
numAssetChartAcntId as itemIncomeAccount,'IA' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,v_numDomainId as numDomainId ,coalesce(GJD.bitReconcile,false) as bitReconcile
      from OpportunityItems opp
      join OpportunityBizDocItems OBI on OBI.numOppItemID = opp.numoppitemtCode
      left join Item i on opp.numItemCode = i.numItemCode
      left join Listdetails L on i.numItemClassification = L.numListItemID
      left join General_Journal_Header GJH on opp.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId = v_numOppBizDocsId
      left join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId And GJD.chBizDocItems = 'IA' And GJD.numoppitemtCode = opp.numoppitemtCode
      where opp.numOppId = v_numOppId And (i.charItemType = 'A' Or  i.charItemType = 'P')    and OBI.numOppBizDocID = v_numOppBizDocsId  and OBI.bitDropShip <> true
      union
      select OBI.numUnitHour*i.monAverageCost as numDebitAmt,0 as numCreditAmt,
case when bitTaxable = false then 0 when bitTaxable = true then(select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID = v_numOppBizDocsId and numTaxItemID = 0)  end as Tax,
i.numItemCode as ItemCode,opp.numoppitemtCode as numoppitemtCode,
numCOGsChartAcntId as itemIncomeAccount,'COG' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile
      from OpportunityItems opp
      join OpportunityBizDocItems OBI on OBI.numOppItemID = opp.numoppitemtCode
      left join Item i on opp.numItemCode = i.numItemCode
      left join Listdetails L on i.numItemClassification = L.numListItemID
      left join General_Journal_Header GJH on opp.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId = v_numOppBizDocsId
      left join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId And GJD.chBizDocItems = 'COG' And GJD.numoppitemtCode = opp.numoppitemtCode
      where opp.numOppId = v_numOppId And  (i.charItemType = 'A' Or  i.charItemType = 'P')  and OBI.numOppBizDocID = v_numOppBizDocsId    and OBI.bitDropShip <> true
      union
      select 0  as numDebitAmt,
case when coalesce(te.numcontractId,0) = 0
      then
         case when numCategory = 1 then
            coalesce(CAST((EXTRACT(DAY FROM dttodate -dtfromdate)*60*24+EXTRACT(HOUR FROM dttodate -dtfromdate)*60+EXTRACT(MINUTE FROM dttodate -dtfromdate)) AS DECIMAL(10,2))*monAmount/60,0)
         when numCategory = 2 then
            coalesce(monamount,0)
         end
      else
         0
      end as numCreditAmt,
CAST(0.00 AS INTEGER) as Tax,0 as ItemCode, te.numCategoryHDRID as numoppitemtCode  ,(Select  numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 824 And COA.chBizDocItems = 'BE' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'TE' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile
      from timeandexpense te
      left join ContractManagement cm on te.numcontractId = cm.numContractId
      left outer join General_Journal_Header GJH on te.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId = v_numOppBizDocsId
      left outer join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      And GJD.chBizDocItems = 'TE' And te.numCategoryHDRID = GJD.numoppitemtCode
      Where (te.numtype = 1 or te.numtype = 2)  And
(te.numOppBizDocsId = v_numOppBizDocsId  or te.numOppId = v_numOppId) --and bitbill=1            
      union
      Select CAST(GJD.numDebitAmt AS DECIMAL(10,2)) as numDebitAmt,0 as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 814 and COA.chBizDocItems = 'AR' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'AR' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'AR'
      union
      Select CAST(0 AS DECIMAL(10,2)) as numDebitAmt ,GJD.numCreditAmt as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 827 And COA.chBizDocItems = 'ST' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'ST' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'ST'
      union
      Select CAST(GJD.numDebitAmt AS DECIMAL(10,2)) as numDebitAmt,0 as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'DG' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'DG' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'DG'
      union
      Select CAST(0 AS DECIMAL(10,2)) as numDebitAmt,GJD.numCreditAmt as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'SI' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'SI' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'SI'
      union
      Select CAST(0 AS DECIMAL(10,2)) as numDebitAmt,GJD.numCreditAmt as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'LC' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'LC' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId  Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'LC'
      union
      Select CAST(0 AS DECIMAL(10,2)) as numDebitAmt ,GJD.numCreditAmt as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,0 as ItemCode,0 as numoppitemtCode,T.numChartofAcntID as itemIncomeAccount,'OT' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      join  TaxItems T on T.numChartofAcntID = GJD.numChartAcntId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'OT';
   ELSEIF v_OppType = 2
   then

      RAISE NOTICE 'Purch';
      open SWV_RefCur for
      select OBI.monTotAmount as numDebitAmt, CAST(0 AS DOUBLE PRECISION) as numCreditAmt,
case when bitTaxable = false then 0 when bitTaxable = true then(select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID = v_numOppBizDocsId and numTaxItemID = 0) end as Tax,
CAST(i.numItemCode AS VARCHAR(30)) as ItemCode,opp.numoppitemtCode as numoppitemtCode,
coalesce(numAssetChartAcntId,0) as itemIncomeAccount,'NI' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,v_numDomainId as numDomainId
,coalesce(GJD.bitReconcile,false) as bitReconcile
      from OpportunityItems opp
      join OpportunityBizDocItems OBI on OBI.numOppItemID = opp.numoppitemtCode
      left join Item i on opp.numItemCode = i.numItemCode
      left join Listdetails L on i.numItemClassification = L.numListItemID
      left join General_Journal_Header GJH on opp.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And  GJH.numOppBizDocsId = v_numOppBizDocsId
      left join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode
      where opp.numOppId = v_numOppId   and OBI.numOppBizDocID = v_numOppBizDocsId
      union
      select case when coalesce(te.numcontractId,0) = 0
      then
         case when numCategory = 1 then
            coalesce(CAST((EXTRACT(DAY FROM dttodate -dtfromdate)*60*24+EXTRACT(HOUR FROM dttodate -dtfromdate)*60+EXTRACT(MINUTE FROM dttodate -dtfromdate)) AS DECIMAL(10,2))*monAmount/60,0)
         when numCategory = 2 then
            coalesce(monamount,0)
         end
      else
         0
      end as numDebitAmt,
CAST(0 AS DOUBLE PRECISION)  as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode, te.numCategoryHDRID as numoppitemtCode  ,(Select  numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 824 And COA.chBizDocItems = 'BE' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'TE' as chBizDocItems
,coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile
      from timeandexpense te
      left join ContractManagement cm on te.numcontractId = cm.numContractId
      left outer join General_Journal_Header GJH on te.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId = v_numOppBizDocsId
      left outer join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      And GJD.chBizDocItems = 'TE' And te.numCategoryHDRID = GJD.numoppitemtCode
      Where (te.numtype = 1 or te.numtype = 2)  And
(te.numOppBizDocsId = v_numOppBizDocsId  or te.numOppId = v_numOppId)
      union
      Select 0 as numDebitAmt,GJD.numCreditAmt as numCreditAmt, CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,(Select   numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 815 and COA.chBizDocItems = 'AP' And COA.numDomainId = v_numDomainId LIMIT 1)  as itemIncomeAccount,'AP' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'AP'
      union
      Select CAST(GJD.numDebitAmt AS VARCHAR(100)) ,CAST(0 AS DOUBLE PRECISION) as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 827 And COA.chBizDocItems = 'ST' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'ST' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'ST'
      union
      Select 0 as numDebitAmt,GJD.numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,(Select    numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'DG' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'DG' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'DG'
      union
      Select CAST(GJD.numDebitAmt AS VARCHAR(100)) ,CAST(0 AS DOUBLE PRECISION) as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,(Select   numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'SI' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'SI' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'SI'
      union
      Select CAST(GJD.numDebitAmt AS VARCHAR(100)) ,CAST(0 AS DOUBLE PRECISION) as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,(Select   numAccountId From Chart_Of_Accounts COA Where COA.numAcntType = 822 And COA.chBizDocItems = 'LC' And COA.numDomainId = v_numDomainId LIMIT 1) as itemIncomeAccount,'LC' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'LC'
      union
      Select CAST(GJD.numDebitAmt AS VARCHAR(100)) ,CAST(0 AS DOUBLE PRECISION) as numCreditAmt,CAST(0.00 AS INTEGER) as Tax,CAST(0 AS VARCHAR(255)) as ItemCode,0 as numoppitemtCode,T.numChartofAcntID as itemIncomeAccount,'OT' as chBizDocItems,
coalesce(GJH.numJOurnal_Id,0) as numJournalId,coalesce(GJD.numTransactionId,0) as numTransactionId,v_DivisionID as numDivisionId,
v_numDomainId as numDomainId,coalesce(GJD.bitReconcile,false) as bitReconcile from General_Journal_Header GJH
      join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
      join  TaxItems T on T.numChartofAcntID = GJD.numChartAcntId
      Where GJH.numOppId = v_numOppId And GJD.chBizDocItems = 'OT';
   end if;              
--Print @strSQL                                                                                                      
--Exec (@strSQL) 

   RAISE NOTICE 'hi';
   RETURN;
END; $$;


