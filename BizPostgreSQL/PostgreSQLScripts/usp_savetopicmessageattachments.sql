-- Stored procedure definition script usp_SaveTopicMessageAttachments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveTopicMessageAttachments(v_numAttachmentId NUMERIC(18,0) DEFAULT 0, 
v_numTopicId NUMERIC(18,0) DEFAULT 0, 
v_numMessageId NUMERIC(18,0) DEFAULT 0, 
v_vcAttachmentName VARCHAR(500) DEFAULT '', 
v_vcAttachmentUrl VARCHAR(500) DEFAULT '', 
v_numCreatedBy NUMERIC(18,0) DEFAULT 0,  
v_numUpdatedBy NUMERIC(18,0) DEFAULT 0,  
v_numDomainId NUMERIC(18,0) DEFAULT 0,
INOUT v_status VARCHAR(100)  DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numAttachmentId = 0 then
		
      INSERT INTO TOPICMESSAGEATTACHMENTS(numTopicId, numMessageId, vcAttachmentName, vcAttachmentUrl, numCreatedBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, numDomainID)
			VALUES(v_numTopicId, v_numMessageId, v_vcAttachmentName, v_vcAttachmentUrl, v_numCreatedBy, LOCALTIMESTAMP , v_numUpdatedBy, LOCALTIMESTAMP , v_numDomainId);
			
      v_status := '1';
   ELSE
      UPDATE
      TOPICMESSAGEATTACHMENTS
      SET
      numTopicId = v_numTopicId,numMessageId = v_numMessageId,vcAttachmentName = v_vcAttachmentName,
      vcAttachmentUrl = v_vcAttachmentUrl,numUpdatedBy = v_numUpdatedBy,
      dtmUpdatedOn = LOCALTIMESTAMP,numDomainID = v_numDomainId
      WHERE
      numAttachmentId = v_numAttachmentId
      AND numDomainID = v_numDomainId;
      v_status := '3';
   end if;
   RETURN;
END; $$;


