-- Stored procedure definition script try1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION try1(v_sortorder VARCHAR(100) DEFAULT 'asc', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sortcolumn  VARCHAR(100);

   v_strsql  VARCHAR(8000);
BEGIN
   v_sortcolumn := 'bittask';


   v_strsql := 'SELECT PriceRank,bittask,textdetails,intsnoozemins
FROM
   (SELECT bittask,textdetails,intsnoozemins,
       ROW_NUMBER() OVER(ORDER BY numcommid DESC) AS PriceRank
    FROM communication 
   ) AS ProductsWithRowNumber
WHERE PriceRank > 0 AND
    PriceRank <= (0 + 20) order by ' || coalesce(v_sortcolumn,'') || ' ' || coalesce(v_sortorder,'') || ' LIMIT 100';
   RAISE NOTICE '%',v_strsql;
   OPEN SWV_RefCur FOR EXECUTE v_strsql;
   RETURN;
END; $$;


