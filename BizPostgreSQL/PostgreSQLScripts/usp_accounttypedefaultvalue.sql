-- Stored procedure definition script USP_AccountTypeDefaultValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--select  * FROM [AccountTypeDetail]

--Created by Chintan                               
--exec [dbo].[USP_AccountTypeDefaultValue] 177
CREATE OR REPLACE FUNCTION USP_AccountTypeDefaultValue(v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intCount  NUMERIC(9,0);
   v_AccountTypeIdPrimary  NUMERIC(9,0);
   v_AccountTypeIdAsset  NUMERIC(9,0);
   v_AccountTypeIdCurrentAsset  NUMERIC(9,0);
   v_AccountTypeIdFixedAsset  NUMERIC(9,0);
   v_AccountTypeIdLiability  NUMERIC(9,0);
   v_AccountTypeIdCurrentLiability  NUMERIC(9,0);
   v_AccountTypeIdIncome  NUMERIC(9,0);
   v_AccountTypeIdExpense  NUMERIC(9,0);
   v_AccountTypeIdDirectExpense  NUMERIC(9,0);
   v_AccountTypeIdInDirectExpense  NUMERIC(9,0);
   v_AccountTypeIdInEquity  NUMERIC(9,0);
   v_AccountTypeIdShareHolderFunds  NUMERIC(9,0);
   v_AccountTypeIdCOGS  NUMERIC(9,0);
   v_AccountTypeIdNonLiability  NUMERIC(9,0);
   v_AccountTypeIdOtherAsset  NUMERIC(9,0);
   v_AccountTypeIdAccountReceivable  NUMERIC(9,0);
   v_AccountTypeIdAccountPayable  NUMERIC(9,0);
BEGIN
   v_intCount := 0;                                  
 
 
   select   count(*) INTO v_intCount From AccountTypeDetail Where numDomainID = v_numDomainId;                                                    
 
   If v_intCount = 0 then
      
	                                                
   --1  
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01','Primary',NULL,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdPrimary := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0101','Assets',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdAsset := CURRVAL('AccountTypeDetail_seq');

   --2
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010101','Current Assets',v_AccountTypeIdAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdCurrentAsset := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010102','Fixed Assets',v_AccountTypeIdAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdFixedAsset := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010103','Non Current Assets',v_AccountTypeIdAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010104','Other Assets',v_AccountTypeIdAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdOtherAsset := CURRVAL('AccountTypeDetail_seq');
    --commneted by chintan
--   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010401','Long Term Investment',@AccountTypeIdOtherAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)

   --3
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010101','Bank Accounts',v_AccountTypeIdCurrentAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010102','Cash-in-Hand',v_AccountTypeIdCurrentAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010103','Investments',v_AccountTypeIdCurrentAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010104','Stock',v_AccountTypeIdCurrentAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
    
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010105','Account Receivable',v_AccountTypeIdCurrentAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
    
      v_AccountTypeIdAccountReceivable := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0101010501','Accounts Receivable Clearing Acct',v_AccountTypeIdAccountReceivable,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
  
    --4
    --commneted by chintan
--    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010201','Statutory Deposits',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010201','Property & Equipment',v_AccountTypeIdFixedAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010202','Long Term Investments',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01010202','Intangible Assets',v_AccountTypeIdFixedAsset,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);

	--5
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0102','Liabilities',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdLiability := CURRVAL('AccountTypeDetail_seq');
	--6
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010201','Current Liabilities',v_AccountTypeIdLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
    
      v_AccountTypeIdCurrentLiability := CURRVAL('AccountTypeDetail_seq');

    --7
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01020101','Duties & Taxes',v_AccountTypeIdCurrentLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01020102','Account Payable',v_AccountTypeIdCurrentLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdAccountPayable := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0102010201','Accounts Payable Clearing Acct',v_AccountTypeIdAccountPayable,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01020103','Short Term Borrowings',v_AccountTypeIdCurrentLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020104','Suspense A/c',@AccountTypeIdCurrentLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	--8
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010202','Non Current Liabilities',v_AccountTypeIdLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdNonLiability := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01020201','Notes Payable',v_AccountTypeIdNonLiability,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
	--9
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0103','Income',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdIncome := CURRVAL('AccountTypeDetail_seq');
	--10
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010301','Direct Income',v_AccountTypeIdIncome,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010302','Other Income',v_AccountTypeIdIncome,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	--11
    
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0104','Expense',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
    
      v_AccountTypeIdExpense := CURRVAL('AccountTypeDetail_seq');
    --12
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010401','Direct Expense',v_AccountTypeIdExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
    
      v_AccountTypeIdDirectExpense := CURRVAL('AccountTypeDetail_seq');
    --13
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040101','Other Direct Expenses',v_AccountTypeIdDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040102','Selling & Distribution Costs',v_AccountTypeIdDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040103','Operating Expenses',v_AccountTypeIdDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	--C.patel says we do not need seperate account type for Price Variance
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040104','Price Variance',@AccountTypeIdDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),0)
	--14
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010402','Other Expenses',v_AccountTypeIdExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdInDirectExpense := CURRVAL('AccountTypeDetail_seq');
	--15
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040201','Administrative Expenses',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040202','Staff Expenses',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040203','Finance Costs',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040204','Establishment Costs',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040205','Non-Cash Expenses',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01040206','UnCategorized Expense',v_AccountTypeIdInDirectExpense,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	--16
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0105','Equity',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdInEquity := CURRVAL('AccountTypeDetail_seq');
	--17
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010501','Shareholders Funds',v_AccountTypeIdInEquity,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      v_AccountTypeIdShareHolderFunds := CURRVAL('AccountTypeDetail_seq');
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010502','Owner''s Capital Account',v_AccountTypeIdInEquity,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
	--18
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01050101','Reserves & Surplus',v_AccountTypeIdShareHolderFunds,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('01050102','Common Stock',v_AccountTypeIdShareHolderFunds,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),false,true);
   
   -- 19
   
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('0106','Cost Of Goods',v_AccountTypeIdPrimary,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
   
      v_AccountTypeIdCOGS := CURRVAL('AccountTypeDetail_seq');
   
   -- 20
      INSERT INTO AccountTypeDetail(vcAccountCode,vcAccountType,numParentID,numDomainID,dtCreateDate,dtModifiedDate,bitSystem,bitActive) Values('010601','Supplies & Materials - COGS',v_AccountTypeIdCOGS,v_numDomainId,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),true,true);
	
	
	----- 30th Nov,2012
	
      UPDATE AccountTypeDetail SET bitSystem = true
      WHERE vcAccountCode IN('01','0101','010101','01010101','01010102','01010104','01010105','010102',
      '0102','010201','01020101','01020102','010202','0103','010301','010302',
      '0104','010401','01040101','010402','01040203','01040205','0105',
      '010501','01050101','0106','010601','0102010201','0101010501')
      AND numDomainID = v_numDomainId;
	-----
      UPDATE AccountTypeDetail SET bitSystem = false
      WHERE vcAccountCode IN('01010103','01010202','01010204','010103','010104','01020103','01020104',
      '01020201','01040102','01040103','01040201','01040202','01040204','010502')
      AND numDomainID = v_numDomainId;
      return;
   end if;
END; $$;


