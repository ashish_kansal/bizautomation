DROP FUNCTION IF EXISTS USP_GetUserSMTPDetails;

CREATE OR REPLACE FUNCTION USP_GetUserSMTPDetails(v_numUserID NUMERIC(9,0) DEFAULT 0,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL
--                          
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF EXISTS (SELECT S.numUserId FROM SMTPUserDetails S WHERE S.numDomainID = v_numDomainID AND S.numUserId =(CASE WHEN coalesce(v_numUserID,0) = 0 THEN -1 ELSE v_numUserID END)) THEN
		open SWV_RefCur for 
		SELECT
			S.numDomainID
			,S.numUserId
			,S.vcSMTPUserName
			,S.vcSMTPPassword
			,S.vcSMTPServer
			,S.numSMTPPort
			,S.bitSMTPSSL
			,S.bitSMTPServer
			,S.bitSMTPAuth
		FROM 
			SMTPUserDetails S 
		JOIN
			Domain D on D.numDomainId =  S.numDomainID
		WHERE  
			S.numDomainID = v_numDomainID
			AND S.numUserId =(CASE WHEN coalesce(v_numUserID,0) = 0 THEN -1 ELSE v_numUserID END);
	ELSE
		open SWV_RefCur for 
		SELECT
			vcUserName AS vcSMTPUserName
			,numUserDetailId AS numUserId
			,vcSMTPPassword
			,vcSMTPServer
			,numSMTPPort
			,bitSMTPSSL
			,bitSMTPServer
			,bitSMTPAuth
		FROM 
			UserMaster 
		WHERE
			numDomainID = v_numDomainID
			AND numUserDetailId = v_numUserID;
	END IF;
   
END; $$;