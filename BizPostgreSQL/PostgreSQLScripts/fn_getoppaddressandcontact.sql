-- Function definition script fn_getOPPAddressAndContact for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getOPPAddressAndContact(v_numOppId NUMERIC,v_numDomainID  NUMERIC,v_tintMode SMALLINT)
RETURNS table
(
   vcAddress VARCHAR(1000),
   numContact NUMERIC(18,0),
   bitAltContact BOOLEAN,
   vcAltContact VARCHAR(200)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strAddress  VARCHAR(1000);
   v_tintOppType  SMALLINT;
   v_tintBillType  SMALLINT;
   v_tintShipType  SMALLINT;
   v_numBillToAddressID  NUMERIC(18,0);
   v_numShipToAddressID  NUMERIC(18,0);
 
   v_numParentOppID  NUMERIC;
   v_numDivisionID  NUMERIC;
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST
   (
      vcAddress VARCHAR(1000),
      numContact NUMERIC(18,0),
      bitAltContact BOOLEAN,
      vcAltContact VARCHAR(200)
   );
   select   tintopptype, tintBillToType, numBillToAddressID, tintShipToType, numShipToAddressID, numDivisionId INTO v_tintOppType,v_tintBillType,v_numBillToAddressID,v_tintShipType,v_numShipToAddressID,
   v_numDivisionID FROM
   OpportunityMaster WHERE
   numOppId = v_numOppId;

	-- When Creating PO from SO and Bill type is Customer selected 
   select   coalesce(numParentOppID,0) INTO v_numParentOppID FROM OpportunityLinking WHERE numChildOppID = v_numOppId;
            
   IF v_tintMode = 1 then --Billing Address
	
      If coalesce(v_numBillToAddressID,0) > 0 then
		
         INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
         || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
         || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcAddress
				,coalesce(AD.numContact,0) AS numContact
				,coalesce(AD.bitAltContact,false) AS bitAltContact
				,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numBillToAddressID;
      ELSE
         IF v_tintBillType IS NULL OR (v_tintBillType = 1 AND v_tintOppType = 1) then --Primary Bill Address or When Sales order and bill to is set to customer	 
				
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
            || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'')
						,coalesce(AD.numContact,0) AS numContact
						,coalesce(AD.bitAltContact,false) AS bitAltContact
						,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
            FROM
            AddressDetails AD
            WHERE
            AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
            AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;
         ELSEIF v_tintBillType = 1 AND v_tintOppType = 2
         then -- When Create PO from SO and Bill to is set to Customer
				
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact) SELECT vcAddress,numContact,bitAltContact,vcAltContact FROM fn_getOPPAddressAndContact(v_numParentOppID,v_numDomainID,v_tintMode);
         ELSEIF v_tintBillType = 0
         then
				
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
            || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'')
						,coalesce(AD.numContact,0) AS numContact
						,coalesce(AD.bitAltContact,false) AS bitAltContact
						,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
            FROM   CompanyInfo Com1 JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
            JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
            JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true
            WHERE  D1.numDomainId = v_numDomainID;
         ELSEIF v_tintBillType = 2 OR v_tintBillType = 3
         then
				
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(vcBillStreet,'') || ' </pre>'
            || coalesce(vcBillCity,'') || ' ,' || coalesce(fn_GetState(numBillState),'') || ' ' || coalesce(vcBillPostCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(numBillCountry),'')
						,coalesce(numBillingContact,0) AS numContact
						,coalesce(bitAltBillingContact,false) AS bitAltContact
						,coalesce(vcAltBillingContact,'') AS vcAltContact
            FROM
            OpportunityAddress
            WHERE
            numOppID = v_numOppId;
         end if;
      end if;
   ELSEIF v_tintMode = 2
   then --Shipping Address
	
      If coalesce(v_numShipToAddressID,0) > 0 then
		
         INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
         || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
         || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'')
				,coalesce(AD.numContact,0) AS numContact
				,coalesce(AD.bitAltContact,false) AS bitAltContact
				,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numShipToAddressID;
      ELSE
         IF v_tintShipType IS NULL OR (v_tintShipType = 1 AND v_tintOppType = 1) then
			
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
            || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'')
					,coalesce(AD.numContact,0) AS numContact
					,coalesce(AD.bitAltContact,false) AS bitAltContact
					,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
            FROM AddressDetails AD
            WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
            AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;
         ELSEIF v_tintShipType = 1 AND v_tintOppType = 2
         then -- When Create PO from SO and Ship to is set to Customer 
			
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact) SELECT vcAddress,numContact,bitAltContact,vcAltContact FROM fn_getOPPAddressAndContact(v_numParentOppID,v_numDomainID,v_tintMode);
         ELSEIF v_tintShipType = 0
         then
			
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(AD.vcStreet,'') || '</pre>'
            || coalesce(AD.vcCity,'') || ' ,' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'')
					,coalesce(AD.numContact,0) AS numContact
					,coalesce(AD.bitAltContact,false) AS bitAltContact
					,coalesce(AD.vcAltContact,cast(0 as TEXT)) AS vcAltContact
            FROM   CompanyInfo Com1 JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
            JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
            JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true
            WHERE  D1.numDomainId = v_numDomainID;
         ELSEIF v_tintShipType = 2 OR v_tintShipType = 3
         then
			
            INSERT INTO tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST(vcAddress,numContact,bitAltContact,vcAltContact)
            SELECT
            '<pre>' || coalesce(vcShipStreet,'') || ' </pre>'
            || coalesce(vcShipCity,'') || ' ,' || coalesce(fn_GetState(numShipState),'') || ' ' || coalesce(vcShipPostCode,'')
            || ' <br>' || coalesce(fn_GetListItemName(numShipCountry),'')
					,coalesce(numShippingContact,0) AS numContact
					,coalesce(bitAltShippingContact,false) AS bitAltContact
					,coalesce(vcAltShippingContact,'') AS vcAltContact
            FROM
            OpportunityAddress
            WHERE
            numOppID = v_numOppId;
         end if;
      end if;
   end if;

   RETURN QUERY (SELECT * FROM tt_FN_GETOPPADDRESSANDCONTACT_PARSEDLIST);
END; $$;

