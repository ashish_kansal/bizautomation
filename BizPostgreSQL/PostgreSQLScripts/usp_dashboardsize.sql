-- Stored procedure definition script USP_DashboardSize for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DashboardSize(v_tintColumn SMALLINT, 
v_tintSize SMALLINT,  
v_numGroupID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numGroupID > 0 then

      update DashBoardSize set
      tintSize = v_tintSize
      where numGroupUserCntID = v_numGroupID and bitGroup = true and tintColumn = v_tintColumn;
   ELSEIF v_numUserCntID > 0
   then

      update DashBoardSize set
      tintSize = v_tintSize
      where numGroupUserCntID = v_numUserCntID and bitGroup = false  and tintColumn = v_tintColumn;
   end if;
   RETURN;
END; $$;


