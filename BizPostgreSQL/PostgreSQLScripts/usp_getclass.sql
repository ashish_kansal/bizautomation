DROP FUNCTION IF EXISTS USP_GetClass;

CREATE OR REPLACE FUNCTION USP_GetClass(v_numDomainID NUMERIC,
    v_tintMode SMALLINT,
    INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      IF NOT EXISTS(SELECT * FROM ClassDetails WHERE numDomainID = v_numDomainID) then
				
         open SWV_RefCur for
         SELECT  CAST(0 AS NUMERIC) AS numParentClassID,
							numListItemID AS numChildClassID,
							coalesce(vcData,'') AS ClassName
         FROM    Listdetails
         WHERE   numDomainid = v_numDomainID
         AND numListID = 381
         UNION
         SELECT CAST(null as INTEGER),0,'Root';
      ELSE
         open SWV_RefCur for
         SELECT numID,
						   numParentClassID,
						   numChildClassID,
						   fn_GetListItemName(numChildClassID) AS ClassName
         FROM
         ClassDetails CD
         WHERE CD.numDomainID = v_numDomainID AND CD.numChildClassID IN(SELECT numListItemID FROM Listdetails WHERE numListID = 381 AND numDomainid = v_numDomainID)
         AND (CD.numParentClassID = 0 OR CD.numParentClassID IN(SELECT numListItemID FROM Listdetails WHERE numListID = 381 AND numDomainid = v_numDomainID))
         UNION
         SELECT 0,CAST(null as INTEGER),0,CAST('Root' AS VARCHAR(100))
         UNION
         SELECT  0,CAST(0 AS NUMERIC) AS numParentClassID,
							numListItemID AS numChildClassID,
							coalesce(vcData,'') AS ClassName
         FROM    Listdetails
         WHERE   numDomainid = v_numDomainID
         AND numListID = 381 AND numListItemID NOT IN(SELECT numChildClassID FROM ClassDetails WHERE numDomainID = v_numDomainID);
      end if;
   end if;
        
   IF v_tintMode = 1 then
     
      IF NOT EXISTS(SELECT * FROM ClassDetails WHERE numDomainID = v_numDomainID) then
			
         open SWV_RefCur for
         SELECT  CAST(0 AS NUMERIC) AS numParentClassID,
						numListItemID AS numChildClassID,
						coalesce(vcData,'') AS ClassName
         FROM    Listdetails
         WHERE   numDomainid = v_numDomainID
         AND numListID = 381
         ORDER BY sintOrder;
      ELSE
         CREATE TEMPORARY TABLE IF NOT EXISTS tt_TEMPGetClass
         (
            numID NUMERIC,
            numParentClassID NUMERIC,
            numChildClassID NUMERIC,
            ClassName VARCHAR(100),
            numDomainID NUMERIC
         ) ON COMMIT DROP;
		 DELETE FROM tt_TEMPGetClass;

         INSERT INTO tt_TEMPGetClass(numID,numParentClassID,numChildClassID,ClassName,numDomainID)
         SELECT X.numID,X.numParentClassID,X.numChildClassID,X.ClassName,X.numDomainID
         FROM(SELECT numID,
							numParentClassID,
							numChildClassID,
							fn_GetListItemName(numChildClassID) AS ClassName,
							numDomainID FROM ClassDetails WHERE numDomainID = v_numDomainID
            UNION
            SELECT CAST(0 AS NUMERIC),NULL,CAST(0 AS NUMERIC),CAST('Root' AS VARCHAR(100)),CAST(1 AS NUMERIC)) X;

		--			select * FROM #Temp;
					
         open SWV_RefCur for
         with recursive DirectReports(numParentClassID,numChildClassID,ClassName,ClassLevel,Sort) AS(SELECT
         numParentClassID AS numParentClassID,
							numChildClassID AS numChildClassID,
							CAST(REPEAT('--',0) || ClassName AS VARCHAR(100)) AS ClassName,
							CAST(0 AS INTEGER) AS ClassLevel,
							CAST(ClassName AS VARCHAR(255)) AS Sort
         FROM tt_TEMPGetClass
         WHERE numDomainID = 1 AND numParentClassID IS NULL
         UNION ALL
         SELECT
         t.numParentClassID AS numParentClassID,
							t.numChildClassID AS numChildClassID,
							CAST(REPEAT('--',ClassLevel+1) || t.ClassName AS VARCHAR(100)) AS ClassName,
							ClassLevel+1 AS ClassLevel ,
							CAST(RTRIM(Sort) || '|    ' || t.ClassName AS VARCHAR(255)) AS Sort
         FROM tt_TEMPGetClass t
         INNER JOIN DirectReports AS d
         ON d.numChildClassID = t.numParentClassID)
         SELECT  numChildClassID, ClassName /*, numParentClassID,ClassLevel,Sort*/
         FROM DirectReports
         WHERE numChildClassID <> 0
         ORDER BY Sort;
         
      end if;
   end if;
   RETURN;	 
END; $$;
