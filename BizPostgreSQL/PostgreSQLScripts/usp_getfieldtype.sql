-- Stored procedure definition script usp_GetFieldType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetFieldType(v_vcTableName VARCHAR(100) DEFAULT ''   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	/*select  syscolumns.NAme ,sysproperties.Value,systypes.name as DataType   from syscolumns , sysobjects  ,sysproperties,systypes where
 syscolumns.id  = sysobjects.id 
and sysproperties.id =  sysobjects.id 
and sysproperties.SmallId = syscolumns.Colid
and systypes.xtype=syscolumns.xtype
and sysobjects.Name  =  @vcTableName */
   open SWV_RefCur for SELECT
   cast(syscolumns.column_name as VARCHAR(255)),
	cast(systypes.name as VARCHAR(255)) as DataType
   FROM
   sysColumns
   INNER JOIN
   sysobjects
   ON
   syscolumns.table_name  = sysobjects.table_name
   INNER JOIN
   systypes
   ON
   systypes.xtype = sysColumns.xtype
   where
   sysobjects.routine_name  = v_vcTableName
   and systypes.xtype in(106,62,60,108,59,52,122,48);
END; $$;












