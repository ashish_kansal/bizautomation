-- Stored procedure definition script USP_GetSimilarItemWithPreCheckout for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSimilarItemWithPreCheckout(v_numDomainID NUMERIC(9,0),
	v_numParentItemCode NUMERIC(9,0),
	v_numWarehouseID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   I.numItemCode,
		I.vcItemName AS vcItemName,
		(SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) As vcPathForTImage,
		I.txtItemDesc AS txtItemDesc,
		fn_GetUOMName(I.numSaleUnit) AS vcUOMName,
		coalesce(WareHouseItems.numWareHouseItemID,0) AS numWareHouseItemID
		,CASE WHEN charItemType = 'P' THEN coalesce((SELECT  monWListPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = SI.numItemCode AND coalesce(monWListPrice,0) > 0 LIMIT 1),0) ELSE monListPrice END AS monListPrice   
		--,(CASE WHEN I.charItemType='P' THEN ISNULL(monWListPrice,0) ELSE I.monListPrice END) monListPrice
		,coalesce(vcRelationship,'') AS vcRelationship
		,coalesce(bitPreUpSell,false) AS bitPreUpSell
		,coalesce(bitPostUpSell,false) AS bitPostUpSell
		,coalesce(bitRequired,false) AS bitRequired
		,coalesce(vcUpSellDesc,'') AS vcUpSellDesc
		,C.vcCategoryName
   FROM
   SimilarItems SI
   INNER JOIN Item I ON I.numItemCode = SI.numItemCode
   LEFT JOIN ItemCategory IC ON IC.numItemID = I.numItemCode
   LEFT JOIN Category C ON C.numCategoryID = IC.numCategoryID
   LEFT JOIN LATERAL(SELECT 
      coalesce(numWareHouseItemID,0) AS numWareHouseItemID,
			monWListPrice
      FROM
      WareHouseItems
      WHERE
      WareHouseItems.numDomainID = v_numDomainID
      AND WareHouseItems.numWareHouseID = v_numWarehouseID
      AND WareHouseItems.numItemID = I.numItemCode LIMIT 1) AS WarehouseItems on TRUE
   WHERE SI.numDomainId = v_numDomainID
   AND SI.numParentItemCode = v_numParentItemCode
   AND SI.bitPreUpSell = true;
END; $$;














