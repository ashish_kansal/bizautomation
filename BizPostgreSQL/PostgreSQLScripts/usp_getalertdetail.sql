-- Stored procedure definition script USP_GetAlertDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertDetail(v_numDomainId NUMERIC(18,0), 
	v_numContactId  NUMERIC(18,0),
	v_numUserContactId  NUMERIC(18,0),
	v_numEmailHstrID  NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcAlert  TEXT DEFAULT '';
   v_bitOpenActionItem  BOOLEAN;
   v_bitOpencases  BOOLEAN;
   v_bitOpenProject  BOOLEAN;
   v_bitOpenSalesOpp  BOOLEAN;
   v_bitBalancedue  BOOLEAN;
   v_bitUnreadEmail  BOOLEAN;
   v_bitCampaign  BOOLEAN;
   v_numDivisionID  NUMERIC(18,0);
   v_numECampaignID  NUMERIC(18,0);
   v_ConECampaignID  VARCHAR(200);

	--GET ALERT CONFIGURATION
BEGIN
   select   bitOpenActionItem, bitOpenCases, bitOpenProject, bitOpenSalesOpp, bitBalancedue, bitUnreadEmail, bitCampaign INTO v_bitOpenActionItem,v_bitOpencases,v_bitOpenProject,v_bitOpenSalesOpp,
   v_bitBalancedue,v_bitUnreadEmail,v_bitCampaign FROM
   AlertConfig WHERE
   AlertConfig.numDomainID = v_numDomainId AND
   AlertConfig.numContactId = v_numUserContactId;

	--GET DIVISION
   select   numDivisionId, numECampaignID INTO v_numDivisionID,v_numECampaignID FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;

	--CHECK AR BALANCE
   IF v_bitBalancedue = true AND(SELECT SUM(TotalBalanceDue) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) > 0 then
    
      v_vcAlert := '<img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
   end if; 

	--CHECK OPEN SALES ORDER COUNT
   IF v_bitOpenSalesOpp = true AND(SELECT SUM(OpenSalesOppCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
   end if; 

	--CHECK OPEN CASES COUNT
   IF v_bitOpencases = true AND(SELECT SUM(OpenCaseCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
   end if; 

	--CHECK OPEN PROJECT COUNT
   IF v_bitOpenProject = true AND(SELECT SUM(OpenProjectCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
   end if; 

	--CHECK UNREAD MAIL COUNT
   IF v_bitUnreadEmail = true AND(SELECT SUM(UnreadEmailCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) > 0 then
    
      v_vcAlert := coalesce(v_vcAlert,'') || '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance(' || SUBSTR(CAST(v_numContactId AS VARCHAR(10)),1,10) || ');">(' || CAST((SELECT SUM(UnreadEmailCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID) AS VARCHAR(10)) || ')</a>&nbsp;';
   end if;
	
	--CHECK ACTIVE CAMPAIGN COUNT
   IF v_bitCampaign = true AND coalesce(v_numECampaignID,0) > 0 then
      select   numConEmailCampID INTO v_ConECampaignID FROM ConECampaign WHERE numECampaignID = v_numECampaignID AND numContactID = v_numContactId   ORDER BY numConEmailCampID DESC LIMIT 1;
      IF(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = v_ConECampaignID AND coalesce(bitSend,false) = false) = 0 then
		
         v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
      ELSE
         v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/Circle_Green_16.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
      end if;
   end if; 

	--CHECK COUNT OF ACTION ITEM
   IF v_bitOpenActionItem = true AND(SELECT SUM(OpenActionItemCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID and v.numContactId = v_numContactId) > 0 then
	
      v_vcAlert := coalesce(v_vcAlert,'') || '<img alt="" src="../images/MasterList-16.gif" onclick="return NewActionItem(' || SUBSTR(CAST(v_numContactId AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(v_numEmailHstrID AS VARCHAR(10)),1,10) || ')" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;';
   end if;

   open SWV_RefCur for SELECT v_vcAlert;
END; $$;















