-- Stored procedure definition script USP_InsertJournalEntryHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryHeader(INOUT v_numJournal_Id NUMERIC(9,0) DEFAULT 0 ,
	v_numRecurringId NUMERIC(9,0) DEFAULT 0,
    v_datEntry_Date TIMESTAMP DEFAULT NULL,
    v_vcDescription VARCHAR(1000) DEFAULT NULL,
    v_numAmount DECIMAL(20,5) DEFAULT NULL,
    v_numCheckId NUMERIC DEFAULT NULL,
    v_numCashCreditCardId NUMERIC DEFAULT NULL,
    v_numChartAcntId NUMERIC DEFAULT NULL,
    v_numOppId NUMERIC DEFAULT NULL,
    v_numOppBizDocsId NUMERIC DEFAULT NULL,
    v_numDepositId NUMERIC DEFAULT NULL,
    v_numBizDocsPaymentDetId NUMERIC DEFAULT NULL,
    v_bitOpeningBalance BOOLEAN DEFAULT NULL,
    v_numCategoryHDRID NUMERIC DEFAULT NULL,
    v_numReturnID NUMERIC DEFAULT NULL,
    v_numCheckHeaderID NUMERIC(18,0) DEFAULT NULL,
	v_numBillID NUMERIC(18,0) DEFAULT NULL,
	v_numBillPaymentID NUMERIC(18,0) DEFAULT NULL,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_numReconcileID NUMERIC(9,0) DEFAULT NULL,
    v_numJournalReferenceNo NUMERIC(9,0) DEFAULT 0,
    v_numPayrollDetailID NUMERIC DEFAULT NULL,
	v_bitReconcileInterest BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql    
	--Validation of closed financial year
   AS $$
   DECLARE
   v_numEntryDateSortOrder  NUMERIC;
   v_numAccountClass  NUMERIC(18,0);

   v_tintDefaultClassType  INTEGER DEFAULT 0;
BEGIN
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainId,v_datEntry_Date);
	
   IF v_numRecurringId = 0 then 
      v_numRecurringId := NULL;
   end if;  
--    IF @numProjectID = 0 SET @numProjectID = NULL
   IF v_numCheckId = 0 then 
      v_numCheckId := NULL;
   end if;
   IF v_numCashCreditCardId = 0 then 
      v_numCashCreditCardId := NULL;
   end if;
   IF v_numChartAcntId = 0 then 
      v_numChartAcntId := NULL;
   end if;
   IF v_numOppId = 0 then 
      v_numOppId := NULL;
   end if;
   IF v_numOppBizDocsId = 0 then 
      v_numOppBizDocsId := NULL;
   end if;
   IF v_numDepositId = 0 then 
      v_numDepositId := NULL;
   end if;
   IF v_numBizDocsPaymentDetId = 0 then 
      v_numBizDocsPaymentDetId := NULL;
   end if;
   IF v_bitOpeningBalance = false then 
      v_bitOpeningBalance := NULL;
   end if;
   IF v_numCategoryHDRID = 0 then 
      v_numCategoryHDRID := NULL;
   end if;
--    IF @numClassID = 0 SET @numClassID = NULL
   IF v_numReturnID = 0 then 
      v_numReturnID := NULL;
   end if;
   IF v_numReconcileID = 0 then 
      v_numReconcileID := NULL;
   end if;
   IF v_numJournalReferenceNo = 0 then 
      v_numJournalReferenceNo := NULL;
   end if;
   IF v_numPayrollDetailID = 0 then 
      v_numPayrollDetailID := NULL;
   end if;
    
   v_numEntryDateSortOrder := CAST(SUBSTR('00000' || CAST(EXTRACT(YEAR FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(YEAR FROM v_datEntry_Date) AS VARCHAR(30))) -4+1) ||
   SUBSTR('00000' || CAST(EXTRACT(MONTH FROM v_datEntry_Date) AS VARCHAR(30)),
   length('00000' || CAST(EXTRACT(MONTH FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
   SUBSTR('00000' || CAST(EXTRACT(DAY FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(DAY FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
   SUBSTR('00000' || CAST(EXTRACT(HOUR FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(HOUR FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
   SUBSTR('00000' || CAST(EXTRACT(MINUTE FROM v_datEntry_Date) AS VARCHAR(30)),
   length('00000' || CAST(EXTRACT(MINUTE FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
   SUBSTR('00000' || CAST(EXTRACT(SECOND FROM v_datEntry_Date) AS VARCHAR(30)),
   length('00000' || CAST(EXTRACT(SECOND FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
   SUBSTR('00000' || CAST(CAST(TO_CHAR(v_datEntry_Date,'MS') AS BIGINT) AS VARCHAR(30)),length('00000' || CAST(CAST(TO_CHAR(v_datEntry_Date,'MS') AS BIGINT) AS VARCHAR(30))) -3+1) AS NUMERIC);

   IF v_numJournal_Id = 0 then
        
           --Set Default Class If enable User Level Class Accountng 
      v_numAccountClass := 0;
      IF coalesce(v_numDepositId,0) > 0 then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM DepositMaster WHERE numDomainId = v_numDomainId AND numDepositId = v_numDepositId;
      ELSEIF coalesce(v_numBillID,0) > 0
      then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM BillHeader WHERE numDomainId = v_numDomainId AND numBillID = v_numBillID;
      ELSEIF coalesce(v_numBillPaymentID,0) > 0
      then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM BillPaymentHeader WHERE numDomainId = v_numDomainId AND numBillPaymentID = v_numBillPaymentID;
      ELSEIF coalesce(v_numCheckHeaderID,0) > 0
      then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM CheckHeader WHERE numDomainID = v_numDomainId AND numCheckHeaderID = v_numCheckHeaderID;
      ELSEIF coalesce(v_numReturnID,0) > 0
      then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM ReturnHeader WHERE numDomainID = v_numDomainId AND numReturnHeaderID = v_numReturnID;
      ELSEIF coalesce(v_numOppId,0) > 0
      then
		  
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM OpportunityMaster WHERE numDomainId = v_numDomainId AND numOppId = v_numOppId;
      ELSE
		  	  -- GET ACCOUNT CLASS IF ENABLED
			
         select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_tintDefaultClassType = 1 then --USER
			
            select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
            UserMaster UM
            JOIN
            Domain D
            ON
            UM.numDomainID = D.numDomainId WHERE
            D.numDomainId = v_numDomainId
            AND UM.numUserDetailId = v_numUserCntID;
         ELSE
            v_numAccountClass := 0;
         end if;
      end if;
      INSERT INTO General_Journal_Header(numRecurringId,
				datEntry_Date,
				varDescription,
				numAmount,
				numDomainId,
				numCheckId,
				numCashCreditCardId,
				numChartAcntId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numBizDocsPaymentDetId,
				bitOpeningBalance,
				dtLastRecurringDate,
				numNoTransactions,
				numCreatedBy,
				datCreatedDate,
				numCategoryHDRID,
				numModifiedBy,
				dtModifiedDate,
				numReturnID,
				numCheckHeaderID,
				numBillID,
				numBillPaymentID,numReconcileID,numJournalReferenceNo,numPayrollDetailID,
				numEntryDateSortOrder,numAccountClass,bitReconcileInterest) VALUES(v_numRecurringId,
				 CAST(v_datEntry_Date AS TIMESTAMP),
				 v_vcDescription,
				 v_numAmount,
				 v_numDomainId,
				 v_numCheckId,
				 v_numCashCreditCardId,
				 v_numChartAcntId,
				 v_numOppId,
				 v_numOppBizDocsId,
				 v_numDepositId,
				 v_numBizDocsPaymentDetId,
				 v_bitOpeningBalance,
				 null,
				 null,
				 v_numUserCntID,
				 TIMEZONE('UTC',now()),
				 v_numCategoryHDRID,
				 v_numUserCntID,
				 TIMEZONE('UTC',now()),
				 v_numReturnID,
				 v_numCheckHeaderID,
				 v_numBillID,
				 v_numBillPaymentID,v_numReconcileID,v_numJournalReferenceNo,v_numPayrollDetailID,
				 v_numEntryDateSortOrder,v_numAccountClass,v_bitReconcileInterest) RETURNING numJournal_Id INTO v_numJournal_Id;
                  
      open SWV_RefCur for
      SELECT  v_numJournal_Id;
   ELSE
      UPDATE  General_Journal_Header
      SET     numRecurringId = v_numRecurringId,datEntry_Date = CAST(v_datEntry_Date AS TIMESTAMP),
      varDescription = v_vcDescription,numAmount = v_numAmount,
      numDomainId = v_numDomainId,numCheckId = v_numCheckId,numCashCreditCardId = v_numCashCreditCardId,
      numOppId = v_numOppId,numOppBizDocsId = v_numOppBizDocsId,
      numDepositId = v_numDepositId,numBizDocsPaymentDetId = v_numBizDocsPaymentDetId,
      bitOpeningBalance = v_bitOpeningBalance,numCategoryHDRID = v_numCategoryHDRID,
      numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
      numReturnID = v_numReturnID,numCheckHeaderID = v_numCheckHeaderID,
      numBillID = v_numBillID,numBillPaymentID = v_numBillPaymentID,
      numReconcileID = v_numReconcileID,numJournalReferenceNo = v_numJournalReferenceNo,
      numPayrollDetailID = v_numPayrollDetailID,
      bitReconcileInterest = v_bitReconcileInterest,numEntryDateSortOrder = v_numEntryDateSortOrder
      WHERE   numJOurnal_Id = v_numJournal_Id
      AND numDomainId = v_numDomainId;
      open SWV_RefCur for
      SELECT  v_numJournal_Id;
   end if;

   UPDATE
   General_Journal_Header
   SET
   numEntryDateSortOrder = coalesce((SELECT MAX(numEntryDateSortOrder) FROM General_Journal_Header WHERE numEntryDateSortOrder = v_numEntryDateSortOrder),v_numEntryDateSortOrder)+1
   WHERE
   numJOurnal_Id = v_numJournal_Id;
   RETURN;
END; $$;


