-- Stored procedure definition script USP_SaveEmailTemplates for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveEmailTemplates(v_numDomainID NUMERIC(9,0) DEFAULT null,          
	 v_vcDocDesc TEXT DEFAULT '',          
	 v_VcFileName VARCHAR(100) DEFAULT '',          
	 v_vcDocName VARCHAR(100) DEFAULT '',          
	 v_numDocCategory NUMERIC(9,0) DEFAULT null,          
	 v_vcfiletype VARCHAR(10) DEFAULT '',          
	 v_numDocStatus NUMERIC(9,0) DEFAULT null,          
	 v_numUserCntID NUMERIC(9,0) DEFAULT null,          
	 INOUT v_numGenDocId NUMERIC(9,0) DEFAULT 0 ,          
	 v_cUrlType VARCHAR(1) DEFAULT '',        
	 v_vcSubject VARCHAR(500) DEFAULT '' ,  
	 v_vcDocumentSection VARCHAR(10) DEFAULT NULL,
	 v_numRecID NUMERIC(9,0) DEFAULT 0,
	 v_tintDocumentType SMALLINT DEFAULT 1,-- 1 = generic, 2= specific document
	 v_numModuleID NUMERIC DEFAULT 0,
	 v_vcContactPosition VARCHAR(200) DEFAULT '',
	 v_BizDocOppType VARCHAR(10) DEFAULT NULL,
	 v_BizDocType VARCHAR(10) DEFAULT NULL,
	 v_BizDocTemplate VARCHAR(10) DEFAULT NULL,
	 v_numCategoryId NUMERIC(18,0) DEFAULT NULL,
	 v_bitLastFollowupUpdate BOOLEAN DEFAULT NULL,
	 v_vcGroupsPermission VARCHAR(500) DEFAULT NULL,
	 v_numFollowUpStatusId NUMERIC(18,0) DEFAULT NULL,
	 v_bitUpdateFollowupStatus BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numGenDocId = 0 then

      IF v_numModuleID = 0 then 
         v_numModuleID := null;
      end if;
      INSERT INTO GenericDocuments(numDomainId,
			 vcDocdesc,
			 VcFileName,
			 VcDocName,
			 numDocCategory,
			 vcfiletype,
			 numDocStatus,
			 numCreatedBy,
			 bintCreatedDate,
			 numModifiedBy,
			 bintModifiedDate,
			 cUrlType,
			 vcSubject,
			 vcDocumentSection,
			 numRecID,
		 tintDocumentType,
		 numModuleId,vcContactPosition,
		 BizDocOppType,
		 BizDocType,
		 BizDocTemplate,
		 numCategoryId,
		 bitLastFollowupUpdate,
		 vcGroupsPermission,
		 numFollowUpStatusId,
		 bitUpdateFollowupStatus)
	VALUES(v_numDomainID,
		 v_vcDocDesc,
		 v_VcFileName,
		 v_vcDocName,
		 v_numDocCategory,
		 v_vcfiletype,
		 v_numDocStatus,
		 v_numUserCntID,
		 TIMEZONE('UTC',now()),
		 v_numUserCntID,
		 TIMEZONE('UTC',now()),
		 v_cUrlType,
		 v_vcSubject,
		 v_vcDocumentSection,
		 v_numRecID,
		 v_tintDocumentType,
		 v_numModuleID,v_vcContactPosition,
		 v_BizDocOppType,
		 v_BizDocType,
		 v_BizDocTemplate,
		 v_numCategoryId,
		 v_bitLastFollowupUpdate,
		 v_vcGroupsPermission,
		 v_numFollowUpStatusId,
		 v_bitUpdateFollowupStatus) RETURNING numGenericDocID INTO v_numGenDocId;

   ELSE
      IF v_numModuleID = 0 then  
         v_numModuleID := NULL;
      end if;
      UPDATE  GenericDocuments
      SET
      vcDocdesc = v_vcDocDesc,VcFileName = v_VcFileName,VcDocName = v_vcDocName,numDocCategory = v_numDocCategory,
      vcfiletype = v_vcfiletype,numDocStatus = v_numDocStatus,
      numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      cUrlType = v_cUrlType,vcSubject = v_vcSubject,vcDocumentSection = v_vcDocumentSection,
      numModuleId = v_numModuleID,vcContactPosition = v_vcContactPosition,
      BizDocOppType = v_BizDocOppType,BizDocType = v_BizDocType,
      BizDocTemplate = v_BizDocTemplate,numCategoryId = v_numCategoryId,
      bitLastFollowupUpdate = v_bitLastFollowupUpdate,vcGroupsPermission = v_vcGroupsPermission,
      numFollowUpStatusId = v_numFollowUpStatusId,bitUpdateFollowupStatus = v_bitUpdateFollowupStatus
      WHERE numGenericDocID = v_numGenDocId;
   end if;
   RETURN;
END; $$;



