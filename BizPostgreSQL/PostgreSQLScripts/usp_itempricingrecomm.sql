DROP FUNCTION IF EXISTS USP_ItemPricingRecomm;

CREATE OR REPLACE FUNCTION USP_ItemPricingRecomm
(
	p_numItemCode NUMERIC(18,0),                                        
	p_units NUMERIC,                                  
	p_numOppID NUMERIC(18,0),                                
	p_numDivisionID NUMERIC(18,0),                  
	p_numDomainID NUMERIC(18,0),            
	p_tintOppType SMALLINT ,          
	p_CalPrice DECIMAL(20,5),      
	p_numWareHouseItemID NUMERIC(18,0),
	p_bitMode SMALLINT DEFAULT 0,
	p_vcSelectedKitChildItems TEXT DEFAULT '',
	p_numOppItemID NUMERIC(18,0) DEFAULT 0,
	p_numSiteID NUMERIC(18,0) DEFAULT 0
	,INOUT SWV_RefCur refcursor DEFAULT NULL
	,INOUT SWV_RefCur2 refcursor DEFAULT NULL
	,INOUT SWV_RefCur3 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
   DECLARE 
	v_numRelationship NUMERIC(9);
	v_numProfile NUMERIC(9);             
	v_monListPrice DECIMAL(20,5);
	v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
	v_bitCalAmtBasedonDepItems BOOLEAN;
	v_numDefaultSalesPricing SMALLINT;
	v_tintPriceLevel INT;
	v_decmUOMConversion decimal(18,2);
	v_intLeadTimeDays INT;
	v_intMinQty INT;
	v_tintKitAssemblyPriceBasedOn SMALLINT;
	v_newPrice DECIMAL(20,5);
	v_finalUnitPrice DOUBLE PRECISION DEFAULT 0;
	v_tintRuleType INT;
	v_tintVendorCostType SMALLINT;
	v_tintDiscountType INT;
	v_decDiscount DOUBLE PRECISION;
	v_ItemPrice DOUBLE PRECISION;
	v_tintPreLoginPriceLevel SMALLINT;
BEGIN
	/*Profile and relationship id */
	SELECT 
		numCompanyType,
		vcProfile,
		COALESCE(D.tintPriceLevel,0)
	INTO 
		v_numRelationship
		,v_numProfile
		,v_tintPriceLevel
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =p_numDivisionID;

	--KEEP THIS BELOW ABOVE IF CONDITION 
	IF COALESCE(p_numSiteId,0) > 0 THEN
		SELECT 
			(CASE WHEN COALESCE(bitShowPriceUsingPriceLevel,false)=true THEN (CASE WHEN p_numDivisionID > 0 THEN (CASE WHEN COALESCE(bitShowPriceUsingPriceLevel,false)=true THEN COALESCE(v_tintPriceLevel,0) ELSE 0 END) ELSE COALESCE(tintPreLoginProceLevel,0) END) ELSE 0 END) INTO v_tintPriceLevel
		FROM 
			eCommerceDTL
		WHERE 
			numSiteID = p_numSiteID;
	END IF;
            
	IF p_tintOppType=1 THEN         
		SELECT
			COALESCE(bitCalAmtBasedonDepItems,false)
			,COALESCE(tintKitAssemblyPriceBasedOn,1)
		INTO
			v_bitCalAmtBasedonDepItems
			,v_tintKitAssemblyPriceBasedOn
		FROM 
			Item 
		WHERE 
			numItemCode = p_numItemCode;
      
		/*Get List Price for item*/      
		IF((p_numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=p_numItemCode AND charItemType='P')) THEN 
			SELECT 
				COALESCE(monWListPrice,0) INTO v_monListPrice
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=p_numWareHouseItemID;
		
			IF v_monListPrice=0 THEN
				SELECT monListPrice INTO v_monListPrice FROM Item WHERE numItemCode=p_numItemCode;
			END IF;
		ELSE
			 SELECT 
				monListPrice INTO v_monListPrice
			FROM 
				Item 
			WHERE 
				numItemCode=p_numItemCode;     
		END  IF;

		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF v_bitCalAmtBasedonDepItems = true THEN
			DROP TABLE IF EXISTS tt_TEMPPrice CASCADE;
			CREATE TEMPORARY TABLE tt_TEMPPrice 
			( 
				bitSuccess BOOLEAN
				,monPrice DECIMAL(20,5)
			);

			INSERT INTO tt_TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monMSRPPrice
			FROM
				fn_GetKitAssemblyCalculatedPrice(p_numDomainID,p_numDivisionID,p_numItemCode,p_units,p_numWareHouseItemID,v_tintKitAssemblyPriceBasedOn,p_numOppID,p_numOppItemID,p_vcSelectedKitChildItems,0,1);

			IF (SELECT bitSuccess FROM tt_TEMPPrice) = true THEN
				p_CalPrice := (SELECT monPrice FROM tt_TEMPPrice);
			ELSE
				RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
				RETURN;
			END IF;
		END IF;

		SELECT 
			 COALESCE(numDefaultSalesPricing,2) INTO v_numDefaultSalesPricing
		FROM 
			Domain 
		WHERE 
			numDomainID = p_numDomainID;

		IF v_numDefaultSalesPricing = 1 THEN -- Use Price Level
			v_tintRuleType := 0;
			v_tintVendorCostType := 1;
			v_tintDiscountType := 0;
			v_decDiscount := 0;
			v_ItemPrice := 0;

			IF COALESCE(v_tintPriceLevel,0) > 0 THEN
				SELECT
					 tintRuleType,
					 tintVendorCostType,
					 tintDiscountType,
					 decDiscount
				INTO
					v_tintRuleType
					,v_tintVendorCostType
					,v_tintDiscountType
					,v_decDiscount
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						tintRuleType,
						tintVendorCostType,
						tintDiscountType,
						decDiscount
					FROM 
						PricingTable 
					WHERE 
						PricingTable.numItemCode = p_numItemCode
						AND COALESCE(numCurrencyID,0) = 0
				) TEMP
				WHERE
					Id = v_tintPriceLevel;
			ELSE
				SELECT
					 tintRuleType,
					 tintVendorCostType,
					 tintDiscountType,
					 decDiscount
				INTO
					v_tintRuleType
					,v_tintVendorCostType
					,v_tintDiscountType
					,v_decDiscount
				FROM 
					PricingTable 
				WHERE 
					numItemCode = p_numItemCode 
					AND COALESCE(numCurrencyID,0) = 0
					AND (p_units BETWEEN intFromQty AND intToQty)
				LIMIT 1;
			END IF;

			IF v_tintRuleType > 0 AND (v_tintDiscountType > 0 OR v_tintRuleType = 3) THEN
				IF v_tintRuleType = 1 THEN -- Deduct from List price
					IF (SELECT COALESCE(charItemType,'') FROM Item WHERE numDomainID = p_numDomainID AND numItemCode = p_numItemCode) = 'P' THEN
						If v_bitCalAmtBasedonDepItems = true THEN
							v_ItemPrice := p_CalPrice;
						ELSE
							SELECT  COALESCE(monWListPrice,0) INTO v_ItemPrice FROM WareHouseItems WHERE numDomainID = p_numDomainID AND numItemID = p_numItemCode AND numWareHouseItemID = p_numWareHouseItemID;
						END IF;
					ELSE
						SELECT v_ItemPrice = COALESCE(monListPrice,0) FROM Item WHERE numDomainID = p_numDomainID AND numItemCode = p_numItemCode;
					END IF;

					IF v_tintDiscountType = 1 AND v_decDiscount > 0 THEN -- Percentage
						v_finalUnitPrice := v_ItemPrice - (v_ItemPrice * (v_decDiscount/100));
					ELSEIF v_tintDiscountType = 2 THEN -- Flat Amount
						v_finalUnitPrice := v_ItemPrice - v_decDiscount;
					ELSEIF v_tintDiscountType = 3 THEN -- Named Price
						v_finalUnitPrice := v_decDiscount;
					END IF;
				ELSEIF v_tintRuleType = 2 THEN -- Add to Primary Vendor Cost
					If v_bitCalAmtBasedonDepItems = true THEN
						v_ItemPrice := p_CalPrice;
					ELSE
						v_ItemPrice := COALESCE(monCost,0) FROM Vendor WHERE numItemCode = p_numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = p_numItemCode);
					END IF;

					IF EXISTS (SELECT
									VendorCostTable.numvendorcosttableid
								FROM 
									Item
								INNER JOIN
									Vendor
								ON
									Item.numVendorID = Vendor.numVendorID
								INNER JOIN
									VendorCostTable
								ON
									Vendor.numVendorTCode = VendorCostTable.numVendorTCode
								WHERE
									Item.numItemCode = p_numItemCode
									AND VendorCostTable.numCurrencyID = (SELECT COALESCE(numCurrencyID,0) FROM Domain WHERE numDomainId = p_numDomainID)
									AND p_units BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,p_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,p_numDomainID,numBaseUnit)) THEN
						SELECT
							COALESCE(VendorCostTable.monDynamicCost,v_ItemPrice)
							,COALESCE(VendorCostTable.monStaticCost,v_ItemPrice)
						INTO 
							v_monVendorDynamicCost
							,v_monVendorStaticCost
						FROM 
							Item
						INNER JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID
						INNER JOIN
							VendorCostTable
						ON
							Vendor.numVendorTCode = VendorCostTable.numVendorTCode
						WHERE
							Item.numItemCode = p_numItemCode
							AND VendorCostTable.numCurrencyID = (SELECT COALESCE(numCurrencyID,0) FROM Domain WHERE numDomainId = p_numDomainID)
							AND p_units BETWEEN VendorCostTable.intFromQty * fn_UOMConversion(numPurchaseUnit,numItemCode,p_numDomainID,numBaseUnit) AND VendorCostTable.intToQty * fn_UOMConversion(numPurchaseUnit,numItemCode,p_numDomainID,numBaseUnit);
					ELSE
						v_monVendorDynamicCost := v_ItemPrice;
						v_monVendorStaticCost := v_ItemPrice;
					END IF;

					v_ItemPrice := (CASE v_tintVendorCostType WHEN 1 THEN v_monVendorDynamicCost WHEN 2 THEN v_monVendorStaticCost ELSE v_ItemPrice END);


					IF v_tintDiscountType = 1 AND v_ItemPrice > 0 THEN -- Percentage
						IF v_decDiscount > 0 THEN
							v_finalUnitPrice := v_ItemPrice + (v_ItemPrice * (v_decDiscount/100));
						END IF;
					ELSEIF v_tintDiscountType = 2 AND v_ItemPrice > 0 THEN -- Flat Amount
						v_finalUnitPrice := v_ItemPrice + v_decDiscount;
					ELSEIF v_tintDiscountType = 3 THEN -- Named Price
						v_finalUnitPrice := v_decDiscount;
					END IF;
				ELSEIF v_tintRuleType = 3 THEN -- Named Price
					IF COALESCE(v_decDiscount,0) = 0 THEN
						v_finalUnitPrice := v_monListPrice;
					ELSE
						v_finalUnitPrice := v_decDiscount;
						v_monListPrice := v_finalUnitPrice;
					END IF;
				
					-- KEEP THIS LINE AT END
					v_decDiscount := 0;
					v_tintDiscountType := 2;	
				END IF;
			END IF;

			IF v_finalUnitPrice = 0 THEN
				v_newPrice := v_monListPrice;
			ELSE
				v_newPrice := v_finalUnitPrice;
			END IF;


			If v_tintRuleType = 2 THEN
				IF v_finalUnitPrice > 0 THEN
					IF v_tintDiscountType = 1 THEN -- Percentage
						IF v_newPrice > v_monListPrice THEN
							v_decDiscount := 0;
						ELSE
							If v_monListPrice > 0 THEN
								v_decDiscount := ((v_monListPrice - v_newPrice) * 100) / CAST(v_monListPrice AS FLOAT);
							ELSE
								v_decDiscount := 0;
							END IF;
						END IF;
					ELSEIF v_tintDiscountType = 2 THEN -- Flat Amount
						If v_newPrice > v_monListPrice THEN
							v_decDiscount := 0;
						ELSE
							v_decDiscount := v_monListPrice - v_newPrice;
						END IF;
					END IF;
				ELSE
					v_decDiscount := 0;
					v_tintDiscountType := 0;
				END IF;
			END IF;

			OPEN SWV_RefCur FOR
			SELECT 
				1 as "numUintHour", 
				COALESCE(v_newPrice,0) AS "ListPrice",
				(CASE WHEN v_bitCalAmtBasedonDepItems = true THEN p_CalPrice ELSE v_monListPrice END) "monItemListPrice",
				'' AS "vcPOppName",
				NULL::TIMESTAMP AS "bintCreatedDate",
				CASE 
					WHEN COALESCE(v_tintRuleType,0) = 0 THEN 'Price - List price'
					ELSE 
					   CASE v_tintRuleType
					   WHEN 1 
							THEN 
								'Deduct from List price ' ||  CASE 
															   WHEN v_tintDiscountType = 1 THEN CAST(v_decDiscount AS VARCHAR(20)) || '%'
															   WHEN v_tintDiscountType = 2 THEN 'flat ' || CAST(CAST(v_decDiscount AS int) AS VARCHAR(20))
															   ELSE 'Name price ' || CAST(CAST(v_decDiscount AS int) AS VARCHAR(20))
															   END 
					   WHEN 2 THEN 'Add to primary vendor cost '  ||  CASE 
															   WHEN v_tintDiscountType = 1 THEN CAST(v_decDiscount AS VARCHAR(20)) || '%'
															   WHEN v_tintDiscountType = 2 THEN 'flat ' || CAST(CAST(v_decDiscount AS int) AS VARCHAR(20))
															   ELSE 'Name price ' || CAST(CAST(v_decDiscount AS int) AS VARCHAR(20))
															   END 
					   ELSE 'Flat ' || CAST(v_decDiscount AS VARCHAR(20))
					   END
				END AS "OppStatus",
				CAST(0 AS NUMERIC) AS "numPricRuleID",
				CAST(1 AS SMALLINT) AS "tintPricingMethod",
				COALESCE(v_tintDiscountType,0)::SMALLINT AS "tintDiscountTypeOriginal",
				COALESCE(v_decDiscount,0) AS "decDiscountOriginal",
				COALESCE(v_tintRuleType,0)::SMALLINT AS "tintRuleType";
		ELSE -- Use Price Rule
			/* Checks Pricebook if exist any.. */      
			OPEN SWV_RefCur FOR
			SELECT 
					1 AS "numUnitHour",
					GetPriceBasedOnPriceBook(P.numPricRuleID,CASE WHEN v_bitCalAmtBasedonDepItems = true THEN p_CalPrice ELSE v_monListPrice END,p_units,I.numItemCode) 
					* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND COALESCE(PBT.tintRuleType,0) = 2)) THEN fn_UOMConversion(numBaseUnit,p_numItemCode,p_numDomainID,CASE WHEN COALESCE(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS "ListPrice",
					(CASE WHEN v_bitCalAmtBasedonDepItems = true THEN p_CalPrice ELSE v_monListPrice END) "monItemListPrice",
					vcRuleName AS "vcPOppName",
					NULL::TIMESTAMP AS "bintCreatedDate",
					CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
						 ELSE 
						  CASE WHEN P.tintPricingMethod = 1
											 THEN 'Price Book Rule'
											 ELSE CASE PBT.tintRuleType
													WHEN 1 THEN 'Deduct from List price '
													WHEN 2 THEN 'Add to primary vendor cost '
													ELSE ''
												  END
												  || CASE 
														WHEN tintDiscountType = 1 THEN CAST(decDiscount AS VARCHAR(20)) || '%'
														WHEN tintDiscountType = 2 THEN 'flat ' || CAST(CAST(decDiscount AS int) AS VARCHAR(20))
														ELSE 'Named price ' || CAST(CAST(decDiscount AS int) AS VARCHAR(20))
													END || ' for every '
												  || CAST(CASE WHEN intQntyItems = 0 THEN 1
																			  ELSE intQntyItems
																		 END  AS VARCHAR(20))
												  || ' units, until maximum of '
												  || CAST(decMaxDedPerAmt AS VARCHAR(20))
												  || CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
					END AS "OppStatus",P.numPricRuleID AS "numPricRuleID",P.tintPricingMethod AS "tintPricingMethod",PBT.tintDiscountTypeOriginal AS "tintDiscountTypeOriginal",CAST(PBT.decDiscountOriginal AS FLOAT) AS "decDiscountOriginal",PBT.tintRuleType AS "tintRuleType"
			FROM    Item I
					LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
					LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
					LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
					LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
					INNER JOIN LATERAL GetPriceBasedOnPriceBookTable(P.numPricRuleID,CASE WHEN bitCalAmtBasedonDepItems=true then p_CalPrice ELSE v_monListPrice END::DECIMAL,p_units::INTEGER,I.numItemCode) as PBT ON TRUE
			WHERE   numItemCode = p_numItemCode AND tintRuleFor=p_tintOppType
			AND 
			(
			((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
			ORDER BY PP.Priority ASC LIMIT 1;
		END IF;

		IF p_bitMode=2 THEN -- When this procedure is being called from USP_GetPriceOfItem for e-com
			RETURN;
		END IF;

        OPEN SWV_RefCur2 FOR                
		select 1 as "numUnitHour",CAST(COALESCE((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0) AS DECIMAL(20,5)) as "ListPrice",vcPOppName AS "vcPOppName",bintCreatedDate as "bintCreatedDate",case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
		when tintOppStatus=2 then 'Deal Lost' end as "OppStatus",CAST(0 AS NUMERIC) AS "numPricRuleID",CAST(0 AS SMALLINT) AS "tintPricingMethod",CAST(0 AS SMALLINT) as "tintDiscountTypeOriginal",CAST(0 AS FLOAT) as "decDiscountOriginal",CAST(0 AS SMALLINT) as "tintRuleType"                        
		from OpportunityItems itm                                        
		join OpportunityMaster mst                                        
		on mst.numOppId=itm.numOppId              
		where  tintOppType=1 and numItemCode=p_numItemCode and mst.numDivisionID=p_numDivisionID order by (case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won' when tintOppStatus=2 then 'Deal Lost' end) desc;
  
		OPEN SWV_RefCur3 FOR
		select 1 as "numUnitHour"
		,CASE WHEN v_bitCalAmtBasedonDepItems=true THEN CAST(COALESCE(p_CalPrice,0) AS DECIMAL(20,5)) ELSE CAST(COALESCE(v_monListPrice,0) AS DECIMAL(20,5)) End as "ListPrice"
		,'' as "vcPOppName",null::TIMESTAMP as "bintCreatedDate",CASE WHEN v_bitCalAmtBasedonDepItems=true THEN 'Sum of dependent items' ELSE '' END as "OppStatus"
		,CAST(0 AS NUMERIC) AS "numPricRuleID",CAST(0 AS SMALLINT) AS "tintPricingMethod" ,CAST(0 AS SMALLINT) as "tintDiscountTypeOriginal",CAST(0 AS FLOAT) as "decDiscountOriginal",CAST(0 AS SMALLINT) as "tintRuleType";                             
	ELSE            
		If p_numDivisionID=0 THEN
			SELECT 
				V.numVendorID INTO p_numDivisionID
			FROM 
				Vendor V 
			INNER JOIN 
				Item I 
			ON 
				V.numVendorID = I.numVendorID 
				AND V.numItemCode=I.numItemCode 
			WHERE 
				V.numItemCode=p_numItemCode 
				AND V.numDomainID=p_numDomainID;
		END IF;

		IF EXISTS
		(
			SELECT 
				COALESCE(monCost,0) ListPrice 
			FROM 
				Vendor V 
			INNER JOIN 
				Item I 
			ON 
				V.numItemCode = I.numItemCode
			WHERE
				V.numItemCode=p_numItemCode 
				AND V.numDomainID=p_numDomainID 
				AND V.numVendorID=p_numDivisionID
		) THEN          
			SELECT 
				COALESCE(monCost,0) INTO v_monListPrice
			FROM 
				Vendor V 
			INNER JOIN 
				Item I 
			ON 
				V.numItemCode = I.numItemCode
			WHERE 
				V.numItemCode=p_numItemCode 
				AND V.numDomainID=p_numDomainID 
				AND V.numVendorID=p_numDivisionID;    
		ELSE  
			SELECT 
				COALESCE(monCost,0) INTO v_monListPrice
			FROM 
				Vendor 
			WHERE 
				numItemCode=p_numItemCode 
				AND numDomainID=p_numDomainID;
		END IF;   
 
		OPEN SWV_RefCur FOR
		SELECT 
			1 AS "numUnitHour",
			COALESCE(GetPriceBasedOnPriceBook(P.numPricRuleID,v_monListPrice,p_units,I.numItemCode),0) AS "ListPrice",
			vcRuleName AS "vcPOppName",
			NULL::TIMESTAMP AS "bintCreatedDate",
			CASE 
			WHEN numPricRuleID IS NULL THEN 'Price - List price'
			ELSE  
					CASE 
					WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
					ELSE 
						(CASE tintRuleType
							WHEN 1 THEN 'Deduct from List price '
							WHEN 2 THEN 'Add to primary vendor cost '
							ELSE ''
						END) || 
						(CASE 
							WHEN tintDiscountType = 1 THEN CAST(decDiscount AS VARCHAR(20)) || '%'
							ELSE 'flat ' || CAST(CAST(decDiscount AS int) AS VARCHAR(20))
						END) || ' for every ' || 
						CAST((CASE WHEN intQntyItems = 0 THEN 1 ELSE intQntyItems END) AS VARCHAR(20))
						|| ' units, until maximum of '
						|| CAST(decMaxDedPerAmt  AS VARCHAR(20))
						|| (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
					END
			END AS "OppStatus",
			P.numPricRuleID AS "numPricRuleID",
			P.tintPricingMethod AS "tintPricingMethod",
			p_numDivisionID AS "numDivisionID"
		FROM
			Item I 
		LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
		WHERE   
			numItemCode = p_numItemCode 
			AND tintRuleFor=p_tintOppType
			AND (
				((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 1
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 2
				OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = p_numDivisionID)) -- Priority 3

				OR ((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
				OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

				OR ((tintStep2 = 1 AND PBI.numValue = p_numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY PP.Priority ASC LIMIT 1;

		v_decmUOMConversion := (SELECT 
									fn_UOMConversion(COALESCE(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, COALESCE(I.numBaseUnit, 0)) 
								FROM 
									Item I 
								WHERE 
									I.numItemCode=p_numItemCode);

		SELECT
			COALESCE(VSM.numListValue,0),
			COALESCE(intMinQty,0)
		INTO 
			v_intLeadTimeDays
			,v_intMinQty
		FROM
			Item
		JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode=Vendor.numItemCode
		LEFT JOIN
			VendorShipmentMethod VSM
		ON
			Vendor.numVendorID = VSM.numVendorID
			AND VSM.numDomainID=p_numDomainID
			AND VSM.bitPrimary = true
			AND VSM.bitPreferredMethod = true
		WHERE
			Item.numItemCode = p_numItemCode;

		OPEN SWV_RefCur2 FOR
			select 1 as "numUnitHour"
			,CAST(COALESCE((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0) AS DECIMAL(20,5)) as "ListPrice"
			,v_decmUOMConversion * CAST(COALESCE((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0) AS DECIMAL(20,5)) as "convrsPrice"
			,vcPOppName AS "vcPOppName"
			,bintCreatedDate as "bintCreatedDate"
			,case when tintOppStatus=0 then 'Opportunity' 
			 when tintOppStatus=1 then 'Deal won'  
			 when tintOppStatus=2 then 'Deal Lost' end as "OppStatus"
			 ,CAST(0 AS NUMERIC) AS "numPricRuleID"
			 ,CAST(0 AS SMALLINT) AS "tintPricingMethod"
			 ,p_numDivisionID as "numDivisionID"                         
			 from OpportunityItems itm                                        
			 join OpportunityMaster mst                                        
			 on mst.numOppId=itm.numOppId                                        
			 where  tintOppType=2 and numItemCode=p_numItemCode and mst.numDivisionID=p_numDivisionID order by (case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won' when tintOppStatus=2 then 'Deal Lost' end),bintCreatedDate DESC;

		OPEN SWV_RefCur3 FOR
 			select 1 as "numUnitHour",CAST(COALESCE(v_monListPrice,0) AS DECIMAL(20,5)) as "ListPrice",
			v_decmUOMConversion*(CAST(COALESCE(v_monListPrice,0) AS DECIMAL(20,5))) as "convrsPrice"
			,v_intLeadTimeDays as "LeadTime",v_intMinQty as "intMinQty",
			(SELECT now() + make_interval(days => v_intLeadTimeDays)) AS "EstimatedArrival"
			,'' as "vcPOppName",NULL::TIMESTAMP as "bintCreatedDate",''  as "OppStatus",p_numDivisionID as "numDivisionID"
 			,CAST(0 AS NUMERIC) AS "numPricRuleID",CAST(0 AS SMALLINT) AS "tintPricingMethod";
	END IF;
	
RETURN;
END; $$;



