-- Stored procedure definition script USP_OpportunityMaster_GetPOForMerge for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetPOForMerge(v_numDomainID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_bitOnlyMergable BOOLEAN
	,v_bitExcludePOSendToVendor BOOLEAN
	,v_bitDisplayOnlyCostSaving BOOLEAN
	,v_tintCostType SMALLINT
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numDivisionID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      vcCompanyName VARCHAR(300),
      vcNavigationURL VARCHAR(300),
      vcPOppName VARCHAR(300),
      vcCreatedBy VARCHAR(300),
      bintCreatedDate VARCHAR(50),
      monDealAmount DECIMAL(20,5),
      vcItemName VARCHAR(300),
      vcSKU VARCHAR(100),
      fltWeight DOUBLE PRECISION,
      numoppitemtCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      monPrice DECIMAL(20,5),
      vcItemCostSaving TEXT
   );
   INSERT INTO tt_TEMP(numDivisionID
		,numOppID
		,numItemCode
		,vcCompanyName
		,vcNavigationURL
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,vcItemName
		,vcSKU
		,fltWeight
		,numoppitemtCode
		,numUnitHour
		,monPrice
		,vcItemCostSaving)
   SELECT
   DivisionMaster.numDivisionID
		,OpportunityMaster.numOppId
		,Item.numItemCode
		,CompanyInfo.vcCompanyName
		,(CASE DivisionMaster.tintCRMType
   WHEN 2 THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID)
   WHEN 1 THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID)
   ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID)
   END)
		,CAST(OpportunityMaster.vcpOppName AS VARCHAR(300))
		,CAST(fn_GetContactName(OpportunityMaster.numCreatedBy) AS VARCHAR(300)) AS vcCreatedBy
		,CAST(FormatedDateFromDate(OpportunityMaster.bintCreatedDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval),v_numDomainID) AS VARCHAR(50)) AS bintCreatedDate
		,OpportunityMaster.monDealAmount
		,Item.vcItemName
		,Item.vcSKU
		,coalesce(Item.fltWeight,0)
		,OpportunityItems.numoppitemtCode
		,coalesce(OpportunityItems.numUnitHour,0)
		,coalesce(OpportunityItems.monPrice,0)
		,(CASE
   WHEN v_tintCostType = 2
   THEN coalesce((SELECT string_agg(CONCAT('<input type="button" class="btn btn-xs btn-success" value="$" onClick="return RecordSelected(this,',OM.numDivisionId,',',coalesce(OI.monPrice,0),
         ',',coalesce(V.intMinQty,0),')" /> <b>$',TO_CHAR((coalesce(OpportunityItems.numUnitHour,0)*coalesce(OI.monPrice,0)) -(coalesce(OpportunityItems.numUnitHour,0)*coalesce(OpportunityItems.monPrice,0)),'FM9,999,999,999,990.00999'),
         '</b> (',coalesce(OI.numUnitHour,0),' U) ',CONCAT('<span style="color:',(CASE
         WHEN (DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) = 0 AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 90)
         THEN '#00b050'
         WHEN (DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) = 91 AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 180)
         THEN '#ff9933'
         WHEN (DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) = 181 AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 270)
         THEN '#9148c8'
         ELSE '#ff0000'
         END),
         '">',DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate),'d</span> '),
         '<b><a target="_blank" href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'">',OM.vcpOppName,
         '</a></b>',' (',CI.vcCompanyName,')'),', ' ORDER BY (coalesce(OpportunityItems.numUnitHour,0)*coalesce(OI.monPrice,0)) -(coalesce(OpportunityItems.numUnitHour,0)*coalesce(OpportunityItems.monPrice,0)) ASC)
         FROM
         OpportunityMaster OM
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         INNER JOIN
         CompanyInfo CI
         ON
         DM.numCompanyID = CI.numCompanyId
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         LEFT JOIN
         Vendor V
         ON
         V.numVendorID = DM.numDivisionID
         AND V.numItemCode = OI.numItemCode
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.numOppId <> OpportunityMaster.numOppId
         AND OM.tintopptype = 2
         AND OM.tintoppstatus = 1
         AND coalesce(OM.bitStockTransfer,false) = false
         AND OI.numItemCode = Item.numItemCode
         AND 1 =(CASE
         WHEN coalesce(v_bitDisplayOnlyCostSaving,false) = true
         THEN(CASE WHEN coalesce(OI.monPrice,0) <> 0 AND coalesce(OI.monPrice,0) < coalesce(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
         ELSE 1
         END)
         AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 600
		  ),'')
   ELSE ''
   END) AS vcItemCostSaving
   FROM
   OpportunityMaster
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   INNER JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.tintopptype = 2
   AND OpportunityMaster.tintoppstatus = 1
   AND coalesce(OpportunityMaster.bitStockTransfer,false) = false
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   AND(SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numoppid = OpportunityMaster.numOppId) = 0
   AND WareHouseItems.numWareHouseID = v_numWarehouseID
   AND coalesce(OpportunityItems.numUnitHourReceived,0) = 0
   AND 1 =(CASE WHEN coalesce(v_bitExcludePOSendToVendor,false) = true THEN(CASE WHEN EXISTS(SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID = OpportunityMaster.numOppId AND tintCorrType IN(2,4)) THEN 0 ELSE 1 END) ELSE 1 END)
   AND 1 =(CASE
   WHEN v_tintCostType = 2
   THEN(CASE WHEN EXISTS(SELECT
         OI.numoppitemtCode
         FROM
         OpportunityMaster OM
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         INNER JOIN
         CompanyInfo CI
         ON
         DM.numCompanyID = CI.numCompanyId
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.numOppId <> OpportunityMaster.numOppId
         AND OM.tintopptype = 2
         AND OM.tintoppstatus = 1
         AND coalesce(OM.bitStockTransfer,false) = false
         AND OI.numItemCode = Item.numItemCode
         AND 1 =(CASE
         WHEN coalesce(v_bitDisplayOnlyCostSaving,false) = true
         THEN(CASE WHEN coalesce(OI.monPrice,0) <> 0 AND coalesce(OI.monPrice,0) < coalesce(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
         ELSE 1
         END)
         AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 600) THEN 1 ELSE 0 END)
   ELSE 1
   END);
	
   IF coalesce(v_bitOnlyMergable,false) = true then
	
      DELETE FROM tt_TEMP WHERE numDivisionID IN(SELECT numDivisionID FROM(SELECT numDivisionID,numOppID FROM tt_TEMP GROUP BY numDivisionID,numOppID) TEMP GROUP BY numDivisionID HAVING COUNT(*) < 2);
   end if;

   open SWV_RefCur for
   SELECT
   numDivisionID AS "numDivisionID"
		,vcCompanyName AS "vcCompanyName"
		,vcNavigationURL AS "vcNavigationURL"
		,CONCAT('[',COALESCE((SELECT string_agg(
      CONCAT('{"intType":"',PurchaseIncentives.intType,'", "vcIncentives":"',PurchaseIncentives.vcIncentives,
      '", "vcbuyingqty":"',PurchaseIncentives.vcBuyingQty,
      '", "tintDiscountType":"',coalesce(PurchaseIncentives.tintDiscountType,0),
      '"}'),',')
      FROM
      PurchaseIncentives
      WHERE
      PurchaseIncentives.numDivisionId = T.numDivisionID),''),']') AS "vcPurchaseIncentive"
   FROM
   tt_TEMP T
   GROUP BY
   numDivisionID,vcCompanyName,vcNavigationURL
   ORDER BY
   T.vcCompanyName;

   open SWV_RefCur2 for
   SELECT
   numDivisionID AS "numDivisionID"
		,numOppID AS "numOppId"
		,vcPOppName AS "vcPOppName"
		,vcCreatedBy AS "vcCreatedBy"
		,bintCreatedDate AS "bintCreatedDate"
		,monDealAmount AS "monDealAmount"
		,(CASE WHEN EXISTS(SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID = T.numOppID AND tintCorrType IN(2,4)) THEN true ELSE false END) AS "bitPOSendToVendor"
		,coalesce((SELECT(CASE
      WHEN EmailHistory.numEmailHstrID IS NOT NULL
      THEN CONCAT('<i></i>',FormatedDateFromDate(EmailHistory.dtReceivedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID))
      ELSE FormatedDateFromDate(dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)
      END)
      FROM
      Correspondence
      LEFT JOIN
      EmailHistory
      ON
      Correspondence.numEmailHistoryID = EmailHistory.numEmailHstrID
      LEFT JOIN
      Communication
      ON
      Correspondence.numCommID = Communication.numCommId
      WHERE
      numOpenRecordID = T.numOppID
      AND tintCorrType IN(2,4) LIMIT 1),'') AS "vcPOSend"
   FROM
   tt_TEMP T
   GROUP BY
   numDivisionID,numOppID,vcPOppName,vcCreatedBy,bintCreatedDate,monDealAmount
   ORDER BY
   numOppID;

   open SWV_RefCur3 for
   SELECT
		numOppID AS "numOppId"
		,numoppitemtCode AS "numoppitemtCode"
		,numItemCode AS "numItemCode"
		,vcItemName AS "vcItemName"
		,vcSKU AS "vcSKU"
		,fltWeight AS "fltWeight"
		,numUnitHour AS "numUnitHour"
		,monPrice AS "monPrice"
		,vcItemCostSaving AS "vcItemCostSaving"
   FROM
   tt_TEMP T;
   RETURN;
END; $$;


