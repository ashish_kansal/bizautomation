-- Stored procedure definition script usp_DisplayUniversalMaster for PostgreSQL
CREATE OR REPLACE FUNCTION usp_DisplayUniversalMaster(v_numDivID NUMERIC(9,0) DEFAULT 0
	 
	 	--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

	--Input Parameter List
   AS $$
BEGIN

	--selecting the value of Universal key to display on prospects-display.aspx page  
   open SWV_RefCur for SELECT numUniversalSupportKey as "numUniversalSupportKey" from UniversalSupportKeyMaster where numDivisionID = v_numDivID;
END; $$;












