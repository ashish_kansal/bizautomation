-- Stored procedure definition script USP_PromotionOffer_GetCustomerSelections for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PromotionOffer_GetCustomerSelections(v_numProId NUMERIC(18,0),
	v_numDomainID NUMERIC(9,0) DEFAULT 0,  
	v_tintCustomerSelectionType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintCustomerSelectionType = 1 then
	
      open SWV_RefCur for
      SELECT numProOrgId, numProId, vcCompanyName,
		GetListIemName(CompanyInfo.numCompanyType) || ',' || GetListIemName(CompanyInfo.vcProfile) AS Relationship,
		(SELECT coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') || ', ' || coalesce(vcEmail,'')
         FROM AdditionalContactsInformation A
         WHERE A.numDivisionId = DivisionMaster.numDivisionID
         AND coalesce(A.bitPrimaryContact,false) = true) AS PrimaryContact
      FROM DivisionMaster
      JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      JOIN PromotionOfferOrganizations POO ON POO.numDivisionID = DivisionMaster.numDivisionID
      WHERE DivisionMaster.numDomainID = v_numDomainID AND numProId = v_numProId AND tintType = 1;
   ELSEIF v_tintCustomerSelectionType = 2
   then
	
      open SWV_RefCur for
      SELECT numProOrgId, numProId, L1.vcData || ' - ' || L2.vcData AS RelProfile FROM PromotionOfferOrganizations POO
      JOIN Listdetails L1 ON L1.numListItemID = POO.numRelationship
      JOIN Listdetails L2 ON L2.numListItemID = POO.numProfile
      WHERE numProId = v_numProId
      AND tintType = 2;
   end if;
   RETURN;
END; $$; 


