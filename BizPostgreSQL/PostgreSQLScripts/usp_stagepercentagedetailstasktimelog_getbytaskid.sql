-- Stored procedure definition script USP_StagePercentageDetailsTaskTimeLog_GetByTaskID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTaskTimeLog_GetByTaskID(v_numDomainID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_bitForProject BOOLEAN DEFAULT false
	,INOUT v_vcTotalTimeSpendOnTask VARCHAR(50)  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_bitForProject = true) then
	
      v_vcTotalTimeSpendOnTask := GetTimeSpendOnTaskByProject(v_numDomainID,v_numTaskID,0::SMALLINT);
   ELSE
      v_vcTotalTimeSpendOnTask := GetTimeSpendOnTask(v_numDomainID,v_numTaskID,0::SMALLINT);
   end if;

   open SWV_RefCur for
   SELECT
   SPDTTL.ID AS "ID"
		,SPDTTL.numTaskId AS "numTaskId"
		,SPDTTL.tintAction AS "tintAction"
		,coalesce(SPDTTL.numProcessedQty,0) AS "numProcessedQty"
		,coalesce(SPDTTL.numReasonForPause,0) AS "numReasonForPause"
		,fn_GetContactName(SPDTTL.numUserCntID) AS "vcEmployee"
		,CASE SPDTTL.tintAction
   WHEN 1 THEN 'Started'
   WHEN 2 THEN 'Paused'
   WHEN 3 THEN 'Resumed'
   WHEN 4 THEN 'Finished'
   ELSE ''
   END AS "vcAction"
		,CONCAT(FormatedDateFromDate(SPDTTL.dtActionTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID),' ',SUBSTR(TO_CHAR(SPDTTL.dtActionTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),'hh24:mi:ss'),1,5),
   ' ',SUBSTR(TO_CHAR(SPDTTL.dtActionTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),'Mon dd yyyy hh:miAM'),length(TO_CHAR(SPDTTL.dtActionTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),'Mon dd yyyy hh:miAM')) -2+1)) AS "vcActionTime"
		,SPDTTL.dtActionTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS "dtActionTime"
		,CASE WHEN tintAction = 2 THEN CAST(numProcessedQty AS VARCHAR(30)) ELSE '' END AS "vcProcessedQty"
		,coalesce(LD.vcData,'') AS "vcReasonForPause"
		,coalesce(SPDTTL.vcNotes,'') AS "vcNotes"
		,(CASE WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog S WHERE S.numTaskId = v_numTaskID AND tintAction = 4) THEN true ELSE false END) AS "bitTaskFinished"
   FROM
   StagePercentageDetailsTaskTimeLog SPDTTL
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 52
   AND SPDTTL.numReasonForPause = LD.numListItemID
   WHERE
   SPDTTL.numDomainID = v_numDomainID
   AND SPDTTL.numTaskId = v_numTaskID
   ORDER BY
   ID;
   RETURN;
END; $$;


