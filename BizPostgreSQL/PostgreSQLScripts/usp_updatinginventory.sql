-- Stored procedure definition script USP_UpdatingInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatingInventory(v_byteMode SMALLINT DEFAULT 0,  
	v_numWareHouseItemID NUMERIC(18,0) DEFAULT 0,  
	v_Units DECIMAL DEFAULT NULL,
	v_numWOId NUMERIC(9,0) DEFAULT 0,
	v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	v_numOppId NUMERIC(9,0) DEFAULT 0,
	v_numAssembledItemID NUMERIC(18,0) DEFAULT 0,
	v_fltQtyRequiredForSingleBuild DOUBLE PRECISION DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Description  VARCHAR(200);
   v_numItemCode  NUMERIC(18,0);
   v_numDomain  NUMERIC(18,0);
   v_ParentWOID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numOverheadServiceItemID  NUMERIC(18,0);

   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_CurrentAverageCost  DECIMAL(20,5);
   v_TotalCurrentOnHand  DOUBLE PRECISION;
   v_newAverageCost  DECIMAL(20,5);

   v_monOverheadCost  DECIMAL(20,5); 
   v_monLabourCost  DECIMAL(20,5);		
	
   v_onHand  DOUBLE PRECISION;                                    
   v_onOrder  DOUBLE PRECISION;                                    
   v_onBackOrder  DOUBLE PRECISION;                                   
   v_onAllocation  DOUBLE PRECISION;    
   v_monAverageCost  DECIMAL(20,5);
   v_numReferenceID  NUMERIC(18,0);
   v_tintRefType  NUMERIC(18,0); 
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   v_desc  VARCHAR(100);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   numParentWOID INTO v_ParentWOID FROM WorkOrder WHERE numWOId = v_numWOId;
      select   numItemID, numDomainID INTO v_numItemCode,v_numDomain from WareHouseItems where numWareHouseItemID = v_numWareHouseItemID;
      select   coalesce(numOverheadServiceItemID,0) INTO v_numOverheadServiceItemID FROM Domain WHERE numDomainId = v_numDomain;
      IF coalesce(v_numOppId,0) > 0 then
	
         select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomain;
         select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppId;
      end if;
      IF v_byteMode = 0 then  -- Aseeembly Item
         select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END) INTO v_CurrentAverageCost FROM Item WHERE numItemCode = v_numItemCode;
         select   coalesce((SUM(numOnHand)+SUM(numAllocation)),0) INTO v_TotalCurrentOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode;
         RAISE NOTICE '%',v_CurrentAverageCost;
         RAISE NOTICE '%',v_TotalCurrentOnHand;
         IF v_numWOId > 0 then
		

			-- Store hourly rate for all task assignee
            UPDATE
            StagePercentageDetailsTask SPDT
            SET
            monHourlyRate = coalesce(UM.monHourlyRate,0)
            FROM
            UserMaster UM
            WHERE
            SPDT.numAssignTo = UM.numUserDetailId AND(SPDT.numDomainID = v_numDomain
            AND SPDT.numWorkOrderId = v_numWOId);
            select   SUM(numQtyItemsReq*coalesce(I.monAverageCost,0)) INTO v_newAverageCost FROM
            WorkOrderDetails WOD
            LEFT JOIN
            Item I
            ON
            I.numItemCode = WOD.numChildItemID WHERE
            WOD.numWOId = v_numWOId
            AND I.charItemType = 'P';
            IF v_numOverheadServiceItemID > 0 AND EXISTS(SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId = v_numWOId AND numChildItemID = v_numOverheadServiceItemID) then
			
               SELECT SUM(numQtyItemsReq*coalesce(Item.monListPrice,0)) INTO v_monOverheadCost FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID = Item.numItemCode WHERE WOD.numWOId = v_numWOId AND WOD.numChildItemID = v_numOverheadServiceItemID;
            end if;
            v_monLabourCost := GetWorkOrderLabourCost(v_numDomain,v_numWOId);
            UPDATE WorkOrder SET monAverageCost = v_newAverageCost,monLabourCost = coalesce(v_monLabourCost,0),
            monOverheadCost = coalesce(v_monOverheadCost,0) WHERE numWOId = v_numWOId;
            v_newAverageCost :=((v_CurrentAverageCost*v_TotalCurrentOnHand)+(v_newAverageCost+coalesce(v_monOverheadCost,0)+coalesce(v_monLabourCost,0)))/(v_TotalCurrentOnHand+v_Units);
         ELSE
            select   SUM((numQtyItemsReq*fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainID,I.numBaseUnit))*I.monAverageCost) INTO v_newAverageCost FROM
            ItemDetails ID
            LEFT JOIN
            Item I
            ON
            I.numItemCode = ID.numChildItemID WHERE
            numItemKitID = v_numItemCode
            AND I.charItemType = 'P';
            UPDATE AssembledItem SET monAverageCost = v_newAverageCost WHERE ID = v_numAssembledItemID;
            v_newAverageCost :=((v_CurrentAverageCost*v_TotalCurrentOnHand)+(v_newAverageCost*v_Units))/(v_TotalCurrentOnHand+v_Units);
         end if;
         UPDATE Item SET monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_newAverageCost END) WHERE numItemCode = v_numItemCode;
         IF coalesce(v_numWOId,0) > 0 then
		
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= v_Units THEN 0 ELSE v_Units -coalesce(numBackOrder,0) END),numAllocation = coalesce(numAllocation,0)+(CASE WHEN coalesce(numBackOrder,0) > v_Units THEN v_Units ELSE coalesce(numBackOrder,0) END),numBackOrder =(CASE WHEN coalesce(numBackOrder,0) > v_Units THEN coalesce(numBackOrder,0) -v_Units ELSE 0 END),
            numonOrder =(CASE WHEN v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = false AND v_numOppId > 0 THEN numonOrder ELSE(CASE WHEN coalesce(numonOrder,0) < v_Units THEN 0 ELSE coalesce(numonOrder,0) -v_Units END) END),dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         ELSE
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= v_Units THEN  0 ELSE v_Units -coalesce(numBackOrder,0) END),numAllocation = coalesce(numAllocation,0)+(CASE WHEN coalesce(numBackOrder,0) > v_Units THEN v_Units ELSE coalesce(numBackOrder,0) END),
            numBackOrder =(CASE WHEN coalesce(numBackOrder,0) > v_Units THEN coalesce(numBackOrder,0) -v_Units ELSE 0 END),dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         end if;
      ELSEIF v_byteMode = 1
      then  --Aseembly Child item
         select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_monAverageCost FROM Item WHERE  numItemCode = v_numItemCode;
         IF v_numWOId > 0 then
		
            UPDATE
            WorkOrderDetails
            SET
            monAverageCost = v_monAverageCost
            WHERE
            numWOId = v_numWOId
            AND numChildItemID = v_numItemCode
            AND numWarehouseItemID = v_numWareHouseItemID;
         ELSE
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
            INSERT INTO AssembledItemChilds(numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild)
			VALUES(v_numAssembledItemID,
				v_numItemCode,
				v_Units,
				v_numWareHouseItemID,
				v_monAverageCost,
				v_fltQtyRequiredForSingleBuild);
         end if;
         select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder from
         WareHouseItems where
         numWareHouseItemID = v_numWareHouseItemID;
         IF coalesce(v_numWOId,0) > 0 then
		
            IF v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = false AND v_numOppId > 0 then -- Allocation When Fulfillment Order (Packing Slip) is added
			
               IF v_onHand >= v_Units then
				
					--RELEASE QTY FROM ON HAND
                  v_onHand := v_onHand -v_Units;
               ELSE
                  RAISE EXCEPTION 'NOTSUFFICIENTQTY_ONHAND';
               end if;
            ELSE
               IF v_onAllocation >= v_Units then
				
					--RELEASE QTY FROM ALLOCATION
                  v_onAllocation := v_onAllocation -v_Units;
               ELSE
                  RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
               end if;
            end if;
         ELSE
			--DECREASE QTY FROM ONHAND
            v_onHand := v_onHand -v_Units;
         end if;

		 --UPDATE INVENTORY
         UPDATE
         WareHouseItems
         SET
         numOnHand = v_onHand,numonOrder = v_onOrder,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
         dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      end if;
      IF v_numWOId > 0 then
	 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
         IF coalesce(v_numOppId,0) > 0 then --FROM SALES ORDER 
		
            IF v_byteMode = 0 then --Aseeembly Item
               v_Description := 'Work Order Completed (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
            ELSEIF v_byteMode = 1
            then --Aseembly Child item
               IF coalesce(v_ParentWOID,0) = 0 then
				
                  v_Description := 'Items Used In SO-WO (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
               ELSE
                  v_Description := 'Items Used In SO-WO Work Order (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
               end if;
            end if;
            v_numReferenceID := v_numWOId;
            v_tintRefType := 2;
         ELSE
            IF v_byteMode = 0 then --Aseeembly Item
               v_Description := 'Work Order Completed (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
            ELSEIF v_byteMode = 1
            then --Aseembly Child item
               v_Description := 'Items Used In Work Order (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
            end if;
            v_numReferenceID := v_numWOId;
            v_tintRefType := 2;
         end if;
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numReferenceID,
         v_tintRefType := v_tintRefType::SMALLINT,v_vcDescription := v_Description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,
         SWV_RefCur := null);
      ELSEIF v_byteMode = 0
      then
	 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
         v_desc := 'Create Assembly:' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ' Units';
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_desc,v_numModifiedBy := v_numUserCntID,
         v_numDomainID := v_numDomain,SWV_RefCur := null);
      ELSEIF v_byteMode = 1
      then
	
	 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
         v_Description := 'Item Used in Assembly (Qty: ' || SUBSTR(CAST(v_Units AS VARCHAR(10)),1,10) || ')';
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_Description,
         v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_numOppId := 0;
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


