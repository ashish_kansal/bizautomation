-- Stored procedure definition script USP_ExtChangePassword for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtChangePassword(v_numContactId NUMERIC,
	v_numDivisionId NUMERIC,
    v_OldPassword VARCHAR(100),
    v_NewPassword VARCHAR(100),
    v_numDomainID NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Count  INTEGER;
BEGIN
   select   COUNT(*) INTO v_Count FROM    ExtarnetAccounts hdr
   JOIN ExtranetAccountsDtl Dtl ON hdr.numExtranetID = Dtl.numExtranetID WHERE   numDivisionId = v_numDivisionId
   AND tintAccessAllowed = 1
   AND numContactID = CAST(v_numContactId AS VARCHAR)
   AND vcPassword = v_OldPassword
   AND Dtl.numDomainID = v_numDomainID;


   IF v_Count = 1 then
    
      UPDATE  ExtranetAccountsDtl
      SET     vcPassword = v_NewPassword
      WHERE   tintAccessAllowed = 1
      AND numContactID = CAST(v_numContactId AS VARCHAR)
      AND numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT  1;
   ELSE
      open SWV_RefCur for
      SELECT  0;
   end if;
   RETURN;
END; $$;


