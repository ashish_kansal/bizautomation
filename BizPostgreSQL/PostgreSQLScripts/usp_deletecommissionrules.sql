-- Stored procedure definition script USP_DeleteCommissionRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCommissionRules(v_numComRuleID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE  FROM CommissionRules
   WHERE   numComRuleID = v_numComRuleID
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;


