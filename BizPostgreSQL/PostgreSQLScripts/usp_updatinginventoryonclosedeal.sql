-- Stored procedure definition script USP_UpdatingInventoryonCloseDeal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatingInventoryonCloseDeal(v_numOppID NUMERIC(18,0),
v_numOppItemId NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOpptype  SMALLINT;        
   v_itemcode  NUMERIC;                                    
   v_numUnits  DOUBLE PRECISION;                                      
   v_numoppitemtCode  NUMERIC(9,0);         
   v_numWareHouseItemID  NUMERIC(9,0); 
   v_numToWarehouseItemID  NUMERIC(9,0); --to be used with stock transfer
   v_monAmount  DECIMAL(20,5); 
   v_bitStockTransfer  BOOLEAN;
   v_QtyShipped  DOUBLE PRECISION;
   v_QtyReceived  DOUBLE PRECISION;
   v_fltExchangeRate  DOUBLE PRECISION; 
   v_bitWorkOrder  BOOLEAN;

   v_numDomain  NUMERIC(18,0);
   v_ErrMsg  VARCHAR(4000);
   v_ErrSeverity  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   OM.numDomainId INTO v_numDomain FROM OpportunityMaster AS OM WHERE OM.numOppId = v_numOppID;
      select   tintopptype, coalesce(bitStockTransfer,false), (CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END) INTO v_tintOpptype,v_bitStockTransfer,v_fltExchangeRate from OpportunityMaster where numOppId = v_numOppID;
      select   numoppitemtCode, OI.numItemCode, numUnitHour, coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), numToWarehouseItemID, monTotAmount*v_fltExchangeRate, OI.bitWorkOrder INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWareHouseItemID,
      v_numToWarehouseItemID,v_monAmount,v_bitWorkOrder FROM OpportunityItems OI join Item I
      on OI.numItemCode = I.numItemCode   and numOppId = v_numOppID where (charitemtype = 'P' OR 1 =(CASE WHEN v_tintOpptype = 1 THEN
         CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
         WHEN coalesce(I.bitKitParent,false) = true THEN 1
         ELSE 0 END
      ELSE 0 END)) and (bitDropShip = false or bitDropShip is null)
      AND I.numDomainID = v_numDomain
      AND (coalesce(v_numOppItemId,0) = 0 OR OI.numoppitemtCode = v_numOppItemId)   ORDER by OI.numoppitemtCode LIMIT 1;
      while v_numoppitemtCode > 0 LOOP
         IF v_bitStockTransfer = true then
				
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
            PERFORM usp_ManageInventory(v_itemcode,v_numWareHouseItemID,v_monAmount,1::SMALLINT,v_numUnits,v_QtyShipped,
            v_QtyReceived,0,1::SMALLINT,v_numOppID,v_numoppitemtCode,v_numUserCntID,
            v_bitWorkOrder);
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
            PERFORM usp_ManageInventory(v_itemcode,v_numToWarehouseItemID,v_monAmount,2::SMALLINT,v_numUnits,v_QtyShipped,
            v_QtyReceived,0,1::SMALLINT,v_numOppID,v_numoppitemtCode,v_numUserCntID,
            v_bitWorkOrder);
         ELSE
            PERFORM usp_ManageInventory(v_itemcode,v_numWareHouseItemID,v_monAmount,v_tintOpptype::SMALLINT,v_numUnits,
            v_QtyShipped,v_QtyReceived,0,1::SMALLINT,v_numOppID,v_numoppitemtCode,
            v_numUserCntID,v_bitWorkOrder);
         end if;
         select   numoppitemtCode, OI.numItemCode, numUnitHour, coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), numToWarehouseItemID, monTotAmount*v_fltExchangeRate, OI.bitWorkOrder INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWareHouseItemID,
         v_numToWarehouseItemID,v_monAmount,v_bitWorkOrder from OpportunityItems OI join Item I
         on OI.numItemCode = I.numItemCode and numOppId = v_numOppID where (charitemtype = 'P' OR 1 =(CASE WHEN v_tintOpptype = 1 THEN
            CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
            WHEN coalesce(I.bitKitParent,false) = true THEN 1
            ELSE 0 END
         ELSE 0 END)) and OI.numoppitemtCode > v_numoppitemtCode and (bitDropShip = false or bitDropShip is null)
         AND I.numDomainID = v_numDomain
         AND (coalesce(v_numOppItemId,0) = 0 OR OI.numoppitemtCode = v_numOppItemId)   order by OI.numoppitemtCode LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numoppitemtCode := 0;
         end if;
      END LOOP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
  -- Whoops, there was an error
         -- ROLLBACK


  -- Raise an error with the details of the exception
  v_ErrMsg := SQLERRM;
         v_ErrSeverity := ERROR_SEVERITY();
         RAISE NOTICE '%',v_ErrMsg;
   END;
   RETURN;
END; $$;
        


