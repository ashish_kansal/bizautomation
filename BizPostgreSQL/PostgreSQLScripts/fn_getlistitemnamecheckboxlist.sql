CREATE OR REPLACE FUNCTION fn_GetListItemNameCheckBoxList(v_vclistIDs VARCHAR(1000))
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(1000);
BEGIN
   v_listName := COALESCE((SELECT string_agg(vcData,',') FROM Listdetails WHERE numListItemID IN (SELECT coalesce(Id,0) FROM SplitIDs(v_vclistIDs,','))),'');

   RETURN coalesce(v_listName,'-');
END; $$;

