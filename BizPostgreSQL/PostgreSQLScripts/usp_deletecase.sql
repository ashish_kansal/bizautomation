-- Stored procedure definition script usp_DeleteCase for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteCase(v_numCaseID NUMERIC(9,0) DEFAULT 0 ,  
 v_numDomainID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select count(*) from Cases where numCaseId = v_numCaseID and numDomainID = v_numDomainID) then

      DELETE FROM Correspondence WHERE numOpenRecordID = v_numCaseID AND tintCorrType = 6;
      DELETE FROM CaseOpportunities WHERE numCaseId = v_numCaseID AND numDomainid = v_numDomainID;
      delete FROM RecentItems where numRecordID = v_numCaseID and chrRecordType = 'S';
      delete from timeandexpense where  numCaseId =  v_numCaseID and numDomainID =   v_numDomainID;
      delete from CaseContacts where numCaseID = v_numCaseID;
      delete from CaseSolutions where numCaseId = v_numCaseID;
      delete  from Comments where numCaseID = v_numCaseID;
      DELETE from Cases where numCaseId = v_numCaseID;
   end if;
   RETURN;
END; $$;


