DROP FUNCTION IF EXISTS GetPercentageChangeInLeadToAccountConversion;

CREATE OR REPLACE FUNCTION GetPercentageChangeInLeadToAccountConversion(v_numDomainID NUMERIC(18,0),
	 v_dtStartDate DATE,
	 v_dtEndDate DATE,
	 v_tintDateRange SMALLINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)
RETURNS NUMERIC(18,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LeadToAccountConversionChange  NUMERIC(18,2) DEFAULT 0.00;
   v_TotalLeadsConvertedToAccount1  INTEGER;
   v_TotalLeadsConvertedToAccount2  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGPCLAC CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGPCLAC
   (
      numDivisionID NUMERIC(18,0),
      FirstRecordID NUMERIC(18,0),
      LastRecordID NUMERIC(18,0),
      tintFirstCRMType SMALLINT,
      tintLastCRMType SMALLINT
   );
   INSERT INTO tt_TEMPGPCLAC(numDivisionID
		,FirstRecordID
		,LastRecordID)
   SELECT
   DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
   FROM
   DivisionMasterPromotionHistory
   INNER JOIN
   DivisionMaster DM
   ON
   DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
   WHERE
   DivisionMasterPromotionHistory.numDomainID = v_numDomainID
   AND DM.numDomainID = v_numDomainID
   AND DM.bintCreatedDate BETWEEN v_dtStartDate AND v_dtEndDate
   GROUP BY
   DivisionMasterPromotionHistory.numDivisionID;

   UPDATE
   tt_TEMPGPCLAC T1
   SET
   tintFirstCRMType = DMPH1.tintPreviousCRMType,tintLastCRMType = DMPH2.tintNewCRMType
   FROM
   DivisionMasterPromotionHistory DMPH1, DivisionMasterPromotionHistory DMPH2
   WHERE
   T1.FirstRecordID = DMPH1.ID AND T1.LastRecordID = DMPH2.ID AND DMPH1.tintPreviousCRMType = 0;

   select   COUNT(*) INTO v_TotalLeadsConvertedToAccount1 FROM
   tt_TEMPGPCLAC T1 WHERE
   T1.tintFirstCRMType = 0
   AND T1.tintLastCRMType = 2;
	
	
   DELETE FROM tt_TEMPGPCLAC;

   INSERT INTO tt_TEMPGPCLAC(numDivisionID
		,FirstRecordID
		,LastRecordID)
   SELECT
   DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
   FROM
   DivisionMasterPromotionHistory
   INNER JOIN
   DivisionMaster DM
   ON
   DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
   WHERE
   DivisionMasterPromotionHistory.numDomainID = v_numDomainID
   AND DM.numDomainID = v_numDomainID
   AND 1 =(CASE
   WHEN v_tintDateRange = 1
   THEN(CASE WHEN DM.bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 year' AND v_dtEndDate+INTERVAL '-1 year' THEN 1 ELSE 0 END)
   WHEN v_tintDateRange = 2
   THEN(CASE WHEN DM.bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 month' AND v_dtEndDate+INTERVAL '-1 month' THEN 1 ELSE 0 END)
   ELSE 0
   END)
   GROUP BY
   DivisionMasterPromotionHistory.numDivisionID;

   UPDATE
   tt_TEMPGPCLAC T1
   SET
   tintFirstCRMType = DMPH1.tintPreviousCRMType,tintLastCRMType = DMPH2.tintNewCRMType
   FROM
   DivisionMasterPromotionHistory DMPH1, DivisionMasterPromotionHistory DMPH2
   WHERE
   T1.FirstRecordID = DMPH1.ID AND T1.LastRecordID = DMPH2.ID AND DMPH1.tintPreviousCRMType = 0;

   select   COUNT(*) INTO v_TotalLeadsConvertedToAccount2 FROM
   tt_TEMPGPCLAC T1 WHERE
   T1.tintFirstCRMType = 0
   AND T1.tintLastCRMType = 2;


   v_LeadToAccountConversionChange := 100.0*(coalesce(v_TotalLeadsConvertedToAccount1,0) -coalesce(v_TotalLeadsConvertedToAccount2,0))/(CASE WHEN coalesce(v_TotalLeadsConvertedToAccount2,0) = 0 THEN 1 ELSE coalesce(v_TotalLeadsConvertedToAccount2,0) END);


   RETURN coalesce(v_LeadToAccountConversionChange,0.00);
END; $$;

