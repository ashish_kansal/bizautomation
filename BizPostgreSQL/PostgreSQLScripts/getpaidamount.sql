-- Function definition script GetPaidAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPaidAmount(v_numOppID NUMERIC)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalAmtPaid  DECIMAL(20,5);
BEGIN
   SELECT SUM(coalesce(monAmountPaid,0)) INTO v_TotalAmtPaid FROM OpportunityBizDocs WHERE numoppid = v_numOppID;
   return v_TotalAmtPaid; -- Set Accuracy of Two precision
END; $$;

