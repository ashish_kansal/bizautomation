-- Stored procedure definition script USP_UpdateEmailHstrForETrack for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateEmailHstrForETrack(v_numEmailHstrID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update EmailHistory set numNoofTimes =(coalesce(numNoofTimes,0)+1) where numEmailHstrID = v_numEmailHstrID;
   RETURN;
END; $$;


