CREATE OR REPLACE FUNCTION USP_GetShippingCharge(v_numDomainID NUMERIC(18,0),
	v_numShippingCountry NUMERIC(18,0),
	v_cookieId TEXT DEFAULT NULL,
	v_numUserCntId NUMERIC DEFAULT 0,
	INOUT v_monShippingAmount DECIMAL(20,5)  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPromotionID  NUMERIC(18,0) DEFAULT 0;

   v_decmPrice  DECIMAL(18,2);
   v_bitFreeShiping  BOOLEAN;
   v_monFreeShippingOrderAmount  DECIMAL(20,5);
   v_numFreeShippingCountry  DECIMAL(20,5);
   v_bitFixShipping1  BOOLEAN;
   v_monFixShipping1OrderAmount  DECIMAL(20,5);
   v_monFixShipping1Charge  DECIMAL(20,5);
   v_bitFixShipping2  BOOLEAN;
   v_monFixShipping2OrderAmount  DECIMAL(20,5);
   v_monFixShipping2Charge  DECIMAL(20,5);
BEGIN
   v_monShippingAmount := -1;
   SELECT
   coalesce(SUM(monTotAmount),0) INTO v_decmPrice FROM
   CartItems WHERE
   numDomainId = v_numDomainID
   AND vcCookieId = v_cookieId
   AND numUserCntId = v_numUserCntId;


   SELECT 
   PromotionID INTO v_numPromotionID FROM
   CartItems AS C
   LEFT JOIN
   PromotionOffer AS P
   ON
   C.PromotionID = P.numProId
   LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainID WHERE
   C.numDomainId = v_numDomainID
   AND C.vcCookieId = v_cookieId
   AND C.numUserCntId = v_numUserCntId
   AND (coalesce(bitFixShipping1,false) = true OR coalesce(bitFixShipping2,false) = true OR coalesce(bitFreeShiping,false) = true)   ORDER BY
   numCartId  LIMIT 1;

   IF(v_numPromotionID > 0) then
      select   bitFreeShiping, monFreeShippingOrderAmount, numFreeShippingCountry, bitFixShipping1, monFixShipping1OrderAmount, monFixShipping1Charge, bitFixShipping2, monFixShipping2OrderAmount, monFixShipping2Charge INTO v_bitFreeShiping,v_monFreeShippingOrderAmount,v_numFreeShippingCountry,
      v_bitFixShipping1,v_monFixShipping1OrderAmount,v_monFixShipping1Charge,
      v_bitFixShipping2,v_monFixShipping2OrderAmount,v_monFixShipping2Charge FROM
      PromotionOffer PO
      LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainID WHERE
      PO.numDomainId = v_numDomainID
      AND numProId = v_numPromotionID;
      IF(coalesce(v_bitFixShipping1,false) = true AND v_decmPrice > v_monFixShipping1OrderAmount) then
		
         v_monShippingAmount := v_monFixShipping1Charge;
      end if;
      IF(coalesce(v_bitFixShipping2,false) = true AND v_decmPrice > v_monFixShipping2OrderAmount) then
		
         v_monShippingAmount := v_monFixShipping2Charge;
      end if;
      IF((coalesce(v_bitFreeShiping,false) = true AND v_numFreeShippingCountry = v_numShippingCountry AND v_decmPrice > v_monFreeShippingOrderAmount)) then
		
         v_monShippingAmount := 0;
      end if;
   end if;
	
   open SWV_RefCur for
   SELECT coalesce(v_monShippingAmount,0);
   RETURN;
END; $$;


