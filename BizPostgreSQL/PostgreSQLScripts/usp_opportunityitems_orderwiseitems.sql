-- Stored procedure definition script USP_OpportunityItems_OrderwiseItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE OR REPLACE FUNCTION USP_OpportunityItems_OrderwiseItems(v_numDomainId NUMERIC(9,0) ,    
    v_numDivisionId NUMERIC(9,0) ,    
    v_keyword VARCHAR(50) ,    
    v_startDate TIMESTAMP DEFAULT NULL,    
    v_endDate TIMESTAMP DEFAULT NULL,    
    v_imode INTEGER DEFAULT NULL ,    
    v_CurrentPage INTEGER DEFAULT 0 ,    
    v_PageSize INTEGER DEFAULT 0 ,    
    INOUT v_TotRecs INTEGER DEFAULT 0 , INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql         
 -- Add the parameters for the stored procedure here        
            
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
   AS $$
   DECLARE
   v_numCompanyID  NUMERIC(18,0);
   v_firstRec  INTEGER;          
   v_lastRec  INTEGER;
   v_VfirstRec  INTEGER;          
   v_VlastRec  INTEGER;
BEGIN
        
                
   select   numCompanyID INTO v_numCompanyID FROM DivisionMaster where numDivisionID = v_numDivisionId and numDomainID = v_numDomainId; 

    -- Insert statements for procedure here        
   IF (v_imode = 1) then
      DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMS
      (
         Units DOUBLE PRECISION,
         BasePrice DECIMAL(18,2),
         ItemName VARCHAR(300),
         vcSKU VARCHAR(300),
         vcUPC VARCHAR(300),
         base DECIMAL(18,2),
         ChargedAmount DECIMAL(18,2),
         ORDERID NUMERIC(9,0),
         ModelId VARCHAR(200),
         OrderDate VARCHAR(30),
         tintOppType SMALLINT,
         tintOppStatus SMALLINT,
         numContactId NUMERIC,
         numDivisionID NUMERIC,
         OppId NUMERIC,
         OrderNo VARCHAR(300),
         CustomerPartNo  VARCHAR(300),
         numItemCode NUMERIC(18,0)
      );
      INSERT  INTO tt_TEMPITEMS
      SELECT  OI.numUnitHour AS Units ,
                                OI.monPrice AS BasePrice ,
                                OI.vcItemName AS ItemName ,
								CASE WHEN coalesce(I.numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcWHSKU,coalesce(I.vcSKU,'')) ELSE I.vcSKU END,
								CASE WHEN coalesce(I.numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,coalesce(I.numBarCodeId,'')) ELSE I.numBarCodeId END,
                                OI.monAvgCost AS base ,
                                OI.monTotAmount AS ChargedAmount ,
                                OI.numItemCode AS ORDERID ,
                                coalesce(OI.vcModelID,'NA') AS ModelId ,
                                FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,
                                OM.tintopptype ,
                                OM.tintoppstatus ,
                                OM.numContactId ,
                                ACI.numDivisionId ,
                                OM.numOppId AS OppId ,
								 CAST(OM.vcpOppName AS VARCHAR(300)) AS OrderNo ,
								 coalesce(CPN.CustomerPartNo,'') AS  CustomerPartNo,
								 OI.numItemCode
      FROM    OpportunityItems AS OI
      INNER JOIN Item I ON OI.numItemCode = I.numItemCode
      INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = OM.numContactId
      LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
      LEFT JOIN  CustomerPartNumber CPN  ON OI.numItemCode = CPN.numItemCode AND numCompanyId = v_numCompanyID and CPN.numDomainID = v_numDomainId
      WHERE   OM.numDomainId = v_numDomainId
      AND ACI.numDivisionId = v_numDivisionId
      AND (OI.vcItemName ilike coalesce(v_keyword,'') || '%'
      OR OI.vcModelID ilike coalesce(v_keyword,'') || '%'
      OR CPN.CustomerPartNo ilike '%' || coalesce(v_keyword,'') || '%')
      AND (OM.bintCreatedDate >= coalesce(v_startDate,OM.bintCreatedDate)
      AND OM.bintCreatedDate <= coalesce(v_endDate,OM.bintCreatedDate));
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPITEMS;
      open SWV_RefCur for
      SELECT * FROM(SELECT * ,
                                    ROW_NUMBER() OVER(ORDER BY ItemName) AS RowNumber
         FROM      tt_TEMPITEMS) a
      WHERE   RowNumber > v_firstRec
      AND RowNumber < v_lastRec
      ORDER BY ItemName;
                
   ELSE
      DROP TABLE IF EXISTS tt_TEMPVITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVITEMS
      (
         numDivisionID NUMERIC(18,0),
         ItemName VARCHAR(300),
         vcSKU VARCHAR(300),
         vcUPC VARCHAR(300),
         MinOrderQty INTEGER,
         MinCost DECIMAL(18,2),
         ModelId VARCHAR(200),
         PartNo VARCHAR(100),
         numItemCode NUMERIC,
         CustomerPartNo  VARCHAR(100)
      );
      INSERT  INTO tt_TEMPVITEMS
      SELECT  v_numDivisionId,
								i.vcItemName AS ItemName,
								coalesce(i.vcSKU,''),
								coalesce(i.numBarCodeId,''),
                                v.intMinQty AS MinOrderQty,
                                v.monCost AS MinCost,
                                i.vcModelID AS ModelId,
                                v.vcPartNo AS PartNo,
                                v.numItemCode AS numItemCode,
								coalesce(CPN.CustomerPartNo,'') AS  CustomerPartNo
      FROM    Item AS i
      INNER JOIN Vendor AS v ON i.numVendorID = v.numVendorID
      LEFT JOIN  CustomerPartNumber CPN  ON i.numItemCode = CPN.numItemCode AND numCompanyId = v_numCompanyID and CPN.numDomainID = v_numDomainId
      WHERE   i.numVendorID = v_numDivisionId
      AND v.numItemCode = i.numItemCode
      AND v.numDomainID = v_numDomainId
      AND (i.vcItemName ilike coalesce(v_keyword,'') || '%'
      OR i.vcModelID ilike coalesce(v_keyword,'') || '%');
      v_VfirstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_VlastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPVITEMS;
      open SWV_RefCur for
      SELECT * FROM(SELECT * ,
                                    ROW_NUMBER() OVER(ORDER BY ItemName) AS RowNumber
         FROM      tt_TEMPVITEMS) a
      WHERE   RowNumber > v_VfirstRec
      AND RowNumber < v_VlastRec
      ORDER BY ItemName;
   end if;
   RETURN;
END; $$; 




