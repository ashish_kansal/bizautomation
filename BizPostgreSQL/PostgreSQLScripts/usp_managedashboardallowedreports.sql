-- Stored procedure definition script USP_ManageDashboardAllowedReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDashboardAllowedReports(v_numDomainId NUMERIC(9,0) ,
v_numGroupId NUMERIC(9,0),
v_strReportIds VARCHAR(2000),
v_strKPIGroupReportIds VARCHAR(2000),
v_strPredefinedReportIds TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ReportDashboardAllowedReports where numDomainID = v_numDomainId AND numGrpID = v_numGroupId;

   IF v_strReportIds <> '' then
	
      INSERT INTO ReportDashboardAllowedReports(numDomainID,numGrpID,numReportID,tintReportCategory)
      select v_numDomainId,v_numGroupId,x.Id,0 from(select Id from SplitIDs(v_strReportIds,',')) x;
   end if;

   IF v_strKPIGroupReportIds <> '' then
	
      INSERT INTO ReportDashboardAllowedReports(numDomainID,numGrpID,numReportID,tintReportCategory)
      select v_numDomainId,v_numGroupId,x.Id,1 from(select Id from SplitIDs(v_strKPIGroupReportIds,',')) x;
   end if;

   IF v_strPredefinedReportIds <> '' then
	
      INSERT INTO ReportDashboardAllowedReports(numDomainID,numGrpID,numReportID,tintReportCategory)
      select v_numDomainId,v_numGroupId,x.Id,2 from(select Id from SplitIDs(v_strPredefinedReportIds,',')) x;
      DELETE FROM
      ReportDashboard
      WHERE
      numUserCntID IN(SELECT numUserDetailId FROM UserMaster WHERE numDomainID = v_numDomainId AND numGroupID = v_numGroupId)
      AND numReportID IN(SELECT
      numReportID
      FROM
      ReportListMaster
      WHERE
      coalesce(numDomainID,0) = 0
      AND coalesce(bitDefault,false) = true
      AND intDefaultReportID NOT IN(SELECT
         numReportID
         FROM
         ReportDashboardAllowedReports
         WHERE
         numDomainID = v_numDomainId
         AND numGrpID = v_numGroupId
         AND tintReportCategory = 2));
   end if;
   RETURN;
END; $$;


