-- Stored procedure definition script USP_RecurringBillRemaining for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecurringBillRemaining(v_numOppID NUMERIC ,
v_numDomainID NUMERIC,
INOUT v_numOppRecID NUMERIC DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_newPercentage  DOUBLE PRECISION;
BEGIN
   IF EXISTS(SELECT * FROM OpportunityRecurring
   WHERE numOppId = v_numOppID AND numDomainID = v_numDomainID AND coalesce(numOppBizDocID,0) > 0) then
      DELETE FROM OpportunityRecurring WHERE numOppId = v_numOppID AND numDomainID = v_numDomainID
      AND coalesce(numOppBizDocID,0) = 0;
      select(100 -SUM(fltBreakupPercentage)) INTO v_newPercentage FROM OpportunityRecurring WHERE numOppId = v_numOppID AND numDomainID = v_numDomainID;
      IF v_newPercentage > 0.0001 then
		
         INSERT INTO OpportunityRecurring(numOppId,
				numOppBizDocID,
				numRecurringId,
				tintRecurringType,
				dtRecurringDate,
				bitRecurringZeroAmt,
				numNoTransactions,
				bitBillingTerms,
				numBillingDays,
				fltBreakupPercentage,
				numDomainID)
         SELECT 
         numOppId,
				   NULL,
				   numRecurringId,
				   tintRecurringType,
				   LOCALTIMESTAMP,
				   bitRecurringZeroAmt,
				   numNoTransactions,
				   bitBillingTerms,
				   numBillingDays,
				   v_newPercentage,
				   numDomainID
         FROM
         OpportunityRecurring WHERE numOppId = v_numOppID AND numDomainID = v_numDomainID LIMIT 1;
         v_numOppRecID := CURRVAL('OpportunityRecurring_seq');
      ELSE
         v_numOppRecID := 0;
      end if;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_SaveMarketBudgetHeader]    Script Date: 07/26/2008 16:21:07 ******/



