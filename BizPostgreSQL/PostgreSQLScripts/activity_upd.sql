DROP FUNCTION IF EXISTS Activity_Upd;

CREATE OR REPLACE FUNCTION Activity_Upd
(	
	v_AllDayEvent BOOLEAN,		-- whether this is an all-day event
	v_ActivityDescription	TEXT,		-- additional information about the activity
	v_Duration INTEGER,		-- length of activity in seconds
	v_Location VARCHAR(64),	-- where the activity takes place
	v_StartDateTimeUtc TIMESTAMP,	-- when the activity begins
	v_Subject VARCHAR(255),	-- brief description of the activity
	v_EnableReminder BOOLEAN,		-- whether this activity should notify the user
	v_ReminderInterval INTEGER,		-- how long before start time (in seconds) to notify the user
	v_ShowTimeAs INTEGER,		-- how to display the time use of this activity to the user in DayView
	v_Importance INTEGER,		-- priority that this activity has
	v_Status INTEGER,		-- reminder notification status (expired, pending, etc.)
	v_RecurrenceKey	INTEGER,		-- relates this activity to its recurrence pattern info, if any
	v_DataKey INTEGER,		-- the key of the Activity to be updated
	v_VarianceKey UUID,	-- corelates all variances of a recurring activity, otherwise null
	v_ItemIdOccur VARCHAR(250),
	v_tintMode SMALLINT DEFAULT 0,
	v_Color SMALLINT DEFAULT 0,
	v_HtmlLink VARCHAR(500) DEFAULT NULL
)
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	v_dtStartTime  TIMESTAMP;
	 v_dtEndtime  TIMESTAMP;
BEGIN
	IF v_tintMode = 0 then
		UPDATE
			Activity
		SET
			AllDayEvent = v_AllDayEvent,ActivityDescription = v_ActivityDescription,
			Duration = v_Duration,Location = v_Location,StartDateTimeUtc = v_StartDateTimeUtc,
			Subject = v_Subject,EnableReminder = v_EnableReminder,ReminderInterval = v_ReminderInterval,
			ShowTimeAs = v_ShowTimeAs,Importance = v_Importance,
			Status = v_Status,RecurrenceID = v_RecurrenceKey,VarianceID = v_VarianceKey,
			Color = v_Color,HtmlLink = v_HtmlLink
		WHERE
		  Activity.ActivityID = v_DataKey;
	ELSEIF v_tintMode = 1 then
		UPDATE
			Activity
		SET
			Duration = v_Duration,StartDateTimeUtc = v_StartDateTimeUtc
		WHERE
			Activity.ActivityID = v_DataKey;
	end if;


	IF EXISTS(SELECT numActivityId FROM Communication WHERE numActivityId = v_DataKey) then
		v_dtStartTime := v_StartDateTimeUtc;
		v_dtEndtime := v_dtStartTime+CAST(v_Duration || 'second' as interval);
      
		UPDATE Communication SET dtStartTime = v_dtStartTime,dtEndTime = v_dtEndtime WHERE numActivityId = v_DataKey;
   end if;

   RETURN;
END; $$;




