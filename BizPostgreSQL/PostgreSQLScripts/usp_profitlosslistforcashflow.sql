-- Stored procedure definition script USP_ProfitLossListForCashFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProfitLossListForCashFlow(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                          
 v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                            
 v_dtFromDate TIMESTAMP DEFAULT NULL,                                                    
 v_dtToDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;                                                                                                                                              
--Begin                                                                                                                                         
--  Select distinct CA.numAccountId as numAccountId,CA.vcCatgyName as AcntTypeDescription,                          
--  dbo.fn_GetCurrentOpeningBalanceForProfitLossForCashFlow(@numChartAcntId,@dtFromDate, @dtToDate,@numDomainId) As Amount                                        
--  from Chart_Of_Accounts CA                
--  Left outer join ListDetails LD on CA.numAcntType=LD.numListItemID                 
--  --inner join General_Journal_Details GJD on CA.numAccountId=GJD.numChartAcntId                          
--  Where  CA.numAccountId=@numChartAcntId And CA.numDomainId=@numDomainId          
--End


