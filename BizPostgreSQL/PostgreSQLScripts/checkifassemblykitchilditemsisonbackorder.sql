-- Function definition script CheckIfAssemblyKitChildItemsIsOnBackorder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CheckIfAssemblyKitChildItemsIsOnBackorder(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numOppChildItemID NUMERIC(18,0),
	v_numOppKitChildItemID NUMERIC(18,0),
	v_bitKitParent BOOLEAN,
	v_bitWorkOrder BOOLEAN,
	v_numQuantity DOUBLE PRECISION)
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitBackOrder  DOUBLE PRECISION DEFAULT 0;
   v_numOnHand  DOUBLE PRECISION;
   v_numAllocation  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION DEFAULT 0;
   v_numWarehouseItemID  NUMERIC(18,0);
   v_numWOStatus  NUMERIC(18,0);

   v_tintCommitAllocation  SMALLINT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempWOID  NUMERIC(18,0);
BEGIN
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID; 

   IF coalesce(v_numOppChildItemID,0) > 0 AND coalesce(v_numOppKitChildItemID,0) = 0 then
	
      select   coalesce(numQtyShipped,0), coalesce(numWareHouseItemId,0) INTO v_numQtyShipped,v_numWarehouseItemID FROM OpportunityKitItems WHERE numOppChildItemID = v_numOppChildItemID;
      select   coalesce(numWOStatus,0) INTO v_numWOStatus FROM WorkOrder WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID;
   ELSEIF coalesce(v_numOppChildItemID,0) > 0 AND coalesce(v_numOppKitChildItemID,0) > 0
   then
	
      select   coalesce(numQtyShipped,0), coalesce(numWareHouseItemId,0) INTO v_numQtyShipped,v_numWarehouseItemID FROM OpportunityKitChildItems WHERE numOppKitChildItemID = v_numOppKitChildItemID;
      select   coalesce(numWOStatus,0) INTO v_numWOStatus FROM WorkOrder WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID AND numOppKitChildItemID = v_numOppKitChildItemID;
   end if;

   v_numQuantity := v_numQuantity -v_numQtyShipped;
	
   IF v_bitWorkOrder = true then
	
      IF v_numWOStatus = 23184 then
		
         IF v_numQuantity > 0 then
			
            IF(SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID AND numAllocation >= v_numQuantity) > 0 then
				
               v_bitBackOrder := 0;
            ELSE
               v_bitBackOrder := 1;
            end if;
         ELSE
            v_bitBackOrder := 0;
         end if;
      ELSE
         DROP TABLE IF EXISTS tt_CheckIfAssemblyKitChildItemsIsOnBackorder CASCADE;
         CREATE TEMPORARY TABLE tt_CheckIfAssemblyKitChildItemsIsOnBackorder
         (
            ItemLevel INTEGER,
            numParentWOID NUMERIC(18,0),
            numWOID NUMERIC(18,0),
            numItemCode NUMERIC(18,0),
            numQty DOUBLE PRECISION,
            numQtyRequiredToBuild DOUBLE PRECISION,
            numWarehouseItemID NUMERIC(18,0),
            bitWorkOrder BOOLEAN,
            bitReadyToBuild BOOLEAN
         );
         INSERT INTO tt_CheckIfAssemblyKitChildItemsIsOnBackorder(ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numQtyRequiredToBuild
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild) with recursive CTEWorkOrder(ItemLevel,numParentWOID,numWOID,numItemCode,numWarehouseItemID,bitWorkOrder,
         numQty,numQtyRequiredToBuild) AS(SELECT
         CAST(1 AS INTEGER) AS ItemLevel,
					numParentWOID AS numParentWOID,
					numWOId AS numWOID,
					numItemCode AS numItemCode,
					numWareHouseItemId AS numWarehouseItemID,
					true AS bitWorkOrder,
					v_numQuantity AS numQty,
					CAST(1 AS DOUBLE PRECISION) AS numQtyRequiredToBuild
         FROM
         WorkOrder
         WHERE
         numOppId = v_numOppID
         AND numOppItemID = v_numOppItemID
         AND numOppChildItemID = v_numOppChildItemID
         AND coalesce(numOppKitChildItemID,0) = v_numOppKitChildItemID
         UNION ALL
         SELECT
         c.ItemLevel+2 AS ItemLevel,
					c.numWOID AS numParentWOID,
					WorkOrder.numWOId AS numWOID,
					WorkOrder.numItemCode AS numItemCode,
					WorkOrder.numWareHouseItemId AS numWarehouseItemID,
					true AS bitWorkOrder,
					c.numQty*WOD.numQtyItemsReq_Orig AS numQty,
					WOD.numQtyItemsReq_Orig AS numQtyRequiredToBuild
         FROM
         WorkOrder
         INNER JOIN
         CTEWorkOrder c
         ON
         WorkOrder.numParentWOID = c.numWOID
         INNER JOIN
         WorkOrderDetails WOD
         ON
         WOD.numWOId = c.numWOID
         AND WOD.numChildItemID = WorkOrder.numItemCode
         AND coalesce(WOD.numWarehouseItemID,0) = coalesce(WorkOrder.numWareHouseItemId,0)) SELECT
         ItemLevel,
				numParentWOID,
				numWOID,
				numItemCode,
				numQty,
				numQtyRequiredToBuild,
				CTEWorkOrder.numWarehouseItemID,
				bitWorkOrder,
				CAST(0 AS BOOLEAN)
         FROM
         CTEWorkOrder;
         INSERT INTO tt_CheckIfAssemblyKitChildItemsIsOnBackorder(ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild)
         SELECT
         t1.ItemLevel::bigint+1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				t1.numQty*WorkOrderDetails.numQtyItemsReq_Orig,
				WorkOrderDetails.numWarehouseItemID,
				CAST(0 AS BOOLEAN) AS bitWorkOrder,
				CAST(CASE
         WHEN Item.charItemType = 'P'
         THEN
            CASE
            WHEN v_tintCommitAllocation = 2
            THEN(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
            ELSE(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END)
            END
         ELSE 1
         END AS BOOLEAN)
         FROM
         WorkOrderDetails
         INNER JOIN
         tt_CheckIfAssemblyKitChildItemsIsOnBackorder t1
         ON
         WorkOrderDetails.numWOId = t1.numWOID
         INNER JOIN
         Item
         ON
         WorkOrderDetails.numChildItemID = Item.numItemCode
         AND 1 =(CASE WHEN(SELECT COUNT(*) FROM tt_CheckIfAssemblyKitChildItemsIsOnBackorder TInner WHERE TInner.bitWorkOrder = true AND TInner.numParentWOID = t1.numWOID AND TInner.numItemCode = Item.numItemCode) > 0 THEN 0 ELSE 1 END)
         LEFT JOIN
         WareHouseItems
         ON
         WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
         LEFT JOIN
         Warehouses
         ON
         WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
         LEFT JOIN
         OpportunityMaster
         ON
         WorkOrderDetails.numPOID = OpportunityMaster.numOppId;
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMPWO_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPWO CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPWO
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numWOID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMPWO(numWOID) SELECT numWOID FROM tt_CheckIfAssemblyKitChildItemsIsOnBackorder WHERE bitWorkOrder = true ORDER BY ItemLevel DESC;
         select   COUNT(*) INTO v_iCount FROM tt_TEMPWO;
         WHILE v_i <= v_iCount LOOP
            select   numWOID INTO v_numTempWOID FROM tt_TEMPWO WHERE ID = v_i;
            UPDATE
            tt_CheckIfAssemblyKitChildItemsIsOnBackorder T1
            SET
            bitReadyToBuild =(CASE WHEN(SELECT COUNT(*) FROM tt_CheckIfAssemblyKitChildItemsIsOnBackorder TInner WHERE TInner.numParentWOID = T1.numWOID AND bitReadyToBuild = false) > 0 THEN false ELSE true END)
				
            WHERE
            numWOID = v_numTempWOID;
            v_i := v_i::bigint+1;
         END LOOP;
         IF(SELECT COUNT(*) FROM tt_CheckIfAssemblyKitChildItemsIsOnBackorder WHERE coalesce(bitReadyToBuild,false) = false) > 0 then
			
            v_bitBackOrder := 1;
         ELSE
            v_bitBackOrder := 0;
         end if;
      end if;
   ELSEIF v_bitKitParent = true
   then
      DROP TABLE IF EXISTS tt_CheckIfAssemblyKitChildItemsIsOnBackorder1 CASCADE;
      CREATE TEMPORARY TABLE tt_CheckIfAssemblyKitChildItemsIsOnBackorder1
      (
         numItemCode NUMERIC(18,0),
         numParentItemID NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         numOppKitChildItemID NUMERIC(18,0),
         vcItemName VARCHAR(300),
         bitAssembly BOOLEAN,
         bitKitParent BOOLEAN,
         numWarehouseItmsID NUMERIC(18,0),
         numQtyItemsReq DOUBLE PRECISION,
         numQtyItemReq_Orig DOUBLE PRECISION,
         numOnHand DOUBLE PRECISION,
         numAllocation DOUBLE PRECISION,
         numPrentItemBackOrder INTEGER
      );
      IF coalesce(v_numOppChildItemID,0) = 0 then
		
         INSERT INTO
         tt_CheckIfAssemblyKitChildItemsIsOnBackorder1
         SELECT
         OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				0,
				I.vcItemName,
				I.bitAssembly,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig*v_numQuantity,
				OKI.numQtyItemsReq_Orig,
				coalesce(WI.numOnHand,0),
				coalesce(WI.numAllocation,0),
				0
         FROM
         OpportunityKitItems OKI
         INNER JOIN
         WareHouseItems WI
         ON
         OKI.numWareHouseItemId = WI.numWareHouseItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OKI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         Item I
         ON
         OKI.numChildItemID = I.numItemCode
         WHERE
         OI.numoppitemtCode = v_numOppItemID
         AND coalesce(I.bitKitParent,false) = false;
      end if;
      INSERT INTO
      tt_CheckIfAssemblyKitChildItemsIsOnBackorder1
      SELECT
      OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			I.vcItemName,
			I.bitAssembly,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig*t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			coalesce(WI.numOnHand,0),
			coalesce(WI.numAllocation,0),
			0
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      tt_CheckIfAssemblyKitChildItemsIsOnBackorder1 t1
      ON
      OKCI.numOppChildItemID = t1.numOppChildItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode
      AND coalesce(I.bitKitParent,false) = true;
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
      IF EXISTS(SELECT
      numItemCode
      FROM
      tt_CheckIfAssemblyKitChildItemsIsOnBackorder1
      WHERE
      1 =(CASE
      WHEN coalesce(bitAssembly,false) = true
      THEN
         cast(CheckIfAssemblyKitChildItemsIsOnBackorder(v_numDomainID,v_numOppID,v_numOppItemID,numOppChildItemID,numOppKitChildItemID,false,true,numQtyItemsReq) as INTEGER)
      WHEN coalesce(bitKitParent,false) = true
      THEN
         cast(CheckIfAssemblyKitChildItemsIsOnBackorder(v_numDomainID,v_numOppID,v_numOppItemID,numOppChildItemID,numOppKitChildItemID,true,false,numQtyItemsReq) as INTEGER)
      ELSE
         CASE
         WHEN v_tintCommitAllocation = 2
         THEN(CASE WHEN coalesce(numQtyItemsReq,0) >= numOnHand THEN 0 ELSE 1 END)
         ELSE(CASE WHEN coalesce(numQtyItemsReq,0) >= numAllocation THEN 0 ELSE 1 END)
         END
      END)) then
		
         v_bitBackOrder := 1;
      ELSE
         v_bitBackOrder := 0;
      end if;
   ELSE
      select   coalesce(numOnHand,0), coalesce(numAllocation,0) INTO v_numOnHand,v_numAllocation FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWarehouseItemID;
      IF v_tintCommitAllocation = 2 then
		
         IF(v_numQuantity -v_numQtyShipped) > v_numOnHand then
            v_bitBackOrder := 1;
         ELSE
            v_bitBackOrder := 0;
         end if;
      ELSE
         IF(v_numQuantity -v_numQtyShipped) > v_numAllocation then
            v_bitBackOrder := 1;
         ELSE
            v_bitBackOrder := 0;
         end if;
      end if;
   end if;

   RETURN v_bitBackOrder;
END; $$;

