CREATE OR REPLACE FUNCTION USP_TicklerActItems
(
	v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                                          
	v_numDomainID NUMERIC(9,0) DEFAULT null,                                                                        
	v_startDate TIMESTAMP DEFAULT NULL,                                                                        
	v_endDate TIMESTAMP DEFAULT NULL,                                                                    
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,                                                                 
	v_bitFilterRecord BOOLEAN DEFAULT false,
	v_columnName VARCHAR(50) DEFAULT NULL, 
	v_columnSortOrder VARCHAR(50) DEFAULT NULL,
	v_vcBProcessValue VARCHAR(500) DEFAULT NULL,
	v_RegularSearchCriteria TEXT DEFAULT '',
	v_CustomSearchCriteria TEXT DEFAULT '', 
	v_OppStatus INTEGER DEFAULT NULL,
	v_CurrentPage INTEGER DEFAULT NULL,                                                              
	v_PageSize INTEGER DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_tintPerformanceFilter  SMALLINT;
	v_tintActionItemsViewRights  SMALLINT DEFAULT 3;
	
	v_vcSelectedEmployess  VARCHAR(100);
	v_numCriteria  INTEGER;
	v_vcActionTypes  VARCHAR(100);
	
	v_strSql  TEXT;                                                            
	v_strSql1  TEXT;                         
	v_strSql2  TEXT;                                                            
	v_vcCustomColumnName TEXT;
	v_vcnullCustomColumnName TEXT;
	-------Change Row Color-------
	v_vcCSOrigDbCOlumnName  VARCHAR(50);
	v_vcCSLookBackTableName  VARCHAR(50);
	v_vcCSAssociatedControlType  VARCHAR(50);
	v_tintOrder SMALLINT;                                                
	v_vcFieldName VARCHAR(50);                                                
	v_vcListItemType VARCHAR(3);                                           
	v_vcAssociatedControlType VARCHAR(20);                                                
	v_numListID NUMERIC(9,0);                                                
	v_vcDbColumnName VARCHAR(20);                    
	v_WhereCondition VARCHAR(2000);                     
	v_vcLookBackTableName VARCHAR(2000);              
	v_bitCustomField BOOLEAN;
	v_bitAllowEdit CHAR(1);                 
	v_numFieldId NUMERIC;  
	v_bitAllowSorting CHAR(1);   
	v_vcColumnName VARCHAR(500);       
	    
	v_ListRelID  NUMERIC(9,0);             
	v_Prefix  VARCHAR(5);
	
	v_Nocolumns  SMALLINT;         
	v_SQLtemp  TEXT;
	v_strSql3  TEXT;
	v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
	v_numUserGroup  NUMERIC(18,0);
	SWV_RowCount INTEGER;
BEGIN
	v_RegularSearchCriteria := REPLACE(v_RegularSearchCriteria,'.numFollowUpStatus','Div.numFollowUpStatus'); 
	v_RegularSearchCriteria := REPLACE(v_RegularSearchCriteria,'itemDesc','textDetails'); 

	                                                                    
  
	IF EXISTS(SELECT * FROM GroupAuthorization WHERE numDomainID = v_numDomainID AND numModuleID = 1 AND numPageID = 2 AND numGroupID IN(SELECT coalesce(numGroupID,0) FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID)) then
		select   coalesce(intViewAllowed,0) INTO v_tintActionItemsViewRights FROM GroupAuthorization WHERE numDomainID = v_numDomainID AND numModuleID = 1 AND numPageID = 2 AND numGroupID IN(SELECT coalesce(numGroupID,0) FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID);
	end if;

	IF coalesce(POSITION(substring(v_RegularSearchCriteria from E'cmp\\.vcPerformance=1') IN v_RegularSearchCriteria),0) > 0 then --Last 3 Months
		v_tintPerformanceFilter := 1;
	ELSEIF coalesce(POSITION(substring(v_RegularSearchCriteria from E'cmp\\.vcPerformance=2') IN v_RegularSearchCriteria),0) > 0 then --Last 6 Months
		v_tintPerformanceFilter := 2;
	ELSEIF coalesce(POSITION(substring(v_RegularSearchCriteria from E'cmp\\.vcPerformance=3') IN v_RegularSearchCriteria),0) > 0 then --Last 1 Year
		v_tintPerformanceFilter := 3;
	ELSE
		v_tintPerformanceFilter := 0;
	end if;

	IF v_columnName = 'vcPerformance' then
		v_columnName := 'monDealAmount';
	end if;

	select 
		vcSelectedEmployee, numCriteria, vcActionTypes INTO v_vcSelectedEmployess,v_numCriteria,v_vcActionTypes 
	FROM
		TicklerListFilterConfiguration 
	WHERE
		numDomainID = v_numDomainID AND
		numUserCntID = v_numUserCntID;

	drop table IF EXISTS tt_TEMPRECORDS CASCADE;
	Create TEMPORARY TABLE tt_TEMPRECORDS 
	(
	   numContactID NUMERIC(9,0),
	   numDivisionID NUMERIC(9,0),
	   tintCRMType SMALLINT,
	   Id NUMERIC(9,0),
	   bitTask VARCHAR(100),
	   Startdate TIMESTAMP,
	   EndTime TIMESTAMP,
	   itemDesc TEXT,
	   Name VARCHAR(200),
	   vcFirstName VARCHAR(100),
	   vcLastName VARCHAR(100),
	   numPhone VARCHAR(15),
	   numPhoneExtension VARCHAR(7),
	   Phone VARCHAR(30),
	   vcCompanyName VARCHAR(100),
	   vcProfile VARCHAR(100), 
	   vcEmail VARCHAR(50),
	   Task VARCHAR(100),
	   Activity TEXT,
	   Status VARCHAR(100),
	   numCreatedBy NUMERIC(9,0),
	   numRecOwner VARCHAR(500), 
	   numModifiedBy VARCHAR(500), 
	   numOrgTerId NUMERIC(18,0), 
	   numTerId VARCHAR(200),
	   numAssignedTo VARCHAR(1000),
	   numAssignedBy VARCHAR(50),
	   caseid NUMERIC(9,0),
	   vcCasenumber VARCHAR(50),
	   casetimeId NUMERIC(9,0),
	   caseExpId NUMERIC(9,0),
	   type INTEGER,
	   itemid VARCHAR(50),
	   changekey VARCHAR(50),
	   DueDate VARCHAR(50),
	   bitFollowUpAnyTime BOOLEAN,
	   numNoOfEmployeesId VARCHAR(50),
	   vcComPhone VARCHAR(50),
	   numFollowUpStatus VARCHAR(200),
	   dtLastFollowUp VARCHAR(500),
	   vcLastSalesOrderDate VARCHAR(100),
	   monDealAmount DECIMAL(20,5), 
	   vcPerformance VARCHAR(100),
	   vcColorScheme VARCHAR(50),
	   Slp_Name VARCHAR(100),
	   intTotalProgress INTEGER,
	   vcPoppName VARCHAR(100), 
	   numOppId NUMERIC(18,0),
	   numBusinessProcessID NUMERIC(18,0),
	   numCompanyRating VARCHAR(500),
	   numStatusID VARCHAR(500),
	   numCampaignID VARCHAR(500),
	   "Action-Item Participants" TEXT,
	   HtmlLink VARCHAR(500),
	   Location VARCHAR(150),
	   OrignalDescription TEXT
	);                                                         
 
	v_vcCSOrigDbCOlumnName := '';
	v_vcCSLookBackTableName := '';
	v_tintOrder := 0;               

	DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFORM
	(
		tintOrder SMALLINT,
		vcDbColumnName VARCHAR(50),
		vcFieldDataType VARCHAR(50),
		vcOrigDbColumnName VARCHAR(50),
		bitAllowFiltering BOOLEAN,
		vcFieldName VARCHAR(50),
		vcAssociatedControlType VARCHAR(50),
		vcListItemType CHAR(3),
		numListID NUMERIC(9,0),
		vcLookBackTableName VARCHAR(50),
		bitCustomField BOOLEAN,
		numFieldId NUMERIC,
		bitAllowSorting BOOLEAN,
		bitAllowEdit BOOLEAN,
		bitIsRequired BOOLEAN,
		bitIsEmail BOOLEAN,
		bitIsAlphaNumeric BOOLEAN,
		bitIsNumeric BOOLEAN,
		bitIsLengthValidation BOOLEAN,
		intMaxLength INTEGER,
		intMinLength INTEGER,
		bitFieldMessage BOOLEAN,
		vcFieldMessage VARCHAR(500),
		ListRelID NUMERIC(9,0),
		intColumnWidth NUMERIC(18,2)
	);
                
	--Custom Fields related to Organization
	INSERT INTO
		tt_TEMPFORM
	SELECT
		tintRow+1 AS tintOrder,vcDbColumnName,CAST('' AS VARCHAR(50)),vcDbColumnName,CAST(1 AS BOOLEAN),vcFieldName,vcAssociatedControlType,CAST('' AS CHAR(3)) as vcListItemType,numListID,CAST('' AS VARCHAR(50)) as vcLookBackTableName
		,bitCustom,numFieldId,bitAllowSorting,false,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
	FROM
		View_DynamicCustomColumns
	WHERE
		numFormId = 43
		AND numUserCntID = v_numUserCntID
		AND numDomainID = v_numDomainID
		AND tintPageType = 1
		AND coalesce(bitCustom,false) = true;
 
	SELECT 
		tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, 0, ListRelID 
	INTO 
		v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
	FROM
		tt_TEMPFORM 
	ORDER BY
		tintOrder ASC LIMIT 1;     
  
	v_vcCustomColumnName := '';
	v_vcnullCustomColumnName := '';

	WHILE v_tintOrder > 0 LOOP
		v_vcColumnName := v_vcDbColumnName;
		v_strSql := 'ALTER TABLE tt_TEMPRECORDS add "' || coalesce(v_vcColumnName,'') || '" TEXT';

		EXECUTE v_strSql;
		
		IF v_bitCustomField = true then
         IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ', COALESCE((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),'''') "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ','''' AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',COALESCE((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),0)  "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',0 AS "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',FormatedDateFromDate((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),' || COALESCE(v_numDomainID,0) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ','''' AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',(SELECT COALESCE(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1)' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',(SELECT string_agg((SELECT vcData FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(COALESCE((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(50)),1,50) || ' AND CFWInner.RecId=Div.numDivisionID),''''),'','')),'', '')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSE
            v_vcCustomColumnName := CONCAT(v_vcCustomColumnName,',(SELECT fn_GetCustFldStringValue(',v_numFieldId,
            ',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',v_numFieldId,
            ' AND CFW.RecId=Div.numDivisionID)',' "',v_vcColumnName,'"');
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, 0, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
      tt_TEMPFORM WHERE
      tintOrder > v_tintOrder -1   ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;

   v_strSql := '';
   RAISE NOTICE '%',v_vcCustomColumnName;

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 43 AND DFCS.numFormID = 43 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
----------------------------                   


   v_strSql := coalesce(v_strSql,'') || 'insert into tt_TEMPRECORDS
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,CAST(bitTask AS varchar) as bitTask,                                               
 dtStartTime + (' || -COALESCE(v_ClientTimeZoneOffset,0) || ' || '' minute'')::INTERVAL as Startdate,                        
 dtEndTime + (' || -COALESCE(v_ClientTimeZoneOffset,0) || ' || '' minute'')::INTERVAL as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 COALESCE(AddC.vcFirstName,'''') || '' '' || COALESCE(AddC.vcLastName,'''')  as "Name", 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then AddC.numPhone || case when AddC.numPhoneExtension<>'''' then '' - '' || AddC.numPhoneExtension else '''' end  else '''' end as "Phone",                                                                       
 Comp.vcCompanyName As vcCompanyName , COALESCE((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 COALESCE(Div.numTerId,0) AS numOrgTerId,
 (select vcData from ListDetails where numListItemID=Div.numTerId LIMIT 1) AS numTerId,                                            
fn_GetContactName(numAssign) || 
COALESCE((SELECT string_agg(fn_GetContactName(numContactId),'','') FROM CommunicationAttendees WHERE numCommId=Comm.numCommId),'''')
As numAssignedTo , 
fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
comm.caseid as caseid,                          
(select vcCaseNumber from cases where cases.numcaseid= comm.caseid LIMIT 1)as vcCasenumber,                          
COALESCE(comm.casetimeId,0) casetimeId,                          
COALESCE(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,null as DueDate ,COALESCE(bitFollowUpAnyTime,false) bitFollowUpAnyTime,
GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' ||
   CONCAT('COALESCE(FormatedDateFromDate(Div.dtLastFollowUp,',v_numDomainID,'),'''')') || ' AS dtLastFollowUp,'
   || CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate,',v_numDomainID,
   '),'''')') || ' AS vcLastSalesOrderDate,COALESCE(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(TO_CHAR(TempPerformance.monDealAmount,''9,999,999,999,999,999.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance ';

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
      v_strSql := coalesce(v_strSql,'') || ',tCS.vcColorScheme';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ','''' as vcColorScheme';
   end if;

   v_strSql := coalesce(v_strSql,'') || ' ,COALESCE(Slp_Name,'''') AS Slp_Name,COALESCE(intTotalProgress,0) AS "intTotalProgress",COALESCE(vcPoppName,'''') AS "vcPoppName", COALESCE(Opp.numOppId,0) AS numOppId,COALESCE(Opp.numBusinessProcessID,0) AS numBusinessProcessID';
   v_strSql := coalesce(v_strSql,'') || ' ,(select vcData from ListDetails where numListItemID=Comp.numCompanyRating LIMIT 1) AS numCompanyRating ,(select vcData from ListDetails where numListItemID=Div.numStatusID LIMIT 1) AS numStatusID,(SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID LIMIT 1) AS numCampaignID
   ,replace(replace((SELECT CASE(COALESCE(Comp.vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' ||  cast(COALESCE(Addc.numDivisionID,0) as varchar(20)) || '','' ||  cast(COALESCE(Div.tintCRMType,0) as varchar(20)) || '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' || ltrim(RTRIM(vcCompanyName)) ||  ''</a> ''  END
|| CASE(COALESCE(AddC.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' ||  cast(COALESCE(AddC.numContactId,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(AddC.vcFirstName)) || ''</a> ''  END
|| CASE(COALESCE(addc.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' ||  cast(COALESCE(AddC.numContactId,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(AddC.vcLastName)) || ''</a> ''  END 
|| CASE(COALESCE(AddC.numPhone,'''')) WHEN '''' then '''' ELSE AddC.numPhone || '' '' END
|| CASE(COALESCE(AddC.numPhoneExtension,'''')) when '''' then '''' else '' ('' || AddC.numPhoneExtension || '') '' END 
|| CASE(COALESCE(addc.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' || ltrim(RTRIM(AddC.vcEmail)) || '''''') style=''''COLOR:#3c8dbc;''''>'' || ltrim(RTRIM(AddC.vcEmail)) || ''</a> ''  END
),''&lt;'', ''<''), ''&gt;'', ''>'') AS "Action-Item Participants",'''' AS HtmlLink, '''' AS Location,'''' As OrignalDescription';
   v_strSql := coalesce(v_strSql,'') || coalesce(v_vcCustomColumnName,'');
   v_strSql := coalesce(v_strSql,'') || ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON TRUE
 LEFT JOIN LATERAL (' || CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',v_tintPerformanceFilter,
   ' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -3) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -6) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(years => -1) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') || ' AS TempPerformance  ON TRUE
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ';
 

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

	   
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'AddC.';
      ELSEIF v_vcCSLookBackTableName = 'DivisionMaster'
      then
         v_Prefix := 'Div.';
      ELSEIF v_vcCSLookBackTableName = 'CompanyInfo'
      then
         v_Prefix := 'Comp.';
      ELSEIF v_vcCSLookBackTableName = 'Tickler'
      then
         v_Prefix := 'Comm.';
      else
         v_Prefix := '';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
	
         IF v_vcCSOrigDbCOlumnName = 'FormattedDate' then
            v_vcCSOrigDbCOlumnName := 'dtStartTime';
         end if;
         v_strSql := coalesce(v_strSql,'') || ' left join #tempColorScheme tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) >= CAST(timezone(''utc'', now()) + make_interval(mins => cast(tCS.vcFieldValue as int)) AS DATE)
			 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) <= CAST(timezone(''utc'', now()) + make_interval(mins => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'  OR v_vcCSAssociatedControlType = 'TextBox'
      then
	
		--PRINT 1
         v_strSql := coalesce(v_strSql,'') || ' left join #tempColorScheme tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'');
      end if;
   end if;

   v_strSql := coalesce(v_strSql,'') || '  WHERE Comm.numDomainID = ' || COALESCE(v_numDomainID,0)  || ' AND 1 = (CASE ' || SUBSTR(CAST(v_tintActionItemsViewRights AS VARCHAR(30)),1,30) ||
   ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END) ';
 
   IF LENGTH(coalesce(v_vcBProcessValue,'')) > 0 then

      v_strSql := coalesce(v_strSql,'') || ' and COALESCE(Opp.numBusinessProcessID,0) in (SELECT Items FROM Split(''' || coalesce(v_vcBProcessValue,'') || ''','',''))';
   end if;

   IF LENGTH(v_vcActionTypes) > 0 then

      v_strSql := coalesce(v_strSql,'') || ' AND Comm.bitTask IN (' || coalesce(v_vcActionTypes,'') || ')';
   end if;
  
   IF v_numCriteria  = 1 then --Action Items	Assigned

      v_strSql := coalesce(v_strSql,'') || ' AND 1=(Case when Comm.numAssign IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE CAST(COALESCE(v_numUserCntID,0) AS TEXT) END || ') then 1 else 0 end)';
   ELSEIF v_numCriteria = 2
   then --Action Items Owned

      v_strSql := coalesce(v_strSql,'') || ' AND 1=(Case when Comm.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE CAST(COALESCE(v_numUserCntID,0) AS TEXT) END || ' ) then 1 else 0 end)';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' AND 1=(Case when (Comm.numAssign IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE CAST(COALESCE(v_numUserCntID,0) AS TEXT) END || ')  or Comm.numCreatedBy IN (' || CASE WHEN LENGTH(v_vcSelectedEmployess) > 0 THEN v_vcSelectedEmployess ELSE CAST(COALESCE(v_numUserCntID,0) AS TEXT) END || ')) then 1 else 0 end)';
   end if;

        
  
   IF(LENGTH(v_RegularSearchCriteria) > 0) then

      IF coalesce(POSITION(substring(v_RegularSearchCriteria from E'cmp\\.vcPerformance') IN v_RegularSearchCriteria),0) > 0 then

	-- WE ARE MANAGING CONDITION IN OUTER APPLY
         v_strSql := v_strSql;
      ELSE
         v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_RegularSearchCriteria,'');
      end if;
   end if;

   IF LENGTH(v_CustomSearchCriteria) > 0 then

      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_CustomSearchCriteria,'');
   end if;

   v_strSql := coalesce(v_strSql,'') || ' AND (dtStartTime + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') >= ''' || v_startDate || ''' and dtStartTime + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') <= ''' || v_endDate || ''')';                                             

   IF v_OppStatus  = 0 then --OPEN

      v_strSql := coalesce(v_strSql,'') || '  AND Comm.bitclosedflag=false AND Comm.bitTask <> 973  ) As X ';
   ELSEIF v_OppStatus = 1
   then --CLOSED

      v_strSql := coalesce(v_strSql,'') || ' AND Comm.bitclosedflag=true AND Comm.bitTask <> 973  ) As X ';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' AND Comm.bitclosedflag in (false,true) AND Comm.bitTask <> 973  ) As X; ';
   end if;
                                
              

   IF LENGTH(coalesce(v_vcBProcessValue,'')) = 0 AND LENGTH(v_RegularSearchCriteria) = 0 AND LENGTH(v_CustomSearchCriteria) = 0 then

      If LENGTH(coalesce(v_vcActionTypes,'')) = 0 OR EXISTS(SELECT OutParam FROM SplitString(v_vcActionTypes,',') WHERE OutParam = '982') then
	
         v_strSql1 :=  ' insert into tt_TEMPRECORDS 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
startdatetimeutc + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') startdate,                        
startdatetimeutc + make_interval(secs => duration) + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') as endtime,                        
(SELECT DISTINCT  
replace(replace(string_agg(SELECT 
CASE(COALESCE(bitTitle,true)) WHEN true THEN ''<b><font color=red>Title</font></b><br>'' || CASE(COALESCE(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END || ''<br>'' else ''''  END  
|| CASE(COALESCE(bitLocation,true)) WHEN true THEN ''<b><font color=green>Location</font></b><br>'' || CASE(COALESCE(ac.location,'''')) WHEN '''' THEN '''' else ac.location END || ''<br>'' else ''''  END  
|| CASE(COALESCE(bitDescription,true)) when true THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM ActivityDisplayConfiguration where numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' and numUserCntId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || '
),''^<br/>''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS itemDesc,                        
ACI.vcFirstName || '' '' || ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then ACI.numPhone || case when ACI.numPhoneExtension<>'''' then '' - '' || ACI.numPhoneExtension else '''' end  else '''' end as "Phone",                        
 Substring ( Comp.vcCompanyName , 0 , 100 ) || ''..'' As vcCompanyName , COALESCE((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,   
subject as Activity,                   
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
COALESCE(itemid,'''') as itemid  ,                    
changekey  ,null as DueDate,false as bitFollowUpAnyTime,
GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' ||
         CONCAT('COALESCE(FormatedDateFromDate(Div.dtLastFollowUp,',v_numDomainID,'),'''')') || ' AS dtLastFollowUp,'
         || CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate,',v_numDomainID,
         '),'''')') || ' AS vcLastSalesOrderDate,COALESCE(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(TO_CHAR(TempPerformance.monDealAmount,''9,999,999,999,999,999.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS numOppId,0 AS numBusinessProcessID ';
         v_strSql1 := coalesce(v_strSql1,'') || ' ,(select vcData from ListDetails where numListItemID=Comp.numCompanyRating LIMIT 1) AS numCompanyRating ,(select vcData from ListDetails where numListItemID=Comp.numCompanyRating LIMIT 1) AS numCompanyRating,''-'' AS numCampaignID ' || coalesce(v_vcnullCustomColumnName,'');
         v_strSql1 := coalesce(v_strSql1,'') || ' ,(SELECT DISTINCT  
replace(replace(string_agg((SELECT CASE(ISNULL(CI.vcCompanyName,'''')) WHEN '''' THEN ''''		else ''<a href=javascript:OpenCompany('' ||  cast(isnull(ATM.DivisionID,0) as varchar(20)) || '','' ||  cast(isnull(ATM.tintCRMType,0) as varchar(20)) || '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' || ltrim(RTRIM(CI.vcCompanyName)) || ''</a> ''  END  
|| CASE(isnull(ATM.AttendeeFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' ||  cast(isnull(ATM.ContactID,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(ATM.AttendeeFirstName)) || ''</a> ''  END
|| CASE(isnull(ATM.AttendeeLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' ||  cast(isnull(ATM.ContactID,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(ATM.AttendeeLastName)) || ''</a> ''  END 
|| CASE(isnull(ATM.AttendeePhone,'''')) WHEN '''' then '''' ELSE ATM.AttendeePhone || '' '' END
|| CASE(isnull(ATM.AttendeePhoneExtension,'''')) when '''' then '''' else '' ('' || ATM.AttendeePhoneExtension || '') '' END 
|| CASE(isnull(ATM.AttendeeEmail,''''))    when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' || ltrim(RTRIM(ATM.AttendeeEmail)) || '''''') style=''''COLOR:#3c8dbc;''''>'' || ltrim(RTRIM(ATM.AttendeeEmail)) || ''</a> ''  END 
|| CASE(isnull(ATM.ResponseStatus,'''')) WHEN '''' THEN '''' WHEN  ''needsAction''  then '''' ELSE ''<img src=''''../images/'' || ATM.ResponseStatus || ''.gif'''' align=''''BASELINE'''' style=''''float:none;vertical-align:middle;width: 15px;''''/>'' END
FROM ActivityAttendees ATM
Left Join CompanyInfo CI on ATM.CompanyNameinBiz=CI.numCompanyId
WHERE ATM.ActivityID = ac.ActivityID and ATM.AttendeeEmail is not null),''^<br>''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS "Action-Item Participants",ac.HtmlLink AS HtmlLink,ac.Location As Location,ac.ActivityDescription As OrignalDescription';
         v_strSql1 := coalesce(v_strSql1,'') || ' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder  ON TRUE                
 LEFT JOIN LATERAL (' || CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',v_tintPerformanceFilter,
         ' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -3) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -6) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(years => -1) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') || ' AS TempPerformance  ON TRUE
 where  res.numUserCntId = ' || COALESCE(v_numUserCntID,0) || '             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' || COALESCE(v_numDomainID,0) || '  and 
(startdatetimeutc + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') >= ''' || v_startDate || ''' and startdatetimeutc + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') <= ''' || v_endDate || ''')
and status <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' || COALESCE(v_numDomainID,0) || '  
AND 1=(Case ' || SUBSTR(Cast(v_bitFilterRecord as VARCHAR(10)),1,10) || ' when 1 then Case when Comm.numAssign =' || COALESCE(v_numUserCntID,0) || '  then 1 else 0 end 
		else  Case when (Comm.numAssign =' || COALESCE(v_numUserCntID,0) || '  or Comm.numCreatedBy=' || COALESCE(v_numUserCntID,0) || ' ) then 1 else 0 end end)   
		AND 1 = (CASE ' || SUBSTR(CAST(v_tintActionItemsViewRights AS VARCHAR(30)),1,30) ||
         ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and dtStartTime <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
) 
 Order by endtime OFFSET ' || CAST(((v_CurrentPage::bigint -1)*v_PageSize::bigint) AS VARCHAR(50)) || ' ROWS FETCH NEXT ' || SUBSTR(CAST(v_PageSize AS VARCHAR(50)),1,50) || ' ROWS ONLY';
      end if;

-- btDocType =2 for Document approval request, =1 for bizdoc
      v_strSql2 := ' insert into tt_TEMPRECORDS
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
CAST(A.bitTask AS VARCHAR) as bitTask,
dtCreatedDate + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') as Startdate,                        
dtCreatedDate + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') as EndTime ,  
'''' as itemDesc,
vcFirstName || '' '' || vcLastName  as "Name",
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then B.numPhone || case when B.numPhoneExtension<>'''' then '' - '' || B.numPhoneExtension else '''' end  else '''' end as "Phone",
vcCompanyName, COALESCE((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
fn_GetContactName(A.numCreatedBy) as numRecOwner,
fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 COALESCE(E.numTerId,0) AS numOrgTerId, 
 (select vcData from ListDetails where numListItemID=E.numTerId LIMIT 1) AS numTerId, 
case When LENGTH( fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( fn_GetContactName(numAssign) , 0  ,  12 ) || ''..''                                           
  Else fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as "type", 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,false as bitFollowUpAnyTime,
GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' ||
      CONCAT('COALESCE(FormatedDateFromDate(E.dtLastFollowUp,',v_numDomainID,'),'''')') || ' AS dtLastFollowUp,' ||
      CONCAT('COALESCE(FormatedDateFromDate(TempLastOrder.bintCreatedDate,',v_numDomainID,
      '),'''')') || ' AS vcLastSalesOrderDate,COALESCE(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(TO_CHAR(TempPerformance.monDealAmount,''9,999,999,999,999,999.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS numOppId,0 AS numBusinessProcessID ';
      v_strSql2 := coalesce(v_strSql2,'') || ' ,(select vcData from ListDetails where numListItemID=D.numCompanyRating LIMIT 1) AS numCompanyRating ,(select vcData from ListDetails where numListItemID=D.numCompanyRating LIMIT 1) AS numCompanyRating,''-'' AS numCampaignID ' || coalesce(v_vcnullCustomColumnName,'');
      v_strSql2 := coalesce(v_strSql2,'') || ',  
replace(replace((SELECT CASE(COALESCE(vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' ||  cast(COALESCE(B.numDivisionID,0) as varchar(20)) || '','' ||  cast(COALESCE(E.tintCRMType,0) as varchar(20)) || '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' || ltrim(RTRIM(vcCompanyName)) ||  ''</a> ''  END
|| CASE(COALESCE(B.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' ||  cast(COALESCE(B.numContactId,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(B.vcFirstName)) || ''</a> ''  END
|| CASE(COALESCE(B.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' ||  cast(COALESCE(B.numContactId,0) as varchar(20)) || '',1) style=''''COLOR:#dd8047;''''>'' || ltrim(RTRIM(B.vcLastName)) || ''</a> ''  END 
|| CASE(COALESCE(B.numPhone,'''')) WHEN '''' then '''' ELSE B.numPhone || '' '' END
|| CASE(COALESCE(B.numPhoneExtension,'''')) when '''' then '''' else '' ('' || B.numPhoneExtension || '') '' END 
|| CASE(COALESCE(B.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' || ltrim(RTRIM(B.vcEmail)) || '''''') style=''''COLOR:#3c8dbc;''''>'' || ltrim(RTRIM(B.vcEmail)) || ''</a> ''  END 
),''&lt;'', ''<''), ''&gt;'', ''>'') AS "Action-Item Participants",'''' AS HtmlLink,'''' As Location,'''' As OrignalDescription
FROM 
	BizDocAction A 
LEFT JOIN 
	BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder  ON TRUE
LEFT JOIN LATERAL (' || CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',v_tintPerformanceFilter,
      ' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -3) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(months => -6) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN timezone(''utc'', now()) + make_interval(years => -1) AND timezone(''utc'', now()) THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') || ' AS TempPerformance  ON TRUE
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' || COALESCE(v_numDomainID,0) || ' and a.numContactId = ' || COALESCE(v_numUserCntID,0) || '  and  
A.numStatus=0 
and (dtCreatedDate + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') >= ''' || v_startDate || ''' and dtCreatedDate + make_interval(mins => ' || -COALESCE(v_ClientTimeZoneOffset,0) || ') <= ''' || v_endDate || ''')';
   end if;


   v_Nocolumns := 0;       
         
   select   coalesce(count(*),0) INTO v_Nocolumns from View_DynamicColumns where numFormId = 43 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1; 
  
  
   if v_Nocolumns > 0 then

      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
      FROM View_DynamicColumns
      where numFormId = 43 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      order by tintOrder asc;
   else 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = v_numUserGroup) > 0 then
	
         v_IsMasterConfAvailable := true;
      end if;

	--If MasterConfiguration is available then load it otherwise load default columns
      IF v_IsMasterConfAvailable = true then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         43,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,NULL,1,0,intColumnWidth
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 43 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false;
         INSERT INTO tt_TEMPFORM
         SELECT(intRowNum+1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 43 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
         ORDER BY
         tintOrder ASC;
      ELSE
         INSERT INTO tt_TEMPFORM
         select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,coalesce(vcCultureFieldName,vcFieldName),
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = 43 and bitDefault = true AND coalesce(bitSettingField,false) = true AND numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
   end if;                                            

   v_SQLtemp := '; update tt_TEMPRECORDS SET "Action-Item Participants" = SUBSTR("Action-Item Participants",5,LENGTH("Action-Item Participants"))';

   EXECUTE coalesce(v_strSql,'') || coalesce(v_strSql1,'') || coalesce(v_strSql2,'') || coalesce(v_SQLtemp,'');

   v_strSql3 :=   'SELECT COUNT(*) OVER() AS TotalRecords, * FROM tt_TEMPRECORDS ';


   IF POSITION(v_columnName IN v_strSql) > 0 OR POSITION(v_columnName IN v_strSql1) > 0 OR POSITION(v_columnName IN v_strSql2) > 0 then

      v_strSql3 := CONCAT(v_strSql3,' order by ',CASE WHEN v_columnName = 'Action-Item Participants' THEN CONCAT('"','Action-Item Participants','"') ELSE v_columnName END,
      ' ',v_columnSortOrder,' LIMIT ',v_PageSize,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint);
   ELSE
      v_strSql3 := CONCAT(v_strSql3,' order by EndTime Asc LIMIT ',v_PageSize,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint);
   end if;

   OPEN SWV_RefCur FOR EXECUTE v_strSql3;
   OPEN SWV_RefCur2 for SELECT * FROM tt_TEMPFORM order by tintOrder;

   RETURN;
END; $$;
