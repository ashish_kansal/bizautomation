CREATE OR REPLACE FUNCTION USP_RecurringAddOpportunity(v_numOppId               BIGINT,
               v_numOppBizDocID NUMERIC,
               v_numRecurringId         BIGINT,
               v_tintRecurringType   SMALLINT,
               v_dtRecurringDate TIMESTAMP,--Start recurring from given date
               v_bitRecurringZeroAmt BOOLEAN DEFAULT null,
               v_numDomainID NUMERIC DEFAULT NULL,
               v_bitBillingTerms BOOLEAN DEFAULT NULL,
               v_numBillingDays NUMERIC(9,0) DEFAULT NULL,
               v_fltBreakupPercentage DOUBLE PRECISION DEFAULT NULL,
               INOUT v_ValidationMessage VARCHAR(200) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_maxPossibleRecurrence  INTEGER;
   v_numNoTransaction  INTEGER;
   v_bitEndTransactionType  BOOLEAN;
   v_dtEndDate  TIMESTAMP;
   v_vcBreakupPercentage  VARCHAR(2000);
   v_bitBillingTerms1  BOOLEAN;
   v_numBillingDays1  NUMERIC;
   v_fltTotalPercentage  DOUBLE PRECISION;
BEGIN
   select   bitEndTransactionType, numNoTransaction, dtEndDate, vcBreakupPercentage, bitBillingTerms, numBillingDays INTO v_bitEndTransactionType,v_numNoTransaction,v_dtEndDate,v_vcBreakupPercentage,
   v_bitBillingTerms1,v_numBillingDays1 FROM RecurringTemplate WHERE numRecurringId = v_numRecurringId;

      
      --Auth BizDoc Recurring
   IF v_tintRecurringType = 2 then
      
      IF(SELECT COUNT(* ) FROM   OpportunityRecurring OPR WHERE  OPR.numOppId = v_numOppId) > 0 then
		  
         IF v_numOppId <> 0 then
			  
            IF v_numRecurringId = 0 then
				  
               IF  EXISTS(SELECT * FROM OpportunityRecurring WHERE numOppId = v_numOppId AND coalesce(numOppBizDocID,0) > 0) then
					
                  v_ValidationMessage := 'Can not turn off recurring, your option is to remove existing bizdocs created by service and try again.';
                  RETURN;
               end if;
               DELETE FROM OpportunityRecurring WHERE numOppId = v_numOppId;
               RETURN;
            end if;
            UPDATE OpportunityRecurring
            SET    numRecurringId = v_numRecurringId,bitRecurringZeroAmt = v_bitRecurringZeroAmt,
            tintRecurringType = v_tintRecurringType
            WHERE  numOppId = v_numOppId;
         end if;
      end if;
      IF NOT EXISTS(SELECT * FROM OpportunityRecurring WHERE numOppId = v_numOppId) then
		
		
		--Validations
         IF  EXISTS(SELECT * FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND bitAuthoritativeBizDocs = true) then
				
            v_ValidationMessage := 'Can not turn on recurring, your option is to remove existing autoritative bizdocs and try again.';
            RETURN;
         end if;
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            RowNumber INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
            ID DOUBLE PRECISION
         );
         INSERT INTO tt_TEMP(ID)
         SELECT items FROM Split(v_vcBreakupPercentage,'~');
         INSERT INTO OpportunityRecurring(numOppId,
		  	numOppBizDocID,
		  	numRecurringId,
		  	tintRecurringType,
		  	dtRecurringDate,
		  	bitRecurringZeroAmt,
		  	numNoTransactions,
		  	bitBillingTerms,
		  	numBillingDays,
		  	fltBreakupPercentage,
		  	numDomainID)
         SELECT v_numOppId,null,v_numRecurringId,v_tintRecurringType,GetNthRecurringDate(v_dtRecurringDate,X.rownumber,v_numRecurringId::NUMERIC),v_bitRecurringZeroAmt,0,v_bitBillingTerms1,v_numBillingDays1,X.ID AS BreakupPercentage,v_numDomainID
         FROM
         tt_TEMP X;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
      end if;
   end if;
	  	
--Manuall BizDoc BreakDown
   IF v_tintRecurringType = 3 then
      select   SUM(fltBreakupPercentage) INTO v_fltTotalPercentage FROM OpportunityRecurring WHERE numOppId = v_numOppId;
      v_fltTotalPercentage := coalesce(v_fltTotalPercentage,0)+v_fltBreakupPercentage;
      IF v_fltTotalPercentage > 100 then
		
         v_ValidationMessage := 'Total Breakup Percentage is higher than 100%,Please reduce Breakup Percentage and try again.';
         RETURN;
      end if;
		--Validations
--		IF  EXISTS(SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=@numOppID)
--				BEGIN
--					SET @ValidationMessage ='Can not turn on recurring, your option is to remove existing autoritative bizdocs and try again.'
--					RETURN
--		END
--		
		
		
		
		
      INSERT INTO OpportunityRecurring(numOppId,
		  		numOppBizDocID,
		  		numRecurringId,
		  		tintRecurringType,
		  		dtRecurringDate,
		  		bitRecurringZeroAmt,
		  		numNoTransactions,
		  		bitBillingTerms,
		  		numBillingDays,
		  		fltBreakupPercentage,
		  		numDomainID)
      SELECT v_numOppId,
					null,
					null,
					v_tintRecurringType,
					v_dtRecurringDate,
					v_bitRecurringZeroAmt,
					0,
					v_bitBillingTerms,
					v_numBillingDays,
					v_fltBreakupPercentage,
					v_numDomainID;

	--Deferred BizDocs
   ELSEIF v_tintRecurringType = 4
   then
      
      INSERT INTO OpportunityRecurring(numOppId,
		  		numOppBizDocID,
		  		numRecurringId,
		  		tintRecurringType,
		  		dtRecurringDate,
		  		bitRecurringZeroAmt,
		  		numNoTransactions,
		  		bitBillingTerms,
		  		numBillingDays,
		  		fltBreakupPercentage,
		  		numDomainID)
      SELECT v_numOppId,
					v_numOppBizDocID,
					null,
					v_tintRecurringType,
					v_dtRecurringDate,
					v_bitRecurringZeroAmt,
					0,
					v_bitBillingTerms,
					v_numBillingDays,
					v_fltBreakupPercentage,
					v_numDomainID;
   end if;
END; $$;
    
      
      
  
/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/



