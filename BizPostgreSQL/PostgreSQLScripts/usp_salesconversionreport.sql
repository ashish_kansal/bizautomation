-- Stored procedure definition script USP_SalesConversionReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SalesConversionReport( --1: Full Report 2:Company Detail 3:Sales Detail
v_numDomainId NUMERIC,
    v_dtStartDate TIMESTAMP,
    v_dtEndDate TIMESTAMP,
    v_RecordType Integer,  --1: Employee is the Assignee  2: Employee is the Owner
    v_numContactID NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
	v_tintType SMALLINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPUSER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPUSER AS
      SELECT A.numContactId,A.vcFirstName || ' ' || A.vcLastname as EmployeeName,UM.numDomainID 
      from UserMaster UM join AdditionalContactsInformation A on UM.numUserDetailId = A.numContactId
      where UM.numDomainID = v_numDomainId and UM.numDomainID = A.numDomainID AND bitActivateFlag = true
      AND 1 =(CASE WHEN v_numContactID = 0 THEN 1 ELSE CASE WHEN A.numContactId = v_numContactID THEN 1 ELSE 0 END END)
      AND A.numTeam in(select F.numTeam from ForReportsByTeam F
      where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainId AND F.tintType = 53);
 
 
   DROP TABLE IF EXISTS tt_TEMPSALES CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSALES
   (
      numContactID NUMERIC(9,0),
      EmployeeName VARCHAR(100),
      numDivisionID NUMERIC(9,0),
      vcDivisionName VARCHAR(100),
      vcCompanyName VARCHAR(100),
      DVCreatedDate TIMESTAMP,
      tintCRMType SMALLINT,
      LeadPromDate TIMESTAMP,
      ProsPromDate TIMESTAMP,
      TotalSalesIncome DECIMAL(20,5),
      TotalSalesOrder INTEGER,
      FirSalesDate TIMESTAMP,
      FirSalesAmount DECIMAL(20,5),
      DaysTook INTEGER
   );
	
   INSERT INTO tt_TEMPSALES(numContactID,EmployeeName,numDivisionID,vcDivisionName,vcCompanyName,DVCreatedDate,tintCRMType,LeadPromDate,ProsPromDate)
   SELECT tU.numContactId,CAST(tU.EmployeeName AS VARCHAR(100)),DV.numDivisionID,DV.vcDivisionName,CI.vcCompanyName,DV.bintCreatedDate,tintCRMType,bintLeadProm,bintProsProm
   FROM DivisionMaster DV JOIN tt_TEMPUSER tU ON DV.numDomainID = tU.numDomainID
   JOIN CompanyInfo CI ON DV.numCompanyID = CI.numCompanyId
   WHERE 1 =(CASE WHEN v_RecordType = 1 THEN CASE WHEN DV.numAssignedTo = tU.numContactId THEN 1 ELSE 0 END
   WHEN v_RecordType = 2 THEN CASE WHEN DV.numRecOwner = tU.numContactId THEN 1 ELSE 0 END END)
   AND (DV.bintCreatedDate >= v_dtStartDate AND DV.bintCreatedDate <= v_dtEndDate);  

   IF v_tintType = 3 then
	
      open SWV_RefCur for
      SELECT ts1.EmployeeName,ts1.vcCompanyName,ts1.numDivisionID,ts1.tintCRMType,OM.numOppId,OM.vcpOppName,OM.tintopptype,
                        CASE coalesce(OM.tintshipped,0)
      WHEN 0 THEN 'Open'
      WHEN 1 THEN 'Completed'
      END AS vcDealStatus,GetListIemName(OM.numStatus) AS vcStatus,
                        OM.monDealAmount,FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId) AS bintCreatedDate
      FROM   tt_TEMPSALES ts1 JOIN OpportunityMaster OM ON OM.numDivisionId = ts1.numDivisionID
      WHERE OM.tintopptype = 1 AND (OM.bintCreatedDate >= v_dtStartDate AND OM.bintCreatedDate <= v_dtEndDate);
   ELSEIF v_tintType = 2 or v_tintType = 1
   then
		UPDATE 
			tt_TEMPSALES AS ts
		SET 
			TotalSalesIncome = OMS.TotalSalesIncome
			,TotalSalesOrder = OMS.TotalSalesOrder
		FROM
		(
			SELECT 
				OM.numDivisionId
				,SUM(coalesce(OM.monDealAmount,0)) AS TotalSalesIncome
				,COUNT(OM.numOppId) AS TotalSalesOrder 
			from 
				tt_TEMPSALES tS1 
			JOIN 
				OpportunityMaster OM 
			ON 
				OM.numDivisionId = tS1.numDivisionID
			WHERE 
				OM.tintopptype = 1 
				AND (OM.bintCreatedDate >= v_dtStartDate AND OM.bintCreatedDate <= v_dtEndDate)
			GROUP BY 
				OM.numDivisionId
		) OMS, tt_TEMPSALES tS1, OpportunityMaster OM WHERE OMS.numDivisionId = tS.numDivisionID;

      UPDATE tt_TEMPSALES AS ts SET FirSalesAmount = OMS.FirSalesAmount,FirSalesDate = OMS.FirSalesDate,DaysTook = DATE_PART('day',OMS.FirSalesDate -tS.DVCreatedDate)
      FROM(SELECT  OM.numDivisionId,coalesce(OM.monDealAmount,0) AS FirSalesAmount,OM.bintCreatedDate AS FirSalesDate,
		   ROW_NUMBER() over(partition by OM.numDivisionId order by OM.bintCreatedDate) as RowNumber
      from tt_TEMPSALES tS1 JOIN OpportunityMaster OM ON OM.numDivisionId = tS1.numDivisionID
      WHERE OM.tintopptype = 1 AND (OM.bintCreatedDate >= v_dtStartDate AND OM.bintCreatedDate <= v_dtEndDate) LIMIT 1) OMS, tt_TEMPSALES tS1, OpportunityMaster OM WHERE(OMS.numDivisionId = tS.numDivisionID AND OMS.ROWNUMBER = 1);
      IF v_tintType = 2 then
		
         open SWV_RefCur for
         SELECT * FROM tt_TEMPSALES;
      ELSEIF v_tintType = 1
      then
		
         DROP TABLE IF EXISTS tt_TEMPREPORT CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPREPORT ON COMMIT DROP AS
            SELECT 	numContactID,EmployeeName,TotalLP,TotalLPConverted,TotalSales,TotalSO,AvgFirstSales,TotalDaysTook,
			CASE WHEN TotalLPConverted > 0 THEN CAST(TotalDaysTook AS decimal)/TotalLPConverted ELSE 0 END AS AvgDayConvert,
			Cast(CAST((TotalLPConverted::bigint*100)/CAST(TotalLP AS DOUBLE PRECISION) AS DECIMAL(10,2)) as NUMERIC(9,2))  AS  TotalLPConvertedPer
			
            FROM(SELECT numContactID,EmployeeName,COUNT(*) AS TotalLP,
			SUM(CASE WHEN (LeadPromDate >= v_dtStartDate AND LeadPromDate <= v_dtEndDate)
            OR (ProsPromDate >= v_dtStartDate AND ProsPromDate <= v_dtEndDate) THEN 1 ELSE 0 END) AS TotalLPConverted,
			SUM(coalesce(TotalSalesIncome,0)) AS TotalSales,
			SUM(coalesce(TotalSalesOrder,0)) AS TotalSO,
			CASE WHEN SUM(coalesce(TotalSalesOrder,0)) = 0 THEN 0 ELSE SUM(coalesce(FirSalesAmount,0))/SUM(coalesce(TotalSalesOrder,0)) END AS AvgFirstSales,
			SUM(coalesce(DaysTook,0)) AS TotalDaysTook
            FROM tt_TEMPSALES GROUP BY numContactID,EmployeeName) temp;
         open SWV_RefCur for
         SELECT * FROM tt_TEMPREPORT;

         open SWV_RefCur2 for
         SELECT CAST(SUM(TotalLPConvertedPer)/COUNT(*) as NUMERIC(9,2)) AS TotalLPConvertedPer,SUM(TotalLPConverted)/COUNT(*) AS TotalLPConverted,
					SUM(AvgFirstSales) AS AvgFirstSales,SUM(TotalSales) AS TotalSales FROM tt_TEMPREPORT;
      end if;
   end if;	
			
	

   drop table IF EXISTS tt_TEMPUSER CASCADE;
   RETURN; 
		
END; $$;





