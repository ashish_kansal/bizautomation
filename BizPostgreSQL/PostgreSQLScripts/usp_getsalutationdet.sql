CREATE OR REPLACE FUNCTION Usp_GetSalutationdet(v_intType INTEGER,
v_numID NUMERIC
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		CFW_FLD_Values_Cont.Fld_Value as "Fld_Value"
		,CFW_Fld_Master.FLd_label as "FLd_label"
		,recid as "recid"
	from
		CFW_Fld_Master 
	INNER JOIN 
		CFW_FLD_Values_Cont 
	ON 
		CFW_Fld_Master.Fld_id = CFW_FLD_Values_Cont.fld_id 
	where 
		fld_label = 'Salutation' and recid = v_numID;
END; $$;












