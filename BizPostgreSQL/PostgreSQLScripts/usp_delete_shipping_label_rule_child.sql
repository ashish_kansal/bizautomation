-- Stored procedure definition script USP_DELETE_SHIPPING_LABEL_RULE_CHILD for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DELETE_SHIPPING_LABEL_RULE_CHILD(v_numChildShipID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   DELETE FROM ShippingLabelRuleChild
   WHERE numChildShipID = v_numChildShipID;
   RETURN;
END; $$;



