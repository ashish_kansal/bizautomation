-- Stored procedure definition script USP_BizFormWizardMasterConfiguration_GetSelectedFields for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizFormWizardMasterConfiguration_GetSelectedFields(v_numDomainID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_numGroupID NUMERIC(18,0),
	v_numRelCntType NUMERIC(18,0),
	v_tintPageType SMALLINT,
	v_tintColumn INTEGER,
	v_bitGridConfiguration BOOLEAN DEFAULT false,
	v_numFormFieldGroupId NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   open SWV_RefCur for SELECT * FROM(SELECT
      CAST(numFieldId AS VARCHAR(15)) || 'R' AS numFormFieldId,
				coalesce(vcCultureFieldName,vcFieldName) AS vcNewFormFieldName,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
				CAST('R' AS CHAR(1)) as vcFieldType,
                vcAssociatedControlType,
                numListID,
                vcDbColumnName,
                vcListItemType,
                intColumnNum AS intColumnNum,
				intRowNum AS intRowNum,
                false as boolRequired,
                coalesce(numAuthGroupID,0) AS numAuthGroupID,
                CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END AS vcFieldDataType,
                false AS boolAOIField,
                numFormId,
                vcLookBackTableName,
                CAST(numFieldId AS VARCHAR(15)) || '~' ||  CAST(coalesce(bitCustom,false) AS VARCHAR(15)) || '~' || '0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(numFormFieldGroupId,0) AS numFormGroupId
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      numFormId = v_numFormID AND
      numDomainID = v_numDomainID AND
      numAuthGroupID = v_numGroupID AND
      numRelCntType = v_numRelCntType AND
      tintPageType = v_tintPageType AND
      intColumnNum = v_tintColumn AND
      bitCustom = false AND
      bitGridConfiguration = v_bitGridConfiguration AND
      coalesce(numFormFieldGroupId,0) = coalesce(v_numFormFieldGroupId,0)
      UNION
      SELECT
      CAST(numFieldId AS VARCHAR(15)) || 'C' AS numFormFieldId,
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CAST('C' AS CHAR(1)) as vcFieldType,
				vcFieldType as vcAssociatedControlType,
				coalesce(numListID,0) AS numListID,
				vcDbColumnName,
				CASE WHEN numListID > 0 THEN 'LI' ELSE '' END AS vcListItemType,
				intColumnNum AS intColumnNum,
				intRowNum AS intRowNum,
				false as boolRequired,
				0 AS numAuthGroupID,
				CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
				false AS boolAOIField,
				v_numFormID AS numFormID,
				CAST('' AS CHAR(1)) AS vcLookBackTableName,
				CAST(numFieldId AS VARCHAR(15)) || '~1' || '~0' AS vcFieldAndType,
				false AS bitDefaultMandatory,
				coalesce(numFormFieldGroupId,0) AS numFormGroupId
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      numFormId = v_numFormID AND
      numDomainID = v_numDomainID AND
      numAuthGroupID = v_numGroupID AND
      numRelCntType = v_numRelCntType AND
      tintPageType = v_tintPageType AND
      intColumnNum = v_tintColumn AND
      bitCustom = true AND
      bitGridConfiguration = v_bitGridConfiguration AND
      coalesce(numFormFieldGroupId,0) = coalesce(v_numFormFieldGroupId,0)) AS TEMPFINAL
   ORDER BY
   TEMPFINAL.intRowNum;
END; $$;













