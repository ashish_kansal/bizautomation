-- Function definition script fn_GetKitAvailabilityByWarehouse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetKitAvailabilityByWarehouse(v_numKitId NUMERIC(18,0),
    v_numWarehouseID NUMERIC(18,0))
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltAvailable  DOUBLE PRECISION DEFAULT 0;
BEGIN
   with recursive CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS(SELECT
   CAST(0 AS NUMERIC(18,0)) AS numItemKitID,
			numItemCode AS numItemCode,
            DTL.numQtyItemsReq AS numQtyItemsReq,
            CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty
   FROM
   Item
   INNER JOIN
   ItemDetails DTL
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numKitId
   UNION ALL
   SELECT
   DTL.numItemKitID AS numItemKitID,
            i.numItemCode AS numItemCode,
            DTL.numQtyItemsReq AS numQtyItemsReq,
            CAST((DTL.numQtyItemsReq*c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
   FROM
   Item i
   INNER JOIN
   ItemDetails DTL
   ON
   DTL.numChildItemID = i.numItemCode
   INNER JOIN
   CTE c
   ON
   DTL.numItemKitID = c.numItemCode
   WHERE
   DTL.numChildItemID != v_numKitId) select   CAST(FLOOR(MIN(CASE
   WHEN coalesce(numOnHand,0)+coalesce(numAllocation,0) = 0 THEN 0
   WHEN coalesce(numOnHand,0)+coalesce(numAllocation,0) >= numQtyItemsReq AND numQtyItemsReq > 0 THEN(coalesce(numOnHand,0)+coalesce(numAllocation,0))/numQtyItemsReq
   ELSE 0
   END)) AS DOUBLE PRECISION) INTO v_fltAvailable FROM
   cte c
   LEFT JOIN LATERAL(SELECT 
      WareHouseItems.* FROM
      WareHouseItems
      INNER JOIN
      Warehouses W
      ON
      W.numWareHouseID = WareHouseItems.numWareHouseID
      WHERE
      numItemID = c.numItemCode
      AND WareHouseItems.numWareHouseID = v_numWarehouseID
      ORDER BY
      numWareHouseItemID LIMIT 1) WI on TRUE;
        
   RETURN v_fltAvailable;
END; $$;

