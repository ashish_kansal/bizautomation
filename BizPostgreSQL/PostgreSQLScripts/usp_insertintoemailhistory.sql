DROP FUNCTION IF EXISTS USP_InsertIntoEmailHistory;

CREATE OR REPLACE FUNCTION USP_InsertIntoEmailHistory(v_vcMessageTo VARCHAR(5000),                     
v_vcMessageToName VARCHAR(1000) DEFAULT '',              
v_vcMessageFrom VARCHAR(5000) DEFAULT NULL,               
v_vcMessageFromName VARCHAR(100) DEFAULT '',              
v_vcSubject VARCHAR(500) DEFAULT NULL,                          
v_vcBody TEXT DEFAULT NULL,                       
v_vcCC VARCHAR(5000) DEFAULT NULL,                        
v_vcCCName VARCHAR(2000) DEFAULT '',                        
v_vcBCC VARCHAR(5000) DEFAULT '',              
v_bitHasAttachment BOOLEAN DEFAULT false,              
v_vcItemId VARCHAR(1000) DEFAULT '',              
v_vcChangeKey VARCHAR(1000) DEFAULT '',              
v_bitIsRead BOOLEAN DEFAULT true,              
v_vcSize NUMERIC DEFAULT 0,              
v_chrSource CHAR(1) DEFAULT 'O',              
v_tinttype NUMERIC DEFAULT 1,              
v_vcCategory VARCHAR(100) DEFAULT '',          
v_vcAttachmentType VARCHAR(500) DEFAULT '',          
v_vcFileName VARCHAR(500) DEFAULT '',              
v_vcAttachmentItemId VARCHAR(1000) DEFAULT ''  ,        
v_dtReceived  TIMESTAMP DEFAULT NULL  ,      
v_numDomainId NUMERIC DEFAULT NULL  ,    
v_vcBodyText TEXT DEFAULT NULL  ,  
v_numUserCntId NUMERIC(9,0) DEFAULT NULL,
v_IsReplied	BOOLEAN DEFAULT false,
v_numRepliedMailID	NUMERIC(18,0) DEFAULT 0,
v_bitInvisible BOOLEAN DEFAULT false,
v_numOppBizDocID NUMERIC(18,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Identity  NUMERIC(9,0);                
   v_FromName  VARCHAR(1000);                
   v_FromEmailAdd  VARCHAR(1000);                
   v_startPos  INTEGER;                
   v_EndPos  INTEGER;                
   v_EndPos1  INTEGER;
   v_updateinsert  BOOLEAN;
   v_node  NUMERIC(18,0);
   v_numNodeId  NUMERIC(18,0) DEFAULT 0;
   v_numDivisionId  NUMERIC(18,0);
   v_numBizDocId  NUMERIC(18,0);
BEGIN
   v_startPos := POSITION('<' IN v_vcMessageFrom);                
   v_EndPos := POSITION('>' IN v_vcMessageFrom);
	    
   IF v_startPos > 0 then
	
      v_FromEmailAdd :=  SUBSTR(v_vcMessageFrom,v_startPos+1,v_EndPos -v_startPos -1);
      v_startPos := POSITION('"' IN v_vcMessageFrom);
      v_EndPos1 := POSITION('"' IN SUBSTR(v_vcMessageFrom,v_startPos+1,LENGTH(v_vcMessageFrom)));
      v_FromName := SUBSTR(v_vcMessageFrom,v_startPos+1,v_EndPos1 -1);
   ELSE
      v_FromEmailAdd := v_vcMessageFrom;
      v_FromName := v_vcMessageFromName;
   end if;                
	
   IF v_vcItemId = '' then
	
      v_updateinsert  := true;
   ELSE
      IF(SELECT COUNT(*) FROM EmailHistory WHERE vcItemId = v_vcItemId) > 0 then
		
         v_updateinsert  := false;
      ELSE
         v_updateinsert  := true;
      end if;
   end if;    
                  
   IF v_updateinsert = false then
	
      UPDATE
      EmailHistory
      SET
      vcSubject = v_vcSubject,vcBody = v_vcBody,vcBodyText = fn_StripHTML(v_vcBodyText),
      bintCreatedOn = TIMEZONE('UTC',now()),bitHasAttachments = v_bitHasAttachment,
      vcItemId = v_vcItemId,vcChangeKey = v_vcChangeKey,bitIsRead = v_bitIsRead,
      vcSize = SUBSTR(CAST(v_vcSize AS VARCHAR(200)),1,200),chrSource = v_chrSource,
      tintType = v_tinttype,vcCategory = v_vcCategory,dtReceivedOn = v_dtReceived --,IsReplied = @IsReplied           
      WHERE
      vcItemId = v_vcItemId
      AND numDomainID = v_numDomainId;
   ELSE
      v_node := case when v_vcCategory = 'B' then coalesce((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND bitSystem = true AND numFixID = 4),
         0) else 0 end;
      SELECT  numNodeID INTO v_node FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent Mail'     LIMIT 1;
      v_vcMessageTo := coalesce(v_vcMessageTo,'') || '#^#';
      v_vcMessageFrom := coalesce(v_vcMessageFrom,'') || '#^#';
      v_vcCC := coalesce(v_vcCC,'') || '#^#';
      v_vcBCC := coalesce(v_vcBCC,'') || '#^#';
      PERFORM USP_InsertEmailMaster(v_vcMessageTo,v_numDomainId);
      PERFORM USP_InsertEmailMaster(v_vcMessageFrom,v_numDomainId);
      PERFORM USP_InsertEmailMaster(v_vcCC,v_numDomainId);
      PERFORM USP_InsertEmailMaster(v_vcBCC,v_numDomainId);
      IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent Mail') > 0) then
		
         SELECT  coalesce(numNodeID,0) INTO v_numNodeId FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent Mail'     LIMIT 1;
      ELSEIF ((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Outbox') > 0)
      then
		
         SELECT  coalesce(numNodeID,0) INTO v_numNodeId FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Outbox'     LIMIT 1;
      ELSEIF ((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent') > 0)
      then
		
         SELECT  coalesce(numNodeID,0) INTO v_numNodeId FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent'     LIMIT 1;
      ELSEIF ((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent Messages') > 0)
      then
		
         SELECT  coalesce(numNodeID,0) INTO v_numNodeId FROM InboxTreeSort WHERE numDomainID = v_numDomainId AND numUserCntID = v_numUserCntId AND vcNodeName = 'Sent Messages'     LIMIT 1;
      end if;
      INSERT INTO EmailHistory(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tintType,vcCategory,dtReceivedOn,numDomainID,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)
		VALUES(v_vcSubject,v_vcBody,fn_StripHTML(v_vcBodyText),TIMEZONE('UTC',now()),v_bitHasAttachment,v_vcItemId,v_vcChangeKey,v_bitIsRead,CAST(v_vcSize AS VARCHAR(200)),v_chrSource,v_tinttype,v_vcCategory,v_dtReceived,v_numDomainId,v_numUserCntId,v_numNodeId,fn_GetNewvcEmailString(v_vcMessageFrom,v_numDomainId),fn_GetNewvcEmailString(v_vcMessageTo,v_numDomainId),fn_GetNewvcEmailString(v_vcCC,v_numDomainId),fn_GetNewvcEmailString(v_vcBCC,v_numDomainId),v_IsReplied,v_bitInvisible);
		
      v_Identity := CURRVAL('emailHistory_seq');
      UPDATE EmailHistory set numEMailId =(SELECT  Item::NUMERIC FROM DelimitedSplit8K(vcFrom,'$^$') LIMIT 1) WHERE numEmailHstrID = v_Identity;
      UPDATE EmailHistory SET IsReplied = v_IsReplied WHERE numEmailHstrID = v_numRepliedMailID;
      IF coalesce(v_numOppBizDocID,0) > 0 AND coalesce((SELECT bitUsePreviousEmailBizDoc FROM Domain WHERE numDomainId = v_numDomainId),false) = true then
         select   numDivisionId, numBizDocId INTO v_numDivisionId,v_numBizDocId FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId WHERE numOppBizDocsId = v_numOppBizDocID;
         IF coalesce(v_numDivisionId,0) > 0 AND coalesce(v_numBizDocId,0) > 0 then
			
            IF EXISTS(SELECT ID FROm DivisionMasterBizDocEmail WHERE numDomainID = v_numDomainId AND numDivisionID = v_numDivisionId AND numBizDocID = v_numBizDocId) then
				
               UPDATE
               DivisionMasterBizDocEmail
               SET
               vcTo = fn_GetNewvcEmailString(v_vcMessageTo,v_numDomainId),vcCC = fn_GetNewvcEmailString(v_vcCC,v_numDomainId),
               vcBCC = fn_GetNewvcEmailString(v_vcBCC,v_numDomainId)
               WHERE
               numDomainID = v_numDomainId
               AND numDivisionID = v_numDivisionId
               AND numBizDocID = v_numBizDocId;
            ELSE
               INSERT INTO DivisionMasterBizDocEmail(numDomainID
						,numDivisionID
						,numBizDocID
						,vcTo
						,vcCC
						,vcBCC)
					VALUES(v_numDomainId
						,v_numDivisionId
						,v_numBizDocId
						,fn_GetNewvcEmailString(v_vcMessageTo,v_numDomainId)
						,fn_GetNewvcEmailString(v_vcCC,v_numDomainId)
						,fn_GetNewvcEmailString(v_vcBCC,v_numDomainId));
            end if;
         end if;
      end if;
      open SWV_RefCur for SELECT cast(coalesce(v_Identity,0) as NUMERIC(9,0));
   end if;
END; $$;












