-- Stored procedure definition script USP_EDIQueue_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EDIQueue_Insert(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_numOrderStatus NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce((SELECT bit3PL FROM Domain WHERE numDomainId = v_numDomainID),false) = true
   AND coalesce(v_numOrderStatus,0) = 15445 -- 15445: SEND SHIPMENT REQUEST (940)  
   AND NOT EXISTS(SELECT ID FROM EDIQueue WHERE numDomainID = v_numDomainID AND numOppID = v_numOppID AND EDIType = 940 AND ((coalesce(bitSuccess,false) = true AND coalesce(bitExecuted,false) = true) OR coalesce(bitExecuted,false) = false)) then
	
      INSERT INTO EDIQueue(numDomainID
			,numOppID
			,bitExecuted
			,bitSuccess
			,dtCreatedDate
			,numCreatedBy
			,EDIType)
		VALUES(v_numDomainID
			,v_numOppID
			,false
			,false
			,TIMEZONE('UTC',now())
			,v_numUserCntID
			,940);
   ELSEIF coalesce((SELECT bitEDI FROM Domain WHERE numDomainId = v_numDomainID),false) = true
   AND coalesce(v_numOrderStatus,0) = 15447  -- 15445: SEND 856
   AND NOT EXISTS(SELECT ID FROM EDIQueue WHERE numDomainID = v_numDomainID AND numOppID = v_numOppID AND EDIType = 856 AND ((coalesce(bitSuccess,false) = true AND coalesce(bitExecuted,false) = true) OR coalesce(bitExecuted,false) = false))
   then
	
      INSERT INTO EDIQueue(numDomainID
			,numOppID
			,bitExecuted
			,bitSuccess
			,dtCreatedDate
			,numCreatedBy
			,EDIType)
		VALUES(v_numDomainID
			,v_numOppID
			,false
			,false
			,TIMEZONE('UTC',now())
			,v_numUserCntID
			,856);
   end if;
   RETURN;
END; $$;


