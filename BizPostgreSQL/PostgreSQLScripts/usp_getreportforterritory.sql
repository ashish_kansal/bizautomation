-- Stored procedure definition script USP_GetReportForTerritory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReportForTerritory(v_numUserCntId NUMERIC(9,0),      
v_numDomainId NUMERIC(9,0),      
v_tintType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numListitemid as "numListitemid",vcdata as "vcdata" from ForReportsByTerritory
   join Listdetails
   on numListitemid = numterritory
   where ForReportsByTerritory.numUserCntId = v_numUserCntId
   and ForReportsByTerritory.numDomainID = v_numDomainId
   and tintType = v_tintType;
END; $$;












