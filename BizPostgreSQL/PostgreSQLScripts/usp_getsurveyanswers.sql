-- Stored procedure definition script usp_GetSurveyAnswers for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyAnswers(v_numDomainId NUMERIC(9,0),
v_SurveyId NUMERIC   ,
v_QuestionId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numSurID as "numSurID",numAnsID as "numAnsID", vcAnsLabel as "vcAnsLabel"
   FROM SurveyAnsMaster
   WHERE  numSurID = v_SurveyId  and numQID = v_QuestionId;
END; $$;

