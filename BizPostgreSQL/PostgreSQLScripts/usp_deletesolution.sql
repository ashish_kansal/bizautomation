-- Stored procedure definition script USP_DeleteSolution for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSolution(v_numSolutionID NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from CaseSolutions where numSolnID = v_numSolutionID;
   delete from SolutionMaster where numSolnID = v_numSolutionID;
   RETURN;
END; $$;


