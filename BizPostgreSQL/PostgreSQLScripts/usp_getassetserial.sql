-- Stored procedure definition script usp_GetAssetSerial for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAssetSerial(v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numAssetItemDTLID as VARCHAR(255)), cast(numAssetItemID as VARCHAR(255)),cast(coalesce(vcSerialNo,'') as VARCHAR(255)) as vcSerialNo,cast(coalesce(numBarCodeId,0) as VARCHAR(255)) as numBarCodeID,
cast(coalesce(vcModelId,'') as VARCHAR(255)) as vcModelID, cast(dtPurchase as VARCHAR(255)),cast(dtWarrante as VARCHAR(255)), cast(coalesce(vcLocation,'') as VARCHAR(255)) as vcLocation,0 as Op_Flag from CompanyAssetSerial
   where numAssetItemID = v_numItemCode;
END; $$;


/****** Object:  UserDefinedFunction [dbo].[USP_GetAttributes]    Script Date: 07/26/2008 18:13:15 ******/













