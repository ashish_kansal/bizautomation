-- Stored procedure definition script USP_RepCampaignDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RepCampaignDetails(v_numCampaignId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numCampaignID ,
numCampaignStatus ,
numCampaignType ,
intLaunchDate ,
intEndDate ,
numRegion ,
numNoSent ,
monCampaignCost ,
numDomainID ,
fn_GetUserName(numCreatedBy) as CreatedBy,
fn_GetDateTimeFromNumber(bintCreatedDate)  as CreatedDate,
fn_GetUserName(numModifiedBy) as ModifiedBy,
fn_GetDateTimeFromNumber(bintModifiedDate)  as ModifiedDate,
GetIncomeforCampaign(numCampaignID,1)  as TIP,
GetIncomeforCampaign(numCampaignID,2)  as TI,
GetIncomeforCampaign(numCampaignID,3)  as CostMFG,
GetIncomeforCampaign(numCampaignID,4)  as ROI,
fn_GetListItemName(numRegion) as Region,
fn_GetListItemName(numCampaignType) as Type,
fn_GetListItemName(numCampaignStatus) as Status,
fn_GetListItemName(numCampaign) as Campaign
   from CampaignMaster
   where numCampaignID = v_numCampaignId;
END; $$;












