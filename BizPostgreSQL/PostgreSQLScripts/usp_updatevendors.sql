-- Stored procedure definition script USP_UpdateVendors for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateVendors(v_strFieldList TEXT DEFAULT '',    
v_Itemcode NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
	IF SUBSTR(v_strFieldList,1,10) <> '' then
		UPDATE 
			Vendor
		SET 
			vcPartNo = X.PartNo
			,monCost = X.monCost
			,intMinQty = X.intMinQty
			,vcNotes = X.vcNotes
		FROM
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				VendorID NUMERIC(9,0) PATH 'VendorID',
				PartNo VARCHAR(100) PATH 'PartNo',
				monCost DECIMAL(20,5) PATH 'monCost',
				intMinQty INTEGER PATH 'intMinQty',
				vcNotes VARCHAR(300) PATH 'vcNotes'
		) AS X 
		WHERE 
			numVendorID = X.VendorID 
			AND numItemCode =  v_Itemcode;
   end if;
   RETURN;
END; $$;


