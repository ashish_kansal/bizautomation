-- Stored procedure definition script USP_DeleteItemImages for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

Create or replace FUNCTION USP_DeleteItemImages(v_numItemImageIds VARCHAR(100) DEFAULT '',
v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_numItemCode NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numItemImageIds != '' then
	     	 --main        
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEM AS
         SELECT id  FROM  SplitIDs(v_numItemImageIds,',');
      IF EXISTS(SELECT * FROM ItemImages II JOIN tt_TEMPITEM TI ON II.numItemImageId = TI.id
      WHERE numDomainId = v_numDomainId AND numItemCode = v_numItemCode and II.bitDefault = true) then
	          
         DELETE FROM ItemImages WHERE  numDomainId = v_numDomainId AND numItemImageId IN(SELECT id FROM tt_TEMPITEM);
         IF EXISTS(SELECT * FROM ItemImages WHERE numDomainId = v_numDomainId AND numItemCode = v_numItemCode) then
                      
            UPDATE ItemImages  SET bitDefault = true WHERE numItemImageId =(SELECT  cd.numItemImageId FROM ItemImages cd WHERE cd.numDomainId = v_numDomainId AND cd.numItemCode = v_numItemCode LIMIT 1);
         end if;
      ELSE
         DELETE FROM ItemImages WHERE  numDomainId = v_numDomainId AND numItemImageId IN(SELECT id FROM tt_TEMPITEM);
      end if;
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE; --main if 
   ELSEIF v_numItemCode != 0
   then
	   
      DELETE FROM ItemImages WHERE  numDomainId = v_numDomainId AND numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$; --root
--EXEC dbo.USP_DeleteItemImages @numItemCode =197605, @numDomainId= 1 ,@numItemImageIds = '81'


