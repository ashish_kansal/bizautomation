-- Stored procedure definition script USP_ManageWebAPIDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--created by chintan prajapati
create or replace FUNCTION USP_ManageWebAPIDetails(v_numDomainID NUMERIC(9,0),
          v_numWebApiId NUMERIC(9,0),
          v_bitStatus   BOOLEAN
--           @str         AS TEXT
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(* ) FROM   WebAPIDetail WHERE  numDomainID = v_numDomainID AND WebApiId = v_numWebApiId) > 0 then
      
      UPDATE WebAPIDetail
      SET    bitEnableAPI = v_bitStatus
      WHERE  numDomainID = v_numDomainID AND WebApiId = v_numWebApiId;
   ELSE
      INSERT INTO WebAPIDetail(WebApiId,
                    numDomainID,
                    bitEnableAPI)
        VALUES(v_numWebApiId,
                    v_numDomainID,
                    v_bitStatus);
   end if;
   RETURN;
END; $$;


