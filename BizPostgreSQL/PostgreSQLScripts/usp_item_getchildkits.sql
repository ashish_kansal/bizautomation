-- Stored procedure definition script USP_Item_GetChildKits for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetChildKits(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		I.numItemCode AS "numItemCode"
		,I.vcItemName AS "vcItemName"
	FROM
		ItemDetails ID
	INNER JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE
		I.numDomainID = v_numDomainID
		AND ID.numItemKitID = v_numItemCode
	ORDER BY
		I.vcItemName;
END; $$;












