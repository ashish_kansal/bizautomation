-- Stored procedure definition script USP_OppSaveMilestone for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OppSaveMilestone(v_numOppID NUMERIC(9,0) DEFAULT null,                                              
 v_strMilestone TEXT DEFAULT '' ,                        
 v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_status  SMALLINT;                                              
   v_hDoc3  INTEGER;                                              
   v_divId  NUMERIC(9,0);                                              
   v_ConId  NUMERIC(9,0);                                              
   v_tintOpptype  SMALLINT;                                              
   v_numItemCode  NUMERIC(9,0);                                              
   v_numOppItemCode  NUMERIC(9,0);                                              
   v_numUnit  NUMERIC(9,0);                                              
   v_numQtyOnHand  NUMERIC(9,0);
   v_numDivisionID  NUMERIC(9,0);        
   v_tintCRMType  NUMERIC(9,0);        
   v_AccountClosingDate  TIMESTAMP;        
                                        
   v_tintOppStatus  SMALLINT;               
   v_tintShipped  SMALLINT;   
   v_tintCommitAllocation  SMALLINT;
BEGIN
   if SUBSTR(CAST(v_strMilestone AS VARCHAR(10)),1,10) <> '' then

      delete from OpportunityStageDetails
      where numOppStageId not in(SELECT StageID FROM XMLTABLE
													(
														'NewDataSet/Table'
														PASSING 
															CAST(v_strMilestone AS XML)
														COLUMNS
															numID FOR ORDINALITY,
															StageID NUMERIC PATH 'StageID',
															Op_Flag NUMERIC PATH 'Op_Flag'
													))
      and numoppid =  v_numOppID;
      update OpportunityStageDetails set
      vcstagedetail = X.vcstageDetail,numModifiedBy = X.numModifiedBy,bintModifiedDate = TIMEZONE('UTC',now()),
      bintDueDate = CAST(replace(X.bintDueDate,'T',' ') AS TIMESTAMP),
      bintStageComDate = CAST(REPLACE(X.bintStageComDate,'T',' ') AS TIMESTAMP),vcComments = X.vcComments,numAssignTo = X.numAssignTo,
      bitAlert = X.bitAlert,bitstagecompleted = X.bitStageCompleted,numStage = X.numStage,
      vcStagePercentageDtl = x.vcStagePercentageDtl,numTemplateId =  x.numTemplateId,
      tintPercentage = X.tintPercentage,numEvent = X.numEvent,numReminder = X.numReminder,
      numET = X.numET,numActivity = X.numActivity,numStartDate = X.numStartDate,
      numStartTime = X.numStartTime,numStartTimePeriod = X.numStartTimePeriod,
      numEndTime = X.numEndTime,numEndTimePeriod = X.numEndTimePeriod,
      txtCom = X.txtCom,numChgStatus = x.numChgStatus,bitChgStatus = x.bitChgStatus,
      bitClose = x.bitClose,numType = x.numType,numCommActId = x.numCommActId
      From(SELECT * FROM
	  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strMilestone AS XML)
			COLUMNS
				numID FOR ORDINALITY,
				StageID NUMERIC PATH 'StageID'
				,vcstageDetail VARCHAR(100) PATH 'vcstageDetail'
				,numModifiedBy NUMERIC PATH 'numModifiedBy'
				,bintModifiedDate VARCHAR(23) PATH 'bintModifiedDate'
				,bintDueDate VARCHAR(23) PATH 'bintDueDate'
				,bintStageComDate VARCHAR(23) PATH 'bintStageComDate'
				,vcComments VARCHAR(1000) PATH 'vcComments'
				,numAssignTo NUMERIC PATH 'numAssignTo'
				,bitAlert BOOLEAN PATH 'bitAlert'
				,bitStageCompleted BOOLEAN PATH 'bitStageCompleted'
				,Op_Flag NUMERIC PATH 'Op_Flag'
				,numStage NUMERIC PATH 'numStage'
				,vcStagePercentageDtl VARCHAR(500) PATH 'vcStagePercentageDtl'
				,numTemplateId NUMERIC PATH 'numTemplateId'
				,tintPercentage SMALLINT PATH 'tintPercentage'
				,numEvent SMALLINT PATH 'numEvent'
				,numReminder NUMERIC PATH 'numReminder'
				,numET NUMERIC PATH 'numET'
				,numActivity NUMERIC PATH 'numActivity'
				,numStartDate SMALLINT PATH 'numStartDate'
				,numStartTime SMALLINT PATH 'numStartTime'
				,numStartTimePeriod SMALLINT PATH 'numStartTimePeriod'
				,numEndTime SMALLINT PATH 'numEndTime'
				,numEndTimePeriod SMALLINT PATH 'numEndTimePeriod'
				,txtCom VARCHAR(1000) PATH 'txtCom'
				,numChgStatus NUMERIC(9,0) PATH 'numChgStatus'
				,bitChgStatus BOOLEAN PATH 'bitChgStatus'
				,bitClose BOOLEAN PATH 'bitClose'
				,numType NUMERIC(9,0) PATH 'numType'
				,numCommActId NUMERIC(9,0) PATH 'numCommActId'
		) WHERE COALESCE(StageID,0) > 0 AND COALESCE(Op_Flag,0) = 0) X
      where  numOppStageId = X.StageID;
      insert into OpportunityStageDetails(numoppid,vcstagedetail,numStagePercentage,numDomainID,
    numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,
    bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,
    bitstagecompleted,numStage,vcStagePercentageDtl,numTemplateId,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,
numEndTimePeriod, txtCom ,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId,tintPercentage)
      select v_numOppID,
    X.vcstageDetail ,
    X.numStagePercentage ,
    X.numDomainId,
    X.numCreatedBy ,
    TIMEZONE('UTC',now()),--replace(X.bintCreatedDate,'T',' ') ,                                              
	X.numModifiedBy ,
    TIMEZONE('UTC',now()),--replace(X.bintModifiedDate,'T',' ') ,                                              
    replace(X.bintDueDate,'T',' ') ,
    replace(X.bintStageComDate,'T',' ') ,
    X.vcComments ,
    X.numAssignTo,
    X.bitAlert ,
    X.bitStageCompleted,
    X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.numEvent,
 X.numReminder,
 X.numET,
 X.numActivity ,
 X.numStartDate,
 X.numStartTime,
 X.numStartTimePeriod ,
 X.numEndTime ,
 X.numEndTimePeriod,
 X.txtCom  ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose ,x.numType,x.numCommActId,x.tintPercentage from(SELECT * FROM 
 XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strMilestone AS XML)
			COLUMNS
				numID FOR ORDINALITY,
				StageID NUMERIC PATH 'StageID'
				,vcstageDetail VARCHAR(100) PATH 'vcstageDetail'
				,numModifiedBy NUMERIC PATH 'numModifiedBy'
				,bintModifiedDate VARCHAR(23) PATH 'bintModifiedDate'
				,bintDueDate VARCHAR(23) PATH 'bintDueDate'
				,bintStageComDate VARCHAR(23) PATH 'bintStageComDate'
				,vcComments VARCHAR(1000) PATH 'vcComments'
				,numAssignTo NUMERIC PATH 'numAssignTo'
				,bitAlert BOOLEAN PATH 'bitAlert'
				,bitStageCompleted BOOLEAN PATH 'bitStageCompleted'
				,Op_Flag NUMERIC PATH 'Op_Flag'
				,numStage NUMERIC PATH 'numStage'
				,vcStagePercentageDtl VARCHAR(500) PATH 'vcStagePercentageDtl'
				,numTemplateId NUMERIC PATH 'numTemplateId'
				,tintPercentage SMALLINT PATH 'tintPercentage'
				,numEvent SMALLINT PATH 'numEvent'
				,numReminder NUMERIC PATH 'numReminder'
				,numET NUMERIC PATH 'numET'
				,numActivity NUMERIC PATH 'numActivity'
				,numStartDate SMALLINT PATH 'numStartDate'
				,numStartTime SMALLINT PATH 'numStartTime'
				,numStartTimePeriod SMALLINT PATH 'numStartTimePeriod'
				,numEndTime SMALLINT PATH 'numEndTime'
				,numEndTimePeriod SMALLINT PATH 'numEndTimePeriod'
				,txtCom VARCHAR(1000) PATH 'txtCom'
				,numChgStatus NUMERIC(9,0) PATH 'numChgStatus'
				,bitChgStatus BOOLEAN PATH 'bitChgStatus'
				,bitClose BOOLEAN PATH 'bitClose'
				,numType NUMERIC(9,0) PATH 'numType'
				,numCommActId NUMERIC(9,0) PATH 'numCommActId'
		) WHERE COALESCE(StageID,0) = 0 AND COALESCE(Op_Flag,0) = 0) X;

   end if;                                          
         
                                            
   if exists(select * from OpportunityStageDetails where numoppid = v_numOppID and   numStagePercentage = 100 and bitstagecompleted = true) then

      select   numDivisionId, bintAccountClosingDate INTO v_numDivisionID,v_AccountClosingDate from  OpportunityMaster where numOppId = v_numOppID;
      select   tintCRMType INTO v_tintCRMType from DivisionMaster where numDivisionID = v_numDivisionID;
      if v_AccountClosingDate is null then
         update OpportunityMaster set tintoppstatus = 1,bintAccountClosingDate = TIMEZONE('UTC',now()) where numOppId = v_numOppID;
      else
         update OpportunityMaster set tintoppstatus = 1 where numOppId = v_numOppID;
      end if;
      if v_tintCRMType = 0 then
 
         update DivisionMaster set tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
         bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
         where numDivisionID = v_numDivisionID;
      ELSEIF v_tintCRMType = 1
      then
 
         update DivisionMaster set tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
         where numDivisionID = v_numDivisionID;
      end if;
   ELSEIF exists(select * from OpportunityStageDetails where numoppid = v_numOppID and   numStagePercentage = 0 and bitstagecompleted = true)
   then

      select   numDivisionId, bintAccountClosingDate INTO v_numDivisionID,v_AccountClosingDate from  OpportunityMaster where numOppId = v_numOppID;
      select   tintCRMType INTO v_tintCRMType from DivisionMaster where numDivisionID = v_numDivisionID;
      if v_AccountClosingDate is null then
         update OpportunityMaster set tintoppstatus = 2,bintAccountClosingDate = TIMEZONE('UTC',now()) where numOppId = v_numOppID;
      else
         update OpportunityMaster set tintoppstatus = 2 where numOppId = v_numOppID;
      end if;
   end if;                    
                            
   open SWV_RefCur for select cast(numoppitemtCode as VARCHAR(255)) from OpportunityItems where numOppId = v_numOppID;                      
              
              
--Updating the warehouse items              
   select   tintoppstatus, tintopptype, tintshipped, coalesce(tintCommitAllocation,1) INTO v_tintOppStatus,v_tintOpptype,v_tintShipped,v_tintCommitAllocation FROM
   OpportunityMaster
   INNER JOIN
   Domain
   ON
   OpportunityMaster.numDomainId = Domain.numDomainId WHERE
   numOppId = v_numOppID;              
	
   IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
	
      PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,v_numUserCntID);
   end if;
   RETURN;
END; $$;












