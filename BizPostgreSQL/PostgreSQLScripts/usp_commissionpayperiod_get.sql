-- Stored procedure definition script USP_CommissionPayPeriod_Get for PostgreSQL
Create or replace FUNCTION USP_CommissionPayPeriod_Get(v_numDomainID NUMERIC(18,0),
	 v_dtStart DATE,
	 v_dtEnd DATE,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.


   open SWV_RefCur for SELECT * FROM CommissionPayPeriod WHERE numDomainID = v_numDomainID AND dtStart = v_dtStart AND dtEnd = v_dtEnd;
END; $$;












