CREATE OR REPLACE FUNCTION USP_ManageGatewayDTLs(v_numDomainID NUMERIC(9,0), 
v_str XML,
v_numSiteId INTEGER,
v_numPaymentGatewayID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numSiteId,0) > 0 then
      UPDATE Sites SET intPaymentGateWay = v_numPaymentGatewayID WHERE numSiteID = v_numSiteId AND numDomainID = v_numDomainID;
   ELSE
      UPDATE Domain SET intPaymentGateWay = v_numPaymentGatewayID WHERE numDomainId = v_numDomainID;
   end if;
  
   delete from PaymentGatewayDTLID where coalesce(numSiteId,0) = coalesce(v_numSiteId,0) AND numDomainID = v_numDomainID;
                   
	insert into PaymentGatewayDTLID
	(intPaymentGateWay,vcFirstFldValue,vcSecndFldValue,vcThirdFldValue,numDomainID,bitTest,numSiteId)
	select 
		X.intPaymentGateWay
		,X.vcFirstFldValue
		,x.vcSecndFldValue
		,x.vcThirdFldValue
		,v_numDomainID
		,X.bitTest
		,v_numSiteId 
	FROM
		XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			v_str 
		COLUMNS
			id FOR ORDINALITY,
			intPaymentGateWay NUMERIC(9,0) PATH 'intPaymentGateWay',
			vcFirstFldValue VARCHAR(1000) PATH 'vcFirstFldValue',
			vcSecndFldValue VARCHAR(1000) PATH 'vcSecndFldValue',
			vcThirdFldValue VARCHAR(1000) PATH 'vcThirdFldValue'
			,bitTest BOOLEAN PATH 'bitTest'
	) X;	
	
   RETURN;
END; $$;


