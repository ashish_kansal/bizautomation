-- FUNCTION: public.usp_getattributesforselecteditemgroup(numeric, numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getattributesforselecteditemgroup(numeric, numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getattributesforselecteditemgroup(
	v_numdomainid numeric,
	v_numlistid numeric DEFAULT 0,
	v_numitemgroupid numeric DEFAULT NULL::numeric,
	v_numwarehouseid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:04:04 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT cast(numListItemID as VARCHAR(255)),cast(vcData as VARCHAR(255))
   FROM
   Listdetails
   WHERE
   numListItemID IN(SELECT
      DISTINCT cast((fld_value) as NUMERIC)
      FROM
      Item I
      JOIN
      WareHouseItems W
      ON
      I.numItemCode = W.numItemID
      JOIN
      ItemAttributes
      ON
      I.numItemCode = ItemAttributes.numItemCode
      JOIN
      CFW_Fld_Master M
      ON
      ItemAttributes.Fld_ID = M.Fld_id
      WHERE
      I.numDomainID = v_numDomainID
      AND W.numDomainID = v_numDomainID
      AND M.numlistid = v_numListID 
											--AND bitMatrix = 0
      AND numItemGroup = v_numItemGroupID
      AND (W.numWareHouseID = v_numWareHouseID OR coalesce(v_numWareHouseID,0) = 0));
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getattributesforselecteditemgroup(numeric, numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
