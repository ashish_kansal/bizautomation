-- Stored procedure definition script USP_MassSalesFulfillment_GetShippingBizDocForPacking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetShippingBizDocForPacking(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_monCurrentShipRate  DECIMAL(20,5);
   v_numShippingReportID  NUMERIC(18,0);
   v_vcPOppName  VARCHAR(300);
BEGIN
   select   coalesce(numShippingServiceItemID,0) INTO v_numShippingServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   v_monCurrentShipRate := coalesce((SELECT  monPrice FROM OpportunityItems OIInner WHERE OIInner.numOppId = v_numOppID AND numItemCode = v_numShippingServiceItemID LIMIT 1),
   -1);
	


   IF EXISTS(SELECT numShippingReportID FROM ShippingReport WHERE numOppID = v_numOppID AND numOppBizDocId = v_numOppBizDocID) then
      select   numShippingReportId, OpportunityMaster.vcpOppName INTO v_numShippingReportID,v_vcPOppName FROM
      ShippingReport
      INNER JOIN
      OpportunityMaster
      ON
      ShippingReport.numOppID = OpportunityMaster.numOppId WHERE
      ShippingReport.numOppID = v_numOppID
      AND ShippingReport.numOppBizDocId = v_numOppBizDocID    LIMIT 1;
      open SWV_RefCur for
      SELECT
      v_numShippingReportID AS "numShippingReportID"
			,v_vcPOppName AS "vcPOppName"
			,numBoxID AS "numBoxID"
			,numShippingReportID AS "numShippingReportID"
			,vcBoxName AS "vcBoxName"
			,fltTotalWeight AS "fltTotalWeight"
			,fltHeight AS "fltHeight"
			,fltWidth AS "fltWidth"
			,fltLength AS "fltLength"
			,v_monCurrentShipRate AS "monCurrentShipRate"
      FROM
      ShippingBox
      WHERE
      numShippingReportID = v_numShippingReportID;

      open SWV_RefCur2 for
      SELECT
      ShippingReportItems.numBoxID AS "numBoxID"
			,ShippingReportItems.numOppBizDocItemID AS "numOppBizDocItemID"
			,Item.numItemCode AS "numItemCode"
			,Item.vcItemName AS "vcItemName"
			,ShippingReportItems.intBoxQty AS "intBoxQty"
      FROM
      ShippingReportItems
      INNER JOIN
      Item
      ON
      ShippingReportItems.numItemCode = Item.numItemCode
      WHERE
      numShippingReportId = v_numShippingReportID;
   ELSE
      open SWV_RefCur for
      SELECT
      OM.numOppId AS "numOppID"
			,coalesce(OM.intUsedShippingCompany,coalesce(DM.intShippingCompany,0)) AS "numShipVia"
			,coalesce(OM.numShippingService,coalesce(DM.numDefaultShippingServiceID,0)) AS "numShipService"
			,OBD.numOppBizDocsId AS "numOppBizDocsId"
			,OBDI.numOppBizDocItemID AS "numOppBizDocItemID"
			,OM.vcpOppName AS "vcpOppName"
			,I.numContainer AS "numContainer"
			,coalesce(IContainer.vcItemName,'') AS "vcContainer"
			,(CASE WHEN coalesce(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS "numNoItemIntoContainer"
			,coalesce(IContainer.fltWeight,0) AS "fltContainerWeight"
			,coalesce(IContainer.fltHeight,0) AS "fltContainerHeight"
			,coalesce(IContainer.fltWidth,0) AS "fltContainerWidth"
			,coalesce(IContainer.fltLength,0) AS "fltContainerLength"
			,OI.numoppitemtCode AS "numoppitemtCode" 
			,I.numItemCode AS "numItemCode"
			,I.vcItemName AS "vcItemName"
			,coalesce(I.fltWeight,0) AS "fltItemWeight"
			,coalesce(I.fltHeight,0) AS "fltItemHeight"
			,coalesce(I.fltWidth,0) AS "fltItemWidth"
			,coalesce(I.fltLength,0) AS "fltItemLength"
			,coalesce(OBDI.numUnitHour,0) AS "numTotalQty"
			,v_monCurrentShipRate AS "monCurrentShipRate"
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      INNER JOIN
      OpportunityMaster OM
      ON
      OBD.numoppid = OM.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      AND OBDI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item AS I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN
      Item AS IContainer
      ON
      I.numContainer = IContainer.numItemCode
      WHERE
      OM.numOppId = v_numOppID
      AND OBD.numOppBizDocsId = v_numOppBizDocID
      AND coalesce(I.bitContainer,false) = false
      AND (OI.numoppitemtCode = v_numOppItemID OR coalesce(v_numOppItemID,0) = 0);
   end if;
   RETURN;
END; $$;


