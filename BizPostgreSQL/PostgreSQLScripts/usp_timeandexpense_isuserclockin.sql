-- Stored procedure definition script USP_TimeAndExpense_IsUserClockIn for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TimeAndExpense_IsUserClockIn(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitUserOnSalary  BOOLEAN DEFAULT true;
   v_bitUserClockedIn  BOOLEAN DEFAULT false;
BEGIN
   select(CASE WHEN coalesce(tintPayrollType,0) = 2 THEN 1 ELSE 0 END) INTO v_bitUserOnSalary FROM
   UserMaster WHERE
   numDomainID = v_numDomainID
   AND numUserDetailId = v_numUserCntID;

   IF EXISTS(SELECT numCategoryHDRID FROM timeandexpense WHERE numUserCntID = v_numUserCntID AND numCategory = 2 AND numtype = 7 AND dtToDate IS NULL) then
	
      v_bitUserClockedIn := true;
   ELSE
      v_bitUserClockedIn := false;
   end if;

   open SWV_RefCur for SELECT
   v_bitUserOnSalary AS bitUserOnSalary
		,v_bitUserClockedIn AS bitUserClockedIn;
END; $$;












