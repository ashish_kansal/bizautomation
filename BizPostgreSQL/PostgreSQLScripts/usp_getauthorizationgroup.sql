-- Stored procedure definition script USP_GetAuthorizationGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAuthorizationGroup(v_numDomainID NUMERIC(18,0),  
	v_vcGroupTypes VARCHAR(200), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(coalesce(v_vcGroupTypes,'')) = 0 then
	
      open SWV_RefCur for
      SELECT
      numGroupID
			,vcGroupName
			,tintGroupType
			,bitConsFlag
			,CASE
      WHEN tintGroupType IN(1,4,5,6) THEN 'Internal Access'
      WHEN tintGroupType = 2 THEN 'Portal Access'
      WHEN tintGroupType = 3 THEN 'Partner Access'
      END AS GroupType
			,0 AS Op_flag
			,(CAST(numGroupID AS VARCHAR(10)) || '~' || CAST(tintGroupType AS CHAR(1))) AS numGroupID1
      FROM
      AuthenticationGroupMaster
      WHERE
      numDomainID = v_numDomainID;
   ELSE
      open SWV_RefCur for
      SELECT
      numGroupID
			,vcGroupName
			,tintGroupType
			,bitConsFlag
			,CASE
      WHEN tintGroupType IN(1,4,5,6) THEN 'Internal Access'
      WHEN tintGroupType = 2 THEN 'Portal Access'
      WHEN tintGroupType = 3 THEN 'Partner Access'
      END AS GroupType
			,0 as Op_flag
			,CONCAT(numGroupID,'~',coalesce(tintGroupType,1)) AS numGroupID1
      FROM
      AuthenticationGroupMaster
      WHERE
      numDomainID = v_numDomainID
      AND tintGroupType IN(SELECT Id FROM SplitIDs(v_vcGroupTypes,','));
   end if;
   RETURN;
END; $$;


