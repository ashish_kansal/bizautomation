-- Function definition script GetCommisionDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCommisionDtls(v_numUserID NUMERIC(9,0),v_dtFrom TIMESTAMP,v_dtTo TIMESTAMP)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitMainComm  BOOLEAN;
   v_bitRoleComm  BOOLEAN;
   v_fltMainCommPer DOUBLE PRECISION;
   v_TotMoney  DECIMAL(20,5);
BEGIN
   select   bitMainComm, fltMainCommPer, bitRoleComm INTO v_bitMainComm,v_fltMainCommPer,v_bitRoleComm from UserMaster where numUserId = v_numUserID;

   if v_bitMainComm = true then

      select   sum(monPAmount)*v_fltMainCommPer INTO v_TotMoney from OpportunityMaster where tintopptype = 1 and tintoppstatus = 1 and numCreatedBy = v_numUserID
      and bintAccountClosingDate between v_dtFrom and v_dtTo;
   end if;

   if v_bitRoleComm = true then

      v_TotMoney := v_TotMoney+0;
   end if;

   return coalesce(v_TotMoney,0);
END; $$;

