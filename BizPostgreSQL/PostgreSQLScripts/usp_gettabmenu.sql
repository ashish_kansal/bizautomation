-- Stored procedure definition script Usp_GetTabMenu for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_GetTabMenu(v_numListID NUMERIC(9,0) DEFAULT 0,      
v_numDomainID NUMERIC(9,0) DEFAULT 0 ,
v_bitMode BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if    v_bitMode = true then
 
      open SWV_RefCur for
      SELECT 6 AS parentid ,numListItemID, vcData,'../prospects/frmCompanyList.aspx?RelId=' ||
      CAST(numListItemID AS VARCHAR(10)) AS vcNavURL,CAST(null as INTEGER) as vcImageURL
      FROM Listdetails WHERE numListID = v_numListID and numListItemID <> 46
      and(numDomainid = v_numDomainID or constFlag = true)
      union
      select    FRDTL.numPrimaryListItemID as parentid,
            0 as numlistitemid,
            L2.vcData as vcPageNavName,
            '../prospects/frmCompanyList.aspx?RelId='
      || CAST(FRDTL.numPrimaryListItemID AS VARCHAR(10)) || '&profileid='
      || CAST(numSecondaryListItemID AS VARCHAR(10)) as vcNavURL,
            CAST(null as INTEGER) as vcImageURL
      from      FieldRelationship FR
      join FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FRDTL.numFieldRelID
      join Listdetails L1 on numPrimaryListItemID = L1.numListItemID
      join Listdetails L2 on numSecondaryListItemID = L2.numListItemID
      where     numPrimaryListItemID <> 46
      and FR.numPrimaryListID = 5
      and FR.numSecondaryListID = 21
      and FR.numDomainID = v_numDomainID
      and L2.numDomainid = v_numDomainID
      order by  parentid;
   else
      open SWV_RefCur for
      SELECT numListItemID, vcData FROM Listdetails WHERE numListID = v_numListID and numListItemID <> 46
      and(numDomainid = v_numDomainID or constFlag = true)
      ORDER BY vcData;
   end if;
   RETURN;
END; $$;


