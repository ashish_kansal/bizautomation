-- Stored procedure definition script USP_ShortCutGrpConf_SetDefaultURL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShortCutGrpConf_SetDefaultURL(v_numGroupID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numTabId NUMERIC(18,0),
	v_numLinkID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE ShortCutGrpConf SET bitInitialPage = false WHERE numDomainId = v_numDomainID AND numGroupId = v_numGroupID AND numTabId = v_numTabId AND bitInitialPage = true;

   IF EXISTS(SELECT * FROM ShortCutGrpConf WHERE numDomainId = v_numDomainID AND numGroupId = v_numGroupID AND numTabId = v_numTabId AND numLinkId = v_numLinkID) then
		
      UPDATE ShortCutGrpConf SET bitInitialPage = true WHERE numDomainId = v_numDomainID AND numGroupId = v_numGroupID AND numTabId = v_numTabId AND numLinkId = v_numLinkID;
   ELSE
      INSERT INTO ShortCutGrpConf(numGroupId,numLinkId,numDomainId,numTabId,bitInitialPage)
			VALUES(v_numGroupID,v_numLinkID,v_numDomainID,v_numTabId,true);
   end if;
   RETURN;
END; $$;



