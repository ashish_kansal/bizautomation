-- Stored procedure definition script usp_GetActionItem for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetActionItem(v_numUserID NUMERIC(9,0)   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from QBActionItem where numUserID = v_numUserID and bitStatus = false;
END; $$;












