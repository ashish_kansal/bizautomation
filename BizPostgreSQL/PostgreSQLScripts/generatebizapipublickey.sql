-- Function definition script GenerateBizAPIPublicKey for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GenerateBizAPIPublicKey()
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_chars  CHAR(62);
   v_PublicKey  VARCHAR(10);
   v_IsUnique  BOOLEAN;
BEGIN
   v_IsUnique := false;
   v_chars := N'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';

   WHILE (v_IsUnique = false) LOOP
      v_PublicKey := SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1)
      || SUBSTR(v_chars,CAST(((SELECT rnd FROM View_Random)*LENGTH(v_chars)) AS INTEGER)+1,1);
      IF(SELECT COUNT(*) FROM UserMaster WHERE vcBizAPIPublicKey = v_PublicKey) = 0 then
         v_IsUnique := true;
      end if;
   END LOOP;
  
   return v_PublicKey;
END; $$;

