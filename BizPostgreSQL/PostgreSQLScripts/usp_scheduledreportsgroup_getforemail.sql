-- Stored procedure definition script USP_ScheduledReportsGroup_GetForEmail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_GetForEmail(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   ScheduledReportsGroup.numDomainID
		,ScheduledReportsGroup.numCreatedBy 
		,ScheduledReportsGroup.ID 
		,ScheduledReportsGroup.numEmailTemplate
		,coalesce(ScheduledReportsGroup.vcSelectedTokens,'') AS vcSelectedTokens
		,coalesce(ScheduledReportsGroup.vcRecipientsEmail,'') AS vcRecipientsEmail
		,coalesce(ScheduledReportsGroup.vcReceipientsContactID,'')AS vcReceipientsContactID
		,coalesce(ScheduledReportsGroup.tintDataCacheStatus,0) AS tintDataCacheStatus
		,coalesce(Domain.vcPSMTPDisplayName,'') AS vcFromName
		,coalesce(Domain.vcPSMTPUserName,'') AS vcFromEmail
		,coalesce(Domain.bitPSMTPServer,false) AS bitPSMTPServer
		,coalesce(Domain.vcLogoForBizTheme,'') AS vcLogoForBizTheme
		,coalesce(Domain.vcThemeClass,'') AS vcThemeClass
   FROM
   ScheduledReportsGroup
   INNER JOIN
   Domain
   ON
   ScheduledReportsGroup.numDomainID = Domain.numDomainId
   WHERE
   1 =(CASE
   WHEN coalesce(v_numDomainID,0) > 0 AND coalesce(v_numSRGID,0) > 0
   THEN(CASE WHEN ScheduledReportsGroup.numDomainID = v_numDomainID AND ScheduledReportsGroup.ID = v_numSRGID THEN 1 ELSE 0 END)
   ELSE(CASE WHEN (dtNextDate <= TIMEZONE('UTC',now()) OR (dtNextDate IS NULL AND dtStartDate <= TIMEZONE('UTC',now()))) AND coalesce(intNotOfTimeTried,0) < 5 THEN 1 ELSE 0 END)
   END);
END; $$;












