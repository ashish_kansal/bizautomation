-- Stored procedure definition script USP_StagePercentageDetails_GetMilestonesByProcess for PostgreSQL
CREATE OR REPLACE FUNCTION USP_StagePercentageDetails_GetMilestonesByProcess(v_numDomainID NUMERIC(18,0)                            
	,v_numProcessID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   vcMileStoneName AS "vcMileStoneName"
   FROM
   StagePercentageDetails
   WHERE
   numdomainid = v_numDomainID
   AND slp_id = v_numProcessID
   GROUP BY
   vcMileStoneName;
END; $$;












