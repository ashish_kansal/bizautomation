-- Stored procedure definition script usp_GetCampaignDetailsFor for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCampaignDetailsFor(v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numCampaignId NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(vcCampaignNumber as VARCHAR(255)),
		cast(textSubject as VARCHAR(255)),
		cast(intNoOfContacts as VARCHAR(255)),
		cast(textMsgDocument as VARCHAR(255)),
		cast(vcEmailId as VARCHAR(255))
   FROM
   Campaign
   WHERE numCreatedBy = v_numCreatedBy
   and numDomainID = v_numDomainID
   and numCampaignID = v_numCampaignId;
END; $$;












