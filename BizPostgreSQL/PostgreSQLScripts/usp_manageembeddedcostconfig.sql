-- Stored procedure definition script USP_ManageEmbeddedCostConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEmbeddedCostConfig(v_numOppBizDocID NUMERIC,
    v_numLastViewedCostCatID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM    EmbeddedCostConfig
   WHERE   numOppBizDocID = v_numOppBizDocID
   AND numDomainID = v_numDomainID) then
            
      INSERT  INTO EmbeddedCostConfig(numOppBizDocID,
                          numLastViewedCostCatID,
                          numDomainID)
                VALUES(v_numOppBizDocID,
                          v_numLastViewedCostCatID,
                          v_numDomainID);
   ELSE
      UPDATE  EmbeddedCostConfig
      SET     numLastViewedCostCatID = v_numLastViewedCostCatID
      WHERE   numOppBizDocID = v_numOppBizDocID;
   end if;
   RETURN;
END; $$;


