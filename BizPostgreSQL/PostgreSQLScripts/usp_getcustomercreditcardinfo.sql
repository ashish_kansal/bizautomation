CREATE OR REPLACE FUNCTION USP_GetCustomerCreditCardInfo(v_numOppBizDocId NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numUserCntId NUMERIC(9,0) DEFAULT 0,
    v_bitFlag BOOLEAN DEFAULT false,
    v_numCCInfoID NUMERIC(9,0) DEFAULT NULL,
    v_IsDefault BOOLEAN DEFAULT false,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numContactId  NUMERIC(18,0);
BEGIN
   IF v_bitFlag = false  AND v_IsDefault = false then
      select   OM.numContactId INTO v_numContactId FROM    OpportunityMaster OM
      INNER JOIN OpportunityBizDocs BD ON OM.numOppId = BD.numoppid WHERE   BD.numOppBizDocsId = v_numOppBizDocId;
      open SWV_RefCur for
      SELECT  numCCInfoID,
                        CC.numContactId,
                        vcCardHolder,
                        vcCreditCardNo,
                        vcCVV2,
                        coalesce(numCardTypeID,0) AS numCardTypeID,
                        tintValidMonth,
                        intValidYear
      FROM    CustomerCreditCardInfo CC
      INNER JOIN AdditionalContactsInformation ADC ON CC.numContactId = ADC.numContactId
      INNER JOIN Domain D ON D.numDomainId = ADC.numDomainID
      WHERE   CC.numContactId = v_numContactId
      AND D.bitSaveCreditCardInfo = true;
   ELSE
      IF v_numDivisionID > 0 then
            
         open SWV_RefCur for
         SELECT   CC.*,Listdetails.vcData
         FROM    CustomerCreditCardInfo  CC
         INNER JOIN AdditionalContactsInformation ADC ON CC.numContactId = ADC.numContactId
         LEFT  JOIN Listdetails ON Listdetails.numListItemID = CC.numCardTypeID
         WHERE   CC.numContactId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID) AND ADC.numDomainID = v_numDomainId 
         AND (CC.numCCInfoID = v_numCCInfoID OR v_numCCInfoID = 0)
         ORDER BY CC.bitIsDefault DESC;
      ELSEIF v_numCCInfoID = 0 AND v_IsDefault = false
      then
            
         open SWV_RefCur for
         SELECT   CC.*,Listdetails.vcData
         FROM    CustomerCreditCardInfo  CC
         INNER JOIN AdditionalContactsInformation ADC ON CC.numContactId = ADC.numContactId
         LEFT  JOIN Listdetails ON Listdetails.numListItemID = CC.numCardTypeID
         WHERE   CC.numContactId = v_numUserCntId AND ADC.numDomainID = v_numDomainId;
      ELSE
         IF v_IsDefault = false then
				
            open SWV_RefCur for
            SELECT   CC.*,Listdetails.vcData
            FROM    CustomerCreditCardInfo  CC
            INNER JOIN AdditionalContactsInformation ADC ON CC.numContactId = ADC.numContactId
            LEFT  JOIN Listdetails ON Listdetails.numListItemID = CC.numCardTypeID
            WHERE  ADC.numDomainID = v_numDomainId AND CC.numCCInfoID = v_numCCInfoID;
         ELSE
            open SWV_RefCur for
            SELECT   CC.*,Listdetails.vcData
            FROM    CustomerCreditCardInfo  CC
            INNER JOIN AdditionalContactsInformation ADC ON CC.numContactId = ADC.numContactId
            LEFT  JOIN Listdetails ON Listdetails.numListItemID = CC.numCardTypeID
            WHERE   CC.numContactId = v_numUserCntId AND ADC.numDomainID = v_numDomainId AND CC.bitIsDefault = true;
         end if;
      end if;
   end if;
   RETURN;
END; $$;



