-- Function definition script fn_IsKitCanBeBuild for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_IsKitCanBeBuild(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numQty DOUBLE PRECISION
	,v_numWarehouseID NUMERIC(18,0)
	,v_vcSelectedKitChildItems TEXT)
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitKitCandBeBuild  BOOLEAN;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      numItemCode NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numUOMId NUMERIC(18,0),
      bitKitParent BOOLEAN,
      bitFirst BOOLEAN,
      numOnHand DOUBLE PRECISION
   );
   DROP TABLE IF EXISTS tt_TEMPEXISTINGITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEXISTINGITEMS
   (
      vcItem VARCHAR(100)
   );
   INSERT INTO tt_TEMPEXISTINGITEMS(vcItem)
   SELECT
   OutParam
   FROM
   SplitString(v_vcSelectedKitChildItems,',');

   DROP TABLE IF EXISTS tt_TEMPSELECTEDKITCHILDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDKITCHILDS
   (
      ChildKitItemID NUMERIC(18,0),
      ChildKitWarehouseItemID NUMERIC(18,0),
      ChildKitChildItemID NUMERIC(18,0),
      ChildKitChildWarehouseItemID NUMERIC(18,0),
      ChildQty DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPSELECTEDKITCHILDS(ChildKitItemID
		,ChildKitChildItemID
		,ChildQty)
   SELECT
   Reverse(ParseName(Replace(Reverse(vcItem),'-','.'),1))
		,Reverse(ParseName(Replace(Reverse(vcItem),'-','.'),2))
		,Reverse(ParseName(Replace(Reverse(vcItem),'-','.'),3))
   FROM(SELECT
      vcItem
      FROM
      tt_TEMPEXISTINGITEMS) As x;

   UPDATE
   tt_TEMPSELECTEDKITCHILDS
   SET
   ChildKitWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID = ChildKitItemID AND WareHouseItems.numWareHouseID = v_numWarehouseID ORDER BY coalesce(WarehouseLocation.numWLocationID,0) ASC LIMIT 1),ChildKitChildWarehouseItemID =(SELECT  numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID = ChildKitChildItemID AND WareHouseItems.numWareHouseID = v_numWarehouseID ORDER BY coalesce(WarehouseLocation.numWLocationID,0) ASC LIMIT 1);


   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0 then
	
      INSERT INTO tt_TEMPITEMS(numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand)
      SELECT
      I.numItemCode
			,ChildQty
			,I.numBaseUnit
			,CAST(0 AS BOOLEAN)
			,coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE numItemID = I.numItemCode AND numWareHouseID = WI.numWareHouseID),0)
      FROM
      tt_TEMPSELECTEDKITCHILDS T1
      INNER JOIN
      Item I
      ON
      T1.ChildKitChildItemID = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      T1.ChildKitChildWarehouseItemID = WI.numWareHouseItemID
      WHERE
      coalesce(ChildKitItemID,0) = 0;
   ELSE
      INSERT INTO tt_TEMPITEMS(numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand) with recursive CTE(numItemCode,vcItemName,bitKitParent,bitCalAmtBasedonDepItems,numQtyItemsReq,
      numUOMId) AS(SELECT
      ID.numChildItemID AS numItemCode,
				I.vcItemName AS vcItemName,
				coalesce(I.bitKitParent,false) AS bitKitParent,
				coalesce(I.bitCalAmtBasedonDepItems,false) AS bitCalAmtBasedonDepItems,
				CAST((v_numQty*coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQtyItemsReq,
				coalesce(numUOMId,0) AS numUOMId
      FROM
      ItemDetails ID
      INNER JOIN
      Item I
      ON
      ID.numChildItemID = I.numItemCode
      WHERE
      numItemKitID = v_numItemCode
      AND coalesce(I.charItemType,0) = 'P'
      AND 1 =(CASE
      WHEN coalesce(I.bitKitParent,false) = true AND LENGTH(coalesce(v_vcSelectedKitChildItems,'')) > 0 THEN(CASE
         WHEN numChildItemID IN(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS)
         THEN 1
         ELSE 0
         END)
      ELSE(CASE
         WHEN(SELECT COUNT(*) FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0) > 0
         THEN(CASE
            WHEN numChildItemID IN(SELECT ChildKitChildItemID FROM tt_TEMPSELECTEDKITCHILDS WHERE coalesce(ChildKitItemID,0) = 0)
            THEN 1
            ELSE 0
            END)
         ELSE 1
         END)
      END)
      UNION ALL
      SELECT
      ID.numChildItemID AS numItemCode,
				I.vcItemName AS vcItemName,
				coalesce(I.bitKitParent,false) AS bitKitParent,
				coalesce(I.bitCalAmtBasedonDepItems,false) AS bitCalAmtBasedonDepItems,
				CAST((coalesce(Temp1.numQtyItemsReq,0)*coalesce(ID.numQtyItemsReq,0)*coalesce(fn_UOMConversion(coalesce(ID.numUOMID,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0)),1)) AS DOUBLE PRECISION) AS numQtyItemsReq,
				coalesce(ID.numUOMID,0) AS numUOMId
      FROM
      CTE As Temp1
      INNER JOIN
      ItemDetails ID
      ON
      ID.numItemKitID = Temp1.numItemCode
      INNER JOIN
      Item I
      ON
      ID.numChildItemID = I.numItemCode
      WHERE
      Temp1.bitKitParent = true
      AND coalesce(I.charItemType,0) = 'P'
      AND EXISTS(SELECT ChildKitItemID FROM tt_TEMPSELECTEDKITCHILDS T2 WHERE coalesce(Temp1.bitKitParent,false) = true AND T2.ChildKitItemID = ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)) SELECT
      numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE numItemID = numItemCode AND numWareHouseID = v_numWarehouseID),0)
      FROM
      CTE;
   end if;

   IF EXISTS(SELECT * FROM tt_TEMPITEMS WHERE coalesce(bitKitParent,false) = false AND numQtyItemsReq > numOnHand) then
	
      v_bitKitCandBeBuild := false;
   ELSE
      v_bitKitCandBeBuild := true;
   end if;

   RETURN v_bitKitCandBeBuild;
END; $$;

