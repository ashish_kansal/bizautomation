-- Stored procedure definition script USP_ManageTaxItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTaxItem(v_numTaxItemID NUMERIC(9,0),
v_vcTaxName VARCHAR(100),
v_numDomainID NUMERIC(9,0),
v_numAccountID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numSalesBizDocFormFieldID  NUMERIC(9,0);
   v_numPurBizDocFormFieldID  NUMERIC(9,0);
   v_numBizDocSummaryFormFieldID  NUMERIC(9,0);
--declare @numChartOfAcntID as numeric(9)
   v_numFieldID  NUMERIC(9,0);
BEGIN
   if v_numTaxItemID = 0 then

      insert into DycFieldMaster(numModuleID, vcFieldName, vcFieldType, vcAssociatedControlType, vcDbColumnName, vcListItemType, numListID, bitDeleted, vcFieldDataType, vcOrigDbCOlumnName, bitAllowEdit, vcLookBackTableName, bitDefault, "order", bitInResults, numDomainID)
values(3,v_vcTaxName,'R','EditBox',v_vcTaxName,'',0,'False','M','vcTax','True','Item','False','0','True',v_numDomainID);

      v_numFieldID := CURRVAL('DycFieldMaster_seq');
      insert into DycFormField_Mapping(numModuleID,numFormID,numFieldID, numDomainID)
values(3,7,v_numFieldID,v_numDomainID);

      insert into DycFormField_Mapping(numModuleID,numFormID,numFieldID, numDomainID)
values(3,8,v_numFieldID,v_numDomainID);

      insert into DycFormField_Mapping(numModuleID,numFormID,numFieldID, numDomainID)
values(3,16,v_numFieldID,v_numDomainID);

--Commented by chintan -Account ID Will be provided form frontend
--declare @numAccounid as numeric(9)
--select top 1 @numAccounid=numAccountId from  Chart_Of_Accounts where numParntAcntId is null and numDomainId=@numDomainId
--Insert into Chart_Of_Accounts(numParntAcntId,numAcntType,vcCatgyName,vcCatgyDescription,numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,chBizDocItems,numDomainId)                                                      
--Values(@numAccounid,827,@vcTaxName,@vcTaxName,Null,Null,getutcdate(),1,0,'OT',@numDomainId) 
--set @numChartOfAcntID=@@identity


      insert into TaxItems(vcTaxName, numSalesBizDocFormFieldID,numPurBizDocFormFieldID, numBizDocSummaryFormFieldID, numDomainID,numChartofAcntID)
values(v_vcTaxName,v_numFieldID,v_numFieldID,v_numFieldID,v_numDomainID,v_numAccountID);
   else
      SELECT numSalesBizDocFormFieldID,numPurBizDocFormFieldID,numBizDocSummaryFormFieldID,numChartofAcntID INTO v_numSalesBizDocFormFieldID,v_numPurBizDocFormFieldID,v_numBizDocSummaryFormFieldID,
      v_numAccountID FROM TaxItems  where numTaxItemID = v_numTaxItemID LIMIT 1;
      update  TaxItems SET vcTaxName = v_vcTaxName  where numTaxItemID = v_numTaxItemID;
      update DycFieldMaster set vcFieldName = v_vcTaxName,vcDbColumnName = v_vcTaxName where numFieldID = v_numSalesBizDocFormFieldID;
   end if;
   RETURN;
END; $$;


