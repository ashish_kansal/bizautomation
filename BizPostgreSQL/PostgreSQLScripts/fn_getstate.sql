-- Function definition script fn_GetState for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetState(v_numValueID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
   DECLARE
   v_vcRetValue  VARCHAR(250);
BEGIN
   select   vcState INTO v_vcRetValue FROM State WHERE numStateID = v_numValueID;
   RETURN v_vcRetValue;
END; $$;

