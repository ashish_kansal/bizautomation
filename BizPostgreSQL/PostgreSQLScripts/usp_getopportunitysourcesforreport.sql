CREATE OR REPLACE FUNCTION usp_GetOpportunitySourcesForReport(              
v_numDomainID NUMERIC,              
        v_dtFromDate TIMESTAMP,              
        v_dtToDate TIMESTAMP,              
        v_numUserCntID NUMERIC DEFAULT 0,                     
        v_intType NUMERIC DEFAULT NULL,              
        v_tintRights NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 1 then
        
      open SWV_RefCur for
      SELECT  coalesce(LD.vcData,'None') as Source, COUNT(OM.numOppId) AS Total,
   (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM--, ListDetails LD              
         Left Outer Join Listdetails LD  on OM.tintSource = LD.numListItemID
         WHERE --LD.numListItemID = tintSource               
     --AND LD.numListID = 18              
         OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
         and OM.numrecowner = v_numUserCntID) as Percentage
      FROM OpportunityMaster OM
      Left Outer Join Listdetails LD  on OM.tintSource = LD.numListItemID
      WHERE               
    --AND LD.numListID = 18              
      OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = 0
      and OM.numrecowner = v_numUserCntID
      AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY OM.tintSource,LD.vcData;
   end if;              
              
   IF v_tintRights = 2 then
        
      open SWV_RefCur for
      SELECT coalesce(LD.vcData,'None') as Source, COUNT(OM.numOppId) AS Total,
   (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM--, ListDetails LD,AdditionalContactsInformation ADC   
         Left Outer Join Listdetails LD  on OM.tintSource = LD.numListItemID
         Join AdditionalContactsInformation ADC On  OM.numrecowner = ADC.numContactId
         WHERE  OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
         and ADC.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)) as Percentage
      FROM OpportunityMaster OM--, ListDetails LD,AdditionalContactsInformation ADC                         
      Left Outer Join Listdetails LD  on OM.tintSource = LD.numListItemID
      Join AdditionalContactsInformation ADC On  OM.numrecowner = ADC.numContactId
      WHERE OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = 0
      AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      and ADC.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      GROUP BY OM.tintSource,LD.vcData;
   end if;              
              
   IF v_tintRights = 3 then               
        -- TERRITORY RECORDS RIGHTS              
        
      open SWV_RefCur for
      SELECT  coalesce(LD.vcData,'None') as Source, COUNT(OM.numOppId) AS Total,
   (COUNT(OM.numOppId)*100.00)/(select Count(*) from OpportunityMaster OM--, ListDetails LD, DivisionMaster DM    
         Left Outer Join Listdetails LD  On OM.tintSource = LD.numListItemID
         Join DivisionMaster DM On OM.numDivisionId = DM.numDivisionID
         WHERE OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                 
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate) as Percentage
      FROM OpportunityMaster OM
      Left Outer Join Listdetails LD  On OM.tintSource = LD.numListItemID
      Join DivisionMaster DM On OM.numDivisionId = DM.numDivisionID
      WHERE OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = 0
      AND DM.numTerID in(select F.numTerritory from ForReportsByTerritory F   -- Added By Anoop                 
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      AND OM.bintCreatedDate BETWEEN v_dtFromDate AND v_dtToDate
      GROUP BY OM.tintSource,LD.vcData;
   end if;
   RETURN;
END; $$;


