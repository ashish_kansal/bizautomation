-- Stored procedure definition script USP_RecurringForBizDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- exec [dbo].USP_RecurringForBizDocs 1730
Create or replace FUNCTION USP_RecurringForBizDocs(v_numOppId NUMERIC(9,0)  DEFAULT 0,
               INOUT v_numOppBizDocId NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN

  --- Obsolete procedure
  
   RETURN;
--    DECLARE  @numRecurringId NUMERIC(9,0)
--    DECLARE  @bitEndTransactionType BIT
--    DECLARE  @numNoTransaction NUMERIC(9,0)
--    DECLARE  @numBizDocId NUMERIC(9,0)
--    DECLARE  @numUserCntID NUMERIC(9,0)
--    --DECLARE  @numOppBizDocsId NUMERIC(9,0)
--    DECLARE  @vcComments VARCHAR(1000)
--    DECLARE  @vcPurchaseOdrNo VARCHAR(100)
--    DECLARE  @bitPartialFulfillment BIT
--    --DECLARE  @strBizDocItems TEXT
--    DECLARE  @bitDiscountType BIT
--    DECLARE  @fltDiscount FLOAT
--    DECLARE  @monShipCost DECIMAL(20,5)
--    DECLARE  @numShipVia NUMERIC(9,0)
--    DECLARE  @vcTrackingURL VARCHAR(1000)
--    DECLARE  @bitBillingTerms BIT
--    DECLARE  @intBillingDays INT
--    DECLARE  @bitInterestType BIT
--    DECLARE  @fltInterest FLOAT
--    DECLARE  @numShipDoc NUMERIC(9,0)
--    DECLARE  @numBizDocStatus NUMERIC(9,0)
--    DECLARE  @tintOppType TINYINT
--    DECLARE  @numDomainId NUMERIC(9,0)
--    
--    SELECT @bitBillingTerms = [bitBillingTerms],
--           @intBillingDays = [intNetDays],
--           @numRecurringId = numRecurringId
--    FROM   [OpportunityRecurring]
--    WHERE  [numOppId] = @numOppId
--    PRINT @numRecurringId
--    SELECT	@tintOppType = tintOppType,
--			@numDomainId = numDomainId,
--			@numUserCntID = numRecOwner
--    FROM   [OpportunityMaster]
--    WHERE numOppId = @numOppId
--    
--	SELECT @numBizDocId = CASE @tintOppType 
--                        WHEN 1 THEN [numAuthoritativeSales]
--                        WHEN 2 THEN [numAuthoritativePurchase]
--                        ELSE [numAuthoritativePurchase]
--                      END
--    FROM   [AuthoritativeBizDocs]
--    WHERE  [numDomainId] =@numDomainId
--    PRINT @numBizDocId
--    
--    SELECT 
--           @bitEndTransactionType = bitEndTransactionType,
--           @numNoTransaction = numNoTransaction
--    FROM   [RecurringTemplate]
--	WHERE numRecurringId = @numRecurringId
--    
--   
--    
--  PRINT 'in'
--  DECLARE  @numOppBizDocsId NUMERIC(9,0)
--	SET @numOppBizDocsId = 0
--  -- Make sure Procedure creates adds all items with unit qty(1) for recurring
--  EXEC [dbo].[USP_CreateBizDocs]
--    @numOppId = @numOppId , --  numeric(9, 0)
--    @numBizDocId = @numBizDocId , --  numeric(9, 0)
--    @numUserCntID = @numUserCntID , --  numeric(9, 0)
--    @numOppBizDocsId = @numOppBizDocsId OUTPUT , --  numeric(9, 0)
--    @vcComments = NULL , --  varchar(1000)
--    @vcPurchaseOdrNo = NULL , --  varchar(100)
--    @bitPartialFulfillment = 0 , --  bit
--    @strBizDocItems = '' , --  text
--    @bitDiscountType = 0 , --  bit
--    @fltDiscount = 0 , --  float
--    @monShipCost = 0 , --  DECIMAL(20,5)
--    @numShipVia = 0 , --  numeric(9, 0)
--    @vcTrackingURL = NULL , --  varchar(1000)
--    @bitBillingTerms = @bitBillingTerms , --  bit
--    @intBillingDays = @intBillingDays , --  int
--    @bitInterestType = 0 , --  bit
--    @fltInterest = 0 , --  float
--    @numShipDoc = 0 , --  numeric(9, 0)
--    @numBizDocStatus = 0,--  numeric(9, 0)
--    @bitRecurringBizDoc =1 -- Will create bizdocs with unit quantity
--  
--   PRINT @numOppBizDocsId
--   IF @numOppBizDocsId <> 0
--     BEGIN
--		SET @numOppBizDocId = @numOppBizDocsId
--		UPDATE OpportunityBizDocs   SET    bitAuthoritativeBizDocs = 1   WHERE  numOppBizDocsId = @numOppBizDocsId
--		INSERT INTO RecurringTransactionReport
--                  (tintRecType,
--                   numRecTranSeedId,
--                   numRecTranOppID,
--                   numRecTranBizDocID,
--                   dteOppCreationDate)
--		VALUES     (2,
--                   @numOppId,
--                   NULL,
--                   @numOppBizDocsId,
--                   getutcdate())
--                   
--		 IF(@bitEndTransactionType =0) --Stop after date 
--			BEGIN
--				-- update [OpportunityRecurring]  for last recurring date 
--				UPDATE [OpportunityRecurring] 
--				SET [dtLastRecurringDate] = GETUTCDATE()
--				WHERE [numOppId] = @numOppId
--			END
--			IF(@bitEndTransactionType =1) --Stop after no of transaction
--			BEGIN
--				--update [OpportunityRecurring] for No of Recurrnce transaction & last recurring date
--				UPDATE [OpportunityRecurring] 
--				SET [numNoTransactions] = ISNULL([numNoTransactions],0) +1,
--					[dtLastRecurringDate] = GETUTCDATE()
--				WHERE [numOppId] = @numOppId
--			END
--               
--     END
--     ELSE
--		SET @numOppBizDocId = 0
--     
     -- GO back to front end and perform accounting using class
    
         
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_RecurringForOpportunity]    Script Date: 03/25/2009 15:13:37 ******/



