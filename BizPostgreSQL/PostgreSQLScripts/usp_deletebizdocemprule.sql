-- Stored procedure definition script USP_DeleteBizDocEmpRule for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DeleteBizDocEmpRule(v_numDomainId NUMERIC(18,0),
v_numBizDocTypeID NUMERIC(18,0),
v_RangeFrom NUMERIC(18,2),
v_RangeTo NUMERIC(18,2),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   delete from BizDocApprovalRuleEmployee where numBizDocAppID in(select numBizDocAppID from BizDocApprovalRule where numBizDocTypeId = v_numBizDocTypeID and
   numDomainID = v_numDomainId and
   numRangeFrom = v_RangeFrom and
   numRangeTo = v_RangeTo);

   delete from BizDocApprovalRule where numBizDocTypeId = v_numBizDocTypeID and
   numDomainID = v_numDomainId and
   numRangeFrom = v_RangeFrom and
   numRangeTo = v_RangeTo;
   open SWV_RefCur for select 1;
   RETURN;
END; $$;














