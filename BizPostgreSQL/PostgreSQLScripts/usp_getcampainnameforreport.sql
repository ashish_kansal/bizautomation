CREATE OR REPLACE FUNCTION usp_GetCampainNameForReport(v_byteMode SMALLINT,
    v_numUserCntID NUMERIC DEFAULT 0,
    v_numDomainID NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
      open SWV_RefCur for
      SELECT  numCampaignID,
                vcCampaignName || '(' || CAST(intLaunchDate AS VARCHAR(20)) || '-'
      || CAST(intEndDate AS VARCHAR(20)) || ')' AS CampaignName
      FROM    CampaignMaster
      WHERE CampaignMaster.numDomainID = v_numDomainID;
   end if;    
    
   IF v_byteMode = 1 then
      open SWV_RefCur for
      SELECT  Cam.numCampaignID,
                vcCampaignName || '(' || CAST(intLaunchDate AS VARCHAR(20)) || '-'
      || CAST(intEndDate AS VARCHAR(20)) || ')' AS CampaignName
      FROM    ForReportsByCampaign ReptCam
      JOIN CampaignMaster Cam ON Cam.numCampaignID = ReptCam.numCampaignID
      WHERE   ReptCam.numUserCntID = v_numUserCntID
      AND Cam.numDomainID = v_numDomainID;
   end if;
                
   IF v_byteMode = 2 then
      open SWV_RefCur for
      SELECT  numCampaignID,
                vcCampaignName
      FROM    CampaignMaster
      WHERE   numDomainID = v_numDomainID
      AND CampaignMaster.bitActive = true;
   end if;
   RETURN;
END; $$;                                       


