-- Stored procedure definition script USP_GetReportDashboardDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReportDashboardDTL(v_numDomainID NUMERIC(18,0),               
v_numDashBoardID NUMERIC(18,0),
v_numReportID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numReportID,0) > 0 AND coalesce(v_numDashBoardID,0) = 0 then
	
      open SWV_RefCur for
      SELECT
      v_numDomainID AS numDomainID
			,ReportListMaster.numReportID
			,0 AS numUserCntID
			,ReportListMaster.tintReportType
			,NULL AS tintChartType
			,coalesce(ReportListMaster.vcReportName,'') AS vcHeaderText
			,NULL AS vcFooterText
			,NULL AS tintRow
			,NULL AS tintColumn
			,NULL AS vcXAxis
			,NULL AS vcYAxis
			,NULL AS vcAggType
			,NULL AS tintReportCategory
			,NULL AS intHeight
			,NULL AS intWidth
			,NULL AS numDashboardTemplateID
			,NULL AS bitNewAdded
			,NULL AS vcTimeLine
			,NULL AS vcGroupBy
			,NULL AS vcTeritorry
			,NULL AS vcFilterBy
			,NULL AS vcFilterValue
			,NULL AS dtFromDate
			,NULL AS dtToDate
			,NULL AS numRecordCount
			,NULL AS tintControlField
			,NULL AS vcDealAmount
			,NULL AS tintOppType
			,NULL AS lngPConclAnalysis
			,0 AS bitTask
			,0 AS tintTotalProgress
			,0 AS tintMinNumber
			,0 AS tintMaxNumber
			,0 AS tintQtyToDisplay
			,NULL AS vcDueDate
			,false AS bitDealWon
			,false AS bitGrossRevenue
			,false AS bitRevenue
			,false AS bitGrossProfit
			,false AS bitCustomTimeline
			,false AS bitBilledButNotReceived
			,false AS bitReceviedButNotBilled
			,false AS bitSoldButNotShipped
			,coalesce(ReportListMaster.bitDefault,false) AS bitDefault
			,coalesce(ReportListMaster.intDefaultReportID,0) AS intDefaultReportID
      FROM
      ReportListMaster
      WHERE
			(ReportListMaster.numDomainID = v_numDomainID OR coalesce(ReportListMaster.numDomainID,0) = 0)
      AND ReportListMaster.numReportID = v_numReportID;
   ELSE
      open SWV_RefCur for
      SELECT
      ReportDashboard.numDomainID
			,ReportDashboard.numReportID
			,numUserCntID
			,ReportDashboard.tintReportType
			,tintChartType
			,(CASE WHEN coalesce(ReportDashboard.vcHeaderText,'') <> '' THEN coalesce(ReportDashboard.vcHeaderText,'') ELSE coalesce(ReportListMaster.vcReportName,'') END) AS vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,ReportDashboard.dtFromDate
			,ReportDashboard.dtToDate
			,numRecordCount
			,tintControlField
			,vcDealAmount
			,tintOppType
			,lngPConclAnalysis
			,bitTask
			,tintTotalProgress
			,tintMinNumber
			,tintMaxNumber
			,tintQtyToDisplay
			,vcDueDate
			,coalesce(bitDealWon,false) AS bitDealWon
			,coalesce(bitGrossRevenue,false) AS bitGrossRevenue
			,coalesce(bitRevenue,false) AS bitRevenue
			,coalesce(bitGrossProfit,false) AS bitGrossProfit
			,coalesce(bitCustomTimeline,false) AS bitCustomTimeline
			,coalesce(bitBilledButNotReceived,false) AS bitBilledButNotReceived
			,coalesce(bitReceviedButNotBilled,false) AS bitReceviedButNotBilled
			,coalesce(bitSoldButNotShipped,false) AS bitSoldButNotShipped
			,coalesce(ReportListMaster.bitDefault,false) AS bitDefault
			,coalesce(ReportListMaster.intDefaultReportID,0) AS intDefaultReportID
      FROM
      ReportDashboard
      LEFT JOIN
      ReportListMaster
      ON
      ReportDashboard.numReportID = ReportListMaster.numReportID
      WHERE
      ReportDashboard.numDomainID = v_numDomainID
      AND numDashBoardID = v_numDashBoardID
      AND 1 =(CASE
      WHEN coalesce(v_numDashBoardID,0) > 0 THEN(CASE WHEN numDashBoardID = v_numDashBoardID THEN 1 ELSE 0 END)
      WHEN coalesce(v_numReportID,0) > 0 THEN(CASE WHEN ReportDashboard.numReportID = v_numReportID THEN 1 ELSE 0 END)
      ELSE 0
      END);
   end if;
   RETURN;
END; $$;


