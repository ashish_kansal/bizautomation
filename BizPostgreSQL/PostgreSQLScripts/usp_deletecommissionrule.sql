-- Stored procedure definition script USP_DeleteCommissionRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCommissionRule(v_numComRuleID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CommissionRuleContacts WHERE numComRuleId = v_numComRuleID;
   DELETE FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID;
   delete from CommissionRuleDtl where numComRuleID = v_numComRuleID;
   delete from CommissionRules where numComRuleID = v_numComRuleID;
   RETURN;
END; $$;




