DROP FUNCTION IF EXISTS GetWorkOrderStatus;

CREATE OR REPLACE FUNCTION GetWorkOrderStatus(v_numDomainID NUMERIC(18,0)
    ,v_tintCommitAllocation SMALLINT
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcWoStatus  VARCHAR(500) DEFAULT '';
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempWOID  NUMERIC(18,0);
BEGIN
   IF(SELECT COUNT(*) FROM WorkOrder WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID) > 0 then
	
      v_vcWoStatus := CONCAT('<a href="#" onClick="return OpenWorkOrder(',(SELECT numWOId FROM WorkOrder WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND coalesce(numParentWOID,0) = 0),
      ');"><img src="../images/Icon/workorder.png" /></a> ');
      DROP TABLE IF EXISTS tt_TEMPGetWorkOrderStatus CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPGetWorkOrderStatus
      (
         ItemLevel INTEGER,
         numParentWOID NUMERIC(18,0),
         numWOID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numQty DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         bitWorkOrder BOOLEAN,
         tintBuildStatus SMALLINT
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPWO_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPWO CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPWO
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numWOID NUMERIC(18,0)
      );
      INSERT
      INTO tt_TEMPGetWorkOrderStatus   with recursive CTEWorkOrder(ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,
      bitWorkOrder,tintBuildStatus) AS(SELECT
      CAST(1 AS INTEGER) AS ItemLevel,
				numParentWOID AS numParentWOID,
				numWOId AS numWOID,
				numItemCode AS numItemKitID,
				CAST(numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
				numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
      FROM
      WorkOrder
      WHERE
      numDomainId = v_numDomainID
      AND numOppId = v_numOppID
      AND numOppItemID = v_numOppItemID
      UNION ALL
      SELECT
      c.ItemLevel+2 AS ItemLevel,
				c.numWOID AS numParentWOID,
				WorkOrder.numWOId AS numWOID,
				WorkOrder.numItemCode AS numItemKitID,
				CAST(WorkOrder.numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
				WorkOrder.numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
      FROM
      WorkOrder
      INNER JOIN
      CTEWorkOrder c
      ON
      WorkOrder.numParentWOID = c.numWOID) SELECT
      ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus
      FROM
      CTEWorkOrder
      INNER JOIN
      Item
      ON
      CTEWorkOrder.numItemKitID = Item.numItemCode
      LEFT JOIN
      WareHouseItems
      ON
      CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
      IF EXISTS(SELECT numWOID FROM tt_TEMPGetWorkOrderStatus WHERE tintBuildStatus <> 2) then
         INSERT INTO
         tt_TEMPGetWorkOrderStatus
         SELECT
         t1.ItemLevel::bigint+1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				WorkOrderDetails.numQtyItemsReq,
				WorkOrderDetails.numWarehouseItemID,
				false AS bitWorkOrder,
				(CASE
         WHEN t1.tintBuildStatus = 2
         THEN 2
         ELSE(CASE
            WHEN Item.charItemType = 'P'
            THEN
               CASE
               WHEN v_tintCommitAllocation = 2
               THEN(CASE WHEN coalesce(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
               ELSE(CASE WHEN coalesce(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END)
               END
            ELSE 1
            END)
         END)
         FROM
         WorkOrderDetails
         INNER JOIN
         tt_TEMPGetWorkOrderStatus t1
         ON
         WorkOrderDetails.numWOId = t1.numWOID
         INNER JOIN
         Item
         ON
         WorkOrderDetails.numChildItemID = Item.numItemCode
         AND 1 =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPGetWorkOrderStatus TInner WHERE TInner.bitWorkOrder = true AND TInner.numParentWOID = t1.numWOID AND TInner.numItemCode = Item.numItemCode) > 0 THEN 0 ELSE 1 END)
         LEFT JOIN
         WareHouseItems
         ON
         WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
         INSERT INTO tt_TEMPWO(numWOID) SELECT numWOID FROM tt_TEMPGetWorkOrderStatus WHERE bitWorkOrder = true ORDER BY ItemLevel DESC;
         select   COUNT(*) INTO v_iCount FROM tt_TEMPWO;
         WHILE v_i <= v_iCount LOOP
            select   numWOID INTO v_numTempWOID FROM tt_TEMPWO WHERE ID = v_i;
            UPDATE
            tt_TEMPGetWorkOrderStatus T1
            SET
            tintBuildStatus =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPGetWorkOrderStatus TInner WHERE TInner.numParentWOID = T1.numWOID AND tintBuildStatus = 0) > 0 THEN 0 ELSE 1 END)
				
            WHERE
            numWOID = v_numTempWOID
            AND T1.tintBuildStatus <> 2;
            v_i := v_i::bigint+1;
         END LOOP;
         IF(SELECT COUNT(*) FROM tt_TEMPGetWorkOrderStatus WHERE bitWorkOrder = true AND tintBuildStatus = 2) =(SELECT COUNT(*) FROM tt_TEMPGetWorkOrderStatus WHERE bitWorkOrder = true) then
			
            v_vcWoStatus := CONCAT(v_vcWoStatus,'Build completed');
         ELSEIF(SELECT COUNT(*) FROM tt_TEMPGetWorkOrderStatus WHERE tintBuildStatus = 0) > 0
         then
			
            v_vcWoStatus := CONCAT(v_vcWoStatus,'Not ready to build');
         ELSE
            v_vcWoStatus := CONCAT(v_vcWoStatus,'Ready to build');
         end if;
      ELSE
         v_vcWoStatus := CONCAT(v_vcWoStatus,'Build completed');
      end if;
   end if;

   RETURN v_vcWoStatus;
END; $$;

