-- Stored procedure definition script USP_Import_GetOrganizationDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Import_GetOrganizationDetails(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		CI.vcCompanyName
		,CI.vcProfile
		,CI.numCompanyType
		,CI.numNoOfEmployeesId
		,CI.numAnnualRevID
		,CI.vcWebSite
		,CI.vcHow
		,CI.numCompanyIndustry
		,CI.numCompanyRating
		,CI.numCompanyCredit
		,CI.txtComments
		,CI.vcWebLink1
		,CI.vcWebLink2
		,CI.vcWebLink3
		,CI.vcWebLink4
		,DM.numCompanyDiff
		,DM.vcCompanyDiff
		,DM.numCampaignID
		,DM.vcComPhone
		,DM.numAssignedTo
		,DM.numFollowUpStatus
		,DM.numTerID
		,DM.numStatusID
		,DM.vcComFax
		,DM.bitPublicFlag
		,DM.bitActiveInActive
		,DM.numDivisionID
		,DM.numGrpId
		,DM.tintCRMType
		,DM.numCurrencyID
		,DM.bitNoTax
		,DM.numBillingDays
		,DM.bitOnCreditHold
		,DM.numDefaultExpenseAccountID
		,coalesce((SELECT numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID LIMIT 1),'') AS vcNonBizCompanyID
   FROM
   DivisionMaster DM
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   DM.numDomainID = v_numDomainID
   AND DM.numDivisionID = v_numDivisionID;
   RETURN;
END; $$;













