-- Stored procedure definition script usp_GetPageList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetPageList(   
--
v_numModuleID NUMERIC(9,0) DEFAULT 0,
	v_numPageID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numPageID <> 0 then
		
      open SWV_RefCur for
      SELECT PM.numPageID, PM.vcPageDesc, PM.vcFileName, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, PM.bitIsDeleteApplicable, PM.bitIsExportApplicable
      FROM PageMaster PM
      WHERE
      PM.numPageID = v_numPageID
      AND PM.numModuleID = v_numModuleID
      ORDER BY PM.numPageID;
   ELSE
      open SWV_RefCur for
      SELECT PM.numPageID, PM.vcPageDesc, PM.vcFileName, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, PM.bitIsDeleteApplicable, PM.bitIsExportApplicable
      FROM PageMaster PM
      WHERE PM.numModuleID = v_numModuleID
      ORDER BY PM.numPageID;
   end if;
   RETURN;
END; $$;


