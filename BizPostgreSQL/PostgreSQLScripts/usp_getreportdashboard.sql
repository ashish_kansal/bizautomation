-- Stored procedure definition script USP_GetReportDashBoard for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReportDashBoard( --IF VALUE IS -1 THEN FETCH DEFAULT TEMPLATE SET FOR THAT USER           
v_numDomainID NUMERIC(18,0),           
	v_numUserCntID NUMERIC(18,0),
	v_numDashboardTemplateID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numDashboardTemplateID,0) = -1 then
	
      v_numDashboardTemplateID := coalesce((SELECT numDashboardTemplateID FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID),0);
   end if;

   IF NOT EXISTS(select 1 from ReportDashBoardSize WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
	
      INSERT INTO ReportDashBoardSize(numDomainID,numUserCntID,tintColumn,tintSize)
      SELECT v_numDomainID,v_numUserCntID,1,1
      UNION ALL
      SELECT v_numDomainID,v_numUserCntID,2,1
      UNION ALL
      SELECT v_numDomainID,v_numUserCntID,3,1;
   end if;     

   open SWV_RefCur for
   SELECT
   tintColumn
		,tintSize
   FROM
   ReportDashBoardSize
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   ORDER BY
   tintColumn;            

   open SWV_RefCur2 for
   SELECT
   RD.numDashBoardID
		,RD.numReportID
		,RD.tintColumn
		,RD.tintRow
		,RD.tintReportType
		,RD.tintChartType
		,RD.tintReportCategory
		,RD.intHeight
		,RD.intWidth
		,coalesce(RLM.bitDefault,false) AS bitDefault
		,coalesce(RLM.intDefaultReportID,0) AS intDefaultReportID
   FROM
   ReportDashboard RD
   LEFT JOIN
   DashboardTemplate DT
   ON
   RD.numDashboardTemplateID = DT.numTemplateID
   LEFT JOIN
   ReportListMaster RLM
   ON
   RD.numReportID = RLM.numReportID
   WHERE
   RD.numDomainID = v_numDomainID
   AND RD.numUserCntID = v_numUserCntID
   AND coalesce(RD.numDashboardTemplateID,0) = coalesce(v_numDashboardTemplateID,0)
   ORDER BY
   tintColumn,tintRow;     
		
   open SWV_RefCur3 for
   SELECT
   numTemplateID
		,vcTemplateName
   FROM
   DashboardTemplate
   WHERE
   numDomainID = v_numDomainID
   AND numTemplateID = v_numDashboardTemplateID;
   RETURN;
END; $$;


