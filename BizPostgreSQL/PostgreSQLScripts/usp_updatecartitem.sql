-- Stored procedure definition script USP_UpdateCartItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE  OR REPLACE FUNCTION USP_UpdateCartItem(v_numUserCntId NUMERIC(9,0),
	v_numDomainId NUMERIC(9,0),
	v_vcCookieId VARCHAR(100),
	v_strXML TEXT,
	v_postselldiscount INTEGER DEFAULT 0,
	v_numSiteID NUMERIC(18,0) DEFAULT NULL,
	v_vcPreferredPromotions TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strXML AS VARCHAR(10)),1,10) <> '' then
      IF v_numUserCntId <> 0 then
		
         UPDATE
         CartItems
         SET
         monPrice = ox.monPrice,numUnitHour = ox.numUnitHour,monTotAmtBefDiscount = ox.monTotAmtBefDiscount,
         monTotAmount = ox.monTotAmount,bitDiscountType = coalesce(ox.bitDiscountType,false),
         vcShippingMethod = ox.vcShippingMethod,
         numServiceTypeId = ox.numServiceTypeID,decShippingCharge = ox.decShippingCharge,
         tintServicetype = ox.tintServiceType,numShippingCompany = ox.numShippingCompany,
         dtDeliveryDate = ox.dtDeliveryDate,fltDiscount = ox.fltDiscount,
         vcCookieId = ox.vcCookieId,vcCoupon =(CASE WHEN CartItems.vcCoupon <> '' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
         FROM
		 XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strXML AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numDomainId NUMERIC(9,0) PATH 'numDomainId'
				,numUserCntId NUMERIC(9,0) PATH 'numUserCntId'
				,vcCookieId VARCHAR(200) PATH 'vcCookieId'
				,numCartID NUMERIC(18,0) PATH 'numCartID'
				,numItemCode NUMERIC(9,0) PATH 'numItemCode'
				,monPrice DECIMAL(20,5) PATH 'monPrice'
				,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
				,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
				,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
				,bitDiscountType BOOLEAN PATH 'bitDiscountType'
				,vcShippingMethod VARCHAR(200) PATH 'vcShippingMethod'
				,numServiceTypeID NUMERIC(9,0) PATH 'numServiceTypeID'
				,decShippingCharge DECIMAL(20,5) PATH 'decShippingCharge'
				,tintServiceType SMALLINT PATH 'tintServiceType'
				,numShippingCompany NUMERIC(9,0) PATH 'numShippingCompany'
				,dtDeliveryDate TIMESTAMP PATH 'dtDeliveryDate'
				,fltDiscount DECIMAL(18,2) PATH 'fltDiscount'
				,vcCoupon VARCHAR(200) PATH 'vcCoupon'
		) AS ox
         WHERE
         CartItems.numDomainId = ox.numDomainId
         AND CartItems.numUserCntId = ox.numUserCntId
         AND CartItems.numItemCode = ox.numItemCode
         AND CartItems.numCartId = ox.numCartID;
      ELSE
         UPDATE
         CartItems
         SET
         monPrice = ox.monPrice,numUnitHour = ox.numUnitHour,monTotAmtBefDiscount = coalesce(ox.numUnitHour,0)*coalesce(decUOMConversionFactor,1)*coalesce(ox.monPrice,0),
         monTotAmount =(CASE
         WHEN coalesce(ox.fltDiscount,0) > 0
         THEN(CASE
            WHEN coalesce(ox.bitDiscountType,false) = true
            THEN(CASE
               WHEN(coalesce(ox.numUnitHour,0)*coalesce(decUOMConversionFactor,1)*coalesce(ox.monPrice,0)) -ox.fltDiscount >= 0
               THEN(coalesce(ox.numUnitHour,0)*coalesce(decUOMConversionFactor,1)*coalesce(ox.monPrice,0)) -ox.fltDiscount
               ELSE 0
               END)
            ELSE(coalesce(ox.numUnitHour,0)*coalesce(decUOMConversionFactor,1))*(coalesce(ox.monPrice,0) -(coalesce(ox.monPrice,0)*(ox.fltDiscount/100)))
            END)
         ELSE coalesce(ox.numUnitHour,0)*coalesce(decUOMConversionFactor,1)*coalesce(ox.monPrice,0)
         END),bitDiscountType = coalesce(ox.bitDiscountType,false),
         vcShippingMethod = ox.vcShippingMethod,
         numServiceTypeId = ox.numServiceTypeID,decShippingCharge = ox.decShippingCharge,
         tintServicetype = ox.tintServiceType,numShippingCompany = ox.numShippingCompany,
         dtDeliveryDate = ox.dtDeliveryDate,fltDiscount = ox.fltDiscount,
         vcCoupon =(CASE WHEN CartItems.vcCoupon <> '' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
         FROM
        XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strXML AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numDomainId NUMERIC(9,0) PATH 'numDomainId'
				,numUserCntId NUMERIC(9,0) PATH 'numUserCntId'
				,vcCookieId VARCHAR(200) PATH 'vcCookieId'
				,numCartID NUMERIC(18,0) PATH 'numCartID'
				,numItemCode NUMERIC(9,0) PATH 'numItemCode'
				,monPrice DECIMAL(20,5) PATH 'monPrice'
				,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
				,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
				,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
				,bitDiscountType BOOLEAN PATH 'bitDiscountType'
				,vcShippingMethod VARCHAR(200) PATH 'vcShippingMethod'
				,numServiceTypeID NUMERIC(9,0) PATH 'numServiceTypeID'
				,decShippingCharge DECIMAL(20,5) PATH 'decShippingCharge'
				,tintServiceType SMALLINT PATH 'tintServiceType'
				,numShippingCompany NUMERIC(9,0) PATH 'numShippingCompany'
				,dtDeliveryDate TIMESTAMP PATH 'dtDeliveryDate'
				,fltDiscount DECIMAL(18,2) PATH 'fltDiscount'
				,vcCoupon VARCHAR(200) PATH 'vcCoupon'
		) AS ox
         WHERE
         CartItems.numDomainId = ox.numDomainId
         AND CartItems.numUserCntId = 0
         AND CartItems.numItemCode = ox.numItemCode
         AND CartItems.vcCookieId = ox.vcCookieId
         AND CartItems.numCartId = ox.numCartID;
      end if;

      IF(SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId = v_numDomainId AND coalesce(bitEnabled,false) = true) > 0 then
		
         PERFORM USP_PromotionOffer_ApplyItemPromotionToECommerce(v_numDomainId,v_numUserCntId,v_numSiteID,v_vcCookieId,false,'');
      end if;
   end if;
   RETURN;
END; $$;



