-- Stored procedure definition script USP_SolManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SolManage(v_numCategory NUMERIC(9,0) DEFAULT null,           
v_vcSolnTitle VARCHAR(1000) DEFAULT null,          
v_txtSolution TEXT DEFAULT '',          
INOUT v_numSolID NUMERIC(9,0) DEFAULT null ,      
v_vcLink VARCHAR(500) DEFAULT NULL,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numSolID = 0 then

      insert into SolutionMaster(numCategory,vcSolnTitle,txtSolution,vcLink,numDomainID,numCreatedby,dtCreatedon,numModifiedBy,dtModifiedOn)
 values(v_numCategory,v_vcSolnTitle,v_txtSolution,'',v_numDomainID,v_numUserCntID,TIMEZONE('UTC',now()),v_numUserCntID,TIMEZONE('UTC',now()));
  
      v_numSolID := CURRVAL('SolutionMaster_seq');
   else
      update SolutionMaster
      set
      numCategory = v_numCategory,vcSolnTitle = v_vcSolnTitle,txtSolution = v_txtSolution,
      vcLink = v_vcLink,numModifiedBy = v_numUserCntID,dtModifiedOn = TIMEZONE('UTC',now())
      where numSolnID = v_numSolID;
   end if;
   RETURN;
END; $$;


