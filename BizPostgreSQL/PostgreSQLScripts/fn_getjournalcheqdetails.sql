-- Function definition script fn_GetJournalCheqDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetJournalCheqDetails(v_numCheckId NUMERIC(8,0),
 v_numDomainId INTEGER)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Amount  VARCHAR(50);
   v_numCheckNo  VARCHAR(50);
   v_vcMemo  VARCHAR(2000);
   v_vcDescription  VARCHAR(50);


   BizDoc_Cursor CURSOR FOR
	
   SELECT cast(SUM(CheckDetails.monAmount) as VARCHAR(50)) as Amount,
	coalesce(CAST(CheckHeader.numCheckNo AS VARCHAR(50)),'No-Cheq') as numCheckNo,
	coalesce(CheckHeader.vcMemo,'No-Memo') as vcMemo FROM CheckHeader INNER JOIN CheckDetails ON CheckHeader.numCheckHeaderID=CheckDetails.numCheckHeaderID
   WHERE CheckHeader.numCheckHeaderID = v_numCheckId AND 
   CheckHeader.numDomainId = v_numDomainId
   GROUP BY CheckHeader.numCheckNo,CheckHeader.vcMemo;
BEGIN
   v_vcDescription := '';

   OPEN BizDoc_Cursor;

   FETCH NEXT FROM BizDoc_Cursor into v_Amount,v_numCheckNo,v_vcMemo;
   WHILE FOUND LOOP
      v_vcDescription := coalesce(v_vcDescription,'') || ',' || coalesce(v_numCheckNo,'');
      FETCH NEXT FROM BizDoc_Cursor into v_Amount,v_numCheckNo,v_vcMemo;
   END LOOP;

   CLOSE BizDoc_Cursor;


   RETURN SUBSTR(v_vcDescription,2,LENGTH(v_vcDescription));
END; $$;

