-- Stored procedure definition script USP_SelectCommunicationAttendees_Detail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SelectCommunicationAttendees_Detail(v_numCommId NUMERIC(9,0) DEFAULT 0,
v_numUserCntId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		ActivityID as "ActivityID"
		,numAttendeeId as "numAttendeeId"
		,coalesce(numStatus,0) as "numStatus" 
	FROM 
		CommunicationAttendees CA
	WHERE 
		CA.numCommId = v_numCommId 
		AND CA.numContactId = v_numUserCntId;
END; $$;












