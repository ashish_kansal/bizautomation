-- FUNCTION: public.usp_getcommunicationinfo(numeric, integer, refcursor)

-- DROP FUNCTION public.usp_getcommunicationinfo(numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcommunicationinfo(
	v_numcommid numeric DEFAULT 0,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_numFollowUpStatus  NUMERIC;
BEGIN
-- This function was converted on Sat Apr 10 15:50:50 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   select   numFollowUpStatus INTO v_numFollowUpStatus FROM DivisionMaster WHERE numDivisionID IN(SELECT numDivisionID FROM Communication WHERE numCommId = v_numCommid);
   open SWV_RefCur for SELECT comm.numCommId, comm.bitTask, bitOutlook,
comm.numContactId,
comm.numDivisionID, cast(coalesce(comm.textDetails,'') as TEXT) AS textDetails, comm.intSnoozeMins, comm.intRemainderMins,
comm.numStatus, comm.numActivity, comm.numAssign, comm.tintSnoozeStatus,
comm.tintRemStatus, comm.numOppId, comm.numCreatedBy,cast(coalesce(bitTimeAddedToContract,false) as BOOLEAN) AS bitTimeAddedToContract,
comm.nummodifiedby, comm.bitClosedFlag, comm.vcCalendarName,
dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtStartTime,
dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as dtEndTime,
CAST('-' AS CHAR(1)) as AssignTo,
fn_SecondsConversion(coalesce((SELECT  coalesce(CAST(timeLeft AS INTEGER),0) -coalesce(CAST(timeUsed AS INTEGER),0) FROM Contracts WHERE numDomainId = comm.numDomainID AND numDivisonId = comm.numDivisionID AND intType = 1 LIMIT 1),0)) AS timeLeft,
coalesce(fn_GetContactName(comm.numCreatedBy),'-') || ' ' || CAST(comm.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as CreatedBy,
coalesce(fn_GetContactName(comm.nummodifiedby),'-') || ' ' || CAST(comm.dtModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as ModifiedBy,
cast(coalesce(fn_GetContactEmail(1::SMALLINT,0::NUMERIC(9,0),comm.numContactId),'') as VARCHAR(100)) AS AssignEmail,
cast(coalesce(fn_GetContactName(comm.numContactId),'') as VARCHAR(100)) AS AssignName,
cast(coalesce(fn_GetContactName(comm.numCreatedBy),'-') as VARCHAR(100)) as RecOwner,
CAST(dtModifiedDate AS VARCHAR(20)) as ModifiedDate,
cast(coalesce(bitsendEmailTemp,false) as BOOLEAN) as bitSendEmailTemp,
cast(coalesce(numEmailTemplate,0) as NUMERIC(18,0)) as numEmailTemplate,
cast(coalesce(tinthours,0) as SMALLINT) as tintHours,
cast(coalesce(bitalert,false) as BOOLEAN) as bitAlert,
comm.CaseId,
cast((select  cast(vcCaseNumber as NUMERIC(18,0)) from Cases where Cases.numCaseId = comm.CaseId LIMIT 1) as VARCHAR(255)) as vcCaseName,
cast(coalesce(caseTimeid,0) as NUMERIC(18,0)) as caseTimeid,
cast(coalesce(caseExpid,0) as NUMERIC(18,0)) as caseExpid   ,
numActivityId,
cast(coalesce(C.numCorrespondenceID,0) as VARCHAR(255)) AS numCorrespondenceID,
cast(coalesce(C.numOpenRecordID,0) as VARCHAR(255)) AS numOpenRecordID,
cast(coalesce(C.tintCorrType,0) as VARCHAR(255)) AS tintCorrType,
cast(coalesce(v_numFollowUpStatus,0) as NUMERIC) AS numFollowUpStatus
,cast(coalesce(comm.bitFollowUpAnyTime,false) as BOOLEAN) AS bitFollowUpAnyTime
,cast(coalesce(C.monMRItemAmount,0) as VARCHAR(255)) as monMRItemAmount,
cast(coalesce(clo.numDivisionID,0) as VARCHAR(255)) AS numLinkedOrganization,
cast(coalesce(clo.numContactID,0) as VARCHAR(255)) AS numLinkedContact,
(CASE WHEN comm.numDivisionID > 0 THEN(SELECT vcData from Listdetails where numListItemID = DivisionMaster.numTerID LIMIT 1) ELSE '' END) AS numTerId,
(CASE WHEN comm.numDivisionID > 0 THEN DivisionMaster.numTerID ELSE 0 END) AS numOrgTerId
   FROM Communication comm LEFT OUTER JOIN Correspondence C ON C.numCommID = comm.numCommId
   LEFT JOIN CommunicationLinkedOrganization clo ON comm.numCommId = clo.numCommID
   LEFT JOIN DivisionMaster ON comm.numDivisionID = DivisionMaster.numDivisionID
   WHERE  comm.numCommId = v_numCommid;
END;
$BODY$;

ALTER FUNCTION public.usp_getcommunicationinfo(numeric, integer, refcursor)
    OWNER TO postgres;
