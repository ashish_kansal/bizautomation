-- Stored procedure definition script USP_SetDripCampaign for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SetDripCampaign(v_vcContactIDList VARCHAR(8000),
    v_numECampaignID NUMERIC,
    v_numUserCntID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numContactID  NUMERIC;
   v_Date  TIMESTAMP;
   SWV_RowCount INTEGER;
BEGIN
   IF LENGTH(v_vcContactIDList) = 0 then
      RETURN;
   end if;
	
	
   UPDATE  AdditionalContactsInformation
   SET     numECampaignID = v_numECampaignID
   WHERE   numContactId IN(SELECT  Id
   FROM    SplitIDs(v_vcContactIDList,','));
   open SWV_RefCur for SELECT  cast(Id as VARCHAR(255))
   FROM    SplitIDs(v_vcContactIDList,',');
                
   DROP TABLE IF EXISTS tt_TEMPSetDripCampaign CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSetDripCampaign 
   ( 
      ContactID NUMERIC 
   );
   INSERT  INTO tt_TEMPSetDripCampaign
   SELECT  Id
   FROM    SplitIDs(v_vcContactIDList,',');
        
   IF v_numECampaignID = -1 then
      v_numECampaignID := 0;
   end if;
            
            
   select   ContactID INTO v_numContactID FROM    tt_TEMPSetDripCampaign     LIMIT 1;
   WHILE v_numContactID <> 0 LOOP
        
		/*Added by chintan BugID-262*/
                 --  numeric(9, 0)
                     --  numeric(9, 0)
                     --  numeric(9, 0)
                     --  datetime
                     --  bit
      v_Date := GetUTCDateWithoutTime();
      PERFORM USP_ManageConEmailCampaign(v_numConEmailCampID := 0,v_numContactID := v_numContactID,v_numECampaignID := v_numECampaignID,
      v_intStartDate := v_Date,v_bitEngaged := true,
      v_numUserCntID := v_numUserCntID); --  numeric(9, 0)
            
      DELETE  FROM tt_TEMPSetDripCampaign WHERE   ContactID = v_numContactID;
      select   ContactID INTO v_numContactID FROM    tt_TEMPSetDripCampaign     LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numContactID := 0;
      end if;
   END LOOP;
       
   DROP TABLE IF EXISTS tt_TEMPSetDripCampaign CASCADE;
   RETURN;
END; $$;













