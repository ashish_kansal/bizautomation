-- Stored procedure definition script usp_DeleteAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteAccounts(v_numDivisionID NUMERIC(9,0),
    v_numContactID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCompanyID  NUMERIC(9,0);
   v_error  VARCHAR(1000);
   SWV_RowCount INTEGER;
BEGIN
   if not exists(select * from    DivisionMaster DM
   join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID
   join UserMaster on numUserDetailid = ADC.numContactId
   join Subscribers sb on DM.numDivisionID = sb.numDivisionid
   where   DM.numDivisionID = v_numDivisionID
   and DM.numDomainID = v_numDomainID
   and bitActivateFlag = true
   and bitActive = 1) then
        
      if exists(select * from    DivisionMaster
      where   numDivisionID = v_numDivisionID
      and numDomainID = v_numDomainID) then
         IF EXISTS(select * from OpportunityMaster WHERE numDivisionId = v_numDivisionID AND numDomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHILD_OPP';
            RETURN;
         end if;
         IF EXISTS(SELECT * FROM Vendor WHERE numVendorID = v_numDivisionID AND numDomainID = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHILD_VENDOR';
            RETURN;
         end if;
         IF EXISTS(select * from ProjectsMaster WHERE numDivisionId = v_numDivisionID AND numdomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHILD_PROJECT';
            RETURN;
         end if;
         IF EXISTS(select * from Cases WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHILD_CASE';
            RETURN;
         end if;
         IF EXISTS(select * from CompanyAssets WHERE numDivID = v_numDivisionID AND numDomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHILD_ASSET';
            RETURN;
         end if;
         IF EXISTS(select * from DepositMaster AS DM WHERE DM.numDivisionID = v_numDivisionID AND numDomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'DEPOSIT_EXISTS';
            RETURN;
         end if;
         IF EXISTS(select * from ReturnHeader AS RH WHERE RH.numDivisionId = v_numDivisionID AND numDomainID = v_numDomainID) then
                  
            RAISE EXCEPTION 'RETURN_EXISTS';
            RETURN;
         end if;
         IF EXISTS(select * from BillPaymentHeader AS BPH WHERE BPH.numDivisionId = v_numDivisionID AND numDomainId = v_numDomainID) then
                  
            RAISE NOTICE 'BILL_PAYMENT_EXISTS';
            RETURN;
         end if;
         IF EXISTS(select * from BillHeader AS BH WHERE BH.numDivisionId = v_numDivisionID AND numDomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'STANDALONE_BILL_EXISTS';
            RETURN;
         end if;
         IF EXISTS(select * from CheckHeader AS CH WHERE CH.numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID) then
                  
            RAISE EXCEPTION 'CHECK_EXISTS';
            RETURN;
         end if;

				  /*Bill added against selected company*/
         IF EXISTS(select * from OpportunityBizDocsDetails WHERE numDivisionID = v_numDivisionID AND tintPaymentType = 2 AND numDomainId = v_numDomainID) then
                  
            RAISE EXCEPTION 'BILL';
            RETURN;
         end if;
         BEGIN
            -- BEGIN TRAN
delete  FROM RecentItems
            where   numRecordID = v_numDivisionID
            and chrRecordType = 'C';
            select   numCompanyID INTO v_numCompanyID from    DivisionMaster where   numDivisionID = v_numDivisionID    AND numDomainID = v_numDomainID;
            DELETE  FROM Communication
            WHERE   numDivisionID = v_numDivisionID    AND numDomainID = v_numDomainID;
            delete  from ExtranetAccountsDtl
            where   numExtranetID in(select  numExtranetID
            from    ExtarnetAccounts
            where   numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID)  AND numDomainID = v_numDomainID;
            delete  from ExtarnetAccounts
            where   numDivisionID = v_numDivisionID  AND numDomainID = v_numDomainID;
            delete  from CaseContacts
            where   numContactID in(select  numContactId
            from    AdditionalContactsInformation
            where   numDivisionId = v_numDivisionID AND numDomainID = v_numDomainID);
            delete  from ProjectsContacts
            where   numContactID in(select  numContactId
            from    AdditionalContactsInformation
            where   numDivisionId = v_numDivisionID AND numDomainID = v_numDomainID);
            delete  from OpportunityContact
            where   numContactID in(select  numContactId
            from    AdditionalContactsInformation
            where   numDivisionId = v_numDivisionID AND numDomainID = v_numDomainID);                     
                        ------------------------------
            DELETE FROM DepositeDetails
            WHERE numDepositID IN(SELECT DM.numDepositId FROM DepositMaster AS DM
            WHERE DM.numDivisionID = v_numDivisionID
            AND DM.numDomainId = v_numDomainID);
						
						------------------------------
            DELETE FROM DepositMaster
            WHERE numDivisionID = v_numDivisionID
            AND numDomainId = v_numDomainID;
						------------------------------
            DELETE FROM ReturnItems WHERE numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
            WHERE numDomainID = v_numDomainID
            AND ReturnHeader.numDivisionId = v_numDivisionID);
            DELETE FROM ReturnPaymentHistory WHERE numReturnHeaderID IN(SELECT numReturnHeaderID FROM ReturnHeader
            WHERE numDomainID = v_numDomainID
            AND ReturnHeader.numDivisionId = v_numDivisionID);
            DELETE FROM	ReturnHeader
            WHERE numDomainID = v_numDomainID
            AND ReturnHeader.numDivisionId = v_numDivisionID;
						------------------------------
            DELETE FROM BillPaymentDetails
            WHERE BillPaymentDetails.numBillPaymentID IN(SELECT BPH.numBillPaymentID FROM BillPaymentHeader AS BPH WHERE BPH.numDivisionId = v_numDivisionID AND BPH.numDomainId = v_numDomainID);
            DELETE FROM BillPaymentHeader WHERE numDivisionId = v_numDivisionID AND numDomainId = v_numDomainID;
						------------------------------
            DELETE FROM BillDetails
            WHERE numBillID IN(SELECT numBillID FROM BillHeader AS BH WHERE BH.numDivisionId = v_numDivisionID AND BH.numDomainId = v_numDomainID);
            DELETE FROM BillHeader WHERE numDivisionId = v_numDivisionID AND numDomainId = v_numDomainID;
						------------------------------
            DELETE FROM CheckDetails WHERE CheckDetails.numCheckHeaderID IN(SELECT CH.numCheckHeaderID FROM CheckHeader AS CH WHERE CH.numDivisionID = v_numDivisionID AND CH.numDomainID = v_numDomainID);
            DELETE FROM CheckDetails_Old WHERE CheckDetails_Old.numCheckId IN(SELECT CH.numCheckHeaderID FROM CheckHeader AS CH WHERE CH.numDivisionID = v_numDivisionID AND CH.numDomainID = v_numDomainID);
            DELETE FROM CheckHeader WHERE numDivisionID = v_numDivisionID AND numDomainID = v_numDomainID;
            v_numContactID := 0;
            select   numContactId INTO v_numContactID from    AdditionalContactsInformation where   numDivisionId = v_numDivisionID AND numDomainID = v_numDomainID   order by numContactId LIMIT 1;
            while v_numContactID > 0 LOOP
               delete  FROM ConECampaignDTL
               where   numConECampID in(select  numConEmailCampID
               from    ConECampaign
               where   numContactID = v_numContactID);
               delete  FROM ConECampaign where   numContactID = v_numContactID;
               delete  FROM AOIContactLink where   numContactID = v_numContactID;
               delete  FROM CaseContacts where   numContactID = v_numContactID;
               delete  FROM ProjectsContacts where   numContactID = v_numContactID;
               delete  FROM OpportunityContact where   numContactID = v_numContactID;
               delete  FROM UserTeams where   numUserCntID = v_numContactID;
               delete  FROM UserTerritory where   numUserCntID = v_numContactID;
               delete from CustomerCreditCardInfo where numContactId = v_numContactID;
               DELETE FROM SiteSubscriberDetails WHERE numContactId = v_numContactID;
               DELETE  FROM AdditionalContactsInformation WHERE   numContactId = v_numContactID    AND numDomainID = v_numDomainID;
               select   numContactId INTO v_numContactID from    AdditionalContactsInformation where   numDivisionId = v_numDivisionID
               and numContactId > v_numContactID
               AND numDomainID = v_numDomainID   order by numContactId LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               if SWV_RowCount = 0 then
                  v_numContactID := 0;
               end if;
            END LOOP;
            DELETE FROM VendorShipmentMethod WHERE numDomainId = v_numDomainID AND numVendorID = v_numDivisionID;
            DELETE FROM CompanyAssociations WHERE numDomainID = v_numDomainID AND numAssociateFromDivisionID = v_numDivisionID;
            DELETE FROM CompanyAssociations WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
            DELETE FROM ImportActionItemReference WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
            DELETE FROM ImportOrganizationContactReference WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
            DELETE  FROM AdditionalContactsInformation WHERE   numDivisionId = v_numDivisionID    AND numDomainID = v_numDomainID;                           
                          
--                        delete  from Vendor where   numVendorID = @numDivisionID
            DROP TABLE IF EXISTS tt_GJDETAILS CASCADE;
            CREATE TEMPORARY TABLE tt_GJDETAILS AS
               SELECT * FROM General_Journal_Details AS GJD WHERE GJD.numCustomerId = v_numDivisionID
               AND GJD.numDomainId = v_numDomainID;
            DELETE FROM General_Journal_Details WHERE General_Journal_Details.numCustomerId = v_numDivisionID
            AND General_Journal_Details.numDomainId = v_numDomainID;


			DELETE FROM CashCreditCardDetails AS CCD
			USING
				General_Journal_Header AS GJH
				,tt_GJDETAILS AS GJD
			WHERE 
				GJH.numDomainId=v_numDomainID
				AND GJD.numDomainId = v_numDomainID 
				AND GJD.numCustomerId = v_numDivisionID
				AND CCD.numCashCreditId = GJH.numCashCreditCardId
				AND GJH.numJournal_Id = GJD.numJournalID;
						
			DELETE FROM RecurringTemplate AS RT
			USING
				General_Journal_Header GJH
				,tt_GJDETAILS GJD
			WHERE 
				GJH.numDomainId=v_numDomainID
				AND GJD.numDomainId = v_numDomainID 
				AND GJD.numCustomerId = v_numDivisionID
				AND RT.numRecurringId = GJH.numRecurringId
				AND GJH.numJournal_Id = GJD.numJournalID;		

			DELETE FROM General_Journal_Header GJH
			USING
				tt_GJDETAILS GJD
			WHERE 
				GJH.numDomainId=v_numDomainID
				AND GJH.numJournal_Id = GJD.numJournalID
				AND GJD.numDomainId = v_numDomainID 
				AND GJD.numCustomerId = v_numDivisionID;

            DROP TABLE IF EXISTS tt_GJDETAILS CASCADE;
            DELETE  FROM DivisionMaster
            WHERE   numDivisionID = v_numDivisionID   AND numDomainID = v_numDomainID;
            DELETE  FROM CompanyInfo
            WHERE   numCompanyId = v_numCompanyID  AND numDomainID = v_numDomainID;
            open SWV_RefCur for
            select  1;
            -- COMMIT
EXCEPTION WHEN OTHERS THEN
               -- ROLLBACK 
v_error := SQLERRM;
               RAISE EXCEPTION '%',v_error;
               RETURN;
         END;
      end if;
   else
      open SWV_RefCur for
      select  0;
   end if;
END; $$;


