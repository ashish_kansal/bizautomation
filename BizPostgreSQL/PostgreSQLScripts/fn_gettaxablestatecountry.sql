-- Function definition script fn_getTaxableStateCountry for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_getTaxableStateCountry(v_numDomainID NUMERIC,
      v_numOppID NUMERIC)
RETURNS TABLE
(
   numState NUMERIC,
   numCountry NUMERIC
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC; 
   v_tintBillToType  SMALLINT;
   v_tintShipToType  SMALLINT;
   v_tintBaseTax  SMALLINT;
   v_numCountry  NUMERIC(9,0);
   v_numState  NUMERIC(9,0);
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETTAXABLESTATECOUNTRY_RETINFO CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETTAXABLESTATECOUNTRY_RETINFO
   (
      numState NUMERIC,
      numCountry NUMERIC
   );
   select   tintBaseTax INTO v_tintBaseTax FROM    Domain WHERE   numDomainId = v_numDomainID;
   select   numDivisionId, coalesce(tintBillToType,1), coalesce(tintShipToType,1) INTO v_numDivisionID,v_tintBillToType,v_tintShipToType FROM    OpportunityMaster WHERE   numOppId = v_numOppID;
    
   IF v_tintBaseTax = 2 then --Shipping Address(Default)
        
			-- Fore Help on Billto /Ship to type Flags See Bug ID 207
      IF v_tintShipToType = 0 then
                
         select   coalesce(AD1.numState,0), coalesce(AD1.numCountry,0) INTO v_numState,v_numCountry FROM    CompanyInfo Com1
         JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
         LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
         AND AD1.numRecordID = div1.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 2 AND AD1.bitIsPrimary = true WHERE   D1.numDomainId = v_numDomainID;
      ELSEIF v_tintShipToType = 1
      then
                    
         select   coalesce(numState,0), coalesce(numCountry,0) INTO v_numState,v_numCountry FROM     AddressDetails WHERE    numDomainID = v_numDomainID
         AND numRecordID = v_numDivisionID
         AND tintAddressOf = 2
         AND tintAddressType = 2
         AND bitIsPrimary = true;
      ELSEIF v_tintShipToType = 2
      then
                        
         select   coalesce(numShipState,0), coalesce(numShipCountry,0) INTO v_numState,v_numCountry FROM    OpportunityAddress WHERE   numOppID = v_numOppID;
      end if;
   end if;

   IF v_tintBaseTax = 1 then -- Billing Address
        
			-- Fore Help on Billto /Ship to type Flags See Bug ID 207
      IF v_tintBillToType = 0 then
                
         select   coalesce(AD1.numState,0), coalesce(AD1.numCountry,0) INTO v_numState,v_numCountry FROM    CompanyInfo Com1
         JOIN DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionId = div1.numDivisionID
         LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
         AND AD1.numRecordID = div1.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true WHERE   D1.numDomainId = v_numDomainID;
      ELSEIF v_tintBillToType = 1
      then
                    
         select   coalesce(numState,0), coalesce(numCountry,0) INTO v_numState,v_numCountry FROM     AddressDetails WHERE    numDomainID = v_numDomainID
         AND numRecordID = v_numDivisionID
         AND tintAddressOf = 2
         AND tintAddressType = 1
         AND bitIsPrimary = true;
      ELSEIF v_tintBillToType = 2
      then
                        
         select   coalesce(numBillState,0), coalesce(numBillCountry,0) INTO v_numState,v_numCountry FROM    OpportunityAddress WHERE   numOppID = v_numOppID;
      end if;
   end if;
	
   INSERT  INTO tt_FN_GETTAXABLESTATECOUNTRY_RETINFO
   SELECT  v_numState,
                    v_numCountry;
    RETURN QUERY (SELECT * FROM tt_FN_GETTAXABLESTATECOUNTRY_RETINFO);
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_getTemplateName]    Script Date: 07/26/2008 18:12:46 ******/

