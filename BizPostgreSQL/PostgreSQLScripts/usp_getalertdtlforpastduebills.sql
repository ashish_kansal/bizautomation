-- Stored procedure definition script USP_GetAlertDtlforPastDueBills for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertDtlforPastDueBills(v_numDepartMentID NUMERIC(9,0) DEFAULT 0 ,
v_numDomainId NUMERIC(9,0)   DEFAULT 1,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(ALT.numAlertDTLid as VARCHAR(255)),cast(ADDT.numEmailTemplate as VARCHAR(255)),cast(ADDT.tintAlertOn as VARCHAR(255)),cast(ADDT.numDaysAfterDue as VARCHAR(255)),cast(ADDT.numBizDocs as VARCHAR(255)),cast(ADDT.numDepartment as VARCHAR(255)),cast(lst1.vcData as VARCHAR(255)) as BizDoc
,cast(vcDocName as VARCHAR(255)) as EmailTemplate  from AlertDTL ALT
   join   AlertDomainDtl ADDT on ADDT.numAlertDTLid = ALT.numAlertDTLid
   join Listdetails as lst1
   on lst1.numListItemID = ADDT.numBizDocs
   join GenericDocuments as Doc
   on Doc.numGenericDocID = ADDT.numEmailTemplate
   where numAlertID = 8 and ADDT.numDepartment = v_numDepartMentID    and ADDT.numDomainID = v_numDomainId;
END; $$;












