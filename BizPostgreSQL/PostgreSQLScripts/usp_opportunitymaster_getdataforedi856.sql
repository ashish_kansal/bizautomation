-- Stored procedure definition script USP_OpportunityMaster_GetDataForEDI856 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetDataForEDI856(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TABLESHIPPINGREPORT CASCADE;
   CREATE TEMPORARY TABLE tt_TABLESHIPPINGREPORT
   (
      numShippingReportId NUMERIC(18,0)
   );
   DROP TABLE IF EXISTS tt_TABLESHIPPINGBOX CASCADE;
   CREATE TEMPORARY TABLE tt_TABLESHIPPINGBOX
   (
      numBoxID NUMERIC(18,0),
      vcTrackingNumber VARCHAR(300)
   );
   INSERT INTO tt_TABLESHIPPINGREPORT(numShippingReportId)
   SELECT
   numShippingReportId
   FROM
   ShippingReport
   WHERE
   ShippingReport.numDomainID = v_numDomainID
   AND ShippingReport.numOppID = v_numOppID
   AND coalesce(ShippingReport.bitSendToTP2,false) = false
   AND 1 =(CASE WHEN(SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId = ShippingReport.numShippingReportID AND vcShippingLabelImage IS NOT NULL AND NULLIF(vcTrackingNumber,'') IS NOT NULL) > 0 THEN 1 ELSE 0 END);

   INSERT INTO tt_TABLESHIPPINGBOX(numBoxID
		,vcTrackingNumber)
   SELECT
   numBoxID
		,vcTrackingNumber
   FROM
   ShippingBox
   WHERE
   numShippingReportId IN(SELECT numShippingReportId FROM tt_TABLESHIPPINGREPORT)
   AND coalesce(vcTrackingNumber,'') <> '';

   open SWV_RefCur for
   SELECT
   numShippingReportId
		,TO_CHAR(ShippingReport.dtCreateDate,'yyyymmdd') AS vcShipDate
		,coalesce(ShippingReport.vcToName,'') AS vcToName
		,coalesce(ShippingReport.vcToAddressLine1,'') AS vcToAddressLine1
		,coalesce(ShippingReport.vcToAddressLine2,'') AS vcToAddressLine2
		,coalesce(ShippingReport.vcToCity,'') AS vcToCity
		,coalesce(fn_GetState(CAST(ShippingReport.vcToState AS NUMERIC)),'') AS vcToState
		,coalesce(ShippingReport.vcToZip,'') AS vcToZip
		,coalesce(fn_GetListName(CAST(ShippingReport.vcToCountry AS NUMERIC),false),'') AS vcToCountry
		,coalesce(ShippingReport.vcFromName,'') AS vcFromName
		,coalesce(ShippingReport.vcFromAddressLine1,'') AS vcFromAddressLine1
		,coalesce(ShippingReport.vcFromAddressLine2,'') AS vcFromAddressLine2
		,coalesce(ShippingReport.vcFromCity,'') AS vcFromCity
		,coalesce(fn_GetState(CAST(ShippingReport.vcFromState AS NUMERIC)),'') AS vcFromState
		,coalesce(ShippingReport.vcFromZip,'') AS vcFromZip
		,coalesce(fn_GetListName(CAST(ShippingReport.vcFromCountry AS NUMERIC),false),'') AS vcFromCountry
		,(CASE numShippingCompany WHEN 88 THEN 'UPS' WHEN 90 THEN 'USPS' WHEN 91 THEN 'FedEx' ELSE '' END) AS vcShipVia
		,CAST(ROUND(CAST(((SELECT SUM(coalesce(fltTotalWeight,0)*coalesce(intBoxQty,0)) FROM ShippingReportItems WHERE numShippingReportId = ShippingReport.numShippingReportID)) AS NUMERIC),2) AS DECIMAL(9,2)) AS fltTotalRegularWeight
		,CAST(ROUND(CAST((SELECT SUM(coalesce(fltDimensionalWeight,0)) FROM ShippingBox WHERE numShippingReportId = ShippingReport.numShippingReportID) AS NUMERIC),2) AS DECIMAL(9,2)) AS fltTotalDimensionalWeight
		,coalesce("vcCustomerPO#",'') AS "vcCustomerPO#"
   FROM
   ShippingReport
   INNER JOIN
   OpportunityMaster
   ON
   ShippingReport.numOppID = OpportunityMaster.numOppId
   WHERE
   numShippingReportId IN(SELECT numShippingReportId FROM tt_TABLESHIPPINGREPORT);


   open SWV_RefCur2 for
   SELECT
   numBoxID
		,coalesce(vcTrackingNumber,'')
		,numShippingReportId
		,fltHeight
		,fltLength
		,fltWidth
		,fltTotalWeight
		,BoxItems.Qty
   FROM
   ShippingBox
   LEFT JOIN LATERAL(SELECT
      SUM(coalesce(ShippingReportItems.intBoxQty,0)*fn_UOMConversion(Item.numBaseUnit,OpportunityItems.numItemCode,v_numDomainID,OpportunityItems.numUOMId)) AS Qty
      FROM
      ShippingReportItems
      INNER JOIN
      OpportunityBizDocItems
      ON
      ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
      INNER JOIN
      OpportunityItems
      ON
      OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OpportunityItems.numItemCode = Item.numItemCode
      WHERE
      numBoxID = ShippingBox.numBoxID) BoxItems on TRUE
   WHERE
   numBoxID IN(SELECT numBoxID FROM tt_TABLESHIPPINGBOX);
		
   open SWV_RefCur3 for
   SELECT
   numBoxID
		,CASE
   WHEN coalesce(Item.numItemGroup,0) > 0 AND coalesce(Item.bitMatrix,false) = false
   THEN(CASE WHEN LENGTH(coalesce(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE coalesce(Item.vcSKU,'') END)
   ELSE(CASE WHEN LENGTH(coalesce(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE coalesce(Item.vcSKU,'') END)
   END AS vcSKU
		,CASE
   WHEN coalesce(Item.numItemGroup,0) > 0 AND coalesce(Item.bitMatrix,false) = false
   THEN(CASE WHEN LENGTH(coalesce(WareHouseItems.vcBarCode,'')) > 0 THEN WareHouseItems.vcBarCode ELSE coalesce(Item.numBarCodeId,'') END)
   ELSE
      coalesce(Item.numBarCodeId,'')
   END AS vcUPC
		,CAST((fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,v_numDomainID, 
   coalesce(OpportunityItems.numUOMId,0))*OpportunityItems.numUnitHour) AS NUMERIC(18,2)) AS numOrderedUnits
		,coalesce(UOM.vcUnitName,'Units') AS vcUOM
		,(CASE
   WHEN charItemType = 'P' THEN 'Inventory Item'
   WHEN charItemType = 'S' THEN 'Service'
   WHEN charItemType = 'A' THEN 'Accessory'
   WHEN charItemType = 'N' THEN 'Non-Inventory Item'
   END) AS vcItemType
		,CAST((fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,v_numDomainID, 
   coalesce(OpportunityItems.numUOMId,0))*coalesce(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18,2)) AS numShippedUnits
		,OpportunityItems.numoppitemtCode
   FROM
   ShippingReportItems
   INNER JOIN
   OpportunityBizDocItems
   ON
   ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
   INNER JOIN
   OpportunityItems
   ON
   OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   LEFT JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   UOM
   ON
   OpportunityItems.numUOMId = UOM.numUOMId
   WHERE
   numBoxID IN(SELECT numBoxID FROM tt_TABLESHIPPINGBOX);
   RETURN;
END; $$;


