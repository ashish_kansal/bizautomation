-- Stored procedure definition script usp_RecreateGlobalCustomReportTables for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_RecreateGlobalCustomReportTables(v_numRefreshTimeInterval NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tempOppItemTable  VARCHAR(100);
   v_tempOrgContactTable  VARCHAR(100);
   SWV_ExecDyn  VARCHAR(5000);
BEGIN
   v_tempOppItemTable := '##OppItemCustomField';         
   v_tempOrgContactTable := '##OrgContactCustomField';   
  
   IF EXISTS(SELECT 'x' FROM sysobjects WHERE   table_name = v_tempOrgContactTable) then
      DROP TABLE IF EXISTS tt_ORGCONTACTCUSTOMFIELD CASCADE;
      SWV_ExecDyn := 'usp_OrganizationContactsCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   end if;  
  
   IF EXISTS(SELECT 'x' FROM sysobjects WHERE   table_name = v_tempOppItemTable) then
 
      DROP TABLE IF EXISTS tt_OPPITEMCUSTOMFIELD CASCADE;
      SWV_ExecDyn := 'usp_OppItemCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   end if;
   RETURN;
END; $$;


