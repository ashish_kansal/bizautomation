-- Stored procedure definition script USP_OpportunityMaster_GetDataForEDIInvoice810 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetDataForEDIInvoice810(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   '' AS AccountingID
		,OBD.vcBizDocID AS Invoice
		,TO_CHAR(OBD.dtCreatedDate,'mm/dd/yyyy')  AS InvoiceDate
		,MST.vcpOppName AS PO
		,TO_CHAR(MST.bintCreatedDate,'mm/dd/yyyy') AS PODate
		,'' AS DepartmentNumber
		,'' AS BillofLading
		,'' AS CarrierPro
		,'' AS SCAC
		,CASE
   WHEN coalesce(OBD.numShipVia,0) = 0 THEN(CASE WHEN MST.intUsedShippingCompany IS NULL THEN '-' WHEN MST.intUsedShippingCompany = -1 THEN 'Will-call' ELSE fn_GetListItemName(MST.intUsedShippingCompany::NUMERIC) END)
   WHEN OBD.numShipVia = -1 THEN 'Will-call'
   ELSE fn_GetListItemName(OBD.numShipVia)
   END AS ShipVia
		,fn_getOPPAddress(v_numOppID,v_numDomainID,2::SMALLINT) AS ShipToName
		,coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT) LIMIT 1),'') AS  "ShipToAddress-LineOne"
		,'' AS "ShipToAddress-LineTwo"
		,coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT) LIMIT 1),'') AS ShipToCity
		,coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT) LIMIT 1),'') AS ShipToState
		,coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT) LIMIT 1),'') AS ShipToZipcode
		,coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT) LIMIT 1),'') AS ShiptoCountry
		,'' AS Store
		,fn_getOPPAddress(v_numOppID,v_numDomainID,1::SMALLINT) AS BilltoName
		,coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,1::SMALLINT) LIMIT 1),'') AS "BilltoAddress-LineOne"
		,'' AS "BilltoAddress-LineTwo"
		,coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,1::SMALLINT) LIMIT 1),'') AS BilltoCity
		,coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,1::SMALLINT) LIMIT 1),'') AS BilltoState
		,coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,1::SMALLINT) LIMIT 1),'') AS BilltoZipcode
		,coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(v_numOppID,v_numDomainID,1::SMALLINT) LIMIT 1),'') AS BilltoCountry
		,'' AS BilltoCode
		,OBD.dtShippedDate AS ShipDate
		,'' AS TermsDescription
		,coalesce(BTR.numNetDueInDays,0) AS NetDaysDue
		,'' AS DiscountDaysDue
		,'' AS DiscountPercent
		,'' AS Note
		,'' AS Weight
		,'' AS TotalCasesShipped
		,'' AS TaxAmount
		,'' AS ChargeAmount1
		,'' AS ChargeAmount2
		,'' AS AllowancePercent1
		,'' AS AllowanceAmount1
		,'' AS AllowancePercent2
		,'' AS AllowanceAmount2
   FROM OpportunityBizDocs OBD
   JOIN OpportunityMaster MST ON MST.numOppId = OBD.numoppid
   LEFT JOIN BillingTerms BTR ON BTR.numTermsID = coalesce(MST.intBillingDays,0)
   WHERE OBD.numBizDocId = 287 AND MST.numOppId = v_numOppID;


   open SWV_RefCur2 for
   SELECT
   '' AS RowType
		,numSortOrder AS Line
		,V.vcPartNo AS VendorPart
		,CASE
   WHEN coalesce(Item.numItemGroup,0) > 0 AND coalesce(Item.bitMatrix,false) = false
   THEN(CASE WHEN LENGTH(coalesce(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE coalesce(Item.vcSKU,'') END)
   ELSE(CASE WHEN LENGTH(coalesce(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE coalesce(Item.vcSKU,'') END)
   END AS BuyerPart
		,numBarCodeId AS UPC
		,txtItemDesc AS Description
		--,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* ISNULL(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18, 2)) AS numShippedUnits
		,'' AS QuantityShipped
		,coalesce(UOM.vcUnitName,'Units') AS UOM
		,OpportunityItems.monPrice AS UnitPrice
		,CAST((fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,v_numDomainID, 
   coalesce(OpportunityItems.numUOMId,0))*OpportunityItems.numUnitHour) AS NUMERIC(18,2)) AS QuantityOrdered
		--,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,'' AS PackSize
		,'' AS ofInnerPacks
		,'' AS ItemAllowancePercent
		,'' AS ItemAllowanceAmount
		,(CASE
   WHEN charItemType = 'P' THEN 'Inventory Item'
   WHEN charItemType = 'S' THEN 'Service'
   WHEN charItemType = 'A' THEN 'Accessory'
   WHEN charItemType = 'N' THEN 'Non-Inventory Item'
   END) AS vcItemType
		,coalesce(OpportunityItems.vcNotes,'') AS vcNotes
		,OpportunityItems.numoppitemtCode AS BizOppItemID
   FROM
   OpportunityItems
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   LEFT JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   UOM
   ON
   OpportunityItems.numUOMId = UOM.numUOMId
   INNER JOIN Vendor V ON Item.numItemCode = V.numItemCode
   WHERE
   numOppId = v_numOppID
   AND coalesce(OpportunityItems.bitDropShip,false) = false;
   RETURN;
END; $$;



