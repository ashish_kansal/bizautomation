-- Stored procedure definition script USP_LandedCostExpenseAccountGet for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LandedCostExpenseAccountGet(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numLCExpenseId as "numLCExpenseId"
		,LCEA.numDomainID as "numDomainID"
		,LCEA.vcDescription as "vcDescription"
		,LCEA.numAccountId as "numAccountId"
		, COA.vcAccountName AS "vcAccountName"
   FROM   LandedCostExpenseAccount LCEA JOIN Chart_Of_Accounts AS COA
   ON LCEA.numAccountId = COA.numAccountId AND LCEA.numDomainID = COA.numDomainId
   WHERE  LCEA.numDomainID = v_numDomainId;
END; $$;














