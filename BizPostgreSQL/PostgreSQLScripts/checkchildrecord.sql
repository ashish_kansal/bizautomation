CREATE OR REPLACE FUNCTION CheckChildRecord(v_numOppID NUMERIC(9,0)  DEFAULT NULL,
    v_numDomainID NUMERIC(9,0)  DEFAULT 0)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ChildCount  INTEGER;
   v_RecurringChildCount  INTEGER;
   v_TotalCount  INTEGER;
   v_vcPoppName  TEXT;
BEGIN
   select   COUNT(distinct numChildOppID) INTO v_ChildCount FROM OpportunityLinking OL
   JOIN OpportunityMaster Opp  ON OL.numParentOppID = Opp.numOppId WHERE
   Opp.numOppId = v_numOppID
   AND
   Opp.numDomainId = v_numDomainID;

   select   COUNT(*) INTO v_RecurringChildCount FROM RecurringTransactionReport RTR
   JOIN OpportunityMaster Opp  ON RTR.numRecTranSeedId = Opp.numOppId WHERE
   Opp.numOppId = v_numOppID
   AND
   Opp.numDomainId = v_numDomainID;

   v_TotalCount := v_ChildCount::bigint+v_RecurringChildCount::bigint;

   select   Opp.vcpOppName INTO v_vcPoppName FROM OpportunityMaster Opp WHERE
   Opp.numOppId = v_numOppID
   AND
   Opp.numDomainId = v_numDomainID;

   IF v_TotalCount > 1 then
	
      v_vcPoppName :=  CONCAT(v_vcPoppName,' <a href=''#'' id=''anchChild'' onclick=''openChild(event,',
      v_numOppID,')''><img src=''../images/GLReport.png'' style=''width: 20px; height: 20px;'' /></a>');
   end if;
	
   RETURN coalesce(v_vcPoppName,'');
END; $$;

