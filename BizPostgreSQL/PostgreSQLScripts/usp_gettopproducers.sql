CREATE OR REPLACE FUNCTION usp_GetTopProducers(              
  --              
v_numDomainID NUMERIC,                
 v_dtDateFrom TIMESTAMP,                
 v_dtDateTo TIMESTAMP,                
 v_numUserCntID NUMERIC DEFAULT 0,                           
 v_intType NUMERIC DEFAULT 0,              
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 1 then
      open SWV_RefCur for
      SELECT vcFirstName || ' ' || vcLastname as Employee,
 (select count(*) from DivisionMaster where tintCRMType = 1 and numRecOwner = numContactID) as ProspectsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsWon,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintoppstatus = 2 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsLost,
 fn_GetPercentageWon(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS PercentageWon,
 (select count(*) from DivisionMaster where tintCRMType = 2 and numRecOwner = numContactID) as AccountsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and numDomainId = v_numDomainID
         and intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesOpened,
 (select sum(monPAmount) from OpportunityMaster where numrecowner = numContactId and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) as AmountWon
      from AdditionalContactsInformation A
      WHERE
      A.numDomainID = v_numDomainID
      and A.numContactId = v_numUserCntID;
   end if;                
                
   If v_tintRights = 2 then
 
      open SWV_RefCur for
      SELECT vcFirstName || ' ' || vcLastname as Employee,
 (select count(*) from DivisionMaster where tintCRMType = 1 and numRecOwner = numContactID) as ProspectsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintopptype = 1 and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsWon,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintopptype = 1  and tintoppstatus = 2 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsLost,
  fn_GetPercentageWon(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS PercentageWon,
 (select count(*) from DivisionMaster where tintCRMType = 2 and numRecOwner = numContactID) as AccountsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintopptype = 1  and numDomainId = v_numDomainID
         and intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesOpened,
 (select sum(monPAmount) from OpportunityMaster where numrecowner = numContactId and tintopptype = 1  and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) as AmountWon
      from AdditionalContactsInformation A
      WHERE
      A.numDomainID = v_numDomainID
      and A.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;                
                
   If v_tintRights = 3 then
 
      open SWV_RefCur for
      SELECT vcFirstName || ' ' || vcLastname as Employee,
 (select count(*) from DivisionMaster where tintCRMType = 1 and numRecOwner = numContactID) as ProspectsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintoppstatus = 1 and tintopptype = 1  and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsWon,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and tintoppstatus = 2 and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) AS DealsLost,
 fn_GetPercentageWon(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS PercentageWon,
 (select count(*) from DivisionMaster where tintCRMType = 2 and numRecOwner = numContactID) as AccountsOwned,
 (select count(*) from OpportunityMaster where numrecowner = numContactId and numDomainId = v_numDomainID
         and intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo and tintopptype = 1) as OpportunitiesOpened,
 (select sum(monPAmount) from OpportunityMaster where numrecowner = numContactId and tintopptype = 1  and tintoppstatus = 1 and numDomainId = v_numDomainID and bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo) as AmountWon
      from AdditionalContactsInformation A
      Left Outer Join DivisionMaster DM On A.numDivisionId = DM.numDivisionID
      WHERE  A.numDomainID = v_numDomainID
      and DM.numTerID  in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                 
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;
   RETURN;
END; $$;


