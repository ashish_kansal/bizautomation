-- Stored procedure definition script SP_Get_If_For_Marketing_Dept for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION SP_Get_If_For_Marketing_Dept(v_numGenericDocID INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   If Exists(Select 1 From GenericDocuments Where numGenericDocID = v_numGenericDocID And numDocCategory = 369 And vcDocumentSection = 'M') then
      open SWV_RefCur for
      Select 1;
   Else
      open SWV_RefCur for
      Select -1;
   end if;
   RETURN;
END; $$;


