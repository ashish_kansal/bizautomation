DROP FUNCTION IF EXISTS USP_GetPriceLevelItemPrice;

CREATE OR REPLACE FUNCTION USP_GetPriceLevelItemPrice(v_numItemCode NUMERIC(9,0),
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
--@numPricingID as numeric(9)=0,
v_numWareHouseItemID NUMERIC(9,0) DEFAULT NULL,
v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monListPrice  DECIMAL(20,5);
   v_monVendorCost  DECIMAL(20,5);	
   v_numRelationship  NUMERIC(9,0);
   v_numProfile  NUMERIC(9,0);
BEGIN
   v_monListPrice := 0;
   v_monVendorCost := 0;

   IF((v_numWareHouseItemID > 0) and exists(select * from Item where numItemCode = v_numItemCode and charItemType = 'P')) then
	
      select   coalesce(monWListPrice,0) INTO v_monListPrice FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      IF v_monListPrice = 0 then
		
         select   monListPrice INTO v_monListPrice FROM Item WHERE numItemCode = v_numItemCode;
      end if;
   ELSE
      select   monListPrice INTO v_monListPrice from Item where numItemCode = v_numItemCode;
   end if; 

   v_monVendorCost := fn_GetVendorCost(v_numItemCode);

	OPEN SWV_RefCur FOR
	SELECT
		numPricingID,
		numPriceRuleID,
		intFromQty,
		intToQty,
		tintRuleType,
		tintDiscountType,
		coalesce(NULLIF(pnt.vcPriceLevelName,''),CONCAT('Price Level ',rowId)) AS vcName,
		CAST(decDiscount AS DECIMAL(20,5)) decDiscount
	FROM
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) as rowId,
			numPricingID,
			numPriceRuleID,
			intFromQty,
			intToQty,
			tintRuleType,
			tintDiscountType,
			CASE
				WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
				THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100))
				WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
				THEN v_monListPrice -coalesce(decDiscount,0)
				WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
				THEN v_monVendorCost+(v_monVendorCost*(coalesce(decDiscount,0)/100))
				WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
				THEN v_monVendorCost+coalesce(decDiscount,0)
				WHEN tintRuleType = 3 --Named Price
				THEN coalesce(decDiscount,0)
				WHEN tintDiscountType = 3 --Named Price
				THEN coalesce(decDiscount,0)
			END AS decDiscount
		FROM
			PricingTable
		WHERE
			coalesce(numItemCode,0) = v_numItemCode
			AND coalesce(numCurrencyID,0) = 0
	) T1
	LEFT JOIN
		PricingNamesTable pnt 
	ON 
		pnt.tintPriceLevel = T1.rowId 
		AND pnt.numDomainID = v_numDomainID
	WHERE
		rowId IS NOT NULL
		OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY
		T1.rowId;
			  
	SELECT 
		numCompanyType
		,vcProfile 
	INTO 
		v_numRelationship
		,v_numProfile 
	FROM
		DivisionMaster D
	JOIN
		CompanyInfo C
	ON
		C.numCompanyId = D.numCompanyID 
	WHERE
		numDivisionID = v_numDivisionID;       

	OPEN SWV_RefCur2 FOR
	SELECT
		PT.numPricingID,
		PT.numPriceRuleID,
		coalesce(PT.intFromQty,0) AS intFromQty,
		coalesce(PT.intToQty,0) AS intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		coalesce(PT.vcName,'') AS vcName,
		(CASE 
			WHEN COALESCE(P.bitRoundTo,false) = true AND COALESCE(P.tintRoundTo,0) > 0
			THEN ROUND(CASE
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
						THEN v_monListPrice -(v_monListPrice*(coalesce(PT.decDiscount,0)/100))
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
						THEN v_monListPrice -coalesce(PT.decDiscount,0)
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
						THEN v_monVendorCost+(v_monVendorCost*(coalesce(PT.decDiscount,0)/100))
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
						THEN v_monVendorCost+coalesce(PT.decDiscount,0)
						WHEN PT.tintRuleType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
						WHEN PT.tintDiscountType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
					END) + (CASE P.tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END)
			ELSE
				CASE
					WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
					THEN v_monListPrice -(v_monListPrice*(coalesce(PT.decDiscount,0)/100))
					WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
					THEN v_monListPrice -coalesce(PT.decDiscount,0)
					WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
					THEN v_monVendorCost+(v_monVendorCost*(coalesce(PT.decDiscount,0)/100))
					WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
					THEN v_monVendorCost+coalesce(PT.decDiscount,0)
					WHEN PT.tintRuleType = 3 --Named Price
					THEN coalesce(PT.decDiscount,0)
					WHEN PT.tintDiscountType = 3 --Named Price
					THEN coalesce(PT.decDiscount,0)
				END
		END) AS decDiscount
	FROM
		Item I
	JOIN
		PriceBookRules P
	ON
		I.numDomainID = P.numDomainID
	LEFT JOIN
		PriceBookRuleDTL PDTL
	ON
		P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN
		PriceBookRuleItems PBI
	ON
		P.numPricRuleID = PBI.numRuleID
	LEFT JOIN
		PriceBookPriorities PP
	ON
		PP.Step2Value = P.tintStep2
		AND PP.Step3Value = P.tintStep3
	JOIN
		PricingTable PT
	ON
		P.numPricRuleID = PT.numPriceRuleID
		AND coalesce(PT.numCurrencyID,0) = 0
	WHERE
		I.numItemCode = v_numItemCode
		AND P.tintRuleFor = 1
		AND P.tintPricingMethod = 1
		AND (((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
			OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3
			OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
			OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6
			OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
			OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
			)
	ORDER BY 
		PP.Priority ASC;

	RETURN;
END; $$;
      


