Create or replace FUNCTION USP_RecurrenceConfiguration_SetDisable(v_numRecConfigID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE RecurrenceConfiguration SET bitDisabled = true WHERE numRecConfigID = v_numRecConfigID;
   RETURN;
END; $$;  


