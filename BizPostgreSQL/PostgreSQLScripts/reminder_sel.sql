-- Stored procedure definition script Reminder_Sel for PostgreSQL
CREATE OR REPLACE FUNCTION Reminder_Sel(v_LookAheadWindowEndTime	TIMESTAMP,	-- a date/time before the current system time which defines the point past
							-- which outstanding Reminder notifications need to be shown to the end
							-- user.
	v_ResourceID			INTEGER		-- the key of the Resource to have reminders selected on behalf of.
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- Select the Activities that need Reminders displayed between Now and the Lookahead Window.
   open SWV_RefCur for SELECT
   Activity.AllDayEvent,
		Activity.Duration,
		Activity.ActivityDescription,
		Activity.ActivityID,
		cast(Resource.ResourceName as VARCHAR(255)),
		Activity.StartDateTimeUtc,
		cast(Activity.Subject as VARCHAR(255)),
		Activity.Location,
		Activity.EnableReminder,
		Activity.ReminderInterval,
		Activity.ShowTimeAs,
		Activity.Importance,
		Activity.Status,
		Activity._ts,
		Activity.itemId,
		Activity.ChangeKey
   FROM((Activity INNER JOIN ActivityResource ON Activity.ActivityID = ActivityResource.ActivityID) INNER JOIN Resource ON ActivityResource.ResourceID = Resource.ResourceID)
   WHERE
   Resource.ResourceID = v_ResourceID  AND
   Activity.StartDateTimeUtc < v_LookAheadWindowEndTime  AND
   Activity.RecurrenceID =(-999)  AND
   Activity.Status < 3  AND
   Activity.EnableReminder = true;	-- Must use '1' in T-SQL for bit column truth value instead of 'True'
END; $$;













