-- Stored procedure definition script USP_ManagegeDashboard for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManagegeDashboard(v_numUserGroupID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_tintReportType SMALLINT DEFAULT NULL,
v_vcHeader VARCHAR(100) DEFAULT '',
v_vcFooter VARCHAR(100) DEFAULT '',
v_tintChartType SMALLINT DEFAULT NULL,
v_numDashBoardReptID NUMERIC(9,0) DEFAULT 0,
v_numReportID NUMERIC(9,0) DEFAULT 0,
v_tintColumn SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numUserGroupID > 0 then

      if v_numDashBoardReptID = 0 then
	
         update Dashboard set tintRow = tintRow+1 where numGroupUserCntID = v_numUserGroupID and tintColumn = v_tintColumn  and bitGroup = true;
         insert into Dashboard(numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup)
		values(v_numReportID, v_numUserGroupID, 1, v_tintColumn, v_tintReportType, v_vcHeader, v_vcFooter, v_tintChartType, true);
		
         v_numDashBoardReptID := CURRVAL('Dashboard_seq');
      ELSEIF v_numDashBoardReptID > 0
      then
	
         update Dashboard set
         numReportID = v_numReportID,numGroupUserCntID = v_numUserGroupID,tintReportType = v_tintReportType,
         vcHeader = v_vcHeader,vcFooter = v_vcFooter,tintChartType = v_tintChartType,
         bitGroup = true where numDashBoardReptID = v_numDashBoardReptID;
      end if;
   ELSEIF v_numUserGroupID = 0
   then

      if v_numDashBoardReptID = 0 then
	
         update Dashboard set tintRow = tintRow+1 where numGroupUserCntID = v_numUserCntID and tintColumn = v_tintColumn and bitGroup = false;
         insert into Dashboard(numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup)
		values(v_numReportID, v_numUserCntID, 1, v_tintColumn, v_tintReportType, v_vcHeader, v_vcFooter, v_tintChartType, false);
		
         v_numDashBoardReptID := CURRVAL('Dashboard_seq');
      ELSEIF v_numDashBoardReptID > 0
      then
	
         update Dashboard set
         numReportID = v_numReportID,numGroupUserCntID = v_numUserCntID,tintReportType = v_tintReportType,
         vcHeader = v_vcHeader,vcFooter = v_vcFooter,tintChartType = v_tintChartType,
         bitGroup = false where numDashBoardReptID = v_numDashBoardReptID;
      end if;
   end if;

   open SWV_RefCur for select v_numDashBoardReptID;
END; $$;












