-- Stored procedure definition script USP_ReportListMaster_CalculateComission for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportListMaster_CalculateComission(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numComPayPeriodID  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_TempDomainID  INTEGER;
BEGIN
   IF coalesce(v_numDomainID,0) > 0 then
	
      IF EXISTS(SELECT numComPayPeriodID FROM CommissionPayPeriod WHERE numDomainID = v_numDomainID AND tintPayPeriod = 0) then
		
         UPDATE
         CommissionPayPeriod
         SET
         numModifiedBy = 0,dtModified = LOCALTIMESTAMP,dtStart = TIMEZONE('UTC',now())+INTERVAL '-12 month',
         dtEnd = TIMEZONE('UTC',now())
         WHERE
         numDomainID = v_numDomainID AND numComPayPeriodID = v_numDomainID*-1;
         v_numComPayPeriodID := v_numDomainID*-1;
		-- RECALCULATE PAY PERIOD COMMISSION
      ELSE
         INSERT INTO CommissionPayPeriod(numDomainID,
				dtStart,
				dtEnd,
				tintPayPeriod,
				numCreatedBy,
				dtCreated)
			VALUES(v_numDomainID,
				TIMEZONE('UTC',now())+INTERVAL '-12 month',
				TIMEZONE('UTC',now()),
				0,
				0,
				LOCALTIMESTAMP) RETURNING numComPayPeriodID INTO v_numComPayPeriodID;
      end if;

		PERFORM USP_CalculateCommission(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID,  v_ClientTimeZoneOffset := 0);
		PERFORM USP_CalculateTimeAndExpense(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID, v_ClientTimeZoneOffset := 0);
		PERFORM USP_CalculateCommissionSalesReturn(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID, v_ClientTimeZoneOffset := 0);
		PERFORM USP_CalculateCommissionOverPayment(v_numDomainID := 0);
   ELSE
      IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 2) AS DATE) <> CAST(LOCALTIMESTAMP  AS DATE) AND EXTRACT(HOUR FROM LOCALTIMESTAMP) > 3 then
         UPDATE ElasticSearchLastReindex SET LastReindexDate = LOCALTIMESTAMP  WHERE ID = 2;
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numDomainID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMP(numDomainID)
         SELECT
         numDomainId
         FROM
         Domain
         WHERE
         numDomainId > 0;
         select   COUNT(*) INTO v_iCount FROM tt_TEMP;
         WHILE v_i <= v_iCount LOOP
            select   numDomainID INTO v_TempDomainID FROM tt_TEMP WHERE ID = v_i;
            IF EXISTS(SELECT numComPayPeriodID FROM CommissionPayPeriod WHERE numDomainID = v_TempDomainID AND tintPayPeriod = 0) then
				
               UPDATE
               CommissionPayPeriod
               SET
               numModifiedBy = 0,dtModified = LOCALTIMESTAMP,dtStart = TIMEZONE('UTC',now())+INTERVAL '-12 month',
               dtEnd = TIMEZONE('UTC',now())
               WHERE
               numDomainID = v_TempDomainID AND numComPayPeriodID = v_TempDomainID::bigint*-1;
               v_numComPayPeriodID := v_TempDomainID::bigint*-1;
				-- RECALCULATE PAY PERIOD COMMISSION
            ELSE
               INSERT INTO CommissionPayPeriod(numDomainID,
						dtStart,
						dtEnd,
						tintPayPeriod,
						numCreatedBy,
						dtCreated)
					VALUES(v_TempDomainID,
						TIMEZONE('UTC',now())+INTERVAL '-12 month',
						TIMEZONE('UTC',now()),
						0,
						0,
						LOCALTIMESTAMP) RETURNING numComPayPeriodID INTO v_numComPayPeriodID;
            end if;

			PERFORM USP_CalculateCommission(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID,  v_ClientTimeZoneOffset := 0);
			PERFORM USP_CalculateTimeAndExpense(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID, v_ClientTimeZoneOffset := 0);
			PERFORM USP_CalculateCommissionSalesReturn(v_numComPayPeriodID := v_numComPayPeriodID,v_numDomainID := v_TempDomainID, v_ClientTimeZoneOffset := 0);
			PERFORM USP_CalculateCommissionOverPayment(v_numDomainID := 0);

            v_i := v_i::bigint+1;
         END LOOP;
      end if;
   end if;
   RETURN;
END; $$;


