-- Stored procedure definition script USP_GetCountOfJournalEntry for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCountOfJournalEntry(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,      
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select Count(numChartAcntId) From General_Journal_Details
   Where numChartAcntId = v_numParntAcntId And numDomainId = v_numDomainId;
END; $$;












