-- Stored procedure definition script USP_GetOpportunityBizDocsCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpportunityBizDocsCount(v_numOppId NUMERIC(9,0),
v_numDomainId NUMERIC(9,0),
v_numBizDocsId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
BEGIN
   select   tintopptype INTO v_tintOppType FROM OpportunityMaster WHERE numDomainId = v_numDomainId AND numOppId = v_numOppId;
 
   IF v_tintOppType = 1 then --Sales
 
      open SWV_RefCur for
      SELECT Count(*) FROM DepositeDetails WHERE numOppBizDocsID = v_numBizDocsId;
   ELSE
      open SWV_RefCur for
      SELECT Count(*) FROM BillPaymentDetails WHERE coalesce(numOppBizDocsID,0) > 0 AND numOppBizDocsID = v_numBizDocsId;
   end if;
   RETURN;
END; $$;


