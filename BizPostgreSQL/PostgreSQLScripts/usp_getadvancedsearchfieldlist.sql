-- Stored procedure definition script USP_GetAdvancedSearchFieldList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAdvancedSearchFieldList(v_numDomainId NUMERIC(9,0),
    v_numFormId INTEGER,
    v_numAuthGroupId NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql  
	--added by chintan to select Custom field form multiple location
   AS $$
   DECLARE
   v_vcLocationID  VARCHAR(100);
	--DECLARE @vcSectionName VARCHAR(500);SET @vcSectionName=''

   v_Nocolumns  SMALLINT;
BEGIN
   v_vcLocationID := '0';
	--DECLARE @vcSectionName VARCHAR(500);SET @vcSectionName=''

   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = v_numFormId;

	--select ItemNumber as intSectionID,Item as vcSectionName from dbo.DelimitedSplit8K(@vcSectionName,',')
   open SWV_RefCur for
   select intSectionID,vcSectionName,Loc_id from DycFormSectionDetail where v_numFormId = numFormID;

   BEGIN
      CREATE TEMP SEQUENCE tt_tempFieldsList_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFIELDSLIST CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSLIST
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      RowNo INTEGER,
      intSectionID INTEGER,
      numModuleID NUMERIC(9,0),
      numFormFieldId  VARCHAR(20),
      vcFormFieldName VARCHAR(100),
      vcFieldType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcDbColumnName VARCHAR(100),
      vcPropertyName VARCHAR(100),
      vcToolTip VARCHAR(1000),
      vcListItemType CHAR(3),
      intColumnNum INTEGER,
      intRowNum INTEGER,
      boolRequired BOOLEAN, 
      numAuthGroupID NUMERIC(18,0),
      vcFieldDataType CHAR(1),
      boolAOIField SMALLINT,
      numFormID NUMERIC(18,0),
      vcLookBackTableName VARCHAR(100),
      GRP_ID INTEGER,
      intFieldMaxLength INTEGER
   );

   v_Nocolumns := 0;                

   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = v_numFormId and numAuthGroupID = v_numAuthGroupId and numDomainID = v_numDomainId
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = v_numFormId and numAuthGroupID = v_numAuthGroupId and numDomainID = v_numDomainId) TotalRows;

   if v_Nocolumns > 0 then

      INSERT INTO tt_TEMPFIELDSLIST(intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)
      SELECT intSectionID,numModuleID,row_number() over(PARTITION BY intSectionID order by intSectionID), CAST(CAST(numFieldID AS VARCHAR(15)) || vcFieldType AS VARCHAR(20)) AS numFormFieldId,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    coalesce(bitIsRequired,false) AS boolRequired,
                    0 AS numAuthGroupID,
                    vcFieldDataType,
                    0 AS boolAOIField,
                    numFormID,
                    vcLookBackTableName,CAST(0 AS INTEGER) as GRP_ID, intFieldMaxLength
      FROM View_DynamicColumns
      where numFormId = v_numFormId and numAuthGroupID = v_numAuthGroupId and numDomainID = v_numDomainId AND 1 =(CASE WHEN numFormId = 29 AND numFieldID = 311 THEN 0 ELSE 1 END)
      UNION
      SELECT CAST(0 AS INTEGER) as intSectionID,CAST(0 AS NUMERIC(9,0)) as numModuleID,row_number() over(PARTITION BY GRP_ID order by GRP_ID),CAST(CAST(numFieldId AS VARCHAR(15)) || 'C' AS VARCHAR(20)) AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType AS vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    CAST(CAST(CAST(numFieldId AS VARCHAR(15)) || 'C' AS VARCHAR(100)) AS VARCHAR(100)) AS vcDbColumnName,
                    CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)) AS vcPropertyName,
                    CAST(CAST('' AS VARCHAR(1000)) AS VARCHAR(1000)) AS vcToolTip,
                    CAST(CAST(CASE WHEN numListID > 0 THEN 'LI' ELSE '' END AS CHAR(3)) AS CHAR(3)) AS vcListItemType,
                   tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    coalesce(bitIsRequired,false) as boolRequired,
                    0 AS numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
                    0 AS boolAOIField,
                    CAST(v_numFormId AS NUMERIC(18,0)) AS numFormID,
                    CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)) AS vcLookBackTableName,GRP_ID,CAST(0 AS INTEGER) as intFieldMaxLength
      from View_DynamicCustomColumns
      where numFormId = v_numFormId and numAuthGroupID = v_numAuthGroupId and numDomainID = v_numDomainId  AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','));
   ELSE
      INSERT INTO tt_TEMPFIELDSLIST(intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)
      SELECT intSectionID,numModuleID,row_number() over(PARTITION BY intSectionID order by intSectionID), CAST(CAST(numFieldID AS VARCHAR(15)) || vcFieldType AS VARCHAR(20)) AS numFormFieldId,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    0 AS intColumnNum,
                    0 AS intRowNum,
                    coalesce(bitIsRequired,false) AS boolRequired,
                    0 AS numAuthGroupID,
                    vcFieldDataType,
                    0 AS boolAOIField,
                    numFormID,
                    vcLookBackTableName,CAST(0 AS INTEGER) as GRP_ID,intFieldMaxLength
      FROM    View_DynamicDefaultColumns
      WHERE   numFormID = v_numFormId
      AND bitDeleted = false
      AND numDomainID = v_numDomainId and intSectionID != 0
      AND 1 =(CASE WHEN numFormId = 29 AND numFieldID = 311 THEN 0 ELSE 1 END)
      UNION
      SELECT CAST(0 AS INTEGER) as intSectionID,CAST(0 AS NUMERIC(9,0)) as numModuleID,row_number() over(PARTITION BY GRP_ID order by GRP_ID),CAST(CAST(Fld_id AS VARCHAR(15)) || 'C' AS VARCHAR(20)) AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    L.vcFieldType AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    coalesce(C.numlistid,0) AS numListID,
                    CAST(CAST(CAST(Fld_id AS VARCHAR(15)) || 'C' AS VARCHAR(100)) AS VARCHAR(100)) AS vcDbColumnName,
                    CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)) AS vcPropertyName,
                    CAST(CAST('' AS VARCHAR(1000)) AS VARCHAR(1000)) AS vcToolTip,
                    CAST(CAST(CASE WHEN C.numlistid > 0 THEN 'LI' ELSE '' END AS CHAR(3)) AS CHAR(3)) AS vcListItemType,
                    0 AS intColumnNum,
                    0 AS intRowNum,
                    coalesce(V.bitIsRequired,false) as boolRequired,
                    0 AS numAuthGroupID,
                    CASE WHEN C.numlistid > 0 THEN 'N' ELSE 'V' END AS vcFieldDataType,
                    0 AS boolAOIField,
                    CAST(v_numFormId AS NUMERIC(18,0)) AS numFormID,
                    CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(100)) AS vcLookBackTableName,GRP_ID,CAST(0 AS INTEGER) as intFieldMaxLength
      FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldId = C.Fld_id
      JOIN CFW_Loc_Master L on C.Grp_id = L.Loc_id
      WHERE   C.numDomainID = v_numDomainId
      AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','));
   end if;
   Update tt_TEMPFIELDSLIST set RowNo = RowNo::bigint -1;
   Update tt_TEMPFIELDSLIST set  intRowNum =(CAST(RowNo::bigint AS decimal)/3)+1,intColumnNum =(MOD(RowNo,3))+1; 

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFIELDSLIST ORDER BY intRowNum,intColumnNum;
        
   RETURN;
END; $$;
        


