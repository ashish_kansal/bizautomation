-- Stored procedure definition script USP_MoveSite for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MoveSite(v_numSiteID NUMERIC(9,0) ,
    v_numFromDomainID NUMERIC(9,0) ,
    v_numToDomainID NUMERIC(9,0),OUT RETURN_VALUE INTEGER)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_error  VARCHAR(1000);
BEGIN
   -- BEGIN TRAN
UPDATE  Category
   SET     numDomainID = v_numToDomainID
   WHERE   numDomainID = v_numFromDomainID;
   UPDATE  RedirectConfig
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
   UPDATE  SiteBreadCrumb
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
   UPDATE  StyleSheets
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
 --update [StyleSheetDetails] set numDomainID
   UPDATE  SiteTemplates
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
   UPDATE  PageElementDetail
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
   UPDATE  SiteMenu
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
   UPDATE  SitePages
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
 
 --update [SiteCategories] set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
   UPDATE  Sites
   SET     numDomainID = v_numToDomainID
   WHERE   numSiteID = v_numSiteID
   AND numDomainID = v_numFromDomainID;
 
 --update Ratings set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 --update Review set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
   DELETE FROM eCommerceDTL WHERE eCommerceDTL.numDomainID = v_numFromDomainID
   AND eCommerceDTL.numSiteId = v_numSiteID;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK 
v_error := SQLERRM;
      RAISE NOTICE '%',v_error;
      RETURN_VALUE := 1;
      RETURN;
END; $$;

