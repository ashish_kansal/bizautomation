-- Stored procedure definition script usp_GetCustomFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCustomFields(v_recid NUMERIC
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(CFW_Fld_Master.FLd_label as VARCHAR(255)), cast(CFW_FLD_Values_Cont.Fld_Value as VARCHAR(255)) , cast(recid as VARCHAR(255))
   from CFW_Fld_Master INNER JOIN CFW_FLD_Values_Cont ON CFW_Fld_Master.Fld_id = CFW_FLD_Values_Cont.fld_id where
   fld_label = 'Salutation'
   and recid = v_recid;
END; $$;












