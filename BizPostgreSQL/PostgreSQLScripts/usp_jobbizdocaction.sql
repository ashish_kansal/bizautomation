-- Stored procedure definition script USP_JobBizDocAction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_JobBizDocAction()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtLog  TIMESTAMP;
   v_dtNow  TIMESTAMP;
   v_maxDomainID  NUMERIC(9,0);
   v_minDomainID  NUMERIC(9,0);
BEGIN
   select   coalesce(max(dtBizDocLog),'10/Feb/2010') INTO v_dtLog from BizDocActionLog;

   v_dtNow := TIMEZONE('UTC',now());

   select   max(numDomainId) INTO v_maxDomainID FROM   Domain; 
   select   min(numDomainId) INTO v_minDomainID FROM   Domain; 
   WHILE v_minDomainID <= v_maxDomainID LOOP
      RAISE NOTICE 'DomainID=%',SUBSTR(CAST(v_minDomainID AS VARCHAR(20)),1,20);
      RAISE NOTICE '--------------';
      PERFORM USP_ManageBizDocActionItem(v_numDomainID := v_minDomainID,v_dtFromDate := v_dtLog,v_dtTodate := v_dtNow);


	---------------------------------------------------
      select   min(numDomainId) INTO v_minDomainID FROM   Domain WHERE  numDomainId > v_minDomainID;
   END LOOP;



   insert into BizDocActionLog  select v_dtNow;
RETURN;
END; $$;



