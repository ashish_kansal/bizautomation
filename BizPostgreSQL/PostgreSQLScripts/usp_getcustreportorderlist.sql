-- Stored procedure definition script usp_getCustReportOrderlist for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustReportOrderlist(v_ReportId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from CustReportOrderlist where numCustomReportId = v_ReportId order by numOrder;
END; $$;












