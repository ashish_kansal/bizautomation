-- Stored procedure definition script USP_GetSpecificDocDtls for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSpecificDocDtls(v_numGenericDocID NUMERIC(9,0),      
v_numUserCntID NUMERIC(9,0) DEFAULT 0 ,
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(A.bintCreatedDate as VARCHAR(255)),cast(A.bintModifiedDate as VARCHAR(255)),cast(A.cUrlType as VARCHAR(255)),cast(A.numDocStatus as VARCHAR(255)),
cast(A.numCreatedBy as VARCHAR(255)),cast(A.numDocCategory as VARCHAR(255)),cast(A.numDomainId as VARCHAR(255)),cast(A.numModifiedBy as VARCHAR(255)),cast(A.numRecID as VARCHAR(255)),
cast(A.numGenericDocID as VARCHAR(255)),cast(A.vcDocdesc as VARCHAR(255)),cast(A.VcDocName as VARCHAR(255)),cast(A.vcDocumentSection as VARCHAR(255)),
cast(coalesce(A.VcFileName,'') as VARCHAR(255)) AS VcFileName ,cast(A.vcfiletype as VARCHAR(255))
 , (select count(*) from DocumentWorkflow
      where numDocID = v_numGenericDocID and numContactID = v_numUserCntID and cDocType = vcDocumentSection and tintApprove = 0) as AppReq,
 (select count(*) from DocumentWorkflow
      where numDocID = v_numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow
      where numDocID = v_numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
 (select count(*) from DocumentWorkflow
      where numDocID = v_numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending
   from GenericDocuments A where numGenericDocID = v_numGenericDocID and numDomainId = v_numDomainID;
END; $$;












