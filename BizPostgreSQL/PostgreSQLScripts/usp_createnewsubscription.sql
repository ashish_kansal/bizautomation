-- Stored procedure definition script USP_CreateNewSubscription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CreateNewSubscription(v_numSubscriberID NUMERIC(9,0),  
v_numUserCntID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into SubscriberHstr(numSubscriberID,intNoofSubscribers,dtSubStartDate,
dtSubRenewDate,bitTrial,numAdminContactID,bitStatus,dtSuspendedDate,vcSuspendedReason,
numTargetDomainID,numDomainID,numCreatedBy,dtCreatedDate)
   select numSubscriberID,intNoofUsersSubscribed,dtSubStartDate,
dtSubEndDate,bitTrial,numAdminContactID,(CASE WHEN bitActive=1 THEN true ELSE false END),dtSuspendedDate,vcSuspendedReason,
numTargetDomainID,numDomainID,v_numUserCntID,TIMEZONE('UTC',now()) from Subscribers where numSubscriberID = v_numSubscriberID;  
  
  
   update Subscribers set intNoofUsersSubscribed = 0,intNoofPartialSubscribed = 0,intNoofMinimalSubscribed = 0,
   dtSubStartDate = null,dtSubEndDate = null,bitTrial = false,bitActive = 0,
   dtSuspendedDate = null,vcSuspendedReason = '' where numSubscriberID = v_numSubscriberID;
   RETURN;
END; $$;


