CREATE OR REPLACE FUNCTION fn_GeTimeDtlsbySalesStgID
(
	v_numOppID NUMERIC
	,v_numOppStageID NUMERIC
	,v_tintType SMALLINT
)
RETURNS VARCHAR(100) LANGUAGE plpgsql
AS $$          
	DECLARE
		v_vcRetValue  VARCHAR(250);
BEGIN
	SELECT 
		CAST(CAST((DATEDIFF('minute',dtFromDate,dtToDate)/60) AS DECIMAL(10,1)) AS VARCHAR(100)) || ' ( R/H ' || case when numtype = 1 then CAST(monAmount AS VARCHAR(100)) else '0.00' end || ')' INTO v_vcRetValue 
	FROM 
		timeandexpense 
	WHERE 
		numOppId = v_numOppID 
		and numStageId = v_numOppStageID
		and numCategory = 1 
		and tintTEType = 1;    
           
	RETURN v_vcRetValue;
END; $$;

