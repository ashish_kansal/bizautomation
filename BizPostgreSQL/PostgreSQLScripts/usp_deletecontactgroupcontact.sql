-- Stored procedure definition script usp_DeleteContactGroupContact for PostgreSQL
CREATE OR REPLACE FUNCTION usp_DeleteContactGroupContact(v_numContactID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM AOIContactLink where numContactID = v_numContactID;  
  
  
  
   DELETE FROM groupcon
   WHERE numContactID = v_numContactID;   
  
  
  
            
   open SWV_RefCur for SELECT 0;
END; $$;












