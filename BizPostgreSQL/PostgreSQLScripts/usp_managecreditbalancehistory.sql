-- Stored procedure definition script USP_ManageCreditBalanceHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCreditBalanceHistory(v_numDomainId	NUMERIC(18,0),
v_numCreatedBy	NUMERIC(18,0),
v_numDivisionID	NUMERIC(18,0) DEFAULT NULL,
v_numReturnID NUMERIC(9,0) DEFAULT NULL,
v_monAmount	DECIMAL(20,5) DEFAULT NULL,
v_vcReference	VARCHAR(200) DEFAULT null,
v_vcMemo	VARCHAR(50) DEFAULT null,
v_tintPaymentAction SMALLINT DEFAULT NULL,
v_tintOppType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numDivisionID,0) = 0 AND coalesce(v_numReturnID,0) > 0 then

      select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster OM join Returns R on OM.numOppId = R.numOppID where R.numReturnID = v_numReturnID;
   end if;

   IF v_numReturnID = 0 then 
      v_numReturnID := NULL;
   end if;

   INSERT INTO CreditBalanceHistory(numReturnID
           ,monAmount
           ,vcReference
           ,vcMemo
           ,dtCreateDate
           ,dtCreatedBy
           ,numDomainID,numDivisionID)
     VALUES(v_numReturnID
           ,Case When v_tintPaymentAction = 1 then v_monAmount else v_monAmount*-1 end
           ,v_vcReference
           ,v_vcMemo
           ,LOCALTIMESTAMP
            ,v_numCreatedBy
           ,v_numDomainId,v_numDivisionID);

    
   IF v_tintOppType = 1 then --Sales
      update DivisionMaster set monSCreditBalance = coalesce(monSCreditBalance,0)+Case When v_tintPaymentAction = 1 then v_monAmount else v_monAmount*-1 end where numDivisionID = v_numDivisionID and numDomainID = v_numDomainId;
   ELSEIF v_tintOppType = 2
   then --Purchase
      update DivisionMaster set monPCreditBalance = coalesce(monPCreditBalance,0)+Case When v_tintPaymentAction = 1 then v_monAmount else v_monAmount*-1 end where numDivisionID = v_numDivisionID and numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;



