-- Stored procedure definition script USP_GetBeginningAmtForCashFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBeginningAmtForCashFlow(v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                                
 v_dtFromDate TIMESTAMP DEFAULT NULL,                                                        
 v_dtToDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN; 
--   Declare @numParentAccountId as numeric(9)    
--  Set @numParentAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = @numDomainId) --and numAccountId = 1     
--     
--                    
-- Select isnull(Sum(dbo.fn_GetCurrentOpeningBalanceForCashFlow(COA.numAccountId,@dtFromDate ,@dtToDate)),0) as BeginningAmt                                        
--   From Chart_Of_Accounts COA Where  COA.numDomainId= @numDomainId And COA.numAccountId <>@numParentAccountId And COA.numAcntType=813 And COA.numOpeningBal is not null --And CoA.numParntAcntId =1                                    
--   And COA.dtOpeningDate<'' + convert(varchar(300),@dtFromDate)     
END; $$;


