-- Stored procedure definition script USP_GetOrderDetailForBluePayHPF for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOrderDetailForBluePayHPF(v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   OpportunityMaster.numDomainId
		,OpportunityMaster.numOppId
		,OpportunityMaster.numDivisionId
		,OpportunityMaster.numContactId
		,OpportunityMaster.numCreatedBy
		,OpportunityMaster.numCurrencyID
		,cast(coalesce(OpportunityMaster.fltExchangeRate,1) as DOUBLE PRECISION) AS fltExchangeRate
		,cast(Domain.numCurrencyID as VARCHAR(255)) AS numBaseCurrencyID
		,cast(coalesce(vcBluePaySuccessURL,'') as VARCHAR(255)) AS vcBluePaySuccessURL
		,cast(coalesce(vcBluePayDeclineURL,'') as VARCHAR(255)) AS vcBluePayDeclineURL
   FROM
   OpportunityMaster
   INNER JOIN
   Domain
   ON
   OpportunityMaster.numDomainId = Domain.numDomainId
   INNER JOIN
   OpportunityBizDocs
   ON
   OpportunityMaster.numOppId = OpportunityBizDocs.numoppid
   WHERE
   OpportunityMaster.numOppId = v_numOppID
   AND OpportunityBizDocs.numOppBizDocsId = v_numOppBizDocID;
END; $$;












