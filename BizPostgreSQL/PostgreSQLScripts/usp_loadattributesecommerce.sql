DROP FUNCTION IF EXISTS USP_LoadAttributesEcommerce;

CREATE OR REPLACE FUNCTION USP_LoadAttributesEcommerce(v_numItemCode NUMERIC(9,0) DEFAULT 0,      
v_numListID NUMERIC(9,0) DEFAULT 0,      
v_strAtrr VARCHAR(1000) DEFAULT '',    
v_numWareHouseID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemGroup  NUMERIC(9,0);      
   v_strSQL  TEXT; 
   v_chrItemType  VARCHAR(5);
   v_numDomainID  NUMERIC(18,0);
   v_bitMatrix  BOOLEAN;
   v_vcItemName  VARCHAR(500);
   v_Cnt  INTEGER DEFAULT 1;
   v_SplitOn  CHAR(1);
BEGIN
   select   numItemGroup, charItemType, numDomainID, coalesce(bitMatrix,false), vcItemName INTO v_ItemGroup,v_chrItemType,v_numDomainID,v_bitMatrix,v_vcItemName FROM
   Item WHERE
   numItemCode = v_numItemCode;      
      
      
   IF v_strAtrr != '' then
      v_SplitOn := ',';
      IF v_bitMatrix = true then
		
         v_strSQL := 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE';
      ELSE
         v_strSQL := 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE';
      end if;
      While(POSITION(v_SplitOn IN v_strAtrr) > 0) LOOP
         IF v_Cnt = 1 then
            v_strSQL := coalesce(v_strSQL,'') || ' fld_value = cast(NULLIF(''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || ''','''') as NUMERIC(18,0))';
         ELSE
            v_strSQL := coalesce(v_strSQL,'') || ' OR fld_value=''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         end if;
         v_strAtrr := SUBSTR(v_strAtrr,POSITION(v_SplitOn IN v_strAtrr)+1,LENGTH(v_strAtrr));
         v_Cnt := v_Cnt::bigint+1;
      END LOOP;
      IF v_bitMatrix = true then
		
         IF v_Cnt = 1 then
            v_strSQL := CONCAT(v_strSQL,' fld_value=''',v_strAtrr,''' AND COALESCE(bitMatrix,false) = true AND COALESCE(Item.numItemGroup,0) = ',
            v_ItemGroup,' AND Item.vcItemName=''',
            v_vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) >',v_Cnt::bigint -1);
         ELSE
            v_strSQL := CONCAT(v_strSQL,' OR fld_value=''',v_strAtrr,''' AND COALESCE(bitMatrix,false) = true AND COALESCE(Item.numItemGroup,0) = ',
            v_ItemGroup,' AND Item.vcItemName=''',
            v_vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) > ',v_Cnt::bigint -1);
         end if;
      ELSE
         IF v_Cnt = 1 then
            v_strSQL := coalesce(v_strSQL,'') || ' fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
         ELSE
            v_strSQL := coalesce(v_strSQL,'') || ' OR fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
         end if;
      end if;
   end if;      
    
      
   IF v_numItemCode > 0 then
	
      IF v_strAtrr = '' then
		
         If v_chrItemType <> 'P' then
			
            open SWV_RefCur for
            SELECT numListItemID,vcData from Listdetails where numListID = v_numListID AND numDomainid = v_numDomainID;
         ELSEIF v_bitMatrix = true
         then
			
            open SWV_RefCur for
            SELECT
            numListItemID,vcData
            FROM
            Listdetails
            WHERE
            numListItemID IN(SELECT
               DISTINCT(fld_value::NUMERIC)
               FROM
               Item I
               JOIN
               WareHouseItems W
               ON
               I.numItemCode = W.numItemID
               JOIN
               ItemAttributes
               ON
               I.numItemCode = ItemAttributes.numItemCode
               JOIN
               CFW_Fld_Master M
               ON
               ItemAttributes.Fld_ID = M.Fld_id
               WHERE
               I.numDomainID = v_numDomainID
               AND W.numDomainID = v_numDomainID
               AND M.numlistid = v_numListID
               AND vcItemName = v_vcItemName
               AND bitMatrix = true
               AND numItemGroup = v_ItemGroup
               AND (W.numWareHouseID = v_numWareHouseID OR coalesce(v_numWareHouseID,0) = 0));
         ELSE
            open SWV_RefCur for
            SELECT
            numListItemID,vcData
            FROM
            Listdetails
            WHERE
            numListItemID IN(SELECT
               DISTINCT(fld_value::NUMERIC)
               FROM
               WareHouseItems W
               JOIN
               CFW_Fld_Values_Serialized_Items CSI
               ON
               W.numWareHouseItemID = CSI.RecId
               JOIN
               CFW_Fld_Master M
               ON
               CSI.Fld_ID = M.Fld_id
               JOIN
               ItemGroupsDTL
               ON
               CSI.Fld_ID = ItemGroupsDTL.numOppAccAttrID
               WHERE
               M.numlistid = v_numListID
               AND tintType = 2
               AND numItemID = v_numItemCode
               AND W.numWareHouseID = v_numWareHouseID);
         end if;
      ELSE
         If v_chrItemType <> 'P' then
			
            open SWV_RefCur for
            select numListItemID,vcData from Listdetails where numListID = v_numListID AND numDomainid = v_numDomainID;
         ELSEIF v_bitMatrix = true
         then
			
            RAISE NOTICE '%',123;
            v_strSQL := CONCAT('SELECT 
											numListItemID,vcData 
										FROM 
											ListDetails 
										WHERE 
											numlistitemid IN (
																SELECT 
																	DISTINCT(fld_value)::NUMERIC 
																FROM 
																	Item I
																JOIN
																	WareHouseItems W      
																ON
																	I.numItemCode=W.numItemID
																JOIN 
																	ItemAttributes
																ON 
																	I.numItemCode=ItemAttributes.numItemCode      
																JOIN 
																	CFW_Fld_Master M 
																ON 
																	ItemAttributes.Fld_ID=M.Fld_ID                             
																WHERE 
																	I.numDomainID=',v_numDomainID,'
																	AND W.numDomainID =',v_numDomainID,
            '
																	AND M.numListID=',v_numListID,'
																	AND vcItemName=''',
            v_vcItemName,'''
																	AND bitMatrix = true
																	AND numItemGroup =',v_ItemGroup,'
																	AND (W.numWareHouseID=',
            v_numWareHouseID,' OR ' || CAST(coalesce(v_numWareHouseID,0) AS VARCHAR(30)) || ' = 0)
																	AND I.numItemCode IN (',
            v_strSQL,'))');
            RAISE NOTICE '%',v_strSQL;
            OPEN SWV_RefCur FOR EXECUTE v_strSQL;
         ELSE
            v_strSQL := 'select numListItemID,vcData from ListDetails where cast(numListItemID as TEXT) in(select distinct(fld_value) from WareHouseItems W      
				join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID = CSI.RecId      
				join CFW_Fld_Master M on CSI.Fld_ID = M.Fld_id      
				join ItemGroupsDTL on CSI.Fld_ID = ItemGroupsDTL.numOppAccAttrID     
				where  M.numlistid =' || SUBSTR(CAST(v_numListID AS VARCHAR(20)),1,20) || ' and tintType = 2 and CSI.bitSerialized = false and numItemID =' || SUBSTR(CAST(v_numItemCode AS VARCHAR(20)),1,20) || ' and W.numWareHouseID =' || SUBSTR(CAST(v_numWareHouseID AS VARCHAR(20)),1,20) || ' and fld_value != ''0'' and fld_value != ''''     
 
				and W.numWareHouseItemID in(' || coalesce(v_strSQL,'') || '))';
            RAISE NOTICE '%',v_strSQL;
            OPEN SWV_RefCur FOR EXECUTE v_strSQL;
         end if;
      end if;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


