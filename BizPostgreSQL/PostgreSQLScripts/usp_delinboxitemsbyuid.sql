-- Stored procedure definition script USP_DelInboxItemsByUid for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_DelInboxItemsByUid(v_numDomainID  NUMERIC(9,0),
           v_numUserCntID NUMERIC(9,0),
           v_numUid       NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmailHstrID  NUMERIC(9,0);
BEGIN
   select   numEmailHstrID INTO v_numEmailHstrID FROM   EmailHistory WHERE  numDomainID = v_numDomainID
   AND numUserCntId = v_numUserCntID
   AND numUid = v_numUid;
   DELETE FROM EmailHistory
   WHERE  numDomainID = v_numDomainID
   AND numUserCntId = v_numUserCntID
   AND numUid = v_numUid;
   DELETE FROM EmailHStrToBCCAndCC
   WHERE  numEmailHstrID = v_numEmailHstrID;
   DELETE FROM EmailHstrAttchDtls
   WHERE  numEmailHstrID = v_numEmailHstrID;
   RETURN;
END; $$;


