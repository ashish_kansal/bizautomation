-- Stored procedure definition script USP_BizRecurrence_WorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
Create or replace FUNCTION USP_BizRecurrence_WorkOrder(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numParentOppID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemCode  NUMERIC(9,0);
   v_numUnitHour  DOUBLE PRECISION;
   v_numWarehouseItmsID  NUMERIC(9,0);
   v_vcItemDesc  VARCHAR(1000);
   v_numWarehouseID  NUMERIC(18,0);
  
   v_numWOId  NUMERIC(9,0);
   v_vcInstruction  VARCHAR(1000);
   v_bintCompliationDate  TIMESTAMP;
   v_numWOAssignedTo  NUMERIC(9,0);

   v_numoppitemtCode  NUMERIC(18,0);

	


   WOCursor CURSOR FOR 
   SELECT 
   X.numoppitemtCode,
			X.numItemCode,
			fn_UOMConversion(X.numUOMId,numItemCode,v_numDomainID,null::NUMERIC)*x.numUnitHour AS numUnitHour,
			X.numWarehouseItmsID,
			X.vcItemDesc,
			X.vcInstruction,
			X.bintCompliationDate,
			X.numAssignedTo
   FROM(SELECT
      OpportunityItems.numoppitemtCode,
					OpportunityItems.numItemCode,
					OpportunityItems.bitWorkOrder,
					OpportunityItems.numUOMId,
					OpportunityItems.numUnitHour,
					OpportunityItems.numWarehouseItmsID,
					OpportunityItems.vcItemDesc,
					WorkOrder.vcInstruction,
					WorkOrder.bintCompliationDate,
					WorkOrder.numAssignedTo
      FROM
      OpportunityItems
      INNER JOIN	
      WorkOrder
      ON
      WorkOrder.numItemCode = OpportunityItems.numItemCode AND
      WorkOrder.numWareHouseItemId = OpportunityItems.numWarehouseItmsID AND
      WorkOrder.numOppId = OpportunityItems.numOppId
      WHERE
      OpportunityItems.numOppId = v_numParentOppID) X  
   WHERE 
   X.bitWorkOrder = true;
BEGIN
   IF(SELECT COUNT(*) FROM WorkOrder WHERE numOppId = v_numOppID) = 0 AND(SELECT COUNT(*) FROM WorkOrder WHERE numOppId = v_numParentOppID) > 0 then
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEM
      (
         numItemKitID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numWarehouseItmsID NUMERIC(18,0),
         numCalculatedQty DOUBLE PRECISION,
         txtItemDesc VARCHAR(500),
         sintOrder INTEGER,
         numUOMId NUMERIC(18,0),
         numQtyItemsReq DOUBLE PRECISION,
         charItemType CHAR
      );
      OPEN WOCursor;
      FETCH NEXT FROM WOCursor INTO v_numoppitemtCode,v_numItemCode,v_numUnitHour,v_numWarehouseItmsID,v_vcItemDesc,
      v_vcInstruction,v_bintCompliationDate,v_numWOAssignedTo;
      WHILE FOUND LOOP
         DELETE FROM tt_TEMPITEM;
         v_numWarehouseID := coalesce((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItmsID),0);
         INSERT INTO tt_TEMPITEM(numItemKitID
			,numItemCode
			,numCalculatedQty
			,numWarehouseItmsID
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId
			,charItemType) with recursive CTE(numItemKitID,numItemCode,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,
         numQtyItemsReq,charItemType)
         AS(select CAST(v_numItemCode AS NUMERIC(9,0)) AS numItemKitID,numItemCode AS numItemCode
			,CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))*v_numUnitHour) AS DOUBLE PRECISION) AS numCalculatedQty,coalesce(DTL.vcItemDesc,txtItemDesc) AS txtItemDesc,coalesce(sintOrder,0) AS sintOrder,
			DTL.numUOMID AS numUOMId,DTL.numQtyItemsReq AS numQtyItemsReq,Item.charItemType AS charItemType
         from Item
         INNER join ItemDetails DTL on numChildItemID = numItemCode
         where  numItemKitID = v_numItemCode
         UNION ALL
         select CAST(DTL.numItemKitID AS NUMERIC(9,0)) AS numItemKitID,i.numItemCode AS numItemCode,
			CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))*c.numCalculatedQty) AS DOUBLE PRECISION) AS numCalculatedQty,coalesce(DTL.vcItemDesc,i.txtItemDesc) AS txtItemDesc,coalesce(DTL.sintOrder,0) AS sintOrder,
			DTL.numUOMID AS numUOMId,DTL.numQtyItemsReq AS numQtyItemsReq,i.charItemType AS charItemType
         from Item i
         INNER JOIN ItemDetails DTL on DTL.numChildItemID = i.numItemCode
         INNER JOIN CTE c ON DTL.numItemKitID = c.numItemCode 
			--WHERE i.charItemType IN ('P','I')
         where DTL.numChildItemID != v_numItemCode) SELECT
         numItemKitID
			,numItemCode
			,numCalculatedQty
			,coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = numItemCode AND numWareHouseID = v_numWarehouseID ORDER BY numWareHouseItemID LIMIT 1),0)
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId
			,charItemType
         FROM
         CTE;
         IF EXISTS(SELECT numItemKitID FROM tt_TEMPITEM WHERE charItemType = 'P' AND coalesce(numWarehouseItmsID,0) = 0) then
		
            RAISE NOTICE 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
            RETURN;
         end if;
         INSERT INTO WorkOrder(numItemCode, numQtyItemsReq, numWareHouseItemId, numCreatedBy, bintCreatedDate,	numDomainId,
		numWOStatus, numOppId, vcItemDesc, vcInstruction, bintCompliationDate, numAssignedTo, numOppItemID)
	VALUES(v_numItemCode,v_numUnitHour,v_numWarehouseItmsID,v_numUserCntID,TIMEZONE('UTC',now()),v_numDomainID,
		0,v_numOppID,v_vcItemDesc,v_vcInstruction,v_bintCompliationDate,v_numWOAssignedTo, v_numoppitemtCode);
	
         v_numWOId := CURRVAL('WorkOrder_seq');
         INSERT INTO WorkOrderDetails(numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWarehouseItemID,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId)
         SELECT
         v_numWOId,numItemKitID,numItemCode,numCalculatedQty,numWarehouseItmsID,txtItemDesc,sintOrder,numQtyItemsReq,numUOMId FROM CTE;
         FETCH NEXT FROM WOCursor INTO v_numoppitemtCode,v_numItemCode,v_numUnitHour,v_numWarehouseItmsID,v_vcItemDesc,
         v_vcInstruction,v_bintCompliationDate,v_numWOAssignedTo;
      END LOOP;
      CLOSE WOCursor;
   end if;
END; $$;  
--Created by anoop jayaraj                                                          


