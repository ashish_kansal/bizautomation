-- Stored procedure definition script usp_GetPhoneDet for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetPhoneDet(v_numContactId NUMERIC  --   
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CONCAT(numPhone,', ',numPhoneExtension) as vcPhonedet FROM AdditionalContactsInformation where numContactId = v_numContactId;
END; $$;












