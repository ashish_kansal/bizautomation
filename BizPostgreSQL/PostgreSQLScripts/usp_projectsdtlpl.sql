-- Stored procedure definition script USP_ProjectsDTLPL for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ProjectsDTLPL(v_numProId NUMERIC(9,0) DEFAULT null  ,                  
v_numDomainID NUMERIC(9,0) DEFAULT NULL,    
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  pro.vcProjectName,
		 pro.numIntPrjMgr,
		 fn_GetContactName(pro.numIntPrjMgr) as numintPrjMgrName,
		 pro.intDueDate,
		 fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgrName,
		 pro.numCustPrjMgr,
         pro.txtComments,
		 fn_GetContactName(pro.numAssignedTo) as numAssignedToName,
		 pro.numAssignedTo,
        (select  count(*) from GenericDocuments   where numRecID = v_numProId and  vcDocumentSection = 'P') as DocumentCount,
		CASE
   WHEN tintProStatus = 1 THEN 100
   ELSE coalesce((SELECT SUM(tintPercentage)
         FROM   ProjectsStageDetails
         WHERE  numProId = v_numProId
         AND bitStageCompleted = true),0)
   END AS TProgress,
         (select  count(*) from ProjectsOpportunities PO Left JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId WHERE numProId = v_numProId) as LinkedOrderCount,
         vcProjectID,
         '' AS TimeAndMaterial,
         numContractId,
         coalesce((SELECT vcContractName FROM ContractManagement WHERE numContractId = pro.numcontractId),'') AS numContractIdName,
         coalesce(numProjectType,0) AS numProjectType,
         fn_GetListItemName(numProjectType) AS vcProjectTypeName,coalesce(pro.numProjectStatus,0) as numProjectStatus,
fn_GetListItemName(numProjectStatus) AS vcProjectStatusName,
(SELECT SUBSTR((SELECT ',' ||  A.vcFirstName || ' ' || A.vcLastname || CASE WHEN coalesce(SR.numContactType,0) > 0 THEN '(' || fn_GetListItemName(SR.numContactType) || ')' ELSE '' END
         FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
         JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
         WHERE SR.numDomainID = pro.numdomainId AND SR.numModuleID = 5 AND SR.numRecordID = pro.numProId
         AND UM.numDomainID = pro.numdomainId and UM.numDomainID = A.numDomainID),2,200000)) AS numShareWith ,
	coalesce(AD.vcStreet,'') ||
   ' <br/>' || coalesce(AD.vcCity,'') || ' ,'
   || coalesce(fn_GetState(AD.numState),'') || ' '
   || coalesce(AD.vcPostalCode,'') || ' <br>'
   || coalesce(fn_GetListItemName(AD.numCountry),'') as ShippingAddress,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtmEndDate,
pro.dtmStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtmStartDate,
coalesce(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
   from ProjectsMaster pro
   LEFT JOIN AddressDetails AD ON AD.numAddressID = pro.numAddressID
   where numProId = v_numProId     and pro.numdomainId =  v_numDomainID;
END; $$;












