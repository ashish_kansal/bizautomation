-- Stored procedure definition script Usp_SaveCaseOpportunities for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_SaveCaseOpportunities(v_numCaseId NUMERIC,
    v_numDomainId NUMERIC,
    v_strOppid VARCHAR(4000),
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

    
    --delete and insert
   IF v_tintMode = 0 then
        
      DELETE  FROM CaseOpportunities
      WHERE   numCaseId = v_numCaseId
      AND numDomainid = v_numDomainId;
      IF v_strOppid <> '' then
                
         IF SUBSTR(CAST(v_strOppid AS VARCHAR(10)),1,10) <> '' then

            INSERT  INTO CaseOpportunities(numCaseId,
                                      numoppid,
                                      numDomainid,
                                      numOppItemID)
            SELECT  v_numCaseId,
                                            X.numOppId,
                                            v_numDomainId,
                                            X.numOppItemID
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strOppid AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numOppId NUMERIC(18,0) PATH 'numOppId',
					numOppItemID NUMERIC(18,0) PATH 'numOppItemID'
			) AS X;
         end if;
      end if;
   end if;
        --Delete selected 
   IF v_tintMode = 1 then
        
      DELETE  FROM CaseOpportunities
      WHERE   numCaseId = v_numCaseId
      AND numDomainid = v_numDomainId
      AND numOppItemID IN(SELECT  Id
      FROM    SplitIDs(v_strOppid,','));
   end if;
  
		--Add selected , only insert
   IF v_tintMode = 2 then
        
      IF v_strOppid <> '' then
                
         IF SUBSTR(CAST(v_strOppid AS VARCHAR(10)),1,10) <> '' then
            INSERT  INTO CaseOpportunities(numCaseId,
                                      numoppid,
                                      numDomainid,
                                      numOppItemID)
            SELECT  v_numCaseId,
                                            X.numOppId,
                                            v_numDomainId,
                                            X.numOppItemID
            FROM
			XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strOppid AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numOppId NUMERIC(18,0) PATH 'numOppId',
					numOppItemID NUMERIC(18,0) PATH 'numOppItemID'
			) AS X;
         end if;
      end if;
   end if;
  
/****** Object:  StoredProcedure [dbo].[USP_SaveCashCreditCardRecurringDetails]    Script Date: 07/26/2008 16:20:55 ******/
   RETURN;
END; $$;


