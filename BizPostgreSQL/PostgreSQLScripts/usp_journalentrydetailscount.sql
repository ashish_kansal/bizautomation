-- Stored procedure definition script USP_JournalEntryDetailsCount for PostgreSQL
CREATE OR REPLACE FUNCTION USP_JournalEntryDetailsCount(v_numJournalId NUMERIC(9,0),  
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select count(numTransactionId) From General_Journal_Details
   Where numJournalId = v_numJournalId And numDomainId = v_numDomainId;
END; $$;












