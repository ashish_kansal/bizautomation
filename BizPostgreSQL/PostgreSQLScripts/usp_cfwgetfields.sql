DROP FUNCTION IF EXISTS USP_cfwGetFields;

CREATE OR REPLACE FUNCTION USP_cfwGetFields(v_PageId SMALLINT,                              
v_numRelation NUMERIC(9,0),                              
v_numRecordId NUMERIC(9,0),                  
v_numDomainID NUMERIC(9,0),
v_numLocationType NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_PageId = 1 then
	
      open SWV_RefCur for
      select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0') ELSE coalesce(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
      on Rec.Fld_ID = CFW_Fld_Master.Fld_id
      where CFW_Fld_Master.Grp_id = v_PageId and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomainID
      union
      select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip from CFw_Grp_Master
      where tintType = 1 and loc_Id = v_PageId   and numDomainID = v_numDomainID
      order by Fld_id;
   end if;                              

	---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
   IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 then
	
      open SWV_RefCur for
      select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0') ELSE coalesce(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
      on Rec.Fld_ID = CFW_Fld_Master.Fld_id
      where CFW_Fld_Master.Grp_id = v_PageId and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomainID
      UNION
      select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0') ELSE coalesce(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      left join GetCustomFieldDTLIDAndValues(1,v_numRecordId) Rec
      on Rec.Fld_ID = CFW_Fld_Master.Fld_id
      where CFW_Fld_Master.Grp_id = 1 and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomainID
      union
      select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip from CFw_Grp_Master
      where tintType = 1 and loc_Id = v_PageId   and numDomainID = v_numDomainID
      order by fld_id;
   end if;


                              
   if v_PageId = 4 then 
      open SWV_RefCur for
      select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,numOrder,
		Rec.FldDTLID,CASE WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0') ELSE coalesce(Rec.Fld_Value,'') END as Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      left join CFw_Grp_Master
      on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
      on Rec.Fld_ID = CFW_Fld_Master.Fld_id
      where CFW_Fld_Master.Grp_id = v_PageId and numRelation = v_numRelation and CFW_Fld_Master.numDomainID = v_numDomainID                             
		--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
		--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
		--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
		--where AddC.numContactID=@numRecordId                              
                              
      union
      select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
		CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip from CFw_Grp_Master
      where tintType = 1 and loc_Id = v_PageId   and numDomainID = v_numDomainID
      order by fld_id;
   end if;                              
                              
   if v_PageId = 2 or  v_PageId = 3 or v_PageId = 5 or v_PageId = 6 or v_PageId = 7 or v_PageId = 8     or v_PageId = 11   or v_PageId = 17 then
	
      IF(v_PageId = 5) then
		
         open SWV_RefCur for
         select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,coalesce(Rec.FldDTLID,0) AS FldDTLID,
			(CASE
         WHEN coalesce(Rec.Fld_Value,'') = '' AND CFW_Fld_Master.fld_type = 'CheckBox' THEN '0'
         WHEN coalesce(Rec.Fld_Value,'') = '' AND CFW_Fld_Master.fld_type = 'SelectBox' THEN '0'
         WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0')
         ELSE coalesce(Rec.Fld_Value,'')
         END)   AS Value,
			subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip,CFW_Fld_Master.vcItemsLocation  from CFW_Fld_Master
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
         on Rec.Fld_ID = CFW_Fld_Master.Fld_id
         where CFW_Fld_Master.Grp_id = v_PageId and CFW_Fld_Master.numDomainID = v_numDomainID   AND 1 =(CASE WHEN CFW_Fld_Master.vcItemsLocation IS NULL THEN 1 WHEN v_numLocationType IN(SELECT CAST(Items As NUMERIC) FROM Split(CFW_Fld_Master.vcItemsLocation,',') WHERE Items <> '') THEN 1 ELSE 0 END)
         union
         select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,CAST('0' AS VARCHAR(1)) as Value,CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip,CAST(null as VARCHAR(255)) AS vcItemsLocation from CFw_Grp_Master
         where tintType = 1 and loc_Id = v_PageId  and numDomainID = v_numDomainID ORDER BY fld_id;
      ELSE
         open SWV_RefCur for
         select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,coalesce(Rec.FldDTLID,0) AS FldDTLID,
		(CASE
         WHEN coalesce(Rec.Fld_Value,'') = '' AND CFW_Fld_Master.fld_type = 'CheckBox' THEN '0'
         WHEN coalesce(Rec.Fld_Value,'') = '' AND CFW_Fld_Master.fld_type = 'SelectBox' THEN '0'
         WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0')
         ELSE coalesce(Rec.Fld_Value,'')
         END)   AS Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip,CFW_Fld_Master.vcItemsLocation  from CFW_Fld_Master
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
         on Rec.Fld_ID = CFW_Fld_Master.Fld_id
         where CFW_Fld_Master.Grp_id = v_PageId and CFW_Fld_Master.numDomainID = v_numDomainID
         union
         select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip,CAST(null as VARCHAR(255)) AS vcItemsLocation from CFw_Grp_Master
         where tintType = 1 and loc_Id = v_PageId  and numDomainID = v_numDomainID ORDER BY Fld_id;
      end if;
   end if;

   if v_PageId = 18 then
	
      open SWV_RefCur for
      select CFW_Fld_Master.Fld_id,fld_type,fld_label,numlistid,coalesce(Rec.FldDTLID,0) AS FldDTLID,
		(CASE
      WHEN coalesce(Rec.Fld_Value,'') = '' AND fld_type = 'CheckBox' THEN '0'
      WHEN coalesce(Rec.Fld_Value,'') = '' AND fld_type = 'SelectBox' THEN '0'
      WHEN fld_type = 'SelectBox' THEN coalesce(Rec.Fld_Value,'0')
      ELSE coalesce(Rec.Fld_Value,'') END)  AS Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master
      left join CFw_Grp_Master
      on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      left join GetCustomFieldDTLIDAndValues(v_PageId,v_numRecordId) Rec
      on Rec.Fld_ID = CFW_Fld_Master.Fld_id
      where CFW_Fld_Master.Grp_id = 5 and CFW_Fld_Master.numDomainID = v_numDomainID
      union
      select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,CAST(Grp_id AS VARCHAR) as TabId,Grp_Name as tabname,vcURLF,CAST(null as VARCHAR(255)) as vcToolTip from CFw_Grp_Master
      where tintType = 1 and loc_Id = 5  and numDomainID = v_numDomainID ORDER BY Fld_id;
   end if;

   open SWV_RefCur2 for
   select CAST(Grp_id AS VARCHAR) as TabId,Grp_Name AS TabName from CFw_Grp_Master where
(loc_Id = v_PageId OR 1 =(CASE WHEN v_PageId IN(12,13,14) THEN CASE WHEN loc_Id = 1 THEN 1 ELSE 0 END ELSE 0 END))
   and numDomainID = v_numDomainID
/*tintType stands for static tabs */ 
   AND tintType <> 2;
   RETURN;
END; $$;
--Created by anoop jayaraj                                                          


