DROP FUNCTION IF EXISTS USP_ReportListMaster_TotalBackOrderValue;

CREATE OR REPLACE FUNCTION USP_ReportListMaster_TotalBackOrderValue(v_numDomainID NUMERIC
,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
,v_vcFilterValue TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   with
   cteReports
   AS(SELECT 
--isnull(dbo.fn_GetContactName(OpportunityMaster.numRecOwner),'') as RecordOwner
   CASE v_vcFilterBy
   WHEN 1 THEN  coalesce(fn_GetContactName(OpportunityMaster.numassignedto),'')
   WHEN 2 THEN coalesce(fn_GetContactName(OpportunityMaster.numrecowner),'')
   ELSE coalesce(fn_GetContactName(OpportunityMaster.numrecowner),'')
   END as RecordOwner
,
 CASE v_vcFilterBy
   WHEN 1 THen OpportunityMaster.numassignedto
   when 2 then OpportunityMaster.numrecowner
   END AS ID,
 v_vcFilterBy as vcFilterBy,
--OpportunityMaster.numAssignedTo as ID,
	coalesce(((CASE WHEN OpportunityMaster.tintopptype = 1 AND OpportunityMaster.tintoppstatus = 1 THEN(CASE WHEN (Item.charItemType = 'P' AND coalesce(OpportunityItems.bitDropShip,false) = false AND coalesce(Item.bitAsset,false) = false) THEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,
         OpportunityItems.numWarehouseItmsID,(CASE WHEN coalesce(Item.bitKitParent,false) = true AND coalesce(Item.bitAssembly,false) = true THEN false WHEN coalesce(Item.bitKitParent,false) = true THEN true ELSE false END)) ELSE 0 END) ELSE 0 END)*(coalesce(Item.monAverageCost,'0'))),0) as BackOrderValue
	,coalesce(((CASE WHEN OpportunityMaster.tintopptype = 1 AND OpportunityMaster.tintoppstatus = 1 THEN(CASE WHEN (Item.charItemType = 'P' AND coalesce(OpportunityItems.bitDropShip,false) = false AND coalesce(Item.bitAsset,false) = false) THEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,
         OpportunityItems.numWarehouseItmsID,(CASE WHEN coalesce(Item.bitKitParent,false) = true AND coalesce(Item.bitAssembly,false) = true THEN false WHEN coalesce(Item.bitKitParent,false) = true THEN true ELSE false END)) ELSE 0 END) ELSE 0 END)*(coalesce(OpportunityItems.monPrice,'0'))),0) as Sales
   FROM OpportunityMaster
   inner join DivisionMaster on OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   inner join CompanyInfo on CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
   inner join AdditionalContactsInformation on OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId  
--Left Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId 
--Left Join OpportunityBizDocItems on OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID 
   Left Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId 
--and OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode 
   Left Join Item on OpportunityItems.numItemCode = Item.numItemCode 
--Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode 
--Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID 
--LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID  
--Left Join Warehouses on Warehouses.numDomainId =  @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  

   WHERE 1 = 1 AND OpportunityMaster.numDomainId = v_numDomainID AND (((OpportunityMaster.tintopptype = 1 and OpportunityMaster.tintoppstatus = 1)))
   AND(CASE WHEN OpportunityMaster.tintopptype = 1 AND OpportunityMaster.tintoppstatus = 1 THEN(CASE WHEN (Item.charItemType = 'P' AND coalesce(OpportunityItems.bitDropShip,false) = false AND coalesce(Item.bitAsset,false) = false) THEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,
         OpportunityItems.numWarehouseItmsID,(CASE WHEN coalesce(Item.bitKitParent,false) = true AND coalesce(Item.bitAssembly,false) = true THEN false WHEN coalesce(Item.bitKitParent,false) = true THEN true ELSE false END)) ELSE 0 END) ELSE 0 END) > 0
   AND
   1 =(CASE
   WHEN LENGTH(coalesce(v_vcFilterValue,cast(0 as TEXT))) > 0 AND v_vcFilterBy IN(1,2,3)
   THEN(CASE v_vcFilterBy
      WHEN 1  -- Assign To  
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 2  -- Record Owner  
      THEN(CASE WHEN OpportunityMaster.numrecowner IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END) 
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT ID FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
      END)
   ELSE 1
   END))
   SELECT cteReports.RecordOwner,Count(cteReports.RecordOwner) as TotalRecords,cteReports.ID,cteReports.vcFilterBy,
CAST(SUM(Sales) as DECIMAL(18,0)) as SalesPrice , CAST(Sum(BackOrderValue) as DECIMAL(18,0)) as CostPrice
   FROM cteReports
   GROUP BY cteReports.RecordOwner,cteReports.ID,cteReports.vcFilterBy
   order by SalesPrice desc;
END; $$;












