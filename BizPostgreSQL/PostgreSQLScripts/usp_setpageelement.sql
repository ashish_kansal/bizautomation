-- Stored procedure definition script usp_SetPageElement for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SetPageElement(v_numModuleID INTEGER,
	v_numPageID INTEGER,
	v_vcOldElementInitiatingAction VARCHAR(20) DEFAULT '',
	v_vcElementInitiatingAction VARCHAR(20) DEFAULT '',
	v_vcElementInitiatingActionDesc VARCHAR(50) DEFAULT '',
	v_vcElementInitiatingActionType VARCHAR(30) DEFAULT '',
	v_vcOldChildElementInitiatingAction VARCHAR(50) DEFAULT '',
	v_vcChildElementInitiatingAction VARCHAR(50) DEFAULT '',
	v_vcChildElementInitiatingActionType VARCHAR(30) DEFAULT '',
	v_vcActionName VARCHAR(15) DEFAULT NULL,
	v_vcActionIndicator CHAR(1) DEFAULT NULL   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcActionIndicator = 'A' then
		
      INSERT INTO PageActionElements(numModuleID, numPageID, vcActionName, vcElementInitiatingAction,
					vcElementInitiatingActionDesc, vcElementInitiatingActionType, vcChildElementInitiatingAction,
					vcChildElementInitiatingActionType)
				VALUES(v_numModuleID, v_numPageID, v_vcActionName, v_vcElementInitiatingAction,
					v_vcElementInitiatingActionDesc, v_vcElementInitiatingActionType, v_vcChildElementInitiatingAction,
					v_vcChildElementInitiatingActionType);
   ELSE
      UPDATE PageActionElements
      SET vcActionName = v_vcActionName,vcElementInitiatingAction = v_vcElementInitiatingAction,
      vcElementInitiatingActionDesc = v_vcElementInitiatingActionDesc,
      vcElementInitiatingActionType = v_vcElementInitiatingActionType,
      vcChildElementInitiatingAction = v_vcChildElementInitiatingAction,
      vcChildElementInitiatingActionType = v_vcChildElementInitiatingActionType
      WHERE numModuleID = v_numModuleID
      AND numPageID = v_numPageID
      AND vcElementInitiatingAction = v_vcOldElementInitiatingAction
      AND vcChildElementInitiatingAction = v_vcOldChildElementInitiatingAction;
   end if;
   RETURN;
END; $$;


