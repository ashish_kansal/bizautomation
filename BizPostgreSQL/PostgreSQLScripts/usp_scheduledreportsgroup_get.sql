-- Stored procedure definition script USP_ScheduledReportsGroup_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_Get(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ID AS "ID"
		,vcName AS "vcName"
		,tintFrequency AS "tintFrequency"
		,dtStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS "dtStartDate"
		,numEmailTemplate AS "numEmailTemplate"
		,vcSelectedTokens AS "vcSelectedTokens"
		,vcRecipientsEmail AS "vcRecipientsEmail"
   FROM
   ScheduledReportsGroup
   WHERE
   numDomainID = v_numDomainID
   AND ID = v_numSRGID;
END; $$;












