-- Stored procedure definition script USP_ManageSiteBreadCumbs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSiteBreadCumbs(INOUT v_numBreadCrumbID NUMERIC(18,0) DEFAULT 0 , 
	v_numSiteID NUMERIC(18,0) DEFAULT 0, 
	v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_numPageID NUMERIC(18,0) DEFAULT 0,
	v_tintLevel SMALLINT DEFAULT NULL,
	v_vcDisplayName VARCHAR(15) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF(v_numBreadCrumbID = 0 AND NOT EXISTS(SELECT numBreadCrumbID
   FROM SiteBreadCrumb
   WHERE vcDisplayName = v_vcDisplayName
   AND numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID)) then
	
      INSERT INTO SiteBreadCrumb(numPageID
			   ,numSiteID
			   ,numDomainID
			   ,tintLevel
			   ,vcDisplayName)
		 VALUES(v_numPageID
			   ,v_numSiteID
			   ,v_numDomainID
			   ,v_tintLevel
			   ,v_vcDisplayName);
		
      v_numBreadCrumbID := CURRVAL('SiteBreadCrumb_seq');
   ELSEIF NOT EXISTS(SELECT numBreadCrumbID
   FROM SiteBreadCrumb
   WHERE vcDisplayName = v_vcDisplayName
   AND numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID)
   then
	
      UPDATE SiteBreadCrumb
      SET numPageID = v_numPageID,numSiteID = v_numSiteID,numDomainID = v_numDomainID,
      tintLevel = v_tintLevel,vcDisplayName = v_vcDisplayName
      WHERE numBreadCrumbID = v_numBreadCrumbID;

   end if;
   RETURN;
END; $$;



