-- Stored procedure definition script USP_DeleteCustomerCreditCardInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCustomerCreditCardInfo(v_numCCInfoID INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CustomerCreditCardInfo WHERE numCCInfoID = v_numCCInfoID;
   RETURN;
END; $$;


