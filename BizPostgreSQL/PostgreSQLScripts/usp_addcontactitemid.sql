-- Stored procedure definition script USP_AddContactItemId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddContactItemId(v_ItemId VARCHAR(250),
v_changeKey VARCHAR(250),
v_ContactId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update AdditionalContactsInformation set vcitemid = v_ItemId,vcChangeKey = v_changeKey where numContactId = v_ContactId;
   RETURN;
END; $$;


