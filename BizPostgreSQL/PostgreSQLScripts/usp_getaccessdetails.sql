-- Stored procedure definition script USP_GetAccessDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAccessDetails(v_numSubscriberID NUMERIC(9,0) DEFAULT 0,    
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcFirstName || ' ' || vcLastname as Name,
cast(dtLoggedInTime as VARCHAR(255)),
cast(dtLoggedOutTime as VARCHAR(255)),
(EXTRACT(DAY FROM dtLoggedOutTime -dtLoggedInTime)*60*24+EXTRACT(HOUR FROM dtLoggedOutTime -dtLoggedInTime)*60+EXTRACT(MINUTE FROM dtLoggedOutTime -dtLoggedInTime)) as Total,cast(bitPortal as VARCHAR(255)) from UserAccessedDTL U
   join AdditionalContactsInformation A
   on A.numContactId = U.numContactID
   where numSubscriberID = v_numSubscriberID  order by dtLoggedInTime Desc;
END; $$;












