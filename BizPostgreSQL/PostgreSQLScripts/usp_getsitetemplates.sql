-- Stored procedure definition script USP_GetSiteTemplates for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSiteTemplates(v_numTemplateID NUMERIC(9,0)  DEFAULT 0,
          v_numSiteID     NUMERIC(9,0) DEFAULT NULL,
          v_numDomainID   NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numTemplateID,
         vcTemplateName,
         '{template:' || REPLACE(LOWER(vcTemplateName),' ','_') || '}' AS vcTemplateCode,
         coalesce(txtTemplateHTML,'') AS txtTemplateHTML,
         numSiteID
   FROM   SiteTemplates
   WHERE  (numTemplateID = v_numTemplateID
   OR v_numTemplateID = 0)
   AND numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID;
END; $$;














