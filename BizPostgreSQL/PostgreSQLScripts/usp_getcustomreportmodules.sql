-- Stored procedure definition script usp_GetCustomReportModules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCustomReportModules(v_numcustModuleId NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select * from CustReptModuleMaster where coalesce(bitCustModuleDisable,false) <> true order by numCustModuleId;  
  
   open SWV_RefCur2 for
   select * from CustRptAndFieldGroups where coalesce(bitGroupDisable,false) <> true and numcustModuleId = v_numcustModuleId order by numReportOptionGroupId;
   RETURN;
END; $$;


