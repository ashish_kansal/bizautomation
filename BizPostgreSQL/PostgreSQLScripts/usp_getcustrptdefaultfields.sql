-- Stored procedure definition script usp_GetCustRptDefaultFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCustRptDefaultFields(v_numReportOptionsTypeId NUMERIC ,                           
v_numDomainId NUMERIC DEFAULT 1  ,                      
v_numContactTypeId NUMERIC DEFAULT 0,                      
v_numRelationTypeId NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   v_strSql := ' ';                   
                  
   v_strSql := coalesce(v_strSql,'') || 'SELECT rel.numFieldGroupID, fld.vcDbFieldName, fld.vcScrFieldName,                                
      fld.numOrderAppearance, fld.boolSummationPossible, fld.boolDefaultColumnSelection,vcFieldType  ,                           
     ListId                       
     ,0 as Ctype ,fld.tintMyself ,fld.tintTeam ,fld.tintTerr                     
     FROM CustRptAndFieldGroupRelation rel, CustRptDefaultFields fld                                
     WHERE rel.numFieldGroupID = fld.numFieldGroupID                                
     AND rel.numReportOptionTypesId = ' || SUBSTR(CAST(v_numReportOptionsTypeId AS VARCHAR(15)),1,15);                   
                  
   if v_numReportOptionsTypeId = 5 or v_numReportOptionsTypeId = 6 or v_numReportOptionsTypeId = 7 or v_numReportOptionsTypeId = 61 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
 SELECT CAST(7 AS NUMERIC(18,0)) AS numFieldGroupID, CAST(''CFld'' || m.fld_id::VARCHAR AS vcDbFieldName, CAST(m.Fld_label AS VARCHAR(50)) AS vcScrFieldName, CAST(0 AS NUMERIC(18,0))                          
  AS  numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, coalesce(numListId,0) as ListId                      
  ,1 as Ctype  ,CAST(0 AS SMALLINT) as tintMyself,CAST(0 AS SMALLINT) as  tintTeam,CAST(0 AS SMALLINT) as  tintTerr                       
  FROM CFW_Fld_Master m, CFW_Fld_Dtl d                          
    WHERE                       
     m.Fld_id = d.numFieldId                             
     and m.Grp_id = 1 --Prospect/ Accounts                          
     AND m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                   
   AND fld_type <> ''Link''';
   end if;                  
   if v_numReportOptionsTypeId = 4 or v_numReportOptionsTypeId = 5 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
    SELECT 6 AS numFieldGroupID, ''CFld'' || m.fld_id::VARCHAR AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection   ,                      
  Fld_type as vcFieldType, COALESCE(numListId,0) as ListId                         
  ,1 as Ctype   ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                           
  FROM CFW_Fld_Master m,CFW_Fld_Dtl d                          
    WHERE                      
    m.Fld_id = d.numFieldId      and 
	m.Grp_id = 4 and                   
    m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                     
                              
   AND fld_type <> ''Link'' ';
   end if;                
              
   if v_numReportOptionsTypeId = 12 or v_numReportOptionsTypeId = 13 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
    SELECT 15 AS numFieldGroupID, ''CFld'' || m.fld_id::VARCHAR AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection   ,                      
  fld_type as vcFieldType, COALESCE(numListId,0) as ListId                 
  ,1 as Ctype    ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                          
  FROM CFW_Fld_Master m              
--,CFW_Fld_Dtl d                          
    WHERE                      
  --  m.Fld_id = d.numFieldId                      
-- and               
m.Grp_id = 3 --Cases                   
    and m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                     
                              
   AND fld_type <> ''Link'' ';
   end if;              
      
   if v_numReportOptionsTypeId = 14 or v_numReportOptionsTypeId = 15 or v_numReportOptionsTypeId = 16 or v_numReportOptionsTypeId = 17 or v_numReportOptionsTypeId = 18 or v_numReportOptionsTypeId = 19 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
    SELECT 18 AS numFieldGroupID, ''CFld'' ||  m.fld_id::VARCHAR AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection   ,                      
  fld_type as vcFieldType, COALESCE(numListId,0) as ListId                         
  ,1 as Ctype    ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                          
  FROM CFW_Fld_Master m              
--,CFW_Fld_Dtl d                          
    WHERE                      
  --  m.Fld_id = d.numFieldId                      
-- and               
(m.Grp_id = 2 or m.Grp_id = 6)      
    and m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                     
                              
   AND fld_type <> ''Link'' ';
   end if;     

   if v_numReportOptionsTypeId = 8 or v_numReportOptionsTypeId = 10 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
 SELECT 9 AS numFieldGroupID, ''CFld'' || m.fld_id::VARCHAR AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, COALESCE(numListId,0) as ListId                      
  ,1 as Ctype  ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                       
  FROM CFW_Fld_Master m, CFW_Fld_Dtl d                          
    WHERE                       
     m.Fld_id = d.numFieldId                             
     and m.Grp_id = 11 --Projects                         
     AND m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                   
   AND fld_type <> ''Link''';
   end if;      
            
   if v_numReportOptionsTypeId = 20 or v_numReportOptionsTypeId = 21 or v_numReportOptionsTypeId = 22 or v_numReportOptionsTypeId = 23 then

      v_strSql := coalesce(v_strSql,'') || '                  
 union                  
 SELECT 27 AS numFieldGroupID, ''CFld'' || m.fld_id::VARCHAR AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, false AS boolSummationPossible, false AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, COALESCE(numListId,0) as ListId                      
  ,1 as Ctype  ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                       
  FROM CFW_Fld_Master m
--, CFW_Fld_Dtl d                          
    WHERE                       
--     m.Fld_id = d.numFieldId   and                          
 m.Grp_id = 5 --Items                         
     AND m.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) ||  '                   
   AND fld_type <> ''Link''';
   end if;      
                
   v_strSql := coalesce(v_strSql,'') || 'order by numFieldGroupID,Ctype, numOrderAppearance';                
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                  
   RAISE NOTICE '%',v_strSql;
   RETURN;
END; $$;


