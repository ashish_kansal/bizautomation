Create or replace FUNCTION USP_getNextPrevMailID(
v_numDomainId NUMERIC(9,0),  
v_numUserCntId NUMERIC(9,0),    
v_numEmailHstrID NUMERIC(9,0), 
v_numNodeId NUMERIC(9,0),
v_EmailStatus NUMERIC(9,0) DEFAULT 0,
INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql                                                      
                                                      
/*Note: Bug fix 2027: selective Searching into subject and Bodytext*/      
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
   v_numPrevEmailHstrID NUMERIC(18,0);
   v_numNextEmailHstrID NUMERIC(18,0);
   v_query  VARCHAR(8000);
BEGIN

	SELECT 
		numEmailHstrID INTO v_numPrevEmailHstrID 
	FROM 
		EmailHistory
	WHERE
		numDomainID = v_numDomainId
		AND numUserCntId = v_numUserCntId
		AND numNodeID = v_numNodeId
		and COALESCE(numemailid,0) > 0
		AND chrSource IN('B','I') 
		AND (COALESCE(v_EmailStatus,0) = 0 OR numListItemId = COALESCE(v_EmailStatus,0))
		and numEmailHstrID < v_numEmailHstrID
	ORDER BY 
		bintCreatedOn DESC
	LIMIT 1;

	SELECT 
		numEmailHstrID INTO v_numPrevEmailHstrID 
	FROM 
		EmailHistory
	WHERE
		numDomainID = v_numDomainId
		AND numUserCntId = v_numUserCntId
		AND numNodeID = v_numNodeId
		and COALESCE(numemailid,0) > 0
		AND chrSource IN ('B','I') 
		AND (COALESCE(v_EmailStatus,0) = 0 OR numListItemId = COALESCE(v_EmailStatus,0))
		and numEmailHstrID > v_numEmailHstrID
	ORDER BY 
		bintCreatedOn ASC
	LIMIT 1;

   OPEN SWV_RefCur FOR select COALESCE(v_numPrevEmailHstrID,0) as numPrevEmailHstrID,COALESCE(v_numNextEmailHstrID,0) as numNextEmailHstrID;

   RETURN;
END; $$;


