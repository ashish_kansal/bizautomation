CREATE OR REPLACE FUNCTION usp_DeleteOpportunityStageComments(v_numOppId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	Delete from OpptStageComments where numoppid = v_numOppId;
	RETURN;
END; $$;


