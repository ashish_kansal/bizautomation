-- Stored procedure definition script USP_GetMutltiIncomeExpenses for PostgreSQL
Create or replace FUNCTION USP_GetMutltiIncomeExpenses(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPINCOMEEXPENSE CASCADE;
   create TEMPORARY TABLE tt_TEMPINCOMEEXPENSE
   (
      numDomainID INTEGER,
      vcDomainName VARCHAR(200),
      vcDomainCode VARCHAR(50),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      Expense NUMERIC(19,4),
      Income NUMERIC(19,4)
   );

   INSERT INTO tt_TEMPINCOMEEXPENSE
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,CAST(0 AS NUMERIC(19,4))
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,CAST(0 AS NUMERIC(19,4))
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,CAST(0 AS NUMERIC(19,4))
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,CAST(0 AS NUMERIC(19,4))
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode;

------------------------
--getting income
-------------------------

   INSERT INTO tt_TEMPINCOMEEXPENSE
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, CAST(0 AS NUMERIC(19,4)),Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,CAST(0 AS NUMERIC(19,4)), Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,CAST(0 AS NUMERIC(19,4)), Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,CAST(0 AS NUMERIC(19,4)), Sum(Total) as Total
   from VIEW_INCOMEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode;


open SWV_RefCur for select
   vcDomainName,
cast(coalesce(Sum(coalesce(Expense,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as NUMERIC(19,4)) as Expense ,
coalesce(sum(coalesce(Income,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4)))*(-1)  as Income
   from tt_TEMPINCOMEEXPENSE
   group by
   vcDomainName;

   RETURN;
END; $$;












