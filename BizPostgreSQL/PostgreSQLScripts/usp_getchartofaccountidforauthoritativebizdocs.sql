-- Stored procedure definition script USP_GetChartOfAccountIdForAuthoritativeBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartOfAccountIdForAuthoritativeBizDocs(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_chChargeCode CHAR(10) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  AC.numAccountID 
   FROM    AccountingChargeTypes ACT
   INNER JOIN AccountingCharges AC ON ACT.numChargeTypeId = AC.numChargeTypeId
   WHERE   ACT.chChargeCode = v_chChargeCode
   AND AC.numDomainID = v_numDomainId;
--	Select min(numAccountId) From Chart_of_Accounts Where numAcntType=@numAcntType And numDomainId=@numDomainId
END; $$;












