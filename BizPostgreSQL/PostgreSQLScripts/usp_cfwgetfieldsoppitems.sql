-- Stored procedure definition script usp_cfwgetfieldsoppitems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_cfwgetfieldsoppitems(v_numDomainID NUMERIC(18,0),                       
	v_numOppItemId NUMERIC(18,0),                  
    v_numItemCode NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM CFW_Fld_Values_OppItems WHERE RecId = v_numOppItemId) then
	
      open SWV_RefCur for
      SELECT
      CFW_Fld_Master.Fld_id
			,fld_type,fld_label
			,numlistid
			,coalesce(CFW_Fld_Values_OppItems.FldDTLID,0) AS FldDTLID
			,(CASE WHEN (coalesce(CFW_Fld_Values_OppItems.Fld_Value,'') = '' OR UPPER(coalesce(CFW_Fld_Values_OppItems.Fld_Value,'')) = 'NO' OR UPPER(coalesce(CFW_Fld_Values_OppItems.Fld_Value,'')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE coalesce(CFW_Fld_Values_OppItems.Fld_Value,'0') END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip
      FROM
      CFW_Fld_Master
      LEFT JOIN
      CFw_Grp_Master
      ON
      subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      LEFT JOIN
      CFW_Fld_Values_OppItems
      ON
      CFW_Fld_Master.Fld_id = CFW_Fld_Values_OppItems.fld_id
      AND CFW_Fld_Values_OppItems.RecId = v_numOppItemId
      WHERE
      CFW_Fld_Master.Grp_id = 5
      and CFW_Fld_Master.numDomainID = v_numDomainID;
   ELSE
      open SWV_RefCur for
      SELECT
      CFW_Fld_Master.Fld_id
			,fld_type,fld_label
			,numlistid
			,coalesce(CFW_FLD_Values_Item.FldDTLID,0) AS FldDTLID
			,(CASE WHEN (coalesce(CFW_FLD_Values_Item.Fld_Value,'') = '' OR UPPER(coalesce(CFW_FLD_Values_Item.Fld_Value,'')) = 'NO' OR UPPER(coalesce(CFW_FLD_Values_Item.Fld_Value,'')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE coalesce(CFW_FLD_Values_Item.Fld_Value,'') END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip
      FROM
      CFW_Fld_Master
      LEFT JOIN
      CFw_Grp_Master
      ON
      subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      LEFT JOIN
      CFW_FLD_Values_Item
      ON
      CFW_Fld_Master.Fld_id = CFW_FLD_Values_Item.Fld_ID
      AND CFW_FLD_Values_Item.RecId = v_numItemCode
      WHERE
      CFW_Fld_Master.Grp_id = 5
      and CFW_Fld_Master.numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;



