CREATE OR REPLACE FUNCTION EmailHistory_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_action  CHAR(1) DEFAULT 'I';
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Email', OLD.numEmailHstrID, 'Delete', OLD.numDomainID);
		RETURN OLD;
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Email', NEW.numEmailHstrID, 'Insert', NEW.numDomainID);
		RETURN NEW;
	END IF;

	RETURN NULL;
END; $$;
CREATE TRIGGER EmailHistory_IUD AFTER INSERT OR UPDATE OR DELETE ON EmailHistory FOR EACH ROW EXECUTE PROCEDURE EmailHistory_IUD_TrFunc();


