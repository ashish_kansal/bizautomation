CREATE OR REPLACE FUNCTION USP_WorkOrder_GetCapacityLoad(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   GetCapacityLoad(v_numDomainID,0,0,WO.numWOId,0,4::SMALLINT,coalesce(WO.dtmStartDate,WO.bintCreatedDate),v_ClientTimeZoneOffset)
   FROM
   WorkOrder WO
   WHERE
   WO.numDomainId = v_numDomainID
   AND WO.numWOId = v_numWOID;
END; $$;

