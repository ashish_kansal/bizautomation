-- Stored procedure definition script USP_DepositMaster_UpdateTransactionHistoryID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DepositMaster_UpdateTransactionHistoryID(v_numDepositId NUMERIC(18,0),
    v_numTransHistoryID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE DepositMaster SET numTransHistoryID = v_numTransHistoryID WHERE numDepositId = v_numDepositId;
   RETURN;
END; $$;


