-- Stored procedure definition script USP_DisableSMTP for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DisableSMTP(v_numUserId NUMERIC(9,0),
v_numDomainId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE UserMaster SET bitSMTPServer = false WHERE numUserId = v_numUserId AND numDomainID = v_numDomainId;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_DocumentWorkFlow]    Script Date: 07/26/2008 16:15:46 ******/



