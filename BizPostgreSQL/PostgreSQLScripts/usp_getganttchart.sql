-- Stored procedure definition script USP_GetGanttChart for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetGanttChart(v_numOppID NUMERIC,
v_numProjectID NUMERIC,
v_numDomainID NUMERIC,
v_numContactId NUMERIC,
v_dtStartDate TIMESTAMP,
v_dtEndDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql

--SELECT  vcMileStoneName AS GanttSeries,
--        vcStageName AS GanttTask,
--        dtStartDate AS GanttStart,
--        dtEndDate AS GanttEnd,
--        numStageDetailsId AS GanttID,
--        numParentStageID AS GanttLinkTo,
--        tinProgressPercentage AS GanttPercentComplete,
--        dbo.fn_GetContactName(numAssignTo) +' (' +CONVERT(VARCHAR(4),tinProgressPercentage) + ' --)' AS GanttOwner
--FROM    dbo.StagePercentageDetails
--WHERE   ISNULL(numProjectID,0) = @numProjectID 
--AND ISNULL(numOppID,0) = @numOppID
--AND numDomainId = @numDomainID


   AS $$
   DECLARE
   v_numRights  INTEGER;
BEGIN
   v_numRights := 0;

   select   numRights INTO v_numRights from ProjectTeamRights where numProId = v_numProjectID and numContactId = v_numContactId;

--SET @numrights=(case when @tintMode=0 then 1 else @numrights end)

   open SWV_RefCur for
   with recursive MileStone(numParentStageID,numStageDetailsId,numStage,bitClose,dtEndDate,dtStartDate,
   vcStageName,tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,
   slp_id)
   AS(
    SELECT numParentStageID AS numParentStageID, numStageDetailsId AS numStageDetailsId ,
        0 AS numStage,bitClose AS bitClose,dtEndDate AS dtEndDate,dtStartDate AS dtStartDate
,vcStageName AS vcStageName,tinProgressPercentage AS tinProgressPercentage,coalesce(numAssignTo,0) AS numAssignTo,
  coalesce(fn_GetContactName(numAssignTo),'-') AS vcAssignTo,vcMileStoneName AS vcMileStoneName,slp_id AS slp_id
   FROM StagePercentageDetails
   WHERE numParentStageID = 0 and coalesce(numProjectid,0) = v_numProjectID
   AND coalesce(numOppid,0) = v_numOppID  AND numdomainid = v_numDomainID
   UNION ALL
-- Recursive member definition
   SELECT p.numParentStageID AS numParentStageID, p.numStageDetailsId AS numStageDetailsId ,
        m.numStage+1 AS numStage,p.bitClose AS bitClose,p.dtEndDate AS dtEndDate,p.dtStartDate AS dtStartDate
,p.vcStageName AS vcStageName,p.tinProgressPercentage AS tinProgressPercentage,coalesce(p.numAssignTo,0) AS numAssignTo,
  coalesce(fn_GetContactName(p.numAssignTo),'-') AS vcAssignTo,p.vcMileStoneName AS vcMileStoneName,p.slp_id AS slp_id
   FROM StagePercentageDetails AS p
   INNER JOIN MileStone AS m
   ON p.numParentStageID = m.numStageDetailsId where coalesce(numProjectID,0) = v_numProjectID
   AND coalesce(numOppID,0) = v_numOppID)
   SELECT vcMileStoneName AS GanttSerie,
		vcStageName AS GanttTask, dtStartDate AS GanttStart,
        dtEndDate AS GanttEnd, numStageDetailsId AS GanttID,
        coalesce(s.numStageDetailId,0) AS GanttLinkTo,
        tinProgressPercentage AS GanttPercentComplete,
        fn_GetContactName(numAssignTo) || ' (' || tinProgressPercentage || ' %)' AS GanttOwner
   FROM MileStone m LEFT JOIN StageDependency s ON (m.numStageDetailsId = s.numDependantOnID)   where 

   dtStartDate between v_dtStartDate and v_dtEndDate
   and dtEndDate between v_dtStartDate and v_dtEndDate;
END; $$;

