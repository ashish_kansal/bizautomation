-- Stored procedure definition script USP_ManageSubTabs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageSubTabs(v_numDomainID NUMERIC(9,0)  DEFAULT 0,
               v_numModuleId NUMERIC(9,0)  DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    
   open SWV_RefCur for SELECT numTabId,
           CASE
   WHEN bitFixed = true THEN CASE
      WHEN EXISTS(SELECT numTabName
         FROM   TabDefault
         WHERE  numDomainID = v_numDomainID
         AND numTabId = TM.numTabId
         AND tintTabType = TM.tintTabType) THEN(SELECT  numTabName
            FROM   TabDefault
            WHERE  numDomainID = v_numDomainID
            AND numTabId = TM.numTabId
            AND tintTabType = TM.tintTabType LIMIT 1)
      ELSE TM.numTabName
      END
   ELSE TM.numTabName
   END AS numTabname,
           fn_IsSubTabInGroupTabDtl(TM.numModuleId,TM.numTabId) AS IsVisible,
           numModuleId
   FROM   TabMaster TM
   WHERE  tintTabType IN(SELECT numGroupID
      FROM   AuthenticationGroupMaster
      WHERE  numDomainID = v_numDomainID)
   AND numModuleId = v_numModuleId
   AND (numDomainID = v_numDomainID
   OR bitFixed = true);
  -- Insert statements for procedure here
END; $$;



