-- Function definition script GetCashInflowForMonths for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCashInflowForMonths(v_month INTEGER,v_year INTEGER,v_numDomainId NUMERIC(9,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql    
--Declare @monDebitIncomeAmt as DECIMAL(20,5)    
--Declare @monCreditIncomeAmt as DECIMAL(20,5)    
--Declare @monDebitARAmt as DECIMAL(20,5)    
--Declare @monCreditARAmt as DECIMAL(20,5)    
--Declare @monTotalIncomeAmt as DECIMAL(20,5)    
--Declare @monTotalARAmt as DECIMAL(20,5)    
   AS $$
   DECLARE
   v_monCashInflowAmt  DECIMAL(20,5);    
    
--Select  @monDebitIncomeAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditIncomeAmt= sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH    
--inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId    
--inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId     
--Where GJH.numDomainId=@numDomainId And CA.numAcntType in (822,825) And month(GJH.datEntry_Date)=@month And Year(GJH.datEntry_Date)=@year     
------Print '@numDebitIncomeAmt=='+convert(varchar(10),@monDebitIncomeAmt)    
------Print '@numCreditIncomeAmt=='+convert(varchar(10),@monCreditIncomeAmt)    
--    
--Select  @monDebitARAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditARAmt= sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH    
--inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId    
--inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId     
--Where GJH.numDomainId=@numDomainId And CA.numAcntType =814 And month(GJH.datEntry_Date)=@month And Year(GJH.datEntry_Date)=@year    
------Print '@monDebitARAmt=='+convert(varchar(10),@monDebitARAmt)    
------Print '@monCreditARAmt=='+convert(varchar(10),@monCreditARAmt)    
--    
--Set @monTotalIncomeAmt=isnull(@monCreditIncomeAmt,0)-isnull(@monDebitIncomeAmt,0)    
--Set @monTotalARAmt=isnull(@monDebitARAmt,0)-isnull(@monCreditARAmt,0)    
----Print '@monTotalIncomeAmt=='+convert(varchar(10),@monTotalIncomeAmt)    
----Print '@monTotalARAmt=='+convert(varchar(10),@monTotalARAmt)    
--Set @monCashInflowAmt=isnull(@monTotalIncomeAmt,0)+isnull(@monTotalARAmt,0)  
----Print '@monCashInflowAmt=='+convert(varchar(10),@monCashInflowAmt)    
BEGIN
   Return v_monCashInflowAmt;
END; $$;

