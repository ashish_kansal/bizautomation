-- Stored procedure definition script USP_EmailBroadcastConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_EmailBroadcastConfiguration_Save(v_numDomainID NUMERIC,  
 v_vcAWSAccessKey VARCHAR(100),  
 v_vcAWSSecretKey VARCHAR(100),
 v_vcAWSDomain VARCHAR(200),
 v_vcFrom VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
  

   IF EXISTS(SELECT * FROM EmailBroadcastConfiguration WHERE numDomainID = v_numDomainID) then

      UPDATE
      EmailBroadcastConfiguration
      SET
      vcAWSAccessKey = v_vcAWSAccessKey,vcAWSSecretKey = v_vcAWSSecretKey,vcAWSDomain = v_vcAWSDomain,
      vcFrom = v_vcFrom
      WHERE
      numDomainID = v_numDomainID;
   ELSE
      INSERT INTO EmailBroadcastConfiguration(numDomainID,
		vcFrom,
		vcAWSDomain,
		vcAWSAccessKey,
		vcAWSSecretKey)
	VALUES(v_numDomainID,
		v_vcFrom,
		v_vcAWSDomain,
		v_vcAWSAccessKey,
		v_vcAWSSecretKey);
   end if;
   RETURN;
END; $$;  








