-- Stored procedure definition script USP_DeleteCompetitor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCompetitor(v_numCompetitorID NUMERIC(9,0),  
v_numDomainID NUMERIC(9,0) ,
v_numItemCode NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from Competitor
   where numCompetitorID = v_numCompetitorID
   and numDomainID = v_numDomainID  and numItemCode = v_numItemCode;
   RETURN;
END; $$;


