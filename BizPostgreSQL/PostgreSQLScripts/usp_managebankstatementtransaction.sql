-- Stored procedure definition script usp_ManageBankStatementTransaction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
/****** Added By : Joseph ******/
/****** Saves Statements Transactions to Table BankStatementTransactions******/

CREATE OR REPLACE FUNCTION usp_ManageBankStatementTransaction(v_numTransactionID NUMERIC(18,0),
	v_numStatementID NUMERIC(18,0),
	v_monAmount DECIMAL(20,5),
	v_vcCheckNumber VARCHAR(50),
	v_dtDatePosted TIMESTAMP,
	v_vcFITransactionID VARCHAR(50),
	v_vcPayeeName VARCHAR(50),
	v_vcMemo VARCHAR(100),
	v_vcTxType VARCHAR(50),
	v_numAccountID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN

   IF EXISTS(SELECT numTransactionID FROM BankStatementTransactions WHERE numTransactionID = v_numTransactionID AND numAccountID = v_numAccountID) then

      UPDATE BankStatementTransactions SET
      numStatementId = v_numStatementID,monAmount = v_monAmount,vcCheckNumber = v_vcCheckNumber,
      dtDatePosted = v_dtDatePosted,vcFITransactionID = v_vcFITransactionID,
      vcPayeeName = v_vcPayeeName,vcMemo = v_vcMemo,vcTxType = v_vcTxType,
      numAccountID = v_numAccountID,numDomainID = v_numDomainID,
      dtModifiedDate = LOCALTIMESTAMP
      WHERE
      numTransactionID = v_numTransactionID;
   ELSE
      select   COUNT(*) INTO v_Check FROM   BankStatementTransactions WHERE  vcFITransactionID = v_vcFITransactionID AND numAccountID = v_numAccountID;
      IF v_Check = 0 then

         INSERT INTO BankStatementTransactions(numStatementId,
		monAmount,
		vcCheckNumber,
		dtDatePosted,
		vcFITransactionID,
		vcPayeeName,
		vcMemo,
		vcTxType,
		numAccountID,
		numDomainID,
		dtCreatedDate,
		bitIsActive) VALUES(v_numStatementID,
		v_monAmount,
		v_vcCheckNumber,
		v_dtDatePosted,
		v_vcFITransactionID,
		v_vcPayeeName,
		v_vcMemo,
		v_vcTxType,
		v_numAccountID,
		v_numDomainID,
		LOCALTIMESTAMP ,
		true);
      end if;
   end if;
   RETURN;
END; $$;




