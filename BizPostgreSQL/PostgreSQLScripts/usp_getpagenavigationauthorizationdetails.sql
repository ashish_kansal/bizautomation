-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE OR REPLACE FUNCTION USP_GetPageNavigationAuthorizationDetails(v_numGroupID NUMERIC(18,0),
      v_numTabID NUMERIC(18,0),
      v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor, INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numModuleID  NUMERIC(18,0);
BEGIN
RAISE NOTICE '%',v_numTabID; 
   select   numModuleID INTO v_numModuleID FROM    PageNavigationDTL WHERE   numTabID = v_numTabID    LIMIT 1; 
   RAISE NOTICE '%',v_numModuleID;
        
   IF v_numModuleID = 10 then
            
      RAISE NOTICE '%',10;
      open SWV_RefCur for
      SELECT * FROM(SELECT    coalesce((SELECT 
            TNA.numPageAuthID
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = PND.numPageNavID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),0) AS numPageAuthID,
                                    coalesce(v_numGroupID,0) AS numGroupID,
                                    coalesce(PND.numTabID,0) AS numTabID,
                                    coalesce(PND.numPageNavID,0) AS numPageNavID,
                                    coalesce((SELECT 
            TNA.bitVisible
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = PND.numPageNavID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),
         false) AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    (SELECT 
            vcPageNavName
            FROM      PageNavigationDTL
            WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID LIMIT 1) AS vcNodeName,
                                    coalesce((SELECT 
            TNA.tintType
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = PND.numPageNavID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),
         1) AS tintType
         FROM      PageNavigationDTL PND
         WHERE     coalesce(PND.bitVisible,false) = true
         AND PND.numTabID = v_numTabID
         AND numModuleID = v_numModuleID
         UNION ALL
         SELECT    coalesce((SELECT 
            TNA.numPageAuthID
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),0) AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    1 AS numTabID,
                                    coalesce(LD.numListItemID,0) AS numPageNavID,
                                    coalesce((SELECT 
            TNA.bitVisible
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),
         false) AS bitVisible,
                                    coalesce(LD.numDomainid,0) AS numDomainID,
                                    coalesce(vcData,'') || 's' AS vcNodeName,
                                    coalesce((SELECT 
            TNA.tintType
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID LIMIT 1),
         1) AS tintType
         FROM      Listdetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
         WHERE     numListID = 27
         AND LD.numDomainid = v_numDomainID
         AND coalesce(constFlag,false) = false
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
         UNION ALL
         SELECT    coalesce((SELECT TNA.numPageAuthID
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID),0) AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    1 AS numTabID,
                                    coalesce(LD.numListItemID,0) AS numPageNavID,
                                    coalesce((SELECT TNA.bitVisible
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID),false) AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(vcData,'') || 's' AS vcNodeName,
                                    coalesce((SELECT TNA.tintType
            FROM   TreeNavigationAuthorization TNA
            WHERE  TNA.numPageNavID = LD.numListItemID
            AND TNA.numDomainID = v_numDomainID
            AND TNA.numGroupID = v_numGroupID
            AND TNA.numTabID = v_numTabID),1) AS tintType
         FROM      Listdetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
         WHERE     numListID = 27
         AND coalesce(constFlag,false) = true) TABLE1
      ORDER BY numPageNavID;
   end if;
		
   IF v_numModuleID = 14 then
            
      IF NOT EXISTS(SELECT * FROM    TreeNavigationAuthorization
      WHERE   numTabID = 8
      AND numDomainID = 1
      AND numGroupID = 1) then
                    
         open SWV_RefCur for
         SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                coalesce(PND.vcPageNavName,'') AS vcNodeName,
                                tintType
         FROM    PageNavigationDTL PND
         JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
         WHERE   1 = 1
                                --AND ISNULL(TNA.bitVisible, 0) = 1
         AND coalesce(PND.bitVisible,false) = true
         AND numModuleID = v_numModuleID
         AND TNA.numDomainID = v_numDomainID
         AND numGroupID = v_numGroupID
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                1111 AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                'Regular Documents' AS vcNodeName,
                                1 AS tintType
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                Ld.numListItemID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         WHERE   Ld.numListID = 29
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                Ld.numListItemID AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         INNER JOIN AuthoritativeBizDocs AB ON (Ld.numListItemID = AB.numAuthoritativeSales)
         WHERE   Ld.numListID = 27
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND AB.numDomainId = v_numDomainID
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                Ld.numListItemID AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         INNER JOIN AuthoritativeBizDocs AB ON (Ld.numListItemID = AB.numAuthoritativePurchase)
         WHERE   Ld.numListID = 27
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND AB.numDomainId = v_numDomainID
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                0 AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(LT.vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails LT
         LEFT JOIN LATERAL(SELECT * FROM Listdetails WHERE numListID = 27
            AND (constFlag = true OR numDomainid = v_numDomainID)
            AND (numListItemID IN(SELECT    AB.numAuthoritativeSales
               FROM      AuthoritativeBizDocs AB
               WHERE     AB.numDomainId = v_numDomainID)
            OR numListItemID IN(SELECT    AB.numAuthoritativePurchase
               FROM      AuthoritativeBizDocs AB
               WHERE     AB.numDomainId = v_numDomainID))) Ld on TRUE
         WHERE   LT.numListID = 11
         AND LT.numDomainid = v_numDomainID
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                2222 AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                'Other BizDocs' AS vcNodeName,
                                1 AS tintType
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                Ld.numListItemID AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails Ld
         WHERE   Ld.numListID = 27
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativeSales
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativePurchase
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)
         UNION
         SELECT  0 AS numPageAuthID,
                                v_numGroupID AS numGroupID,
                                v_numTabID AS numTabID,
                                ls.numListItemID AS numPageNavID,
                                true AS bitVisible,
                                v_numDomainID AS numDomainID,
                                coalesce(ls.vcData,'') AS vcNodeName,
                                1 AS tintType
         FROM    Listdetails Ld,
                                (SELECT * FROM      Listdetails ls
            WHERE     ls.numDomainid = v_numDomainID
            AND ls.numListID = 11) ls
         WHERE   Ld.numListID = 27
         AND (Ld.constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativeSales
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID
            UNION
            SELECT  AB.numAuthoritativePurchase
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)
         ORDER BY 8,4;
      end if;
   ELSE
      open SWV_RefCur2 for
      SELECT * FROM(SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    coalesce(PND.vcPageNavName,'') AS vcNodeName,
                                    tintType
         FROM      PageNavigationDTL PND
         JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
         WHERE     1 = 1
         AND coalesce(PND.bitVisible,false) = true
         AND numModuleID = v_numModuleID
         AND TNA.numTabID = v_numTabID
         AND TNA.numDomainID = v_numDomainID
         AND numGroupID = v_numGroupID
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    1111 AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    'Regular Documents' AS vcNodeName,
                                    1 AS tintType
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    Ld.numListItemID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         WHERE     Ld.numListID = 29
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    Ld.numListItemID AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         INNER JOIN AuthoritativeBizDocs AB ON (Ld.numListItemID = AB.numAuthoritativeSales)
         WHERE     Ld.numListID = 27
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND AB.numDomainId = v_numDomainID
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    Ld.numListItemID AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails Ld
         LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
         AND LO.numDomainId = v_numDomainID
         INNER JOIN AuthoritativeBizDocs AB ON (Ld.numListItemID = AB.numAuthoritativePurchase)
         WHERE     Ld.numListID = 27
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND AB.numDomainId = v_numDomainID
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    0 AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(LT.vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails LT,
                                    Listdetails Ld
         WHERE     LT.numListID = 11
         AND LT.numDomainid = v_numDomainID
         AND Ld.numListID = 27
         AND (Ld.constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND (Ld.numListItemID IN(SELECT    AB.numAuthoritativeSales
            FROM      AuthoritativeBizDocs AB
            WHERE     AB.numDomainId = v_numDomainID)
         OR Ld.numListItemID IN(SELECT    AB.numAuthoritativePurchase
            FROM      AuthoritativeBizDocs AB
            WHERE     AB.numDomainId = v_numDomainID))
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    2222 AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    'Other BizDocs' AS vcNodeName,
                                    1 AS tintType
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    Ld.numListItemID AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails Ld
         WHERE     Ld.numListID = 27
         AND v_numTabID <> 133
         AND (constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativeSales
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativePurchase
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)
         UNION
         SELECT    0 AS numPageAuthID,
                                    v_numGroupID AS numGroupID,
                                    v_numTabID AS numTabID,
                                    ls.numListItemID AS numPageNavID,
                                    true AS bitVisible,
                                    v_numDomainID AS numDomainID,
                                    coalesce(ls.vcData,'') AS vcNodeName,
                                    1 AS tintType
         FROM      Listdetails Ld,
                                    (SELECT * FROM      Listdetails ls
            WHERE     ls.numDomainid = v_numDomainID
            AND ls.numListID = 11) ls
         WHERE     Ld.numListID = 27
         AND v_numTabID <> 133
         AND (Ld.constFlag = true
         OR Ld.numDomainid = v_numDomainID)
         AND Ld.numListItemID NOT IN(SELECT  AB.numAuthoritativeSales
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID
            UNION
            SELECT  AB.numAuthoritativePurchase
            FROM    AuthoritativeBizDocs AB
            WHERE   AB.numDomainId = v_numDomainID)) PageNavTable
      JOIN TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
      AND PageNavTable.numGroupID = TreeNav.numGroupID
      AND PageNavTable.numTabID = TreeNav.numTabID
      ORDER BY TreeNav.tintType,TreeNav.numPageNavID;
   end if;

   open SWV_RefCur3 for
   SELECT * FROM(SELECT    coalesce((SELECT 
         TNA.numPageAuthID
         FROM   TreeNavigationAuthorization TNA
         WHERE  TNA.numPageNavID = PND.numPageNavID
         AND TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numTabID = v_numTabID LIMIT 1),0) AS numPageAuthID,
                            coalesce(v_numGroupID,0) AS numGroupID,
                            coalesce(PND.numTabID,0) AS numTabID,
                            coalesce(PND.numPageNavID,0) AS numPageNavID,
                            coalesce((SELECT 
         TNA.bitVisible
         FROM   TreeNavigationAuthorization TNA
         WHERE  TNA.numPageNavID = PND.numPageNavID
         AND TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numTabID = v_numTabID LIMIT 1),false) AS bitVisible,
                            v_numDomainID AS numDomainID,
                            (SELECT 
         vcPageNavName
         FROM      PageNavigationDTL
         WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID LIMIT 1) AS vcNodeName,
                            coalesce((SELECT 
         TNA.tintType
         FROM   TreeNavigationAuthorization TNA
         WHERE  TNA.numPageNavID = PND.numPageNavID
         AND TNA.numDomainID = v_numDomainID
         AND TNA.numGroupID = v_numGroupID
         AND TNA.numTabID = v_numTabID LIMIT 1),1) AS tintType
      FROM      PageNavigationDTL PND
      WHERE     coalesce(PND.bitVisible,false) = true
      AND PND.numTabID = v_numTabID
      AND numModuleID = v_numModuleID
      UNION ALL
      SELECT    coalesce(numPageAuthID,0) AS numPageAuthID,
                            v_numGroupID AS numGroupID,
                            v_numTabID AS numTabID,
                            coalesce(LD.numListItemID,0) AS numPageNavID,
                            coalesce(TNA.bitVisible,false) AS bitVisible,
                            coalesce(LD.numDomainid,0) AS numDomainID,
                            vcData AS vcNodeName,
                            1 AS tintType
      FROM      Listdetails LD
      LEFT JOIN TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
      WHERE     numListID = 27
      AND LD.numDomainid = v_numDomainID
      AND coalesce(constFlag,false) = false
      AND numTabID =(CASE WHEN v_numModuleID = 10
      THEN 1
      ELSE 0
      END)
      UNION ALL
      SELECT    coalesce(numPageAuthID,0) AS numPageAuthID,
                            v_numGroupID AS numGroupID,
                            v_numTabID AS numTabID,
                            coalesce(LD.numListItemID,0) AS numPageNavID,
                            coalesce(TNA.bitVisible,false) AS bitVisible,
                            coalesce(TNA.numDomainID,0) AS numDomainID,
                            vcData AS vcNodeName,
                            1 AS tintType
      FROM      Listdetails LD
      LEFT JOIN TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
      AND coalesce(TNA.numDomainID,0) = v_numDomainID
      WHERE     numListID = 27
      AND coalesce(constFlag,false) = true
      AND numTabID = v_numTabID) TABLE2
   ORDER BY numPageNavID;
   RETURN;
END; $$;


