-- Stored procedure definition script USP_GetPayPeriod for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPayPeriod(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select coalesce(tintPayPeriod,0) AS "tintPayPeriod" From Domain Where numDomainId = v_numDomainId;
END; $$;












