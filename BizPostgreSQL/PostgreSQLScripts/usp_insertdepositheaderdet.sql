-- Stored procedure definition script USP_InsertDepositHeaderDet for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertDepositHeaderDet(v_numDepositId NUMERIC(9,0) DEFAULT 0,        
v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                             
v_datEntry_Date TIMESTAMP DEFAULT NULL,                                          
v_numAmount DECIMAL(20,5) DEFAULT NULL,        
v_numRecurringId NUMERIC(9,0) DEFAULT 0,                               
v_numDomainId NUMERIC(9,0) DEFAULT 0,              
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_strItems TEXT DEFAULT ''
,v_numPaymentMethod NUMERIC DEFAULT NULL
,v_vcReference VARCHAR(500) DEFAULT NULL
,v_vcMemo VARCHAR(1000) DEFAULT NULL
,v_tintDepositeToType SMALLINT DEFAULT 1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,v_numDivisionID NUMERIC DEFAULT NULL
,v_tintMode SMALLINT DEFAULT 0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,v_tintDepositePage SMALLINT DEFAULT NULL --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,v_numTransHistoryID NUMERIC(9,0) DEFAULT 0,
v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,
v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
v_fltExchangeRate DOUBLE PRECISION DEFAULT 1,
v_numAccountClass NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
 
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_numOppBizDocsId  NUMERIC(18,0);
   SWV_RCur3 REFCURSOR;
   SWV_RCur4 REFCURSOR;
BEGIN
   IF coalesce(v_numCurrencyID,0) = 0 then

      v_fltExchangeRate := 1;
      select   coalesce(numCurrencyID,0) INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
   end if;
 

--Validation of closed financial year
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainId,v_datEntry_Date);

   IF v_tintMode != 2 then
 
      IF v_numRecurringId = 0 then 
         v_numRecurringId := NULL;
      end if;
      IF v_numDivisionID = 0 then 
         v_numDivisionID := NULL;
      end if;
      IF v_numReturnHeaderID = 0 then 
         v_numReturnHeaderID := NULL;
      end if;
      IF v_numDepositId = 0 then
	  
         INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES(v_numChartAcntId,v_datEntry_Date,v_numAmount,v_numRecurringId,v_numDomainId,v_numUserCntID,TIMEZONE('UTC',now()),v_numPaymentMethod,v_vcReference,v_vcMemo,v_tintDepositeToType,v_numDivisionID,v_tintDepositePage,v_numTransHistoryID,v_numReturnHeaderID,v_numCurrencyID,coalesce(v_fltExchangeRate, 1),v_numAccountClass);
	   
         v_numDepositId := CURRVAL('DepositMaster_seq');
      end if;
      IF v_numDepositId <> 0 then
	  
         UPDATE DepositMaster SET numChartAcntId = v_numChartAcntId,dtDepositDate = v_datEntry_Date,monDepositAmount = v_numAmount,
         numRecurringId = v_numRecurringId,numModifiedBy = v_numUserCntID,
         dtModifiedDate = TIMEZONE('UTC',now()),numPaymentMethod = v_numPaymentMethod,
         vcReference = v_vcReference,vcMemo = v_vcMemo,tintDepositeToType = v_tintDepositeToType,
         numDivisionID = v_numDivisionID,tintDepositePage = v_tintDepositePage,
         numTransHistoryID = v_numTransHistoryID,numCurrencyID = v_numCurrencyID,
         fltExchangeRate = v_fltExchangeRate
         WHERE numDepositId = v_numDepositId AND numDomainId = v_numDomainId;
      end if;
   end if; 
  
   IF v_tintMode = 1 then
  
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
				-- When user unchecks deposit entry of undeposited fund, reset original entry
         UPDATE DepositMaster
         SET bitDepositedToAcnt = false WHERE numDepositId IN(SELECT numChildDepositID FROM DepositeDetails WHERE numDepositID = v_numDepositId AND numChildDepositID > 0
         AND numDepositeDetailID NOT IN(SELECT X.numDepositeDetailID
            FROM 
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numDepositeDetailID NUMERIC(18,0) PATH 'numDepositeDetailID'
			) AS X WHERE COALESCE(X.numDepositeDetailID,0) > 0));
						
         DELETE FROM DepositeDetails WHERE numDepositID = v_numDepositId
         AND numDepositeDetailID NOT IN(SELECT X.numDepositeDetailID
            FROM 
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numDepositeDetailID NUMERIC(18,0) PATH 'numDepositeDetailID'
			) AS X WHERE COALESCE(X.numDepositeDetailID,0) > 0);

         INSERT INTO DepositeDetails(numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate)
         SELECT
         v_numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,TIMEZONE('UTC',now()),TIMEZONE('UTC',now())
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numDepositeDetailID NUMERIC(18,0) PATH 'numDepositeDetailID',
					numChildDepositID NUMERIC(18,0) PATH 'numChildDepositID',
					monAmountPaid DECIMAL(20,5) PATH 'monAmountPaid',
					numPaymentMethod NUMERIC(18,0) PATH 'numPaymentMethod',
					vcMemo VARCHAR(1000) PATH 'vcMemo',
					vcReference VARCHAR(500) PATH 'vcReference',
					numClassID NUMERIC(18,0) PATH 'numClassID',
					numProjectID NUMERIC(18,0) PATH 'numProjectID',
					numReceivedFrom NUMERIC(18,0) PATH 'numReceivedFrom',
					numAccountID NUMERIC(18,0) PATH 'numAccountID'
			) AS X 
			WHERE
				COALESCE(X.numDepositeDetailID,0)=0;

         UPDATE DepositeDetails
         SET
         monAmountPaid = X.monAmountPaid,numChildDepositID = X.numChildDepositID,
         numPaymentMethod = X.numPaymentMethod,vcMemo = X.vcMemo,vcReference = X.vcReference,
         numClassID = X.numClassID,numProjectID = X.numProjectID,
         numReceivedFrom = X.numReceivedFrom,numAccountID = X.numAccountID,dtModifiedDate = TIMEZONE('UTC',now())
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numDepositeDetailID NUMERIC(18,0) PATH 'numDepositeDetailID',
					numChildDepositID NUMERIC(18,0) PATH 'numChildDepositID',
					monAmountPaid DECIMAL(20,5) PATH 'monAmountPaid',
					numPaymentMethod NUMERIC(18,0) PATH 'numPaymentMethod',
					vcMemo VARCHAR(1000) PATH 'vcMemo',
					vcReference VARCHAR(500) PATH 'vcReference',
					numClassID NUMERIC(18,0) PATH 'numClassID',
					numProjectID NUMERIC(18,0) PATH 'numProjectID',
					numReceivedFrom NUMERIC(18,0) PATH 'numReceivedFrom',
					numAccountID NUMERIC(18,0) PATH 'numAccountID'
			) AS X 
         WHERE 
			COALESCE(X.numDepositeDetailID,0) > 0 
			AND X.numDepositeDetailID = DepositeDetails.numDepositeDetailID 
			AND DepositeDetails.numDepositID = v_numDepositId;

         UPDATE DepositMaster SET bitDepositedToAcnt = true WHERE numDomainId = v_numDomainId AND  numDepositId IN(SELECT numChildDepositID
         FROM  
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numChildDepositID NUMERIC(9,0) PATH 'numChildDepositID'
			) AS X
		 WHERE X.numChildDepositID > 0);
         
      end if;
   end if; 
  
  
  
   IF v_tintMode = 0 OR v_tintMode = 2 then
  
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP AS
            SELECT
            numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid 
            FROM
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numDepositeDetailID NUMERIC(9,0) PATH 'numDepositeDetailID',
					numOppBizDocsID NUMERIC(9,0) PATH 'numOppBizDocsID',
					numOppID NUMERIC(9,0) PATH 'numOppID',
					monAmountPaid DECIMAL(20,5) PATH 'monAmountPaid'
			) AS X;

         IF v_tintMode = 0 then
					
            UPDATE OpportunityBizDocs OBD
            SET monAmountPaid = coalesce(OBD.monAmountPaid,0) -DD.monAmountPaid,numModifiedBy = v_numUserCntID
            FROM DepositeDetails DD
            WHERE OBD.numOppBizDocsId = DD.numOppBizDocsID AND(DD.numDepositID = v_numDepositId AND DD.numOppBizDocsID > 0
            AND DD.numDepositeDetailID NOT IN(SELECT numDepositeDetailID
            FROM tt_TEMP WHERE numDepositeDetailID > 0));
            DELETE FROM DepositeDetails WHERE numDepositID = v_numDepositId
            AND numDepositeDetailID NOT IN(SELECT X.numDepositeDetailID
            FROM 
			XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numDepositeDetailID NUMERIC(18,0) PATH 'numDepositeDetailID'
			) AS X WHERE COALESCE(X.numDepositeDetailID,0) > 0);
         end if;
         UPDATE  DepositeDetails DD SET monAmountPaid = DD.monAmountPaid+X.monAmountPaid
         FROM tt_TEMP X
         WHERE(DD.numOppBizDocsID = X.numOppBizDocsId AND
         DD.numOppID = X.numOppID) AND(X.numDepositeDetailID = 0 AND DD.numDepositID = v_numDepositId);
         INSERT INTO DepositeDetails(numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate)
         SELECT v_numDepositId, numOppBizDocsId , numOppID , monAmountPaid,TIMEZONE('UTC',now()),TIMEZONE('UTC',now())
         FROM tt_TEMP WHERE numDepositeDetailID = 0 AND numOppBizDocsId NOT IN(SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = v_numDepositId);
         IF v_tintMode = 0 then
						
            UPDATE  DepositeDetails
            SET     monAmountPaid = X.monAmountPaid,numOppBizDocsID = X.numOppBizDocsId,numOppID = X.numOppID,
            dtModifiedDate = TIMEZONE('UTC',now())
            FROM    tt_TEMP X
            WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
            AND DepositeDetails.numDepositID = v_numDepositId;
         end if;
         UPDATE OpportunityBizDocs
         SET monAmountPaid =(SELECT coalesce(SUM(coalesce(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numoppid),numModifiedBy = v_numUserCntID
         WHERE  OpportunityBizDocs.numOppBizDocsId IN(SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = v_numDepositId);

					--Add to OpportunityAutomationQueue if full Amount Paid	
         INSERT INTO OpportunityAutomationQueue(numDomainID, numOppId, numOppBizDocsId, numBizDocStatus, numCreatedBy, dtCreatedDate, tintProcessStatus,numRuleID)
         SELECT OM.numDomainId, OM.numOppId, OBD.numOppBizDocsId, 0, v_numUserCntID, TIMEZONE('UTC',now()), 1,7
         FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
         WHERE OM.numDomainId = v_numDomainId AND OBD.numOppBizDocsId IN(SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = v_numDepositId)
         AND coalesce(OBD.monDealAmount,0) = coalesce(OBD.monAmountPaid,0) AND coalesce(OBD.monAmountPaid,0) > 0;

					--Add to OpportunityAutomationQueue if Balance due
         INSERT INTO OpportunityAutomationQueue(numDomainID, numOppId, numOppBizDocsId, numBizDocStatus, numCreatedBy, dtCreatedDate, tintProcessStatus,numRuleID)
         SELECT OM.numDomainId, OM.numOppId, OBD.numOppBizDocsId, 0, v_numUserCntID, TIMEZONE('UTC',now()), 1,14
         FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numoppid = OM.numOppId
         WHERE OM.numDomainId = v_numDomainId AND OBD.numOppBizDocsId IN(SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = v_numDepositId)
         AND coalesce(OBD.monDealAmount,0) > coalesce(OBD.monAmountPaid,0) AND coalesce(OBD.monAmountPaid,0) > 0;
         UPDATE DepositMaster SET monAppliedAmount =(SELECT coalesce(SUM(coalesce(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositID = v_numDepositId)
         WHERE numDepositId = v_numDepositId;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         
      ELSE
			--IF @tintMode = 0
			--BEGIN
         UPDATE OpportunityBizDocs OBD
         SET monAmountPaid = coalesce(OBD.monAmountPaid,0) -DD.monAmountPaid,numModifiedBy = v_numUserCntID
         FROM DepositeDetails DD
         WHERE OBD.numOppBizDocsId = DD.numOppBizDocsID AND DD.numDepositID = v_numDepositId;
         DELETE FROM DepositeDetails WHERE numDepositID = v_numDepositId;
         UPDATE DepositMaster SET monAppliedAmount =(SELECT coalesce(SUM(coalesce(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositID = v_numDepositId)
         WHERE numDepositId = v_numDepositId;
      end if;
   end if;
  
   open SWV_RefCur for SELECT v_numDepositId;   
  
   BEGIN
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppBizDocsId NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numOppBizDocsId)
      SELECT
      numOppBizDocsId
      FROM
      DepositeDetails
      WHERE
      numDepositID = v_numDepositId
      AND coalesce(numOppBizDocsId,0) > 0;
      select   COUNT(*) INTO v_iCount FROM tt_TEMP;
      WHILE v_i <= v_iCount LOOP
         select   numOppBizDocsId INTO v_numOppBizDocsId FROM tt_TEMP WHERE ID = v_i;
         SELECT * INTO SWV_RCur3,SWV_RCur4 FROM USP_OpportunityBizDocs_CT(v_numDomainId,v_numUserCntID,v_numOppBizDocsId,SWV_RefCur := 'SWV_RCur3',
         SWV_RefCur2 := 'SWV_RCur4');
         v_i := v_i::bigint+1;
      END LOOP;
      EXCEPTION WHEN OTHERS THEN
	-- DO NOT RAISE ERROR
         NULL;
   END;
   RETURN;
END; $$;












