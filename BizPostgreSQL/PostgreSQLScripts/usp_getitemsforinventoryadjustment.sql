-- Stored procedure definition script USP_GetItemsForInventoryAdjustment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_GetItemsForInventoryAdjustment( --05052018 Change: Warehouse Location added
v_numDomainID NUMERIC(18,0),
v_numWarehouseID NUMERIC(18,0),
v_numItemGroupID NUMERIC DEFAULT 0,
v_KeyWord VARCHAR(1000) DEFAULT '', 
v_numCurrentPage NUMERIC(9,0) DEFAULT 0,
v_PageSize INTEGER DEFAULT 0,
INOUT v_numTotalPage NUMERIC(9,0)  DEFAULT NULL,    
v_SortChar CHAR(1) DEFAULT '0',
v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
v_numItemCode NUMERIC(18,0) DEFAULT NULL,
v_numWarehouseItemID NUMERIC(18,0) DEFAULT NULL,
v_numWLocationID NUMERIC(18,0) DEFAULT NULL, 
INOUT SWV_RefCur refcursor default null, 
INOUT SWV_RefCur2 refcursor default null,
INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strsql  VARCHAR(8000);
   v_strRowCount  VARCHAR(8000);
    /*paging logic*/
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_strColumns  TEXT DEFAULT '';
   v_strJoin  TEXT DEFAULT '';
   v_numFormID  NUMERIC(18,0) DEFAULT 126;

   v_Nocolumns  SMALLINT;
   v_bitCustom  BOOLEAN;
   v_tintOrder  SMALLINT;
   v_numFieldId  NUMERIC(18,0);
   v_vcFieldName  VARCHAR(200);
   v_vcAssociatedControlType  VARCHAR(200);
   v_vcDbColumnName  VARCHAR(200);

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
BEGIN
   v_firstRec :=(v_numCurrentPage
   -1)*v_PageSize::bigint;
   v_lastRec :=(v_numCurrentPage*v_PageSize::bigint+1);

   BEGIN
      CREATE TEMP SEQUENCE tt_TempForm_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50), 
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );


   v_Nocolumns := 0;

   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormID
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormID
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1) TotalRows;

   IF v_Nocolumns = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth)
      SELECT
      v_numFormID,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,1,false,intColumnWidth
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_numFormID AND bitDefault = true AND coalesce(bitSettingField,false) = true AND numDomainID = v_numDomainID
      ORDER BY
      tintOrder ASC;
   end if;


   INSERT INTO tt_TEMPFORM(tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType)
   SELECT
   tintRow+1 as tintOrder,	vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
	vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit, bitIsRequired,bitIsEmail,
	bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
	ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
   FROM
   View_DynamicColumns
   WHERE
   numFormId = v_numFormID
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitSettingField,false) = true
   AND coalesce(bitCustom,false) = false
   UNION
   SELECT
   tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,
		'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,
		bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,
		ListRelID,intColumnWidth,false,''
   from View_DynamicCustomColumns
   where numFormId = v_numFormID and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
   AND coalesce(bitCustom,false) = true;  

   select   COUNT(*) INTO v_COUNT FROM tt_TEMPFORM;

   WHILE v_i <= v_COUNT LOOP
      select   bitCustomField, tintOrder, vcFieldName, numFieldId, vcAssociatedControlType, vcDbColumnName INTO v_bitCustom,v_tintOrder,v_vcFieldName,v_numFieldId,v_vcAssociatedControlType,
      v_vcDbColumnName FROM
      tt_TEMPFORM WHERE
      ID = v_i;
      IF v_bitCustom = true then
    
         IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
        
            v_strColumns := coalesce(v_strColumns,'') || ',CFW'  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)  || '.Fld_Value  "' || coalesce(v_vcDbColumnName,'') || '"';
            v_strJoin := coalesce(v_strJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=I.numItemCode ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
        
            v_strColumns := coalesce(v_strColumns,'') || ',CASE WHEN COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=1 then ''Yes'' end   "'
            || coalesce(v_vcDbColumnName,'')
            || '"';
            v_strJoin := coalesce(v_strJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=I.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
        
            v_strColumns := coalesce(v_strColumns,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
            || ')  "' || coalesce(v_vcDbColumnName,'') || '"';
            v_strJoin := coalesce(v_strJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=I.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
        
            v_vcDbColumnName := 'Cust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcDbColumnName,'') || '"';
            v_strJoin := coalesce(v_strJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || ' and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=I.numItemCode    ';
            v_strJoin := coalesce(v_strJoin,'') || ' LEFT JOIN ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value';
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;


   v_strsql := '';
   v_strRowCount := '';

   v_strsql := coalesce(v_strsql,'') ||
   'WITH WarehouseItems AS (
	SELECT 
		ROW_NUMBER() OVER(ORDER by WI.numWareHouseItemID) AS RowNumber,
		WI.numWareHouseItemID,
		COALESCE(W.vcWarehouse,'''') || (CASE WHEN WI.numWLocationID = -1 THEN '' (Global)'' ELSE (CASE WHEN COALESCE(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' || COALESCE(WL.vcLocation,'''') END) END) vcWarehouse,
		COALESCE(WL.numWLocationID,0) AS numWLocationID,
		Case WHEN I.bitKitParent=true THEN 0 ELSE COALESCE(numOnHand,0) END as numOnHand,
		COALESCE(WI.numOnHand,0) + COALESCE(WI.numAllocation,0) AS TotalOnHand,
		Case WHEN I.bitKitParent=true THEN 0 ELSE COALESCE(numReorder,0) END as Reorder,
		Case WHEN I.bitKitParent=true THEN 0 ELSE COALESCE(numOnOrder,0) END as OnOrder,
		Case WHEN I.bitKitParent=true THEN 0 ELSE COALESCE(numAllocation,0) END as Allocation,
		Case WHEN I.bitKitParent=true THEN 0 ELSE COALESCE(numBackOrder,0) END as BackOrder,
		(CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END) monAverageCost,
		(COALESCE(WI.numOnHand,0) * (CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END)) AS monCurrentValue,
		CASE WHEN I.bitSerialized =true THEN 1 WHEN I.bitLotNo =true THEN 1 ELSE 0 END AS IsLotSerializedItem,
		I.numAssetChartAcntId,
		CASE 
			WHEN COALESCE(I.numItemGroup,0) > 0 
			THEN fn_GetAttributes(WI.numWareHouseItemID,false)
			ELSE ''''
		END AS vcAttribute,
		COALESCE(I.bitKitParent,false) AS bitKitParent,
		COALESCE(I.bitSerialized,false) AS bitSerialized,
		COALESCE(I.bitLotNo,false) as bitLotNo,
		COALESCE(I.bitAssembly,false) as bitAssembly,
		I.numItemcode,
		I.vcItemName,
		COALESCE(CompanyInfo.vcCompanyName,'''') AS vcCompanyName,
		COALESCE(WI.monWListPrice,0) AS monListPrice,
		COALESCE(I.txtItemDesc,'''') AS txtItemDesc,
		COALESCE(I.vcModelID,'''') AS vcModelID,
		COALESCE(I.vcManufacturer,'''') AS vcManufacturer,
		COALESCE(I.fltHeight,0) AS fltHeight,
		COALESCE(I.fltLength,0) AS fltLength,
		COALESCE(I.fltWeight,0) AS fltWeight,
		COALESCE(I.fltWidth,0) AS fltWidth,
		COALESCE(I.numItemGroup,0) AS ItemGroup,
		COALESCE(ItemGroups.vcItemGroup,'''') AS numItemGroup,
		COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numPurchaseUnit) ,'''') AS numPurchaseUnit,
		COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numSaleUnit) ,'''') AS numSaleUnit,
		COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numBaseUnit) ,'''') AS numBaseUnit,
		COALESCE(WI.vcWHSKU,'''') AS vcSKU,
		COALESCE(WI.vcBarCode,'''') AS numBarCodeId,
		COALESCE(TEMPVendor.monCost,0) AS monCost,
		COALESCE(TEMPVendor.intMinQty,0) AS intMinQty,
		COALESCE((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN COALESCE(I.bitKitParent,false) = true AND COALESCE((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) "IsDeleteKitWarehouse",
		(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WI.numItemID) > 0 THEN true ELSE false END) AS bitChildItemWarehouse '
   ||
   coalesce(v_strColumns,'')
   || ' FROM 
		WareHouseItems WI 
		INNER JOIN Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
		INNER JOIN Item I ON I.numItemCode = WI.numItemID 
		LEFT JOIN LATERAL
		(
			SELECT
				numVendorID,
				monCost,
				intMinQty
			FROM
				Vendor
			WHERE
				Vendor.numItemCode = I.numItemCode AND Vendor.numVendorID = I.numVendorID
		) AS TEMPVendor ON TRUE
		LEFT JOIN DivisionMaster ON TEMPVendor.numVendorID = DivisionMaster.numDivisionID
		LEFT JOIN CompanyInfo ON DivisionMaster.numCompanyID  = CompanyInfo.numCompanyId
		LEFT JOIN ItemGroups ON I.numItemGroup = ItemGroups.numItemGroupID '
   ||
   coalesce(v_strJoin,'')
   || ' WHERE (I.numItemCode = ' || SUBSTR(CAST(v_numItemCode AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numItemCode AS VARCHAR(30)),1,30) || ' = 0) AND
		(WI.numWareHouseItemID = ' || SUBSTR(CAST(v_numWarehouseItemID AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numWarehouseItemID AS VARCHAR(30)),1,30) || ' = 0) AND
		I.charItemType = ''P'' AND 
		I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) ||
   ' AND  WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) ||
   ' AND (WI.numWareHouseID=' || SUBSTR(CAST(v_numWarehouseID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numWarehouseID AS VARCHAR(20)),1,20) || ' = 0)' ||
   ' AND (WI.numWLocationID=' || SUBSTR(CAST(v_numWLocationID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numWLocationID AS VARCHAR(20)),1,20) || ' = 0)' ||
   ' AND (COALESCE(I.numItemGroup,0) =' || SUBSTR(CAST(v_numItemGroupID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numItemGroupID AS VARCHAR(20)),1,20) || ' = 0)';

   v_strRowCount := coalesce(v_strRowCount,'') ||
   ' SELECT 
COUNT(*) AS TotalRowCount
FROM WareHouseItems WI INNER JOIN Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
INNER JOIN Item I ON I.numItemCode = WI.numItemID
WHERE (I.numItemCode = ' || SUBSTR(CAST(v_numItemCode AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numItemCode AS VARCHAR(30)),1,30) || ' = 0) AND
(WI.numWareHouseItemID = ' || SUBSTR(CAST(v_numWarehouseItemID AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numWarehouseItemID AS VARCHAR(30)),1,30) || ' = 0) AND 
I.charItemType = ''P''  
AND I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20)  || ' 
AND  WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || '  
AND (WI.numWareHouseID=' || SUBSTR(CAST(v_numWarehouseID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numWarehouseID AS VARCHAR(20)),1,20) || ' = 0)' ||
   ' AND (WI.numWLocationID=' || SUBSTR(CAST(v_numWLocationID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numWLocationID AS VARCHAR(20)),1,20) || ' = 0)' ||
   'AND (COALESCE(I.numItemGroup,0) =' || SUBSTR(CAST(v_numItemGroupID AS VARCHAR(20)),1,20) || ' OR ' || SUBSTR(CAST(v_numItemGroupID AS VARCHAR(20)),1,20) || ' = 0) ';


   IF v_KeyWord <> '' then
        
      IF POSITION('vcCompanyName' IN v_KeyWord) > 0 then
                
         v_strsql := coalesce(v_strsql,'')
         || ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
         || '  
									and Vendor.numItemCode= I.numItemCode '
         || coalesce(v_KeyWord,'') || ')';
         v_strRowCount :=  coalesce(v_strRowCount,'')  ||	 ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
         || '  
									and Vendor.numItemCode= I.numItemCode '
         || coalesce(v_KeyWord,'') || ')';
      ELSE
         v_strsql := coalesce(v_strsql,'') || ' and  1 = 1 ' || coalesce(v_KeyWord,'');
         v_strRowCount :=  coalesce(v_strRowCount,'')  || ' and  1 = 1 ' || coalesce(v_KeyWord,'');
      end if;
   end if;


   if v_SortChar <> '0' then
	
      v_strsql := coalesce(v_strsql,'') || ' AND vcItemName like ''' || coalesce(v_SortChar,'') || '%'')';
      v_strRowCount := coalesce(v_strRowCount,'') || ' AND vcItemName like ''' || coalesce(v_SortChar,'') || '%''';
   ELSE
      v_strsql := coalesce(v_strsql,'') || ' ) ';
   end if;


 





   v_strsql := coalesce(v_strsql,'') || ' SELECT * FROM WarehouseItems WHERE 
                            RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(20)),1,20) || ' AND RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(20)),1,20) || '; ' || ;
 
   
  
   RAISE NOTICE '%',(v_strsql);
   OPEN SWV_RefCur FOR EXECUTE v_strsql;

   RAISE NOTICE '%',(v_strRowCount);
   OPEN SWV_RefCur2 FOR EXECUTE v_strRowCount;

   open SWV_RefCur3 for
   SELECT * FROM tt_TEMPFORM;
   RETURN;
END; $$;




