-- Stored procedure definition script USP_OpportunityMaster_DisassociatePO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_DisassociatePO(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_vcOppItemIds TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_vcOppItemIds,'') <> '' then
	
      IF EXISTS(SELECT numComissionID FROM BizDocComission WHERE numDomainId = v_numDomainID AND coalesce(monCommissionPaid,0) <> 0 AND numOppItemID IN(SELECT Id FROM SplitIDs(v_vcOppItemIds,','))) then
		
         RAISE EXCEPTION 'COMMISSION_PAID';
      end if;
      IF EXISTS(SELECT numComissionID FROM BizDocComission INNER JOIN OpportunityBizDocItems OBDI ON BizDocComission.numOppBizDocItemID = OBDI.numOppBizDocItemID WHERE numDomainId = v_numDomainID AND coalesce(monCommissionPaid,0) <> 0 AND OBDI.numOppItemID IN(SELECT Id FROM SplitIDs(v_vcOppItemIds,','))) then
		
         RAISE EXCEPTION 'COMMISSION_PAID';
      end if;
      DELETE FROM SalesOrderLineItemsPOLinking WHERE numDomainID = v_numDomainID AND numSalesOrderID = v_numOppID AND numSalesOrderItemID IN(SELECT Id FROM SplitIDs(v_vcOppItemIds,','));
   end if;
   RETURN;
END; $$;


