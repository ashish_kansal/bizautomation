-- Stored procedure definition script USP_GetProfitLossDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProfitLossDetails(v_numDomainId NUMERIC(9,0),                                              
v_dtFromDate TIMESTAMP,                                            
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL TEXT;                                            
   v_i  INTEGER;
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   v_strSQL := '';            
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1           
                                            
   select   count(*) INTO v_i From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                   
   GJH.numDomainId = v_numDomainId And
   GJH.datEntry_Date <= CAST(v_dtToDate AS timestamp);     
   
   if v_i = 0 then
  
      v_strSQL := ' Select COA.numAcntType as numAcntType,COA.numParntAcntId as numParntAcntId,        
     COA.numAccountId as numAccountId,COA.vcCatgyName as AcntTypeDescription,                                
     fn_GetCurrentOpeningBalanceForProfitLoss(COA.numAccountId,''' || v_dtFromDate || ''',''' || v_dtToDate || ''',' || COALESCE(v_numDomainId,0) || ') As Amount         
              From Chart_Of_Accounts COA          
  inner join ListDetails LD on COA.numAcntType = LD.numListItemID                          
       Where COA.numDomainId =' || COALESCE(v_numDomainId,0) || 'And COA.numAccountId <>' || COALESCE(v_numParntAcntId,0) ||
      'And (COA.numAcntType = 822 Or COA.numAcntType = 823 Or COA.numAcntType = 824 Or COA.numAcntType = 825 Or COA.numAcntType = 826)';
      v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>= CAST(''' || v_dtFromDate || ''' AS DATE) And  COA.dtOpeningDate<= CAST(''' || v_dtToDate || ''' AS DATE)';
   Else
      v_strSQL := ' Select COA.numAcntType as numAcntType,COA.numParntAcntId as numParntAcntId,         
   COA.numAccountId as numAccountId,COA.vcCatgyName as AcntTypeDescription,                                
   fn_GetCurrentOpeningBalanceForProfitLoss(COA.numAccountId,''' || v_dtFromDate || ''',''' || v_dtToDate || ''',' || COALESCE(v_numDomainId,0) || ') As Amount          
         From Chart_Of_Accounts COA        
        inner join ListDetails LD on COA.numAcntType = LD.numListItemID                         
        Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <>' || COALESCE(v_numParntAcntId,0) ||
      'And(COA.numAcntType = 822 Or COA.numAcntType = 823 Or COA.numAcntType = 824 Or COA.numAcntType = 825 Or COA.numAcntType = 826)';
   end if;                                      
   RAISE NOTICE '%',v_strSQL;                                            
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


