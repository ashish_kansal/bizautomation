CREATE OR REPLACE FUNCTION USP_CheckRuleConditonAction(v_numDomainID NUMERIC(9,0),
	v_numRecordID NUMERIC(18,0) ,   
	v_numRuleID NUMERIC(9,0),
	v_tintModuleType SMALLINT,
	v_numRuleConditionID NUMERIC(9,0),
	INOUT SWV_RefCur refcursor,
	INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCondition  VARCHAR(4000);
   v_sql  TEXT;
   v_numROWCOUNT  NUMERIC;
   v_minROWNUMBER  INTEGER;
   v_maxROWNUMBER  INTEGER;
   v_vcDbColumnName  VARCHAR(100);
   v_vcFieldValue  VARCHAR(1000);
   v_sqlUpdate  TEXT;
BEGIN
	v_vcCondition := '';
	v_numROWCOUNT := 0;

	select 
		vcCondition INTO v_vcCondition 
	from 
		RuleCondition 
	where 
		numRuleID = v_numRuleID 
		and numDomainID = v_numDomainID 
		and numRuleConditionID = v_numRuleConditionID;

	IF v_tintModuleType = 49 then --Biz Doc Approval

      v_sql := 'select count(*) from OpportunityBizDocs where numOppBizDocsId=$1 ' || Case WHEN v_vcCondition = '(  )' then '' else 'and' || coalesce(v_vcCondition,'')  end;
     EXECUTE v_sql INTO v_numROWCOUNT USING v_numRecordID;
      
	  IF v_numROWCOUNT > 0 then
         IF(select count(*) from ruleAction where numRuleID = v_numRuleID and numDomainID = v_numDomainID and numRuleConditionID = v_numRuleConditionID and tintActionType = 4) > 0 then
            DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
            Create TEMPORARY TABLE tt_TEMPTABLE
            (
               ROWNUMBER  INTEGER,
               vcDbColumnName  VARCHAR(100),
               vcFieldValue VARCHAR(1000)
            );
            insert into tt_TEMPTABLE  select ROW_NUMBER() OVER(order by RA.tintActionOrder) AS ROWNUMBER,DFM.vcDbColumnName,RA.vcFieldValue
            from ruleAction RA join View_DynamicDefaultColumns DFM on RA.numActionTemplateID = DFM.numFieldID
            where DFM.numDomainID = v_numDomainID and RA.numRuleID = v_numRuleID and RA.numDomainID = v_numDomainID and RA.numRuleConditionID = v_numRuleConditionID and RA.tintActionType = 4
            and DFM.numFormId = v_tintModuleType;
            select   MIN(ROWNUMBER), MAX(ROWNUMBER) INTO v_minROWNUMBER,v_maxROWNUMBER FROM tt_TEMPTABLE;
            WHILE  v_minROWNUMBER <= v_maxROWNUMBER LOOP
               select   vcDbColumnName, vcFieldValue INTO v_vcDbColumnName,v_vcFieldValue FROM tt_TEMPTABLE WHERE ROWNUMBER = v_minROWNUMBER;
               v_sqlUpdate := 'update OpportunityBizDocs set ' || coalesce(v_vcDbColumnName,'') || '= $2 where numOppBizDocsId=$1';
               EXECUTE v_sqlUpdate USING v_numRecordID,v_vcFieldValue;
               select   MIN(ROWNUMBER) INTO v_minROWNUMBER FROM  tt_TEMPTABLE WHERE  ROWNUMBER > v_minROWNUMBER;
            END LOOP;
            DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         end if;
         open SWV_RefCur for
         select * from ruleAction where numRuleID = v_numRuleID and numDomainID = v_numDomainID and numRuleConditionID = v_numRuleConditionID and tintActionType != 4 order by tintActionOrder;
         open SWV_RefCur2 for
         select OppBizDocs.vcBizDocID,OPPMaster.numDivisionId,OPPMaster.numContactId from OpportunityBizDocs OppBizDocs join OpportunityMaster OPPMaster
         on OppBizDocs.numoppid = OPPMaster.numOppId
         where OppBizDocs.numOppBizDocsId = v_numRecordID and OPPMaster.numDomainId = v_numDomainID;
      end if;
   end if;
   RETURN;
END; $$;


