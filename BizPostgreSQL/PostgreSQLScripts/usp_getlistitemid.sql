CREATE OR REPLACE FUNCTION usp_GetListItemId(v_numStageItemId NUMERIC   
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numlistitemid AS "usp_getlistitemid" 
	FROM
		stagelistitemdetails 
	WHERE 
		numstagelistitemid = v_numStageItemId;
END; $$;












