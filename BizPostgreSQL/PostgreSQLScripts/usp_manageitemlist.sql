-- Stored procedure definition script USP_ManageItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageItemList(INOUT v_numListItemID NUMERIC(9,0) ,                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,                     
v_vcData VARCHAR(100) DEFAULT '',             
v_numListID NUMERIC(9,0) DEFAULT 0,                      
v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
v_numListType NUMERIC(9,0) DEFAULT NULL,
v_tintOppOrOrder SMALLINT DEFAULT NULL,
v_bitEnforceMinOrderAmount BOOLEAN DEFAULT false,
v_fltMinOrderAmount DECIMAL DEFAULT 0,	
v_numListItemGroupId NUMERIC DEFAULT 0,
v_vcColorScheme VARCHAR(500) DEFAULT '')
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sintOrder  SMALLINT;     
   v_numListItemIDPK  NUMERIC(9,0);
   v_CurrentDate  TIMESTAMP;
BEGIN
   if v_numListItemID = 0 then 
      update Listdetails set  sintOrder = 0 where sintOrder is null;
      select max(sintOrder)+1 INTO v_sintOrder from Listdetails  where numListID = v_numListID;
      insert into Listdetails(numListID,vcData,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainid,constFlag,sintOrder,numListType,tintOppOrOrder)
	values(v_numListID,v_vcData,v_numUserCntID,TIMEZONE('UTC',now()),v_numUserCntID,TIMEZONE('UTC',now()),false,v_numDomainID,false,v_sintOrder,v_numListType,v_tintOppOrOrder);
	
      v_numListItemIDPK := CURRVAL('ListDetails_seq');
      v_numListItemID := CURRVAL('ListDetails_seq');
      v_CurrentDate := TIMEZONE('UTC',now());
      IF v_bitEnforceMinOrderAmount = true AND v_numListID = 21 then
	
         INSERT INTO SalesOrderRule(numDomainID, numlistitemid, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
		VALUES(v_numDomainID, v_numListItemID, v_numListID, v_bitEnforceMinOrderAmount, v_fltMinOrderAmount);
      end if;
   else
      update Listdetails set vcData = v_vcData,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      numListType = v_numListType,tintOppOrOrder = v_tintOppOrOrder,
      numListItemGroupId = v_numListItemGroupId
      where numListItemID = v_numListItemID;     
	--To update Category Description in Chart_of_accounts table  
	--update Chart_Of_Accounts Set vcCatgyName=@vcData,vcCatgyDescription=@vcData Where numListItemID=@numListItemID  
      IF(v_numListID = 30) then
	
         IF((SELECT COUNT(*) FROM DycFieldColorScheme WHERE numFieldID = 18 AND numFormID = 34 AND numDomainID = v_numDomainID AND vcFieldValue = v_numListItemID) = 0) then
		
            IF(v_vcColorScheme <> '') then
			
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,34,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,35,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,36,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,38,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,39,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,40,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
				
               INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES(18,41,v_numDomainID,v_numListItemID,'0',v_vcColorScheme);
            ELSE
               DELETE FROM DycFieldColorScheme WHERE numFieldID = 18 AND numFormID IN(34,35,36,38,39,40,41) AND numDomainID = v_numDomainID AND vcFieldValue = v_numListItemID;
            end if;
         ELSE
            IF(v_vcColorScheme <> '') then
			
               UPDATE
               DycFieldColorScheme
               SET
               vcColorScheme = v_vcColorScheme
               WHERE
               numDomainID = v_numDomainID AND
               vcFieldValue = v_numListItemID AND
               numFieldID = 18 AND
               numFormID IN(34,35,36,38,39,40,41);
            ELSE
               DELETE FROM DycFieldColorScheme WHERE numFieldID = 18 AND numFormID IN(34,35,36,38,39,40,41) AND numDomainID = v_numDomainID AND vcFieldValue = v_numListItemID;
            end if;
         end if;
      end if;
      IF v_bitEnforceMinOrderAmount = true AND v_numListID = 21 then
	
         IF EXISTS(SELECT * FROM SalesOrderRule WHERE numlistitemid = v_numListItemID) then
		
            UPDATE SalesOrderRule SET
            bitEnforceMinOrderAmount = v_bitEnforceMinOrderAmount,fltMinOrderAmount = v_fltMinOrderAmount
            WHERE numlistitemid = v_numListItemID;
         ELSE
            INSERT INTO SalesOrderRule(numDomainID, numlistitemid, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
			VALUES(v_numDomainID, v_numListItemID, v_numListID, v_bitEnforceMinOrderAmount, v_fltMinOrderAmount);
         end if;
      ELSEIF v_bitEnforceMinOrderAmount = false AND v_numListID = 21
      then
	
         IF EXISTS(SELECT * FROM SalesOrderRule WHERE numlistitemid = v_numListItemID) then
		
            UPDATE SalesOrderRule SET
            bitEnforceMinOrderAmount = v_bitEnforceMinOrderAmount,fltMinOrderAmount = 0
            WHERE numlistitemid = v_numListItemID;
         end if;
      end if;
   end if;
   RETURN;
END; $$;


