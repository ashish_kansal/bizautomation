-- Stored procedure definition script USP_ManageUOMConversion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageUOMConversion(v_numDomainID NUMERIC(9,0),
	v_vcUnitConversion TEXT,
	v_bitEnableItemLevelUOM BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_iDoc  INTEGER;   

  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
UPDATE Domain SET bitEnableItemLevelUOM = v_bitEnableItemLevelUOM WHERE numDomainId = v_numDomainID;
      DELETE FROM UOMConversion WHERE numDomainID = v_numDomainID;

      INSERT INTO UOMConversion(numUOM1,decConv1,numUOM2,decConv2,numDomainID)
      SELECT
      numUOM1,decConv1,numUOM2,decConv2,v_numDomainID
      FROM
	  XMLTABLE
			(
				'NewDataSet/UnitConversion'
				PASSING 
					CAST(v_vcUnitConversion AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numUOM1 NUMERIC(9,0) PATH 'numUOM1',
					decConv1 DECIMAL(18,5) PATH 'decConv1',
					numUOM2 NUMERIC(9,0) PATH 'numUOM2',
					decConv2 DECIMAL(18,5) PATH 'decConv2'
			) AS X;

      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$; 
	





