-- Stored procedure definition script USP_GetAdminUserForDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAdminUserForDomain(v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numAdminID as VARCHAR(255)) FROM Domain WHERE numDomainId = v_numDomainID;
END; $$;













