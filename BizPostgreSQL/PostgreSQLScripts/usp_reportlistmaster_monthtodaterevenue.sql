-- Stored procedure definition script USP_ReportListMaster_MonthToDateRevenue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_MonthToDateRevenue(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   coalesce(fn_GetContactName(OpportunityMaster.numassignedto),'') as "AssignedTo"
		,CONCAT('~/contact/frmContacts.aspx?CntID=',OpportunityMaster.numassignedto) AS "URL"
		,COUNT(*) as "TotalRecords"
		,CAST(SUM(OpportunityBizDocs.monDealAmount) as NUMERIC(18,0))  as "InvoiceGrandtotal"
   FROM
   OpportunityMaster
   INNER JOIN DivisionMaster on OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN CompanyInfo on CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
   INNER JOIN AdditionalContactsInformation on OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
   LEFT JOIN OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numoppid
   WHERE
   OpportunityMaster.tintopptype = 1
   AND OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
   AND OpportunityMaster.bintCreatedDate  between get_month_start(TO_CHAR(TIMEZONE('UTC',now())+INTERVAL '-330 minute','mm/dd/yyyy'):: timestamp+INTERVAL '-330 minute') and get_month_end(TO_CHAR(TIMEZONE('UTC',now())+INTERVAL '-330 minute','mm/dd/yyyy'):: timestamp+INTERVAL '-330 minute')
   GROUP BY
   OpportunityMaster.numassignedto;
END; $$;












