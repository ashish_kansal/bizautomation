-- Stored procedure definition script usp_updateoppserializeditems_BizDoc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_updateoppserializeditems_BizDoc(v_numOppItemTcode NUMERIC(9,0) DEFAULT 0,  
v_numOppID NUMERIC(9,0) DEFAULT 0,  
v_strItems TEXT DEFAULT '',
v_OppType SMALLINT DEFAULT NULL,
v_numBizDocID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;  
-- STORE PROCEDURE LOGIC IS COMMENTED BECAUSE IT IS NOT USED NOW AND CODE IS NOT RELEVANT TO BIZ NOW.
--declare @hDoc as int                
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    


--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID,UsedQty,@numBizDocID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),UsedQty NUMERIC(9)))
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),UsedQty NUMERIC(9),TotalQty NUMERIC(9)) WHERE (TotalQty-UsedQty)=0)


  
--EXEC sp_xml_removedocument @hDoc


