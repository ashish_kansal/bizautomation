-- Stored procedure definition script USP_ConECampaign for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConECampaign(v_numContactID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  numConEmailCampID,ConECampaign.numECampaignID,intStartDate,bitEngaged,coalesce(txtDesc,'-') as txtDesc from ConECampaign
   join ECampaign on ECampaign.numECampaignID = ConECampaign.numECampaignID
   where  numContactID = v_numContactID order by numConEmailCampID desc LIMIT 1;
END; $$;
