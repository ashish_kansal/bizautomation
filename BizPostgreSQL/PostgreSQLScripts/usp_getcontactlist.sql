-- Stored procedure definition script USP_GetContactList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetContactList(                                                 
--                                                        
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                        
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                        
 v_tintUserRightType SMALLINT DEFAULT 0,                                                        
 v_tintSortOrder SMALLINT DEFAULT 4,                                                        
 v_SortChar CHAR(1) DEFAULT '0',                                                       
 v_FirstName VARCHAR(100) DEFAULT '',                                                      
 v_LastName VARCHAR(100) DEFAULT '',                                                      
 v_CustName VARCHAR(100) DEFAULT '',                                                      
 v_CurrentPage INTEGER DEFAULT NULL,                                                      
v_PageSize INTEGER DEFAULT NULL,                                                      
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                      
v_columnName VARCHAR(50) DEFAULT NULL,                                                      
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                      
v_numDivisionID NUMERIC(9,0) DEFAULT 0,                  
v_bitPartner BOOLEAN DEFAULT NULL ,                       
v_inttype NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                
   v_firstRec  INTEGER;                                                      
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numcontactid VARCHAR(15)
   );                                                      
                                                      
                                                      
                                                      
                                                      
   v_strSql := 'Select ';                                              
   if (v_tintSortOrder = 5 or v_tintSortOrder = 6) then 
      v_strSql := coalesce(v_strSql,'') || '  ';
   end if;                                               
                                                        
   v_strSql := coalesce(v_strSql,'') || ' ADC.numContactId                                                  
  FROM AdditionalContactsInformation ADC                                     
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                    
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                     
  left join ListDetails LD on LD.numListItemID = C.numCompanyRating                                               
  left join ListDetails LD1 on LD1.numListItemID = ADC.numEmpStatus';                   
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID = DM.numDivisionID and bitShareportal = true and CA.bitDeleted = false                     
and CA.numDivisionID =(select numDivisionId from AdditionalContactsInformation where numContactId =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;                                       
                                        
   if v_tintSortOrder = 7 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactID = ADC.numContactId ';
   end if;                                         
                                                        
   v_strSql := coalesce(v_strSql,'') || '                                                        
        where DM.tintCRMType <> 0                         
    and DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ''; 
   if v_inttype <> 0 then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numContactType  = ' || SUBSTR(CAST(v_inttype AS VARCHAR(10)),1,10);
   end if;
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                                      
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''';
   end if;                  
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and C.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                      
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And ADC.vcFirstName ilike ''' || coalesce(v_SortChar,'') || '%'' LIMIT 20';
   end if;                                                               
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or (CA.bitShareportal=true and  CA.bitDeleted=false)' else '' end;
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;                                                       
   if v_numDivisionID <> 0 then  
      v_strSql := coalesce(v_strSql,'') || ' And DM.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10) || '   ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                                                   
                                                                
   if v_tintSortOrder = 2 then  
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || '  AND ADC.numContactType=92  ';
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND ADC.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 5
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ORDER BY ADC.bintCreateddate desc ';
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ORDER BY ADC.bintmodifieddate desc ';
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                                                 
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF (v_tintSortOrder = 5 and v_columnName != 'ADC.bintcreateddate')
   then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF (v_tintSortOrder = 6 and v_columnName != 'ADC.bintcreateddate')
   then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and cType=''C'' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                                                     
                               
   RAISE NOTICE '%',v_strSql;          
   EXECUTE 'insert into tt_TEMPTABLE(numContactID)                                                      
' || v_strSql;                                                       
                                                      
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                       
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                                                         
   open SWV_RefCur for
   Select ADC.numContactId AS numContactID,
              ADC.vcFirstName || ' ' || ADC.vcLastname AS Contact,
              C.vcCompanyName || ', ' || coalesce(fn_GetListItemName(numCompanyType),'-') AS Company,
              case when ADC.numPhone <> '' then ADC.numPhone || case when ADC.numPhoneExtension <> '' then ' - ' || ADC.numPhoneExtension else '' end  else '' end as Telephone,
              ADC.vcEmail,
              LD.vcData  AS Rating,
              LD1.vcData AS Status,
     C.numCompanyId,
     DM.tintCRMType,
     DM.numTerID,
     DM.numDivisionID,
     ADC.numRecOwner
   FROM AdditionalContactsInformation ADC
   JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
   join CompanyInfo C ON DM.numCompanyID = C.numCompanyId
   left join Listdetails LD on LD.numListItemID = C.numCompanyRating
   left join Listdetails LD1 on LD1.numListItemID = ADC.numEmpStatus
   join tt_TEMPTABLE T on cast(NULLIF(T.numContactID,'') as NUMERIC(18,0)) = ADC.numContactId
   WHERE ID > v_firstRec and ID < v_lastRec order by ID;                                     
                                                    
   RETURN;
END; $$;


