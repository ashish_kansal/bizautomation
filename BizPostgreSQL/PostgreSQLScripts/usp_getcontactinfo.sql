-- Stored procedure definition script usp_GetContactInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetContactInfo(           
--        
        
v_numContactID NUMERIC DEFAULT 0,        
 v_numDivID NUMERIC DEFAULT 0,                
 v_tintUserRightType SMALLINT DEFAULT 0,        
 v_bitShowAll BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDivID = 0 then
  
      open SWV_RefCur for
      SELECT vcLastname, vcFirstName,vcFirstName || ' ' || vcLastname as Name,vcGivenName, vcPosition, vcEmail, vcDepartment,
    numPhone, numPhoneExtension, vcFax, numContactType,   vcData ,
    AC.numDivisionId, bintDOB, txtNotes, AC.numCreatedBy,
   DM.numTerID, --New        
   DM.numCreatedBy, --New
   vcFirstName || ' ' || vcLastname || CASE WHEN lst.vcData IS NOT NULL THEN ' (' || lst.vcData || ')' ELSE '' end as ContactType,
    AC.bitPrimaryContact,
   GetListIemName(coalesce(vcDepartment,0)) AS vcDepartmentName,
   GetListIemName(coalesce(vcPosition,0)) AS vcPositionName,
   GetListIemName(coalesce(numContactType,0)) AS vcContactTypeName
      FROM AdditionalContactsInformation AC join
      DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
      left join Listdetails lst on AC.numContactType = lst.numListItemID
      WHERE numContactId = v_numContactID;
   ELSE
      open SWV_RefCur for
      SELECT numContactId,vcFirstName || ' ' || vcLastname as Name,vcLastname, vcFirstName,vcGivenName, vcPosition, vcEmail, vcDepartment,
    numPhone, numPhoneExtension, vcFax, numContactType,   vcData ,
     bintDOB, txtNotes, AC.numCreatedBy,
   DM.numTerID, --New        
   DM.numCreatedBy, --New  
   vcFirstName || ' ' || vcLastname || CASE WHEN lst.vcData IS NOT NULL THEN ' (' || lst.vcData || ')' ELSE '' end as ContactType,
    AC.bitPrimaryContact,
   GetListIemName(coalesce(vcDepartment,0)) AS vcDepartmentName,
   GetListIemName(coalesce(vcPosition,0)) AS vcPositionName,
   GetListIemName(coalesce(numContactType,0)) AS vcContactTypeName
      FROM AdditionalContactsInformation AC join
      DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
      left join Listdetails lst on AC.numContactType = lst.numListItemID
      WHERE AC.numDivisionId = v_numDivID;
   end if;
   RETURN;
END; $$;


