-- Stored procedure definition script USP_Communication_Finish for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Communication_Finish(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numCommID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Communication
   SET
   nummodifiedby = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),bitClosedFlag = true,
   dtEventClosedDate = TIMEZONE('UTC',now())
   WHERE
   numDomainID = v_numDomainID
   AND numCommId = v_numCommID;
   RETURN;
END; $$;


