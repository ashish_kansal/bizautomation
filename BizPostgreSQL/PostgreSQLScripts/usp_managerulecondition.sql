-- Stored procedure definition script USP_ManageRuleCondition for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageRuleCondition(v_numRuleID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_vcCondition VARCHAR(4000),
    v_tintConditionOrder SMALLINT, 
    v_vcJSON TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintConditionOrder = 0 then
	
      delete from RuleCondition where numRuleID = v_numRuleID;
      delete from ruleAction where numRuleID = v_numRuleID;
   end if;
           
   INSERT  INTO RuleCondition(numDomainID,numRuleID,vcCondition,tintConditionOrder,vcJSON)
            VALUES(v_numDomainID,v_numRuleID,v_vcCondition,v_tintConditionOrder,v_vcJSON);

            
open SWV_RefCur for SELECT  cast(CURRVAL('RuleCondition_seq') as VARCHAR(255));
END; $$;  












