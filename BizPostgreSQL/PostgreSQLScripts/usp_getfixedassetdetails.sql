-- Stored procedure definition script USP_GetFixedAssetDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetFixedAssetDetails(v_numFixedAssetId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From FixedAssetDetails Where numFixedAssetId = v_numFixedAssetId;
END; $$;












