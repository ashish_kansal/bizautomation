-- Stored procedure definition script USP_DeleteCommissionsContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteCommissionsContacts(v_numCommContactID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CommissionContacts WHERE numCommContactID = v_numCommContactID;
   RETURN;
END; $$;


