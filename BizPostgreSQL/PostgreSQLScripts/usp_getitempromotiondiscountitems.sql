-- Stored procedure definition script USP_GetItemPromotionDiscountItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemPromotionDiscountItems(v_numDomainID NUMERIC(18,0), 
	v_numDivisionID NUMERIC(18,0),
	v_numPromotionID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numPageIndex INTEGER,
	v_numPageSize INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintDiscountBasedOn  SMALLINT;
   v_tintDiscountType  SMALLINT;
   v_fltDiscountValue  DOUBLE PRECISION;
   v_vcCurrency  VARCHAR(10);
   v_numWarehouseID  NUMERIC(18,0);
   v_tintItemCalDiscount  SMALLINT;
   v_monDiscountedItemPrice  DECIMAL(20,5);
BEGIN
   select   coalesce(vcCurrency,'$') INTO v_vcCurrency FROm Domain WHERE numDomainId = v_numDomainID;

   select   coalesce(numWareHouseID,0) INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
	
   select   tintDiscoutBaseOn, tintDiscountType, coalesce(fltDiscountValue,0), coalesce(tintItemCalDiscount,0), coalesce(monDiscountedItemPrice,0) INTO v_tintDiscountBasedOn,v_tintDiscountType,v_fltDiscountValue,v_tintItemCalDiscount,
   v_monDiscountedItemPrice FROM
   PromotionOffer WHERE
   numProId = v_numPromotionID; 

   DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEM
   (
      numItemCode NUMERIC(18,0)
   );

   RAISE NOTICE '%',v_tintDiscountBasedOn;

   IF v_tintDiscountBasedOn = 1 then -- Based on individual item(s)
	
      INSERT INTO tt_TEMPITEM(numItemCode)
      SELECT DISTINCT
      numValue
      FROM
      PromotionOfferItems
      WHERE
      numProId = v_numPromotionID
      AND tintRecordType = 6
      AND tintType = 1;
   ELSEIF v_tintDiscountBasedOn = 2
   then -- Based on individual item classification(s)
	
      INSERT INTO tt_TEMPITEM(numItemCode)
      SELECT DISTINCT
      numItemCode
      FROM
      Item
      WHERE
      numDomainID = v_numDomainID
      AND numItemClassification IN(SELECT
         numValue
         FROM
         PromotionOfferItems
         WHERE
         numProId = v_numPromotionID
         AND tintRecordType = 6
         AND tintType = 2);
   ELSEIF v_tintDiscountBasedOn = 3
   then  -- Based on related item(s)
	
      INSERT INTO tt_TEMPITEM(numItemCode)
      SELECT DISTINCT
      numItemCode
      FROM
      SimilarItems
      WHERE
      numParentItemCode = v_numItemCode;
   ELSEIF v_tintDiscountBasedOn = 6
   then
	
      INSERT INTO tt_TEMPITEM(numItemCode)
      SELECT DISTINCT
      numValue
      FROM
      PromotionOfferItems
      WHERE
      numProId = v_numPromotionID
      AND tintRecordType = 6
      AND tintType = 4;
   end if;

   open SWV_RefCur for SELECT
   COUNT(Item.numItemCode) OVER() AS TotalRecords
		,Item.numItemCode
		,coalesce(Item.vcItemName,'') AS vcItemName
		,coalesce(Item.txtItemDesc,'') AS txtItemDesc
		,(CASE
   WHEN v_tintDiscountBasedOn = 6
   THEN v_monDiscountedItemPrice
   ELSE(CASE
      WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
      THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
         coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
         0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
      ELSE coalesce(monListPrice,0)
      END)
   END) AS monListPrice
		,(CASE v_tintDiscountType
   WHEN 1 THEN 0
   WHEN 2 THEN 1
   WHEN 3 THEN 0
   END) AS tintDiscountType
		,(CASE v_tintDiscountType
   WHEN 1 THEN CONCAT(v_fltDiscountValue,'%')
   WHEN 2 THEN CONCAT(v_vcCurrency,v_fltDiscountValue)
   WHEN 3 THEN '100%'
   END) AS vcDiscountType
		,(CASE v_tintDiscountType
   WHEN 1 THEN v_fltDiscountValue
   WHEN 2 THEN v_fltDiscountValue
   WHEN 3 THEN 100
   END) AS fltDiscount
		,(CASE v_tintDiscountType
   WHEN 1 THEN(CASE WHEN v_fltDiscountValue > 0 THEN(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) -((CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END)*(v_fltDiscountValue/100))  ELSE(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) END)
   WHEN 2 THEN(CASE WHEN v_fltDiscountValue > 0 THEN(CASE WHEN(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) > v_fltDiscountValue THEN((CASE
            WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
            THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
               coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
               0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
            ELSE coalesce(monListPrice,0)
            END) -v_fltDiscountValue) ELSE 0 END)  ELSE(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) END)
   WHEN 3 THEN 0
   END) AS monSalePrice
		,(CASE v_tintDiscountType
   WHEN 1 THEN(CASE WHEN v_fltDiscountValue > 0 THEN(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) -((CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END)*(v_fltDiscountValue/100))  ELSE(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) END)
   WHEN 2 THEN(CASE WHEN v_fltDiscountValue > 0 THEN(CASE WHEN(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) > v_fltDiscountValue THEN((CASE
            WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
            THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
               coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
               0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
            ELSE coalesce(monListPrice,0)
            END) -v_fltDiscountValue) ELSE 0 END)  ELSE(CASE
         WHEN (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true) AND coalesce(bitCalAmtBasedonDepItems,false) = true
         THEN GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,Item.numItemCode,1::DOUBLE PRECISION,v_numWarehouseItemID,
            coalesce(tintKitAssemblyPriceBasedOn,1)::SMALLINT,0::NUMERIC(18,0),
            0::NUMERIC(18,0),'',0::NUMERIC(18,0),1::DOUBLE PRECISION)
         ELSE coalesce(monListPrice,0)
         END) END)
   WHEN 3 THEN 0
   END) AS monTotalAmount
		,numWareHouseItemID
		,coalesce(Item.bitMatrix,false) AS bitMatrix
		,coalesce(numItemGroup,0) AS numItemGroup
		,(CASE WHEN coalesce(bitKitParent,false) = true THEN(CASE WHEN coalesce((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID = IInner.numItemCode WHERE numItemKitID = Item.numItemCode AND coalesce(IInner.bitKitParent,false) = true),0) > 0 THEN 1 ELSE 0 END) ELSE 0 END) AS bitHasChildKits
		,(CASE WHEN coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true THEN coalesce(bitCalAmtBasedonDepItems,false) ELSE false END) AS bitCalAmtBasedonDepItems
   FROM
   Item
   INNER JOIN
   tt_TEMPITEM T1
   ON
   Item.numItemCode = T1.numItemCode
   LEFT JOIN LATERAL(SELECT 
      numWareHouseItemID
      FROM
      WareHouseItems
      WHERE
      numItemID = Item.numItemCode
      AND (numWareHouseID = v_numWarehouseID OR v_numWarehouseItemID = 0) LIMIT 1) T2 on TRUE
   WHERE
   numDomainID = v_numDomainID
   ORDER BY
   vcItemName;
END; $$;












