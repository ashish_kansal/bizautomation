DROP FUNCTION IF EXISTS usp_getdealslist1;

CREATE OR REPLACE FUNCTION public.usp_getdealslist1(
	v_numusercntid numeric DEFAULT 0,
	v_numdomainid numeric DEFAULT 0,
	v_tintsalesuserrighttype smallint DEFAULT 0,
	v_tintpurchaseuserrighttype smallint DEFAULT 0,
	v_tintsortorder smallint DEFAULT 4,
	v_currentpage integer DEFAULT NULL::integer,
	v_pagesize integer DEFAULT NULL::integer,
	v_totrecs integer DEFAULT NULL::integer,
	v_columnname character varying DEFAULT NULL::character varying,
	v_columnsortorder character varying DEFAULT NULL::character varying,
	v_numcompanyid numeric DEFAULT 0,
	v_bitpartner boolean DEFAULT false,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	v_tintshipped smallint DEFAULT NULL::smallint,
	v_intorder smallint DEFAULT NULL::smallint,
	v_inttype smallint DEFAULT NULL::smallint,
	v_tintfilterby smallint DEFAULT 0,
	v_numorderstatus numeric DEFAULT NULL::numeric,
	v_vcregularsearchcriteria text DEFAULT ''::text,
	v_vccustomsearchcriteria text DEFAULT ''::text,
	v_searchtext character varying DEFAULT ''::character varying,
	v_sortchar character DEFAULT '0'::bpchar,
	v_tintdashboardremindertype smallint DEFAULT 0,INOUT SWV_RefCur refcursor default null,INOUT SWV_RefCur2 refcursor default null)
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
   v_PageId  SMALLINT;
   v_numFormId  INTEGER; 
   v_tintDecimalPoints  SMALLINT;
   v_Nocolumns  SMALLINT DEFAULT 0;

   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_strColumns  TEXT;
   v_tintOrder  SMALLINT;
   v_vcFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(1);
   v_vcAssociatedControlType  VARCHAR(30);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(200);
   v_WhereCondition  VARCHAR(2000);
   v_vcLookBackTableName  VARCHAR(2000);
   v_bitCustom  BOOLEAN;
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;   
   v_bitAllowEdit  BOOLEAN;                 
   v_vcColumnName  VARCHAR(500);          
	
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT '';        

   v_strShareRedordWith  VARCHAR(300);
   v_strExternalUser  TEXT DEFAULT '';
   v_StrSql  TEXT DEFAULT '';

   v_vcCSOrigDbCOlumnName  VARCHAR(50) DEFAULT '';
   v_vcCSLookBackTableName  VARCHAR(50) DEFAULT '';
   v_vcCSAssociatedControlType  VARCHAR(50);

   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
  
   v_strFinal  TEXT;
   v_vcSortFieldType  VARCHAR(50);
   v_Prefix  VARCHAR(10);
   v_temp  TEXT;
   v_tmpIds  TEXT DEFAULT '';
   SWV_RowCount INTEGER;
BEGIN
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'^LIKE?','ilike','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'^LIKE?','ilike','gi');
	v_vcRegularSearchCriteria := regexp_replace(v_vcRegularSearchCriteria,'OpportunityMaster\.','Opp.','gi');
	v_vcCustomSearchCriteria := regexp_replace(v_vcCustomSearchCriteria,'OpportunityMaster\.','Opp.','gi');

-- This procedure was converted on Wed Apr 07 23:33:33 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   select   coalesce(tintDecimalPoints,0) INTO v_tintDecimalPoints FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   v_PageId := 0;

   IF v_intType = 1 then
	
      v_PageId := 2;
      v_intType := 3;
      v_numFormId := 39;
   ELSEIF v_intType = 2
   then
	
      v_PageId := 6;
      v_intType := 4;
      v_numFormId := 41;
   end if;

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType VARCHAR(100)
   );

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = v_numFormId
      AND 1 =(CASE WHEN v_numFormId = 39 AND numFieldID = 771 THEN 0 ELSE 1 END)
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = v_numFormId) TotalRows;

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = v_numFormId AND numRelCntType = v_numFormId AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numFormId,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainId = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      AND 1 =(CASE WHEN v_numFormId = 39 AND numFieldID = 771 THEN 0 ELSE 1 END)
      UNION
      SELECT
      v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,v_numFormId,1,true,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainId = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO
      tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicColumnsMasterConfig.numDomainId = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.numDomainId = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = v_numFormId AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      IF v_Nocolumns = 0 then
		
         INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         v_numFormId,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,v_numFormId,1,false,intColumnWidth
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = v_numFormId
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = v_numDomainID
         AND 1 =(CASE WHEN v_numFormId = 39 AND numFieldID = 771 THEN 0 ELSE 1 END)
         ORDER BY
         tintOrder asc;
      end if;
      INSERT INTO
      tt_TEMPFORM
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,CAST(bitCustom AS Boolean),numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,CAST(bitAllowFiltering AS Boolean),vcFieldDataType
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      AND numRelCntType = v_numFormId
      AND 1 =(CASE WHEN v_numFormId = 39 AND numFieldID = 771 THEN 0 ELSE 1 END)
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_numFormId
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      AND numRelCntType = v_numFormId
      ORDER BY
      tintOrder asc;
   end if;
   UPDATE tt_TEMPFORM SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'intUsedShippingCompany' AND numModuleID = 3)  AS VARCHAR(30)) || '#' || CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'numShippingService' AND numModuleID = 3)  AS VARCHAR(30))
   WHERE vcDbColumnName = 'vcOrderedShipped';
   UPDATE tt_TEMPFORM SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'monAmountPaid' AND numModuleID = 3)  AS VARCHAR(30))
   WHERE vcDbColumnName = 'vcPOppName';
   v_strColumns := ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,COALESCE(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, COALESCE(Opp.monDealAmount,0) monDealAmount 
	,COALESCE(opp.numShippingService,0) AS numShippingService,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName,COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) as monAmountPaid ';
	--Corr.bitIsEmailSent,

   v_tintOrder := 0;
   v_WhereCondition := '';
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
   tt_TEMPFORM    ORDER BY
   tintOrder ASC LIMIT 1;            

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'Div.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'Opp.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityRecurring' then
            v_Prefix := 'OPR.';
         end if;
         IF v_vcLookBackTableName = 'CompanyInfo' then
            v_Prefix := 'cmp.';
         end if;
         IF v_vcLookBackTableName = 'ProjectProgress' then
            v_Prefix := 'PP.';
         end if;

         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';

		 If v_vcDbColumnName = 'vcCustomerPO#' THEN
			v_vcDbColumnName := '"vcCustomerPO#"';
		 END IF;

         IF v_vcAssociatedControlType = 'SelectBox' then
			
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
            IF v_vcDbColumnName = 'numStatus' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE(TEMPApproval.numApprovalCount,0) ApprovalMarginCount';
            end if;
            IF v_vcDbColumnName = 'numPartner' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(Opp.numPartner) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner';
            ELSEIF v_vcDbColumnName = 'vcSignatureType'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numShippingService'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR COALESCE(numDomainID,0)=0) AND numShipmentServiceID = COALESCE(Opp.numShippingService,0)),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'tintEDIStatus'
            then
				
					
               v_strColumns := coalesce(v_strColumns,'') || ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numPartenerContact'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,(Opp.numPartenerContact) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(A.vcFirstName,'' '',A.vcLastName) FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact';
            ELSEIF v_vcDbColumnName = 'tintInvoicing'
            then
				
               v_strColumns := CONCAT(v_strColumns,', CONCAT(COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',COALESCE(bitisemailsent,false),''#^#'',COALESCE(monDealAmount,0),''#^#'',COALESCE(monAmountPaid,0),''#^#'', CAST(cast(dtFromDate as date) AS VARCHAR),''#^#''),''$^$'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = COALESCE((SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' LIMIT 1),287)),'''')',CASE
               WHEN POSITION('tintInvoicing=3' IN v_vcRegularSearchCriteria) > 0
               THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														COALESCE(OI.numUnitHour,0) AS OrderedQty,
														COALESCE(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													LEFT JOIN LATERAL
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice ON TRUE
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'
               WHEN POSITION('tintInvoicing=4' IN v_vcRegularSearchCriteria) > 0
               THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM OpportunityRecurring OPR 
														INNER JOIN OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'
               ELSE
                  ','''''
               END,') "',v_vcColumnName,
               '"');
            ELSEIF v_vcDbColumnName = 'tintSource'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',CONCAT(COALESCE(fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0)::SMALLINT,Opp.numDomainID),''Internal Order''),(CASE WHEN COALESCE(TEMPMapping.numMappingCount,0) > 0 THEN CONCAT(''&nbsp;<a target="_blank" href="../opportunity/frmOpportunities.aspx?frm=deallist&OpID='',Opp.numOppID,''&SelectedTab=ProductsServices"><i class="fa fa-map-marker" style="color:red;font-size:18px" aria-hidden="true"></i></a>'') ELSE '''' END))' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'COALESCE(fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0)::SMALLINT,Opp.numDomainID),''Internal Order'') iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcDbColumnName = 'numCampainID'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'COALESCE((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
			ELSEIF v_vcDbColumnName = 'numProjectID'
            then
				v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((select vcprojectname from projectsmaster where numproid=' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '),'''') "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numContactID'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcDbColumnName = 'numReleaseStatus'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',(CASE Opp.numReleaseStatus WHEN 1 THEN ''Open'' WHEN 2 THEN ''Purchased'' ELSE '''' END) "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'LI'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'BP'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',SPLM.Slp_Name' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'SPLM.Slp_Name iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID ';
            ELSEIF v_vcListItemType = 'PP'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(PP.intTotalProgress,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'COALESCE(PP.intTotalProgress,0) iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || 'fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcListItemType = 'WI'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', Case when COALESCE(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || '(Case when COALESCE(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=COALESCE(OL.numWebApiId,0)
													left join Sites on COALESCE(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            IF v_Prefix = 'OPR.' then
				
               v_strColumns := coalesce(v_strColumns,'') || ', (SELECT FormatedDateFromDate(dtRecurringDate + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')' || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') FROM  OpportunityRecurring WHERE numOppID=OPP.numOppID AND COALESCE(numOppBizDocID,0)=0 and dtRecurringDate > GetUTCDateWithoutTime() LIMIT 1) AS  "' || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || '(SELECT FormatedDateFromDate(dtRecurringDate + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')' || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') FROM  OpportunityRecurring WHERE numOppID=OPP.numOppID AND COALESCE(numOppBizDocID,0)=0 and dtRecurringDate > GetUTCDateWithoutTime() LIMIT 1) iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcDbColumnName = 'dtReleaseDate'
            then
				
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now()  + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ','
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSEIF v_vcDbColumnName = 'dtExpectedDate'
            then
				
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' AS DATE)= CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ','
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            ELSE
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
               v_strColumns := coalesce(v_strColumns,'')
               || 'when CAST('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               v_strColumns := coalesce(v_strColumns,'')
               || 'else FormatedDateFromDate('
               || coalesce(v_Prefix,'')
               || coalesce(v_vcDbColumnName,'')
               || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),'
               || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
               || ') end  "'
               || coalesce(v_vcColumnName,'') || '"';
               IF LENGTH(v_SearchText) > 0 then
					
                  v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
            
            IF v_vcDbColumnName = 'vcCompactContactDetails' then
				
               v_strColumns := coalesce(v_strColumns,'') || ' ,'''' AS vcCompactContactDetails';
            ELSE
               v_strColumns := coalesce(v_strColumns,'')
               || ','
               || CASE
               WHEN v_vcDbColumnName = 'numAge' THEN 'EXTRACT(year FROM timezone(''utc'', now()))::INTEGER -EXTRACT(YEAR FROM bintDOB)::INTEGER'
               WHEN v_vcDbColumnName = 'monPAmount' THEN 'COALESCE((SELECT varCurrSymbol FROM Currency WHERE numCurrencyID = Opp.numCurrencyID),'''') || '' ''|| CAST(CAST(monPAmount AS DECIMAL(20,5)) AS VARCHAR)'
               WHEN v_vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
               WHEN v_vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
               WHEN v_vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
               WHEN v_vcDbColumnName = 'vcPOppName' THEN 'CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'vcBizDocsList' THEN 'COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''#^#'',vcBizDocID),''$^$'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' ||(CASE WHEN v_intType = 1 THEN '1' ELSE '0' END) || '  Then 0 ELSE 1 END)),'''')'
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               END
               || ' "'
               || coalesce(v_vcColumnName,'') || '"';
            end if;
            IF(v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'vcBizDocsList') then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND COALESCE(bitAuthoritativeBizDocs,0)=1)) AS List_Item_Approval_UNIT ';
            end if;
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) ||(CASE
               WHEN v_vcDbColumnName = 'numAge' THEN 'EXTRACT(year FROM timezone(''utc'', now()))::INTEGER -EXTRACT(YEAR FROM bintDOB)::INTEGER'
               WHEN v_vcDbColumnName = 'monPAmount' THEN 'COALESCE((SELECT varCurrSymbol FROM Currency WHERE numCurrencyID = Opp.numCurrencyID),'''') || '' '' || CAST(CAST(monPAmount AS DECIMAL(20,5)) AS VARCHAR)'
               WHEN v_vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
               WHEN v_vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
               WHEN v_vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
               WHEN v_vcDbColumnName = 'vcPOppName' THEN 'CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'vcBizDocsList' THEN 'COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''#^#'',vcBizDocID),''$^$'') 
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)),'''')'
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')
               END) || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
            
            v_strColumns := coalesce(v_strColumns,'') || ',' ||  coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Label'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',' || CASE
            WHEN v_vcDbColumnName = 'vcTrackingDetail' THEN 'COALESCE((SELECT 
																			string_agg(CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail),'', '')
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND COALESCE(ShippingReport.vcTrackingDetail,'''') <> ''''), '''')'
            WHEN v_vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
            WHEN v_vcDbColumnName = 'vcInventoryStatus' THEN 'COALESCE(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT),'''')'
            WHEN v_vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped::SMALLINT,',v_tintDecimalPoints,
               '::SMALLINT)')
            WHEN v_vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped::SMALLINT,',v_tintDecimalPoints,
               '::SMALLINT)')
            WHEN v_vcDbColumnName = 'vcShipStreet' THEN 'fn_getOPPState(Opp.numOppId,Opp.numDomainID,2::SMALLINT)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') END || ' "' || coalesce(v_vcColumnName,'') || '"';
            IF v_vcDbColumnName = 'vcOrderedShipped' then
               v_temp := '
				COALESCE((SELECT string_agg(CONCAT(numOppBizDocsId,''~'',COALESCE(numShippingReportId,0)),'', '')
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=true),'''')';
               v_strColumns := coalesce(v_strColumns,'') || ' , ' || coalesce(v_temp,'') || ' AS ShippingIcons ';
               v_strColumns := coalesce(v_strColumns,'') || ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LENGTH(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END "ISTrackingNumGenerated"';
               v_strColumns := coalesce(v_strColumns,'') || ' ,fn_GetListItemName(intUsedShippingCompany) AS ShipVia ';
               v_strColumns := coalesce(v_strColumns,'') || ' ,Opp.intUsedShippingCompany AS intUsedShippingCompany ';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''intUsedShippingCompany'' AND numModuleID=3) AS ShipViaFieldId ';
               v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''numShippingService'' AND numModuleID=3) AS ShippingServiceFieldId ';
               v_strColumns := coalesce(v_strColumns,'') || ' ,COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR COALESCE(numDomainID,0)=0) AND numShipmentServiceID = COALESCE(Opp.numShippingService,0)),'''')' || ' AS vcShipmentService';
            end if;
            IF LENGTH(v_SearchText) > 0 then
				
               v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) ||(CASE
               WHEN v_vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
               WHEN v_vcDbColumnName = 'vcInventoryStatus' THEN 'COALESCE(CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT),'''')'
               WHEN v_vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped::SMALLINT,',v_tintDecimalPoints,
                  '::SMALLINT)')
               WHEN v_vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped::SMALLINT,',v_tintDecimalPoints,
                  '::SMALLINT)')
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
               WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
               else coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') END) || ' iLIKE ''%' || coalesce(v_SearchText,'') || '%''';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
         then
			
            v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT string_agg(CONCAT(A.vcFirstName,'' '',A.vcLastName,CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('',fn_GetListItemName(SR.numContactType),'')'' ELSE '''' END),'','')
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID),'''') "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSE
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value  "'
            || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',case when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,''0'')=''0'' then 0 when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,''0'')=''1'' then 1 end   "'
            || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
            
            v_strColumns := coalesce(v_strColumns,'')
            || ',(CASE 
					WHEN is_date(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)|| '.Fld_Value) 
					THEN FormatedDateFromDate(CFW'
							|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
							|| '.Fld_Value::TIMESTAMP,'
							|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
							|| ')
					ELSE '''' 
				END)  "'
            || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid    ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
            
            v_vcDbColumnName := 'DCust'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strColumns := coalesce(v_strColumns,'')
            || ',L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.vcData'
            || ' "'
            || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Opp CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=Opp.numOppid ';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ListDetails L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=(CASE WHEN isnumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value ELSE ''0'' END)::NUMERIC';
         ELSE
            v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValueOpp(',v_numFieldId,',opp.numOppID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
         IF LENGTH(v_SearchText) > 0 then
			
            v_SearchQuery := coalesce(v_SearchQuery,'') ||(CASE WHEN LENGTH(v_SearchQuery) > 0 THEN ' OR ' ELSE '' END) || CONCAT('GetCustFldValueOpp(',v_numFieldId,',opp.numOppID) iLIKE ''%',v_SearchText,
            '%''');
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM
      tt_TEMPFORM WHERE
      tintOrder > v_tintOrder -1   ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP; 

	

   v_strShareRedordWith := ' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=3 AND SR.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';

   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,'::VARCHAR AND EA.numDivisionID=Div.numDivisionID))');

   v_StrSql := coalesce(v_StrSql,'') || ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId   
								 LEFT JOIN LATERAL (SELECT OIInner.numOppID,COUNT(*) numApprovalCount FROM OpportunityItems OIInner WHERE OIInner.numOppID=Opp.numOppID AND COALESCE(OIInner.bitItemPriceApprovalRequired,false)=true GROUP BY OIInner.numOppID) TEMPApproval ON TRUE
								 LEFT JOIN LATERAL (SELECT OIInner.numOppID,COUNT(*) numMappingCount FROM OpportunityItems OIInner WHERE OIInner.numOppID=Opp.numOppID AND COALESCE(OIInner.bitMappingRequired,false)=true GROUP BY OIInner.numOppID) TEMPMapping ON TRUE                                                        
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' || coalesce(v_WhereCondition,'');
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   INSERT INTO
   tt_TEMPCOLORSCHEME
   SELECT
   DFM.vcOrigDbColumnName,
		CAST(DFM.vcLookBackTableName AS VARCHAR(50)),
		CAST(DFCS.vcFieldValue AS VARCHAR(50)),
		CAST(DFCS.vcFieldValue1 AS VARCHAR(50)),
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType
   FROM
   DycFieldColorScheme DFCS
   JOIN
   DycFormField_Mapping DFFM
   ON
   DFCS.numFieldID = DFFM.numFieldID
   JOIN
   DycFieldMaster DFM
   ON
   DFM.numModuleID = DFFM.numModuleID and DFM.numFieldId = DFFM.numFieldID
   WHERE
   DFCS.numDomainID = v_numDomainID
   AND DFFM.numFormID = v_numFormId
   AND DFCS.numFormID = v_numFormId
   AND coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then
	
      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;                        

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      v_strColumns := coalesce(v_strColumns,'') || ',tCS.vcColorScheme';
   end if;

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
	
      IF v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      IF v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'Div.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      if v_vcCSLookBackTableName = 'OpportunityMaster' then
         v_Prefix := 'Opp.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
		
         v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN tt_TEMPCOLORSCHEME tCS ON CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now()) + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)
				 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now()) + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
		
         v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS VARCHAR)';
      end if;
   end if;
                          
   IF v_columnName ilike 'CFW.Cust%' then
      v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','') || ' ';
      select   Fld_type INTO v_vcSortFieldType FROM CFW_Fld_Master WHERE Fld_id = CAST(REPLACE(v_columnName,'CFW.Cust','') AS INTEGER);
      IF coalesce(v_vcSortFieldType,'') = 'DateField' then
		
         v_columnName := '(CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END)';
      ELSEIF CAST(REPLACE(v_columnName,'CFW.Cust','') AS INTEGER) IN(12745,12846)
      then
		
         v_columnName := '(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)';
      ELSE
         v_columnName := 'CFW.Fld_Value';
      end if;
   end if; 

   IF v_bitPartner = true then
      v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=true AND OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;
                    
                
   IF v_tintFilterBy = 1 then --Partially Fulfilled Orders 
      v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppId 
                             WHERE Opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || '=2)';
   ELSE
      v_StrSql := coalesce(v_StrSql,'') || ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15)  || ' OR ' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || '=2)';
   end if;
  
  
   IF v_intOrder <> 0 then
    
      IF v_intOrder = 1 then
         v_StrSql := coalesce(v_StrSql,'') || ' and Opp.bitOrder = false ';
      end if;
      IF v_intOrder = 2 then
         v_StrSql := coalesce(v_StrSql,'') || ' and Opp.bitOrder = true';
      end if;
   end if;
    
   IF v_numCompanyID <> 0 then
      v_StrSql := coalesce(v_StrSql,'') || ' And Div.numCompanyID =' || SUBSTR(CAST(v_numCompanyID AS VARCHAR(15)),1,15);
   end if;

   IF v_SortChar <> '0' then
      v_StrSql := coalesce(v_StrSql,'') || ' And Opp.vcPOppName iLIKE ''' || coalesce(v_SortChar,'') || '%''';
   end if;
  

   IF v_intType = 3 then
	
      IF v_tintSalesUserRightType = 0 then
         v_StrSql := coalesce(v_StrSql,'') || ' AND opp.tintOppType=0';
      ELSEIF v_tintSalesUserRightType = 1
      then
         v_StrSql := CONCAT(v_StrSql,' AND (Opp.numRecOwner= ',v_numUserCntID,' or Opp.numAssignedTo=',
         v_numUserCntID,(CASE WHEN v_bitPartner = true THEN ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' ELSE '' END),' or ',v_strShareRedordWith,
         ' or ',v_strExternalUser,')');
      ELSEIF v_tintSalesUserRightType = 2
      then
         v_StrSql := coalesce(v_StrSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) 
									 or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
         || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
   ELSEIF v_intType = 4
   then
	
      IF v_tintPurchaseUserRightType = 0 then
         v_StrSql := coalesce(v_StrSql,'') || ' AND opp.tintOppType=0';
      ELSEIF v_tintPurchaseUserRightType = 1
      then
         v_StrSql := CONCAT(v_StrSql,' AND (Opp.numRecOwner= ',v_numUserCntID,' or Opp.numAssignedTo= ',
         v_numUserCntID,CASE WHEN v_bitPartner = true THEN ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' ELSE '' END,' or ' || coalesce(v_strShareRedordWith,''),
         ' or ',v_strExternalUser,')');
      ELSEIF v_tintPurchaseUserRightType = 2
      then
         v_StrSql := coalesce(v_StrSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) 
									 or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
         || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
   end if;

	
					
   IF v_tintSortOrder <> cast(NULLIF('0','') as SMALLINT) then
      v_StrSql := coalesce(v_StrSql,'') || '  AND Opp.tintOppType= ' || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1);
   end if;
    
    
   IF v_tintFilterBy = 1 then --Partially Fulfilled Orders
      v_StrSql := coalesce(v_StrSql,'') || ' AND OB.bitPartialFulfilment = true AND OB.bitAuthoritativeBizDocs = 1 ';
   ELSEIF v_tintFilterBy = 2
   then --Fulfilled Orders
      v_StrSql := coalesce(v_StrSql,'') || ' AND Opp.numOppId IN (SELECT OM.numOppId FROM OpportunityMaster OM
                      			INNER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppId
                      			INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
                      			INNER JOIN OpportunityBizDocItems BI ON BI.numOppBizDocID = OB.numOppBizDocsId
                      			Where OM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || 'AND OB.bitAuthoritativeBizDocs = 1 GROUP BY OM.numOppId
                      			HAVING   COUNT(OI.numUnitHour) = COUNT(BI.numUnitHour)) ';
   ELSEIF v_tintFilterBy = 3
   then --Orders without Authoritative-BizDocs
      v_StrSql := coalesce(v_StrSql,'') || ' AND Opp.numOppId not IN (SELECT DISTINCT OM.numOppId
                      		  FROM OpportunityMaster OM JOIN OpportunityBizDocs OB ON OM.numOppId = OB.numOppId
                      		  Where OM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_tintShipped AS VARCHAR(15)),1,15) || '=2)' || ' AND OM.tintOppType= ' || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1) || ' AND COALESCE(OB.bitAuthoritativeBizDocs,0)=1) ';
   ELSEIF v_tintFilterBy = 4
   then --Orders with items yet to be added to Invoices
      v_StrSql := coalesce(v_StrSql,'') || ' AND Opp.numOppId IN (SELECT distinct OPPM.numOppId FROM OpportunityMaster OPPM
                                  INNER JOIN OpportunityItems OPPI ON OPPM.numOppId = OPPI.numOppId
                                  Where OPPM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems BI
                      			  ON BI.numOppBizDocID = OB.numOppBizDocsId where OB.numOppId = OPPM.numOppId))';
   ELSEIF v_tintFilterBy = 6
   then --Orders with deferred Income/Invoices
      v_StrSql := coalesce(v_StrSql,'') || ' AND EXISTS (SELECT OM.numOppId
                      		 FROM OpportunityMaster OM LEFT OUTER JOIN OpportunityBizDocs OB ON OM.numOppId = OB.numOppId
                      		 Where OM.numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND OM.numOppId=Opp.numOppId AND OB.bitAuthoritativeBizDocs =1 and OB.tintDeferred=1)';
   end if;

   IF v_numOrderStatus <> 0 then
      v_StrSql := coalesce(v_StrSql,'') || ' AND Opp.numStatus = ' || SUBSTR(CAST(v_numOrderStatus AS VARCHAR(15)),1,15);
   end if;

   IF POSITION('OI.vcInventoryStatus' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('OI.vcInventoryStatus=1' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' position(''Not Applicable'' in CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT)) > 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=2' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' position(''Shipped'' in CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT)) > 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=3' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' position(''BO'' in CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT)) > 0 AND position(''Shippable'' in CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT)) = 0 ');
      end if;
      IF POSITION('OI.vcInventoryStatus=4' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' position(''Shippable'' in CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1::SMALLINT)) > 0 ');
      end if;
   end if;
	
   IF POSITION('Opp.tintInvoicing' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('Opp.tintInvoicing=0' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ');
      end if;
      IF POSITION('Opp.tintInvoicing=1' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(COALESCE(InvoicedQty,0)) = SUM(COALESCE(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, COALESCE(OI.numUnitHour,0) AS OrderedQty, COALESCE(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		LEFT JOIN LATERAL
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice ON TRUE
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ');
      end if;
      IF POSITION('Opp.tintInvoicing=2' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ');
      end if;
      IF POSITION('Opp.tintInvoicing=3' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(COALESCE(InvoicedQty,0)) > 0 THEN SUM(COALESCE(OrderedQty,0)) - SUM(COALESCE(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, COALESCE(OI.numUnitHour,0) AS OrderedQty, COALESCE(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		LEFT JOIN LATERAL
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice ON TRUE
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ');
      end if;
      IF POSITION('Opp.tintInvoicing=4' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(COALESCE(InvoicedQty,0)) > 0 THEN SUM(COALESCE(OrderedQty,0)) - SUM(COALESCE(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, COALESCE(OI.numUnitHour,0) AS OrderedQty, COALESCE(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		LEFT JOIN LATERAL
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice ON TRUE
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ');
      end if;
      IF POSITION('Opp.tintInvoicing=5' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				EXISTS (SELECT DISTINCT OM.numOppId
                      		 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OB ON OM.numOppId = OB.numOppId
                      		 Where OM.numDomainId = Opp.numDomainID AND OM.numOppId=Opp.numOppId AND OB.bitAuthoritativeBizDocs = 1 and OB.tintDeferred=1) ');
      end if;
   end if;
	
   IF POSITION('Opp.tintEDIStatus' IN v_vcRegularSearchCriteria) > 0 then
	
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
      IF POSITION('Opp.tintEDIStatus=2' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=3' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=4' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=5' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=6' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=7' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=7' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.tintEDIStatus=7' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ');
      end if;
   end if;

   IF POSITION('Opp.vcSignatureType' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('Opp.vcSignatureType=0' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.vcSignatureType=1' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.vcSignatureType=2' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.vcSignatureType=3' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('Opp.vcSignatureType=4' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ');
      end if;
   end if;

   IF POSITION('OBD.monAmountPaid' IN v_vcRegularSearchCriteria) > 0 then
	
      IF POSITION('OBD.monAmountPaid ilike ''%0%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid ilike ''%0%''',' 1 = (SELECT COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)) ');
      end if;
      IF POSITION('OBD.monAmountPaid ilike ''%1%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid ilike ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monDealAmount,0)) = SUM(COALESCE(monAmountPaid,0)) AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ');
      end if;
      IF POSITION('OBD.monAmountPaid ilike ''%2%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid ilike ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(COALESCE(monDealAmount,0)) - SUM(COALESCE(monAmountPaid,0))) > 0 AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ');
      end if;
      IF POSITION('OBD.monAmountPaid ilike ''%3%''' IN v_vcRegularSearchCriteria) > 0 then
         v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'OBD.monAmountPaid ilike ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ');
      end if;
   end if;
	
	
   IF v_vcRegularSearchCriteria <> '' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;
	
	

   IF v_vcCustomSearchCriteria <> '' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND ' ||  coalesce(v_vcCustomSearchCriteria,'');
   end if;

   IF LENGTH(v_SearchQuery) > 0 then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
   end if;

   IF coalesce(v_tintDashboardReminderType,0) = 19 then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',
      v_numDomainID,'
																	AND OMInner.numDomainId=',
      v_numDomainID,' 
																	AND OMInner.tintOppType = 1
																	AND COALESCE(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 10
   then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																LEFT JOIN LATERAL
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT COALESCE(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',
      v_numDomainID,')
																) AS TempBilled ON TRUE
																WHERE
																	OMInner.numDomainId = ',v_numDomainID,'
																	AND COALESCE(OMInner.bitStockTransfer,false) = false
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND COALESCE(OIInner.numUnitHour,0) <> COALESCE(TempBilled.BilledQty,0)',
      ') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 11
   then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																LEFT JOIN LATERAL
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND COALESCE(OpportunityBizDocs.numBizDocId,0) = ',(SELECT coalesce(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId = v_numDomainID),
      '
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled ON TRUE
																WHERE
																	OMInner.numDomainId = ',v_numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND COALESCE(OMInner.tintshipped,0) = 0
																	AND COALESCE(OIInner.bitDropShip,false) = false
																	AND COALESCE(OIInner.numUnitHour,0) <> COALESCE(TempFulFilled.PickPacked,0)',') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 12
   then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																LEFT JOIN LATERAL
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND COALESCE(OpportunityBizDocs.numBizDocId,0) = 296
																		AND COALESCE(OpportunityBizDocs.bitFulFilled,false) = true
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled ON TRUE
																WHERE
																	OMInner.numDomainId = ',
      v_numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND COALESCE(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND COALESCE(OIInner.bitDropShip,false) = false
																	AND COALESCE(OIInner.numUnitHour,0) <> COALESCE(TempFulFilled.FulFilledQty,0)',') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 13
   then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																LEFT JOIN LATERAL
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = COALESCE((SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',
      v_numDomainID,' LIMIT 1),287)
																) AS TempInvoice ON TRUE
																WHERE
																	OMInner.numDomainId = ',v_numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND COALESCE(OIInner.numUnitHour,0) <> COALESCE(TempInvoice.InvoicedQty,0)',
      ') ');
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 14
   then
      DROP TABLE IF EXISTS tt_TEMPOPPID CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPOPPID
      (
         numOppID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPOPPID(numOppID)
      SELECT DISTINCT
      OM.numOppId
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.tintOppType = 1
      AND OM.tintOppStatus = 1
      AND coalesce(OI.bitDropShip,false) = true
      AND 1 =(CASE WHEN(SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID = OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppId AND OIInner.numItemCode = OI.numItemCode AND coalesce(OIInner.numWarehouseItmsID,0) = coalesce(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END);
      If coalesce((SELECT bitReOrderPoint FROM Domain WHERE numDomainId = v_numDomainID),
      false) = true then
		
         INSERT INTO tt_TEMPOPPID(numOppID)
         SELECT DISTINCT
         OM.numOppId
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OI.numOppId = OM.numOppId
         INNER JOIN
         Item I
         ON
         I.numItemCode = OI.numItemCode
         INNER JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         WHERE
         OM.numDomainId = v_numDomainID
         AND coalesce((SELECT bitReOrderPoint FROM Domain WHERE numDomainId = v_numDomainID),
         false) = true
         AND coalesce(WI.numOnHand,0)+coalesce(WI.numOnOrder,0) <= coalesce(WI.numReorder,0)
         AND OM.tintOppType = 1
         AND OM.tintOppStatus = 1
         AND coalesce(I.bitAssembly,false) = false
         AND coalesce(I.bitKitParent,false) = false
         AND coalesce(OI.bitDropShip,false) = false
         AND OM.numOppId NOT IN(SELECT numOppID FROM tt_TEMPOPPID);
         IF(SELECT COUNT(*) FROM tt_TEMPOPPID) > 0 then
            select   CONCAT(v_tmpIds,numOppID,', ') INTO v_tmpIds FROM tt_TEMPOPPID;
            v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (',SUBSTR(v_tmpIds,0,LENGTH(v_tmpIds)),
            ') ');
         ELSE
            v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (0) ');
         end if;
      end if;
   ELSEIF coalesce(v_tintDashboardReminderType,0) = 15
   then
	
      v_StrSql := CONCAT(v_StrSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',v_numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND COALESCE(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < timezone(''utc'', now()) + make_interval(mins => ',v_ClientTimeZoneOffset::bigint*-1,',)
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				COALESCE(OIInner.numUnitHour,0) AS OrderedQty,
																				COALESCE(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			LEFT JOIN LATERAL
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND COALESCE(OpportunityBizDocs.numBizDocId,0) = 296
																					AND COALESCE(OpportunityBizDocs.bitFulFilled,false) = true
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled ON TRUE
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND COALESCE(OIInner.bitDropShip,false) = false
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ');
   end if;

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

	-- EXECUTE FINAL QUERY
   IF v_CurrentPage = -1 AND v_PageSize = -1 then
	
      v_strFinal := CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',v_strColumns,v_StrSql,' ORDER BY ',
      v_columnName,' ',v_columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',
      25,' ROWS ONLY;');
   ELSE
      v_strFinal := CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',v_strColumns,v_StrSql,' ORDER BY ',
      v_columnName,' ',v_columnSortOrder,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint,
      ' ROWS FETCH NEXT ',v_PageSize,' ROWS ONLY;');
   end if;
   RAISE NOTICE '%',CAST(v_strFinal AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_strFinal;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM WHERE vcOrigDbColumnName <> 'numShippingService' ORDER BY tintOrder;
END;
$BODY$;
