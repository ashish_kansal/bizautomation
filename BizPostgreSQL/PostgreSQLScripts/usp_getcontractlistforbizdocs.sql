-- Stored procedure definition script USP_GetContractListForBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContractListForBizDocs(v_numOppId NUMERIC(9,0) DEFAULT 0,     
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                          
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionId  NUMERIC(9,0);
BEGIN
   v_numDivisionId := 0;
   select   numDivisionId INTO v_numDivisionId From OpportunityMaster Where numOppId = v_numOppId  And v_numDomainID = numDomainId;

   open SWV_RefCur for Select cast(numContractId as VARCHAR(255)),cast(vcContractName as VARCHAR(255)) from ContractManagement c
   where
   v_numUserCntID = numUserCntID
   and
   c.numdomainId = v_numDomainID and numDivisionID = v_numDivisionId;
END; $$;












