-- Stored procedure definition script USP_SimpleSearch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SimpleSearch(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_searchText VARCHAR(100),
	v_searchType VARCHAR(1),
	v_searchCriteria VARCHAR(1),
	v_orderType VARCHAR(1),
	v_bizDocType INTEGER ,
	v_numSkip INTEGER,v_isStartWithSearch INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SalesOppRights  SMALLINT;                    
   v_PurchaseOppRights  SMALLINT;                   
   v_SalesOrderRights  SMALLINT;
   v_PurchaseOrderRights  SMALLINT;
   v_BizDocsRights  SMALLINT;           
   v_strSQL  TEXT DEFAULT '';
   v_ItemListRights  SMALLINT;
   v_strItemSearch  VARCHAR(1000) DEFAULT '';
   v_strAttributeSearch  VARCHAR(1000) DEFAULT '';
   v_strSearch  VARCHAR(1000);

   v_vcCustomDisplayField  VARCHAR(4000) DEFAULT '';
   SWV_ExecDyn  VARCHAR(5000);
   v_ContactsRights  SMALLINT;
   v_ProspectsRights  SMALLINT;                    
   v_accountsRights  SMALLINT;                   
   v_leadsRights  SMALLINT;
   v_searchSQL  TEXT DEFAULT '';
BEGIN
   IF v_searchType = '1' then -- Organization
	
      IF v_searchCriteria = '1' then --Organizations
		
         PERFORM USP_CompanyInfo_Search(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_isStartWithSearch := v_isStartWithSearch,
         v_searchText := v_searchText);
      ELSE
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_ProspectsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
         AND PM.numModuleID = 3
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_accountsRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 2
         AND PM.numModuleID = 4
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_leadsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
         AND PM.numModuleID = 2
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;  

			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
         DROP TABLE IF EXISTS tt_TEMPORGSEARCH CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPORGSEARCH ON COMMIT DROP AS
            SELECT * FROM(SELECT
            numFieldID,
					vcDbColumnName,
					coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
            FROM
            View_DynamicColumns
            WHERE
            numFormId = 97 AND numUserCntID = 0 AND numDomainID = v_numDomainID AND
            tintPageType = 1 AND bitCustom = false  AND coalesce(bitSettingField,false) = true AND numRelCntType = 0
            UNION
            SELECT
            numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
            FROM
            View_DynamicCustomColumns
            WHERE
            Grp_id = 1 AND numFormId = 97 AND numUserCntID = 0 AND numDomainID = v_numDomainID AND
            tintPageType = 1 AND bitCustom = true AND numRelCntType = 0) Y;
         IF(SELECT COUNT(*) FROM tt_TEMPORGSEARCH) > 0 then
			
            if(v_isStartWithSearch = 0) then
				
               v_searchSQL := OVERLAY((SELECT ' OR ' || CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),CASE WHEN Custom = 1 THEN 'Fld_Value' ELSE vcDbColumnName END) || ' LIKE ''%' || coalesce(v_searchText,'') || '%''' FROM tt_TEMPORGSEARCH) placing '' from 1 for 4);
            else
               v_searchSQL := OVERLAY((SELECT ' OR ' || CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),CASE WHEN Custom = 1 THEN 'Fld_Value' ELSE vcDbColumnName END) || ' LIKE ' || coalesce(v_searchText,'') || '%''' FROM tt_TEMPORGSEARCH) placing '' from 1 for 4);
            end if;
         ELSE
            if(v_isStartWithSearch = 0) then
				
               v_searchSQL := 'CompanyInfo.vcCompanyName LIKE ''%' || coalesce(v_searchText,'') || '%''';
            else
               v_searchSQL := 'CompanyInfo.vcCompanyName LIKE ' || coalesce(v_searchText,'') || '%''';
            end if;
         end if;
         DROP TABLE IF EXISTS tt_TEMPORGANIZATION CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPORGANIZATION
         (
            numDivisionID NUMERIC(18,0),
            vcCompanyName VARCHAR(500)
         );
         v_strSQL := 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND (DivisionMaster.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' OR 
								1 = CASE 
									WHEN DivisionMaster.tintCRMType <> 0 and DivisionMaster.tintCRMType=1 THEN ' ||
         CASE v_ProspectsRights
         WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
         WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' THEN 1 ELSE 0 END'
         WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0) THEN 1 ELSE 0 END'
         ELSE '1'
         END || ' WHEN DivisionMaster.tintCRMType<>0 and DivisionMaster.tintCRMType=2 THEN' ||
         CASE v_accountsRights
         WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
         WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' THEN 1 ELSE 0 END'
         WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0) THEN 1 ELSE 0 END'
         ELSE '1'
         END || ' WHEN DivisionMaster.tintCRMType=0 THEN' ||
         CASE v_leadsRights
         WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
         WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' THEN 1 ELSE 0 END'
         WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or d.numTerID=0) THEN 1 ELSE 0 END'
         ELSE '1'
         END || ') AND (' || coalesce(v_searchSQL,'') || ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName';
         RAISE NOTICE '%',v_strSQL;
         EXECUTE v_strSQL;
         IF v_searchCriteria = '2' then --Items
            DROP TABLE IF EXISTS tt_TEMPORGITEM CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPORGITEM
            (
               numOppId NUMERIC(18,0),
               CreatedDate TIMESTAMP,
               vcItemName VARCHAR(500),
               vcPOppName VARCHAR(200),
               vcOppType VARCHAR(50),
               numUnitHour DOUBLE PRECISION,
               monPrice NUMERIC(18,2),
               vcCompanyName  VARCHAR(200),
               tintType SMALLINT
            );
            INSERT INTO tt_TEMPORGITEM(numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType)
            SELECT
            numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS DOUBLE PRECISION) AS numUnitHour,
					CAST(monPrice AS DECIMAL(18,2)) AS monPrice,
					vcCompanyName,
					tintType
            FROM(SELECT
               OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcpOppName,
						CASE
               WHEN tintopptype = 1 THEN 'Sales ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
               WHEN tintopptype = 2 THEN 'Purchase ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
               END AS vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
               FROM
               OpportunityMaster
               INNER JOIN
               tt_TEMPORGANIZATION TEMP
               ON
               OpportunityMaster.numDivisionId = TEMP.numDivisionID
               INNER JOIN
               OpportunityItems
               ON
               OpportunityMaster.numOppId = OpportunityItems.numOppId
               INNER JOIN
               Item
               ON
               OpportunityItems.numItemCode = Item.numItemCode
               WHERE
               OpportunityMaster.numDomainId = v_numDomainID
               UNION
               SELECT
               ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE
               WHEN tintReturnType = 1 THEN 'Sales Return'
               WHEN tintReturnType = 2 THEN 'Purchase Return'
               END AS vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
               FROM
               ReturnHeader
               INNER JOIN
               tt_TEMPORGANIZATION TEMP
               ON
               ReturnHeader.numDivisionId = TEMP.numDivisionID
               INNER JOIN
               ReturnItems
               ON
               ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
               INNER JOIN
               Item
               ON
               Item.numItemCode = ReturnItems.numItemCode
               WHERE
               ReturnHeader.numDomainID = v_numDomainID AND tintReturnType IN(1,2)) TEMPFinal;
            open SWV_RefCur for
            SELECT
            coalesce(numOppId,0) AS numOppId,
					coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
            '') AS bintCreatedDate,
					coalesce(vcItemName,'') AS vcItemName,
					coalesce(vcPOppName,'') AS vcPOppName,
					coalesce(vcOppType,'') AS vcOppType,
					coalesce(numUnitHour,0) AS numUnitHour,
					coalesce(monPrice,0.00) AS monPrice,
					coalesce(vcCompanyName,'') AS vcCompanyName,
					coalesce(tintType,0) AS tintType
            FROM
            tt_TEMPORGITEM
            ORDER BY
            CreatedDate DESC;

            open SWV_RefCur2 for
            SELECT COUNT(numOppId) AS TotalItems FROM tt_TEMPORGITEM;
         ELSEIF v_searchCriteria = '3'
         then -- Opp/Orders
			
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            DROP TABLE IF EXISTS tt_TEMPORGORDER CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPORGORDER
            (
               numOppId NUMERIC(18,0),
               CreatedDate TIMESTAMP,
               vcPOppName VARCHAR(200),
               vcOppType VARCHAR(50),
               vcCompanyName  VARCHAR(200),
               tintType SMALLINT
            );
            INSERT INTO
            tt_TEMPORGORDER
            SELECT
            numOppId,
					bintCreatedDate,
					vcPOppName,
					CAST(vcOppType AS VARCHAR(50)),
					vcCompanyName,
					tintType
            FROM(SELECT
               OpportunityMaster.numOppId,
						OpportunityMaster.vcpOppName,
						CASE
               WHEN tintopptype = 1 THEN 'Sales ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
               WHEN tintopptype = 2 THEN 'Purchase ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
               END AS vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
               FROM
               OpportunityMaster
               INNER JOIN
               tt_TEMPORGANIZATION TEMP
               ON
               OpportunityMaster.numDivisionId = TEMP.numDivisionID
               WHERE
               numDomainId = v_numDomainID
               AND tintopptype <> 0
               AND (OpportunityMaster.numassignedto = v_numUserCntID OR 1 =(CASE
               WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
               THEN
                  CASE v_SalesOppRights
                  WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
                  WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
                  ELSE 1
                  END
               WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
               THEN
                  CASE v_PurchaseOrderRights
                  WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
                  WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
                  ELSE 1
                  END
               WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
               THEN
                  CASE v_SalesOrderRights
                  WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
                  WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
                  ELSE 1
                  END
               WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
               THEN
                  CASE v_PurchaseOrderRights
                  WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
                  WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
                  ELSE 1
                  END
               ELSE 1
               END))
               UNION
               SELECT
               ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE
               WHEN tintReturnType = 1 THEN 'Sales Return'
               WHEN tintReturnType = 2 THEN 'Purchase Return'
               END AS vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
               FROM
               ReturnHeader
               INNER JOIN
               tt_TEMPORGANIZATION TEMP
               ON
               ReturnHeader.numDivisionId = TEMP.numDivisionID
               WHERE
               numDomainId = v_numDomainID AND tintReturnType IN(1,2)) TEMPFinal;
            open SWV_RefCur for
            SELECT
            coalesce(numOppId,0) AS numOppId,
					coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
            '') AS bintCreatedDate,
					coalesce(vcPOppName,'') AS vcPOppName,
					coalesce(vcOppType,'') AS vcOppType,
					coalesce(vcCompanyName,'') AS vcCompanyName,
					coalesce(tintType,0) AS tintType
            FROM
            tt_TEMPORGORDER
            ORDER BY
            CreatedDate DESC;

            open SWV_RefCur2 for
            SELECT COUNT(*) AS TotalRows FROM tt_TEMPORGORDER;
         ELSEIF v_searchCriteria = '4'
         then -- BizDocs
			
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            SELECT 
            MAX(GA.intViewAllowed) AS intViewAllowed INTO v_BizDocsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 6
            AND PM.numModuleID = 10
            AND UM.numUserId =(SELECT numUserId
               FROM   UserMaster
               WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
            DROP TABLE IF EXISTS tt_TEMPORGBIZDOC CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPORGBIZDOC
            (
               numOppBizDocsId NUMERIC(18,0),
               CreatedDate TIMESTAMP,
               vcBizDocID VARCHAR(200),
               vcBizDocType VARCHAR(100),
               vcCompanyName  VARCHAR(200)
            );
            INSERT INTO
            tt_TEMPORGBIZDOC
            SELECT
            numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					Listdetails.vcData AS vcBizDocType,
					CAST(TEMP.vcCompanyName AS VARCHAR(200))
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityMaster
            ON
            OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
            INNER JOIN
            tt_TEMPORGANIZATION TEMP
            ON
            OpportunityMaster.numDivisionId = TEMP.numDivisionID
            INNER JOIN
            Listdetails
            ON
            Listdetails.numListID = 27
            AND OpportunityBizDocs.numBizDocId = Listdetails.numListItemID
            WHERE
            OpportunityMaster.numDomainId = v_numDomainID
            AND (OpportunityMaster.numassignedto = v_numUserCntID OR 1 =(CASE
            WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
            THEN
               CASE v_SalesOppRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
            THEN
               CASE v_PurchaseOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
            THEN
               CASE v_SalesOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
            THEN
               CASE v_PurchaseOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               ELSE 1
               END
            ELSE 1
            END))
            AND 1 =(CASE v_BizDocsRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = v_numUserCntID THEN 1 ELSE 0 END)
            ELSE 1
            END);

            open SWV_RefCur for
            SELECT
            coalesce(numOppBizDocsId,0) AS numOppBizDocsId,
					coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
            '') AS dtCreatedDate,
					coalesce(vcBizDocID,'') AS vcBizDocID,
					coalesce(vcBizDocType,'') AS vcBizDocType,
					coalesce(vcCompanyName,'') AS vcCompanyName
            FROM
            tt_TEMPORGBIZDOC
            ORDER BY
            CreatedDate DESC;

            open SWV_RefCur2 for
            SELECT COUNT(*) AS TotalRows FROM tt_TEMPORGBIZDOC;
         end if;
         DROP TABLE IF EXISTS tt_TEMPORGSEARCH CASCADE;
         DROP TABLE IF EXISTS tt_TEMPORGANIZATION CASCADE;
      end if;
   ELSEIF v_searchType = '2'
   then -- Item
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_ItemListRights FROM
      UserMaster UM
      INNER JOIN
      GroupAuthorization GA
      ON
      GA.numGroupID = UM.numGroupID
      INNER JOIN
      PageMaster PM
      ON
      PM.numPageID = GA.numPageID
      AND PM.numModuleID = GA.numModuleID WHERE
      PM.numPageID = 9
      AND PM.numModuleID = 37
      AND UM.numUserId =(SELECT numUserId FROM UserMaster WHERE numUserDetailId = v_numUserCntID) GROUP BY
      UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      DROP TABLE IF EXISTS tt_TEMPSEARCHEDITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSEARCHEDITEM
      (
         numItemCode NUMERIC(18,0),
         vcItemName VARCHAR(200),
         vcPathForTImage VARCHAR(200),
         vcCompanyName VARCHAR(200),
         monListPrice NUMERIC(18,0),
         txtItemDesc TEXT,
         vcModelID VARCHAR(100),
         numBarCodeId VARCHAR(100),
         vcBarCode VARCHAR(100),
         vcSKU VARCHAR(100),
         vcWHSKU VARCHAR(100),
         vcPartNo VARCHAR(100),
         numWarehouseItemID NUMERIC(18,0),
         vcAttributes VARCHAR(500)
      );
      IF POSITION(',' IN v_searchText) > 0 then
		
         v_strItemSearch := LTRIM(RTRIM(SUBSTR(v_searchText,0,POSITION(',' IN v_searchText))));
         v_strAttributeSearch := SUBSTR(v_searchText,POSITION(',' IN v_searchText)+1,LENGTH(v_searchText));
         IF LENGTH(v_strAttributeSearch) > 0 then
			
            if(v_isStartWithSearch = 0) then
				
               v_strAttributeSearch := 'TEMPAttributes.vcAttributes LIKE ''%' || REPLACE(v_strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') || '%''';
            else
               v_strAttributeSearch := 'TEMPAttributes.vcAttributes LIKE ''%' || REPLACE(v_strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') || '%''';
            end if;
         ELSE
            v_strAttributeSearch := '';
         end if;
      ELSE
         v_strItemSearch := v_searchText;
      end if;
      DROP TABLE IF EXISTS tt_TEMPITEMCUSTOMDISPLAYFIELDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMCUSTOMDISPLAYFIELDS ON COMMIT DROP AS
         SELECT * FROM(SELECT
         numFieldId,
				vcFieldName
         FROM
         View_DynamicCustomColumns
         WHERE
         Grp_id = 5 AND numFormId = 22 AND numUserCntID = 0 AND numDomainID = v_numDomainID AND
         tintPageType = 1 AND bitCustom = true AND numRelCntType = 0) TD;
      IF(SELECT COUNT(*) FROM tt_TEMPITEMCUSTOMDISPLAYFIELDS) > 0 then
		
         v_vcCustomDisplayField := OVERLAY((SELECT ', ' ||  'ISNULL(dbo.GetCustFldValueItem(' || CAST(numFieldId AS VARCHAR(30)) || ',numItemCode),'''') AS [' || vcFieldName || ']' FROM tt_TEMPITEMCUSTOMDISPLAYFIELDS) placing '' from 1 for 0);
      end if;
      DROP TABLE IF EXISTS tt_TEMPITEMSEARCHFIELDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMSEARCHFIELDS ON COMMIT DROP AS
         SELECT * FROM(SELECT
         numFieldID,
				vcDbColumnName,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 22 AND numUserCntID = 0 AND numDomainID = v_numDomainID AND
         tintPageType = 1 AND bitCustom = false  AND coalesce(bitSettingField,false) = true AND numRelCntType = 1
         AND vcDbColumnName <> 'vcAttributes'
         UNION
         SELECT
         numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
         FROM
         View_DynamicCustomColumns
         WHERE
         Grp_id = 5 AND numFormId = 22 AND numUserCntID = 0 AND numDomainID = v_numDomainID AND
         tintPageType = 1 AND bitCustom = true AND numRelCntType = 1) Y;
      IF(SELECT COUNT(*) FROM tt_TEMPITEMSEARCHFIELDS) > 0 then
		
         if(v_isStartWithSearch = 0) then
				
            v_strSearch := OVERLAY((SELECT ' OR ' ||  CASE WHEN Custom = 1 THEN 'dbo.GetCustFldValueItem(' || CAST(numFieldId AS VARCHAR(30)) || ',numItemCode)' ELSE vcDbColumnName END || ' LIKE ''%' || coalesce(v_strItemSearch,'') || '%''' FROM tt_TEMPITEMSEARCHFIELDS) placing '' from 1 for 3);
         else
            v_strSearch := OVERLAY((SELECT ' OR ' ||  CASE WHEN Custom = 1 THEN 'dbo.GetCustFldValueItem(' || CAST(numFieldId AS VARCHAR(30)) || ',numItemCode)' ELSE vcDbColumnName END || ' LIKE ''' || coalesce(v_strItemSearch,'') || '%''' FROM tt_TEMPITEMSEARCHFIELDS) placing '' from 1 for 3);
         end if;
      ELSE
         if(v_isStartWithSearch = 0) then
			
            v_strSearch := 'vcItemName LIKE ''%' || coalesce(v_strItemSearch,'') || '%''';
         else
            v_strSearch := 'vcItemName LIKE ''' || coalesce(v_strItemSearch,'') || '%''';
         end if;
      end if;
      v_strSQL := 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,false) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' AND 1=' ||
      CASE v_ItemListRights
      WHEN 0 THEN ' CASE WHEN Item.numCreatedBy=0 THEN 1 ELSE 0 END'
      WHEN 1 THEN ' CASE WHEN Item.numCreatedBy= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' THEN 1 ELSE 0 END'
      ELSE '1'
      END || '
						)V WHERE ' || coalesce(v_strSearch,'')  || '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo';
      IF LENGTH(v_strAttributeSearch) > 0 then
		
         v_strSQL := 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' || coalesce(v_strSQL,'') || ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' || coalesce(v_strAttributeSearch,'');
      end if;
      SWV_ExecDyn := 'INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' || coalesce(v_strSQL,'') || ')TEMPFinal';
      EXECUTE SWV_ExecDyn;
      IF v_searchCriteria = '1' then --Items
		
         v_strSQL := 'SELECT * ' || coalesce(v_vcCustomDisplayField,'') || ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' || SUBSTR(CAST(v_numSkip AS VARCHAR(100)),1,100) || ' ROWS FETCH NEXT 10 ROWS ONLY';
         OPEN SWV_RefCur FOR EXECUTE v_strSQL;

         open SWV_RefCur2 for
         SELECT COUNT(*) AS TotalRows FROM tt_TEMPSEARCHEDITEM;
      ELSEIF v_searchCriteria = '3'
      then -- Opp/Orders
		
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         DROP TABLE IF EXISTS tt_TEMPITEMORDER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEMORDER
         (
            numOppId NUMERIC(18,0),
            CreatedDate TIMESTAMP,
            vcPOppName VARCHAR(200),
            vcOppType VARCHAR(50),
            vcCompanyName  VARCHAR(200),
            tintType SMALLINT
         );
         INSERT INTO
         tt_TEMPITEMORDER
         SELECT
         numOppId,
				bintCreatedDate,
				vcPOppName,
				CAST(vcOppType AS VARCHAR(50)),
				vcCompanyName,
				tintType
         FROM(SELECT
            OpportunityMaster.numOppId,
						OpportunityMaster.vcpOppName,
						CASE
            WHEN tintopptype = 1 THEN 'Sales ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
            WHEN tintopptype = 2 THEN 'Purchase ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
            END AS vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
            FROM
            OpportunityMaster
            INNER JOIN(SELECT
               numOppId
               FROM
               OpportunityItems OI
               INNER JOIN
               tt_TEMPSEARCHEDITEM
               ON
               OI.numItemCode = tt_TEMPSEARCHEDITEM.numItemCode
               AND (OI.numWarehouseItmsID = tt_TEMPSEARCHEDITEM.numWarehouseItemID OR coalesce(tt_TEMPSEARCHEDITEM.numWarehouseItemID,0) = 0)
               GROUP BY
               numOppId) TempOppItems
            ON
            OpportunityMaster.numOppId = TempOppItems.numOppId
            INNER JOIN
            DivisionMaster
            ON
            OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
            INNER JOIN
            CompanyInfo
            ON
            DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
            WHERE
            OpportunityMaster.numDomainId = v_numDomainID
            AND tintopptype <> 0
            AND 1 =(CASE v_orderType
            WHEN 0 THEN 1
            WHEN 1 THEN(CASE WHEN (tintopptype = 1 AND tintoppstatus = 1) THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (tintopptype = 2 AND tintoppstatus = 1) THEN 1 ELSE 0 END)
            WHEN 3 THEN(CASE WHEN (tintopptype = 1 AND coalesce(tintoppstatus,0) = 0) THEN 1 ELSE 0 END)
            WHEN 4 THEN(CASE WHEN (tintopptype = 2 AND coalesce(tintoppstatus,0) = 0) THEN 1 ELSE 0 END)
            WHEN 5 THEN 0
            WHEN 6 THEN 0
            END)
            AND (OpportunityMaster.numassignedto = v_numUserCntID AND 1 =(CASE
            WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
            THEN
               CASE v_SalesOppRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
            THEN
               CASE v_PurchaseOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
            THEN
               CASE v_SalesOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
               ELSE 1
               END
            WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
            THEN
               CASE v_PurchaseOrderRights
               WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
               WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
               WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
               ELSE 1
               END
            ELSE 1
            END))
            UNION
            SELECT
            ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE
            WHEN tintReturnType = 1 THEN 'Sales Return'
            WHEN tintReturnType = 2 THEN 'Purchase Return'
            END AS vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
            FROM
            ReturnHeader
            INNER JOIN(SELECT
               numReturnHeaderID
               FROM
               ReturnItems RI
               INNER JOIN
               tt_TEMPSEARCHEDITEM
               ON
               RI.numItemCode = tt_TEMPSEARCHEDITEM.numItemCode
               AND (RI.numWareHouseItemID = tt_TEMPSEARCHEDITEM.numWarehouseItemID OR coalesce(tt_TEMPSEARCHEDITEM.numWarehouseItemID,0) = 0)
               GROUP BY
               numReturnHeaderID) TempReturnItems
            ON
            ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
            INNER JOIN
            DivisionMaster
            ON
            ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
            INNER JOIN
            CompanyInfo
            ON
            DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
            WHERE
            ReturnHeader.numDomainID = v_numDomainID
            AND 1 =(CASE v_orderType
            WHEN 0 THEN(CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
            WHEN 1 THEN 0
            WHEN 2 THEN 0
            WHEN 3 THEN 0
            WHEN 4 THEN 0
            WHEN 5 THEN(CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
            WHEN 6 THEN(CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
            END)) TEMPFinal;
         open SWV_RefCur for
         SELECT
         coalesce(numOppId,0) AS numOppId,
				coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
         '') AS bintCreatedDate,
				coalesce(vcPOppName,'') AS vcPOppName,
				coalesce(vcOppType,'') AS vcOppType,
				coalesce(vcCompanyName,'') AS vcCompanyName,
				coalesce(tintType,0) AS tintType
         FROM
         tt_TEMPITEMORDER
         ORDER BY
         CreatedDate DESC;

         open SWV_RefCur2 for
         SELECT COUNT(*) AS TotalRows FROM tt_TEMPITEMORDER;
      ELSEIF v_searchCriteria = '4'
      then -- BizDocs
		
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         SELECT 
         MAX(GA.intViewAllowed) AS intViewAllowed INTO v_BizDocsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 6
         AND PM.numModuleID = 10
         AND UM.numUserId =(SELECT numUserId
            FROM   UserMaster
            WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
         DROP TABLE IF EXISTS tt_TEMPITEMBIZDOC CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPITEMBIZDOC
         (
            numOppBizDocsId NUMERIC(18,0),
            CreatedDate TIMESTAMP,
            vcBizDocID VARCHAR(200),
            vcBizDocType VARCHAR(100),
            vcCompanyName  VARCHAR(200)
         );
         INSERT INTO
         tt_TEMPITEMBIZDOC
         SELECT
         numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				Listdetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
         FROM
         OpportunityBizDocs
         INNER JOIN(SELECT
            numOppBizDocID
            FROM
            OpportunityBizDocItems OBI
            INNER JOIN
            tt_TEMPSEARCHEDITEM
            ON
            OBI.numItemCode = tt_TEMPSEARCHEDITEM.numItemCode
            AND (OBI.numWarehouseItmsID = tt_TEMPSEARCHEDITEM.numWarehouseItemID OR coalesce(tt_TEMPSEARCHEDITEM.numWarehouseItemID,0) = 0)
            GROUP BY
            numOppBizDocID) TempOppBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
         INNER JOIN
         OpportunityMaster
         ON
         OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
         INNER JOIN
         DivisionMaster
         ON
         OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
         INNER JOIN
         CompanyInfo
         ON
         DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         INNER JOIN
         Listdetails
         ON
         Listdetails.numListID = 27
         AND OpportunityBizDocs.numBizDocId = Listdetails.numListItemID
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND (OpportunityBizDocs.numBizDocId = v_bizDocType OR coalesce(v_bizDocType,0) = 0)
         AND (OpportunityMaster.numassignedto = v_numUserCntID OR 1 =(CASE
         WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
         THEN
            CASE v_SalesOppRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
         THEN
            CASE v_PurchaseOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
         THEN
            CASE v_SalesOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
         THEN
            CASE v_PurchaseOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         ELSE 1
         END))
         AND 1 =(CASE v_BizDocsRights
         WHEN 0 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
         WHEN 1 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = v_numUserCntID THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
         ELSE 1
         END);
         open SWV_RefCur for
         SELECT
         coalesce(numOppBizDocsId,0) AS numOppBizDocsId,
				coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
         '') AS dtCreatedDate,
				coalesce(vcBizDocID,'') AS vcBizDocID,
				coalesce(vcBizDocType,'') AS vcBizDocType,
				coalesce(vcCompanyName,'') AS vcCompanyName
         FROM
         tt_TEMPITEMBIZDOC
         ORDER BY
         CreatedDate DESC;

         open SWV_RefCur2 for
         SELECT COUNT(*) AS TotalRows FROM tt_TEMPITEMBIZDOC;
      end if;
      DROP TABLE IF EXISTS tt_TEMPITEMSEARCHFIELDS CASCADE;
      DROP TABLE IF EXISTS tt_TEMPITEMCUSTOMDISPLAYFIELDS CASCADE;
   ELSEIF v_searchType = '3'
   then -- Opps/Orders
	
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      RAISE NOTICE '%',v_SalesOppRights;
      RAISE NOTICE '%',v_PurchaseOppRights;
      RAISE NOTICE '%',v_SalesOrderRights;
      RAISE NOTICE '%',v_PurchaseOrderRights;
      DROP TABLE IF EXISTS tt_TEMPORDER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPORDER
      (
         numOppId NUMERIC(18,0),
         CreatedDate TIMESTAMP,
         vcPOppName VARCHAR(200),
         vcOppType VARCHAR(50),
         vcCompanyName  VARCHAR(200),
         tintType SMALLINT
      );
      INSERT INTO
      tt_TEMPORDER
      SELECT
      numOppId,
			bintCreatedDate,
			vcPOppName,
			CAST(vcOppType AS VARCHAR(50)),
			vcCompanyName,
			tintType
      FROM(SELECT
         OpportunityMaster.numOppId,
					OpportunityMaster.vcpOppName,
					CASE
         WHEN tintopptype = 1 THEN 'Sales ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
         WHEN tintopptype = 2 THEN 'Purchase ' ||(CASE WHEN tintoppstatus = 1 THEN 'Order' ELSE 'Opportunity' END)
         END AS vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
         FROM
         OpportunityMaster
         INNER JOIN
         DivisionMaster
         ON
         OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
         INNER JOIN
         CompanyInfo
         ON
         DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         WHERE
         OpportunityMaster.numDomainId = v_numDomainID
         AND tintopptype <> 0
         AND (vcPOppName ilike CASE WHEN v_isStartWithSearch = 0 THEN N'%' || coalesce(v_searchText,'') || '%' ELSE N'' || coalesce(v_searchText,'') || '%' END OR numOppId ilike CASE WHEN v_isStartWithSearch = 0 THEN N'%' || coalesce(v_searchText,'') || '%' ELSE N'' || coalesce(v_searchText,'') || '%' END)
         AND 1 =(CASE v_orderType
         WHEN 0 THEN 1
         WHEN 1 THEN(CASE WHEN (tintopptype = 1 AND tintoppstatus = 1) THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (tintopptype = 2 AND tintoppstatus = 1) THEN 1 ELSE 0 END)
         WHEN 3 THEN(CASE WHEN (tintopptype = 1 AND coalesce(tintoppstatus,0) = 0) THEN 1 ELSE 0 END)
         WHEN 4 THEN(CASE WHEN (tintopptype = 2 AND coalesce(tintoppstatus,0) = 0) THEN 1 ELSE 0 END)
         WHEN 5 THEN 0
         WHEN 6 THEN 0
         END)
         AND (OpportunityMaster.numassignedto = v_numUserCntID OR 1 =(CASE
         WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
         THEN
            CASE v_SalesOppRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
         THEN
            CASE v_PurchaseOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
         THEN
            CASE v_SalesOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
         THEN
            CASE v_PurchaseOrderRights
            WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
            WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
            ELSE 1
            END
         ELSE 1
         END))
         UNION
         SELECT
         ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE
         WHEN tintReturnType = 1 THEN 'Sales Return'
         WHEN tintReturnType = 2 THEN 'Purchase Return'
         END AS vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
         FROM
         ReturnHeader
         INNER JOIN
         DivisionMaster
         ON
         ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
         INNER JOIN
         CompanyInfo
         ON
         DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         WHERE
         ReturnHeader.numDomainID = v_numDomainID
         AND (vcRMA ilike CASE WHEN v_isStartWithSearch = 0 THEN N'%' || coalesce(v_searchText,'') || '%' ELSE N'' || coalesce(v_searchText,'') || '%' END OR ReturnHeader.numReturnHeaderID ilike  CASE WHEN v_isStartWithSearch = 0 THEN N'%' || coalesce(v_searchText,'') || '%' ELSE N'' || coalesce(v_searchText,'') || '%' END)
         AND 1 =(CASE v_orderType
         WHEN 0 THEN(CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
         WHEN 1 THEN 0
         WHEN 2 THEN 0
         WHEN 3 THEN 0
         WHEN 4 THEN 0
         WHEN 5 THEN(CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
         WHEN 6 THEN(CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
         END)) TEMPFinal;
      open SWV_RefCur for
      SELECT
      coalesce(numOppId,0) AS numOppId,
			coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
      '') AS bintCreatedDate,
			coalesce(vcPOppName,'') AS vcPOppName,
			coalesce(vcOppType,'') AS vcOppType,
			coalesce(vcCompanyName,'') AS vcCompanyName,
			coalesce(tintType,0) AS tintType
      FROM
      tt_TEMPORDER
      ORDER BY
      CreatedDate DESC;

      open SWV_RefCur2 for
      SELECT COUNT(*) AS TotalRows FROM tt_TEMPORDER;
   ELSEIF v_searchType = '4'
   then -- BizDocs
	
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOppRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOppRights FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE PM.numPageID = 8
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_SalesOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 10
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_PurchaseOrderRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 11
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_BizDocsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 6
      AND PM.numModuleID = 10
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      DROP TABLE IF EXISTS tt_TEMPBIZDOC CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPBIZDOC
      (
         numOppBizDocsId NUMERIC(18,0),
         CreatedDate TIMESTAMP,
         vcBizDocID VARCHAR(200),
         vcBizDocType VARCHAR(100),
         vcCompanyName  VARCHAR(200)
      );
      INSERT INTO
      tt_TEMPBIZDOC
      SELECT
      numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			Listdetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityMaster
      ON
      OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      Listdetails
      ON
      Listdetails.numListID = 27
      AND OpportunityBizDocs.numBizDocId = Listdetails.numListItemID
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND vcBizDocID ilike CASE WHEN v_isStartWithSearch = 0 THEN N'%' || coalesce(v_searchText,'') || '%' ELSE N'' || coalesce(v_searchText,'') || '%' END
      AND (OpportunityBizDocs.numBizDocId = v_bizDocType OR coalesce(v_bizDocType,0) = 0)
      AND (OpportunityMaster.numassignedto = v_numUserCntID OR 1 =(CASE
      WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 0
      THEN
         CASE v_SalesOppRights
         WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
         WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
         ELSE 1
         END
      WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 0
      THEN
         CASE v_PurchaseOrderRights
         WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
         WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
         ELSE 1
         END
      WHEN tintopptype = 1 AND coalesce(tintoppstatus,0) = 1
      THEN
         CASE v_SalesOrderRights
         WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
         WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
         ELSE 1
         END
      WHEN tintopptype = 2 AND coalesce(tintoppstatus,0) = 1
      THEN
         CASE v_PurchaseOrderRights
         WHEN 0 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = 0 THEN 1 ELSE 0 END)
         WHEN 1 THEN(CASE WHEN coalesce(OpportunityMaster.numrecowner,0) = v_numUserCntID THEN 1 ELSE 0 END)
         WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
         ELSE 1
         END
      ELSE 1
      END))
      AND 1 =(CASE v_BizDocsRights
      WHEN 0 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
      WHEN 1 THEN(CASE WHEN coalesce(OpportunityBizDocs.numCreatedBy,0) = v_numUserCntID THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN (DivisionMaster.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DivisionMaster.numTerID = 0) THEN 1 ELSE 0 END)
      ELSE 1
      END);
      open SWV_RefCur for
      SELECT
      coalesce(numOppBizDocsId,0) AS numOppBizDocsId,
			coalesce(FormatedDateFromDate(CreatedDate::VARCHAR(50),v_numDomainID),
      '') AS dtCreatedDate,
			coalesce(vcBizDocID,'') AS vcBizDocID,
			coalesce(vcBizDocType,'') AS vcBizDocType,
			coalesce(vcCompanyName,'') AS vcCompanyName
      FROM
      tt_TEMPBIZDOC
      ORDER BY
      CreatedDate DESC;

      open SWV_RefCur2 for
      SELECT COUNT(*) AS TotalRows FROM tt_TEMPBIZDOC;
   ELSEIF v_searchType = '5'
   then -- Contact
      SELECT 
      MAX(GA.intViewAllowed) AS intViewAllowed INTO v_ContactsRights FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID WHERE  PM.numPageID = 2
      AND PM.numModuleID = 11
      AND UM.numUserId =(SELECT numUserId
         FROM   UserMaster
         WHERE  numUserDetailId = v_numUserCntID) GROUP BY UM.numUserId,UM.vcUserName,PM.vcFileName,GA.numModuleID,GA.numPageID    LIMIT 1;
      DROP TABLE IF EXISTS tt_TABLECONTACT CASCADE;
      CREATE TEMPORARY TABLE tt_TABLECONTACT
      (
         numContactId NUMERIC(18,0),
         vcFirstName VARCHAR(100),
         vcLastname VARCHAR(100),
         vcFullName VARCHAR(200),
         vcEmail VARCHAR(100),
         numPhone VARCHAR(100),
         numPhoneExtension VARCHAR(100),
         vcCompanyName VARCHAR(200),
         bitPrimaryContact VARCHAR(5)
      );
      INSERT INTO
      tt_TABLECONTACT
      SELECT
      ADC.numContactId,
			coalesce(vcFirstName,'') AS vcFirstName,
			coalesce(vcLastname,'') AS vcLastName,
			CAST(coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS VARCHAR(200)) AS vcFullName,
			coalesce(vcEmail,'') AS vcEmail,
			CAST(coalesce(ADC.numPhone,'') AS VARCHAR(100)) AS numPhone,
			CAST(coalesce(ADC.numPhoneExtension,'') AS VARCHAR(100)) AS numPhoneExtension,
			coalesce(CI.vcCompanyName,'') AS vcCompanyName,
			CAST((CASE WHEN cast(NULLIF(coalesce(bitPrimaryContact,cast(0 as TEXT)),'') as INTEGER) = 1 THEN 'Yes' ELSE 'No' END) AS VARCHAR(5)) AS bitPrimaryContact
      FROM
      AdditionalContactsInformation ADC
      INNER JOIN
      DivisionMaster DM
      ON
      ADC.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      WHERE
      ADC.numDomainID = v_numDomainID AND
			(vcFirstName ilike CASE WHEN v_isStartWithSearch = 0 THEN '%' || coalesce(v_searchText,'') || '%' ELSE '' || coalesce(v_searchText,'') || '%' END
      OR vcLastname ilike CASE WHEN v_isStartWithSearch = 0 THEN '%' || coalesce(v_searchText,'') || '%' ELSE '' || coalesce(v_searchText,'') || '%' END
      OR vcEmail ilike CASE WHEN v_isStartWithSearch = 0 THEN '%' || coalesce(v_searchText,'') || '%' ELSE '' || coalesce(v_searchText,'') || '%' END
      OR numPhone ilike CASE WHEN v_isStartWithSearch = 0 THEN '%' || coalesce(v_searchText,'') || '%' ELSE '' || coalesce(v_searchText,'') || '%' END)
      AND 1 =(CASE v_ContactsRights
      WHEN 0 THEN(CASE WHEN coalesce(ADC.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
      WHEN 1 THEN(CASE WHEN coalesce(ADC.numRecOwner,0) = v_numUserCntID THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN (DM.numTerID IN(select numTerritoryID  from UserTerritory where numUserCntID = v_numUserCntID) OR DM.numTerID = 0) THEN 1 ELSE 0 END)
      ELSE 1
      END);
      open SWV_RefCur for
      SELECT * FROM
      tt_TABLECONTACT
      ORDER BY
      vcCompanyName ASC,bitPrimaryContact DESC;

      open SWV_RefCur2 for
      SELECT COUNT(*) FROM tt_TABLECONTACT;
   end if;
   RETURN;
END; $$;


