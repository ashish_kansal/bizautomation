-- Stored procedure definition script USP_GetDomainWiseSort for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDomainWiseSort(v_numDomainId NUMERIC(9,0),        
v_vcModuleType VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcSortOn as VARCHAR(255)) from DomainWiseSort where vcModuleType = v_vcModuleType and numDomainId = v_numDomainId;
END; $$;  












