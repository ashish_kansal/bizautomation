-- Stored procedure definition script usp_SetAllSelectedSystemModulesPagesAndAccesses for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SetAllSelectedSystemModulesPagesAndAccesses(v_numModuleID NUMERIC(9,0) DEFAULT 0,  
 v_numGroupID NUMERIC(9,0) DEFAULT 0,  
 v_numPageID NUMERIC(9,0) DEFAULT 0,  
 v_vcColumnName VARCHAR(30) DEFAULT '',  
 v_vcColumnValue SMALLINT DEFAULT NULL,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL
--  
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitCustomRelationship  BOOLEAN;
   v_vcQuerString  VARCHAR(200);
BEGIN
   IF(SELECT COUNT(*) FROM GroupAuthorization WHERE numGroupID = v_numGroupID AND numModuleID = v_numModuleID AND numPageID = v_numPageID AND numDomainID = v_numDomainID) > 0 then
    
      IF(SELECT COUNT(*) FROM PageMaster WHERE numModuleID = v_numModuleID AND numPageID = v_numPageID) > 0 then
		
         v_bitCustomRelationship := false;
      ELSE
         v_bitCustomRelationship := true;
      end if;

     --PRINT 'UPDATE'  
      v_vcQuerString := 'UPDATE GroupAuthorization SET '  || coalesce(v_vcColumnName,'') || ' = ' || SUBSTR(CAST(v_vcColumnValue AS CHAR(30)),1,30) || ',bitCustomRelationship =' || SUBSTR(CAST(v_bitCustomRelationship AS VARCHAR(30)),1,30) || '  WHERE numGroupID = ' || SUBSTR(CAST(v_numGroupID AS VARCHAR(30)),1,30) || ' AND numModuleID =' || SUBSTR(CAST(v_numModuleID AS VARCHAR(30)),1,30) || ' AND numPageID =' || SUBSTR(CAST(v_numPageID AS VARCHAR(30)),1,30) || ' AND numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30);
   ELSE
      IF(SELECT COUNT(*) FROM PageMaster WHERE numModuleID = v_numModuleID AND numPageID = v_numPageID) > 0 then
		
         v_bitCustomRelationship := false;
      ELSE
         v_bitCustomRelationship := true;
      end if;

     --PRINT 'INSERT'  
      v_vcQuerString := 'INSERT INTO GroupAuthorization(numGroupID, numModuleID, numPageID,numDomainID,bitCustomRelationship, ' || coalesce(v_vcColumnName,'') || ') VALUES(' || SUBSTR(CAST(v_numGroupID AS VARCHAR(30)),1,30) || ', ' || SUBSTR(CAST(v_numModuleID AS VARCHAR(30)),1,30) || ', ' || SUBSTR(CAST(v_numPageID AS VARCHAR(30)),1,30) || ', ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ', ' || SUBSTR(CAST(v_bitCustomRelationship AS VARCHAR(30)),1,30) || ', ' || SUBSTR(CAST(v_vcColumnValue AS VARCHAR(30)),1,30) || ')';
   end if;  
   RAISE NOTICE '%',v_vcQuerString;  
   EXECUTE v_vcQuerString;
   RETURN;
END; $$;


