DROP FUNCTION IF EXISTS USP_GetWareHouseItems;

CREATE OR REPLACE FUNCTION USP_GetWareHouseItems
(              
	v_numItemCode NUMERIC(9,0) DEFAULT 0,
	v_byteMode SMALLINT DEFAULT 0,
	v_numWarehouseItemID NUMERIC(18,0) DEFAULT NULL,
	v_numWarehouseID NUMERIC(18,0) DEFAULT 0,
	v_numWLocationID NUMERIC(18,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor DEFAULT null, 
	INOUT SWV_RefCur2 refcursor DEFAULT null,
	INOUT SWV_RefCur3 refcursor DEFAULT null
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_bitSerialize  BOOLEAN;
   v_str  TEXT;                 
   v_str1  TEXT;
   v_ColName  VARCHAR(500);
   v_numDomainID  INTEGER; 
   v_numBaseUnit  NUMERIC(18,0);
   v_vcUnitName  VARCHAR(100); 
   v_numSaleUnit  NUMERIC(18,0);
   v_vcSaleUnitName  VARCHAR(100); 
   v_numPurchaseUnit  NUMERIC(18,0);
   v_vcPurchaseUnitName  VARCHAR(100);
   v_bitLot  BOOLEAN;                     
   v_bitKitParent  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitMatrix  BOOLEAN;
   v_numItemGroupID  NUMERIC(9,0);                                                
   v_numSaleUOMFactor  DECIMAL(28,14);
   v_numPurchaseUOMFactor  DECIMAL(28,14);

   v_ID  NUMERIC(18,0) DEFAULT 0;                       
   v_numCusFlDItemID  VARCHAR(20);                        
   v_fld_label  VARCHAR(100);
   v_fld_type  VARCHAR(100);                        
   v_Fld_ValueMatrix  NUMERIC(18,0);
   v_Fld_ValueNameMatrix  VARCHAR(300);

   v_KitOnHand  NUMERIC(9,0);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   v_str := '';                       

   SELECT numDomainID, coalesce(numBaseUnit,0), coalesce(numSaleUnit,0), coalesce(numPurchaseUnit,0), numItemGroup, bitLotNo
   , CASE WHEN bitSerialized = false THEN bitLotNo ELSE bitSerialized END
   , (CASE WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true THEN 0
   		WHEN coalesce(bitKitParent,false) = true THEN 1
   		ELSE 0
   	END), bitMatrix, coalesce(bitAssembly,false) INTO v_numDomainID,v_numBaseUnit,v_numSaleUnit,v_numPurchaseUnit,v_numItemGroupID,
   v_bitLot,v_bitSerialize,v_bitKitParent,v_bitMatrix,v_bitAssembly 
   FROM Item 
   WHERE numItemCode = v_numItemCode;     

   select   coalesce(vcUnitName,'-') INTO v_vcUnitName FROM UOM u WHERE u.numUOMId = v_numBaseUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcSaleUnitName FROM UOM u WHERE u.numUOMId = v_numSaleUnit;
   select   coalesce(vcUnitName,'-') INTO v_vcPurchaseUnitName FROM UOM u WHERE u.numUOMId = v_numPurchaseUnit;
	
   v_numSaleUOMFactor := fn_UOMConversion(v_numBaseUnit,v_numItemCode,v_numDomainID::NUMERIC,v_numSaleUnit);
   v_numPurchaseUOMFactor := fn_UOMConversion(v_numBaseUnit,v_numItemCode,v_numDomainID::NUMERIC,v_numPurchaseUnit);


   IF v_bitMatrix = true then
      v_ColName := 'I.numItemCode,1::SMALLINT';
      DROP TABLE IF EXISTS tt_TEMPTABLEMATRIX CASCADE;
      CREATE TEMPORARY TABLE IF NOT EXISTS tt_TEMPTABLEMATRIX AS
         SELECT
         ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.Fld_id) AS ID
			,CFW_Fld_Master.FLd_label
			,CFW_Fld_Master.Fld_id
			,CFW_Fld_Master.fld_type
			,CFW_Fld_Master.numlistid
			,CFW_Fld_Master.vcURL
			,ItemAttributes.Fld_Value
			,CASE
         WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN REPLACE(GetListIemName(Fld_Value),'''','''''')
         WHEN CFW_Fld_Master.fld_type = 'CheckBox' THEN CASE WHEN coalesce(Fld_Value,0) = 1 THEN 'Yes' ELSE 'No' END
         ELSE SUBSTR(CAST(Fld_Value AS VARCHAR(30)),1,30)
         END AS FLD_ValueName
		
         FROM
         CFW_Fld_Master
         INNER JOIN
         ItemGroupsDTL
         ON
         CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
         AND ItemGroupsDTL.tintType = 2
         LEFT JOIN
         ItemAttributes
         ON
         CFW_Fld_Master.Fld_id = ItemAttributes.Fld_ID
         AND ItemAttributes.numItemCode = v_numItemCode
         LEFT JOIN
         Listdetails LD
         ON
         CFW_Fld_Master.numlistid = LD.numListID
         WHERE
         CFW_Fld_Master.numDomainID = v_numDomainID
         AND ItemGroupsDTL.numItemGroupID = v_numItemGroupID
         GROUP BY
         CFW_Fld_Master.FLd_label,CFW_Fld_Master.Fld_id,CFW_Fld_Master.fld_type,
         CFW_Fld_Master.bitAutocomplete,CFW_Fld_Master.numlistid,CFW_Fld_Master.vcURL,
         ItemAttributes.Fld_Value;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPTABLEMATRIX;
      WHILE v_i <= v_iCount LOOP
         select   Fld_label, FLD_Value, FLD_ValueName INTO v_fld_label,v_Fld_ValueMatrix,v_Fld_ValueNameMatrix FROM tt_TEMPTABLEMATRIX WHERE ID = v_i;
         v_str := coalesce(v_str,'') || CONCAT(',''',v_Fld_ValueMatrix,''' AS "',v_fld_label,'"');
         IF v_byteMode = 1 then
            v_str := coalesce(v_str,'') || CONCAT(',''',v_Fld_ValueNameMatrix,''' AS "',v_fld_label,'Value"');
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   ELSE
      v_ColName := 'WareHouseItems.numWareHouseItemID,0::SMALLINT'; 

		--Create a Temporary table to hold data                                                            
      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      create TEMPORARY TABLE tt_TEMPTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         numCusFlDItemID NUMERIC(9,0),
         Fld_Value VARCHAR(20)
      );
      INSERT INTO tt_TEMPTABLE(numCusFlDItemID
			,Fld_Value)
      SELECT DISTINCT
      numOppAccAttrID
			,CAST('' AS VARCHAR(20))
      FROM
      ItemGroupsDTL
      WHERE
      numItemGroupID = v_numItemGroupID
      AND tintType = 2;
      select   ID, numCusFlDItemID, fld_label, fld_type INTO v_ID,v_numCusFlDItemID,v_fld_label,v_fld_type FROM
      tt_TEMPTABLE
      JOIN
      CFW_Fld_Master
      ON
      numCusFlDItemID = Fld_ID     LIMIT 1;
      WHILE v_ID > 0 LOOP
         v_str := coalesce(v_str,'') || ',  GetCustFldItems(' || coalesce(v_numCusFlDItemID,'') || ',9::SMALLINT,' || coalesce(v_ColName,'') || ') AS "' || coalesce(v_fld_label,'') || '"';
         IF v_byteMode = 1 then
            v_str := coalesce(v_str,'') || ',  GetCustFldItemsValue(' || coalesce(v_numCusFlDItemID,'') || ',9::SMALLINT,' || coalesce(v_ColName,'') || ',''' || coalesce(v_fld_type,'') || ''') as "' || coalesce(v_fld_label,'') || 'Value"';
         end if;
         select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
         join CFW_Fld_Master on numCusFlDItemID = Fld_ID and ID > v_ID     LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_ID := 0;
         end if;
      END LOOP;
   end if;          

   v_KitOnHand := 0;      
  
   v_str1 := 'select ';
   
    v_str1 := CONCAT(v_str1,'numWareHouseItemID AS "numWareHouseItemID",WareHouseItems.numWareHouseID AS "numWareHouseID",
	COALESCE(vcWarehouse,'''') || (CASE WHEN COALESCE(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' || COALESCE(WL.vcLocation,'''') END) AS "vcWarehouse",
	COALESCE(vcWarehouse,'''') AS "vcExternalLocation",
	(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE COALESCE(WL.vcLocation,'''') END) AS "vcInternalLocation",
	COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0) AS "TotalOnHand",
	(COALESCE(WareHouseItems.numOnHand,0) * (CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END)) AS "monCurrentValue",
	Case when '|| COALESCE(v_bitKitParent, false) ||'=true then CAST(COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(COALESCE(numOnHand,0) AS FLOAT) END AS "OnHand",
	CAST(('|| COALESCE(v_numPurchaseUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE COALESCE(numOnHand,0) end) AS FLOAT) AS "PurchaseOnHand",
	CAST(('|| COALESCE(v_numSaleUOMFactor, 0)||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE COALESCE(numOnHand,0) end) AS FLOAT) AS "SalesOnHand",
	CAST(COALESCE(numReorder,0) AS FLOAT) as "Reorder"
	,Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE CAST(COALESCE(numOnOrder,0) AS FLOAT) END as "OnOrder"
	,Case when '|| COALESCE(v_bitKitParent, false) ||'=true OR '|| COALESCE(v_bitAssembly, false) || '=true THEN 0 ELSE CAST(COALESCE((SELECT SUM(numUnitHour) FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppID=OM.numOppID WHERE OM.numDomainID=' || CAST(coalesce(v_numDomainID,0) AS VARCHAR)|| ' AND tintOppType=1 AND tintOppStatus=0 AND OI.numItemCode=WareHouseItems.numItemID AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) END as "Requisitions"
	,Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE CAST(COALESCE(numAllocation,0) AS FLOAT) END AS "Allocation"
	,Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE CAST(COALESCE(numBackOrder,0) AS FLOAT) END AS "BackOrder",
	CAST(('|| COALESCE(v_numSaleUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN COALESCE(fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE COALESCE(numOnHand,0) end) as numeric(18,2)) as "OnHandUOM",
	CAST(('|| COALESCE(v_numPurchaseUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE COALESCE(numOnOrder,0) END) AS DECIMAL(18,2)) as "OnOrderUOM",
	CAST(('|| COALESCE(v_numPurchaseUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE COALESCE(numReorder,0) END) AS DECIMAL(18,2)) as "ReorderUOM",
	CAST(('|| COALESCE(v_numSaleUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE COALESCE(numAllocation,0) END) AS DECIMAL(18,2)) as "AllocationUOM",
	CAST(('|| COALESCE(v_numSaleUOMFactor, 0) ||' * Case when '|| COALESCE(v_bitKitParent, false) ||'=true THEN 0 ELSE COALESCE(numBackOrder,0) END) AS DECIMAL(18,2)) as "BackOrderUOM",
	'''|| COALESCE(v_vcUnitName, '') ||''' As "vcBaseUnit",
	'''|| COALESCE(v_vcSaleUnitName, '') ||''' As "vcSaleUnit",
	'''|| COALESCE(v_vcPurchaseUnitName, '') ||''' As "vcPurchaseUnit"
	,0 as Op_Flag , COALESCE(WL.vcLocation,'''') "Location",COALESCE(WL.numWLocationID,0) as "numWLocationID",
	round(COALESCE(monWListPrice,0),2) Price,COALESCE(vcWHSKU,'''') as SKU,COALESCE(vcBarCode,'''') as "BarCode" ',
    COALESCE(v_str, ''),'                   
	,(SELECT CASE WHEN COALESCE(subI.bitKitParent,false) = true AND COALESCE((SELECT COUNT(*) FROM OpportunityItems 
																   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
																   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
				ELSE 0 
			END 
	FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) "IsDeleteKitWarehouse",
	'|| COALESCE(v_bitKitParent, false) ||' AS "bitKitParent", 
	(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN true ELSE false END) AS "bitChildItemWarehouse"
	,',
    (CASE WHEN v_bitSerialize = true THEN 'GetWarehouseSerialLot('|| coalesce(v_numDomainID,0) || ',WareHouseItems.numWareHouseItemID,'
      || coalesce(v_bitLot,false) || ')' ELSE '''''' END),' AS "vcSerialLot",
	CASE 
		WHEN COALESCE(I.numItemGroup,0) > 0 
		THEN fn_GetAttributes(WareHouseItems.numWareHouseItemID,0::BOOLEAN)
		ELSE ''''
	END AS "vcAttribute",
	I.numItemcode AS "numItemcode",
	COALESCE(I.bitKitParent,false) AS "bitKitParent",
	COALESCE(I.bitSerialized,false) AS "bitSerialized",
	COALESCE(I.bitLotNo,false) as "bitLotNo",
	COALESCE(I.bitAssembly,false) as "bitAssembly",
	I.numAssetChartAcntId AS "numAssetChartAcntId",
	(CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END) "monAverageCost"
	from WareHouseItems                           
	join Warehouses W                             
	on W.numWareHouseID=WareHouseItems.numWareHouseID 
	left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
	join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
	where numItemID=',v_numItemCode,' AND (WareHouseItems.numWarehouseItemID = ',
   coalesce(v_numWarehouseItemID,0),
   ' OR ',coalesce(v_numWarehouseItemID,0),' = 0 )',' AND (WareHouseItems.numWareHouseID=',
   coalesce(v_numWarehouseID,0),' OR ',coalesce(v_numWarehouseID,0),
   ' = 0)',' AND (WareHouseItems.numWLocationID=',coalesce(v_numWLocationID,0),
   ' OR ',coalesce(v_numWLocationID,0),' = 0)');        
   
   RAISE NOTICE '%',CAST(v_str1 AS TEXT);           

   OPEN SWV_RefCur FOR 
   EXECUTE (v_str1);
   
   v_str1 := CONCAT('select numWareHouseItmsDTLID AS "numWareHouseItmsDTLID"
	,WDTL.numWareHouseItemID AS "numWareHouseItemID"
	,vcSerialNo AS "vcSerialNo"
	,WDTL.vcComments as "Comments"
	,WDTL.numQty AS "numQty"
	,WDTL.numQty AS "OldQty"
	,COALESCE(W.vcWarehouse,'''') || (CASE WHEN COALESCE(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' || COALESCE(WL.vcLocation,'''') END) AS "vcWarehouse" ',case when v_bitSerialize = true then v_str else '' end,'
	,WDTL.dExpirationDate AS "dExpirationDate"
	, WDTL.bitAddedFromPO AS "bitAddedFromPO"
	from WareHouseItmsDTL WDTL                             
	join WareHouseItems                             
	on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
	join Warehouses W                             
	on W.numWareHouseID=WareHouseItems.numWareHouseID  
	left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
	where COALESCE(numQty,0) > 0 and numItemID=' , COALESCE(v_numItemCode,0) ,
    ' AND (WareHouseItems.numWarehouseItemID = ' || COALESCE(v_numWarehouseItemID,0) || ' OR ' || COALESCE(v_numWarehouseItemID,0) || ' = 0 )'
    ' AND (WareHouseItems.numWareHouseID=' || COALESCE(v_numWarehouseID,0) || ' OR ' || COALESCE(v_numWarehouseID,0) || ' = 0)'
    ' AND (WareHouseItems.numWLocationID=' || COALESCE(v_numWLocationID,0) || ' OR ' || COALESCE(v_numWLocationID,0) || ' = 0)');   
                         
   RAISE NOTICE '%',CAST(v_str1 AS TEXT);                      

   OPEN SWV_RefCur2 FOR 
   EXECUTE (v_str1);
                       
   IF v_bitMatrix = true then
      OPEN SWV_RefCur3 FOR
      SELECT Fld_label AS "Fld_label"
			,fld_id AS "fld_id"
			,fld_type AS "fld_type"
			,numlistid AS "numlistid" 
			,vcURL AS "vcURL"
			,Fld_Value AS "Fld_Value" 
      FROM tt_TEMPTABLEMATRIX;
   ELSE
      OPEN SWV_RefCur3 for
      SELECT Fld_label AS "Fld_label" 
			,fld_id AS "fld_id"
			,fld_type AS "fld_type"
			,numlistid AS "numlistid"
			,vcURL AS "vcURL" 
			,Fld_Value AS "Fld_Value"
      FROM
      tt_TEMPTABLE
      JOIN
      CFW_Fld_Master
      ON
      numCusFlDItemID = Fld_ID;
   END IF;

END
$$;
