-- Stored procedure definition script USP_Item_GetEcommerceWarehouseAvailability for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_GetEcommerceWarehouseAvailability(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_tintWarehouseAvailability SMALLINT
	,v_bitDisplayQtyAvailable BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintWarehouseAvailability = 3 then
	
      open SWV_RefCur for
      SELECT
      TO_CHAR(coalesce(SUM(numOnHand),0),'FM999,999,999,999,990')
      FROM
      WareHouseItems
      INNER JOIN
      Warehouses
      ON
      WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
      WHERE
      WareHouseItems.numDomainID = v_numDomainID
      AND WareHouseItems.numItemID = v_numItemCode;
   ELSE
      open SWV_RefCur for
      SELECT COALESCE((SELECT
         string_agg(CONCAT(vcWareHouse,(CASE WHEN v_bitDisplayQtyAvailable = true THEN CONCAT('(',numOnHand,')') ELSE '' END)),', ' ORDER BY vcWareHouse)
         FROM(SELECT
            Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
						,SUM(numOnHand) AS numOnHand
            FROM
            WareHouseItems
            INNER JOIN
            Warehouses
            ON
            WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
            WHERE
            WareHouseItems.numDomainID = v_numDomainID
            AND WareHouseItems.numItemID = v_numItemCode
            GROUP BY
            Warehouses.numWareHouseID,Warehouses.vcWareHouse) TEMP
         WHERE
         1 =(CASE WHEN v_tintWarehouseAvailability = 2 THEN(CASE WHEN Temp.numOnHand > 0 THEN 1 ELSE 0 END) ELSE 1 END)),'');
   end if;
   RETURN;
END; $$;


