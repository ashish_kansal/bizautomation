-- Stored procedure definition script USP_GetWebReferringInfo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWebReferringInfo(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM TrackingVisitorsHDR
   WHERE numDomainID = v_numDomainID AND coalesce(numListItemID,0) > 0;
END; $$;












