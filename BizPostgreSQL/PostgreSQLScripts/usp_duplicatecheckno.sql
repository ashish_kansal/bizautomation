-- Stored procedure definition script USP_DuplicateCheckNo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DuplicateCheckNo(v_numDomainId NUMERIC,
v_numCheckNo INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COUNT(*) FROM CheckDetails WHERE numDomainId = v_numDomainId AND numCheckNo = v_numCheckNo;
END; $$;













