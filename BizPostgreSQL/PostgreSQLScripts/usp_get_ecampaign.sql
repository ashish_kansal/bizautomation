-- Stored procedure definition script USP_GET_ECampaign for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_ECampaign(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numECampaignID AS "numECampaignID",vcECampName as "vcECampName" FROM ECampaign
   WHERE numDomainID = v_numDomainID;
END; $$;












