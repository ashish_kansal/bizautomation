DROP FUNCTION IF EXISTS USP_GetOppItemsForPickPackShip;

CREATE OR REPLACE FUNCTION USP_GetOppItemsForPickPackShip(v_numDomainID NUMERIC(18,0),
    v_numOppID NUMERIC(18,0),
	v_numBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;
BEGIN
   IF v_numBizDocID = 29397 then -- Packing Slip
	
      open SWV_RefCur for
      SELECT
      OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour -coalesce(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour -(CASE WHEN coalesce(OBDFulfillment.numUnitHour,0) > 0 THEN coalesce(OBDFulfillment.numUnitHour,0) ELSE coalesce(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill,
			'<ul class="list-unstyled" style="text-align:center;">' || COALESCE((SELECT
         string_agg(CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numoppid,''',''',OB.numOppBizDocsId,
         ''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>'),'' ORDER BY OB.numOppBizDocsId)
         FROM
         OpportunityBizDocs OB
         INNER JOIN
         OpportunityBizDocItems OBI
         ON
         OB.numOppBizDocsId = OBI.numOppBizDocID
         WHERE
         OB.numoppid = v_numOppID
         AND OB.numBizDocId = v_numBizDocID
         AND OBI.numOppItemID = OI.numoppitemtCode
         AND coalesce(OBI.numUnitHour,0) <> 0
        ),'') || 
      '</ul>' AS vcPickLists
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      LEFT JOIN
      OpportunityBizDocs OBDPackingSlip
      ON
      OM.numOppId = OBDPackingSlip.numoppid
      AND OBDPackingSlip.numBizDocId = v_numBizDocID
      LEFT JOIN
      OpportunityBizDocItems OBIPackingSlip
      ON
      OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
      AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
      LEFT JOIN LATERAL(SELECT
         SUM(numUnitHour) AS numUnitHour
         FROM
         OpportunityBizDocs
         LEFT JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         WHERE
         OpportunityBizDocs.numoppid = OM.numOppId
         AND OpportunityBizDocs.numBizDocId = 296) OBDFulfillment on TRUE
      WHERE
      OI.numOppId = v_numOppID
      GROUP BY
      OI.numoppitemtCode,OI.numUnitHour,OBDFulfillment.numUnitHour;
   ELSEIF v_numBizDocID = 296
   then -- Fulfillment
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppID;
      open SWV_RefCur for
      SELECT
      OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			coalesce(CASE
      WHEN v_numBizDocID = 296
      THEN(CASE
         WHEN (I.charItemType = 'P' AND coalesce(OI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
         THEN(CASE
            WHEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) <= GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
            v_bitAllocateInventoryOnPickList)
            THEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
            ELSE GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
               v_bitAllocateInventoryOnPickList)
            END)
         ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
         END)
      ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
      END,0) AS QtytoFulFillOriginal,
			(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtyToFulfillWithoutAllocationCheck,
			coalesce((CASE
      WHEN coalesce(SUM(OBI.numUnitHour),0) >= coalesce(OBDPackingSlip.numUnitHour,0)
      THEN(CASE
         WHEN (I.charItemType = 'P' AND coalesce(OI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
         THEN(CASE
            WHEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) <= GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
            v_bitAllocateInventoryOnPickList)
            THEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
            ELSE GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
               v_bitAllocateInventoryOnPickList)
            END)
         ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
         END)
      ELSE(CASE
         WHEN (I.charItemType = 'P' AND coalesce(OI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
         THEN(CASE
            WHEN(coalesce(OBDPackingSlip.numUnitHour,0) -coalesce(SUM(OBI.numUnitHour),0)) <= GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
            v_bitAllocateInventoryOnPickList)
            THEN(coalesce(OBDPackingSlip.numUnitHour,0) -coalesce(SUM(OBI.numUnitHour),0))
            ELSE GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
               v_bitAllocateInventoryOnPickList)
            END)
         ELSE coalesce(OBDPackingSlip.numUnitHour,0) -coalesce(SUM(OBI.numUnitHour),0)
         END)
      END),0) AS QtytoFulFill,
			'<ul class="list-unstyled" style="text-align:center;">' || COALESCE((SELECT
         string_agg(CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numoppid,''',''',OB.numOppBizDocsId,
         ''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>'),' ' ORDER BY OB.numOppBizDocsId)
         FROM
         OpportunityBizDocs OB
         INNER JOIN
         OpportunityBizDocItems OBI
         ON
         OB.numOppBizDocsId = OBI.numOppBizDocID
         WHERE
         OB.numoppid = v_numOppID
         AND OB.numBizDocId = v_numBizDocID
         AND OBI.numOppItemID = OI.numoppitemtCode
         AND coalesce(OBI.numUnitHour,0) <> 0
         ),'') || 
      '</ul>' AS vcFulfillmentBizDocs
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN
      OpportunityBizDocs OBD
      ON
      OM.numOppId = OBD.numoppid
      AND OBD.numBizDocId = v_numBizDocID
      LEFT JOIN
      OpportunityBizDocItems OBI
      ON
      OBD.numOppBizDocsId = OBI.numOppBizDocID
      AND OBI.numOppItemID = OI.numoppitemtCode
      LEFT JOIN LATERAL(SELECT
         SUM(numUnitHour) AS numUnitHour
         FROM
         OpportunityBizDocs
         LEFT JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         WHERE
         OpportunityBizDocs.numoppid = OM.numOppId
         AND OpportunityBizDocs.numBizDocId = 29397) OBDPackingSlip on TRUE
      WHERE
      OI.numOppId = v_numOppID
      GROUP BY
      OI.numoppitemtCode,OI.numUnitHour,OI.numWarehouseItmsID,OI.bitDropShip,
      I.charItemType,I.bitAsset,I.bitKitParent,OBDPackingSlip.numUnitHour;
   ELSEIF v_numBizDocID = 287
   then -- Invoice
	
      open SWV_RefCur for
      SELECT
      OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			coalesce((CASE
      WHEN coalesce(SUM(OBI.numUnitHour),0) >= coalesce(OBDFulfillment.numUnitHour,0)
      THEN OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)
      ELSE coalesce(OBDFulfillment.numUnitHour,0) -coalesce(SUM(OBI.numUnitHour),0)
      END),0) AS QtytoFulFill,
			'<ul class="list-unstyled" style="text-align:center;">' || COALESCE((SELECT
         string_agg(CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numoppid,''',''',OB.numOppBizDocsId,
         ''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>'),' ' ORDER BY OB.numOppBizDocsId)
         FROM
         OpportunityBizDocs OB
         INNER JOIN
         OpportunityBizDocItems OBI
         ON
         OB.numOppBizDocsId = OBI.numOppBizDocID
         WHERE
         OB.numoppid = v_numOppID
         AND OB.numBizDocId = v_numBizDocID
         AND OBI.numOppItemID = OI.numoppitemtCode
         AND coalesce(OBI.numUnitHour,0) <> 0
        ),'') ||
      '</ul>' AS vcInvoices
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      LEFT JOIN
      OpportunityBizDocs OBD
      ON
      OM.numOppId = OBD.numoppid
      AND OBD.numBizDocId = v_numBizDocID
      LEFT JOIN
      OpportunityBizDocItems OBI
      ON
      OBD.numOppBizDocsId = OBI.numOppBizDocID
      AND OBI.numOppItemID = OI.numoppitemtCode
      LEFT JOIN LATERAL(SELECT
         SUM(numUnitHour) AS numUnitHour
         FROM
         OpportunityBizDocs
         LEFT JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         WHERE
         OpportunityBizDocs.numoppid = OM.numOppId
         AND OpportunityBizDocs.numBizDocId = 296) OBDFulfillment on TRUE
      WHERE
      OI.numOppId = v_numOppID
      GROUP BY
      OI.numoppitemtCode,OI.numUnitHour,OBDFulfillment.numUnitHour;
   end if;
   RETURN;
END; $$;


