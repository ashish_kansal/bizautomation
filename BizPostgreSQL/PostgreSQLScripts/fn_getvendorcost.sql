-- Function definition script fn_GetVendorCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetVendorCost(v_numItemCode NUMERIC(18,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Cost  DECIMAL(20,5);
BEGIN
   v_Cost := 0;

   select   coalesce(monCost,0) INTO v_Cost from Vendor V inner join Item I on V.numVendorID = I.numVendorID and
   V.numItemCode = I.numItemCode where V.numItemCode = v_numItemCode;

   return coalesce(v_Cost,0);
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fnGetUserOppAmt]    Script Date: 07/26/2008 18:12:52 ******/

