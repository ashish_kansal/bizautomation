-- Function definition script fn_GetSubStgDtlsbyProStgID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetSubStgDtlsbyProStgID(v_numProID NUMERIC, v_numProStageID NUMERIC,v_tintType SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_vcRetValue := '';

   select  COUNT(*) INTO v_vcRetValue from  ProjectsSubStageDetails where numProId = v_numProID and numStageDetailsId = v_numProStageID;
	
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   if SWV_RowCount > 0 then 
      v_vcRetValue := CAST(1 AS VARCHAR(100));
   else 
      v_vcRetValue := CAST(0 AS VARCHAR(100));
   end if;
   RETURN v_vcRetValue;
END; $$;

