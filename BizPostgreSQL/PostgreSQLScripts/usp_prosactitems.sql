CREATE OR REPLACE FUNCTION USP_ProsActItems
(
	v_bytemode SMALLINT DEFAULT 0,                                                  
	v_numdivisionid NUMERIC(9,0) DEFAULT 0,                                                  
	v_numContactId NUMERIC(9,0) DEFAULT 0 ,                                              
	v_bitTask SMALLINT DEFAULT 0,                                              
	v_FromDate TIMESTAMP DEFAULT NULL,                                              
	v_ToDate TIMESTAMP DEFAULT NULL,                                              
	v_KeyWord VARCHAR(100) DEFAULT '',                                              
	v_CurrentPage INTEGER DEFAULT NULL,                                              
	v_PageSize INTEGER DEFAULT NULL,                                            
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Debasish to enable calculation of date according to client machine                                            
	INOUT v_TotRecs INTEGER  DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql                                              
AS $$
	DECLARE
	v_strSql TEXT;                               
	v_firstRec  INTEGER;                                              
	v_lastRec  INTEGER;
BEGIN
	drop table IF EXISTS tt_TEMPTABLE CASCADE;
           
                                          
	CREATE TEMPORARY TABLE tt_TEMPTABLE AS
	select 
		COUNT(*) OVER() AS numTotalRecords
		,C.numCommId
		,coalesce(A2.vcFirstName,'') || ' ' || coalesce(A2.vcLastname,'') as vcusername
		,A1.numContactId
		,textDetails
		,coalesce(vcData,'-') as Activity
		,coalesce(A1.vcFirstName,'') || ' ' || coalesce(A1.vcLastname,'') as Name
		,case when A1.numPhone <> '' then A1.numPhone || case when A1.numPhoneExtension <> '' then ' - ' || A1.numPhoneExtension else '' end  else '' end as Phone
		,GetListIemName(C.bitTask) AS bitTask
		,dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) As CloseDate
		,dtCreatedDate As CreatedDate
		,C.CaseId
		,(select  vcCaseNumber from Cases where Cases.numCaseId = C.CaseId LIMIT 1) as vcCasenumber
		,coalesce(caseTimeid,0) as caseTimeid
		,coalesce(caseExpid,0) as caseExpid
	FROM Communication C
	JOIN AdditionalContactsInformation  A1
	ON C.numContactId = A1.numContactId
	left join AdditionalContactsInformation A2 on A2.numContactId = C.numAssign
	left join Listdetails on numActivity = numListID
	where
		bitclosedflag = (CASE WHEN v_bytemode=1 THEN true ELSE false END)     
		AND (COALESCE(v_numdivisionid,0) > 0 OR COALESCE(C.numContactId,0) > 0)
		AND (C.numdivisionid =  v_numdivisionid OR COALESCE(v_numdivisionid,0) = 0)
		AND (C.numContactId =  v_numContactId OR COALESCE(v_numContactId,0) = 0) 
		AND (COALESCE(v_KeyWord,'') = '' OR textDetails LIKE '%' || v_KeyWord || '%')
		AND 1 = (CASE 
					WHEN v_FromDate > '1900-01-01 00:00:00.000' AND v_ToDate > '1900-01-01 00:00:00.000'
					THEN (CASE WHEN dtStartTime BETWEEN v_FromDate AND v_ToDate THEN 1 ELSE 0 END)
					ELSE 1 
				END)
		AND 1 = (CASE WHEN v_bitTask > 0 THEN (CASE WHEN bitTask = v_bitTask THEN 1 ELSE 0 END) ELSE 1 END)
	order by  C.numCommId  Desc
	LIMIT v_PageSize OFFSET (v_CurrentPage * v_PageSize);
		
	IF COALESCE((SELECT COUNT(*) FROM tt_TEMPTABLE),0) > 0 THEN
		SELECT numTotalRecords INTO v_TotRecs from tt_TEMPTABLE LIMIT 1;    
	END IF;
                     
	OPEN SWV_RefCur FOR SELECT * FROM tt_TEMPTABLE;

	RETURN;
END; $$;


