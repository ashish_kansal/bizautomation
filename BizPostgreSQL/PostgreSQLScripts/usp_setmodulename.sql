-- Stored procedure definition script usp_SetModuleName for PostgreSQL
CREATE OR REPLACE FUNCTION usp_SetModuleName(v_vcModuleName VARCHAR(50),
	v_numModuleID NUMERIC(9,0)   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numModuleID > 0 then
			
      UPDATE ModuleMaster SET vcModuleName = v_vcModuleName
      WHERE numModuleID = v_numModuleID;
   ELSE
      INSERT INTO ModuleMaster(vcModuleName)
				VALUES(v_vcModuleName);
				
open SWV_RefCur for SELECT cast(CURRVAL('ModuleMaster_seq') as VARCHAR(255));
   end if;
END; $$;












