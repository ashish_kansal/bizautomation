create or replace FUNCTION USP_GetOpportunityRecurringList(v_numSeedOppId    NUMERIC(9,0),
               v_tintRecType SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF (v_tintRecType = 1) then
      
      open SWV_RefCur for
      SELECT
      RTR.numRecTranOppID,
			0 AS numRecTranBizDocID,
			OM.vcpOppName AS RecurringName,
			GetDealAmount(RTR.numRecTranOppID,TIMEZONE('UTC',now()),0::NUMERIC) AS TotalCostAmount,
			v_tintRecType AS tintRecType
      FROM   OpportunityMaster AS OM
      INNER JOIN RecurringTransactionReport AS RTR
      ON OM.numOppId = RTR.numRecTranOppID
      WHERE  RTR.tintRecType = 1
      AND RTR.numRecTranSeedId = v_numSeedOppId;
   ELSEIF (v_tintRecType = 2)
   then
        
      open SWV_RefCur for
      SELECT
      OPR.numOppId AS numRecTranOppID,
			  OPR.numOppBizDocID AS numRecTranBizDocID,
			  OBD.vcBizDocID AS RecurringName,
			  OBD.monDealAmount AS TotalCostAmount,
			  v_tintRecType AS tintRecType
      FROM   OpportunityRecurring OPR
      INNER JOIN OpportunityBizDocs AS OBD
      ON OPR.numOppBizDocID = OBD.numOppBizDocsId
      WHERE  OPR.tintRecurringType = 2
      AND OPR.numOppId IN(SELECT numOppId FROM OpportunityRecurring WHERE numOppBizDocID = v_numSeedOppId);
   end if;
   RETURN;
END; $$;



