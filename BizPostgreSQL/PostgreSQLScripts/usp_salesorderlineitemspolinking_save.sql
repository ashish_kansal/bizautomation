DROP FUNCTION IF EXISTS USP_SalesOrderLineItemsPOLinking_Save;

CREATE OR REPLACE FUNCTION USP_SalesOrderLineItemsPOLinking_Save(v_numDomainID NUMERIC(18,0)
	,v_numSalesOrderID NUMERIC(18,0)
	,v_numSalesOrderItemID NUMERIC(18,0)
	,v_numSalesOrderOppChildItemID NUMERIC(18,0)
	,v_numSalesOrderOppKitChildItemID  NUMERIC(18,0)
	,v_numPurchaseOrderID NUMERIC(18,0)
	,v_numPurchaseOrderItemID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM SalesOrderLineItemsPOLinking WHERE numDomainID = v_numDomainID AND numSalesOrderID = v_numSalesOrderID AND numSalesOrderItemID = v_numSalesOrderItemID AND COALESCE(numSalesOrderOppChildItemID,0) = v_numSalesOrderOppChildItemID AND COALESCE(numSalesOrderOppKitChildItemID,0) = v_numSalesOrderOppKitChildItemID) then
		UPDATE
			SalesOrderLineItemsPOLinking
		SET
			numPurchaseOrderID = v_numPurchaseOrderID,numPurchaseOrderItemID = v_numPurchaseOrderItemID
		WHERE
			numDomainID = v_numDomainID
			AND numSalesOrderID = v_numSalesOrderID
			AND numSalesOrderItemID = v_numSalesOrderItemID
			AND COALESCE(numSalesOrderOppChildItemID,0) = v_numSalesOrderOppChildItemID 
			AND COALESCE(numSalesOrderOppKitChildItemID,0) = v_numSalesOrderOppKitChildItemID;
   ELSE
		INSERT INTO SalesOrderLineItemsPOLinking
		(
			numDomainID,numSalesOrderID,numSalesOrderItemID,numSalesOrderOppChildItemID,numSalesOrderOppKitChildItemID,numPurchaseOrderID,numPurchaseOrderItemID
		)
		VALUES
		(
			v_numDomainID,v_numSalesOrderID,v_numSalesOrderItemID,v_numSalesOrderOppChildItemID,v_numSalesOrderOppKitChildItemID,v_numPurchaseOrderID,v_numPurchaseOrderItemID
		);
   end if;
   RETURN;
END; $$;


