-- Stored procedure definition script USP_GetCountOfParentCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCountOfParentCategory(v_numParntAcntId NUMERIC(9,0) DEFAULT 0,          
v_numDomainId NUMERIC(9,0) DEFAULT 0,      
INOUT v_intCount INTEGER DEFAULT 0 ,    -- Obsolet, as there will be no child account, any account can be added to leaf node of account category.
INOUT v_intJournalEntryCount INTEGER DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);    
   v_numAcntTypeId  NUMERIC(9,0);     
--  Select @intCount=Count([numParntAcntTypeId]) From Chart_Of_Accounts Where nump=@numParntAcntId And numDomainId=@numDomainId         
--  Select @numAcntTypeId=isnull([numAcntTypeId],0) From Chart_Of_Accounts Where numAccountId=@numParntAcntId And numDomainId=@numDomainId         
--  Select @numJournalId=numJournal_Id From General_Journal_Header where numChartAcntId=@numParntAcntId  And numDomainId=@numDomainId
	--as OPening balance of account is entered as journal entry we need to discard it while checking for deletion
BEGIN
   select   numJOurnal_Id INTO v_numJournalId FROM General_Journal_Header WHERE numChartAcntId = v_numParntAcntId    LIMIT 1;
  
   select   Count(numChartAcntId) INTO v_intJournalEntryCount From General_Journal_Details Where numChartAcntId = v_numParntAcntId And numDomainId = v_numDomainId    AND numJournalId <> v_numJournalId;
   RETURN;
END; $$;


