CREATE OR REPLACE FUNCTION GetIncomeforCampaign
(
	p_numCampaignid numeric(18,0)
	,p_tintType SMALLINT
	,p_dtStartDate TIMESTAMP DEFAULT NULL
	,p_dtEndDate TIMESTAMP DEFAULT NULL
)              
RETURNS DECIMAL(20,5) LANGUAGE plpgsql 
AS $$            
	DECLARE v_amount DECIMAL(20,5);
	v_monCampaignCost DECIMAL(20,5);
	v_oppTime DECIMAL(20,5);
	v_ServiceCost DECIMAL(20,5);
	v_TotalIncome DECIMAL(20,5);
	v_NoofClosedDeals integer ;            
	v_CostofGoodsSold DECIMAL(20,5);            
	v_CostofLabor DECIMAL(20,5); 
BEGIN
	SELECT
		(SELECT SUM(monAmount) FROM BillDetails BD JOIN BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CampaignMaster.numCampaignId GROUP BY numCampaignId) INTO v_monCampaignCost           
	from 
		campaignmaster              
	where 
		numCampaignid=p_numCampaignid LIMIT 1;            
	

	IF p_tintType=1 THEN ---Total Income potential -> the sum total of all sales opportunities that come from the organization tied to the campaign.                       
		SELECT 
			SUM(opp.monDealAmount) INTO v_amount 
		from 
			OpportunityMaster  Opp     
		where           
			opp.tintOppType = 1
			AND opp.tintOppStatus = 0
			AND opp.numCampainID=p_numCampaignid;
	END IF;           

	IF p_tintType=2 THEN ---Total Income 
	  select sum(opp.monDealAmount) INTO v_amount from OpportunityMaster  Opp              
	  where 
	  opp.tintOppType = 1              
	  and opp.tintOppStatus=1  
	  AND opp.numCampainID=p_numCampaignid;
	End IF;
	
	If p_tintType=3 THEN---Cost to Mfg. Deal                
		SELECT 
			COUNT(*) INTO v_NoofClosedDeals 
		FROM 
			OpportunityMaster  Opp              
		WHERE 
			opp.tintOppType= 1              
			AND opp.tintOppStatus=1   
			AND opp.numCampainID=p_numCampaignid;

		--Cost of Goods Sold            
		SELECT 
			Sum(oppIt.numUnitHour * (CASE WHEN COALESCE(i.bitVirtualInventory,false) = true THEN 0 ELSE i.monAverageCost END)) INTO v_CostofGoodsSold 
		From             
		OpportunityItems oppIt              
		Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
		left join item i on oppIt.numItemCode=i.numItemCode               
		left join ListDetails L on i.numItemClassification=L.numListItemID            
		Where Opp.tintOppType= 1 And opp.tintOppStatus=1 
		And (i.charItemType='P' or i.charItemType='A') 
		AND opp.numCampainID=p_numCampaignid;
     
		--Cost of Labor            
		SELECT 
			case 
				when Sum((CAST(cm.numamount - (GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-CAST(datediff('minute',dtfromdate,dttodate) AS decimal(10,2))*monamount/60) AS DECIMAL(10,2))))>=0              
			then 0 else Sum((CAST(cm.numamount - (GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-CAST(datediff('minute',dtfromdate,dttodate) AS decimal(10,2))*monamount/60) AS decimal(10,2))))*-1 end  INTO v_oppTime        
		from timeandexpense OT               
		join opportunitymaster Opp on OT.numoppid=Opp.numoppid               
		join contractmanagement cm on ot.numcontractid = cm.numcontractid             
		Where ot.numcategory =1 and ot.numtype =1 and Opp.tintOppType= 1 And Opp.tintOppStatus=1 
		AND opp.numCampainID=p_numCampaignid;
  
		--For Service Item            
		Select sum(oppIt.monTotAmount*COALESCE(i.monCampaignLabourCost,0)/100) INTO v_ServiceCost From             
		OpportunityItems oppIt              
		Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
		left join item i on oppIt.numItemCode=i.numItemCode               
		left join ListDetails L on i.numItemClassification=L.numListItemID            
		Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
		And i.charItemType='S' 
		AND opp.numCampainID=p_numCampaignid;
     
		If v_NoofClosedDeals>0 THEN               
			v_amount := (CAST(COALESCE(v_monCampaignCost,0) AS DECIMAL(20,5)))/v_NoofClosedDeals;                       
		Else 
			v_amount := 0; 
		END IF;
               
	End IF;      
	
	If p_tintType=4 THEN ---ROI                       
		v_TotalIncome := COALESCE(GetIncomeforCampaign(p_numCampaignid,2::SMALLINT,p_dtStartDate,p_dtEndDate),0);
		v_amount := CAST(COALESCE(v_TotalIncome,0) AS DECIMAL(20,5))-CAST(COALESCE(v_monCampaignCost,0) AS DECIMAL(20,5));
	End IF;             
            
	If p_tintType=5 THEN -- COGS                 
	 Select Sum(oppIt.numUnitHour*(CASE WHEN COALESCE(i.bitVirtualInventory,false) = true THEN 0 ELSE i.monAverageCost END)) INTO v_amount From             
	 OpportunityItems oppIt              
	 Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
	 left join item i on oppIt.numItemCode=i.numItemCode               
	 left join ListDetails L on i.numItemClassification=L.numListItemID            
	 Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
	 And (i.charItemType='P' or i.charItemType='A') 
	 AND opp.numCampainID=p_numCampaignid;
 
	End IF;           
             
	if p_tintType=6 THEN -- Cost of Labor                       
	   Select case when Sum((CAST(cm.numamount - (GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-CAST(datediff('minute',dtfromdate,dttodate) AS decimal(10,2))*monamount/60) AS decimal(10,2))))>=0              
	  then 0 else Sum((CAST(cm.numamount - (GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-CAST(datediff('minute',dtfromdate,dttodate) AS decimal(10,2))*monamount/60) AS decimal(10,2))))*-1 end INTO v_oppTime
	 From timeandexpense OT              
	 join opportunitymaster Opp on OT.numoppid=Opp.numoppid               
	 join contractmanagement cm on ot.numcontractid = cm.numcontractid             
	 Where ot.numCategory =1 and ot.numType =1 and Opp.tintOppType= 1 And Opp.tintOppStatus=1 
	 AND opp.numCampainID=p_numCampaignid;
            
	 --For Service Item            
	 Select sum(oppIt.monTotAmount*COALESCE(monCampaignLabourCost,0)/100) INTO v_ServiceCost From             
	 OpportunityItems oppIt              
	 Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
	 left join item i on oppIt.numItemCode=i.numItemCode               
	 left join ListDetails L on i.numItemClassification=L.numListItemID            
	 Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
	 And i.charItemType='S' 
	 AND opp.numCampainID=p_numCampaignid;
             
	 v_amount := COALESCE(v_oppTime,0)+COALESCE(v_ServiceCost,0);   
	End IF;   


	RETURN ROUND(COALESCE(v_amount,0),2);
            
END; $$;

