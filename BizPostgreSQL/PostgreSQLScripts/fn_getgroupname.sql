-- Function definition script fn_GetGroupName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetGroupName(v_numGrpID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetName  VARCHAR(20);
BEGIN
   v_RetName := '';
   select   vcGrpName INTO v_RetName from Groups where numGrpId = v_numGrpID;
   RETURN v_RetName;
END; $$;

