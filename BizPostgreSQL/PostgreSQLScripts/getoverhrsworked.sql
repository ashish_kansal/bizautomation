-- Function definition script GetOverHrsWorked for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOverHrsWorked(v_bitOverTime BOOLEAN,v_bitOverTimeHrsDailyOrWeekly BOOLEAN,v_decOverTime DECIMAL,v_dtStartDate TIMESTAMP,v_dtEndDate TIMESTAMP,v_numUserCntID NUMERIC,v_numDomainId NUMERIC,v_ClientTimeZoneOffset INTEGER)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql                             
 --Declare @decOverTime as decimal(10,2)                          
   AS $$
   DECLARE
   v_decTotalHrsWorked  DECIMAL(10,2);                            
   v_decTotalOverHrsWorked  DECIMAL(10,2);                             
   v_decTotalOverHrsWorked1  DECIMAL(10,2);
BEGIN
   v_decTotalHrsWorked := 0;    
   v_decTotalOverHrsWorked := 0;    
   v_decTotalOverHrsWorked1 := 0;                 
 --Set  @decOverTime =12                           
                          
   if v_bitOverTime = true then
 
      if v_bitOverTimeHrsDailyOrWeekly = true then
                       
                   
    --Set @decTotalHrsWorked=dbo.fn_GetTotalHrsWorked(@dtStartDate,@dtEndDate,@numUserCntID,@numDomainId)                  
         select   sum(x.Hrs) INTO v_decTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
            from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1
            And (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtStartDate And v_dtEndDate Or
            dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtStartDate And v_dtEndDate) And
            numUserCntID = v_numUserCntID  And numDomainID = v_numDomainId) x;        
   -- Set @decTotalOverHrsWorked= @decTotalHrsWorked-(@decOverTime*datediff(day,@dtStartDate,@dtendDate))            
         v_decTotalOverHrsWorked := v_decTotalHrsWorked -v_decOverTime;
      end if;     
                         
                    
                      
                           
  --print @decTotalOverHrsWorked                          
      If v_decTotalOverHrsWorked > 0 then
         v_decTotalOverHrsWorked1 := v_decTotalOverHrsWorked;
      Else
         v_decTotalOverHrsWorked1 := 0;
      end if;
   end if;                       
   Return v_decTotalOverHrsWorked1;
END; $$;

