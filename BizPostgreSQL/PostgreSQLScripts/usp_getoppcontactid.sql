-- Stored procedure definition script usp_GetOppContactId for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetOppContactId(v_numOppId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numContactId  NUMERIC;
BEGIN
   select   numContactId INTO v_numContactId from OpportunityMaster WHERE numOppId = v_numOppId;
   open SWV_RefCur for SELECT v_numContactId;
END; $$;












