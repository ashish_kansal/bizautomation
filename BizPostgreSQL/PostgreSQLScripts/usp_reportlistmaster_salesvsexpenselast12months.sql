-- Stored procedure definition script USP_ReportListMaster_SalesVsExpenseLast12Months for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_SalesVsExpenseLast12Months(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcGroupBy VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_dtFromDate DATE
	,v_dtToDate DATE,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StartDate  DATE;
   v_EndDate  DATE;
   v_TableInsertCount  INTEGER DEFAULT 0;
   v_j  INTEGER DEFAULT 0;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_TempSales  DECIMAL(20,5) DEFAULT 0;
   v_TempExpense  DECIMAL(20,5) DEFAULT 0;
BEGIN
   v_EndDate := date_trunc('month', TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) + interval '1 month' - interval '1 day';

   BEGIN
      CREATE TEMP SEQUENCE tt_TABLE_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TABLE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      MonthStartDate DATE,
      MonthEndDate DATE,
      MonthLabel VARCHAR(20),
      MonthSales DECIMAL(20,5),
      MonthExpense DECIMAL(20,5)
   );

   IF v_vcTimeLine = 'Last12Months' then
	
      v_StartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_EndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_EndDate,'1900-01-01':: DATE))) || 'month' as interval);
      v_TableInsertCount := 12;
   end if;
   IF v_vcTimeLine = 'Last6Months' then
	
      v_StartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_EndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_EndDate,'1900-01-01':: DATE))) || 'month' as interval);
      v_TableInsertCount := 6;
   end if;
   IF v_vcTimeLine = 'Last3Months' then
	
      v_StartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_EndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_EndDate,'1900-01-01':: DATE))) || 'month' as interval);
      v_TableInsertCount := 3;
   end if;
   IF v_vcTimeLine = 'Last30Days' then
	
      v_EndDate := now();
      v_StartDate := '1900-01-01':: date + CAST(EXTRACT(day from v_EndDate::TIMESTAMP - '1900-01-01'::date + INTERVAL '30 day') || 'day' AS INTERVAL);
	  
      v_TableInsertCount := 30;
   end if;
   IF v_vcTimeLine = 'Last7Days' then
	
      v_EndDate :=(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
      v_StartDate := '1900-01-01':: date + CAST(EXTRACT(day from v_EndDate::TIMESTAMP - '1900-01-01'::date + INTERVAL '7 day') || 'day' AS INTERVAL);
      v_TableInsertCount := 7;
   end if;
	

   WHILE v_j < v_TableInsertCount LOOP
      IF(v_vcTimeLine = 'Last30Days' OR v_vcTimeLine = 'Last7Days') then
		
         INSERT INTO tt_TABLE(MonthStartDate,
				MonthEndDate,
				MonthLabel)
			VALUES(v_EndDate+CAST(-(v_j::bigint+1) || 'day' as interval),
				v_EndDate+CAST(-v_j || 'day' as interval),
				CONCAT(SUBSTR(TO_CHAR(v_EndDate+CAST(-v_j || 'day' as interval), 'Mon dd yyyy hh:miAM'), 1,6),' ',EXTRACT(YEAR FROM v_EndDate+CAST(-v_j || 'day' as interval))));
      ELSE
         INSERT INTO tt_TABLE(MonthStartDate,
				MonthEndDate,
				MonthLabel)
			VALUES(v_StartDate+CAST(-v_j || 'month' as interval),
				v_EndDate+CAST(-v_j || 'month' as interval),
				CONCAT(SUBSTR(TO_CHAR(v_EndDate+CAST(-v_j || 'month' as interval), 'Mon dd yyyy hh:miAM'), 1,3),' ',EXTRACT(YEAR FROM v_EndDate+CAST(-v_j || 'month' as interval))));
      end if;
      v_j := v_j::bigint+1;
   END LOOP;

   select   COUNT(*) INTO v_iCount FROM tt_TABLE;

   WHILE v_i <= v_iCount LOOP
      v_TempSales := 0;
      v_TempExpense := 0;
      select   MonthStartDate, MonthEndDate INTO v_StartDate,v_EndDate FROM tt_TABLE WHERE ID = v_i;
      select   SUM(monTotAmount) INTO v_TempSales FROM(SELECT
         coalesce(monTotAmount,0) AS monTotAmount
         FROM
         OpportunityItems OI
         INNER JOIN
         OpportunityMaster OM
         ON
         OI.numOppId = OM.numOppId
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         WHERE
         OM.numDomainId = v_numDomainID
         AND CAST(OM.bintCreatedDate AS DATE) BETWEEN  v_StartDate AND v_EndDate
         AND coalesce(OM.tintopptype,0) = 1
         AND coalesce(OM.tintoppstatus,0) = 1) TEMP;
      select   SUM(numDebitAmt) -SUM(numCreditAmt) INTO v_TempExpense FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId = General_Journal_Header.numJOurnal_Id WHERE General_Journal_Details.numDomainId = v_numDomainID AND CAST(datEntry_Date AS DATE) BETWEEN  v_StartDate AND v_EndDate AND General_Journal_Details.numChartAcntId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID AND bitActive = true AND numAcntTypeId IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode ilike '0104%'));
      UPDATE tt_TABLE SET MonthSales = coalesce(v_TempSales,0),MonthExpense = coalesce(v_TempExpense,0) WHERE ID = v_i;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT * FROM tt_TABLE;
END; $$;












