-- Stored procedure definition script usp_UpdateFollowUpDatesByContactId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateFollowUpDatesByContactId(v_numDomainID NUMERIC(9,0) DEFAULT null,          
	 v_strContactIds TEXT DEFAULT '',
	 v_bitUpdateFollowUpStatus BOOLEAN DEFAULT false,
	 v_numFollowUpStatusId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   DivisionMaster
   SET
   dtLastFollowUp = TIMEZONE('UTC',now())
   WHERE
   numDomainID = v_numDomainID AND numDivisionID IN(SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId IN (SELECT Id FROM SplitIDs(v_strContactIds,',')));
   IF(v_bitUpdateFollowUpStatus = true AND v_numFollowUpStatusId > 0) then
	
      UPDATE
      DivisionMaster
      SET
      numFollowUpStatus = v_numFollowUpStatusId
      WHERE
      numDomainID = v_numDomainID AND numDivisionID IN(SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId IN (SELECT Id FROM SplitIDs(v_strContactIds,',')));
   end if;
   RETURN;
END; $$;     


