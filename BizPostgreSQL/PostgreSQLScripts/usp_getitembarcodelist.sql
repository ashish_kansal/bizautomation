-- Stored procedure definition script USP_GetItemBarcodeList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemBarcodeList(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_strItemIdList TEXT DEFAULT NULL,
v_bitAllItems BOOLEAN DEFAULT false,
v_tinyBarcodeBasedOn SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_str1  VARCHAR(8000);               

   v_str  VARCHAR(8000);               

--Create a Temporary table to hold data                                                            
   v_ID  NUMERIC(9,0);                        
   v_numCusFlDItemID  VARCHAR(20);                        
   v_fld_label  VARCHAR(100);
   v_fld_type  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   drop table IF EXISTS  tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   (
      numItemCode NUMERIC(9,0)
   );  

   insert into tt_TEMPTABLE  select Id FROM SplitIds(v_strItemIdList,',');

   v_str1 := '';               

   v_str := ''' ''';               

--Create a Temporary table to hold data                                                            
   BEGIN
      CREATE TEMP SEQUENCE tt_tempCustTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPCUSTTABLE CASCADE;
   create TEMPORARY TABLE tt_TEMPCUSTTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCusFlDItemID NUMERIC(9,0),
      fld_label VARCHAR(50),
      fld_type VARCHAR(50)
   );                         
   v_ID := 0;                        

   If v_tinyBarcodeBasedOn = 0 then --UPC/Barcode

      insert into tt_TEMPCUSTTABLE(numCusFlDItemID,fld_label,fld_type)
      select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID = Fld_id where numDomainID = v_numDomainID and tintType = 2;
      select   ID, numCusFlDItemID, fld_label, fld_type INTO v_ID,v_numCusFlDItemID,v_fld_label,v_fld_type from tt_TEMPCUSTTABLE     LIMIT 1;
      while v_ID > 0 LOOP
         IF v_ID = 1 then  
            v_str := '''' || coalesce(v_fld_label,'');
         Else   
            v_str := coalesce(v_str,'') || ' || ''  ' || coalesce(v_fld_label,'');
         end if;
         v_str := coalesce(v_str,'') || ':'' || GetCustFldItemsValue(' || coalesce(v_numCusFlDItemID,'') || ',9::SMALLINT,numWareHouseItemID,0::SMALLINT,''' || coalesce(v_fld_type,'') || ''')';
         select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPCUSTTABLE where ID > v_ID    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_ID := 0;
         end if;
      END LOOP;
      v_str1 := 'select I.numItemCode,I.vcItemName,I.numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute 
			from Item I 
			join tt_TEMPTABLE T on I.numItemCode = T.numItemCode 
			where I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' 
			and COALESCE(numItemGroup,0)=0 
			and I.numBarCodeId is not null 
			and LENGTH(I.numBarCodeId)>1 
 
			Union

			select I.numItemCode,I.vcItemName,WI.vcBarCode as numBarCodeId,W.vcWareHouse,' || coalesce(v_str,'') || ' as vcAttribute  
			from Item I 
			join tt_TEMPTABLE T on I.numItemCode=T.numItemCode
			join WareHouseItems WI on WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and I.numItemCode=WI.numItemID  
			join Warehouses W on W.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and W.numWareHouseID=WI.numWareHouseID  
			where I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' 
			and COALESCE(numItemGroup,0)>0 
			and WI.vcBarCode is not null 
			and LENGTH(WI.vcBarCode)>1';
   ELSEIF v_tinyBarcodeBasedOn = 1
   then --SKU
	
      v_str1 := 'select I.numItemCode,I.vcItemName,I.vcSKU as numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute from Item I join tt_TEMPTABLE T on I.numItemCode = T.numItemCode 
	where I.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and NULLIF(I.vcSKU,'''') is not null and LENGTH(I.vcSKU) > 1';
   ELSEIF v_tinyBarcodeBasedOn = 2
   then --Vendor Part#
	
      v_str1 := 'select I.numItemCode,I.vcItemName,Vendor.vcPartNo as numBarCodeId,com.vcCompanyName as vcWareHouse,'''' as vcAttribute
	from Vendor 
	join Item I on I.numItemCode = Vendor.numItemCode
	join tt_TEMPTABLE T on I.numItemCode = T.numItemCode  
	join DivisionMaster div on div.numDivisionID = Vendor.numVendorid        
	join CompanyInfo com on com.numCompanyId = div.numCompanyID        
	WHERE I.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and Vendor.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' 
	and NULLIF(Vendor.vcPartNo,'''') is not null and LENGTH(Vendor.vcPartNo) > 1';
   ELSEIF v_tinyBarcodeBasedOn = 3
   then --Serial #/LOT #
	
      insert into tt_TEMPCUSTTABLE(numCusFlDItemID,fld_label,fld_type)
      select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID = Fld_id where numDomainID = v_numDomainID and tintType = 2;
      select   ID, numCusFlDItemID, fld_label, fld_type INTO v_ID,v_numCusFlDItemID,v_fld_label,v_fld_type from tt_TEMPCUSTTABLE     LIMIT 1;
      while v_ID > 0 LOOP
         IF v_ID = 1 then  
            v_str := '''' || coalesce(v_fld_label,'');
         Else   
            v_str := coalesce(v_str,'') || ' || ''  ' || coalesce(v_fld_label,'');
         end if;
         v_str := coalesce(v_str,'') || ':'' || GetCustFldItemsValue(' || coalesce(v_numCusFlDItemID,'') || ',9::SMALLINT,numWareHouseItmsDTLID,0::SMALLINT,''' || coalesce(v_fld_type,'') || ''')';
         select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPCUSTTABLE where ID > v_ID    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_ID := 0;
         end if;
      END LOOP;
      v_str1 := 'select I.numItemCode,I.vcItemName,WDTL.vcSerialNo as numBarCodeId,W.vcWareHouse,' || coalesce(v_str,'') || ' as vcAttribute 
	from Item I join tt_TEMPTABLE T on I.numItemCode=T.numItemCode
	join WareHouseItems WI on WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and W.numWareHouseID=WI.numWareHouseID  
	join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
	where I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and (I.bitSerialized=true or I.bitLotNo=true) and WDTL.vcSerialNo is not null and LENGTH(WDTL.vcSerialNo)>1
	and COALESCE(WDTL.numQty,0) > 0 ';
   ELSEIF v_tinyBarcodeBasedOn = 4
   then --Item Code
	
      v_str1 := 'SELECT I.numItemCode,I.vcItemName,I.numItemCode AS numBarCodeId,'''' AS vcWareHouse,'''' AS vcAttribute 
				FROM Item I JOIN tt_TEMPTABLE T ON I.numItemCode = T.numItemCode 
				WHERE I.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
   end if;


   RAISE NOTICE '%',(v_str1);           
   OPEN SWV_RefCur FOR EXECUTE v_str1;  

   drop table IF EXISTS tt_TEMPCUSTTABLE CASCADE;
   RETURN;
END; $$;


