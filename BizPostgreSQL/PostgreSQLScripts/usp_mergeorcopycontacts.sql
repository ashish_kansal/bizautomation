-- Stored procedure definition script usp_MergeOrCopyContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_MergeOrCopyContacts(v_vcContactIDs VARCHAR(400),                      
 v_numCompanyId NUMERIC,                    
 v_numDivisionId NUMERIC,                    
 v_numUserCntId NUMERIC,   --The User who is performing the action                    
 v_vcAction CHAR(1)   --C: Copy/ M: merge                    
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcAction = 'C' then --Request is for Copying Contacts                    
   
      INSERT INTO AdditionalContactsInformation(vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastname,
       numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension,
       numCell, numHomePhone, vcFax, vcEmail, VcAsstFirstName, vcAsstLastName,
       numAsstPhone, numAsstExtn, vcAsstEmail,
       charSex, bintDOB, vcPosition, txtNotes,
       numCreatedBy, bintCreatedDate,numModifiedBy,bintModifiedDate,
       numDomainID, bitOptOut,bitPrimaryContact)
      SELECT vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastname,
              v_numDivisionId AS numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension,
              numCell, numHomePhone, vcFax, vcEmail, VcAsstFirstName, vcAsstLastName,
              numAsstPhone, numAsstExtn, vcAsstEmail,
              charSex, bintDOB, vcPosition, txtNotes,
              v_numUserCntId, TIMEZONE('UTC',now()),v_numUserCntId, TIMEZONE('UTC',now()),
              numDomainID, bitOptOut,bitPrimaryContact
      FROM AdditionalContactsInformation WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));      
    
      
      /*    
       INSERT INTO ContactAddress(numContactId, vcStreet, vcCity, intPostalCode,                    
       vcState, vcCountry, vcContactLocation, vcContactLocation1, vcStreet1, vcCity1,                    
       vcPostalCode1, vcState1, vcCountry1, vcContactLocation2, vcStreet2, vcCity2,                    
       vcPostalCode2, vcState2, vcCountry2, vcContactLocation3, vcStreet3, vcCity3,                    
       vcPostalCode3, vcState3, vcCountry3, vcContactLocation4, vcStreet4, vcCity4,                    
       vcPostalCode4, vcState4, vcCountry4,                     
       vcPStreet, vcPCity, vcPPostalCode, vcPState, vcPCountry)    
      */            
      UPDATE AdditionalContactsInformation SET bitOptOut = true,numEmpStatus = 3,numModifiedBy = v_numUserCntId,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));
      if(select count(*) from AdditionalContactsInformation where numDivisionId = v_numDivisionId and  coalesce(bitPrimaryContact,false) = true) > 1 then
      
         UPDATE AdditionalContactsInformation SET bitPrimaryContact = false
         WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','))
         AND coalesce(bitPrimaryContact,false) = true;
      end if;
   ELSEIF v_vcAction = 'M'
   then --Request is for Merging Contacts                    
   
      RAISE NOTICE 'In Merge';                   
       --Changing the Division Id of the Contact in Communication                  
      UPDATE Communication SET numDivisionID = v_numDivisionId,nummodifiedby = v_numUserCntId,dtModifiedDate = TIMEZONE('UTC',now())
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));                  
       --Changing the Company Id in CampaignDetails                   
      UPDATE CampaignDetails SET numCompanyId = v_numCompanyId
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));                  
       --Changing the Division Id of the Contact in Cases                  
      UPDATE Cases SET numDivisionID = v_numDivisionId,numModifiedBy = v_numUserCntId,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));                  
       --Changing the Division Id for the contact in OpportunityMaster                  
      UPDATE OpportunityMaster SET numDivisionId = v_numDivisionId,numModifiedBy = v_numUserCntId,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));                  
       --Changing the Division Id for the contact in AdditionalContactsInformation                  
      UPDATE AdditionalContactsInformation SET numDivisionId = v_numDivisionId,numModifiedBy = v_numUserCntId,bintModifiedDate = TIMEZONE('UTC',now())
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','));
      UPDATE AdditionalContactsInformation SET bitPrimaryContact = false
      WHERE numContactId IN(SELECT Id FROM SplitIds(v_vcContactIDs,','))
      AND coalesce(bitPrimaryContact,false) = true;
   end if;
   RETURN;
END; $$;


