-- Stored procedure definition script usp_getcorrespondancenew for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getcorrespondancenew(v_FromDate TIMESTAMP,                          
v_toDate TIMESTAMP,                          
v_numContactId NUMERIC,                          
v_vcMessageFrom VARCHAR(200) DEFAULT '' ,                        
v_tintSortOrder NUMERIC DEFAULT NULL,                        
v_SeachKeyword VARCHAR(100) DEFAULT NULL,                        
v_CurrentPage INTEGER DEFAULT NULL,                        
v_PageSize INTEGER DEFAULT NULL,                        
INOUT v_TotRecs NUMERIC  DEFAULT NULL,                        
v_columnName VARCHAR(100) DEFAULT NULL,                        
v_columnSortOrder VARCHAR(50) DEFAULT NULL,                        
v_filter NUMERIC DEFAULT NULL,                        
v_numdivisionId NUMERIC DEFAULT 0,              
v_numDomainID NUMERIC(9,0) DEFAULT 0,              
v_numCaseID NUMERIC(9,0) DEFAULT 0,              
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
v_numOpenRecordID NUMERIC DEFAULT 0,
v_tintMode SMALLINT DEFAULT 0,
v_bitOpenCommu BOOLEAN DEFAULT false,
v_numUserCntID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcUserLoginEmail  VARCHAR(500) DEFAULT '';
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SELECT  replace(vcEmailID,'''','''''') INTO v_vcUserLoginEmail FROM UserMaster where numUserDetailId = v_numUserCntID     LIMIT 1;
   v_vcMessageFrom := replace(v_vcMessageFrom,'''','|');

	
   DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDATA
   (
      RowNumber INTEGER,
      tintCountType SMALLINT,
      numEmailHstrID NUMERIC(18,0),
      DelData VARCHAR(200),
      bintCreatedOn TIMESTAMP,
      date VARCHAR(100),
      Subject TEXT,
      type VARCHAR(100),
      phone VARCHAR(100),
      assignedto VARCHAR(100),
      caseid NUMERIC(18,0),
      vcCasenumber VARCHAR(200),
      caseTimeid NUMERIC(18,0),
      caseExpid NUMERIC(18,0),
      tinttype NUMERIC(18,0),
      dtCreatedDate TIMESTAMP,
      "From" TEXT,
      bitClosedflag BOOLEAN,
      bitHasAttachments BOOLEAN,
      vcBody TEXT,
      InlineEdit VARCHAR(1000),
      numCreatedBy NUMERIC(18,0),
      numOrgTerId NUMERIC(18,0),
      TypeClass VARCHAR(100)
   );


   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numRecordID NUMERIC(18,0),
      bintCreatedOn TIMESTAMP,
      tintRecordType SMALLINT
   );

   INSERT INTO
   tt_TEMP
   SELECT
   HDR.numEmailHstrID
		,HDR.bintCreatedOn
		,1
   FROM
   EmailHistory HDR
   LEFT JOIN
   Correspondence CR
   ON
   CR.numEmailHistoryID = HDR.numEmailHstrID
   WHERE
   HDR.numDomainID = v_numDomainID
   AND coalesce(HDR.bitInvisible,false) = false
   AND (HDR.tintType = 1 OR HDR.tintType = 2 OR HDR.tintType = 5)
   AND (coalesce(v_numOpenRecordID,0) = 0 OR CR.numOpenRecordID = v_numOpenRecordID)
   AND 1 =(CASE
   WHEN v_numdivisionId = 0 AND cast(NULLIF(v_vcMessageFrom,'') as INTEGER) <> 1
   THEN(CASE WHEN (vcFrom ilike CONCAT('%',REPLACE(v_vcMessageFrom,'''',''''''),'%') OR vcTo ilike CONCAT('%',REPLACE(v_vcMessageFrom,'''',''''''),'%')) THEN 1 ELSE 0 END)
   WHEN v_numdivisionId <> 0
   THEN(CASE WHEN EXISTS(SELECT numContactId FROM AdditionalContactsInformation ACI where ACI.numDivisionId = v_numdivisionId AND LENGTH(ACI.vcEmail) > 0 AND (vcFrom ilike CONCAT('%',REPLACE(ACI.vcEmail,'''',''''''),'%') OR vcTo ilike CONCAT('%',REPLACE(ACI.vcEmail,'''',''''''),'%'))) THEN 1 ELSE 0 END)
   WHEN v_numContactId <> 0
   THEN(CASE WHEN EXISTS(SELECT numContactId FROM AdditionalContactsInformation ACI where ACI.numContactId = v_numContactId AND LENGTH(ACI.vcEmail) > 0 AND (vcFrom ilike CONCAT('%',REPLACE(ACI.vcEmail,'''',''''''),'%') OR vcTo ilike CONCAT('%',REPLACE(ACI.vcEmail,'''',''''''),'%'))) THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE
   WHEN v_FromDate IS NOT NULL AND swf_isdate(v_FromDate:: text) = 1
   THEN(CASE WHEN dtReceivedOn >= v_FromDate THEN 1 ELSE 0 END)
   WHEN v_toDate IS NOT NULL AND swf_isdate(v_toDate:: text) = 1
   THEN(CASE WHEN dtReceivedOn <= v_toDate THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE WHEN v_tintMode = 1 THEN(CASE WHEN CR.tintCorrType IN(1,3) OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 2 THEN(CASE WHEN CR.tintCorrType IN(2,4) OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 5 THEN(CASE WHEN CR.tintCorrType = 5 OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 6 THEN(CASE WHEN CR.tintCorrType = 6 OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END);

   INSERT INTO
   tt_TEMP
   SELECT
   C.numCommId
		,(CASE WHEN C.bitTask = 973 THEN dtCreatedDate WHEN C.bitTask = 974 THEN dtCreatedDate WHEN C.bitTask = 971 THEN dtStartTime ELSE dtEndTime END)
		,2
   FROM
   Communication C
   INNER JOIN
   AdditionalContactsInformation  A1
   ON
   C.numContactId = A1.numContactId
   INNER JOIN
   DivisionMaster Div
   ON
   A1.numDivisionId = Div.numDivisionID
   LEFT JOIN
   CommunicationLinkedOrganization clo ON C.numCommId = clo.numCommID
   LEFT JOIN
   AdditionalContactsInformation A2 on A2.numContactId = C.numAssign
   LEFT JOIN
   UserMaster UM
   ON
   A2.numContactId = UM.numUserDetailId and coalesce(UM.bitactivateflag,false) = true
   LEFT JOIN
   Listdetails
   ON
   numActivity = numListID
   LEFT JOIN
   Correspondence CR
   ON
   CR.numCommID = C.numCommId
   WHERE
   C.numDomainID = v_numDomainID
   AND (coalesce(v_numOpenRecordID,0) = 0 OR CR.numOpenRecordID = v_numOpenRecordID)
   AND 1 =(CASE
   WHEN v_numdivisionId = 0 AND v_numContactId <> 0
   THEN(CASE WHEN C.numContactId = v_numContactId THEN 1 ELSE 0 END)
   WHEN v_numdivisionId <> 0 THEN(CASE WHEN (C.numDivisionID = v_numdivisionId OR clo.numDivisionID = v_numdivisionId) THEN 1 ELSE 0 END)
   WHEN v_numCaseID > 0 THEN(CASE WHEN C.CaseId = v_numCaseID THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE
   WHEN v_FromDate IS NOT NULL AND swf_isdate(v_FromDate:: text) = 1
   THEN(CASE WHEN dtStartTime >= v_FromDate OR dtEndTime >= v_FromDate THEN 1 ELSE 0 END)
   WHEN v_toDate IS NOT NULL AND swf_isdate(v_toDate:: text) = 1
   THEN(CASE WHEN dtStartTime <= v_toDate OR dtEndTime <= v_FromDate THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE WHEN v_tintMode = 1 THEN(CASE WHEN CR.tintCorrType IN(1,3) OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 2 THEN(CASE WHEN CR.tintCorrType IN(2,4) OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 5 THEN(CASE WHEN CR.tintCorrType = 5 OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_tintMode = 6 THEN(CASE WHEN CR.tintCorrType = 6 OR coalesce(v_numOpenRecordID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_bitOpenCommu = true  THEN(CASE WHEN coalesce(C.bitClosedFlag,false) = false THEN 1 ELSE 0 END) ELSE 1 END);


   v_TotRecs := coalesce((SELECT COUNT(*) FROM tt_TEMP),0);

   INSERT INTO tt_TEMPDATA(RowNumber
		,tintCountType
		,numEmailHstrID)
   SELECT
   ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC)
		,tintRecordType
		,numRecordID
   FROM
   tt_TEMP
   ORDER BY
   bintCreatedOn DESC;

   UPDATE
   tt_TEMPDATA T1
   SET
   DelData = CONCAT(HDR.numEmailHstrID,'~1~0',HDR.numUserCntId),bintCreatedOn = HDR.dtReceivedOn,
   date = FormatedDateTimeFromDate(dtReceivedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID),Subject = vcSubject,
   type =(CASE WHEN vcFrom ilike CONCAT('%',REPLACE(v_vcUserLoginEmail,'''',''''''),'%') THEN 'Email Out' ELSE 'Email In' END),phone = '',
   assignedto = '',caseid = 0,vcCasenumber = null,caseTimeid = 0,caseExpid = 0,
   tinttype = HDR.tintType,dtCreatedDate = HDR.dtReceivedOn,"From" = CONCAT(vcFrom,',',vcTo),
   bitClosedflag = false,bitHasAttachments = coalesce(HDR.bitHasAttachments,false),
   vcBody =(CASE WHEN LENGTH(SUBSTR(Cast(vcBodyText AS VARCHAR(1000)),1,1000)) > 150 THEN SUBSTR(vcBodyText,0,150) || '...' ELSE SUBSTR(Cast(vcBodyText AS VARCHAR(1000)),1,1000) END),
   InlineEdit = '',numCreatedBy = HDR.numUserCntId,numOrgTerId = 0,
   TypeClass =(CASE WHEN vcFrom ilike CONCAT('%',REPLACE(v_vcUserLoginEmail,'''',''''''),'%') THEN 'redColorLabel' ELSE 'greenColorLabel' END)
   FROM
   EmailHistory HDR WHERE T1.numEmailHstrID = HDR.numEmailHstrID;

   UPDATE
   tt_TEMPDATA T1
   SET
   DelData = CONCAT(C.numCommId,'~2~',C.numCreatedBy),bintCreatedOn = CASE WHEN C.bitTask = 973 THEN C.dtCreatedDate WHEN C.bitTask = 974 THEN C.dtCreatedDate WHEN C.bitTask = 971 THEN dtStartTime ELSE dtEndTime END,
   date =(CASE
   WHEN C.bitTask = 973
   THEN FormatedDateTimeFromDate(C.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)
   WHEN C.bitTask = 974
   THEN FormatedDateTimeFromDate(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)
   WHEN C.bitTask = 971
   THEN CONCAT(FormatedDateTimeFromDate(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID),' ',SUBSTR(TO_CHAR(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'Mon dd yyyy hh:miAM'),length(TO_CHAR(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'Mon dd yyyy hh:miAM')) -7+1),' - ',SUBSTR(TO_CHAR(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Mon dd yyyy hh:miAM'),length(TO_CHAR(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Mon dd yyyy hh:miAM')) -7+1))
   ELSE  FormatedDateFromDate(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)::VARCHAR(50),v_numDomainID) || ' ' || SUBSTR(TO_CHAR(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'Mon dd yyyy hh:miAM'),length(TO_CHAR(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
      'Mon dd yyyy hh:miAM')) -7+1)  || ' - ' || SUBSTR(TO_CHAR(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Mon dd yyyy hh:miAM'),length(TO_CHAR(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Mon dd yyyy hh:miAM')) -7+1)
   END),Subject = SUBSTR(CAST(textDetails AS VARCHAR(8000)),1,8000),
   type = fn_GetListItemName(C.bitTask),phone = CONCAT(A1.vcFirstName,' ',A1.vcLastname,' ',CASE WHEN A1.numPhone <> '' then CONCAT(A1.numPhone,CASE WHEN A1.numPhoneExtension <> '' THEN ' - ' || A1.numPhoneExtension ELSE '' END) ELSE '' END),
   assignedto = CONCAT(A2.vcFirstName,' ',A2.vcLastname),
   caseid = C.CaseId,vcCasenumber =(select  vcCaseNumber from Cases where Cases.numCaseId = C.CaseId LIMIT 1),
   caseTimeid = coalesce(C.caseTimeid,0),caseExpid = coalesce(C.caseExpid,0),
   tinttype = 1,dtCreatedDate = C.dtCreatedDate,"From" = '',bitClosedflag = coalesce(C.bitClosedFlag,false),
   bitHasAttachments = false,vcBody = '',
   InlineEdit = 'id="Tickler~184~False~' || SUBSTR(CAST(C.bitTask AS VARCHAR(30)),1,30) || '~' || SUBSTR(CAST(C.numCommId AS VARCHAR(30)),1,30) || '" class="editable_textarea" onmousemove="bgColor=''lightgoldenRodYellow''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" ',numCreatedBy = C.numCreatedBy,numOrgTerId = coalesce(Div.numTerID,0),
   TypeClass = 'blueColorLabel'
   FROM
   Communication C INNER JOIN AdditionalContactsInformation  A1 ON C.numContactId = A1.numContactId INNER JOIN DivisionMaster Div ON A1.numDivisionId = Div.numDivisionID LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommId = clo.numCommID LEFT JOIN AdditionalContactsInformation A2 ON A2.numContactId = C.numAssign LEFT JOIN UserMaster UM ON A2.numContactId = UM.numUserDetailId and coalesce(UM.bitactivateflag,false) = true LEFT JOIN Listdetails ON numActivity = numListID LEFT JOIN Correspondence CR ON CR.numCommID = C.numCommId WHERE T1.numEmailHstrID = C.numCommId;
	
   open SWV_RefCur for
   SELECT * FROM tt_TEMPDATA ORDER BY RowNumber;

   IF EXISTS(SELECT * FROM pg_tables WHERE tablename = N'tempdb..#TEMPData') then
	
      DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
   end if;
   RETURN;
END; $$;


