-- Stored procedure definition script USP_ManageOpportunityAutomationQueue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageOpportunityAutomationQueue(v_numOppQueueID NUMERIC(18,0) DEFAULT 0,
    v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_numOppId NUMERIC(18,0) DEFAULT 0,
    v_numOppBizDocsId NUMERIC(18,0) DEFAULT 0,
    v_numBizDocStatus NUMERIC(18,0) DEFAULT 0,
    v_numOrderStatus NUMERIC(18,0) DEFAULT 0,
    v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_tintProcessStatus SMALLINT DEFAULT 1,
    v_vcDescription VARCHAR(1000) DEFAULT '',
    v_numRuleID NUMERIC(18,0) DEFAULT 0,
    v_bitSuccess BOOLEAN DEFAULT false,
    v_tintMode SMALLINT DEFAULT NULL,
    v_numShippingReportID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 2 then
        
      open SWV_RefCur for SELECT  
							OAQ.numOppQueueID ,
							OAQ.numDomainID,
							OAQ.numOppId,
							OAQ.numOppBizDocsId,
							OAQ.numBizDocStatus,
							OAQ.numCreatedBy,
							OAQ.dtCreatedDate,
							OAQ.tintProcessStatus,
							OAQ.numRuleID,
							OAQ.numOrderStatus,
							CASE WHEN coalesce(OAQ.numShippingReportID,0) > 0 THEN OAQ.numShippingReportID
      ELSE(SELECT  ShippingReport.numShippingReportID FROM ShippingReport WHERE OBD.numOppBizDocsId = ShippingReport.numOppBizDocId ORDER BY ShippingReport.numShippingReportID DESC LIMIT 1)
      END AS numShippingReportID,
							OBD.numBizDocId,OM.numDivisionId,OM.tintoppstatus,OM.tintshipped,OM.tintopptype FROM OpportunityAutomationQueue OAQ JOIN OpportunityMaster OM ON OAQ.numOppId = OM.numOppId
      LEFT JOIN OpportunityBizDocs OBD ON OAQ.numOppId = OBD.numoppid AND OAQ.numOppBizDocsId = OBD.numOppBizDocsId
      WHERE tintProcessStatus IN(1,2) ORDER BY numOppQueueID LIMIT 25;
   ELSEIF v_tintMode = 3
   then
        
      select   coalesce(numStatus,0) INTO v_numOrderStatus FROM OpportunityMaster WHERE numOppId = v_numOppId;
      IF coalesce(v_numOppBizDocsId,0) > 0 then
			
         select   coalesce(numBizDocStatus,0) INTO v_numBizDocStatus FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId;
      end if;
      INSERT INTO OpportunityAutomationQueueExecution(numOppQueueID,bitSuccess,vcDescription,dtExecutionDate,numRuleID,numOppId,numOppBizDocsId,numOrderStatus,numBizDocStatus) VALUES(v_numOppQueueID,v_bitSuccess,v_vcDescription,TIMEZONE('UTC',now()),v_numRuleID,v_numOppId,v_numOppBizDocsId,v_numOrderStatus,v_numBizDocStatus);
   ELSEIF v_tintMode = 1
   then
        
      IF v_numOppQueueID > 0 then
			
         UPDATE OpportunityAutomationQueue SET tintProcessStatus = v_tintProcessStatus WHERE  numOppQueueID = v_numOppQueueID AND numDomainID = v_numDomainID;
      ELSE
         IF EXISTS(SELECT * FROM OpportunityAutomationQueue WHERE numDomainID = v_numDomainID AND numOppId = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId AND tintProcessStatus = 1 AND coalesce(numRuleID,0) = v_numRuleID) then
				
            UPDATE OpportunityAutomationQueue SET tintProcessStatus = 5 WHERE numDomainID = v_numDomainID AND numOppId = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId AND tintProcessStatus = 1 AND coalesce(numRuleID,0) = v_numRuleID;
         end if;
         IF (coalesce(v_numOppBizDocsId,0) > 0 AND v_numRuleID = 0) then
			
            select   coalesce(numBizDocStatus,0) INTO v_numBizDocStatus FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numOppBizDocsId = v_numOppBizDocsId;
         end if;
         IF v_numBizDocStatus > 0 OR v_numRuleID > 0 OR v_numOrderStatus > 0 then
				
            INSERT INTO OpportunityAutomationQueue(numDomainID, numOppId, numOppBizDocsId, numBizDocStatus, numCreatedBy, dtCreatedDate, tintProcessStatus,numRuleID,numOrderStatus,numShippingReportID)
            SELECT v_numDomainID, v_numOppId, v_numOppBizDocsId, v_numBizDocStatus, v_numUserCntID, TIMEZONE('UTC',now()), v_tintProcessStatus,v_numRuleID,v_numOrderStatus,v_numShippingReportID;
         end if;
      end if;
   end if;
   RETURN;
END; $$;



/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/













