-- Stored procedure definition script USP_MassSalesFulfillmentWM_GetWarehousePriority for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWM_GetWarehousePriority(v_numDomainID NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0)
	,v_tintSourceType SMALLINT
	,v_numCountryID NUMERIC(18,0)
	,v_numStateID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   MassSalesFulfillmentWP.numWarehouseID
   FROM
   MassSalesFulfillmentWM
   INNER JOIN
   MassSalesFulfillmentWMState
   ON
   MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
   INNER JOIN
   MassSalesFulfillmentWP
   ON
   MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
   WHERE
   MassSalesFulfillmentWM.numDomainID = v_numDomainID
   AND MassSalesFulfillmentWM.numOrderSource = v_numOrderSource
   AND MassSalesFulfillmentWM.tintSourceType = v_tintSourceType
   AND MassSalesFulfillmentWM.numCountryID = v_numCountryID
   AND MassSalesFulfillmentWMState.numStateID = v_numStateID
   ORDER BY
   MassSalesFulfillmentWP.intOrder;
END; $$;












