-- Stored procedure definition script usp_DeleteSurveyQuestion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteSurveyQuestion( 
--  		
v_numQID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
--This SP will Delete a question for a Survey.
   AS $$
   DECLARE
   v_numCount  NUMERIC;
BEGIN
   v_numCount := 0;

   select   COUNT(numQuestionID) INTO v_numCount FROM SurveyResponse WHERE numQuestionID = v_numQID;
   IF v_numCount = 0 then
		
      DELETE FROM SurveyAnsMaster
      WHERE numQID = v_numQID;
      DELETE FROM SurveyQuestionMaster
      WHERE numQID = v_numQID;
      open SWV_RefCur for
      SELECT 0;
   ELSE
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;


