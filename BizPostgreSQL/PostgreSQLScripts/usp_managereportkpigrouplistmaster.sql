-- Stored procedure definition script USP_ManageReportKPIGroupListMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageReportKPIGroupListMaster(INOUT v_numReportKPIGroupID NUMERIC(18,0) ,
	v_numDomainID NUMERIC(18,0),	                                       
    v_vcKPIGroupName VARCHAR(200),
    v_vcKPIGroupDescription VARCHAR(500),
    v_tintKPIGroupReportType SMALLINT,
    v_strText TEXT DEFAULT '',
    v_tintMode SMALLINT DEFAULT NULL, --0:Insert/Update KPIGroupList 1:Insert KPIGroupDetail 2: Delete KPIGroupList 3: Delete KPIGroupDetail
    v_numReportID NUMERIC(18,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then --0:Insert/Update KPIGroupList

      IF v_numReportKPIGroupID = 0 then
	
         INSERT INTO ReportKPIGroupListMaster(numDomainID,vcKPIGroupName,vcKPIGroupDescription,tintKPIGroupReportType) VALUES(v_numDomainID, v_vcKPIGroupName, v_vcKPIGroupDescription, v_tintKPIGroupReportType);
		
         v_numReportKPIGroupID := CURRVAL('ReportKPIGroupListMaster_seq');
      ELSE
         UPDATE ReportKPIGroupListMaster
         SET  vcKPIGroupName = v_vcKPIGroupName,vcKPIGroupDescription = v_vcKPIGroupDescription
         WHERE  numDomainID = v_numDomainID AND numReportKPIGroupID = v_numReportKPIGroupID;
      end if;
   ELSEIF v_tintMode = 1
   then --1:Insert KPIGroupDetail

      IF NOT EXISTS(SELECT 1 FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID = v_numReportKPIGroupID AND numReportID = v_numReportID) then
	
         INSERT INTO ReportKPIGroupDetailList(numReportKPIGroupID,numReportID)
         SELECT v_numReportKPIGroupID,v_numReportID;
      end if;
   ELSEIF v_tintMode = 2
   then --2: Delete KPIGroupList

      DELETE FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID = v_numReportKPIGroupID;
      DELETE FROM ReportKPIGroupListMaster WHERE numDomainID = v_numDomainID AND numReportKPIGroupID = v_numReportKPIGroupID;
   ELSEIF v_tintMode = 3
   then --3: Delete KPIGroupDetail

      DELETE FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID = v_numReportKPIGroupID AND numReportID = v_numReportID;
   end if;
   RETURN;
END; $$;

	
	


	


