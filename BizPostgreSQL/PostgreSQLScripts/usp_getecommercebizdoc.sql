-- Stored procedure definition script USP_GetECommerceBizDoc for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetECommerceBizDoc(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numAuthoritativeSales,0) as NUMERIC(18,0)) From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
END; $$;












