-- Stored procedure definition script USP_WorkflowAlertList_UpdateStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,2ndApril2014>  
-- Description: <Description,,To update Alert Message Status>  
-- =============================================  
CREATE OR REPLACE FUNCTION USP_WorkflowAlertList_UpdateStatus(v_numWFAlertID NUMERIC(18,0),  
 v_intAlertStatus  INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE WorkFlowAlertList SET intAlertStatus = v_intAlertStatus WHERE numWFAlertID = v_numWFAlertID;
   RETURN;
END; $$;   

