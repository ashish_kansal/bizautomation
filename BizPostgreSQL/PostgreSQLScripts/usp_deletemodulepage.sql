-- Stored procedure definition script usp_DeleteModulePage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteModulePage(v_numModuleID INTEGER,
	v_numPageID INTEGER   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PageMaster
   WHERE numModuleID = v_numModuleID
   AND numPageID = v_numPageID;
   RETURN;
END; $$;


