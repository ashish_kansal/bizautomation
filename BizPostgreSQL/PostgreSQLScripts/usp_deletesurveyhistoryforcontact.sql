-- Stored procedure definition script usp_DeleteSurveyHistoryForContact for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteSurveyHistoryForContact(v_numSurID NUMERIC,             
 v_numRespondentID NUMERIC)
RETURNS VOID LANGUAGE plpgsql              
--This procedure will delete the survey responses for a contact  
   AS $$
BEGIN
              
 --Delete all Responses  
   DELETE FROM SurveyResponse
   WHERE numParentSurID = v_numSurID
   AND numRespondantID = v_numRespondentID;  
   
 --Delete Respondents Information  
   DELETE FROM SurveyRespondentsChild
   WHERE numSurID = v_numSurID
   AND numRespondantID = v_numRespondentID;  
  
   DELETE FROM SurveyRespondentsMaster
   WHERE numSurID = v_numSurID
   AND numRespondantID = v_numRespondentID;
   RETURN;
END; $$;


