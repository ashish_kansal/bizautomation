-- Stored procedure definition script USP_Subscribers_GetSubscribtionCount for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Subscribers_GetSubscribtionCount(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(CONCAT(coalesce((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID = v_numDomainID AND coalesce(bitActivateFlag,false) = true AND (AuthenticationGroupMaster.tintGroupType = 1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' used / ',coalesce(intNoofUsersSubscribed,0) -coalesce((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID = v_numDomainID AND coalesce(bitActivateFlag,false) = true AND (AuthenticationGroupMaster.tintGroupType = 1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' remaining') as VARCHAR(255)) AS vcFullUsers
		,cast(CONCAT(coalesce((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID = v_numDomainID AND coalesce(bitActivateFlag,false) = true AND AuthenticationGroupMaster.tintGroupType = 4),0),' used / ',
   coalesce(intNoofLimitedAccessUsers,0) -coalesce((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID = v_numDomainID AND coalesce(bitActivateFlag,false) = true AND AuthenticationGroupMaster.tintGroupType = 4),0),' remaining') as VARCHAR(255)) AS vcLimitedAccessUsers
		,cast(CONCAT(coalesce((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID = v_numDomainID AND coalesce(bitPartnerAccess,false) = true),0),' used / ',coalesce(intNoofBusinessPortalUsers,0) -coalesce((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID = v_numDomainID AND coalesce(bitPartnerAccess,false) = true),0),
   ' remaining') as VARCHAR(255)) AS vcBusinessPortalUsers
   FROM
   Subscribers
   WHERE
   numTargetDomainID = v_numDomainID;
END; $$;












