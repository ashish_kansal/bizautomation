CREATE OR REPLACE FUNCTION fn_GetItemCountBasedOnAttribute
(
	v_fieldId NUMERIC(9,0)
	,v_fieldValueId NUMERIC(9,0) DEFAULT 0
	,v_numCategoryId NUMERIC(9,0) DEFAULT 0
	,v_numDomainId NUMERIC(9,0) DEFAULT 0
)
RETURNS VARCHAR(10) LANGUAGE plpgsql
AS $$
	DECLARE
	v_attributeCount  VARCHAR(10);
BEGIN
	select 
		COUNT(*) INTO v_attributeCount 
	FROM
	(
		SELECT DISTINCT
			WHI.numItemID,
            CFVSI.Fld_ID,
            CFVSI.Fld_Value
		FROM
			Item I 
		INNER JOIN ItemCategory IC ON IC.numItemID = I.numItemCode
		INNER JOIN ItemGroupsDTL IGD ON I.numItemGroup = IGD.numItemGroupID
		INNER JOIN CFW_Fld_Master CFM ON IGD.numOppAccAttrID = CFM.Fld_id AND CFM.numDomainID = 1
		LEFT JOIN Listdetails LD ON LD.numListID = CFM.numlistid AND CFM.numDomainID = LD.numDomainid
		INNER JOIN WareHouseItems WHI ON WHI.numItemID = I.numItemCode
		INNER JOIN CFW_Fld_Values_Serialized_Items CFVSI ON CFVSI.RecId = WHI.numWareHouseItemID
		WHERE
			IC.numCategoryID = v_numCategoryId
			AND I.numDomainID = v_numDomainId
			AND LD.numListItemID = CAST(CFVSI.Fld_Value AS NUMERIC)
			AND CFM.Fld_id  =  CFVSI.Fld_ID
			AND CFM.Fld_id = v_fieldId
			AND LD.numListItemID = v_fieldValueId
		GROUP BY 
			WHI.numItemID
			,CFVSI.Fld_ID
			,CFVSI.Fld_Value
	) X;   
	  
	Return v_attributeCount;
END; $$;
 
 
