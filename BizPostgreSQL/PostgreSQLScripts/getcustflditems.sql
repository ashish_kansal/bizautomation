-- Function definition script GetCustFldItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldItems(v_numFldId NUMERIC(18,0)
	,v_pageId SMALLINT
	,v_numRecordId NUMERIC(18,0)
	,v_tintMode SMALLINT  -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
)
RETURNS VARCHAR(300) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(300);
BEGIN
   IF v_pageId = 9 AND coalesce(v_tintMode,0) = 0 then
	
      select   Fld_Value INTO v_vcValue FROM
      CFW_Fld_Values_Serialized_Items WHERE
      Fld_ID = v_numFldId
      AND RecId = v_numRecordId;
   ELSEIF coalesce(v_tintMode,0) = 1
   then
	
      select   Fld_Value INTO v_vcValue FROM
      ItemAttributes WHERE
      Fld_ID = v_numFldId
      AND numItemCode = v_numRecordId;
   end if;

   IF v_vcValue IS NULL then
      v_vcValue := '0';
   end if;  
	
   RETURN v_vcValue;
END; $$;

