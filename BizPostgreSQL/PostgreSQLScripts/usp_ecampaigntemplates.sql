-- Stored procedure definition script USP_ECampaignTemplates for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ECampaignTemplates(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numECampaignID as "numECampaignID",vcECampName as "vcECampName"
   from ECampaign  where numDomainID = v_numDomainID;
END; $$;












