CREATE OR REPLACE FUNCTION DivisionMaster_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_action  CHAR(1) DEFAULT 'I';
   v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
	v_numAssignedTo  NUMERIC(18,0);
   v_numAssignedBy  NUMERIC(18,0);
   v_numWebLead  NUMERIC(18,0);
   v_bitLeadBoxFlg  BOOLEAN;
   v_TempbintCreatedDate TIMESTAMP;
   v_TempnumDivisionId  NUMERIC;
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Organization', OLD.numDivisionID, 'Delete', OLD.numDomainID);
		v_tintWFTriggerOn := 5;
		v_numDomainID := OLD.numDomainID;
		v_numRecordID := OLD.numDivisionID;
		v_numUserCntID := OLD.numModifiedBy;
	ELSIF (TG_OP = 'UPDATE') THEN
		v_tintWFTriggerOn := 2;
		v_numDomainID := NEW.numDomainID;
		v_numRecordID := NEW.numDivisionID;
		v_numUserCntID := NEW.numModifiedBy;

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Organization', NEW.numCompanyID, 'Update', NEW.numDomainID);

		IF NEW.numTerID <> OLD.numTerID then
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',NEW.numDomainID FROM AdditionalContactsInformation WHERE NEW.numDivisionID = AdditionalContactsInformation.numDivisionId;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',NEW.numDomainID FROM OpportunityMaster WHERE NEW.numDivisionID = OpportunityMaster.numDivisionId;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',NEW.numDomainID FROM Communication WHERE NEW.numDivisionID = Communication.numDivisionID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',NEW.numDomainID FROM OpportunityMaster  INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numoppid WHERE NEW.numDivisionID = OpportunityMaster.numDivisionId;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',NEW.numDomainID FROM Cases WHERE NEW.numDivisionID = Cases.numDivisionID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',NEW.numDomainID FROM ProjectsMaster WHERE NEW.numDivisionID = ProjectsMaster.numDivisionId;
         END IF;

		IF OLD.bitActiveInActive <> NEW.bitActiveInActive THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitActiveInActive,');
		END IF;
		IF OLD.numAssignedBy <> NEW.numAssignedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedBy,');
		END IF;
		IF OLD.numAssignedTo <> NEW.numAssignedTo THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedTo,');
		END IF;
		IF OLD.numCampaignID <> NEW.numCampaignID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCampaignID,');
		END IF;
		IF OLD.numCompanyDiff <> NEW.numCompanyDiff THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCompanyDiff,');
		END IF;
		IF OLD.vcCompanyDiff <> NEW.vcCompanyDiff THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcCompanyDiff,');
		END IF;
		IF OLD.numCurrencyID <> NEW.numCurrencyID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCurrencyID,');
		END IF;
		IF OLD.numFollowUpStatus <> NEW.numFollowUpStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numFollowUpStatus,');
		END IF;
		IF OLD.numGrpID <> NEW.numGrpID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numGrpID,');
		END IF;
		IF OLD.bintCreatedDate <> NEW.bintCreatedDate THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bintCreatedDate,');
		END IF;
		IF OLD.vcComFax <> NEW.vcComFax THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcComFax,');
		END IF;
		IF OLD.bitPublicFlag <> NEW.bitPublicFlag THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitPublicFlag,');
		END IF;
		IF OLD.tintCRMType <> NEW.tintCRMType THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintCRMType,');
		END IF;
		IF OLD.bitNoTax <> NEW.bitNoTax THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitNoTax,');
		END IF;
		IF OLD.numTerID <> NEW.numTerID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numTerID,');
		END IF;
		IF OLD.numStatusID <> NEW.numStatusID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numStatusID,');
		END IF;
		IF OLD.vcComPhone <> NEW.vcComPhone THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcComPhone,');
		END IF;
		IF OLD.numAccountClassID <> NEW.numAccountClassID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAccountClassID,');
		END IF;
		IF OLD.numBillingDays <> NEW.numBillingDays THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numBillingDays,');
		END IF;
		IF OLD.tintPriceLevel <> NEW.tintPriceLevel THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'tintPriceLevel,');
		END IF;
		IF OLD.numDefaultPaymentMethod <> NEW.numDefaultPaymentMethod THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numDefaultPaymentMethod,');
		END IF;
		IF OLD.intShippingCompany <> NEW.intShippingCompany THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intShippingCompany,');
		END IF;
		IF OLD.numDefaultShippingServiceID <> NEW.numDefaultShippingServiceID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numDefaultShippingServiceID,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');
	ELSIF (TG_OP = 'INSERT') THEN
		v_tintWFTriggerOn := 1;
		v_numDomainID := NEW.numDomainID;
		v_numRecordID := NEW.numDivisionID;
		v_numUserCntID := NEW.numCreatedBy;

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Organization', NEW.numCompanyID, 'Insert', NEW.numDomainID);
	END IF;
	
	SELECT bintCreatedDate INTO v_TempbintCreatedDate FROM DivisionMaster WHERE numDivisionID = v_numRecordID;	
	
   IF v_tintWFTriggerOn = 1 then
	
      IF EXISTS(SELECT numDivisionID FROM DivisionMaster_TempDateFields WHERE numDivisionID = v_numRecordID) then
		
         IF (v_TempbintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND v_TempbintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day') then
			
            UPDATE DivisionMaster_TempDateFields
            SET bintCreatedDate = v_TempbintCreatedDate
            WHERE numDivisionID = v_numRecordID;
         ELSE
            DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = v_numRecordID;
         end if;
      ELSE
         INSERT INTO DivisionMaster_TempDateFields(numDivisionID, numDomainID, bintCreatedDate)
         SELECT numDivisionID,numDomainID, bintCreatedDate
         FROM DivisionMaster
         WHERE (
					(CAST(bintCreatedDate AS timestamp) >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND CAST(bintCreatedDate AS timestamp) <= TIMEZONE('UTC',now())+INTERVAL '90 day'))
         AND numDivisionID = v_numRecordID;
      end if;
   ELSEIF v_tintWFTriggerOn = 2
   then
	
      SELECT numDivisionID INTO v_TempnumDivisionId FROM DivisionMaster_TempDateFields WHERE numDivisionID = v_numRecordID;
      IF(v_TempnumDivisionId IS NOT NULL) then
		
         IF (v_TempbintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-30 day' AND v_TempbintCreatedDate <= TIMEZONE('UTC',now())+INTERVAL '90 day') then
			
            UPDATE DivisionMaster_TempDateFields
            SET bintCreatedDate = v_TempbintCreatedDate
            WHERE numDivisionID = v_numRecordID;
         ELSE
            DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = v_numRecordID;
         end if;
      end if;
   ELSEIF v_tintWFTriggerOn = 5
   then
	
      DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = v_numRecordID;
   end if;	

	select   numAssignedTo, numAssignedBy, numGrpId, bitLeadBoxFlg INTO v_numAssignedTo,v_numAssignedBy,v_numWebLead,v_bitLeadBoxFlg from DivisionMaster where numDivisionID = v_numRecordID;

	IF (v_vcColumnsUpdated = 'numAssignedBy,numAssignedTo') then
	
      IF (v_numWebLead = 1) then --web Leads
		
         IF	(v_bitLeadBoxFlg = true) then --(through System)
			
            IF(v_numAssignedTo = 0 AND v_numAssignedBy = 0) then
				
               v_tintWFTriggerOn := 1;
            ELSE
               v_tintWFTriggerOn := 2;
            end if;
         ELSE
            IF(v_numAssignedTo > 0 AND v_numAssignedBy > 0) then
				
               v_tintWFTriggerOn := 1;
            ELSE
               v_tintWFTriggerOn := 2;
            end if;
         end if;
      ELSE
         IF	(v_numAssignedTo = 0 AND v_numAssignedBy = 0) then
			
            v_tintWFTriggerOn := 1;
         ELSE
            v_tintWFTriggerOn := 2;
         end if;
      end if;
   end if;

	PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 68,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER DivisionMaster_IUD AFTER INSERT OR UPDATE OR DELETE ON DivisionMaster FOR EACH ROW WHEN (pg_trigger_depth() < 1) EXECUTE PROCEDURE DivisionMaster_IUD_TrFunc();


