-- Stored procedure definition script USP_EcommerceRelationshipProfile_GetBySiteID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EcommerceRelationshipProfile_GetBySiteID(v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   ID
		,GetListIemName(numRelationship) AS vcRelationship
		,GetListIemName(numProfile) AS vcProfile
		,COALESCE((SELECT string_agg(vcData,', ')
      FROM
      ECommerceItemClassification EIC
      INNER JOIN
      Listdetails
      ON
      EIC.numItemClassification = Listdetails.numListItemID
      WHERE
      EIC.numECommRelatiionshipProfileID = ERP.ID),'') AS vcItemClassifications
   FROM
   EcommerceRelationshipProfile ERP
   WHERE
   numSiteID = v_numSiteID;
END; $$;












