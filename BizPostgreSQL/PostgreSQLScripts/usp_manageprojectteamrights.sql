-- Stored procedure definition script USP_ManageProjectTeamRights for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageProjectTeamRights(v_strContactId TEXT,
v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_numProId NUMERIC(9,0) DEFAULT 0,
v_tintUserType SMALLINT DEFAULT NULL,--1:Internal User 2:External User
v_tintMode SMALLINT DEFAULT NULL, --1: ADD/DELETE/UPDATE 2:ADD 3:Delete
v_numUserCntID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF SUBSTR(CAST(v_strContactId AS VARCHAR(10)),1,10) <> '' then
      DROP TABLE IF EXISTS tt_TEMPUSER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPUSER AS
         SELECT  X.numContactId,X.numRights,X.numProjectRole 
         FROM
		 XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strContactId AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numContactId NUMERIC(18,0) PATH 'numContactId',
				numRights NUMERIC(18,0) PATH 'numRights',
				numProjectRole NUMERIC(18,0) PATH 'numProjectRole'
		) X;
		
      IF v_tintMode = 1 or v_tintMode = 2 then

         if v_tintMode = 1 then

            delete from ProjectTeamRights where numProId = v_numProId and tintUserType = v_tintUserType
            and numContactId not in(select numContactId from tt_TEMPUSER);
            update ProjectTeamRights PTR set numRights = X.numRights,numProjectRole = X.numProjectRole from tt_TEMPUSER X where PTR.numContactId = X.numContactId AND(PTR.numProId = v_numProId and tintUserType = v_tintUserType);
         end if;
         INSERT INTo ProjectTeamRights(numContactId, numRights,numProjectRole,numProId,tintUserType)
         SELECT  X.numContactId,X.numRights,X.numProjectRole,v_numProId,v_tintUserType
         from tt_TEMPUSER X where numContactId not in(select numContactId from ProjectTeamRights
            where numProId = v_numProId and tintUserType = v_tintUserType);
      end if;
      IF v_tintMode = 3 then

         Update StagePercentageDetails set numAssignTo = v_numUserCntID where numProjectid = v_numProId
         and numAssignTo in(select numContactId from tt_TEMPUSER);
         delete from ProjectTeamRights where numProId = v_numProId and tintUserType = v_tintUserType
         and numContactId in(select numContactId from tt_TEMPUSER);
      end if;
     
   end if;
              








   RETURN;
END; $$;


