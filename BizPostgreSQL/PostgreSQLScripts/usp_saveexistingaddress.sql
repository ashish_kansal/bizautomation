-- Stored procedure definition script USP_SaveExistingAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveExistingAddress(v_numNewDomainId NUMERIC(9,0),        
v_numOldDomainId NUMERIC(9,0),        

v_numNewUserId NUMERIC(9,0),        
v_numOldUserId NUMERIC(9,0),        

v_numNewDivisionId NUMERIC(9,0),
v_numOldDivisionId NUMERIC(9,0),

v_vcAddressType VARCHAR(15),
v_numPrimaryContactCheck VARCHAR(10))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcBillStreet  VARCHAR(50);
   v_vcBillCity  VARCHAR(50);
   v_vcBilState  NUMERIC(18,0);
   v_vcBillPostCode  VARCHAR(15);
   v_vcBillCountry  NUMERIC(18,0);

   v_numContactId  NUMERIC(18,0);
BEGIN
   BEGIN      
	-- Fetching Existing Company Address.
      if (v_numPrimaryContactCheck = 'Yes') then
		
         select   numContactId INTO v_numContactId from AdditionalContactsInformation where numDomainID = v_numNewDomainId and numDivisionId = v_numNewDivisionId;
         select   vcStreet, vcCity, vcPostalCode, numState, numCountry INTO v_vcBillStreet,v_vcBillCity,v_vcBillPostCode,v_vcBilState,v_vcBillCountry FROM AddressDetails WHERE numDomainID = v_numNewDomainId AND numRecordID = v_numContactId AND tintAddressOf = 1 AND tintAddressType = 0 AND bitIsPrimary = true;
      end if;
      if (v_numPrimaryContactCheck = 'No') then
	
         select   vcStreet, vcCity, numState, vcPostalCode, numCountry INTO v_vcBillStreet,v_vcBillCity,v_vcBilState,v_vcBillPostCode,v_vcBillCountry FROM     AddressDetails WHERE    numDomainID = v_numNewDomainId
         AND numRecordID = v_numNewDivisionId
         AND tintAddressOf = 2
         AND tintAddressType = 1
         AND bitIsPrimary = true;
      end if;
	-- End of the Code.
	
	-- Updating Existing Company Address.
      if (v_vcAddressType = 'BillTo') then
	
         update AddressDetails set vcStreet = v_vcBillStreet,vcCity = v_vcBillCity,numState = v_vcBilState,vcPostalCode = v_vcBillPostCode,
         numCountry = v_vcBillCountry
		 
         WHERE    numDomainID = v_numOldDomainId
         AND numRecordID = v_numOldDivisionId
         AND tintAddressOf = 2
         AND tintAddressType = 1
         AND bitIsPrimary = true;
      end if;
      if (v_vcAddressType = 'ShipTo') then
	
         update AddressDetails set vcStreet = v_vcBillStreet,vcCity = v_vcBillCity,numState = v_vcBilState,vcPostalCode = v_vcBillPostCode,
         numCountry = v_vcBillCountry
         where numDomainID = v_numOldDomainId
         AND numRecordID = v_numOldDivisionId
         AND tintAddressOf = 2
         AND tintAddressType = 2
         AND bitIsPrimary = true;
      end if;
	-- End of the Code.
   END;
   RETURN;
END; $$;  



