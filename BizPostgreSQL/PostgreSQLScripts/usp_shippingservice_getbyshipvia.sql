-- Stored procedure definition script USP_ShippingService_GetByShipVia for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ShippingService_GetByShipVia(v_numDomainID NUMERIC(9,0) DEFAULT 0  ,
v_numShipViaID NUMERIC(18,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ShippingService.numDomainID AS "numDomainID"
		,ShippingService.numShippingServiceID AS "numShippingServiceID"
		,ShippingService.vcShipmentService AS "vcShipmentService"
		,coalesce(ShippingServiceAbbreviations.vcAbbreviation,'') AS "vcAbbreviation"
   FROM
   ShippingService
   LEFT JOIN
   ShippingServiceAbbreviations
   ON
   ShippingService.numShippingServiceID = ShippingServiceAbbreviations.numShippingServiceID
   WHERE
		(ShippingService.numDomainID = v_numDomainID OR coalesce(ShippingService.numDomainID,0) = 0)
   AND (numShipViaID = v_numShipViaID OR coalesce(v_numShipViaID,0) = 0);
END; $$;    












