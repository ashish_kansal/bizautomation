CREATE OR REPLACE FUNCTION Activity_Sel
(
	v_ResourceName	VARCHAR(64)	-- resource name (will be used to lookup ID)
	,INOUT SWV_RefCur Refcursor
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ResourceID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_ResourceName) = true then
      v_ResourceID := CAST(v_ResourceName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if; 
	--
	-- Second step is to retrieve the Activities organized (or owned by) this resource.
	-- They will be the ones in the ActivityResource table where ResourceID is the
	-- key number I've just identified. Exclude variances by only retrieving root
	-- Activities (those with a null OriginalStartDateTimeUTC). See Variance_Sel.
	--
	OPEN SWV_RefCur FOR 
	SELECT
		Activity.AllDayEvent,
		Activity.ActivityDescription,
		Activity.Duration,
		Activity.Location,
		Activity.ActivityID,
		Activity.StartDateTimeUtc,
		cast(Activity.Subject as VARCHAR(255)),
		Activity.EnableReminder,
		Activity.ReminderInterval,
		Activity.ShowTimeAs,
		Activity.Importance,
		Activity.Status,
		Activity.RecurrenceID,
		Activity.VarianceID,
		ActivityResource.ResourceID,
		Activity._ts,
		Activity.itemId,
		Activity.ChangeKey
	FROM
		Activity 
	INNER JOIN 
		ActivityResource 
	ON 
		Activity.ActivityID = ActivityResource.ActivityID
	WHERE
		(ActivityResource.ResourceID = v_ResourceID AND  Activity.OriginalStartDateTimeUtc IS NULL);
END; $$;


