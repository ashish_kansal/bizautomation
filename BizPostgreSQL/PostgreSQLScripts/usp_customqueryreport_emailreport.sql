-- Stored procedure definition script USP_CustomQueryReport_EmailReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_EmailReport(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;

   v_bitAddForEmail  BOOLEAN DEFAULT false;
   v_numReportID  NUMERIC(18,0);
   v_numReportDomainID  NUMERIC(18,0);
   v_dtCreatedDate  DATE;
   v_dtFiscalStartDate  DATE;
   v_tintEmailFrequency  SMALLINT;
   v_dtDateToSend  DATE;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numReportID NUMERIC(18,0),
      numReportDomainID NUMERIC(18,0),
      vcReportName VARCHAR(300),
      vcEmailTo VARCHAR(1000),
      tintEmailFrequency SMALLINT,
      vcCSS TEXT,
      dtCreatedDate TIMESTAMP
   );
   DROP TABLE IF EXISTS tt_TEMPFINAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFINAL
   (
      numReportID NUMERIC(18,0),
      numReportDomainID NUMERIC(18,0),
      vcReportName VARCHAR(300),
      vcEmailTo VARCHAR(1000),
      tintEmailFrequency SMALLINT,
      vcCSS TEXT,
      dtDateToSend DATE
   );
   INSERT INTO tt_TEMP(numReportID,
		numReportDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate)
   SELECT
   numReportID,
		numDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate
   FROM
   CustomQueryReport
   WHERE
   LENGTH(coalesce(vcEmailTo,'')) > 0
   AND coalesce(tintEmailFrequency,0) > 0;

   select   COUNT(*) INTO v_COUNT FROM tt_TEMP;

   WHILE v_i <= v_COUNT LOOP
      v_bitAddForEmail := false;
      select   numReportID, numReportDomainID, tintEmailFrequency, CAST(dtCreatedDate AS DATE) INTO v_numReportID,v_numReportDomainID,v_tintEmailFrequency,v_dtCreatedDate FROM
      tt_TEMP WHERE
      ID = v_i;
      v_dtFiscalStartDate := CAST(GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP)::INT,v_numReportDomainID) AS DATE);
      IF v_tintEmailFrequency = 1 then --DAILY
		
         v_dtDateToSend := CAST(LOCALTIMESTAMP  AS DATE);
         IF(SELECT
         COUNT(*)
         FROM
         CustomQueryReportEmail
         WHERE
         numReportID = v_numReportID
         AND tintEmailFrequency = v_tintEmailFrequency
         AND dtDateToSend =(CAST(LOCALTIMESTAMP  AS DATE))) = 0 then
			
            v_bitAddForEmail := true;
         end if;
      ELSEIF v_tintEmailFrequency = 2
      then --WEEKLY
		
         v_dtDateToSend := CAST(get_week_start(LOCALTIMESTAMP) AS DATE);
         IF v_dtCreatedDate < v_dtDateToSend AND ((CAST(LOCALTIMESTAMP  AS DATE) = v_dtDateToSend) OR(SELECT
         COUNT(*)
         FROM
         CustomQueryReportEmail
         WHERE
         numReportID = v_numReportID
         AND tintEmailFrequency = v_tintEmailFrequency
         AND dtDateToSend = v_dtDateToSend) = 0) then
			
            v_bitAddForEmail := true;
         end if;
      ELSEIF v_tintEmailFrequency = 3
      then --MONTHLY
		
         v_dtDateToSend := CAST(get_month_start(LOCALTIMESTAMP) AS DATE);
         IF v_dtCreatedDate < v_dtDateToSend AND ((CAST(LOCALTIMESTAMP  AS DATE) = v_dtDateToSend) OR(SELECT
         COUNT(*)
         FROM
         CustomQueryReportEmail
         WHERE
         numReportID = v_numReportID
         AND tintEmailFrequency = v_tintEmailFrequency
         AND dtDateToSend = v_dtDateToSend) = 0) then
			
            v_bitAddForEmail := true;
         end if;
      ELSEIF v_tintEmailFrequency = 4
      then --YEARLY
		
         v_dtDateToSend := v_dtFiscalStartDate;
         IF v_dtCreatedDate < v_dtDateToSend AND ((CAST(LOCALTIMESTAMP  AS DATE) = v_dtDateToSend) OR(SELECT
         COUNT(*)
         FROM
         CustomQueryReportEmail
         WHERE
         numReportID = v_numReportID
         AND tintEmailFrequency = v_tintEmailFrequency
         AND dtDateToSend = v_dtDateToSend) = 0) then
			
            v_bitAddForEmail := true;
         end if;
      end if;
      IF v_bitAddForEmail = true then
		
         INSERT INTO
         tt_TEMPFINAL
         SELECT
         numReportID,
				numReportDomainID,
				vcReportName,
				vcEmailTo,
				tintEmailFrequency,
				coalesce(vcCSS,''),
				v_dtDateToSend
         FROM
         tt_TEMP
         WHERE
         ID = v_i;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT * FROM tt_TEMPFINAL;
END; $$;











