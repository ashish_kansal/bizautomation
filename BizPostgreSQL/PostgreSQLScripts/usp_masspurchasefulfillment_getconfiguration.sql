-- Stored procedure definition script USP_MassPurchaseFulfillment_GetConfiguration for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_GetConfiguration(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID) then
	
      open SWV_RefCur for
      SELECT
			coalesce(bitGroupByOrderForReceive,false) AS "bitGroupByOrderForReceive"
			,coalesce(bitGroupByOrderForPutAway,false) AS "bitGroupByOrderForPutAway"
			,coalesce(bitGroupByOrderForBill,false) AS "bitGroupByOrderForBill"
			,coalesce(bitGroupByOrderForClose,false) AS "bitGroupByOrderForClose"
			,coalesce(bitReceiveBillOnClose,false) AS "bitReceiveBillOnClose"
			,coalesce(tintScanValue,1) AS "tintScanValue"
      FROM
      MassPurchaseFulfillmentConfiguration
      WHERE
      numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntID;
   ELSE
      open SWV_RefCur for
      SELECT
      false AS "bitGroupByOrderForReceive"
			,false AS "bitGroupByOrderForPutAway"
			,false AS "bitGroupByOrderForBill"
			,false AS "bitGroupByOrderForClose"
			,false AS "bitReceiveBillOnClose"
			,1 AS "tintScanValue";
   end if;
   RETURN;
END; $$;


