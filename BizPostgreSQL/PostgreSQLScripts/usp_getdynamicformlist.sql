-- Stored procedure definition script usp_getDynamicFormList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getDynamicFormList(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_tintFlag SMALLINT DEFAULT 0,
v_bitWorkFlow BOOLEAN DEFAULT false,
v_bitAllowGridColor BOOLEAN DEFAULT false,
v_numBizFormModuleID INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitAllowGridColor = true then

      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated
      from dynamicFormMaster DM where coalesce(bitAllowGridColor,false) = v_bitAllowGridColor order by DM.numFormID;
   ELSEIF v_bitWorkFlow = true
   then

      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated
      from dynamicFormMaster DM where coalesce(bitWorkFlow,false) = v_bitWorkFlow order by DM.numFormID;
   ELSEIF v_tintFlag = 1 OR v_tintFlag = 3
   then

      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated
      from dynamicFormMaster DM
      where tintFlag = v_tintFlag
      order by DM.numFormID;
   ELSEIF v_tintFlag = 6
   then

      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated
      from dynamicFormMaster DM
      where tintFlag = 1  AND DM.numFormID IN(34,35,12,36,10,13,40,41,38,39)
      order by DM.numFormID;
   ELSEIF (v_tintFlag = 5)
   then

      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated, coalesce(vcAdditionalParam,'') as vcAdditionalParam, numGrpId
      from dynamicFormMaster DM
      left join DynamicFormMasterParam  DMP
      on DMP.numFormID = DM.numFormID and DMP.numDomainID = v_numDomainID
      where bitDeleted = false AND (numBizFormModuleID = v_numBizFormModuleID OR coalesce(v_numBizFormModuleID,0) = 0)
      AND tintFlag = 0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
      AND DM.numFormID IN(34,35,12,36,10,13,40,41,38,39)
      order by DM.numFormID;
   ELSE
      open SWV_RefCur for
      Select DM.numFormID, vcFormName, cCustomFieldsAssociated, cAOIAssociated, coalesce(vcAdditionalParam,'') as vcAdditionalParam, numGrpId, DM.tintPageType
      from dynamicFormMaster DM
      left join DynamicFormMasterParam  DMP
      on DMP.numFormID = DM.numFormID and DMP.numDomainID = v_numDomainID
      where bitDeleted = false AND (numBizFormModuleID = v_numBizFormModuleID OR coalesce(v_numBizFormModuleID,0) = 0)
      AND tintFlag = 0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
      order by DM.numFormID;
   end if;
   RETURN;
END; $$;


