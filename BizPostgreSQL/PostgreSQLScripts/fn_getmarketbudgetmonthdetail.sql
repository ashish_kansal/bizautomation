-- Function definition script fn_GetMarketBudgetMonthDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetMarketBudgetMonthDetail(v_numMarketId NUMERIC(9,0),v_tintMonth SMALLINT,v_intYear NUMERIC(9,0),v_numDomainId NUMERIC(9,0),v_numListItemID NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  VARCHAR(10);
BEGIN
   select   MBD.monAmount INTO v_monAmount From MarketBudgetMaster MBM
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId Where MBM.numDomainID = v_numDomainId And MBD.numMarketId = v_numMarketId
   And MBD.numListItemID = v_numListItemID and MBD.tintMonth = v_tintMonth And MBD.intYear = v_intYear;              
   if v_monAmount = '0.00' then 
      v_monAmount := '';
   end if;              
   Return v_monAmount;
END; $$;

