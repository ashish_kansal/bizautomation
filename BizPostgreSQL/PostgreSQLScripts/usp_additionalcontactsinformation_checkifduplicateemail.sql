CREATE OR REPLACE FUNCTION USP_AdditionalContactsInformation_CheckIfDuplicateEmail(v_numDomainID NUMERIC(18,0)
	,v_numContactID NUMERIC(18,0)
	,v_vcEmail VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF LENGTH(coalesce(v_vcEmail,'')) > 0 AND EXISTS(SELECT 
   numContactId
   FROM
   AdditionalContactsInformation
   INNER JOIN
   DivisionMaster
   ON
   AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   AdditionalContactsInformation.numDomainID = v_numDomainID
   AND LOWER(vcEmail) = LOWER(coalesce(v_vcEmail,''))
   AND AdditionalContactsInformation.numContactId <> coalesce(v_numContactID,0) LIMIT 1) then
	
      open SWV_RefCur for
      SELECT true;
   ELSE
      open SWV_RefCur for
      SELECT false;
   end if;
   RETURN;
END; $$;


