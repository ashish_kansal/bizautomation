-- Stored procedure definition script USP_GetDefaultAccounts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDefaultAccounts(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(AC.numChargeId as VARCHAR(255)),
                cast(AC.numChargeTypeId as VARCHAR(255)),
                cast(coalesce(AC.numAccountID,0) as VARCHAR(255)) AS numAccountID,
                cast(AC.numDomainID as VARCHAR(255)),
                cast(ACT.chChargeCode as VARCHAR(255))
   FROM    AccountingCharges AC
   INNER JOIN AccountingChargeTypes ACT ON AC.numChargeTypeId = ACT.numChargeTypeId
   WHERE   numDomainID = v_numDomainID;
END; $$;












