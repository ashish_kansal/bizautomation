-- Stored procedure definition script USP_ServiceItemList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ServiceItemList(v_numDomainID NUMERIC(9,0) DEFAULT 0 ,   
v_ItemType CHAR(10) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numItemCode as "numItemCode",vcItemName as "vcItemName" FROM Item WHERE numDomainID = v_numDomainID AND charItemType = v_ItemType
   order by vcItemName desc;
END; $$;












