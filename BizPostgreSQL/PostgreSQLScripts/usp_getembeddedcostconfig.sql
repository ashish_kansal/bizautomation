-- Stored procedure definition script USP_GetEmbeddedCostConfig for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmbeddedCostConfig(v_numOppBizDocID NUMERIC,
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numLastViewedCostCatID as VARCHAR(255))
   FROM    EmbeddedCostConfig
   WHERE   numOppBizDocID = v_numOppBizDocID
   AND numDomainID = v_numDomainID;
END; $$;
    
 
-- exec USP_GetEmbeddedCostDefaults @numEmbeddedCostID=0,@numCostCatID=16572,@numDomainID=1












