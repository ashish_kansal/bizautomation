-- Stored procedure definition script USP_AssembledItem_GetByID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AssembledItem_GetByID(v_numDomainID NUMERIC(18,0),
	v_ID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   AI.ID,
		AI.numAssembledQty,
		AI.monAverageCost,
		I.vcItemName,
		I.numAssetChartAcntId
   FROM
   AssembledItem AI
   INNER JOIN
   Item I
   ON
   AI.numItemCode = I.numItemCode
   WHERE
   AI.numDomainID = v_numDomainID
   AND AI.ID = v_ID;

   open SWV_RefCur2 for
   SELECT
   AIC.*,
		I.vcItemName,
		I.numAssetChartAcntId
   FROM
   AssembledItemChilds AIC
   INNER JOIN
   Item I
   ON
   AIC.numItemCode = I.numItemCode
   WHERE
   numAssembledItemID = v_ID;
   RETURN;
END; $$;




