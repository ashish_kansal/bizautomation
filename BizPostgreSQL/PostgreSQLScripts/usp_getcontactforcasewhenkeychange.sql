-- Stored procedure definition script usp_GetContactForCaseWhenKeyChange for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactForCaseWhenKeyChange(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numContactID NUMERIC(9,0) DEFAULT 0  
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     AdditionalContactsInformation.vcFirstName, AdditionalContactsInformation.vcLastname,AdditionalContactsInformation.numContactId
   FROM AdditionalContactsInformation INNER JOIN OpportunityMaster
   ON AdditionalContactsInformation.numContactId = OpportunityMaster.numContactId
   WHERE AdditionalContactsInformation.numDomainID = v_numDomainID
   AND AdditionalContactsInformation.numContactId = v_numContactID;
END; $$;












