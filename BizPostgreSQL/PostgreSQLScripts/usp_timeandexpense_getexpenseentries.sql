DROP FUNCTION IF EXISTS USP_TimeAndExpense_GetExpenseEntries;

CREATE OR REPLACE FUNCTION USP_TimeAndExpense_GetExpenseEntries(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_vcEmployeeName VARCHAR(200)
	,v_vcTeams TEXT
	,v_tintPayrollType SMALLINT
	,v_ClientTimeZoneOffset INTEGER
	,v_vcSortColumn VARCHAR(200)
	,v_vcSortOrder VARCHAR(4)
	,v_tintUserRightType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGetExpenseEntries CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetExpenseEntries
   (
      numUserCntID NUMERIC(18,0),
      vcEmployee VARCHAR(200),
      vcTeam VARCHAR(200),
      monExpense DECIMAL(20,5),
      vcPayrollType VARCHAR(50)
   );
   INSERT INTO tt_TEMPGetExpenseEntries(numUserCntID
		,vcEmployee
		,vcTeam
		,monExpense
		,vcPayrollType)
   SELECT
   UserMaster.numUserDetailId
		,CONCAT(coalesce(AdditionalContactsInformation.vcFirstName,'-'),' ',coalesce(AdditionalContactsInformation.vcLastname,'-'))
		,CAST(GetListIemName(AdditionalContactsInformation.numTeam) AS VARCHAR(200))
		,TEMPExpense.monExpense
		,CAST((CASE coalesce(tintPayrollType,0) WHEN 2 THEN 'Salary' WHEN 1 THEN 'Hourly' ELSE '' END) AS VARCHAR(50))
   FROM
   UserMaster
   INNER JOIN
   AdditionalContactsInformation
   ON
   UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
   LEFT JOIN LATERAL(SELECT
      coalesce(SUM(monExpense),0) AS monExpense
      FROM
      fn_GetPayrollEmployeeExpense(v_numDomainID,CAST(UserMaster.numUserDetailId AS INTEGER),v_dtFromDate::DATE,v_dtToDate::DATE,v_ClientTimeZoneOffset)) TEMPExpense on TRUE
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND coalesce(UserMaster.bitactivateflag,false) = true
   AND (coalesce(v_vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName ilike CONCAT('%',v_vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastname ilike CONCAT('%',v_vcEmployeeName,'%'))
   AND (coalesce(v_vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN(SELECT Id FROM SplitIDs(v_vcTeams,',')))
   AND 1 =(CASE WHEN v_tintPayrollType = 0 THEN 1 ELSE(CASE WHEN coalesce(UserMaster.tintPayrollType,0) = v_tintPayrollType THEN 1 ELSE 0 END) END)
   AND 1 =(CASE coalesce(v_tintUserRightType,0)
   WHEN 3 THEN 1
   WHEN 1 THEN(CASE WHEN UserMaster.numUserDetailId = v_numUserCntID THEN 1 ELSE 0 END)
   ELSE 0
   END); 

   IF coalesce(v_vcSortColumn,'') = '' then
	
      v_vcSortColumn := 'vcEmployee';
      v_vcSortOrder := 'ASC';
   end if;

   IF coalesce(v_vcSortColumn,'') = '' then
	
      v_vcSortOrder := 'ASC';
   end if;


   open SWV_RefCur for SELECT * FROM
   tt_TEMPGetExpenseEntries
   ORDER BY
   CASE WHEN v_vcSortColumn = 'vcEmployee' AND v_vcSortOrder = 'ASC' then vcEmployee END ASC,CASE WHEN v_vcSortColumn = 'vcEmployee' AND v_vcSortOrder = 'DESC' then vcEmployee END DESC,CASE WHEN v_vcSortColumn = 'vcTeam' AND v_vcSortOrder = 'ASC' then vcTeam END ASC,CASE WHEN v_vcSortColumn = 'vcTeam' AND v_vcSortOrder = 'DESC' then vcTeam END DESC,CASE WHEN v_vcSortColumn = 'vcPayrollType' AND v_vcSortOrder = 'ASC' then vcPayrollType END ASC,
   CASE WHEN v_vcSortColumn = 'vcPayrollType' AND v_vcSortOrder = 'DESC' then vcPayrollType END DESC;
END; $$;












