-- Function definition script fn_GetContactNameFromUserID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetContactNameFromUserID(v_numUserID NUMERIC)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetVal  VARCHAR(1000);
BEGIN
   select   vcFirstName || ' ' || vcLastname INTO v_RetVal from AdditionalContactsInformation
   join UserMaster on numUserDetailId = numContactId where numuserid = v_numUserID; 
   RETURN coalesce(v_RetVal,'-');
END; $$;

