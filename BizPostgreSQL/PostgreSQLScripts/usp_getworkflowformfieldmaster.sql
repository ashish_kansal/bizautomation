DROP FUNCTION IF EXISTS USP_GetWorkFlowFormFieldMaster;

CREATE OR REPLACE FUNCTION USP_GetWorkFlowFormFieldMaster(v_numDomainID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcLocationID  VARCHAR(100);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPFIELD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELD
   (
      numFieldID NUMERIC,
      vcFieldName VARCHAR(50),
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldDataType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC,
      bitCustom BOOLEAN,
      vcLookBackTableName VARCHAR(50),
      bitAllowFiltering BOOLEAN,
      bitAllowEdit BOOLEAN,
      vcGroup VARCHAR(100),
      intWFCompare INTEGER
   );

--Regular Fields
   INSERT INTO tt_TEMPFIELD
   SELECT numFieldID,vcFieldName,
		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
		   vcListItemType,numListID,CAST(0 AS BOOLEAN) AS bitCustom,
		   (CASE WHEN vcAssociatedControlType = 'DateField' THEN CONCAT(vcLookBackTableName,'_TempDateFields')
   ELSE vcLookBackTableName
   END) AS vcLookBackTableName,
		   bitAllowFiltering,bitAllowEdit,coalesce(vcGroup,'Custom Fields') as vcGroup,coalesce(intWFCompare,0) as intWFCompare
   FROM View_DynamicDefaultColumns
   where numFormId = v_numFormID and numDomainID = v_numDomainID
   AND coalesce(bitWorkFlowField,false) = true AND coalesce(bitCustom,0) = 0 AND 1 =(CASE
   WHEN v_numFormID = 68
   THEN(CASE WHEN vcGroup = 'Contact Fields' THEN 0 ELSE 1 END)
   WHEN v_numFormID = 70
   THEN(CASE WHEN (vcGroup = 'BizDoc Fields' AND vcDbColumnName <> 'monAmountPaid') OR vcGroup = 'Contact Fields' OR vcGroup = 'Milestone/Stage Fields' THEN 0 ELSE 1 END)
   WHEN v_numFormID = 94
   THEN(CASE WHEN vcGroup = 'Project Fields' THEN 0 ELSE 1 END)
   ELSE 1
   END);


   v_vcLocationID := '0';
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = v_numFormID;

   IF v_numFormID = 49 then

      IF LENGTH(coalesce(v_vcLocationID,'')) > 0 then
	
         select   coalesce(v_vcLocationID,'') || ',' || coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 70;
      ELSE
         select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 70;
      end if;
   ELSEIF v_numFormID = 94
   then

	-- DO NOT SHOW CUSTOM FIELDS
      v_vcLocationID := '';
   end if;

   IF v_numFormID IN(69,70,49,72,73,124) then

      IF LENGTH(coalesce(v_vcLocationID,'')) > 0 then
	
         v_vcLocationID := CONCAT(v_vcLocationID,',1,12,13,14');
      ELSE
         v_vcLocationID := '1,12,13,14';
      end if;
   end if;

--Custom Fields			
   INSERT INTO tt_TEMPFIELD
   SELECT coalesce(CFM.Fld_id,0) as numFieldID,CFM.FLd_label as vcFieldName,
	   CAST('Cust' || CAST(Fld_Id AS VARCHAR(10)) AS VARCHAR(50)) as vcDbColumnName,CAST('Cust' || CAST(Fld_Id AS VARCHAR(10)) AS VARCHAR(50)) AS vcOrigDbColumnName,CASE CFM.fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.fld_type AS vcAssociatedControlType,CAST(CASE WHEN CFM.fld_type = 'SelectBox' THEN 'LI' ELSE '' END AS VARCHAR(3)) AS vcListItemType,coalesce(CFM.numlistid,0) as numListID,
	   true AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,false AS bitAllowFiltering,true AS bitAllowEdit,CAST((CASE WHEN Grp_id IN(1,12,13,14) THEN 'Organization Custom Fields' WHEN Grp_id = 4 THEN 'Contact Custom Fields' WHEN Grp_id IN(2,6) THEN 'Opportunity & Order Custom Fields' WHEN Grp_id = 3 THEN 'Case Custom Fields' WHEN Grp_id = 11 THEN 'Project Custom Fields' ELSE 'Custom Fields' END) AS VARCHAR(100))  as vcGroup,0 as intWFCompare
   FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldId = CFM.Fld_id
   JOIN CFW_Loc_Master L on CFM.Grp_id = L.Loc_id
   WHERE   CFM.numDomainID = v_numDomainID
   AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','));
 


	open SWV_RefCur for 
	SELECT 
		numFieldID AS "numFieldID",
      vcFieldName AS vcFieldName,
      vcDbColumnName AS vcDbColumnName,
      vcOrigDbColumnName AS "vcOrigDbColumnName",
      vcFieldDataType AS "vcFieldDataType",
      vcAssociatedControlType AS "vcAssociatedControlType",
      vcListItemType AS "vcListItemType",
      numListID AS "numListID",
      bitCustom AS "bitCustom",
      vcLookBackTableName AS "vcLookBackTableName",
      bitAllowFiltering AS "bitAllowFiltering",
      bitAllowEdit AS "bitAllowEdit",
      vcGroup AS "vcGroup",
      intWFCompare AS "intWFCompare"
	  ,CAST(numFieldID AS VARCHAR(18)) || '_' || CAST(CASE WHEN bitCustom = true THEN 'True' ELSE 'False' END AS VARCHAR(10)) AS "ID"  FROM tt_TEMPFIELD
   ORDER BY vcGroup,vcFieldName,bitCustom;

   
   RETURN;
END; $$;












