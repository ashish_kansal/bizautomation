-- Stored procedure definition script USP_SelectCommunicationAttendees for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SelectCommunicationAttendees(v_numCommId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') as vcContact,cast(CA.numContactId as VARCHAR(255)),cast(CI.vcCompanyName as VARCHAR(255)) AS vcCompanyname,
  CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType,cast(CA.ActivityID as VARCHAR(255)),AC.vcEmail as Email,
  GetListIemName(CA.numStatus) AS vcStatus,
  GetListIemName(AC.vcPosition) AS vcPosition,
  AC.vcEmail,
  AC.numPhone,
  AC.numPhoneExtension,
  false AS bitDefault
   FROM CommunicationAttendees CA LEFT JOIN AdditionalContactsInformation AC ON CA.numContactId = AC.numContactId
   LEFT JOIN DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
   LEFT JOIN CompanyInfo CI on DM.numCompanyID = CI.numCompanyId
   LEFT JOIN UserMaster UM  ON UM.numUserDetailId = CA.numContactId
   WHERE CA.numCommId = v_numCommId;
END; $$;












