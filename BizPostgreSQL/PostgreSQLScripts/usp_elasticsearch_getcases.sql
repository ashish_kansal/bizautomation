DROP FUNCTION IF EXISTS USP_ElasticSearch_GetCases;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetCases(v_numDomainID NUMERIC(18,0)
	,v_vcCaseIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numCaseId AS id
		,'case' AS module
		,coalesce(Cases.numRecOwner,0) AS "numRecOwner"
		,coalesce(Cases.numAssignedTo,0) AS "numAssignedTo"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,CONCAT('/cases/frmCases.aspx?frm=Caselist&CaseID=',numCaseId) AS url
		,CONCAT('<b style="color:#7f6000">','Case:</b> ',coalesce(vcCaseNumber,''),', ',
   vcCompanyName,', ',coalesce(textSubject,'')) AS displaytext
		,vcCaseNumber AS text
		,coalesce(vcCaseNumber,'') AS "Search_vcCaseNumber"
		,coalesce(textSubject,'') AS "Search_textSubject"
   FROM
   Cases
   INNER JOIN
   DivisionMaster
   ON
   Cases.numDivisionID = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   Cases.numDomainID = v_numDomainID
   AND (NULLIF(v_vcCaseIds,'') IS NULL OR numCaseId IN(SELECT id FROM SplitIDs(coalesce(v_vcCaseIds,'0'),',')));
END; $$;