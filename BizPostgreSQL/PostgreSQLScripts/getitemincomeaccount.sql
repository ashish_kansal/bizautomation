-- Function definition script GetItemIncomeAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemIncomeAccount(v_numItemCode NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemIncomeAccount  NUMERIC(9,0);
BEGIN
   select   coalesce(numIncomeChartAcntId,'0') INTO v_numItemIncomeAccount from Item where numItemCode = v_numItemCode;     
  
  
   return coalesce(v_numItemIncomeAccount,cast('0' as NUMERIC));
END; $$;

