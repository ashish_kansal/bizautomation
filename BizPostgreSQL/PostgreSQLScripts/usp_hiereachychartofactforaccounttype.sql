-- Stored procedure definition script USP_HiereachyChartOfActForAccountType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_HiereachyChartOfActForAccountType(v_numDomainID NUMERIC(9,0) DEFAULT 0,      
v_numParentAcntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   Delete from HierChartOfAct where numDomainID = v_numDomainID;      
   PERFORM USP_ArranageHierChartAct(v_numParentAcntID,v_numDomainID);      
      
   open SWV_RefCur for select numChartOfAcntID,vcCategoryName from HierChartOfAct
   where numDomainID = v_numDomainID and  numChartOfAcntID <> v_numParentAcntID  And (numAcntType = 813 or numAcntType = 817);
END; $$;












