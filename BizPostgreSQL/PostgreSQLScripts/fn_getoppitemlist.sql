CREATE OR REPLACE FUNCTION fn_GetOppItemList(v_numOppID NUMERIC)
RETURNS VARCHAR(400) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemIds  VARCHAR(400);
BEGIN
   select string_agg(CAST(numItemCode AS VARCHAR),',') INTO v_vcItemIds FROM OpportunityItems WHERE numOppId = v_numOppID;

   RETURN v_vcItemIds;
END; $$;

