-- Function definition script fn_GetMarketBudgetMonthTotalAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetMarketBudgetMonthTotalAmt(v_numMarketId NUMERIC(9,0),v_stDate TIMESTAMP,v_numListItemID NUMERIC(9,0) DEFAULT 0,v_numDomainId NUMERIC(9,0) DEFAULT NULL)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  DECIMAL(20,5);              
   v_montotalAmt  DECIMAL(20,5);             
   v_numMonth  SMALLINT;           
   v_montotAmt  VARCHAR(10);
   v_dtFiscalStDate  TIMESTAMP;                            
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN
   v_numMonth := 1;            
   v_montotalAmt := 0;   
   v_monAmount := 0;        
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_stDate,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_stDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      select   coalesce(MBD.monAmount,0) INTO v_monAmount From MarketBudgetMaster MBM
      inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId Where MBM.numDomainID = v_numDomainId And MBD.numListItemID = v_numListItemID And MBD.numMarketId = v_numMarketId and  MBD.tintMonth = EXTRACT(month FROM v_dtFiscalStDate) And MBD.intYear = EXTRACT(year FROM v_dtFiscalStDate);
      v_montotalAmt := v_montotalAmt+v_monAmount;
      v_numMonth := v_numMonth+1;
   END LOOP;          
   if v_montotalAmt = 0.00 then 
      v_montotAmt := '';  
   Else 
      v_montotAmt := SUBSTR(CAST(v_montotalAmt AS VARCHAR(10)),1,10);
   end if;        
                    
   Return v_montotAmt;
END; $$;

