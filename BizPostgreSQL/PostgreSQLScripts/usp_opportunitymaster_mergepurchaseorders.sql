CREATE OR REPLACE FUNCTION USP_OpportunityMaster_MergePurchaseOrders
(
	p_numDomainID NUMERIC(18,0),
	p_numUserCntID NUMERIC(18,0),
	p_numMasterPOID NUMERIC(18,0),
	p_vcItemsToMerge TEXT
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
	DECLARE v_numContactId NUMERIC(18,0);                                                                       
			v_numDivisionId numeric(9);                                                                         
			v_tintSource numeric(9);                                                                         
			v_vcPOppName Varchar(100);                                                               
			v_Comments varchar(1000);                                                                       
			v_bitPublicFlag Boolean DEFAULT FALSE;                                                                                                                                                    
			v_monPAmount DECIMAL(20,5) DEFAULT 0;                                           
			v_numAssignedTo numeric(9) DEFAULT 0;
			v_strItems TEXT DEFAULT NULL;                                                                     
			v_strMilestone VARCHAR(100) DEFAULT NULL;                                                                       
			v_dtEstimatedCloseDate TIMESTAMP ;                                                                        
			v_CampaignID numeric(9) DEFAULT NULL;                                                                       
			v_lngPConclAnalysis numeric(9) DEFAULT NULL;                                                                     
			v_tintOppType SMALLINT;                                                                                                                                            
			v_tintActive SMALLINT;                                                             
			v_numSalesOrPurType numeric(9);              
			v_numRecurringId numeric(9);
			v_numCurrencyID numeric(9) DEFAULT 0;
			v_DealStatus SMALLINT DEFAULT 0;
			v_numStatus NUMERIC(9);
			v_vcOppRefOrderNo VARCHAR(100);
			v_numBillAddressId numeric(9) DEFAULT 0;
			v_numShipAddressId numeric(9) DEFAULT 0;
			v_bitStockTransfer Boolean DEFAULT FALSE;
			v_WebApiId INT DEFAULT 0;
			v_tintSourceType SMALLINT DEFAULT 0;
			v_bitDiscountType BOOLEAN;
			v_fltDiscount  NUMERIC;
			v_bitBillingTerms BOOLEAN;
			v_intBillingDays integer;
			v_bitInterestType BOOLEAN;
			v_fltInterest NUMERIC;
			v_tintTaxOperator SMALLINT;
			v_numDiscountAcntType NUMERIC(9);
			v_vcWebApiOrderNo VARCHAR(100) DEFAULT NULL;
			v_vcCouponCode		VARCHAR(100) DEFAULT '';
			v_vcMarketplaceOrderID VARCHAR(100) DEFAULT NULL;
			v_vcMarketplaceOrderDate  TIMESTAMP DEFAULT NULL;
			v_vcMarketplaceOrderReportId VARCHAR(100) DEFAULT NULL;
			v_numPercentageComplete NUMERIC(9);
			v_bitUseShippersAccountNo BOOLEAN DEFAULT FALSE;
			v_bitUseMarkupShippingRate BOOLEAN DEFAULT FALSE;
			v_numMarkupShippingRate NUMERIC(19,2) DEFAULT 0;
			v_intUsedShippingCompany INT DEFAULT 0;
			v_numShipmentMethod NUMERIC(18,0) DEFAULT 0;
			v_dtReleaseDate DATE DEFAULT NULL;
			v_numPartner NUMERIC(18,0) DEFAULT 0;
			v_tintClickBtn INT DEFAULT 0;
			v_numPartenerContactId NUMERIC(18,0) DEFAULT 0;
			v_numAccountClass NUMERIC(18,0) DEFAULT 0;
			v_numWillCallWarehouseID NUMERIC(18,0) DEFAULT 0;
			v_numVendorAddressID NUMERIC(18,0) DEFAULT 0;
			v_i INT DEFAULT 1;
			v_iCount INT;
			v_numOppID NUMERIC(18,0);
			my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=p_numDomainID AND numOppId=p_numMasterPOID) THEN
		DROP TABLE IF EXISTS tt_TEMPMergeVendorPO CASCADE;
		CREATE TEMPORARY TABLE tt_TEMPMergeVendorPO
		(
			ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numUnitHour DOUBLE PRECISION
			,monPrice DECIMAL(20,5)
		);

		INSERT INTO tt_TEMPMergeVendorPO
		(
			numOppID
			,numOppItemID
			,numUnitHour
			,monPrice
		)
		SELECT
			COALESCE(OppID,0)
			,COALESCE(OppItemID,0)
			,COALESCE(Units,0)
			,COALESCE(UnitCost,0)
		FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(p_vcItemsToMerge AS XML)
			COLUMNS
				id FOR ORDINALITY,
				OppID NUMERIC(18,0) PATH 'OppID',
				OppItemID NUMERIC(18,0) PATH 'OppItemID',
				Units DOUBLE PRECISION PATH 'Units',
				UnitCost DECIMAL(20,5) PATH 'UnitCost'
		) AS X;

		IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN tt_TEMPMergeVendorPO T ON OB.numOppId = T.numOppID) THEN
			RAISE EXCEPTION 'BIZ_DOC_EXISTS';
		ELSE
			BEGIN                                                               
				--GET MASTER PO FIELDS 
				SELECT
					numContactId,numDivisionId,tintSource,vcPOppName,txtComments,
					bitPublicFlag,monPAmount,numAssignedTo,null,intPEstimatedCloseDate,
					numCampainID,lngPConclAnalysis,tintOppType,tintActive,numSalesOrPurType,
					null,numCurrencyID,tintOppStatus,numStatus,vcOppRefOrderNo,0,
					0,false,0,tintSourceType,bitDiscountType,fltDiscount,
					bitBillingTerms,intBillingDays,bitInterestType,fltInterest,tintTaxOperator,
					numDiscountAcntType,vcWebApiOrderNo,vcCouponCode,vcMarketplaceOrderID,
					bintCreatedDate,vcMarketplaceOrderReportId, numPercentageComplete,
					bitUseShippersAccountNo,bitUseMarkupShippingRate,numMarkupShippingRate,
					intUsedShippingCompany,numShipmentMethod,dtReleaseDate,numPartner,numPartenerContact
					,numAccountClass,numVendorAddressID
				INTO 
					v_numContactId ,v_numDivisionId,v_tintSource,v_vcPOppName,v_Comments,
					v_bitPublicFlag,v_monPAmount,v_numAssignedTo,v_strMilestone,v_dtEstimatedCloseDate,
					v_CampaignID,v_lngPConclAnalysis,v_tintOppType,v_tintActive,v_numSalesOrPurType,
					v_numRecurringId,v_numCurrencyID,v_DealStatus,v_numStatus,v_vcOppRefOrderNo,v_numBillAddressId,
					v_numShipAddressId,v_bitStockTransfer,v_WebApiId,v_tintSourceType,v_bitDiscountType,v_fltDiscount,
					v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,v_tintTaxOperator,
					v_numDiscountAcntType,v_vcWebApiOrderNo,v_vcCouponCode,v_vcMarketplaceOrderID,
					v_vcMarketplaceOrderDate,v_vcMarketplaceOrderReportId,v_numPercentageComplete,
					v_bitUseShippersAccountNo,v_bitUseMarkupShippingRate,v_numMarkupShippingRate,
					v_intUsedShippingCompany,v_numShipmentMethod,v_dtReleaseDate,v_numPartner,v_numPartenerContactId
					,v_numAccountClass,v_numVendorAddressID
				FROM
					OpportunityMaster
				WHERE
					numDomainId = p_numDomainID
					AND numOppId = p_numMasterPOID;
				
				DROP TABLE IF EXISTS v_TEMPMasterItems CASCADE;
				CREATE TEMPORARY TABLE v_TEMPMasterItems
				(
					numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
					monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
					Op_Flag INT, ItemType VARCHAR(50),DropShip Boolean,bitDiscountType Boolean, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
					vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder Boolean,numVendorWareHouse NUMERIC(18,0),
					numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
					numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired Boolean
				);

				--GET ITEMS OF MASTER PO
				INSERT INTO 
					v_TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					OpportunityItems
				WHERE 
					numOppId = p_numMasterPOID;

				--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
				INSERT INTO 
					v_TEMPMasterItems
				SELECT 
					0,numItemCode,T.numUnitHour,T.monPrice,(T.numUnitHour * T.monPrice),vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,vcType,bitDropShip,bitDiscountType,0,(T.numUnitHour * T.monPrice),vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					tt_TEMPMergeVendorPO T
				INNER JOIN
					OpportunityItems OI
				ON
					T.numOppItemID = OI.numoppitemtCode;
		
				-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
				v_strItems := CONCAT('<NewDataSet>',COALESCE((SELECT string_agg(CONCAT('<Item>'
															,'<numoppitemtCode>',numoppitemtCode,'</numoppitemtCode>'
														   	,'<numItemCode>',numItemCode,'</numItemCode>'
														   	,'<numUnitHour>',COALESCE(numUnitHour,0),'</numUnitHour>'
														   	,'<monPrice>',COALESCE(monPrice,0),'</monPrice>'
													   		,'<monTotAmount>',COALESCE(monTotAmount,0),'</monTotAmount>'
														   	,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
													   	   	,'<numWarehouseItmsID>',COALESCE(numWarehouseItmsID,0),'</numWarehouseItmsID>'
														   	,'<numToWarehouseItemID>',COALESCE(numToWarehouseItemID,0),'</numToWarehouseItemID>'
														   	,'<Op_Flag>',Op_Flag,'</Op_Flag>'
														   	,'<ItemType>',ItemType,'</ItemType>'
															,'<DropShip>',(CASE WHEN DropShip THEN 1 ELSE 0 END),'</DropShip>'
														   	,'<bitDiscountType>',(CASE WHEN bitDiscountType THEN 1 ELSE 0 END),'</bitDiscountType>'
														   	,'<fltDiscount>',CAST(fltDiscount AS DECIMAL(18,8)),'</fltDiscount>'
														   	,'<monTotAmtBefDiscount>',COALESCE(monTotAmtBefDiscount,0),'</monTotAmtBefDiscount>'
														   	,'<vcItemName>',vcItemName,'</vcItemName>'
														   	,'<numUOM>',COALESCE(numUOM,0),'</numUOM>'
															,'<bitWorkOrder>',(CASE WHEN bitWorkOrder THEN 1 ELSE 0 END),'</bitWorkOrder>'
														   	,'<numVendorWareHouse>',COALESCE(numVendorWareHouse,0),'</numVendorWareHouse>'
														   	,'<numShipmentMethod>',COALESCE(numShipmentMethod,0),'</numShipmentMethod>'
														   	,'<numSOVendorId>',COALESCE(numSOVendorId,0),'</numSOVendorId>'
														   	,'<numProjectID>',COALESCE(numProjectID,0),'</numProjectID>'
														   	,'<numProjectStageID>',COALESCE(numProjectStageID,0),'</numProjectStageID>'
														   	,'<Attributes>',Attributes,'</Attributes>'
														   	,'<bitItemPriceApprovalRequired>',(CASE WHEN bitItemPriceApprovalRequired THEN 1 ELSE 0 END),'</bitItemPriceApprovalRequired>'
															 ,'</Item>'),'')
															FROM 
																v_TEMPMasterItems),''),'</NewDataSet>');
		
				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				PERFORM USP_OppManage (v_numOppID := p_numMasterPOID,v_numContactId := v_numContactId,v_numDivisionId := v_numDivisionId,v_tintSource := v_tintSource,v_vcPOppName := v_vcPOppName,
									v_Comments := v_Comments,v_bitPublicFlag := v_bitPublicFlag,v_numUserCntID := p_numUserCntID,v_monPAmount := v_monPAmount,v_numAssignedTo := v_numAssignedTo,
									v_numDomainID := p_numDomainID,v_strItems := v_strItems,v_strMilestone := v_strMilestone,v_dtEstimatedCloseDate := v_dtEstimatedCloseDate,v_CampaignID := v_CampaignID,
									v_lngPConclAnalysis := v_lngPConclAnalysis,v_tintOppType := v_tintOppType,v_tintActive := v_tintActive,v_numSalesOrPurType := v_numSalesOrPurType,
									v_numRecurringId := v_numRecurringId,v_numCurrencyID := v_numCurrencyID,v_DealStatus := v_DealStatus,v_numStatus := v_numStatus,v_vcOppRefOrderNo := v_vcOppRefOrderNo,
									v_numBillAddressId := v_numBillAddressId,v_numShipAddressId := v_numShipAddressId,v_bitStockTransfer := v_bitStockTransfer,v_WebApiId := v_WebApiId,
									v_tintSourceType := v_tintSourceType,v_bitDiscountType := v_bitDiscountType,v_fltDiscount := v_fltDiscount,v_bitBillingTerms := v_bitBillingTerms,
									v_intBillingDays := v_intBillingDays,v_bitInterestType := v_bitInterestType,v_fltInterest := v_fltInterest,v_tintTaxOperator := v_tintTaxOperator,
									v_numDiscountAcntType := v_numDiscountAcntType,v_vcWebApiOrderNo := v_vcWebApiOrderNo,v_vcCouponCode := v_vcCouponCode,v_vcMarketplaceOrderID := v_vcMarketplaceOrderID,
									v_vcMarketplaceOrderDate := v_vcMarketplaceOrderDate,v_vcMarketplaceOrderReportId := v_vcMarketplaceOrderReportId,v_numPercentageComplete := v_numPercentageComplete,
									v_bitUseShippersAccountNo := v_bitUseShippersAccountNo,v_bitUseMarkupShippingRate := v_bitUseMarkupShippingRate,
									v_numMarkupShippingRate := v_numMarkupShippingRate,v_intUsedShippingCompany := v_intUsedShippingCompany,v_numShipmentMethod := v_numShipmentMethod,v_dtReleaseDate := v_dtReleaseDate,v_numPartner := v_numPartner,v_tintClickBtn :=  0,v_numPartenerContactId := v_numPartenerContactId
									,v_numAccountClass := v_numAccountClass,v_numVendorAddressID := v_numVendorAddressID, v_numShipFromWarehouse := 0, v_numShippingService := 0,
									v_ClientTimeZoneOffset := 0,v_vcCustomerPO := '',v_bitDropShipAddress := false,v_PromCouponCode := NULL,v_numPromotionId := 0,v_dtExpectedDate := null,v_numProjectID := 0);				
				
				DROP TABLE IF EXISTS v_TEMPOppMergeVendorPO CASCADE;
				CREATE TEMPORARY TABLE v_TEMPOppMergeVendorPO
				(
					ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
					,numOppID NUMERIC(18,0)
				);

				INSERT INTO v_TEMPOppMergeVendorPO
				(
					numOppID
				)
				SELECT DISTINCT
					numOppID
				FROM
					tt_TEMPMergeVendorPO;

				SELECT COUNT(*) INTO v_iCount FROM v_TEMPOppMergeVendorPO;

				-- DELETE MERGED POS
				WHILE v_i <= v_iCount LOOP
					SELECT numOppID INTO v_numOppID FROM v_TEMPOppMergeVendorPO WHERE ID = v_i;

					IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=p_numDomainID AND numOppId=v_numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=v_numOppID AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM tt_TEMPMergeVendorPO)) THEN
						PERFORM USP_DeleteOppurtunity(v_numOppID,p_numDomainID,p_numUserCntID);
					ELSE
						PERFORM USP_RevertDetailsOpp(v_numOppID,0,p_numUserCntID);

						DELETE FROM OpportunityItems OI 
						using 
							tt_TEMPMergeVendorPO T	
						WHERE
							T.numOppID = v_numOppID
							AND T.numOppItemID = OI.numoppitemtCode;

						PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,p_numUserCntID);

						UPDATE OpportunityMaster SET monDealAmount=GetDealAmount(v_numOppID,now()::TIMESTAMP,0),bintModifiedDate=timezone('utc', now()),numModifiedBy=p_numUserCntID WHERE numOppId=v_numOppID;

						--INSERT TAX FOR DIVISION   
						IF (select COALESCE(bitPurchaseTaxCredit,false) from Domain where numDomainId=p_numDomainID)=true THEN
							--Delete Tax for Opportunity Items if item deleted 
							DELETE FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=v_numOppID);

							--Insert Tax for Opportunity Items
							INSERT INTO OpportunityItemsTaxItems 
							(
								numOppId,
								numOppItemID,
								numTaxItemID,
								numTaxID
							) 
							SELECT 
								v_numOppId,
								OI.numoppitemtCode,
								TI.numTaxItemID,
								0
							FROM 
								OpportunityItems OI 
							JOIN 
								ItemTax IT 
							ON 
								OI.numItemCode=IT.numItemCode 
							JOIN
								TaxItems TI 
							ON 
								TI.numTaxItemID = IT.numTaxItemID 
							WHERE 
								OI.numOppId=v_numOppID 
								AND IT.bitApplicable=true 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID)
							UNION
							SELECT 
								v_numOppId,
								OI.numoppitemtCode,
								0,
								0
							FROM 
								OpportunityItems OI 
							JOIN 
								Item I 
							ON 
								OI.numItemCode=I.numItemCode
							WHERE 
								OI.numOppId=v_numOppID 
								AND I.bitTaxable=true
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID)
							UNION
							SELECT
								v_numOppId,
								OI.numoppitemtCode,
								1,
								TD.numTaxID
							FROM
								OpportunityItems OI 
							INNER JOIN
								ItemTax IT
							ON
								IT.numItemCode = OI.numItemCode
							INNER JOIN
								TaxDetails TD
							ON
								TD.numTaxID = IT.numTaxID
								AND TD.numDomainId = p_numDomainID
							WHERE
								OI.numOppId = v_numOppID
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID);
						END IF;
					END IF;

					v_i := v_i + 1;
				END LOOP;
			EXCEPTION WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS
				my_ex_state   = RETURNED_SQLSTATE,
				my_ex_message = MESSAGE_TEXT,
				my_ex_detail  = PG_EXCEPTION_DETAIL,
				my_ex_hint    = PG_EXCEPTION_HINT,
				my_ex_ctx     = PG_EXCEPTION_CONTEXT;

				raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
			END;	
		END IF;
	ELSE
		RAISE EXCEPTION 'MASTER_PO_DOES_NOT_EXISTS';
	END IF;
END; $$;
