-- Stored procedure definition script USP_UpdateInvenotry_RentalAsset for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10thSept2014>
-- Description:	<Description,,Update Inventory stock for Rental/Asset items as per new requirement>
-- =============================================
Create or replace FUNCTION USP_UpdateInvenotry_RentalAsset(v_itemcode NUMERIC(9,0) DEFAULT 0,   
	v_numUserCntID NUMERIC(9,0) DEFAULT 0   ,
	v_onAllocation NUMERIC(9,0) DEFAULT 0,
	v_numDomain NUMERIC(9,0) DEFAULT 0,
	v_numOppId NUMERIC(9,0) DEFAULT 0,
	v_Mode  INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_onHand  NUMERIC;                                    
   v_onOrder  NUMERIC; 
   v_onBackOrder  NUMERIC;
		--DECLARE @onAllocation AS NUMERIC
   v_monAvgCost  NUMERIC(19,4); 
   v_numWareHouseItemID  NUMERIC(9,0);
   v_tintOpptype  SMALLINT;     
   v_numOrigUnits  NUMERIC;		
   v_numAllocated  NUMERIC;
   v_TotalOnHand  NUMERIC;
BEGIN


    -- Insert statements for procedure here
   v_numOrigUnits := v_onAllocation;   
		
   select   tintopptype INTO v_tintOpptype FROM OpportunityMaster WHERE numOppId = v_numOppId;
	 
   select   OI.numItemCode, coalesce(numWarehouseItmsID,0) INTO v_itemcode,v_numWareHouseItemID FROM OpportunityItems OI join Item I
   on OI.numItemCode = I.numItemCode   and numOppId = v_numOppId where (charitemtype = 'P' and bitAsset = true OR 1 =(CASE WHEN v_tintOpptype = 1 THEN
      CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
      WHEN coalesce(I.bitKitParent,false) = true THEN 1
      ELSE 0 END
   ELSE 0 END)) and (bitDropShip = false or bitDropShip is null) AND coalesce(OI.bitWorkOrder,false) = false
   AND I.numDomainID = v_numDomain
   AND I.numItemCode = v_itemcode    LIMIT 1;


		

   IF v_numWareHouseItemID > 0 then
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_numAllocated,v_onOrder,v_onBackOrder FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWareHouseItemID;
      v_TotalOnHand := 0;
      select   SUM(coalesce(numOnHand,0)) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_itemcode;
      If	(v_Mode = 0) then --On Click of Out
				
         IF v_onHand >= v_onAllocation then
                        
            v_onHand := v_onHand -v_onAllocation;
            v_numAllocated := v_onAllocation+v_numAllocated;
         ELSEIF v_onHand < v_onAllocation
         then
                            
            v_onAllocation := v_onAllocation+v_onHand;
            v_onBackOrder := v_onBackOrder+v_onAllocation
            -v_onHand;
            v_onHand := 0;
         end if;    

				-- select @onAllocation
         UPDATE  WareHouseItems
         SET     numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
         dtModified = LOCALTIMESTAMP
         WHERE   numWareHouseItemID = v_numWareHouseItemID;
         UPDATE
         OpportunityItems
         SET 
					--numRentalIN=0,
					--numRentalLost=0,
         numRentalOut = v_onAllocation
         WHERE
         numOppId = v_numOppId
         AND numItemCode = v_itemcode
         AND numWarehouseItmsID = v_numWareHouseItemID;
      ELSEIF (v_Mode = 2)
      then--On Click of Lost
				
         UPDATE  WareHouseItems
         SET     
						--numOnHand = @onHand-@onAllocation,
         numAllocation = v_numAllocated -v_onAllocation,numBackOrder = v_onBackOrder,
         dtModified = LOCALTIMESTAMP
         WHERE   numWareHouseItemID = v_numWareHouseItemID;
         UPDATE
         OpportunityItems
         SET
         numRentalLost = v_onAllocation
         WHERE
         numOppId = v_numOppId
         AND numItemCode = v_itemcode
         AND numWarehouseItmsID = v_numWareHouseItemID;
      ELSE
         UPDATE  WareHouseItems
         SET     numOnHand = v_onHand+v_onAllocation,numAllocation = v_numAllocated -v_onAllocation,
         numBackOrder = v_onBackOrder,dtModified = LOCALTIMESTAMP
         WHERE   numWareHouseItemID = v_numWareHouseItemID;
         UPDATE
         OpportunityItems
         SET
         numRentalIN = v_onAllocation
         WHERE
         numOppId = v_numOppId
         AND numItemCode = v_itemcode
         AND numWarehouseItmsID = v_numWareHouseItemID;
      end if;
   end if;
   RETURN;
END; $$;



