Create or replace FUNCTION USP_DeleteUOM(v_numDomainID NUMERIC(9,0),
	 v_numUOMId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN
   IF Exists(Select 'col1' From Item c Where c.numSaleUnit = v_numUOMId or c.numSaleUnit = v_numUOMId) then
      RAISE EXCEPTION 'Child Record Exists,Can''t perform delete operation';
   ELSEIF Exists(Select 'col1' From OpportunityItems c Where c.numUOMId = v_numUOMId) then
      RAISE EXCEPTION 'Child Record Exists,Can''t perform delete operation';
   ELSE
   -- Insert statements for procedure here
      Delete  from UOM where numDomainID = v_numDomainID and numUOMId = v_numUOMId;
   end if;

   RETURN;
END; $$;


