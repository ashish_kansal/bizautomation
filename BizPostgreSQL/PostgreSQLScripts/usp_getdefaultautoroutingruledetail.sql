-- Stored procedure definition script usp_GetDefaultAutoRoutingRuleDetail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDefaultAutoRoutingRuleDetail(v_numDomainID NUMERIC,
 v_numRuleID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all the Default Routing Rules details  
   AS $$
BEGIN
   open SWV_RefCur for SELECT r.numRoutID, r.vcRoutName,rd.numEmpId
   FROM RoutingLeads r INNER JOIN RoutingLeadDetails rd ON r.numRoutID = rd.numRoutID
   where
   r.numRoutID = v_numRuleID;
END; $$;












