-- Function definition script GetHighestPriceLevelDiscountForEcommerceItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetHighestPriceLevelDiscountForEcommerceItem(v_numItemCode NUMERIC(9,0),
      v_numDomainID NUMERIC(9,0) DEFAULT 0,
      v_numWareHouseItemID NUMERIC(9,0) DEFAULT NULL)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
--Below function applies Price Rule on given Price and Qty and returns discount
   AS $$
   DECLARE
   v_monHighestDiscount  DECIMAL(20,5);
	
   v_monListPrice  DECIMAL(20,5);
   v_monVendorCost  DECIMAL(20,5);
BEGIN
   v_monListPrice := 0;
   v_monVendorCost := 0;

   IF ( (v_numWareHouseItemID > 0)
   AND EXISTS(SELECT * FROM      Item
   WHERE     numItemCode = v_numItemCode
   AND bitSerialized = false
   AND charItemType = 'P')) then
        
      select   coalesce(monWListPrice,0) INTO v_monListPrice FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWareHouseItemID;
      IF v_monListPrice = 0 then
         select   monListPrice INTO v_monListPrice FROM    Item WHERE   numItemCode = v_numItemCode;
      end if;
   ELSE
      select   monListPrice INTO v_monListPrice FROM    Item WHERE   numItemCode = v_numItemCode;
   end if; 

   v_monVendorCost := fn_GetVendorCost(v_numItemCode);

   select   CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
   THEN decDiscount
   WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
   THEN(decDiscount*100)/(v_monListPrice -decDiscount)
   WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
   THEN decDiscount
   WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
   THEN(decDiscount*100)/(v_monVendorCost+decDiscount)
   WHEN tintRuleType = 3 --Named Price 
   THEN 0
   END INTO v_monHighestDiscount FROM    PricingTable WHERE   coalesce(numItemCode,0) = v_numItemCode AND coalesce(numCurrencyID,0) = 0   ORDER BY numPricingID LIMIT 1; 
    
   RETURN coalesce(v_monHighestDiscount,0);
END; $$;

