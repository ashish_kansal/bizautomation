DROP FUNCTION IF EXISTS usp_getallselectedsystemmodulespagesandaccesses;

CREATE OR REPLACE FUNCTION public.usp_getallselectedsystemmodulespagesandaccesses(
	v_nummoduleid numeric DEFAULT 0,
	v_numgroupid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_tintGroupType  SMALLINT;
BEGIN
   select   coalesce(tintGroupType,0) INTO v_tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupID;

   IF v_numModuleID = -1 then --All Modules
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numModuleID NUMERIC(18,0),
         vcModuleName VARCHAR(100)
      );
      IF EXISTS(SELECT ID FROM ModuleMasterGroupType WHERE tintGroupType = v_tintGroupType) then
		
         INSERT INTO tt_TEMP(numModuleID
				,vcModuleName)
         SELECT
         ModuleMaster.numModuleID
				,ModuleMaster.vcModuleName
         FROM
         ModuleMaster
         INNER JOIN
         ModuleMasterGroupType
         ON
         ModuleMaster.numModuleID = ModuleMasterGroupType.numModuleID
         WHERE
         ModuleMasterGroupType.tintGroupType = v_tintGroupType;
      ELSE
         INSERT INTO tt_TEMP(numModuleID
				,vcModuleName)
         SELECT
         ModuleMaster.numModuleID
				,ModuleMaster.vcModuleName
         FROM
         ModuleMaster
         WHERE
         tintGroupType = v_tintGroupType;
      end if;
      open SWV_RefCur for
      SELECT 
			numModuleID AS "numModuleID"
			,numPageID AS "numPageID"
			,vcPageDesc AS "vcPageDesc"
			,intViewAllowed AS "intViewAllowed"
			,intAddAllowed AS "intAddAllowed"
			,intUpdateAllowed AS "intUpdateAllowed"
			,intDeleteAllowed AS "intDeleteAllowed"
			,intExportAllowed AS "intExportAllowed"
			,vcToolTip AS "vcToolTip"
			,bitIsViewApplicable AS "bitIsViewApplicable"
			,bitViewPermission1Applicable AS "bitViewPermission1Applicable"
			,bitViewPermission2Applicable AS "bitViewPermission2Applicable"
			,bitViewPermission3Applicable AS "bitViewPermission3Applicable"
			,bitIsAddApplicable AS "bitIsAddApplicable"
			,bitAddPermission1Applicable AS "bitAddPermission1Applicable"
			,bitAddPermission2Applicable AS "bitAddPermission2Applicable"
			,bitAddPermission3Applicable AS "bitAddPermission3Applicable"
			,bitIsUpdateApplicable AS "bitIsUpdateApplicable"
			,bitUpdatePermission1Applicable AS "bitUpdatePermission1Applicable"
			,bitUpdatePermission2Applicable AS "bitUpdatePermission2Applicable"
			,bitUpdatePermission3Applicable AS "bitUpdatePermission3Applicable"
			,bitIsDeleteApplicable AS "bitIsDeleteApplicable"
			,bitDeletePermission1Applicable AS "bitDeletePermission1Applicable"
			,bitDeletePermission2Applicable AS "bitDeletePermission2Applicable"
			,bitDeletePermission3Applicable AS "bitDeletePermission3Applicable"
			,bitIsExportApplicable AS "bitIsExportApplicable"
			,bitExportPermission1Applicable AS "bitExportPermission1Applicable"
			,bitExportPermission2Applicable AS "bitExportPermission2Applicable"
			,bitExportPermission3Applicable AS "bitExportPermission3Applicable"
	  FROM (SELECT
				PM.numModuleID
				,PM.numPageID
				,PM.vcPageDesc
				,GM.intViewAllowed
				,GM.intAddAllowed
				,GM.intUpdateAllowed
				,GM.intDeleteAllowed
				,GM.intExportAllowed
				,coalesce(PM.vcToolTip,'') AS vcToolTip
				,coalesce(PMGS.bitIsViewApplicable,CAST(PM.bitIsViewApplicable AS BOOLEAN)) AS bitIsViewApplicable
				,coalesce(PMGS.bitViewPermission1Applicable,PM.bitViewPermission1Applicable) AS bitViewPermission1Applicable
				,coalesce(PMGS.bitViewPermission2Applicable,PM.bitViewPermission2Applicable) AS bitViewPermission2Applicable
				,coalesce(PMGS.bitViewPermission3Applicable,PM.bitViewPermission3Applicable) AS bitViewPermission3Applicable
				,coalesce(PMGS.bitIsAddApplicable,CAST(PM.bitIsAddApplicable AS BOOLEAN)) AS bitIsAddApplicable
				,coalesce(PMGS.bitAddPermission1Applicable,PM.bitAddPermission1Applicable) AS bitAddPermission1Applicable
				,coalesce(PMGS.bitAddPermission2Applicable,PM.bitAddPermission2Applicable) AS bitAddPermission2Applicable
				,coalesce(PMGS.bitAddPermission3Applicable,PM.bitAddPermission3Applicable) AS bitAddPermission3Applicable
				,coalesce(PMGS.bitIsUpdateApplicable,CAST(PM.bitIsUpdateApplicable AS BOOLEAN)) AS bitIsUpdateApplicable
				,coalesce(PMGS.bitUpdatePermission1Applicable,PM.bitUpdatePermission1Applicable) AS bitUpdatePermission1Applicable
				,coalesce(PMGS.bitUpdatePermission2Applicable,PM.bitUpdatePermission2Applicable) AS bitUpdatePermission2Applicable
				,coalesce(PMGS.bitUpdatePermission3Applicable,PM.bitUpdatePermission3Applicable) AS bitUpdatePermission3Applicable
				,coalesce(PMGS.bitIsDeleteApplicable,CAST(PM.bitIsDeleteApplicable AS BOOLEAN)) AS bitIsDeleteApplicable
				,coalesce(PMGS.bitDeletePermission1Applicable,PM.bitDeletePermission1Applicable) AS bitDeletePermission1Applicable
				,coalesce(PMGS.bitDeletePermission2Applicable,PM.bitDeletePermission2Applicable) AS bitDeletePermission2Applicable
				,coalesce(PMGS.bitDeletePermission3Applicable,PM.bitDeletePermission3Applicable) AS bitDeletePermission3Applicable
				,coalesce(PMGS.bitIsExportApplicable,CAST(PM.bitIsExportApplicable AS BOOLEAN)) AS bitIsExportApplicable
				,coalesce(PMGS.bitExportPermission1Applicable,PM.bitExportPermission1Applicable) AS bitExportPermission1Applicable
				,coalesce(PMGS.bitExportPermission2Applicable,PM.bitExportPermission2Applicable) AS bitExportPermission2Applicable
				,coalesce(PMGS.bitExportPermission3Applicable,PM.bitExportPermission3Applicable) AS bitExportPermission3Applicable
         FROM
         PageMaster PM
         INNER JOIN
         tt_TEMP TM
         ON
         PM.numModuleID = TM.numModuleID
         LEFT JOIN
         PageMasterGroupSetting PMGS
         ON
         PM.numPageID = PMGS.numPageID
         AND PM.numModuleID = PMGS.numModuleID
         AND PMGS.numGroupID = v_numGroupID
         LEFT JOIN
         GroupAuthorization GM
         ON
         PM.numPageID = GM.numPageID
         AND PM.numModuleID = GM.numModuleID
         AND (GM.numGroupID = v_numGroupID OR GM.numGroupID IS NULL)
         AND 1 =(CASE WHEN PM.numModuleID = 32 THEN(CASE WHEN coalesce(GM.bitCustomRelationship,false) = false THEN 1 ELSE 0 END) ELSE 1 END)
         WHERE
         coalesce(bitDeleted,false) = false
         UNION
         SELECT
         32 AS numModuleID,
				TableCustomRelationship.numListItemID AS numPageID,
				TableCustomRelationship.vcData || ' List' AS vcPageDesc,
				coalesce(GM.intViewAllowed,1) AS intViewAllowed,
				0 AS intAddAllowed,
				0 AS intUpdateAllowed,
				0 AS intDeleteAllowed,
				0 AS intExportAllowed,
				'' as  vcToolTip,
				true AS bitIsViewApplicable,
				true AS bitViewPermission1Applicable,
				(CASE WHEN v_tintGroupType = 2 THEN false ELSE true END) AS bitViewPermission2Applicable,
				(CASE WHEN v_tintGroupType = 2 THEN false ELSE true END) AS bitViewPermission3Applicable,
				false AS bitIsAddApplicable,
				false AS bitAddPermission1Applicable,
				false AS bitAddPermission2Applicable,
				false AS bitAddPermission3Applicable,
				false As bitIsUpdateApplicable,
				false AS bitUpdatePermission1Applicable,
				false AS bitUpdatePermission2Applicable,
				false AS bitUpdatePermission3Applicable,
				false AS bitIsDeleteApplicable,
				false AS bitDeletePermission1Applicable,
				false AS bitDeletePermission2Applicable,
				false AS bitDeletePermission3Applicable,
				false AS bitIsExportApplicable ,
				false AS bitExportPermission1Applicable,
				false AS bitExportPermission2Applicable,
				false AS bitExportPermission3Applicable
         FROM(SELECT
            numListItemID,
						vcData
            FROM
            Listdetails
            WHERE
            numListID = 5 AND
						(coalesce(constFlag,false) = true OR (numDomainid = coalesce((SELECT numDomainID FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupID),0) AND
            coalesce(bitDelete,false) = false)) AND
            numListItemID <> 46) AS TableCustomRelationship
         LEFT JOIN
         GroupAuthorization GM
         ON
         GM.numPageID = TableCustomRelationship.numListItemID AND
				(GM.numGroupID = v_numGroupID OR GM.numGroupID IS NULL) AND
         GM.bitCustomRelationship = true) TEMP
      ORDER BY
      TEMP.vcPageDesc ASC;
   ELSE
      open SWV_RefCur for
      SELECT numModuleID AS "numModuleID"
			,numPageID AS "numPageID"
			,vcPageDesc AS "vcPageDesc"
			,intViewAllowed AS "intViewAllowed"
			,intAddAllowed AS "intAddAllowed"
			,intUpdateAllowed AS "intUpdateAllowed"
			,intDeleteAllowed AS "intDeleteAllowed"
			,intExportAllowed AS "intExportAllowed"
			,vcToolTip AS "vcToolTip"
			,bitIsViewApplicable AS "bitIsViewApplicable"
			,bitViewPermission1Applicable AS "bitViewPermission1Applicable"
			,bitViewPermission2Applicable AS "bitViewPermission2Applicable"
			,bitViewPermission3Applicable AS "bitViewPermission3Applicable"
			,bitIsAddApplicable AS "bitIsAddApplicable"
			,bitAddPermission1Applicable AS "bitAddPermission1Applicable"
			,bitAddPermission2Applicable AS "bitAddPermission2Applicable"
			,bitAddPermission3Applicable AS "bitAddPermission3Applicable"
			,bitIsUpdateApplicable AS "bitIsUpdateApplicable"
			,bitUpdatePermission1Applicable AS "bitUpdatePermission1Applicable"
			,bitUpdatePermission2Applicable AS "bitUpdatePermission2Applicable"
			,bitUpdatePermission3Applicable AS "bitUpdatePermission3Applicable"
			,bitIsDeleteApplicable AS "bitIsDeleteApplicable"
			,bitDeletePermission1Applicable AS "bitDeletePermission1Applicable"
			,bitDeletePermission2Applicable AS "bitDeletePermission2Applicable"
			,bitDeletePermission3Applicable AS "bitDeletePermission3Applicable"
			,bitIsExportApplicable AS "bitIsExportApplicable"
			,bitExportPermission1Applicable AS "bitExportPermission1Applicable"
			,bitExportPermission2Applicable AS "bitExportPermission2Applicable"
			,bitExportPermission3Applicable AS "bitExportPermission3Applicable" FROM(SELECT
         PM.numModuleID
				,PM.numPageID
				,PM.vcPageDesc
				,GM.intViewAllowed
				,GM.intAddAllowed
				,GM.intUpdateAllowed
				,GM.intDeleteAllowed
				,GM.intExportAllowed
				,coalesce(PM.vcToolTip,'') AS vcToolTip
				,coalesce(PMGS.bitIsViewApplicable,CAST(PM.bitIsViewApplicable AS BOOLEAN)) AS bitIsViewApplicable
				,coalesce(PMGS.bitViewPermission1Applicable,PM.bitViewPermission1Applicable) AS bitViewPermission1Applicable
				,coalesce(PMGS.bitViewPermission2Applicable,PM.bitViewPermission2Applicable) AS bitViewPermission2Applicable
				,coalesce(PMGS.bitViewPermission3Applicable,PM.bitViewPermission3Applicable) AS bitViewPermission3Applicable
				,coalesce(PMGS.bitIsAddApplicable,CAST(PM.bitIsAddApplicable AS BOOLEAN)) AS bitIsAddApplicable
				,coalesce(PMGS.bitAddPermission1Applicable,PM.bitAddPermission1Applicable) AS bitAddPermission1Applicable
				,coalesce(PMGS.bitAddPermission2Applicable,PM.bitAddPermission2Applicable) AS bitAddPermission2Applicable
				,coalesce(PMGS.bitAddPermission3Applicable,PM.bitAddPermission3Applicable) AS bitAddPermission3Applicable
				,coalesce(PMGS.bitIsUpdateApplicable,CAST(PM.bitIsUpdateApplicable AS BOOLEAN))  AS bitIsUpdateApplicable
				,coalesce(PMGS.bitUpdatePermission1Applicable,PM.bitUpdatePermission1Applicable) AS bitUpdatePermission1Applicable
				,coalesce(PMGS.bitUpdatePermission2Applicable,PM.bitUpdatePermission2Applicable) AS bitUpdatePermission2Applicable
				,coalesce(PMGS.bitUpdatePermission3Applicable,PM.bitUpdatePermission3Applicable) AS bitUpdatePermission3Applicable
				,coalesce(PMGS.bitIsDeleteApplicable,CAST(PM.bitIsDeleteApplicable AS BOOLEAN)) AS bitIsDeleteApplicable
				,coalesce(PMGS.bitDeletePermission1Applicable,PM.bitDeletePermission1Applicable) AS bitDeletePermission1Applicable
				,coalesce(PMGS.bitDeletePermission2Applicable,PM.bitDeletePermission2Applicable) AS bitDeletePermission2Applicable
				,coalesce(PMGS.bitDeletePermission3Applicable,PM.bitDeletePermission3Applicable) AS bitDeletePermission3Applicable
				,coalesce(PMGS.bitIsExportApplicable,CAST(PM.bitIsExportApplicable AS BOOLEAN)) AS bitIsExportApplicable
				,coalesce(PMGS.bitExportPermission1Applicable,PM.bitExportPermission1Applicable) AS bitExportPermission1Applicable
				,coalesce(PMGS.bitExportPermission2Applicable,PM.bitExportPermission2Applicable) AS bitExportPermission2Applicable
				,coalesce(PMGS.bitExportPermission3Applicable,PM.bitExportPermission3Applicable) AS bitExportPermission3Applicable
         FROM
         PageMaster PM
         LEFT JOIN
         PageMasterGroupSetting PMGS
         ON
         PM.numPageID = PMGS.numPageID
         AND PM.numModuleID = PMGS.numModuleID
         AND PMGS.numGroupID = v_numGroupID
         LEFT JOIN
         GroupAuthorization GM
         ON
         PM.numPageID = GM.numPageID
         AND PM.numModuleID = GM.numModuleID
         AND (GM.numGroupID = v_numGroupID OR GM.numGroupID IS NULL)
         AND 1 =(CASE WHEN PM.numModuleID = 32 THEN(CASE WHEN coalesce(GM.bitCustomRelationship,false) = false THEN 1 ELSE 0 END) ELSE 1 END)
         WHERE
         PM.numModuleID = v_numModuleID
         AND coalesce(bitDeleted,false) = false
         UNION
         SELECT
         32 AS numModuleID,
					TableCustomRelationship.numListItemID AS numPageID,
					TableCustomRelationship.vcData || ' List' AS vcPageDesc,
					coalesce(GM.intViewAllowed,1) AS intViewAllowed,
					0 AS intAddAllowed,
					0 AS intUpdateAllowed,
					0 AS intDeleteAllowed,
					0 AS intExportAllowed,
					'' as  vcToolTip,
					true AS bitIsViewApplicable,
					(CASE WHEN v_tintGroupType = 2 THEN true ELSE true END) AS bitViewPermission1Applicable,
					(CASE WHEN v_tintGroupType = 2 THEN false ELSE true END) AS bitViewPermission2Applicable,
					(CASE WHEN v_tintGroupType = 2 THEN false ELSE true END) AS bitViewPermission3Applicable,
					false AS bitIsAddApplicable,
					false AS bitAddPermission1Applicable,
					false AS bitAddPermission2Applicable,
					false AS bitAddPermission3Applicable,
					false As bitIsUpdateApplicable,
					false AS bitUpdatePermission1Applicable,
					false AS bitUpdatePermission2Applicable,
					false AS bitUpdatePermission3Applicable,
					false AS bitIsDeleteApplicable,
					false AS bitDeletePermission1Applicable,
					false AS bitDeletePermission2Applicable,
					false AS bitDeletePermission3Applicable,
					false AS bitIsExportApplicable ,
					false AS bitExportPermission1Applicable,
					false AS bitExportPermission2Applicable,
					false AS bitExportPermission3Applicable
         FROM(SELECT
            numListItemID,
							vcData
            FROM
            Listdetails
            WHERE
            numListID = 5 AND
							(coalesce(constFlag,false) = true OR (numDomainid = coalesce((SELECT numDomainID FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupID),0) AND
            coalesce(bitDelete,false) = false)) AND
            numListItemID <> 46) AS TableCustomRelationship
         LEFT JOIN
         GroupAuthorization GM
         ON
         GM.numPageID = TableCustomRelationship.numListItemID AND
					(GM.numGroupID = v_numGroupID OR GM.numGroupID IS NULL) AND
         GM.bitCustomRelationship = true
         WHERE
         1 =(CASE WHEN v_numModuleID = 32 THEN 1 ELSE 0 END)) TEMP
      ORDER BY
      TEMP.vcPageDesc ASC;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getallselectedsystemmodulespagesandaccesses(numeric, numeric, refcursor)
    OWNER TO postgres;
