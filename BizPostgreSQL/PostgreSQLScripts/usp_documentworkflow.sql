-- Stored procedure definition script USP_DocumentWorkFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DocumentWorkFlow(v_numDocID NUMERIC(9,0) DEFAULT 0,      
v_numContactID NUMERIC(9,0) DEFAULT 0,      
v_cDocType VARCHAR(10) DEFAULT '',      
v_byteMode SMALLINT DEFAULT NULL,
v_vcComment TEXT DEFAULT '',
v_numUserCntID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizActionId  NUMERIC;
   v_numDivisionID  NUMERIC;
   v_numDomainID  NUMERIC;
BEGIN
   v_numBizActionId := 0;

   if v_byteMode = 1 then


/*validate if given contact is internal/external user*/
      IF NOT EXISTS(SELECT 'col1' FROM UserMaster WHERE numUserDetailId = v_numContactID) then

         IF NOT EXISTS(SELECT 'col1' FROM ExtranetAccountsDtl EAD INNER JOIN AdditionalContactsInformation ACI ON EAD.numDomainID = ACI.numDomainID
         AND EAD.numContactID = ACI.numContactId WHERE EAD.numContactID = v_numContactID) then
	
            RAISE EXCEPTION 'No_EXTRANET';
            RETURN;
         end if;
      end if;
      delete from DocumentWorkflow where numDocID = v_numDocID and numContactID = v_numContactID and cDocType = v_cDocType;
      insert into DocumentWorkflow(numDocID,numContactID,cDocType,dtCreatedDate)
 values(v_numDocID,v_numContactID,v_cDocType,TIMEZONE('UTC',now())); 
 --Add to Tickler,bizdoc action item
 
      IF v_cDocType = 'B' OR v_cDocType = 'D' then--bizdoc,generic docs & specific
         select   numDivisionId, numDomainID INTO v_numDivisionID,v_numDomainID FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
         IF NOT EXISTS(SELECT * FROM BizDocAction B JOIN BizActionDetails BA
         ON BA.numBizActionId = B.numBizActionId WHERE B.numDomainID = v_numDomainID AND B.numContactId = v_numContactID AND BA.numOppBizDocsId = v_numDocID) then
	
            insert into BizDocAction(numContactId ,numDivisionId,numStatus,numDomainID,dtCreatedDate,numAssign,numCreatedBy,bitTask,numBizDocAppId)
		values(v_numContactID,v_numDivisionID,0,v_numDomainID,LOCALTIMESTAMP ,v_numContactID,v_numUserCntID,972,0);
		
            v_numBizActionId := CURRVAL('BizDocAction_seq');							
		
--		PRINT @numBizActionId
		-- btDocType =2 for Document approval request, =1 for bizdoc
            INSERT INTO BizActionDetails(numBizActionId,numOppBizDocsId,btStatus,btDocType)
		VALUES(v_numBizActionId,v_numDocID,0,CASE WHEN v_cDocType = 'D' THEN 2 ELSE 1 END);
         end if;
      end if;
   ELSEIF v_byteMode = 2
   then

      delete from DocumentWorkflow where numDocID = v_numDocID and numContactID = v_numContactID and cDocType = v_cDocType;
      IF v_cDocType = 'B' OR v_cDocType = 'D' then--bizdoc,generic docs & specific
 
         select   BA.numBizActionId INTO v_numBizActionId from BizDocAction BA join BizActionDetails BD on BA.numBizActionId = BD.numBizActionId where BD.numOppBizDocsId = v_numDocID and BD.btDocType =(CASE WHEN v_cDocType = 'D' THEN 2 ELSE 1 END)
         and BA.numContactId = v_numContactID;
         DELETE from BizActionDetails where numBizActionId = v_numBizActionId;
         DELETE from  BizDocAction where numBizActionId = v_numBizActionId;
      end if;
   ELSEIF v_byteMode = 3
   then

      update DocumentWorkflow set dtApprovedOn = TIMEZONE('UTC',now()),tintApprove = 1,vcComment = v_vcComment  where numDocID = v_numDocID and numContactID = v_numContactID and cDocType = v_cDocType;
      IF v_cDocType = 'B' OR v_cDocType = 'D' then--bizdoc,generic docs & specific
	 
         select   BA.numBizActionId INTO v_numBizActionId from BizDocAction BA join BizActionDetails BD on BA.numBizActionId = BD.numBizActionId where BD.numOppBizDocsId = v_numDocID and BD.btDocType =(CASE WHEN v_cDocType = 'D' THEN 2 ELSE 1 END)
         and BA.numContactId = v_numContactID;
         UPDATE BizActionDetails SET btStatus = 1 WHERE numBizActionId = v_numBizActionId;
         UPDATE BizDocAction SET numStatus = 1 WHERE numBizActionId = v_numBizActionId;
      end if;
   ELSEIF v_byteMode = 4
   then

      update DocumentWorkflow set dtApprovedOn = TIMEZONE('UTC',now()),tintApprove = 2,vcComment = v_vcComment where numDocID = v_numDocID and numContactID = v_numContactID and cDocType = v_cDocType;
   end if;
END; $$;


