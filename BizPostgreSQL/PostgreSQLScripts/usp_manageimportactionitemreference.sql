-- Stored procedure definition script USP_ManageImportActionItemReference for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageImportActionItemReference(v_numDomainID NUMERIC,
    INOUT v_numCompanyID NUMERIC ,
    INOUT v_numDivisionID NUMERIC ,
    INOUT v_numContactID NUMERIC ,
    v_numNonBizContactID VARCHAR(50),
    v_tintMode SMALLINT DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      INSERT  INTO ImportActionItemReference(numDomainID,
                                                         numNonBizContactID,
                                                         numCompanyID,
                                                         numDivisionID,
                                                         numContactID)
            VALUES(v_numDomainID,
                      v_numNonBizContactID,
                      v_numCompanyID,
                      v_numDivisionID,
                      v_numContactID);
   end if;
   IF v_tintMode = 1 then
        
      select   numCompanyID, numDivisionID, numContactID INTO v_numCompanyID,v_numDivisionID,v_numContactID FROM    ImportActionItemReference WHERE   numNonBizContactID = v_numNonBizContactID
      AND numDomainID = v_numDomainID    LIMIT 1;
   end if;
   RETURN;
END; $$;


