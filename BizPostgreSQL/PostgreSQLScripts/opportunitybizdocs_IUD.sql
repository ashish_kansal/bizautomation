CREATE OR REPLACE FUNCTION OpportunityBizDocs_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
DECLARE 
	v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
	v_numOppBizDocsId  NUMERIC(9,0);
	v_numOppId  NUMERIC(9,0);
	v_numBizDocId  NUMERIC(9,0);
	v_numBizDocStatus  NUMERIC(9,0);
	v_vcBizDocID  VARCHAR(250);
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',OLD.numOppBizDocsId,'Delete',OpportunityMaster.numDomainId FROM OpportunityMaster WHERE OpportunityMaster.numOppId = OLD.numOppID;

		SELECT OM.numDomainID INTO v_numDomainID FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID WHERE numOppBizDocsId=OLD.numOppBizDocsId;

		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numOppBizDocsId;
		v_numUserCntID := OLD.numModifiedBy;
	ELSIF (TG_OP = 'UPDATE') THEN
		SELECT OM.numDomainID INTO v_numDomainID FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID WHERE numOppBizDocsId=New.numOppBizDocsId;

		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numOppBizDocsId;
		v_numUserCntID := NEW.numModifiedBy;

		IF OLD.vcBizDocID <> NEW.vcBizDocID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'vcBizDocID,');
		END IF;
		IF OLD.monAmountPaid <> NEW.monAmountPaid THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'monAmountPaid,');
		END IF;
		IF OLD.numBizDocStatus <> NEW.numBizDocStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numBizDocStatus,');
		END IF;
		IF OLD.numBizDocId <> NEW.numBizDocId THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numBizDocId,');
		END IF;
		IF OLD.monDealAmount <> NEW.monDealAmount THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'monDealAmount,');
		END IF;
		IF OLD.numModifiedBy <> NEW.numModifiedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numModifiedBy,');
		END IF;
		IF OLD.numCreatedBy <> NEW.numCreatedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numCreatedBy,');
		END IF;
		IF OLD.bitRecurred <> NEW.bitRecurred THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitRecurred,');
		END IF;
		

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');

		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',NEW.numOppBizDocsId,'Update',OpportunityMaster.numDomainId FROM OpportunityMaster WHERE OpportunityMaster.numOppId = NEW.numOppId;

		IF OLD.monAmountPaid <> NEW.monAmountPaid OR OLD.bitPartialFulfilment <> NEW.bitPartialFulfilment OR OLD.monShipCost <> NEW.monShipCost OR OLD.numShipVia <> NEW.numShipVia then
			UPDATE 
				OpportunityBizDocs
			SET 
				monDealAmount = GetDealAmount(New.numOppId,TIMEZONE('UTC',now()),NEW.numOppBizDocsId)
			WHERE 
				numOppBizDocsId = NEW.numOppBizDocsId;
		END IF;

		IF OLD.numBizDocStatus <> NEW.numBizDocStatus then	
			INSERT INTO Communication
			(
				bitTask, numContactId, numDivisionID, textDetails, intSnoozeMins, intRemainderMins, numStatus, numActivity, numAssign, tintSnoozeStatus, tintRemStatus, numOppId, numCreatedBy, dtCreatedDate, nummodifiedby, dtModifiedDate, numDomainID, bitClosedFlag, vcCalendarName, bitOutlook, dtStartTime, dtEndTime, numAssignedBy, bitsendEmailTemp, numEmailTemplate, tinthours, bitalert, CaseId, caseTimeid, caseExpid, numActivityId, bitEmailSent, bitFollowUpAnyTime, dtEventClosedDate, LastSnoozDateTimeUtc, bitTimeAddedToContract
			)
			SELECT 
				972,numEmployeeId,ACI.numDivisionId,'BizDoc No: ' || coalesce(New.vcBizDocID,'') || ' Status Into: ' || GetListIemName(New.numBizDocStatus)  ,0,0,0,numActionTypeId,numEmployeeId,
				0,0,0,numEmployeeId,TIMEZONE('UTC',now()),numEmployeeId,TIMEZONE('UTC',now()),v_numDomainID,false,Null,false,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),numEmployeeId,true,0,0,false,0,0,0,numActionTypeId,false,false,null,null,null from BizDocStatusApprove BS
			inner join AdditionalContactsInformation ACI
			on ACI.numContactId = BS.numEmployeeId and
			BS.numBizDocTypeId = New.numBizDocId and
			BS.numBizDocStatusID = New.numBizDocStatus and
			ACI.numDomainID = BS.numDomainID and
			BS.numDomainID = v_numDomainID;
		end if;

	ELSIF (TG_OP = 'INSERT') THEN
		SELECT OM.numDomainID INTO v_numDomainID FROM OpportunityMaster OM WHERE OM.numOppID=New.numOppID;

		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.numOppBizDocsId;
		v_numUserCntID := New.numCreatedBy;

		INSERT INTO ElasticSearchModifiedRecords
		(
			vcModule,numRecordID, vcAction, numDomainID
		) 
		SELECT 
			'BizDoc',New.numOppBizDocsId,'Insert',OpportunityMaster.numDomainId 
		FROM 
			OpportunityMaster 
		WHERE 
			OpportunityMaster.numOppId = New.numOppId;

		UPDATE 
			OpportunityBizDocs
		SET 
			monDealAmount = GetDealAmount(NEW.numOppId,TIMEZONE('UTC',now()),NEW.numOppBizDocsId)
		WHERE 
			OpportunityBizDocs.numOppBizDocsId = NEW.numOppBizDocsId;

        INSERT INTO Communication(bitTask, numContactId, numDivisionID, textDetails, intSnoozeMins, intRemainderMins, numStatus, numActivity, numAssign, tintSnoozeStatus, tintRemStatus, numOppId, numCreatedBy, dtCreatedDate, nummodifiedby, dtModifiedDate, numDomainID, bitClosedFlag, vcCalendarName, bitOutlook, dtStartTime, dtEndTime, numAssignedBy, bitsendEmailTemp, numEmailTemplate, tinthours, bitalert, CaseId, caseTimeid, caseExpid, numActivityId, bitEmailSent, bitFollowUpAnyTime, dtEventClosedDate, LastSnoozDateTimeUtc, bitTimeAddedToContract)
        select 972,numEmployeeId,ACI.numDivisionId,'BizDoc No: ' || coalesce(NEW.vcBizDocID,'') || ' Created With Status: ' || GetListIemName(NEW.numBizDocStatus)  ,0,0,0,numActionTypeId,numEmployeeId,
		0,0,0,numEmployeeId,TIMEZONE('UTC',now()),numEmployeeId,TIMEZONE('UTC',now()),v_numDomainID,false,Null,false,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),numEmployeeId,true,0,0,false,0,0,0,numActionTypeId,false,false,null,null,null from BizDocStatusApprove BS
        inner join AdditionalContactsInformation ACI
        on ACI.numContactId = BS.numEmployeeId and
        BS.numBizDocTypeId = NEW.numBizDocId and
        BS.numBizDocStatusID = NEW.numBizDocStatus and
        ACI.numDomainID = BS.numDomainID and
        BS.numDomainID = v_numDomainID;
	END IF;

	 PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
   v_numRecordID := v_numRecordID,v_numFormID := 71,v_tintProcessStatus := 1::SMALLINT,
   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER OpportunityBizDocs_IUD AFTER INSERT OR UPDATE OR DELETE ON OpportunityBizDocs FOR EACH ROW WHEN (pg_trigger_depth() < 1) EXECUTE PROCEDURE OpportunityBizDocs_IUD_TrFunc();


