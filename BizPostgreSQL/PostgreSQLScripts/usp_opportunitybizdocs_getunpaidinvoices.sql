-- Stored procedure definition script USP_OpportunityBizDocs_GetUnPaidInvoices for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetUnPaidInvoices(v_numDomainID NUMERIC(18,0),
 v_numOppId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numOppBizDocsId,
		cast(coalesce(OpportunityBizDocs.monDealAmount,0) as DECIMAL(20,5)) AS monDealAmount,
		cast(coalesce(monAmountPaid,0) as DECIMAL(20,5)) AS monAmountPaid,
		coalesce(OpportunityBizDocs.monDealAmount,0) -coalesce(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay,
		cast(coalesce(OpportunityMaster.numCurrencyID,0) as NUMERIC(18,0)) AS numCurrencyID,
		OpportunityMaster.numDivisionId
   FROM
   OpportunityBizDocs
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityBizDocs.numoppid = OpportunityMaster.numOppId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityBizDocs.numoppid = v_numOppId
   AND numBizDocId = 287
   AND bitAuthoritativeBizDocs = 1
   AND coalesce(OpportunityBizDocs.monDealAmount,0) -coalesce(OpportunityBizDocs.monAmountPaid,0) > 0;
   RETURN;
END; $$;













