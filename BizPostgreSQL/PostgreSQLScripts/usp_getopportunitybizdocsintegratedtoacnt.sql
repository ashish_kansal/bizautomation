-- Stored procedure definition script USP_GetOpportunityBizDocsIntegratedToAcnt for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpportunityBizDocsIntegratedToAcnt(v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select bitIntegratedToAcnt From OpportunityBizDocsDetails Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;
END; $$;












