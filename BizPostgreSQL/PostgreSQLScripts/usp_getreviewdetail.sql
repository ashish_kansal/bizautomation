-- Stored procedure definition script USP_GetReviewDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReviewDetail(v_numReviewId NUMERIC(18,0) , 
v_numContactId NUMERIC(18,0),
v_Mode INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_Mode = 1 then
	  
      open SWV_RefCur for SELECT * FROM ReviewDetail
      WHERE numReviewId = v_numReviewId
      AND numContactId = v_numContactId;
   end if;
   RETURN;
END; $$;













