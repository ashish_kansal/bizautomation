-- FUNCTION: public.usp_getaragingworkflowqueue(numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getaragingworkflowqueue(numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getaragingworkflowqueue(
	v_numdomainid numeric DEFAULT 0,
	v_numusercntid numeric DEFAULT NULL::numeric,
	v_noofdayspastdue numeric DEFAULT NULL::numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_numDivisionId  NUMERIC(9,0) DEFAULT NULL;
   v_numAccountClass  NUMERIC(9,0) DEFAULT 0;
   v_ClientTimeZoneOffset  INTEGER DEFAULT 0;
   v_dtFromDate  TIMESTAMP DEFAULT NULL;
   v_dtToDate  TIMESTAMP DEFAULT NULL;

   v_AuthoritativeSalesBizDocId  INTEGER;
BEGIN
-- This function was converted on Sat Apr 10 15:03:03 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).

   IF v_dtFromDate IS NULL then
      v_dtFromDate := CAST('1990-01-01 00:00:00.000' AS TIMESTAMP);
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;	

   RAISE NOTICE '%',v_dtFromDate;
   RAISE NOTICE '%',v_dtToDate;

   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId = v_numDomainId;
  
   INSERT  INTO TempARRecord(numUserCntID,
          numOppId,
          numDivisionId,
          numOppBizDocsId,
          DealAmount,
          numBizDocId,
          numCurrencyID,
          dtDueDate,AmountPaid)
   SELECT  v_numUserCntID,
                OB.numoppid,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0) -coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount,
                OB.numBizDocId,
                coalesce(Opp.numCurrencyID,0) AS numCurrencyID,
				dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) AS dtDueDate,
                coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0) as AmountPaid
   FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = Opp.numDivisionId
   LEFT JOIN LATERAL(SELECT
      SUM(monAmountPaid) AS monPaidAmount
      FROM
      DepositeDetails DD
      INNER JOIN
      DepositMaster DM
      ON
      DD.numDepositID = DM.numDepositId
      WHERE
      numOppID = Opp.numOppID
      AND numOppBizDocsId = OB.numOppBizDocsID
      AND CAST(DM.dtDepositDate AS DATE) <= CAST(v_dtToDate AS DATE)) TablePayments on TRUE
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numDomainId
   AND numBizDocId = v_AuthoritativeSalesBizDocId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND (Opp.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND (Opp.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(OB.dtCreatedDate AS DATE) <= v_dtToDate
   GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
   OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
   OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate,TablePayments.monPaidAmount
   HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0)
   -coalesce(coalesce(TablePayments.monPaidAmount,0)*coalesce(Opp.fltExchangeRate,1),0)) != 0
                           
	--Show Impact of AR journal entries in report as well 
   UNION
   SELECT v_numUserCntID,
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCustomerId,
        CAST(0 AS NUMERIC(18,0)),
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCurrencyID,
        GJH.datEntry_Date ,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = GJD.numCustomerId
   WHERE  GJH.numDomainId = v_numDomainId
   AND COA.vcAccountCode ilike '0101010501'
   AND coalesce(numOppId,0) = 0 AND coalesce(numOppBizDocsId,0) = 0
   AND (GJD.numCustomerId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND coalesce(GJH.numDepositId,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
   AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(GJH.datEntry_Date AS DATE) <= v_dtToDate
   UNION ALL
   SELECT v_numUserCntID,
        CAST(0 AS NUMERIC(18,0)),
        DM.numDivisionID,
        CAST(0 AS NUMERIC(18,0)),
        CAST((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/)*-1 AS DOUBLE PRECISION),
        CAST(0 AS NUMERIC(18,0)),
        DM.numCurrencyID,
        DM.dtDepositDate ,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   DepositMaster DM
   JOIN DivisionMaster AS DIV ON DM.numDivisionID = DIV.numDivisionID
   WHERE DM.numDomainId = v_numDomainId AND tintDepositePage IN(2,3)
   AND (DM.numDivisionID = v_numDivisionId OR v_numDivisionId IS NULL)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0
   AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
   AND CAST(DM.dtDepositDate AS DATE) <= v_dtToDate
   UNION ALL --Standalone Refund against Account Receivable
   SELECT v_numUserCntID,
        CAST(0 AS NUMERIC(18,0)),
        RH.numDivisionId,
        CAST(0 AS NUMERIC(18,0)),
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        CAST(0 AS NUMERIC(18,0)),
        GJD.numCurrencyID,
        GJH.datEntry_Date ,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   ReturnHeader RH JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = RH.numDivisionId
   JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode ilike '0101010501'
   WHERE RH.tintReturnType = 4 AND RH.numDomainID = v_numDomainId
   AND coalesce(RH.numParentID,0) = 0
   AND coalesce(RH.IsUnappliedPayment,false) = false
   AND (RH.numDivisionId = v_numDivisionId OR v_numDivisionId IS NULL)
   AND monBizDocAmount > 0 AND coalesce(RH.numBillPaymentIDRef,0) = 0 --AND ISNULL(RH.numDepositIDRef,0) > 0
   AND (RH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(GJH.datEntry_Date AS DATE) <= v_dtToDate;
		

   INSERT  INTO TempARRecord(numUserCntID,
          numOppId,
          numDivisionId,
          numOppBizDocsId,
          DealAmount,
          numBizDocId,
          numCurrencyID,
          dtDueDate,AmountPaid,monUnAppliedAmount)
   SELECT v_numUserCntID,0, DM.numDivisionID,0,0,0,0,NULL,0,sum(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))
   FROM DepositMaster DM
   JOIN DivisionMaster AS DIV ON DIV.numDivisionID = DM.numDivisionID
   WHERE DM.numDomainId = v_numDomainId AND tintDepositePage IN(2,3)
   AND (DM.numDivisionID = v_numDivisionId OR v_numDivisionId IS NULL)
   AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0
   AND (DM.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)
   AND CAST(DM.dtDepositDate AS DATE) <= v_dtToDate
   GROUP BY DM.numDivisionID;

--SELECT  * FROM    [TempARRecord]

open SWV_RefCur for SELECT  cast(SUM(DealAmount) as VARCHAR(255)) AS DealAmount,
        cast(numDivisionId as VARCHAR(255)) AS numRecordID
   FROM    TempARRecord
   WHERE   numUserCntID = v_numUserCntID AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate:: timestamp) >= v_NoOfDaysPastDue
   AND TempARRecord.numDivisionId NOT IN(SELECT cast(numRecordID as NUMERIC) FROM WorkFlowARAgingExecutionHistory WHERE FormatedDateFromDate(dtCreatedDate,v_numDomainId) =(FormatedDateFromDate(TIMEZONE('UTC',now()),v_numDomainId)))
   GROUP BY numDivisionId;

--DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
   DELETE  FROM TempARRecord WHERE   numUserCntID = v_numUserCntID;

   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getaragingworkflowqueue(numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
