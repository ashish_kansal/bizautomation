-- Stored procedure definition script USP_GetAccountPayableAging for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAccountPayableAging(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numDivisionId NUMERIC(9,0) DEFAULT NULL,
    v_numAccountClass NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate TIMESTAMP DEFAULT NULL,
	v_dtToDate TIMESTAMP DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTempDomainId  NUMERIC(18,0);
   v_numTempDivisionId  NUMERIC(18,0);
   v_numTempAccountClass  NUMERIC(18,0);
   v_dtTempFromDate  TIMESTAMP;
   v_dtTempToDate  TIMESTAMP;

   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
BEGIN
-- This function was converted on Sat Apr 10 14:51:51 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
v_numTempDomainId := v_numDomainId;
   v_numTempDivisionId := v_numDivisionId;
   v_numTempAccountClass := v_numAccountClass;
   v_dtTempFromDate := v_dtFromDate;
   v_dtTempToDate := v_dtToDate;

   IF v_dtTempFromDate IS NULL then
      v_dtTempFromDate := CAST('1990-01-01 00:00:00.000' AS TIMESTAMP);
   end if;
   IF v_dtTempToDate IS NULL then
      v_dtTempToDate := TIMEZONE('UTC',now());
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TempAPRecord_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPAPRECORD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAPRECORD
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
      NOT NULL,
      numUserCntID NUMERIC(18,0) NOT NULL,
      numOppId NUMERIC(18,0),
      numDivisionId NUMERIC(18,0),
      numOppBizDocsId NUMERIC(18,0),
      DealAmount DOUBLE PRECISION,
      numBizDocId NUMERIC(18,0),
      dtDueDate TIMESTAMP,
      numCurrencyID NUMERIC(18,0),
      AmountPaid DOUBLE PRECISION,
      monUnAppliedAmount DECIMAL(20,5) 
   );
  
  
   BEGIN
      CREATE TEMP SEQUENCE tt_TempAPRecord1_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPAPRECORD1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAPRECORD1
   (
      numID NUMERIC(18,0) DEFAULT NEXTVAL('tt_TempAPRecord1_seq')
      NOT NULL,
      numUserCntID NUMERIC(18,0) NOT NULL,
      numDivisionId NUMERIC(18,0),
      tintCRMType SMALLINT,
      vcCompanyName VARCHAR(200),
      vcCustPhone VARCHAR(50),
      numContactId NUMERIC(18,0),
      vcEmail VARCHAR(100),
      numPhone VARCHAR(50),
      vcContactName VARCHAR(100),
      numCurrentDays NUMERIC(18,2),
      numThirtyDays NUMERIC(18,2),
      numSixtyDays NUMERIC(18,2),
      numNinetyDays NUMERIC(18,2),
      numOverNinetyDays NUMERIC(18,2),
      numThirtyDaysOverDue NUMERIC(18,2),
      numSixtyDaysOverDue NUMERIC(18,2),
      numNinetyDaysOverDue NUMERIC(18,2),
      numOverNinetyDaysOverDue NUMERIC(18,2),
      numCompanyID NUMERIC(18,2),
      numDomainID NUMERIC(18,2),
      intCurrentDaysCount INTEGER,
      intThirtyDaysCount INTEGER,
      intThirtyDaysOverDueCount INTEGER,
      intSixtyDaysCount INTEGER,
      intSixtyDaysOverDueCount INTEGER,
      intNinetyDaysCount INTEGER,
      intOverNinetyDaysCount INTEGER,
      intNinetyDaysOverDueCount INTEGER,
      intOverNinetyDaysOverDueCount INTEGER,
      numCurrentDaysPaid NUMERIC(18,2),
      numThirtyDaysPaid NUMERIC(18,2),
      numSixtyDaysPaid NUMERIC(18,2),
      numNinetyDaysPaid NUMERIC(18,2),
      numOverNinetyDaysPaid NUMERIC(18,2),
      numThirtyDaysOverDuePaid NUMERIC(18,2),
      numSixtyDaysOverDuePaid NUMERIC(18,2),
      numNinetyDaysOverDuePaid NUMERIC(18,2),
      numOverNinetyDaysOverDuePaid NUMERIC(18,2),
      monUnAppliedAmount DECIMAL(20,5),
      numTotal DECIMAL(20,5) 
   );
  
  
  
   select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM    AuthoritativeBizDocs WHERE   numDomainId = v_numTempDomainId;

   DROP TABLE IF EXISTS tt_TEMPBILLPAYMENTS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPBILLPAYMENTS
   (
      numBillID NUMERIC(18,0),
      numOppBizDocsID NUMERIC(18,0),
      monAmount DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPBILLPAYMENTS(numBillID
			,numOppBizDocsID
			,monAmount)
   SELECT
   BPD.numBillID
			,BPD.numOppBizDocsID
			,BPD.monAmount
   FROM
   BillPaymentHeader BPH
   INNER JOIN
   BillPaymentDetails BPD
   ON
   BPH.numBillPaymentID = BPD.numBillPaymentID
   WHERE
   BPH.numDomainId = v_numTempDomainId
   AND CAST(BPH.dtPaymentDate AS DATE) <= v_dtTempToDate;        
     
   INSERT  INTO tt_TEMPAPRECORD(numUserCntID,
                  numOppId,
                  numDivisionId,
                  numOppBizDocsId,
                  DealAmount,
                  numBizDocId,
                  dtDueDate,
                  numCurrencyID,AmountPaid)
   SELECT  v_numTempDomainId,
                        OB.numoppid,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        coalesce(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               *coalesce(Opp.fltExchangeRate,1),0)
   -coalesce(coalesce(SUM(TBP.monAmount),0)*coalesce(Opp.fltExchangeRate,1),0) AS DealAmount,
                        OB.numBizDocId,
                        dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) AS dtDueDate,
                        coalesce(Opp.numCurrencyID,0) AS numCurrencyID,
                        coalesce(coalesce(SUM(TBP.monAmount),0)*coalesce(Opp.fltExchangeRate,1),
   0) AS AmountPaid
   FROM    OpportunityMaster Opp
   JOIN OpportunityBizDocs OB ON OB.numoppid = Opp.numOppId
   LEFT OUTER JOIN Currency C ON Opp.numCurrencyID = C.numCurrencyID
   LEFT JOIN tt_TEMPBILLPAYMENTS TBP ON OB.numOppBizDocsId = TBP.numOppBizDocsID
   WHERE   tintopptype = 2
   AND tintoppstatus = 1
   AND Opp.numDomainId = v_numTempDomainId
   AND OB.bitAuthoritativeBizDocs = 1
   AND coalesce(OB.tintDeferred,0) <> 1
   AND numBizDocId = v_AuthoritativePurchaseBizDocId
   AND (Opp.numDivisionId = v_numTempDivisionId
   OR v_numTempDivisionId IS NULL)
   AND (Opp.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND OB.dtCreatedDate <= v_dtTempToDate
   GROUP BY OB.numoppid,Opp.numDivisionId,OB.numOppBizDocsId,Opp.fltExchangeRate,OB.numBizDocId,
   OB.dtCreatedDate,Opp.bitBillingTerms,Opp.intBillingDays,
   OB.monDealAmount,Opp.numCurrencyID,OB.dtFromDate
   HAVING(coalesce(OB.monDealAmount*coalesce(Opp.fltExchangeRate,1),0)
   -coalesce(coalesce(SUM(TBP.monAmount),0)*coalesce(Opp.fltExchangeRate,1),0)) > 0
   UNION ALL 
		--Add Bill Amount
   SELECT
   v_numTempDomainId,
					CAST(0 AS NUMERIC(18,0)) AS numOppId,
					numDivisionId,
					CAST(0 AS NUMERIC(18,0)) AS numBizDocsId,
					coalesce(SUM(monAmountDue),0) -coalesce(SUM(monAmountPaid),0) AS DealAmount,
					CAST(0 AS NUMERIC(18,0)) AS numBizDocId,
					dtDueDate AS dtDueDate,
					CAST(0 AS NUMERIC(18,0)) AS numCurrencyID
					,CAST(coalesce(SUM(monAmountPaid),0) AS DOUBLE PRECISION) AS AmountPaid
   FROM(SELECT
      BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
						,coalesce(SUM(TEMPPaid.monAmount),0) AS monAmountPaid
      FROM
      BillHeader BH
      LEFT JOIN
      tt_TEMPBILLPAYMENTS TEMPPaid
      ON
      BH.numBillID = TEMPPaid.numBillID
      WHERE
      BH.numDomainId = v_numTempDomainId
      AND (BH.numDivisionId = v_numTempDivisionId OR v_numTempDivisionId IS NULL)
      AND (BH.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
      AND BH.dtBillDate <= v_dtTempToDate
      GROUP BY
      BH.numBillID,BH.numDivisionId,BH.dtDueDate,BH.monAmountDue
      HAVING
      coalesce(BH.monAmountDue,0) -coalesce(SUM(TEMPPaid.monAmount),0) > 0) TEMP
   GROUP BY
   numDivisionId,dtDueDate
   HAVING
   coalesce(SUM(monAmountDue),0) -coalesce(SUM(monAmountPaid),0) > 0
   UNION ALL
		--Add Write Check entry (As Amount Paid)
   SELECT  v_numTempDomainId,
                        CAST(0 AS NUMERIC(18,0)) AS numOppId,
                        CD.numCustomerId as numDivisionId,
                        CAST(0 AS NUMERIC(18,0)) AS numBizDocsId,
                        CAST(0 AS DOUBLE PRECISION) AS DealAmount,
                        CAST(0 AS NUMERIC(18,0)) AS numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        CAST(0 AS NUMERIC(18,0)) AS numCurrencyID,coalesce(SUM(CD.monAmount),0) AS AmountPaid
   FROM    CheckHeader CH
   JOIN  CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
   WHERE   CH.numDomainID = v_numTempDomainId AND CH.tintReferenceType = 1
   AND COA.vcAccountCode ilike '01020102%'
   AND (CD.numCustomerId = v_numTempDivisionId
   OR v_numTempDivisionId IS NULL)
   AND (CH.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND CH.dtCheckDate <= v_dtTempToDate
   GROUP BY CD.numCustomerId,CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
   UNION ALL
   SELECT  v_numTempDomainId,
                        CAST(0 AS NUMERIC(18,0)),
                        GJD.numCustomerId,
                        CAST(0 AS NUMERIC(18,0)),
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1*GJD.numDebitAmt
   ELSE GJD.numCreditAmt
   END,
                        CAST(0 AS NUMERIC(18,0)),
                        GJH.datEntry_Date,
                        GJD.numCurrencyID,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM    General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   AND GJH.numDomainId = GJD.numDomainId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   WHERE   GJH.numDomainId = v_numTempDomainId
   AND COA.vcAccountCode ilike '01020102%'
   AND coalesce(numOppId,0) = 0
   AND coalesce(numOppBizDocsId,0) = 0
   AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numCheckHeaderID,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
   AND coalesce(GJH.numPayrollDetailID,0) = 0
   AND (GJD.numCustomerId = v_numTempDivisionId
   OR v_numTempDivisionId IS NULL)
   AND (GJH.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND GJH.datEntry_Date <= v_dtTempToDate
   UNION ALL
				--Add Commission Amount
   SELECT  v_numTempDomainId,
                        OBD.numoppid AS numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId AS numBizDocsId,
                       CAST(coalesce(sum(BDC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
   0) AS DOUBLE PRECISION)  AS DealAmount,
                        CAST(0 AS NUMERIC(18,0)) AS numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        coalesce(Opp.numCurrencyID,0) AS numCurrencyID,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM    BizDocComission BDC
   JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
   join OpportunityMaster Opp on Opp.numOppId = OBD.numoppid
   JOIN Domain D on D.numDomainId = BDC.numDomainId
   LEFT JOIN PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
   WHERE   Opp.numDomainId = v_numTempDomainId
   and BDC.numDomainId = v_numTempDomainId
   AND (D.numDivisionId = v_numTempDivisionId OR v_numTempDivisionId IS NULL)
   AND (Opp.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND OBD.dtCreatedDate <= v_dtTempToDate
   AND coalesce(BDC.bitCommisionPaid,false) = false
   AND OBD.bitAuthoritativeBizDocs = 1
   GROUP BY D.numDivisionId,OBD.numoppid,BDC.numOppBizDocId,Opp.numCurrencyID,BDC.numComissionAmount,
   OBD.dtCreatedDate
   HAVING  coalesce(BDC.numComissionAmount,0) > 0
   UNION ALL --Standalone Refund against Account Receivable
   SELECT v_numTempDomainId,
					CAST(0 AS NUMERIC(18,0)) AS numOppId,
					RH.numDivisionId,
					CAST(0 AS NUMERIC(18,0)),
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt*-1 ELSE GJD.numCreditAmt END AS DealAmount,
					CAST(0 AS NUMERIC(18,0)) AS numBizDocId,
					GJH.datEntry_Date,GJD.numCurrencyID,CAST(0 AS DOUBLE PRECISION) AS AmountPaid
   FROM   ReturnHeader RH JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
   JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
   WHERE RH.tintReturnType = 4 AND RH.numDomainID = v_numTempDomainId  AND COA.vcAccountCode ilike '01020102%'
   AND (RH.numDivisionId = v_numTempDivisionId OR v_numTempDivisionId IS NULL)
   AND monBizDocAmount > 0 AND coalesce(RH.numBillPaymentIDRef,0) > 0
   AND (RH.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND GJH.datEntry_Date <= v_dtTempToDate;
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO tt_TEMPAPRECORD(numUserCntID,
          numOppId,
          numDivisionId,
          numOppBizDocsId,
          DealAmount,
          numBizDocId,
          numCurrencyID,
          dtDueDate,AmountPaid,monUnAppliedAmount)
   SELECT v_numTempDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0))
   FROM BillPaymentHeader BPH
   WHERE BPH.numDomainId = v_numTempDomainId
   AND (BPH.numDivisionId = v_numTempDivisionId
   OR v_numTempDivisionId IS NULL)
   AND(coalesce(monPaymentAmount,0) -coalesce(monAppliedAmount,0)) > 0
   AND (BPH.numAccountClass = v_numTempAccountClass OR v_numTempAccountClass = 0)
   AND BPH.dtPaymentDate <= v_dtTempToDate
   GROUP BY BPH.numDivisionId;
		

--SELECT  GETDATE()
   INSERT  INTO tt_TEMPAPRECORD1(numUserCntID,
                  numDivisionId,
                  tintCRMType,
                  vcCompanyName,
                  vcCustPhone,
                  numContactId,
                  vcEmail,
                  numPhone,
                  vcContactName,
				  numCurrentDays,
                  numThirtyDays,
                  numSixtyDays,
                  numNinetyDays,
                  numOverNinetyDays,
                  numThirtyDaysOverDue,
                  numSixtyDaysOverDue,
                  numNinetyDaysOverDue,
                  numOverNinetyDaysOverDue,
                  numCompanyID,
                  numDomainID,
				  intCurrentDaysCount,
                  intThirtyDaysCount,
                  intThirtyDaysOverDueCount,
                  intSixtyDaysCount,
                  intSixtyDaysOverDueCount,
                  intNinetyDaysCount,
                  intOverNinetyDaysCount,
                  intNinetyDaysOverDueCount,
                  intOverNinetyDaysOverDueCount,
				  numCurrentDaysPaid,
                  numThirtyDaysPaid,
                  numSixtyDaysPaid,
                  numNinetyDaysPaid,
                  numOverNinetyDaysPaid,
                  numThirtyDaysOverDuePaid,
                  numSixtyDaysOverDuePaid,
                  numNinetyDaysOverDuePaid,
                  numOverNinetyDaysOverDuePaid,monUnAppliedAmount)
   SELECT DISTINCT
   v_numTempDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        CAST('' AS VARCHAR(50)),
                        0,
                        CAST('' AS VARCHAR(100)),
                        CAST('' AS VARCHAR(50)),
                        CAST('' AS VARCHAR(100)),
						0 AS numCurrentDays,
                        0 AS numThirtyDays,
                        0 AS numSixtyDays,
                        0 AS numNinetyDays,
                        0 AS numOverNinetyDays,
                        0 AS numThirtyDaysOverDue,
                        0 AS numSixtyDaysOverDue,
                        0 AS numNinetyDaysOverDue,
                        0 AS numOverNinetyDaysOverDue,
                        C.numCompanyId,
                        Div.numDomainID,
						0 AS intCurrentDaysCount,
                        0 AS intThirtyDaysCount,
                        0 AS intThirtyDaysOverDueCount,
                        0 AS intSixtyDaysCount,
                        0 AS intSixtyDaysOverDueCount,
                        0 AS intNinetyDaysCount,
                        0 AS intOverNinetyDaysCount,
                        0 AS intNinetyDaysOverDueCount,
                        0 AS intOverNinetyDaysOverDueCount,
						0 AS numCurrentDaysPaid,
                         0 AS numThirtyDaysPaid,
                        0 AS numSixtyDaysPaid,
                        0 AS numNinetyDaysPaid,
                        0 AS numOverNinetyDaysPaid,
                        0 AS numThirtyDaysOverDuePaid,
                        0 AS numSixtyDaysOverDuePaid,
                        0 AS numNinetyDaysOverDuePaid,
                        0 AS numOverNinetyDaysOverDuePaid,0 AS monUnAppliedAmount
   FROM    DivisionMaster Div
   INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
   WHERE   Div.numDomainID = v_numTempDomainId
   AND Div.numDivisionID IN(SELECT DISTINCT
      numDivisionId
      FROM   tt_TEMPAPRECORD);

--Update Custome Phone 
   UPDATE  tt_TEMPAPRECORD1
   SET     vcCustPhone = X.vcCustPhone
   FROM(SELECT    CASE WHEN NULLIF(numPhone,'') IS NULL THEN ''
   WHEN numPhone = '' THEN ''
   ELSE ', ' || numPhone
   END AS vcCustPhone,
                            numDivisionId
   FROM      AdditionalContactsInformation
   WHERE     bitPrimaryContact = true
   AND numDivisionId IN(SELECT DISTINCT
      numDivisionId
      FROM    tt_TEMPAPRECORD1
      WHERE   numUserCntID = v_numTempDomainId)) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;

--Update Customer info 
   UPDATE  tt_TEMPAPRECORD1
   SET     numContactId = X.numContactID,vcEmail = X.vcEmail,vcContactName = X.vcContactName,
   numPhone = X.numPhone
   FROM(SELECT  numContactId AS numContactID,
                            coalesce(vcEmail,'') AS vcEmail,
                            coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS vcContactName,
                            CAST(coalesce(CASE WHEN NULLIF(numPhone,'') IS NULL THEN ''
   WHEN numPhone = '' THEN ''
   ELSE ', ' || numPhone
   END,'') AS VARCHAR(30)) AS numPhone,
                            numDivisionId
   FROM      AdditionalContactsInformation
   WHERE     (vcPosition = 843 or coalesce(bitPrimaryContact,false) = true)
   AND numDivisionId IN(SELECT DISTINCT
      numDivisionId
      FROM    tt_TEMPAPRECORD1
      WHERE   numUserCntID = v_numTempDomainId)) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;

                    
--SELECT  GETDATE()
------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM    Domain WHERE   numDomainID = v_numTempDomainId;
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------

-----------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numCurrentDays = X.numCurrentDays
   FROM(SELECT    SUM(DealAmount) AS numCurrentDays,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId AND
   DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) >= 0
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numCurrentDaysPaid = X.numCurrentDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) >= 0
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intCurrentDaysCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) >= 0
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------

   UPDATE  tt_TEMPAPRECORD1
   SET     numThirtyDays = X.numThirtyDays
   FROM(SELECT    SUM(DealAmount) AS numThirtyDays,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 1
   AND     30
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numThirtyDaysPaid = X.numThirtyDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 1
   AND     30
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intThirtyDaysCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 1
   AND     30
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numSixtyDays = X.numSixtyDays
   FROM(SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 31
   AND     60
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numSixtyDaysPaid = X.numSixtyDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 31
   AND     60
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intSixtyDaysCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 31
   AND     60
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);

------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numNinetyDays = X.numNinetyDays
   FROM(SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 61
   AND     90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numNinetyDaysPaid = X.numNinetyDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 61
   AND     90
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intNinetyDaysCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND DATE_PART('day',dtDueDate -GetUTCDateWithoutTime()) BETWEEN 61
   AND     90
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);

------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numOverNinetyDays = X.numOverNinetyDays
   FROM(SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND dtDueDate >= GetUTCDateWithoutTime()+INTERVAL '91 day'
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
   FROM(SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND dtDueDate >= GetUTCDateWithoutTime()+INTERVAL '91 day'
   AND GetUTCDateWithoutTime() <= dtDueDate
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intOverNinetyDaysCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND dtDueDate >= GetUTCDateWithoutTime()+INTERVAL '91 day'
   AND GetUTCDateWithoutTime() <= dtDueDate
   AND numCurrencyID <> v_baseCurrency);

------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
   FROM(SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 1
   AND     30
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
   FROM(SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 1
   AND     30
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intThirtyDaysOverDueCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 1
   AND     30
   AND numCurrencyID <> v_baseCurrency);

----------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
   FROM(SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 31
   AND     60
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
   FROM(SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 31
   AND     60
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intSixtyDaysOverDueCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 31
   AND     60
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
   FROM(SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 61
   AND     90
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
   FROM(SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 61
   AND     90
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intNinetyDaysOverDueCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) BETWEEN 61
   AND     90
   AND numCurrencyID <> v_baseCurrency);
------------------------------------------
   UPDATE  tt_TEMPAPRECORD1
   SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
   FROM(SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) > 90
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
   FROM(SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
   FROM      tt_TEMPAPRECORD
   WHERE     numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) > 90
   GROUP BY  numDivisionId) X
   WHERE   tt_TEMPAPRECORD1.numDivisionID = X.numDivisionID;
        
   UPDATE  tt_TEMPAPRECORD1
   SET     intOverNinetyDaysOverDueCount = 2
   WHERE   numDivisionId IN(SELECT DISTINCT
   numDivisionId
   FROM    tt_TEMPAPRECORD
   WHERE   numUserCntID = v_numTempDomainId
   AND GetUTCDateWithoutTime() > dtDueDate
   AND DATE_PART('day',GetUTCDateWithoutTime() -dtDueDate) > 90
   AND numCurrencyID <> v_baseCurrency);
             

   UPDATE
   tt_TEMPAPRECORD1 TR1
   SET
   monUnAppliedAmount = coalesce((SELECT SUM(coalesce(monUnAppliedAmount,0)) FROM  tt_TEMPAPRECORD TR2 WHERE TR2.numDivisionId = TR1.numDivisionId),0);

   UPDATE  tt_TEMPAPRECORD1 SET numTotal =  coalesce(numCurrentDays,0)+ --isnull(numThirtyDays, 0) + 
				coalesce(numThirtyDaysOverDue,0)
                --+ isnull(numSixtyDays, 0) 
				+coalesce(numSixtyDaysOverDue,0)
                --+ isnull(numNinetyDays, 0) 
				+coalesce(numNinetyDaysOverDue,0)
                --+ isnull(numOverNinetyDays, 0)
                +coalesce(numOverNinetyDaysOverDue,0); 			
             
   open SWV_RefCur for SELECT  numDivisionId,
                numContactId,
                tintCRMType,
                vcCompanyName,
                vcCustPhone,
                vcContactName as vcApName,
                vcEmail,
                numPhone as vcPhone,
				numCurrentDays,
                numThirtyDays,
                numSixtyDays,
                numNinetyDays,
                numOverNinetyDays,
                numThirtyDaysOverDue,
                numSixtyDaysOverDue,
                numNinetyDaysOverDue,
                numOverNinetyDaysOverDue,
             coalesce(numTotal,0) -coalesce(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
				intCurrentDaysCount,
                intThirtyDaysCount,
                intThirtyDaysOverDueCount,
                intSixtyDaysCount,
                intSixtyDaysOverDueCount,
                intNinetyDaysCount,
                intOverNinetyDaysCount,
                intNinetyDaysOverDueCount,
                intOverNinetyDaysOverDueCount,
				numCurrentDaysPaid,
                numThirtyDaysPaid,
                numSixtyDaysPaid,
                numNinetyDaysPaid,
                numOverNinetyDaysPaid,
                numThirtyDaysOverDuePaid,
                numSixtyDaysOverDuePaid,
                numNinetyDaysOverDuePaid,
                numOverNinetyDaysOverDuePaid,cast(coalesce(monUnAppliedAmount,0) as DECIMAL(20,5)) AS monUnAppliedAmount
   FROM    tt_TEMPAPRECORD1
   WHERE   numUserCntID = v_numTempDomainId AND (numDivisionId = v_numTempDivisionId OR v_numTempDivisionId IS NULL)
   ORDER BY vcCompanyName;

  
   RETURN; 
END; 
$$;












