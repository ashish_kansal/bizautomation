-- Stored procedure definition script USP_WarehouseItems_GetByItemAndExternalInternalLocation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WarehouseItems_GetByItemAndExternalInternalLocation(v_numDomainID NUMERIC(18,0),
v_numItemCode NUMERIC(18,0),
v_numWareHouseID NUMERIC(18,0),
v_numWarehouseLocationID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   numWareHouseItemID
   FROM
   WareHouseItems
   WHERE
   numDomainID = v_numDomainID
   AND numItemID = v_numItemCode
   AND numWareHouseID = v_numWareHouseID
   AND (numWLocationID = v_numWarehouseLocationID OR v_numWarehouseLocationID = 0)
   ORDER BY
   numWareHouseItemID LIMIT 1;
END; $$;












