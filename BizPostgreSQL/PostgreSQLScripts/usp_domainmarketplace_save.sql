-- Stored procedure definition script USP_DomainMarketplace_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DomainMarketplace_Save(v_numDomainID NUMERIC(18,0)
	,v_numMarketplaceID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   DomainMarketplace
   SET
   numDivisionID = v_numDivisionID
   WHERE
   numDomainID = v_numDomainID
   AND numMarketplaceID = v_numMarketplaceID;
   RETURN;
END; $$;



