-- Stored procedure definition script Usp_AddContactScheduled for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_AddContactScheduled(v_numContactID NUMERIC(9,0),
v_numScheduleId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into CustRptSchContacts(numScheduleId,numcontactId)
values(v_numScheduleId,v_numContactID);
RETURN;
END; $$;


