-- Stored procedure definition script USP_GetAllOppBizDocItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAllOppBizDocItems(v_numOppId NUMERIC(9,0) DEFAULT null,                                                                                                                                                                                
 v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,                                                                                                
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(OBI.numOppBizDocID as VARCHAR(255)), cast(OBI.numItemCode as VARCHAR(255)),cast(IT.vcSKU as VARCHAR(255)),cast(OBI.numOppBizDocItemID as VARCHAR(255)), cast(OMAPI.vcAPIOppId as VARCHAR(255)),cast(OBI.monShipCost as VARCHAR(255)),
cast(OBI.vcTrackingNo as VARCHAR(255)),cast(OBI.vcShippingMethod as VARCHAR(255)),cast(OBI.vcNotes as VARCHAR(255)) FROM  OpportunityBizDocItems OBI
   INNER JOIN OpportunityBizDocs OB ON OBI.numOppBizDocID = OB.numOppBizDocsId
   INNER JOIN OpportunityMaster OM ON OB.numoppid = OM.numOppId
   INNER JOIN OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
   INNER JOIN Item IT ON OBI.numItemCode = IT.numItemCode
   WHERE OB.numoppid = v_numOppId AND OB.numOppBizDocsId = v_numOppBizDocsId;
        
/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:16:13 ******/
   RETURN;
END; $$;












