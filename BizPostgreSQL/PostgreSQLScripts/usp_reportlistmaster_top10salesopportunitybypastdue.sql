-- Stored procedure definition script USP_ReportListMaster_Top10SalesOpportunityByPastDue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10SalesOpportunityByPastDue(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_tintOppType INTEGER
	,v_vcDealAmount VARCHAR(250)
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_tintTotalProgress INTEGER
	,v_tintMinNumber INTEGER
	,v_tintMaxNumber INTEGER
	,v_tintQtyToDisplay INTEGER
	,v_vcDueDate VARCHAR(250)
	,v_dtFromDate DATE
	,v_dtToDate DATE,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintFiscalStartMonth  SMALLINT;
   v_CurrentYearStartDate  DATE; 
   v_CurrentYearEndDate  DATE;

   v_TodayDate  DATE;
   v_CreatedDate  DATE;
BEGIN
   select   coalesce(tintFiscalStartMonth,1) INTO v_tintFiscalStartMonth FROM Domain WHERE numDomainId = v_numDomainID;
   v_CurrentYearStartDate := make_date(EXTRACT(YEAR FROM TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,v_tintFiscalStartMonth,1);
   IF v_CurrentYearStartDate > TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) then
	
      v_CurrentYearStartDate := v_CurrentYearStartDate+INTERVAL '-1 year';
   end if;	
   v_CurrentYearEndDate := v_CurrentYearStartDate+INTERVAL '1 year'+INTERVAL '-1 day';

   DROP TABLE IF EXISTS tt_TABLEQUATER CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEQUATER
   (
      ID INTEGER,
      QuaterStartDate DATE,
      QuaterEndDate DATE
   );

   INSERT INTO tt_TABLEQUATER(ID
		,QuaterStartDate
		,QuaterEndDate)
	VALUES(1,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1),make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '3 month'+INTERVAL '-1 day'),(2,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '3 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '6 month'+INTERVAL '-1 day'),
   (3,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '6 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '9 month'+INTERVAL '-1 day'),(4,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '9 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '12 month'+INTERVAL '-1 day');

	
   IF v_vcDueDate = 'CurYear' then
	
      v_dtFromDate := v_CurrentYearStartDate;
      v_dtToDate := v_CurrentYearEndDate;
   ELSEIF v_vcDueDate = 'PreYear'
   then
	
      v_dtFromDate :=  v_CurrentYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_dtFromDate+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'Pre2Year'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-2 year';
      v_dtToDate := v_dtFromDate+INTERVAL '2 year'+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'Ago2Year'
   then
	
      v_dtFromDate :=  NULL;
      v_dtToDate := v_CurrentYearStartDate+INTERVAL '-2 year'+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'CurPreYear'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_dtFromDate+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'CurPre2Year'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-2 year';
      v_dtToDate := make_date(EXTRACT(YEAR FROM TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'CuQur'
   then
	
      select   QuaterStartDate, QuaterEndDate INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate:: BYTEA AND QuaterEndDate:: BYTEA;
   ELSEIF v_vcDueDate = 'PreQur'
   then
	
      select   QuaterStartDate+INTERVAL '-3 month', QuaterEndDate+INTERVAL '-3 month' INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate:: BYTEA AND QuaterEndDate:: BYTEA;
   ELSEIF v_vcDueDate = 'CurPreQur'
   then
	
      select   QuaterStartDate+INTERVAL '-3 month', QuaterEndDate INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate:: BYTEA AND QuaterEndDate:: BYTEA;
   ELSEIF v_vcDueDate = 'ThisMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcDueDate = 'LastMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
   ELSEIF v_vcDueDate = 'CurPreMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcDueDate = 'LastWeek'
   then
	
      v_dtFromDate := get_week_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-7 day';
      v_dtToDate := get_week_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-7 day';
   ELSEIF v_vcDueDate = 'ThisWeek'
   then
	
      v_dtFromDate := get_week_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
      v_dtToDate := get_week_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcDueDate = 'Yesterday'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 day';
   ELSEIF v_vcDueDate = 'Today'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcDueDate = 'Last7Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-7 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcDueDate = 'Last30Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-30 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcDueDate = 'Last60Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-60 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcDueDate = 'Last90Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-90 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcDueDate = 'Last120Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-120 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   end if;

   v_TodayDate := TIMEZONE('UTC',now())+CAST(v_ClientTimeZoneOffset || 'minute' as interval);

   IF v_vcTimeLine = 'Last12Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-12 month';
   end if;
   IF v_vcTimeLine = 'Last6Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-6 month';
   end if;
   IF v_vcTimeLine = 'Last3Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-3 month';
   end if;
   IF v_vcTimeLine = 'Last30Days' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-30 day';
   end if;
   IF v_vcTimeLine = 'Last7Days' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-7 day';
   end if;
	

   open SWV_RefCur for SELECT DISTINCT OM.vcpOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,coalesce(monTotAmount,0) AS monTotAmount
		,cast(coalesce(OM.numPercentageComplete,0) as NUMERIC(9,0)) AS numPercentageComplete
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   LEFT JOIN
   ProjectProgress
   ON
   OM.numOppId = ProjectProgress.numOppId
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 50
   AND (coalesce(LD.constFlag,false) = true OR LD.numDomainid = v_numDomainID)
   AND LD.numListItemID = OM.numPercentageComplete
   WHERE
   OM.numDomainId = v_numDomainID
   AND coalesce(OM.tintopptype,0) = v_tintOppType
   AND coalesce(OM.tintoppstatus,0) = 0
   AND 1 =(CASE WHEN(v_tintTotalProgress) > 0 THEN(CASE WHEN coalesce(ProjectProgress.intTotalProgress,0) >= v_tintTotalProgress  THEN 1 ELSE 0 END) ELSE 1 END)
   AND	1 =(CASE WHEN OM.monDealAmount BETWEEN v_tintMinNumber:: DECIMAL(20,5) AND v_tintMaxNumber:: DECIMAL(20,5) THEN 1 ELSE 0 END)
   AND OM.intPEstimatedCloseDate IS NOT NULL
		--AND OM.intPEstimatedCloseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
   AND CAST(OM.intPEstimatedCloseDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate 
   AND OM.bintCreatedDate >= v_CreatedDate
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcTeritorry,'')) > 0 THEN(CASE WHEN DM.numTerID IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE
   WHEN LENGTH(coalesce(v_vcFilterValue,cast(0 as TEXT))) > 0 AND v_vcFilterBy IN(1,2,3)
   THEN(CASE v_vcFilterBy
      WHEN 1  -- Assign To
      THEN(CASE WHEN OM.numassignedto IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 2  -- Record Owner
      THEN(CASE WHEN OM.numrecowner IN(SELECT Id FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 3  -- Teams (Based on Assigned To)
      THEN(CASE WHEN OM.numassignedto IN(SELECT numUserCntID FROM UserTeams WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT Id FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
      END)
   ELSE 1
   END)
   AND (1 =(CASE WHEN POSITION('1-5K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OM.monDealAmount BETWEEN 1000:: DECIMAL(20,5) AND 5000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('5-10K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OM.monDealAmount BETWEEN 5000:: DECIMAL(20,5) AND 10000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('10-20K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OM.monDealAmount BETWEEN 10000:: DECIMAL(20,5) AND 20000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('20-50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OM.monDealAmount BETWEEN 20000:: DECIMAL(20,5) AND 50000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('>50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OM.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
   ORDER BY
   coalesce(monTotAmount,0) DESC;
END; $$;












