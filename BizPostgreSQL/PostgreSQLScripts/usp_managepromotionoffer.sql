DROP FUNCTION IF EXISTS USP_ManagePromotionOffer;

CREATE OR REPLACE FUNCTION USP_ManagePromotionOffer(v_numProId NUMERIC(9,0) DEFAULT 0,
    v_vcProName VARCHAR(100) DEFAULT NULL,
    v_numDomainId NUMERIC(18,0) DEFAULT NULL,
    v_dtValidFrom DATE DEFAULT NULL,
    v_dtValidTo DATE DEFAULT NULL,
	v_bitNeverExpires BOOLEAN DEFAULT NULL,
	v_bitUseOrderPromotion BOOLEAN DEFAULT NULL,
	v_numOrderPromotionID NUMERIC(18,0) DEFAULT NULL,
	v_tintOfferTriggerValueType SMALLINT DEFAULT NULL,
	v_fltOfferTriggerValue INTEGER DEFAULT NULL,
	v_tintOfferBasedOn SMALLINT DEFAULT NULL,
	v_tintDiscountType SMALLINT DEFAULT NULL,
	v_fltDiscountValue DOUBLE PRECISION DEFAULT NULL,
	v_tintDiscoutBaseOn SMALLINT DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
	v_tintCustomersBasedOn SMALLINT DEFAULT NULL,
	v_tintOfferTriggerValueTypeRange SMALLINT DEFAULT NULL,
	v_fltOfferTriggerValueRange DOUBLE PRECISION DEFAULT NULL,
	v_vcShortDesc VARCHAR(100) DEFAULT NULL,
	v_vcLongDesc VARCHAR(500) DEFAULT NULL,
	v_tintItemCalDiscount SMALLINT DEFAULT NULL,
	v_monDiscountedItemPrice DECIMAL(20,5) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_ERRORMESSAGEVALIDATION  VARCHAR(50);
BEGIN
   IF v_numProId = 0 then
	
      INSERT INTO PromotionOffer(vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion)
        VALUES(v_vcProName
           ,v_numDomainId
           ,v_numUserCntID
		   ,TIMEZONE('UTC',now())
		   ,false
		   ,false);
		
      open SWV_RefCur for
      SELECT CURRVAL('PromotionOffer_seq');
   ELSE
      BEGIN
         -- BEGIN TRANSACTION
IF v_tintOfferBasedOn <> 4 then
			
            IF(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = coalesce(v_tintOfferBasedOn,0) AND tintRecordType = 5) = 0 then
				
               RAISE EXCEPTION 'SELECT_ITEMS_FOR_PROMOTIONS';
               RETURN;
            end if;
         end if;
         IF coalesce(v_tintDiscoutBaseOn,0) = 1 AND(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 1 AND tintRecordType = 6) = 0 then
			
            RAISE EXCEPTION 'SELECT_DISCOUNTED_ITEMS';
            RETURN;
         ELSEIF coalesce(v_tintDiscoutBaseOn,0) = 2  AND(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 2 AND tintRecordType = 6) = 0
         then
			
            RAISE EXCEPTION 'SELECT_DISCOUNTED_ITEMS';
            RETURN;
         ELSEIF coalesce(v_tintDiscoutBaseOn,0) = 6  AND(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = v_numProId AND tintType = 4 AND tintRecordType = 6) = 0
         then
			
            RAISE EXCEPTION 'SELECT_DISCOUNTED_ITEMS';
            RETURN;
         end if;
         IF coalesce(v_bitUseOrderPromotion,false) = false then
			
				/*Check For Duplicate Customer-Item relationship 
					1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say “You’ve already created a promotion rule that targets this customer and item by name”*/
            IF v_tintCustomersBasedOn = 1 then
				
               IF coalesce(v_tintCustomersBasedOn,0) = 1 AND(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = v_numProId AND tintType = 1) = 0 then
					
                  RAISE EXCEPTION 'SELECT_INDIVIDUAL_CUSTOMER';
                  RETURN;
               end if;
			
					/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say “You’ve already created a promotion rule that targets this relationship / profile and one or more item specifically”*/
            ELSEIF v_tintCustomersBasedOn = 2
            then
				
               IF coalesce(v_tintCustomersBasedOn,0) = 2 AND(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = v_numProId AND tintType = 2) = 0 then
					
                  RAISE EXCEPTION 'SELECT_RELATIONSHIP_PROFILE';
                  RETURN;
               end if;
            ELSEIF v_tintCustomersBasedOn = 0
            then
				
               RAISE EXCEPTION 'SELECT_CUSTOMERS';
               RETURN;
            end if;
            IF v_tintCustomersBasedOn = 3 then
				
               DELETE FROM PromotionOfferOrganizations
               WHERE tintType IN(1,2)
               AND numProId = v_numProId;
            end if;
            IF(SELECT
            COUNT(*)
            FROM
            PromotionOffer
            LEFT JOIN
            PromotionOfferOrganizations
            ON
            PromotionOffer.numProId = PromotionOfferOrganizations.numProId
            AND 1 =(CASE
            WHEN v_tintCustomersBasedOn = 1 THEN(CASE WHEN PromotionOfferOrganizations.tintType = 1 THEN 1 ELSE 0 END)
            WHEN v_tintCustomersBasedOn = 2 THEN(CASE WHEN PromotionOfferOrganizations.tintType = 2 THEN 1 ELSE 0 END)
            ELSE 1
            END)
            LEFT JOIN
            PromotionOfferItems
            ON
            PromotionOffer.numProId = PromotionOfferItems.numProId
            AND 1 =(CASE
            WHEN v_tintOfferBasedOn = 1 THEN(CASE WHEN PromotionOfferItems.tintType = 1 THEN 1 ELSE 0 END)
            WHEN v_tintOfferBasedOn = 2 THEN(CASE WHEN PromotionOfferItems.tintType = 2 THEN 1 ELSE 0 END)
            ELSE 1
            END)
            AND PromotionOfferItems.tintRecordType = 5
            LEFT JOIN
            PromotionOfferItems PromotionOfferItemsDiscount
            ON
            PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
            AND 1 =(CASE
            WHEN v_tintDiscoutBaseOn = 1 THEN(CASE WHEN PromotionOfferItemsDiscount.tintType = 1 THEN 1 ELSE 0 END)
            WHEN v_tintDiscoutBaseOn = 2 THEN(CASE WHEN PromotionOfferItemsDiscount.tintType = 2 THEN 1 ELSE 0 END)
            ELSE 1
            END)
            AND PromotionOfferItemsDiscount.tintRecordType = 6
            WHERE
            PromotionOffer.numDomainId = v_numDomainId
            AND PromotionOffer.numProId <> v_numProId
            AND tintCustomersBasedOn = v_tintCustomersBasedOn
            AND tintDiscoutBaseOn = v_tintDiscoutBaseOn
            AND tintOfferBasedOn = v_tintOfferBasedOn
            AND tintOfferTriggerValueType = v_tintOfferTriggerValueType
            AND 1 =(CASE
            WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
            THEN(CASE
               WHEN v_tintOfferTriggerValueTypeRange = 1 THEN(CASE WHEN fltOfferTriggerValue = v_fltOfferTriggerValue THEN 1 ELSE 0 END)
               WHEN v_tintOfferTriggerValueTypeRange = 2 THEN(CASE WHEN fltOfferTriggerValue BETWEEN v_fltOfferTriggerValue AND v_fltOfferTriggerValueRange THEN 1 ELSE 0 END)
               ELSE 0
               END)
            WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
            THEN(CASE
               WHEN v_tintOfferTriggerValueTypeRange = 1 THEN(CASE WHEN v_fltOfferTriggerValue BETWEEN fltOfferTriggerValue:: INTEGER AND fltOfferTriggerValueRange:: INTEGER THEN 1 ELSE 0 END)
               WHEN v_tintOfferTriggerValueTypeRange = 2 THEN(CASE
                  WHEN v_fltOfferTriggerValue BETWEEN fltOfferTriggerValue:: INTEGER AND fltOfferTriggerValueRange:: INTEGER OR v_fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue:: DOUBLE PRECISION AND fltOfferTriggerValueRange:: DOUBLE PRECISION THEN 1 ELSE 0 END)
               ELSE 0
               END)
            ELSE 0
            END)
            AND 1 =(CASE
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 1 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 2  AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 2  AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 2 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4) THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 4 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 1 AND POOInner.numDivisionID = PromotionOfferOrganizations.numDivisionID) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 1 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 2 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 2 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 2 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 4 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId = v_numProId AND POOInner.tintType = 2 AND POOInner.numRelationship = PromotionOfferOrganizations.numRelationship AND POOInner.numProfile = PromotionOfferOrganizations.numProfile) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 1 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 1 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 2 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 2 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               AND(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 2 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4) THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 5 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItems.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 1 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 1 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 4 AND v_tintDiscoutBaseOn = 2 THEN(CASE
               WHEN(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId = v_numProId AND POIInner.tintRecordType = 6 AND POIInner.tintType = 2 AND POIInner.numValue = PromotionOfferItemsDiscount.numValue) > 0
               THEN
                  1
               ELSE
                  0
               END)
            WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 4 AND (v_tintDiscoutBaseOn = 3 OR v_tintDiscoutBaseOn = 4)  THEN 3
            END)) > 0 then
               v_ERRORMESSAGEVALIDATION :=(CASE
               WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 1 THEN 'ORGANIZATION-ITEM'
               WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
               WHEN v_tintCustomersBasedOn = 1 AND v_tintOfferBasedOn = 4 THEN 'ORGANIZATION-ALLITEMS'
               WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 1 THEN 'RELATIONSHIPPROFILE-ITEMS'
               WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
               WHEN v_tintCustomersBasedOn = 2 AND v_tintOfferBasedOn = 4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
               WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 1 THEN 'ALLORGANIZATIONS-ITEMS'
               WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
               WHEN v_tintCustomersBasedOn = 3 AND v_tintOfferBasedOn = 4 THEN 'ALLORGANIZATIONS-ALLITEMS'
               END);
               RAISE EXCEPTION '%',v_ERRORMESSAGEVALIDATION;
               RETURN;
            end if;
         ELSE
            DELETE FROM PromotionOfferOrganizations WHERE numProId = v_numProId;
         end if;
         UPDATE
         PromotionOffer
         SET
         vcProName = v_vcProName,numDomainId = v_numDomainId,dtValidFrom = v_dtValidFrom,
         dtValidTo = v_dtValidTo,bitNeverExpires = v_bitNeverExpires,
         bitUseOrderPromotion = v_bitUseOrderPromotion,numOrderPromotionID =(CASE WHEN coalesce(v_bitUseOrderPromotion,false) = true THEN v_numOrderPromotionID ELSE 0 END),
         tintOfferTriggerValueType = v_tintOfferTriggerValueType,
         fltOfferTriggerValue = v_fltOfferTriggerValue,tintOfferBasedOn = v_tintOfferBasedOn,
         tintDiscountType = v_tintDiscountType,fltDiscountValue = v_fltDiscountValue,
         tintDiscoutBaseOn = v_tintDiscoutBaseOn,numModifiedBy = v_numUserCntID,
         dtModified = TIMEZONE('UTC',now()),tintCustomersBasedOn = v_tintCustomersBasedOn,
         tintOfferTriggerValueTypeRange = v_tintOfferTriggerValueTypeRange,
         fltOfferTriggerValueRange = v_fltOfferTriggerValueRange,
         vcShortDesc = v_vcShortDesc,vcLongDesc = v_vcLongDesc,
         tintItemCalDiscount = v_tintItemCalDiscount,bitError = false,monDiscountedItemPrice = v_monDiscountedItemPrice
         WHERE
         numProId = v_numProId;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            -- ROLLBACK TRANSACTION
UPDATE PromotionOffer SET bitEnabled = false,bitError = true WHERE numProId = v_numProId;
            GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
      open SWV_RefCur for
      SELECT  v_numProId;
   end if;
END; $$;



