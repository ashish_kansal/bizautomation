-- Stored procedure definition script USP_Item_MassStockTransfer for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_MassStockTransfer(v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_strItems TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   
   v_description  TEXT;

		

   v_j  INTEGER; 
   v_jCount  INTEGER;
   v_numTempWareHouseItmsDTLID  NUMERIC(18,0);
   v_numTempLotQty  NUMERIC(18,0);
   v_vcTemp  TEXT;
   v_i  INTEGER DEFAULT 1;
   v_numTempItemCode  NUMERIC(18,0);
   v_numTempToItemCode  NUMERIC(18,0);
   v_numTempQty  NUMERIC(18,0);
   v_numTempFromWarehouseItemID  NUMERIC(18,0);
   v_numTempToWarehouseItemID  NUMERIC(18,0);
   v_bitTempSerial  BOOLEAN;
   v_bitTempLot  BOOLEAN;
   v_vcTempSelectedSerialLotNumbers  TEXT;
   v_numFromOnHandBefore  DOUBLE PRECISION;
   v_numFromOnAllocationBefore  DOUBLE PRECISION;
   v_numFromOnHandAfter  DOUBLE PRECISION;
   v_numToOnHandBefore  DOUBLE PRECISION;
   v_numToOnHandAfter  DOUBLE PRECISION;

   v_iCount  INTEGER;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF LENGTH(coalesce(v_strItems,'')) > 0 then
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numFromItemCode NUMERIC(18,0),
            numQty NUMERIC(18,0),
            numFromWarehouseItemID NUMERIC(18,0),
            numToItemCode NUMERIC(18,0),
            numToWarehouseItemID NUMERIC(18,0),
            bitSerial BOOLEAN,
            bitLot BOOLEAN,
            vcSelectedSerialLotNumbers TEXT
         );
         DROP TABLE IF EXISTS tt_TEMPSETIALLOT CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPSETIALLOT
         (
            ID INTEGER,
            vcSerialLot VARCHAR(100)
         );
         INSERT INTO tt_TEMP(numFromItemCode
			,numQty
			,numFromWarehouseItemID
			,numToItemCode
			,numToWarehouseItemID
			,bitSerial
			,bitLot
			,vcSelectedSerialLotNumbers)
         SELECT
         coalesce(numFromItemCode,0),
			coalesce(numUnitHour,0),
			coalesce(numFromWarehouseItemID,0),
			coalesce(numToItemCode,0),
			coalesce(numToWarehouseItemID,0),
			coalesce(IsSerial,false),
			coalesce(IsLot,false),
			coalesce(vcSelectedSerialLotNumbers,'')
         FROM
		 XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				numFromItemCode NUMERIC(18,0) PATH 'numFromItemCode',
				numUnitHour NUMERIC(18,0) PATH 'numUnitHour',
				numFromWarehouseItemID NUMERIC(18,0) PATH 'numFromWarehouseItemID',
				numToItemCode NUMERIC(18,0) PATH 'numToItemCode',
				numToWarehouseItemID NUMERIC(18,0) PATH 'numToWarehouseItemID',
				IsSerial BOOLEAN PATH 'IsSerial',
				IsLot BOOLEAN PATH 'IsLot',
				vcSelectedSerialLotNumbers TEXT PATH 'vcSelectedSerialLotNumbers'
		) AS X;

         
         select   COUNT(ID) INTO v_iCount FROM tt_TEMP;
         IF v_iCount = 0 then
		
            RAISE EXCEPTION 'NO_ITEMS_ADDED_FOR_STOCK_TRANSFER';
         ELSE
            WHILE v_i <= v_iCount LOOP
               select   numFromItemCode, numToItemCode, numQty, numFromWarehouseItemID, numToWarehouseItemID, bitSerial, bitLot, vcSelectedSerialLotNumbers INTO v_numTempItemCode,v_numTempToItemCode,v_numTempQty,v_numTempFromWarehouseItemID,
               v_numTempToWarehouseItemID,v_bitTempSerial,v_bitTempLot,v_vcTempSelectedSerialLotNumbers FROM
               tt_TEMP WHERE
               ID = v_i;
               IF v_numTempQty > 0 then
				
                  IF NOT EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numTempItemCode) then
					
                     RAISE EXCEPTION 'INVLID_ITEM_ID';
                  end if;
                  IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numTempFromWarehouseItemID) then
					
                     RAISE EXCEPTION 'INVLID_FROM_LOCATION';
                  end if;
                  IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numTempToWarehouseItemID) then
					
                     RAISE EXCEPTION 'INVLID_TO_LOCATION';
                  end if;
                  IF (v_bitTempSerial = true OR v_bitTempLot = true) AND LENGTH(v_vcTempSelectedSerialLotNumbers) = 0 then
					
                     RAISE EXCEPTION 'SERIAL/LOT#_ARE_NOT_SELECTED';
                  end if;
                  select   coalesce(numOnHand,0), coalesce(numAllocation,0) INTO v_numFromOnHandBefore,v_numFromOnAllocationBefore FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numTempFromWarehouseItemID;
                  IF(v_numFromOnHandBefore+v_numFromOnAllocationBefore) < v_numTempQty then
					
                     RAISE EXCEPTION 'INSUFFICIENT_ONHAND_QUANTITY';
                  ELSE
                     IF v_bitTempSerial = true OR v_bitTempLot = true then
						
                        v_vcTemp := '';
                        DELETE FROM tt_TEMPSETIALLOT;
                        INSERT INTO tt_TEMPSETIALLOT(ID,
								vcSerialLot)
                        SELECT
                        ROW_NUMBER() OVER(ORDER BY OutParam)
								,OutParam
                        FROM
                        SplitString(v_vcTempSelectedSerialLotNumbers,',');
                        v_j := 1;
                        select   COUNT(*) INTO v_jCount FROM tt_TEMPSETIALLOT;
                        IF v_jCount > 0 then
							
                           WHILE v_j <= v_jCount LOOP
                              select   SUBSTR(vcSerialLot,0,POSITION('-' IN vcSerialLot)), SUBSTR(vcSerialLot,POSITION('-' IN vcSerialLot)+1,LENGTH(vcSerialLot)+1) INTO v_numTempWareHouseItmsDTLID,v_numTempLotQty FROM
                              tt_TEMPSETIALLOT WHERE
                              ID = v_j;
                              IF coalesce(v_numTempLotQty,0) > 0 then
									
                                 IF v_bitTempLot = true then
										
                                    IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempFromWarehouseItemID AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID AND numQty >= v_numTempLotQty) then
											
                                       SELECT CONCAT(v_vcTemp,(CASE WHEN LENGTH(v_vcTemp) = 0 THEN '' ELSE ',' END),coalesce(vcSerialNo,''),
                                       '(',v_numTempLotQty,')') INTO v_vcTemp FROM WareHouseItmsDTL  WHERE
                                       numWareHouseItemID = v_numTempFromWarehouseItemID
                                       AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID LIMIT 1;
                                       UPDATE
                                       WareHouseItmsDTL SET numQty = numQty -v_numTempLotQty  WHERE
                                       numWareHouseItemID = v_numTempFromWarehouseItemID
                                       AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
                                       INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
													vcSerialNo,
													numQty,
													dExpirationDate,
													bitAddedFromPO)
                                       SELECT
                                       v_numTempToWarehouseItemID,
													vcSerialNo,
													v_numTempLotQty,
													dExpirationDate,
													bitAddedFromPO
                                       FROM
                                       WareHouseItmsDTL
                                       WHERE
                                       numWareHouseItemID = v_numTempFromWarehouseItemID AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
                                    ELSE
                                       RAISE EXCEPTION 'INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY';
                                    end if;
                                 ELSE
                                    IF EXISTS(SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempFromWarehouseItemID AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID AND numQty = 1) then
											
                                       SELECT coalesce(v_vcTemp,'') ||(CASE WHEN LENGTH(v_vcTemp) = 0 THEN '' ELSE ',' END) || coalesce(vcSerialNo,'') INTO v_vcTemp FROM WareHouseItmsDTL  WHERE
                                       numWareHouseItemID = v_numTempFromWarehouseItemID
                                       AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID LIMIT 1;
                                       UPDATE
                                       WareHouseItmsDTL SET numWareHouseItemID = v_numTempToWarehouseItemID  WHERE
                                       numWareHouseItemID = v_numTempFromWarehouseItemID
                                       AND numWareHouseItmsDTLID = v_numTempWareHouseItmsDTLID;
                                    ELSE
                                       RAISE EXCEPTION 'INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY';
                                    end if;
                                 end if;
                              ELSE
                                 RAISE EXCEPTION 'INVALID_SERIAL/LOT#_QTY';
                              end if;
                              v_j := v_j::bigint+1;
                           END LOOP;
                        ELSE
                           RAISE EXCEPTION 'SERIAL/LOT#_ARE_NOT_SELECTED';
                        end if;
                     end if;
                     v_description := '';
                     IF v_bitTempSerial = true OR v_bitTempLot = true then
						
                        v_description := CONCAT('Mass Stock Transfer - Transferred (Qty:',v_numTempQty,', Serial/Lot#:',
                        ')');
                     ELSE
                        v_description := CONCAT('Mass Stock Transfer - Transferred (Qty:',v_numTempQty,')');
                     end if;
                     SELECT(CASE WHEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) >= 0 THEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) ELSE 0 END) INTO v_numFromOnHandAfter FROM WareHouseItems  WHERE
                     numWareHouseItemID = v_numTempFromWarehouseItemID LIMIT 1;
                     UPDATE
                     WareHouseItems SET numOnHand =(CASE WHEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) >= 0 THEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) ELSE 0 END),numAllocation =(CASE WHEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) < 0 THEN(CASE WHEN coalesce(numAllocation,0) -(coalesce(v_numTempQty,0) -coalesce(numOnHand,0)) >= 0 THEN coalesce(numAllocation,0) -(coalesce(v_numTempQty,0) -coalesce(numOnHand,0)) ELSE 0 END) ELSE coalesce(numAllocation,0) END),numBackOrder =(CASE WHEN coalesce(numOnHand,0) -coalesce(v_numTempQty,0) < 0 THEN coalesce(numBackOrder,0)+(CASE WHEN coalesce(numAllocation,0) -(coalesce(v_numTempQty,0) -coalesce(numOnHand,0)) >= 0 THEN coalesce(v_numTempQty,0) -coalesce(numOnHand,0) ELSE coalesce(numAllocation,0) END) ELSE coalesce(numBackOrder,0) END),
                     dtModified = LOCALTIMESTAMP   WHERE
                     numWareHouseItemID = v_numTempFromWarehouseItemID;
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempFromWarehouseItemID,v_numReferenceID := v_numTempItemCode,
                     v_tintRefType := 1::SMALLINT,v_vcDescription := v_description,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                     IF v_bitTempSerial = true OR v_bitTempLot = true then
						
                        v_description := CONCAT('Mass Stock Transfer - Receieved (Qty:',v_numTempQty,', Serial/Lot#:',
                        ')');
                     ELSE
                        v_description := CONCAT('Mass Stock Transfer - Receieved (Qty:',v_numTempQty,')');
                     end if;
                     select   numOnHand INTO v_numToOnHandBefore FROM WareHouseItems WHERE numWareHouseItemID = v_numTempToWarehouseItemID;
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand =(CASE WHEN numBackOrder >= v_numTempQty THEN numOnHand ELSE coalesce(numOnHand,0)+(v_numTempQty -coalesce(numBackOrder,0)) END),numAllocation =(CASE WHEN numBackOrder >= v_numTempQty THEN coalesce(numAllocation,0)+v_numTempQty ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),numBackOrder =(CASE WHEN numBackOrder >= v_numTempQty THEN coalesce(numBackOrder,0) -v_numTempQty ELSE 0 END),
                     dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numTempToWarehouseItemID;
                     select   numOnHand INTO v_numToOnHandAfter FROM WareHouseItems WHERE numWareHouseItemID = v_numTempToWarehouseItemID;
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempToWarehouseItemID,v_numReferenceID := v_numTempToItemCode,
                     v_tintRefType := 1::SMALLINT,v_vcDescription := v_description,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                     INSERT INTO MassStockTransfer(numDomainId
							,numItemCode
							,numFromWarehouseItemID
							,numToItemCode
							,numToWarehouseItemID
							,numQty
							,numTransferredBy
							,dtTransferredDate
							,bitMassTransfer
							,vcSerialLot
							,numFromQtyBefore
							,numToQtyBefore
							,numFromQtyAfter
							,numToQtyAfter)
						VALUES(v_numDomainID
							,v_numTempItemCode
							,v_numTempFromWarehouseItemID
							,v_numTempToItemCode
							,v_numTempToWarehouseItemID
							,v_numTempQty
							,v_numUserCntID
							,TIMEZONE('UTC',now())
							,true
							,v_vcTemp
							,v_numFromOnHandBefore
							,v_numToOnHandBefore
							,v_numFromOnHandAfter
							,v_numToOnHandAfter);
                  end if;
               ELSE
                  RAISE EXCEPTION 'QTY_TO_TRANSFER_CAN_NOT_BE_0';
               end if;
               v_i := v_i::bigint+1;
            END LOOP;
         end if;
      ELSE
         RAISE EXCEPTION 'NO_ITEMS_ADDED_FOR_STOCK_TRANSFER';
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
			GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


