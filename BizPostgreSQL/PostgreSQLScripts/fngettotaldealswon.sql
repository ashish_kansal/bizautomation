CREATE OR REPLACE FUNCTION fnGetTotalDealsWon(v_numDomainID NUMERIC(9,0),
	v_intFromDate TIMESTAMP,
	v_intToDate TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dcDealsWon  INTEGER;
BEGIN
   select   COUNT(OM.numCreatedBy) INTO v_dcDealsWon FROM OpportunityMaster OM WHERE OM.tintoppstatus = 1
   AND OM.numDomainId = v_numDomainID
   AND OM.bintAccountClosingDate BETWEEN v_intFromDate AND v_intToDate;

   RETURN v_dcDealsWon;
END; $$;

