-- Stored procedure definition script USP_MassSalesFulfillment_GetWarehouses for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetWarehouses(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numWareHouseID  AS "numWareHouseID"
		,coalesce(vcWareHouse,'') AS "vcWareHouse"
   FROM
   Warehouses
   WHERE
   numDomainID = v_numDomainID
   ORDER BY
   vcWareHouse;
END; $$;












