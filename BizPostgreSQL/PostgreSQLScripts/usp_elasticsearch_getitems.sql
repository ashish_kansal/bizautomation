DROP FUNCTION IF EXISTS USP_ElasticSearch_GetItems;

CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetItems(v_numDomainID NUMERIC(18,0)
	,v_vcItemCodes TEXT DEFAULT NULL
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CustomerSearchFields  TEXT DEFAULT '';
   v_CustomerSearchCustomFields  TEXT DEFAULT '';
   v_CustomerDisplayFields  TEXT DEFAULT '';
   v_query  TEXT;
   v_CustomFields TEXT;
BEGIN
	DROP TABLE IF EXISTS tt_TEMPSEARCHFIELDS CASCADE;

	CREATE TEMPORARY TABLE tt_TEMPSEARCHFIELDS
	(
		numFieldID NUMERIC(18,0),
		vcDbColumnName VARCHAR(200),
		tintOrder SMALLINT,
		bitCustom BOOLEAN
	);

	INSERT INTO tt_TEMPSEARCHFIELDS
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		CAST(0 AS BOOLEAN) AS Custom
	FROM
		View_DynamicColumns
	WHERE
		numFormId = 22
		AND numDomainID = v_numDomainID
		AND tintPageType = 1
		AND bitCustom = false
		AND coalesce(bitSettingField,false) = true
		AND numRelCntType = 1
   UNION
	SELECT
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		CAST(1 AS BOOLEAN) AS Custom
	FROM
		View_DynamicCustomColumns
	WHERE
		Grp_id = 5
		AND numFormId = 22
		AND numDomainID = v_numDomainID
		AND tintPageType = 1
		AND bitCustom = true
		AND numRelCntType = 1;

	IF NOT EXISTS(SELECT * FROM tt_TEMPSEARCHFIELDS) then
		INSERT INTO tt_TEMPSEARCHFIELDS
		(
			numFieldID
			,vcDbColumnName
			,tintOrder
			,bitCustom
		)
		SELECT
			numFieldId ,
			vcDbColumnName,
			tintorder,
			CAST(0 AS BOOLEAN)
		FROM
			View_DynamicDefaultColumns
		WHERE
			numFormId = 22
			AND bitDefault = true
			AND coalesce(bitSettingField,false) = true
			AND numDomainID = v_numDomainID;
   end if;

	IF EXISTS(SELECT * FROM tt_TEMPSEARCHFIELDS) then
		SELECT 
			string_agg(vcDbColumnName || ' AS "Search_' || vcDbColumnName || '"',', ' ORDER BY tintOrder) INTO v_CustomerSearchFields 
		FROM
			tt_TEMPSEARCHFIELDS 
		WHERE
			coalesce(bitCustom,false) = false;

		IF EXISTS(SELECT * FROM tt_TEMPSEARCHFIELDS WHERE COALESCE(bitCustom,false) = true) then
			SELECT 
				string_agg('COALESCE("CFW' || numFieldID || '",'''') AS ' || '"Search_CFW' || numFieldID || '"', ', ' ORDER BY tintOrder) INTO v_CustomerSearchCustomFields 
			FROM
				tt_TEMPSEARCHFIELDS 
			WHERE
				COALESCE(bitCustom,false) = true;
		END IF;
	end if;

	IF  LENGTH(v_CustomerSearchFields) = 0 AND LENGTH(v_CustomerSearchCustomFields) = 0 then
		v_CustomerSearchFields := 'vcItemName AS "Search_vcItemName"';
	end if;


	----------------------------   Display Fields ---------------------------
	DROP TABLE IF EXISTS tt_TEMPDISPLAYFIELDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPDISPLAYFIELDS
	(
		numFieldID NUMERIC(18,0),
		vcDbColumnName VARCHAR(200),
		tintOrder SMALLINT,
		bitCustom BOOLEAN
	);


	INSERT INTO tt_TEMPDISPLAYFIELDS
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		CAST(0 AS BOOLEAN) AS Custom
	FROM
		View_DynamicColumns
	WHERE
		numFormId = 22
		AND numDomainID = v_numDomainID
		AND tintPageType = 1
		AND bitCustom = false
		AND coalesce(bitSettingField,false) = true
		AND numRelCntType = 0
		AND vcDbColumnName <> 'CustomerPartNo'
	UNION
	SELECT
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		CAST(1 AS BOOLEAN) AS Custom
	FROM
		View_DynamicCustomColumns
	WHERE
		Grp_id = 5
		AND numFormId = 22
		AND numDomainID = v_numDomainID
		AND tintPageType = 1
		AND bitCustom = true
		AND numRelCntType = 0;

	 IF EXISTS(SELECT * FROM tt_TEMPDISPLAYFIELDS) then
		v_CustomerDisplayFields := coalesce((select 
													string_agg(CONCAT('COALESCE(',vcDbColumnName,'::TEXT,'''')'), ' || '','' || ' ORDER BY tintOrder) 
												FROM
													tt_TEMPDISPLAYFIELDS 
												WHERE
													coalesce(bitCustom,false) = false),'');

		IF EXISTS(SELECT * FROM tt_TEMPDISPLAYFIELDS WHERE coalesce(bitCustom,false) = true) then
			v_CustomerDisplayFields := v_CustomerDisplayFields || (CASE WHEN LENGTH(v_CustomerDisplayFields) > 0 THEN ' || '' , '' || ' ELSE ''''' || ' END) || COALESCE((SELECT 
				string_agg(CONCAT('COALESCE("CFW',numFieldID,'",'''')'),' || '','' || ' ORDER BY tintOrder)
			FROM
				tt_TEMPDISPLAYFIELDS 
			WHERE
				coalesce(bitCustom,false) = true),'');
		END IF;
	end if;

	IF  LENGTH(v_CustomerDisplayFields) = 0 then
		v_CustomerDisplayFields := 'vcItemName';
	end if;

	select 
		string_agg(CONCAT('getcustfldvalueitemwithtypelist(' , Fld_id::VARCHAR , ',''' , CFW_Fld_Master.Fld_type , ''',' ,COALESCE(CFW_Fld_Master.numlistid,0), ',Item.numItemCode) AS "CFW' , Fld_id::VARCHAR , '"'), ', ') INTO v_CustomFields 
	FROM
		CFW_Fld_Master 
	WHERE 
		numDomainID = v_numDomainID 
		AND Grp_id = 5;

	v_query := ('SELECT numItemCode as id
	,''item'' As module
	, (''/Items/frmKitDetails.aspx?ItemCode='' || numItemCode) AS url
	,(''<b style="color:#ff9900">Item ('' || (CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END) || '':)</b> '' || ' || v_CustomerDisplayFields || ') AS displaytext
	, (''Item ('' || (CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END) || ''): '' || ' ||  v_CustomerDisplayFields || ') AS text' || CASE v_CustomerSearchFields
   WHEN '' THEN ''
   ELSE ',' || coalesce(v_CustomerSearchFields,'')
   END ||
   CASE v_CustomerSearchCustomFields
   WHEN '' THEN ''
   ELSE ',' || coalesce(v_CustomerSearchCustomFields,'')
   END || ' FROM (SELECT 
				Item.numItemCode
				,charItemType
				,COALESCE(vcItemName,'''') AS vcItemName
				,COALESCE(txtItemDesc,'''') AS txtItemDesc
				,COALESCE(vcModelID,'''') AS vcModelID
				,COALESCE(vcSKU,'''') AS vcSKU
				,COALESCE(numBarCodeId,'''') AS numBarCodeId
				,COALESCE(CompanyInfo.vcCompanyName,'''') vcCompanyName
				,(CASE WHEN charItemType=''p'' THEN COALESCE((SELECT monWListPrice FROM WarehouseItems WHERE numItemID=Item.numItemCode AND COALESCE(monWListPrice,0) > 0 LIMIT 1),COALESCE(monListPrice,0)) ELSE COALESCE(monListPrice,0) END) AS monListPrice
				,COALESCE(Vendor.vcPartNo,'''') vcPartNo
				,COALESCE((SELECT string_agg(CustomerPartNo,'','') FROM CustomerPartNumber WHERE numItemCode=Item.numItemCode),'''') CustomerPartNo
				,(CASE WHEN numItemGroup > 0 AND COALESCE(bitMatrix,false) = true THEN COALESCE((SELECT
																									string_agg(CONCAT(CFW_FLD_Master.FLd_label,'':'',listdetails.vcData),'','')
																								FROM
																									ItemAttributes
																								INNER JOIN
																									CFW_FLD_Master 
																								ON
																									ItemAttributes.fld_id=CFW_FLD_Master.fld_id 
																								INNER JOIN
																									listdetails
																								ON
																									ItemAttributes.Fld_Value=listdetails.numlistitemid
																								WHERE
																									ItemAttributes.numDomainID=Item.numDomainID
																									AND ItemAttributes.numItemCode=Item.numItemCode),'''') ELSE '''' END) AS vcAttributes
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND COALESCE(bitMatrix,false) = false THEN COALESCE((SELECT string_agg(vcWHSKU,'', '') FROM WareHouseItems WHERE numItemID = Item.numItemCode),'''') ELSE '''' END) AS vcWHSKU
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND COALESCE(bitMatrix,false) = false THEN COALESCE((SELECT string_agg(vcBarCode,'', '') FROM WareHouseItems WHERE numItemID = Item.numItemCode),'''') ELSE '''' END) AS vcBarCode
				' ||(CASE WHEN LENGTH(v_CustomFields) > 0 THEN (',' || v_CustomFields) ELSE '' END) || '
			FROM 
				Item
			LEFT JOIN 
				Vendor
			ON
				Item.numVendorID=Vendor.numVendorID
				AND Item.numItemCode=Vendor.numItemCode
			LEFT JOIN 
				DivisionMaster
			ON
				Vendor.numVendorID = DivisionMaster.numDivisionID
			LEFT JOIN 
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			WHERE
				Item.numDomainID ='  || v_numDomainID || ' AND (Item.numItemCode IN (' || CASE WHEN coalesce(v_vcItemCodes,'') = '' THEN '0' ELSE v_vcItemCodes END || ') OR LENGTH(''' || coalesce(v_vcItemCodes,'') || ''') = 0)) TEMP1');

   RAISE NOTICE '%',v_query;

   OPEN SWV_RefCur FOR EXECUTE v_query;
   RETURN;
END; $$;


