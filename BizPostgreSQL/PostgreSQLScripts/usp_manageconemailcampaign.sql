-- Stored procedure definition script USP_ManageConEmailCampaign for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageConEmailCampaign(v_numConEmailCampID NUMERIC(9,0) DEFAULT 0,        
v_numContactID NUMERIC(9,0) DEFAULT 0,        
v_numECampaignID NUMERIC(9,0) DEFAULT 0,        
v_intStartDate TIMESTAMP DEFAULT NULL,        
v_bitEngaged BOOLEAN DEFAULT NULL,    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
    
--DisEngage Current contact from ECampaign
   AS $$
   DECLARE
   v_StartDateTime  TIMESTAMP; 	   
   v_dtECampStartTime1  TIMESTAMP;
   v_i  INTEGER DEFAULT 0;
   v_Count  INTEGER;
   v_TempDate  TIMESTAMP;
   v_numECampDTLId  NUMERIC(18,0);
   v_TempDays  INTEGER;
   v_TempWaitPeriod  INTEGER;
BEGIN
   IF v_numECampaignID = 0 then

      UPDATE ConECampaign SET bitEngaged = false WHERE numContactID = v_numContactID;
      RETURN;
   end if;
        
        
   if v_numConEmailCampID = 0 then
        
	
	-- stop all other campaign for given contact 
      update ConECampaign set bitEngaged = false where numContactID = v_numContactID AND numECampaignID <> v_numECampaignID;
	
	--Prevent Duplicate entry
      IF NOT EXISTS(SELECT * FROM ConECampaign WHERE numContactID = v_numContactID AND numECampaignID = v_numECampaignID AND bitEngaged = true) then
	
		
		/*Add Start Time to Given Start Date from ECampaign*/
         select   coalesce(dtStartTime,null::TIMESTAMP) INTO v_dtECampStartTime1 FROM ECampaign WHERE numECampaignID = v_numECampaignID;
		--	SELECT @StartDateTime;
         v_StartDateTime := v_intStartDate+CAST(EXTRACT(HOUR FROM v_dtECampStartTime1) || 'hour' as interval);
         v_StartDateTime := v_StartDateTime+CAST(EXTRACT(MINUTE FROM v_dtECampStartTime1) || 'minute' as interval);
			
	
		--Insert To Header
         insert into ConECampaign(numContactID,numECampaignID,intStartDate,bitEngaged,numRecOwner)
		values(v_numContactID,v_numECampaignID,v_StartDateTime,v_bitEngaged,v_numUserCntID) RETURNING numConEmailCampID INTO v_numConEmailCampID;
		
      
		--Insert To Detail
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            RowNo INTEGER,
            numECampDTLId NUMERIC(18,0),
            tintDays INTEGER,
            tintWaitPeriod SMALLINT
         );
         INSERT INTO
         tt_TEMP
         SELECT
         ROW_NUMBER() OVER(ORDER BY numECampDTLId),
			numECampDTLId,
			tintDays,
			tintWaitPeriod
         FROM
         ECampaignDTLs
         WHERE
         numECampID = v_numECampaignID  AND
			(numEmailTemplate > 0 OR numActionItemTemplate > 0)
         ORDER BY
         numECampDTLId;
         select   COUNT(*) INTO v_Count FROM tt_TEMP;
         v_TempDate := v_StartDateTime;
         WHILE v_i <= v_Count LOOP
            select   numECampDTLId, tintDays, tintWaitPeriod INTO v_numECampDTLId,v_TempDays,v_TempWaitPeriod FROM tt_TEMP WHERE RowNO = v_i;
            INSERT INTO ConECampaignDTL(numConECampID,
				numECampDTLID,
				dtExecutionDate)
            SELECT
            v_numConEmailCampID,
				v_numECampDTLId,
				v_TempDate
            FROM
            tt_TEMP
            WHERE
            RowNO = v_i;
            IF (v_TempWaitPeriod = 1) then --Day
               v_TempDate := v_TempDate+CAST(v_TempDays || 'day' as interval);
            ELSEIF (v_TempWaitPeriod = 2)
            then --Week
               v_TempDate := v_TempDate+CAST((v_TempDays::bigint*7) || 'day' as interval);
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
   else
      update ConECampaign set numContactID = v_numContactID,numECampaignID = v_numECampaignID,intStartDate = v_intStartDate,
      bitEngaged = v_bitEngaged
      where numConEmailCampID = v_numConEmailCampID;
   end if;        
	        
   open SWV_RefCur for select v_numConEmailCampID;
END; $$;












