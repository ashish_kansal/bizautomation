-- Stored procedure definition script USP_AdminSubStages for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AdminSubStages(v_byteMode SMALLINT DEFAULT null,
v_ProcessID NUMERIC(9,0) DEFAULT null,
v_strSubStages TEXT DEFAULT '',
v_strSubStageName VARCHAR(100) DEFAULT NULL,
INOUT v_SubStageID NUMERIC(9,0) DEFAULT null , INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;

   v_StageID  NUMERIC(9,0);
BEGIN
   if v_byteMode = 0 then

      open SWV_RefCur for
      select 	numSubstageDtlid,
		numProcessListID,
		numSubStageHdrID,
		vcSubStageElemt  from SubStageDetails
      where  	numSubStageHdrID = v_SubStageID;
   end if;

   if v_byteMode = 1 then
      if v_SubStageID = 0 then

         insert into SubStageHDR(numProcessId,
			vcSubStageName)
		values(v_ProcessID,v_strSubStageName);
		
         v_StageID := CURRVAL('SubStageHDR_seq');
         v_SubStageID := CURRVAL('SubStageHDR_seq');
      else
         update SubStageHDR
         set vcSubStageName = v_strSubStageName
         where numSubStageHDRId = v_SubStageID;
         v_StageID := v_SubStageID;
      end if;
      SELECT * INTO v_hDoc FROM SWF_Xml_PrepareDocument(v_strSubStages);
      delete from SubStageDetails where numProcessListID = v_ProcessID and numSubStageHdrID = v_StageID;
      insert into SubStageDetails(numSubStageHdrID,numProcessListID,vcSubStageElemt)
      select v_StageID,v_ProcessID,X.* from(SELECT *FROM SWF_OpenXml(v_hDoc,'/NewDataSet/Table1','vcSubStageElemt') SWA_OpenXml(vcSubStageElemt VARCHAR(100))) X;
      insert into SubStageDetails(numSubStageHdrID,numProcessListID,vcSubStageElemt)
      select v_StageID,v_ProcessID,X.* from(SELECT *FROM SWF_OpenXml(v_hDoc,'/NewDataSet/Table','vcSubStageElemt') SWA_OpenXml(vcSubStageElemt VARCHAR(100))) X;
      PERFORM SWF_Xml_RemoveDocument(v_hDoc);
   end if;
   RETURN;
END; $$;


