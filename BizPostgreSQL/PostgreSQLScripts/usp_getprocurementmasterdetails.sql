-- Stored procedure definition script USP_GetProcurementMasterDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProcurementMasterDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_intFiscalYear NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numProcurementBudgetId,bitPurchaseDeal From ProcurementBudgetMaster Where  numDomainID = v_numDomainId;
END; $$;












