-- Stored procedure definition script Temp_CreateCustomReportChecksToBePrinted for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION Temp_CreateCustomReportChecksToBePrinted(v_numDomainID NUMERIC,
v_numUserCntID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CustRptID  NUMERIC;
BEGIN
   INSERT INTO CustomReport(vcReportName,
	vcReportDescription,
	textQuery,
	numCreatedBy,
	numdomainId,
	numModifiedBy,
	numModuleId,
	numGroupId,
	bitFilterRelAnd,
	varstdFieldId,
	custFilter,
	FromDate,
	ToDate,
	bintRows,
	bitGridType,
	varGrpfld,
	varGrpOrd,
	varGrpflt,
	textQueryGrp,
	FilterType,
	AdvFilterStr,
	OrderbyFld,
	Orderf,
	MttId,
	MttValue,
	CompFilter1,
	CompFilter2,
	CompFilterOpr,
	bitDrillDown,
	bitSortRecCount) VALUES( 
	/* vcReportName - varchar(200) */ 'Checks To be Processed',
	/* vcReportDescription - varchar(500) */ 'Shows payments made through checks which are not printed yet',
	/* textQuery - text */ N'Select  CompanyInfo.vcCompanyName as ''Company Name'',dbo.fn_GetListItemName(CompanyInfo.numCompanyType) as ''Relationship'',OpportunityBizDocsPaymentDetails.monAmount* opportunitybizdocsdetails.fltExchangeRate as ''Amount'',dbo.fn_GetListItemName(opportunitybizdocsdetails.numPaymentMethod) as ''Payment Method'',opportunitybizdocsdetails.vcMemo as ''Memo'',dbo.FormatedDateTimeFromDate(OpportunityBizDocsPaymentDetails.dtDueDate,@numDomainId) as ''Due Date'',opportunitybizdocsdetails.vcreference as ''Reference'' from   opportunitybizdocsdetails  INNER JOIN OpportunityBizDocsPaymentDetails  ON opportunitybizdocsdetails.numBizDocsPaymentDetId = OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId INNER JOIN DivisionMaster ON opportunitybizdocsdetails.numDivisionId = DivisionMaster.numDivisionID INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId where opportunitybizdocsdetails.numDomainId = @numDomainId  AND opportunitybizdocsdetails.bitAuthoritativeBizDocs = 1  AND OpportunityBizDocsPaymentDetails.bitIntegrated = 0  AND opportunitybizdocsdetails.numBizDocsId = 0     and (  opportunitybizdocsdetails.numPaymentMethod= ''5'')',
	/* numCreatedBy - numeric(18, 0) */ v_numUserCntID,
	/* numDomainID - numeric(18, 0) */ v_numDomainID,
	/* numModifiedBy - numeric(18, 0) */ v_numUserCntID,
	/* numModuleId - numeric(18, 0) */ 11,
	/* numGroupId - numeric(18, 0) */ 35,
	/* bitFilterRelAnd - bit */ true,
	/* varstdFieldId - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* custFilter - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* FromDate - datetime */ CAST('1900-01-01 00:00:00.000' AS TIMESTAMP),
	/* ToDate - datetime */ CAST('2010-06-25 00:00:00.000' AS TIMESTAMP),
	/* bintRows - numeric(18, 0) */ 0,
	/* bitGridType - bit */ true,
	/* varGrpfld - varchar(200) */ '',
	/* varGrpOrd - varchar(200) */ '',
	/* varGrpflt - varchar(200) */ '',
	/* textQueryGrp - text */ '',
	/* FilterType - tinyint */ 0,
	/* AdvFilterStr - varchar(max) */ '',
	/* OrderbyFld - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* Orderf - varchar(200) */ 'Asc',
	/* MttId - int */ 0,
	/* MttValue - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* CompFilter1 - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* CompFilter2 - varchar(200) */ CAST(0 AS VARCHAR(200)),
	/* CompFilterOpr - varchar(200) */ '=',
	/* bitDrillDown - bit */ false,
	/* bitSortRecCount - bit */ false); 


   v_CustRptID := CURRVAL('customreport_seq');
   INSERT INTO CustReportFields(numCustomReportId,
	vcFieldName,
	vcValue)
   SELECT v_CustRptID,'CompanyInfo.vcCompanyName~7~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'CompanyInfo.numCompanyType~7~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'OpportunityBizDocsPaymentDetails.monAmount* opportunitybizdocsdetails.fltExchangeRate~43~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'opportunitybizdocsdetails.numPaymentMethod~43~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'opportunitybizdocsdetails.vcMemo~43~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'OpportunityBizDocsPaymentDetails.dtDueDate~44~0',CAST('1' AS BOOLEAN)
   UNION
   SELECT v_CustRptID,'opportunitybizdocsdetails.vcreference~43~0',CAST('1' AS BOOLEAN);

   INSERT INTO CustReportFilterlist(numCustomReportId,
	vcFieldsText,
	vcFieldsValue,
	vcFieldsOperator,
	vcFilterValue,
	vcFilterValued) VALUES( 
	/* numCustomReportId - numeric(18, 0) */ v_CustRptID,
	/* vcFieldsText - varchar(200) */ 'Payment Method',
	/* vcFieldsValue - varchar(200) */ 'opportunitybizdocsdetails.numPaymentMethod~43~Drop Down List~31~0',
	/* vcFieldsOperator - varchar(200) */ '=',
	/* vcFilterValue - varchar(200) */ 'Checks',
	/* vcFilterValued - varchar(200) */ '5'); 
	

   INSERT INTO CustReportOrderlist(numCustomReportId,
	vcFieldText,
	vcValue,
	numOrder)
   SELECT v_CustRptID,'Company Name'  ,'CompanyInfo.vcCompanyName~7~False~~0~0',1
   UNION
   SELECT v_CustRptID,'Relationship'  ,'CompanyInfo.numCompanyType~7~False~Drop Down List~0~5',2
   UNION
   SELECT v_CustRptID,'Amount'  ,'OpportunityBizDocsPaymentDetails.monAmount* opportunitybizdocsdetails.fltExchangeRate~43~False~~0~0',3
   UNION
   SELECT v_CustRptID,'Payment Method'  ,'opportunitybizdocsdetails.numPaymentMethod~43~False~Drop Down List~0~31',4
   UNION
   SELECT v_CustRptID,'Due Date'  ,'OpportunityBizDocsPaymentDetails.dtDueDate~44~False~Date Field~0~0',5
   UNION
   SELECT v_CustRptID,'Memo'  ,'opportunitybizdocsdetails.vcMemo~43~False~~0~0',6
   UNION
   SELECT v_CustRptID,'Reference'  ,'opportunitybizdocsdetails.vcreference~43~False~~0~0',7;
RETURN;
END; $$; 


