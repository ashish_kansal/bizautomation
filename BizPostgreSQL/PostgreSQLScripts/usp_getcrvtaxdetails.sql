-- Stored procedure definition script USP_GetCRVTaxDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCRVTaxDetails(v_numDomainID NUMERIC(18,0),
v_numItemCode NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   1 AS numTaxItemID,
		TaxDetails.numTaxID,
		CONCAT('CRV',CASE WHEN coalesce(vcTaxUniqueName,'') = '' THEN '' ELSE ' (' || vcTaxUniqueName || ')' END) AS vcTaxName,
		coalesce(bitApplicable,false) AS bitApplicable
   FROM
   TaxDetails
   LEFT JOIN
   ItemTax
   ON
   ItemTax.numTaxItemID = 1
   AND ItemTax.numTaxID = TaxDetails.numTaxID
   AND numItemCode = v_numItemCode
   WHERE
   numDomainID = v_numDomainID
   AND TaxDetails.numTaxItemID = 1;
END; $$;













