DROP FUNCTION IF EXISTS USP_OpportunityItems_UnReceiveQty;

CREATE OR REPLACE FUNCTION USP_OpportunityItems_UnReceiveQty(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_tintMode SMALLINT,
	v_numUnitHourToUnreceive DOUBLE PRECISION)
RETURNS VOID LANGUAGE plpgsql
	-- CODE LEVE TRANSACTION SCOPE IS USED

   AS $$
   DECLARE
   v_description  VARCHAR(500);
   v_fltExchangeRate  DOUBLE PRECISION;
   v_numItemCode  NUMERIC(18,0);
   v_numUnits  DOUBLE PRECISION;
   v_onHand  DOUBLE PRECISION;                                            
   v_onOrder  DOUBLE PRECISION;                                            
   v_onBackOrder  DOUBLE PRECISION;                                              
   v_onAllocation  DOUBLE PRECISION;
   v_numUnitHourReceived  DOUBLE PRECISION;
   v_numDeletedReceievedQty  DOUBLE PRECISION;
   v_bitDropship  BOOLEAN;
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numWLocationID  NUMERIC(18,0);
   v_TotalOnHand  NUMERIC(18,0);
   v_monAvgCost  DECIMAL(20,5);
   v_monPrice  DECIMAL(20,5);
   v_j  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numTempOIRLID  NUMERIC(18,0);
   v_numTempOnHand  DOUBLE PRECISION;
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempUnitReceieved  DOUBLE PRECISION;
   v_numTempDeletedReceievedQty  DOUBLE PRECISION;				
   v_vcFromLocation  VARCHAR(300);
   SWV_RCur REFCURSOR;
BEGIN
   select(CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END) INTO v_fltExchangeRate FROM OpportunityMaster WHERE numOppId = v_numOppId;

   DROP TABLE IF EXISTS tt_TEMPRECEIEVEDITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRECEIEVEDITEMS
   (
      ID INTEGER,
      numOIRLID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numUnitReceieved DOUBLE PRECISION,
      numDeletedReceievedQty DOUBLE PRECISION
   );
		
   IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemID AND coalesce(numUnitHourReceived,0) = v_numUnitHourToUnreceive) then
	
      IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId WHERE numoppitemtCode = v_numOppItemID AND coalesce(numUnitHourReceived,0) > 0 AND coalesce(OM.tintshipped,0) = 0) then
		
         select   OI.numItemCode, numUnitHour, numUnitHourReceived, coalesce(numDeletedReceievedQty,0), coalesce(numWarehouseItmsID,0), coalesce(numWLocationID,0), monTotAmount*v_fltExchangeRate, I.monAverageCost, coalesce(OI.bitDropShip,false) INTO v_numItemCode,v_numUnits,v_numUnitHourReceived,v_numDeletedReceievedQty,
         v_numWareHouseItemID,v_numWLocationID,v_monPrice,v_monAvgCost,v_bitDropship FROM
         OpportunityItems OI
         INNER JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode WHERE
         numoppitemtCode = v_numOppItemID;
         IF v_numWareHouseItemID > 0 AND v_bitDropship = false then
            v_TotalOnHand := 0;
            select(SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0))) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode;
            select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder FROM
            WareHouseItems WHERE
            numWareHouseItemID = v_numWareHouseItemID;
            IF(SELECT
            COUNT(*)
            FROM
            OppWarehouseSerializedItem OWSI
            INNER JOIN
            OpportunityMaster OM
            ON
            OWSI.numOppID = OM.numOppId
            AND tintopptype = 1
            WHERE
            OWSI.numWarehouseItmsDTLID IN(SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = v_numOppId AND numOppItemID = v_numOppItemID)) > 0 then
				
               RAISE EXCEPTION 'SERIAL/LOT#_USED';
            ELSE
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
               UPDATE WareHouseItmsDTL WHIDL
               SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0) -coalesce(OWSI.numQty,0) ELSE 0 END)
               FROM
               OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
               WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
               AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND(OWSI.numOppID = v_numOppId
               AND numOppItemID = v_numOppItemID);
            end if;
            INSERT INTO
            tt_TEMPRECEIEVEDITEMS
            SELECT
            ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					coalesce(numDeletedReceievedQty,0)
            FROM
            OpportunityItemsReceievedLocation
            WHERE
            numDomainId = v_numDomainID
            AND numOppID = v_numOppId
            AND numOppItemID = v_numOppItemID;
            select   COUNT(*) INTO v_COUNT FROM tt_TEMPRECEIEVEDITEMS;
            WHILE v_j <= v_COUNT LOOP
               select   TRI.numOIRLID, coalesce(numOnHand,0), coalesce(TRI.numWarehouseItemID,0), coalesce(numUnitReceieved,0), coalesce(numDeletedReceievedQty,0) INTO v_numTempOIRLID,v_numTempOnHand,v_numTempWarehouseItemID,v_numTempUnitReceieved,
               v_numTempDeletedReceievedQty FROM
               tt_TEMPRECEIEVEDITEMS TRI
               INNER JOIN
               WareHouseItems WI
               ON
               TRI.numWarehouseItemID = WI.numWareHouseItemID WHERE
               ID = v_j;
               select   coalesce(WL.vcLocation,'') INTO v_vcFromLocation FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numWareHouseItemID = v_numTempWarehouseItemID;
               IF v_numTempOnHand >=(v_numTempUnitReceieved -v_numTempDeletedReceievedQty) then
					
                  UPDATE
                  WareHouseItems
                  SET
                  numOnHand = coalesce(numOnHand,0) -(coalesce(v_numTempUnitReceieved,0) -coalesce(v_numTempDeletedReceievedQty,0)),numonOrder = coalesce(numonOrder,0),
                  dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numTempWarehouseItemID;
                  UPDATE
                  WareHouseItems
                  SET
                  numonOrder = coalesce(numonOrder,0)+(coalesce(v_numTempUnitReceieved,0) -coalesce(v_numTempDeletedReceievedQty,0)),dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWareHouseItemID;
                  UPDATE
                  OpportunityItems
                  SET
                  numDeletedReceievedQty = coalesce(numDeletedReceievedQty,0)+(coalesce(v_numTempUnitReceieved,0) -coalesce(v_numTempDeletedReceievedQty,0))
                  WHERE
                  numoppitemtCode = v_numOppItemID;
               ELSE
                  RAISE EXCEPTION 'INSUFFICIENT_ONHAND_QTY';
                  RETURN;
               end if;
               v_description := CONCAT('PO Qty Unreceived (Qty: ',CAST((v_numTempUnitReceieved -v_numTempDeletedReceievedQty) AS VARCHAR(10)),')');
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppId,
               v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,
               v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
               v_numUnitHourReceived := coalesce(v_numUnitHourReceived,0) -(v_numTempUnitReceieved -v_numTempDeletedReceievedQty);
               v_description := CONCAT('PO Qty Unreceived From Internal Location ',coalesce(v_vcFromLocation,''),
               ' (Qty:',CAST((v_numTempUnitReceieved -v_numTempDeletedReceievedQty) AS VARCHAR(10)),')');
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppId,
               v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
               v_numDomainID := v_numDomainID,SWV_RefCur := null);
               DELETE FROM OpportunityItemsReceievedLocation WHERE ID = v_numTempOIRLID;
               v_j := v_j::bigint+1;
            END LOOP;

				--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
            select   coalesce(numDeletedReceievedQty,0) INTO v_numDeletedReceievedQty FROM OpportunityItems WHERE	numoppitemtCode = v_numOppItemID;
            v_description := CONCAT('PO Qty Unreceived (Qty:',v_numUnitHourReceived,')');
            IF v_onHand >= v_numUnitHourReceived then
				
               IF v_TotalOnHand -v_numUnitHourReceived <= 0 then
					
                  v_monAvgCost := 0;
               ELSE
                  v_monAvgCost :=((v_TotalOnHand*v_monAvgCost) -(v_numUnitHourReceived*(v_monPrice/v_numUnits)))/(v_TotalOnHand -v_numUnitHourReceived);
               end if;
               RAISE NOTICE '%',v_monAvgCost;
               RAISE NOTICE '%',v_numItemCode;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END)
               WHERE
               numItemCode = v_numItemCode;
               UPDATE
               WareHouseItems
               SET
               numOnHand = coalesce(numOnHand,0) -coalesce(v_numUnitHourReceived,0),numonOrder = coalesce(numonOrder,0)+coalesce(v_numUnitHourReceived,0),dtModified = LOCALTIMESTAMP
               WHERE
               numWareHouseItemID = v_numWareHouseItemID;
               UPDATE OpportunityItems SET numUnitHourReceived = 0,numDeletedReceievedQty = 0,numQtyReceived=(CASE WHEN v_tintMode =1 THEN 0 ELSE numQtyReceived END) WHERE numoppitemtCode = v_numOppItemID;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppId,
               v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
               v_numDomainID := v_numDomainID,SWV_RefCur := null);
            ELSE
               RAISE EXCEPTION 'INSUFFICIENT_ONHAND_QTY';
               RETURN;
            end if;
         ELSE
            UPDATE OpportunityItems SET numUnitHourReceived = 0,numQtyReceived=(CASE WHEN v_tintMode =1 THEN 0 ELSE numQtyReceived END) WHERE numoppitemtCode = v_numOppItemID;
         end if;
         IF EXISTS(SELECT
         OI.numoppitemtCode
         FROM
         OpportunityMaster OM
         INNER JOIN
         OpportunityItems OI
         ON
         OM.numOppId = OI.numOppId
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         WHERE
         OM.numOppId = v_numOppId
         AND OM.tintopptype = 2
         AND OM.tintoppstatus = 1
         AND OI.numoppitemtCode = v_numOppItemID
         AND coalesce(I.bitAsset,false) = true) then
			
            PERFORM USP_ItemDepreciation_Save(v_numDomainID,v_numOppId,v_numOppItemID);
         end if;
      end if;
   ELSE
      RAISE EXCEPTION 'RECEIVED_QTY_IS_CHANGED_BY_OTHER_USER';
   end if;
END; $$;


