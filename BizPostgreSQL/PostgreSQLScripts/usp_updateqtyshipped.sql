-- Stored procedure definition script USP_UpdateQtyShipped for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateQtyShipped(v_numQtyShipped DOUBLE PRECISION,
    v_numOppItemID NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
    INOUT v_vcError VARCHAR(500) DEFAULT '')
LANGUAGE plpgsql
-- TRANSACTION FROM CODE IS USED FOR PURCHASE FULFILLMENT
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);          
   v_onAllocation  DOUBLE PRECISION;          
   v_numWarehouseItemID  NUMERIC;       
   v_numOldQtyShipped  DOUBLE PRECISION;       
   v_numNewQtyShipped  DOUBLE PRECISION;       
   v_numOppId  NUMERIC;
   v_numUnits  DOUBLE PRECISION;
   v_Kit  BOOLEAN;                   
   v_vcOppName  VARCHAR(200);

         
   v_description  VARCHAR(100);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_minRowNumber  NUMERIC(9,0);
   v_maxRowNumber  NUMERIC(9,0);
   v_KitWareHouseItemID  NUMERIC(9,0);
   v_KitonAllocation  DOUBLE PRECISION;
   v_KitQtyShipped  DOUBLE PRECISION;
   v_KiyQtyItemsReq_Orig  DOUBLE PRECISION;
   v_KiyQtyItemsReq  DOUBLE PRECISION;  
   v_KitNewQtyShipped  DOUBLE PRECISION;
   v_KitOppChildItemID  NUMERIC(9,0);
   v_Kitdescription  VARCHAR(100);				
			   
   SWV_RCur REFCURSOR;
BEGIN
   BEGIN
      select   coalesce(numWarehouseItmsID,0), coalesce(numQtyShipped,0), numOppId, numUnitHour, (CASE WHEN bitKitParent = true AND bitAssembly = true THEN false WHEN bitKitParent = true THEN true ELSE false END), I.numDomainID INTO v_numWarehouseItemID,v_numOldQtyShipped,v_numOppId,v_numUnits,v_Kit,v_numDomain FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode WHERE
      numoppitemtCode = v_numOppItemID;
      v_numNewQtyShipped := v_numQtyShipped -v_numOldQtyShipped;
      v_description := CONCAT('SO Qty Shipped (Qty:',v_numUnits,' Shipped:',v_numNewQtyShipped,')');
      IF v_Kit = true then
	
         DROP TABLE IF EXISTS tt_TEMPKITS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPKITS AS
            SELECT
            OKI.numOppChildItemID
			,OKI.numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyShipped
			,coalesce(numAllocation,0) AS numAllocation
			,ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber
		
            FROM
            OpportunityKitItems OKI
            INNER JOIN
            Item I
            ON
            OKI.numChildItemID = I.numItemCode
            INNER JOIN
            WareHouseItems WI
            ON
            WI.numWareHouseItemID = OKI.numWareHouseItemId
            WHERE
            charitemtype = 'P'
            AND OKI.numWareHouseItemId > 0
            AND OKI.numWareHouseItemId is not null
            AND numOppItemID = v_numOppItemID;
         IF(SELECT COUNT(*) FROM tt_TEMPKITS WHERE numAllocation -((v_numQtyShipped*numQtyItemsReq_Orig) -numQtyShipped) < 0) > 0 then
		
            select   coalesce(vcpOppName,'') INTO v_vcOppName FROM
            OpportunityMaster WHERE
            numOppId IN(SELECT numOppId FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemID)    LIMIT 1;
            v_vcError := 'You do not have enough KIT inventory to support this shipment(' || coalesce(v_vcOppName,'') || '). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.';
         ELSE
            select   MIN(RowNumber), MAX(RowNumber) INTO v_minRowNumber,v_maxRowNumber FROM tt_TEMPKITS;
            WHILE  v_minRowNumber <= v_maxRowNumber LOOP
               select   numOppChildItemID, numWareHouseItemID, numQtyItemsReq, numQtyShipped, numQtyItemsReq_Orig, numAllocation INTO v_KitOppChildItemID,v_KitWareHouseItemID,v_KiyQtyItemsReq,v_KitQtyShipped,
               v_KiyQtyItemsReq_Orig,v_KitonAllocation FROM tt_TEMPKITS WHERE RowNumber = v_minRowNumber;
               v_KitNewQtyShipped :=(v_numNewQtyShipped*v_KiyQtyItemsReq_Orig) -v_KitQtyShipped;
               v_Kitdescription := CONCAT('Transfer Order KIT Qty Shipped (Qty:',v_KiyQtyItemsReq,' Shipped:',v_KitNewQtyShipped,
               ')');
               v_KitonAllocation := v_KitonAllocation -v_KitNewQtyShipped;
               IF (v_KitonAllocation >= 0) then
				
                  UPDATE  WareHouseItems
                  SET     numAllocation = v_KitonAllocation,dtModified = LOCALTIMESTAMP
                  WHERE   numWareHouseItemID = v_KitWareHouseItemID;
					 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  tinyint
							 --  varchar(100)
                  UPDATE  OpportunityKitItems
                  SET     numQtyShipped = v_numQtyShipped*v_KiyQtyItemsReq_Orig
                  WHERE   numOppChildItemID = v_KitOppChildItemID;
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_KitWareHouseItemID,v_numReferenceID := v_numOppId,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_Kitdescription,
                  v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
               end if;
               v_minRowNumber := v_minRowNumber+1;
            END LOOP;
			 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
            UPDATE
            OpportunityItems
            SET
            numQtyShipped = v_numQtyShipped
            WHERE
            numoppitemtCode = v_numOppItemID;
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
            v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
            v_numDomainID := v_numDomain,SWV_RefCur := null);
         end if;
      ELSE
         select   coalesce(numAllocation,0) INTO v_onAllocation FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWarehouseItemID;
         v_onAllocation := v_onAllocation -v_numNewQtyShipped;
         IF (v_onAllocation >= 0) then
        
            UPDATE
            WareHouseItems
            SET
            numAllocation = v_onAllocation,dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWarehouseItemID;
             --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
            UPDATE
            OpportunityItems
            SET
            numQtyShipped = v_numQtyShipped
            WHERE
            numoppitemtCode = v_numOppItemID;
            PERFORM FROM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
            v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
            v_numDomainID := v_numDomain,SWV_RefCur := null);
         ELSE
            select   coalesce(vcpOppName,'') INTO v_vcOppName FROM OpportunityMaster WHERE numOppId IN(SELECT numOppId FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemID)    LIMIT 1;
            v_vcError := 'You do not have enough inventory to support this shipment(' || coalesce(v_vcOppName,'') || '). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.';
         end if;
      end if;
      EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


