-- Stored procedure definition script USP_GetCurrentOpeningBalanceForGeneralLedgerDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCurrentOpeningBalanceForGeneralLedgerDetails(v_numChartAcntId NUMERIC(9,0),          
 v_numDomainId NUMERIC(9,0),          
 v_dtFromDate TIMESTAMP,          
 v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTransactionId  INTEGER;                                                    
   v_numDebitAmt  DECIMAL(20,5);                                                    
   v_numCreditAmt  DECIMAL(20,5);                                                    
   v_numAcntTypeId  INTEGER;                                                     
   v_monCurrentOpeningBalance  DECIMAL(20,5);                                                    
   v_numOriginalOpeningBal  DECIMAL(20,5);                                          
   v_numOpeningBal  DECIMAL(20,5);                                             
   v_count  INTEGER;                                    
   v_bal  DECIMAL(20,5);                              
   v_numAccountId  VARCHAR(1000);                              
   v_numAccountId1  VARCHAR(1000);                              
   v_numOrgBal  DECIMAL(20,5);          
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
   v_TotRecs  INTEGER;                            
   v_i  INTEGER;
BEGIN
   v_monCurrentOpeningBalance := 0;                                                  
   v_numOriginalOpeningBal := 0;                                                  
   v_count := 0;                                               
   v_numOpeningBal := 0;                                       
   v_numOrgBal := 0;                            
      
   Select numAccountId INTO v_numAccountIdOpeningEquity From Chart_Of_Accounts Where bitOpeningBalanceEquity = true  and numDomainId = v_numDomainId;       
            
                                                          
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY, 
      numChartAcntId NUMERIC(9,0)
   );                                                                                                                                  
                                             
   v_numAccountId1 := fn_ChildCategory(v_numChartAcntId::VARCHAR(100),v_numDomainId);                            
                            
                             
   insert into tt_TEMPTABLE(numChartAcntId)(SELECT * FROM SplitIDs(v_numAccountId1,','));                            
	--exec ('insert into #tempTable' +  )                              
                           
   v_i := 0;                            
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                            
   RAISE NOTICE '%',v_numAccountId;                              
   while v_i <= v_TotRecs LOOP
      select   coalesce(numChartAcntId,0) INTO v_numAccountId From tt_TEMPTABLE Where ID = v_i;
      RAISE NOTICE '%',v_numAccountId;
      If cast(NULLIF(v_numAccountId,'') as NUMERIC(9,0)) <> v_numAccountIdOpeningEquity then
         select   Count(*) INTO v_count From General_Journal_Header  GJH
         Inner Join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where (GJD.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)) Or GJH.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)))
         And GJH.numDomainId = v_numDomainId And GJH.datEntry_Date <= CAST(v_dtFromDate AS timestamp);
      end if;
      If v_count = 0 then
					
         select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Header  GJH
         Inner Join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJD.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)) And GJH.numDomainId = v_numDomainId
         And GJH.datEntry_Date >= CAST(v_dtFromDate AS timestamp) And GJH.datEntry_Date <= CAST(v_dtToDate AS timestamp);
         select   coalesce(numOriginalOpeningBal,0), coalesce(numOpeningBal,0) INTO v_numOriginalOpeningBal,v_numOpeningBal From Chart_Of_Accounts Where numAccountId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)) And numDomainId = v_numDomainId -- And dtOpeningDate <='' + convert(varchar(300),@dtFromDate) +''                                            
         And dtOpeningDate >= CAST(v_dtFromDate AS timestamp)  And dtOpeningDate <= CAST(v_dtToDate AS timestamp);
      Else
         select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Header  GJH
         Inner Join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJD.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0))  And GJH.numDomainId = v_numDomainId                                                  
						-- And GJH.datEntry_Date >='' + convert(varchar(300),@dtFromDate) +''                                              
         And GJH.datEntry_Date <= CAST(v_dtToDate AS timestamp);
         select   coalesce(numOriginalOpeningBal,0), coalesce(numOpeningBal,0) INTO v_numOriginalOpeningBal,v_numOpeningBal From Chart_Of_Accounts Where numAccountId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)) And numDomainId = v_numDomainId --And dtOpeningDate>='' + convert(varchar(300),@dtFromDate) +''                                            
         And dtOpeningDate <= CAST(v_dtToDate AS timestamp);
         RAISE NOTICE '@numOriginalOpeningBal';
      end if;
      v_numOrgBal := v_numOrgBal+v_numOriginalOpeningBal;
      RAISE NOTICE '@numOrgBal%',SUBSTR(CAST(v_numOrgBal AS VARCHAR(100)),1,100);
      While v_numTransactionId <> 0 LOOP
         select   numAcntTypeId INTO v_numAcntTypeId From Chart_Of_Accounts Where numAccountId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0))  And numDomainId = v_numDomainId;
         If v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then
				
            select   coalesce(numDebitAmt,0), coalesce(numCreditAmt,0) INTO v_numDebitAmt,v_numCreditAmt From General_Journal_Details Where numTransactionId = v_numTransactionId And numDomainId = v_numDomainId;
            If v_numDebitAmt <> 0 then
               v_monCurrentOpeningBalance := v_monCurrentOpeningBalance -v_numDebitAmt;
            Else
               v_monCurrentOpeningBalance := v_monCurrentOpeningBalance+v_numCreditAmt;
            end if;
         Else
            select   numDebitAmt, numCreditAmt INTO v_numDebitAmt,v_numCreditAmt From General_Journal_Details Where numTransactionId = v_numTransactionId;
            If v_numDebitAmt <> 0 then
               v_monCurrentOpeningBalance := v_monCurrentOpeningBalance+v_numDebitAmt;
            Else
               v_monCurrentOpeningBalance := v_monCurrentOpeningBalance -v_numCreditAmt;
            end if;
         end if;
         If v_count = 0 then
					
            select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Header  GJH
            inner join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJD.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0))  And GJD.numTransactionId > v_numTransactionId
            And GJH.numDomainId = v_numDomainId And GJH.datEntry_Date >= CAST(v_dtFromDate AS timestamp) And GJH.datEntry_Date <= CAST(v_dtToDate AS timestamp);
         Else
            select   min(numTransactionId) INTO v_numTransactionId From General_Journal_Header  GJH
            inner join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId Where GJD.numChartAcntId = cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0)) And GJD.numTransactionId > v_numTransactionId
            And GJH.numDomainId = v_numDomainId And  GJH.datEntry_Date <= CAST(v_dtToDate AS timestamp);
         end if;
      END LOOP;

      select   MIN(numAccountId) INTO v_numAccountId From Chart_Of_Accounts Where numParntAcntTypeID = v_numChartAcntId  And numAccountId > cast(NULLIF(v_numAccountId,'') as NUMERIC(18,0))  And numDomainId = v_numDomainId;
      v_i := v_i::bigint+1;
   END LOOP;                                  
                             
   If v_numChartAcntId <> v_numAccountIdOpeningEquity then -- For Opening Balance Equity                               
		
      v_monCurrentOpeningBalance := v_monCurrentOpeningBalance;
   end if;                            
   RAISE NOTICE '%',v_monCurrentOpeningBalance;                            
   v_monCurrentOpeningBalance := v_bal+v_monCurrentOpeningBalance;                                           
                                 
   open SWV_RefCur for Select CAST(v_monCurrentOpeningBalance AS VARCHAR(20));
END; $$;












