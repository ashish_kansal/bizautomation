-- Stored procedure definition script USP_Import_GetItemDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Import_GetItemDetails(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Item.numItemCode
		,Item.numShipClass
		,Item.vcItemName
		,Item.monListPrice
		,Item.txtItemDesc
		,Item.vcModelID
		,Item.vcManufacturer
		,Item.numBarCodeId
		,Item.fltWidth
		,Item.fltHeight
		,Item.fltWeight
		,Item.fltLength
		,Item.monAverageCost
		,Item.bitSerialized
		,Item.bitKitParent
		,Item.bitAssembly
		,Item.IsArchieve
		,Item.bitLotNo
		,Item.bitAllowBackOrder
		,Item.numIncomeChartAcntId
		,Item.numAssetChartAcntId
		,Item.numCOGsChartAcntId
		,Item.numItemClassification
		,Item.numItemGroup
		,Item.numPurchaseUnit
		,Item.numSaleUnit
		,Item.numItemClass
		,Item.numBaseUnit
		,Item.vcSKU
		,Item.charItemType
		,Item.bitTaxable
		,ItemExtendedDetails.txtDesc
		,Item.vcExportToAPI
		,Item.bitAllowDropShip
		,Item.bitMatrix
		,coalesce((SELECT MAX(numReorder) FROM WareHouseItems WHERE numItemID = Item.numItemCode),0) AS numReOrder
   FROM
   Item
   LEFT JOIN
   ItemExtendedDetails
   ON
   Item.numItemCode = ItemExtendedDetails.numItemCode
   WHERE
   numDomainID = v_numDomainID
   AND Item.numItemCode = v_numItemCode;
   RETURN;
END; $$;













