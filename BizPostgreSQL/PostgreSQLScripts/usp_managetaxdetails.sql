-- Stored procedure definition script usp_ManageTaxDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageTaxDetails(v_numCountry NUMERIC(9,0),  
v_numState NUMERIC(9,0),  
v_decTax DECIMAL(10,4),    
v_numTaxId NUMERIC(10,0),  
v_numDomainId  NUMERIC(9,0),  
v_tintMode SMALLINT,
v_numTaxItemID NUMERIC(9,0),
v_vcCity VARCHAR(100),
v_vcZipPostal VARCHAR(20),
v_tintTaxType SMALLINT,
v_vcTaxUniqueName VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintMode = 0 then --Insert record
	
      IF v_numState = 0 then
		
         v_vcCity := '';
         v_vcZipPostal := '';
      end if;
      IF(SELECT COUNT(*) FROM TaxDetails WHERE numCountryID = v_numCountry AND numStateID = v_numState AND numDomainId = v_numDomainId AND numTaxItemID = v_numTaxItemID and vcCity = v_vcCity and vcZipPostal = v_vcZipPostal AND coalesce(vcTaxUniqueName,'') = coalesce(v_vcTaxUniqueName,'')) = 0 then
		
         INSERT INTO TaxDetails(numTaxItemID, numCountryID, numStateID, decTaxPercentage, numDomainId, vcCity, vcZipPostal, tintTaxType, vcTaxUniqueName)
			VALUES(coalesce(v_numTaxItemID, 0), v_numCountry, v_numState, v_decTax, v_numDomainId, v_vcCity, v_vcZipPostal, v_tintTaxType, v_vcTaxUniqueName);
			
         open SWV_RefCur for
         SELECT  CURRVAL('TaxDetails_seq');
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   ELSEIF v_tintMode = 1
   then --Delete particular record
	
      DELETE FROM TaxDetails WHERE numTaxID = v_numTaxId AND numDomainId = v_numDomainId;
      open SWV_RefCur for
      SELECT v_numTaxId;
   ELSEIF v_tintMode = 2
   then --Delete All records
	
      DELETE FROM TaxDetails WHERE numDomainId = v_numDomainId;
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


