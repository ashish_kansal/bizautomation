-- Stored procedure definition script USP_GetChartAcntGeneralLedgerDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntGeneralLedgerDetails()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);                                                                                  
   v_strChildCategory  VARCHAR(5000);                                          
   v_numAcntTypeId  INTEGER;        
   v_numChartAcntId  INTEGER;                                                                         
   v_OpeningBal  VARCHAR(8000);
BEGIN
   v_OpeningBal := 'Opening Balance';                                                              
   v_strSQL := '';         
    
    
--Create a Temporary table to hold data                                                                                                  
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCustomerId NUMERIC(9,0),
      JournalId NUMERIC(9,0),
      TransactionType VARCHAR(500),
      EntryDate TIMESTAMP,
      CompanyName VARCHAR(8000),
      CheckId NUMERIC(9,0),
      CashCreditCardId NUMERIC(9,0),
      Memo VARCHAR(8000),
      Payment DECIMAL(20,5),
      Deposit DECIMAL(20,5),
      numTransactionId NUMERIC(9,0),
      numBalance DECIMAL(20,5),
      numAccountId NUMERIC(9,0)
   );                                                                                                  
                                                                           
                                                                    
   v_strSQL := coalesce(v_strSQL,'') || ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,           
   case when coalesce(GJH.numCheckId,0) <> 0 then ''Checks'' Else case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then ''Cash''      
   ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then ''Charge''       
   ELse Case when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = false then ''Credit''       
   Else Case When GJH.numJournal_Id <> 0 then ''Journal'' End  End End  End End  as TransactionType,                                
   GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,                                                  
   GJH.numCashCreditCardId as CashCreditCardId,                                     
   GJD.varDescription as Memo,                                                                                            
   (case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit ,                                        
   (case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,                                         
   GJD.numTransactionId as numTransactionId,                                                              
   null AS numBalance,                                              
   coalesce(GJD.numChartAcntId,0) as numAcntType                                                                          
   From General_Journal_Header as GJH                                                                                          
   Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id = GJD.numJournalId                                                                                   
   Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID                                                             
   Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId                                                                       
   Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId = CA.numAccountId        
   Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId        
   Left outer join CheckDetails CD on GJH.numCheckId = CD.numCheckId ';       
                                                                                         
                                
   RAISE NOTICE '%',(v_strChildCategory);                                                                                  
   RAISE NOTICE '%',(v_strSQL);                                                                                  
   EXECUTE v_strSQL;
   RETURN;
END; $$;


