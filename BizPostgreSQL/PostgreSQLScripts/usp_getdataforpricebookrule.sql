-- FUNCTION: public.usp_getdataforpricebookrule(numeric, numeric, smallint, refcursor, refcursor)

-- DROP FUNCTION public.usp_getdataforpricebookrule(numeric, numeric, smallint, refcursor, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getdataforpricebookrule(
	v_numruleid numeric DEFAULT 0,
	v_numdomainid numeric DEFAULT 0,
	v_tintruleapptype smallint DEFAULT NULL::smallint,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor,
	INOUT swv_refcur2 refcursor DEFAULT NULL::refcursor)
    RETURNS record
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   if v_tintRuleAppType = 1 then
      open SWV_RefCur for
      select numPriceBookItemID,numRuleID, vcItemName,txtItemDesc,vcModelID, GetListIemName(numItemClassification) AS ItemClassification from Item
      join PriceBookRuleItems
      on numValue = numItemCode
      where numDomainID = v_numDomainID and numRuleID = v_numRuleID and tintType = 1;
   end if;    
   if v_tintRuleAppType = 2 then
      open SWV_RefCur for
      SELECT numListItemID AS numItemClassification,coalesce(vcData,'-') AS ItemClassification  FROM Listdetails WHERE numListID = 36 AND numDomainid = v_numDomainID AND numListItemID NOT IN(select numValue from PriceBookRuleItems where tintType = 2 and numRuleID = v_numRuleID);
      open SWV_RefCur2 for
      select numPriceBookItemID,numRuleID,numValue,GetListIemName(numValue) AS ItemClassification,(SELECT COUNT(*) FROM Item I1 WHERE I1.numItemClassification = numValue) AS ItemsCount from
      PriceBookRuleItems
      where numRuleID = v_numRuleID and tintType = 2;
   end if;    
   if v_tintRuleAppType = 3 then
      open SWV_RefCur for
      select numPriceBookRuleDTLID,numRuleID,vcCompanyName,GetListIemName(CompanyInfo.numCompanyType) || ',' || GetListIemName(CompanyInfo.vcProfile) AS Relationship,
 (SELECT coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') || ', ' || coalesce(vcEmail,'')  FROM AdditionalContactsInformation A WHERE A.numDivisionId = DivisionMaster.numDivisionID AND coalesce(A.bitPrimaryContact,false) = true) AS PrimaryContact  from DivisionMaster
      join CompanyInfo on DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      join PriceBookRuleDTL on numValue = DivisionMaster.numDivisionID
      where DivisionMaster.numDomainID = v_numDomainID and numRuleID = v_numRuleID and tintType = 3;
   end if;       
   if v_tintRuleAppType = 4 then
      open SWV_RefCur for
      select numPriceBookRuleDTLID,numRuleID,L1.vcData || ' - ' || L2.vcData as RelProfile from PriceBookRuleDTL DTL
      Join Listdetails L1
      on L1.numListItemID = DTL.numValue
      Join Listdetails L2
      on L2.numListItemID =CAST( DTL.numProfile AS NUMERIC)
      where numRuleID = v_numRuleID and tintType = 4;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getdataforpricebookrule(numeric, numeric, smallint, refcursor, refcursor)
    OWNER TO postgres;
