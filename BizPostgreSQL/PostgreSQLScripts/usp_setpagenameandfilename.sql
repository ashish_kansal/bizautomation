-- Stored procedure definition script usp_SetPageNameAndFileName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SetPageNameAndFileName(v_vcFileName VARCHAR(50),
	v_vcPageDesc VARCHAR(100),
	v_numModuleID NUMERIC(9,0),
	v_numPageID NUMERIC(9,0) DEFAULT 0,
	v_bitIsViewApplicable INTEGER DEFAULT NULL,
	v_bitIsAddApplicable INTEGER DEFAULT NULL,
	v_bitIsUpdateApplicable INTEGER DEFAULT NULL,
	v_bitIsDeleteApplicable INTEGER DEFAULT NULL,
	v_bitIsExportApplicable INTEGER DEFAULT NULL   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numPageID > 0 then
			
      UPDATE PageMaster SET vcFileName = v_vcFileName,vcPageDesc = v_vcPageDesc,bitIsViewApplicable = v_bitIsViewApplicable,
      bitIsAddApplicable = v_bitIsAddApplicable,bitIsUpdateApplicable = v_bitIsUpdateApplicable,
      bitIsDeleteApplicable = v_bitIsDeleteApplicable,
      bitIsExportApplicable = v_bitIsExportApplicable
      WHERE numModuleID = v_numModuleID
      AND numPageID = v_numPageID;
   ELSE
      select   coalesce(MAX(numPageID),0)+1 INTO v_numPageID FROM PageMaster WHERE numModuleID = v_numModuleID;
      INSERT INTO PageMaster(numPageID, numModuleID, vcFileName, vcPageDesc,
								bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable,
								bitIsExportApplicable)
				VALUES(v_numPageID, v_numModuleID, v_vcFileName, v_vcPageDesc,
								v_bitIsViewApplicable, v_bitIsAddApplicable, v_bitIsUpdateApplicable, v_bitIsDeleteApplicable,
								v_bitIsExportApplicable);
   end if;
   RETURN;
END; $$;


