-- Stored procedure definition script USP_ManageRedirectConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE PROCEDURE USP_ManageRedirectConfig(INOUT v_numRedirectConfigID NUMERIC(18,0) ,
	v_numDomainId NUMERIC(18,0),
	v_numUserCntId NUMERIC(18,0),
	v_numSiteId NUMERIC(18,0) DEFAULT 0,
	v_vcOldUrl VARCHAR(500) DEFAULT '',
	v_vcNewUrl VARCHAR(500) DEFAULT '',
	v_numRedirectType NUMERIC(18,0) DEFAULT 0,
	v_numReferenceType NUMERIC(18,0) DEFAULT 0,
	v_numReferenceNo NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
   v_DuplicateId  INTEGER;
BEGIN


   select   COUNT(*) INTO v_Check FROM RedirectConfig WHERE  numDomainID = v_numDomainId AND numSiteID = v_numSiteId
   AND (vcOldUrl = v_vcOldUrl OR vcOldUrl = v_vcNewUrl OR vcNewUrl = v_vcOldUrl) AND bitIsActive = true;
  
   IF v_Check <= 0 then

      IF EXISTS(SELECT numRedirectConfigID FROM RedirectConfig WHERE numRedirectConfigID = v_numRedirectConfigID) then

         UPDATE RedirectConfig SET
         numDomainID = v_numDomainId,numUserCntId = v_numUserCntId,numSiteID = v_numSiteId,
         vcOldUrl = v_vcOldUrl,vcNewUrl = v_vcNewUrl,numRedirectType = v_numRedirectType,
         numReferenceType = v_numReferenceType,numReferenceNo = v_numReferenceNo,
         dtModified = LOCALTIMESTAMP
         WHERE
         numRedirectConfigID = v_numRedirectConfigID;
      ELSE
         INSERT INTO RedirectConfig(numDomainID,
		numUserCntId,
		numSiteID,
		vcOldUrl,
		vcNewUrl,
		numRedirectType,
		numReferenceType,
		numReferenceNo,
		dtCreated) VALUES(v_numDomainId,
		v_numUserCntId,
		v_numSiteId,
		v_vcOldUrl,
		v_vcNewUrl,
		v_numRedirectType,
		v_numReferenceType,
		v_numReferenceNo,
		LOCALTIMESTAMP);

         v_numRedirectConfigID := CURRVAL('RedirectConfig_seq');
      end if;
   ELSE
      select   numRedirectConfigID INTO v_DuplicateId FROM RedirectConfig WHERE  numDomainID = v_numDomainId AND numSiteID = v_numSiteId
      AND (vcOldUrl = v_vcOldUrl OR vcOldUrl = v_vcNewUrl OR vcNewUrl = v_vcOldUrl) AND bitIsActive = true;
      RAISE EXCEPTION 'URL_ALREADY_MAPPED,%i',v_DuplicateId;
   end if;
   RETURN;
END; $$;


