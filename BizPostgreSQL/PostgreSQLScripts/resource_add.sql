-- Stored procedure definition script Resource_Add for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Resource_Add(v_ResourceName VARCHAR(64), -- name of the resource  
 v_ResourceDesc TEXT,  -- optional description of the resource  
 v_ResourceEmail VARCHAR(64), -- optional e-mail contact information  
 v_EnableEmailReminders INTEGER, -- indicates preference to receive reminder  
 v_DomainId NUMERIC(9,0),    -- notification by e-mail (REN only)  
 v_numUserCntId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_NewResourceID  INTEGER;  
  
 -- Add a row to the Resource table.  
 --  
  
BEGIN
   INSERT INTO Resource(ResourceName,
  ResourceDescription,
  EmailAddress,
  numDomainId,
  numUserCntId)
 VALUES(v_ResourceName,
  v_ResourceDesc,
  v_ResourceEmail,
  v_DomainId,
  v_numUserCntId);  
  
   v_NewResourceID := currval(pg_get_serial_sequence('Resource', 'resourceid'));
  
 -- Add a row to the ResourcePreference table.  
 --  
   INSERT INTO ResourcePreference(ResourceID,
  EnableEmailReminders)
 VALUES(v_NewResourceID,
  v_EnableEmailReminders);
RETURN;  
  
 -- Re-select the ResourceID to pass the DataKey back to the application.  
 --  
-- SELECT  
--  @NewResourceID AS [ResourceID];  
END; $$;


