-- Stored procedure definition script USP_TicklerCalItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TicklerCalItem(v_numUserCntID VARCHAR(200),        
v_startDate TIMESTAMP,            
v_endDate TIMESTAMP,                                                          
v_ClientTimeZoneOffset INTEGER DEFAULT -330,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   0,0,0,ac.ActivityID as id ,case when AllDayEvent = true then '5' else '0' end as bitTask,
StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS startdate,
StartDateTimeUtc+CAST(Duration || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as endtime,
ActivityDescription as itemdesc,
cast(res.ResourceName as VARCHAR(255)) AS name,
 case when ACI.numPhone <> '' then ACI.numPhone || case when ACI.numPhoneExtension <> '' then ' - ' || ACI.numPhoneExtension else '' end  else '' end as Phone,
 SUBSTR(Comp.vcCompanyName,0,12) || '..' As vcCompanyName ,
cast(EmailAddress as VARCHAR(255)) as vcEmail,
CAST('Calandar' AS CHAR(8)) as task,
cast(Subject as VARCHAR(255)) as Activity,
case when Importance = 0 then 'Low' when Importance = 1 then 'Normal' when Importance = 2 then 'High' end as status,
0 as numcreatedby,
0 as numterid,
CAST('-/-' AS CHAR(3)) as assignedto,
CAST('0' AS CHAR(1)) as caseid,
null as vccasenumber,
0 as casetimeid,
0 as caseexpid,
1 as type ,
itemId  ,
ChangeKey
   from Activity ac join
   ActivityResource ar on  ac.ActivityID = ar.ActivityID
   join Resource res on ar.ResourceID = res.ResourceID             
--join usermaster UM on um.numUserId = res.numUserCntId    
   join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId
   JOIN DivisionMaster Div
   ON ACI.numDivisionId = Div.numDivisionID
   JOIN CompanyInfo  Comp
   ON Div.numCompanyID = Comp.numCompanyId
   where  res.numUserCntId = v_numUserCntID    and StartDateTimeUtc between v_startDate and v_endDate and Status <> -1;
END; $$;












