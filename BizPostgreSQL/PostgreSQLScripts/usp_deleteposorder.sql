CREATE OR REPLACE FUNCTION USP_DeletePOSOrder(v_numDomainID NUMERIC(9,0) DEFAULT null,                        
 v_numOppId NUMERIC(9,0) DEFAULT null,                        
 v_numOppBizDocsId NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocsPaymentDetId  NUMERIC(9,0);

   BizDocsPayments CURSOR FOR
   select numBizDocsPaymentDetId from tt_TEMPNUMBIZDOCSPAYMENTS order by numBizDocsPaymentDetId;

   v_numJournalId  NUMERIC(9,0);
BEGIN
	drop table IF EXISTS tt_TEMPNUMBIZDOCSPAYMENTS CASCADE;
	create TEMPORARY TABLE tt_TEMPNUMBIZDOCSPAYMENTS
	(
		numBizDocsPaymentDetId NUMERIC(9,0)
	);

	insert into tt_TEMPNUMBIZDOCSPAYMENTS
	select numBizDocsPaymentDetId from OpportunityBizDocsDetails where numDomainId = v_numDomainID And numBizDocsId = v_numOppBizDocsId;  

	OPEN BizDocsPayments;

	FETCH NEXT FROM BizDocsPayments into v_numBizDocsPaymentDetId;
	WHILE FOUND LOOP
		PERFORM USP_DeletePaymentDetails(v_numBizDocsPaymentDetId := v_numBizDocsPaymentDetId:: VARCHAR,v_numBizDocsId := v_numOppBizDocsId:: VARCHAR,
		v_numOppId := v_numOppId:: VARCHAR);
		FETCH NEXT FROM BizDocsPayments into v_numBizDocsPaymentDetId;
	END LOOP;

	CLOSE BizDocsPayments;

	drop table IF EXISTS tt_TEMPNUMBIZDOCSPAYMENTS CASCADE;
  
	--Delete Journal Headers
	v_numJournalId := 0;

	select 
		GJH.numJOurnal_Id INTO v_numJournalId 
	from 
		General_Journal_Header GJH 
	Where 
		GJH.numOppId = v_numOppId 
		And GJH.numOppBizDocsId = v_numOppBizDocsId
		AND numBizDocsPaymentDetId IS NULL;

	IF v_numJournalId > 0 then
		PERFORM USP_DeleteJournalDetails(v_numJournalId,v_numDomainID);
	end if;

	--Delete CommissionDetails                    
	PERFORM DeleteComissionDetails(v_numDomainID,v_numOppBizDocsId);

	--Delete Opp BizDocs
	PERFORM USP_OppBizDocs(v_byteMode := 1::SMALLINT,v_numOppId := v_numOppId,v_numOppBizDocsId := v_numOppBizDocsId,v_ClientTimeZoneOffset := 0,SWV_RefCur := null);

	--Delete Opp
	CALL USP_DeleteOppurtunity(v_numOppId,v_numDomainID,0);
	
	RETURN;
END; $$;
		



