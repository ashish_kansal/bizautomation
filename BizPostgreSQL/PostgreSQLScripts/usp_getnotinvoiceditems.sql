-- Stored procedure definition script usp_getNotInvoicedItems for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getNotInvoicedItems(v_numDomainID NUMERIC(18,2) DEFAULT 0,
	v_numoppId NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COALESCE((SELECT string_agg(I.vcItemName,',')
         FROM
         OpportunityItems AS t LEFT JOIN  Item as I ON
         t.numItemCode = I.numItemCode WHERE
         t.numOppId = v_numoppId AND t.numoppitemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN
            OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE OpportunityBizDocs.numoppid = v_numoppId AND coalesce(bitAuthoritativeBizDocs,0) = 1)),'') AS List_Item_Approval_UNIT;
   RETURN;
END; $$;

