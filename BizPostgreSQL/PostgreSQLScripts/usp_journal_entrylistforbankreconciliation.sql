-- Stored procedure definition script USP_Journal_EntryListForBankReconciliation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Journal_EntryListForBankReconciliation( --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
v_numChartAcntId NUMERIC(9,0),
               v_tintFlag       SMALLINT DEFAULT 0,
               v_numDomainId    NUMERIC(9,0) DEFAULT NULL,
               v_bitDateHide BOOLEAN DEFAULT false,
               v_numReconcileID NUMERIC(9,0) DEFAULT NULL,
               v_tintReconcile SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCreditAmt  NUMERIC(9,0);
   v_numDebitAmt  NUMERIC(9,0);
   v_dtStatementDate  TIMESTAMP;
   v_i  INTEGER DEFAULT 1;
   v_numRuleID  NUMERIC(18,0);
   v_bitMatchAllConditions  BOOLEAN;
   v_Count  INTEGER;
BEGIN
   select   dtStatementDate INTO v_dtStatementDate FROM BankReconcileMaster WHERE numReconcileID = v_numReconcileID;

   drop table IF EXISTS tt_TEMPBANKRECON CASCADE;
   create TEMPORARY TABLE tt_TEMPBANKRECON
   (
      numChartAcntId NUMERIC(18,0),
      numTransactionId NUMERIC(18,0),
      bitReconcile BOOLEAN,
      EntryDate DATE,
      numDivisionID NUMERIC(18,0),
      CompanyName VARCHAR(500),
      Deposit DOUBLE PRECISION,
      Payment DOUBLE PRECISION,
      Memo  VARCHAR(250),
      TransactionType VARCHAR(100),
      JournalId NUMERIC(18,0),
      numCheckHeaderID NUMERIC(18,0),
      vcReference VARCHAR(1000),
      tintOppType INTEGER,
      bitCleared BOOLEAN,
      numCashCreditCardId NUMERIC(18,0),
      numOppId NUMERIC(18,0),
      numOppBizDocsId NUMERIC(18,0),
      numDepositId NUMERIC(18,0),
      numCategoryHDRID NUMERIC(9,0),
      tintTEType SMALLINT,
      numCategory NUMERIC(18,0),
      numUserCntID NUMERIC(18,0),
      dtFromDate TIMESTAMP,
      numBillId NUMERIC(18,0),
      numBillPaymentID NUMERIC(18,0),
      numLandedCostOppId NUMERIC(18,0),
      bitMatched BOOLEAN,
      vcCheckNo VARCHAR(20),
      tintType SMALLINT,
      vcOrder VARCHAR(10)
   ); /* tintType: 0=No Match, 3=Perfect Match, 2=only amount match with single bank statement row, 1 = only amount match with multiple bank statement row  */
  
   IF (v_tintFlag = 1) then
      v_numDebitAmt := 0;
   ELSEIF (v_tintFlag = 2)
   then
      v_numCreditAmt := 0;
   end if; 
  
   insert into tt_TEMPBANKRECON
   SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   coalesce(GJD.bitReconcile,false) AS bitReconcile,
           GJH.datEntry_Date AS EntryDate,
		   coalesce(DM.numDivisionID,0),
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt = 0 then GJD.numDebitAmt  end) as Deposit,
			(case when GJD.numDebitAmt = 0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           CAST(GJD.varDescription AS VARCHAR(250)) as Memo,
            CAST(case when coalesce(GJH.numCheckHeaderID,0) <> 0 THEN case(select coalesce(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId = coalesce(GJH.numBizDocsPaymentDetId,0))
      when 0 then 'Cash' else 'Checks' end
   when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = false then 'Cash'
   when coalesce(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut = true And CCCD.bitChargeBack = true  then 'Charge'
   when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 1 then 'BizDocs Invoice'
   when coalesce(GJH.numOppId,0) <> 0 And coalesce(GJH.numOppBizDocsId,0) <> 0 And coalesce(GJH.numBizDocsPaymentDetId,0) = 0 And Opp.tintOppType = 2 then 'BizDocs Purchase'
   when coalesce(GJH.numDepositId,0) <> 0 then 'Deposit'
   when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 1 then 'Receive Amt'
   when coalesce(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType = 2 then 'Vendor Amt'
   when coalesce(GJH.numCategoryHDRID,0) <> 0 then 'Time And Expenses'
   When coalesce(GJH.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
   When coalesce(GJH.numBillID,0) <> 0 then 'Bill'
   When coalesce(GJH.numBillPaymentID,0) <> 0 then 'Pay Bill'
   When GJH.numJOurnal_Id <> 0 then 'Journal' End AS VARCHAR(100))  as TransactionType,
			GJD.numJournalId AS JournalId,
			coalesce(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			coalesce((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID = GJH.numDomainId AND numBizDocsPaymentDetId = coalesce(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) AS vcReference,
			coalesce(Opp.tintOppType,0),coalesce(GJD.bitCleared,false) AS bitCleared,
			coalesce(GJH.numCashCreditCardId,0) AS numCashCreditCardId,coalesce(GJH.numOppId,0) AS numOppId,
			coalesce(GJH.numOppBizDocsId,0) AS numOppBizDocsId,coalesce(GJH.numDepositId,0) AS numDepositId,
			coalesce(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,coalesce(GJH.numBillID,0) AS numBillID,coalesce(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS numLandedCostOppId,
			CAST(0 AS BOOLEAN) AS bitMatched
			,CAST(coalesce(CAST(coalesce(CHPayBill.numCheckNo,CheckHeader.numCheckNo) AS VARCHAR(30)),'') AS VARCHAR(20))
			,0
			,CAST('' AS VARCHAR(10))
   FROM   General_Journal_Header GJH
   INNER JOIN General_Journal_Details GJD
   ON GJH.numJOurnal_Id = GJD.numJournalId
   LEFT OUTER JOIN DivisionMaster AS DM
   ON GJD.numCustomerId = DM.numDivisionID
   LEFT OUTER JOIN CompanyInfo AS CI
   ON DM.numCompanyID = CI.numCompanyId
   Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId = CCCD.numCashCreditId
   Left outer join(select * from OpportunityMaster WHERE tintopptype = 1) Opp on GJH.numOppId = Opp.numOppId
   LEFT OUTER JOIN timeandexpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
   LEFT OUTER JOIN BillHeader BH  ON coalesce(GJH.numBillID,0) = BH.numBillID
   LEFT OUTER JOIN CheckHeader ON GJH.numCheckHeaderID = CheckHeader.numCheckHeaderID
   LEFT OUTER JOIN CheckHeader CHPayBill ON coalesce(GJH.numBillPaymentID,0) > 0 AND GJH.numBillPaymentID = CHPayBill.numReferenceID
   WHERE
		   ((GJH.datEntry_Date <= v_dtStatementDate AND v_bitDateHide = true) OR v_bitDateHide = false)
   AND GJD.numChartAcntId = v_numChartAcntId
   AND GJD.numDomainId = v_numDomainId
   AND (GJD.numCreditAmt <> v_numCreditAmt OR v_numCreditAmt IS null)
   AND (GJD.numDebitAmt <> v_numDebitAmt OR v_numDebitAmt IS null)
   AND (coalesce(GJH.numReconcileID,0) = 0 OR (coalesce(GJH.numReconcileID,0) = v_numReconcileID  AND coalesce(bitReconcileInterest,false) = true))
   AND 1 =(CASE WHEN v_tintReconcile = 0 THEN CASE WHEN coalesce(GJD.bitReconcile,false) = false AND (coalesce(GJD.numReconcileID,0) = 0 OR coalesce(GJD.numReconcileID,0) = v_numReconcileID) THEN 1 ELSE 0 END
   WHEN v_tintReconcile = 1 THEN CASE WHEN coalesce(GJD.numReconcileID,0) = v_numReconcileID THEN 1 ELSE 0 END
   WHEN v_tintReconcile = 2 THEN CASE WHEN  ((coalesce(GJD.bitReconcile,false) = true OR coalesce(GJD.bitCleared,false) = true) AND coalesce(GJD.numReconcileID,0) = v_numReconcileID) OR (coalesce(GJD.bitReconcile,false) = false AND coalesce(GJD.numReconcileID,0) = 0) THEN 1 ELSE 0 END END)
   ORDER BY GJH.datEntry_Date;

   drop table IF EXISTS tt_TEMPSTATEMEMNT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSTATEMEMNT ON COMMIT DROP AS
      SELECT
      B1.ID,
		FormatedDateFromDate(B1.dtEntryDate,v_numDomainId) as dtEntryDate,
		coalesce(vcReference,'') as vcReference,
		fltAmount,
		coalesce(vcPayee,'') as vcPayee,
		coalesce(vcDescription,'') as vcDescription,
		coalesce(B1.bitCleared,false) AS bitCleared,
		coalesce(B1.bitReconcile,false) AS bitReconcile,
		0 AS numBizTransactionId,
		false AS bitMatched,
		false AS bitDuplicate,
		0 AS tintType
	
      FROM
      BankReconcileFileData B1
      WHERE
      numReconcileID = v_numReconcileID
      AND coalesce(B1.bitReconcile,false) = false;

   IF EXISTS(SELECT * FROM BankReconcileMatchRule WHERE numDomainID = v_numDomainId AND v_numChartAcntId IN(SELECT Id FROM SplitIDs(vcBankAccounts,','))) then
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numRuleID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numRuleID)
      SELECT
      ID
      FROM
      BankReconcileMatchRule
      WHERE
      numDomainID = v_numDomainId
      AND v_numChartAcntId IN(SELECT Id FROM SplitIDs(vcBankAccounts,','))
      ORDER BY
      tintOrder;
      select   COUNT(*) INTO v_Count FROM tt_TEMP;
      WHILE v_i <= v_Count LOOP
         select   numRuleID, bitMatchAllConditions INTO v_numRuleID,v_bitMatchAllConditions FROM tt_TEMP T1 INNER JOIN BankReconcileMatchRule BRMR ON T1.numRuleID = BRMR.ID WHERE T1.ID = v_i;
         UPDATE
         tt_TEMPSTATEMEMNT TS
         SET
         numBizTransactionId =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),bitMatched =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN true ELSE false END),bitDuplicate =(CASE WHEN coalesce(TEMP2.ID,0) > 0 THEN true ELSE false END),tintType =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN 3 ELSE 0 END)
         FROM
         BankReconcileFileData B1 LEFT JOIN LATERAL(SELECT 
         t1.numTransactionId
         FROM
         tt_TEMPBANKRECON t1
         LEFT JOIN LATERAL(SELECT(CASE
            WHEN v_bitMatchAllConditions = true
            THEN(SELECT(CASE WHEN COUNT(*) > 0 THEN false ELSE true END)
                  FROM(SELECT
                     numConditionID
										,(CASE tintConditionOperator
                     WHEN 1 --contains
                     THEN(CASE WHEN LOWER((CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END)) ilike '%' || LOWER(BRMRC.vcTextToMatch) || '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 2 --equals 
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 3 --starts with
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) ilike BRMRC.vcTextToMatch || '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 4 --ends with
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) ilike '%' || BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     ELSE
                        false
                     END) AS isMatched
                     FROM
                     BankReconcileMatchRuleCondition BRMRC
                     WHERE
                     numRuleID = v_numRuleID) T3
                  WHERE
                  T3.isMatched = false)
            ELSE(SELECT(CASE WHEN COUNT(*) > 0 THEN true ELSE false END)
                  FROM(SELECT
                     numConditionID
										,(CASE tintConditionOperator
                     WHEN 1 --contains
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) ilike '%' || BRMRC.vcTextToMatch || '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 2 --equals 
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 3 --starts with
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) ilike BRMRC.vcTextToMatch || '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     WHEN 4 --ends with
                     THEN(CASE WHEN(CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) ilike '%' || BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN true ELSE false END)
                     ELSE
                        false
                     END) AS isMatched
                     FROM
                     BankReconcileMatchRuleCondition BRMRC
                     WHERE
                     numRuleID = v_numRuleID) T4
                  WHERE
                  T4.isMatched = true)
            END) AS bitMatched) TEMPConditions on TRUE
         WHERE
         t1.EntryDate = B1.dtEntryDate
         AND TEMPConditions.bitMatched = true
         AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1 = B1.fltAmount)
         AND coalesce(t1.bitMatched,false) = false LIMIT 1) AS TEMP1 LEFT JOIN LATERAL(SELECT 
         t1.ID
         FROM
         BankReconcileFileData t1
         WHERE
         t1.numReconcileID <> v_numReconcileID
         AND t1.numDomainID = v_numDomainId
         AND (coalesce(t1.bitReconcile,false) = true OR coalesce(t1.bitCleared,false) = true)
         AND t1.dtEntryDate = B1.dtEntryDate
         AND B1.vcPayee = t1.vcPayee
         AND B1.fltAmount = t1.fltAmount
         AND B1.vcReference = t1.vcReference
         AND B1.vcDescription = t1.vcDescription LIMIT 1) AS TEMP2 ON TRUE ON TRUE
         WHERE(TS.ID = B1.ID
         AND coalesce(TS.numBizTransactionId,0) = 0
         AND coalesce(TS.bitMatched,false) = false) AND numReconcileID = v_numReconcileID;
         UPDATE
         tt_TEMPBANKRECON t2
         SET
         bitMatched = true,tintType = 3,vcOrder = CONCAT('D',t1.ID)
         FROM
         tt_TEMPSTATEMEMNT t1
         WHERE
         t2.numTransactionId = coalesce(t1.numBizTransactionId,0) AND coalesce(t2.bitMatched,false) = false;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;

   UPDATE
   tt_TEMPSTATEMEMNT TS
   SET
   numBizTransactionId =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),bitMatched =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN true ELSE false END),bitDuplicate =(CASE WHEN coalesce(TEMP2.ID,0) > 0 THEN true ELSE false END),tintType =(CASE WHEN coalesce(TEMP1.numTransactionId,0) > 0 THEN 3 ELSE 0 END)
   FROM
   BankReconcileFileData B1 LEFT JOIN LATERAL(SELECT 
   t1.numTransactionId
   FROM
   tt_TEMPBANKRECON t1
   WHERE
   t1.EntryDate = B1.dtEntryDate
   AND (POSITION(UPPER(t1.CompanyName) IN UPPER(B1.vcPayee)) > 0 OR POSITION(UPPER(t1.CompanyName) IN UPPER(B1.vcDescription)) > 0 OR (POSITION('CHECK' IN UPPER(B1.vcDescription)) > 0 AND LENGTH(t1.vcCheckNo) > 0 AND POSITION(UPPER(t1.vcCheckNo) IN UPPER(B1.vcDescription)) > 0))
   AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1 = B1.fltAmount) LIMIT 1) AS TEMP1 LEFT JOIN LATERAL(SELECT 
   t1.ID
   FROM
   BankReconcileFileData t1
   WHERE
   t1.numReconcileID <> v_numReconcileID
   AND t1.numDomainID = v_numDomainId
   AND (coalesce(t1.bitReconcile,false) = true OR coalesce(t1.bitCleared,false) = true)
   AND t1.dtEntryDate = B1.dtEntryDate
   AND B1.vcPayee = t1.vcPayee
   AND B1.fltAmount = t1.fltAmount
   AND B1.vcReference = t1.vcReference
   AND B1.vcDescription = t1.vcDescription LIMIT 1) AS TEMP2 ON TRUE ON TRUE
   WHERE(TS.ID = B1.ID
   AND coalesce(TS.numBizTransactionId,0) = 0
   AND coalesce(TS.bitMatched,false) = false) AND(numReconcileID = v_numReconcileID
   AND coalesce(TS.bitMatched,false) = false);

   UPDATE
   tt_TEMPBANKRECON t2
   SET
   bitMatched = true,tintType = t1.tintType,vcOrder = CONCAT('D',t1.ID)
   FROM
   tt_TEMPSTATEMEMNT t1
   WHERE
   t2.numTransactionId = t1.numBizTransactionId AND coalesce(t2.bitMatched,false) = false;

	------------------------------- AMOUNT MATCH ONLY SINGAL JOURNAL ENTRY IN BIZ ------------------------------------------
   UPDATE
   tt_TEMPSTATEMEMNT
   SET
   bitMatched = true,tintType = 2,numBizTransactionId =(SELECT numTransactionId FROM tt_TEMPBANKRECON WHERE coalesce(bitMatched,false) = false AND (Deposit = tt_TEMPSTATEMEMNT.fltAmount OR Payment*-1 = tt_TEMPSTATEMEMNT.fltAmount))
   WHERE
   coalesce(bitMatched,false) = false
   AND(SELECT COUNT(*) FROM tt_TEMPBANKRECON WHERE coalesce(bitMatched,false) = false AND (Deposit = tt_TEMPSTATEMEMNT.fltAmount OR Payment*-1 = tt_TEMPSTATEMEMNT.fltAmount)) = 1;

   UPDATE
   tt_TEMPBANKRECON t2
   SET
   bitMatched = true,tintType = t1.tintType,vcOrder = CONCAT('C',t1.ID)
   FROM
   tt_TEMPSTATEMEMNT t1
   WHERE
   t2.numTransactionId = t1.numBizTransactionId AND coalesce(t2.bitMatched,false) = false;

	------------------------------- AMOUNT MATCH ONLY MULTIPLE JOURNAL ENTRY IN BIZ ------------------------------------------
   UPDATE
   tt_TEMPSTATEMEMNT
   SET
   tintType = 1
   WHERE
   coalesce(bitMatched,false) = false
   AND(SELECT COUNT(*) FROM tt_TEMPBANKRECON WHERE coalesce(bitMatched,false) = false AND (Deposit = tt_TEMPSTATEMEMNT.fltAmount OR Payment*-1 = tt_TEMPSTATEMEMNT.fltAmount)) > 1;

   UPDATE
   tt_TEMPBANKRECON t2
   SET
   tintType = 1,vcOrder = CONCAT('B',t1.ID)
   FROM
   tt_TEMPSTATEMEMNT t1
   WHERE(coalesce(t1.tintType,0) = 1
   AND(t2.Deposit = t1.fltAmount OR t2.Payment*-1 = t1.fltAmount)) AND coalesce(t2.bitMatched,false) = false;

	 
   open SWV_RefCur for
   SELECT * FROM tt_TEMPBANKRECON ORDER BY tintType DESC,vcOrder ASC,EntryDate ASC;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPSTATEMEMNT WHERE coalesce(bitDuplicate,false) = false ORDER BY tintType DESC,ID;

   open SWV_RefCur3 for
   SELECT(SELECT COUNT(*) FROM tt_TEMPSTATEMEMNT) AS TotalRecords
		,(SELECT COUNT(*) FROM tt_TEMPSTATEMEMNT WHERE bitDuplicate = true) AS DuplicateRecords
		,(SELECT COUNT(*) FROM tt_TEMPSTATEMEMNT WHERE bitMatched = true) AS MatchedRecords;

	    
   RETURN;
END; $$;


