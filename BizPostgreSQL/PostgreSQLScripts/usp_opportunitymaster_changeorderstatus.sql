-- Stored procedure definition script USP_OpportunityMaster_ChangeOrderStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_ChangeOrderStatus(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numStatus NUMERIC(18,0),
	v_vcOppIds TEXT -- Comma seperated list of order ids
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numOppID  NUMERIC(18,0);
BEGIN
   IF LENGTH(coalesce(v_vcOppIds,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numOppID)
      SELECT
      ID
      FROM
      SplitIDs(v_vcOppIds,',');
      UPDATE OpportunityMaster SET numStatus = v_numStatus,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numDomainId = v_numDomainID AND numOppId IN(SELECT numOppID FROM tt_TEMP);
      select   COUNT(*) INTO v_iCount FROM tt_TEMP;
      WHILE v_i <= v_iCount LOOP
         select   numOppID INTO v_numOppID FROM tt_TEMP WHERE ID = v_i;
         PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppID,v_numStatus);
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
   RETURN;
END; $$;



