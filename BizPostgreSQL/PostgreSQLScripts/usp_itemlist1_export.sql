CREATE OR REPLACE FUNCTION public.usp_itemlist1_export (
  p_numdomainid numeric,
  p_numwarehouseid numeric,
  INOUT SWV_RefCur refcursor 
)
LANGUAGE plpgsql
AS $$
BEGIN
OPEN SWV_RefCur FOR
SELECT	i.numItemCode as ItemId,
        i.vcItemName as Itemname,
        whi.numWareHouseItemID,
        numOnHand AS OnHand,
        numAllocation AS OnAllocation,
        COALESCE(numOnHand, 0) + COALESCE(numAllocation, 0) AS TotalOnHandPlusAllocation,
        COALESCE(i.monAverageCost, 0) as AverageCost,
        (COALESCE(numOnHand, 0) + COALESCE(numAllocation, 0)) * COALESCE(i.monAverageCost, 0)  AS StockValue,
        numWareHouseID 											
   FROM	WareHouseItems whi JOIN Item i ON (i.numItemCode = whi.numItemID)   
   WHERE (whi.numWareHouseID = p_numWareHouseID OR p_numWareHouseID = 0)
	 AND i.numDomainID = p_numDomainID
	 AND COALESCE(i.IsArchieve, FALSE) = FALSE 
	 AND COALESCE(whi.numOnHand, 0) + COALESCE(whi.numAllocation, 0) > 0
     AND COALESCE(i.monAverageCost, 0) > 0;
END;
$$;