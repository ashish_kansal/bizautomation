DROP FUNCTION IF EXISTS usp_getfieldsshrtbar;

CREATE OR REPLACE FUNCTION public.usp_getfieldsshrtbar(
	v_numgroupid numeric,
	v_numdomainid numeric,
	v_numtabid numeric,
	INOUT swv_refcur refcursor,
	INOUT swv_refcur2 refcursor)
    RETURNS record
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF(SELECT COUNT(*) FROM ShortCutGrpConf WHERE numTabId = v_numTabId and numDomainId = v_numDomainId and numGroupId = v_numGroupID) > 0 then
	
      open SWV_RefCur for
      SELECT
      CAST(Hdr.ID AS VARCHAR(10)) || '~0' AS id
			,Hdr.vcLinkName
			,coalesce(Sconf.bitInitialPage,false) AS bitInitialPage
			,Sconf.numOrder
      FROM
      ShortCutGrpConf Sconf
      JOIN
      ShortCutBar Hdr
      ON
      Hdr.ID = Sconf.numLinkId
      WHERE
      Hdr.numTabId = v_numTabId
      AND Sconf.numTabId = Hdr.numTabId
      AND Sconf.numDomainId = v_numDomainId
      AND Sconf.numGroupId = v_numGroupID
      AND Hdr.numContactId = 0
      AND coalesce(Sconf.tintLinkType,0) = 0
      UNION
      SELECT
      CAST(LD.numListItemID AS VARCHAR(10)) || '~5' AS id
			,LD.vcData as vclinkname
			,coalesce(Sconf.bitInitialPage,false) AS bitInitialPage
			,Sconf.numOrder
      FROM
      ShortCutGrpConf Sconf
      JOIN
      Listdetails LD
      ON
      LD.numListItemID = Sconf.numLinkId
      WHERE
      LD.numListID = 5
      AND LD.numListItemID <> 46
      AND (LD.numDomainid = v_numDomainId OR constflag = true)
      AND Sconf.numTabId = v_numTabId
      AND Sconf.numDomainId = v_numDomainId
      AND Sconf.numGroupId = v_numGroupID
      AND coalesce(Sconf.tintLinkType,0) = 5
      AND v_numTabId = 7
      ORDER BY
      4;
   ELSE
      open SWV_RefCur for
      SELECT
      CAST(ID AS VARCHAR(10)) || '~0' AS id,vcLinkName
			,coalesce(bitInitialPage,false) AS bitInitialPage
      FROM
      ShortCutBar
      WHERE
      numTabId = v_numTabId
      AND bitdefault = true
      UNION
      SELECT
      CAST(numListItemID AS VARCHAR(10)) || '~5' AS id
			,vcData as vclinkname
			,false as bitInitialPage
      FROM
      Listdetails
      WHERE
      numListID = 5
      AND numListItemID <> 46
      AND (numDomainid = v_numDomainId or constFlag = true)
      AND v_numTabId = 7;
   end if;    

   IF(SELECT COUNT(*) FROM ShortCutGrpConf WHERE numTabId = v_numTabId and numDomainId = v_numDomainId and numGroupId = v_numGroupID AND bitDefaultTab = true) > 0 then
	
      open SWV_RefCur2 for
      SELECT 1;
   ELSE
      open SWV_RefCur2 for
      SELECT 0;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getfieldsshrtbar(numeric, numeric, numeric, refcursor, refcursor)
    OWNER TO postgres;
