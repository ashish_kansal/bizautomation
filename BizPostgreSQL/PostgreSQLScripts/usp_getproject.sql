-- Stored procedure definition script USP_GetProject for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetProject(v_numProId NUMERIC(9,0) DEFAULT null  ,                  
v_numDomainID NUMERIC(9,0) DEFAULT NULL,    
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strOppName  VARCHAR(200);
BEGIN
   v_strOppName := '';              
   select   coalesce(v_strOppName,'') || ' <br> ' || vcpOppName INTO v_strOppName from ProjectsOpportunities CO
   join OpportunityMaster OM on OM.numOppId = CO.numOppId and numProId = v_numProId
   and CO.numDomainId = v_numDomainID;                                   
                                    
   open SWV_RefCur for select  pro.numProId,
  pro.vcProjectName,tintCRMType,
  fn_GetContactName(pro.numIntPrjMgr) as numintPrjMgr,
v_strOppName as numOppId,
 FormatedDateFromDate(pro.intDueDate,v_numDomainID) as intDueDate,
    fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgr,
  pro.numDivisionId,
  pro.txtComments,
  div.vcDivisionName,
  com.vcCompanyName,
  fn_GetContactName(pro.numCreatedBy) || ' ' || CAST(pro.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcCreatedBy ,
  fn_GetContactName(pro.numModifiedBy) || ' ' || CAST(pro.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcModifiedby,
  pro.numdomainId,
  fn_GetContactName(pro.numRecOwner) as vcRecOwner,
  pro.numAssignedby,
pro.numcontractId              ,
pro.numAssignedTo ,
(select  count(*) from GenericDocuments   where numRecID = v_numProId and  vcDocumentSection = 'P') as DocumentCount,
(select sum(tintPercentage) from ProjectsStageDetails where numProId = v_numProId and bitStageCompleted = true) as TProgress
   from ProjectsMaster pro
   left join DivisionMaster div
   on pro.numDivisionId = div.numDivisionID
   left join CompanyInfo com
   on div.numCompanyID = com.numCompanyId
   where numProId = v_numProId     and pro.numdomainId =  v_numDomainID;
END; $$;  













