-- Stored procedure definition script USP_ReportFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportFields(v_byteMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then
      open SWV_RefCur for
      select vcFieldName,numRepFieldID from ReportFields;
   end if;

   if v_byteMode = 1 then
      open SWV_RefCur for
      select vcFieldName,numRepFieldID from ReportFields where tintSelected = 1;
   end if;
   RETURN;
END; $$;


