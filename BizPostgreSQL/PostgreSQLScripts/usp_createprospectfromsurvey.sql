-- Stored procedure definition script usp_CreateProspectFromSurvey for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CreateProspectFromSurvey(v_vcCompanyName VARCHAR(250),
	v_vcFirstName VARCHAR(250) DEFAULT '',
	v_vcLastName VARCHAR(250) DEFAULT '',
	v_vcEmail VARCHAR(200) DEFAULT ''
--

)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_revsql  VARCHAR(250);
   v_x  INTEGER;
   v_LastPivot  VARCHAR(100);
   v_FirstPivot  VARCHAR(100);
   v_companyid  INTEGER;
   v_divisionid  INTEGER;
BEGIN
   update surveyrespondants set bitpromoted = true where vcCompanyName || ', ' || vcDivisionName = v_vcCompanyName;
	--select vccompanyname from companyinfo where vccompanyname = 
	--Reverse the string	

   v_revsql := Reverse(v_vcCompanyName);	--Search for last occurence of WHEN	
   v_x := POSITION(',' IN v_revsql);	--Reverse the string again	

   v_LastPivot := Reverse(SUBSTR(v_revsql,1,v_x -1));	--Reset @sql to remove the imcomplete item	
   v_LastPivot := Reverse(SUBSTR(v_revsql,1,v_x -1));	--Reset @sql to remove the imcomplete item	
   v_vcCompanyName := SUBSTR(v_vcCompanyName,1,LENGTH(v_vcCompanyName) -(v_x -1));
   v_FirstPivot := Reverse(SUBSTR(v_revsql,length(v_revsql) -(v_x -1)+1));	--Reset @sql to remove the imcomplete item	
--	Set @LastPivot = Substring(@LastPivot, 4, charindex("' ", @LastPivot) - 4)
	
   select   numCompanyId INTO v_companyid from CompanyInfo where vcCompanyName = v_FirstPivot; 	
   select   numDivisionID INTO v_divisionid from DivisionMaster where numCompanyID = v_companyid and vcDivisionName = v_LastPivot;
   if v_vcFirstName = '' then
	
      update DivisionMaster set tintCRMType = 1 where numDivisionID = v_divisionid;
   Else
      update DivisionMaster set tintCRMType = 1 where numDivisionID =(Select numDivisionId from AdditionalContactsInformation where vcFirstName = v_vcFirstName and vcLastname = v_vcLastName and vcEmail = v_vcEmail);
   end if;
   RETURN;

--	INSERT INTO Forecast(sintYear,	tintquarter,tintMonth,vcQuota,numClosed,numPipeline,decProbability,numForecastAmount,numCreatedBy,bintCreatedDate,numDomainID)
--	VALUES(@sintYear,@tintquarter,@tintmonth,@vcQuota,@numClosed,@numPipeline,@decProbability,@numForecastAmount,@numCreatedBy,@bintCreatedDate,@numDomainID)
	
END; $$;


