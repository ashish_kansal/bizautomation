DROP FUNCTION IF EXISTS CheckOrderInventoryStatus;

CREATE OR REPLACE FUNCTION CheckOrderInventoryStatus(v_numOppID NUMERIC(18,0),
      v_numDomainID NUMERIC(18,0),
	  v_tintMode SMALLINT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcInventoryStatus  TEXT DEFAULT '';
   v_numTotalOrdered  DOUBLE PRECISION;
   v_numTotalShipped  DOUBLE PRECISION;
   v_numTotalBackOrder  DOUBLE PRECISION;
BEGIN
   IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numDomainId = v_numDomainID
   AND numOppId = v_numOppID AND coalesce(tintshipped,0) = 1) OR(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
																			coalesce(OI.numUnitHour,0) AS OrderedQty,
																			coalesce(TempFulFilled.FulFilledQty,0) AS FulFilledQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
         AND CAST(OpportunityBizDocItems.numOppItemID AS NUMERIC) = OI.numoppitemtCode
         AND coalesce(bitFulFilled,false) = true) AS TempFulFilled on TRUE
      WHERE
      OI.numOppId = v_numOppID
      AND UPPER(I.charItemType) = 'P') X
   WHERE
   X.OrderedQty <> X.FulFilledQty) = 0 then
	
      v_vcInventoryStatus := '<font color="#008000">Shipped</font>';
   ELSE
      DROP TABLE IF EXISTS tt_TEMPCheckOrderInventoryStatus CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCheckOrderInventoryStatus
      (
         numOppItemID NUMERIC(18,0),
         numQtyOrdered NUMERIC(18,0),
         numQtyShipped NUMERIC(18,0),
         numQtyBackOrder NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPCheckOrderInventoryStatus(numOppItemID
			,numQtyOrdered
			,numQtyShipped
			,numQtyBackOrder)
      SELECT
      OI.numoppitemtCode
			,OI.numUnitHour
			,coalesce(numQtyShipped,0)
			,CASE WHEN coalesce(OI.bitDropShip,false) = true THEN 0 ELSE CheckOrderItemInventoryStatus(OI.numItemCode,OI.numUnitHour,OI.numoppitemtCode,coalesce(OI.numWarehouseItmsID,0),
         I.bitKitParent) END
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS numShippedQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
         AND CAST(OpportunityBizDocItems.numOppItemID AS NUMERIC) = OI.numoppitemtCode
         AND coalesce(bitFulFilled,false) = true) AS TempShipped on TRUE
      WHERE
      numoppid = v_numOppID
      AND I.charItemType = 'P';
      select   SUM(numQtyOrdered) INTO v_numTotalOrdered FROM tt_TEMPCheckOrderInventoryStatus;
      select   SUM(numQtyShipped) INTO v_numTotalShipped FROM tt_TEMPCheckOrderInventoryStatus;
      select   SUM(numQtyBackOrder) INTO v_numTotalBackOrder FROM tt_TEMPCheckOrderInventoryStatus;
      IF v_numTotalBackOrder > 0 then
		
         v_vcInventoryStatus := CONCAT('<font color="#FF0000">BO (',v_numTotalBackOrder,')</font>');
      end if;
      IF((v_numTotalOrdered -v_numTotalShipped) -v_numTotalBackOrder) > 0 then
		
         v_vcInventoryStatus := CONCAT(v_vcInventoryStatus,(CASE WHEN LENGTH(v_vcInventoryStatus) > 0 THEN ' ' ELSE '' END),
         '<font color="#6f30a0">Shippable (',((v_numTotalOrdered -v_numTotalShipped) -v_numTotalBackOrder),
         ')</font>');
      end if;
      IF v_numTotalShipped > 0 then
		
         v_vcInventoryStatus := CONCAT(v_vcInventoryStatus,(CASE WHEN LENGTH(v_vcInventoryStatus) > 0 THEN ' ' ELSE '' END),
         '<font color="#00b050">Shipped (',v_numTotalShipped,')</font>');
      end if;
   end if;

   IF(SELECT
   COUNT(*)
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OM.numOppId = OI.numOppId
   WHERE
   OI.numOppId = v_numOppID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OI.bitDropShip,false) = true
   AND 1 =(CASE WHEN(SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID = OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode = OI.numItemCode AND coalesce(OIInner.numWarehouseItmsID,0) = coalesce(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)) > 0 then
	
      IF v_tintMode = 1 then
		
         v_vcInventoryStatus := CONCAT(v_vcInventoryStatus,' <a href="#" onclick="OpenCreateDopshipPOWindow(',
         v_numOppID,')"><img src="../images/Dropship.png" style="height:25px;" /></a>');
      end if;
   end if;

   IF(SELECT
   COUNT(*)
   FROM
   OpportunityItems OI
   INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
   INNER JOIN Item I ON I.numItemCode = OI.numItemCode
   INNER JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
   WHERE
   OI.numOppId = v_numOppID
   AND OM.numDomainId = v_numDomainID
   AND coalesce((SELECT bitReOrderPoint FROM Domain WHERE numDomainId = v_numDomainID),
   false) = true
   AND coalesce(WI.numOnHand,0)+coalesce(WI.numonOrder,0) <= coalesce(WI.numReorder,0)
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(I.bitAssembly,false) = false
   AND coalesce(I.bitKitParent,false) = false
   AND coalesce(OI.bitDropShip,false) = false) > 0 then
	
      IF v_tintMode = 1 then
		
         v_vcInventoryStatus := CONCAT(v_vcInventoryStatus,' <a href="#" onclick="OpenCreateBackOrderPOWindow(',
         v_numOppID,')"><img src="../images/BackorderPO.png" style="height:25px;" /></a>');
      end if;
   end if;

   IF LENGTH(coalesce(v_vcInventoryStatus,'')) = 0 then
	
      v_vcInventoryStatus := '<font color="#000000">Not Applicable</font>';
   end if;
    
   RETURN coalesce(v_vcInventoryStatus,'');
END; $$;

