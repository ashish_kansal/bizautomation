-- Function definition script GetPromotionDiscountDescription for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPromotionDiscountDescription(v_numPromotionID NUMERIC(18,0)
	,v_vcCurrency VARCHAR(10))
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  VARCHAR(1000) DEFAULT '';
BEGIN
   select(CASE
   WHEN tintDiscountType = 1
   THEN
      CONCAT('Get ',coalesce(fltDiscountValue,0),'% off any of these item’s ',(CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
   WHEN tintDiscountType = 2
   THEN
      CONCAT('Get ',v_vcCurrency,coalesce(fltDiscountValue,0),' off any of these item’s ',
      (CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
   WHEN tintDiscountType = 3
   THEN
      CONCAT('Get any ',coalesce(fltDiscountValue,0),' of these items for FREE !')
   END) INTO v_vcValue FROM
   PromotionOffer WHERE
   numProId = v_numPromotionID;

   RETURN coalesce(v_vcValue,'');
END; $$;

