-- Stored procedure definition script USP_GetChartOfAcntDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartOfAcntDetails(v_numAcntTypeId NUMERIC(9,0) DEFAULT 0,    
 v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1 
        
   open SWV_RefCur for Select numAccountId as numAccountId From Chart_Of_Accounts Where numAcntTypeId = v_numAcntTypeId And numParntAcntTypeID = v_numParntAcntId;
END; $$;












