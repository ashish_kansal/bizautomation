-- Stored procedure definition script USP_ManageSiteTemplates for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSiteTemplates(INOUT v_numTemplateID NUMERIC(9,0) DEFAULT 0 ,
	v_vcTemplateName VARCHAR(50) DEFAULT NULL,
	v_txtTemplateHTML TEXT DEFAULT NULL,
	v_numSiteID NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_numUserCntID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numTemplateID = 0 AND (NOT EXISTS(SELECT vcTemplateName FROM SiteTemplates WHERE vcTemplateName = v_vcTemplateName
   AND numSiteID = v_numSiteID)) then

      INSERT INTO SiteTemplates(vcTemplateName,
		txtTemplateHTML,
		numSiteID,
		numDomainID,
		numCreatedBy,
		dtCreateDate,
		numModifiedBy,
		dtModifiedDate)
	VALUES(v_vcTemplateName,
		v_txtTemplateHTML,
		v_numSiteID,
		v_numDomainID,
		v_numUserCntID,
		TIMEZONE('UTC',now()),
		v_numUserCntID,
		TIMEZONE('UTC',now()));

      v_numTemplateID := CURRVAL('SiteTemplates_seq');
   ELSE
      UPDATE SiteTemplates SET
      vcTemplateName = v_vcTemplateName,txtTemplateHTML = v_txtTemplateHTML,
      numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
      WHERE numTemplateID = v_numTemplateID AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;




