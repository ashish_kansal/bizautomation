-- Stored procedure definition script USP_ManageAlertConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAlertConfig(INOUT v_numAlertConfigId NUMERIC(18,0)  ,
    v_numDomainId NUMERIC(18,0),
    v_numContactId NUMERIC(9,0),
    v_bitOpenActionItem BOOLEAN,
    v_vcOpenActionMsg VARCHAR(15),
    v_bitOpenCases BOOLEAN,
    v_vcOpenCasesMsg VARCHAR(15),
    v_bitOpenProject BOOLEAN,
    v_vcOpenProjectMsg VARCHAR(15),
    v_bitOpenSalesOpp BOOLEAN,
    v_vcOpenSalesOppMsg VARCHAR(15),
    v_bitOpenPurchaseOpp BOOLEAN,
    v_vcOpenPurchaseOppMsg VARCHAR(15),
    v_bitBalancedue BOOLEAN,
    v_monBalancedue DECIMAL(20,5),
    v_vcBalancedueMsg VARCHAR(15),
    v_bitUnreadEmail BOOLEAN,
    v_intUnreadEmail INTEGER,
    v_vcUnreadEmailMsg VARCHAR(15),
    v_bitPurchasedPast BOOLEAN,
    v_monPurchasedPast DECIMAL(20,5),
    v_vcPurchasedPastMsg VARCHAR(15),
    v_bitSoldPast BOOLEAN,
    v_monSoldPast DECIMAL(20,5),
    v_vcSoldPastMsg VARCHAR(15),
    v_bitCustomField BOOLEAN,
    v_numCustomFieldId NUMERIC(9,0),
    v_vcCustomFieldValue VARCHAR(50),
    v_vcCustomFieldMsg VARCHAR(15),
	v_bitCampaign BOOLEAN)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCount  NUMERIC;
BEGIN
   v_numCount := 0;
   select   COUNT(*) INTO v_numCount FROM AlertConfig WHERE numDomainID = v_numDomainId AND numContactId = v_numContactId;
   RAISE NOTICE '%',v_numCount; 
   IF v_numCount > 0 then
    
      UPDATE AlertConfig
      SET    numDomainID = v_numDomainId,numContactId = v_numContactId,bitOpenActionItem = v_bitOpenActionItem, 
      vcOpenActionMsg = v_vcOpenActionMsg,bitOpenCases = v_bitOpenCases, 
      vcOpenCasesMsg = v_vcOpenCasesMsg,bitOpenProject = v_bitOpenProject, 
      vcOpenProjectMsg = v_vcOpenProjectMsg,bitOpenSalesOpp = v_bitOpenSalesOpp, 
      vcOpenSalesOppMsg = v_vcOpenSalesOppMsg,bitOpenPurchaseOpp = v_bitOpenPurchaseOpp,
      vcOpenPurchaseOppMsg = v_vcOpenPurchaseOppMsg, 
      bitBalancedue = v_bitBalancedue,monBalancedue = v_monBalancedue,
      vcBalancedueMsg = v_vcBalancedueMsg,bitUnreadEmail = v_bitUnreadEmail, 
      intUnreadEmail = v_intUnreadEmail,vcUnreadEmailMsg = v_vcUnreadEmailMsg, 
      bitPurchasedPast = v_bitPurchasedPast,monPurchasedPast = v_monPurchasedPast, 
      vcPurchasedPastMsg = v_vcPurchasedPastMsg,bitSoldPast = v_bitSoldPast, 
      monSoldPast = v_monSoldPast,vcSoldPastMsg = v_vcSoldPastMsg, 
      bitCustomField = v_bitCustomField,numCustomFieldId = v_numCustomFieldId, 
      vcCustomFieldValue = v_vcCustomFieldValue,vcCustomFieldMsg = v_vcCustomFieldMsg,
      bitCampaign = v_bitCampaign
      WHERE numDomainID = v_numDomainId AND numContactId = v_numContactId;
   else
      INSERT INTO AlertConfig(numDomainID, numContactId, bitOpenActionItem, vcOpenActionMsg, bitOpenCases, vcOpenCasesMsg, bitOpenProject, vcOpenProjectMsg, bitOpenSalesOpp, vcOpenSalesOppMsg, bitOpenPurchaseOpp, vcOpenPurchaseOppMsg, bitBalancedue, monBalancedue, vcBalancedueMsg, bitUnreadEmail, intUnreadEmail, vcUnreadEmailMsg, bitPurchasedPast, monPurchasedPast, vcPurchasedPastMsg, bitSoldPast, monSoldPast, vcSoldPastMsg, bitCustomField, numCustomFieldId, vcCustomFieldValue, vcCustomFieldMsg, bitCampaign)
      SELECT v_numDomainId, v_numContactId, v_bitOpenActionItem, v_vcOpenActionMsg, v_bitOpenCases, v_vcOpenCasesMsg, v_bitOpenProject, v_vcOpenProjectMsg, v_bitOpenSalesOpp, v_vcOpenSalesOppMsg, v_bitOpenPurchaseOpp, v_vcOpenPurchaseOppMsg, v_bitBalancedue, v_monBalancedue, v_vcBalancedueMsg,v_bitUnreadEmail, v_intUnreadEmail, v_vcUnreadEmailMsg, v_bitPurchasedPast, v_monPurchasedPast, v_vcPurchasedPastMsg, v_bitSoldPast, v_monSoldPast, v_vcSoldPastMsg, v_bitCustomField, v_numCustomFieldId, v_vcCustomFieldValue, v_vcCustomFieldMsg, v_bitCampaign;
      v_numAlertConfigId := CURRVAL('dbo.AlertConfig_seq');
   end if; 
 
/****** Object:  StoredProcedure [dbo].[USP_ManageAlerts]    Script Date: 07/26/2008 16:19:33 ******/
   RETURN;
END; $$;


