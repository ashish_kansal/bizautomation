-- Stored procedure definition script USP_GetContactList2 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetContactList2(                                                             
--                                                                    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                    
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                                    
 v_tintUserRightType SMALLINT DEFAULT 0,                                                                    
 v_tintSortOrder SMALLINT DEFAULT 4,                                                                    
 v_SortChar CHAR(1) DEFAULT '0',                                                                   
 v_FirstName VARCHAR(100) DEFAULT '',                                                                  
 v_LastName VARCHAR(100) DEFAULT '',                                                                  
 v_CustName VARCHAR(100) DEFAULT '',                                                                  
 v_CurrentPage INTEGER DEFAULT NULL,                                                                  
v_PageSize INTEGER DEFAULT NULL,                                                                  
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                  
v_columnName VARCHAR(50) DEFAULT NULL,                                                                  
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                                  
v_numDivisionID NUMERIC(9,0) DEFAULT 0,                              
v_bitPartner BOOLEAN DEFAULT NULL ,                                   
v_inttype NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_join  VARCHAR(400);  
   v_firstRec  INTEGER;                                                                  
   v_lastRec  INTEGER;                                                                  
   v_strSql  VARCHAR(8000);                                                            
   v_tintOrder  SMALLINT;                                        
   v_vcFormFieldName  VARCHAR(50);                                        
   v_vcListItemType  VARCHAR(1);                                   
   v_vcListItemType1  VARCHAR(1);                                       
   v_vcAssociatedControlType  VARCHAR(10);                                        
   v_numListID  NUMERIC(9,0);                                        
   v_vcDbColumnName  VARCHAR(20);            
   v_WhereCondition  VARCHAR(2000);             
   v_Table  VARCHAR(2000);      
   v_bitCustom  BOOLEAN;  
   v_numFormFieldId  NUMERIC;  
          
   v_Nocolumns  SMALLINT;      
   v_DefaultNocolumns  SMALLINT;   
   v_strColumns  VARCHAR(2000);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_join := ''; 
        
   if v_columnName ilike 'Cust%' then

      v_join := ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= ' || replace(v_columnName,'Cust','') || ' ';
      v_columnName := 'CFW.Fld_Value';
   end if;                         
   if v_columnName ilike 'DCust%' then

      v_join := ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= ' || replace(v_columnName,'DCust','');
      v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  ';
      v_columnName := 'LstCF.vcData';
   end if;                                                              
                                                                  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                   
                                                                  
                                                                  
   v_strSql := 'with tblcontacts as (Select ';                                                          
   if (v_tintSortOrder = 5 or v_tintSortOrder = 6) then 
      v_strSql := coalesce(v_strSql,'') || ' top 20 ';
   end if;                                                           
                                                                    
   v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber, ADC.numContactId                                                              
  FROM AdditionalContactsInformation ADC                                                 
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'  || coalesce(v_join,'') ||  '                                                  
  left join ListDetails LD on LD.numListItemID=C.numCompanyRating                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus';                           
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=true and CA.bitDeleted=false                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   end if;                                                   
                                                    
   if v_tintSortOrder = 7 then 
      v_strSql := coalesce(v_strSql,'') || ' join Favorites F on F.numContactid=ADC.numContactID ';
   end if;                                                     
                                       
   v_strSql := coalesce(v_strSql,'') || '                                                
        where DM.tintCRMType<>0                              
    and DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';             
   if v_inttype <> 0 then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numContactType  = ' || SUBSTR(CAST(v_inttype AS VARCHAR(10)),1,10);
   end if;            
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                             
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                              
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and c.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                                  
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And ADC.vcFirstName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                                           
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or (CA.bitShareportal=true and  CA.bitDeleted=false)' else '' end;
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;                                                           
   if v_numDivisionID <> 0 then  
      v_strSql := coalesce(v_strSql,'') || ' And DM.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(10)),1,10);
   end if;      
                                                                         
   if v_tintSortOrder = 2 then  
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || '  AND ADC.numContactType=92  ';
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND ADC.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-7 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 5
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numCreatedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numModifiedby=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and F.numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                                                             
    
   if (v_tintSortOrder = 5 and v_columnName != 'ADC.bintcreateddate') then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF (v_tintSortOrder = 6 and v_columnName != 'ADC.bintcreateddate')
   then  
      v_strSql := 'select * from (' || coalesce(v_strSql,'') || ')X order by ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 7
   then  
      v_strSql := coalesce(v_strSql,'') || ' and cType=''C'' ';
   end if;      
                                                                
    
   v_strSql := coalesce(v_strSql,'') || ')';      
                                                                 
   v_tintOrder := 0;                                        
   v_WhereCondition := '';       
         
   v_Nocolumns := 0;      
   select   coalesce(count(*),0) INTO v_Nocolumns from InitialListColumnConf A where A.numFormId = 10 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID  and numtype = v_inttype;          
   v_DefaultNocolumns :=  v_Nocolumns;    
   v_strColumns := '';      
   while v_DefaultNocolumns > 0 LOOP
      v_strColumns := coalesce(v_strColumns,'') || ',null';
      v_DefaultNocolumns := v_DefaultNocolumns -1;
   END LOOP;   
     
      
   v_strSql := coalesce(v_strSql,'') || ' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber';      
    
   if v_Nocolumns > 0 then

      select   tintOrder+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, a.bitCustom, a.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from InitialListColumnConf a
      left join DynamicFormFieldMaster D
      on D.numFormFieldId = a.numFormFieldId where a.numFormId = 10 and numUserCntID = v_numUserCntID and a.numDomainID = v_numDomainID  and numtype = v_inttype   order by tintOrder asc LIMIT 1;
   else
      select   coalesce(Count(*),0) INTO v_DefaultNocolumns from  DynamicFormFieldMaster D where D.numFormId = 10   and bitDefault = true;
      while v_DefaultNocolumns > 0 LOOP
         RAISE NOTICE '%',v_Nocolumns;
         v_strColumns := coalesce(v_strColumns,'') || ',null';
         v_DefaultNocolumns := v_DefaultNocolumns -1;
      END LOOP;
      select   "Order"+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, 0, D.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from  DynamicFormFieldMaster D where D.numFormId = 10   and bitDefault = true   order by "Order" asc LIMIT 1;
   end if;    
   v_DefaultNocolumns :=  v_Nocolumns;     
   while v_tintOrder > 0 LOOP
      if v_bitCustom = false then
	
         if v_vcAssociatedControlType = 'SelectBox' then
        
            if v_vcListItemType = 'L' then
			  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
			  
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
			  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
               if v_Table = 'Contacts' then
                  v_Prefix := 'ADC.';
               end if;
               if v_Table = 'Division' then
                  v_Prefix := 'DM.';
               end if;
               v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            end if;
         else 
            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'year(getutcdate())-year(bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
         end if;
      else
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_Id AS VARCHAR(10)),1,10) INTO v_vcFormFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master join CFW_Fld_Dtl
         on Fld_id = numFieldId
         left join CFw_Grp_Master
         on subgrp = CFw_Grp_Master.Grp_id where CFW_Fld_Master.Grp_id = 4 and numRelation = v_inttype and CFW_Fld_Master.numDomainID = v_numDomainID  and CFW_Fld_Master.Fld_id = v_numFormFieldId;
         RAISE NOTICE '%',v_vcAssociatedControlType;
         if v_vcAssociatedControlType <> 'Drop Down list Box' then
			
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         end if;
         if v_vcAssociatedControlType = 'Drop Down list Box' then
			
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' 
					on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFormFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         end if;
      end if;
      if v_DefaultNocolumns > 0 then
 
         select   tintOrder+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, a.bitCustom, a.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
         v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from InitialListColumnConf a
         join DynamicFormFieldMaster D
         on D.numFormFieldId = a.numFormFieldId where a.numFormId = 10 and numUserCntID = v_numUserCntID and a.numDomainID = v_numDomainID  and numtype = v_inttype and
         tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      else
         select   "Order"+1, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, 0, D.numFormFieldId INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
         v_vcListItemType,v_numListID,v_Table,v_bitCustom,v_numFormFieldId from DynamicFormFieldMaster D where D.numFormId = 10 and bitDefault = true  and
         "Order" > v_tintOrder -1   order by "Order" asc LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      end if;
   END LOOP;             
          
          
                  
   v_strSql := coalesce(v_strSql,'') || ' FROM AdditionalContactsInformation ADC                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId                                        
  left JOIN ContactAddress CA on CA.numContactID=ADC.numContactID ' || coalesce(v_WhereCondition,'') ||
   ' join tblcontacts T on T.numContactId=ADC.numContactId                                                                         
  WHERE RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) ||
   ' union select count(*),null,null,null,null,null ' || coalesce(v_strColumns,'') || ' from tblcontacts order by RowNumber';                                 
                                     
   RAISE NOTICE '%',v_strSql;              
      
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


