-- Function definition script fn_SurveyMatrixAns for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION fn_SurveyMatrixAns(v_numSurId NUMERIC,
      v_numQuestionID NUMERIC,
      v_numRespondantID NUMERIC,
      v_numAnsID NUMERIC)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listStr  TEXT;
BEGIN
   select   COALESCE(coalesce(v_listStr,'') || ',','') || sam.vcAnsLabel INTO v_listStr FROM
   SurveyMatrixAnsMaster sam
   INNER JOIN
   SurveyResponse sr
   ON
   sr.numSurID = sam.numSurID WHERE
   sr.numParentSurID = sam.numSurID
   AND sr.numSurID = v_numSurId
   AND sr.numQuestionID = sam.numQID
   AND sr.numMatrixID = sam.numMatrixID
   AND sr.numRespondantID = v_numRespondantID
   AND sr.numQuestionID = v_numQuestionID
   AND sam.numQID = v_numQuestionID
   AND sr.numAnsID = v_numAnsID;          
                       
   RETURN v_listStr;
END; $$;

