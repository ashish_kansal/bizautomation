-- Stored procedure definition script USP_GetCOARelationship for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCOARelationship(v_numCOARelationshipID NUMERIC(9,0) DEFAULT 0,
    v_numRelationshipID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF v_numDivisionID > 0 then
        
      select   C.numCompanyType INTO v_numRelationshipID FROM    CompanyInfo C
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = C.numCompanyId
      INNER JOIN Listdetails LD ON C.numCompanyType = LD.numListItemID WHERE   numDivisionID = v_numDivisionID;
   end if;

   open SWV_RefCur for SELECT  cast(R.numCOARelationshipID as VARCHAR(255)),
			cast(numRelationshipID as VARCHAR(255)),
            cast(numRelationshipID as VARCHAR(255)),
            cast(coalesce(numARParentAcntTypeID,0) as VARCHAR(255)) AS numARParentAcntTypeID,
            cast(coalesce(numAPParentAcntTypeID,0) as VARCHAR(255)) AS numAPParentAcntTypeID,
--            [numARAccountId],
--            [numAPAccountId],
			cast(coalesce(A.numAccountId,0) as NUMERIC(18,0)) AS numARAccountId,
			cast(coalesce(B.numAccountId,0) as NUMERIC(18,0)) AS numAPAccountId,
            cast(R.numDomainID as VARCHAR(255)),
            cast(dtCreateDate as VARCHAR(255)),
            cast(dtModifiedDate as VARCHAR(255)),
            cast(numCreatedBy as VARCHAR(255)),
            cast(numModifiedBy as VARCHAR(255)),
            cast(coalesce(A.vcAccountName,'') as VARCHAR(100)) AS ARAccountName,
            cast(coalesce(B.vcAccountName,'') as VARCHAR(100)) AS APAccountName
   FROM    COARelationships R
   LEFT OUTER JOIN Chart_Of_Accounts A ON A.numAccountId = R.numARAccountId
   LEFT OUTER JOIN Chart_Of_Accounts B ON B.numAccountId = R.numAPAccountId
   WHERE   (numCOARelationshipID = v_numCOARelationshipID
   OR v_numCOARelationshipID = 0)
   AND (numRelationshipID = v_numRelationshipID
   OR v_numRelationshipID = 0)
   AND R.numDomainID = v_numDomainID;
END; $$;













