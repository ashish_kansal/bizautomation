-- Stored procedure definition script usp_AssignProjectStageDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_AssignProjectStageDate(v_numDomainid NUMERIC DEFAULT 0,
    v_numProjectID NUMERIC DEFAULT 0 ,
    v_numOppID NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_minROWNUMBER  INTEGER;
   v_maxROWNUMBER  INTEGER;
   v_intDueDays  INTEGER;
   v_EndDate  TIMESTAMP;
   v_numStageDetailId  NUMERIC(9,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT  MAX(intDueDays) AS intDueDays,SD.numStageDetailId,ROW_NUMBER() OVER(order by MAX(intDueDays)) AS ROWNUMBER

      FROM StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID = SP.numStageDetailsId
      WHERE SP.numdomainid = v_numDomainid AND numProjectid = v_numProjectID and numOppid = v_numOppID GROUP BY SD.numStageDetailId
      ORDER BY MAX(intDueDays);

--SELECT * FROM #TempTable T 

   select   MIN(ROWNUMBER), MAX(ROWNUMBER) INTO v_minROWNUMBER,v_maxROWNUMBER FROM tt_TEMPTABLE;

   WHILE  v_minROWNUMBER <= v_maxROWNUMBER LOOP
      select   intDueDays, numStageDetailId INTO v_intDueDays,v_numStageDetailId FROM tt_TEMPTABLE WHERE ROWNUMBER = v_minROWNUMBER;
      select   MAX(dtEndDate) INTO v_EndDate FROM StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID = SP.numStageDetailsId where SP.numdomainid = v_numDomainid AND numProjectid = v_numProjectID and numOppid = v_numOppID AND SD.numStageDetailId = v_numStageDetailId AND SP.intDueDays = v_intDueDays;
      UPDATE StagePercentageDetails SET dtStartDate = v_EndDate+INTERVAL '1 day',dtEndDate = v_EndDate+intDueDays
      WHERE numStageDetailsId = v_numStageDetailId AND numdomainid = v_numDomainid AND numProjectid = v_numProjectID and numOppid = v_numOppID;
      select   MIN(ROWNUMBER) INTO v_minROWNUMBER FROM  tt_TEMPTABLE WHERE  ROWNUMBER > v_minROWNUMBER;
   END LOOP;	

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


