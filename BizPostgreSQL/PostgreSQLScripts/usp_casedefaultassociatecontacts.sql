-- Stored procedure definition script usp_CaseDefaultAssociateContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CaseDefaultAssociateContacts(v_numCaseId NUMERIC(9,0) DEFAULT 0 ,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecordOwnerRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numRecordAssigneeRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numRecordOrganizationContactsRoleId  NUMERIC(18,0) DEFAULT 0;
   v_numROwnerContactID  NUMERIC(18,0) DEFAULT 0;
   v_numRAssigneContactID  NUMERIC(18,0) DEFAULT 0;
   v_numROrgContactID  NUMERIC(18,0) DEFAULT 0;
--Record Owner
BEGIN
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Record Owner') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Record Owner');
	
      v_numRecordOwnerRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordOwnerRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Record Owner'    LIMIT 1;
   end if;
--Assignee
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Assignee') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Assignee');
	
      v_numRecordAssigneeRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordAssigneeRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Assignee'    LIMIT 1;
   end if;
--Organization Contact
   IF((SELECT COUNT(*) FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Organization Contact') = 0) then

      INSERT INTO Listdetails(numListID,numDomainid,vcData)VALUES(26,v_numDomainID,'Organization Contact');
	
      v_numRecordOrganizationContactsRoleId := CURRVAL('ListDetails_seq');
   ELSE
      select   numListItemID INTO v_numRecordOrganizationContactsRoleId FROM Listdetails WHERE numListID = 26  AND numDomainid = v_numDomainID AND vcData = 'Organization Contact'    LIMIT 1;
   end if;

   IF((SELECT COUNT(*) FROM CaseContacts WHERE numCaseID = v_numCaseId AND numRole = v_numRecordOwnerRoleId) = 0) then
      SELECT  numRecOwner INTO v_numROwnerContactID FROM Cases WHERE numCaseId = v_numCaseId     LIMIT 1;
      IF(coalesce(v_numROwnerContactID,0) > 0) then
	
         insert into CaseContacts(numCaseID, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numCaseId,v_numROwnerContactID,v_numRecordOwnerRoleId,false,false);
      end if;
   end if;
   IF((SELECT COUNT(*) FROM CaseContacts WHERE numCaseID = v_numCaseId AND numRole = v_numRecordAssigneeRoleId) = 0) then
      SELECT  numAssignedBy INTO v_numRAssigneContactID FROM Cases WHERE numCaseId = v_numCaseId     LIMIT 1;
      IF(coalesce(v_numRAssigneContactID,0) > 0) then
	
         insert into CaseContacts(numCaseID, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numCaseId,v_numRAssigneContactID,v_numRecordAssigneeRoleId,false,false);
      end if;
   end if;
   IF((SELECT COUNT(*) FROM CaseContacts WHERE numCaseID = v_numCaseId AND numRole = v_numRecordOrganizationContactsRoleId) = 0) then
      SELECT  numContactId INTO v_numROrgContactID FROM Cases WHERE numCaseId = v_numCaseId     LIMIT 1;
      IF(coalesce(v_numROrgContactID,0) > 0) then
	
         insert into CaseContacts(numCaseID, numContactID, numRole, bitPartner,bitSubscribedEmailAlert)
	values(v_numCaseId,v_numROrgContactID,v_numRecordOrganizationContactsRoleId,false,false);
      end if;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_CaseDetailsdtlPL]    Script Date: 07/26/2008 16:15:03 ******/



