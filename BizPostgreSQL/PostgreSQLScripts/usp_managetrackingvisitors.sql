DROP FUNCTION IF EXISTS USP_ManageTrackingVisitors;

CREATE OR REPLACE FUNCTION USP_ManageTrackingVisitors(v_vcUserHostAddress VARCHAR(100) DEFAULT '',
v_vcUserDomain VARCHAR(100) DEFAULT '',
v_vcUserAgent VARCHAR(1000) DEFAULT '',
v_vcBrowser VARCHAR(50) DEFAULT '',
v_vcCrawler VARCHAR(50) DEFAULT '',
v_vcURL VARCHAR(500) DEFAULT '',
v_vcReferrer VARCHAR(500) DEFAULT '',
v_numVisits NUMERIC(9,0) DEFAULT 0,
v_vcOrginalURL VARCHAR(500) DEFAULT '',                      
v_vcOrginalRef VARCHAR(500) DEFAULT '',                      
v_numVistedPages NUMERIC(9,0) DEFAULT 0,                      
v_vcTotalTime VARCHAR(100) DEFAULT '',                      
v_strURL TEXT DEFAULT '',                               
v_vcCountry VARCHAR(50) DEFAULT NULL,                
v_IsVisited BOOLEAN DEFAULT NULL,
v_numDivisionID NUMERIC(9,0) DEFAULT NULL  ,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_vcCampaign VARCHAR(500) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	v_numIdentity  NUMERIC;           
	v_vcTime  VARCHAR(50);           

	/*Check for online campaign existance*/   
	v_numCampaignID  NUMERIC(9,0);
	v_hDoc  INTEGER;
	v_startPos  INTEGER;          
	v_EndPos  INTEGER;          
	v_vcSearchTerm  VARCHAR(100);          
          
                           
	v_sec1  INTEGER;          
	v_min1  INTEGER;           
	v_hour1  INTEGER;          
	v_sec2  INTEGER;          
	v_min2  INTEGER;          
	v_hour2  INTEGER;          
          
                           
	v_vcTempOrigReferer  TEXT;
	v_numTempDomainID  NUMERIC(18,0);
	v_vcHow  NUMERIC(18,0);

	v_i INT;
	v_iCount INT;
	v_URL VARCHAR(500);
	v_ElapsedTime VARCHAR(100);
	v_Time TIMESTAMP;
	v_numTracVisitorsDTLID NUMERIC(18,0);
BEGIN
   v_vcOrginalURL := replace(replace(replace(replace(coalesce(v_vcOrginalURL,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&');                      
   v_vcOrginalRef := replace(replace(replace(replace(coalesce(v_vcOrginalRef,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&');  
   v_vcURL := replace(replace(replace(replace(coalesce(v_vcURL,'-'),'%3A',':'),'%3F','?'),'%3D',
   '='),'%26','&');                      
   v_vcReferrer := replace(replace(replace(replace(coalesce(v_vcReferrer,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&');
           
   IF v_numDivisionID > 0 then
	
      select   numCampaignID INTO v_numCampaignID FROM
      CampaignMaster WHERE
      vcCampaignCode = v_vcCampaign
      AND numDomainID = v_numDomainId
      AND bitIsOnline = true    LIMIT 1;
   end if;          
          
          


   select   COUNT(*) INTO v_IsVisited FROM
   TrackingVisitorsHDR WHERE
   vcUserHostAddress = v_vcUserHostAddress
   AND dtcreated BETWEEN TIMEZONE('UTC',now()) AND TIMEZONE('UTC',now())+INTERVAL '1 day'
   AND numDomainID = v_numDomainId;


   IF v_numDivisionID > 0 then
		
			/*Check if previous visit had already created Division- if yes then update old record else create new record for TrackingVisitorsHDR */          
      IF(SELECT
      COUNT(*)
      FROM
      TrackingVisitorsHDR
      WHERE
      numDomainID = v_numDomainId
      AND coalesce(numDivisionID,0) = v_numDivisionID) > 0 then
			
         v_IsVisited := true;
      end if;
   end if;
       
       
   RAISE NOTICE '%',v_IsVisited;
   if v_IsVisited = false then
      v_startPos := POSITION('Referrer' IN v_vcURL);
      if v_startPos > 0 then
		 
         v_vcReferrer := SUBSTR(v_vcURL,v_startPos+9,500);
      end if;
      v_startPos := POSITION('&q=' IN v_vcReferrer);
      if v_startPos > 0 then
		 
         v_vcSearchTerm := SUBSTR(v_vcReferrer,v_startPos+1,150);
         v_EndPos := POSITION('&' IN v_vcSearchTerm);
         if v_EndPos = 0 then 
            v_EndPos := 100;
         end if;
         v_vcSearchTerm := SUBSTR(v_vcSearchTerm,3,v_EndPos -3);
      end if;
      v_startPos := POSITION('&p=' IN v_vcReferrer);
      if v_startPos > 0 then
		 
         v_vcSearchTerm := SUBSTR(v_vcReferrer,v_startPos+1,150);
         v_EndPos := POSITION('&' IN v_vcSearchTerm);
         if v_EndPos = 0 then 
            v_EndPos := 100;
         end if;
         v_vcSearchTerm := SUBSTR(v_vcSearchTerm,3,v_EndPos -3);
      end if;
      v_startPos := POSITION('?q=' IN v_vcReferrer);
      if v_startPos > 0 then
		 
         v_vcSearchTerm := SUBSTR(v_vcReferrer,v_startPos+1,150);
         v_EndPos := POSITION('&' IN v_vcSearchTerm);
         if v_EndPos = 0 then 
            v_EndPos := 100;
         end if;
         v_vcSearchTerm := SUBSTR(v_vcSearchTerm,3,v_EndPos -3);
      end if;
      v_startPos := POSITION('?p=' IN v_vcReferrer);
      if v_startPos > 0 then
		 
         v_vcSearchTerm := SUBSTR(v_vcReferrer,v_startPos+1,150);
         v_EndPos := POSITION('&' IN v_vcSearchTerm);
         if v_EndPos = 0 then 
            v_EndPos := 100;
         end if;
         v_vcSearchTerm := SUBSTR(v_vcSearchTerm,3,v_EndPos -3);
      end if;
      v_vcSearchTerm := replace(v_vcSearchTerm,'+',' ');
      if (v_vcUserDomain ilike '%inktomisearch.com' or v_vcUserDomain ilike '%ask.com'  or v_vcUserDomain ilike  '%yahoo.net' or v_vcUserDomain ilike  '%chello.nl' or v_vcUserDomain ilike  '%linksmanager.com' or v_vcUserDomain ilike  '%googlebot.com') then 
         v_vcCrawler := 'True';
      end if;
      INSERT  INTO TrackingVisitorsHDR(vcUserHostAddress,
					  vcUserDomain,
					  vcUserAgent,
					  vcBrowser,
					  vcCrawler,
					  vcURL,
					  vcReferrer,
					  vcSearchTerm,
					  numVisits,
					  vcOrginalURL,
					  vcOrginalRef,
					  numVistedPages,
					  vcTotalTime,
					  dtcreated,
					  vcCountry,
					  numDivisionID,
					  numDomainID,
					  numCampaignID,
					  vcDomainName)
			VALUES(v_vcUserHostAddress,
					  v_vcUserDomain,
					  v_vcUserAgent,
					  v_vcBrowser,
					  v_vcCrawler,
					  v_vcURL,
					  v_vcReferrer,
					  v_vcSearchTerm,
					  v_numVisits,
					  v_vcOrginalURL,
					  v_vcOrginalRef,
					  v_numVistedPages,
					  v_vcTotalTime,
					  TIMEZONE('UTC',now()),
					  v_vcCountry,
					  v_numDivisionID,
					  v_numDomainId,
					  v_numCampaignID,
					  GetDomainNameFromURL(v_vcOrginalURL)) RETURNING numtrackingid INTO v_numIdentity;     
   else      
	         
	      
--set @vcTotalTime=convert(varchar,@hour1)+':'+convert(varchar,@min1)+':'+convert(varchar,@sec1)          
--print @vcTotalTime          

      select   numTrackingID, vcTotalTime INTO v_numIdentity,v_vcTime from TrackingVisitorsHDR where vcUserHostAddress = v_vcUserHostAddress   order by numTrackingID desc LIMIT 1;
      v_sec1 := (CASE WHEN SUBSTR(v_vcTime,7,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTime,7,2) AS INTEGER) END);
      v_min1 := (CASE WHEN SUBSTR(v_vcTime,4,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTime,4,2) AS INTEGER) END);
      v_hour1 := (CASE WHEN SUBSTR(v_vcTime,1,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTime,1,2) AS INTEGER) END);
      v_sec2 := (CASE WHEN SUBSTR(v_vcTotalTime,7,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTotalTime,7,2) AS INTEGER) END);
      v_min2 := (CASE WHEN SUBSTR(v_vcTotalTime,4,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTotalTime,4,2) AS INTEGER) END);
      v_hour2 := (CASE WHEN SUBSTR(v_vcTotalTime,1,2) = '' THEN 0 ELSE CAST(SUBSTR(v_vcTotalTime,1,2) AS INTEGER) END);      
	--print @sec1          
	--print @min1          
	--print @hour1          
	--print @sec2          
	--print @min2          
	--print @hour2          
	          
      v_sec1 := v_sec1::bigint+v_sec2::bigint;
      if v_sec1 >= 60 then
	 
         v_sec1 := v_sec1::bigint -60;
         v_min1 := v_min1::bigint+1;
      end if;
      v_min1 := v_min1::bigint+v_min2::bigint;
      if v_min1 >= 60 then
	 
         v_min1 := v_min1::bigint -60;
         v_hour1 := v_hour1::bigint+1;
      end if;
      v_hour1 := v_hour1::bigint+v_hour2::bigint;          
	--print @sec1          
	--print @min1          
	--print @hour1      
      if LENGTH(v_hour1:: TEXT) = 1 then 
         v_vcTotalTime := '0' || SUBSTR(CAST(v_hour1 AS VARCHAR(30)),1,30) || ':';
      else 
         v_vcTotalTime := SUBSTR(CAST(v_hour1 AS VARCHAR(30)),1,30) || ':';
      end if;
      if LENGTH(v_min1:: TEXT) = 1 then  
         v_vcTotalTime := coalesce(v_vcTotalTime,'') || '0' || SUBSTR(CAST(v_min1 AS VARCHAR(30)),1,30) || ':';
      else 
         v_vcTotalTime := coalesce(v_vcTotalTime,'') || SUBSTR(CAST(v_min1 AS VARCHAR(30)),1,30) || ':';
      end if;
      if LENGTH(v_sec1:: TEXT) = 1 then 
         v_vcTotalTime := coalesce(v_vcTotalTime,'') || '0' || SUBSTR(CAST(v_sec1 AS VARCHAR(30)),1,30);
      else 
         v_vcTotalTime := coalesce(v_vcTotalTime,'') || SUBSTR(CAST(v_sec1 AS VARCHAR(30)),1,30);
      end if;
   end if;          
          
	DROP TABLE IF EXISTS TEMP_VisitedPages;
	CREATE TEMPORARY TABLE TEMP_VisitedPages(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
		URL VARCHAR(500),
		ElapsedTime VARCHAR(100),
		Time TIMESTAMP
	);

	INSERT INTO TEMP_VisitedPages
	(
		URL,
		ElapsedTime,
		Time
	)
	SELECT
		URL,
		ElapsedTime,
		Time
	FROM
		XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strURL AS XML)
			COLUMNS
				id FOR ORDINALITY,
				URL VARCHAR(500) PATH 'URL',
				ElapsedTime VARCHAR(100) PATH 'ElapsedTime',
				Time TIMESTAMP PATH 'Time'
		) AS X;

	v_i := 1;
	SELECT COUNT(*) INTO v_iCount FROM TEMP_VisitedPages;

	while v_i <= v_iCount loop
		SELECT 
			URL,
			ElapsedTime,
			Time
		INTO
			v_URL,
			v_ElapsedTime,
			v_Time
		FROM 
			TEMP_VisitedPages 
		WHERE 
			ID = v_i;

		INSERT INTO TrackingVisitorsDTL
		(
			numTracVisitorsHDRID,vcPageName,vcElapsedTime,numPageID,dtTime
		)
		VALUES
		(
			v_numIdentity,v_URL,v_ElapsedTime,(select numPageID from PageMasterWebAnlys where v_URL ilike '%' || vcPageURL || '%'),v_Time 
		)
		RETURNING
			numTracVisitorsDTLID
		INTO
			v_numTracVisitorsDTLID;

		IF NOT EXISTS (SELECT numWFQueueID FROM WorkFlowQueue INNER JOIN TrackingVisitorsDTL ON WorkFlowQueue.numRecordID= TrackingVisitorsDTL.numTracVisitorsDTLID WHERE WorkFlowQueue.numDomainID=v_numDomainID AND LOWER(TrackingVisitorsDTL.vcPageName)=LOWER(v_URL) AND CAST(WorkFlowQueue.dtCreatedDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE)) THEN
			PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainId,v_numUserCntID := 0,v_numRecordID := v_numTracVisitorsDTLID,v_numFormID := 148,v_tintProcessStatus := 1::SMALLINT,v_tintWFTriggerOn := 1::SMALLINT,v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := '');
		END IF;
		
		v_i := v_i + 1;
   end loop;


	DROP TABLE TEMP_VisitedPages;

	
                          
   update TrackingVisitorsHDR set vcTotalTime = v_vcTotalTime,numVistedPages =(select count(*) from TrackingVisitorsDTL where numTracVisitorsHDRID = v_numIdentity)  where  numTrackingID = v_numIdentity;                      
        
   if v_numDivisionID > 0 then
      update TrackingVisitorsHDR set numDivisionID = v_numDivisionID  where  numTrackingID = v_numIdentity;
      select(CASE WHEN coalesce(vcOrginalRef,'-') = '-' THEN coalesce(vcOrginalURL,'-') ELSE vcOrginalRef END) INTO v_vcTempOrigReferer FROM TrackingVisitorsHDR WHERE numTrackingID = v_numIdentity;
      select   numDomainID INTO v_numTempDomainID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      IF EXISTS(SELECT CompanyInfo.numCompanyId FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = v_numDivisionID AND coalesce(vcHow,0) = 0) then
	
         IF EXISTS(SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDomainID = v_numTempDomainID AND coalesce(numListItemID,0) > 0 AND POSITION(vcOrginalRef IN v_vcTempOrigReferer) > 0) then
            select   numListItemID INTO v_vcHow FROM
            TrackingVisitorsHDR WHERE
            numDomainID = v_numTempDomainID
            AND coalesce(numListItemID,0) > 0
            AND v_vcTempOrigReferer ilike CONCAT(vcOrginalRef,'%')   ORDER BY
            LENGTH(coalesce(vcOrginalRef,'')) DESC LIMIT 1;
            UPDATE
            CompanyInfo C
            SET
            vcHow = v_vcHow
            FROM
            DivisionMaster D
            WHERE
            D.numCompanyID = C.numCompanyId AND numDivisionID = v_numDivisionID;
         end if;
      end if;
   end if;


/*In Order to increase performace for campaign report we need to insert filtered data into seperate table-chintan*/
   IF v_numCampaignID > 0 AND v_numIdentity > 0 AND v_numDivisionID > 0 then
	
	-- making TrackingCampaignData table obsolete .. by directly updating data on division
      UPDATE DivisionMaster SET numCampaignID = v_numCampaignID WHERE numDivisionID = v_numDivisionID;
   end if;
   RETURN;
END; $$;


