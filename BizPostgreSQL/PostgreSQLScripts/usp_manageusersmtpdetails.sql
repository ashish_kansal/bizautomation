-- Stored procedure definition script USP_ManageUserSMTPDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageUserSMTPDetails(v_numUserId NUMERIC(18,0),
	v_numDomainId NUMERIC(18,0),
    v_vcSMTPUserName VARCHAR(50),
    v_vcSMTPPassword VARCHAR(100),
    v_vcSMTPServer VARCHAR(100),
    v_numSMTPPort NUMERIC(18,0),
    v_bitSMTPSSL BOOLEAN,
    v_bitSMTPServer BOOLEAN,
    v_bitSMTPAuth BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT 1 FROM SMTPUserDetails WHERE numDomainID = v_numDomainId AND numUserId = v_numUserId) then
	
      INSERT INTO SMTPUserDetails(numDomainID, numUserId, vcSMTPUserName, vcSMTPPassword, vcSMTPServer, numSMTPPort, bitSMTPSSL, bitSMTPServer, bitSMTPAuth)
      SELECT v_numDomainId, v_numUserId, v_vcSMTPUserName, v_vcSMTPPassword, v_vcSMTPServer, v_numSMTPPort, v_bitSMTPSSL, v_bitSMTPServer, v_bitSMTPAuth;
   ELSE
      UPDATE SMTPUserDetails
      SET    numDomainID = v_numDomainId,numUserId = v_numUserId,vcSMTPUserName = v_vcSMTPUserName, 
      vcSMTPPassword = v_vcSMTPPassword,vcSMTPServer = v_vcSMTPServer, 
      numSMTPPort = v_numSMTPPort,bitSMTPSSL = v_bitSMTPSSL,bitSMTPServer = v_bitSMTPServer, 
      bitSMTPAuth = v_bitSMTPAuth
      WHERE  numDomainID = v_numDomainId
      AND numUserId = v_numUserId;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/



