-- Stored procedure definition script Recurrence_SelTs for PostgreSQL
CREATE OR REPLACE FUNCTION Recurrence_SelTs(v_RecurrenceID		INTEGER		-- primary key number of the recurrence
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   _ts
   FROM
   Recurrence
   WHERE
   Recurrence.recurrenceid = v_RecurrenceID;
END; $$;













