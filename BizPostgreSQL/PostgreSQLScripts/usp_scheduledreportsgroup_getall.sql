-- Stored procedure definition script USP_ScheduledReportsGroup_GetAll for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_GetAll(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintMode SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   ID 
		,coalesce(vcName,'') AS "vcName"
		,CONCAT((CASE tintFrequency WHEN 3 THEN 'Monthly' WHEN 2 THEN 'Weekly' ELSE 'Daily' END),' at ',EXTRACT(HOUR FROM dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)),SUBSTR(TO_CHAR(dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
   'Mon dd yyyy hh:miAM'),18,3),' starting ',
   TO_CHAR(dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
   'Day'),' ',FormatedDateFromDate(dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)) AS "vcSchedule"
		,numEmailTemplate AS "numEmailTemplate"
		,coalesce(GenericDocuments.VcDocName,'') AS "vcEmailTemplate"
		,coalesce(vcRecipientsEmail,'') AS "vcRecipients"
   FROM
   ScheduledReportsGroup
   LEFT JOIN
   GenericDocuments
   ON
   ScheduledReportsGroup.numEmailTemplate = GenericDocuments.numGenericDocID
   WHERE
   ScheduledReportsGroup.numDomainID = v_numDomainID
   AND 1 =(CASE WHEN v_tintMode = 1 THEN(CASE WHEN coalesce((SELECT COUNT(*) FROM ScheduledReportsGroupReports SRGR WHERE SRGR.numSRGID = ScheduledReportsGroup.ID),0) < 5 THEN 1 ELSE 0 END) ELSE 1 END)
   ORDER BY
   vcName;
END; $$;












