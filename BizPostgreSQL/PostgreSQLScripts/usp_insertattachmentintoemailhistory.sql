-- Stored procedure definition script USP_InsertAttachmentIntoEmailHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertAttachmentIntoEmailHistory(v_numEmailHstrID NUMERIC(9,0) DEFAULT 0, 
v_fileName VARCHAR(500) DEFAULT '',
v_vcLocation VARCHAR(1000) DEFAULT '' ,
v_vcFileSize VARCHAR(200) DEFAULT '',
v_vcFileType VARCHAR(200) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcLocation,vcAttachmentType,vcSize)
values(v_numEmailHstrID,v_fileName,v_vcLocation,v_vcFileType,v_vcFileSize);
RETURN;
END; $$;


