DROP FUNCTION IF EXISTS USP_ExportDataBackUp;

CREATE OR REPLACE FUNCTION USP_ExportDataBackUp(v_numDomainID NUMERIC(9,0),
    v_tintTable SMALLINT,
	v_dtFromDate TIMESTAMP,
	v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CustomFields  VARCHAR(1000);
   v_vcSQL  VARCHAR(8000);
BEGIN
   IF v_tintTable = 1 then
            
      open SWV_RefCur for
      SELECT  CMP.numCompanyId AS CompanyID,
                        CMP.vcCompanyName AS CompanyName,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = DM.numStatusID) AS CompanyStatus,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numCompanyRating) AS CompanyRating,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numCompanyType) AS CompanyType,
                        CASE WHEN coalesce(DM.bitPublicFlag,false) = false THEN 'False'
      ELSE 'True'
      END AS IsPrivate,
                        DM.numDivisionID AS DivisionID,
                        DM.vcDivisionName AS DivisionName,
                        coalesce(AD1.vcStreet,'') AS BillStreet,
                        coalesce(AD1.vcCity,'') AS BillCity,
                        coalesce(AD1.vcPostalCode,'') AS BillPostCode,
                        fn_GetListName(AD1.numCountry,0::BOOLEAN) AS BillCountry,
                        coalesce(fn_GetState(AD1.numState),'') AS BillState,
                        coalesce(AD2.vcStreet,'') AS ShipStreet,
                        coalesce(AD2.vcCity,'') AS ShipCity,
                        coalesce(AD2.vcPostalCode,'') AS ShipPostCode,
                        fn_GetListName(AD2.numCountry,0::BOOLEAN) AS ShipCountry,
                        coalesce(fn_GetState(AD2.numState),'') AS ShipState,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = numFollowUpStatus) AS FollowUpStatus,
                        CMP.vcWebLabel1 AS WebLabel1,
                        CMP.vcWebLink1 AS WebLink1,
                        CMP.vcWebLabel2 AS WebLabel2,
                        CMP.vcWebLink2 AS WebLink2,
                        CMP.vcWebLabel3 AS WebLabel3,
                        CMP.vcWebLink3 AS WebLink3,
                        CMP.vcWeblabel4 AS Weblabel4,
                        CMP.vcWebLink4 AS WebLink4,
                        CASE WHEN coalesce(tintBillingTerms,0) = 0 THEN 'False'
      ELSE 'True'
      END AS IsBillingTerms,
                        (SELECT numNetDueInDays FROM BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS BillingDays,
                        (SELECT vcTerms FROM BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS BillingDaysName,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = DM.numTerID) AS Territory,
--                        DM.numTerID TerritoryID,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numCompanyIndustry) AS CompanyIndustry,
--                        CMP.numCompanyIndustry,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numCompanyCredit) AS CompanyCredit,
--                        CMP.numCompanyCredit,
                        coalesce(CMP.txtComments,'') AS Comments,
                        coalesce(CMP.vcWebSite,'') AS WebSite,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numAnnualRevID) AS AnnualRevenue,
--                        CMP.numAnnualRevID,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.numNoOfEmployeesId) AS NoOfEmployees,
--                        numNoOfEmployeesID,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.vcProfile) AS Profile,
--                        CMP.vcProfile,
                        DM.numCreatedBy AS CreatedBy,
                        fn_GetContactName(DM.numRecOwner) AS RecordOwner,
                        DM.numRecOwner AS RecordOwnerID,
                        DM.numCreatedBy AS CreatedBy,
                        DM.bintCreatedDate AS CreateDate,
--                        DM.numRecOwner RecordOwner,
                        DM.numModifiedBy AS ModifiedBy,
                        coalesce(vcComPhone,'') AS CompanyPhone,
                        vcComFax AS CompanyFax,
--                        DM.numGrpID,
                        (SELECT    vcGrpName
         FROM      Groups
         WHERE     numGrpId = DM.numGrpId) AS GroupName,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = CMP.vcHow) AS InformationSource,
--                        CMP.vcHow AS vcInfoSource,
                        CASE DM.tintCRMType
      WHEN 0 THEN 'Leads'
      WHEN 1 THEN 'Prospects'
      WHEN 2 THEN 'Accounts'
      ELSE 'NA'
      END AS RelationshipType,
                        (SELECT    vcCampaignName
         FROM      CampaignMaster
         WHERE     numCampaignID = DM.numCampaignID) AS CampaignName,
                        numCampaignID AS CampaignID,
                        DM.numAssignedBy AS AssignedByID,
                        coalesce(ACI2.vcFirstName,'') || ' ' || coalesce(ACI2.vcLastname,'') AS AssignedBy,
                        CASE WHEN coalesce(bitNoTax,false) = false THEN 'False'
      ELSE 'True'
      END AS IsSalesTax,
--                        ( SELECT    A.vcFirstName + ' ' + A.vcLastName
--                          FROM      AdditionalContactsInformation A
--                                    LEFT JOIN DivisionMaster D ON D.numDivisionID = A.numDivisionID
--                          WHERE     A.numContactID = D.numAssignedTo AND D.numDomainId=cmp.numDomainID and cmp.numCompanyId=D.numCompanyId
--                        ) AS AssignedName,
						coalesce(ACI1.vcFirstName,'') || ' ' || coalesce(ACI1.vcLastname,'') AS AssignedTo,
                        DM.numAssignedTo AS AssignedToID,
                        ACI.numContactId AS ContactID,
                        ACI.vcFirstName AS FirstName,
                        ACI.vcLastname AS LastName,
                        ACI.vcEmail AS ContactEmailAddress,
                        ACI.vcAltEmail AS AlternetEmailAddress,
                        ACI.numPhone AS ContactPhone,
                        ACI.numPhoneExtension AS ContactPhoneExt,
                        L8.vcData AS ContactStatus,
                        L9.vcData AS ContactType,
                        ACI.vcGivenName AS GivenName,
--                        ACI.numTeam,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = ACI.numTeam) AS Team,
                        ACI.vcFax AS FaxNo,
                        CASE coalesce(ACI.charSex,'')
      WHEN 'M' THEN 'Male'
      WHEN 'F' THEN 'Female'
      ELSE ''
      END AS Gender,
                        ACI.bintDOB AS DateOfBirth,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = ACI.vcPosition) AS "Position",
                        ACI.txtNotes AS Notes,
                        ACI.numCell AS CellNo,
                        ACI.numHomePhone AS HomePhone,
                        ACI.VcAsstFirstName AS AsstFirstName,
                        ACI.vcAsstLastName AS AsstLastName,
                        ACI.numAsstPhone AS AsstPhone,
                        ACI.numAsstExtn AS AsstExtn,
                        ACI.vcAsstEmail AS AsstEmail,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = ACI.vcDepartment) AS Department,
                        AD.vcStreet AS PrimaryStreet,
                        AD.vcCity AS PrimaryCity,
                        fn_GetState(AD.numState) AS PrimaryState,
                        fn_GetListName(AD.numCountry,0::BOOLEAN) AS PrimaryCountry,
                        AD.vcPostalCode AS PrimaryPotalCode,
--                        AddC.vcContactLocation,
--                        AddC.vcStreet,
--                        AddC.vcCity,
--                        AddC.vcState,
--                        AddC.intPostalCode,
--                        AddC.vcCountry,
                        ACI.numManagerID AS ManagersContactID,
                        ACI.vcCategory,
                        ACI.vcTitle,
                        CASE WHEN coalesce(ACI.bitOptOut,false) = false THEN 'False'
      ELSE 'True'
      END AS ContactEmailOptOut,
                         CASE WHEN coalesce(ACI.bitPrimaryContact,false) = false THEN 'False'
      ELSE 'True'
      END AS PrimaryContact,
                        C.vcCurrencyDesc AS DefaultTradingCurrency,
                        IACR.numNonBizCompanyID AS NonBizCompanyID,
                        IAI.numNonBizContactID AS NonBizContactID
      FROM    CompanyInfo CMP
      JOIN DivisionMaster DM ON DM.numCompanyID = CMP.numCompanyId
      LEFT OUTER JOIN AdditionalContactsInformation ACI ON DM.numDivisionID = ACI.numDivisionId
      LEFT JOIN AdditionalContactsInformation ACI1 ON ACI1.numContactId = DM.numAssignedTo
      LEFT JOIN AdditionalContactsInformation ACI2 ON ACI2.numContactId = DM.numAssignedBy
      LEFT JOIN Listdetails L8 ON L8.numListItemID = ACI.numEmpStatus
      LEFT JOIN Listdetails L9 ON L9.numListItemID = ACI.numContactType
      LEFT JOIN AddressDetails AD ON AD.numRecordID = ACI.numContactId AND AD.tintAddressOf = 1 AND AD.tintAddressType = 0 AND AD.bitIsPrimary = true
      LEFT JOIN AddressDetails AD1 ON AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN AddressDetails AD2 ON AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID AND DM.numDomainID = C.numDomainId
      LEFT JOIN ImportActionItemReference IAI ON IAI.numContactID  = ACI.numContactId
      LEFT JOIN ImportOrganizationContactReference IACR ON IACR.numDivisionID = DM.numDivisionID
      WHERE   DM.numDomainID = v_numDomainID
      ORDER BY DM.numCompanyID;   
                
                --Get field name 
				-- 14 for leads,13 for Accounts,12 for Prospects

      open SWV_RefCur2 for
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 1
      UNION
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 12
      UNION
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 13
      UNION
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 14;
				
                --Get values
      open SWV_RefCur3 for
      SELECT  FldDTLID,
                        CF.Fld_ID,
                        CM.FLd_label,
                        CASE WHEN coalesce(CM.numlistid,0) > 0
      THEN fn_GetCustFldStringValue(CF.Fld_ID,CF.RecId,CF.Fld_Value)
      ELSE CF.Fld_Value
      END AS Fld_Value,
                        RecId
      FROM    CFW_FLD_Values CF
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = CF.RecId
      INNER JOIN CFW_Fld_Master CM ON CM.Fld_id = CF.Fld_ID
      WHERE   DM.numDomainID = v_numDomainID
      AND CM.numDomainID = v_numDomainID;
   end if;
            
   IF v_tintTable = 2 then
            
      open SWV_RefCur for
      SELECT  Opp.numOppId AS OpportunityID,
                        (SELECT    vcFirstName || ' ' || vcLastname
         FROM      AdditionalContactsInformation
         WHERE     numContactId = Opp.numContactId) AS ContactName,
                        Opp.txtComments AS Comments,
                        Opp.numDivisionId AS DivisionID,
                        CASE tintopptype
      WHEN 1 THEN 'Sales'
      WHEN 2 THEN 'Purchase'
      ELSE 'NA'
      END AS OpportunityType,
--                        coalesce(bintAccountClosingDate, '') AS bintAccountClosingDate,
                        Opp.numContactId AS ContactID,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = Opp.tintSource) AS OpportunitySource,
                        C2.vcCompanyName AS CompanyName, 
--                        D2.tintCRMType,
                        Opp.vcpOppName AS OpportunityName,
--                        intpEstimatedCloseDate,
                        Opp.monDealAmount AS CalculatedAmount,
--                        lngPConclAnalysis,
                        monPAmount AS OppAmount,
--                        numSalesOrPurType,
                        CASE WHEN coalesce(Opp.tintActive,0) = 0 THEN 'False'
      ELSE 'True'
      END AS IsActive,
                        Opp.numCreatedBy AS Createdby,
                        Opp.numModifiedBy AS ModifiedBy,
                        fn_GetContactName(Opp.numrecowner) AS RecordOwner,
                        Opp.numrecowner AS RecordOwnerID,
                        CASE WHEN coalesce(tintshipped,0) = 0 THEN 'False'
      ELSE 'True'
      END AS IsShipped,
--                        coalesce(tintOppStatus, 0) AS tintOppStatus,
                        CASE coalesce(tintoppstatus,0)
      WHEN 0 THEN 'Open'
      WHEN 1 THEN 'Closed-Won'
      WHEN 2 THEN 'Closed-Lost'
      END AS OpportunityStatus,
                        Opp.numassignedto AS AssignedTo,
                        Opp.numAssignedBy AS AssignedBy,
                        (SELECT    vcData
         FROM      Listdetails
         WHERE     numListItemID = D2.numTerID) AS Territory,
                        coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
                        Opp.bintCreatedDate AS CreatedDate,
                        Opp.bintAccountClosingDate AS ClosingDate,
                        Opp.intPEstimatedCloseDate AS DueDate,
                        fn_GetContactAddedToOpportunity(numOppId) AS AssociatedContactIds
      FROM    OpportunityMaster Opp
      JOIN DivisionMaster D2 ON Opp.numDivisionId = D2.numDivisionID
      LEFT JOIN CompanyInfo C2 ON C2.numCompanyId = D2.numCompanyID
      LEFT JOIN Currency C ON C.numCurrencyID = Opp.numCurrencyID
      WHERE   Opp.numDomainId = v_numDomainID
      ORDER BY Opp.numDivisionId;
      open SWV_RefCur2 for
      SELECT  OI.numoppitemtCode AS OppItemID,
                        OI.numOppId AS OpportunityID,
                        OI.numItemCode AS ItemID,
                        OI.numUnitHour AS Quantity,
                        OI.monPrice AS Price,
                        OI.monTotAmount AS TotalAmount,
                        OI.vcItemDesc AS ItemDescription,
                        CASE OI.vcType
      WHEN 'P' THEN 'Inventory Item'
      WHEN 'S' THEN 'Service'
      WHEN 'N' THEN 'Non-Inventory Item'
      WHEN 'A' THEN 'Asset Item'
      ELSE OI.vcType
      END AS ItemType,
                        OI.vcAttributes AS Attributes,
                        OI.bitDropShip AS IsDropship,
                        OI.monTotAmtBefDiscount AS TotalAmtBeforeDiscount
      FROM    OpportunityMaster OM
      INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
      WHERE   OM.numDomainId = v_numDomainID;
                
				--Get field name 
      open SWV_RefCur3 for
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id IN(2,6); -- 2 for Sales Records, 6 for Purchase records
				
                --Get values
      open SWV_RefCur4 for
      SELECT  FldDTLID,
                        CF.Fld_ID,
                        CM.FLd_label,
                        CASE WHEN coalesce(CM.numlistid,0) > 0
      THEN fn_GetCustFldStringValue(CF.Fld_ID,CF.RecId,CF.Fld_Value)
      ELSE CF.Fld_Value
      END AS Fld_Value,
                        RecId
      FROM    CFW_Fld_Values_Opp CF
      INNER JOIN OpportunityMaster OM ON OM.numOppId = CF.RecId
      INNER JOIN CFW_Fld_Master CM ON CM.Fld_id = CF.Fld_ID
      WHERE   OM.numDomainId = v_numDomainID
      AND CM.numDomainID = v_numDomainID;
   end if;
   IF v_tintTable = 3 then
            
      open SWV_RefCur for
      SELECT  pro.numProId AS ProjID,
                        pro.vcProjectName AS ProjectName,
                        pro.vcProjectID AS ProjectID,
                        pro.numDivisionId AS DivisionID,
                        pro.numIntPrjMgr AS IntPrjMgrID,
                        fn_GetContactName(pro.numIntPrjMgr) AS InternalProjectMgr,
                        fn_GetContactName(pro.numCustPrjMgr) AS CustomerPrjectMgr,
                        pro.numCustPrjMgr AS CustPrjMgrID,
                        pro.txtComments AS Comments,
                        fn_GetContactName(pro.numAssignedTo) AS AssignedTo,
                        pro.numAssignedTo AS AssignedToID,
                        pro.intDueDate AS DueDate,
                        pro.bintProClosingDate AS ClosingDate,
                        pro.bintCreatedDate AS CreatDate
      FROM    ProjectsMaster pro
      WHERE   pro.numdomainId = v_numDomainID; 
	   		--Get field name 
      open SWV_RefCur2 for
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 11; -- 11 for projects
				
                --Get values
      open SWV_RefCur3 for
      SELECT  FldDTLID,
                        CF.Fld_ID,
                        CM.FLd_label,
                        CASE WHEN coalesce(CM.numlistid,0) > 0
      THEN fn_GetCustFldStringValue(CF.Fld_ID,CF.RecId,CF.Fld_Value)
      ELSE CF.Fld_Value
      END AS Fld_Value,
                        RecId
      FROM    CFW_Fld_Values_Pro CF
      INNER JOIN ProjectsMaster PM ON PM.numProId = CF.RecId
      INNER JOIN CFW_Fld_Master CM ON CM.Fld_id = CF.Fld_ID
      WHERE   PM.numdomainId = v_numDomainID
      AND CM.numDomainID = v_numDomainID;
   end if;
   IF v_tintTable = 4 then
            
      open SWV_RefCur for
      SELECT  C.numCaseId AS CaseID,
                        C.numContactId AS ContactID,
                        vcCaseNumber AS CaseNumber,
                        intTargetResolveDate AS ResolveDate,
                        textSubject AS Subject,
                        Div.numDivisionID AS DivisionID,
                        GetListIemName(numStatus) AS Status,
                        GetListIemName(numPriority) AS Priority,
                        textDesc AS CaseDescription,
--                        tintCRMType ,
                        textInternalComments AS InternalComments,
                        GetListIemName(numReason) AS Reason,
                        C.numContractID AS ContractID,
                        GetListIemName(numOrigin) AS CaseOrigin,
                        GetListIemName(numType) AS CaseType,
                        C.bintCreatedDate AS CreateDate,
                        C.numCreatedby AS CreatedBy,
                        C.numModifiedBy AS ModifiedBy,
                        C.bintModifiedDate AS ModifiedDate,
                        C.numRecOwner AS RecordOwnerID,
--                        tintSupportKeyType,
                        C.numAssignedTo AS AssignedTo,
                        C.numAssignedBy AS AssignedBy,
                        coalesce(Addc.vcFirstName,'') || ' '
      || coalesce(Addc.vcLastname,'') AS ContactName,
                        coalesce(vcEmail,'') AS ContactEmail,
                        coalesce(numPhone,'')
      || CASE WHEN NULLIF(numPhoneExtension,'') IS NULL THEN ''
      WHEN numPhoneExtension = '' THEN ''
      ELSE ', ' || numPhoneExtension
      END AS Phone,
                        Com.numCompanyId AS CompanyID,
                        Com.vcCompanyName AS CompanyName
      FROM    Cases C
      LEFT JOIN AdditionalContactsInformation Addc ON Addc.numContactId = C.numContactId
      JOIN DivisionMaster Div ON Div.numDivisionID = Addc.numDivisionId
      JOIN CompanyInfo Com ON Com.numCompanyId = Div.numCompanyID
      WHERE   C.numDomainID = v_numDomainID;
            
            --Get field name 
      open SWV_RefCur2 for
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = v_numDomainID
      AND CFW_Fld_Master.Grp_id = 3; -- 3 for cases
				
                --Get values
      open SWV_RefCur3 for
      SELECT  FldDTLID,
                        CF.Fld_ID,
                        CM.FLd_label,
                        CASE WHEN coalesce(CM.numlistid,0) > 0
      THEN fn_GetCustFldStringValue(CF.Fld_ID,CF.RecId,CF.Fld_Value)
      ELSE CF.Fld_Value
      END AS Fld_Value,
                        RecId
      FROM    CFW_FLD_Values_Case CF
      INNER JOIN Cases C ON C.numCaseId = CF.RecId
      INNER JOIN CFW_Fld_Master CM ON CM.Fld_id = CF.Fld_ID
      WHERE   C.numDomainID = v_numDomainID
      AND CM.numDomainID = v_numDomainID;
   end if;
   IF v_tintTable = 5 then
        
      open SWV_RefCur for
      SELECT  numSolnID AS SolutionID,
                    GetListIemName(numCategory) AS Category,
                    vcSolnTitle AS SolutionName,
                    txtSolution AS SolutionDescription,
                    vcLink AS URL
      FROM    SolutionMaster
      WHERE   numDomainID = v_numDomainID;
   end if;
   IF v_tintTable = 6 then
        
      open SWV_RefCur for
      SELECT  numContractId AS ContractID,
                    numDivisionID AS DivisionID,
                    vcContractName AS ContractName,
                    bintStartDate AS StatDate,
                    bintExpDate AS ExpirationDate,
                    numincidents AS NoOfIncidents,
                    numHours AS NoOfHours,
                    numEmailDays AS EmailDays,
                    numEmailIncidents AS NoOfEmailIncidents,
                    numEmailHours AS EmailHours,
                    numUserCntID AS ContactID,
                    vcNotes AS Notes,
                    bintcreatedOn AS CreateDate,
                    numAmount AS Amount,
                    bitAmount AS IsAmount,
                    bitIncidents AS IsIncidents,
                    bitDays AS IsDays,
                    bitHour AS IsHour,
                    bitEDays AS IsEMailDays,
                    bitEIncidents AS IsEmailIncedents,
                    bitEHour,
                    numTemplateId AS EmailTemplateID,
                    decrate,
                    fn_GetContactAddedToContract(numdomainId,numContractId) AS ContactsAddedToContract
      FROM    ContractManagement
      WHERE   numdomainId = v_numDomainID;
   end if;
   IF v_tintTable = 7 then
        
      open SWV_RefCur for
      SELECT 
      datEntry_Date AS JournalEntryDate,
					TransactionType,
					CompanyName,
                    varDescription AS Description,
					BizDocID AS BizDocName,
					numOppBizDocsId AS BizDocID,
					numDebitAmt AS DebitAmount,
                    numCreditAmt AS CreditAmount,
                    TranRef AS TransactinoReferece,
                    TranDesc AS TransactionDescription,
					BizPayment,
					CheqNo AS CheckNo
      FROM    VIEW_GENERALLEDGER
      WHERE   numDomainId = v_numDomainID
      AND VIEW_GENERALLEDGER.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate;
   end if;
   IF v_tintTable = 8 then
      select string_agg(COALESCE(v_CustomFields,'') || '"' || FLd_label || '"',',') INTO v_CustomFields FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 5;
				-- Removes last , from string
      IF OCTET_LENGTH(v_CustomFields) > 0 then
         v_CustomFields := SUBSTR(v_CustomFields,1,LENGTH(v_CustomFields) -1);
      end if;
      v_vcSQL := CONCAT('SELECT  
										I.numItemCode "Item ID",
										vcItemName "Item Name",
										coalesce(txtItemDesc, '''') "Item Description",
										CASE charItemType
										  WHEN ''P'' THEN ''Inventory Item''
										  WHEN ''S'' THEN ''Service''
										  WHEN ''N'' THEN ''Non-Inventory Item''
										  WHEN ''A'' THEN ''Asset Item''
										  ELSE charItemType
										END "Item Type",
										vcModelID "Model ID",
										(select vcItemGroup from ItemGroups where numItemGroupID=numItemGroup and numDomainId=',v_numDomainID,') as "Item Group",
										vcSKU "SKU(NI)",
										GetListIemName(numItemClassification) "Item Classification",
										COALESCE((SELECT string_agg(DISTINCT vcCategoryName,'', '') FROM ItemCategory IC INNER JOIN CATEGORY CT ON IC.numCategoryID=CT.numCategoryId AND CT.numDomainID=',v_numDomainID,' and IC.numItemId=i.numItemCode),'''') as "Item Category",
										coalesce(fltWeight, 0) AS Weight,
										coalesce(fltHeight, 0) AS Height,
										coalesce(fltWidth, 0) AS Width,
										coalesce(fltLength, 0) AS Length,
										(select vcUnitName from UOM where numUOMId=coalesce(I.numBaseUnit,0)) AS "Base Unit",
										(select vcUnitName from UOM where numUOMId=coalesce(I.numPurchaseUnit,0)) AS "Purchase Unit",
										(select vcUnitName from UOM where numUOMId=coalesce(I.numSaleUnit,0)) AS "Sale Unit",
										CASE charItemType WHEN ''P'' THEN coalesce(monListPrice,0) END AS "List Price (I)",
										CASE charItemType WHEN ''N'' THEN coalesce(monListPrice,0) END AS "List Price (NI)",
										coalesce(CAST(numBarCodeId AS VARCHAR(50)), '''') AS BarCodeId,
										coalesce(vcManufacturer,'''') as Manufacturer,
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where DM.numDivisionId=numVendorID) as Vendor,
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numCOGsChartAcntId and COA.numDomainID=',
      v_numDomainID,') as "COGs/Expense Account",
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numAssetChartAcntId and COA.numDomainID=',
      v_numDomainID,') as "Asset Account",
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numIncomeChartAcntId and COA.numDomainID=',v_numDomainID,') as "Income Account",
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where  
										DM.numDivisionId=CA.numDivId) as AssetOwner,
										coalesce(numAppvalue,0) as AppreciationValue,
										coalesce(numDepValue,0) as DepreciationValue,
										dtPurchase as PurchaseDate,
										dtWarrentyTill as WarranteeTill,
										(SELECT vcPathForImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=true AND II.bitIsImage = true LIMIT 1) ImagePath,
										(SELECT vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=true AND II.bitIsImage = true LIMIT 1) ThumbImagePath,
										CASE WHEN coalesce(bitTaxable, false) = false THEN ''False''
											 ELSE ''True''
										END "Taxable",
										CASE WHEN coalesce(bitKitParent, false) = false THEN ''False''
											 ELSE ''True''
										END "Is Kit",
										CASE WHEN coalesce(bitSerialized, false) = false THEN ''False''
											 ELSE ''True''
										END "Is Serialize",                    					
										monAverageCost "Average Cost",
										monCampaignLabourCost CampaignLabourCost,
										numCreatedBy CreatedBy,
										bintCreatedDate CreatedDate,
										numModifiedBy ModifiedBy,
										bintModifiedDate ModifiedDate,                    
										CASE WHEN coalesce(bitFreeShipping, false) = false THEN ''False''
											 ELSE ''True''
										END IsFreeShipping,
										CASE WHEN coalesce(bitAllowBackOrder, false) = false THEN ''False''
											 ELSE ''True''
										END "Allow Back Orders",
										CASE WHEN coalesce(bitCalAmtBasedonDepItems, false) = false
											 THEN ''False''
											 ELSE ''True''
										END CalAmtBasedonDepItems,
										CASE WHEN coalesce(bitAssembly, false) = false THEN ''False''
											 ELSE ''True''
										END "Is Assembly" , 
										CASE WHEN coalesce(IsArchieve, false) = false THEN ''False''
											 ELSE ''True''
										END "Archive",       
										(SELECT  coalesce(vcData, '''') FROM  ListDetails WHERE numDomainID = 1 AND numListItemID = numItemClass AND numListID = 381) AS "Item Class",
										(SELECT  coalesce(vcData, '''') FROM  ListDetails WHERE numDomainID = 1 AND numListItemID = numShipClass AND numListID = 461) AS "Shipping Class"
										' ||(CASE WHEN OCTET_LENGTH(v_CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) || '
								FROM    Item I  left outer join  (select * from CompanyAssets CA where CA.numDomainID=',
      v_numDomainID,') CA
										ON CA.numItemCode=I.numItemCode
									' ||(CASE WHEN OCTET_LENGTH(v_CustomFields) > 0 THEN CONCAT('
									LEFT JOIN 
									(
										SELECT
											* 
										FROM 
											CROSSTAB(''SELECT 
															t2.RecId
															,t1.fld_id
															,MAX(t2.Fld_Value) as Fld_Value 
														FROM 
															CFW_Fld_Master t1 
														LEFT JOIN 
															CFW_Fld_Values_Item AS t2 
														ON 
															t1.Fld_ID = t2.Fld_ID 
														WHERE 
															t1.numDomainID=',v_numDomainID,'
														group by 
															t1.fld_id,t2.RecId 
														order by 1'') AS TEMPCFW(RecId NUMERIC(18,0),',REPLACE(v_CustomFields,',',' TEXT, '),'" TEXT)) CustomFields ON I.numItemCode=CustomFields.RecId
									') ELSE '' END) || ' WHERE   I.numDomainiD = ',
      v_numDomainID);
      open SWV_RefCur for EXECUTE v_vcSQL;
      open SWV_RefCur2 for
      SELECT  numVendorID AS VendorID,
							vcPartNo AS "Vendor Part#",
							monCost AS "Vendor Cost",
							numItemCode AS "Item ID",
							intMinQty AS "Vendor Minimum Qty"
      FROM    Vendor
      WHERE   numDomainID = v_numDomainID;
			--Get field name 
      open SWV_RefCur3 for
      SELECT * FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.numDomainID = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
      AND CFW_Fld_Master.Grp_id = 5; -- 5 for Item details
				
                --Get values
      open SWV_RefCur4 for
      SELECT  FldDTLID,
                    CF.Fld_ID,
                    CM.FLd_label,
                    CASE WHEN coalesce(CM.numlistid,0) > 0
      THEN fn_GetCustFldStringValue(CF.Fld_ID,CF.RecId,CF.Fld_Value)
      ELSE CF.Fld_Value
      END AS Fld_Value,
                    RecId
      FROM    CFW_FLD_Values_Item CF
      INNER JOIN Item I ON I.numItemCode = CF.RecId
      INNER JOIN CFW_Fld_Master CM ON CM.Fld_id = CF.Fld_ID
      WHERE   I.numDomainID = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
      AND CM.Grp_id = 5
      AND CM.numDomainID = v_numDomainID;
   end if;
   IF v_tintTable = 9 then
        
      open SWV_RefCur for
      SELECT  c.numAccountId AS ChartOfAccountID,
                    AT.numAccountTypeID AS AccountTypeID,
                    c.numParntAcntTypeID AS ParentAcntTypeID,
                    c.vcAccountCode AS AccountCode,
                    c.vcAccountName AS AccountName,
                    AT.vcAccountType AS AccountType,
                    CASE WHEN c.numParntAcntTypeID IS NULL THEN NULL
      ELSE c.numOpeningBal
      END AS OpeningBal,
                    c.vcAccountDescription AS Description,
                    c.dtOpeningDate AS OpeningDate,
					CASE WHEN coalesce(c.bitFixed,false) = false THEN 'False'
      ELSE 'True'
      END AS IsFixed,
                    CASE WHEN coalesce(c.bitDepreciation,false) = false THEN 'False'
      ELSE 'True'
      END AS IsDepreciation,
                    CASE WHEN coalesce(c.bitOpeningBalanceEquity,false) = false THEN 'False'
      ELSE 'True'
      END AS IsOpeningBalanceEquity,
					c.monEndingOpeningBal AS EndingOpeningBalace,
					c.monEndingBal AS EndingBalance,
					c.dtEndStatementDate AS EndStatementDate,
					CASE WHEN coalesce(c.bitProfitLoss,false) = false THEN 'False'
      ELSE 'True'
      END AS IsProfitLoss,
                    c.vcNumber AS AccountNumber
					,CASE WHEN coalesce(c.bitIsSubAccount,false) = false THEN 'False'
      ELSE 'True'
      END AS IsSubAccount
					,CCC.vcAccountName AS ParentAccountName
					,CASE WHEN coalesce(c.IsBankAccount,false) = false THEN 'False'
      ELSE 'True'
      END AS IsBankAccount
					,c.vcStartingCheckNumber AS StartingCheckNumber
					,c.vcBankRountingNumber AS BankRountingNumber
      FROM    Chart_Of_Accounts c
      LEFT OUTER JOIN AccountTypeDetail AT ON AT.numAccountTypeID = c.numParntAcntTypeID
      LEFT JOIN Chart_Of_Accounts CCC ON c.numParentAccId = CCC.numAccountId
      WHERE   c.numDomainId = v_numDomainID
      ORDER BY c.numAccountId ASC;
   end if;
   RETURN;
END; $$;
    


