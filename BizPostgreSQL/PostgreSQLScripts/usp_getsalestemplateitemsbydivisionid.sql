-- Stored procedure definition script USP_GetSalesTemplateItemsByDivisionId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSalesTemplateItemsByDivisionId(v_numDivisionID NUMERIC,
    v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  X.*, (SELECT  vcCategoryName FROM Category WHERE numCategoryID IN(SELECT numCategoryID FROM ItemCategory IC WHERE X.numItemCode = IC.numItemID) LIMIT 1) AS vcCategoryName
   FROM(SELECT  DISTINCT
      STI.numSalesTemplateItemID,
                        STI.numSalesTemplateID,
                        STI.numSalesTemplateItemID AS numoppitemtCode,
						ST.vcTemplateName AS vcTemplateName,
                        STI.numItemCode,
                        STI.numUnitHour,
                        STI.monPrice,
                        STI.monTotAmount,
                        STI.numSourceID,
                        STI.numWarehouseID,
                        STI.Warehouse,
                        STI.numWarehouseItmsID,
                        STI.ItemType,
                        STI.ItemType as vcType,
                        STI.Attributes,
						STI.Attributes as vcAttributes,
                        STI.AttrValues,
                        I.bitFreeShipping AS FreeShipping, --Use from Item table
                        STI.Weight,
                        STI.Op_Flag,
                        STI.bitDiscountType,
                        STI.fltDiscount,
                        STI.monTotAmtBefDiscount,
                        STI.ItemURL,
                        STI.numDomainID,
                        I.txtItemDesc AS vcItemDesc,
                       (SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) AS vcPathForTImage,
                        I.vcItemName,
                        false AS DropShip,
					    false as bitDropShip,
                        I.bitTaxable,
                        coalesce(STI.numUOM,0) AS numUOM,
						coalesce(STI.numUOM,0) as numUOMId,
                        coalesce(STI.vcUOMName,'') AS vcUOMName,
                        coalesce(STI.UOMConversionFactor,1) AS UOMConversionFactor,
						coalesce(STI.numVendorWareHouse,0) as numVendorWareHouse,coalesce(STI.numShipmentMethod,0) as numShipmentMethod,coalesce(STI.numSOVendorId,0) as numSOVendorId,
						false as bitWorkOrder,I.charItemType,fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder
--                        ISNULL(dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numdomainid,0),1) UOMConversionFactor
      FROM      SalesTemplateItems STI
      LEFT JOIN OpportunitySalesTemplate AS ST ON STI.numSalesTemplateID = ST.numSalesTemplateID
      INNER JOIN Item I ON I.numItemCode = STI.numItemCode
      LEFT JOIN UOM U ON U.numUOMId = I.numSaleUnit
      WHERE     ST.numDivisionID = v_numDivisionID
      AND STI.numDomainID = v_numDomainID) X;
END; $$;
--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems












