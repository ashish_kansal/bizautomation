-- Stored procedure definition script USP_UpdateBroadHstrForETrack for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateBroadHstrForETrack(v_numBroadHstrID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update BroadCastDtls set numNoofTimes =(coalesce(numNoofTimes,0)+1) where numBroadcastID = v_numBroadHstrID and numContactID = v_numContactID;
   RETURN;
END; $$;


