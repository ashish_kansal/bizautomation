-- Stored procedure definition script USP_ManageCustomerCreditCardInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCustomerCreditCardInfo(v_numContactId   NUMERIC(18,0),
               v_vcCardHolder   VARCHAR(500),
               v_vcCreditCardNo VARCHAR(500),
               v_vcCVV2         VARCHAR(200),
               v_numCardTypeID   NUMERIC(9,0),
               v_tintValidMonth SMALLINT,
               v_intValidYear   INTEGER,
               v_numUserCntId   NUMERIC(18,0),
               v_numCCInfoID INTEGER DEFAULT 0,
               v_bitIsdefault BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitIsdefault <> false then
    
      UPDATE CustomerCreditCardInfo SET bitIsDefault = false
      WHERE numContactId = v_numUserCntId;
   end if;
   IF v_numCCInfoID = 0 then
    
      INSERT INTO CustomerCreditCardInfo(numContactId,
                    vcCardHolder,
                    vcCreditCardNo,
                    vcCVV2,
                    numCardTypeID,
                    tintValidMonth,
                    intValidYear,
                    numCreatedby,
                    dtCreated,
                    numModifiedby,
                    dtModified,bitIsDefault)
        VALUES(v_numContactId,
                    v_vcCardHolder,
                    v_vcCreditCardNo,
                    v_vcCVV2,
                    v_numCardTypeID,
                    v_tintValidMonth,
                    v_intValidYear,
                    v_numUserCntId,
                    TIMEZONE('UTC',now()),
                    v_numUserCntId,
                    TIMEZONE('UTC',now()),v_bitIsdefault);
   ELSE
      UPDATE CustomerCreditCardInfo SET vcCardHolder =  v_vcCardHolder,vcCreditCardNo = v_vcCreditCardNo,vcCVV2 = v_vcCVV2,
      tintValidMonth = v_tintValidMonth,intValidYear = v_intValidYear,numCardTypeID = v_numCardTypeID,
      bitIsDefault = v_bitIsdefault
      WHERE numCCInfoID = v_numCCInfoID;
   end if;
   RETURN;
END; $$;


