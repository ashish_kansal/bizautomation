-- Stored procedure definition script USP_ECommerceCreditCardTransactionLog_CallCompleted for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ECommerceCreditCardTransactionLog_CallCompleted(v_numID NUMERIC(18,0)
	,v_bitSuccess BOOLEAN
	,v_vcMessage TEXT
	,v_vcExceptionMessage TEXT
	,v_vcStackStrace TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   ECommerceCreditCardTransactionLog
   SET
   bitNSoftwareCallCompleted = true,bitSuccess = v_bitSuccess,vcMessage = v_vcMessage,
   vcExceptionMessage = v_vcExceptionMessage,vcStackStrace = v_vcStackStrace
   WHERE
   ID = v_numID;

   open SWV_RefCur for SELECT v_numID;
END; $$;












