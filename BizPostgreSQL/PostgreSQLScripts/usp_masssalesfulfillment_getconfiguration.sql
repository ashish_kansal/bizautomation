DROP FUNCTION IF EXISTS USP_MassSalesFulfillment_GetConfiguration;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetConfiguration(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDefaultSalesShippingDoc  NUMERIC(18,0);
BEGIN
	select 
		coalesce(numDefaultSalesShippingDoc,0) INTO v_numDefaultSalesShippingDoc 
	FROM 
		Domain 
	WHERE 
		numDomainId = v_numDomainID;

	IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) then
		open SWV_RefCur for
		SELECT
			coalesce(bitGroupByOrderForPick,false) AS "bitGroupByOrderForPick"
			,coalesce(bitGroupByOrderForShip,false) AS "bitGroupByOrderForShip"
			,coalesce(bitGroupByOrderForInvoice,false) AS "bitGroupByOrderForInvoice"
			,coalesce(bitGroupByOrderForPay,false) AS "bitGroupByOrderForPay"
			,coalesce(bitGroupByOrderForClose,false) AS "bitGroupByOrderForClose"
			,coalesce(tintInvoicingType,1) AS "tintInvoicingType"
			,coalesce(tintScanValue,1) AS "tintScanValue"
			,coalesce(tintPackingMode,1) AS "tintPackingMode"
			,v_numDefaultSalesShippingDoc AS "numDefaultSalesShippingDoc"
			,coalesce(tintPendingCloseFilter,1) AS "tintPendingCloseFilter"
			,coalesce(bitGeneratePickListByOrder,false) AS "bitGeneratePickListByOrder"
			,coalesce(bitPickWithoutInventoryCheck,false) AS "bitPickWithoutInventoryCheck"
			,coalesce(bitEnablePickListMapping,false) AS "bitEnablePickListMapping"
			,coalesce(bitEnableFulfillmentBizDocMapping,false) AS "bitEnableFulfillmentBizDocMapping"
			,coalesce(tintPackShipButtons,0) AS "tintPackShipButtons"
		FROM
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID = v_numDomainID;
	ELSE
		open SWV_RefCur for
		SELECT
			false AS "bitGroupByOrderForPick"
			,false AS "bitGroupByOrderForShip"
			,false AS "bitGroupByOrderForInvoice"
			,false AS "bitGroupByOrderForPay"
			,false AS "bitGroupByOrderForClose"
			,2 AS "tintInvoicingType"
			,1 AS "tintScanValue"
			,1 AS "tintPackingMode"
			,v_numDefaultSalesShippingDoc AS "numDefaultSalesShippingDoc"
			,1 AS "tintPendingCloseFilter"
			,false AS "bitGeneratePickListByOrder"
			,false AS "bitPickWithoutInventoryCheck"
			,false AS "bitEnablePickListMapping"
			,false AS "bitEnableFulfillmentBizDocMapping"
			,0 AS "tintPackShipButtons";
	end if;

	RETURN;
END; $$;


