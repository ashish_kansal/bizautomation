-- Stored procedure definition script USP_GetOperationBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOperationBudget(v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_tintType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMonth  SMALLINT;          
   v_str  VARCHAR(10);          
   v_lstrSQL  VARCHAR(800);          
   v_vc  VARCHAR(10);          
   v_len  NUMERIC(9,0);
   v_Date  TIMESTAMP;
   v_dtFiscalStDate  TIMESTAMP;          
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN
   v_lstrSQL := 'Select  ' || ''''' as numChartAcntId,' ||  ''''' as numParentAcntId, ' || ''''' as vcCategoryName,' ||  ''''' as TOC,';           
   v_numMonth := 1;    
   v_Date := TIMEZONE('UTC',now())+CAST(v_tintType || 'year' as interval);     
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      v_lstrSQL := coalesce(v_lstrSQL,'') || ' ' ||  '''0''' || ' as "' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || '",';
      v_numMonth := v_numMonth+1;
   END LOOP;          
             
   v_lstrSQL := coalesce(v_lstrSQL,'') ||  '''''' || ' as Total' || ',''''' || ' as  Comments';           
   RAISE NOTICE '%',v_lstrSQL;          
   OPEN SWV_RefCur FOR EXECUTE v_lstrSQL;
   RETURN;
END; $$;


