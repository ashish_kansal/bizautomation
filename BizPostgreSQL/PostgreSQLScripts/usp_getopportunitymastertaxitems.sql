CREATE OR REPLACE FUNCTION USP_GetOpportunityMasterTaxItems(v_numOppId NUMERIC(18,0) DEFAULT 0,
    v_numOppItemID NUMERIC(18,0) DEFAULT 0,
    v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
	
      open SWV_RefCur for
      SELECT * FROM OpportunityMasterTaxItems WHERE coalesce(numOppId,0) = v_numOppId;
   ELSEIF v_tintMode = 2
   then
	
      open SWV_RefCur for
      SELECT
      OITI.*
			,OMTI.fltPercentage
			,OMTI.tintTaxType
      FROM
      OpportunityItemsTaxItems OITI
      INNER JOIN
      OpportunityMasterTaxItems OMTI
      ON
      OMTI.numOppId = v_numOppId
      AND OITI.numTaxItemID = OMTI.numTaxItemID
      AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0)
      WHERE
      coalesce(OITI.numOppId,0) = v_numOppId
      AND coalesce(numOppItemID,0) = v_numOppItemID;
   end if;
   RETURN;
END; $$;
