DROP FUNCTION IF EXISTS USP_InLineEditDataSave;

CREATE OR REPLACE FUNCTION USP_InLineEditDataSave(v_numDomainID NUMERIC(18,0),
 v_numUserCntID NUMERIC(18,0),
 v_numFormFieldId NUMERIC(18,0),
 v_bitCustom BOOLEAN DEFAULT false,
 v_InlineEditValue VARCHAR(8000) DEFAULT NULL,
 v_numContactID NUMERIC(18,0) DEFAULT NULL,
 v_numDivisionID NUMERIC(18,0) DEFAULT NULL,
 v_numWOId NUMERIC(18,0) DEFAULT NULL,
 v_numProId NUMERIC(18,0) DEFAULT NULL,
 v_numOppId NUMERIC(18,0) DEFAULT NULL,
 v_numOppItemId NUMERIC(18,0) DEFAULT NULL,
 v_numRecId NUMERIC(18,0) DEFAULT NULL,
 v_vcPageName VARCHAR(100) DEFAULT NULL,
 v_numCaseId NUMERIC(18,0) DEFAULT NULL,
 v_numWODetailId NUMERIC(18,0) DEFAULT NULL,
 v_numCommID NUMERIC(18,0) DEFAULT NULL, 
 INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_strSql  VARCHAR(4000);
   v_vcDbColumnName  VARCHAR(100);
   v_vcOrigDbColumnName  VARCHAR(100);
   v_vcFieldDataType VARCHAR(10);
   v_vcLookBackTableName  VARCHAR(100);
   v_vcListItemType  VARCHAR(3);
   v_numListID  NUMERIC(9,0);
   v_vcAssociatedControlType  VARCHAR(50);
   v_tempAssignedTo  NUMERIC(9,0);           
   v_Value1  VARCHAR(18);
   v_Value2  VARCHAR(18);
   v_intExecuteDiv  INTEGER DEFAULT 0;
   v_numItemCode  NUMERIC(9,0);
   v_Date  TIMESTAMP;
   v_numECampaignID  NUMERIC(18,0);
   v_numFollow  VARCHAR(10);
   v_binAdded  VARCHAR(100);
   v_PreFollow  VARCHAR(10);
   
   v_bitPrimaryContactCheck  BOOLEAN;             
   v_numID  NUMERIC(9,0);
   v_numPickedQty  DOUBLE PRECISION DEFAULT 0;
   v_bitViolatePrice  BOOLEAN;
   v_intPendingApprovePending  INTEGER;
   v_tintCommitAllocation1  SMALLINT;
   v_tintOppStatus  SMALLINT;
   v_tintCRMType  SMALLINT;
				
   v_tintOppType  SMALLINT;
   v_numOppStatus  NUMERIC(18,0);
   v_numGroupID  NUMERIC;
   v_numCompanyID  NUMERIC(18,0);
   v_numExtranetID  NUMERIC(18,0);
   v_numTempOppItemID  NUMERIC(18,0);
   v_numTempItemCode  NUMERIC(18,0);
   v_numOldWarehouseItemID  NUMERIC(18,0);
   v_numNewWarehouseItemID  NUMERIC(18,0);
   v_tintCommitAllocation  SMALLINT;
   v_j  INTEGER DEFAULT 1;
   v_jCount  INTEGER;
   v_ErrMsg  VARCHAR(4000);
   v_ErrSeverity  INTEGER;
   v_dtDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_str  TEXT;
   SWV_RowCount INTEGER;
BEGIN
   v_tempAssignedTo := null;           
   IF  v_bitCustom = false then
      
	  select   vcDbColumnName, (CASE WHEN vcLookBackTableName = 'T' THEN 'Tickler' ELSE vcLookBackTableName END), vcListItemType, numListID, vcAssociatedControlType,vcOrigDbCOlumnName,vcFieldDataType
	  INTO v_vcDbColumnName,v_vcLookBackTableName,v_vcListItemType,v_numListID,v_vcAssociatedControlType,v_vcOrigDbColumnName ,v_vcFieldDataType
	  FROM DycFieldMaster 
	  WHERE numFieldID = v_numFormFieldId;
      
	  IF v_vcAssociatedControlType = 'DateField' AND  v_InlineEditValue = '' then
	
         v_InlineEditValue := null;
      end if;
      if v_vcLookBackTableName = 'DivisionMaster' then
	 
         IF v_vcDbColumnName = 'numFollowUpStatus' then --Add to FollowUpHistory
            IF(v_numCommID > 0 AND v_numDivisionID = 0) then
			
               SELECT  numDivisionID INTO v_numDivisionID FROM Communication WHERE numCommId = v_numCommID     LIMIT 1;
            end if;
            select count(*)  AS numFollowUpStatus INTO v_PreFollow from FollowUpHistory where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            
            select   numFollowUpStatus, bintModifiedDate INTO v_numFollow,v_binAdded from DivisionMaster where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
            if v_numFollow <> '0' and v_numFollow <> v_InlineEditValue then
			
               if cast(NULLIF(v_PreFollow,'') as INTEGER) <> 0 then
					
                  if cast(NULLIF(v_numFollow,'') as NUMERIC(18,0)) <>(select   numFollowUpstatus from FollowUpHistory where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID order by numFollowUpStatusID desc LIMIT 1) then
							
                     insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainID)
									values(CAST(v_numFollow AS NUMERIC(18,0)),v_numDivisionID,CAST(v_binAdded AS TIMESTAMP),v_numDomainID);
                  end if;
               else
                  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainID)
						 values(CAST(v_numFollow AS NUMERIC(18,0)),v_numDivisionID,CAST(v_binAdded AS TIMESTAMP),v_numDomainID);
               end if;
            end if;
            UPDATE  DivisionMaster SET  dtLastFollowUp = TIMEZONE('UTC',now()) where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
         end if;
         IF v_vcDbColumnName = 'numAssignedTo' then ---Updating if organization is assigned to someone
			
            select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo from DivisionMaster where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID;
            if (v_tempAssignedTo <> cast(NULLIF(v_InlineEditValue,'') as NUMERIC(9,0)) and  v_InlineEditValue <> '0') then
					
               update DivisionMaster set numAssignedTo = CAST(v_InlineEditValue AS NUMERIC(18,0)),numAssignedBy = v_numUserCntID where numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID;
               UPDATE CompanyInfo SET bintModifiedDate = TIMEZONE('UTC',now()) WHERE numCompanyId IN(SELECT numCompanyID
               FROM
               DivisionMaster
               WHERE
               numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID);
            ELSEIF (v_InlineEditValue = '0')
            then
					
               update DivisionMaster set numAssignedTo = 0,numAssignedBy = 0 where numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID;
               UPDATE CompanyInfo SET bintModifiedDate = TIMEZONE('UTC',now()) WHERE numCompanyId IN(SELECT numCompanyID
               FROM
               DivisionMaster
               WHERE
               numDivisionID = v_numDivisionID  and numDomainID = v_numDomainID);
            end if;
         end if;
         UPDATE CompanyInfo SET numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numCompanyId =(SELECT  numCompanyID FROM DivisionMaster where numDivisionID = v_numDivisionID and numDomainID = v_numDomainID LIMIT 1) and numDomainID = v_numDomainID;
         IF v_vcDbColumnName = 'vcPartnerCode' then
		
            IF(v_InlineEditValue <> '') then
			
               IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID <> v_numDivisionID and numDomainID = v_numDomainID AND vcPartnerCode = v_InlineEditValue) then
				
                  v_intExecuteDiv := 1;
                  v_InlineEditValue := '-#12';
               end if;
            end if;
         end if;
         IF v_intExecuteDiv = 0 then
		
            v_strSql := 'update DivisionMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy= ' || v_numUserCntID || ',bintModifiedDate=TIMEZONE(''UTC'',now()) where numDivisionID= ' || v_numDivisionID ||' and numDomainID= ' || v_numDomainID;
         end if;
      ELSEIF v_vcLookBackTableName = 'AdditionalContactsInformation'
      then
	 
			 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
         IF v_vcDbColumnName = 'bitPrimaryContact' AND v_InlineEditValue = 'True' then
            select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then 
               v_numID := 0;
            end if;
            WHILE v_numID > 0 LOOP
               IF v_bitPrimaryContactCheck = true then
                  UPDATE  AdditionalContactsInformation SET bitPrimaryContact = false WHERE numContactId = v_numID;
               end if;
               select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND numContactId > v_numID    LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               IF SWV_RowCount = 0 then 
                  v_numID := 0;
               end if;
            END LOOP;
         ELSEIF v_vcDbColumnName = 'bitPrimaryContact' AND v_InlineEditValue = 'False'
         then
				
            IF(SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND numContactId != v_numContactID AND coalesce(bitPrimaryContact,false) = true) = 0 then
               v_InlineEditValue := 'True';
            end if;
         ELSEIF v_vcDbColumnName = 'numECampaignID'
         then
				
            IF EXISTS(select 1 from AdditionalContactsInformation WHERE numContactId = v_numContactID AND (vcEmail IS NULL OR vcEmail = '')) then
					
               RAISE EXCEPTION 'EMAIL_ADDRESS_REQUIRED';
               RETURN;
            end if;
         end if;
         v_strSql := 'update AdditionalContactsInformation set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numDivisionID='||v_numDivisionID||' and numDomainID='||v_numDomainID||' and numContactId='||v_numContactID;
      ELSEIF v_vcLookBackTableName = 'CompanyInfo'
      then
	 
         UPDATE DivisionMaster SET numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID;
         v_strSql := 'update CompanyInfo set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID='||v_numDivisionID||' and numDomainID='||v_numDomainID||') and numDomainID='||v_numDomainID;
      ELSEIF v_vcLookBackTableName = 'WorkOrder'
      then
         IF v_numWODetailId > 0 AND v_vcDbColumnName <> 'numQtyItemsReq' then
		
            v_strSql := 'update WorkOrderDetails set ' || coalesce(v_vcDbColumnName,'') || '=$5 where numWOId='||v_numWOId||' and numWODetailId='||v_numDomainID1;
         ELSE
            v_strSql := 'update WorkOrder set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numWOId='||v_numWOId||' and numDomainID='||v_numDomainID;
            IF v_vcDbColumnName = 'numQtyItemsReq' then
               v_numPickedQty := coalesce((SELECT
               MAX(numPickedQty)
               FROM(SELECT
                  WorkOrderDetails.numWODetailId
													,CEIL(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig:: NUMERIC) AS numPickedQty
                  FROM
                  WorkOrderDetails
                  INNER JOIN
                  WorkOrderPickedItems
                  ON
                  WorkOrderDetails.numWODetailId = WorkOrderPickedItems.numWODetailId
                  WHERE
                  WorkOrderDetails.numWOId = v_numWOId
                  AND coalesce(WorkOrderDetails.numWarehouseItemID,0) > 0
                  GROUP BY
                  WorkOrderDetails.numWODetailId,WorkOrderDetails.numQtyItemsReq_Orig) TEMP),0);
               IF cast(NULLIF(v_InlineEditValue,'') as DOUBLE PRECISION) < coalesce((v_numPickedQty),0) then
				
                  RAISE EXCEPTION 'WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY';
                  RETURN;
               end if;
               IF EXISTS(SELECT
               SPDTTL.ID
               FROM
               StagePercentageDetailsTask SPDT
               INNER JOIN
               StagePercentageDetailsTaskTimeLog SPDTTL
               ON
               SPDT.numTaskId = SPDTTL.numTaskId
               WHERE
               SPDT.numDomainID = v_numDomainID
               AND SPDT.numWorkOrderId = v_numWOId) then
				
                  RAISE EXCEPTION 'TASKS_ARE_ALREADY_STARTED';
                  RETURN;
               end if;
               IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOId AND numWOStatus = 23184) then
				
                  RAISE EXCEPTION 'WORKORDR_COMPLETED';
                  RETURN;
               end if;
               select   numItemCode INTO v_numItemCode FROM WorkOrder WHERE numWOId = v_numWOId and numDomainId = v_numDomainID;
               PERFORM USP_UpdateWorkOrderDetailQuantity(v_numDomainID,v_numUserCntID,v_numItemCode,v_InlineEditValue,v_numWOId);
            end if;
         end if;
      ELSEIF v_vcLookBackTableName = 'ProjectsMaster'
      then
	 
         IF v_vcDbColumnName = 'numAssignedTo' then ---Updating if Project is assigned to someone 
			
            select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo from ProjectsMaster where numProId = v_numProId and numdomainId = v_numDomainID;
            if (v_tempAssignedTo <> cast(NULLIF(v_InlineEditValue,'') as NUMERIC(9,0)) and  v_InlineEditValue <> '0') then
					
               update ProjectsMaster set numAssignedTo = v_InlineEditValue::NUMERIC,numAssignedby = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numProId = v_numProId  and numdomainId = v_numDomainID;
            ELSEIF (v_InlineEditValue = '0')
            then
					
               update ProjectsMaster set numAssignedTo = 0,numAssignedby = 0,bintModifiedDate = TIMEZONE('UTC',now()) where numProId = v_numProId  and numdomainId = v_numDomainID;
            end if;
         ELSEIF v_vcDbColumnName = 'numProjectStatus'
         then  ---Updating All Statge to 100-- if Completed
			
            IF v_InlineEditValue = '27492' then
				
               UPDATE ProjectsMaster PM SET dtCompletionDate = TIMEZONE('UTC',now()),monTotalExpense = PIE.monExpense,
               monTotalIncome = PIE.monIncome,monTotalGrossProfit = PIE.monBalance,numProjectStatus = 27492
               FROM GetProjectIncomeExpense(v_numDomainID,v_numProId) PIE
               WHERE PM.numProId = v_numProId;
               PERFORM USP_ManageProjectCommission(v_numDomainID,v_numProId);
					 --  numeric(9, 0)
							 --  numeric(9, 0)
               Update StagePercentageDetails set tinProgressPercentage = 100,bitClose = true where numProjectid = v_numProId;
               PERFORM USP_GetProjectTotalProgress(v_numDomainID := v_numDomainID,v_numProId := v_numProId,v_tintMode := 1::SMALLINT);
            ELSE
               DELETE from BizDocComission WHERE numDomainId = v_numDomainID AND numProId = v_numProId AND numComissionID
               NOT IN(SELECT BC.numComissionID FROM BizDocComission BC JOIN PayrollTracking PT
               ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainID WHERE BC.numDomainId = v_numDomainID AND coalesce(BC.numProId,0) = v_numProId);
            end if;
         end if;
         v_strSql := 'update ProjectsMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numProId='||v_numProId||' and numDomainID='||v_numDomainID;
      ELSEIF v_vcLookBackTableName = 'OpportunityMaster'
      then
	 
         IF v_vcDbColumnName = 'numAssignedTo' then ---Updating if organization is assigned to someone  
			
            select   coalesce(numassignedto,0) INTO v_tempAssignedTo from OpportunityMaster where numOppId = v_numOppId and numDomainId = v_numDomainID;
            if (v_tempAssignedTo <> cast(NULLIF(v_InlineEditValue,'') as NUMERIC(9,0)) and  v_InlineEditValue <> '0') then
					
               update OpportunityMaster set numassignedto = CAST(v_InlineEditValue AS NUMERIC(18,0)),numAssignedBy = v_numUserCntID,
               bintModifiedDate = TIMEZONE('UTC',now()) where numOppId = v_numOppId  and numDomainId = v_numDomainID;
            ELSEIF (v_InlineEditValue = '0')
            then
					
               update OpportunityMaster set numassignedto = 0,numAssignedBy = 0,bintModifiedDate = TIMEZONE('UTC',now()) where numOppId = v_numOppId  and numDomainId = v_numDomainID;
            end if;
         ELSEIF v_vcDbColumnName = 'numPartner' OR v_vcDbColumnName = 'numPartenerSource'
         then
		 
            update OpportunityMaster set numPartner = CAST(v_InlineEditValue AS NUMERIC(18,0)),bintModifiedDate = TIMEZONE('UTC',now()) where numOppId = v_numOppId  and numDomainId = v_numDomainID;
         ELSEIF v_vcDbColumnName = 'tintActive'
         then
		 
            v_InlineEditValue := (CASE v_InlineEditValue WHEN 'True' THEN '1' ELSE '0' END);
         ELSEIF v_vcDbColumnName = 'tintSource'
         then
		 
            IF POSITION('~' IN v_InlineEditValue) > 0 then
			
               v_Value1 := SUBSTR(v_InlineEditValue,1,POSITION('~' IN v_InlineEditValue) -1);
               v_Value2 := SUBSTR(v_InlineEditValue,POSITION('~' IN v_InlineEditValue)+1,LENGTH(v_InlineEditValue));
            ELSE
               v_Value1 := v_InlineEditValue;
               v_Value2 := CAST(0 AS VARCHAR(18));
            end if;
            update OpportunityMaster set tintSourceType = CAST(v_Value2 AS SMALLINT) where numOppId = v_numOppId  and numDomainId = v_numDomainID;
            v_InlineEditValue := v_Value1;
         ELSEIF v_vcDbColumnName = 'tintOppStatus'
         then --Update Inventory  0=Open,1=Won,2=Lost
            select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation1 FROM Domain WHERE numDomainId = v_numDomainID;
            IF EXISTS(SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID = v_numDomainID AND coalesce(bitMarginPriceViolated,false) = true) then
			
               v_bitViolatePrice := true;
            end if;
            SELECT COUNT(*) INTO v_intPendingApprovePending FROM OpportunityItems WHERE numOppId = v_numOppId AND coalesce(bitItemPriceApprovalRequired,false) = true;
            IF(v_intPendingApprovePending > 0 AND v_bitViolatePrice = true AND v_InlineEditValue = '1') then
			
               v_intExecuteDiv := 1;
               v_InlineEditValue := '-#123';
            end if;
            IF coalesce(v_intExecuteDiv,0) = 0 then
               select   tintoppstatus INTO v_tintOppStatus FROM OpportunityMaster WHERE numOppId = v_numOppId;
               IF (v_tintOppStatus = 0 OR v_tintOppStatus = 2) AND v_InlineEditValue = '1' then --Open to Won
                  select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numOppId = v_numOppId;
                  select   tintCRMType INTO v_tintCRMType FROM DivisionMaster WHERE numDivisionID = v_numDivisionID; 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
                  IF v_tintCRMType = 0 then --Lead & Order
					
                     UPDATE
                     DivisionMaster
                     SET
                     tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
                     bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
                     WHERE
                     numDivisionID = v_numDivisionID;
					--Promote Prospect to Account
                  ELSEIF v_tintCRMType = 1
                  then
					
                     UPDATE
                     DivisionMaster
                     SET
                     tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
                     WHERE
                     numDivisionID = v_numDivisionID;
                  end if;
                  IF v_tintCommitAllocation1 = 1 then
					
                     PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppId,0,v_numUserCntID);
                  end if;
                  UPDATE OpportunityMaster SET bintOppToOrder = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppId and numDomainId = v_numDomainID;
                  UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = NULL WHERE numOppId = v_numOppId;
                  PERFORM USP_OpportunityItems_CheckWarehouseMapping(v_numDomainID,v_numOppId);
               ELSEIF (v_InlineEditValue = '2')
               then -- Win to Lost
				
                  UPDATE OpportunityMaster SET dtDealLost = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppId and numDomainId = v_numDomainID;
               end if;
               IF ((v_tintOppStatus = 1 and v_InlineEditValue = '2') or (v_tintOppStatus = 1 and v_InlineEditValue = '0')) AND v_tintCommitAllocation1 = 1 then --Won to Lost or Won to Open
                  PERFORM USP_RevertDetailsOpp(v_numOppId,0,0::SMALLINT,v_numUserCntID);
               end if;
               UPDATE OpportunityMaster SET tintoppstatus = CAST(v_InlineEditValue AS SMALLINT) WHERE numOppId = v_numOppId;
            end if;
         ELSEIF v_vcDbColumnName = 'vcOppRefOrderNo'
         then --Update vcRefOrderNo OpportunityBizDocs
		 
            IF LENGTH(coalesce(v_InlineEditValue,'')) > 0 then
			 
               UPDATE OpportunityBizDocs SET vcRefOrderNo = v_InlineEditValue WHERE numoppid = v_numOppId;
            end if;
         ELSEIF v_vcDbColumnName = 'numStatus'
         then
            select   tintoppstatus, tintopptype INTO v_tintOppStatus,v_tintOppType from OpportunityMaster where numOppId = v_numOppId;
            IF v_tintOppType = 1 AND v_tintOppStatus = 1 then
				v_numOppStatus := CAST(NULLIF(v_InlineEditValue,'') AS NUMERIC(18,0));
               PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainID,v_numUserCntID,v_numOppId,v_numOppStatus);
            end if;
            IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppId AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1 AND coalesce(numStatus,0) != cast(NULLIF(v_InlineEditValue,'') as NUMERIC(18,0))) then
			 
	   				 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- numeric(18, 0)
							 -- tinyint
               PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
               v_numOppBizDocsId := 0,v_numOrderStatus := v_InlineEditValue::NUMERIC,v_numUserCntID := v_numUserCntID,
               v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
            end if;
         ELSEIF v_vcDbColumnName = 'dtReleaseDate'
         then
		 
            IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId AND tintopptype = 1 AND tintoppstatus = 0) then
			
               IF(SELECT GetListIemName(coalesce(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId) = '100' then
				
                  v_strSql := 'update OpportunityMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5 + (EXTRACT(HOUR FROM TIMEZONE(''UTC'',NOW()) - NOW()) || '' hours'')::interval,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',NOW()) where numOppId='||v_numOppId||' and numDomainID='||v_numDomainID;
               ELSE
                  RAISE EXCEPTION 'PERCENT_COMPLETE_MUST_BE_100';
                  RETURN;
               end if;
            ELSE
               v_strSql := 'update OpportunityMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5 + (EXTRACT(HOUR FROM TIMEZONE(''UTC'',NOW()) - NOW()) || '' hours'')::interval,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',NOW()) where numOppId='||v_numOppId||' and numDomainID='||v_numDomainID;
            end if;
         ELSEIF v_vcDbColumnName = 'numPercentageComplete'
         then
		
            IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId AND tintopptype = 1 AND tintoppstatus = 0 AND GetListIemName(coalesce(numPercentageComplete,0)) = '100') then
			
               IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId AND dtReleaseDate IS NULL) then
				
                  v_strSql := 'update OpportunityMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',NOW()) where numOppId='||v_numOppId||' and numDomainID='||v_numDomainID;
               ELSE
                  RAISE EXCEPTION 'REMOVE_RELEASE_DATE';
                  RETURN;
               end if;
            ELSE
               v_strSql := 'update OpportunityMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',NOW()) where numOppId='||v_numOppId||' and numDomainID='||v_numDomainID;
            end if;
         ELSEIF v_vcDbColumnName = 'numProjectID' AND isnumeric(v_InlineEditValue)
         then
		
            UPDATE OpportunityItems SET numProjectID = v_InlineEditValue::NUMERIC WHERE numOppId = v_numOppId;
         ELSEIF v_vcDbColumnName = 'intUsedShippingCompany'
         then
		
            UPDATE OpportunityMaster SET numShippingService = 0 WHERE numOppId = v_numOppId;
         end if;
         IF(v_vcDbColumnName != 'tintOppStatus') then
		
            v_strSql := 'update OpportunityMaster set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',NOW()) where numOppId='||v_numOppId||' and numDomainID='||v_numDomainID;
         end if;
	ELSEIF v_vcLookBackTableName = 'OpportunityItems'
    then
		IF v_vcDbColumnName = 'monPrice' then
			IF COALESCE((SELECT tintOppType FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppID=OM.numOppID WHERE OM.numDomainID=v_numDomainID AND OI.numOppItemtCode=v_numOppItemId),0) = 2 THEN
				IF EXISTS (SELECT OBI.numOppBizDocItemID FROM OpportunityBizDocItems OBI INNER JOIN OpportunityBizDocs OB ON OBI.numOppBizDocID=OB.numOppBizDocsId WHERE OBI.numOppItemID=v_numOppItemId AND numBizDocId=COALESCE((SELECT numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE numDomainId=187),644)) THEN
					RAISE EXCEPTION 'AUTHORITATIVE_BIZDOC_EXISTS';
					RETURN;
				ELSE
					 v_strSql := CONCAT('UPDATE OpportunityItems SET monPrice=',COALESCE(v_InlineEditValue,'0'),',monTotAmtBefDiscount=numUnitHour * ',COALESCE(v_InlineEditValue,'0'),',monTotAmount=(COALESCE(numUnitHour,0) * (CASE WHEN COALESCE(bitDiscountType,false)=false THEN (',COALESCE(v_InlineEditValue,'0'),'-(',COALESCE(v_InlineEditValue,'0'),'*(COALESCE(fltDiscount,0)/100))) ELSE (',COALESCE(v_InlineEditValue,'0'),'-COALESCE(fltDiscount,0)) END)) WHERE numOppItemtCode=',v_numOppItemId);
				END IF;
			END IF;
		END IF;
    ELSEIF v_vcLookBackTableName = 'Cases'
    then
	 
        IF v_vcDbColumnName = 'numAssignedTo' then ---Updating if Case is assigned to someone  
			
        select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo from Cases where numCaseId = v_numCaseId and numDomainID = v_numDomainID;
        if (v_tempAssignedTo <> cast(NULLIF(v_InlineEditValue,'') as NUMERIC(9,0)) and  v_InlineEditValue <> '0') then
					
            update Cases set numAssignedTo = v_InlineEditValue::NUMERIC,numAssignedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numCaseId = v_numCaseId  and numDomainID = v_numDomainID;
        ELSEIF (v_InlineEditValue = '0')
        then
					
            update Cases set numAssignedTo = 0,numAssignedBy = 0,bintModifiedDate = TIMEZONE('UTC',now()) where numCaseId = v_numCaseId  and numDomainID = v_numDomainID;
        end if;
        end if;
        UPDATE Cases SET bintModifiedDate = TIMEZONE('UTC',now()) WHERE numCaseId = v_numCaseId  and numDomainID = v_numDomainID;
        v_strSql := 'update Cases set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numCaseId='||v_numCaseId||' and numDomainID='||v_numDomainID;
    ELSEIF v_vcLookBackTableName = 'Tickler'
    then
        IF v_vcOrigDbColumnName = 'Description' then
			v_vcOrigDbColumnName := 'textDetails';
        ELSEIF v_vcOrigDbColumnName = 'Activity'
        then
			v_vcOrigDbColumnName := 'numActivity';
        ELSEIF v_vcOrigDbColumnName = 'Priority'
        then
			v_vcOrigDbColumnName := 'numStatus';
        end if;
        IF(v_vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId = v_numCommID and numDomainID = v_numDomainID)) then
			v_strSql := 'update Communication set ' || coalesce(v_vcOrigDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',dtModifiedDate=TIMEZONE(''UTC'',now()) where numCommId='||v_numCommID||' and numDomainID='||v_numDomainID;
        ELSEIF (v_vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID = v_numCommID))
        then
			v_strSql := 'update Activity set Comments=$5 where ActivityID='||v_numDomainID2;
        ELSE
			v_strSql := 'update Communication set ' || coalesce(v_vcOrigDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',dtModifiedDate=TIMEZONE(''UTC'',now()) where numCommId='||v_numCommID||' and numDomainID='||v_numDomainID;
        end if;
      ELSEIF v_vcLookBackTableName = 'ExtarnetAccounts'
      then
	 
         IF v_vcDbColumnName = 'bitEcommerceAccess' then
		
            IF LOWER(v_InlineEditValue) = 'yes' OR LOWER(v_InlineEditValue) = 'true' then
			
               IF NOT EXISTS(SELECT * FROM ExtarnetAccounts where numDivisionID = v_numDivisionID) then
                  select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
                  select   numGroupID INTO v_numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID AND tintGroupType = 2    LIMIT 1;
                  IF v_numGroupID IS NULL then 
                     v_numGroupID := 0;
                  end if;
                  INSERT INTO ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)
					VALUES(v_numCompanyID,v_numDivisionID,v_numGroupID,v_numDomainID);
               end if;
            ELSE
               DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN(SELECT coalesce(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID = v_numDivisionID);
               DELETE FROM ExtarnetAccounts where numDivisionID = v_numDivisionID;
            end if;
            open SWV_RefCur for
            SELECT(CASE WHEN LOWER(v_InlineEditValue) = 'yes' OR LOWER(v_InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData;
            
         end if;
      ELSEIF v_vcLookBackTableName = 'ExtranetAccountsDtl'
      then
	
         IF v_vcDbColumnName = 'vcPassword' then
		
            IF LENGTH(v_InlineEditValue) = 0 then
			
               DELETE FROM ExtranetAccountsDtl WHERE numContactID = v_numContactID;
            ELSE
               IF EXISTS(SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivisionID) then
				
                  IF EXISTS(SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID = v_numContactID) then
					
                     UPDATE
                     ExtranetAccountsDtl
                     SET
                     vcPassword = v_InlineEditValue,tintAccessAllowed = 1
                     WHERE
                     numContactID = v_numContactID;
                  ELSE
                     select   numExtranetID INTO v_numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = v_numDivisionID;
                     INSERT INTO ExtranetAccountsDtl(numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES(v_numExtranetID,v_numContactID,v_InlineEditValue,1,v_numDomainID);
                  end if;
               ELSE
                  RAISE EXCEPTION 'Organization e-commerce access is not enabled';
               end if;
            end if;
            OPEN SWV_RefCur FOR
            SELECT v_InlineEditValue AS vcData;
            
         end if;
      ELSEIF v_vcLookBackTableName = 'CustomerPartNumber'
      then
	
         IF v_vcDbColumnName = 'CustomerPartNo' then
		
            select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
            IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = v_numRecId AND numCompanyId = v_numCompanyID) then
			
               UPDATE CustomerPartNumber SET CustomerPartNo = v_InlineEditValue WHERE numItemCode = v_numRecId AND numCompanyId = v_numCompanyID AND numDomainID = v_numDomainID;
            ELSE
               INSERT INTO CustomerPartNumber(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES(v_numRecId, v_numCompanyID, v_numDomainID, v_InlineEditValue);
            end if;
            open SWV_RefCur for
            SELECT v_InlineEditValue AS vcData;
            
         end if;
      ELSEIF v_vcLookBackTableName = 'CheckHeader'
      then
	
         IF EXISTS(SELECT 1 FROM CheckHeader WHERE numDomainID = v_numDomainID AND numCheckNo = v_InlineEditValue AND numCheckHeaderID <> v_numRecId) then
		
            RAISE EXCEPTION 'DUPLICATE_CHECK_NUMBER';
            RETURN;
         end if;
         v_strSql := 'UPDATE CheckHeader SET ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) WHERE numCheckHeaderID='||v_numRecId||' and numDomainID='||v_numDomainID;
      ELSEIF v_vcLookBackTableName = 'Item'
      then
	
         IF v_vcDbColumnName = 'monListPrice' then
		
            v_InlineEditValue := REPLACE(v_InlineEditValue,',','');
            IF EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numRecId AND 1 =(CASE WHEN coalesce(numItemGroup,0) > 0 THEN(CASE WHEN coalesce(bitMatrix,false) = true THEN 1 ELSE 0 END) ELSE 1 END)) then
			
               UPDATE WareHouseItems SET monWListPrice = v_InlineEditValue WHERE numDomainID = v_numDomainID AND numItemID = v_numRecId;
               open SWV_RefCur for
               SELECT v_InlineEditValue AS vcData;
			   
            ELSE
               RAISE EXCEPTION 'You are not allowed to edit list price for warehouse level matrix item.';
            end if;
            RETURN;
         end if;
         v_strSql := 'UPDATE Item set ' || coalesce(v_vcDbColumnName,'') || '=$5,numModifiedBy='||v_numUserCntID||',bintModifiedDate=TIMEZONE(''UTC'',now()) where numItemCode='||v_numRecId||' AND numDomainID='||v_numDomainID;
      ELSEIF v_vcLookBackTableName = 'Warehouses'
      then
	
         IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppId AND coalesce(tintoppstatus,0) = 1 AND coalesce(tintshipped,0) = 0) then
		
            IF EXISTS(SELECT numWareHouseID FROM Warehouses WHERE numDomainID = v_numDomainID AND numWareHouseID = v_InlineEditValue) then
               select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
               BEGIN
                  CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
                  EXCEPTION WHEN OTHERS THEN
                     NULL;
               END;
               DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
               CREATE TEMPORARY TABLE tt_TEMPITEMS
               (
                  ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
                  numOppItemID NUMERIC(18,0),
                  numItemCode NUMERIC(18,0),
                  numWarehouseItemID NUMERIC(18,0)
               );
               INSERT INTO tt_TEMPITEMS(numOppItemID
					,numItemCode
					,numWarehouseItemID)
               SELECT
               numoppitemtCode
					,numItemCode
					,numWarehouseItmsID
               FROM
               OpportunityItems
               WHERE
               numOppId = v_numOppId
               AND (numoppitemtCode = v_numOppItemId OR coalesce(v_numOppItemId,0) = 0);
               IF EXISTS(SELECT TI.numItemCode FROM tt_TEMPITEMS TI LEFT JOIN WareHouseItems WI ON TI.numItemCode = WI.numItemID AND WI.numWareHouseID = v_InlineEditValue WHERE WI.numWareHouseItemID IS NULL) then
				
                  RAISE EXCEPTION 'WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM';
               ELSE
                  select   COUNT(*) INTO v_jCount FROM tt_TEMPITEMS;
                  WHILE v_j <= v_jCount LOOP
                     select   numOppItemID, numItemCode, numWarehouseItemID INTO v_numTempOppItemID,v_numTempItemCode,v_numOldWarehouseItemID FROM
                     tt_TEMPITEMS TI WHERE
                     ID = v_j;
                     select   numWareHouseItemID INTO v_numNewWarehouseItemID FROM
                     WareHouseItems WHERE
                     numDomainID = v_numDomainID
                     AND numItemID = v_numTempItemCode
                     AND numWareHouseID = v_InlineEditValue   ORDER BY
                     coalesce(numWLocationID,0) ASC LIMIT 1;
                     IF v_numOldWarehouseItemID <> v_numNewWarehouseItemID then
                        BEGIN
                           -- BEGIN TRANSACTION
 
								--REVERTING BACK THE WAREHOUSE ITEMS                  
								IF v_tintCommitAllocation = 1 then
								
                              PERFORM USP_RevertDetailsOpp(v_numOppId,v_numTempOppItemID,0::SMALLINT,v_numUserCntID);
                           end if;
                           UPDATE OpportunityItems SET numWarehouseItmsID = v_numNewWarehouseItemID WHERE numoppitemtCode = v_numTempOppItemID;
                           IF v_tintCommitAllocation = 1 then
								
                              PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppId,v_numTempOppItemID,v_numUserCntID);
                           end if;
                           UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = false WHERE numoppitemtCode = v_numTempOppItemID;
                           -- COMMITv_strSql$5
EXCEPTION WHEN OTHERS THEN
                              -- ROLLBACK


								-- Raise an error with the details of the exception
								v_ErrMsg := 'SQLERRM';
                              v_ErrSeverity := 1;
                              RAISE EXCEPTION '%',v_ErrMsg;
                        END;
                     end if;
                     v_j := v_j::bigint+1;
                  END LOOP;
               end if;
            ELSE
               RAISE EXCEPTION 'WAREHOUSE_DOES_NOT_EXISTS';
            end if;
         end if;
      end if;
	  v_strSql := COALESCE(v_strSql, '');

	  IF v_vcFieldDataType='N' THEN 
		v_strSql := regexp_replace(v_strSql,'\$5','$5::NUMERIC','gi');
	  END IF;

	   IF v_vcAssociatedControlType='DateField' THEN 
		v_strSql := regexp_replace(v_strSql,'\$5','$5::TIMESTAMP','gi');
	  END IF;

	  IF v_vcAssociatedControlType='CheckBox' THEN 
		IF v_vcDbColumnName = 'tintActive' THEN
			v_strSql := regexp_replace(v_strSql,'\$5','$5::SMALLINT','gi');
		ELSE
			v_strSql := regexp_replace(v_strSql,'\$5','$5::BOOLEAN','gi');
		END IF;
	  END IF;

      EXECUTE v_strSql USING v_numDomainID,v_numUserCntID,v_numDivisionID,v_numContactID,v_InlineEditValue,v_numWOId,v_numProId,v_numOppId,v_numOppItemId,v_numCaseId,v_numWODetailId,v_numCommID,v_numRecId;
      IF v_vcAssociatedControlType = 'SelectBox' then
       
         IF v_vcDbColumnName = 'tintSource' AND v_vcLookBackTableName = 'OpportunityMaster' then
			
            open SWV_RefCur for
            SELECT fn_GetOpportunitySourceValue(v_Value1::NUMERIC,v_Value2::SMALLINT,v_numDomainID) AS vcData;
			
         ELSEIF v_vcDbColumnName = 'numContactID'
         then
			
            open SWV_RefCur for
            SELECT fn_GetContactName(v_InlineEditValue);
			
         ELSEIF v_vcDbColumnName = 'numPartner' OR v_vcDbColumnName = 'numPartenerSource'
         then
			
            v_intExecuteDiv := 1;
            open SWV_RefCur for
            SELECT D.vcPartnerCode || '-' || C.vcCompanyName  AS vcData FROM DivisionMaster AS D
            LEFT JOIN CompanyInfo AS C
            ON D.numCompanyID = C.numCompanyId WHERE D.numDivisionID = cast(NULLIF(v_InlineEditValue,'') as NUMERIC(18,0));
			
			
         ELSEIF v_vcDbColumnName = 'tintOppStatus' AND v_intExecuteDiv = 1
         then
			
            open SWV_RefCur for
            SELECT v_InlineEditValue AS vcData;
			
			
         ELSEIF v_vcDbColumnName = 'numReleaseStatus'
         then
			
            If v_InlineEditValue = '1' then
				
               open SWV_RefCur for
               SELECT 'Open';
			   
			   
            ELSEIF v_InlineEditValue = '2'
            then
				
               open SWV_RefCur for
               SELECT 'Purchased';
			   
			   
            ELSE
               
			   open SWV_RefCur for
               SELECT '';
			   
			   
            end if;
         ELSEIF v_vcDbColumnName = 'numPartenerContact'
         then
			
            SELECT  A.vcGivenName INTO v_InlineEditValue FROM AdditionalContactsInformation AS A
            LEFT JOIN DivisionMaster AS D
            ON D.numDivisionID = A.numDivisionId WHERE D.numDomainID = v_numDomainID AND A.numContactId = cast(NULLIF(v_InlineEditValue,'') as NUMERIC(18,0))     LIMIT 1;
            
			open SWV_RefCur for
            SELECT v_InlineEditValue AS vcData;
			
			
         ELSEIF v_vcDbColumnName = 'numPartenerSource'
         then
			
            SELECT D.vcPartnerCode || '-' || C.vcCompanyName  AS vcData INTO v_InlineEditValue FROM DivisionMaster AS D
            LEFT JOIN CompanyInfo AS C
            ON D.numCompanyID = C.numCompanyId WHERE D.numDivisionID = cast(NULLIF(v_InlineEditValue,'') as NUMERIC(18,0));
            
			open SWV_RefCur for
            SELECT v_InlineEditValue AS vcData;
			
			
         ELSEIF v_vcDbColumnName = 'numShippingService'
         then
			
            open SWV_RefCur for
            SELECT coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = (CASE WHEN isnumeric(v_InlineEditValue) THEN v_InlineEditValue ELSE '0' END)::NUMERIC),'') AS vcData;
			
			
         ELSEIF v_vcDbColumnName = 'intDropShip'
         then
			
            open SWV_RefCur for
            SELECT CASE WHEN cast(NULLIF(coalesce(v_InlineEditValue,cast(0 as TEXT)),'') as INTEGER) = 1 THEN 'Not Available'
            WHEN cast(NULLIF(coalesce(v_InlineEditValue,cast(0 as TEXT)),'') as INTEGER) = 2 THEN 'Blank Available'
            WHEN cast(NULLIF(coalesce(v_InlineEditValue,cast(0 as TEXT)),'') as INTEGER) = 3 THEN 'Vendor Label'
            WHEN cast(NULLIF(coalesce(v_InlineEditValue,cast(0 as TEXT)),'') as INTEGER) = 4 THEN 'Private Label'
            ELSE 'Select One'
            END AS vcData;
			
			
         ELSEIF v_vcDbColumnName = 'numProjectID'
         then
			
            open SWV_RefCur for
            SELECT coalesce((SELECT vcProjectName FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = (CASE WHEN isnumeric(v_InlineEditValue) THEN v_InlineEditValue ELSE '0' END)::NUMERIC),'-') AS vcData;
			
			
         else
            
			open SWV_RefCur for
            SELECT fn_getSelectBoxValue(v_vcListItemType,v_InlineEditValue::NUMERIC,v_vcDbColumnName) AS vcData;
			
			
         end if;
      ELSE
         IF v_vcDbColumnName = 'tintActive' then
		  	
            v_InlineEditValue := CASE v_InlineEditValue WHEN '1' THEN 'Yes' WHEN 'True' THEN 'Yes' ELSE 'No' END;
         ELSEIF v_vcDbColumnName = 'vcWareHouse' AND SWF_IsNumeric(v_InlineEditValue) = true
         then
			
            select   vcWareHouse INTO v_InlineEditValue FROM Warehouses WHERE numDomainID = v_numDomainID AND numWareHouseID = v_InlineEditValue;
         end if;
         IF v_vcAssociatedControlType = 'Check box' or v_vcAssociatedControlType = 'Checkbox' then
			
            v_InlineEditValue := Case v_InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END;
         end if;
         
		 open SWV_RefCur for
         SELECT v_InlineEditValue AS vcData;
		 
		 
      end if;
      IF v_vcLookBackTableName = 'AdditionalContactsInformation' AND v_vcDbColumnName = 'numECampaignID' then
	
		--Manage ECampiagn When Campaign is changed from inline edit
		 --  numeric(9, 0)
         v_Date := GetUTCDateWithoutTime();
         v_numECampaignID := CAST(v_InlineEditValue AS NUMERIC(18,0));
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  datetime
		 --  bit
         IF v_numECampaignID = -1 then
            v_numECampaignID := 0;
         end if;
         PERFORM USP_ManageConEmailCampaign(v_numConEmailCampID := 0,v_numContactID := v_numContactID,v_numECampaignID := v_numECampaignID,
         v_intStartDate := v_Date,v_bitEngaged := true,
         v_numUserCntID := v_numUserCntID);
      end if;
   ELSE
      v_InlineEditValue := SUBSTR(Cast(v_InlineEditValue as VARCHAR(1000)),1,1000);
      select   fld_type INTO v_vcAssociatedControlType FROM CFW_Fld_Master WHERE Fld_id = v_numFormFieldId;
      IF v_vcAssociatedControlType = 'CheckBox' then
	
         v_InlineEditValue := CAST((case when v_InlineEditValue = 'False' then 0 ELSE 1 END) AS VARCHAR(8000));
      end if;
      IF v_vcPageName = 'organization' then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
			
            UPDATE CFW_FLD_Values SET Fld_Value = v_InlineEditValue WHERE FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            insert into CFW_FLD_Values(Fld_ID,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;

			--Added By :Sachin Sadhu ||Datev_numDomainID3thOct2014
			--Purpose  :To manage CustomFields in Queue
         PERFORM USP_CFW_FLD_Values_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecId,
         v_numFormFieldId := v_numFormFieldId);
      ELSEIF v_vcPageName = 'contact'
      then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id = CFW.fld_id WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
			
            UPDATE CFW_FLD_Values_Cont SET Fld_Value = v_InlineEditValue WHERE FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id = CFW.fld_id WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            insert into CFW_FLD_Values_Cont(fld_id,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;
			
			--Added By :Sachin Sadhu ||Datev_numDomainID3thOct2014
			--Purpose  :To manage CustomFields in Queue
         PERFORM USP_CFW_FLD_Values_Cont_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecId,v_numFormFieldId := v_numFormFieldId);
      
	  ELSEIF v_vcPageName = 'project'
      then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Pro CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
			
            UPDATE CFW_Fld_Values_Pro SET Fld_Value = v_InlineEditValue WHERE FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Pro CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            insert into CFW_Fld_Values_Pro(Fld_ID,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;

			
			--Added By :Sachin Sadhu ||Datev_numDomainID4thOct2014
			--Purpose  :To manage CustomFields in Queue
         PERFORM USP_CFW_FLD_Values_Pro_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecId,v_numFormFieldId := v_numFormFieldId);
      ELSEIF v_vcPageName = 'opp'
      then
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
		
            UPDATE
            CFW_Fld_Values_Opp
            SET
            Fld_Value = v_InlineEditValue,numModifiedBy = v_numUserCntID,bintModifiedDate = v_dtDate
            WHERE
            FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            INSERT INTO CFW_Fld_Values_Opp(Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId,v_numUserCntID,v_dtDate);
         end if;
         UPDATE OpportunityMaster SET numModifiedBy = v_numUserCntID,bintModifiedDate = v_dtDate WHERE numOppId = v_numRecId; 

		--Added By :Sachin Sadhu ||Datev_numDomainID4thOct2014
		--Purpose  :To manage CustomFields in Queue
         PERFORM USP_CFW_Fld_Values_Opp_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecId,v_numFormFieldId := v_numFormFieldId);
      ELSEIF v_vcPageName = 'oppitems'
      then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id = CFW.fld_id WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
		
            UPDATE
            CFW_Fld_Values_OppItems
            SET
            Fld_Value = v_InlineEditValue
            WHERE
            FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id = CFW.fld_id WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            INSERT INTO CFW_Fld_Values_OppItems(fld_id,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;
      ELSEIF v_vcPageName = 'case'
      then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
			
            UPDATE CFW_FLD_Values_Case SET Fld_Value = v_InlineEditValue WHERE FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            insert into CFW_FLD_Values_Case(Fld_ID,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;

			
			--Added By :Sachin Sadhu ||Datev_numDomainID4thOct2014
			--Purpose  :To manage CustomFields in Queue
         PERFORM USP_CFW_FLD_Values_Case_CT(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numRecordID := v_numRecId,v_numFormFieldId := v_numFormFieldId);
      ELSEIF v_vcPageName = 'item'
      then
	
         IF(SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId) > 0 then
		
            UPDATE CFW_FLD_Values_Item SET Fld_Value = v_InlineEditValue WHERE FldDTLID =(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id = CFW.Fld_ID WHERE CFM.Fld_id = v_numFormFieldId AND CFW.RecId = v_numRecId);
         ELSE
            INSERT INTO CFW_FLD_Values_Item(Fld_ID,Fld_Value,RecId) VALUES(v_numFormFieldId,v_InlineEditValue,v_numRecId);
         end if;
      end if;
      IF v_vcAssociatedControlType = 'SelectBox' then
	
         OPEN SWV_RefCur FOR
         SELECT vcData FROM Listdetails WHERE numListItemID = CAST(v_InlineEditValue AS NUMERIC(18));
		 
		 
      ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
      then
         v_str := 'SELECT string_agg(vcData, '','') FROM ListDetails WHERE numlistitemid IN (' ||(CASE WHEN LENGTH(v_InlineEditValue) > 0 THEN v_InlineEditValue ELSE '0' END) || ') ';
         OPEN SWV_RefCur FOR 
		 EXECUTE COALESCE(v_str, '');
		 
      
	  ELSEIF v_vcAssociatedControlType = 'CheckBox'
      then
	
         OPEN SWV_RefCur FOR
         select CASE v_InlineEditValue WHEN '1' THEN 'Yes' WHEN 'True' THEN 'Yes'  ELSE 'No' END AS vcData;
		 
      ELSE
         OPEN SWV_RefCur FOR
         SELECT v_InlineEditValue AS vcData;
		 
      end if;
   end if;
END; 
$$;

 


