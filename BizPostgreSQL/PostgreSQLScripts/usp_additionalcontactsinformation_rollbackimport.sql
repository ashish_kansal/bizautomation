CREATE OR REPLACE FUNCTION USP_AdditionalContactsInformation_RollbackImport(v_numContactID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Email  VARCHAR(1000); 
   v_AddEmail  VARCHAR(1000);
   v_numDomainID  NUMERIC(18,0); 
   v_numDivisionID  NUMERIC(18,0);
   v_bitPrimaryContact  BOOLEAN;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   select   vcEmail INTO v_Email FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
   select   vcAsstEmail INTO v_AddEmail FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;

   select   numDomainID, numDivisionId, coalesce(bitPrimaryContact,false) INTO v_numDomainID,v_numDivisionID,v_bitPrimaryContact FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;    


   IF EXISTS(SELECT * FROM UserMaster where numUserDetailId = v_numContactID and numDomainID = v_numDomainID and bitactivateflag = true) then
	
      RAISE EXCEPTION 'USER';
   end if;

   IF EXISTS(select * from OpportunityMaster WHERE numContactId = v_numContactID) then
	
      RAISE EXCEPTION 'CHILD_OPP';
   end if;

   IF EXISTS(SELECT * FROM Cases WHERE numContactId = v_numContactID) then
	
      RAISE EXCEPTION 'CHILD_CASE';
      RETURN;
   end if;

   BEGIN
      -- BEGIN TRAN
DELETE FROM CompanyAssociations WHERE numDomainID = v_numDomainID AND numContactID = v_numContactID;
      DELETE FROM ImportActionItemReference WHERE numContactID = v_numContactID;
      delete FROM ConECampaignDTL where numConECampID in(select numConEmailCampID from ConECampaign where numContactID = v_numContactID);
      delete FROM ConECampaign where numContactID = v_numContactID;
      delete FROM AOIContactLink where numContactID = v_numContactID;
      delete FROM CaseContacts where numContactID = v_numContactID;
      delete FROM ProjectsContacts where numContactID = v_numContactID;
      delete FROM OpportunityContact where numContactID = v_numContactID;
      delete FROM UserTeams where numUserCntID = v_numContactID;
      delete FROM UserTerritory where numUserCntID = v_numContactID;
      DELETE FROM UserMaster WHERE numUserDetailId = v_numContactID AND bitactivateflag = false;
      DELETE FROM AdditionalContactsInformation WHERE numContactId = v_numContactID;
      UPDATE EmailMaster SET numContactID = NULL WHERE vcEmailId = v_Email AND numContactID = v_numContactID;
      UPDATE EmailMaster SET numContactID = NULL WHERE vcEmailId = v_AddEmail AND numContactID = v_numContactID;
      IF v_bitPrimaryContact = true AND(SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID) > 0 then
		
         UPDATE AdditionalContactsInformation SET bitPrimaryContact = true WHERE numContactId = coalesce((SELECT  numContactId FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID LIMIT 1),0);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;


