DROP FUNCTION IF EXISTS fn_GetItemAttributes;

CREATE OR REPLACE FUNCTION fn_GetItemAttributes(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_bitEcommerce BOOLEAN)
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strAttribute  VARCHAR(500) DEFAULT '';
   v_numCusFldID  NUMERIC(18,0) DEFAULT 0;
   v_numCusFldValue  NUMERIC(18,0) DEFAULT 0;
   SWV_RowCount INTEGER;
BEGIN
   select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue FROM
   ItemAttributes WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode
   AND Fld_Value != 0   ORDER BY
   Fld_ID LIMIT 1;

   while v_numCusFldID > 0 LOOP
      IF SWF_isnumeric(CAST(v_numCusFldValue AS TEXT)) = true then
		
         select   CONCAT(v_strAttribute,(CASE WHEN LENGTH(v_strAttribute) > 0 THEN(CASE WHEN coalesce(v_bitEcommerce,false) = true THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN coalesce(v_bitEcommerce,false) = true THEN CONCAT('<b><i>(',FLd_label,')</i></b>&nbsp;') ELSE FLd_label END)) INTO v_strAttribute FROM
         CFW_Fld_Master WHERE
         Fld_id = v_numCusFldID
         AND Grp_id = 9;
         IF LENGTH(v_strAttribute) > 0 then
			
            IF coalesce(v_bitEcommerce,false) = true then
				
               select   CONCAT(v_strAttribute,vcData) INTO v_strAttribute FROM
               Listdetails WHERE
               numListItemID = v_numCusFldValue;
            ELSE
               select   coalesce(v_strAttribute,'') || ':' || vcData INTO v_strAttribute FROM Listdetails WHERE numListItemID = v_numCusFldValue;
            end if;
         end if;
      ELSE
         select   CONCAT(v_strAttribute,(CASE WHEN LENGTH(v_strAttribute) > 0 THEN(CASE WHEN coalesce(v_bitEcommerce,false) = true THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN coalesce(v_bitEcommerce,false) = true THEN CONCAT('<b><i>(',FLd_label,')</i></b>&nbsp;') ELSE FLd_label END)) INTO v_strAttribute FROM
         CFW_Fld_Master WHERE
         Fld_id = v_numCusFldID
         AND Grp_id = 9;
         IF LENGTH(v_strAttribute) > 0 then
			
            IF coalesce(v_bitEcommerce,false) = true then
				
               select   CONCAT(v_strAttribute,'-') INTO v_strAttribute FROM
               Listdetails WHERE
               numListItemID = v_numCusFldValue;
            ELSE
               v_strAttribute := coalesce(v_strAttribute,'') || ':-';
            end if;
         end if;
      end if;
      select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue FROM
      ItemAttributes WHERE
      numDomainID = v_numDomainID
      AND numItemCode = v_numItemCode
	  AND Fld_ID > v_numCusFldID
      AND Fld_Value <> 0
         ORDER BY
      Fld_ID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_numCusFldID := 0;
      end if;
   END LOOP;

   RETURN v_strAttribute;
END; $$;

