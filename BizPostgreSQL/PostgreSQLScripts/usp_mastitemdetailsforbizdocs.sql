-- Stored procedure definition script USP_MastItemDetailsForBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MastItemDetailsForBizDocs(v_numDomainID NUMERIC(9,0) DEFAULT 0,        
v_numListID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numListItemID AS "numListItemID",vcData as "vcData" from Listdetails
   where (numDomainid = v_numDomainID or constFlag = true) and numListID = v_numListID
   order by sintOrder;
END; $$;












