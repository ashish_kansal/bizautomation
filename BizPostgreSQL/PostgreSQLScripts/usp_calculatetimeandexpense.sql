-- Stored procedure definition script USP_CalculateTimeAndExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CalculateTimeAndExpense(v_numComPayPeriodID NUMERIC(18,0),
	 v_numDomainID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtPayStart  DATE;
   v_dtPayEnd  DATE;
BEGIN
   select   dtStart, dtEnd INTO v_dtPayStart,v_dtPayEnd FROM
   CommissionPayPeriod WHERE
   numComPayPeriodID = v_numComPayPeriodID;

   DELETE FROM
   TimeAndExpenseCommission
   WHERE
   numDomainID = v_numDomainID
   AND coalesce(bitCommissionPaid,false) = false
   AND (CAST(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd OR CAST(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);

   DELETE FROM
   StagePercentageDetailsTaskCommission
   WHERE
   numDomainID = v_numDomainID
   AND coalesce(bitCommissionPaid,false) = false
   AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND CAST(dtmCreatedOn+CAST(-1*(EXTRACT(DAY FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60*24+EXTRACT(HOUR FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60+EXTRACT(MINUTE FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60+EXTRACT(SECOND FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))) || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd);

   DELETE FROM TimeAndExpenseCommission TEC using timeandexpense TE where TEC.numTimeAndExpenseID = TE.numCategoryHDRID AND TEC.numDomainID = v_numDomainID
   AND coalesce(TEC.bitCommissionPaid,false) = false
   AND TE.numCategoryHDRID IS NULL;

   DELETE FROM StagePercentageDetailsTaskCommission SPDTC using StagePercentageDetailsTask SPDT where SPDTC.numTaskID = SPDT.numTaskId AND SPDTC.numDomainID = v_numDomainID
   AND coalesce(SPDTC.bitCommissionPaid,false) = false
   AND SPDT.numTaskId IS NULL;

   INSERT INTO TimeAndExpenseCommission(numDomainID
		,numComPayPeriodID
		,numTimeAndExpenseID
		,numUserCntID
		,dtFromDate
		,dtToDate
		,numType
		,numCategory
		,bitReimburse
		,numHours
		,monHourlyRate
		,monTotalAmount
		,bitCommissionPaid)
   SELECT
   v_numDomainID
		,v_numComPayPeriodID
		,numCategoryHDRID
		,timeandexpense.numUserCntID
		,timeandexpense.dtFromDate
		,timeandexpense.dtToDate
		,timeandexpense.numtype
		,timeandexpense.numCategory
		,coalesce(timeandexpense.bitReimburse,false)
		,coalesce(CAST((EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
   0)
		,coalesce(UserMaster.monHourlyRate,0)
		,(CASE WHEN coalesce(timeandexpense.bitReimburse,false) = true AND timeandexpense.numCategory = 2 THEN coalesce(monAmount,0) ELSE coalesce(CAST((EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
      0)*coalesce(UserMaster.monHourlyRate,0) END)
		,false
   FROM
   timeandexpense
   INNER JOIN
   UserMaster
   ON
   timeandexpense.numUserCntID = UserMaster.numUserDetailId
   LEFT JOIN
   TimeAndExpenseCommission
   ON
   timeandexpense.numCategoryHDRID = TimeAndExpenseCommission.numTimeAndExpenseID
   WHERE
   timeandexpense.numDomainID = v_numDomainID
   AND TimeAndExpenseCommission.ID IS NULL
   AND (timeandexpense.numtype = 1 OR timeandexpense.numtype = 2)
   AND 1 =(CASE WHEN timeandexpense.numCategory = 2 THEN(CASE WHEN coalesce(timeandexpense.bitReimburse,false) = true THEN 1 ELSE 0 END) WHEN timeandexpense.numCategory = 1 THEN 1 ELSE 0 END)
   AND (CAST(timeandexpense.dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd
   OR
   CAST(timeandexpense.dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd); 


   INSERT INTO StagePercentageDetailsTaskCommission(numDomainID
		,numComPayPeriodID
		,numTaskID
		,numUserCntID
		,numHours
		,monHourlyRate
		,monTotalAmount
		,bitCommissionPaid
		,monAmountPaid)
   SELECT
   v_numDomainID
		,v_numComPayPeriodID
		,numTaskId
		,SPDT.numAssignTo
		,coalesce(CAST(((coalesce(numHours,0)*60)+coalesce(numMinutes,0)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),0) AS numHours
		,coalesce(UserMaster.monHourlyRate,0)
		,coalesce(CAST(((coalesce(numHours,0)*60)+coalesce(numMinutes,0)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),0)*coalesce(UserMaster.monHourlyRate,0)
		,false
		,0
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   StagePercentageDetails SPD
   ON
   SPDT.numStageDetailsId = SPD.numStageDetailsId
   INNER JOIN
   UserMaster
   ON
   SPDT.numAssignTo = UserMaster.numUserDetailId
   WHERE
   coalesce(bitTaskClosed,false) = true
   AND CAST(dtmCreatedOn+CAST(-1*(EXTRACT(DAY FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60*24+EXTRACT(HOUR FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60+EXTRACT(MINUTE FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60+EXTRACT(SECOND FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))) || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd;
RETURN;
END; $$;


