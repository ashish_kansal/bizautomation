-- Stored procedure definition script USP_UpdateECampaignAlertStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateECampaignAlertStatus(v_numConECampDTLID NUMERIC(9,0),
    v_tintDeliveryStatus SMALLINT,
    v_vcEmailLog VARCHAR(500) DEFAULT NULL,
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Date  TIMESTAMP;
BEGIN
   v_Date := TIMEZONE('UTC',now());

   IF v_tintMode = 0 then
        
      UPDATE  ConECampaignDTL
      SET     vcEmailLog = v_vcEmailLog,tintDeliveryStatus = v_tintDeliveryStatus,bitSend = true,
      bintSentON = v_Date
      WHERE   numConECampDTLID = v_numConECampDTLID;
   end if;

   IF v_tintMode = 1 then
        
      UPDATE  ConECampaignDTL
      SET     bitFollowUpStatus = true
      WHERE   numConECampDTLID = v_numConECampDTLID;
   end if;
			
   IF v_tintMode = 2 then --For Action Item
        
      UPDATE  ConECampaignDTL
      SET     vcEmailLog = v_vcEmailLog,bitSend = true,tintDeliveryStatus = v_tintDeliveryStatus,
      bintSentON = TIMEZONE('UTC',now())
      WHERE   numConECampDTLID = v_numConECampDTLID;
   end if;
   RETURN;
END; $$;



