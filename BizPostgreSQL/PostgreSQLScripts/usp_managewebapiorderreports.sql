-- Stored procedure definition script USP_ManageWebAPIOrderReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--created by Joseph
CREATE OR REPLACE FUNCTION USP_ManageWebAPIOrderReports(v_numOrdReportId NUMERIC(9,0),
          v_numDomainID NUMERIC(9,0),
          v_numWebApiId NUMERIC(9,0),
          v_vcWebApiOrderReportId VARCHAR(25),
          v_tintStatus SMALLINT,
          v_RStatus SMALLINT DEFAULT 0,
          v_ReportTypeId SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   IF v_numOrdReportId = 0 then
	  
      select   COUNT(*) INTO v_Check FROM   WebApiOrderReports WHERE  numDomainID = v_numDomainID AND WebApiId = v_numWebApiId
      AND vcWebApiOrderReportId = v_vcWebApiOrderReportId AND tintStatus = 0;
      IF v_Check = 0 then
		 
         INSERT INTO WebApiOrderReports(numDomainID,
						WebApiId,
						vcWebApiOrderReportId,
						tintStatus,
						dtCreated,
						RStatus,
						tintReportTypeId)
			VALUES(v_numDomainID,
						v_numWebApiId,
						v_vcWebApiOrderReportId,
						v_tintStatus,
						LOCALTIMESTAMP ,
						v_RStatus,
						v_ReportTypeId);
      end if;
   ELSEIF v_numOrdReportId <> 0
   then
	  
      UPDATE WebApiOrderReports
      SET    tintStatus = v_tintStatus,dtModified = LOCALTIMESTAMP
      WHERE  numDomainID = v_numDomainID AND WebApiId = v_numWebApiId
      AND vcWebApiOrderReportId = v_vcWebApiOrderReportId;
   end if;
   RETURN;
END; $$;



