-- Stored procedure definition script USP_UpdateRecurringTemplateForRecurringTransaction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateRecurringTemplateForRecurringTransaction(v_numAccountTypeMode SMALLINT,    
v_numPrimaryKeyId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numAccountTypeMode = 1 then
      Update General_Journal_Header Set numRecurringId = 0,dtLastRecurringDate = null Where numJOurnal_Id = v_numPrimaryKeyId And  numDomainId = v_numDomainId;
   ELSEIF v_numAccountTypeMode = 2
   then
      Update CheckDetails Set numRecurringId = 0,LastRecurringDate = null Where numCheckId = v_numPrimaryKeyId And numDomainId = v_numDomainId;
   ELSEIF v_numAccountTypeMode = 3
   then
      Update CashCreditCardDetails Set numRecurringId = 0,dtLastRecurringDate = null Where numCashCreditId = v_numPrimaryKeyId And numDomainID = v_numDomainId;
   ELSEIF v_numAccountTypeMode = 4
   then
      Update OpportunityRecurring Set numRecurringId = 0,dtLastRecurringDate = null Where numOppId = v_numPrimaryKeyId;
   end if;
   RETURN; --And numDomainId=@numDomainId  
END; $$;
/****** Object:  StoredProcedure [dbo].[Usp_UpdateScheduledOccurance]    Script Date: 07/26/2008 16:21:50 ******/



