CREATE OR REPLACE FUNCTION usp_GetCampaignPerformanceReport(v_numDomainID NUMERIC(9,0),
               v_tintCRMtype SMALLINT,
               v_dtStartDate TIMESTAMP,
               v_dtEndDate   TIMESTAMP, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Val  INTEGER;
BEGIN
   v_Val := ROUND(CAST(CAST(DATE_PART('day',v_dtEndDate -v_dtStartDate) AS DOUBLE PRECISION)/30 AS NUMERIC),0);--		SELECT ROUND(CONVERT(float,75)/30,0)
   IF coalesce(v_Val,0) = 0 then
      v_Val := 1;
   end if;
		--PRINT @Val
	
		-- Online Campaign, Will work based on Dates provided 
   open SWV_RefCur for
   SELECT 1;
    
    --Offline Campaign, Will not consider date.. It will show performance of whole time period.
   DROP TABLE IF EXISTS tt_TEMPBILL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPBILL AS
      SELECT numCampaignId,SUM(monAmount) AS monAmount  FROM BillHeader BH JOIN BillDetails BD
      ON BD.numBillID = BH.numBillID WHERE BH.numDomainId = v_numDomainID
      and coalesce(numCampaignId,0) > 0 GROUP BY numCampaignId;
																
   open SWV_RefCur2 for
   SELECT DISTINCT CM.numCampaignID,
                    vcCampaignName AS CampaignName,
                    CASE WHEN CM.bitIsMonthly = true THEN(coalesce(t.monAmount,0)*v_Val)
   ELSE coalesce(t.monAmount,0)
   END AS  monCampaignCost,
                    CAST(GetRecordsCountforCampaign(CM.numCampaignID,v_dtStartDate,v_dtEndDate,0::SMALLINT,v_tintCRMtype) AS DECIMAL(9,0)) AS RecordsCreated,
                    CASE v_tintCRMtype
   WHEN 2 THEN coalesce(GetIncomeforCampaign(CM.numCampaignID,2::SMALLINT,v_dtStartDate,v_dtEndDate),
      0)
   ELSE 0
   END AS AmtFromOrders,
                    CASE v_tintCRMtype
   WHEN 2 THEN coalesce(GetIncomeforCampaign(CM.numCampaignID,4::SMALLINT,v_dtStartDate,v_dtEndDate),
      0)
   ELSE 0
   END AS ROI,
                    CM.intLaunchDate,
                    CM.intEndDate
   FROM     CampaignMaster CM LEFT JOIN tt_TEMPBILL t ON CM.numCampaignID = t.numCampaignId
   WHERE    CM.numDomainID = v_numDomainID
   AND CM.bitIsOnline <> true
   AND CM.bitActive = true -- bug fix 1440
   ORDER BY CampaignName;
   RETURN;
END; $$;



