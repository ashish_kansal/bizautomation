-- Stored procedure definition script USP_RevertDetailsOpp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RevertDetailsOpp(v_numOppId NUMERIC(18,0),
v_numOppItemId NUMERIC(18,0),
v_tintMode SMALLINT DEFAULT 0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
v_numUserCntID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql ---reverting back to previous state if deal is being edited
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);
   v_tintCommitAllocation  SMALLINT;
   v_OppType  VARCHAR(2);              
   v_itemcode  NUMERIC;       
   v_numWareHouseItemID  NUMERIC;
   v_numWLocationID  NUMERIC(18,0);                     
   v_numToWarehouseItemID  NUMERIC; 
   v_numUnits  DOUBLE PRECISION;                                              
   v_onHand  DOUBLE PRECISION;                                            
   v_onOrder  DOUBLE PRECISION;                                            
   v_onBackOrder  DOUBLE PRECISION;                                              
   v_onAllocation  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_numUnitHourReceived  DOUBLE PRECISION;
   v_numDeletedReceievedQty  DOUBLE PRECISION;
   v_numoppitemtCode  NUMERIC(9,0); 
   v_monAmount  DECIMAL(20,5); 
   v_monAvgCost  DECIMAL(20,5);   
   v_Kit  BOOLEAN;                                        
   v_bitKitParent  BOOLEAN;
   v_bitStockTransfer  BOOLEAN; 
   v_numOrigUnits  DOUBLE PRECISION;			
   v_description  VARCHAR(100);
   v_bitWorkOrder  BOOLEAN;
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
   v_numRentalIN  DOUBLE PRECISION;
   v_numRentalOut  DOUBLE PRECISION;
   v_numRentalLost  DOUBLE PRECISION;
   v_bitAsset  BOOLEAN;
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numTempOIRLID  NUMERIC(18,0);
   v_numTempOnHand  DOUBLE PRECISION;
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempUnitReceieved  DOUBLE PRECISION;
   v_numTempDeletedReceievedQty  DOUBLE PRECISION;
		
			--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
   v_numDiff  NUMERIC;
   SWV_RowCount INTEGER;
BEGIN
   select   OM.numDomainId, coalesce(tintCommitAllocation,1) INTO v_numDomain,v_tintCommitAllocation FROM OpportunityMaster AS OM INNER JOIN Domain D ON OM.numDomainId = D.numDomainId WHERE OM.numOppId = v_numOppId;
    
	
	--end sachin

    						
   select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numWarehouseItmsID,0), coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true
   AND bitAssembly = true THEN false
   WHEN bitKitParent = true THEN true
   ELSE false
   END), coalesce(monTotAmount,0)*(CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END), coalesce(monAverageCost,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numDeletedReceievedQty,0), coalesce(bitKitParent,false), OI.numToWarehouseItemID, coalesce(OM.bitStockTransfer,false), tintopptype, coalesce(OI.numRentalIN,0), coalesce(OI.numRentalOut,0), coalesce(OI.numRentalLost,0), coalesce(I.bitAsset,false), coalesce(OI.bitWorkOrder,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numWareHouseItemID,v_numWLocationID,
   v_Kit,v_monAmount,v_monAvgCost,v_numQtyShipped,v_numUnitHourReceived,
   v_numDeletedReceievedQty,v_bitKitParent,v_numToWarehouseItemID,
   v_bitStockTransfer,v_OppType,v_numRentalIN,v_numRentalOut,v_numRentalLost,
   v_bitAsset,v_bitWorkOrder FROM    OpportunityItems OI
   LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
   JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
   JOIN Item I ON OI.numItemCode = I.numItemCode WHERE
		(coalesce(v_numOppItemId,0) = 0 OR OI.numoppitemtCode = v_numOppItemId)
   AND (charitemtype = 'P' OR 1 =(CASE WHEN tintopptype = 1 THEN
      CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
      WHEN coalesce(I.bitKitParent,false) = true THEN 1
      ELSE 0 END
   ELSE 0 END)) AND OI.numOppId = v_numOppId
   AND (bitDropShip = false
   OR bitDropShip IS NULL)   ORDER BY OI.numoppitemtCode LIMIT 1;

	
   WHILE v_numoppitemtCode > 0 LOOP
      v_numOrigUnits := v_numUnits;
      IF v_bitStockTransfer = true then
        
         v_OppType := CAST(1 AS VARCHAR(2));
      end if;
      IF v_numWareHouseItemID > 0 then
        
         select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWareHouseItemID;
      end if;
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 1 AND v_tintCommitAllocation = 1 then
        
         IF v_tintMode = 2 then
			
            v_description := CONCAT('SO DEMOTED TO OPPORTUNITY (Qty:',v_numUnits,' Shipped:',v_numQtyShipped,
            ')');
         ELSE
            v_description := CONCAT('SO Deleted (Qty:',v_numUnits,' Shipped:',v_numQtyShipped,')');
         end if;
         IF v_Kit = true then
			
            PERFORM USP_UpdatingInventoryForEmbeddedKits(v_numoppitemtCode,v_numOrigUnits,1::SMALLINT,v_numOppId,v_tintMode::SMALLINT,
            v_numUserCntID);
         ELSE
            IF v_numQtyShipped > 0 then
				
               IF v_tintMode = 0 then
                  v_numUnits := v_numUnits -v_numQtyShipped;
               ELSEIF v_tintMode = 1 OR v_tintMode = 2
               then
                  v_onAllocation := v_onAllocation+v_numQtyShipped;
               end if;
            end if;
            IF v_numUnits >= v_onBackOrder then
                
               v_numUnits := v_numUnits -v_onBackOrder;
               v_onBackOrder := 0;
               IF (v_onAllocation -v_numUnits >= 0) then
                  v_onAllocation := v_onAllocation -v_numUnits;
               end if;
               IF v_bitAsset = true then--Not Asset
					
                  v_onHand := v_onHand+v_numRentalIN+v_numRentalLost+v_numRentalOut;
               ELSE
                  v_onHand := v_onHand+v_numUnits;
               end if;
            ELSEIF v_numUnits < v_onBackOrder
            then
                
               IF (v_onBackOrder -v_numUnits > 0) then
                  v_onBackOrder := v_onBackOrder -v_numUnits;
               end if;
            end if;
            UPDATE
            WareHouseItems
            SET
            numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
            dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         end if;
         IF v_numWareHouseItemID > 0 AND v_description <> 'DO NOT ADD WAREHOUSE TRACKING' then
			
					 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppId,
            v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
            v_numDomainID := v_numDomain,SWV_RefCur := null);
         end if;
      end if;
      IF v_bitStockTransfer = true then
        
         v_numWareHouseItemID := v_numToWarehouseItemID;
         v_OppType := CAST(2 AS VARCHAR(2));
         v_numUnits := v_numOrigUnits;
         select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      end if;
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 2 then
        
         IF (v_tintMode = 1 OR v_tintMode = 2) AND coalesce(v_bitStockTransfer,false) = false AND EXISTS(SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomain AND numOppID = v_numOppId AND numOppItemID = v_numoppitemtCode) then
            DROP TABLE IF EXISTS tt_TEMPRECEIEVEDITEMS CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPRECEIEVEDITEMS
            (
               ID INTEGER,
               numOIRLID NUMERIC(18,0),
               numWarehouseItemID NUMERIC(18,0),
               numUnitReceieved DOUBLE PRECISION,
               numDeletedReceievedQty DOUBLE PRECISION
            );
            DELETE FROM tt_TEMPRECEIEVEDITEMS;
            INSERT INTO
            tt_TEMPRECEIEVEDITEMS
            SELECT
            ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					coalesce(numDeletedReceievedQty,0)
            FROM
            OpportunityItemsReceievedLocation
            WHERE
            numDomainId = v_numDomain
            AND numOppID = v_numOppId
            AND numOppItemID = v_numoppitemtCode;
            select   COUNT(*) INTO v_COUNT FROM tt_TEMPRECEIEVEDITEMS;
            WHILE v_i <= v_COUNT LOOP
               select   TRI.numOIRLID, coalesce(numOnHand,0), coalesce(TRI.numWarehouseItemID,0), coalesce(numUnitReceieved,0), coalesce(numDeletedReceievedQty,0) INTO v_numTempOIRLID,v_numTempOnHand,v_numTempWarehouseItemID,v_numTempUnitReceieved,
               v_numTempDeletedReceievedQty FROM
               tt_TEMPRECEIEVEDITEMS TRI
               INNER JOIN
               WareHouseItems WI
               ON
               TRI.numWarehouseItemID = WI.numWareHouseItemID WHERE
               ID = v_i;
               IF v_numTempOnHand >=(v_numTempUnitReceieved -v_numTempDeletedReceievedQty) then
					
                  UPDATE
                  WareHouseItems
                  SET
                  numOnHand = coalesce(numOnHand,0) -(v_numTempUnitReceieved -v_numTempDeletedReceievedQty),dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numTempWarehouseItemID;
                  UPDATE
                  OpportunityItems
                  SET
                  numDeletedReceievedQty = coalesce(numDeletedReceievedQty,0)+(v_numTempUnitReceieved -v_numTempDeletedReceievedQty)
                  WHERE
                  numoppitemtCode = v_numoppitemtCode;
               ELSE
                  RAISE NOTICE 'INSUFFICIENT_ONHAND_QTY';
                  RETURN;
               end if;
               IF v_tintMode = 2 then
					
                  v_description := CONCAT('PO DEMOTED TO OPPORTUNITY (Total Qty:',v_numUnits,' Deleted Qty: ',(v_numTempUnitReceieved -v_numTempDeletedReceievedQty),
                  ' Received:',v_numTempUnitReceieved,
                  ')');
               ELSE
                  v_description := CONCAT('PO Deleted (Total Qty:',v_numUnits,' Deleted Qty: ',(v_numTempUnitReceieved -v_numTempDeletedReceievedQty),
                  ' Received:',v_numTempUnitReceieved,
                  ')');
               end if;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppId,
               v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,
               v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomain,SWV_RefCur := null);
               DELETE FROM OpportunityItemsReceievedLocation WHERE ID = v_numTempOIRLID;
               v_i := v_i::bigint+1;
            END LOOP;
         end if;
		
			--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
         select   coalesce(numDeletedReceievedQty,0) INTO v_numDeletedReceievedQty FROM OpportunityItems WHERE	numoppitemtCode = v_numoppitemtCode;
         IF v_tintMode = 2 then
			
            v_description := CONCAT('PO DEMOTED TO OPPORTUNITY (Qty:',v_numUnits,' Received:',v_numUnitHourReceived,
            ' Deleted:',v_numDeletedReceievedQty,')');
         ELSE
            v_description := CONCAT('PO Deleted (Qty:',v_numUnits,' Received:',v_numUnitHourReceived,' Deleted:',
            v_numDeletedReceievedQty,')');
         end if;

			--Partial Fulfillment
         IF (v_tintMode = 1 OR v_tintMode = 2) and  v_onHand >=(v_numUnitHourReceived -v_numDeletedReceievedQty) then
			
            v_onHand := v_onHand -(v_numUnitHourReceived -v_numDeletedReceievedQty);
         end if;
         v_numUnits := v_numUnits -v_numUnitHourReceived;
         IF(v_onOrder -v_numUnits) >= 0 then
			
				--Causing Negative Inventory Bug ID:494
            v_onOrder := v_onOrder -v_numUnits;
         ELSEIF(v_onHand+v_onOrder) -v_numUnits >= 0
         then
			
            v_onHand := v_onHand -(v_numUnits -v_onOrder);
            v_onOrder := 0;
         ELSEIF(v_onHand+v_onOrder+v_onAllocation) -v_numUnits >= 0
         then
            v_numDiff := v_numUnits -v_onOrder;
            v_onOrder := 0;
            v_numDiff := v_numDiff -v_onHand;
            v_onHand := 0;
            v_onAllocation := v_onAllocation -v_numDiff;
            v_onBackOrder := v_onBackOrder+v_numDiff;
         end if;
         UPDATE
         WareHouseItems
         SET
         numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
         numonOrder = v_onOrder,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
			 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
         IF (v_tintMode = 1 OR v_tintMode = 2) then
			
            UPDATE
            OpportunityItems
            SET
            numUnitHourReceived = 0,numDeletedReceievedQty = 0
            WHERE
            numoppitemtCode = v_numoppitemtCode;
         end if;
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppId,
         v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
         v_numDomainID := v_numDomain,SWV_RefCur := null);
      end if;
      select   numoppitemtCode, OI.numItemCode, numUnitHour, coalesce(numWarehouseItmsID,0), coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true
      AND bitAssembly = true THEN false
      WHEN bitKitParent = true THEN true
      ELSE false
      END), coalesce(monTotAmount,0)*(CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END), monAverageCost, coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numDeletedReceievedQty,0), coalesce(bitKitParent,false), OI.numToWarehouseItemID, coalesce(OM.bitStockTransfer,false), tintopptype, coalesce(OI.numRentalIN,0), coalesce(OI.numRentalOut,0), coalesce(OI.numRentalLost,0), coalesce(I.bitAsset,false), coalesce(OI.bitWorkOrder,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numWareHouseItemID,v_numWLocationID,
      v_Kit,v_monAmount,v_monAvgCost,v_numQtyShipped,v_numUnitHourReceived,
      v_numDeletedReceievedQty,v_bitKitParent,v_numToWarehouseItemID,
      v_bitStockTransfer,v_OppType,v_numRentalIN,v_numRentalOut,v_numRentalLost,
      v_bitAsset,v_bitWorkOrder FROM    OpportunityItems OI
      LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
      JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      JOIN Item I ON OI.numItemCode = I.numItemCode WHERE
			(coalesce(v_numOppItemId,0) = 0 OR OI.numoppitemtCode = v_numOppItemId)
      AND (charitemtype = 'P' OR 1 =(CASE WHEN tintopptype = 1 THEN
         CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
         WHEN coalesce(I.bitKitParent,false) = true THEN 1
         ELSE 0 END
      ELSE 0 END))
      AND OI.numOppId = v_numOppId
      AND OI.numoppitemtCode > v_numoppitemtCode
      AND (bitDropShip = false
      OR bitDropShip IS NULL)   ORDER BY OI.numoppitemtCode LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numoppitemtCode := 0;
      end if;
   END LOOP;
END; $$;


