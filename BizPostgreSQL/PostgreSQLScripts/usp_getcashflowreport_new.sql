-- Stored procedure definition script USP_GetCashFlowReport_New for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCashFlowReport_New(v_numDomainId INTEGER,
	v_dtFromDate TIMESTAMP,
	v_dtToDate TIMESTAMP,
	v_ClientTimeZoneOffset INTEGER,
	v_numAccountClass NUMERIC(9,0) DEFAULT 0,                                           
	v_DateFilter VARCHAR(20) DEFAULT NULL,
	v_ReportColumn VARCHAR(20) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PLCHARTID  NUMERIC(18,0);
   v_FinancialYearStartDate  TIMESTAMP;
   v_columns  VARCHAR(8000);--SET @columns = '';
   v_SUMColumns  VARCHAR(8000);--SET @SUMColumns = '';
   v_PivotColumns  VARCHAR(8000);--SET @PivotColumns = '';
   v_Select  VARCHAR(8000) DEFAULT 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc';
   v_Where  VARCHAR(8000);
   v_Update6  TEXT DEFAULT '';
   v_Update8  TEXT DEFAULT '';
   v_sql  TEXT DEFAULT '';
BEGIN
   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL
   (
      numAccountId NUMERIC(18,0),
      datEntry_Date TIMESTAMP,
      vcAccountCode VARCHAR(300),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

   INSERT INTO
   tt_VIEW_JOURNAL
   SELECT
   numAccountId
		,datEntry_Date
		,vcAccountCode
		,Debit
		,Credit
   FROM
   VIEW_JOURNAL
   WHERE
   numDomainID = v_numDomainId
   AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   select   numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND bitProfitLoss = true    LIMIT 1;

   v_FinancialYearStartDate := GetFiscalStartDate(EXTRACT(YEAR FROM LOCALTIMESTAMP),v_numDomainId::NUMERIC);

   DROP TABLE IF EXISTS tt_TEMPQUARTER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPQUARTER
   (
      ID INTEGER,
      StartDate TIMESTAMP,
      EndDate TIMESTAMP
   );

   INSERT INTO tt_TEMPQUARTER  VALUES(1,v_FinancialYearStartDate,v_FinancialYearStartDate+INTERVAL '1 '+INTERVAL '.-3 ');
	
   INSERT INTO tt_TEMPQUARTER  VALUES(2,v_FinancialYearStartDate+INTERVAL '1 ',v_FinancialYearStartDate+INTERVAL '2 '+INTERVAL '.-3 ');
	
   INSERT INTO tt_TEMPQUARTER  VALUES(3,v_FinancialYearStartDate+INTERVAL '2 ',v_FinancialYearStartDate+INTERVAL '3 '+INTERVAL '.-3 ');
	
   INSERT INTO tt_TEMPQUARTER  VALUES(4,v_FinancialYearStartDate+INTERVAL '3 ',v_FinancialYearStartDate+INTERVAL '4 '+INTERVAL '.-3 ');

	
   IF v_DateFilter = 'CurYear' then
	
      v_dtFromDate := v_FinancialYearStartDate;
      v_dtToDate := v_FinancialYearStartDate+INTERVAL '1 year'+INTERVAL '.-3 ';
   ELSEIF v_DateFilter = 'PreYear'
   then
	
      v_dtFromDate := v_FinancialYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_FinancialYearStartDate+INTERVAL '-1 year'+INTERVAL '1 year'+INTERVAL '.-3 ';
   ELSEIF v_DateFilter = 'CurPreYear'
   then
	
      v_dtFromDate := v_FinancialYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_FinancialYearStartDate+INTERVAL '1 year'+INTERVAL '.-3 ';
   ELSEIF v_DateFilter = 'CuQur'
   then
	
      select   StartDate, EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP  BETWEEN StartDate AND EndDate;
   ELSEIF v_DateFilter = 'CurPreQur'
   then
	
      select   StartDate+INTERVAL '-1 ', EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP  BETWEEN StartDate AND EndDate;
   ELSEIF v_DateFilter = 'PreQur'
   then
	
      select   StartDate+INTERVAL '-1 ', EndDate+INTERVAL '-1 ' INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTER WHERE LOCALTIMESTAMP  BETWEEN StartDate AND EndDate;
   ELSEIF v_DateFilter = 'LastMonth'
   then
	
      v_dtFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '-1 month';
      v_dtToDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '0 month'+INTERVAL '.-3 ';
   ELSEIF v_DateFilter = 'ThisMonth'
   then
	
      v_dtFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval);
      v_dtToDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE)))+1 || 'month' as interval)+INTERVAL '0 month'+INTERVAL '.- 3 ';
   ELSEIF v_DateFilter = 'CurPreMonth'
   then
	
      v_dtFromDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval)+INTERVAL '-1 month';
      v_dtToDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE)))+1 || 'month' as interval)+INTERVAL '0 month'+INTERVAL '.- 3 ';
   end if;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ParentId VARCHAR(300),
      vcCompundParentKey VARCHAR(300),
      numAccountTypeID NUMERIC(18,0),
      numAccountID NUMERIC(18,0),
      vcAccountType VARCHAR(300),
      vcAccountCode VARCHAR(300),
      LEVEL INTEGER,
      Struc VARCHAR(300),
      bitTotal BOOLEAN,
      Type INTEGER
   );

   INSERT INTO tt_TEMP(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type)
	VALUES(CAST('' AS VARCHAR(300)),CAST('-1' AS VARCHAR(300)),-1,CAST('Operating Activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-1' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),(CAST('-1' AS VARCHAR(300)),CAST('-2' AS VARCHAR(300)),-2,CAST('Net Income' AS VARCHAR(300)),1,NULL,NULL,CAST('-1#2' AS VARCHAR(300)),CAST(0 AS BOOLEAN),1),(CAST('-1' AS VARCHAR(300)),CAST('-3' AS VARCHAR(300)),-3,CAST('Adjustments to reconcile Net Income to Net Cash provided by operations' AS VARCHAR(300)),1,NULL,NULL,CAST('-1#3' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),(CAST('-1' AS VARCHAR(300)),CAST('-3' AS VARCHAR(300)),-3,CAST('Total Adjustments to reconcile Net Income to Net Cash provided by operations' AS VARCHAR(300)),1,NULL,NULL,CAST('-1#3#Total' AS VARCHAR(300)),CAST(1 AS BOOLEAN),2),
   (CAST('' AS VARCHAR(300)),CAST('-1' AS VARCHAR(300)),-1,CAST('Net cash provided by operating activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-1#Total' AS VARCHAR(300)),CAST(1 AS BOOLEAN),2),(CAST('' AS VARCHAR(300)),CAST('-4' AS VARCHAR(300)),-4,CAST('Investing Activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-4' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),
   (CAST('' AS VARCHAR(300)),CAST('-4' AS VARCHAR(300)),-4,CAST('Net cash provided by investing activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-4#Total' AS VARCHAR(300)),CAST(1 AS BOOLEAN),2),
   (CAST('' AS VARCHAR(300)),CAST('-5' AS VARCHAR(300)),-5,CAST('Financing Activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-5' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),(CAST('' AS VARCHAR(300)),CAST('-5' AS VARCHAR(300)),-5,CAST('Net cash provided by financing activities' AS VARCHAR(300)),0,NULL,NULL,CAST('-5#Total' AS VARCHAR(300)),CAST(1 AS BOOLEAN),2),
   (CAST('' AS VARCHAR(300)),CAST('-6' AS VARCHAR(300)),-6,CAST('Net cash increase for period' AS VARCHAR(300)),0,NULL,NULL,CAST('-6' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),(CAST('' AS VARCHAR(300)),CAST('-7' AS VARCHAR(300)),-7,CAST('Net cash at Beginning of Period' AS VARCHAR(300)),0,NULL,NULL,CAST('-7' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2),
   (CAST('' AS VARCHAR(300)),CAST('-8' AS VARCHAR(300)),-8,CAST('Net cash at End of Period' AS VARCHAR(300)),0,NULL,NULL,CAST('-8' AS VARCHAR(300)),CAST(0 AS BOOLEAN),2);

	-- Operating Activities
	
   INSERT INTO tt_TEMP(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type) with recursive DirectReport(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,
   vcAccountCode,Struc,bitTotal,Type)
   AS(SELECT
   CAST('-3' AS VARCHAR(30)) AS ParentId
			,CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey
			,CAST(null as NUMERIC(18,0)) AS numAccountTypeID
			,vcAccountName AS vcAccountType
			,CAST(2 AS INTEGER) AS LEVEL
			,numAccountId AS numAccountID
			,vcAccountCode AS vcAccountCode
			,CAST(CONCAT('-1#3#',numAccountId) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = false
   AND (vcAccountCode ilike '01010105%' OR vcAccountCode ilike '01010104%' OR vcAccountCode ilike '010105%' OR vcAccountCode ilike '01020102%' OR vcAccountCode ilike '01020101%')
   UNION ALL
   SELECT
   CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numParentAccId) AS VARCHAR(30)) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey,
			CAST(null as NUMERIC(18,0)) AS numAccountTypeID,
			vcAccountName AS vcAccountType,
			D.LEVEL+1 AS LEVEL,
			COA.numAccountId AS numAccountID,
			COA.vcAccountCode AS vcAccountCode,
			CAST(D.Struc || '#' || CAST(COA.numAccountId AS VARCHAR(30)) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   JOIN
   DirectReport D
   ON
   D.numAccountId = COA.numParentAccId
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = true
   AND (COA.vcAccountCode ilike '01010105%' OR COA.vcAccountCode ilike '01010104%' OR COA.vcAccountCode ilike '010105%' OR COA.vcAccountCode ilike '01020102%' OR COA.vcAccountCode ilike '01020101%')) SELECT
   ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type
   FROM
   DirectReport;

	-- Investing Activities
   INSERT INTO tt_TEMP(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type) with recursive DirectReport(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,
   vcAccountCode,Struc,bitTotal,Type)
   AS(SELECT
   CAST('-4' AS VARCHAR(30)) AS ParentId
			,CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey
			,CAST(null as NUMERIC(18,0)) AS numAccountTypeID
			,vcAccountName AS vcAccountType
			,CAST(1 AS INTEGER) AS LEVEL
			,numAccountId AS numAccountID
			,vcAccountCode AS vcAccountCode
			,CAST(CONCAT('-4#',numAccountId) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = false
   AND vcAccountCode ilike '010102%'
   UNION ALL
   SELECT
   CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numParentAccId) AS VARCHAR(30)) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey,
			CAST(null as NUMERIC(18,0)) AS numAccountTypeID,
			vcAccountName AS vcAccountType,
			LEVEL::bigint+1 AS LEVEL,
			COA.numAccountId AS numAccountID,
			COA.vcAccountCode AS vcAccountCode,
			CAST(d.Struc || '#' || CAST(COA.numAccountId AS VARCHAR(30)) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   JOIN
   DirectReport d
   ON
   d.numAccountId = COA.numParentAccId
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = true
   AND COA.vcAccountCode ilike '010102%') SELECT
   ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type
   FROM
   DirectReport;

	-- Financing Activities
   INSERT INTO tt_TEMP(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type) with recursive DirectReport(ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,
   vcAccountCode,Struc,bitTotal,Type)
   AS(SELECT
   CAST('-5'  AS VARCHAR(30)) AS ParentId
			,CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey
			,CAST(null as NUMERIC(18,0)) AS numAccountTypeID
			,vcAccountName AS vcAccountType
			,CAST(1 AS INTEGER) AS LEVEL
			,numAccountId AS numAccountID
			,vcAccountCode AS vcAccountCode
			,CAST(CONCAT('-5#',numAccountId) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = false
   AND vcAccountCode ilike '010202%'
   UNION ALL
   SELECT
   CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numParentAccId) AS VARCHAR(30)) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeID,'#',COA.numAccountId) AS VARCHAR(30)) AS vcCompundParentKey,
			CAST(null as NUMERIC(18,0)) AS numAccountTypeID,
			vcAccountName AS vcAccountType,
			LEVEL::bigint+1 AS LEVEL,
			COA.numAccountId AS numAccountID,
			COA.vcAccountCode AS vcAccountCode,
			CAST(d.Struc || '#' || CAST(COA.numAccountId AS VARCHAR(30)) AS VARCHAR(300)) AS Struc
			,false AS bitTotal
			,1 AS Type
   FROM
   Chart_Of_Accounts AS COA
   JOIN
   DirectReport d
   ON
   d.numAccountId = COA.numParentAccId
   WHERE
   COA.numDomainId = v_numDomainId
   AND coalesce(COA.bitIsSubAccount,false) = true
   AND COA.vcAccountCode ilike '010202%') SELECT
   ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,numAccountID,vcAccountCode,Struc,bitTotal,Type
   FROM
   DirectReport;

   IF v_ReportColumn = 'Year' then
	
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTYEAR CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTYEAR AS
         SELECT * FROM
         tt_TEMP;
      DROP TABLE IF EXISTS tt_TEMPYEARMONTH CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTH
      (
         intYear INTEGER,
         intMonth INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         NetIncomeAmount DECIMAL(20,5),
         CashAtBegining DECIMAL(20,5)
      );
      INSERT INTO
      tt_TEMPYEARMONTH   with recursive CTE AS(SELECT
      EXTRACT(YEAR FROM v_dtFromDate) AS yr,
				EXTRACT(MONTH FROM v_dtFromDate) AS mm,
				TO_CHAR(v_dtFromDate,'MMM') AS mon,
				(TO_CHAR(v_dtFromDate,'MMM') || '_' || CAST(EXTRACT(YEAR FROM v_dtFromDate) AS VARCHAR(30))) AS MonthYear,
				CAST(DATEFROMPARTS(EXTRACT(YEAR FROM v_dtFromDate),EXTRACT(MONTH FROM v_dtFromDate),
      1) AS TIMESTAMP) AS dtStartDate,
				CAST(DATEFROMPARTS(EXTRACT(YEAR FROM v_dtFromDate),EXTRACT(MONTH FROM v_dtFromDate),
      1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '.-2 ' AS dtEndDate,
				v_dtFromDate AS new_date
      UNION ALL
      SELECT
      EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS yr,
				EXTRACT(MONTH FROM new_date+INTERVAL '1 day') AS mm,
				TO_CHAR(new_date+INTERVAL '1 day','MMM') AS mon,
				(TO_CHAR(new_date+INTERVAL '1 day','MMM') || '_' || CAST(EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS VARCHAR(30))) AS MonthYear,
				CAST(DATEFROMPARTS(EXTRACT(YEAR FROM new_date+INTERVAL '1 day'),EXTRACT(MONTH FROM new_date+INTERVAL '1 day'),
      1) AS TIMESTAMP) AS dtStartDate,
				CAST(DATEFROMPARTS(EXTRACT(YEAR FROM new_date+INTERVAL '1 day'),EXTRACT(MONTH FROM new_date+INTERVAL '1 day'),
      1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '.-2 ' AS dtEndDate,
				new_date+INTERVAL '1 day' AS new_date
      FROM CTE
      WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
      yr,mm,MonthYear,dtStartDate,dtEndDate,0,0
      FROM
      CTE
      GROUP BY
      yr,mm,MonthYear,dtStartDate,dtEndDate
      ORDER BY
      yr,mm;
      INSERT INTO tt_TEMPYEARMONTH  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0,0);
		
      UPDATE
      tt_TEMPYEARMONTH
      SET
      EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      UPDATE
      tt_TEMPYEARMONTH
      SET
      NetIncomeAmount = coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
											((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date BETWEEN StartDate AND EndDate),
      0)+coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
																												((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date <  StartDate),0),CashAtBegining = coalesce((SELECT
      coalesce(SUM(Credit),0) -coalesce(SUM(Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
      vcAccountCode ilike '01010101%'
      AND datentry_date < StartDate),0);
      v_columns := OVERLAY((SELECT ', ISNULL(' || MonthYear  || ',0) AS [' || REPLACE(MonthYear,'_',' ') || ']' FROM tt_TEMPYEARMONTH ORDER BY intYear,intMonth) placing '' from 1 for 2);
      v_SUMColumns := OVERLAY((SELECT ', SUM(ISNULL(V.[' || REPLACE(MonthYear,'_',' ') || ']' || ',0)) AS [' || REPLACE(MonthYear,'_',' ') || ']' FROM tt_TEMPYEARMONTH ORDER BY intYear,intMonth) placing '' from 1 for 2);
      v_Update6 := CONCAT('UPDATE #tempFinalDataYear1 SET ',OVERLAY((SELECT ', [' || REPLACE(MonthYear,'_',' ') || '] = ISNULL((SELECT SUM(TInner.[' || REPLACE(MonthYear,'_',' ') || ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=true),0)' FROM tt_TEMPYEARMONTH ORDER BY intYear,intMonth) placing '' from 1 for 2),' WHERE numAccountTypeID=-6');
      v_Update8 := CONCAT('UPDATE #tempFinalDataYear1 SET ',OVERLAY((SELECT ', [' || REPLACE(MonthYear,'_',' ') || '] = ISNULL((SELECT SUM(TInner.[' || REPLACE(MonthYear,'_',' ') || ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM tt_TEMPYEARMONTH ORDER BY intYear,intMonth) placing '' from 1 for 2),
      ' WHERE numAccountTypeID=-8');
      v_PivotColumns := OVERLAY((SELECT ', ' || MonthYear FROM tt_TEMPYEARMONTH ORDER BY intYear,intMonth) placing '' from 1 for 2);
      v_sql := 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' || coalesce(v_columns,'') || '
					INTO
						#tempFinalDataYear
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportYear T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#tempYearMonth
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' || coalesce(v_PivotColumns,'') || ' )
					) AS P; ';
      v_sql := CONCAT(v_sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',v_SUMColumns,' INTO #tempFinalDataYear1
								FROM 
									#tempDirectReportYear COA 
								LEFT JOIN 
									#tempFinalDataYear V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',v_Update6,
      ';',v_Update8,'; SELECT * FROM #tempFinalDataYear1; DROP TABLE #tempFinalDataYear; DROP TABLE #tempFinalDataYear1;');
      RAISE NOTICE '%',CAST(v_sql AS TEXT);
      EXECUTE v_sql;
      DROP TABLE IF EXISTS tt_TEMPYEARMONTH CASCADE;
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTYEAR CASCADE;
   ELSEIF v_ReportColumn = 'Quarter'
   then
	
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTQUARTER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTQUARTER AS
         SELECT * FROM
         tt_TEMP;
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHQUARTER CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTHQUARTER
      (
         intYear INTEGER,
         intQuarter INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         NetIncomeAmount DECIMAL(20,5),
         CashAtBegining DECIMAL(20,5)
      );
      INSERT INTO
      tt_TEMPYEARMONTHQUARTER   with recursive CTE AS(SELECT
      GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0)) AS yr,
				GetFiscalQuarter(v_dtFromDate,v_numDomainId::NUMERIC(4,0)) AS qq,
				('Q' || cast(GetFiscalQuarter(v_dtFromDate,v_numDomainId::NUMERIC(4,0)) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0)) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId::NUMERIC(4,0))
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId::NUMERIC(4,0))
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '3 month'+INTERVAL '.-3 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '6 month'+INTERVAL '.-3 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '9 month'+INTERVAL '.-3 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '12 month'+INTERVAL '.-3 '
      END AS dtEndDate,
				v_dtFromDate AS new_date
      UNION ALL
      SELECT
      GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0)) AS yr,
				GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(4,0)) AS qq,
				('Q' || cast(GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(4,0)) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0)) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(4,0))
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(4,0))
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '3 month'+INTERVAL '.-3 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '6 month'+INTERVAL '.-3 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '9 month'+INTERVAL '.-3 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId::NUMERIC(9,0))::INTEGER,v_numDomainId::NUMERIC)+INTERVAL '12 month'+INTERVAL '.-3 '
      END AS dtEndDate,
				new_date+INTERVAL '1 day' AS new_date
      FROM CTE
      WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
      yr,qq,MonthYear,dtStartDate,dtEndDate,0,0
      FROM
      CTE
      GROUP BY
      yr,qq,MonthYear,dtStartDate,dtEndDate
      ORDER BY
      yr,qq;
      INSERT INTO tt_TEMPYEARMONTHQUARTER  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0,0);
		
      UPDATE
      tt_TEMPYEARMONTHQUARTER
      SET
      EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      UPDATE
      tt_TEMPYEARMONTHQUARTER
      SET
      NetIncomeAmount = coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
											((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date BETWEEN StartDate AND EndDate),
      0)+coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
																												((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date <  StartDate),0),CashAtBegining = coalesce((SELECT
      coalesce(SUM(Credit),0) -coalesce(SUM(Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
      vcAccountCode ilike '01010101%'
      AND datentry_date < StartDate),0);
      v_columns := OVERLAY((SELECT ', ISNULL(' || MonthYear  || ',0) AS [' || REPLACE(MonthYear,'_',' ') || ']' FROM tt_TEMPYEARMONTHQUARTER ORDER BY intYear,intQuarter) placing '' from 1 for 2);
      v_SUMColumns := OVERLAY((SELECT ', SUM(ISNULL(V.[' || REPLACE(MonthYear,'_',' ') || ']' || ',0)) AS [' || REPLACE(MonthYear,'_',' ') || ']' FROM tt_TEMPYEARMONTHQUARTER ORDER BY intYear,intQuarter) placing '' from 1 for 2);
      v_Update6 := CONCAT('UPDATE #tempFinalDataQuarter1 SET ',OVERLAY((SELECT ', [' || REPLACE(MonthYear,'_',' ') || '] = ISNULL((SELECT SUM(TInner.[' || REPLACE(MonthYear,'_',' ') || ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=true),0)' FROM tt_TEMPYEARMONTHQUARTER ORDER BY intYear,intQuarter) placing '' from 1 for 2),' WHERE numAccountTypeID=-6');
      v_Update8 := CONCAT('UPDATE #tempFinalDataQuarter1 SET ',OVERLAY((SELECT ', [' || REPLACE(MonthYear,'_',' ') || '] = ISNULL((SELECT SUM(TInner.[' || REPLACE(MonthYear,'_',' ') || ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM tt_TEMPYEARMONTHQUARTER ORDER BY intYear,intQuarter) placing '' from 1 for 2),' WHERE numAccountTypeID=-8');
      v_PivotColumns := OVERLAY((SELECT ', ' || MonthYear FROM tt_TEMPYEARMONTHQUARTER ORDER BY intYear,intQuarter) placing '' from 1 for 2);
      v_sql := 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' || coalesce(v_columns,'') || '
					INTO
						#tempFinalDataQuarter
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportQuarter T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#TempYearMonthQuarter
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' || coalesce(v_PivotColumns,'') || ' )
					) AS P; ';
      v_sql := CONCAT(v_sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',v_SUMColumns,' INTO #tempFinalDataQuarter1
								FROM 
									#tempDirectReportQuarter COA 
								LEFT JOIN 
									#tempFinalDataQuarter V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',
      v_Update6,';',v_Update8,'; SELECT * FROM #tempFinalDataQuarter1; DROP TABLE #tempFinalDataQuarter; DROP TABLE #tempFinalDataQuarter1;');
      RAISE NOTICE '%',CAST(v_sql AS TEXT);
      EXECUTE v_sql;
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHQUARTER CASCADE;
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTQUARTER CASCADE;
   ELSE
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTMAIN CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTMAIN AS
         SELECT
         COA.ParentId
			,COA.vcCompundParentKey
			,COA.numAccountTypeID
			,COA.vcAccountType
			,COA.LEVEL
			,COA.vcAccountCode
			,COA.numAccountID
			,COA.Struc
			,COA.Type
			,COA.bitTotal
			,0 AS Total
		
         FROM
         tt_TEMP COA;
      UPDATE
      tt_TEMPDIRECTREPORTMAIN T
      SET
      Total = coalesce(TCurrent.monAmount,0) -coalesce(TOpening.monAmount,0)
      FROM(SELECT
      coalesce(SUM(VF.Credit),0) -coalesce(SUM(VF.Debit),0) AS monAmount
      FROM
      tt_VIEW_JOURNAL VF
      WHERE
      VF.numaccountid = T.numAccountId
      AND VF.datentry_date < v_dtFromDate) TOpening
      WHERE
      T.numAccountId IS NOT NULL;
      UPDATE
      tt_TEMPDIRECTREPORTMAIN
      SET
      Total = coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
								((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date BETWEEN v_dtFromDate AND v_dtToDate),0)+coalesce((SELECT
      coalesce(SUM(VJ.Credit),0) -coalesce(SUM(VJ.Debit),0)
      FROM
      tt_VIEW_JOURNAL VJ
      WHERE
																									((VJ.vcAccountCode  ilike '0103%' OR VJ.vcAccountCode ilike '0104%' OR VJ.vcAccountCode ilike '0106%') OR VJ.numaccountid = v_PLCHARTID)
      AND VJ.datentry_date <  v_dtFromDate),
      0)
      WHERE
      numAccountTypeID = -2;
      DROP TABLE IF EXISTS tt_TEMPFINALDATAMAIN CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINALDATAMAIN ON COMMIT DROP AS
         SELECT
         ParentId
			,vcCompundParentKey
			,numAccountTypeID
			,vcAccountType
			,LEVEL
			,vcAccountCode
			,numAccountId
			,('#' || Struc || '#') AS Struc
			,Type
			,bitTotal
			,Total
		
         FROM(SELECT
         COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountID
				,COA.Struc
				,COA.Type
				,COA.bitTotal
				,SUM(coalesce(V.Total,0)) AS Total
         FROM
         tt_TEMP COA
         LEFT JOIN
         tt_TEMPDIRECTREPORTMAIN V
         ON
         V.Struc ilike REPLACE(COA.Struc,'#Total','') || '%'
         GROUP BY
         COA.ParentId,COA.vcCompundParentKey,COA.numAccountTypeID,COA.vcAccountType,
         COA.LEVEL,COA.vcAccountCode,COA.numAccountID,COA.Struc,COA.Type,COA.bitTotal,
         COA.LEVEL) AS t
         ORDER BY
         Struc;
      UPDATE
      tt_TEMPFINALDATAMAIN
      SET
      Total = coalesce((SELECT SUM(Total) FROM tt_TEMPFINALDATAMAIN TInner WHERE TInner.numAccountTypeID IN(-1,-4,-5) AND bitTotal = true),0)
      WHERE
      numAccountTypeID = -6;
      UPDATE
      tt_TEMPFINALDATAMAIN
      SET
      Total = coalesce((SELECT
      coalesce(SUM(Credit),0) -coalesce(SUM(Debit),0)
      FROM
      tt_VIEW_JOURNAL vj
      WHERE
      vcAccountCode ilike '01010101%'
      AND datentry_date < v_dtFromDate),
      0)
      WHERE
      numAccountTypeID = -7;
      UPDATE
      tt_TEMPFINALDATAMAIN
      SET
      Total =  coalesce((SELECT SUM(Total) FROM tt_TEMPFINALDATAMAIN TInner WHERE TInner.numAccountTypeID IN(-6,-7)),0)
      WHERE
      numAccountTypeID = -8;
      open SWV_RefCur for SELECT * FROM tt_TEMPFINALDATAMAIN;
      DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTMAIN CASCADE;
      DROP TABLE IF EXISTS tt_TEMPFINALDATAMAIN CASCADE;
   end if;

   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   RETURN;
END; $$;











