-- Stored procedure definition script USP_GetApprovalForTickler for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetApprovalForTickler(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_startDate TIMESTAMP DEFAULT null,
	v_endDate TIMESTAMP DEFAULT null,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_startDate <> '1900-01-01 00:00:00.000') then
 
      open SWV_RefCur for
      SELECT  CAST(numCategoryHDRID AS VARCHAR(30)) as RecordId,
                    CASE WHEN numCategory = 1 AND numType = 1 THEN 'Billable Time'
      WHEN numCategory = 1 AND numType = 2 THEN 'Non-Billable Time'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 2  THEN 'Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 6 THEN 'Billable + Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = false AND numType = 1 THEN 'Billable Expense'
      END AS ApprovalRequests, coalesce(ACIC.vcFirstName,'') || ' ' || coalesce(ACIC.vcLastname,'') || ',' || CAST((TE.dtTCreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) AS VARCHAR(30)) as CreatedDate,
					CASE WHEN TE.numApprovalComplete = -1 THEN 'Declined' ELSE 'Pending Approval' END as status,TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcpOppName as vcPOppName,
					 CASE WHEN I.vcItemName = '' THEN '' ELSE '(' || I.vcItemName || ')' END as vcItemName,
					 coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
					 Case WHEN numCategory = 1 AND numType = 1 AND OM.numOppId > 0
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*TE.monAmount)
      WHEN  OM.numOppId > 0
      THEN TE.monAmount
      ELSE cast('0' as DOUBLE PRECISION)
      END AS ClientCharge,
					Case WHEN numCategory = 1 AND (numType = 1 or numType = 2)
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*(select coalesce(monHourlyRate,0) from UserMaster where numUserDetailId = TE.numUserCntID
            and UserMaster.numDomainID = v_numDomainID))
      WHEN numCategory = 2
      THEN TE.monAmount
      ELSE  0
      END AS EmployeeCharge,
					coalesce(OM.numOppId,0) AS numOppId,
					'1' AS TypeApproval
      FROM    timeandexpense TE
      LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId AND OM.tintopptype = 1
      LEFT JOIN Item as I ON TE.numServiceItemID = I.numItemCode
      LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
      LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
      LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactId = TE.numTCreatedBy
      LEFT JOIN ApprovalConfig AS A on TE.numUserCntID = A.numUserId AND A.numDomainID = v_numDomainID
      WHERE  A.numModule = 1 AND (A.numLevel1Authority = v_numUserCntID OR A.numLevel2Authority = v_numUserCntID OR A.numLevel3Authority = v_numUserCntID
      OR A.numLevel4Authority = v_numUserCntID OR A.numLevel5Authority = v_numUserCntID) AND TE.numApprovalComplete NOT IN(0)
      AND TE.dtTCreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) >= v_startDate AND TE.dtTCreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) <= v_endDate
      UNION ALL
      SELECT CAST(M.numOppId AS VARCHAR(255)) as RecordId,CAST('Price Margin Approval' AS VARCHAR(16)) AS ApprovalRequests,
				 coalesce(ACIC.vcFirstName,'') || ' ' || coalesce(ACIC.vcLastname,'') || ',' || CAST((M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) AS VARCHAR(30)) as CreatedDate
				,CAST('Pending Approval' AS VARCHAR(16)) as status,0 as ApprovalStatus,''  as Notes,M.vcpOppName,
				'(' ||(SELECT SUBSTR((SELECT ',' || CAST(OpportunityItems.vcItemName AS VARCHAR(30)) FROM OpportunityItems
            WHERE OpportunityItems.bitItemPriceApprovalRequired = true AND OpportunityItems.numOppId = M.numOppId),2,10000)) || ')'
      AS vcItemName,
				 CAST('' AS VARCHAR(50)) AS vcEmployee,CAST(0 AS DOUBLE PRECISION) AS  ClientCharge,CAST(0 AS DOUBLE PRECISION) AS EmployeeCharge,M.numOppId
				 ,'2' AS TypeApproval
      FROM  OpportunityMaster AS M
      LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactId = M.numCreatedBy
      WHERE M.numDomainId = v_numDomainID
      AND(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems
         WHERE OpportunityItems.bitItemPriceApprovalRequired = true AND OpportunityItems.numOppId = M.numOppId) > 0
      AND v_numUserCntID IN(SELECT numUserID FROM UnitPriceApprover WHERE numDomainID = v_numDomainID)
      AND M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) >= v_startDate AND M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) <= v_endDate
      AND M.tintopptype = 1
      UNION ALL
      SELECT CAST(M.numOppId AS VARCHAR(255)) as RecordId,CAST('Promotion Approval' AS VARCHAR(16)) AS ApprovalRequests,
				 coalesce(ACIC.vcFirstName,'') || ' ' || coalesce(ACIC.vcLastname,'') || ',' || CAST((M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) AS VARCHAR(30)) as CreatedDate
				,CAST('Pending Approval' AS VARCHAR(16)) as status,M.intPromotionApprovalStatus as ApprovalStatus,''  as Notes,M.vcpOppName,
				'(' ||(SELECT SUBSTR((SELECT ',' || CAST(OpportunityItems.vcItemName AS VARCHAR(30)) FROM OpportunityItems
            WHERE OpportunityItems.bitItemPriceApprovalRequired = true AND OpportunityItems.numOppId = M.numOppId),2,10000)) || ')'
      AS vcItemName,
				 CAST('' AS VARCHAR(50)) AS vcEmployee,CAST(0 AS DOUBLE PRECISION) AS  ClientCharge,CAST(0 AS DOUBLE PRECISION) AS EmployeeCharge,M.numOppId
				 ,'3' AS TypeApproval
      FROM  OpportunityMaster AS M
      LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactId = M.numCreatedBy
      LEFT JOIN ApprovalConfig AS A on M.numCreatedBy = A.numUserId WHERE  A.numModule = 2 AND
      M.intPromotionApprovalStatus NOT IN(0,-1) AND A.numDomainID = v_numDomainID
      AND (A.numLevel1Authority = v_numUserCntID OR A.numLevel2Authority = v_numUserCntID OR A.numLevel3Authority = v_numUserCntID
      OR A.numLevel4Authority = v_numUserCntID OR A.numLevel5Authority = v_numUserCntID)
      AND M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) >= v_startDate AND M.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) <= v_endDate
      AND M.tintopptype = 1;
   ELSE
      open SWV_RefCur for
      SELECT  CAST(numCategoryHDRID AS VARCHAR(30)) as RecordId,
                    CASE WHEN numCategory = 1 AND numType = 1 THEN 'Billable Time'
      WHEN numCategory = 1 AND numType = 2 THEN 'Non-Billable Time'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 2  THEN 'Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 6 THEN 'Billable + Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = false AND numType = 1 THEN 'Billable Expense'
      END AS ApprovalRequests, coalesce(ACIC.vcFirstName,'') || ' ' || coalesce(ACIC.vcLastname,'') || ',' || CAST((TE.dtTCreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) AS VARCHAR(30)) as CreatedDate,
					CASE WHEN TE.numApprovalComplete = -1 THEN 'Declined' ELSE 'Pending Approval' END as status,TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcpOppName as vcPOppName,
					 CASE WHEN I.vcItemName = '' THEN '' ELSE '(' || I.vcItemName || ')' END as vcItemName,
					 coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
					 Case WHEN numCategory = 1 AND numType = 1 AND OM.numOppId > 0
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*TE.monAmount)
      WHEN  OM.numOppId > 0
      THEN TE.monAmount
      ELSE cast('0' as DOUBLE PRECISION)
      END AS ClientCharge,
					Case WHEN numCategory = 1 AND (numType = 1 or numType = 2)
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*(select coalesce(monHourlyRate,0) from UserMaster where numUserDetailId = TE.numUserCntID
            and UserMaster.numDomainID = v_numDomainID))
      WHEN numCategory = 2
      THEN TE.monAmount
      ELSE  0
      END AS EmployeeCharge,
					coalesce(OM.numOppId,0) AS numOppId
      FROM    timeandexpense TE
      LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId  AND OM.tintopptype = 1
      LEFT JOIN Item as I ON TE.numServiceItemID = I.numItemCode
      LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
      LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
      LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactId = TE.numTCreatedBy
      LEFT JOIN ApprovalConfig AS A on TE.numUserCntID = A.numUserId AND A.numDomainID = v_numDomainID AND A.numModule = 1
      WHERE
      TE.numDomainID = v_numDomainID
      AND v_numUserCntID IN(A.numLevel1Authority,A.numLevel2Authority,A.numLevel3Authority,A.numLevel4Authority,
      A.numLevel5Authority)
      AND TE.numApprovalComplete NOT IN(0);
   end if;
   RETURN;
END; $$;


