-- Stored procedure definition script USP_RepItemsDtl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RepItemsDtl(v_numDomainID NUMERIC,            
  v_dtFromDate TIMESTAMP,            
  v_dtToDate TIMESTAMP,            
  v_numUserCntID NUMERIC DEFAULT 0,                 
  v_tintType SMALLINT DEFAULT NULL,    
  v_SortOrder INTEGER DEFAULT NULL,    
  v_ItemCode NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_SortOrder = 1 then

      open SWV_RefCur for
      SELECT FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as "Purchase Deal Date",
vcpOppName as "Deal ID"

--Commented by chintan- don't know if its required here or not. 
--'' as [Shipping Company],    
--'' as [Tracking URL]    
      from OpportunityMaster mst
      join OpportunityItems Itm
      on Itm.numOppId = mst.numOppId
      where numItemCode = v_ItemCode and  mst.tintoppstatus = 1 and tintshipped = 0 and tintopptype = 2
      and mst.numDomainId = v_numDomainID;
   end if;    
   if  v_SortOrder = 2 then

      open SWV_RefCur for
      select FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as "Sales Deal Date",
vcpOppName as "Deal ID"
      from OpportunityMaster mst
      join OpportunityItems Itm
      on Itm.numOppId = mst.numOppId
      where numItemCode = v_ItemCode and  mst.tintoppstatus = 1 and tintshipped = 0 and tintopptype = 1
      and mst.numDomainId = v_numDomainID;
   end if;    
   if  v_SortOrder = 3 then

      open SWV_RefCur for
      select FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as "Sales Deal Date",
vcpOppName as "Deal ID"  
      from OpportunityMaster mst
      join OpportunityItems Itm
      on Itm.numOppId = mst.numOppId
      where numItemCode = v_ItemCode and  mst.tintoppstatus = 1 and tintshipped = 0 and tintopptype = 1
      and mst.numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


