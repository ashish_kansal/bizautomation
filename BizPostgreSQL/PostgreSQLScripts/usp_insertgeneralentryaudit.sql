-- Stored procedure definition script Usp_InsertGeneralEntryAudit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

-- Usp_InsertGeneralEntryAudit 
CREATE OR REPLACE FUNCTION Usp_InsertGeneralEntryAudit(v_numDomainID NUMERIC(18,0),
    v_numJournalID NUMERIC(18,0), 
    v_numTransactionID NUMERIC(18,0),
    v_dtCreatedDate TIMESTAMP,
    v_varDescription VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO GenealEntryAudit(numDomainID
           ,numJournalID
           ,numTransactionID
           ,dtCreatedDate
           ,varDescription)
     VALUES(v_numDomainID,
           v_numJournalID,
           v_numTransactionID,
           v_dtCreatedDate,
           v_varDescription);
RETURN;
END; $$;



