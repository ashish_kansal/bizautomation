-- Stored procedure definition script USP_GetCheckJournalIdForDeposit for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckJournalIdForDeposit(v_numDepositId NUMERIC(9,0) DEFAULT 0,              
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);
BEGIN
   open SWV_RefCur for Select numJOurnal_Id From General_Journal_Header Where numDepositId = v_numDepositId and numDomainId = v_numDomainId;
END; $$;












