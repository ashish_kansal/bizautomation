-- Stored procedure definition script USP_GetSpecificDocuments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSpecificDocuments(v_numRecID NUMERIC(9,0) DEFAULT 0,        
v_strType VARCHAR(10) DEFAULT '',          
v_numCategory NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numCategory = 0 then

      open SWV_RefCur for
      select
      numGenericDocID,
  VcFileName ,
  VcDocName ,
  fn_GetListItemName(numDocCategory) as Category ,
  vcfiletype,
  cUrlType,
  fn_GetListItemName(numDocStatus) as BusClass,
  fn_GetContactName(numModifiedBy) || ',' || CAST(bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as Mod,
  (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 2) as Declined,
 (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 0) as Pending,
 fn_GetContactName(numLastCheckedOutBy) || ', ' || CAST(dtCheckOutDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as CheckedOut
      from GenericDocuments
      where numRecID = v_numRecID
      and  vcDocumentSection = v_strType   and numDomainId = v_numDomainID;
   end if;           
        
   if v_numCategory > 0 then

      open SWV_RefCur for
      select
      numGenericDocID,
  VcFileName ,
  VcDocName ,
  fn_GetListItemName(numDocCategory) as Category ,
  vcfiletype,
  cUrlType,
  fn_GetListItemName(numDocStatus) as BusClass,
  fn_GetContactName(numModifiedBy) || ',' || CAST(bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as Mod,
   (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 2) as Declined,
 (select count(*) from DocumentWorkflow
         where numDocID = numGenericDocID and tintApprove = 0) as Pending,
 fn_GetContactName(numLastCheckedOutBy) || ', ' || CAST(dtCheckOutDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as CheckedOut
      from GenericDocuments
      where numRecID = v_numRecID and numDocCategory = v_numCategory
      and  vcDocumentSection = v_strType and numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


