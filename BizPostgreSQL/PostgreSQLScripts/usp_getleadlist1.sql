-- Stored procedure definition script USP_GetLeadList1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetLeadList1(v_numGrpID NUMERIC,                                                                              
 v_numUserCntID NUMERIC,
 v_tintUserRightType SMALLINT,
 v_CustName VARCHAR(100) DEFAULT '',                                                                              
 v_FirstName VARCHAR(100) DEFAULT '',                                                                              
 v_LastName VARCHAR(100) DEFAULT '',                                                                            
 v_SortChar CHAR(1) DEFAULT '0' ,                                                                            
 v_numFollowUpStatus NUMERIC(9,0) DEFAULT 0 ,                                                                            
 v_CurrentPage INTEGER DEFAULT NULL,                                                                            
 v_PageSize INTEGER DEFAULT NULL,                                                                            
 v_columnName VARCHAR(50) DEFAULT '',                                                                            
 v_columnSortOrder VARCHAR(10) DEFAULT '',                                                    
 v_ListType SMALLINT DEFAULT 0,                                                    
 v_TeamType SMALLINT DEFAULT 0,                                                     
 v_TeamMemID NUMERIC(9,0) DEFAULT 0,                                                
 v_numDomainID NUMERIC(9,0) DEFAULT 0    ,                  
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL  ,              
 v_bitPartner BOOLEAN DEFAULT NULL,
 v_bitActiveInActive BOOLEAN DEFAULT NULL,
 v_vcRegularSearchCriteria TEXT DEFAULT '',
 v_vcCustomSearchCriteria TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  TEXT;                                                                             


   v_firstRec  INTEGER;                                                                            
   v_lastRec  INTEGER;                                                                            
   v_strShareRedordWith  VARCHAR(300);
   v_strExternalUser  TEXT DEFAULT '';
   v_tintOrder  SMALLINT;                                                
   v_vcFieldName  VARCHAR(50);                                                
   v_vcListItemType  VARCHAR(3);                                           
   v_vcAssociatedControlType  VARCHAR(20);                                                
   v_numListID  NUMERIC(9,0);                                                
   v_vcDbColumnName  VARCHAR(200);                    
   v_WhereCondition  VARCHAR(2000);                     
   v_vcLookBackTableName  VARCHAR(2000);              
   v_bitCustomField  BOOLEAN;
   v_bitAllowEdit  BOOLEAN;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;   
   v_vcColumnName  VARCHAR(500);                  
   v_Nocolumns  SMALLINT;              
--declare @DefaultNocolumns as tinyint         
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_ListRelID  NUMERIC(9,0);  
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF POSITION('AD.numBillCountry' IN v_vcRegularSearchCriteria) > 0 then

      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry');
   end if;
   IF POSITION('AD.numShipCountry' IN v_vcRegularSearchCriteria) > 0 then

      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry');
   end if;
   IF POSITION('ADC.vcLastFollowup' IN v_vcRegularSearchCriteria) > 0 then

      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcLastFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=true))');
   end if;
   IF POSITION('ADC.vcNextFollowup' IN v_vcRegularSearchCriteria) > 0 then

      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND COALESCE(ADC.numECampaignID,0) > 0 AND COALESCE(bitSend,false)=false))');
   end if;      
	                                                                         
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                                           
   v_lastRec := v_CurrentPage * v_PageSize;
 
   v_strShareRedordWith := ' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=1 AND SR.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
                                                                                                                    
   v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',v_numDomainID,' AND numUserDetailID=',v_numUserCntID,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
   v_numDomainID,' AND EAD.numContactID=',v_numUserCntID,' AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))');
                                                              
   v_strSql := 'with tblLeads as ( SELECT     ';         
   v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || CASE WHEN coalesce(POSITION(substring(v_columnName from 'txt') IN v_columnName),
   0) > 0 THEN ' CONVERT(VARCHAR(Max),' || coalesce(v_columnName,'') || ')'  WHEN v_columnName = 'numRecOwner' THEN 'DM.numRecOwner' ELSE v_columnName end || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID    
   left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus left join VIEW_OPEN_ACTION_ITEMS VOA ON VOA.numDomainID = ' || CAST(v_numDomainID AS VARCHAR) || ' AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE   ON VIE.numDomainID = ' || CAST(v_numDomainID AS VARCHAR) || ' AND VIE.numContactId = ADC.numContactId ##JOIN##                                           
   
 WHERE                                                                               
   DM.tintCRMType=0  and  (COALESCE(ADC.bitPrimaryContact,false)=true OR ADC.numContactID IS NULL)
  AND CMP.numDomainID=DM.numDomainID                                                                                                         
 AND DM.numDomainID=' || CAST(v_numDomainID AS VARCHAR);               
      
   v_strSql := coalesce(v_strSql,'') || ' AND DM.bitActiveInActive=' || SUBSTR(CAST(v_bitActiveInActive AS VARCHAR(15)),1,15); 
        
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' and (DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) ||
      ' or DM.numCreatedBy=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
   end if;              
                                                                                    
   if v_FirstName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
   end if;                                            
   if v_LastName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
   end if;                                            
   if v_CustName <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and CMP.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
   end if;                                                                           
                                                                                   
   if v_numFollowUpStatus <> 0 then 
      v_strSql := coalesce(v_strSql,'') || '  AND DM.numFollowUpStatus=' || SUBSTR(CAST(v_numFollowUpStatus AS VARCHAR(15)),1,15);
   end if;                                                                             
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And CMP.vcCompanyName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                            
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
   end if; 


   if v_ListType = 0 then -- when MySelf radiobutton is checked

      IF v_tintUserRightType = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (DM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or DM.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
      end if;
   ELSEIF v_ListType = 1
   then -- when Team Selected radiobutton is checked

      if v_TeamMemID = 0 then -- When All team members is selected in dropdown
 
         v_strSql := coalesce(v_strSql,'') || '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || ') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(15)),1,15) || '))' || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
      else
         v_strSql := coalesce(v_strSql,'') || '  AND ( DM.numRecOwner=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15) || '  or  DM.numAssignedTo=' || SUBSTR(CAST(v_TeamMemID AS VARCHAR(15)),1,15) || ' or ' || coalesce(v_strShareRedordWith,'') || ' OR ' || coalesce(v_strExternalUser,'') || ')';
      end if;
   end if;

   IF v_vcRegularSearchCriteria <> '' then

      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;
 
   IF v_vcCustomSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;

   v_strSql := coalesce(v_strSql,'') || ')';
                                                                 
   v_tintOrder := 0;                                                
   v_WhereCondition := '';               
                 
   v_Nocolumns := 0;              
 
   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = 34 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = 1
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 34 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = 1) TotalRows;
 

--Set @DefaultNocolumns=  @Nocolumns
  

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );

   v_strSql := coalesce(v_strSql,'') || ' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber,CONCAT(ADC.vcFirstName,'' '',ADC.vcLastName) AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail ';      

---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 COALESCE( (CASE WHEN 
				COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) 
				= COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),'''')



	ELSE '''' 
	END)) as FollowupFlag ';

   v_strSql := coalesce(v_strSql,'') || ' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND COALESCE(bitSend,false) = true AND ADC.bitPrimaryContact = true),0) AS varchar)

			|| ''/'' || CAST(COALESCE((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =true AND ADC.bitPrimaryContact = true ),0)AS varchar) || '')''
			|| ''<a onclick=openDrip('' || (cast(COALESCE((SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactID AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true AND ADC.bitPrimaryContact = true),0)AS varchar)) || '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr ';
	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
--Check if Master Form Configuration is created by administrator if NoColumns=0
   IF v_Nocolumns = 0 then

      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 34 AND numRelCntType = 1 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
	
         v_IsMasterConfAvailable := true;
      end if;
   end if;


--If MasterConfiguration is available then load it otherwise load default columns
   IF v_IsMasterConfAvailable = true then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      34,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,1,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 34 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = 1 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      34,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,1,1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 34 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 1 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 34 AND
      View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicColumnsMasterConfig.numRelCntType = 1 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 34 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 1 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      if v_Nocolumns = 0 then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         select 34,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,1,1,false,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = 34 and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
      FROM View_DynamicColumns
      where numFormId = 34 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = 1
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,CAST(CAST(vcDbColumnName AS VARCHAR(50)) AS VARCHAR(50)),vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
      from View_DynamicCustomColumns
      where numFormId = 34 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1 AND numRelCntType = 1
      AND coalesce(bitCustom,false) = true
      order by tintOrder asc;
   end if;        
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            
 


   while v_tintOrder > 0 LOOP
      if v_bitCustomField = false then
         if v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         if v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
			 
	
	----- Added by Priya For Followups(14Jan2018)---- 
			
			----- Added by Priya For Followups(14Jan2018)---- 
                                                   
         IF v_vcDbColumnName = 'bitEcommerceAccess' then
			
            v_strSql := coalesce(v_strSql,'') || ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcLastFollowup'
         then
			
            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN (ADC.numECampaignID > 0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcDbColumnName = 'vcNextFollowup'
         then
			
            v_strSql := coalesce(v_strSql,'') || ',  COALESCE((CASE WHEN  (ADC.numECampaignID >0) THEN fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox' or v_vcAssociatedControlType = 'ListBox'
         then
        
            IF v_vcDbColumnName = 'numPartenerSource' then
	
               v_strSql := coalesce(v_strSql,'') || ' ,(DM.numPartenerSource) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strSql := coalesce(v_strSql,'') || ' ,(SELECT D.vcPartnerCode||''-''||C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource';
            end if;
            IF v_vcDbColumnName = 'numPartenerContact' then
	
               v_strSql := coalesce(v_strSql,'') || ' ,(DM.numPartenerContact) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_strSql := coalesce(v_strSql,'') || ' ,(SELECT A.vcFirstName||'' ''|| A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=DM.numPartenerContact) AS vcPartenerContact';
            end if;
            if v_vcListItemType = 'LI' then
     
               IF v_numListID = 40 then
		 
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF v_vcDbColumnName = 'numShipCountry' then
				
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry ';
                  ELSE
                     v_WhereCondition := coalesce(v_WhereCondition,'')
                     || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= DM.numDomainID '
                     || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry ';
                  end if;
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'S'
            then
     
               IF v_vcDbColumnName = 'numShipState' then
			
                  v_strSql := coalesce(v_strSql,'') || ',ShipState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=true and AD3.numDomainID= DM.numDomainID '
                  || ' left Join State ShipState on ShipState.numStateID=AD3.numState ';
               ELSEIF v_vcDbColumnName = 'numBillState'
               then
			
                  v_strSql := coalesce(v_strSql,'') || ',BillState.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'')
                  || ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=true and AD4.numDomainID= DM.numDomainID '
                  || ' left Join State BillState on BillState.numStateID=AD4.numState ';
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
               end if;
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'C'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcCampaignName' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CampaignMaster C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on C' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numCampaignId=DM.' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'AG'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',AG' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcGrpName' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join Groups AG' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on AG' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numGrpId=DM.' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'DC'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',DC' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcECampName' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ECampaign DC' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on DC' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numECampaignID=ADC.' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
    
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
 
            v_strSql := coalesce(v_strSql,'') || ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => -1 * ' || CAST(v_ClientTimeZoneOffset AS VARCHAR) ||') AS DATE) = CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => -1 * ' || CAST(v_ClientTimeZoneOffset AS VARCHAR) ||') AS DATE)= CAST(now()::TIMESTAMP + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => -1 * ' || CAST(v_ClientTimeZoneOffset AS VARCHAR) ||') AS DATE)= CAST(now()::TIMESTAMP + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => -1 * ' || CAST(v_ClientTimeZoneOffset AS VARCHAR) ||'),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'Label'
         then

            IF v_vcDbColumnName = 'vcCompactContactDetails' then
	
               v_strSql := coalesce(v_strSql,'') || ' ,'''' AS vcCompactContactDetails';
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when
               v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',COALESCE(
	(SELECT string_agg(CONCAT(A.vcFirstName,'' '',A.vcLastName , CASE WHEN COALESCE(SR.numContactType,0)>0 THEN CONCAT(''('' , fn_GetListItemName(SR.numContactType) , '')'') ELSE '''' END),'','')
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID),'''') "' || coalesce(v_vcColumnName,'') || '"';
         else
            if v_vcDbColumnName = 'vcCampaignAudit' then
               v_strSql := coalesce(v_strSql,'') || ', GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) "' || coalesce(v_vcColumnName,'') || '"';
            else
               v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         end if;
      ELSEIF v_bitCustomField = true
      then

         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         RAISE NOTICE '%',v_vcAssociatedControlType;
         RAISE NOTICE '%',v_numFieldId;
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
                           
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=DM.numDivisionId    ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID::VARCHAR=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
	
            v_strSql := coalesce(v_strSql,'') || ',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=DM.numDivisionId ';
         end if;
         BEGIN
            v_strSql := coalesce(v_strSql,'') || CONCAT(',GetCustFldValue(',v_numFieldId,',1::SMALLINT,DM.numDivisionID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
         END;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                     
 
-------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   drop table IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 34 AND DFCS.numFormID = 34 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
----------------------------                       

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      v_strSql := coalesce(v_strSql,'') || ',tCS.vcColorScheme';
   end if;

   v_strSql := coalesce(v_strSql,'') || ' ,TotalRowCount
 ,COALESCE(ADC.numECampaignID,0) as numECampaignID
 ,COALESCE( (SELECT MAX(numConEmailCampID) FROM ConECampaign WHERE numContactID = ADC.numContactId AND numECampaignID= ADC.numECampaignID AND COALESCE(bitEngaged,false)=true),0) numConEmailCampID
 ,COALESCE(VIE.Total,0) as TotalEmail,
 COALESCE(VOA.OpenActionItemCount,0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
   left join VIEW_OPEN_ACTION_ITEMS VOA  ON VOA.numDomainID = ' || CAST(v_numDomainID AS VARCHAR) || ' AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE   ON VIE.numDomainID = ' || CAST(v_numDomainID AS VARCHAR) || ' AND VIE.numContactId = ADC.numContactId                                               
   join tblLeads T on T.numDivisionID=DM.numDivisionID   ' || coalesce(v_WhereCondition,'');


   v_strSql := REPLACE(v_strSql,'##JOIN##',v_WhereCondition);

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      if v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CMP.';
      end if;
      IF v_vcCSAssociatedControlType = 'DateField' then
		v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
	
         v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
      end if;
   end if;

   v_strSql := coalesce(v_strSql,'') || ' WHERE                                               
   DM.tintCRMType=0 and COALESCE(ADC.bitPrimaryContact,false)=true AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' order by RowNumber';
                                             
   RAISE NOTICE '%',v_strSql;                      
              
   OPEN SWV_RefCur FOR EXECUTE v_strSql;  

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;

   RETURN;
END; $$;


