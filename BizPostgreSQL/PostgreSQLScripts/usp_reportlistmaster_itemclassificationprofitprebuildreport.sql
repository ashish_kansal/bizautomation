-- Stored procedure definition script USP_ReportListMaster_ItemClassificationProfitPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_ItemClassificationProfitPreBuildReport(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(numShippingServiceItemID,0), coalesce(numDiscountServiceItemID,0) INTO v_numShippingItemID,v_numDiscountItemID FROM Domain WHERE numDomainId = v_numDomainID;


	open SWV_RefCur for 
	SELECT
		vcItemClassification as "vcItemClassification"
		,(SUM(Profit)/SUM(monTotAmount))*100 AS "BlendedProfit"
	FROM
	(
		SELECT
			LD.vcData AS vcItemClassification
			,cast(coalesce(monTotAmount,0) as DECIMAL(20,5)) AS monTotAmount
			,coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      INNER JOIN
      Listdetails LD
      ON
      I.numItemClassification = LD.numListItemID
      AND LD.numListID = 36
      AND LD.numDomainid = v_numDomainID
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP
   GROUP BY
   vcItemClassification
   HAVING(SUM(Profit)/SUM(monTotAmount))*100 > 0
   ORDER BY(SUM(Profit)/SUM(monTotAmount))*100 DESC;
END; $$;












