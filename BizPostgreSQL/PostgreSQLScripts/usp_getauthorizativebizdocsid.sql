-- Stored procedure definition script USP_GetAuthorizativeBizDocsId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAuthorizativeBizDocsId(v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numBizDocId From OpportunityBizDocs Where numOppBizDocsId = v_numOppBizDocsId;
END; $$;












