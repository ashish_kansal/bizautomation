-- Stored procedure definition script USP_DeleteSalesTemplateItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSalesTemplateItems(v_numSalesTemplateItemID NUMERIC,
	v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   DELETE FROM SalesTemplateItems
   WHERE numSalesTemplateItemID = v_numSalesTemplateItemID
   AND numDomainID = v_numDomainID;
/****** Object:  StoredProcedure [dbo].[USP_DeleteShippingReportItem]    Script Date: 05/07/2009 17:40:20 ******/
   RETURN;
END; $$;


