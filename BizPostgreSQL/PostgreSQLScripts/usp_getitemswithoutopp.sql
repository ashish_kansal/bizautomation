-- Stored procedure definition script usp_GetItemsWithoutOpp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetItemsWithoutOpp(  
-- 
v_numoppid NUMERIC,
	v_numDomainId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numoppid > 0 then
      open SWV_RefCur for
      SELECT a.numOppId,b.vcItemName,b.txtItemDesc,b.numItemCode,b.charItemType FROM OpportunityItems a INNER JOIN Item b ON a.numItemCode <> b.numItemCode
      WHERE a.numOppId = v_numoppid and b.numDomainID = v_numDomainId order by b.vcItemName;
   ELSE
      open SWV_RefCur for
      SELECT * from Item where numDomainID = v_numDomainId order by vcItemName;
   end if;
   RETURN;
END; $$;


