-- Stored procedure definition script USP_GetDropDownValue for PostgreSQL
Create or replace FUNCTION USP_GetDropDownValue(v_numDomainID NUMERIC(18,0),
    v_numListID NUMERIC(18,0),
    v_vcListItemType VARCHAR(5),
    v_vcDbColumnName VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numID VARCHAR(500),
      vcData TEXT
   );

   INSERT INTO tt_TEMP   SELECT CAST(0 AS VARCHAR(500)),'-- Select One --';

			 

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25--</asp:ListItem>
    --                                <asp:ListItem Value="50">50--</asp:ListItem>
    --                                <asp:ListItem Value="75">75--</asp:ListItem>
    --                                <asp:ListItem Value="100">100--</asp:ListItem>
   IF v_vcListItemType = 'LI' OR v_vcListItemType = 'T' then
				
      INSERT INTO tt_TEMP
      SELECT Ld.numListItemID,vcData from Listdetails Ld
      left join listorder LO on Ld.numListItemID = LO.numListItemID  and LO.numDomainId = v_numDomainID
      where (Ld.numDomainid = v_numDomainID or Ld.constFlag = true) and Ld.numListID = v_numListID order by intSortOrder;
   ELSEIF v_vcListItemType = 'C'
   then
				
      INSERT INTO tt_TEMP
      SELECT  numCampaignID,vcCampaignName || CASE bitIsOnline WHEN true THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
      FROM CampaignMaster WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcListItemType = 'AG'
   then
				
      INSERT INTO tt_TEMP
      SELECT numGrpId, vcGrpName FROM Groups
      ORDER BY vcGrpName;
   ELSEIF v_vcListItemType = 'DC'
   then
				
      INSERT INTO tt_TEMP
      select numECampaignID,vcECampName from ECampaign where numDomainID = v_numDomainID;
   ELSEIF v_vcListItemType = 'U' OR v_vcDbColumnName = 'numManagerID'
   then
			    
      INSERT INTO tt_TEMP
      SELECT A.numContactId,coalesce(A.vcFirstName,'') || ' ' || coalesce(A.vcLastname,'') as vcUserName
      from UserMaster UM join AdditionalContactsInformation A
      on UM.numUserDetailId = A.numContactId
      where UM.numDomainID = v_numDomainID and UM.numDomainID = A.numDomainID  ORDER BY A.vcFirstName,A.vcLastname;
   ELSEIF v_vcListItemType = 'S'
   then
			    
      INSERT INTO tt_TEMP
      SELECT S.numStateID,S.vcState
      from State S
      where S.numDomainID = v_numDomainID ORDER BY S.vcState;
   ELSEIF v_vcListItemType = 'OC'
   then
				
      INSERT INTO tt_TEMP
      SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM Currency C WHERE C.numDomainId = v_numDomainID;
   ELSEIF v_vcListItemType = 'UOM'
   then
				
      INSERT INTO tt_TEMP
      SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcListItemType = 'IG'
   then
				
      INSERT INTO tt_TEMP
      SELECT numItemGroupID,vcItemGroup FROM ItemGroups WHERE numDomainID = v_numDomainID;
   ELSEIF v_vcListItemType = 'SYS'
   then
				
      INSERT INTO tt_TEMP
      SELECT CAST(0 AS VARCHAR(500)) AS numItemID,'Lead' AS vcItemName
      UNION ALL
      SELECT CAST(1 AS VARCHAR(500)) AS numItemID,'Prospect' AS vcItemName
      UNION ALL
      SELECT CAST(2 AS VARCHAR(500)) AS numItemID,'Account' AS vcItemName;
   ELSEIF v_vcListItemType = 'PP'
   then
				
      INSERT INTO tt_TEMP
      SELECT CAST('P' AS VARCHAR(500)),'Inventory Item'
      UNION
      SELECT CAST('N' AS VARCHAR(500)),'Non-Inventory Item'
      UNION
      SELECT CAST('S' AS VARCHAR(500)),'Service';
   ELSEIF v_vcListItemType = 'SP'
   then --Progress Percentage
				
      INSERT INTO tt_TEMP
      SELECT CAST('0' AS VARCHAR(500)),'0%'
      UNION
      SELECT CAST('25' AS VARCHAR(500)),'25%'
      UNION
      SELECT CAST('50' AS VARCHAR(500)),'50%'
      UNION
      SELECT CAST('75' AS VARCHAR(500)),'75%'
      UNION
      SELECT CAST('100' AS VARCHAR(500)),'100%';
   ELSEIF v_vcListItemType = 'ST'
   then --Stage Details
				
      INSERT INTO tt_TEMP
      SELECT vcStageName ,vcStageName  from StagePercentageDetails where numdomainid = v_numDomainID and slp_id = v_numListID  AND coalesce(numProjectid,0) = 0 AND coalesce(numOppid,0) = 0;
   ELSEIF v_vcListItemType = 'SG'
   then --Stage Details
				
      INSERT INTO tt_TEMP
      SELECT vcStageName ,vcStageName  from StagePercentageDetails where numdomainid = v_numDomainID   AND coalesce(numProjectid,0) = 0 AND coalesce(numOppid,0) = 0;
   ELSEIF v_vcListItemType = 'SM'
   then --Reminer,snooze time
				
      INSERT INTO tt_TEMP
      SELECT CAST('5' AS VARCHAR(500)),'5 minutes'
      UNION
      SELECT CAST('15' AS VARCHAR(500)),'15 minutes'
      UNION
      SELECT CAST('30' AS VARCHAR(500)),'30 Minutes'
      UNION
      SELECT CAST('60' AS VARCHAR(500)),'1 Hour'
      UNION
      SELECT CAST('240' AS VARCHAR(500)),'4 Hour'
      UNION
      SELECT CAST('480' AS VARCHAR(500)),'8 Hour'
      UNION
      SELECT CAST('1440' AS VARCHAR(500)),'24 Hour';
   ELSEIF v_vcListItemType = 'PT'
   then --Pin To
				
      INSERT INTO tt_TEMP
      SELECT CAST('1' AS VARCHAR(500)),'Sales Opportunity'
      UNION
      SELECT CAST('2' AS VARCHAR(500)),'Purchase Opportunity'
      UNION
      SELECT CAST('3' AS VARCHAR(500)),'Sales Order'
      UNION
      SELECT CAST('4' AS VARCHAR(500)),'Purchase Order'
      UNION
      SELECT CAST('5' AS VARCHAR(500)),'Project'
      UNION
      SELECT CAST('6' AS VARCHAR(500)),'Cases'
      UNION
      SELECT CAST('7' AS VARCHAR(500)),'Item';
   ELSEIF v_vcListItemType = 'DV'
   then --Customer
				
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
      INSERT INTO tt_TEMP
      SELECT CAST(d.numDivisionID AS VARCHAR(500)),a.vcCompanyName || Case when coalesce(d.numCompanyDiff,0) > 0 then  ' ' || fn_GetListItemName(d.numCompanyDiff) || ':' || coalesce(d.vcCompanyDiff,'') else '' end as vcCompanyname from   CompanyInfo AS a
      join DivisionMaster d
      on  a.numCompanyId = d.numCompanyID
      WHERE d.numDomainID = v_numDomainID;
   ELSEIF v_vcListItemType = 'BD'
   then --Net Days
				
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
      INSERT INTO tt_TEMP
      SELECT numTermsID,vcTerms
      FROM BillingTerms
      WHERE
      numDomainID = v_numDomainID
      AND coalesce(bitActive,false) = true;
   ELSEIF v_vcListItemType = 'BT'
   then --Net Days
				
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
      INSERT INTO tt_TEMP
      SELECT numBizDocTempID,
							vcTemplateName
      FROM BizDocTemplate
      WHERE numDomainID = v_numDomainID and tintTemplateType = 0;
   ELSEIF v_vcListItemType = 'PSS'
   then --Parcel Shipping Service
				
      INSERT INTO tt_TEMP(numID,vcData)
      SELECT
      CAST(numShippingServiceID AS VARCHAR)
						,vcShipmentService
      FROM
      ShippingService
      WHERE
      numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0
      UNION
      SELECT
      CAST(-1 AS VARCHAR(500))
						,'Set Manually';
   ELSEIF v_vcListItemType = 'IC'
   then  -- Item Category
				
      INSERT INTO tt_TEMP(numID,vcData)
      SELECT
      CAST(C1.numCategoryID AS VARCHAR),
							C1.vcCategoryName
      FROM
      Category C1
      WHERE
      numDomainID = v_numDomainID
      ORDER BY
      vcCategoryName;
   ELSEIF v_vcDbColumnName = 'vcInventoryStatus'
   then
				
      INSERT INTO tt_TEMP
      SELECT CAST(1 AS VARCHAR(500)),'Not Applicable' UNION
      SELECT CAST(2 AS VARCHAR(500)),'Shipped' UNION
      SELECT CAST(3 AS VARCHAR(500)),'Back Order' UNION
      SELECT CAST(4 AS VARCHAR(500)),'Shippable';
   ELSEIF v_vcDbColumnName = 'charSex'
   then
					
      INSERT INTO tt_TEMP
      SELECT CAST('M' AS VARCHAR(500)),'Male' UNION
      SELECT CAST('F' AS VARCHAR(500)),'Female';
   ELSEIF v_vcDbColumnName = 'tintOppStatus'
   then
					
      INSERT INTO tt_TEMP
      SELECT CAST(1 AS VARCHAR(500)),'Deal Won' UNION
      SELECT CAST(2 AS VARCHAR(500)),'Deal Lost';
   ELSEIF v_vcDbColumnName = 'tintOppType'
   then
					
      INSERT INTO tt_TEMP
      SELECT CAST(1 AS VARCHAR(500)),'Sales Order' UNION
      SELECT CAST(2 AS VARCHAR(500)),'Purchase Order' UNION
      SELECT CAST(3 AS VARCHAR(500)),'Sales Opportunity' UNION
      SELECT CAST(4 AS VARCHAR(500)),'Purchase Opportunity';
   ELSEIF v_vcDbColumnName = 'vcWarehouse'
   then
					
      INSERT INTO tt_TEMP
      SELECT
      numWareHouseID,
							coalesce(vcWareHouse,'')
      FROM
      Warehouses
      WHERE
      numDomainID = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'vcLocation'
   then
					
      INSERT INTO tt_TEMP
      SELECT CAST(-1 AS VARCHAR(500)),'Global' UNION
      SELECT
      CAST(numWLocationID AS VARCHAR(500)),
							coalesce(vcLocation,'')
      FROM
      WarehouseLocation
      WHERE
      numDomainID = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'tintPriceLevel'
   then
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER
      );
      INSERT INTO tt_TEMP(ID)
      SELECT DISTINCT
      ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) AS Id
      FROM
      PricingTable pt
      INNER JOIN
      Item
      ON
      Item.numItemCode = pt.numItemCode
      AND Item.numDomainID = v_numDomainID
      WHERE
      tintRuleType = 3
      AND coalesce(pt.numCurrencyID,0) = 0
      ORDER BY Id;
      INSERT INTO
      tt_TEMP
      SELECT
      pt.ID
							,coalesce(NULLIF(pnt.vcPriceLevelName,''),CONCAT('Price Level ',pt.ID)) AS Value
      FROM
      tt_TEMP pt
      LEFT JOIN
      PricingNamesTable pnt
      ON
      pt.ID = pnt.tintPriceLevel
      AND
      pnt.numDomainID = v_numDomainID;
   ELSEIF v_vcDbColumnName = 'numExpenseServiceItems'
   then
					
      INSERT INTO tt_TEMP  SELECT numItemCode,vcItemName FROM Item WHERE numDomainID = v_numDomainID AND charItemType = 'S' AND coalesce(bitExpenseItem,false) = true ORDER BY vcItemName;
   end if;
 				
        
   open SWV_RefCur for SELECT numID AS "numID",
      vcData AS "vcData" FROM tt_TEMP; 

   RETURN;
END; $$;	












