-- Stored procedure definition script usp_UpdateSignature for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateSignature(v_numUserID NUMERIC(9,0),  
v_txtSignature VARCHAR(8000) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update UserMaster set txtSignature = v_txtSignature where numUserId = v_numUserID;
   RETURN;
END; $$;


