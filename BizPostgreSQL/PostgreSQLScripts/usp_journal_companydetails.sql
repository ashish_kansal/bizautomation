-- Stored procedure definition script USP_Journal_CompanyDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Journal_CompanyDetails(v_numDomainID NUMERIC(9,0) DEFAULT 0,      
v_varCompanyName VARCHAR(800) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);
BEGIN
   v_strSQL := '';    
    
   v_strSQL := 'Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,(CAST(DM.numDivisionID AS VARCHAR(10)) || ''~'' || coalesce(vcFirstName,'''') || '' '' ||  coalesce(vcLastName,'''')) AS PrimaryContact
From CompanyInfo CI join DivisionMaster DM 
	ON  CI.numCompanyId = DM.numCompanyID                              
LEFT OUTER JOIN AdditionalContactsInformation ACI
	ON ACI.numDivisionId = DM.numDivisionID
Where coalesce(ACI.bitPrimaryContact,false) = true and CI.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(5)),1,5);    
   if v_varCompanyName <> '' then
      v_strSQL := coalesce(v_strSQL,'') || ' And CI.vcCompanyName ilike ''' || coalesce(v_varCompanyName,'') || '%''';
   end if;    
   RAISE NOTICE '%',v_strSQL;      
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;






