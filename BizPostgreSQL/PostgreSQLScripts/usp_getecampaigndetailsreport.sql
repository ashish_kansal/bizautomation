-- Stored procedure definition script USP_GetECampaignDetailsReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetECampaignDetailsReport(v_numDomainId NUMERIC(9,0)  DEFAULT 0,
               v_CurrentPage INTEGER DEFAULT NULL,
               v_PageSize    INTEGER DEFAULT NULL,
               INOUT v_TotRecs     INTEGER   DEFAULT NULL,
			   v_numECampaingID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE 
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numECampaignID NUMERIC(9,0),
      vcECampName VARCHAR(200),
      numConEmailCampID NUMERIC(9,0),
      numcontactid NUMERIC(9,0),
      CompanyName VARCHAR(100),
      ContactName VARCHAR(100),
      Recipient VARCHAR(100),
      Status VARCHAR(20),
      Template VARCHAR(500),
      dtSentDate VARCHAR(80),
      SendStatus VARCHAR(20),
      dtNextStage VARCHAR(80),
      STAGE INTEGER,
      NoOfStagesCompleted INTEGER,
      NextTemplate VARCHAR(100),
      FromEmail VARCHAR(100),
      NoOfEmailSent INTEGER,
      NoOfEmailOpened INTEGER
   );
   INSERT INTO tt_TEMPTABLE(numECampaignID, vcECampName, numConEmailCampID, numContactID, CompanyName, ContactName, Recipient, Status, Template, dtSentDate, SendStatus, dtNextStage, Stage, NoOfStagesCompleted, NextTemplate, FromEmail, NoOfEmailSent, NoOfEmailOpened)
   SELECT
   ECampaign.numECampaignID,
		ECampaign.vcECampName,
		ConECampaign.numConEmailCampID,
		ConECampaign.numContactID,
		CAST(GetCompanyNameFromContactID(ConECampaign.numContactID,ECampaign.numDomainID) AS VARCHAR(100)) AS CompanyName,
		fn_GetContactName(ConECampaign.numContactID) AS ContactName,
		CAST((coalesce(AdditionalContactsInformation.vcEmail,'')) || '(' || CAST((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = true AND coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true) AS VARCHAR(30)) || ')' AS VARCHAR(100)) AS Recipient,
		CAST((CASE
   WHEN coalesce(ECampaign.bitDeleted,false) = false
   THEN CASE WHEN coalesce(ConECampaign.bitEngaged,false) = false THEN 'Disengaged' ELSE 'Engaged' END
   ELSE 'Campaign Deleted'
   END) AS VARCHAR(20)) AS Status,
		CAST(coalesce(CampaignExecutedDetail.Template,'') AS VARCHAR(500)),
		CAST(coalesce(FormatedDateFromDate(CampaignExecutedDetail.bintSentON,v_numDomainId),'') AS VARCHAR(80)),
		CAST((CASE
   WHEN coalesce(CampaignExecutedDetail.bitSend,false) = true AND coalesce(CampaignExecutedDetail.tintDeliveryStatus,0) = 0 THEN 'Failed'
   WHEN coalesce(CampaignExecutedDetail.bitSend,false) = true AND coalesce(CampaignExecutedDetail.tintDeliveryStatus,0) = 1 THEN 'Success'
   ELSE 'Pending'
   END) AS VARCHAR(20)) AS SendStatus
		,
		 CAST(FormatedDateFromDate(CampaignNextExecutationDetail.dtExecutionDate+CAST(coalesce(ECampaign.fltTimeZone,0)*60 || 'minute' as interval),v_numDomainId) AS VARCHAR(80)),
		 coalesce((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS NoOfStages,
		 coalesce((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true),0) AS NoOfStagesCompleted,

		 -----------------Added By Priya-----(04/01/2018)
		 CAST((select gn.VcDocName from(select *, ROW_NUMBER() over(order by numECampID) as RowNumber
         from ECampaignDTLs
         where  (coalesce(numEmailTemplate,0) <> 0 or coalesce(numActionItemTemplate,0) <> 0)
         and numECampID = ECampaign.numECampaignID) as Ecamp
      INNER JOIN  GenericDocuments gn
      on gn.numGenericDocID =(case when Ecamp.numActionItemTemplate > 0 then Ecamp.numActionItemTemplate else Ecamp.numEmailTemplate end) where RowNumber = coalesce((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true),0)+1)
   || '(' ||
   CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND coalesce(bitSend,false) = true),0) AS VARCHAR(30)) || ' Of ' ||
   CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS VARCHAR(30))
   || ')' AS VARCHAR(100))  AS NextTemplate,
		  	CAST((SELECT vcEmail from AdditionalContactsInformation
      WHERE AdditionalContactsInformation.numContactId = ConECampaign.numRecOwner) AS VARCHAR(100)) AS FromEmail,
		 -----------------Added By Priya-----(04/01/2018)		 

		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = true AND coalesce(tintDeliveryStatus,0) = 1) AS NoOfEmailSent,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = true AND coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true) AS NoOfEmailOpened
   FROM
   ConECampaign
   INNER JOIN
   ECampaign
   ON
   ConECampaign.numECampaignID = ECampaign.numECampaignID
   LEFT JOIN
   AdditionalContactsInformation
   ON
   ConECampaign.numContactID = AdditionalContactsInformation.numContactId
   LEFT JOIN LATERAL(SELECT(CASE
      WHEN ECampaignDTLs.numEmailTemplate IS NOT NULL THEN GenericDocuments.VcDocName
      WHEN ECampaignDTLs.numActionItemTemplate IS NOT NULL THEN tblActionItemData.TemplateName
      ELSE ''
      END) AS Template,
				ConECampaignDTL.bitSend,
				ConECampaignDTL.bintSentON,
				ConECampaignDTL.tintDeliveryStatus
      FROM
      ConECampaignDTL
      INNER JOIN
      ECampaignDTLs
      ON
      ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
      LEFT JOIN
      GenericDocuments
      ON
      GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
      LEFT JOIN
      tblActionItemData
      ON
      tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
      WHERE
      ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
      ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
      ConECampaignDTL.bitSend IS NOT NULL
      ORDER BY
      numConECampDTLID DESC LIMIT 1) CampaignExecutedDetail
   LEFT JOIN LATERAL(SELECT
				
      ConECampaignDTL.dtExecutionDate
      FROM
      ConECampaignDTL
      INNER JOIN
      ECampaignDTLs
      ON
      ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
      LEFT JOIN
      GenericDocuments
      ON
      ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
      LEFT JOIN
      tblActionItemData
      ON
      ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
      WHERE
      ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
      ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
      ConECampaignDTL.bitSend IS NULL
      ORDER BY
      numConECampDTLID LIMIT 1) CampaignNextExecutationDetail on TRUE on TRUE
   WHERE
   ECampaign.numDomainID = v_numDomainId AND
		(ECampaign.numECampaignID = v_numECampaingID OR coalesce(v_numECampaingID,0) = 0)
   AND bitEngaged = true; -- Added by Priya(06/01/2018)
    
   
            
   v_firstRec :=(v_CurrentPage::bigint
   -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   SELECT COUNT(* ) INTO v_TotRecs FROM   tt_TEMPTABLE;
   open SWV_RefCur for
   SELECT * FROM     tt_TEMPTABLE
   WHERE    ID > v_firstRec
   AND ID < v_lastRec
   ORDER BY ID;

    
   open SWV_RefCur2 for
   SELECT v_TotRecs AS TotRecs;
   RETURN;
END; $$;


