-- Stored procedure definition script USP_GetMonyTrailDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMonyTrailDetails(v_numDomainID NUMERIC(9,0),
v_numDivisionID NUMERIC(9,0),
v_numAccountID NUMERIC(9,0),
v_dtFrom TIMESTAMP,
v_dtTo TIMESTAMP,
v_tintMode SMALLINT,
v_strSearch VARCHAR(100), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(4000);
BEGIN
   if v_dtFrom = '1753-01-01 00:00:00' then 
      v_dtFrom := null;
   end if; 
   if v_dtTo = '1753-01-01 00:00:00' then 
      v_dtTo := null;
   end if; 



   v_strSQL := 'Select numTransactionId,numJournalId,numDepositId,numCashCreditCardId,numCheckId,OppB.numBizDocsPaymentDetId,OppBDTL.numBizDocsPaymentDetailsId,
OppBiz1.numOppBizDocsId,OppM.numOppId,numCheckNo,OppBDTL.monAmount,dtDueDate,varCurrSymbol,'''' vcCatgyName,vcBizDocID,
fn_GetListItemName(OppB.numPaymentMethod) AS PaymentMethod,
OppB.vcReference,vcMemo
from OpportunityBizDocsDetails OppB
Join OpportunityBizDocsPaymentDetails OppBDTL
on OppBDTL.numBizDocsPaymentDetId = OppB.numBizDocsPaymentDetId
Join OpportunityBizDocs OppBiz1 
on OppB.numBizDocsId = OppBiz1.numOppBizDocsId
Join OpportunityMaster OppM
on OppM.numOppId = OppBiz1.numOppId
left join Currency C
on C.numCurrencyID = OppB.numCurrencyID
left Join General_Journal_Details G
on G.numBizDocsPaymentDetailsId = OppBDTL.numBizDocsPaymentDetailsId
left join General_Journal_Header GH
on GH.numJournal_Id = G.numJournalId
Left join Chart_Of_Accounts CA
on CA.numAccountId = G.numChartAcntId
where bitIntegrated = true 
and OppB.numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || '
and OppM.tintOppType =' || SUBSTR(CAST(v_tintMode AS VARCHAR(1)),1,1);

   if v_numDivisionID > 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and numCustomerId =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20);
   end if;

   if v_numAccountID > 0 then  
      v_strSQL := coalesce(v_strSQL,'') || ' and G.numChartAcntId =' || SUBSTR(CAST(v_numAccountID AS VARCHAR(20)),1,20);
   end if;

   if v_dtFrom is not null then  
      v_strSQL := coalesce(v_strSQL,'') || ' and dtDueDate >= ''' || SUBSTR(CAST(v_dtFrom AS VARCHAR(30)),1,30) || '''';
   end if;

   if v_dtTo is not null then  
      v_strSQL := coalesce(v_strSQL,'') || ' and dtDueDate <= ''' || SUBSTR(CAST(v_dtTo AS VARCHAR(30)),1,30) || '''';
   end if;

   if v_strSearch > '' then  
      v_strSQL := coalesce(v_strSQL,'') || ' and (OppB.vcReference ilike ''%' || coalesce(v_strSearch,'') || '%'''
      || ' or vcMemo ilike ''%' || coalesce(v_strSearch,'') || '%''';
   end if; 

   if SWF_IsNumeric(v_strSearch) = true  and v_strSearch <> '' then

      v_strSQL := coalesce(v_strSQL,'') || ' or numCheckNo = ' || coalesce(v_strSearch,'')  || ')';
   ELSEIF SWF_IsNumeric(v_strSearch) = false  and v_strSearch <> ''
   then

      v_strSQL := coalesce(v_strSQL,'') || ')';
   end if;

   if v_tintMode = 2 then

      v_strSQL := coalesce(v_strSQL,'') || ' union  Select numTransactionId,numJournalId,numDepositId,numCashCreditCardId,numCheckId,OppB.numBizDocsPaymentDetId,OppBDTL.numBizDocsPaymentDetailsId,
0 as numOppBizDocsId,0 as numOppID,numCheckNo,OppBDTL.monAmount,dtDueDate,varCurrSymbol,'''' vcCatgyName,'''' vcBizDocID,
dbo.fn_GetListItemName(OppB.numPaymentMethod) PaymentMethod,
OppB.vcReference,vcMemo
from OpportunityBizDocsDetails OppB
Join OpportunityBizDocsPaymentDetails OppBDTL
on OppBDTL.numBizDocsPaymentDetId=OppB.numBizDocsPaymentDetId
left join Currency C
on C.numCurrencyID =OppB.numCurrencyID
left Join General_Journal_Details G
on G.numBizDocsPaymentDetailsId=OppBDTL.numBizDocsPaymentDetailsId
left join General_Journal_Header GH
on GH.numJournal_Id=G.numJournalId
Left join Chart_Of_Accounts CA
on CA.numAccountId=G.numChartAcntId
where  ([tintPaymentType] =2 or [tintPaymentType]=3)
and OppB.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20);
      if v_numDivisionID > 0 then  
         v_strSQL := coalesce(v_strSQL,'') || ' and numCustomerId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(20)),1,20);
      end if;
      if v_numAccountID > 0 then  
         v_strSQL := coalesce(v_strSQL,'') || ' and G.numChartAcntId=' || SUBSTR(CAST(v_numAccountID AS VARCHAR(20)),1,20);
      end if;
      if v_dtFrom is not null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and dtDueDate>=''' || SUBSTR(CAST(v_dtFrom AS VARCHAR(30)),1,30) || '''';
      end if;
      if v_dtTo is not null then  
         v_strSQL := coalesce(v_strSQL,'') || ' and dtDueDate<=''' || SUBSTR(CAST(v_dtTo AS VARCHAR(30)),1,30) || '''';
      end if;
      if v_strSearch > '' then  
         v_strSQL := coalesce(v_strSQL,'') || ' and (OppB.vcReference iLike ''%' || coalesce(v_strSearch,'') || '%'''
         || ' or vcMemo iLike ''%' || coalesce(v_strSearch,'') || '%''';
      end if;
      if SWF_IsNumeric(v_strSearch) = true  and v_strSearch <> '' then

         v_strSQL := coalesce(v_strSQL,'') || ' or numCheckNo = ' || coalesce(v_strSearch,'')  || ')';
      ELSEIF SWF_IsNumeric(v_strSearch) = false  and v_strSearch <> ''
      then

         v_strSQL := coalesce(v_strSQL,'') || ')';
      end if;
   end if;

--print @strSQL
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


