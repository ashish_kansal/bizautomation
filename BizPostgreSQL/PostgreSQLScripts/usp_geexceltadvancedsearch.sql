-- Stored procedure definition script usp_GeExceltAdvancedSearch for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GeExceltAdvancedSearch(   --
  
v_numDomainID NUMERIC,  
 v_bitPublicPrivate SMALLINT DEFAULT 2,  
 v_vcFirstName VARCHAR(50) DEFAULT '',  
 v_vcLastName VARCHAR(50) DEFAULT '',  
 v_vcCompanyName VARCHAR(50) DEFAULT '',  
 v_vcDivisionName VARCHAR(50) DEFAULT '',  
 v_vcCountry VARCHAR(50) DEFAULT '',  
 v_numContactType NUMERIC DEFAULT 0,  
 v_numCredit NUMERIC DEFAULT 0,  
 v_numCompanyType NUMERIC DEFAULT 0,  
 v_numRating NUMERIC DEFAULT 0,  
 v_numIndustry NUMERIC DEFAULT 0,  
 v_numCloseProb NUMERIC DEFAULT 0,  
 v_monOpptAmt DECIMAL(20,5) DEFAULT 0,  
 v_tintStage SMALLINT DEFAULT 0,  
 v_numGroup NUMERIC DEFAULT 0,  
 v_vcProfile VARCHAR(50) DEFAULT '',  
 v_numTeriD NUMERIC DEFAULT 0,  
 v_vcAOIList VARCHAR(250) DEFAULT '',  
 v_SortChar CHAR(1) DEFAULT '',  
 v_numUserID NUMERIC DEFAULT 0,  
 v_numUserTerID NUMERIC DEFAULT 0,  
 v_tintRights SMALLINT DEFAULT 0,  
 v_numCustomerID NUMERIC DEFAULT 0,  
 v_vcState VARCHAR(50) DEFAULT '',  
 v_vcCity VARCHAR(50) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitPublicPrivate = 1 then  --ONLY PRIVATE RECORDS  
 
      open SWV_RefCur for
      SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' || cast(VCD.numCompanyID as VARCHAR(30)) || '&DivID=' || cast(VCD.numDivisionID as VARCHAR(30)) || '&CRMType=' || cast(VCD.tintCRMType AS VARCHAR(30)) || '&CntID=' || cast(VCD.numContactID AS VARCHAR(30)) || '">' || VCD.vcCompanyName || ' - (<I>' ||  VCD.vcDivisionName || '</I>)</A>' AS vcCompName,
   '<A href="contactdetails.aspx?frm=search&CntID=' || cast(VCD.numContactID AS VARCHAR(30)) || '">' || VCD.vcFirstname || ' ' || VCD.vcLastName || '</A>' AS vcContactName,
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' || VCD.vcEmail || ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' ||  VCD.vcEmail || '</A>' AS vcEMail,
   VCD.vcCompanyName as vcCompanyName,
   VCD.vcDivisionName as vcDivName,
   VCD.vcEmail  AS vcMail,
   VCD.vcBillStreet || ',' || ' ' || VCD.vcBillCity || ',' || ' ' || VCD.vcBilState || ',' || ' ' || VCD.vcBillPostCode || ',' || ' ' || VCD.vcBillCountry as BillingAddress,
   VCD.vcShipStreet || ',' || ' ' || VCD.vcShipCity || ',' || ' ' || VCD.vcShipState || ',' || ' ' || VCD.vcShipPostCode || ',' || ' ' || VCD.vcShipCountry as ShippingAddress,
   VCD.vcFirstname || ' ' || VCD.vcLastName  AS vcContactName,
   CASE
      WHEN VCD.numPhone <> '' THEN VCD.numPhone ||
         CASE
         WHEN VCD.numPhoneExtension <> '' THEN ' (' ||  VCD.numPhoneExtension || ')'
         ELSE ''
         END
      ELSE ''
      END as vcPhone,
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID
      FROM vwContactDetails VCD, OpportunityMaster OM
      WHERE VCD.vcCompanyName ilike
      CASE
      WHEN v_vcCompanyName <> '' THEN coalesce(v_vcCompanyName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcDivisionName ilike
      CASE
      WHEN v_vcDivisionName <> '' THEN coalesce(v_vcDivisionName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcFirstname ilike
      CASE
      WHEN v_vcFirstName <> '' THEN coalesce(v_vcFirstName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcLastName ilike
      CASE
      WHEN v_vcLastName <> '' THEN coalesce(v_vcLastName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcProfile ilike
      CASE
      WHEN v_vcProfile <> '' THEN coalesce(v_vcProfile,'') || '%'
      ELSE '%'
      END
      AND VCD.vcBillCountry ilike
      CASE
      WHEN v_vcCountry <> '' THEN coalesce(v_vcCountry,'') || '%'
      ELSE '%'
      END
      AND (OM.numPClosingPercent =(CASE
      WHEN v_numCloseProb > 0 THEN v_numCloseProb
      END)
      OR
      OM.numPClosingPercent >=(CASE
      WHEN v_numCloseProb = 0 THEN 0
      END))
      AND (OM.monPAmount =(CASE
      WHEN v_monOpptAmt > 0 THEN v_monOpptAmt
      END)
      OR
      OM.monPAmount >=(CASE
      WHEN v_monOpptAmt = 0 THEN 0
      END))
      AND (OM.tintPStage =(CASE
      WHEN v_tintStage > 0 THEN v_tintStage
      END)
      OR
      OM.tintPStage >=(CASE
      WHEN v_tintStage = 0 THEN 0
      END))
      AND (VCD.numGrpID =(CASE
      WHEN v_numGroup > 0 THEN v_numGroup
      END)
      OR
      VCD.numGrpID >=(CASE
      WHEN v_numGroup = 0 THEN 0
      END))
      AND (VCD.numCompanyRating =(CASE
      WHEN v_numRating > 0 THEN v_numRating
      END)
      OR
      VCD.numCompanyRating >=(CASE
      WHEN v_numRating = 0 THEN 0
      END))
      AND (VCD.numCompanyType =(CASE
      WHEN v_numCompanyType > 0 THEN v_numCompanyType
      END)
      OR
      VCD.numCompanyType >=(CASE
      WHEN v_numCompanyType = 0 THEN 0
      END))
      AND (VCD.numCompanyIndustry =(CASE
      WHEN v_numIndustry > 0 THEN v_numIndustry
      END)
      OR
      VCD.numCompanyIndustry >=(CASE
      WHEN v_numIndustry = 0 THEN 0
      END))
      AND (VCD.numCompanyCredit =(CASE
      WHEN v_numCredit > 0 THEN v_numCredit
      END)
      OR
      VCD.numCompanyCredit >=(CASE
      WHEN v_numCredit = 0 THEN 0
      END))
      AND VCD.vcFirstname ilike
      CASE
      WHEN v_SortChar <> '' THEN coalesce(v_SortChar,'') || '%'
      ELSE '%'
      END
      AND (VCD.numContactType =(CASE
      WHEN v_numContactType > 0 THEN v_numContactType
      END)
      OR
      VCD.numContactType >=(CASE
      WHEN v_numContactType = 0 THEN 0
      END))
      AND VCD.bitPublicFlag = true
      AND (VCD.numTerID =(CASE
      WHEN v_numTeriD > 0 THEN v_numTeriD
      END)
      OR
      VCD.numTerID >=(CASE
      WHEN v_numTeriD = 0 THEN 0
      END))
      AND (VCD.numTerID =(CASE
      WHEN v_tintRights = 0 THEN 0
      WHEN v_tintRights = 2 THEN v_numUserTerID
      END)
      OR VCD.numTerID >=(CASE
      WHEN v_tintRights = 3 THEN 0
      WHEN v_tintRights = 1 THEN 0
      END))  
   --Sridhar -- Start  
      AND (VCD.numDivisionID =(CASE
      WHEN v_numCustomerID > 0 THEN v_numCustomerID
      END)
      OR
      VCD.numDivisionID >=(CASE
      WHEN v_numCustomerID = 0 THEN 0
      END))
      AND (VCD.vcBillCity ilike CASE
      WHEN v_vcCity <> '' THEN coalesce(v_vcCity,'') || '%'
      ELSE '%'
      END
      OR VCD.vcShipCity ilike CASE
      WHEN v_vcCity <> '' THEN coalesce(v_vcCity,'') || '%'
      ELSE '%'
      END)
      AND (VCD.vcBilState ilike CASE
      WHEN v_vcState <> '' THEN coalesce(v_vcState,'') || '%'
      ELSE '%'
      END
      OR VCD.vcShipState ilike CASE
      WHEN v_vcState <> '' THEN coalesce(v_vcState,'') || '%'
      ELSE '%'
      END)  
   --Sridhar -- End  
      AND VCD.numContCreatedBy = v_numUserID  
   --AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
      AND VCD.numDivDomainID = v_numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
      AND (OM.numContactId =(CASE
      WHEN v_monOpptAmt > 0 THEN VCD.numContactID
      WHEN v_tintStage > 0 THEN VCD.numContactID
      END)
      OR
      OM.numContactId >=(CASE
      WHEN v_tintStage = 0 AND v_monOpptAmt = 0 THEN 0
      END))
      AND (VCD.tintCRMType = 1 OR VCD.tintCRMType = 2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
      ORDER BY vcCompName,vcContactName;
   ELSE
      open SWV_RefCur for
      SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' || cast(VCD.numCompanyID as VARCHAR(30)) || '&DivID=' || cast(VCD.numDivisionID as VARCHAR(30)) || '&CRMType=' || cast(VCD.tintCRMType AS VARCHAR(30)) || '&CntID=' || cast(VCD.numContactID AS VARCHAR(30)) || '">' || VCD.vcCompanyName || ' - (<I>' ||  VCD.vcDivisionName || '</I>)</A>' AS vcCompName,
   '<A href="contactdetails.aspx?frm=search&CntID=' || cast(VCD.numContactID AS VARCHAR(30)) || '">' || VCD.vcFirstname || ' ' || VCD.vcLastName || '</A>' AS vcContactName,
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' || VCD.vcEmail || ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' ||  VCD.vcEmail || '</A>' AS vcEMail,
   VCD.vcCompanyName as vcCompanyName,
   VCD.vcDivisionName as vcDivName,
   VCD.vcEmail  AS vcMail,
   VCD.vcBillStreet || ',' || ' ' || VCD.vcBillCity || ',' || ' ' || VCD.vcBilState || ',' || ' ' || VCD.vcBillPostCode || ',' || ' ' || VCD.vcBillCountry as BillingAddress,
   VCD.vcShipStreet || ',' || ' ' || VCD.vcShipCity || ',' || ' ' || VCD.vcShipState || ',' || ' ' || VCD.vcShipPostCode || ',' || ' ' || VCD.vcShipCountry as ShippingAddress,
   VCD.vcFirstname || ' ' || VCD.vcLastName  AS vcContactName,
   CASE
      WHEN VCD.numPhone <> '' THEN VCD.numPhone ||
         CASE
         WHEN VCD.numPhoneExtension <> '' THEN ' (' ||  VCD.numPhoneExtension || ')'
         ELSE ''
         END
      ELSE ''
      END as vcPhone,
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID
      FROM vwContactDetails VCD, OpportunityMaster OM
      WHERE VCD.vcCompanyName ilike
      CASE
      WHEN v_vcCompanyName <> '' THEN coalesce(v_vcCompanyName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcDivisionName ilike
      CASE
      WHEN v_vcDivisionName <> '' THEN coalesce(v_vcDivisionName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcFirstname ilike
      CASE
      WHEN v_vcFirstName <> '' THEN coalesce(v_vcFirstName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcLastName ilike
      CASE
      WHEN v_vcLastName <> '' THEN coalesce(v_vcLastName,'') || '%'
      ELSE '%'
      END
      AND VCD.vcProfile ilike
      CASE
      WHEN v_vcProfile <> '' THEN coalesce(v_vcProfile,'') || '%'
      ELSE '%'
      END
      AND VCD.vcBillCountry ilike
      CASE
      WHEN v_vcCountry <> '' THEN coalesce(v_vcCountry,'') || '%'
      ELSE '%'
      END
      AND (OM.numPClosingPercent =(CASE
      WHEN v_numCloseProb > 0 THEN v_numCloseProb
      END)
      OR
      OM.numPClosingPercent >=(CASE
      WHEN v_numCloseProb = 0 THEN 0
      END))
      AND (OM.monPAmount =(CASE
      WHEN v_monOpptAmt > 0 THEN v_monOpptAmt
      END)
      OR
      OM.monPAmount >=(CASE
      WHEN v_monOpptAmt = 0 THEN 0
      END))
      AND (OM.tintPStage =(CASE
      WHEN v_tintStage > 0 THEN v_tintStage
      END)
      OR
      OM.tintPStage >=(CASE
      WHEN v_tintStage = 0 THEN 0
      END))
      AND (VCD.numGrpID =(CASE
      WHEN v_numGroup > 0 THEN v_numGroup
      END)
      OR
      VCD.numGrpID >=(CASE
      WHEN v_numGroup = 0 THEN 0
      END))
      AND (VCD.numCompanyRating =(CASE
      WHEN v_numRating > 0 THEN v_numRating
      END)
      OR
      VCD.numCompanyRating >=(CASE
      WHEN v_numRating = 0 THEN 0
      END))
      AND (VCD.numCompanyType =(CASE
      WHEN v_numCompanyType > 0 THEN v_numCompanyType
      END)
      OR
      VCD.numCompanyType >=(CASE
      WHEN v_numCompanyType = 0 THEN 0
      END))
      AND (VCD.numCompanyIndustry =(CASE
      WHEN v_numIndustry > 0 THEN v_numIndustry
      END)
      OR
      VCD.numCompanyIndustry >=(CASE
      WHEN v_numIndustry = 0 THEN 0
      END))
      AND (VCD.numCompanyCredit =(CASE
      WHEN v_numCredit > 0 THEN v_numCredit
      END)
      OR
      VCD.numCompanyCredit >=(CASE
      WHEN v_numCredit = 0 THEN 0
      END))
      AND VCD.vcFirstname ilike
      CASE
      WHEN v_SortChar <> '' THEN coalesce(v_SortChar,'') || '%'
      ELSE '%'
      END
      AND (VCD.numContactType =(CASE
      WHEN v_numContactType > 0 THEN v_numContactType
      END)
      OR
      VCD.numContactType >=(CASE
      WHEN v_numContactType = 0 THEN 0
      END))
      AND (VCD.bitPublicFlag = false
      OR
    (VCD.bitPublicFlag = true
      AND
      VCD.numContCreatedBy = v_numUserID))
      AND (VCD.numTerID =(CASE
      WHEN v_numTeriD > 0 THEN v_numTeriD
      END)
      OR
      VCD.numTerID >=(CASE
      WHEN v_numTeriD = 0 THEN 0
      END))
      AND (VCD.numTerID =(CASE
      WHEN v_tintRights = 0 THEN 0
      WHEN v_tintRights = 2 THEN v_numUserTerID
      END)
      OR VCD.numTerID >=(CASE
      WHEN v_tintRights = 3 THEN 0
      WHEN v_tintRights = 1 THEN 0
      END))
      AND (VCD.numContCreatedBy =(CASE
      WHEN v_tintRights = 1 THEN v_numUserID
      END)
      OR
      VCD.numContCreatedBy >(CASE
      WHEN v_tintRights <> 1 THEN 0
      END))  
  
   --Sridhar -- Start  
      AND (VCD.numDivisionID =(CASE
      WHEN v_numCustomerID > 0 THEN v_numCustomerID
      END)
      OR
      VCD.numDivisionID >=(CASE
      WHEN v_numCustomerID = 0 THEN 0
      END))
      AND (VCD.vcBillCity ilike CASE
      WHEN v_vcCity <> '' THEN coalesce(v_vcCity,'') || '%'
      ELSE '%'
      END
      OR VCD.vcShipCity ilike CASE
      WHEN v_vcCity <> '' THEN coalesce(v_vcCity,'') || '%'
      ELSE '%'
      END)
      AND (VCD.vcBilState ilike CASE
      WHEN v_vcState <> '' THEN coalesce(v_vcState,'') || '%'
      ELSE '%'
      END
      OR VCD.vcShipState ilike CASE
      WHEN v_vcState <> '' THEN coalesce(v_vcState,'') || '%'
      ELSE '%'
      END)  
   --Sridhar -- End  
  
 --  AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
      AND VCD.numDivDomainID = v_numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
      AND (OM.numContactId =(CASE
      WHEN v_monOpptAmt > 0 THEN VCD.numContactID
      WHEN v_tintStage > 0 THEN VCD.numContactID
      END)
      OR
      OM.numContactId >=(CASE
      WHEN v_tintStage = 0 AND v_monOpptAmt = 0 THEN 0
      END))
      AND (VCD.tintCRMType = 1 OR VCD.tintCRMType = 2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
      ORDER BY vcCompName,vcContactName;
   end if;
   RETURN;
END; $$;


