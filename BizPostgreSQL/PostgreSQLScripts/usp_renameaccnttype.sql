-- Stored procedure definition script USP_RenameAccntType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RenameAccntType(v_numDomainId NUMERIC(9,0) DEFAULT 0,        
v_numAcntType NUMERIC(9,0) DEFAULT 0,        
v_vcRenamedListName VARCHAR(50) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecExist  NUMERIC(9,0);
   v_numListId  NUMERIC(9,0);
BEGIN
   BEGIN
      select   count(numListItemID) INTO v_numRecExist from listorder where numListItemID = v_numAcntType and numDomainId = v_numDomainId;

	-- To fetch the numListId Against Account Id.
      select   numListID INTO v_numListId from Listdetails where numListItemID = v_numAcntType;
      if (v_numRecExist <> 0) then
	
         update listorder set vcRenamedListName = v_vcRenamedListName
         where numListItemID = v_numAcntType and numDomainId = v_numDomainId;
      else
         insert into listorder(numListId , numListItemID, numDomainId, intSortOrder, vcRenamedListName)
		values(v_numListId, v_numAcntType, v_numDomainId, null, v_vcRenamedListName);
      end if;
   END;
   RETURN;
END; $$;


