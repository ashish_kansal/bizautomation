DROP FUNCTION IF EXISTS USP_ElasticSearch_GetOrganizations;

CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetOrganizations(v_numDomainID NUMERIC(18,0)
	,v_vcCompanyIds TEXT
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CustomerSearchFields  TEXT DEFAULT '';
   v_CustomerSearchCustomFields  TEXT DEFAULT '';
   v_CustomerDisplayFields  TEXT DEFAULT '';
   v_query  TEXT;
   v_CustomFields TEXT;
BEGIN
	DROP TABLE IF EXISTS tt_TEMPSEARCHFIELDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPSEARCHFIELDS
	(
		numFieldID NUMERIC(18,0),
		vcDbColumnName VARCHAR(200),
		tintOrder SMALLINT,
		bitCustom BOOLEAN
	);
	INSERT INTO tt_TEMPSEARCHFIELDS
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		CAST(0 AS BOOLEAN) as Custom
	FROM
		View_DynamicColumns
	WHERE
		numFormId = 97 
		AND numDomainID = v_numDomainID 
		AND tintPageType = 1 
		AND bitCustom = false 
		AND coalesce(bitSettingField,false) = true 
		AND numRelCntType = 0
	UNION
	SELECT
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		CAST(1 AS BOOLEAN) as Custom
	FROM
		View_DynamicCustomColumns
	WHERE
		Grp_id = 1 
		AND numFormId = 97 
		AND numDomainID = v_numDomainID 
		AND tintPageType = 1 
		AND bitCustom = true 
		AND numRelCntType = 0;

	IF EXISTS(SELECT * FROM tt_TEMPSEARCHFIELDS) then
		select 
			string_agg(CONCAT(vcDbColumnName,' AS "Search_',vcDbColumnName,'"'),', ' ORDER BY tintOrder) INTO v_CustomerSearchFields 
		FROM
			tt_TEMPSEARCHFIELDS 
		WHERE
			coalesce(bitCustom,false) = false;
		
		IF EXISTS(SELECT * FROM tt_TEMPSEARCHFIELDS WHERE coalesce(bitCustom,false) = true) then
			select 
				string_agg(CONCAT('coalesce("CFW',numFieldID,'",'''') AS ','"Search_CFW',numFieldID,'"'),',' ORDER BY tintOrder) INTO v_CustomerSearchCustomFields 
			FROM
				tt_TEMPSEARCHFIELDS 
			WHERE
				coalesce(bitCustom,false) = true;
		END IF;
   END IF;

	IF  LENGTH(v_CustomerSearchFields) = 0 AND LENGTH(v_CustomerSearchCustomFields) = 0 then
		v_CustomerSearchFields := 'vcCompanyName AS "Search_vcCompanyName"';
	end if;

	----------------------------   Display Fields ---------------------------
	DROP TABLE IF EXISTS tt_TEMPDISPLAYFIELDS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPDISPLAYFIELDS
	(
		numFieldID NUMERIC(18,0),
		vcDbColumnName VARCHAR(200),
		tintOrder SMALLINT,
		bitCustom BOOLEAN
	);


	INSERT INTO tt_TEMPDISPLAYFIELDS
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		CAST(0 AS BOOLEAN) as Custom
	FROM
		View_DynamicColumns
	WHERE
		numFormId = 96 
		AND numDomainID = v_numDomainID 
		AND tintPageType = 1 
		AND bitCustom = false  
		AND coalesce(bitSettingField,false) = true 
		AND numRelCntType = 0
	UNION
	SELECT
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		CAST(1 AS BOOLEAN) as Custom
	FROM
		View_DynamicCustomColumns
	WHERE
		Grp_id = 1 
		AND numFormId = 96 
		AND numDomainID = v_numDomainID 
		AND tintPageType = 1 
		AND bitCustom = true 
		AND numRelCntType = 0
	ORDER BY
		tintOrder;


	IF EXISTS(SELECT * FROM tt_TEMPDISPLAYFIELDS) then
		v_CustomerDisplayFields := coalesce((select 
													string_agg(CONCAT('COALESCE(',vcDbColumnName,'::TEXT,'''')'), ' || '','' || ' ORDER BY tintOrder) 
												FROM
													tt_TEMPDISPLAYFIELDS 
												WHERE
													coalesce(bitCustom,false) = false),'');

		IF EXISTS(SELECT * FROM tt_TEMPDISPLAYFIELDS WHERE coalesce(bitCustom,false) = true) then
			v_CustomerDisplayFields := v_CustomerDisplayFields || (CASE WHEN LENGTH(v_CustomerDisplayFields) > 0 THEN ' || '' , '' || ' ELSE ''''' || ' END) || COALESCE((SELECT 
				string_agg(CONCAT('COALESCE("CFW',numFieldID,'",'''')'),' || '','' || ' ORDER BY tintOrder)
			FROM
				tt_TEMPDISPLAYFIELDS 
			WHERE
				coalesce(bitCustom,false) = true),'');
		END IF;
	END IF;

	IF LENGTH(v_CustomerDisplayFields) = 0 then
		v_CustomerDisplayFields := 'vcCompanyName';
	end if;

	SELECT 
		string_agg('getcustfldvalueorganization(' || Fld_id::VARCHAR || ',DivisionMaster.numDivisionID) AS "CFW' || Fld_id::VARCHAR || '"', ', ') INTO v_CustomFields  
	FROM 
		CFW_Fld_Master 
	WHERE 
		numDomainID = v_numDomainID 
		AND Grp_id = 1;


	v_query := ('SELECT 
					numDivisionID as id
					,''organization'' As module
					,coalesce(tintCRMType,0) AS "tintOrgCRMType"
					,numOrgAssignedTo AS "numOrgAssignedTo"
					,numOrgRecOwner AS "numOrgRecOwner"
					,numOrgTerID AS "numOrgTerID"
					,numOrgCompanyType AS "numOrgCompanyType"
					,(CASE tintCRMType WHEN 1 THEN (''/prospects/frmProspects.aspx?DivID='' || numDivisionID) WHEN 2 THEN (''/Account/frmAccounts.aspx?DivID='' || numDivisionID) ELSE (''/Leads/frmLeads.aspx?DivID='' || numDivisionID) END) url
					,(''<b style="color:#0070C0">'' || numCompanyType || (CASE WHEN numOrgCompanyType=46 THEN CONCAT('' ('' || (CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END),'')'') ELSE '''' END) || '':</b> '' || ' || v_CustomerDisplayFields || ') AS displaytext
					,(numCompanyType || (CASE WHEN numOrgCompanyType=46 THEN ('' ('' || (CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END) || '')'') ELSE '''' END) || '': '' || ' || v_CustomerDisplayFields || ') AS text'
					|| CASE v_CustomerSearchFields
					   WHEN '' THEN ''
					   ELSE ',' || coalesce(v_CustomerSearchFields,'')
					   END || 
					   CASE v_CustomerSearchCustomFields
					   WHEN '' THEN ''
					   ELSE ',' || coalesce(v_CustomerSearchCustomFields,'')
					   END || ' FROM (SELECT
					DivisionMaster.numDivisionID
					,coalesce(DivisionMaster.numAssignedTo,0) numOrgAssignedTo
					,coalesce(DivisionMaster.numRecOwner,0) numOrgRecOwner
					,coalesce(DivisionMaster.numTerID,0) numOrgTerID
					,CompanyInfo.vcCompanyName
					,CompanyInfo.numCompanyType AS numOrgCompanyType
					,DivisionMaster.tintCRMType
					,coalesce(LDComDiff.vcData,'''') numCompanyDiff
					,coalesce(DivisionMaster.vcCompanyDiff,'''') vcCompanyDiff
					,coalesce(LDCMPProfile.vcData,'''') vcProfile
					,coalesce(LDCMPType.vcData,'''') numCompanyType
					,coalesce(vcComPhone,'''') vcComPhone
					,coalesce(LDCMPNoOfEmployees.vcData,'''') numNoOfEmployeesId
					,coalesce(LDCMPAnnualRevenue.vcData,'''') numAnnualRevID
					,coalesce(vcWebSite,'''') vcWebSite 
					,coalesce(LDTerritory.vcData,'''') numTerID
					,coalesce(LDLeadSource.vcData,'''') vcHow
					,coalesce(LDCMPIndustry.vcData,'''') numCompanyIndustry
					,coalesce(LDCMPRating.vcData,'''') numCompanyRating
					,coalesce(LDDMStatus.vcData,'''') numStatusID
					,coalesce(LDCMPCreditLimit.vcData,'''') numCompanyCredit
					,coalesce(vcComFax,'''') vcComFax
					,coalesce(txtComments,'''') txtComments
					,coalesce(ADC.vcFirstName,'''') vcFirstName
					,coalesce(ADC.vcLastName,'''') vcLastName
					,coalesce(ShippingAddress.vcStreet,'''') vcShipStreet
					,coalesce(ShippingAddress.vcCity,'''') vcShipCity
					,coalesce(ShippingAddress.vcPostalCode,'''') vcShipPostCode
					,coalesce(LDShipCountry.vcData,'''') numShipCountry 
					,coalesce(ShipState.vcState,'''') numShipState
					,coalesce(BillingAddress.vcStreet,'''') vcBillStreet
					,coalesce(BillingAddress.vcCity,'''') vcBillCity
					,coalesce(BillingAddress.vcPostalCode,'''') vcBillPostCode
					,coalesce(LDBillCountry.vcData,'''') numBillCountry 
					,coalesce(BillState.vcState,'''') numBillState 
					,coalesce(LDDMFollowup.vcData,'''') numFollowUpStatus
					,coalesce(BillingTerms.vcTerms,'''') numBillingDays
					,DivisionMaster.vcPartnerCode
					,TEMPProjects.vcProjectName
					' ||(CASE WHEN OCTET_LENGTH(v_CustomFields) > 0 THEN (',' || v_CustomFields) ELSE '' END) || '
				FROM 
					CompanyInfo
				INNER JOIN
					DivisionMaster
				ON
					CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
				LEFT JOIN
					ListDetails LDCMPRating
				ON
					CompanyInfo.numCompanyCredit = LDCMPRating.numListItemID
					AND LDCMPRating.numListID = 2
				LEFT JOIN
					ListDetails LDCMPCreditLimit
				ON
					CompanyInfo.numCompanyRating = LDCMPCreditLimit.numListItemID
					AND LDCMPCreditLimit.numListID = 3
				LEFT JOIN
					ListDetails LDCMPIndustry
				ON
					CompanyInfo.numCompanyIndustry = LDCMPIndustry.numListItemID
					AND LDCMPIndustry.numListID = 4
				LEFT JOIN
					ListDetails LDCMPType
				ON
					CompanyInfo.numCompanyType = LDCMPType.numListItemID
					AND LDCMPType.numListID = 5
				LEFT JOIN
					ListDetails LDCMPAnnualRevenue
				ON
					CompanyInfo.numAnnualRevID = LDCMPAnnualRevenue.numListItemID
					AND LDCMPAnnualRevenue.numListID = 6
				LEFT JOIN
					ListDetails LDCMPNoOfEmployees
				ON
					CompanyInfo.numNoOfEmployeesId = LDCMPNoOfEmployees.numListItemID
					AND LDCMPNoOfEmployees.numListID = 7
				LEFT JOIN
					ListDetails LDCMPProfile
				ON
					CompanyInfo.vcProfile = LDCMPProfile.numListItemID
					AND LDCMPProfile.numListID = 21
				LEFT JOIN
					ListDetails LDDMFollowup
				ON
					DivisionMaster.numFollowUpStatus = LDDMFollowup.numListItemID
					AND LDDMFollowup.numListID = 30
				LEFT JOIN
					ListDetails LDDMStatus
				ON
					DivisionMaster.numStatusID = LDDMStatus.numListItemID
					AND LDDMStatus.numListID = 1
				LEFT JOIN
					BillingTerms
				ON
					DivisionMaster.numBillingDays = BillingTerms.numTermsID
				LEFT JOIN
					ListDetails LDComDiff
				ON 
					DivisionMaster.numCompanyDiff = LDComDiff.numListItemID
					AND LDComDiff.numListID = 438
				LEFT JOIN
					ListDetails LDTerritory
				ON 
					DivisionMaster.numTerID = LDTerritory.numListItemID
					AND LDTerritory.numListID = 78
				LEFT JOIN
					ListDetails LDLeadSource
				ON 
					CompanyInfo.vcHow = LDLeadSource.numListItemID
					AND LDLeadSource.numListID = 18
				LEFT JOIN LATERAL
				(
					SELECT
						vcFirstName
						,vcLastName
					FROM
						AdditionalContactsInformation
					WHERE
						AdditionalContactsInformation.numDomainID = DivisionMaster.numDomainID
						AND AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
						AND coalesce(bitPrimaryContact,false) = true
					LIMIT 1
				) ADC ON true
				LEFT JOIN
					AddressDetails BillingAddress
				ON
					BillingAddress.numRecordID = DivisionMaster.numDivisionID 
					AND BillingAddress.tintAddressOf = 2 
					AND BillingAddress.tintAddressType = 1 
					AND BillingAddress.bitIsPrimary = true
					AND BillingAddress.numDomainID = DivisionMaster.numDomainID
				LEFT JOIN
					ListDetails LDBillCountry
				ON 
					BillingAddress.numCountry = LDBillCountry.numListItemID
					AND LDBillCountry.numListID = 40
				LEFT JOIN
					State BillState
				ON 
					BillingAddress.numState = BillState.numStateID
					AND BillState.numDomainID = DivisionMaster.numDomainID
				LEFT JOIN
					AddressDetails ShippingAddress
				ON
					ShippingAddress.numRecordID = DivisionMaster.numDivisionID 
					AND ShippingAddress.tintAddressOf = 2 
					AND ShippingAddress.tintAddressType = 2 
					AND ShippingAddress.bitIsPrimary = true
					AND ShippingAddress.numDomainID = DivisionMaster.numDomainID
				LEFT JOIN
					ListDetails LDShipCountry
				ON 
					ShippingAddress.numCountry = LDShipCountry.numListItemID
					AND LDShipCountry.numListID = 40
				LEFT JOIN
					State ShipState
				ON 
					ShippingAddress.numState = ShipState.numStateID
					AND ShipState.numDomainID = DivisionMaster.numDomainID
				LEFT JOIN LATERAL
				(
					SELECT COALESCE((SELECT 
										string_agg(PM.vcProjectName,'', '')
									FROM
										ProjectsMaster PM 
									WHERE 
										PM.numDivisionId = DivisionMaster.numDomainID),'''')  AS vcProjectName
				) TEMPProjects ON TRUE
				WHERE
					CompanyInfo.numDomainID =' || v_numDomainID || ' AND (CompanyInfo.numCompanyId IN (' || CASE WHEN coalesce(v_vcCompanyIds,'') = '' THEN '0' ELSE v_vcCompanyIds END || ') OR LENGTH(''' || coalesce(v_vcCompanyIds,'') || ''') = 0)) TEMP1');

   RAISE NOTICE '%',v_query;

   OPEN SWV_RefCur FOR EXECUTE v_query;
   RETURN;
END; $$;

