-- Stored procedure definition script USP_ManageBillHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageBillHeader(INOUT v_numBillID NUMERIC(18,0) ,
    v_numDivisionID NUMERIC(18,0),
    v_numAccountID NUMERIC(18,0),
    v_dtBillDate TIMESTAMP,
    v_numTermsID NUMERIC(18,0),
    v_numDueAmount NUMERIC(18,2),
    v_dtDueDate TIMESTAMP,
    v_vcMemo VARCHAR(1000),
    v_vcReference VARCHAR(500),
    v_numBillTotalAmount NUMERIC(18,2),
    v_bitIsPaid BOOLEAN,
    v_numDomainID NUMERIC(18,0),
    v_strItems TEXT,
    v_numUserCntID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0) DEFAULT 0,
	v_bitLandedCost BOOLEAN DEFAULT false,
	v_numCurrencyID NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltExchangeRate  DOUBLE PRECISION;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numAccountClass  NUMERIC(18,0) DEFAULT 0;

   v_tintDefaultClassType  INTEGER DEFAULT 0;
BEGIN
   -- BEGIN TRAN
  
        --Validation of closed financial year
		PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_dtBillDate);
   IF coalesce(v_numCurrencyID,0) = 0 then
		
      v_fltExchangeRate := 1;
      select   numCurrencyID INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainID;
   ELSE
      v_fltExchangeRate := GetExchangeRate(v_numDomainID,v_numCurrencyID);
   end if;
   IF EXISTS(SELECT  numBillID
   FROM    BillHeader
   WHERE   numBillID = v_numBillID) then
            
      UPDATE  BillHeader
      SET     numDivisionId = v_numDivisionID,numAccountID = v_numAccountID,dtBillDate = v_dtBillDate,
      numTermsID = v_numTermsID,monAmountDue = v_numDueAmount,
      dtDueDate = v_dtDueDate,vcMemo = v_vcMemo,vcReference = v_vcReference,
                        --monAmtPaid = @numBillTotalAmount,
      bitIsPaid =(CASE WHEN coalesce(monAmtPaid,0) = v_numDueAmount THEN true
      ELSE false END),numDomainId = v_numDomainID,dtModifiedDate = TIMEZONE('UTC',now()),
      numModifiedBy = v_numUserCntID,numCurrencyID = v_numCurrencyID,
      fltExchangeRate = v_fltExchangeRate
      WHERE   numBillID = v_numBillID
      AND numDomainId = v_numDomainID;
   ELSE

				-- GET ACCOUNT CLASS IF ENABLED
				
      select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainID;
      IF v_tintDefaultClassType = 1 then --USER
				
         select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
         UserMaster UM
         JOIN
         Domain D
         ON
         UM.numDomainID = D.numDomainId WHERE
         D.numDomainId = v_numDomainID
         AND UM.numUserDetailId = v_numUserCntID;
      ELSEIF v_tintDefaultClassType = 2
      then --COMPANY
				
         select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
         DivisionMaster DM WHERE
         DM.numDomainID = v_numDomainID
         AND DM.numDivisionID = v_numDivisionID;
      ELSE
         v_numAccountClass := 0;
      end if;
      INSERT  INTO BillHeader(numDivisionId,
                          numAccountID,
                          dtBillDate,
                          numTermsID,
                          monAmountDue,
                          dtDueDate,
                          vcMemo,
                          vcReference,
                          --[monAmtPaid],
                          bitIsPaid,
                          numDomainId,
                          numCreatedBy,
                          dtCreatedDate,numAccountClass,numOppId,bitLandedCost,numCurrencyID,fltExchangeRate)
                VALUES(v_numDivisionID,
                          v_numAccountID,
                          v_dtBillDate,
                          v_numTermsID,
                          v_numDueAmount,
                          v_dtDueDate,
                          v_vcMemo,
                          v_vcReference,
                          --@numBillTotalAmount,
                          v_bitIsPaid,
                          v_numDomainID,
                          v_numUserCntID,
                          TIMEZONE('UTC',now()),v_numAccountClass,v_numOppId,v_bitLandedCost,v_numCurrencyID,v_fltExchangeRate) RETURNING numBillID INTO v_numBillID;
                
   end if;
   RAISE NOTICE '%',v_numBillID;		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
   IF CAST(v_strItems AS TEXT) <> '' then
		DELETE FROM 
			BillDetails 
		WHERE 
			numBillID = v_numBillID 
			AND numBillDetailID NOT IN (SELECT 
											X.numBillDetailID 
										FROM XMLTABLE
										(
											'NewDataSet/Table1'
											PASSING 
												CAST(v_strItems AS XML)
											COLUMNS
												id FOR ORDINALITY,
												numBillDetailID NUMERIC(18,0) PATH 'numBillDetailID'
										) AS X WHERE COALESCE(X.numBillDetailID,0) > 0);
			                                                           
			--Update transactions
      Update
      BillDetails
      SET
      numExpenseAccountID = X.numExpenseAccountID,monAmount = X.monAmount,vcDescription = X.vcDescription,
      numProjectID = X.numProjectID,numClassID = X.numClassID,
      numCampaignID = X.numCampaignID,numOppItemID = X.numOppItemID,numOppID = X.numOppID
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numBillDetailID NUMERIC(18,0) PATH 'numBillDetailID',
				numExpenseAccountID NUMERIC(18,0) PATH 'numExpenseAccountID',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				vcDescription VARCHAR(1000) PATH 'vcDescription',
				numProjectID NUMERIC(18,0) PATH 'numProjectID',
				numClassID NUMERIC(18,0) PATH 'numClassID',
				numCampaignID NUMERIC(18,0) PATH 'numCampaignID',
				numOppItemID NUMERIC(18,0) PATH 'numOppItemID',
				numOppID NUMERIC(18,0) PATH 'numOppID'
		) AS X
      WHERE
	  COALESCE(X.numBillDetailID,0) > 0
      AND BillDetails.numBillDetailID = X.numBillDetailID
      AND numBillID = v_numBillID;

      INSERT INTO BillDetails(numBillID,
                numExpenseAccountID,
                monAmount,
                vcDescription,
                numProjectID,
                numClassID,
                numCampaignID
				,numOppItemID
				,numOppID)
      SELECT
      v_numBillID,
                X.numExpenseAccountID,
                X.monAmount,
                X.vcDescription,
                X.numProjectID,
                X.numClassID,
                X.numCampaignID
				,X.numOppItemID
				,X.numOppID
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numBillDetailID NUMERIC(18,0) PATH 'numBillDetailID',
				numExpenseAccountID NUMERIC(18,0) PATH 'numExpenseAccountID',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				vcDescription VARCHAR(1000) PATH 'vcDescription',
				numProjectID NUMERIC(18,0) PATH 'numProjectID',
				numClassID NUMERIC(18,0) PATH 'numClassID',
				numCampaignID NUMERIC(18,0) PATH 'numCampaignID',
				numOppItemID NUMERIC(18,0) PATH 'numOppItemID',
				numOppID NUMERIC(18,0) PATH 'numOppID'
		) AS X
		WHERE
			COALESCE(X.numBillDetailID,0)=0;
   end if;
   IF EXISTS(SELECT numBillDetailID FROM BillDetails WHERE numBillID = v_numBillID AND numExpenseAccountID NOT IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainID)) then
		
      RAISE EXCEPTION 'INVALID_EXPENSE_ACCOUNT';
   end if;
		-- Update leads,prospects to Accounts
   PERFORM USP_UpdateCRMTypeToAccounts(v_numDivisionID,v_numDomainID,v_numUserCntID);
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;	


