-- Stored procedure definition script USP_OpportunityMaster_GetShippingDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetShippingDetail(v_numDomainId NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numShippingReportID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_IsCOD  BOOLEAN;
   v_IsDryIce  BOOLEAN;
   v_IsHoldSaturday  BOOLEAN;
   v_IsHomeDelivery  BOOLEAN;
   v_IsInsideDelevery  BOOLEAN;
   v_IsInsidePickup  BOOLEAN;
   v_IsReturnShipment  BOOLEAN;
   v_IsSaturdayDelivery  BOOLEAN;
   v_IsSaturdayPickup  BOOLEAN;
   v_IsAdditionalHandling  BOOLEAN;
   v_IsLargePackage  BOOLEAN;
   v_vcCODType  VARCHAR(50);
   v_vcDeliveryConfirmation  VARCHAR(1000);
   v_vcDescription  TEXT;
   v_tintSignatureType  SMALLINT;
   v_numOrderShippingCompany  NUMERIC(18,0);
   v_numOrderShippingService  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numDivisionShippingCompany  NUMERIC(18,0);
   v_numDivisionShippingService  NUMERIC(18,0);
   v_numShippingCompany  NUMERIC(18,0);
   v_numShippingService  NUMERIC(18,0);
   v_vcFromName  VARCHAR(1000);
   v_vcFromCompany  VARCHAR(1000);
   v_vcFromPhone  VARCHAR(100);
   v_vcFromAddressLine1  VARCHAR(50);
   v_vcFromAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcFromCity  VARCHAR(50);
   v_vcFromState  NUMERIC(18,0);
   v_vcFromZip  VARCHAR(50);
   v_vcFromCountry  NUMERIC(18,0);
   v_bitFromResidential  BOOLEAN DEFAULT 0;

   v_vcToName  VARCHAR(1000);
   v_vcToCompany  VARCHAR(1000);
   v_vcToPhone  VARCHAR(100);
   v_vcToAddressLine1  VARCHAR(50);
   v_vcToAddressLine2  VARCHAR(50) DEFAULT '';
   v_vcToCity  VARCHAR(50);
   v_vcToState  NUMERIC(18,0);
   v_vcToZip  VARCHAR(50);
   v_vcToCountry  NUMERIC(18,0);
   v_bitToResidential  BOOLEAN DEFAULT 0;
   v_numOppBizDocID  NUMERIC(18,0) DEFAULT 0;
   v_tintPayorType  SMALLINT;
   v_vcPayorAccountNo  VARCHAR(100);
   v_numPayorCountry  NUMERIC(18,0);
   v_vcPayorZip  VARCHAR(20);
BEGIN
   IF EXISTS(SELECT numShippingReportID FROM ShippingReport WHERE numDomainID = v_numDomainId AND numShippingReportID = v_numShippingReportID) then
	
      select   numOppBizDocId, IsCOD, IsHomeDelivery, IsInsideDelevery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, vcCODType, vcDeliveryConfirmation, vcDescription, coalesce(tintSignatureType,0), vcFromName, vcFromCompany, vcFromPhone, vcFromAddressLine1, vcFromCity, CAST(vcFromState AS NUMERIC), vcFromZip, CAST(vcFromCountry AS NUMERIC), bitFromResidential, vcToName, vcToCompany, vcToPhone, vcToAddressLine1, vcToCity, CAST(vcToState AS NUMERIC), vcToZip, CAST(vcToCountry AS NUMERIC), bitToResidential, vcCODType, vcDeliveryConfirmation, vcDescription, numShippingCompany, vcValue2, coalesce(tintPayorType,0), coalesce(vcPayorAccountNo,''), coalesce(numPayorCountry,0), coalesce(vcPayorAccountNo,'') INTO v_numOppBizDocID,v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,
      v_IsSaturdayDelivery,v_IsSaturdayPickup,v_IsAdditionalHandling,
      v_IsLargePackage,v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,
      v_tintSignatureType,v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,
      v_vcFromCity,v_vcFromState,v_vcFromZip,v_vcFromCountry,
      v_bitFromResidential,v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,
      v_vcToCity,v_vcToState,v_vcToZip,v_vcToCountry,v_bitToResidential,
      v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,v_numShippingCompany,
      v_numShippingService,v_tintPayorType,v_vcPayorAccountNo,v_numPayorCountry,
      v_vcPayorZip FROM
      ShippingReport WHERE
      numDomainID = v_numDomainId
      AND numShippingReportID = v_numShippingReportID;
   ELSE
      select   IsCOD, IsHomeDelivery, IsInsideDelevery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, vcCODType, vcDeliveryConfirmation, vcDescription, CAST(coalesce(vcSignatureType,'0') AS SMALLINT), coalesce(intUsedShippingCompany,0), coalesce(numShippingService,0), DivisionMaster.numDivisionID, coalesce(intShippingCompany,0), coalesce(numDefaultShippingServiceID,0), (CASE WHEN coalesce(bitUseShippersAccountNo,false) = true THEN 2 ELSE 0 END) INTO v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsSaturdayDelivery,
      v_IsSaturdayPickup,v_IsAdditionalHandling,v_IsLargePackage,
      v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,v_tintSignatureType,
      v_numOrderShippingCompany,v_numOrderShippingService,v_numDivisionID,
      v_numDivisionShippingCompany,v_numDivisionShippingService,v_tintPayorType FROM
      OpportunityMaster
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      LEFT JOIN
      DivisionMasterShippingConfiguration
      ON
      DivisionMasterShippingConfiguration.numDomainID = v_numDomainId
      AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID WHERE
      OpportunityMaster.numDomainId = v_numDomainId
      AND DivisionMaster.numDomainID = v_numDomainId
      AND OpportunityMaster.numOppId = v_numOppID;

		-- GET FROM ADDRESS
      select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, vcState, vcZipCode, vcCountry, bitResidential INTO v_vcFromName,v_vcFromCompany,v_vcFromPhone,v_vcFromAddressLine1,v_vcFromCity,
      v_vcFromState,v_vcFromZip,v_vcFromCountry,v_bitFromResidential FROM
      fn_GetShippingReportAddress(1,v_numDomainId,v_numOppID);

		-- GET TO ADDRESS
      select   vcName, vcCompanyName, vcPhone, vcStreet, vcCity, vcState, vcZipCode, vcCountry, bitResidential INTO v_vcToName,v_vcToCompany,v_vcToPhone,v_vcToAddressLine1,v_vcToCity,v_vcToState,
      v_vcToZip,v_vcToCountry,v_bitToResidential FROM
      fn_GetShippingReportAddress(2,v_numDomainId,v_numOppID);

		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
      IF coalesce(v_numOrderShippingCompany,0) > 0 then
		
         IF coalesce(v_numOrderShippingService,0) = 0 then
			
            v_numOrderShippingService := CASE v_numOrderShippingCompany
            WHEN 91 THEN 15 --FEDEX
            WHEN 88 THEN 43 --UPS
            WHEN 90 THEN 70 --USPS
            END;
         end if;
         v_numShippingCompany := v_numOrderShippingCompany;
         v_numShippingService := v_numOrderShippingService;
      ELSEIF coalesce(v_numDivisionShippingCompany,0) > 0
      then -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		
         IF coalesce(v_numDivisionShippingService,0) = 0 then
			
            v_numDivisionShippingService := CASE v_numDivisionShippingCompany
            WHEN 91 THEN 15 --FEDEX
            WHEN 88 THEN 43 --UPS
            WHEN 90 THEN 70 --USPS
            END;
         end if;
         v_numShippingCompany := v_numDivisionShippingCompany;
         v_numShippingService := v_numDivisionShippingService;
      ELSE
         select   coalesce(numShipCompany,0) INTO v_numShippingCompany FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_numShippingCompany <> 91 OR v_numShippingCompany <> 88 OR v_numShippingCompany <> 90 then
			
            v_numShippingCompany := 91;
         end if;
         v_numShippingService := CASE v_numShippingCompany
         WHEN 91 THEN 15 --FEDEX
         WHEN 88 THEN 43 --UPS
         WHEN 90 THEN 70 --USPS
         END;
      end if;
      IF v_tintPayorType = 2 AND EXISTS(SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID = v_numDivisionID AND numShipViaID = v_numShippingCompany) then
		
         select   coalesce(vcAccountNumber,''), numCountry, vcZipCode INTO v_vcPayorAccountNo,v_numPayorCountry,v_vcPayorZip FROM
         DivisionMasterShippingAccount WHERE
         numDivisionID = v_numDivisionID
         AND numShipViaID = v_numShippingCompany;
      end if;
   end if;

   open SWV_RefCur for SELECT
   v_numOppID AS "numOppID"
		,cast(coalesce(v_numOppBizDocID,0) as NUMERIC(18,0)) AS "numOppBizDocID"
		,v_numShippingCompany AS "numShippingCompany"
		,v_numShippingService AS "numShippingService"
		,v_vcFromName AS "vcFromName"
		,v_vcFromCompany AS "vcFromCompanyName"
		,v_vcFromPhone AS "vcFromPhone"
		,v_vcFromAddressLine1 AS "vcFromStreet"
		,v_vcFromCity AS "vcFromCity"
		,v_vcFromState AS "numFromState"
		,v_vcFromZip AS "vcFromZipCode"
		,v_vcFromCountry AS "numFromCountry"
		,v_bitFromResidential AS "bitFromResidential"
		,v_vcToName AS "vcToName"
		,v_vcToCompany AS "vcToCompanyName"
		,v_vcToPhone AS "vcToPhone"
		,v_vcToAddressLine1 AS "vcToStreet"
		,v_vcToCity AS "vcToCity"
		,v_vcToState AS "numToState"
		,v_vcToZip AS "vcToZipCode"
		,v_vcToCountry AS "numToCountry"
		,v_bitToResidential AS "bitToResidential"
		,v_IsCOD AS "bitCOD"
		,v_IsDryIce AS "bitDryIce"
		,v_IsHoldSaturday AS "bitHoldSaturday"
		,v_IsHomeDelivery AS "bitHomeDelivery"
		,v_IsInsideDelevery AS "bitInsideDelevery"
		,v_IsInsidePickup AS "bitIsInsidePickup"
		,v_IsReturnShipment AS "bitReturnShipment"
		,v_IsSaturdayDelivery AS "bitSaturdayDelivery"
		,v_IsSaturdayPickup AS "bitIsSaturdayPickup"
		,v_IsAdditionalHandling AS "bitAdditionalHandling"
		,v_IsLargePackage AS "bitLargePackage"
		,v_vcCODType AS "vcCODType"
		,v_vcDeliveryConfirmation AS "vcDeliveryConfirmation"
		,v_vcDescription AS "vcDescription"
		,v_tintSignatureType AS "tintSignatureType"
		,v_tintPayorType AS "tintPayorType"
		,v_vcPayorAccountNo AS "vcPayorAccountNo"
		,v_numPayorCountry AS "numPayorCountry"
		,v_vcPayorZip AS "vcPayorZip";
END; $$;












