CREATE OR REPLACE FUNCTION fn_GetDepDtlsbySalesStgID
(
	v_numOppID NUMERIC,
	v_numOppStageID NUMERIC,
	v_tintType SMALLINT
)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_vcRetValue := '';

   select   @@rowcount INTO v_vcRetValue from  OpportunityDependency where numOpportunityId = v_numOppID and numOppstageid = v_numOppStageID;
	
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   if SWV_RowCount > 0 then 
      v_vcRetValue := CAST(1 AS VARCHAR(100));
   else 
      v_vcRetValue := CAST(0 AS VARCHAR(100));
   end if;
   RETURN v_vcRetValue;
END; $$;

