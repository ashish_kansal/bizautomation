DROP FUNCTION IF EXISTS USP_OpportunityBizDocItems_ChangePackingSlipQty;

CREATE OR REPLACE FUNCTION USP_OpportunityBizDocItems_ChangePackingSlipQty(v_numDomainID NUMERIC(18,0)                                
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppId NUMERIC(18,0)
	,v_numOppBizDocsId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
	-- CODE LEVEL TRANSACTION
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;
   v_monDealAmount  DECIMAL(20,5);
BEGIN
   IF EXISTS(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = 296 AND numSourceBizDocId = v_numOppBizDocsId) then
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numOppItemID NUMERIC(18,0),
         numOldUnitHour DOUBLE PRECISION,
         numNewUnitHour DOUBLE PRECISION
      );
      INSERT INTO tt_TEMP(numOppItemID
			,numOldUnitHour
			,numNewUnitHour)
      SELECT
      OBDI.numOppItemID
			,OBDI.numUnitHour
			,SUM(OBDIFulfillment.numUnitHour)
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBDI.numOppBizDocID = OBD.numOppBizDocsId
      INNER JOIN
      OpportunityBizDocs OBDFulfillment
      ON
      OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
      INNER JOIN
      OpportunityBizDocItems OBDIFulfillment
      ON
      OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
      AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
      WHERE
      OBD.numoppid = v_numOppId
      AND OBD.numOppBizDocsId = v_numOppBizDocsId
      GROUP BY
      OBDI.numOppItemID,OBDI.numUnitHour;
      IF EXISTS(SELECT
      OBDI.numOppItemID
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBDI.numOppBizDocID = OBD.numOppBizDocsId
      INNER JOIN
      OpportunityBizDocs OBDFulfillment
      ON
      OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
      INNER JOIN
      OpportunityBizDocItems OBDIFulfillment
      ON
      OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
      AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
      WHERE
      OBD.numoppid = v_numOppId
      AND OBD.numOppBizDocsId = v_numOppBizDocsId
      GROUP BY
      OBDI.numOppItemID,OBDI.numUnitHour
      HAVING
      OBDI.numUnitHour > coalesce(SUM(OBDIFulfillment.numUnitHour),0)) then
		

			--IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			--BEGIN
			--	EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
			--	-- Allocate Inventory
			--	EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			--END
         select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
         select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppId;
         IF v_numOppBizDocsId > 0 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
			
				-- Revert Allocation
            PERFORM USP_RevertAllocationPickList(v_numDomainID,v_numOppId,v_numOppBizDocsId,v_numUserCntID);
         end if;
         UPDATE
         OpportunityBizDocs
         SET
         numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
         WHERE
         numOppBizDocsId = v_numOppBizDocsId;
         IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppId AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1) then
			
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)					
				 -- numeric(18, 0)					
				 -- tinyint						
            PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
            v_numOppBizDocsId := v_numOppBizDocsId,v_numOrderStatus := 0,v_numUserCntID := v_numUserCntID,
            v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
         end if;
         UPDATE
         OpportunityBizDocItems OBI
         SET
         numUnitHour = T1.numNewUnitHour,monPrice = OI.monPrice,monTotAmount =(OI.monTotAmount/OI.numUnitHour)*T1.numNewUnitHour,
         fltDiscount =(CASE WHEN OI.bitDiscountType = false THEN OI.fltDiscount WHEN OI.bitDiscountType = true THEN(OI.fltDiscount/OI.numUnitHour)*T1.numNewUnitHour ELSE OI.fltDiscount END),
         monTotAmtBefDiscount =(OI.monTotAmtBefDiscount/OI.numUnitHour)*T1.numNewUnitHour
         FROM
         OpportunityItems OI, tt_TEMP T1
         WHERE
         OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppItemID = T1.numOppItemID AND(OI.numOppId = v_numOppId
         AND OBI.numOppBizDocID = v_numOppBizDocsId
         AND OBI.numUnitHour > T1.numNewUnitHour);
         v_monDealAmount := coalesce(GetDealAmount(v_numOppId,TIMEZONE('UTC',now()),v_numOppBizDocsId),
         0);
         UPDATE
         OpportunityBizDocs
         SET
         monDealAmount = v_monDealAmount
         WHERE
         numOppBizDocsId = v_numOppBizDocsId;
      end if;
      UPDATE
      OpportunityBizDocItems OBI
      SET
      numUnitHour = 0,monPrice = OI.monPrice,monTotAmount = 0,fltDiscount = 0,
      monTotAmtBefDiscount = 0
      FROM
      OpportunityItems OI
      WHERE
      OBI.numOppItemID = OI.numoppitemtCode AND(OI.numOppId = v_numOppId
      AND OBI.numOppBizDocID = v_numOppBizDocsId
      AND OBI.numOppItemID NOT IN(SELECT OBIInner.numOppItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBIInner ON OB.numOppBizDocsId = OBIInner.numOppBizDocID WHERE OB.numoppid = v_numOppId AND OB.numSourceBizDocId = v_numOppBizDocsId)
      AND OBI.numOppItemID NOT IN(SELECT numOppItemID FROM tt_TEMP));
      
   end if;
   RETURN;
END; $$;


