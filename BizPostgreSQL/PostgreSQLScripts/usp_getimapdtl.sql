-- Stored procedure definition script usp_GetImapDTl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetImapDTl(v_numUserCntId NUMERIC(9,0),
    v_numDomainId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numUserCntId = -1 AND v_numDomainId > 0 then --Support Email
 
      open SWV_RefCur for
      SELECT  coalesce(bitImap,false) AS bitImap,
        vcImapServerUrl,
        vcImapPassword,
        coalesce(bitSSl,false) AS bitSSl,
        numPort,
        vcImapUserName AS vcEmailID,
        coalesce(numLastUID,0) AS LastUID,
        coalesce(bitUseUserName,false) AS bitUseUserName,coalesce(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
      FROM ImapUserDetails ImapUserDTL
      where ImapUserDTL.numUserCntID = v_numUserCntId
      AND ImapUserDTL.numDomainID = v_numDomainId;
   ELSE
      open SWV_RefCur for
      SELECT  coalesce(bitImap,false) AS bitImap,
        vcImapServerUrl,
        vcImapPassword,
        coalesce(bitSSl,false) AS bitSSl,
        numPort,
        vcEmailID,
        coalesce(numLastUID,0) AS LastUID,
        coalesce(bitUseUserName,false) AS bitUseUserName,coalesce(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
      FROM    ImapUserDetails
      JOIN UserMaster ON numUserDetailId = numUserCntId
      WHERE   numUserCntId = v_numUserCntId
      AND ImapUserDetails.numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;        


