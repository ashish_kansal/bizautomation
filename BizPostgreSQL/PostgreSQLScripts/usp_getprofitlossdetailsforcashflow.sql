-- Stored procedure definition script USP_GetProfitLossDetailsForCashFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProfitLossDetailsForCashFlow(v_numDomainId NUMERIC(9,0),                                                
v_dtFromDate TIMESTAMP,                                              
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL TEXT;                                              
   v_i  INTEGER;
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   v_strSQL := '';              
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainId; --and numAccountId = 1             
                                              
   select   count(*) INTO v_i From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                     
   numDomainId = v_numDomainId And
   GJH.datEntry_Date <= CAST('' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '' AS timestamp);                                    
   if v_i = 0 then
  
      v_strSQL := ' Select COA.numAcntTypeId as numAcntType,COA.numParntAcntTypeId as numParntAcntId,          
     COA.numAccountId as numAccountId,COA.vcAccountName as AcntTypeDescription,                                  
     fn_GetCurrentOpeningBalanceForProfitLossForCashFlow(COA.numAccountId,''' || v_dtFromDate || ''',''' || v_dtToDate || ''',' || v_numDomainId || ') As Amount           
              From Chart_Of_Accounts COA            
  inner join ListDetails LD on COA.numAcntTypeId = LD.numListItemID                            
       Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <>' || COALESCE(v_numParntAcntId,0) ||
      'And (COA.numAcntTypeId = 822 Or COA.numAcntTypeId = 823 Or COA.numAcntTypeId = 824 Or COA.numAcntTypeId = 825 Or COA.numAcntTypeId = 826)';
      v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || v_dtFromDate || ''' And  COA.dtOpeningDate<=''' || v_dtToDate || '''';
   Else
      v_strSQL := ' Select COA.numAcntTypeId as numAcntType,COA.numParntAcntTypeId as numParntAcntId,           
   COA.numAccountId as numAccountId,COA.vcAccountName as AcntTypeDescription,                                  
   fn_GetCurrentOpeningBalanceForProfitLossForCashFlow(COA.numAccountId,''' || v_dtFromDate || ''',''' || v_dtToDate || ''',' || v_numDomainId || ') As Amount            
         From Chart_Of_Accounts COA          
        inner join ListDetails LD on COA.numAcntTypeId = LD.numListItemID                           
        Where COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <>' || COALESCE(v_numParntAcntId,0) ||
      'And(COA.numAcntTypeId = 822 Or COA.numAcntTypeId = 823 Or COA.numAcntTypeId = 824 Or COA.numAcntTypeId = 825 Or COA.numAcntTypeId = 826)';
   end if;                                        
   RAISE NOTICE '%',v_strSQL;                                              
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


