-- Stored procedure definition script USP_ValidateBizDocSequenceId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ValidateBizDocSequenceId(v_numDomainID NUMERIC(9,0),
v_numOppId NUMERIC(9,0),
v_numOppBizDocsId NUMERIC(9,0),
v_numBizDocId NUMERIC(9,0),
v_numSequenceId VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT OBD.numOppBizDocsId,OBD.numoppid FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
   WHERE OM.numDomainId = v_numDomainID
   AND OBD.numBizDocId = v_numBizDocId 
	--AND om.numOppId != @numOppId 
   AND (OBD.numOppBizDocsId = coalesce(v_numOppBizDocsId,0) OR v_numOppBizDocsId = 0)
   AND OBD.numSequenceId = v_numSequenceId;
END; $$;












