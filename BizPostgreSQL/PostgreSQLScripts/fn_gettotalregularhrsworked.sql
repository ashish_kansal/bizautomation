-- Function definition script fn_GetTotalRegularHrsWorked for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetTotalRegularHrsWorked(v_dtStartDate TIMESTAMP,v_dtEndDate TIMESTAMP,v_numUserCntID NUMERIC,v_numDomainId NUMERIC)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_decTotalHrsWorked  DECIMAL(10,2);
BEGIN
   select   sum(x.Hrs) INTO v_decTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
      from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1
      And (dtFromDate between v_dtStartDate And v_dtEndDate Or dtToDate between v_dtStartDate And v_dtEndDate) And
      numUserCntID = v_numUserCntID  And numDomainID = v_numDomainId
      union all
      Select coalesce(sum(Cast((EXTRACT(DAY FROM dtEndDate -dtStartDate)*60*24+EXTRACT(HOUR FROM dtEndDate -dtStartDate)*60+EXTRACT(MINUTE FROM dtEndDate -dtStartDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
      From OpportunityTime Where (dtStartDate between v_dtStartDate And v_dtEndDate or dtEndDate between v_dtStartDate and v_dtEndDate)
      And numPCreatedBy = v_numUserCntID
      union all
      Select coalesce(Sum(Cast((EXTRACT(DAY FROM dtEndTime -dtStartTime)*60*24+EXTRACT(HOUR FROM dtEndTime -dtStartTime)*60+EXTRACT(MINUTE FROM dtEndTime -dtStartTime)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs From CaseTime
      Where (dtStartTime between v_dtStartDate and v_dtEndDate Or dtEndTime between v_dtStartDate and v_dtEndDate)
      And numCreatedBy = v_numUserCntID
      union all
      Select coalesce(Sum(Cast((EXTRACT(DAY FROM dtEndTime -dtStartTime)*60*24+EXTRACT(HOUR FROM dtEndTime -dtStartTime)*60+EXTRACT(MINUTE FROM dtEndTime -dtStartTime)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs From ProjectsTime
      Where (dtStartTime between v_dtStartDate and v_dtEndDate Or dtEndTime between v_dtStartDate and v_dtEndDate)
      And numPCreatedBy = v_numUserCntID) x;    
   Return v_decTotalHrsWorked;
END; $$;

