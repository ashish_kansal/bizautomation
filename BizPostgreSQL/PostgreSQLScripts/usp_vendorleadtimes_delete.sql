CREATE OR REPLACE FUNCTION usp_vendorleadtimes_delete
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numdivisionid NUMERIC(18,0)
	,p_numvendorleadtimesids TEXT
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	IF COALESCE(p_numvendorleadtimesids,'') <> '' THEN
		DELETE FROM 
			VendorLeadTimes VLT
		USING 
			DivisionMaster DM 
		WHERE
			VLT.numdivisionid = p_numdivisionid
			AND VLT.numdivisionid = DM.numdivisionid
			AND DM.numdomainid = p_numdomainid
			AND VLT.numvendorleadtimesid IN (SELECT Id FROM SplitIDs(p_numvendorleadtimesids,','));
	END IF;
END; $$;