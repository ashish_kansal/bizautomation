-- Stored procedure definition script USP_OpportunityMaster_PutAway for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_PutAway(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppItemID NUMERIC(18,0)
	,v_numQtyReceived DOUBLE PRECISION
	,v_dtItemReceivedDate TIMESTAMP
	,v_vcWarehouses TEXT
	,v_numVendorInvoiceBizDocID NUMERIC(18,0)
	,v_tintMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
   v_description  VARCHAR(100);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_numFromWarehouseID  NUMERIC(18,0);
   v_numFromWarehouseItemID  NUMERIC(18,0);
   v_numWLocationID  NUMERIC(18,0);     
   v_numOldQtyReceived  DOUBLE PRECISION;
   v_bitStockTransfer  BOOLEAN;
   v_numToWarehouseItemID  NUMERIC;
   v_numUnits  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5); 
   v_numItemCode  NUMERIC(18,0);
   v_monListPrice  DECIMAL(20,5);
   v_numWarehouseID  NUMERIC(18,0);
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;
		
   v_i  INTEGER;
   v_j  INTEGER;
   v_k  INTEGER;
   v_iCount  INTEGER DEFAULT 0;
   v_jCount  INTEGER DEFAULT 0;
   v_kCount  INTEGER DEFAULT 0;
   v_vcSerialLot  TEXT;
   v_numWareHouseItmsDTLID  NUMERIC(18,0);
   v_vcSerailLotNumber  VARCHAR(300);
   v_numSerialLotQty  DOUBLE PRECISION;
   v_numTempWLocationID  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempReceivedQty  DOUBLE PRECISION;
   v_numLotWareHouseItmsDTLID  NUMERIC(18,0);
   v_numLotQty  DOUBLE PRECISION;
   v_p3  TEXT DEFAULT '';	

   v_TotalOnHand  DOUBLE PRECISION DEFAULT 0;
   v_monAvgCost  DECIMAL(20,5);	

   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   v_dtDATE  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_vcDescription  VARCHAR(100) DEFAULT 'INSERT WareHouse';
BEGIN
   IF coalesce(v_numQtyReceived,0) <= 0 then
	
      RAISE EXCEPTION 'QTY_TO_RECEIVE_NOT_PROVIDED';
      RETURN;
   end if; 

   IF LENGTH(coalesce(v_vcWarehouses,'')) = 0 then
	
      RAISE EXCEPTION 'LOCATION_NOT_SELECTED';
      RETURN;
   end if;

   DROP TABLE IF EXISTS tt_TEMPWAREHOUSELOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSELOT
   (
      ID INTEGER,
      numWareHouseItmsDTLID NUMERIC(18,0),
      vcSerialNo VARCHAR(300),
      numQty DOUBLE PRECISION
   );

   DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
   (
      ID INTEGER,
      vcSerialNo VARCHAR(300),
      numQty DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWarehouseLocations_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWAREHOUSELOCATIONS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSELOCATIONS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseLocationID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numReceivedQty DOUBLE PRECISION,
      vcSerialLotNo TEXT
   );

   INSERT INTO tt_TEMPWAREHOUSELOCATIONS(numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo)
   SELECT(CASE WHEN coalesce(bitNewLocation,false) = true THEN ID ELSE 0 END)
		,(CASE WHEN coalesce(bitNewLocation,false) = true THEN 0 ELSE ID END)
		,coalesce(Qty,0)
		,coalesce(SerialLotNo,'')
   FROM
   XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_vcWarehouses AS XML)
		COLUMNS
			numID FOR ORDINALITY,
			bitNewLocation BOOLEAN PATH 'bitNewLocation',
			ID NUMERIC(18,0) PATH 'ID',
			Qty DOUBLE PRECISION PATH 'Qty',
			SerialLotNo TEXT PATH 'SerialLotNo'
	) X;

   IF v_numQtyReceived <> coalesce((SELECT SUM(numReceivedQty) FROM tt_TEMPWAREHOUSELOCATIONS),0) then
	
      RAISE EXCEPTION 'INALID_QTY_RECEIVED';
      RETURN;
   end if;

   IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
	
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
      IF(SELECT
      COUNT(*)
      FROM
      OpportunityBizDocItems OBDI
      INNER JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      WHERE
      numOppBizDocID = v_numVendorInvoiceBizDocID
      AND OBDI.numOppItemID = v_numOppItemID
      AND v_numQtyReceived >(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0))) > 0 then
		
         RAISE EXCEPTION 'INVALID_VENDOR_INVOICE_RECEIVE_QTY';
         RETURN;
      end if;
   end if;

   select   coalesce(numWarehouseItmsID,0), coalesce(WI.numWareHouseID,0), coalesce(numWarehouseItmsID,0), coalesce(WI.numWLocationID,0), coalesce(numUnitHourReceived,0), coalesce(OM.bitStockTransfer,false), coalesce(numToWarehouseItemID,0), OM.numOppId, OI.numUnitHour, coalesce(monPrice,0)*(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 THEN 1 ELSE OM.fltExchangeRate END), OI.numItemCode, coalesce(I.monListPrice,0), WI.numWareHouseID, coalesce(I.bitSerialized,false), coalesce(I.bitLotNo,false) INTO v_numWarehouseItemID,v_numFromWarehouseID,v_numFromWarehouseItemID,v_numWLocationID,
   v_numOldQtyReceived,v_bitStockTransfer,v_numToWarehouseItemID,
   v_numOppID,v_numUnits,v_monPrice,v_numItemCode,v_monListPrice,v_numWarehouseID,
   v_bitSerial,v_bitLot FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID WHERE
   numoppitemtCode = v_numOppItemID;

   IF(v_numQtyReceived+v_numOldQtyReceived) > v_numUnits then
	
      RAISE EXCEPTION 'RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY';
      RETURN;
   end if;

   IF v_bitStockTransfer = true then --added by chintan
      SELECT * INTO v_p3 FROM USP_UpdateQtyShipped(v_numQtyReceived,v_numOppItemID,v_numUserCntID,v_vcError := v_p3);
      IF LENGTH(v_p3) > 0 then
		
         RAISE EXCEPTION '%',v_p3;
         RETURN;
      end if;
      v_numWarehouseItemID := v_numToWarehouseItemID;
   ELSE
		--Updating the Average Cost
		
      select(SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0))) INTO v_TotalOnHand FROM
      WareHouseItems WHERE
      numItemID = v_numItemCode;
      select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_monAvgCost FROM
      Item WHERE
      numItemCode = v_numItemCode;
      v_monAvgCost :=((v_TotalOnHand*v_monAvgCost)+(v_numQtyReceived*v_monPrice))/(v_TotalOnHand+v_numQtyReceived);
      UPDATE
      Item
      SET
      monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END)
      WHERE
      numItemCode = v_numItemCode;
   end if;	

   UPDATE
   WareHouseItems
   SET
   numonOrder = coalesce(numonOrder,0) -v_numQtyReceived,dtModified = LOCALTIMESTAMP
   WHERE
   numWareHouseItemID = v_numWarehouseItemID;
		
	 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  tinyint
   v_description := CONCAT('PO Qty Received (Qty:',v_numQtyReceived,')');

   PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppID,
   v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
   v_dtRecordDate := v_dtItemReceivedDate,
   v_numDomainID := v_numDomainID,SWV_RefCur := null);


   v_i := 1;
   SELECT COUNT(*) INTO v_iCount FROM tt_TEMPWAREHOUSELOCATIONS;

   WHILE v_i <= v_iCount LOOP
      select   coalesce(numWarehouseLocationID,0), coalesce(numWarehouseItemID,0), coalesce(numReceivedQty,0), coalesce(vcSerialLotNo,'') INTO v_numTempWLocationID,v_numTempWarehouseItemID,v_numTempReceivedQty,v_vcSerialLot FROM
      tt_TEMPWAREHOUSELOCATIONS WHERE
      ID = v_i;
      IF (coalesce(v_bitLot,false) = true OR coalesce(v_bitSerial,false) = true) then
		
         IF LENGTH(coalesce(v_vcSerialLot,'')) = 0 then
			
            RAISE EXCEPTION 'SERIALLOT_REQUIRED';
            RETURN;
         ELSE
            DELETE FROM tt_TEMPSERIALLOT;
            IF coalesce(v_bitSerial,false) = true then
				
               INSERT INTO tt_TEMPSERIALLOT(ID
						,vcSerialNo
						,numQty)
               SELECT
               ROW_NUMBER() OVER(ORDER BY OutParam)
						,CAST(RTRIM(LTRIM(OutParam)) AS VARCHAR(300))
						,1
               FROM
               SplitString(v_vcSerialLot,',');
            ELSEIF v_bitLot = true
            then
				
               INSERT INTO tt_TEMPSERIALLOT(ID
						,vcSerialNo
						,numQty)
               SELECT
               ROW_NUMBER() OVER(ORDER BY OutParam)
						,CAST((CASE
               WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
               THEN RTRIM(LTRIM(SUBSTR(OutParam,0,coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0))))
               ELSE ''
               END) AS VARCHAR(300))
						,(CASE
               WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
               THEN(CASE WHEN SWF_IsNumeric(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1))) = true
                  THEN CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1)) AS DOUBLE PRECISION)
                  ELSE -1
                  END)
               ELSE -1
               END)
               FROM
               SplitString(v_vcSerialLot,',');
            end if;
            IF EXISTS(SELECT vcSerialNo FROM tt_TEMPSERIALLOT WHERE numQty = -1) then
				
               RAISE EXCEPTION 'INVALID_SERIAL_NO';
               RETURN;
            ELSEIF v_numTempReceivedQty <> coalesce((SELECT SUM(numQty) FROM tt_TEMPSERIALLOT),0)
            then
				
               RAISE EXCEPTION 'INALID_SERIALLOT';
               RETURN;
            end if;
         end if;
      end if;
      IF coalesce(v_numTempWarehouseItemID,0) = 0 then
		
         IF EXISTS(SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID = v_numDomainID AND numWLocationID = coalesce(v_numTempWLocationID,0)) then
			
            IF EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID AND numWLocationID = v_numTempWLocationID) then
				
               SELECT  numWareHouseItemID INTO v_numTempWarehouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID AND numWLocationID = v_numTempWLocationID     LIMIT 1;
            ELSE
				
               INSERT INTO WareHouseItems(numItemID,
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numAllocation,
						numonOrder,
						numBackOrder,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified)
					VALUES(v_numItemCode,
						v_numWarehouseID,
						v_numTempWLocationID,
						0,
						0,
						0,
						0,
						0,
						v_monListPrice,
						coalesce((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID = v_numDomainID AND numWLocationID = coalesce(v_numTempWLocationID, 0)), ''),
						'',
						'',
						v_numDomainID,
						LOCALTIMESTAMP);
					
               v_numTempWarehouseItemID := CURRVAL('WareHouseItems_seq');
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numItemCode,
               v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
               v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,v_dtRecordDate := v_dtDATE,
               v_numDomainID := v_numDomainID,SWV_RefCur := null);
            end if;
         ELSE
            RAISE EXCEPTION 'INVALID_WAREHOUSE_LOCATION';
            RETURN;
         end if;
      end if;
      IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numTempWarehouseItemID) then
		
         RAISE EXCEPTION 'INVALID_WAREHOUSE';
         RETURN;
      ELSE
			-- INCREASE THE OnHand Of Destination Location
         UPDATE
         WareHouseItems
         SET
         numBackOrder =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN coalesce(numBackOrder,0) -v_numTempReceivedQty ELSE 0 END),numAllocation =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN coalesce(numAllocation,0)+v_numTempReceivedQty ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),
         numOnHand =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN numOnHand ELSE coalesce(numOnHand,0)+(v_numTempReceivedQty -coalesce(numBackOrder,0)) END),dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numTempWarehouseItemID;
			 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
         v_description := CONCAT('PO Qty Put-Away (Qty:',v_numTempReceivedQty,')');
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppID,
         v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,
         v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
         v_numDomainID := v_numDomainID,SWV_RefCur := null);
         IF coalesce(v_numTempWarehouseItemID,0) > 0 AND coalesce(v_numTempWarehouseItemID,0) <> v_numWarehouseItemID then
			
            INSERT INTO OpportunityItemsReceievedLocation(numDomainId,
					numOppID,
					numOppItemID,
					numWarehouseItemID,
					numUnitReceieved)
				VALUES(v_numWarehouseItemID,
					v_numOppID,
					v_numOppItemID,
					v_numTempWarehouseItemID,
					v_numTempReceivedQty);
         end if;
         IF (coalesce(v_bitLot,false) = true OR coalesce(v_bitSerial,false) = true) then
			
            v_j := 1;
            SELECT COUNT(*) INTO v_jCount FROM tt_TEMPSERIALLOT;
            WHILE (v_j <= v_jCount AND v_numTempReceivedQty > 0) LOOP
               select   coalesce(vcSerialNo,''), coalesce(numQty,0) INTO v_vcSerailLotNumber,v_numSerialLotQty FROM
               tt_TEMPSERIALLOT WHERE
               ID = v_j;
               IF v_numSerialLotQty > 0 then
					
                  IF v_bitStockTransfer = true then
						
                     IF(SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID = v_numItemCode AND WareHouseItems.numWareHouseID = v_numFromWarehouseID AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber)) = 0 then
							
                        RAISE EXCEPTION 'INVALID_SERIAL_NO';
                        RETURN;
                     end if;
                     IF v_bitLot = true then
							
                        IF(SELECT SUM(WareHouseItmsDTL.numQty) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID = v_numItemCode AND WareHouseItems.numWareHouseID = v_numFromWarehouseID AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber)) < v_numTempReceivedQty then
								
                           RAISE EXCEPTION 'INVALID_LOT_QUANTITY';
                           RETURN;
                        end if;
                        DELETE FROM tt_TEMPWAREHOUSELOT;
                        INSERT INTO tt_TEMPWAREHOUSELOT(ID
									,numWareHouseItmsDTLID
									,numQty)
                        SELECT
                        ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.numQty)
									,WareHouseItmsDTL.numWareHouseItmsDTLID
									,coalesce(WareHouseItmsDTL.numQty,0)
                        FROM
                        WareHouseItems
                        INNER JOIN
                        WareHouseItmsDTL
                        ON
                        WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID
                        WHERE
                        WareHouseItems.numItemID = v_numItemCode
                        AND WareHouseItems.numWareHouseID = v_numFromWarehouseID
                        AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber);
                        v_k := 1;
                        select   COUNT(*) INTO v_kCount FROM tt_TEMPWAREHOUSELOT;
                        WHILE v_k <= v_kCount AND v_numTempReceivedQty > 0 LOOP
                           select   numWareHouseItmsDTLID, numQty INTO v_numLotWareHouseItmsDTLID,v_numLotQty FROM
                           tt_TEMPWAREHOUSELOT WHERE
                           ID = v_k;
                           IF v_numTempReceivedQty > v_numLotQty then
									
                              UPDATE
                              WareHouseItmsDTL
                              SET
                              numWareHouseItemID = v_numTempWarehouseItemID
                              WHERE
                              numWareHouseItmsDTLID = v_numLotWareHouseItmsDTLID;
                              UPDATE tt_TEMPSERIALLOT SET numQty = numQty -v_numLotQty WHERE ID = v_j;
                              v_numTempReceivedQty := v_numTempReceivedQty -v_numLotQty;
                           ELSE
                              UPDATE
                              WareHouseItmsDTL
                              SET
                              numQty = numQty -v_numTempReceivedQty
                              WHERE
                              numWareHouseItmsDTLID = v_numLotWareHouseItmsDTLID;
                              INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
											vcSerialNo,
											numQty,
											dExpirationDate,
											bitAddedFromPO)
                              SELECT
                              v_numTempWarehouseItemID,
											vcSerialNo,
											v_numTempReceivedQty,
											dExpirationDate,
											bitAddedFromPO
                              FROM
                              WareHouseItmsDTL
                              WHERE
                              numWareHouseItmsDTLID = v_numLotWareHouseItmsDTLID;
                              UPDATE tt_TEMPSERIALLOT SET numQty = numQty -v_numTempReceivedQty WHERE ID = v_j;
                              v_numTempReceivedQty := 0;
                           end if;
                           v_k := v_k::bigint+1;
                        END LOOP;
                     ELSE
                        IF NOT EXISTS(SELECT WareHouseItmsDTL.numWareHouseItmsDTLID FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID = v_numItemCode AND WareHouseItems.numWareHouseID = v_numFromWarehouseID AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber) AND coalesce(WareHouseItmsDTL.numQty,0) = 1) then
								
                           RAISE EXCEPTION 'INVALID_SERIAL_QUANTITY';
                           RETURN;
                        end if;
                        UPDATE
                        WareHouseItmsDTL WHID
                        SET
                        numWareHouseItemID = v_numTempWarehouseItemID
                        FROM
                        WareHouseItems WI
                        WHERE
                        WI.numWareHouseItemID = WHID.numWareHouseItemID AND(WI.numItemID = v_numItemCode
                        AND WI.numWareHouseID = v_numFromWarehouseID
                        AND LOWER(WHID.vcSerialNo) = LOWER(v_vcSerailLotNumber));
                        UPDATE tt_TEMPSERIALLOT SET numQty = 0 WHERE ID = v_j;
                        v_numTempReceivedQty := v_numTempReceivedQty -1;
                     end if;
                  ELSE
                     IF v_bitSerial = true AND(SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID = v_numItemCode AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber)) > 0 then
							
                        RAISE EXCEPTION 'DUPLICATE_SERIAL_NO';
                        RETURN;
                     end if;
                     INSERT INTO WareHouseItmsDTL(numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO)
							VALUES(v_numTempWarehouseItemID
								,v_vcSerailLotNumber
								,(CASE WHEN v_numTempReceivedQty >= v_numSerialLotQty THEN v_numSerialLotQty ELSE v_numTempReceivedQty END)
								,true);
							
                     v_numWareHouseItmsDTLID := CURRVAL('WareHouseItmsDTL_seq');
                     INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID
								,numOppID
								,numOppItemID
								,numWarehouseItmsID
								,numQty)
							VALUES(v_numWareHouseItmsDTLID
								,v_numOppID
								,v_numOppItemID
								,v_numTempWarehouseItemID
								,(CASE WHEN v_numTempReceivedQty >= v_numSerialLotQty THEN v_numSerialLotQty ELSE v_numTempReceivedQty END));
							
                     IF v_numTempReceivedQty >= v_numSerialLotQty then
							
                        v_numTempReceivedQty := v_numTempReceivedQty -v_numSerialLotQty;
                        UPDATE tt_TEMPSERIALLOT SET numQty = 0  WHERE ID = v_j;
                     ELSE
                        UPDATE tt_TEMPSERIALLOT SET numQty = numQty -v_numTempReceivedQty  WHERE ID = v_j;
                        v_numTempReceivedQty := 0;
                     end if;
                  end if;
               end if;
               v_j := v_j::bigint+1;
            END LOOP;
            IF v_numTempReceivedQty > 0 then
				
               RAISE EXCEPTION 'INALID_SERIALLOT';
               RETURN;
            end if;
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   UPDATE
   OpportunityItems
   SET
   numUnitHourReceived = coalesce(numUnitHourReceived,0)+coalesce(v_numQtyReceived,0),numQtyReceived =(CASE WHEN coalesce(v_tintMode,0) = 1 THEN coalesce(numQtyReceived,0)+coalesce(v_numQtyReceived,0) ELSE coalesce(numQtyReceived,0) END)
   WHERE
   numoppitemtCode = v_numOppItemID;


   UPDATE Item I SET dtItemReceivedDate = v_dtItemReceivedDate FROM OpportunityItems OI WHERE OI.numItemCode = I.numItemCode AND OI.numoppitemtCode = v_numOppItemID;


   IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
	
      UPDATE
      OpportunityBizDocItems
      SET
      numVendorInvoiceUnitReceived = coalesce(numVendorInvoiceUnitReceived,0)+coalesce(v_numQtyReceived,0)
      WHERE
      numOppBizDocID = v_numVendorInvoiceBizDocID
      AND numOppItemID = v_numOppItemID;
   end if;

   UPDATE OpportunityMaster SET dtItemReceivedDate = v_dtItemReceivedDate WHERE numOppId = v_numOppID;

   IF EXISTS(SELECT
   OI.numoppitemtCode
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   OM.numOppId = v_numOppID
   AND OM.tintopptype = 2
   AND OM.tintoppstatus = 1
   AND OI.numoppitemtCode = v_numOppItemID
   AND coalesce(I.bitAsset,false) = true) then
	
      PERFORM USP_ItemDepreciation_Save(v_numDomainID,v_numOppID,v_numOppItemID);
   end if;
END; $$;


