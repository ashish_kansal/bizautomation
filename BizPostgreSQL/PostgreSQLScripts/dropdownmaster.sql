-- Stored procedure definition script DRopdownMaster for PostgreSQL
CREATE OR REPLACE FUNCTION DRopdownMaster(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(DropdownId as VARCHAR(255))
      ,cast(ParentId as VARCHAR(255))
      ,cast(ModuleId as VARCHAR(255))
      ,cast(Code as VARCHAR(255))
      ,cast(Name as VARCHAR(255))
      ,cast(Description as VARCHAR(255))
      ,cast(ShowType as VARCHAR(255))
      ,cast(IsActive as VARCHAR(255))
      ,cast(IsSystem as VARCHAR(255))
      ,cast(CompanyId as VARCHAR(255))
      ,cast(CustomField1 as VARCHAR(255))
      ,cast(CustomField2 as VARCHAR(255))
      ,cast(CustomField3 as VARCHAR(255))
      ,cast(CustomField4 as VARCHAR(255))
      ,cast(CustomField5 as VARCHAR(255))
      ,cast(AccountId as VARCHAR(255))
      ,cast(CreatedBy as VARCHAR(255))
      ,cast(ModifiedBy as VARCHAR(255))
      ,cast(CreatedDate as VARCHAR(255))
      ,cast(ModifiedDate as VARCHAR(255))
   FROM DRopdownMaster;
END; $$;












