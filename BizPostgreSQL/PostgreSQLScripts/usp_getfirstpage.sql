-- Stored procedure definition script USP_GetFirstPage for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetFirstPage(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numGroupID NUMERIC(9,0) DEFAULT 0,
v_numRelationShip NUMERIC(9,0) DEFAULT 0,
v_numProfileId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcURL from TabMaster T
   join GroupTabDetails G
   on G.numTabId = T.numTabId
   where (numDomainID = v_numDomainID or bitFixed = true) and numGroupID = v_numGroupID and coalesce(numRelationShip,0) = v_numRelationShip AND coalesce(numProfileID,0) = v_numProfileId
   AND coalesce(G.tintType,0) <> 1
   order by numOrder LIMIT 1;
END; $$;












