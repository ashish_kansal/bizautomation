-- Stored procedure definition script USP_GetTransactionHistoryByCreditCard for PostgreSQL
Create or replace FUNCTION USP_GetTransactionHistoryByCreditCard(v_numDomainId NUMERIC(18,0),
	v_numContactId NUMERIC(18,0),
	v_vcCreditCardNo TEXT DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN
   open SWV_RefCur for select * from
   TransactionHistory
   WHERE
   numDomainID = v_numDomainId AND
   vcCreditCardNo = v_vcCreditCardNo
   ORDER BY
   dtCreatedDate desc LIMIT 1;
END; $$;
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''












