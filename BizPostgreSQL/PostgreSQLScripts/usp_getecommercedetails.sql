-- Stored procedure definition script USP_GetECommerceDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetECommerceDetails(v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_numSiteID INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numSiteID,0) = 0 then
	
      select   numDefaultSiteID INTO v_numSiteID FROM Domain WHERE numDomainId = v_numDomainID;
   end if;
	              
   open SWV_RefCur for SELECT
   coalesce(vcPaymentGateWay,'0') as vcPaymentGateWay,
		coalesce(vcPGWManagerUId,'') as vcPGWManagerUId ,
		coalesce(vcPGWManagerPassword,'') as vcPGWManagerPassword,
		coalesce(bitShowInStock,false) as bitShowInStock,
		coalesce(bitShowQOnHand,false) as bitShowQOnHand,
		coalesce(numDefaultWareHouseID,0) AS numDefaultWareHouseID,
		coalesce(bitCheckCreditStatus,false) as bitCheckCreditStatus,
		coalesce(numRelationshipId,0) AS numRelationshipId,
		coalesce(numProfileId,0) AS numProfileId,
		coalesce(bitHidePriceBeforeLogin,false) AS bitHidePriceBeforeLogin,
		coalesce(numBizDocForCreditCard,0) AS numBizDocForCreditCard,
		coalesce(numBizDocForCreditTerm,0) AS numBizDocForCreditTerm,
		coalesce(bitAuthOnlyCreditCard,false) AS bitAuthOnlyCreditCard,
		coalesce(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
		coalesce(bitSendEmail,true) AS bitSendEmail,
		coalesce(vcGoogleMerchantID,'') AS vcGoogleMerchantID,
		coalesce(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
		coalesce(IsSandbox,false) AS IsSandbox,
		coalesce(numSiteId,0) AS numSiteID,
		coalesce(vcPaypalUserName,'') AS vcPaypalUserName,
		coalesce(vcPaypalPassword,'') AS vcPaypalPassword,
		coalesce(vcPaypalSignature,'') AS vcPaypalSignature,
		coalesce(IsPaypalSandbox,false) AS IsPaypalSandbox,
		coalesce(bitSkipStep2,false) AS bitSkipStep2,
		coalesce(bitDisplayCategory,false) AS bitDisplayCategory,
		coalesce(bitShowPriceUsingPriceLevel,false) AS bitShowPriceUsingPriceLevel,
		coalesce(bitEnableSecSorting,false) AS bitEnableSecSorting,
		coalesce(bitSortPriceMode,false) AS bitSortPriceMode,
		coalesce(numPageSize,0) AS numPageSize,
		coalesce(numPageVariant,0) AS numPageVariant,
		coalesce(bitAutoSelectWarehouse,false) AS bitAutoSelectWarehouse,
		coalesce(bitPreSellUp,false) AS bitPreSellUp,
		coalesce(bitPostSellUp,false) AS bitPostSellUp,
		coalesce(dcPostSellDiscount,0) AS dcPostSellDiscount,
		coalesce(numDefaultClass,0) AS numDefaultClass,
		vcPreSellUp
		,vcPostSellUp
		,coalesce(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
		,coalesce(tintPreLoginProceLevel,0) AS tintPreLoginProceLevel
		,coalesce(bitHideAddtoCart,false) AS bitHideAddtoCart
		,coalesce(bitShowPromoDetailsLink,false) AS bitShowPromoDetailsLink
		,coalesce(tintWarehouseAvailability,1) AS tintWarehouseAvailability
		,coalesce(bitDisplayQtyAvailable,false) AS bitDisplayQtyAvailable
		,coalesce(bitElasticSearch,false) AS bitElasticSearch
   FROM
   eCommerceDTL
   WHERE
   numDomainID = v_numDomainID
   AND (numSiteId = v_numSiteID OR coalesce(v_numSiteID,0) = 0);
END; $$;












