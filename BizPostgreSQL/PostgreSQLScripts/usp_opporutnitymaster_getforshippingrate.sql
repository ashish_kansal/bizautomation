-- Stored procedure definition script USP_OpporutnityMaster_GetForShippingRate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpporutnityMaster_GetForShippingRate(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numWareHouseID NUMERIC(18,0)
	,v_dtShipByDate DATE
	,v_bitFromService BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT 
   OM.numDomainId
		,OM.numOppId
		,coalesce(OM.monMarketplaceShippingCost,0) AS monMarketplaceShippingCost
		,coalesce(OM.intUsedShippingCompany,0) AS numShipVia
		,coalesce(OM.numShippingService,0) AS numShipService
		,coalesce(DM.intShippingCompany,0) AS numPreferredShipVia
		,coalesce(DM.numDefaultShippingServiceID,0) AS numPreferredShipService
		,W.numWareHouseID
		,OM.dtExpectedDate
		,coalesce(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) AS dtShipByDate
		,MSFSC.tintType
		,MSFSC.vcPriorities
		,MSFSC.numShipVia
		,MSFSC.numShipService
		,MSFSC.bitOverride
		,MSFSC.fltWeight
		,MSFSC.numShipViaOverride
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   Domain D
   ON
   OM.numDomainId = D.numDomainId
   INNER JOIN
   MassSalesFulfillmentShippingConfig MSFSC
   ON
   OM.numDomainId = MSFSC.numDomainID
   AND OM.tintSource = MSFSC.numOrderSource
   AND OM.tintSourceType = MSFSC.tintSourceType
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   INNER JOIN
   Warehouses W
   ON
   WI.numWareHouseID = W.numWareHouseID
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   OpportunityMasterShippingRateError OMSRE
   ON
   OM.numDomainId = OMSRE.numDomainID
   AND OM.numOppId = OMSRE.numOppID
   WHERE
		(coalesce(v_numDomainID,0) = 0 OR OM.numDomainId = v_numDomainID)
   AND (coalesce(v_numOppID,0) = 0 OR OM.numOppId = v_numOppID)
   AND (coalesce(v_numWareHouseID,0) = 0 OR W.numWareHouseID = v_numWareHouseID)
   AND 1 =(CASE WHEN v_bitFromService = true THEN 1 ELSE(CASE WHEN coalesce(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) = v_dtShipByDate THEN 1 ELSE 0 END) END)
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND 1 =(CASE WHEN coalesce(v_bitFromService,false) = true THEN(CASE WHEN DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -OM.bintCreatedDate) <= 1 THEN 1 ELSE 0 END) ELSE 1 END)
   AND coalesce(D.numShippingServiceItemID,0) > 0
   AND coalesce(OMSRE.intNoOfTimeFailed,0) < 3
   AND NOT EXISTS(SELECT cast(OIInner.numoppitemtCode as NUMERIC(18,0)) FROM OpportunityItems OIInner WHERE OIInner.numOppId = OM.numOppId AND OIInner.numItemCode = D.numShippingServiceItemID AND OIInner.numShipFromWarehouse = W.numWareHouseID AND OIInner.ItemReleaseDate = coalesce(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)))
   GROUP BY
   OM.numDomainId,OM.numOppId,OM.monMarketplaceShippingCost,OM.dtExpectedDate,
   OM.intUsedShippingCompany,OM.numShippingService,DM.intShippingCompany,
   DM.numDefaultShippingServiceID,OM.dtReleaseDate,OI.ItemReleaseDate,
   W.numWareHouseID,MSFSC.tintType,MSFSC.vcPriorities,MSFSC.numShipVia,
   MSFSC.numShipService,MSFSC.bitOverride,MSFSC.fltWeight,MSFSC.numShipViaOverride,
   OMSRE.intNoOfTimeFailed
   ORDER BY
   coalesce(OMSRE.intNoOfTimeFailed,0) ASC,OM.numOppId LIMIT 15;
END; $$;












