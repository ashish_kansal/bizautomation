CREATE OR REPLACE FUNCTION usp_TicklerStages(v_numUserCntID NUMERIC(9,0) DEFAULT null,          
v_numDomainID NUMERIC(9,0) DEFAULT null,          
v_startDate TIMESTAMP DEFAULT NULL,          
v_endDate TIMESTAMP DEFAULT NULL,          
v_numOppId NUMERIC(9,0) DEFAULT null,        
v_OppType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_OppType = 1 then -- Sales or Purchase        

      open SWV_RefCur for
      select vcStageDetail from OpportunityMaster opp
      join OpportunityStageDetails stg
      on stg.numoppid = opp.numOppId
      where stg.numAssignTo = v_numUserCntID and opp.numDomainId = v_numDomainID and opp.numOppId = v_numOppId
      and bitStageCompleted = false;
   end if;        
   if  v_OppType = 2 then --- Projects        

      open SWV_RefCur for
      select vcStageDetail from ProjectsMaster Pro
      join ProjectsStageDetails stg
      on stg.numProId = Pro.numProId
      where stg.numAssignTo = v_numUserCntID and Pro.numdomainId = v_numDomainID and Pro.numProId = v_numOppId
      and bitStageCompleted = false;
   end if;
   RETURN;
END; $$;


