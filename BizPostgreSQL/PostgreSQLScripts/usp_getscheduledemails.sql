-- Stored procedure definition script Usp_GetScheduledEmails for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetScheduledEmails(v_numScheduleid NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcEmail AS "vcEmail" from CustRptSchContacts CRS
   join AdditionalContactsInformation AC on   CRS.numcontactId =  AC.numContactId
   where
   numScheduleid = v_numScheduleid;
END; $$;












