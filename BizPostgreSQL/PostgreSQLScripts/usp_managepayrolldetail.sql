-- Stored procedure definition script USP_ManagePayrollDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePayrollDetail(v_numDomainId NUMERIC(9,0),
    v_numPayrollHeaderID NUMERIC(9,0),
    v_numPayrollDetailID NUMERIC(9,0),
    v_numCheckStatus NUMERIC(9,0),
    v_tintMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then  --Approve Checks

      UPDATE PayrollDetail SET numCheckStatus = v_numCheckStatus WHERE
      numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID;
	
	--If Paid then check commission is fully paid
      IF v_numCheckStatus = 2 then
	
         UPDATE BizDocComission SET bitCommisionPaid = true WHERE numDomainId = v_numDomainId
         AND numComissionID IN(SELECT BC.numComissionID FROM BizDocComission BC JOIN PayrollTracking PT ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainID
         JOIN PayrollDetail PD ON PT.numPayrollHeaderID = PD.numPayrollHeaderID AND PT.numPayrollDetailID = PD.numPayrollDetailID
         WHERE PD.numCheckStatus = 2 AND PD.numDomainID = v_numDomainId
         AND BC.numComissionID IN(SELECT numComissionID FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID)
         GROUP BY BC.numComissionID HAVING SUM(PT.monCommissionAmt) = MIN(BC.numComissionAmount));
      end if;
   ELSEIF v_tintMode = 2
   then  --Delete

      DELETE FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID;
      DELETE FROM PayrollDetail WHERE numDomainID = v_numDomainId AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID;
   end if;
   RETURN;
END; $$;


