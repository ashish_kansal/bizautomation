-- Stored procedure definition script USP_CfwEditFieldList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE         OR REPLACE FUNCTION USP_CfwEditFieldList(v_strFieldList TEXT DEFAULT '',
v_relId NUMERIC(9,0) DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN

   update CFW_Fld_Dtl
   set tintFldReq = X.req
   from
   XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFieldList AS XML)
		COLUMNS
			id FOR ORDINALITY,
			fieldid NUMERIC(9,0) PATH 'fieldid',
			req SMALLINT PATH 'req'
	) AS X 
   where numFieldId = X.fieldid and numRelation = v_relId;

   RETURN;
END; $$;


