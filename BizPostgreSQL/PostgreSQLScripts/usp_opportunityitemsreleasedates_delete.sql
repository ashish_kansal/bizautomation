-- Stored procedure definition script USP_OpportunityItemsReleaseDates_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItemsReleaseDates_Delete(v_ID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OpportunityItemsReleaseDates WHERE numID = v_ID;
   RETURN;
END; $$;

