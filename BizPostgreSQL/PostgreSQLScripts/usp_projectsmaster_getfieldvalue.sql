-- Stored procedure definition script USP_ProjectsMaster_GetFieldValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectsMaster_GetFieldValue(v_numDomainID NUMERIC(18,0),    
	v_numProId NUMERIC(18,0),
	v_vcFieldName VARCHAR(100), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcFieldName = 'numAddressID' then
	
      open SWV_RefCur for
      SELECT numAddressID FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = v_numProId;
   ELSEIF v_vcFieldName = 'numDivisionID'
   then
	
      open SWV_RefCur for
      SELECT numDivisionId FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = v_numProId;
   ELSEIF v_vcFieldName = 'numIntPrjMgr'
   then
	
      open SWV_RefCur for
      SELECT numIntPrjMgr FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = v_numProId;
   ELSEIF v_vcFieldName = 'vcCompanyName'
   then
	
      open SWV_RefCur for
      SELECT
      CompanyInfo.vcCompanyName
      FROM
      ProjectsMaster 
      INNER JOIN DivisionMaster  ON ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN CompanyInfo  ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      ProjectsMaster.numdomainId = v_numDomainID
      AND ProjectsMaster.numProId = v_numProId;
   ELSE
      RAISE EXCEPTION 'FIELD_NOT_FOUND';
   end if;
   RETURN;
END; $$;


