-- Stored procedure definition script USP_CheckCommissionAppliesTo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckCommissionAppliesTo(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
 v_tintComAppliesTo SMALLINT DEFAULT NULL,
 v_tintCommissionType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommissionType_Old  SMALLINT;
   v_Total  INTEGER;
BEGIN
   select   coalesce(tintCommissionType,1) INTO v_tintCommissionType_Old FROM Domain WHERE numDomainId = v_numDomainId;
	
   select   count(*) INTO v_Total From CommissionRules Where numDomainID = v_numDomainId;
	
   IF (v_tintCommissionType != v_tintCommissionType_Old) then
	
      open SWV_RefCur for
      SELECT v_Total;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


