-- Stored procedure definition script USP_OppSaveSubStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppSaveSubStage(v_byteMode SMALLINT DEFAULT null,  
 v_OppID NUMERIC(9,0) DEFAULT null,  
 v_stageDetailsId NUMERIC(9,0) DEFAULT null,  
 v_strSubStage TEXT DEFAULT '',  
 v_numSubStageHdrID NUMERIC(9,0) DEFAULT NULL,  
 v_numSubStageID NUMERIC(9,0) DEFAULT null, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   if v_byteMode = 0 then

      delete from OpportunitySubStageDetails where numStageDetailsId = v_stageDetailsId and numOppId = v_OppID;
      insert into OpportunitySubStageDetails(numOppId,
    numStageDetailsId,
    numSubStageID,
    numProcessListId,
    vcSubStageDetail,
    vcComments,
    bitStageCompleted)
      select v_OppID,v_stageDetailsId,v_numSubStageID,X.* from(SELECT * FROM 
	  XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strSubStage AS XML)
		COLUMNS
			numSalesProsessList NUMERIC(9,0) PATH 'numSalesProsessList',
			vcSubStageDetail VARCHAR(1000) PATH 'vcSubStageDetail',
			vcComments VARCHAR(250) PATH 'vcComments',
			bitStageCompleted BOOLEAN PATH 'bitStageCompleted'
	)) X;

   end if;  
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numSubStageElmtID,numOppId,numStageDetailsId,
    numSubStageID ,
    numProcessListId as numSalesProsessList,
    vcSubStageDetail,
    vcComments,
    bitStageCompleted
      from OpportunitySubStageDetails
      where numOppId = v_OppID and numStageDetailsId = v_stageDetailsId;
   end if;  
  
   if v_byteMode = 2 then

      open SWV_RefCur for
      select  numSubstageDtlid,
  0 as numOppId,
  0 as numStageDetailsId,
  0 as numSubStageID,
  numProcessListID as numSalesProsessList,
  vcSubStageElemt as vcSubStageDetail,
  '' AS vcComments,
  false as bitStageCompleted
      from SubStageDetails
      where  numSubStageHdrID = v_numSubStageHdrID;
   end if;
   RETURN;
END; $$;


