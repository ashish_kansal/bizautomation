-- Stored procedure definition script usp_GetAutoRoutingRuleList for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAutoRoutingRuleList(v_numDomainID NUMERIC DEFAULT 0,    
 v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmpId  NUMERIC(9,0);
BEGIN
   IF NOT EXISTS(SELECT numRoutID FROM RoutingLeads WHERE numDomainID = v_numDomainID AND bitDefault = true) then
          
     --SELECT @numEmpId = um.numUserID FROM UserMaster UM WHERE um.numDomainID=@numDomainID         
        
      INSERT INTO RoutingLeads(numDomainID, vcRoutName,tintEqualTo, numCreatedBy, dtCreatedDate,numModifiedBy, dtModifiedDate,bitDefault, tintPriority)
                         VALUES(v_numDomainID, 'Default',0, v_numUserCntID, TIMEZONE('UTC',now()), v_numUserCntID, TIMEZONE('UTC',now()),  v_numUserCntID, 0);
     
      INSERT INTO RoutingLeadDetails(numRoutID, numEmpId, intAssignOrder) VALUES(CURRVAL('RoutingLeads_seq'), 0, 0);
   end if;        
                  
   open SWV_RefCur for SELECT numRoutID, vcRoutName, bitDefault,coalesce(tintPriority,0) AS tintPriority
   FROM RoutingLeads
   WHERE
   numDomainID = v_numDomainID
   ORDER BY tintPriority;
END; $$;












