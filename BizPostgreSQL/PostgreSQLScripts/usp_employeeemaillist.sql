-- Stored procedure definition script USP_EmployeeEmailList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EmployeeEmailList(v_numUserID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT UM.vcEmailID as "numUserID"
   from UserMaster UM
   where UM.numUserId = v_numUserID;
END; $$;












