-- Stored procedure definition script USP_PaymentGateway_GetDefault for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PaymentGateway_GetDefault(v_numDomainID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numSiteID,0) > 0 AND(SELECT coalesce(intPaymentGateWay,0) FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteID) > 0 then
	
      open SWV_RefCur for
      SELECT
      coalesce(S.intPaymentGateWay,0) AS intPaymentGateWay
      FROM
      Sites S
      WHERE
      S.numDomainID = v_numDomainID
      AND coalesce(S.numSiteID,0) = v_numSiteID;
   ELSE
      open SWV_RefCur for
      SELECT
      coalesce(D.intPaymentGateWay,0) AS intPaymentGateWay
      FROM
      Domain D
      WHERE
      D.numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;


