-- Stored procedure definition script USP_SelectGoogleContactGroups for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SelectGoogleContactGroups(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numContactId NUMERIC(9,0) DEFAULT 0,
v_tintType NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM GoogleContactGroups WHERE numDomainID = v_numDomainID AND numContactId = v_numContactId AND tintType = v_tintType;
END; $$;
-- exec usp_SetDripCampaign @vcContactIDList='27,10366',@numECampaignID=-1,@numUserCntID=1












