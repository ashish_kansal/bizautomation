-- Stored procedure definition script usp_ManageCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageCompany(
--                
v_numCompanyId NUMERIC DEFAULT 0 ,                
 v_vcCompanyName VARCHAR(100) DEFAULT '',                
 v_numCompanyType NUMERIC DEFAULT 0,                
 v_numCompanyRating NUMERIC DEFAULT 0,                      
 v_numCompanyIndustry NUMERIC DEFAULT 0,                
 v_numCompanyCredit NUMERIC DEFAULT 0,                      
 v_txtComments TEXT DEFAULT '',                
 v_vcWebSite VARCHAR(255) DEFAULT '',                
 v_vcWebLabel1 VARCHAR(100) DEFAULT '',                
 v_vcWebLink1 VARCHAR(255) DEFAULT '',                
 v_vcWebLabel2 VARCHAR(100) DEFAULT '',                
 v_vcWebLink2 VARCHAR(255) DEFAULT '',                
 v_vcWebLabel3 VARCHAR(100) DEFAULT '',                
 v_vcWebLink3 VARCHAR(255) DEFAULT '',                
 v_vcWebLabel4 VARCHAR(100) DEFAULT '',                
 v_vcWebLink4 VARCHAR(255) DEFAULT '',                
 v_numAnnualRevID NUMERIC DEFAULT 0,                
 v_numNoOfEmployeesId NUMERIC DEFAULT 0,                
 v_vcHow NUMERIC DEFAULT 0,                
 v_vcProfile NUMERIC DEFAULT 0,                
 v_numUserCntID NUMERIC DEFAULT 0,                                               
 v_bitPublicFlag BOOLEAN DEFAULT false,                         
 v_numDomainID NUMERIC DEFAULT 0,
 v_bitSelectiveUpdate BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sql  VARCHAR(1000);
BEGIN
   IF v_numCompanyId = 0 then
  
      IF v_numDomainID = 170 AND (POSITION('www.' IN v_vcCompanyName) > 0 OR POSITION('http://' IN v_vcCompanyName) > 0 OR POSITION('https://' IN v_vcCompanyName) > 0) then
	
         RAISE EXCEPTION 'INVALID';
         RETURN;
      end if;
      INSERT INTO CompanyInfo(vcCompanyName,
   numCompanyType,
   numCompanyRating,
   numCompanyIndustry,
   numCompanyCredit,
   txtComments,
   vcWebSite,
   vcWebLabel1,
   vcWebLink1,
   vcWebLabel2,
   vcWebLink2,
   vcWebLabel3,
   vcWebLink3,
   vcWeblabel4,
   vcWebLink4,
   numAnnualRevID,
   numNoOfEmployeesId,
   vcHow,
   vcProfile,
   numCreatedBy,
   bintCreatedDate,
   numModifiedBy,
   bintModifiedDate,
   bitPublicFlag,
   numDomainID)
 values(v_vcCompanyName,
   v_numCompanyType,
   v_numCompanyRating,
   v_numCompanyIndustry,
   v_numCompanyCredit,
   v_txtComments,
   v_vcWebSite,
   v_vcWebLabel1,
   v_vcWebLink1,
   v_vcWebLabel2,
   v_vcWebLink2,
   v_vcWebLabel3,
   v_vcWebLink3,
   v_vcWebLabel4,
   v_vcWebLink4,
   v_numAnnualRevID,
   v_numNoOfEmployeesId,
   v_vcHow,
   v_vcProfile,
   v_numUserCntID,
   TIMEZONE('UTC',now()),
   v_numUserCntID,
   TIMEZONE('UTC',now()),
   v_bitPublicFlag,
   v_numDomainID);
              
      v_numCompanyId := CURRVAL('CompanyInfo_seq');
      open SWV_RefCur for
      select v_numCompanyId;
   ELSEIF v_numCompanyId > 0
   then
  
      IF v_bitSelectiveUpdate = false then
	
         UPDATE CompanyInfo SET
         vcCompanyName = v_vcCompanyName,numCompanyType = v_numCompanyType,numCompanyRating = v_numCompanyRating,
         numCompanyIndustry = v_numCompanyIndustry,
         numCompanyCredit = v_numCompanyCredit,txtComments = v_txtComments,
         vcWebSite = v_vcWebSite,vcWebLabel1 = v_vcWebLabel1,vcWebLabel2 = v_vcWebLabel2,
         vcWebLabel3 = v_vcWebLabel3,vcWeblabel4 = v_vcWebLabel4,vcWebLink1 = v_vcWebLink1,
         vcWebLink2 = v_vcWebLink2,vcWebLink3 = v_vcWebLink3,
         vcWebLink4 = v_vcWebLink4,numAnnualRevID = v_numAnnualRevID,numNoOfEmployeesId = v_numNoOfEmployeesId,
         vcHow = v_vcHow,vcProfile = v_vcProfile,
         numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
         bitPublicFlag = v_bitPublicFlag
         WHERE
         numCompanyId = v_numCompanyId    and numDomainID = v_numDomainID;
         open SWV_RefCur for
         SELECT v_numCompanyId;
      ELSEIF v_bitSelectiveUpdate = true
      then
         v_sql := 'update CompanyInfo Set ';
         IF(LENGTH(v_vcCompanyName) > 0) then
		
            v_sql := coalesce(v_sql,'') || ' vcCompanyName=''' || coalesce(v_vcCompanyName,'') || ''', ';
         end if;
         IF(LENGTH(v_vcWebSite) > 0) then
		
            v_sql := coalesce(v_sql,'') || ' vcWebSite=''' || coalesce(v_vcWebSite,'') || ''', ';
         end if;
         IF(v_numUserCntID > 0) then
		
            v_sql := coalesce(v_sql,'') || ' numModifiedBy=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(10)),1,10) || ', ';
         end if;
         v_sql := SUBSTR(v_sql,0,LENGTH(v_sql)) || ' ';
         v_sql := coalesce(v_sql,'') || 'where numCompanyId= ' || SUBSTR(CAST(v_numCompanyId AS VARCHAR(10)),1,10);
         RAISE NOTICE '%',v_sql;
         EXECUTE v_sql;
      end if;
   end if;
END; $$;


