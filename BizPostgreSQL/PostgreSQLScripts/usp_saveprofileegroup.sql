-- Stored procedure definition script USP_SaveProfileEGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveProfileEGroup(v_strcontact TEXT DEFAULT NULL,
    v_strGroupName VARCHAR(10) DEFAULT NULL,
    v_numDomainID NUMERIC DEFAULT NULL,
    v_numUserCntID NUMERIC DEFAULT NULL,
    v_numGroupID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_strGroupName <> '' then
    
      IF v_strGroupName NOT IN(SELECT DISTINCT
      vcEmailGroupName
      FROM      ProfileEmailGroup) then
            
         INSERT  INTO ProfileEmailGroup(vcEmailGroupName,
                                                 numDomainID,
                                                 numCreatedBy,
                                                 dtCreatedDate,
                                                 numModifiedBy,
                                                 dtModifiedDate)
                VALUES(v_strGroupName,
                          v_numDomainID,
                          v_numUserCntID,
                          TIMEZONE('UTC',now()),
                          v_numUserCntID,
                          TIMEZONE('UTC',now()));
                
         IF SUBSTR(CAST(v_strcontact AS VARCHAR(10)),1,10) <> '' then
            INSERT  INTO ProfileEGroupDTL
			(
				numContactId,
                numEmailGroupID
			)
            SELECT 
				X.numContactId,
                (SELECT numEmailGroupID FROM ProfileEmailGroup WHERE vcEmailGroupName = v_strGroupName LIMIT 1)
            FROM
			(
				SELECT 
					* 
				FROM 
				XMLTABLE
				(
					'NewDataSet/Table'
					PASSING 
						CAST(v_strcontact AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numContactId NUMERIC(18,0) PATH 'numContactId'
				) AS TEMP
			) X;
         end if;
      end if;
   end if;


	IF v_numGroupID <> 0 then
		IF SUBSTR(CAST(v_strcontact AS VARCHAR(10)),1,10) <> '' then
			NULL;
		end if;
		BEGIN
			INSERT  INTO ProfileEGroupDTL(numContactId,
											numEmailGroupID)
			SELECT  Y.numContactId,
							v_numGroupID
			FROM
			(
			SELECT 
					* 
				FROM 
				XMLTABLE
				(
					'NewDataSet/Table'
					PASSING 
						CAST(v_strcontact AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numContactId NUMERIC(18,0) PATH 'numContactId'
				) AS TEMP
				WHERE numContactId NOT IN(SELECT  numContactId
				FROM    ProfileEGroupDTL
				WHERE   numEmailGroupID = v_numGroupID)
		) Y;
		END;
   end if;

   RETURN;
END; $$;


