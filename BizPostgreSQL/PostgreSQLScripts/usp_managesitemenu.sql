-- Stored procedure definition script USP_ManageSiteMenu for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSiteMenu(INOUT v_numMenuID NUMERIC(9,0) DEFAULT 0 ,
	v_vcTitle VARCHAR(100) DEFAULT NULL,
	v_vcNavigationURL VARCHAR(1000) DEFAULT NULL,
	v_tintLevel SMALLINT DEFAULT NULL,
	v_numParentID NUMERIC(9,0) DEFAULT NULL,
	v_numSiteID NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_numPageID NUMERIC(9,0) DEFAULT NULL,
	v_bitStatus BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intDisplayOrder  INTEGER;
BEGIN
   IF v_numPageID = 0 then
      v_numPageID := NULL;
   end if;
   IF v_numMenuID = 0 AND (NOT EXISTS(SELECT vcTitle FROM SiteMenu WHERE vcTitle = v_vcTitle
   AND numSiteID = v_numSiteID)) then
      select(coalesce(MAX(intDisplayOrder),0)+1) INTO v_intDisplayOrder FROM SiteMenu WHERE numSiteID = v_numSiteID;
      INSERT INTO SiteMenu(vcTitle,
		vcNavigationURL,
		numPageID,
		tintLevel,
		numParentID,
		numSiteID,
		numDomainID,
		bitStatus,
		intDisplayOrder)
	VALUES(v_vcTitle,
		v_vcNavigationURL,
		v_numPageID,
		v_tintLevel,
		v_numParentID,
		v_numSiteID,
		v_numDomainID,
		v_bitStatus,
		v_intDisplayOrder);
	
      v_numMenuID := CURRVAL('SiteMenu_seq');
   ELSE
      UPDATE SiteMenu SET
      vcTitle = v_vcTitle,vcNavigationURL = v_vcNavigationURL,numPageID = v_numPageID,
      tintLevel = v_tintLevel,numParentID = v_numParentID,bitStatus = v_bitStatus
      WHERE numMenuID = v_numMenuID;
   end if;
   RETURN;
END; $$;




