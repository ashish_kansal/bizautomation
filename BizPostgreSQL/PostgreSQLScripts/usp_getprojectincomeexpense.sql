CREATE OR REPLACE FUNCTION USP_GetProjectIncomeExpense(v_numDomainID NUMERIC(9,0),
v_numProId	NUMERIC(9,0),
v_Mode SMALLINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_BillableHours  DOUBLE PRECISION;
   v_NonBillableHours  DOUBLE PRECISION;
   v_Income  DECIMAL(20,5);
   v_Income1  DECIMAL(20,5);
   v_Income2  DECIMAL(20,5);
   v_Expense  DECIMAL(20,5);
   v_Expense1  DECIMAL(20,5);
   v_Expense2  DECIMAL(20,5);
   v_Expense3  DECIMAL(20,5);
   v_Expense4  DECIMAL(20,5);
   v_numAccountID  NUMERIC(9,0);
BEGIN
   IF v_Mode = 0 then

      select   SUM(CAST(CAST(CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)) AS VARCHAR(100)) AS  DOUBLE PRECISION)) INTO v_BillableHours from  timeandexpense where numProId = v_numProId
      and numCategory = 1 and tintTEType = 2 AND numtype = 1; 
		--  	 Non-Billable Hours: 
      select   SUM(CAST(CAST(CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)) AS VARCHAR(100)) AS  DOUBLE PRECISION)) INTO v_NonBillableHours from  timeandexpense where numProId = v_numProId
      and numCategory = 1 and tintTEType = 2 AND numtype = 2;
		
		--Income of the Project = Sum(Item*rate) of SO linked with the Project.
      select   SUM(OBI.monPrice*OBI.numUnitHour) INTO v_Income1 FROM ProjectsOpportunities PO
      LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
      LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
      LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 1; 
			--below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
      select   SUM((OBI.monPrice*OBI.numUnitHour)) INTO v_Income2 FROM OpportunityMaster OM
      LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numoppid = OM.numOppId
      LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId WHERE OM.numOppId
      IN(SELECT
         OL.numChildOppID
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityLinking OL ON OL.numParentOppID = PO.numOppId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2);
      v_Income := coalesce(v_Income1,0)+coalesce(v_Income2,0);
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project] + Bills added to project
      select   SUM(OBI.monPrice*OBI.numUnitHour) INTO v_Expense1 FROM ProjectsOpportunities PO
      LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
      LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
      LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
      LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
      LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
      AND I.charItemType = 'P'
      AND COA.numAccountId IS NOT NULL;
      select   SUM(OBI.monPrice*OBI.numUnitHour) INTO v_Expense4 FROM ProjectsOpportunities PO
      LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
      LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
      LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
      LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
      LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
      AND I.charItemType <> 'P'
      AND COA.numAccountId IS NOT null;
      select   numAccountID INTO v_numAccountID FROM ProjectsMaster WHERE numProId = v_numProId;
      select   SUM(numDebitAmt) INTO v_Expense2 FROM General_Journal_Header H
      INNER JOIN General_Journal_Details D ON H.numJOurnal_Id = D.numJournalId
      LEFT OUTER JOIN Chart_Of_Accounts COA ON D.numChartAcntId = COA.numAccountId WHERE D.numChartAcntId = v_numAccountID AND H.numProjectID = v_numProId AND COA.numAccountId IS NOT null;
		--Bill Expense
      select   SUM(OBD.monAmount) INTO v_Expense3 FROM ProjectsOpportunities PO
      LEFT OUTER JOIN OpportunityBizDocsDetails OBD ON OBD.numBizDocsPaymentDetId = PO.numBillId
      LEFT OUTER JOIN Chart_Of_Accounts COA ON OBD.numExpenseAccount = COA.numAccountId WHERE PO.numProId = v_numProId AND COA.numAccountId IS NOT null;
      v_Expense := coalesce(v_Expense1,0)+coalesce(v_Expense2,0)+coalesce(v_Expense3,0)+coalesce(v_Expense4,0);
      open SWV_RefCur for
      SELECT coalesce(v_BillableHours,0) AS BillableHours,coalesce(v_NonBillableHours,0) AS NonBillableHours,coalesce(v_Income,0) AS Income, coalesce(v_Expense,0) AS Expense;
      open SWV_RefCur2 for
      SELECT DISTINCT PM.numProId,
				PM.numAccountID,
				coalesce(PM.vcProjectName,'') AS vcProjectName,
				coalesce(CM.vcContractName,'') AS vcContractName,
				coalesce(PM.numcontractId,0) AS numContractId,
				PM.tintProStatus,
				coalesce(PM.txtComments,'') AS txtComments,
				PM.numDivisionId
		--        TE.[numType],
		--        TE.[monAmount]
      FROM   ProjectsMaster PM
      INNER JOIN timeandexpense TE ON PM.numProId = TE.numProId
      LEFT OUTER JOIN ContractManagement CM ON PM.numcontractId = CM.numContractId
      WHERE  PM.numProId = v_numProId AND PM.numdomainId = v_numDomainID;
		 
		 --below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
      open SWV_RefCur3 for
      SELECT SUM(X.IncomeAmount) AS IncomeAmount,X.vcAccountName,X.Type FROM(SELECT
         SUM((OBI.monPrice*OBI.numUnitHour)) AS IncomeAmount,
			COA.vcAccountName,
			'Income' AS Type
         FROM OpportunityMaster OM
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numoppid = OM.numOppId
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE OM.numOppId
         IN(SELECT
            OL.numChildOppID
            FROM ProjectsOpportunities PO
            LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
            LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
            LEFT OUTER JOIN OpportunityLinking OL ON OL.numParentOppID = PO.numOppId
            WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2)
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM((OBI.monPrice*OBI.numUnitHour)) AS IncomeAmount,
			COA.vcAccountName,
			'Income' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 1
         AND COA.numAccountId IS NOT null
         GROUP BY COA.numAccountId,COA.vcAccountName) as X
      GROUP BY X.vcAccountName,X.Type
      ORDER BY vcAccountName;
		
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project]
      open SWV_RefCur4 for
      SELECT SUM(X.ExpenseAmount) AS ExpenseAmount,X.vcAccountName,X.Type FROM(SELECT
         SUM(OBI.monPrice*OBI.numUnitHour) AS ExpenseAmount,
		COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
         AND I.charItemType = 'P'
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM(OBI.monPrice*OBI.numUnitHour) AS ExpenseAmount,
		COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
         AND I.charItemType <> 'P'
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM(numDebitAmt) AS ExpenseAmount,COA.vcAccountName,'Expense' AS Type FROM General_Journal_Header H
         INNER JOIN General_Journal_Details D ON H.numJOurnal_Id = D.numJournalId
         LEFT OUTER JOIN Chart_Of_Accounts COA ON D.numChartAcntId = COA.numAccountId
         WHERE D.numChartAcntId = v_numAccountID AND H.numProjectID = v_numProId AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION -- Bills expense
         SELECT
         coalesce(SUM(OBD.monAmount),0) AS ExpenseAmount,
			COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityBizDocsDetails OBD ON OBD.numBizDocsPaymentDetId = PO.numBillId
         LEFT OUTER JOIN Chart_Of_Accounts COA ON OBD.numExpenseAccount = COA.numAccountId
         WHERE PO.numProId = v_numProId AND COA.numAccountId IS NOT null
         GROUP BY vcAccountName) as X
      GROUP BY X.vcAccountName,X.Type
      ORDER BY vcAccountName;
   end if;
 
   IF v_Mode = 1 then
	
      select   numAccountID INTO v_numAccountID FROM ProjectsMaster WHERE numProId = v_numProId;
		
		--below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
      open SWV_RefCur for
      SELECT SUM(X.IncomeAmount) AS IncomeAmount,SUM(X.ExpenseAmount) AS ExpenseAmount,X.vcAccountName,X.Type FROM(SELECT
         SUM((OBI.monPrice*OBI.numUnitHour)) AS Amount,
			SUM((OBI.monPrice*OBI.numUnitHour)) AS IncomeAmount,
			0 AS ExpenseAmount,
			COA.vcAccountName,
			'Income' AS Type
         FROM OpportunityMaster OM
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numoppid = OM.numOppId
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE OM.numOppId
         IN(SELECT
            OL.numChildOppID
            FROM ProjectsOpportunities PO
            LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
            LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
            LEFT OUTER JOIN OpportunityLinking OL ON OL.numParentOppID = PO.numOppId
            WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2)
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM((OBI.monPrice*OBI.numUnitHour)) AS Amount,
			SUM((OBI.monPrice*OBI.numUnitHour)) AS IncomeAmount,
			0 AS ExpenseAmount,
			COA.vcAccountName,
			'Income' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 1
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION 
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project]
         SELECT
         SUM(OBI.monPrice*OBI.numUnitHour) AS Amount,
		0 AS IncomeAmount,
		SUM(OBI.monPrice*OBI.numUnitHour) AS ExpenseAmount,
		COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
         AND I.charItemType = 'P'
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM(OBI.monPrice*OBI.numUnitHour) AS Amount,
		0 AS IncomeAmount,
		SUM(OBI.monPrice*OBI.numUnitHour) AS ExpenseAmount,
		COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityMaster OM ON OM.numOppId = PO.numOppId
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppBizDocsId = PO.numOppBizDocID
         LEFT OUTER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocID = PO.numOppBizDocID
         LEFT OUTER JOIN Item I ON OBI.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numIncomeChartAcntId = COA.numAccountId
         WHERE PO.numProId = v_numProId AND OB.bitAuthoritativeBizDocs = 1 AND OM.tintopptype = 2
         AND I.charItemType <> 'P'
         AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION
         SELECT
         SUM(numDebitAmt) AS Amount,
			0 AS IncomeAmount,
			SUM(numDebitAmt) AS ExpenseAmount,COA.vcAccountName,'Expense' AS Type FROM General_Journal_Header H
         INNER JOIN General_Journal_Details D ON H.numJOurnal_Id = D.numJournalId
         LEFT OUTER JOIN Chart_Of_Accounts COA ON D.numChartAcntId = COA.numAccountId
         WHERE D.numChartAcntId = v_numAccountID AND H.numProjectID = v_numProId AND COA.numAccountId IS NOT null
         GROUP BY  COA.vcAccountName
         UNION -- Bills expense
         SELECT
         coalesce(SUM(OBD.monAmount),0) AS Amount,
			0 AS IncomeAmount,
			coalesce(SUM(OBD.monAmount),0) AS ExpenseAmount,
			COA.vcAccountName,'Expense' AS Type
         FROM ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityBizDocsDetails OBD ON OBD.numBizDocsPaymentDetId = PO.numBillId
         LEFT OUTER JOIN Chart_Of_Accounts COA ON OBD.numExpenseAccount = COA.numAccountId
         WHERE PO.numProId = v_numProId AND COA.numAccountId IS NOT null
         GROUP BY vcAccountName) as X
      GROUP BY X.vcAccountName,X.Type
      ORDER BY vcAccountName;
      open SWV_RefCur2 for
      SELECT coalesce(vcProjectName,'') AS vcProjectName FROM ProjectsMaster WHERE numProId = v_numProId;
   end if;
   RETURN;
END; $$;
