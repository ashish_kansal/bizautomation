-- Stored procedure definition script USP_DeleteRelProfile for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteRelProfile(v_numRelProID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete  from RelProfile  where numRelProID = v_numRelProID;
   RETURN;
END; $$;


