CREATE OR REPLACE FUNCTION usp_GetBestPartnersForReport(         
v_numDomainID NUMERIC,            
 v_dtDateFrom TIMESTAMP,            
 v_dtDateTo TIMESTAMP,            
 v_numUserCntID NUMERIC DEFAULT 0,                    
 v_intType NUMERIC DEFAULT 0,          
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 1 then          
 --All Records Rights            
 
      open SWV_RefCur for
      SELECT CI.numCompanyId,DM.numDivisionID,DM.tintCRMType,fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName || ',' || DM.vcDivisionName as Partner,
 COUNT(CA.numCompanyID) as Referrals,
 GetTotalDealsWonForBestPartners(CI.numCompanyId) AS DealsWon
      FROM CompanyInfo CI
      JOIN DivisionMaster DM
      ON DM.numCompanyID = CI.numCompanyId
      JOIN CompanyAssociations CA
      ON CA.numDivisionID = DM.numDivisionID
      WHERE CI.numDomainID = v_numDomainID
      AND DM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyId,DM.numDivisionID,DM.tintCRMType;
   end if;            
            
   If v_tintRights = 2 then
 
      open SWV_RefCur for
      SELECT CI.numCompanyId,DM.numDivisionID,DM.tintCRMType,fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName || ',' || DM.vcDivisionName as Partner,
 COUNT(CA.numCompanyID) as Referrals,
 GetTotalDealsWonForBestPartners(CI.numCompanyId) AS DealsWon
      FROM CompanyInfo CI
      JOIN DivisionMaster DM
      ON DM.numCompanyID = CI.numCompanyId
      JOIN CompanyAssociations CA
      ON CA.numDivisionID = DM.numDivisionID
      JOIN AdditionalContactsInformation AI
      ON AI.numDivisionId = DM.numDivisionID
      WHERE CI.numDomainID = v_numDomainID
      AND CI.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      AND AI.numTeam is not null
      AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      AND DM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyId,DM.numDivisionID,DM.tintCRMType;
   end if;            
            
   If v_tintRights = 3 then
 
      open SWV_RefCur for
      SELECT CI.numCompanyId,DM.numDivisionID,DM.tintCRMType,fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName || ',' || DM.vcDivisionName as Partner,
 COUNT(CA.numCompanyID) as Referrals,
 GetTotalDealsWonForBestPartners(CI.numCompanyId) AS DealsWon
      FROM CompanyInfo CI
      JOIN DivisionMaster DM
      ON DM.numCompanyID = CI.numCompanyId
      JOIN CompanyAssociations CA
      ON CA.numDivisionID = DM.numDivisionID
      WHERE CI.numDomainID = v_numDomainID
      AND DM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      AND DM.numTerID in(SELECT F.numTerritory FROM ForReportsByTerritory F
         WHERE F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyId,DM.numDivisionID,DM.tintCRMType;
   end if;
   RETURN;
END; $$;


