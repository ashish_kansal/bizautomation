-- Stored procedure definition script usp_GetSurveyInformation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetSurveyInformation(v_numSurId NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor)
LANGUAGE plpgsql
--This procedure will return all survey master information for the selected survey            
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT numQID, vcQuestion, tIntAnsType, intNumOfAns, 0 as boolDeleted,Case boolMatrixColumn When true Then 1 Else 0 End As boolMatrixColumn ,intNumOfMatrixColumn
   FROM SurveyQuestionMaster
   WHERE numSurID = v_numSurId;            
          
             
   open SWV_RefCur2 for
   SELECT numSurID, numQID, numAnsID, vcAnsLabel, Case boolSurveyRuleAttached When true Then 1 Else 0 End As boolSurveyRuleAttached, 0 as boolDeleted
   FROM SurveyAnsMaster
   WHERE  numSurID = v_numSurId;            
          
   open SWV_RefCur3 for
   SELECT numRuleID, numSurID, numAnsID, numQID, Case boolActivation When true Then 1 Else 0 End As boolActivation, numEventOrder,
  coalesce(vcQuestionList,'') AS vcQuestionList, numLinkedSurID, numResponseRating, coalesce(vcPopUpURL,'') AS vcPopUpURL, numGrpId, numCompanyType,
  tIntCRMType, numRecOwner , coalesce(tIntRuleFourRadio,0) as tIntRuleFourRadio ,coalesce(tIntCreateNewID,0) as tIntCreateNewID,coalesce(vcRuleFourSelectFields,'') as vcRuleFourSelectFields,
boolPopulateSalesOpportunity, coalesce(vcRuleFiveSelectFields,'') as vcRuleFiveSelectFields,numMatrixID,vcItem
   FROM SurveyWorkflowRules
   WHERE  numSurID = v_numSurId;   
  
   open SWV_RefCur4 for
   SELECT numSurID, numQID, numMatrixID, vcAnsLabel, Case boolSurveyRuleAttached When true Then 1 Else 0 End As boolSurveyRuleAttached, 0 as boolDeleted
   FROM SurveyMatrixAnsMaster
   WHERE  numSurID = v_numSurId;   
  
   open SWV_RefCur5 for
   SELECT * FROM SurveyCreateRecord
   WHERE  numSurID = v_numSurId;
   RETURN;
END; $$;


