-- Stored procedure definition script USP_GetParentChildCustomFieldMap for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetParentChildCustomFieldMap(v_numDomainID NUMERIC(18,0),
	v_tintChildModule SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.loc_name AS vcParentModule
		,CM.loc_name AS vcChildModule
		,DFMP.vcFieldName AS vcParentField
		,DFMC.vcFieldName AS vcChildField
   FROM
   ParentChildCustomFieldMap PCFM
   JOIN CFW_Loc_Master PM ON PCFM.tintParentModule = PM.Loc_id
   JOIN CFW_Loc_Master CM ON PCFM.tintChildModule = CM.Loc_id
   INNER JOIN
   DycFieldMaster DFMP
   ON
   PCFM.numParentFieldID = DFMP.numFieldID
   INNER JOIN
   DycFieldMaster DFMC
   ON
   PCFM.numChildFieldID = DFMC.numFieldID
   WHERE
   PCFM.numDomainID = v_numDomainID
   AND coalesce(PCFM.bitCustomField,false) = false
   AND (PCFM.tintChildModule = v_tintChildModule OR v_tintChildModule = 0)
   UNION
   SELECT
   numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.loc_name AS vcParentModule
		,CM.loc_name AS vcChildModule
		,PFM.FLd_label AS vcParentField
		,CFM.FLd_label AS vcChildField
   FROM
   ParentChildCustomFieldMap PCFM
   JOIN CFW_Loc_Master PM ON PCFM.tintParentModule = PM.Loc_id
   JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID = PFM.Fld_id AND PCFM.numDomainID = PFM.numDomainID AND PCFM.tintParentModule = PFM.Grp_id
   JOIN CFW_Loc_Master CM ON PCFM.tintChildModule = CM.Loc_id
   JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID = CFM.Fld_id AND PCFM.numDomainID = CFM.numDomainID AND PCFM.tintChildModule = CFM.Grp_id
   WHERE
   PCFM.numDomainID = v_numDomainID
   AND coalesce(PCFM.bitCustomField,false) = true
   AND (PCFM.tintChildModule = v_tintChildModule OR v_tintChildModule = 0);
END; $$;












