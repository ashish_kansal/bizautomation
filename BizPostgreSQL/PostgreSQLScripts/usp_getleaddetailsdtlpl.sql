-- Stored procedure definition script usp_GetLeadDetailsDTLPL for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetLeadDetailsDTLPL(v_numContactID NUMERIC,                  
 v_numDomainID NUMERIC ,  
v_ClientTimeZoneOffset  INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cmp.vcCompanyName,
  DM.vcDivisionName,
  CONCAT(ADC.vcFirstName,' ',ADC.vcLastname) as Name,
  cmp.vcWebSite,
  ADC.vcEmail,              
    getCompanyAddress(DM.numDivisionID,1::SMALLINT,DM.numDomainID) as address,
AD.vcStreet,AD.numCountry AS vccountry,
  (select vcData from Listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatus,
   (select vcData from Listdetails where numListItemID = DM.numTerID) as numTerID,
  (select vcData from Listdetails where numListItemID = AD.numState) as vcState,
  (select vcData from Listdetails where numListItemID = ADC.vcPosition) as vcPosition,
  ADC.numPhone || ' ' || ADC.numPhoneExtension as phone,
  (select vcData from Listdetails where numListItemID = cmp.numNoOfEmployeesId) as numNoOfEmployeesId,
  (select vcData from Listdetails where numListItemID = cmp.numAnnualRevID) as numAnnualRevID,
  (select vcData from Listdetails where numListItemID = cmp.vcHow) as  vcHow,
  (select vcData from Listdetails where numListItemID = cmp.vcProfile) as  vcProfile,
  ADC.txtNotes,
  DM.numCreatedBy,
  (select vcGrpName from Groups where numGrpId = DM.numGrpId) as numgrpid ,   DM.numGrpId as grpid,
  cmp.txtComments,
 (select vcData from Listdetails where numListItemID = cmp.numCompanyType) as numCompanyType ,cmp.numCompanyType as numCmptype,
  vcTitle,
  vcComPhone || ' ' || vcComFax as vcComPhone,
  coalesce(fn_GetContactName(DM.numCreatedBy),'&nbsp;&nbsp;-') || '' || DM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS vcCreatedBy,                                                          DM.numModifiedBy,
  coalesce(fn_GetContactName(DM.numModifiedBy),'&nbsp;&nbsp;-') || ' ' || DM.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS vcModifiedBy,
  DM.numRecOwner,
  coalesce(fn_GetContactName(DM.numRecOwner),'&nbsp;&nbsp;-') AS vcRecordOwner,
  numAssignedBy,
 (select A.vcFirstName || ' ' || A.vcLastname
      from AdditionalContactsInformation A
      join DivisionMaster D
      on D.numDivisionID = A.numDivisionId
      where A.numContactId = DM.numAssignedTo) as numAssignedTo,
  (select vcData from Listdetails where numListItemID = numCampaignID) as numCampaignID
   FROM
   CompanyInfo cmp join DivisionMaster DM
   on cmp.numCompanyId = DM.numCompanyID
   join  AdditionalContactsInformation ADC
   on  DM.numDivisionID = ADC.numDivisionId
   LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactId
   AND AD.tintAddressOf = 1
   AND AD.tintAddressType = 0
   AND AD.bitIsPrimary = true
   AND AD.numDomainID = ADC.numDomainID
   WHERE
   DM.tintCRMType = 0
   AND ADC.numContactId = v_numContactID and ADC.numDomainID = v_numDomainID;
END; $$;












