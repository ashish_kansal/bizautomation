-- Stored procedure definition script chandan for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
DROP TYPE IF EXISTS chandan_rs CASCADE;
CREATE TYPE chandan_rs AS(numAccountId NUMERIC(9,0), vcAccountName VARCHAR(250), numParntAcntTypeID NUMERIC(9,0), vcAccountDescription VARCHAR(250), vcAccountCode VARCHAR(50), Opening NUMERIC(19,4), Debit NUMERIC(19,4), Credit NUMERIC(19,4), Balance NUMERIC(19,4), AccountCode1 VARCHAR(100), vcAccountName1 TEXT, vcAccountCode2 VARCHAR(50), Type INTEGER);
CREATE OR REPLACE FUNCTION chandan(v_numDomainId NUMERIC(9,0),                                                       
  
v_dtFromDate TIMESTAMP,                                                      
  
v_dtToDate TIMESTAMP,     
  
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine                                                        
  
v_numFRID NUMERIC DEFAULT 0     
  
      
  
--@tintByteMode as tinyint                                                                 
  
)
RETURNS SETOF chandan_rs LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  NUMERIC(19,4);     
  
   v_PLCHARTID  NUMERIC(8,0);     
  
   v_PLOPENING  NUMERIC(19,4);     
  
   v_numFinYear  INTEGER;     
  
   v_dtFinYearFrom  TIMESTAMP;     
  
   v_TotalIncome  NUMERIC(19,4);     
  
   v_TotalExpense  NUMERIC(19,4);     
  
   v_TotalCOGS  NUMERIC(19,4);
   v_numFRDtlID  NUMERIC;     
  
   v_numValue1  NUMERIC;     
  
   v_numValue2  NUMERIC;     
  
   v_vcValue1  VARCHAR(50);     
  
   v_strSql  VARCHAR(8000);     
  
   v_strColumnName  VARCHAR(50);     
  
   v_ColumnName  VARCHAR(50);
   SWV_RowCount INTEGER;
BEGIN
   Drop table IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNALBS ON COMMIT DROP AS
      select * from VIEW_JOURNALBS where numdomainid = v_numDomainId;    
  
      
  
--Select Financial Year and From Date     
  
   select   numFinYearId, dtPeriodFrom INTO v_numFinYear,v_dtFinYearFrom FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
   dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;     
  
      
  
   select   MIN(datEntry_Date) INTO v_dtFromDate FROM tt_VIEW_JOURNALBS WHERE numDomainID = v_numDomainId;     
  
    
  
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);     
  
      
  
   RAISE NOTICE '%',v_numFinYear;     
  
   RAISE NOTICE '%',v_dtFinYearFrom;     
  
   RAISE NOTICE '%',v_dtFromDate;     
  
     
  
   v_CURRENTPL := CAST(0 AS NUMERIC(19,4));      
  
   v_PLOPENING := CAST(0 AS NUMERIC(19,4));     
  
      
  
 /*ISNULL(SUM(Opening),0)+*/     
  
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_CURRENTPL FROM
   VIEW_DAILY_INCOME_EXPENSE P WHERE
   numDomainID = v_numDomainId AND datEntry_Date between v_dtFinYearFrom and v_dtToDate;     
  
      
  
      
  
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalIncome FROM VIEW_JOURNAL VJ WHERE numDomainID = v_numDomainId AND vcAccountCode  ilike '0103%' AND datEntry_Date between v_dtFinYearFrom and v_dtToDate;     
  
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalExpense FROM VIEW_JOURNAL VJ WHERE numDomainID = v_numDomainId AND vcAccountCode  ilike '0104%' AND datEntry_Date between v_dtFinYearFrom and v_dtToDate;     
  
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalCOGS FROM VIEW_JOURNAL VJ WHERE numDomainID = v_numDomainId AND vcAccountCode  ilike '0106%' AND datEntry_Date between v_dtFinYearFrom and v_dtToDate;     
  
   RAISE NOTICE 'TotalIncome=         %',SUBSTR(CAST(v_TotalIncome AS VARCHAR(20)),1,20);     
  
   RAISE NOTICE 'TotalExpense=         %',SUBSTR(CAST(v_TotalExpense AS VARCHAR(20)),1,20);     
  
   RAISE NOTICE 'TotalCOGS=          %',SUBSTR(CAST(v_TotalCOGS AS VARCHAR(20)),1,20);     
  
   v_CURRENTPL :=(v_TotalIncome+v_TotalExpense+v_TotalCOGS);     
  
   RAISE NOTICE 'Current Profit/Loss = (Income - expense - cogs)= %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);     
  
      
  
      
  
      
  
   RAISE NOTICE '%',v_CURRENTPL;     
  
      
  
      
  
   select   numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND bitProfitLoss = true;     
  
   RAISE NOTICE 'PL Account ID=%',SUBSTR(CAST(v_PLCHARTID AS VARCHAR(20)),1,20);     
  
   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM VIEW_JOURNAL VJ WHERE numDomainID = v_numDomainId AND numAccountId = v_PLCHARTID;     
  
   RAISE NOTICE 'PL Account Opening=%',SUBSTR(CAST(v_PLOPENING AS VARCHAR(20)),1,20);     
  
   v_CURRENTPL := v_PLOPENING+v_CURRENTPL;     
  
   RAISE NOTICE 'PL = %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);     
  
      
  
      
  
      
  
      
  
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      bitIsSubAccount BOOLEAN
   );     
  
      
  
   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
CAST(coalesce((SELECT SUM(coalesce(monOpening,0)) from ChartAccountOpening CAO WHERE
      numFinYearId = v_numFinYear and numDomainID = v_numDomainId
      AND CAO.numAccountId IN(SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId = v_numDomainId AND vcAccountCode ilike COA.vcAccountCode || '%')),0)+coalesce((SELECT sum(coalesce(Debit,cast(0 as NUMERIC(19,4))) -coalesce(Credit,cast(0 as NUMERIC(19,4)))) FROM tt_VIEW_JOURNALBS VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datEntry_Date BETWEEN v_dtFinYearFrom AND  v_dtFromDate+INTERVAL '-1 day'),cast(0 as NUMERIC(19,4))) AS NUMERIC(19,4)) AS OPENING,
coalesce((SELECT sum(coalesce(Debit,cast(0 as NUMERIC(19,4)))) FROM tt_VIEW_JOURNALBS VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as DEBIT,
coalesce((SELECT sum(coalesce(Credit,cast(0 as NUMERIC(19,4)))) FROM tt_VIEW_JOURNALBS VJ
      WHERE VJ.numDomainID = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as CREDIT
,coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId  AND COA.bitActive = true  AND
   coalesce(bitProfitLoss,false) = false AND
      (COA.vcAccountCode ilike '0101%' OR
   COA.vcAccountCode ilike '0102%' OR
   COA.vcAccountCode ilike '0105%')
   UNION     
  
      
  
      
  
/*(* -1) added by chintan to fix bug 2148 #3  */     
  
   SELECT v_PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
v_CURRENTPL,CAST(0 AS NUMERIC(19,4)),CAST(0 AS NUMERIC(19,4)),coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId AND COA.bitActive = true and
   bitProfitLoss = true AND COA.numAccountId = v_PLCHARTID;     
  
      
  
      
  
/*Added by chintan bugid 962 #3, For presentation do follwoing Show Credit amount in Posite and Debit Amount in Negative (only for accounts under quaity except PL account)*/     
  
   UPDATE tt_PLSUMMARY SET
   Opening = Opening*(-1),--CASE WHEN Opening <0 THEN Opening * (-1) ELSE Opening END ,     
  
Debit = Debit*(-1),--CASE WHEN Debit >0 THEN Debit * (-1) ELSE Debit END,     
  
Credit = Credit*(-1)--CASE WHEN Credit <0 THEN Credit * (-1) ELSE Credit END      
  
   WHERE vcAccountCode ilike '0105%' AND numAccountId <> v_PLCHARTID;     
  
      
  
      
  
      
  
   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );     
  
--insert account types and balances     
  
   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
 coalesce(SUM(coalesce(Opening,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Opening,
coalesce(Sum(coalesce(Debit,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Debit,coalesce(Sum(coalesce(Credit,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0101%' OR
   ATD.vcAccountCode ilike '0102%' OR
   ATD.vcAccountCode ilike '0105%')
   WHERE PL.bitIsSubAccount = false
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;     
  
      
  
   ALTER TABLE tt_PLSUMMARY
   DROP COLUMN bitIsSubAccount;     
  
      
  
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      Balance NUMERIC(19,4),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );     
  
      
  
      
  
      
  
      
  
--Add new columns to Result table     
  
   IF v_numFRID > 0 then

      select   numFRDtlID, numValue1, numValue2, vcValue1 INTO v_numFRDtlID,v_numValue1,v_numValue2,v_vcValue1 FROM FinancialReportDetail WHERE numFRID = v_numFRID   ORDER BY numFRDtlID LIMIT 1;
      WHILE v_numFRDtlID > 0 LOOP
         v_ColumnName := coalesce(fn_GetListItemName(coalesce(v_numValue1,0)),'');
         IF LENGTH(v_ColumnName) > 0 then
  
            v_strColumnName :=  v_ColumnName;
            v_strSql := 'ALTER TABLE #PLSummary ADD Custom_Debit_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLSummary ADD Custom_Credit_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLSummary ADD Custom_Opening_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLOutPut ADD Custom_Debit_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLOutPut ADD Custom_Credit_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLOutPut ADD Custom_Opening_' || coalesce(v_strColumnName,'') || ' Money NULL;';
            v_strSql := coalesce(v_strSql,'') || 'ALTER TABLE #PLShow ADD Custom_Balance_' || coalesce(v_strColumnName,'') || ' Money NULL;';     
  
--   SET @strSql =@strSql + 'ALTER TABLE #PLOutPut ADD Custom_Balance_'+ @strColumnName + ' Money NULL;'     
  
         
  
         
  
         
  
            RAISE NOTICE '%',v_strSql;
            EXECUTE v_strSql;
            v_strSql := ' UPDATE #PLSummary      
  
    SET Custom_Debit_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
      select sum(ISNULL(X.Debit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLSummary.numAccountID = VJ.numAccountID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFromDate as VARCHAR(20)),1,20) || ''' AND ''' || SUBSTR(cast(v_dtToDate as VARCHAR(20)),1,20) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X ),0) ,     
  
             
  
    Custom_Credit_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
          
  
      select sum(ISNULL(X.Credit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLSummary.numAccountID = VJ.numAccountID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFromDate as VARCHAR(20)),1,20) || ''' AND ''' || SUBSTR(cast(v_dtToDate as VARCHAR(20)),1,20) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X) ,0) ,     
  
             
  
    Custom_Opening_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
      select sum(ISNULL(Credit,0)) - sum(ISNULL(Debit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLSummary.numAccountID = VJ.numAccountID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFinYearFrom as VARCHAR(20)),1,20) || ''' AND ''' || cast(v_dtFromDate+INTERVAL '-1 day' as VARCHAR(20)) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X),0);     
  
             
  
             
  
    Update #PLOutPut SET      
  
     Custom_Debit_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
      select sum(ISNULL(X.Debit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLOutPut.numAccountID = VJ.numAccountTypeID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFromDate as VARCHAR(20)),1,20) || ''' AND ''' || SUBSTR(cast(v_dtToDate as VARCHAR(20)),1,20) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X ),0) ,     
  
             
  
     Custom_Credit_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
      select sum(ISNULL(X.Credit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLOutPut.numAccountID = VJ.numAccountTypeID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFromDate as VARCHAR(20)),1,20) || ''' AND ''' || SUBSTR(cast(v_dtToDate as VARCHAR(20)),1,20) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X ),0) ,     
  
             
  
     Custom_Opening_' || coalesce(v_strColumnName,'') || ' =isnull((      
  
      select sum(ISNULL(Credit,0)) - sum(ISNULL(Debit,0)) from (SELECT Distinct VJ.*     
  
      FROM VIEW_JOURNALBS VJ     
  
      inner join Item I on I.numItemCode = VJ.numItemID     
  
      WHERE VJ.numItemID>0 and VJ.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' AND     
  
        #PLOutPut.numAccountID = VJ.numAccountTypeID AND     
  
       datEntry_Date BETWEEN  ''' || SUBSTR(cast(v_dtFinYearFrom as VARCHAR(20)),1,20) || ''' AND ''' || cast(v_dtFromDate+INTERVAL '-1 day' as VARCHAR(20)) || ''' AND     
  
       I.numItemClassification = ' || CAST(coalesce(v_numValue1,0) AS VARCHAR(10))  || '      
  
       )X),0);     
  
          
  
       ';
            RAISE NOTICE '%',v_strSql;
            EXECUTE v_strSql;
         end if;
         select   numFRDtlID, numValue1, numValue2, vcValue1 INTO v_numFRDtlID,v_numValue1,v_numValue2,v_vcValue1 FROM FinancialReportDetail WHERE numFRID = v_numFRID AND numFRDtlID > v_numFRDtlID   ORDER BY numFRDtlID LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numFRDtlID := 0;
         end if;
      END LOOP;
   end if;     
  
      
  
      
  
      
  
   INSERT INTO tt_PLSHOW(numAccountId,
     vcAccountName,
     numParntAcntTypeID,
     vcAccountDescription,
     vcAccountCode,
     Opening,
     Debit,
     Credit,
     Balance,
     AccountCode1,
     vcAccountName1,
     Type)
   SELECT numAccountId,
    vcAccountName,
    numParntAcntTypeID,
    vcAccountDescription,
    vcAccountCode,
    Opening,
    Debit,
    Credit,(Opening*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end)+(Debit*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end)
   -(Credit*case SUBSTR(P.vcAccountCode,1,4) when '0102' then(-1) else 1 end) as Balance,
CAST(CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE  P.vcAccountCode
   END AS VARCHAR(100)) AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   UNION
   SELECT numAccountId,
    vcAccountName,
    numParntAcntTypeID,
    vcAccountDescription,
    vcAccountCode,
    Opening,
    Debit,
    Credit,(Opening*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end)+(Debit*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end) -(Credit*case SUBSTR(O.vcAccountCode,1,4) when '0102' then(-1) else 1 end) as Balance,
CAST(CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE  O.vcAccountCode
   END AS VARCHAR(100)) AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, 2 as Type
   FROM tt_PLOUTPUT O;     
  
      
  
   UPDATE tt_PLSHOW SET TYPE = 3 WHERE numAccountId = v_PLCHARTID;     
  
      
  
--SELECT * FROM #PLOutPut     
  
      
  
      
  
--- Update Balance for New Columns added dyamically     
  
   IF v_numFRID > 0 then

      select   numFRDtlID, numValue1, numValue2, vcValue1 INTO v_numFRDtlID,v_numValue1,v_numValue2,v_vcValue1 FROM FinancialReportDetail WHERE numFRID = v_numFRID   ORDER BY numFRDtlID LIMIT 1;
      WHILE v_numFRDtlID > 0 LOOP
         v_ColumnName := coalesce(fn_GetListItemName(coalesce(v_numValue1,0)),'');
         IF LENGTH(v_ColumnName) > 0 then
  
            v_strColumnName := 'Custom_Balance_' || coalesce(v_ColumnName,'');
            v_strSql := ' UPDATE #PLShow SET ' ||  coalesce(v_strColumnName,'') || ' = ISNULL(X.Balance,0)     
  
    FROM (SELECT numAccountID,( Custom_Opening_' || coalesce(v_ColumnName,'') || ' * case substring(P.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end)     
  
    +(Custom_Debit_' || coalesce(v_ColumnName,'') || ' * case substring(P.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end )     
  
    -(Custom_Credit_' || coalesce(v_ColumnName,'') || ' * case substring(P.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end) as Balance     
  
     FROM #PLSummary P ) X     
  
    WHERE X.numAccountID = #PLShow.numAccountId; ';
            v_strSql := coalesce(v_strSql,'') || ' UPDATE #PLShow SET ' ||  coalesce(v_strColumnName,'') || ' = ISNULL(X.Balance,0)     
  
    FROM (SELECT numAccountID,(Custom_Opening_' || coalesce(v_ColumnName,'') || ' * case substring(O.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end)+     
  
    (Custom_Debit_' || coalesce(v_ColumnName,'') || ' * case substring(O.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end)-      
  
    (Custom_Credit_' || coalesce(v_ColumnName,'') || ' * case substring(O.[vcAccountCode],1,4) when ''0102'' then (-1) else 1 end) as Balance     
  
    FROM #PLOutPut O ) X     
  
    WHERE X.numAccountID = #PLShow.numParntAcntTypeID and Type=2;      
  
    update #PLShow set ' ||  coalesce(v_strColumnName,'') || ' =isnull( ' ||  coalesce(v_strColumnName,'') || ' ,0)     
  
    ';
            RAISE NOTICE '%',v_strSql;
            EXECUTE v_strSql;
            select   numFRDtlID, numValue1, numValue2, vcValue1 INTO v_numFRDtlID,v_numValue1,v_numValue2,v_vcValue1 FROM FinancialReportDetail WHERE numFRID = v_numFRID AND numFRDtlID > v_numFRDtlID   ORDER BY numFRDtlID LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numFRDtlID := 0;
            end if;
         end if;
      END LOOP;
   end if;     
  
      
  
      
  
      
  
   return query select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LENGTH(A.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(A.vcAccountCode) -4) || A.vcAccountName
   ELSE A.vcAccountName
   END AS vcAccountName1, A.vcAccountCode ,
        A.TYPE from tt_PLSHOW A ORDER BY A.vcAccountCode;     
  
      
  
   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;     
  
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;     
  
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;     
  
   Drop table IF EXISTS tt_VIEW_JOURNALBS CASCADE;
   RETURN;
END; $$;     
  

