-- Stored procedure definition script USP_ManageOpportunityLinking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageOpportunityLinking(v_numParentOppID NUMERIC(9,0) DEFAULT 0,
    v_numChildOppID NUMERIC(9,0) DEFAULT 0,
    v_vcSource VARCHAR(50) DEFAULT NULL,
    v_numParentOppBizDocID NUMERIC(9,0) DEFAULT NULL,
    v_numParentProjectID NUMERIC(9,0) DEFAULT NULL,
    v_numWebApiId NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numParentOppID = 0 then
      v_numParentOppID := NULL;
   end if;
   IF v_numParentOppBizDocID = 0 then
      v_numParentOppBizDocID := NULL;
   end if;
   IF v_numParentProjectID = 0 then
      v_numParentProjectID := NULL;
   end if;
    
   INSERT  INTO OpportunityLinking(numParentOppID,
                  numChildOppID,
                  vcSource,
                  numParentOppBizDocID,
                  numParentProjectID,
                  numWebApiId)
        VALUES(v_numParentOppID,
                  v_numChildOppID,
                  v_vcSource,
                  v_numParentOppBizDocID,
                  v_numParentProjectID,
                  v_numWebApiId);
RETURN;
END; $$;


