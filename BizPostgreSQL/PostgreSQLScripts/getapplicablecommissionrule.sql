-- Function definition script GetApplicableCommissionRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetApplicableCommissionRule(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0), 
	v_numDivisionID NUMERIC(18,0), 
	v_tintAssignedTo SMALLINT, --2:RECORD OWNER, 1:RECORD ASSIGNED TO
	v_numContactID NUMERIC(18,0))
RETURNS NUMERIC(18,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numComRuleID  NUMERIC(18,0) DEFAULT 0;

   v_numItemClassification  NUMERIC(18,0);
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);

	--CHECK FOR COMMISSION CONTACT	
   v_bitCommContact  BOOLEAN DEFAULT 0;
   v_numComDivisionID  NUMERIC(18,0) DEFAULT 0;
BEGIN
   IF EXISTS(SELECT  UM.numUserId
   FROM    UserMaster UM
   JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
   WHERE   UM.numDomainID = v_numDomainID
   AND UM.numDomainID = A.numDomainID
   AND A.numContactId = v_numContactID) then
    
      v_bitCommContact := false;
   ELSE
      v_bitCommContact := true;
      select   A.numDivisionId INTO v_numComDivisionID FROM
      AdditionalContactsInformation A WHERE
      A.numContactId = v_numContactID;
   end if;


	--GET ORGANIZATION RELATIOnSHIP AND PROFILE VALUE
   select   coalesce(numCompanyType,0), coalesce(CompanyInfo.vcProfile,0) INTO v_numRelationship,v_numProfile FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = v_numDivisionID;

	--GET ITEM CLASSIFICATION
   select   coalesce(numItemClassification,0) INTO v_numItemClassification FROM Item WHERE numItemCode = v_numItemCode;

	--SELECT TOP 1 COMMISSION RULE APPLICABLE BY PRIORITY
   select   CR.numComRuleID INTO v_numComRuleID FROM
   CommissionRules CR
   LEFT JOIN
   CommissionRuleItems CRIIndividual
   ON
   CR.numComRuleID = CRIIndividual.numComRuleID
   AND CRIIndividual.tintType = 1
   LEFT JOIN
   CommissionRuleItems CRIClassification
   ON
   CR.numComRuleID = CRIClassification.numComRuleID
   AND CRIClassification.tintType = 2
   LEFT JOIN
   CommissionRuleOrganization CROIndividual
   ON
   CR.numComRuleID = CROIndividual.numComRuleID
   AND CROIndividual.tintType = 3
   LEFT JOIN
   CommissionRuleOrganization CRORelProfile
   ON
   CR.numComRuleID = CRORelProfile.numComRuleID
   AND CRORelProfile.tintType = 4
   LEFT JOIN
   CommissionRuleContacts CRC
   ON
   CR.numComRuleID = CRC.numComRuleId
   LEFT JOIN
   PriceBookPriorities PBP
   ON
   CR.tintComAppliesTo = PBP.Step2Value
   AND CR.tintComOrgType = PBP.Step3Value WHERE
   CR.numDomainID = v_numDomainID
   AND CR.tintAssignTo = v_tintAssignedTo
   AND PBP.Priority > 0
   AND (coalesce(CR.tintComAppliesTo,0) = 3 OR (CRIIndividual.numValue = v_numItemCode OR CRIClassification.numValue = v_numItemClassification))
   AND (coalesce(CR.tintComOrgType,0) = 3 OR (CROIndividual.numValue = v_numDivisionID OR (CRORelProfile.numValue = v_numRelationship AND CRORelProfile.numProfile = v_numProfile)))
   AND CRC.numValue =(CASE coalesce(v_bitCommContact,false) WHEN true THEN v_numComDivisionID ELSE  v_numContactID END)   ORDER BY
   PBP.Priority LIMIT 1;

   RETURN coalesce(v_numComRuleID,0);
END; $$;

