-- Stored procedure definition script USP_DeleteEmailGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteEmailGroup(v_numEmailGroup NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from ProfileEGroupDTL where numEmailGroupID = v_numEmailGroup;
   delete FROM ProfileEmailGroup where numEmailGroupID = v_numEmailGroup;
   RETURN;
END; $$;


