DROP FUNCTION IF EXISTS USP_GetWFRecordData;

Create or replace FUNCTION USP_GetWFRecordData(v_numDomainID NUMERIC(18,0),
    v_numRecordID NUMERIC(18,0),
    v_numFormID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numContactId  NUMERIC(18,0);
   v_numRecOwner  NUMERIC(18,0);
   v_numAssignedTo  NUMERIC(18,0);
   v_numDivisionId  NUMERIC(18,0);
   v_txtSignature  VARCHAR(8000);
   v_vcDateFormat  VARCHAR(30);
   v_numInternalPM  NUMERIC(18,0);
   v_numExternalPM  NUMERIC(18,0);
   v_numNextAssignedTo  NUMERIC(18,0);
   v_numPhone  VARCHAR(15);
   v_numOppIDPC  NUMERIC(18,0);
   v_numStageIDPC  NUMERIC(18,0);
   v_numOppBizDOCIDPC  NUMERIC(18,0);
   v_numProjectID  NUMERIC(18,0);
   v_numCaseId  NUMERIC(18,0);
   v_numCommId  NUMERIC(18,0);
   v_vcEmailID  VARCHAR(100);
   v_ContactName  VARCHAR(150);
   v_bitSMTPServer  BOOLEAN;
   v_vcSMTPServer  VARCHAR(100);
   v_numSMTPPort  NUMERIC;
   v_bitSMTPAuth  BOOLEAN;
   v_vcSmtpPassword  VARCHAR(100);
   v_bitSMTPSSL  BOOLEAN;
   v_numOppId  NUMERIC(18,0);
	
   v_numRuleID  NUMERIC;
BEGIN
   IF v_numFormID = 70 then --Opportunities & Orders
	
      select   coalesce(numContactId,0), coalesce(numrecowner,0), coalesce(numassignedto,0), coalesce(numDivisionId,0) INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId FROM
      OpportunityMaster WHERE
      numDomainId = v_numDomainID AND numOppId = v_numRecordID;
      select   coalesce(s.numStageDetailsId,0) INTO v_numStageIDPC FROM
      OpportunityMaster o
      LEFT JOIN
      StagePercentageDetails s
      on
      s.numOppid = o.numOppId WHERE
      o.numDomainId = v_numDomainID
      AND o.numOppId = v_numRecordID;
      select   coalesce(s.numOppBizDocsId,0), coalesce(o.numOppId,0) INTO v_numOppBizDOCIDPC,v_numOppIDPC FROM
      OpportunityMaster o
      LEFT JOIN
      OpportunityBizDocs s
      ON
      s.numoppid = o.numOppId WHERE
      o.numDomainId = v_numDomainID
      AND o.numOppId = v_numRecordID;
   end if;

   IF v_numFormID = 49 then --BizDocs
	
      select   coalesce(obd.numOppBizDocsId,0), coalesce(om.numContactId,0), coalesce(om.numrecowner,0), coalesce(om.numOppId,0), coalesce(om.numassignedto,0), coalesce(om.numDivisionId,0), D.vcDateFormat INTO v_numOppBizDOCIDPC,v_numContactId,v_numRecOwner,v_numOppIDPC,v_numAssignedTo,
      v_numDivisionId,v_vcDateFormat FROM
      OpportunityBizDocs AS obd
      INNER JOIN
      OpportunityMaster AS om
      ON
      obd.numoppid = om.numOppId
      INNER JOIN
      Domain AS D
      ON
      D.numDomainId = om.numDomainId WHERE
      om.numDomainId = v_numDomainID
      AND obd.numOppBizDocsId = v_numRecordID;
   end if;

   IF v_numFormID = 94 then --Sales/Purchase/Project
      select   coalesce(numProjectid,0), coalesce(numOppid,0) INTO v_numProjectID,v_numOppId FROM
      StagePercentageDetails WHERE
      numStageDetailsId = v_numRecordID;

		--@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
      IF v_numProjectID = 0 then--Order
		
         select   coalesce(om.numContactId,0), coalesce(om.numrecowner,0), coalesce(obd.numAssignTo,0), coalesce(om.numDivisionId,0), D.vcDateFormat INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,v_vcDateFormat FROM
         StagePercentageDetails AS obd
         INNER JOIN
         OpportunityMaster AS om
         on
         obd.numOppid = om.numOppId
         INNER JOIN
         Domain AS D
         ON
         D.numDomainId = om.numDomainId WHERE
         om.numDomainId = v_numDomainID
         AND obd.numStageDetailsId = v_numRecordID;
      ELSE
         select   numAssignTo INTO v_numNextAssignedTo FROM
         StagePercentageDetails WHERE
         numStageDetailsId = v_numRecordID+1;
         select   coalesce(om.numIntPrjMgr,0), coalesce(om.numRecOwner,0), coalesce(obd.numAssignTo,0), coalesce(om.numDivisionId,0), coalesce(om.numIntPrjMgr,0), coalesce(om.numCustPrjMgr,0) INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,v_numInternalPM,
         v_numExternalPM FROM
         StagePercentageDetails AS  obd
         INNER JOIN
         ProjectsMaster AS om
         on
         obd.numProjectid = om.numProId WHERE
         om.numdomainId = v_numDomainID
         AND obd.numStageDetailsId = v_numRecordID;
      end if;
   end if;
	
   IF (v_numFormID = 68 OR v_numFormID = 138) then --Organization , AR Aging
	
      select   coalesce(adc.numContactId,0), coalesce(obd.numRecOwner,0), coalesce(obd.numAssignedTo,0), coalesce(obd.numDivisionID,0), d.vcDateFormat INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,v_vcDateFormat FROM
      DivisionMaster AS  obd
      INNER join
      AdditionalContactsInformation as adc
      ON
      adc.numDivisionId = obd.numDivisionID
      INNER JOIN
      Domain AS d
      ON
      d.numDomainId = obd.numDomainID WHERE
      obd.numDomainID = v_numDomainID
      AND obd.numDivisionID = v_numRecordID;
   end if;

   IF v_numFormID = 69 then --Contacts
	
      select   coalesce(adc.numContactId,0), coalesce(obd.numRecOwner,0), coalesce(obd.numAssignedTo,0), coalesce(obd.numDivisionID,0), d.vcDateFormat INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,v_vcDateFormat FROM
      DivisionMaster AS  obd
      INNER JOIN
      AdditionalContactsInformation as adc
      ON
      adc.numDivisionId = obd.numDivisionID
      INNER JOIN
      Domain AS d
      ON
      d.numDomainId = obd.numDomainID WHERE
      obd.numDomainID = v_numDomainID
      AND adc.numContactId = v_numRecordID;
   end if;

   IF v_numFormID = 73 then --Projects
	
      select   coalesce(OM.numProId,0), coalesce(OM.numIntPrjMgr,0), coalesce(OM.numRecOwner,0), coalesce(OM.numAssignedTo,0), coalesce(OM.numDivisionId,0), coalesce(OM.numIntPrjMgr,0), coalesce(OM.numCustPrjMgr,0) INTO v_numProjectID,v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,
      v_numInternalPM,v_numExternalPM FROM
      ProjectsMaster AS OM WHERE
      OM.numdomainId = v_numDomainID
      AND OM.numProId = v_numRecordID;
      select   coalesce(s.numStageDetailsId,0) INTO v_numStageIDPC FROM
      ProjectsMaster o
      left join
      StagePercentageDetails s
      on
      s.numProjectid = o.numProId WHERE
      o.numdomainId = v_numDomainID
      AND o.numProId = v_numRecordID;
   end if;

   IF v_numFormID = 72 then --Cases
	
      select   coalesce(OM.numCaseId,0), coalesce(OM.numContactId,0), coalesce(OM.numRecOwner,0), coalesce(OM.numAssignedTo,0), coalesce(OM.numDivisionID,0), d.vcDateFormat INTO v_numCaseId,v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,
      v_vcDateFormat FROM
      Cases AS OM
      INNER JOIN
      Domain AS d
      ON
      d.numDomainId = OM.numDomainID WHERE
      OM.numDomainID = v_numDomainID
      AND OM.numCaseId = v_numRecordID;
      select   coalesce(o.numOppId,0) INTO v_numOppIDPC FROM
      Cases om
      left JOIN
      CaseOpportunities co
      on
      om.numCaseId = co.numCaseId
      left join
      OpportunityMaster o
      on
      o.numOppId = co.numoppid WHERE
      om.numDomainID = v_numDomainID
      AND om.numCaseId = v_numRecordID;
   end if;

   IF v_numFormID = 124 then --Action items(Ticklers)
	
      select   coalesce(OM.numCommId,0), coalesce(OM.numContactId,0), coalesce(OM.numAssignedBy,0), coalesce(OM.numAssign,0), coalesce(OM.numDivisionID,0), d.vcDateFormat INTO v_numCommId,v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,
      v_vcDateFormat FROM
      Communication AS OM
      INNER JOIN
      Domain AS d
      ON
      d.numDomainId = OM.numDomainID WHERE
      OM.numDomainID = v_numDomainID
      AND OM.numCommId = v_numRecordID;
   end if;

   IF v_numRecOwner > 0 then
	
      select   vcEmailID, coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), coalesce(bitSMTPServer,false), case when bitSMTPServer = true then vcSMTPServer else '' end, case when bitSMTPServer = true then  coalesce(numSMTPPort,0) else 0 end, coalesce(bitSMTPAuth,false), coalesce(vcSmtpPassword,''), coalesce(bitSMTPSSL,false), u.txtSignature, numPhone INTO v_vcEmailID,v_ContactName,v_bitSMTPServer,v_vcSMTPServer,v_numSMTPPort,
      v_bitSMTPAuth,v_vcSmtpPassword,v_bitSMTPSSL,v_txtSignature,v_numPhone FROM
      UserMaster u
      JOIN
      AdditionalContactsInformation A
      ON
      A.numContactId = u.numUserDetailId WHERE
      u.numDomainID = v_numDomainID
      AND numUserDetailId = v_numRecOwner;
   end if;

   IF v_numAssignedTo = 0 then --As per Auto Routing Rule
      select   numRoutID INTO v_numRuleID FROM RoutingLeads WHERE numDomainID = v_numDomainID and bitDefault = true;
      select   rd.numEmpId INTO v_numAssignedTo FROM
      RoutingLeads r
      INNER JOIN
      RoutingLeadDetails rd
      ON
      r.numRoutID = rd.numRoutID WHERE
      r.numRoutID = v_numRuleID;
   end if;

    IF (v_numFormID = 148) then --Website Page
	
      select   coalesce(adc.numContactId,0), coalesce(DivisionMaster.numRecOwner,0), coalesce(DivisionMaster.numAssignedTo,0), coalesce(DivisionMaster.numDivisionID,0), d.vcDateFormat INTO v_numContactId,v_numRecOwner,v_numAssignedTo,v_numDivisionId,v_vcDateFormat 
	  FROM
      TrackingVisitorsDTL 
		INNER JOIN 
			TrackingVisitorsHDR 
		ON 
			TrackingVisitorsDTL.numTracVisitorsHDRID=TrackingVisitorsHDR.numTrackingID 
		INNER JOIN
			DivisionMaster
		ON
			TrackingVisitorsHDR.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER join
      AdditionalContactsInformation as adc
      ON
      adc.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      Domain AS d
      ON
      d.numDomainId = DivisionMaster.numDomainID WHERE TrackingVisitorsDTL.numTracVisitorsDTLID = v_numRecordID;
   end if;


   select   coalesce(vcPSMTPUserName,v_vcEmailID) INTO v_vcEmailID FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   open SWV_RefCur for SELECT
   v_numContactId AS numContactId
		,v_numRecOwner AS numRecOwner
		,v_numAssignedTo AS numAssignedTo
		,v_numDivisionId AS numDivisionId
		,v_vcEmailID AS vcEmailID
		,coalesce((SELECT vcPSMTPDisplayName FROM Domain WHERE numDomainId = v_numDomainID),v_ContactName) AS ContactName
		,v_bitSMTPServer AS bitSMTPServer
		,v_vcSMTPServer AS vcSMTPServer
		,v_numSMTPPort AS numSMTPPort
		,v_bitSMTPAuth AS bitSMTPAuth
		,v_vcSmtpPassword AS vcSmtpPassword
		,v_bitSMTPSSL AS bitSMTPSSL
		,v_txtSignature AS Signature
		,v_numInternalPM as numInternalPM
		,v_numExternalPM as numExternalPM
		,v_numNextAssignedTo as numNextAssignedTo
		,v_numPhone as numPhone
		,v_numOppIDPC as numOppIDPC
		,v_numStageIDPC as numStageIDPC
		,v_numOppBizDOCIDPC as numOppBizDOCIDPC
		,v_numProjectID AS numProjectID
		,v_numCaseId AS numCaseId
		,v_numCommId AS numCommId;
END; $$;












