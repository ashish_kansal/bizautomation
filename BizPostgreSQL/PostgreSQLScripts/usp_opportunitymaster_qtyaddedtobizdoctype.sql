-- Stored procedure definition script USP_OpportunityMaster_QtyAddedToBizDocType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_QtyAddedToBizDocType(v_numDomainID NUMERIC(18,0),
 v_numOppId NUMERIC(18,0),
 v_numBizDocId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitAllQtyAddedToBizDocType  BOOLEAN DEFAULT true;
BEGIN
   IF(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
				coalesce(OI.numUnitHour,0) AS OrderedQty,
				coalesce(TempInvoice.QtyAddedToBizDocType,0) AS QtyAddedToBizDocType
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS QtyAddedToBizDocType
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppId
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND coalesce(OpportunityBizDocs.numBizDocId,0) = v_numBizDocId) AS TempInvoice on TRUE
      WHERE
      OI.numOppId = v_numOppId) X
   WHERE
   X.OrderedQty <> X.QtyAddedToBizDocType) > 0 then
	
      v_bitAllQtyAddedToBizDocType := false;
   end if;

   open SWV_RefCur for SELECT v_bitAllQtyAddedToBizDocType;
END; $$;











