-- Stored procedure definition script USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs(v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numoppid = v_numOppID AND numBizDocId = 296 AND coalesce(bitFulFilled,false) = false;
END; $$;















