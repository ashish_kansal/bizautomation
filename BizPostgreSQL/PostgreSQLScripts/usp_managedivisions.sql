-- Stored procedure definition script USP_ManageDivisions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDivisions(v_numDivisionID  NUMERIC DEFAULT 0,                                                         
 v_numCompanyID  NUMERIC DEFAULT 0,                                                         
 v_vcDivisionName  VARCHAR(100) DEFAULT '',                                                        
 v_vcBillStreet  VARCHAR(50) DEFAULT '',                                                        
 v_vcBillCity   VARCHAR(50) DEFAULT '',                                                        
 v_vcBillState  NUMERIC DEFAULT 0,                                                        
 v_vcBillPostCode  VARCHAR(15) DEFAULT '',                                                        
 v_vcBillCountry  NUMERIC DEFAULT 0,                                                        
 v_bitSameAddr  BOOLEAN DEFAULT false,                                                        
 v_vcShipStreet  VARCHAR(50) DEFAULT '',                                                        
 v_vcShipCity   VARCHAR(50) DEFAULT '',                                                        
 v_vcShipState  NUMERIC DEFAULT 0,                                                        
 v_vcShipPostCode  VARCHAR(15) DEFAULT '',                                                        
 v_vcShipCountry  NUMERIC DEFAULT 0,                                                        
 v_numGrpId   NUMERIC DEFAULT 0,                                                                                          
 v_numTerID   NUMERIC DEFAULT 0,                                                        
 v_bitPublicFlag  BOOLEAN DEFAULT false,                                                        
 v_tintCRMType  Integer DEFAULT 0,                                                        
 v_numUserCntID  NUMERIC DEFAULT 0,                                                                                                                                                              
 v_numDomainID  NUMERIC DEFAULT 0,                                                        
 v_bitLeadBoxFlg  BOOLEAN DEFAULT false,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 v_numStatusID  NUMERIC DEFAULT 0,                                                      
 v_numCampaignID NUMERIC DEFAULT 0,                                          
 v_numFollowUpStatus NUMERIC(9,0) DEFAULT 0,                                                                                  
 v_tintBillingTerms SMALLINT DEFAULT NULL,                                                        
 v_numBillingDays NUMERIC(18,0) DEFAULT NULL,                                                       
 v_tintInterestType SMALLINT DEFAULT NULL,                                                        
 v_fltInterest NUMERIC(20,5) DEFAULT NULL,                                        
 v_vcComFax VARCHAR(50) DEFAULT 0,                          
 v_vcComPhone VARCHAR(50) DEFAULT 0,                
 v_numAssignedTo NUMERIC(9,0) DEFAULT 0,    
 v_bitNoTax BOOLEAN DEFAULT NULL,
 v_UpdateDefaultTax BOOLEAN DEFAULT true,
v_numCompanyDiff NUMERIC(9,0) DEFAULT 0,
v_vcCompanyDiff VARCHAR(100) DEFAULT '',
v_numCurrencyID	NUMERIC(9,0) DEFAULT NULL,
v_numAccountClass NUMERIC(18,0) DEFAULT 0,
v_numBillAddressID NUMERIC(18,0) DEFAULT 0,
v_numShipAddressID NUMERIC(18,0) DEFAULT 0,
v_vcPartenerSource VARCHAR(300) DEFAULT 0,              
v_vcPartenerContact VARCHAR(300) DEFAULT 0,
v_numPartner NUMERIC(18,0) DEFAULT 0,
v_vcBuiltWithJson TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPartenerSource  NUMERIC(18,0);      
   v_numPartenerContact  NUMERIC(18,0);
   v_tempAssignedTo  NUMERIC(9,0);
   v_numGroupID  NUMERIC;

    ---Updating if organization is assigned to someone                
   v_numFollow  VARCHAR(10);                                        
   v_binAdded  VARCHAR(20);                                        
   v_PreFollow  VARCHAR(10);                                        
   v_RowCount  VARCHAR(2);
   SWV_RowCount INTEGER;
BEGIN
   IF coalesce(v_numPartner,0) = 0 then
	
      SELECT  numDivisionID INTO v_numPartenerSource FROM DivisionMaster WHERE numDomainID = v_numDomainID AND vcPartnerCode = v_vcPartenerSource     LIMIT 1;
   ELSE
      v_numPartenerSource := v_numPartner;
   end if;

   SELECT  numContactId INTO v_numPartenerContact FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = v_numPartenerSource AND vcEmail = v_vcPartenerContact     LIMIT 1;
                                 
   IF coalesce(v_numGrpId,0) = 0 AND coalesce(v_tintCRMType,0) = 0 then
	
      v_numGrpId := 5;
   end if;
                                                                                                               
   IF v_numDivisionID is null OR v_numDivisionID = 0 then
      IF v_UpdateDefaultTax = true then
         v_bitNoTax := false;
      end if;
      INSERT INTO DivisionMaster(numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,bitPublicFlag,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,
			bitLeadBoxFlg,numTerID,numStatusID,numRecOwner,tintBillingTerms,numBillingDays,tintInterestType,fltInterest,numCampaignID,vcComPhone,vcComFax,bitNoTax,
			numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact,vcBuiltWithJson)
		VALUES(v_numCompanyID,v_vcDivisionName,v_numGrpId,v_numFollowUpStatus,v_bitPublicFlag,v_numUserCntID,TIMEZONE('UTC',now()),v_numUserCntID,TIMEZONE('UTC',now()),v_tintCRMType,v_numDomainID,
			v_bitLeadBoxFlg,v_numTerID,v_numStatusID,v_numUserCntID,(CASE WHEN coalesce(v_numBillingDays, 0) > 0 THEN 1 ELSE 0 END),v_numBillingDays,0,0,v_numCampaignID,v_vcComPhone,
			v_vcComFax,v_bitNoTax,v_numCompanyDiff,v_vcCompanyDiff,true,v_numCurrencyID,v_numAccountClass,v_numPartenerSource,v_numPartenerContact,v_vcBuiltWithJson);
		
      v_numDivisionID := CURRVAL('DivisionMaster_seq');
      IF v_numBillAddressID > 0 OR v_numShipAddressID > 0 then
		
         IF v_numBillAddressID > 0 then
			
            UPDATE AddressDetails SET numRecordID = v_numDivisionID WHERE numAddressID = v_numBillAddressID;
         end if;
         IF v_numShipAddressID > 0 then
			
            UPDATE AddressDetails SET numRecordID = v_numDivisionID WHERE numAddressID = v_numShipAddressID;
         end if;
      ELSE
		  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
         IF EXISTS(SELECT 'col1' FROM AddressDetails WHERE  bitIsPrimary = true AND numDomainID = v_numDomainID AND numRecordID = v_numDivisionID) then
			
            INSERT INTO AddressDetails(vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID)
            SELECT 'Address1',v_vcBillStreet,v_vcBillCity,v_vcBillPostCode,v_vcBillState,v_vcBillCountry,false,2,1,v_numDivisionID,v_numDomainID
            union
            SELECT 'Address1',v_vcShipStreet,v_vcShipCity,v_vcShipPostCode,CAST(v_vcShipState AS NUMERIC),CAST(v_vcShipCountry AS NUMERIC),false,2,2,v_numDivisionID,v_numDomainID;
         ELSE
            INSERT INTO AddressDetails(vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID)
            SELECT 'Address1',v_vcBillStreet,v_vcBillCity,v_vcBillPostCode,v_vcBillState,v_vcBillCountry,true,2,1,v_numDivisionID,v_numDomainID
            union
            SELECT 'Address1',v_vcShipStreet,v_vcShipCity,v_vcShipPostCode,CAST(v_vcShipState AS NUMERIC),CAST(v_vcShipCountry AS NUMERIC),true,2,2,v_numDivisionID,v_numDomainID;
         end if;
      end if;
       --End of Sachin Script                   

      IF v_UpdateDefaultTax = true then
		
         INSERT INTO DivisionTaxTypes
         SELECT
         v_numDivisionID,numTaxItemID,true
         FROM
         TaxItems
         WHERE
         numDomainID = v_numDomainID
         UNION
         SELECT
         v_numDivisionID,0,true;
      end if;
      select   numGroupID INTO v_numGroupID FROM
      AuthenticationGroupMaster WHERE
      numDomainID = v_numDomainID
      AND tintGroupType = 2    LIMIT 1;
      IF v_numGroupID IS NULL then
         v_numGroupID := 0;
      end if;
      INSERT INTO ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)
		VALUES(v_numCompanyID,v_numDivisionID,v_numGroupID,v_numDomainID);
   ELSEIF v_numDivisionID > 0
   then
      SELECT COUNT(*) AS numFollowUpStatus INTO v_PreFollow FROM FollowUpHistory WHERE numDivisionID = v_numDivisionID;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      v_RowCount := CAST(@@rowcount AS VARCHAR(2));
      select   numFollowUpStatus, bintModifiedDate INTO v_numFollow,v_binAdded FROM
      DivisionMaster WHERE
      numDivisionID = v_numDivisionID;
      IF v_numFollow <> '0' AND cast(NULLIF(v_numFollow,'') as NUMERIC(9,0)) <> v_numFollowUpStatus then
         IF cast(NULLIF(v_PreFollow,'') as INTEGER) <> 0 then
			
            IF cast(NULLIF(v_numFollow,'') as NUMERIC(18,0)) <>(select   numFollowUpstatus from FollowUpHistory where numDivisionID = v_numDivisionID order by numFollowUpStatusID desc LIMIT 1) then
				
               INSERT INTO FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)
					VALUES(CAST(v_numFollow AS NUMERIC(18,0)),v_numDivisionID,CAST(v_binAdded AS TIMESTAMP));
            end if;
         ELSE
            INSERT INTO FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)
				VALUES(CAST(v_numFollow AS NUMERIC(18,0)),v_numDivisionID,CAST(v_binAdded AS TIMESTAMP));
         end if;
      end if;
      UPDATE
      DivisionMaster
      SET
      numCompanyID = v_numCompanyID,vcDivisionName = v_vcDivisionName,numGrpId = v_numGrpId,
      numFollowUpStatus = v_numFollowUpStatus,numTerID = v_numTerID,
      bitPublicFlag = v_bitPublicFlag,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      tintCRMType = v_tintCRMType,numStatusID = v_numStatusID,
      tintBillingTerms =(CASE WHEN coalesce(v_numBillingDays,0) > 0 THEN 1 ELSE 0 END),numBillingDays = v_numBillingDays,
      tintInterestType = v_tintInterestType,fltInterest = v_fltInterest,vcComPhone = v_vcComPhone,
      vcComFax = v_vcComFax,numCampaignID = v_numCampaignID,
      bitNoTax = v_bitNoTax,numCompanyDiff = v_numCompanyDiff,vcCompanyDiff = v_vcCompanyDiff,
      numCurrencyID = v_numCurrencyID,numPartenerSource = v_numPartenerSource,
      numPartenerContact = v_numPartenerContact
      WHERE
      numDivisionID = v_numDivisionID;
      UPDATE
      AddressDetails
      SET
      vcStreet = v_vcBillStreet,vcCity = v_vcBillCity,vcPostalCode = v_vcBillPostCode,
      numState = v_vcBillState,numCountry = v_vcBillCountry
      WHERE
      numDomainID = v_numDomainID
      AND numRecordID = v_numDivisionID
      AND bitIsPrimary = true
      AND tintAddressOf = 2
      AND tintAddressType = 1;
      UPDATE
      AddressDetails
      SET
      vcStreet = v_vcShipStreet,vcCity = v_vcShipCity,vcPostalCode = v_vcShipPostCode,
      numState = v_vcShipState,numCountry = v_vcShipCountry
      WHERE
      numDomainID = v_numDomainID
      AND numRecordID = v_numDivisionID
      AND bitIsPrimary = true
      AND tintAddressOf = 2
      AND tintAddressType = 2;
   end if;

    ---Updating if organization is assigned to someone                
   select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo FROM
   DivisionMaster WHERE
   numDivisionID = v_numDivisionID;                
        
   IF (v_tempAssignedTo <> v_numAssignedTo and  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
	
      UPDATE
      DivisionMaster
      SET
      numAssignedTo = v_numAssignedTo,numAssignedBy = v_numUserCntID
      WHERE
      numDivisionID = v_numDivisionID;
   end if;               
  
   open SWV_RefCur for
   SELECT v_numDivisionID;
   RETURN;
END; $$;


