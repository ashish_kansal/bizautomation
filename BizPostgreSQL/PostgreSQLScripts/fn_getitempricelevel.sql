DROP FUNCTION IF EXISTS fn_GetItemPriceLevel;

CREATE OR REPLACE FUNCTION fn_GetItemPriceLevel(v_numItemCode NUMERIC,
	v_numWareHouseItemID NUMERIC(18,0))
RETURNS table
(
   intFromQty INTEGER,
   intToQty INTEGER,
   vcRuleType VARCHAR(50),
   vcDiscountType VARCHAR(50),
   decDiscount DECIMAL(18,2),
   monPriceLevelPrice DECIMAL(20,5),
   vcPriceLevelType VARCHAR(50)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monListPrice  DECIMAL(20,5) DEFAULT 0;
   v_monWListPrice  DECIMAL(20,5) DEFAULT 0;
   v_monVendorCost  DECIMAL(20,5) DEFAULT 0;
   v_numDomainID  NUMERIC(18,0);
   v_numItemClassification  NUMERIC(18,0);
   v_numRelationship  NUMERIC(9,0);                
   v_numProfile  NUMERIC(9,0);  
   v_numDivisionID  NUMERIC(9,0);
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETITEMPRICELEVEL_PRICEBOOKRULES CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETITEMPRICELEVEL_PRICEBOOKRULES
   (
      intFromQty INTEGER,
      intToQty INTEGER,
      vcRuleType VARCHAR(50),
      vcDiscountType VARCHAR(50),
      decDiscount DECIMAL(18,2),
      monPriceLevelPrice DECIMAL(20,5),
      vcPriceLevelType VARCHAR(50)
   );
   select   numDomainID, numItemClassification, monListPrice INTO v_numDomainID,v_numItemClassification,v_monListPrice FROM Item WHERE numItemCode = v_numItemCode;

   IF ((v_numWareHouseItemID > 0) AND EXISTS(SELECT * FROM Item WHERE numItemCode = v_numItemCode and  charItemType = 'P')) then
	
      select   coalesce(monWListPrice,0) INTO v_monWListPrice FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      IF coalesce(v_monWListPrice,0) > 0 then
         v_monListPrice := v_monWListPrice;
      end if;
   end if;      



   v_monVendorCost := fn_GetVendorCost(v_numItemCode);


   INSERT INTO tt_FN_GETITEMPRICELEVEL_PRICEBOOKRULES
   SELECT
   PricingTable.intFromQty
		,PricingTable.intToQty
		,CAST(CASE tintRuleType
   WHEN 1 THEN 'Deduct from List price'
   WHEN 2 THEN 'Add to Primary Vendor Cost'
   WHEN 3 THEN 'Named Price'
   END AS VARCHAR(50)) AS vcRuleType
		,CAST(CASE
   WHEN (tintRuleType = 1 OR tintRuleType = 2) AND tintDiscountType = 1 THEN 'Percentage'
   WHEN (tintRuleType = 1 OR tintRuleType = 2) AND tintDiscountType = 2 THEN 'Flat'
   ELSE ''
   END AS VARCHAR(50)) AS vcDiscountType
		,CASE WHEN tintRuleType = 3 THEN 0 ELSE PricingTable.decDiscount END AS decDiscount
		,CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
   THEN v_monListPrice -(v_monListPrice*(PricingTable.decDiscount/100))
   WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
   THEN v_monListPrice - PricingTable.decDiscount
   WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
   THEN v_monVendorCost+(v_monVendorCost*(PricingTable.decDiscount/100))
   WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
   THEN v_monVendorCost+PricingTable.decDiscount
   WHEN tintRuleType = 3 --Named Price
   THEN PricingTable.decDiscount
   END AS monPriceLevelPrice,CAST('Item Price Level' AS VARCHAR(50))
   FROM    PricingTable
   WHERE   coalesce(numItemCode,0) = v_numItemCode AND coalesce(numCurrencyID,0) = 0
   ORDER BY numPricingID; 


   v_numRelationship := 0;                
   v_numProfile := 0;  
   v_numDivisionID := 0;                
                

   INSERT INTO tt_FN_GETITEMPRICELEVEL_PRICEBOOKRULES
   SELECT PT.intFromQty,PT.intToQty,CAST(CASE PT.tintRuleType WHEN 1 THEN 'Deduct from List price'
   WHEN 2 THEN 'Add to Primary Vendor Cost'
   WHEN 3 THEN 'Named Price' END AS VARCHAR(50)) AS vcRuleType,
	CAST(CASE WHEN (PT.tintRuleType = 1 OR PT.tintRuleType = 2) AND PT.tintDiscountType = 1 THEN 'Percentage'
   WHEN (PT.tintRuleType = 1 OR PT.tintRuleType = 2) AND PT.tintDiscountType = 2 THEN 'Flat'
   ELSE '' END AS VARCHAR(50)) AS vcDiscountType,CASE WHEN PT.tintRuleType = 3 THEN 0 ELSE PT.decDiscount END AS decDiscount,
   (CASE 
		WHEN COALESCE(P.bitRoundTo,false) = true AND COALESCE(P.tintRoundTo,0) > 0
		THEN ROUND(CASE 
				WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
				THEN v_monListPrice -(v_monListPrice*(PT.decDiscount/100))
				WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
				THEN v_monListPrice -PT.decDiscount
				WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
				THEN v_monVendorCost+(v_monVendorCost*(PT.decDiscount/100))
				WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
				THEN v_monVendorCost+PT.decDiscount
				WHEN PT.tintRuleType = 3 --Named Price
				THEN PT.decDiscount
			END ) + (CASE P.tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END)
		ELSE
			CASE 
				WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
				THEN v_monListPrice -(v_monListPrice*(PT.decDiscount/100))
				WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
				THEN v_monListPrice -PT.decDiscount
				WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
				THEN v_monVendorCost+(v_monVendorCost*(PT.decDiscount/100))
				WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
				THEN v_monVendorCost+PT.decDiscount
				WHEN PT.tintRuleType = 3 --Named Price
				THEN PT.decDiscount
			END 
	END) AS monPriceLevelPrice,CAST('Price Rule' AS VARCHAR(50))
   FROM   PriceBookRules P
   LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
   LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
   LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
   JOIN PricingTable PT ON P.numPricRuleID = PT.numPriceRuleID AND coalesce(PT.numCurrencyID,0) = 0
   WHERE   P.numDomainID = v_numDomainID AND P.tintRuleFor = 1 AND P.tintPricingMethod = 1
   AND
(
((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
   OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
   OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

   OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
   OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
   OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

   OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
   OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND tintStep3 = 3) -- Priority 8
   OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
)
   ORDER BY PP.Priority ASC;

RETURN QUERY (SELECT * FROM tt_FN_GETITEMPRICELEVEL_PRICEBOOKRULES);
END; $$;

