-- Stored procedure definition script USP_ImapInboxItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ImapInboxItems(v_PageSize INTEGER ,                            
v_CurrentPage INTEGER,                            
v_srchSubjectBody VARCHAR(100) DEFAULT '',                            
INOUT v_TotRecs INTEGER  DEFAULT NULL        ,                         
v_columnName VARCHAR(50) DEFAULT NULL ,        
v_columnSortOrder VARCHAR(4) DEFAULT NULL ,  
v_numDomainId NUMERIC DEFAULT NULL,  
v_numUserCntId NUMERIC DEFAULT NULL ,
v_tinttype SMALLINT DEFAULT NULL ,
v_ToEmail VARCHAR(150) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                      
   v_lastRec  INTEGER;                                                      
   v_strSql  VARCHAR(8000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                      
                                                     
                     
   if v_columnName = 'FromName' then
 
      v_columnName :=  'GetEmaillName(emailHistory.numEmailHstrID,4::SMALLINT)';
   end if;        
   if v_columnName = 'FromEmail' then
 
      v_columnName := 'GetEmaillAdd(emailHistory.numEmailHstrID,4::SMALLINT)';
   end if;        
                  
   v_strSql := 'With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS  RowNumber             
from emailHistory  ';                              
   if v_tinttype = 5 then 
      v_strSql := coalesce(v_strSql,'') ||
      'where emailHistory.tinttype=5  and numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and numUserCntId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(10)),1,10);
   end if;                  

   if v_tinttype = 2 then 
      v_strSql := coalesce(v_strSql,'') ||
      'join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                   
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId   
where emailHistory.tinttype=2 and numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and (EmailMaster.vcEmailId iLike ''' || coalesce(v_ToEmail,'') || ''' and EmailHStrToBCCAndCC.tintType=4 ) ';
   end if;

   if v_tinttype = 3 then 
      v_strSql := coalesce(v_strSql,'') ||
      'join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                   
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId   
where emailHistory.tinttype=3 and numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and (EmailMaster.vcEmailId iLike ''' || coalesce(v_ToEmail,'') || ''' ) ';
   end if;
                 
   if v_srchSubjectBody <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and (vcSubject  iLike ''%' || coalesce(v_srchSubjectBody,'') || '%''  
  or vcBodyText iLike ''%' || coalesce(v_srchSubjectBody,'') || '%''    
          
or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
(numemailid in (select numEmailid from emailmaster where vcemailid iLike ''%' || coalesce(v_srchSubjectBody,'') || '%'')) or  
   vcName iLike ''%' || coalesce(v_srchSubjectBody,'') || '%'') )';
   end if;                
           
            
            
                 
   v_strSql := coalesce(v_strSql,'') || ')                     
 select RowNumber,            
GetEmaillAdd(emailHistory.numEmailHstrID,4::SMALLINT) as FromEmail,                  
 GetEmaillName(emailHistory.numEmailHstrID,4::SMALLINT) as FromName,   
GetEmaillAdd(emailHistory.numEmailHstrID,1::SMALLINT) as ToEmail,                                 
GetEmaillAdd(emailHistory.numEmailHstrID,2::SMALLINT) as CCEmail,  
emailHistory.numEmailHstrID,                            
COALESCE(vcSubject,'''') as vcSubject,   
FormatedDateFromDate(bintCreatedOn,numDomainId) as bintCreatedOn,  
COALESCE(bitIsRead,false) as IsRead,                  
COALESCE(emailHistory.vcSize,''0'') as vcSize,                              
COALESCE(bitHasAttachments,false) as HasAttachments,                             
COALESCE(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                                                       
FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,                              
COALESCE(emailHistory.tintType,5) as type,                              
COALESCE(chrSource,''B'') as chrSource,                          
numUid  
  
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where emailHistory.tinttype=' || SUBSTR(CAST(v_tinttype AS VARCHAR(10)),1,10) || '  and                
  RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '              
union                 
 select 0 as RowNumber,null,null,null,null,count(*),null,null,            
null,null,null,null,null,null,null,null from tblSubscriber  order by RowNumber';            
   RAISE NOTICE '%',v_strSql;             
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


