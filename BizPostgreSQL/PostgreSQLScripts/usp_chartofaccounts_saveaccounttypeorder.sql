-- Stored procedure definition script USP_ChartOfAccounts_SaveAccountTypeOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ChartOfAccounts_SaveAccountTypeOrder(v_numDomainID NUMERIC(18,0)
	,v_vcAccountTypes TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
	IF LENGTH(coalesce(v_vcAccountTypes,'')) > 0 then
		UPDATE
			AccountTypeDetail ATD
		SET
			tintSortOrder = T1.tintSortOrder
		FROM
		(
			SELECT
				numAccountTypeID,
				tintSortOrder
			FROM
			XMLTABLE
			(
				'NewDataSet/AccountType'
				PASSING 
					CAST(v_vcAccountTypes AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numAccountTypeID NUMERIC(18,0) PATH 'numAccountTypeID',
					tintSortOrder INTEGER PATH 'tintSortOrder'
			) AS X
		) AS T1
		WHERE
			ATD.numAccountTypeID = T1.numAccountTypeID AND ATD.numDomainID = v_numDomainID;
   end if;

   RETURN;
END; $$;



