-- Stored procedure definition script USP_DeleteShippingRuleStateList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteShippingRuleStateList(v_numDomainID NUMERIC,
    v_numCountryID NUMERIC,
	v_numStateID NUMERIC,
	v_numRuleId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingRuleStateList
   WHERE numDomainID = v_numDomainID AND numCountryID = v_numCountryID
   AND numStateID = v_numStateID AND numRuleID = v_numRuleId;
   RETURN;
END; $$;



