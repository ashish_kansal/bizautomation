-- Stored procedure definition script USP_DeleteOperationBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteOperationBudget(v_numOperationId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OperationBudgetDetails OBD using OperationBudgetMaster OBM where OBM.numBudgetId = OBD.numBudgetID AND OBD.numBudgetID = v_numOperationId
   And OBM.numDomainId = v_numDomainId; 
   Delete from OperationBudgetMaster Where numBudgetId = v_numOperationId  And numDomainId = v_numDomainId;
   RETURN;
END; $$;


