CREATE OR REPLACE FUNCTION USP_GetFinancialYear(v_numFinYearId NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_NextFinYearFromDate  VARCHAR(20);
   v_MaxFinYearToDate  VARCHAR(20);
BEGIN
   IF v_tintMode = 0 then
            
      open SWV_RefCur for
      SELECT  numFinYearId,
                        numDomainId,
                        FormatedDateFromDate(dtPeriodFrom,numDomainId) AS dtPeriodFrom,
                        FormatedDateFromDate(dtPeriodTo,numDomainId) AS dtPeriodTo,
                        vcFinYearDesc,
                        bitCloseStatus,
                        bitAuditStatus,
                        bitCurrentYear AS bitCurrentYear,
                        CASE bitCurrentYear
      WHEN true THEN 'Yes'
      ELSE 'No'
      END AS CurrentYear,
                        CASE bitCloseStatus WHEN true THEN 'Yes' ELSE 'No' END  AS vcCloseStatus,
                        CASE bitAuditStatus WHEN true THEN 'Yes' ELSE 'No' END  AS vcAuditStatus
      FROM    FinancialYear
      WHERE   (numFinYearId = v_numFinYearId
      OR v_numFinYearId = 0)
      AND numDomainId = v_numDomainID;
   ELSEIF v_tintMode = 1
   then
                
      open SWV_RefCur for
      SELECT  numFinYearId,
                            vcFinYearDesc
      FROM    FinancialYear
      WHERE   bitCurrentYear = false
      AND numDomainId = v_numDomainID
      AND dtPeriodFrom >(SELECT dtPeriodTo FROM FinancialYear WHERE numDomainId = v_numDomainID AND bitCurrentYear = true);
   ELSEIF v_tintMode = 2
   then
                
      open SWV_RefCur for
      SELECT 
      numFinYearId,
                            vcFinYearDesc,
                            FormatedDateFromDate(dtPeriodFrom,numDomainId) AS dtPeriodFrom,
                            FormatedDateFromDate(dtPeriodTo,numDomainId) AS dtPeriodTo
      FROM    FinancialYear
      WHERE   bitCurrentYear = true
      AND numDomainId = v_numDomainID LIMIT 1;
   ELSEIF v_tintMode = 3
   then -- Get next FY start date
      select   FormatedDateFromDate(MAX(dtPeriodTo)+INTERVAL '1 day',v_numDomainID) INTO v_NextFinYearFromDate FROM    FinancialYear WHERE   numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT v_NextFinYearFromDate AS NextFinYearFromDate;
   ELSEIF v_tintMode = 4
   then -- Get to date of last closed FY
      select   coalesce(FormatedDateFromDate(MAX(dtPeriodTo),v_numDomainID),
      '') INTO v_MaxFinYearToDate FROM    FinancialYear WHERE
                    (bitAuditStatus = true OR bitCloseStatus = true) AND
      numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT  v_MaxFinYearToDate AS  FYClosingDate;
   end if;
   RETURN;
END; $$; 
    



