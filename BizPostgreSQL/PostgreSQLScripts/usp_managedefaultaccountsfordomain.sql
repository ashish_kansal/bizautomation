-- Stored procedure definition script USP_ManageDefaultAccountsForDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDefaultAccountsForDomain(v_numDomainID NUMERIC(9,0),
    v_str VARCHAR(8000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_str AS VARCHAR(10)),1,10) <> '' then

      DELETE  FROM AccountingCharges
      WHERE   numDomainID = v_numDomainID;
      INSERT  INTO AccountingCharges(numChargeTypeId,
                          numAccountID,
                          numDomainID)
      SELECT
      coalesce((SELECT numChargeTypeId FROM AccountingChargeTypes WHERE chChargeCode = X.chChargeCode),0) AS numChargeTypeId,
                                X.numAccountID,
                                v_numDomainID
      FROM
	  XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_str AS XML)
				COLUMNS
					id FOR ORDINALITY,
					chChargeCode CHAR(2) PATH 'chChargeCode',
					numAccountID NUMERIC(9,0) PATH 'numAccountID'
			) AS X;
	  
   end if;
   RETURN;
END; $$;



