-- Stored procedure definition script usp_SaveMessageMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveMessageMaster(INOUT v_numMessageId NUMERIC(18,0) DEFAULT 0 , 
v_numTopicId NUMERIC(18,0) DEFAULT 0, 
v_numParentMessageId NUMERIC(18,0) DEFAULT 0, 
v_intRecordType INTEGER DEFAULT 0, 
v_numRecordId NUMERIC(18,0) DEFAULT 0, 
v_vcMessage TEXT DEFAULT '', 
v_numCreatedBy NUMERIC(18,0) DEFAULT 0, 
v_numUpdatedBy NUMERIC(18,0) DEFAULT 0, 
v_bitIsInternal BOOLEAN DEFAULT false, 
v_numDomainId NUMERIC(18,0) DEFAULT 0,
INOUT v_status VARCHAR(100)  DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_numMessageId = 0 then
		
      INSERT INTO MessageMaster(numTopicId, numParentMessageId, intRecordType, numRecordId, vcMessage, numCreatedBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, bitIsInternal, numDomainID)
			VALUES(v_numTopicId, v_numParentMessageId, v_intRecordType, v_numRecordId, v_vcMessage, v_numCreatedBy, LOCALTIMESTAMP , v_numUpdatedBy, LOCALTIMESTAMP , v_bitIsInternal, v_numDomainId);
			
      v_numMessageId := CURRVAL('MessageMaster_seq');
      v_status := '1';
   ELSE
      UPDATE
      MessageMaster
      SET
      numTopicId = v_numTopicId,numParentMessageId = v_numParentMessageId,intRecordType = v_intRecordType,
      numRecordId = v_numRecordId,vcMessage = v_vcMessage,
      numUpdatedBy = v_numUpdatedBy,dtmUpdatedOn = LOCALTIMESTAMP,bitIsInternal = v_bitIsInternal,
      numDomainID = v_numDomainId
      WHERE
      numMessageId = v_numMessageId
      AND numDomainID = v_numDomainId;
      v_status := '3';
   end if;
   RETURN;
END; $$;


