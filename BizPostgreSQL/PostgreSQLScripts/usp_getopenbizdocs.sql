CREATE OR REPLACE FUNCTION usp_GetOpenBizDocs
(
	v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_vcBizDocStatus TEXT DEFAULT '',
    v_tintOppType SMALLINT DEFAULT NULL,
    v_SortCol VARCHAR(50) DEFAULT 'dtCreatedDate',
    v_SortDirection VARCHAR(4) DEFAULT 'desc',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,
	v_numBizDocId NUMERIC(9,0) DEFAULT 0,
    v_tintSalesUserRightType SMALLINT DEFAULT NULL,
    v_tintPurchaseUserRightType SMALLINT DEFAULT NULL,
    v_numUserCntID NUMERIC DEFAULT NULL,
	v_tintFilter SMALLINT DEFAULT NULL,
    v_dtFromDate TIMESTAMP DEFAULT NULL,
    v_dtToDate TIMESTAMP DEFAULT NULL,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_BaseCurrencySymbol  VARCHAR(3);
   v_firstRec  INTEGER;                                                              
   v_lastRec  INTEGER;                                                     
--        IF @vcOppBizDocIds <>'' SET @CurrentPage = 1; --bug id 1081
   v_strSql  TEXT;
   v_SELECT  TEXT;
   v_FROM  TEXT;
   v_WHERE  TEXT;
BEGIN
   v_BaseCurrencySymbol := '';
   select   coalesce(varCurrSymbol,'$') INTO v_BaseCurrencySymbol FROM Currency WHERE numCurrencyID IN(SELECT numCurrencyID FROM Domain
      WHERE numDomainId = v_numDomainId);

/*paging logic*/  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                             
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                    


/*Dynamic sql*/
   IF v_vcBizDocStatus IS NULL then
      v_vcBizDocStatus := '';
   end if;
       
   IF v_SortCol = 'status' then 
      v_SortCol := 'ld.vcData';
   end if;
      
--SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositeID = 432



   v_SELECT := 'WITH bizdocs
         AS (SELECT Row_number() OVER(ORDER BY ' || coalesce(v_SortCol,'') || ' '
   || coalesce(v_SortDirection,'')
   || ') AS row,
			om.vcpoppname,
			COALESCE(C.fltExchangeRate,1) fltExchangeRateCurrent,
			COALESCE(om.fltExchangeRate,1) fltExchangeRateOfOrder,
			''' || SUBSTR(CAST(v_BaseCurrencySymbol AS VARCHAR(100)),1,100) || ''' BaseCurrencySymbol,
			COALESCE(tintOppType,0) AS tintOppType,
			om.numoppid,
			obd.numoppbizdocsid,
			obd.vcbizdocid,
			obd.numbizdocstatus,
			obd.vccomments,
			obd.monamountpaid,
			obd.monDealAmount,
			obd.monCreditAmount monCreditAmount,
			COALESCE(C.varCurrSymbol,'''') varCurrSymbol,
			ld.vcData AS status,
			FormatedDateFromDate(dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') dtFromDate,
			CASE COALESCE(om.bitBillingTerms,false)
			WHEN true THEN FormatedDateFromDate(dtFromDate + COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays, 0)), 0) * INTERVAL ''1 day'','
   || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') 
			WHEN false THEN FormatedDateFromDate(obd.dtFromDate,'
   || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') 
			END AS dtDueDate,
			om.numContactId,
			om.numDivisionID,
			(obd.monDealAmount - COALESCE(monCreditAmount,0) - COALESCE(monamountpaid,0) ) AS baldue,
			vcCompanyName,DM.tintCRMType,
			obd.vcRefOrderNo,COALESCE(con.vcEmail,'''') AS vcEmail,COALESCE(obd.numBizDocTempID,0) AS numBizDocTempID,COALESCE(obd.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			CASE WHEN SWF_ISNUMERIC(CAST((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)) AS TEXT)) = true
 			     THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0))
				 ELSE 0
			END AS numBillingDaysName,
			obd.numBizDocId  ';
			
   v_FROM := ' FROM   opportunitymaster om
                    INNER JOIN opportunitybizdocs obd
                      ON om.numoppid = obd.numoppid
                     INNER JOIN DivisionMaster DM
						ON om.numDivisionId = DM.numDivisionID
						Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID  
                    LEFT OUTER JOIN  ListDetails ld
					  ON ld.numListItemID = obd.numbizdocstatus
					LEFT OUTER JOIN Currency C
					ON C.numCurrencyID = om.numCurrencyID
					JOIN AdditionalContactsInformation con ON con.numdivisionId = DM.numdivisionId AND con.numContactid = om.numContactId';
					
   v_WHERE := ' WHERE  (obd.bitauthoritativebizdocs = 1 OR obd.bitauthoritativebizdocs = 0)
                    /*AND om.tintopptype = ' || SUBSTR(CAST(v_tintOppType AS VARCHAR(15)),1,15) || '*/
					AND om.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
					AND (om.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || ' = 0) 
					AND (obd.monDealAmount - monamountpaid >0 ) 
					AND obd.dtFromDate + make_interval(mins =>  -1 * ' || CAST(v_ClientTimeZoneOffset AS VARCHAR(10)) || ') 
						BETWEEN ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' AND ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''';
					
		
		--@WHERE = @WHERE + ' AND COALESCE(' + @vcBizDocStatus + ','''') = '''')'
   IF coalesce(v_vcBizDocStatus,'') = '-1' then
      v_WHERE := coalesce(v_WHERE,'') || 'AND COALESCE(obd.numbizdocstatus,0) = 0 ';
   ELSEIF coalesce(v_vcBizDocStatus,'') = ''
   then
      v_vcBizDocStatus := '';
   ELSE
      v_WHERE := coalesce(v_WHERE,'') || ' AND obd.numbizdocstatus  IN (' || coalesce(v_vcBizDocStatus,'') || ')';
   end if;
			
--		IF @intBizDocStatus = -1 
--			SET @WHERE = @WHERE + 'AND COALESCE(obd.numbizdocstatus,0) = 0 ';
--		ELSE 
--			SET @WHERE = @WHERE + 'AND (obd.numbizdocstatus = ' + CONVERT(VARCHAR(15), @intBizDocStatus) + ' OR ' + CONVERT(VARCHAR(15), @intBizDocStatus)+ ' = 0) ';
					
   IF v_numBizDocId > 0 then
      v_WHERE := coalesce(v_WHERE,'') || ' AND obd.numBizDocId = ' || SUBSTR(CAST(v_numBizDocId AS VARCHAR(10)),1,10);
   end if;
   IF v_tintSalesUserRightType = 1 then --Owner
            
      v_WHERE := coalesce(v_WHERE,'') || ' AND (om.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or om.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or  om.numOppId in (select distinct(numOppId)                             
							from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
   ELSEIF v_tintSalesUserRightType = 2
   then -- Terittory
                
      v_WHERE := coalesce(v_WHERE,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)
      || ' ) or DM.numTerID=0 or om.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or  om.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
   end if; 

   IF v_tintFilter = 1 then
		
      v_WHERE := coalesce(v_WHERE,'') || ' AND om.numRecOwner=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;
		
   IF v_tintFilter = 2 then
		
      v_WHERE := coalesce(v_WHERE,'') || ' AND om.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;
   v_strSql := coalesce(v_SELECT,'') || coalesce(v_FROM,'') ||
   coalesce(v_WHERE,'') || ') SELECT * FROM   bizdocs WHERE row > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and row <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10); 
        
        
   RAISE NOTICE '%',v_strSql;
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
        
	/*Get total count */
	v_strSql :=  ' (SELECT COUNT(*) ' || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ')';
        
	EXECUTE v_strSql INTO v_TotRecs;
   RETURN;
END; $$;
  


