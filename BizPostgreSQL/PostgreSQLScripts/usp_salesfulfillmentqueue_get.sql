-- Stored procedure definition script USP_SalesFulfillmentQueue_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SalesFulfillmentQueue_Get(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM SalesFulfillmentQueue WHERE numOppID NOT IN(SELECT numOppId FROM OpportunityMaster);


	-- FIRST DELETE ENTRIES FROM QUEUE WHICH ARE NOT VALID BECAUSE MASS FULFILLMENT RULE MAY BE CHANGED
   DELETE FROM
   SalesFulfillmentQueue
   WHERE
   coalesce(bitExecuted,false) = false
   AND numSFQID NOT IN(SELECT
   SFQ.numSFQID
   FROM
   SalesFulfillmentQueue SFQ
   INNER JOIN
   SalesFulfillmentConfiguration SFC
   ON
   SFQ.numDomainID = SFC.numDomainID
   WHERE
									(SFC.bitRule1IsActive = true AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus = SFC.numRule1OrderStatus)
   OR (SFC.bitRule2IsActive = true AND SFQ.numOrderStatus = SFC.numRule2OrderStatus)
   OR (SFC.bitRule3IsActive = true AND SFQ.numOrderStatus = SFC.numRule3OrderStatus)
   OR (SFC.bitRule4IsActive = true AND SFQ.numOrderStatus = SFC.numRule4OrderStatus));
	
	-- GET TOP 25 entries
   open SWV_RefCur for SELECT 
   SFQ.numSFQID,
		SFQ.numDomainID,
		SFQ.numUserCntID,
		SFQ.numOppID,
		OM.numDivisionId,
		OM.numContactId,
		D.numCurrencyID,
		D.bitMinUnitPriceRule,
		OM.tintopptype,
		OM.tintoppstatus,
		OM.tintshipped,
		D.numDefaultSalesShippingDoc,
		D.IsEnableDeferredIncome,
		D.bitAutolinkUnappliedPayment,
		coalesce(OM.intUsedShippingCompany,coalesce(D.numShipCompany,91)) AS numShipCompany,
		CASE
   WHEN (SFC.bitRule1IsActive = true AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus = SFC.numRule1OrderStatus) THEN 1
   WHEN (SFC.bitRule2IsActive = true AND SFQ.numOrderStatus = SFC.numRule2OrderStatus) THEN 2
   WHEN (SFC.bitRule3IsActive = true AND SFQ.numOrderStatus = SFC.numRule3OrderStatus) THEN 3
   WHEN (SFC.bitRule4IsActive = true AND SFQ.numOrderStatus = SFC.numRule4OrderStatus) THEN 4
   ELSE 0
   END	AS tintRule
   FROM
   SalesFulfillmentQueue SFQ
   INNER JOIN
   SalesFulfillmentConfiguration SFC
   ON
   SFQ.numDomainID = SFC.numDomainID
   INNER JOIN
   OpportunityMaster OM
   ON
   SFQ.numOppID = OM.numOppId
   INNER JOIN
   Domain D
   ON
   SFQ.numDomainID = D.numDomainId
   WHERE
   coalesce(SFQ.bitExecuted,false) = false
   AND ((SFC.bitRule1IsActive = true AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus = SFC.numRule1OrderStatus)
   OR (SFC.bitRule2IsActive = true AND SFQ.numOrderStatus = SFC.numRule2OrderStatus)
   OR (SFC.bitRule3IsActive = true AND SFQ.numOrderStatus = SFC.numRule3OrderStatus)
   OR (SFC.bitRule4IsActive = true AND SFQ.numOrderStatus = SFC.numRule4OrderStatus)) LIMIT 25;
END; $$;











