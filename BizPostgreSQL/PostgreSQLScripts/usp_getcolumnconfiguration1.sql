DROP FUNCTION IF EXISTS USP_GetColumnConfiguration1;

CREATE OR REPLACE FUNCTION USP_GetColumnConfiguration1(v_numDomainID NUMERIC(9,0) ,
    v_numUserCntId NUMERIC(9,0) ,
    v_FormId SMALLINT ,
    v_numtype NUMERIC(9,0) ,
    v_numViewID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  SMALLINT;
BEGIN
   IF v_FormId = 10 OR v_FormId = 44 then
      v_PageId := 4;
   ELSEIF v_FormId = 11 OR v_FormId = 34 OR v_FormId = 35 OR v_FormId = 36 OR v_FormId = 96 OR v_FormId = 97
   then
      v_PageId := 1;
   ELSEIF v_FormId = 12
   then
      v_PageId := 3;
   ELSEIF v_FormId = 13
   then
      v_PageId := 11;
   ELSEIF (v_FormId = 14 AND (v_numtype = 1 OR v_numtype = 3)) OR v_FormId = 33 OR v_FormId = 38 OR v_FormId = 39 OR v_FormId = 23
   then
      v_PageId := 2;
   ELSEIF v_FormId = 14 AND (v_numtype = 2 OR v_numtype = 4) OR v_FormId = 40 OR v_FormId = 41
   then
      v_PageId := 6;
   ELSEIF v_FormId = 21 OR v_FormId = 26 OR v_FormId = 32 OR v_FormId = 126 OR v_FormId = 129
   then
      v_PageId := 5;
   ELSEIF v_FormId = 22 OR v_FormId = 30
   then
      v_PageId := 5;
   ELSEIF v_FormId = 76
   then
      v_PageId := 10;
   ELSEIF v_FormId = 84 OR v_FormId = 85
   then
      v_PageId := 5;
   end if;

              
   IF v_PageId = 1 OR v_PageId = 4 then
    
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                0 AS Custom
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
      FROM    CFW_Fld_Master
      LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
      AND numRelation =(CASE
      WHEN v_numtype IN(1,2,3)
      THEN numRelation
      ELSE v_numtype
      END)
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.Grp_id = v_PageId
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_type <> 'Link'
      ORDER BY Custom,vcFieldName;
   ELSEIF v_PageId = 10
   then
    
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                0 AS Custom
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
      FROM    CFW_Fld_Master
      LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
      AND numRelation =(CASE
      WHEN v_numtype IN(1,2,3)
      THEN numRelation
      ELSE v_numtype
      END)
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.Grp_id = 5
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_Type = 'SelectBox'
      ORDER BY Custom,vcFieldName;
   ELSEIF v_FormId = 43
   then
	
      RAISE NOTICE 'ABC';
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    0 AS Custom ,
                    vcDbColumnName
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom ,
                    CAST(fld_id AS VARCHAR(10)) AS vcDbColumnName
      FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.Grp_id = 1
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_type <> 'Link'
      ORDER BY Custom,vcFieldName;
   ELSEIF v_FormId = 125 AND v_PageId = 0
   then
	
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    0 AS Custom
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID;
   ELSEIF v_PageId = 5 AND (v_FormId = 84 OR v_FormId = 85)
   then
    
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CAST(fld_id AS VARCHAR(10)) AS vcDbColumnName
      FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   CFW_Fld_Master.Grp_id = v_PageId
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_type = 'TextBox'
      ORDER BY Custom,vcFieldName;
   ELSEIF v_FormId = 141
   then
	
      open SWV_RefCur for
      SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
            (CASE WHEN v_numViewID = 6 AND numFieldID = 248 THEN 'BizDoc' ELSE coalesce(vcCultureFieldName,vcFieldName) END) AS vcFieldName ,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT
      CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
            fld_label AS vcFieldName ,
            1 AS Custom ,
            CAST(fld_id AS VARCHAR(10)) AS vcDbColumnName,
			false AS bitRequired
      FROM
      CFW_Fld_Master
      LEFT JOIN
      CFw_Grp_Master
      ON
      subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE
      CFW_Fld_Master.Grp_id IN(2,5)
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_type <> 'Link'
      ORDER BY
      Custom,vcFieldName;
   ELSEIF v_FormId = 142
   then
	
      open SWV_RefCur for
      SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID,
            coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      ORDER BY
      vcFieldName;
   ELSEIF v_FormId = 145
   then
	
      open SWV_RefCur for
      SELECT
      '6~0' AS numFieldID,'Actual Start' AS vcFieldName,0 AS Custom,'dtmActualStartDate' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '12~0' AS numFieldID,'Build Manager' AS vcFieldName,0 AS Custom,'vcAssignedTo' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '11~0' AS numFieldID,'Build Process' AS vcFieldName,0 AS Custom,'Slp_Name' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '13~0' AS numFieldID,'Capacity Load' AS vcFieldName,0 AS Custom,'CapacityLoad' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '10~0' AS numFieldID,'Forecast' AS vcFieldName,0 AS Custom,'ForecastDetails' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '4~0' AS numFieldID,'Item' AS vcFieldName,0 AS Custom,'AssemblyItemSKU' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '9~0' AS numFieldID,'Item Release' AS vcFieldName,0 AS Custom,'ItemReleaseDate' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '2~0' AS numFieldID,'Max Build Qty' AS vcFieldName,0 AS Custom,'MaxBuildQty' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '15~0' AS numFieldID,'Picked | Remaining' AS vcFieldName,0 AS Custom,'vcPickedRemaining' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '5~0' AS numFieldID,'Planned Start' AS vcFieldName,0 AS Custom,'dtmStartDate' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '8~0' AS numFieldID,'Projected Finish' AS vcFieldName,0 AS Custom,'dtmProjectFinishDate' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '7~0' AS numFieldID,'Requested Finish' AS vcFieldName,0 AS Custom,'dtmEndDate' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '3~0' AS numFieldID,'Total Progress' AS vcFieldName,0 AS Custom,'TotalProgress' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '14~0' AS numFieldID,'Work Order Status' AS vcFieldName,0 AS Custom,'vcWorkOrderStatus' AS vcDbColumnName,0 AS bitRequired
      UNION SELECT '1~0' AS numFieldID,'Work Order(Qty)' AS vcFieldName,0 AS Custom,'vcWorkOrderNameWithQty' AS vcDbColumnName,0 AS bitRequired;
   ELSE
      open SWV_RefCur for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = v_FormId
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT  CAST(fld_id AS VARCHAR(9)) || '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CAST(fld_id AS VARCHAR(10)) AS vcDbColumnName
      FROM    CFW_Fld_Master
      LEFT JOIN CFw_Grp_Master ON subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
      WHERE   1 =(CASE WHEN v_FormId = 21 THEN(CASE WHEN  CFW_Fld_Master.Grp_id IN(5,9) THEN 1 ELSE 0 END) ELSE(CASE WHEN  CFW_Fld_Master.Grp_id = v_PageId THEN 1 ELSE 0 END) END)
      AND CFW_Fld_Master.numDomainID = v_numDomainID
      AND Fld_type <> 'Link'
      ORDER BY Custom,vcFieldName;
   end if;
	                          
                   
   IF v_PageId = 1 OR v_PageId = 4 then
    
      IF (v_FormId = 96 OR v_FormId = 97) then
         open SWV_RefCur2 for
         SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                        coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
         FROM    View_DynamicColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND coalesce(bitCustom,false) = false
         AND tintPageType = 1
         AND coalesce(bitSettingField,false) = true
         AND numRelCntType = v_numtype
         AND coalesce(bitDeleted,false) = false
         UNION
         SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
         FROM    View_DynamicCustomColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND Grp_id = v_PageId
         AND numDomainID = v_numDomainID
         AND vcAssociatedControlType <> 'Link'
         AND coalesce(bitCustom,false) = true
         AND tintPageType = 1
         AND numRelCntType = v_numtype
         ORDER BY tintOrder;
      ELSE
         open SWV_RefCur2 for
         SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                        coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
         FROM    View_DynamicColumns
         WHERE   numFormId = v_FormId
         AND numUserCntID = v_numUserCntId
         AND numDomainID = v_numDomainID
         AND coalesce(bitCustom,false) = false
         AND tintPageType = 1
         AND coalesce(bitSettingField,false) = true
         AND numRelCntType = v_numtype
         AND coalesce(bitDeleted,false) = false
         UNION
         SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
         FROM    View_DynamicCustomColumns
         WHERE   numFormId = v_FormId
         AND numUserCntID = v_numUserCntId
         AND numDomainID = v_numDomainID
         AND Grp_id = v_PageId
         AND numDomainID = v_numDomainID
         AND vcAssociatedControlType <> 'Link'
         AND coalesce(bitCustom,false) = true
         AND tintPageType = 1
         AND numRelCntType = v_numtype
         ORDER BY tintOrder;
      end if;
   ELSEIF v_PageId = 10
   then
    
      open SWV_RefCur2 for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                vcDbColumnName ,
                0 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS numFieldID1
      FROM    View_DynamicColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND coalesce(bitCustom,false) = false
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND numRelCntType = v_numtype
      AND coalesce(bitDeleted,false) = false
      UNION
      SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                vcFieldName ,
                '' AS vcDbColumnName ,
                1 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldId AS numFieldID1
      FROM    View_DynamicCustomColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND numDomainID = v_numDomainID
      AND vcAssociatedControlType = 'SelectBox'
      AND coalesce(bitCustom,false) = true
      AND tintPageType = 1
      AND numRelCntType = v_numtype
      ORDER BY tintOrder;
   ELSEIF v_PageId = 5 AND (v_FormId = 84 OR v_FormId = 85)
   then
    
      IF EXISTS(SELECT
      'col1'
      FROM
      DycFormConfigurationDetails
      WHERE
      numDomainID = v_numDomainID
      AND numFormID = v_FormId
      AND numFieldID IN(507,508,509,510,511,512)) then
        
         open SWV_RefCur2 for
         SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
         FROM    View_DynamicColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND coalesce(bitCustom,false) = false
         AND coalesce(bitSettingField,false) = true
         AND coalesce(bitDeleted,false) = false
         UNION
         SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    '' AS vcLookBackTableName ,
                    numFieldId AS FieldId ,
                    false AS bitAllowEdit ,
                    0 AS AZordering
         FROM    View_DynamicCustomColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND Grp_id = v_PageId
         AND vcAssociatedControlType = 'TextBox'
         AND coalesce(bitCustom,false) = true
         ORDER BY tintOrder;
      ELSE
         open SWV_RefCur2 for
         SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
         FROM    View_DynamicColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND coalesce(bitCustom,false) = false
         AND coalesce(bitSettingField,false) = true
         AND coalesce(bitDeleted,false) = false
         UNION
         SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    '' AS vcLookBackTableName ,
                    numFieldId AS FieldId ,
                    false AS bitAllowEdit ,
                    0 AS AZordering
         FROM    View_DynamicCustomColumns
         WHERE   numFormId = v_FormId
         AND numDomainID = v_numDomainID
         AND Grp_id = v_PageId
         AND vcAssociatedControlType = 'TextBox'
         AND coalesce(bitCustom,false) = true
         UNION
         SELECT  CAST('507~0' AS VARCHAR(9)) AS numFieldID ,
                    'Title:A-Z' AS vcFieldName ,
                    'Title:A-Z' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    507 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         UNION
         SELECT  CAST('508~0' AS VARCHAR(9)) AS numFieldID ,
                    'Title:Z-A' AS vcFieldName ,
                    'Title:Z-A' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    508 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         UNION
         SELECT  CAST('509~0' AS VARCHAR(9)) AS numFieldID ,
                    'Price:Low to High' AS vcFieldName ,
                    'Price:Low to High' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    509 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         UNION
         SELECT  CAST('510~0' AS VARCHAR(9)) AS numFieldID ,
                    'Price:High to Low' AS vcFieldName ,
                    'Price:High to Low' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    510 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         UNION
         SELECT  CAST('511~0' AS VARCHAR(9)) AS numFieldID ,
                    'New Arrival' AS vcFieldName ,
                    'New Arrival' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    511 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         UNION
         SELECT  CAST('512~0' AS VARCHAR(9)) AS numFieldID ,
                    'Oldest' AS vcFieldName ,
                    'Oldest' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    512 AS FieldId ,
                    false AS bitAllowEdit ,
                    1 AS IsFixed
         ORDER BY tintOrder;
      end if;
   ELSEIF v_FormId = 43
   then
	
      open SWV_RefCur2 for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
                    coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit
      FROM    View_DynamicColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND coalesce(bitCustom,false) = false
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(bitDeleted,false) = false
      AND coalesce(numViewID,0) = v_numViewID
      UNION
      SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numListID ,
                    '' AS vcLookBackTableName ,
                    numFieldId AS FieldId ,
                    false AS bitAllowEdit
      FROM    View_DynamicCustomColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND Grp_id = 1
      AND numDomainID = v_numDomainID
      AND vcAssociatedControlType <> 'Link'
      AND coalesce(bitCustom,false) = true
      AND tintPageType = 1
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(numViewID,0) = v_numViewID
      ORDER BY tintOrder;
   ELSEIF v_FormId = 141
   then
	
      open SWV_RefCur2 for
      SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
            (CASE WHEN v_numViewID = 6 AND numFieldID = 248 THEN 'BizDoc' ELSE coalesce(vcCultureFieldName,vcFieldName) END) AS vcFieldName ,
            vcDbColumnName,
            0 AS Custom,
            tintRow AS tintOrder,
            vcAssociatedControlType,
            numListID,
            vcLookBackTableName,
            numFieldID AS FieldId,
            bitAllowEdit
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND coalesce(bitCustom,false) = false
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(bitDeleted,false) = false
      AND coalesce(numViewID,0) = v_numViewID
      UNION
      SELECT
      CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
            vcFieldName ,
            '' AS vcDbColumnName ,
            1 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numListID ,
            '' AS vcLookBackTableName ,
            numFieldId AS FieldId ,
            false AS bitAllowEdit
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND Grp_id IN(2,5)
      AND numDomainID = v_numDomainID
      AND vcAssociatedControlType <> 'Link'
      AND coalesce(bitCustom,false) = true
      AND tintPageType = 1
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(numViewID,0) = v_numViewID
      ORDER BY
      tintOrder;
   ELSEIF v_FormId = 142
   then
	
      open SWV_RefCur2 for
      SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
            coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
            vcDbColumnName ,
            0 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numListID ,
            vcLookBackTableName ,
            numFieldID AS FieldId ,
            bitAllowEdit
      FROM
      View_DynamicColumns
      WHERE
      numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND coalesce(bitCustom,false) = false
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(bitDeleted,false) = false
      AND coalesce(numViewID,0) = v_numViewID;
   ELSEIF v_FormId = 145
   then
	
      open SWV_RefCur2 for
      SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
            (CASE numFieldID
      WHEN 1 THEN 'Work Order(Qty)'
      WHEN 2 THEN 'Max Build Qty'
      WHEN 3 THEN 'Total Progress'
      WHEN 4 THEN 'Item'
      WHEN 5 THEN 'Planned Start'
      WHEN 6 THEN 'Actual Start'
      WHEN 7 THEN 'Requested Finish'
      WHEN 8 THEN 'Projected Finish'
      WHEN 9 THEN 'Item Release'
      WHEN 10 THEN 'Forecast'
      WHEN 11 THEN 'Build Process'
      WHEN 12 THEN 'Build Manager'
      WHEN 13 THEN 'Capacity Load'
      WHEN 14 THEN 'Work Order Status'
      WHEN 15 THEN 'Picked | Remaining'
      ELSE '-'
      END) AS vcFieldName ,
            (CASE numFieldID
      WHEN 1 THEN 'vcWorkOrderNameWithQty'
      WHEN 2 THEN 'MaxBuildQty'
      WHEN 3 THEN 'TotalProgress'
      WHEN 4 THEN 'AssemblyItemSKU'
      WHEN 5 THEN 'dtmStartDate'
      WHEN 6 THEN 'dtmActualStartDate'
      WHEN 7 THEN 'dtmEndDate'
      WHEN 8 THEN 'dtmProjectFinishDate'
      WHEN 9 THEN 'ItemReleaseDate'
      WHEN 10 THEN 'ForecastDetails'
      WHEN 11 THEN 'Slp_Name'
      WHEN 12 THEN 'vcAssignedTo'
      WHEN 13 THEN 'CapacityLoad'
      WHEN 14 THEN 'vcWorkOrderStatus'
      WHEN 15 THEN 'vcPickedRemaining'
      ELSE '-'
      END) AS vcDbColumnName ,
            0 AS Custom ,
            intRowNum AS tintOrder ,
            'Label' AS vcAssociatedControlType ,
            0 AS numlistid,
            'WorkOrder' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            false AS  bitAllowEdit
      FROM
      DycFormConfigurationDetails
      WHERE
      numFormID = 145
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID;
   ELSE
      open SWV_RefCur2 for
      SELECT  CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID ,
				coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName ,
				vcDbColumnName ,
				0 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numListID ,
				vcLookBackTableName ,
				numFieldID AS FieldId ,
				bitAllowEdit
      FROM    View_DynamicColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND coalesce(bitCustom,false) = false
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(bitDeleted,false) = false
      AND coalesce(numViewID,0) = v_numViewID
      UNION
      SELECT  CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID ,
				vcFieldName ,
				'' AS vcDbColumnName ,
				1 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numListID ,
				'' AS vcLookBackTableName ,
				numFieldId AS FieldId ,
				false AS bitAllowEdit
      FROM    View_DynamicCustomColumns
      WHERE   numFormId = v_FormId
      AND numUserCntID = v_numUserCntId
      AND numDomainID = v_numDomainID
      AND 1 =(CASE WHEN v_FormId = 21 THEN(CASE WHEN  Grp_id IN(5,9) THEN 1 ELSE 0 END) ELSE(CASE WHEN  Grp_id = v_PageId THEN 1 ELSE 0 END) END)
      AND numDomainID = v_numDomainID
      AND vcAssociatedControlType <> 'Link'
      AND coalesce(bitCustom,false) = true
      AND tintPageType = 1
      AND coalesce(numRelCntType,0) = v_numtype
      AND coalesce(numViewID,0) = v_numViewID
      ORDER BY tintOrder;
   end if;
   RETURN;
END; $$;


