-- Stored procedure definition script USP_InsertJournalEntryHeaderForReceivePayment for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryHeaderForReceivePayment(v_datEntry_Date TIMESTAMP,                                    
v_numAmount DECIMAL(20,5),                                  
v_numJournal_Id NUMERIC(9,0) DEFAULT 0,                              
v_numDomainId NUMERIC(9,0) DEFAULT 0,                      
v_numRecurringId NUMERIC(9,0) DEFAULT 0,                  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,    
v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0,  
v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,  
v_numOppId NUMERIC(9,0) DEFAULT 0 ,
v_numCheckNo NUMERIC(9,0) DEFAULT 0,
v_numProjectID NUMERIC DEFAULT 0,
v_numClassID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   v_numCheckNo :=(case v_numCheckNo when 0 then null else v_numCheckNo end);
   if v_numRecurringId = 0 then 
      v_numRecurringId := null;
   end if;                                 
   IF v_numOppId = 0 then 
      v_numOppId := NULL;
   end if; /*added by chintan for clean data and while using inner join it should not be skipped*/
   IF v_numProjectID = 0 then 
      v_numProjectID := NULL;
   end if; 
   IF v_numClassID = 0 then 
      v_numClassID := NULL;
   end if; 
   IF v_numOppBizDocsId = 0 then 
      v_numOppBizDocsId := NULL;
   end if; 
   If v_numJournal_Id = 0 then
  
      Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numRecurringId,numCreatedBy,datCreatedDate,numBizDocsPaymentDetId,numOppBizDocsId,numOppId,numCheckId,numProjectID,numClassID)
   Values(v_datEntry_Date,v_numAmount,v_numDomainId,v_numRecurringId,v_numUserCntID,TIMEZONE('UTC',now()),v_numBizDocsPaymentDetId,v_numOppBizDocsId,v_numOppId,v_numCheckNo,v_numProjectID,v_numClassID);
   
      v_numJournal_Id := CURRVAL('General_Journal_Header_seq');
      open SWV_RefCur for Select  v_numJournal_Id;
   end if;
END; $$;












