-- Stored procedure definition script USP_UpdateOpportunityBizDocsIntegratedToAcnt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOpportunityBizDocsIntegratedToAcnt(v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE OpportunityBizDocsPaymentDetails SET bitIntegrated = false WHERE numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;
   Update OpportunityBizDocsDetails Set bitIntegratedToAcnt = false  Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;
   RETURN;
END; $$;


