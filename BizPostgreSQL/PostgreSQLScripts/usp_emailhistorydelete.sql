-- Stored procedure definition script USP_EmailHistoryDelete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistoryDelete(v_numEmailHstrID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from EmailHistory where numEmailHstrID = v_numEmailHstrID;
   RETURN;
END; $$;


