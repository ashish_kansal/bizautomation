-- Stored procedure definition script USP_SaveCheckCompanyDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCheckCompanyDetails(v_numCheckCompanyId NUMERIC(9,0) DEFAULT 0,    
v_numCompanyId NUMERIC(9,0) DEFAULT 0,    
v_monAmount DECIMAL(20,5) DEFAULT 0,    
v_vcMemo VARCHAR(200) DEFAULT '',    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numCheckCompanyId = 0 then
  
      Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values(v_numCompanyId,v_monAmount,v_vcMemo,v_numDomainID,v_numUserCntID,TIMEZONE('UTC',now()));
   Else
      Update CheckCompanyDetails Set numCompanyId = v_numCompanyId,monAmount = v_monAmount,vcMemo = v_vcMemo,numDomainId = v_numDomainID,
      numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
      Where numCheckCompanyId = v_numCheckCompanyId;
   end if;
   RETURN;
END; $$;


