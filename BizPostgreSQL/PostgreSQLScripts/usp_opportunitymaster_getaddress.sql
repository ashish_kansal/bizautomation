-- Stored procedure definition script USP_OpportunityMaster_GetAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetAddress(v_numDomainID NUMERIC(18,0)
    ,v_numOppID NUMERIC(18,0)
	,v_tintAddressType SMALLINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintAddressType = 1 then --Billing Address
      	PERFORM USP_GetOppAddress(v_numOppID,0::SMALLINT,0,'SWV_RefCur');
	  	OPEN SWV_RefCur FOR FETCH ALL IN "SWV_RefCur";
      
	  open SWV_RefCur2 for
      SELECT vcAddress AS BillingAdderss,numContact,bitAltContact,vcAltContact FROM fn_getOPPAddressAndContact(v_numOppID,v_numDomainID,2::SMALLINT) AS ShippingAdderss;
   ELSEIF v_tintAddressType = 2
   then --Shipping Address
	
      PERFORM USP_GetOppAddress(v_numOppID,1::SMALLINT,0,'SWV_RefCur');
	  OPEN SWV_RefCur FOR FETCH ALL IN "SWV_RefCur";

      open SWV_RefCur2 for
      SELECT vcAddress AS ShippingAdderss,numContact,bitAltContact,vcAltContact FROM fn_getOPPAddressAndContact(v_numOppID,v_numDomainID,2::SMALLINT) AS ShippingAdderss;
   end if;
   RETURN;
END; $$;


