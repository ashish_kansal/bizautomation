DROP FUNCTION IF EXISTS USP_ItemDisplayFieldsForBarcode;

CREATE OR REPLACE FUNCTION USP_ItemDisplayFieldsForBarcode
(
	v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_strItemIdList TEXT DEFAULT NULL,
    v_bitAllItems BOOLEAN DEFAULT false,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
		v_join  VARCHAR(8000);
		v_Nocolumns  SMALLINT;               
		v_bitLocation  BOOLEAN;  
		v_strSql  TEXT;
		v_strWhere  TEXT;
		v_tintOrder  SMALLINT;                                                  
		v_vcFieldName  VARCHAR(50);                                                  
		v_vcListItemType  VARCHAR(3);                                                                                           
		v_vcAssociatedControlType  VARCHAR(10);                                                  
		v_numListID  NUMERIC(9,0);                                                  
		v_vcDbColumnName  VARCHAR(20);                      
		v_WhereCondition  VARCHAR(8000);                       
		v_vcLookBackTableName  VARCHAR(2000);                
		v_bitCustom  BOOLEAN;                  
		v_numFieldId  NUMERIC;  
		v_bitAllowSorting Boolean;   
		v_bitAllowEdit  Boolean;                   
                 
		v_ListRelID  NUMERIC(9,0);
		SWV_RowCount INTEGER;
BEGIN
   v_join := '';                                                        
           
   v_Nocolumns := 0;                
 
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicColumns
      WHERE     numFormId = 67
      AND numDomainID = v_numDomainID
      AND coalesce(numRelCntType,0) = 0
      UNION
      SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicCustomColumns
      WHERE     numFormId = 67
      AND numDomainID = v_numDomainID
      AND coalesce(numRelCntType,0) = 0) TotalRows;
       
        
   IF v_Nocolumns = 0 then
        
      INSERT  INTO DycFormConfigurationDetails(numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth,
                      numViewId)
      SELECT  67,
                            numFieldID,
                            0,
                            Row_number() OVER(ORDER BY tintRow DESC),
                            v_numDomainID,
                            v_numUserCntID,
                            0,
                            1,
                            0,
                            intColumnWidth,
                            0
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = 67
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID
      ORDER BY tintOrder ASC;
   end if;

   select(CASE WHEN COUNT(*) > 0 THEN 1
   ELSE 0
   END) INTO v_bitLocation FROM    View_DynamicColumns WHERE   numFormId = 67
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND bitCustom = false
   AND coalesce(bitSettingField,false) = true
   AND vcDbColumnName = 'vcWareHouse';
                                                                   
                                        
   v_strWhere := ' AND 1=1';
    
   IF LENGTH(coalesce(v_strItemIdList,'')) > 0 then
      v_strWhere := coalesce(v_strWhere,'') || ' AND numItemCode IN (SELECT Id FROM SplitIds(''' || CAST(v_strItemIdList AS TEXT) || ''','',''))';
   ELSE
      v_strWhere := coalesce(v_strWhere,'') || ' AND charItemType = ''P''';
   end if;
        
   v_strSql := 'DROP TABLE IF EXISTS tt_Items CASCADE; CREATE TEMPORARY TABLE tt_Items AS With tblItem AS (SELECT COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,numItemCode';       
	           
   v_strWhere := coalesce(v_strWhere,'') || ' group by numItemCode ';  
  
   IF v_bitLocation = true then
      v_strWhere := coalesce(v_strWhere,'') || '  ,vcWarehouse';
   end if;
        
   v_join := coalesce(v_join,'')
   || ' LEFT JOIN WareHouseItems ON numItemID = numItemCode ';
   v_join := coalesce(v_join,'')
   || ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID ';
        
      
   v_strSql := coalesce(v_strSql,'') || ' FROM Item  ' || coalesce(v_join,'') || '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= ' || COALESCE(v_numDomainID,0) || '
	AND ( WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' OR WareHouseItems.numWareHouseItemID IS NULL) ' || coalesce(v_strWhere,'');
    
   v_strSql := coalesce(v_strSql,'') || ')';
 
   v_strSql := coalesce(v_strSql,'')
   || ' select min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
	case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
	COALESCE(sum(numOnHand),0) as numOnHand,COALESCE(sum(numOnOrder),0) as numOnOrder,COALESCE(sum(numReorder),0) as numReorder,COALESCE(sum(numBackOrder),0) as numBackOrder,                  
	COALESCE(sum(numAllocation),0) as numAllocation,vcCompanyName,CAST(COALESCE(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,CAST(COALESCE(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,I.vcModelID,
	Cast(CASE WHEN charItemType=''P'' THEN COALESCE(WareHouseItems.monWListPrice,0) 
	     WHEN charItemType=''N'' THEN COALESCE(I.monListPrice,0)  
	     WHEN charItemType=''S'' THEN COALESCE(I.monListPrice,0)
	     ELSE COALESCE(I.monListPrice,0)
	END as decimal(18,2)) AS monListPrice ,
	I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass,
	COALESCE(I.numItemClassification,0) As numItemClassification,COALESCE(LDC.vcData,'''') AS ItemClassification,COALESCE(I.bitTaxable,false) As bitTaxable,COALESCE(I.bitKitParent,false) bitKitParent,
	COALESCE(I.numVendorID,0) numVendorID,COALESCE(I.bitAllowBackOrder,false) bitAllowBackOrder,COALESCE(I.bitSerialized,false) bitSerialized,COALESCE(I.numItemGroup,0) numItemGroup,
	COALESCE(I.monCampaignLabourCost,0) monCampaignLabourCost,COALESCE(I.bitFreeShipping,false) bitFreeShipping,COALESCE(I.vcUnitofMeasure,'''') vcUnitofMeasure,
	COALESCE(I.bitLotNo,false) bitLotNo,COALESCE(I.IsArchieve,false) IsArchieve,COALESCE(I.numItemClass,0) numItemClass,COALESCE(I.bitArchiveItem,false) bitArchiveItem,
	COALESCE(UOM1.vcUnitName,'''') vcBaseUnit,COALESCE(UOM2.vcUnitName,'''') vcSaleUnit,COALESCE(UOM3.vcUnitName,'''') vcPurchaseUnit,COALESCE(com.vcCompanyName,'''') vcVendor,
	COALESCE(V.vcPartNo,'''') vcPartNo,COALESCE(V.intMinQty,0) intMinQty,COALESCE(I.numBaseUnit,0) numBaseUnit,COALESCE(I.numSaleUnit,0) numSaleUnit,
	COALESCE(I.numPurchaseUnit,0) numPurchaseUnit,COALESCE(I.bitAssembly,false) As bitAssembly,COALESCE(vcItemGroup,'''') AS vcItemGroup ';
	
   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse';
   end if;  

   v_strSql := coalesce(v_strSql,'')
   || ' FROM tblItem JOIN Item I ON tblItem.numItemCode = I.numItemCode 
	left join WareHouseItems on numItemID = I.numItemCode                                  
	left join DivisionMaster div  on I.numVendorID = div.numDivisionID                                    
	left join CompanyInfo com on com.numCompanyId = div.numCompanyID   
	left join Warehouses W  on W.numWareHouseID = WareHouseItems.numWareHouseID 
	LEFT JOIN ListDetails LD ON LD.numListItemID = I.numShipClass
	LEFT JOIN ListDetails LDC ON LDC.numListItemID = I.numItemClassification
	LEFT JOIN UOM UOM1 ON UOM1.numUOMId = I.numBaseUnit
	LEFT JOIN UOM UOM2 ON UOM2.numUOMId = I.numSaleUnit
	LEFT JOIN UOM UOM3 ON UOM3.numUOMId = I.numPurchaseUnit
	LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID AND V.numVendorID = div.numDivisionID AND V.numItemCode = I.numItemCode 
	LEFT JOIN ItemGroups IG ON  IG.numItemGroupID = I.numItemGroup  
	group by I.numItemCode,vcCompanyName,monListPrice,
	monAverageCost,I.vcSKU,LD.vcData,    numItemClassification,bitTaxable,bitKitParent,I.numVendorID,bitAllowBackOrder,    bitSerialized,numItemGroup,
	monCampaignLabourCost,bitFreeShipping,    vcUnitofMeasure,bitLotNo,IsArchieve,numItemClass,bitArchiveItem,    vcCompanyName,vcPartNo,intMinQty,UOM1.vcUnitName, 
	UOM2.vcUnitName,UOM3.vcUnitName,    I.numBaseUnit,I.numSaleUnit,I.numPurchaseUnit,I.bitAssembly,    vcItemGroup,LDC.vcData,WareHouseItems.monWListPrice ';  
																																		   
   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse,I.vcItemName,    I.txtItemDesc,I.charItemType,I.vcModelID,I.vcManufacturer,I.numBarCodeId,    I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight';
   end if;  
        
    --SET @strSql = @strSql + ' order by RunningCount '   
          
   v_tintOrder := 0;                                                  
   v_WhereCondition := '';                 
   
   BEGIN
      CREATE TEMP SEQUENCE tt_TempForm_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType CHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0)
   );
        

   INSERT INTO
   tt_TEMPFORM(vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID)
   SELECT
   vcDbColumnName,
        vcFieldName,
        vcAssociatedControlType,
        vcListItemType,
        numListID,
        vcLookBackTableName,
        bitCustom,
        numFieldId,
        bitAllowSorting,
        bitAllowEdit,
        bitIsRequired,
        bitIsEmail,
        bitIsAlphaNumeric,
        bitIsNumeric,
        bitIsLengthValidation,
        intMaxLength,
        intMinLength,
        bitFieldMessage,
        vcFieldMessage,
        ListRelID
   FROM(SELECT
      tintRow+1 AS tintOrder,
			vcDbColumnName,
            coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
            vcAssociatedControlType,
            vcListItemType,
            numListID,
            vcLookBackTableName,
            bitCustom,
            numFieldId,
            bitAllowSorting,
            bitAllowEdit,
            bitIsRequired,
            bitIsEmail,
            bitIsAlphaNumeric,
            bitIsNumeric,
            bitIsLengthValidation,
            intMaxLength,
            intMinLength,
            bitFieldMessage,
            vcFieldMessage AS vcFieldMessage,
            ListRelID
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 67
            --AND numUserCntID = @numUserCntID
      AND numDomainID = v_numDomainID
            --AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      AND coalesce(numRelCntType,0) = 0
      UNION
      SELECT
      tintRow+1 AS tintOrder,
            vcDbColumnName,
            vcFieldName,
            vcAssociatedControlType,
            '' AS vcListItemType,
            numListID,
            '' AS vcLookBackTableName,
            bitCustom,
            numFieldId,
            bitAllowSorting,
            bitAllowEdit,
            bitIsRequired,
            bitIsEmail,
            bitIsAlphaNumeric,
            bitIsNumeric,
            bitIsLengthValidation,
            intMaxLength,
            intMinLength,
            bitFieldMessage,
            vcFieldMessage,
            ListRelID
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 67
            --AND numUserCntID = @numUserCntID
      AND numDomainID = v_numDomainID
            --AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      AND coalesce(numRelCntType,0) = 0) TEMP
   ORDER BY tintOrder ASC;      
            
  EXECUTE v_strSql;  
            
   v_strSql :=' select  TotalRowCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,
					CASE WHEN charItemType = ''P'' THEN ''Inventory Item'' 
						 WHEN charItemType = ''N'' THEN ''Non Inventory Item''
						 WHEN charItemType = ''A'' THEN ''Assembly Item''
						 WHEN charItemType = ''S'' THEN ''Service Item'' 
						 ELSE '''' 
					END AS ItemType,numOnHand, LD.vcData AS numShipClass,numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,
				   vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,
				   (Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode = temp.numItemCode and WO.numWOStatus = 0) as WorkOrder,
				   monAverageCost,monStockValue,vcSKU,numItemClassification,CASE WHEN bitTaxable = true THEN ''Yes'' ELSE ''No'' END AS bitTaxable,
				   CASE WHEN bitKitParent = true THEN ''Yes'' ELSE ''No'' END AS bitKitParent,numVendorID,
				   CASE WHEN bitAllowBackOrder = true THEN ''Yes'' ELSE ''No'' END AS bitAllowBackOrder,
				   CASE WHEN bitSerialized = true THEN ''Yes'' ELSE ''No'' END AS bitSerialized,numItemGroup,vcItemGroup,monCampaignLabourCost,
				   CASE WHEN bitFreeShipping = true THEN ''Yes'' ELSE ''No'' END AS bitFreeShipping,vcUnitofMeasure,
				   CASE WHEN bitLotNo = true THEN ''Yes'' ELSE ''No'' END AS bitLotNo,CASE WHEN IsArchieve = true THEN ''Yes'' ELSE ''No'' END AS IsArchieve,numItemClass,
				   CASE WHEN bitArchiveItem = true THEN ''Yes'' ELSE ''No'' END AS bitArchiveItem,vcBaseUnit,vcSaleUnit,vcPurchaseUnit,vcVendor,vcPartNo,intMinQty,
				   CASE WHEN bitAssembly = true THEN ''Yes'' ELSE ''No'' END AS bitAssembly,numBaseUnit,numSaleUnit,numPurchaseUnit,ItemClassification ';     
  
   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse';
   end if;                                               

     --WHERE bitCustomField=1
   select   tintOrder, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM    ORDER BY tintOrder ASC LIMIT 1;            

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
                
         RAISE NOTICE '%',v_vcDbColumnName;
         IF v_vcDbColumnName = 'vcPathForTImage' then
                        
            v_strSql := coalesce(v_strSql,'')
            || ',coalesce(II.vcPathForTImage,'''') AS vcPathForTImage';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = true';
         end if;
      ELSE
         RAISE NOTICE '%',v_vcDbColumnName;
      end if;
      IF v_bitCustom = true then
                    
         select   FLd_label, fld_type, 'Cust'
         || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName FROM    CFW_Fld_Master WHERE   CFW_Fld_Master.Fld_id = v_numFieldId;                 
     
              
--    print @vcAssociatedControlType                
         IF v_vcAssociatedControlType = 'TextBox'
         OR v_vcAssociatedControlType = 'TextArea' then
                            
            v_strSql := coalesce(v_strSql,'') || ',CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value  "' || coalesce(v_vcDbColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
                                
            v_strSql := coalesce(v_strSql,'')
            || ',case when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcDbColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
                                    
            v_strSql := coalesce(v_strSql,'')
            || ',FormatedDateFromDate(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,'
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
            || ')  "' || coalesce(v_vcDbColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
                                        
            v_vcDbColumnName := 'Cust'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.vcData' || ' "' || coalesce(v_vcDbColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode    ';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ListDetails L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=(CASE WHEN SWF_ISNUMERIC(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value::NUMERIC ELSE 0 END)';
         end if;
      end if;
             --AND bitCustomField=1
      select   tintOrder, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM WHERE   tintOrder > v_tintOrder   ORDER BY tintOrder ASC LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_tintOrder := 0;
      end if;
   END LOOP;                       
      
   RAISE NOTICE '%',v_bitLocation;
   v_strSql := coalesce(v_strSql,'')
   || ' from tt_Items temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '
   || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' ' || coalesce(v_WhereCondition,'');

   v_strSql := REPLACE(v_strSql,'|',','); 
   RAISE NOTICE '%',CAST(v_strSql AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_strSql;  

   UPDATE  tt_TEMPFORM
   SET     vcDbColumnName = CASE WHEN bitCustomField = true
   THEN vcDbColumnName
   ELSE vcDbColumnName
   END; 

   open SWV_RefCur2 for
   SELECT * FROM    tt_TEMPFORM;
   RETURN;
END; $$;


