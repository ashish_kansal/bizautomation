CREATE OR REPLACE FUNCTION usp_GetContactInfoForURLRedirection(v_numEntityId NUMERIC,    
 v_vcEntityType VARCHAR(4), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcEntityType = 'Cont' then
      open SWV_RefCur for
      SELECT c.numContactId, d.numDivisionID, d.tintCRMType, d.numCompanyID, d.numGrpId
      FROM AdditionalContactsInformation c INNER JOIN DivisionMaster d ON c.numDivisionId = d.numDivisionID
      WHERE c.numContactId = v_numEntityId;
   ELSEIF v_vcEntityType = 'Div'
   then
      open SWV_RefCur for
      SELECT  c.numContactId, d.numDivisionID, d.tintCRMType, d.numCompanyID, d.numGrpId
      FROM AdditionalContactsInformation c INNER JOIN DivisionMaster d ON c.numDivisionId = d.numDivisionID
      WHERE
      c.numDivisionId = v_numEntityId LIMIT 1;
   end if;
   RETURN;
END; $$;


