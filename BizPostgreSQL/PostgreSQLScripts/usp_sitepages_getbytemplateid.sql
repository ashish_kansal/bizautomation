-- Stored procedure definition script USP_SitePages_GetByTemplateID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SitePages_GetByTemplateID(v_numDomainID NUMERIC(18,0),
	v_numTemplateID  NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcTemplateCode  VARCHAR(500);
BEGIN
   select   '{template:' || REPLACE(LOWER(vcTemplateName),' ','_') || '}' INTO v_vcTemplateCode FROM SiteTemplates WHERE numTemplateID = v_numTemplateID AND numDomainID = v_numDomainID;


   open SWV_RefCur for SELECT * FROM
   SitePages
   WHERE
   numTemplateID IN(SELECT
      numTemplateID
      FROM
      SiteTemplates
      WHERE
      numDomainID = v_numDomainID AND
      numSiteID = v_numSiteID AND
      POSITION(LOWER(v_vcTemplateCode) IN LOWER(txtTemplateHTML)) > 0);
   RETURN;
END; $$;



/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/













