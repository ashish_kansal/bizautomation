-- Stored procedure definition script usp_UpdateCases for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateCases(v_intTargetResolveDate INTEGER DEFAULT 0,
	v_textSubject TEXT DEFAULT '',
	v_vcStatus VARCHAR(50) DEFAULT NULL,
	v_vcPriority VARCHAR(25) DEFAULT '',
	v_textDesc TEXT DEFAULT '',
	v_textInternalComments TEXT DEFAULT '',
	v_vcReason VARCHAR(250) DEFAULT '',
	v_vcOrigin VARCHAR(50) DEFAULT NULL,
	v_vcType VARCHAR(50) DEFAULT NULL,
	v_vcAssignTo VARCHAR(100) DEFAULT '',
	v_numCaseID NUMERIC(9,0) DEFAULT 0   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN

	--Insertion into Table  
   Update Cases set intTargetResolveDate = v_intTargetResolveDate,textSubject = v_textSubject,
   numStatus = v_vcStatus,numPriority = v_vcPriority,textDesc = v_textDesc,textInternalComments = v_textInternalComments,
   numReason = v_vcReason,numOrigin = v_vcOrigin,
   numType = v_vcType,numAssignedTo = v_vcAssignTo
   WHERE numCaseId = v_numCaseID;
   RETURN;
END; $$;


