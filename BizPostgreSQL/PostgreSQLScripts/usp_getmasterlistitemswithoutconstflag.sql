-- Stored procedure definition script USP_GetMasterListItemsWithoutConstFlag for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMasterListItemsWithoutConstFlag(v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numListID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bit3PL  BOOLEAN;
   v_bitEDI  BOOLEAN;
BEGIN
   select   coalesce(bit3PL,false), coalesce(bitEDI,false) INTO v_bit3PL,v_bitEDI FROM Domain WHERE numDomainId = v_numDomainID;
      
   open SWV_RefCur for SELECT
   Ld.numListItemID
		,coalesce(LDN.vcName,vcData) AS vcData
		,(SELECT  vcData FROM Listdetails WHERE numListItemID = Ld.numListItemGroupId LIMIT 1) AS vcListItemGroupName
		,coalesce(CASE WHEN Ld.numListID = 30 THEN(SELECT  vcColorScheme FROM DycFieldColorScheme WHERE numDomainID = v_numDomainID AND numFieldID = 18 AND numFormID = 34 AND vcFieldValue = CAST(Ld.numListItemID AS VARCHAR) LIMIT 1) ELSE '' END,'') AS vcColorScheme
		,Ld.constFlag
		,bitDelete
		,coalesce(intSortOrder,0) AS intSortOrder
		,coalesce(numListType,0) as numListType
		,(SELECT vcData FROM Listdetails WHERE numListID = 27 and numListItemID = Ld.numListType) AS vcListType
		,(CASE WHEN Ld.numListType = 1 THEN 'Sales' WHEN Ld.numListType = 2 THEN 'Purchase' ELSE 'All' END) AS vcOrderType
		,(CASE WHEN Ld.tintOppOrOrder = 1 THEN 'Opportunity' WHEN Ld.tintOppOrOrder = 2 THEN 'Order' ELSE 'All' END) AS vcOppOrOrder
		,numListType
		,tintOppOrOrder,coalesce(SOR.bitEnforceMinOrderAmount,false) AS bitEnforceMinOrderAmount, coalesce(SOR.fltMinOrderAmount,0) AS fltMinOrderAmount
		,(SELECT coalesce(Ld.vcData,'') WHERE Ld.numListID = 27) AS vcOriginalBizDocName
		,coalesce(numListItemGroupId,0) AS numListItemGroupId
   FROM
   Listdetails Ld
   LEFT JOIN
   listorder LO
   ON
   Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   LEFT JOIN
   ListDetailsName LDN
   ON
   LDN.numDomainID = v_numDomainID
   AND LDN.numListID = v_numListID
   AND LDN.numListItemID = Ld.numListItemID
   LEFT JOIN
   SalesOrderRule SOR ON Ld.numListItemID = SOR.numlistitemid
   WHERE
		(Ld.numDomainid = v_numDomainID OR Ld.constFlag = true)
   AND Ld.numListID = v_numListID
   AND 1 =(CASE
   WHEN Ld.numListItemID IN(15445,15446) THEN(CASE WHEN v_bit3PL = true THEN 1 ELSE 0 END)
   WHEN Ld.numListItemID IN(15447,15448) THEN(CASE WHEN v_bitEDI = true THEN 1 ELSE 0 END)
   WHEN Ld.numListID IN(29) THEN(CASE WHEN Ld.constFlag = true THEN 0 ELSE 1 END)
   ELSE 1
   END)
   ORDER BY
   intSortOrder;
END; $$;












