-- Stored procedure definition script USP_GetDivPartnerAllContacts for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivPartnerAllContacts(v_numDomainID  NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT A.numContactId,A.vcFirstName || ' ' || A.vcLastname AS vcGivenName FROM DivisionMaster AS O
   LEFT JOIN AdditionalContactsInformation AS A
   ON O.numPartenerContact = A.numContactId
   WHERE A.numDomainID = v_numDomainID AND coalesce(O.numPartenerContact,0) > 0 AND A.vcGivenName <> '';
END; $$;













