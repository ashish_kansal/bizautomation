-- Stored procedure definition script usp_GetParentOrgForCurrentOrg for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetParentOrgForCurrentOrg(v_numDomainID NUMERIC,
    v_numAssociateFromDivisionID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  cast(Ld.vcData as VARCHAR(255)),
                    cast(CI.vcCompanyName as VARCHAR(255)),
                    DM.vcDivisionName,
                    DM.numDivisionID,
                    DM.tintCRMType
   FROM
   CompanyAssociations CA
   INNER JOIN
   DivisionMaster DM
   ON
   CA.numDivisionID = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   INNER JOIN
   Listdetails Ld
   ON
   CA.numAssoTypeLabel = Ld.numListItemID
   WHERE   CA.numAssociateFromDivisionID = v_numAssociateFromDivisionID
   AND CA.numDomainID = v_numDomainID
   AND bitTypeLabel = true;
END; $$;













