-- Function definition script fn_CalOppItemTotalTaxAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CalOppItemTotalTaxAmt(v_numDomainID NUMERIC(9,0),    
v_numTaxItemID NUMERIC(9,0),    
v_numOppID NUMERIC(9,0), 
v_numOppBizDocID NUMERIC(9,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppItemID  NUMERIC(9,0); 
   v_numOppBizDocItemID  NUMERIC(9,0); 
   v_numItemCode  NUMERIC(9,0);  
   v_numUnitHour  DOUBLE PRECISION;
   v_ItemAmount  DECIMAL(20,5);    
   v_TotalTaxAmt  DECIMAL(20,5) DEFAULT 0; 
   v_tintTaxOperator  SMALLINT DEFAULT 0;
   v_numShippingServiceItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(tintTaxOperator,0) INTO v_tintTaxOperator FROM
   OpportunityMaster WHERE
   numOppId = v_numOppID;

   select   numShippingServiceItemID INTO v_numShippingServiceItemID FROM Domain WHERE numDomainId = v_numDomainID;

   IF v_numOppBizDocID = 0 then
	
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
      IF v_numTaxItemID = 0 and v_tintTaxOperator = 1 then
		
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(OI.numUOMId,0))*coalesce(numUnitHour,0),0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(monTotAmount,0)/100 END,0)) INTO v_TotalTaxAmt FROM
         OpportunityItems OI
         INNER JOIN
         OpportunityMasterTaxItems OMTI
         ON
         OMTI.numOppId = OI.numOppId
         AND OMTI.numTaxItemID = 0
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode WHERE
         OI.numOppId = v_numOppID
         AND coalesce(OI.numItemCode,0) <> coalesce(v_numShippingServiceItemID,0);
      ELSEIF v_numTaxItemID = 0 and v_tintTaxOperator = 2
      then
		
         v_TotalTaxAmt := 0;
      ELSE
         select   SUM(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(OI.numUOMId,0))*coalesce(numUnitHour,0),0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(monTotAmount,0)/100 END) INTO v_TotalTaxAmt FROM
         OpportunityItems OI
         INNER JOIN
         OpportunityItemsTaxItems OITI
         ON
         OI.numoppitemtCode = OITI.numOppItemID
         AND OITI.numOppId = v_numOppID
         INNER JOIN
         OpportunityMasterTaxItems OMTI
         ON
         OMTI.numOppId = v_numOppID
         AND OITI.numTaxItemID = OMTI.numTaxItemID
         AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0)
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode WHERE
         OI.numOppId = v_numOppID
         AND coalesce(OI.numItemCode,0) <> coalesce(v_numShippingServiceItemID,0)
         AND OITI.numTaxItemID = v_numTaxItemID;
      end if;
   ELSE
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
      IF v_numTaxItemID = 0 and v_tintTaxOperator = 1 then
		
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(OI.numUOMId,0))*coalesce(OpportunityBizDocItems.numUnitHour,0),0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(OpportunityBizDocItems.monTotAmount,0)/100 END,0)) INTO v_TotalTaxAmt FROM
         OpportunityBizDocItems
         JOIN
         OpportunityItems OI
         ON
         OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         OpportunityMasterTaxItems OMTI
         ON
         OMTI.numOppId = OI.numOppId
         AND OMTI.numTaxItemID = 0
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode WHERE
         numOppBizDocID = v_numOppBizDocID
         AND coalesce(OpportunityBizDocItems.numItemCode,0) <> coalesce(v_numShippingServiceItemID,0)
         AND OI.numOppId = v_numOppID;
      ELSEIF v_numTaxItemID = 0 and v_tintTaxOperator = 2
      then
		
         v_TotalTaxAmt := 0;
      ELSE
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(OI.numUOMId,0))*coalesce(OpportunityBizDocItems.numUnitHour,0),0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(OpportunityBizDocItems.monTotAmount,0)/100 END,0)) INTO v_TotalTaxAmt FROM
         OpportunityBizDocItems
         JOIN
         OpportunityItems OI
         ON
         OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         OpportunityItemsTaxItems OITI
         ON
         OI.numoppitemtCode = OITI.numOppItemID
         AND OITI.numOppId = v_numOppID
         INNER JOIN
         OpportunityMasterTaxItems OMTI
         ON
         OMTI.numOppId = v_numOppID
         AND OITI.numTaxItemID = OMTI.numTaxItemID
         AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0)
         JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode WHERE
         numOppBizDocID = v_numOppBizDocID
         AND coalesce(OpportunityBizDocItems.numItemCode,0) <> coalesce(v_numShippingServiceItemID,0)
         AND OI.numOppId = v_numOppID
         AND OITI.numTaxItemID = v_numTaxItemID;
      end if;
   end if;

   RETURN coalesce(v_TotalTaxAmt,0);
END; $$;

--This function is specific to sales tax. generic function for calculating tax is fn_CalItemTaxAmt
--SELECT * FROM [OpportunityMaster] WHERE [numOppId]=12275
--SELECT * FROM [OpportunityBizDocs] WHERE [numOppBizDocsId]=12360
--SELECT * FROM [OpportunityAddress] WHERE [numOppID]=12340
-- select dbo.[fn_CalSalesTaxAmt](11946,110)

