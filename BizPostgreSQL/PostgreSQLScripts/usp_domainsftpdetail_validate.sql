-- Stored procedure definition script USP_DomainSFTPDetail_Validate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DomainSFTPDetail_Validate(v_vcUsername VARCHAR(20)
	,v_vcPassword VARCHAR(20),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numDomainID as "numDomainID"
		,tintType as "tintType"
   FROM
   DomainSFTPDetail
   WHERE
   vcUsername = v_vcUsername
   AND vcPassword = v_vcPassword;
END; $$;












