-- Function definition script fn_GetBudgetComments for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetBudgetComments(v_numBudgetId NUMERIC(9,0),v_numChartAcntId NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcComments  VARCHAR(50);
BEGIN
   select   coalesce(vcComments,'') INTO v_vcComments From OperationBudgetDetails Where numBudgetID = v_numBudgetId And numChartAcntId = v_numChartAcntId;
----     
   
      
   Return v_vcComments;
END; $$;

