-- Stored procedure definition script usp_getDefaultValues for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getDefaultValues(v_intOption INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcSQLValue as VARCHAR(255)),cast(vcOptionName as VARCHAR(255)) from CustomReptOptValues where intOptions = v_intOption;
END; $$;












