-- Stored procedure definition script USp_UpdateUserSMTP for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USp_UpdateUserSMTP(v_numUserID NUMERIC(9,0),                                     
v_SMTPAuth BOOLEAN   ,
v_SMTPSSL BOOLEAN   ,
v_vcSMTPPassword VARCHAR(100), 
v_SMTPServer VARCHAR(200), 
v_SMTPPort  NUMERIC(9,0),
v_bitSMTPServer BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE UserMaster SET bitSMTPAuth = v_SMTPAuth,vcSmtpPassword = v_vcSMTPPassword,vcSMTPServer = v_SMTPServer,
   numSMTPPort = v_SMTPPort,bitSMTPSSL  =  v_SMTPSSL,bitSMTPServer = v_bitSMTPServer,
   tintMailProvider = 3
   WHERE numUserId = v_numUserID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_LeadConversionReport]    Script Date: 07/26/2008 16:19:24 ******/



