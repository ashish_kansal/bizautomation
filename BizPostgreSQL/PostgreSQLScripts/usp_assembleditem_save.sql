CREATE OR REPLACE FUNCTION USP_AssembledItem_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numQty NUMERIC(18,0)
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO AssembledItem(numDomainID,
		numItemCode,
		numWarehouseItemID,
		numAssembledQty,
		numCreatedBy,
		dtCreatedDate)
	VALUES(v_numDomainID,
		v_numItemCode,
		v_numWarehouseItemID,
		v_numQty,
		v_numUserCntID,
		TIMEZONE('UTC',now()));

	
open SWV_RefCur for SELECT cast(CURRVAL('AssembledItem_seq') as VARCHAR(255));
END; $$;







