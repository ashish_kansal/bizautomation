-- Function definition script fn_GetContactsAOINamesInCSV for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetContactsAOINamesInCSV(v_numContactID NUMERIC)
RETURNS VARCHAR(250) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AOINames  VARCHAR(250);
BEGIN
   select string_agg(AOIM.vcAOIName,',')  INTO v_AOINames FROM
   AOIContactLink AOIC
   INNER JOIN
   AOIMaster AOIM
   ON
   AOIC.numContactID = v_numContactID
   AND AOIC.numAOIID = AOIM.numAOIID WHERE
   AOIC.tintSelected = 1;
   RETURN v_AOINames;
END; $$;

