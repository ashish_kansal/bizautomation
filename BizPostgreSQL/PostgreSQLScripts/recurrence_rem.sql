-- Stored procedure definition script Recurrence_Rem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Recurrence_Rem(v_DataKey	INTEGER,	-- identifies the root activity
	v_RecurrenceKey	INTEGER		-- identfieis the recurrence
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_VarianceKey  UUID;
BEGIN
   -- BEGIN TRANSACTION


	

	-- Get the variance correlation ID, if any, off of the Root Activity
	-- before striking it out.
	--
	select   Activity.VarianceID INTO v_VarianceKey FROM
   Activity WHERE
   Activity.ActivityID = v_DataKey;

	-- Detach the Root Activity from the recurring series and clear-out any
	-- variance correlation ID so it isn't deleted when deleting variances.
	--
   UPDATE
   Activity
   SET
   RecurrenceID = -999,VarianceID   = NULL
   WHERE
   Activity.ActivityID = v_DataKey;

	-- Delete all variance, if any, rows in the Activity table related to
	-- the Recurrence.
	--
   IF (NOT v_VarianceKey IS NULL) then
	
      DELETE FROM Activity
      WHERE (Activity.VarianceID = v_VarianceKey  AND
      Activity.RecurrenceID = v_RecurrenceKey);
   end if;

	-- Now that all child table references to the Recurrence have been
	-- updated or deleted, eliminate the Recurrence.
	--
   DELETE FROM Recurrence
   WHERE  Recurrence.recurrenceid = v_RecurrenceKey;

   -- COMMIT
RETURN;
END; $$;


