-- Stored procedure definition script USP_GetWareHouseOptionsAcc for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWareHouseOptionsAcc(v_numItemCode NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcItemName as "vcItemName",numItemCode as "numItemCode" from  Item
   where numItemCode in (select numOppAccAttrID from  ItemGroupsDTL
      join Item on Item.numItemGroup = numItemGroupID where
      numItemCode = v_numItemCode and tintType = 1);
END; $$;












