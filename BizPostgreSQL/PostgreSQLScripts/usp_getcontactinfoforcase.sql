-- Stored procedure definition script usp_GetContactInfoForCase for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactInfoForCase(v_numDomainID NUMERIC(9,0) DEFAULT 0   
--				
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numContactId,vcFirstName,numCreatedBy,vcLastname
   FROM
   AdditionalContactsInformation
   WHERE
   numDomainID = v_numDomainID
   AND numCreatedBy IS NOT NULL;
END; $$;












