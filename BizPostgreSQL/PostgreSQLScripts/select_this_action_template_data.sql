-- Stored procedure definition script Select_This_Action_Template_Data for PostgreSQL
CREATE OR REPLACE FUNCTION Select_This_Action_Template_Data(v_rowID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select TemplateName , DueDays , Priority , Type , Activity , cast(coalesce(Comments,'') as VARCHAR(2000)) as Comments,cast(coalesce(bitSendEmailTemplate,false) as BOOLEAN) AS bitSendEmailTemp,cast(coalesce(numEmailTemplate,0) as NUMERIC(18,0)) AS numEmailTemplate,cast(coalesce(tintHours,0) as SMALLINT) AS tintHours,cast(coalesce(bitAlert,false) as BOOLEAN) AS bitAlert,
cast(coalesce(numTaskType,0) as NUMERIC(18,0)) as numTaskType,cast(coalesce(bitRemind,false) as BOOLEAN) AS bitRemind,cast(coalesce(numRemindBeforeMinutes,0) as INTEGER) AS numRemindBeforeMinutes
   From tblActionItemData Where RowID = v_rowID;
END; $$;













