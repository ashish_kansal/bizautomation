-- Stored procedure definition script usp_GetContractHrsAmount for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContractHrsAmount(v_numContractID  BIGINT,                                                                          
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hrs  DECIMAL(10,2);
   v_Amount  DECIMAL(10,2);
BEGIN
   v_Amount := 0; 
   v_hrs := 0; 

   if v_numContractID <> 0 then
 
      select   case when numCategory = 2 then coalesce(monAmount,0) else 0 end, case when numCategory = 1 then CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 else 0 end INTO v_Amount,v_hrs from timeandexpense where numDomainID = v_numDomainID and numCategoryHDRID = v_numCategoryHDRID;
   end if;


  
  
   open SWV_RefCur for select
   bitAmount,
bitHour,
bitDays,
bitIncidents ,
(v_hrs+getContractRemainingHrsAmt(0::BOOLEAN,v_numContractID::NUMERIC)) as RemHours,
(v_Amount+getContractRemainingHrsAmt(1::BOOLEAN,v_numContractID::NUMERIC)) as RemAmount ,
decrate
   from ContractManagement c
   where  c.numdomainId = v_numDomainID and c.numContractId = v_numContractID;
END; $$;












