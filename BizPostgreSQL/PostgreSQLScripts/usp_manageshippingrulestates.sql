-- Stored procedure definition script USP_ManageShippingRuleStates for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingRuleStates(v_numCountryID NUMERIC,
    v_numStateID NUMERIC,
    v_numRuleID NUMERIC,
    v_numDomainID NUMERIC,
	v_vcZipPostalRange VARCHAR(200))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
BEGIN
   select   numRelationship, numProfile INTO v_numRelationship,v_numProfile FROM
   ShippingRules WHERE
   numDomainId = v_numDomainID
   AND numRuleID = v_numRuleID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numDomainID NUMERIC(18,0),
      numRuleID NUMERIC(18,0),
      numCountryID NUMERIC(18,0),
      numStateID NUMERIC(18,0),
      vcZipPostal VARCHAR(20)
   );

   IF LENGTH(coalesce(v_vcZipPostalRange,'')) > 0 then
	
      INSERT INTO tt_TEMP(numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal)
      SELECT
      v_numDomainID
			,v_numRuleID
			,v_numCountryID
			,v_numStateID
			,OutParam
      FROM
      SplitString(v_vcZipPostalRange,',');
   ELSE
      INSERT INTO tt_TEMP(numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal)
      SELECT
      v_numDomainID
			,v_numRuleID
			,v_numCountryID
			,v_numStateID
			,CAST('' AS VARCHAR(20));
   end if;

   IF(SELECT
   COUNT(*)
   FROM
   ShippingRules SR
   WHERE
   numDomainId = v_numDomainID
   AND numRuleID <> coalesce(v_numRuleID,0)
   AND coalesce(numRelationship,0) = coalesce(v_numRelationship,0)
   AND coalesce(numProfile,0) = coalesce(v_numProfile,0)
   AND EXISTS(SELECT
      SRSL1.numRuleID
      FROM
      ShippingRuleStateList SRSL1
      INNER JOIN
      tt_TEMP SRSL2
      ON
      coalesce(SRSL1.numStateID,0) = coalesce(SRSL2.numStateID,0)
      AND coalesce(SRSL1.vcZipPostal,'') = coalesce(SRSL2.vcZipPostal,'')
      AND coalesce(SRSL1.numCountryID,0) = coalesce(SRSL2.numCountryID,0)
      WHERE
      SRSL1.numDomainID = v_numDomainID
      AND SRSL1.numRuleID = SR.numRuleID
      AND SRSL2.numRuleID = v_numRuleID)) > 0 then
	
      RAISE NOTICE 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP';
      RETURN;
   end if;

   INSERT INTO ShippingRuleStateList(numCountryID,
		numStateID,
		numRuleID,
		numDomainID,
		vcZipPostal)
   SELECT
   T1.numCountryID
		,T1.numStateID
		,T1.numRuleID
		,T1.numDomainID
		,T1.vcZipPostal
   FROM
   tt_TEMP T1
   LEFT JOIN
   ShippingRuleStateList SRSL
   ON
   SRSL.numRuleID = v_numRuleID
   AND T1.numRuleID = SRSL.numRuleID
   AND T1.numCountryID = SRSL.numCountryID
   AND T1.numStateID = SRSL.numStateID
   AND coalesce(T1.vcZipPostal,'') = coalesce(SRSL.vcZipPostal,'')
   WHERE
   SRSL.numRuleStateID IS NULL;
END; $$;




