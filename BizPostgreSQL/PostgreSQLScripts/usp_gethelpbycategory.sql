-- Stored procedure definition script Usp_GetHelpByCategory for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetHelpByCategory(v_helpcategory NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT ROW_NUMBER() OVER(ORDER BY intDisplayOrder ASC) AS row,
				c.numHelpCategoryID,
                c.vcCatecoryName,
                c.bitLinkPage,
                c.numPageID,
                c.numParentCatID,
                c.tintLevel,
                h.numHelpID,
                h.helpCategory,
                h.helpHeader,
                h.helpMetatag,
                h.helpDescription,
                h.helpPageUrl,
                h.helpshortdesc,
                h.helpLinkingPageURL,
                coalesce(h.intDisplayOrder,0) AS intDisplayOrder,
                c.vcCatecoryName
   FROM    HelpMaster h
   LEFT OUTER JOIN HelpCategories c ON h.helpCategory = c.numHelpCategoryID
   WHERE   helpcategory = v_helpcategory
   OR v_helpcategory = 0;
END; $$;      

