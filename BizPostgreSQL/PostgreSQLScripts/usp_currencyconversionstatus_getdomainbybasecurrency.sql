-- Stored procedure definition script USP_CurrencyConversionStatus_GetDomainByBaseCurrency for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CurrencyConversionStatus_GetDomainByBaseCurrency(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for SELECT
		Currency.chrCurrency,
		COALESCE((SELECT string_agg(CAST(a.numDomainId AS VARCHAR),',' ORDER BY a.numDOmainID) FROM Domain a INNER JOIN Currency b ON a.numCurrencyID = b.numCurrencyID WHERE a.numDomainId > 0 AND b.chrCurrency = Currency.chrCurrency),'') AS vcDomainIDs,
		COALESCE((SELECT DISTINCT string_agg(CONCAT(a.chrCurrency,Currency.chrCurrency),',') FROM Currency a WHERE bitEnabled = true AND a.chrCurrency <> Currency.chrCurrency),'') AS vcCurrencyCodes
	FROM
		Domain
	INNER JOIN
		Currency
	ON
		Domain.numCurrencyID = Currency.numCurrencyID
	GROUP BY
		Currency.chrCurrency;
END; $$;

