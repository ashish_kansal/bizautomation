-- Stored procedure definition script GetVarianceKeyByItemId for PostgreSQL
CREATE OR REPLACE FUNCTION GetVarianceKeyByItemId(v_ItemId	VARCHAR(250)	-- ItemId of the root Activity
	
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   Activity.VarianceID,
		Activity.RecurrenceID
   FROM
   Activity
   WHERE
   Activity.itemId = v_ItemId  AND
		  (Activity.OriginalStartDateTimeUtc IS NULL);
END; $$;













