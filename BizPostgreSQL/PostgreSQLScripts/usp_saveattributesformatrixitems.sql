-- Stored procedure definition script USP_SaveAttributesForMatrixItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Object:  StoredProcedure [dbo].[USP_SaveGridColumnWidth]    Script Date: 07/26/2008 16:14:45 ******/

--05052018 Change: Procedure Created to update Attrbutes for Matrix Items
CREATE OR REPLACE FUNCTION USP_SaveAttributesForMatrixItems(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	INOUT v_numWareHouseItemID NUMERIC(18,0) ,
	v_numItemCode NUMERIC(18,0),
	v_monWListPrice DECIMAL(20,5),
	v_vcWHSKU VARCHAR(100),
	v_vcBarCode VARCHAR(100),
	v_strFieldList TEXT)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitMatrix  BOOLEAN DEFAULT 0;
   v_numItemGroup  NUMERIC(18,0);
   v_vcDescription  VARCHAR(100);
   v_numGlobalWarehouseItemID  NUMERIC(18,0) DEFAULT 0;
   v_monPrice  DECIMAL(20,5);
   v_vcSKU  VARCHAR(200) DEFAULT '';
   v_vcUPC  VARCHAR(200) DEFAULT '';
   v_bitRemoveGlobalLocation  BOOLEAN;

   v_dtDATE  TIMESTAMP DEFAULT TIMEZONE('UTC',now());

  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   coalesce(Item.numItemGroup,0), coalesce(Item.monListPrice,0), coalesce(bitMatrix,false), coalesce(vcSKU,''), coalesce(numBarCodeId,'') INTO v_numItemGroup,v_monPrice,v_bitMatrix,v_vcSKU,v_vcUPC FROM
      Item
      INNER JOIN
      Domain
      ON
      Item.numDomainID = Domain.numDomainId WHERE
      numItemCode = v_numItemCode
      AND Item.numDomainID = v_numDomainID;
      BEGIN --UPDATE
         UPDATE
         WareHouseItems
         SET
         vcWHSKU =(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = true THEN v_vcSKU ELSE v_vcWHSKU END),vcBarCode =(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = true THEN v_vcUPC ELSE v_vcBarCode END),
         dtModified = LOCALTIMESTAMP
         WHERE
         numItemID = v_numItemCode
         AND numDomainID = v_numDomainID
         AND numWareHouseItemID = v_numWareHouseItemID;


		 --WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
         IF v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = false then
		
            UPDATE WareHouseItems SET monWListPrice = v_monWListPrice WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numWareHouseItemID;
         end if;
         v_vcDescription := 'UPDATE WareHouse';
      END;

	-- SAVE MATRIX ITEM ATTRIBUTES
      IF OCTET_LENGTH(v_strFieldList) > 2 then
	
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
         BEGIN
            CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            Fld_ID NUMERIC(18,0),
            Fld_Value VARCHAR(300)
         );
         INSERT INTO tt_TEMPTABLE(Fld_ID,
			Fld_Value)
         SELECT Fld_ID,Fld_Value FROM 
		 XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strFieldList AS XML)
				COLUMNS
					id FOR ORDINALITY,
					Fld_ID NUMERIC(18,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(300) PATH 'Fld_Value'
			) AS X;
         IF(SELECT COUNT(*) FROM tt_TEMPTABLE) > 0 then
		
            DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID AND C.RecId = v_numWareHouseItemID;
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized)
            SELECT
            Fld_ID,
				coalesce(Fld_Value,'') AS Fld_Value,
				v_numWareHouseItemID,
				false
            FROM
            tt_TEMPTABLE;
            IF coalesce(v_bitMatrix,false) = true then
			
               DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID AND C.RecId = v_numGlobalWarehouseItemID;
               INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized)
               SELECT
               Fld_ID,
					coalesce(Fld_Value,'') AS Fld_Value,
					v_numGlobalWarehouseItemID,
					false
               FROM
               tt_TEMPTABLE;
            end if;
         end if;
   
      end if;
      PERFORM FROM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
      v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,v_dtRecordDate := v_dtDATE,
      v_numDomainID := v_numDomainID,SWV_RefCur := null);
      IF coalesce(v_numGlobalWarehouseItemID,0) > 0 then
	
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numGlobalWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
         v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,
         v_dtRecordDate := v_dtDATE,v_numDomainID := v_numDomainID,SWV_RefCur := null);
      end if;
      UPDATE Item SET bintModifiedDate = v_dtDATE,numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_numWareHouseItemID := 0;
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;




