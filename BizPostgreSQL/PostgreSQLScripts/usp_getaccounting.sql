-- Stored procedure definition script usp_GetAccounting for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAccounting(v_vcDomainName VARCHAR(30),
	v_numCompanyID NUMERIC(9,0)   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select QA.* from CompanyInfo CI,QBAccounting QA,Domain D where
   CI.vcCompanyName = QA.vcCompanyName and D.numDomainId = CI.numDomainID
   and D.vcDomainName = v_vcDomainName and CI.numCompanyId = v_numCompanyID
   and QA.numLastUpdateDate in(SELECT Max(numLastUpdateDate) from QBAccounting where vcCompanyName = CI.vcCompanyName)
   order by QA.numLastUpdateDate desc;
END; $$;












