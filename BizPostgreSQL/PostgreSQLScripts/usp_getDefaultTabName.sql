CREATE OR REPLACE FUNCTION usp_getDefaultTabName(v_numDomainId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $BODY$
BEGIN
   if(select count(*) from DefaultTabs where numDomainID = v_numDomainId) > 0 then

      open SWV_RefCur for
      select
      numDomainID,
	 vcContact,
	 vcProspect,
	 vcAccount  ,
	 vcLead,
	 vcContactPlural,
	 vcProspectPlural,
	 vcAccountPlural,
	 vcLeadPlural
      from DefaultTabs
      where numDomainID = v_numDomainId;
   else
      open SWV_RefCur for
      select
      v_numDomainId as numDomainId,
	'Contact' as vcContact,
	'Prospect' as vcProspect,
	'Account' as vcAccount  ,
	'Lead' as vcLead,
	'Contacts' as  vcContactPlural,
	'Prospects' as vcProspectPlural,
	'Accounts' as vcAccountPlural,
	'Leads' as vcLeadPlural;
   end if;
   RETURN;
END; 
$BODY$;


