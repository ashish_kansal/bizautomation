-- Function definition script fn_OppStageStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_OppStageStatus(v_numOppID NUMERIC, v_vcReturnData VARCHAR(20))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppStatusReturnEntity NUMERIC;
BEGIN
   IF v_numOppID > 0 then
        
      select   numStage INTO v_OppStatusReturnEntity FROM OpportunityStageDetails WHERE numoppid = coalesce(v_numOppID,0) AND bitstagecompleted = true ORDER BY numStagePercentage,numOppStageId DESC LIMIT 1;
      IF v_vcReturnData = 'numStatusName' then
         v_OppStatusReturnEntity := fn_AdvSearchColumnName(coalesce(v_OppStatusReturnEntity,0),'L');
      end if;
   ELSE
      v_OppStatusReturnEntity := '';
   end if;

   RETURN COALESCE(v_OppStatusReturnEntity,'');
END; $$;

