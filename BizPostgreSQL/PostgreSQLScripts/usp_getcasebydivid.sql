CREATE OR REPLACE FUNCTION USP_GetCasebyDivId(v_numdivisionid NUMERIC(9,0)  ,  
v_byteMode BOOLEAN,  
v_numDomainId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = true then

      open SWV_RefCur for
      select vcCompanyName || ' - ' || fn_GetContactName(Cases.numContactId) || ' - ' || vcCaseNumber as vcCaseNumber,numCaseId from Cases
      join DivisionMaster dm on    Cases.numDivisionID =  dm.numDivisionID
      join CompanyInfo comp on comp.numCompanyId =  dm.numCompanyID
      where Cases.numDivisionID = v_numdivisionid  and Cases.numDomainID = v_numDomainId;
   end if;  
  
   if v_byteMode = false then

      open SWV_RefCur for
      select vcCompanyName || ' - ' || fn_GetContactName(Cases.numContactId) || ' - ' || vcCaseNumber as vcCaseNumber,numCaseId from Cases
      join DivisionMaster dm on    Cases.numDivisionID =  dm.numDivisionID
      join CompanyInfo comp on comp.numCompanyId =  dm.numCompanyID
      where Cases.numDivisionID = v_numdivisionid  and Cases.numDomainID = v_numDomainId and numStatus <> 136;
   end if;
   RETURN;
END; $$;


