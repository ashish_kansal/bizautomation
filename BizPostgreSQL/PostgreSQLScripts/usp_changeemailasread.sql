-- Stored procedure definition script USP_ChangeEmailAsRead for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ChangeEmailAsRead(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_strXml TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	update 
		EmailHistory 
	set 
		bitIsRead = true
	from
	XMLTABLE
	(
		'NewDataSet/EmailHistory'
		PASSING 
			CAST(v_strXml AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numEmailHstrID NUMERIC(9,0) PATH 'numEmailHstrID'
	) AS X 
	where EmailHistory.numEmailHstrID = X.numEmailHstrID AND numDomainID = v_numDomainID;


   RETURN;
END; $$;


