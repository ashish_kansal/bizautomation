-- Stored procedure definition script USP_ProjectManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectManage(INOUT v_numProId NUMERIC ,
	v_numDivisionId NUMERIC(9,0) DEFAULT null,
	v_numIntPrjMgr NUMERIC(9,0) DEFAULT null,
	v_numOppId NUMERIC(9,0) DEFAULT null,
	v_numCustPrjMgr NUMERIC(9,0) DEFAULT null,
	INOUT v_vcPProName VARCHAR(100) DEFAULT '' ,
	v_Comments VARCHAR(1000) DEFAULT '',
	v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	v_numAssignedTo NUMERIC(9,0) DEFAULT 0,
	v_numDomainId NUMERIC(9,0) DEFAULT 0,
	v_dtDueDate TIMESTAMP DEFAULT NULL,
	v_numContractId NUMERIC(9,0) DEFAULT 0,
	v_vcProjectName VARCHAR(100) DEFAULT NULL,
	v_numProjectType NUMERIC(9,0) DEFAULT NULL,
	v_numProjectStatus NUMERIC(9,0) DEFAULT NULL,
	v_dtmStartDate TIMESTAMP DEFAULT NULL,
	v_dtmEndDate TIMESTAMP DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalAmount  INTEGER;
   v_intProTcode  NUMERIC(9,0);
   v_tintPageID  SMALLINT;
   v_update  BOOLEAN;
   v_tempAssignedTo  NUMERIC(9,0);
   v_numOldProjectStatus  NUMERIC;	                        
                       
   v_numPrimaryAddressID  NUMERIC(18,0);
   v_numAddressID  NUMERIC(18,0);
   v_Usedincidents  NUMERIC(9,0);
   v_Availableincidents  NUMERIC(9,0);
BEGIN
   IF EXTRACT(YEAR FROM v_dtDueDate) = 1753 then 
      v_dtDueDate := null;
   end if;
                     
                        
   if v_numProId = 0 then
      select   max(numProId) INTO v_intProTcode from ProjectsMaster;
      v_intProTcode := coalesce(v_intProTcode,0)+1;
      v_vcPProName := coalesce(v_vcPProName,'') || '-' || CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS VARCHAR(4))  || '-A' || SUBSTR(CAST(v_intProTcode AS VARCHAR(10)),1,10);
      insert into ProjectsMaster(vcProjectID,
  numIntPrjMgr,
  numOppId,
  numCustPrjMgr,
  numDivisionId,
  txtComments,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numdomainId,
  numRecOwner,
  intDueDate,
  numAssignedby,
  numAssignedTo ,
  numcontractId,
  vcProjectName,
  numProjectType,numProjectStatus,dtmStartDate,dtmEndDate)
 Values(v_vcPProName,
  v_numIntPrjMgr,
  v_numOppId,
  v_numCustPrjMgr,
  v_numDivisionId,
  v_Comments,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numDomainId,
  v_numUserCntID,
  v_dtDueDate,
  0,
  0 ,
 v_numContractId,
 v_vcProjectName,
 v_numProjectType,v_numProjectStatus,v_dtmStartDate,v_dtmEndDate) RETURNING numProId INTO v_numProId;
 
  
      v_tintPageID := 11;
      PERFORM USP_AddParentChildCustomFieldMap(v_numDomainId := v_numDomainId,v_numRecordID := v_numProId,v_numParentRecId := v_numDivisionId,
      v_tintPageID := v_tintPageID::SMALLINT); --  tinyint

      PERFORM usp_ProjectDefaultAssociateContacts(v_numProId := v_numProId,v_numDomainId := v_numDomainId);
      insert into ProjectsStageDetails(numProId,
  numstagepercentage,
  vcstagedetail,
  vcComments,
  numDomainID,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numAssignTo,
  bintStageComDate,
  bintDueDate,
  bitAlert,
  bitStageCompleted)
  values(v_numProId,
  100,
  'Project Completed',
  '',
  v_numDomainId,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  0,
  null,
  null,
  false,
  false--@DealCompleted
  );
  
      insert into ProjectsStageDetails(numProId,
  numstagepercentage,
  vcstagedetail,
  vcComments,
  numDomainID,
  numCreatedBy,
  bintCreatedDate,
  numModifiedBy,
  bintModifiedDate,
  numAssignTo,
  bintStageComDate,
  bintDueDate,
  bitAlert,
  bitStageCompleted)
  values(v_numProId,
  0,
  'Project Lost',
  '',
 v_numDomainId,
  v_numUserCntID,
  TIMEZONE('UTC',now()),
  v_numUserCntID,
  TIMEZONE('UTC',now()),
   0,
   null,
  null,
  false,
  false);
   else                        
		-- Checking For Incidents
		
      v_update := false;
      if v_numContractId <> 0 then
		
         IF(SELECT bitIncidents FROM ContractManagement WHERE numdomainId = v_numDomainId  and numContractId = v_numContractId) = 1 then
            v_Usedincidents := 0;
            select   v_Usedincidents+count(*) INTO v_Usedincidents from Cases where numContractID = v_numContractId and numDomainID = v_numDomainId;
            select   v_Usedincidents+count(*) INTO v_Usedincidents from ProjectsMaster where numcontractId = v_numContractId and numdomainId = v_numDomainId and numProId <> v_numProId;
            select   coalesce(numincidents,0) INTO v_Availableincidents from ContractManagement where numdomainId = v_numDomainId  and numContractId = v_numContractId;
            RAISE NOTICE '@Usedincidents-----';
            RAISE NOTICE '%',v_Usedincidents;
            RAISE NOTICE '%',v_Availableincidents;
            if v_Usedincidents >= v_Availableincidents then
               v_update := true;
            end if;
         end if;
      end if;
      if v_update = false then
         select   coalesce(numProjectStatus,0) INTO v_numOldProjectStatus FROM ProjectsMaster where numProId = v_numProId;
         update ProjectsMaster
         set
         vcProjectID = v_vcPProName,numIntPrjMgr = v_numIntPrjMgr,numOppId = v_numOppId,
         numCustPrjMgr = v_numCustPrjMgr,txtComments = v_Comments,numModifiedBy = v_numUserCntID,
         bintModifiedDate = TIMEZONE('UTC',now()),intDueDate = v_dtDueDate,
         numcontractId = v_numContractId,vcProjectName = v_vcProjectName,numProjectType = v_numProjectType,
         numProjectStatus = v_numProjectStatus,dtmStartDate = v_dtmStartDate,
         dtmEndDate = v_dtmEndDate
         where numProId = v_numProId;
         IF v_numOldProjectStatus != v_numProjectStatus then
		
            IF v_numProjectStatus = 27492 then  ---Updating All Statge to 100-- if Completed
			
               UPDATE ProjectsMaster PM SET dtCompletionDate = TIMEZONE('UTC',now()),monTotalExpense = PIE.monExpense,
               monTotalIncome = PIE.monIncome,monTotalGrossProfit = PIE.monBalance
               FROM GetProjectIncomeExpense(v_numDomainId,v_numProId) PIE
               WHERE PM.numProId = v_numProId;
               PERFORM USP_ManageProjectCommission(v_numDomainId,v_numProId);
					 --  numeric(9, 0)
							 --  numeric(9, 0)
               Update StagePercentageDetails set tinProgressPercentage = 100,bitClose = true where numProjectid = v_numProId;
               PERFORM USP_GetProjectTotalProgress(v_numDomainId := v_numDomainId,v_numProId := v_numProId,v_tintMode := 1::SMALLINT);
            ELSE
               DELETE from BizDocComission WHERE numDomainId = v_numDomainId AND numProId = v_numProId AND numComissionID
               NOT IN(SELECT BC.numComissionID FROM BizDocComission BC JOIN PayrollTracking PT
               ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainID WHERE BC.numDomainId = v_numDomainId AND coalesce(BC.numProId,0) = v_numProId);
            end if;
         end if;
      else
         v_numProId := 0;
      end if;
      RAISE NOTICE '@numProId : %',SUBSTR(CAST(v_numProId AS VARCHAR(10)),1,10);
      IF coalesce(v_numProId,0) > 0 then
         select   coalesce(numAddressID,0) INTO v_numPrimaryAddressID FROM AddressDetails WHERE numRecordID = v_numDivisionId  AND coalesce(bitIsPrimary,false) = true;
         RAISE NOTICE '@numPrimaryAddressID : %',SUBSTR(CAST(v_numPrimaryAddressID AS VARCHAR(10)),1,10);
         INSERT INTO AddressDetails(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID)
         SELECT vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,4,tintAddressType,v_numProId ,numDomainID
         FROM AddressDetails
         WHERE numAddressID = v_numPrimaryAddressID RETURNING numAddressID INTO v_numAddressID;
		
         RAISE NOTICE '@numAddressID : %',SUBSTR(CAST(v_numAddressID AS VARCHAR(10)),1,10);
         UPDATE ProjectsMaster SET numAddressID = v_numAddressID 	WHERE numProId = v_numProId;
      end if; 
	                     
    ---Updating if Project is assigned to someone                      
      v_tempAssignedTo := null;
      select   coalesce(numAssignedTo,0) INTO v_tempAssignedTo from ProjectsMaster where numProId = v_numProId;
      RAISE NOTICE '%',v_tempAssignedTo;
      if (v_tempAssignedTo <> v_numAssignedTo and  v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
  
         update ProjectsMaster set numAssignedTo = v_numAssignedTo,numAssignedby = v_numUserCntID where numProId = v_numProId;
      ELSEIF (v_numAssignedTo = 0)
      then
  
         update ProjectsMaster set numAssignedTo = 0,numAssignedby = 0 where numProId = v_numProId;
      end if;
      v_vcPProName := 'erer';
   end if;
   RETURN;
END; $$;


