-- Stored procedure definition script usp_GetUserNameFromContacts for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetUserNameFromContacts(v_numDomainID NUMERIC,      
 v_numUserID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT aci.vcFirstName || ' ' || aci.vcLastname AS vcUserName
   FROM UserMaster UM INNER JOIN AdditionalContactsInformation aci ON aci.numContactId = UM.numUserDetailId
   WHERE UM.numDomainID = v_numDomainID
   AND UM.numUserId  = v_numUserID;
END; $$;












