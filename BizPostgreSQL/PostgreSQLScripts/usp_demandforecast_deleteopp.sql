-- Stored procedure definition script USP_DemandForecast_DeleteOpp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_DeleteOpp(v_vcOppID TEXT,
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;
   v_numOppID  NUMERIC(18,0);
   v_strMsg  VARCHAR(200);
BEGIN
   BEGIN
      -- BEGIN TRAN
DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         NUMBER INTEGER,
         numOppID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPTABLE(NUMBER,
			numOppID)
      SELECT
      ROW_NUMBER() OVER(ORDER BY Items) AS NUMBER,
		CAST(Items AS NUMERIC(18,0))
      FROM
      Split(v_vcOppID,',');
      select   COUNT(*) INTO v_Count FROM tt_TEMPTABLE;
      WHILE v_i <= v_Count LOOP
         select   numOppID INTO v_numOppID FROM tt_TEMPTABLE WHERE NUMBER = v_i;
         PERFORM USP_DeleteOppurtunity(v_numOppID := v_numOppID,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID);
         v_i := v_i::bigint+1;
      END LOOP;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         v_strMsg := SQLERRM;
         RAISE NOTICE '%',v_strMsg;
         -- ROLLBACK 
END;
   RETURN;
END; $$;



