-- Stored procedure definition script USP_ManageDocumentRepositary for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDocumentRepositary(v_tintMode SMALLINT,
    v_numGenericDocID NUMERIC,
    v_numUserCntID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then --CheckOut
        
      UPDATE  GenericDocuments
      SET     numLastCheckedOutBy = v_numUserCntID,dtCheckOutDate = TIMEZONE('UTC',now()),
      tintCheckOutStatus = 1,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
--                    intDocumentVersion = ISNULL(intDocumentVersion, 0) + 1
      WHERE   numGenericDocID = v_numGenericDocID
      AND numDomainId = v_numDomainID;
   end if;
        
   IF v_tintMode = 2 then --Check in 
        
      UPDATE  GenericDocuments
      SET
      dtCheckInDate = TIMEZONE('UTC',now()),intDocumentVersion = coalesce(intDocumentVersion,0)+1,
      tintCheckOutStatus = 0,numModifiedBy = v_numUserCntID,
      bintModifiedDate = TIMEZONE('UTC',now())
      WHERE   numGenericDocID = v_numGenericDocID
      AND numDomainId = v_numDomainID;
   end if;
   RETURN;
        

--SELECT * FROM dbo.GenericDocuments
END; $$;



