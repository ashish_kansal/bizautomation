-- Stored procedure definition script USP_GetTaskByStageDetailsId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTaskByStageDetailsId(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numStageDetailsId NUMERIC(18,0) DEFAULT 0,
v_numOppId NUMERIC(18,0) DEFAULT 0,   
v_numProjectId NUMERIC(18,0) DEFAULT 0,
v_vcSearchText TEXT DEFAULT '',
v_bitAlphabetical BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dynamicQuery  TEXT DEFAULT NULL;
BEGIN
   v_dynamicQuery := 'SELECT 
		ST.numTaskId AS "numTaskId",
		ST.vcTaskName AS "vcTaskName",
		ST.numHours AS "numHours",
		ST.numMinutes AS "numMinutes",
		ST.numAssignTo AS "numAssignTo",
		coalesce(AC.vcFirstName || '' '' || AC.vcLastName,''-'') As "vcContactName",
		coalesce(ST.bitDefaultTask,false) AS "bitDefaultTask",
		coalesce(ST.bitTaskClosed,false) AS "bitTaskClosed",
		coalesce(ST.bitSavedTask,false) AS "bitSavedTask",
		coalesce(ST.numParentTaskId,0) AS "numParentTaskId",
		coalesce(AU.vcFirstName || '' '' || AU.vcLastName,''-'') As "vcUpdatedByContactName",
		TO_CHAR(ST.dtmUpdatedOn,''dd-mm-yyyy'') || SUBSTR(TO_CHAR(ST.dtmUpdatedOn,''Mon dd yyyy hh:miAM''),length(TO_CHAR(ST.dtmUpdatedOn,''Mon dd yyyy hh:miAM'')) -8+1) As "dtmUpdatedOn",
		 TO_CHAR(PPD.dtmStartDate,''mm/dd/yyyy'') || SUBSTR(TO_CHAR(PPD.dtmStartDate,''Mon dd yyyy hh:miAM''),length(TO_CHAR(PPD.dtmStartDate,''Mon dd yyyy hh:miAM'')) -8+1) AS "StageStartDate",
		ST.numStageDetailsId AS "numStageDetailsId",
		AC.numTeam AS "numTeam",
		coalesce(S.numTeamId,0) AS "numTeamId",
		coalesce(ST.numOrder,0) AS "numOrder", 
		coalesce(S.numStageOrder,0) AS "numStageOrder",
		M.numStagePercentage AS "numStagePercentage",
		coalesce(S.intTaskType,0) AS "intTaskType"
	FROM
		StagePercentageDetailsTask AS ST
	LEFT JOIN 
		StagePercentageDetails As S
	ON
		ST.numStageDetailsId = S.numStageDetailsId
	LEFT JOIN
		StagePercentageMaster AS M
	ON
		M.numStagePercentageId = S.numStagePercentageId
	LEFT JOIN
		AdditionalContactsInformation AS AC
	ON
		ST.numAssignTo = AC.numContactId
	LEFT JOIN 
		AdditionalContactsInformation AS AU
	ON
		ST.numCreatedBy = AU.numContactId
	LEFT JOIN
		ProjectProcessStageDetails AS PPD
	ON
		ST.numStageDetailsId = PPD.numStageDetailsId AND ST.numOppId = PPD.numOppId
	WHERE
		ST.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || ' ';
   IF(v_numStageDetailsId > 0) then
	
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND ST.numStageDetailsId =' || SUBSTR(CAST(v_numStageDetailsId AS VARCHAR(100)),1,100) || ' ';
   end if;
   v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND ST.numOppId =' || SUBSTR(CAST(v_numOppId AS VARCHAR(100)),1,100) || ' ';
   v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND ST.numProjectId =' || SUBSTR(CAST(v_numProjectId AS VARCHAR(100)),1,100) || ' ';
	
   IF(LENGTH(v_vcSearchText) > 0) then
	
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' AND ST.vcTaskName ilike ''%' || SUBSTR(CAST(v_vcSearchText AS VARCHAR(100)),1,100) || '%'' ';
   end if;
   IF(v_bitAlphabetical = true) then
	
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' ORDER BY ST.vcTaskName ASC ';
   ELSE
      v_dynamicQuery := coalesce(v_dynamicQuery,'') || ' ORDER BY ST.numOrder ASC ';
   end if;
   OPEN SWV_RefCur FOR EXECUTE v_dynamicQuery;
   RETURN;
END; $$; 



