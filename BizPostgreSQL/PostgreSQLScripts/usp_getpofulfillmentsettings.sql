-- Stored procedure definition script USP_GetPOFulfillmentSettings for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPOFulfillmentSettings(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM PurchaseOrderFulfillmentSettings WHERE numDomainID = v_numDomainID;
END; $$;













