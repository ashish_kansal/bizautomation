-- Stored procedure definition script usp_GetCustRptOppItemCustomFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCustRptOppItemCustomFields(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT CASE Mst.Grp_id WHEN 1 THEN 1 WHEN 2 THEN 3 WHEN 4 THEN 2 ELSE 4 END AS numFieldGroupID, 'CFld' || CAST(Mst.Fld_id AS VARCHAR(30)) AS vcDbFieldName, cast(Mst.FLd_label as VARCHAR(255)) AS vcScrFieldName,
                      cast(Dtl.numOrder as VARCHAR(255)) AS numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection
   FROM         CFW_Fld_Master Mst LEFT OUTER JOIN
   CFW_Fld_Dtl Dtl ON Mst.Fld_id = Dtl.numFieldId AND Mst.numDomainID = v_numDomainId
   GROUP BY Mst.Grp_id,'CFld' || CAST(Mst.Fld_id AS VARCHAR(30)),Mst.FLd_label,Dtl.numOrder, 
   Mst.Fld_id
   HAVING      (Mst.Grp_id IN(1,2,4,5));  
 --1: Organization; 2: Opportunities; 4: Contacts; 5: Items    
END; $$;












