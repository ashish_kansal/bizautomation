-- Stored procedure definition script USP_HelpCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_HelpCategory(v_Search VARCHAR(500),
      v_intSearchType INTEGER,
      v_numHelpId INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intSearchType = 1 then
            
      open SWV_RefCur for
      SELECT  helpcategory,
                        helpheader,
                        numHelpId,
                        helpMetatag,
                        coalesce(C.vcCatecoryName,'') AS vcCatecoryName,
                        CASE WHEN C.bitLinkPage = true THEN coalesce(C.numPageID,0)
      ELSE 0 END  AS CatHelpID
      FROM    HelpMaster h
      LEFT OUTER JOIN HelpCategories C ON h.helpCategory = C.numHelpCategoryID
--                WHERE   helpMetatag LIKE +'--' + @Search + '--'
      WHERE helpcategory <> 0
      ORDER BY helpcategory,intDisplayOrder;
   ELSEIF v_intSearchType = 2
   then
                
      open SWV_RefCur for
      SELECT  helpcategory,
                            helpheader,
                            numHelpId,
                            helpMetatag,
                            coalesce(helpshortdesc,'') AS helpshortdesc,
                            coalesce(C.vcCatecoryName,'') AS vcCatecoryName
      FROM    HelpMaster h
      LEFT OUTER JOIN HelpCategories C ON h.helpCategory = C.numHelpCategoryID
      WHERE
      position(v_Search in helpDescription) > 0
      OR position(v_Search in helpheader) > 0
      OR position(v_Search in helpMetatag) > 0
--                            OR helpshortdesc LIKE +'--' + @Search + '--'
--                            OR helpMetatag LIKE +'--' + @Search + '--'
      ORDER BY helpheader;
   ELSEIF v_intSearchType = 3
   then
                    
      open SWV_RefCur for
      SELECT  coalesce(helpDescription,'') AS helpDescription,
                                coalesce(C.vcCatecoryName,'') AS vcCatecoryName
      FROM    HelpMaster h
      LEFT OUTER JOIN HelpCategories C ON h.helpCategory = C.numHelpCategoryID
      WHERE   numHelpId = v_numHelpId;
   ELSEIF v_intSearchType = 4
   then
            
      v_Search := '"' || coalesce(v_Search,'') || '"';
      open SWV_RefCur for
      SELECT coalesce(helpDescription,'') AS helpDescription,
                numhelpID
      FROM    HelpMaster h
      LEFT OUTER JOIN HelpCategories c ON h.helpCategory = c.numHelpCategoryID
      WHERE position(v_Search in helpLinkingPageURL) > 0
      ORDER BY helpcategory,intDisplayOrder;
   end if;
   RETURN;
END; $$;



