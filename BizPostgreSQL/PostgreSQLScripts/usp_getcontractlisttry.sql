-- Stored procedure definition script USP_GetContractListtry for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetContractListtry(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                      
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                                      
 v_tintSortOrder SMALLINT DEFAULT 4,                                                                      
 v_SortChar CHAR(1) DEFAULT '0',                                                                                                                        
 v_CurrentPage INTEGER DEFAULT NULL,                                                                    
v_PageSize INTEGER DEFAULT NULL,                                                                    
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                    
v_columnName VARCHAR(50) DEFAULT NULL,                                                                    
v_columnSortOrder VARCHAR(10) DEFAULT 'desc', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                            
   v_firstRec  INTEGER;                                                  
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numContractID VARCHAR(15)
   );  
                                                  
   v_strSql := 'select                               
c.numContractId             
from ContractManagement c                                  
join DivisionMaster d on c.numDivisionID = d.numDivisionID                
join CompanyInfo com on d.numCompanyID = com.numCompanyId                
where numUserCntID = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(100)),1,100) || ' and c.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || '
';
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcContractName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                           
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');   

   RAISE NOTICE '%',v_strSql;      
   EXECUTE 'insert into tt_TEMPTABLE(numContractID)                                                  
' || v_strSql;

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                  
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                   
   select count(*) INTO v_TotRecs from tt_TEMPTABLE; 

   open SWV_RefCur for
   select
   c.numContractId,
c.numDivisionID,
vcContractName,
case when bitdays = true then
      coalesce(CAST(case when DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) >= 0 then DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) else 0 end
      AS VARCHAR(100)),
      cast(0 as TEXT)) || ' of ' || coalesce(CAST(DATE_PART('day',bintExpDate:: timestamp -bintStartDate:: timestamp) AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Days,
case when bitincidents = true then
      coalesce(CAST(GetIncidents(c.numContractId,c.numDivisionID,v_numDomainID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || coalesce(CAST(numincidents AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Incidents,
case when bitamount = true then
      coalesce(CAST(GetContractRemainingAmount(c.numContractId,c.numDivisionID,v_numDomainID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(CAST(c.numAmount AS DECIMAL(10,2)) AS VARCHAR(100))
   else '-' end  as Amount,
case when bithour = true then
      coalesce(CAST(CAST(GetContractRemainingHours(c.numContractId,c.numDivisionID,v_numDomainID) AS DECIMAL(5,2)) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(c.numHours AS VARCHAR(100))
   else '-' end as Hours,
c.bintcreatedOn,
com.vcCompanyName
   from ContractManagement c
   join DivisionMaster d on c.numDivisionID = d.numDivisionID
   join CompanyInfo com on d.numCompanyID = com.numCompanyId
   join tt_TEMPTABLE T on T.numContractId = c.numContractId
   WHERE ID > v_firstRec and ID < v_lastRec
   order by ID;                                 
                                                
   RETURN;
END; $$;


