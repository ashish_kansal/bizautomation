-- Stored procedure definition script USP_CashFlowStatementList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CashFlowStatementList(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                        
 v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                          
 v_dtFromDate TIMESTAMP DEFAULT NULL,                                                  
 v_dtToDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select distinct CA.numAccountId as numAccountId,'' as AcntTypeDescription,
 fn_GetCurrentOpeningBalanceForCashFlowStatement(v_numChartAcntId,v_dtFromDate,v_dtToDate) As Amount,null as numAccountTypeId
   from Chart_Of_Accounts CA
   Left outer join Listdetails LD on CA.numacnttypeid = LD.numListItemID               
 --inner join General_Journal_Details GJD on CA.numAccountId=GJD.numChartAcntId                        
   Where  CA.numAccountId = v_numChartAcntId And CA.numDomainId = v_numDomainId;
END; $$;













