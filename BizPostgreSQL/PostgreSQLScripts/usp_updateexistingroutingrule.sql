-- Stored procedure definition script usp_UpdateExistingRoutingRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateExistingRoutingRule(v_numRoutID NUMERIC(9,0) DEFAULT 0,                  
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                  
 v_vcRoutName VARCHAR(50) DEFAULT '',  
 v_tintEqualTo SMALLINT DEFAULT 0,                                              
 v_numValue NUMERIC(9,0) DEFAULT 0,                                
 v_numUserCntID NUMERIC DEFAULT 0,                       
 v_tintPriority SMALLINT DEFAULT 0,  
 v_strValues TEXT DEFAULT NULL,  
 v_strDistribitionList TEXT DEFAULT NULL,
 v_vcDBCoulmnName VARCHAR(100) DEFAULT '',
 v_vcSelectedColumn VARCHAR(200) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update RoutingLeads
   set numDomainID = v_numDomainID,vcRoutName = v_vcRoutName,tintEqualTo = v_tintEqualTo,
   numValue = v_numValue,numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
   vcDbColumnName = v_vcDBCoulmnName,vcSelectedCoulmn = v_vcSelectedColumn,
   tintPriority = v_tintPriority
   where numRoutID = v_numRoutID;  
   delete from RoutinLeadsValues where numRoutID = v_numRoutID;  
   if SUBSTR(CAST(v_strValues AS VARCHAR(2)),1,2) <> '' then
      insert into RoutinLeadsValues(numRoutID,vcValue)
      VALUES (v_numRoutID,unnest(xpath('//vcValue/text()', v_strValues::xml))::text::VARCHAR);
   end if;  
   delete from RoutingLeadDetails where numRoutID = v_numRoutID;  
   if SUBSTR(CAST(v_strDistribitionList AS VARCHAR(2)),1,2) <> '' then
      insert into RoutingLeadDetails(numRoutID,numEmpId,intAssignOrder)
	  VALUES
      (v_numRoutID,unnest(xpath('//numEmpId/text()', v_strDistribitionList::xml))::text::NUMERIC,unnest(xpath('//intAssignOrder/text()', v_strDistribitionList::xml))::text::INTEGER);
   end if;
   RETURN;
END; $$;


