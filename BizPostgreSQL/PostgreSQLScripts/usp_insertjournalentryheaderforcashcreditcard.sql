-- Stored procedure definition script USP_InsertJournalEntryHeaderForCashCreditCard for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryHeaderForCashCreditCard(v_datEntry_Date TIMESTAMP,                    
v_numAmount DECIMAL(20,5),                  
v_numJournal_Id NUMERIC(9,0) DEFAULT 0,              
v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_numCashCreditCardId NUMERIC(9,0) DEFAULT 0,  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numJournal_Id = 0 then
		
      Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numCashCreditCardId,numCreatedBy,datCreatedDate) Values(v_datEntry_Date,v_numAmount,v_numDomainId,v_numCashCreditCardId,v_numUserCntID,TIMEZONE('UTC',now()));
			
      v_numJournal_Id := CURRVAL('General_Journal_Header_seq');
      open SWV_RefCur for Select  v_numJournal_Id;
   end if;
END; $$;












