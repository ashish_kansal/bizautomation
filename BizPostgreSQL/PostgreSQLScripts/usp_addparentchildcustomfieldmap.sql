-- Stored procedure definition script USP_AddParentChildCustomFieldMap for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_AddParentChildCustomFieldMap(v_numDomainID NUMERIC(18,0),
	v_numRecordID NUMERIC(18,0),
	v_numParentRecId NUMERIC(18,0),
	v_tintPageID SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_minNo  NUMERIC;
   v_maxNo  NUMERIC;
   v_tintParentModule  SMALLINT;
   v_numParentFieldID  NUMERIC;
   v_ParentFld_type  VARCHAR(20);
   v_Parentlistid  NUMERIC;
   v_tintChildModule  SMALLINT;
   v_numChildFieldID  NUMERIC;
   v_ChildFld_type  VARCHAR(20);
   v_Childlistid  NUMERIC;
   v_ParentRecId  NUMERIC;
   v_Fld_Value  VARCHAR(1000);
   v_vcData  VARCHAR(50);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 
   v_Fld_VlaueTemp  NUMERIC(18,0);
   v_Fld_ValueCheckBoxList  TEXT DEFAULT '';
BEGIN
   DROP TABLE IF EXISTS tt_TEMPAddParentChildCustomFieldMap CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAddParentChildCustomFieldMap AS
      SELECT
      tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PFM.fld_type AS ParentFld_type
		,coalesce(PFM.numlistid,0) AS Parentlistid
		,CFM.fld_type AS ChildFld_type
		,coalesce(CFM.numlistid,0) AS Childlistid
		,ROW_NUMBER() OVER(ORDER BY numParentFieldID) AS RowNO
	
      FROM
      ParentChildCustomFieldMap PCFM
      JOIN CFW_Loc_Master PM ON PCFM.tintParentModule = PM.Loc_id
      JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID = PFM.Fld_id AND PCFM.numDomainID = PFM.numDomainID AND PCFM.tintParentModule = PFM.Grp_id
      JOIN CFW_Loc_Master CM ON PCFM.tintChildModule = CM.Loc_id
      JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID = CFM.Fld_id AND PCFM.numDomainID = CFM.numDomainID AND PCFM.tintChildModule = CFM.Grp_id
      WHERE
      PCFM.numDomainID = v_numDomainID
      AND PCFM.tintChildModule = v_tintPageID
      AND PFM.fld_type = CFM.fld_type;

   select   MIN(RowNO), MAX(RowNO) INTO v_minNo,v_maxNo FROM tt_TEMPAddParentChildCustomFieldMap;

   WHILE v_minNo <= v_maxNo LOOP
      select   tintParentModule, numParentFieldID, ParentFld_type, Parentlistid, tintChildModule, numChildFieldID, ChildFld_type, Childlistid INTO v_tintParentModule,v_numParentFieldID,v_ParentFld_type,v_Parentlistid,
      v_tintChildModule,v_numChildFieldID,v_ChildFld_type,v_Childlistid FROM
      tt_TEMPAddParentChildCustomFieldMap WHERE
      RowNO = v_minNo;
      IF v_ParentFld_type = 'SelectBox' AND v_ChildFld_type = 'SelectBox' then
		
         v_Fld_Value := '';
         IF v_tintParentModule = 1 then --Organizations
			
            select   Fld_Value INTO v_Fld_Value FROM CFW_FLD_Values WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID;
         ELSEIF (v_tintParentModule = 6 OR v_tintParentModule = 2)
         then   --Sales Opportunities / Orders
			
            select   Fld_Value INTO v_Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID;
         end if;
         IF v_Parentlistid > 0 AND v_Childlistid > 0 AND v_Fld_Value <> '' then
			
            select   vcData INTO v_vcData FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Parentlistid AND CAST(numListItemID AS VARCHAR) = v_Fld_Value;
            v_Fld_Value := NULL;
            select   numListItemID INTO v_Fld_Value FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Childlistid AND LOWER(vcData) = LOWER(coalesce(v_vcData,''));
         ELSE
            v_Fld_Value := NULL;
         end if;
         IF v_Fld_Value IS NOT NULL then
			
            IF v_tintChildModule = 2 OR v_tintChildModule = 6 then --Sales Opportunities / Orders
				
               DELETE FROM CFW_Fld_Values_Opp WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_Fld_Values_Opp(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            ELSEIF v_tintChildModule = 3
            then --Case
				
               DELETE FROM CFW_FLD_Values_Case WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_FLD_Values_Case(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            ELSEIF v_tintChildModule = 11
            then --Projects
				
               DELETE FROM CFW_Fld_Values_Pro WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_Fld_Values_Pro(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            end if;
         end if;
      ELSEIF v_ParentFld_type = 'CheckBoxList' AND v_ChildFld_type = 'CheckBoxList'
      then
		
         IF v_Parentlistid > 0 AND v_Childlistid > 0 then
            BEGIN
               CREATE TEMP SEQUENCE tt_TEMPList_seq INCREMENT BY 1 START WITH 1;
               EXCEPTION WHEN OTHERS THEN
                  NULL;
            END;
            DROP TABLE IF EXISTS tt_TEMPList CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPList
            (
               ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
               ListItemID NUMERIC(18,0)
            );
            IF v_tintParentModule = 1 then --Organizations
				
               INSERT INTO tt_TEMPList(ListItemID)
               SELECT * FROM SplitIDs(coalesce((SELECT Fld_Value FROM CFW_FLD_Values WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID),''),',');
            ELSEIF (v_tintParentModule = 6 OR v_tintParentModule = 2)
            then --Sales Opportunities / Orders
				
               INSERT INTO tt_TEMPList(ListItemID)
               SELECT Id FROM SplitIDs(coalesce((SELECT Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID),''),',');
            end if;
            v_Fld_ValueCheckBoxList := NULL;
            select   COUNT(*) INTO v_iCount FROM tt_TEMPList;
            WHILE v_i <= v_iCount LOOP
               select   ListItemID INTO v_Fld_VlaueTemp FROM tt_TEMPList WHERE ID = v_i;
               select   vcData INTO v_vcData FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Parentlistid AND numListItemID = v_Fld_VlaueTemp;
               IF EXISTS(SELECT numListItemID FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Childlistid AND LOWER(vcData) = LOWER(coalesce(v_vcData,''))) then
					
                  v_Fld_ValueCheckBoxList := CONCAT(v_Fld_ValueCheckBoxList,(CASE WHEN LENGTH(coalesce(v_Fld_ValueCheckBoxList,'')) > 0 THEN CONCAT(',',(SELECT numListItemID FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Childlistid AND LOWER(vcData) = LOWER(coalesce(v_vcData,'')))) ELSE CAST((SELECT numListItemID FROM Listdetails WHERE numDomainid = v_numDomainID AND numListID = v_Childlistid AND LOWER(vcData) = LOWER(coalesce(v_vcData,''))) AS VARCHAR(30)) END));
               end if;
               v_i := v_i::bigint+1;
            END LOOP;
            IF v_Fld_ValueCheckBoxList IS NOT NULL then
				
               IF v_tintChildModule = 2 OR v_tintChildModule = 6 then --Sales Opportunities / Orders
					
                  DELETE FROM CFW_Fld_Values_Opp WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
                  INSERT INTO CFW_Fld_Values_Opp(Fld_ID,Fld_Value,RecId)
                  SELECT
                  v_numChildFieldID,v_Fld_ValueCheckBoxList,v_numRecordID;
               ELSEIF v_tintChildModule = 3
               then --Case
					
                  DELETE FROM CFW_FLD_Values_Case WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
                  INSERT INTO CFW_FLD_Values_Case(Fld_ID,Fld_Value,RecId)
                  SELECT
                  v_numChildFieldID,v_Fld_ValueCheckBoxList,v_numRecordID;
               ELSEIF v_tintChildModule = 11
               then --Projects
					
                  DELETE FROM CFW_Fld_Values_Pro WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
                  INSERT INTO CFW_Fld_Values_Pro(Fld_ID,Fld_Value,RecId)
                  SELECT
                  v_numChildFieldID,v_Fld_ValueCheckBoxList,v_numRecordID;
               end if;
            end if;
         end if;
      ELSE
         IF v_tintParentModule = 1 then --Organizations
			
            select   Fld_Value INTO v_Fld_Value FROM CFW_FLD_Values WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID;
         ELSEIF (v_tintParentModule = 6 OR v_tintParentModule = 2)
         then --Sales Opportunities / Orders
			
            select   Fld_Value INTO v_Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId = v_numParentRecId AND Fld_ID = v_numParentFieldID;
         end if;
         IF v_Fld_Value IS NOT NULL then
			
            IF v_tintChildModule = 2 OR v_tintChildModule = 6 then --Sales Opportunities / Orders
				
               DELETE FROM CFW_Fld_Values_Opp WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_Fld_Values_Opp(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            ELSEIF v_tintChildModule = 3
            then --Case
				
               DELETE FROM CFW_FLD_Values_Case WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_FLD_Values_Case(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            ELSEIF v_tintChildModule = 11
            then --Projects
				
               DELETE FROM CFW_Fld_Values_Pro WHERE RecId = v_numRecordID AND Fld_ID = v_numChildFieldID;
               INSERT INTO CFW_Fld_Values_Pro(Fld_ID,Fld_Value,RecId)
               SELECT
               v_numChildFieldID,v_Fld_Value,v_numRecordID;
            end if;
         end if;
      end if;
      v_minNo := v_minNo+1;
   END LOOP;
   RETURN;
END; $$;


