-- Stored procedure definition script USP_GetGroups for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetGroups(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numGrpId as VARCHAR(255)), cast(vcGrpName as VARCHAR(255)) FROM Groups
   ORDER BY vcGrpName;
END; $$;












