DROP FUNCTION IF EXISTS USP_TicklerActItemsV2;

CREATE OR REPLACE FUNCTION USP_TicklerActItemsV2(v_numUserCntID NUMERIC(9,0) DEFAULT null,                                                                        
v_numDomainID NUMERIC(9,0) DEFAULT null,                                                                        
v_startDate TIMESTAMP DEFAULT NULL,                                                                        
v_endDate TIMESTAMP DEFAULT NULL,                                                                    
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Debasish to enable calculation of date according to client machine             
v_bitFilterRecord BOOLEAN DEFAULT false,
v_columnName VARCHAR(50) DEFAULT NULL, 
v_columnSortOrder VARCHAR(50) DEFAULT NULL,
v_vcBProcessValue VARCHAR(500) DEFAULT NULL,
v_RegularSearchCriteria TEXT DEFAULT '',
v_CustomSearchCriteria TEXT DEFAULT '', 
v_OppStatus INTEGER DEFAULT NULL,
v_CurrentPage INTEGER DEFAULT NULL,                                                              
v_PageSize INTEGER DEFAULT NULL, 
v_tintFilterBy SMALLINT DEFAULT NULL,
v_vcFilterValues TEXT DEFAULT NULL,
INOUT SWV_RefCur refcursor default null, 
INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_tintActionItemsViewRights  SMALLINT DEFAULT 3;
   v_bitREQPOApproval  BOOLEAN;
   v_bitARInvoiceDue  BOOLEAN;
   v_bitAPBillsDue  BOOLEAN;
   v_vchARInvoiceDue  VARCHAR(500);
   v_vchAPBillsDue  VARCHAR(500);
   v_decReqPOMinValue  DECIMAL(18,2);
   v_vchREQPOApprovalEmp  VARCHAR(500) DEFAULT '';
   v_numDomainDivisionID  NUMERIC(18,0);
   v_dynamicPartQuery  TEXT DEFAULT '';
   v_ActivityRegularSearch  TEXT DEFAULT '';
   v_OtherRegularSearch  TEXT DEFAULT '';
   v_IndivRecordPaging  TEXT DEFAULT '';
--SET @IndivRecordPaging=' '
   v_TotalRecordCount  INTEGER DEFAULT 0;
   v_dynamicQuery  TEXT DEFAULT '';
   v_dynamicQueryCount  TEXT DEFAULT '';

   v_strSql  TEXT;                                                            
   v_strSql1  TEXT;                         
   v_strSql2  TEXT;                                                            
   v_vcCustomColumnName  VARCHAR(8000);
   v_vcnullCustomColumnName  VARCHAR(8000);
   -------Change Row Color-------
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_tintOrder  SMALLINT;                                                
   v_vcFieldName  VARCHAR(50);                                                
   v_vcListItemType  VARCHAR(3);                                           
   v_vcAssociatedControlType  VARCHAR(20);                                                
   v_numListID  NUMERIC(9,0);                                                
   v_vcDbColumnName  VARCHAR(20);                    
   v_WhereCondition  VARCHAR(2000);                     
   v_vcLookBackTableName  VARCHAR(2000);              
   v_bitCustomField  BOOLEAN;
   v_bitAllowEdit  Boolean;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  Boolean;   
   v_vcColumnName  VARCHAR(500);       
   v_vcCustomColumnNameOnly  VARCHAR(500);       


   v_ListRelID  NUMERIC(9,0);             
   v_StaticColumns  TEXT DEFAULT '';

   v_strDueDateUpdate1  TEXT DEFAULT '';
   v_strDueDateUpdate2  TEXT DEFAULT '';

   v_strSql3  TEXT DEFAULT '';
   v_Nocolumns  SMALLINT;
   v_FormattedItems  TEXT;
   v_dynamicQuery1  TEXT DEFAULT '';

   v_AuthoritativeSalesBizDocId  INTEGER;
------------------------11 - A/P Bill Due------------------------------------
   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   v_numDomainID := COALESCE(v_numDomainID, 0);
   v_numUserCntID := COALESCE(v_numUserCntID, 0);
   
   v_endDate := v_endDate+INTERVAL '1 day'+INTERVAL ' -2 milliseconds ';

   v_IndivRecordPaging :=  ' ORDER BY dtDueDate DESC OFFSET ' || CAST(((v_CurrentPage::bigint -1)*v_PageSize::bigint) AS VARCHAR(30)) || ' ROWS FETCH NEXT ' || SUBSTR(CAST(COALESCE(v_PageSize,0) AS VARCHAR(30)),1,30) || ' ROWS ONLY';

   select   bitREQPOApproval, vchREQPOApprovalEmp, coalesce(decReqPOMinValue,0), bitARInvoiceDue, bitAPBillsDue, vchARInvoiceDue, vchAPBillsDue, numDivisionId 
   INTO v_bitREQPOApproval,v_vchREQPOApprovalEmp,v_decReqPOMinValue,v_bitARInvoiceDue,
   v_bitAPBillsDue,v_vchARInvoiceDue,v_vchAPBillsDue,v_numDomainDivisionID 
   FROM Domain 
   WHERE numDomainId = v_numDomainID    LIMIT 1;

   IF EXISTS(SELECT * FROM GroupAuthorization WHERE numDomainID = v_numDomainID AND numModuleID = 1 AND numPageID = 1 AND numGroupID IN(SELECT coalesce(numGroupID,0) FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID)) then

      select   coalesce(intViewAllowed,0) INTO v_tintActionItemsViewRights FROM GroupAuthorization WHERE numDomainID = v_numDomainID AND numModuleID = 1 AND numPageID = 1 AND numGroupID IN(SELECT coalesce(numGroupID,0) FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_numUserCntID);
   end if;
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

   DROP TABLE IF EXISTS tt_TEMPRECORDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRECORDS 
   (
      RecordCount NUMERIC(9,0),
      RecordId NUMERIC(9,0),
      numTaskId NUMERIC(9,0),
      IsTaskClosed BOOLEAN,
      TaskTypeName TEXT,
      TaskType NUMERIC(9,0),
      OrigDescription TEXT,
      Description TEXT,
      TotalProgress VARCHAR(50),
      numAssignToId NUMERIC(9,0),
      Priority TEXT,
      PriorityId  NUMERIC(9,0),
      ActivityId NUMERIC(9,0),
      OrgName TEXT,
      CompanyRating TEXT,
      numContactID NUMERIC(9,0),
      numDivisionID NUMERIC(9,0),
      tintCRMType SMALLINT,
      Id NUMERIC(9,0),
      bitTask VARCHAR(100),
      Startdate TIMESTAMP,
      EndTime TIMESTAMP,
      itemDesc TEXT,
      Name VARCHAR(200),
      vcFirstName VARCHAR(100),
      vcLastName VARCHAR(100),
      numPhone VARCHAR(15),
      numPhoneExtension VARCHAR(7),
      Phone VARCHAR(30),
      vcCompanyName VARCHAR(100),
      vcProfile VARCHAR(100), 
      vcEmail VARCHAR(50),
      Task VARCHAR(100),
      Activity TEXT,
      Status VARCHAR(100),
      numCreatedBy NUMERIC(9,0),
      numRecOwner VARCHAR(500), 
      numModifiedBy VARCHAR(500), 
      numOrgTerId NUMERIC(18,0), 
      numTerId VARCHAR(200),
      numAssignedTo VARCHAR(1000),
      numAssignedBy VARCHAR(50),
      caseid VARCHAR(50),
      vcCasenumber VARCHAR(50),
      casetimeId NUMERIC(9,0),
      caseExpId NUMERIC(9,0),
      type INTEGER,
      itemid VARCHAR(50),
      changekey VARCHAR(50),
      dtDueDate TIMESTAMP, 
      DueDate VARCHAR(500),
      bitFollowUpAnyTime BOOLEAN,
      numNoOfEmployeesId VARCHAR(50),
      vcComPhone VARCHAR(50),
      numFollowUpStatus VARCHAR(200),
      dtLastFollowUp VARCHAR(500),
      vcLastSalesOrderDate VARCHAR(100),
      monDealAmount DECIMAL(20,5), 
      vcPerformance VARCHAR(100),
      vcColorScheme VARCHAR(50),
      Slp_Name VARCHAR(100),
      intTotalProgress INTEGER,
      vcPoppName VARCHAR(100), 
      numOppId NUMERIC(18,0),
      numBusinessProcessID NUMERIC(18,0),
      numCompanyRating VARCHAR(500),
      numStatusID VARCHAR(500),
      numCampaignID VARCHAR(500),
      "Action-Item Participants" TEXT,
      HtmlLink VARCHAR(500),
      Location VARCHAR(150),
      OrignalDescription TEXT,
      numTaskEstimationInMinutes INTEGER,
      numTimeSpentInMinutes CHARACTER VARYING,
      dtLastStartDate TIMESTAMP,
      numRemainingQty DOUBLE PRECISION
   );                                                         
 

   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   IF(v_RegularSearchCriteria = '') then

      v_RegularSearchCriteria := coalesce(v_RegularSearchCriteria,'') || ' 1=1';
   end if;
   if(v_OppStatus = 0) then

      v_RegularSearchCriteria := coalesce(v_RegularSearchCriteria,'') || ' AND IsTaskClosed = false';
   end if;
   if(v_OppStatus = 1) then

      v_RegularSearchCriteria := coalesce(v_RegularSearchCriteria,'') || ' AND IsTaskClosed = true';
   end if;
   v_RegularSearchCriteria  := REPLACE(v_RegularSearchCriteria,'TaskTypeName','TaskType');

   v_ActivityRegularSearch := '';
   v_OtherRegularSearch := '';

   v_RegularSearchCriteria := REPLACE(v_RegularSearchCriteria,'DueDate','dtDueDate');
   v_RegularSearchCriteria := regexp_replace(v_RegularSearchCriteria,'numTerID=','numOrgTerId=','gi');   

   v_ActivityRegularSearch := coalesce((SELECT string_agg(Items, ' AND ') FROM (SELECT Items FROM SplitByString(v_RegularSearchCriteria,'and') WHERE Items <> '' AND 1 =(CASE WHEN Items ilike '%IsTaskClosed%' THEN 0
         WHEN Items ilike '%TaskType%'  THEN 0
         WHEN  Items ilike '%numAssignToId%'  THEN 0
         WHEN  Items ilike '%PriorityId%'  THEN 0
         WHEN  Items ilike '%ActivityId%'  THEN 0
         WHEN  Items ilike '%Description%'   THEN 0
         WHEN Items ilike '%dtDueDate%' THEN 0
         WHEN  Items ilike '%OrgName%' THEN 0 ELSE 1 END)) AS Items),'');
		 
   v_ActivityRegularSearch := REPLACE(v_ActivityRegularSearch,'&gt;','>');
   v_ActivityRegularSearch := REPLACE(v_ActivityRegularSearch,'&lt;','<');
   v_ActivityRegularSearch := REPLACE(v_ActivityRegularSearch,'dtDueDate','(dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval)');
   
   v_OtherRegularSearch := coalesce((SELECT string_agg(Items, ' AND ') FROM(SELECT Items FROM SplitByString(v_RegularSearchCriteria,'and') WHERE Items <> '' AND 1 =(CASE WHEN Items ilike '%IsTaskClosed%' THEN 1
         WHEN Items ilike '%TaskType%'  THEN 1
         WHEN  Items ilike '%numAssignToId%'  THEN 1
         WHEN  Items ilike '%PriorityId%'  THEN 1
         WHEN  Items ilike '%ActivityId%'  THEN 1
         WHEN  Items ilike '%Description%'   THEN 1
         WHEN Items ilike '%dtDueDate%' THEN 1
         WHEN  Items ilike '%OrgName%' THEN 1 ELSE 0 END)) AS Items),'');
		 
   v_OtherRegularSearch := coalesce(REPLACE(v_OtherRegularSearch,'T.',''),'');
   v_OtherRegularSearch := REPLACE(v_OtherRegularSearch,'&gt;','>');
   v_OtherRegularSearch := REPLACE(v_OtherRegularSearch,'&lt;','<');
   
   IF(LENGTH(v_OtherRegularSearch) = 0) then
      v_OtherRegularSearch := ' 1=1 ';
   end if;
   v_tintOrder := 0;   

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder INTEGER,
      vcDbColumnName VARCHAR(50),
      vcFieldDataType VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      bitAllowFiltering BOOLEAN,
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType CHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth NUMERIC(18,2)
   );
                
	--Custom Fields related to Organization
   INSERT INTO
   tt_TEMPFORM
   SELECT
   tintRow+1 AS tintOrder,vcDbColumnName,CAST('' AS VARCHAR(50)),vcDbColumnName,CAST(1 AS BOOLEAN),vcFieldName,vcAssociatedControlType,CAST('' AS CHAR(3)) as vcListItemType,numListID,CAST('' AS VARCHAR(50)) as vcLookBackTableName
		,bitCustom,numFieldId,bitAllowSorting,false,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
   FROM
   View_DynamicCustomColumns
   WHERE
   numFormId = 43
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitCustom,false) = true;
 
   select   tintOrder::bigint+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, 0, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
   FROM tt_TEMPFORM    
   ORDER BY tintOrder ASC 
   LIMIT 1;     
  
   v_vcCustomColumnName := '';
   v_vcnullCustomColumnName := '';
   v_vcCustomColumnNameOnly := '';
   WHILE v_tintOrder > 0 LOOP
      v_vcColumnName := v_vcDbColumnName;
      v_strSql := 'ALTER TABLE tt_TEMPRECORDS add ' || coalesce(v_vcColumnName,'') || ' TEXT';
      v_vcCustomColumnNameOnly := coalesce(v_vcCustomColumnNameOnly,'') || ',' || coalesce(v_vcColumnName,'');
      EXECUTE v_strSql;
      IF v_bitCustomField = true then
		
         IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ', COALESCE((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),'''') "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ','''' AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',COALESCE((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),0) "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',0 AS "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',FormatedDateFromDate((SELECT CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1),' || SUBSTR(Cast(COALESCE(v_numDomainID,0) AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ','''' AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',(SELECT COALESCE(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=(CASE WHEN isnumeric(CFW.Fld_Value) THEN CFW.Fld_Value ELSE ''0'' END)::NUMERIC WHERE  CFW.Fld_ID=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(50)),1,50) || ' AND CFW.RecId=Div.numDivisionID LIMIT 1)' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
			
            v_vcCustomColumnName := coalesce(v_vcCustomColumnName,'') || ',(SELECT string_agg(vcData, '', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(COALESCE(v_numListID,0) AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(COALESCE((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(50)),1,50) || ' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) )' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         ELSE
            v_vcCustomColumnName := CONCAT(v_vcCustomColumnName,',(SELECT fn_GetCustFldStringValue(',v_numFieldId,
            ',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=(CASE WHEN isnumeric(CFW.Fld_Value) THEN CFW.Fld_Value ELSE ''0'' END)::NUMERIC WHERE  CFW.Fld_ID=',v_numFieldId,
            ' AND CFW.RecId=Div.numDivisionID)',' "',v_vcColumnName,'"');
            v_vcnullCustomColumnName := coalesce(v_vcnullCustomColumnName,'') || ',''''' || 'AS  "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      end if;
      select   tintOrder::bigint+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, 0, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustomField,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
	  FROM tt_TEMPFORM 
	  WHERE tintOrder > v_tintOrder -1   
	  ORDER BY tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;
   v_strSql := '';

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 43 AND DFCS.numFormID = 43 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
-------------------- 0 - TASK----------------------------

   IF(LENGTH(v_CustomSearchCriteria) > 0) then

      v_CustomSearchCriteria := ' AND ' || coalesce(v_CustomSearchCriteria,'');
   end if;
   IF(LENGTH(v_ActivityRegularSearch) = 0) then

      v_ActivityRegularSearch := '1=1';
   end if;
   v_StaticColumns := ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
Name,vcFirstName,vcLastName,numPhone,numPhoneExtension,Phone,vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance';

v_dynamicQuery := ' INSERT INTO tt_tempRecords(
 ' || coalesce(v_StaticColumns,'') || coalesce(v_vcCustomColumnNameOnly,'') || '
) 
SELECT COUNT(*) OVER() RecordCount, * 
FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
COALESCE(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openCommTask('',Comm.numCommId,'',0)" href="javascript:void(0)">'',GetListIemName(Comm.bitTask),''</a><br/>'',COALESCE(Comp.vcCompanyName,''''),(CASE WHEN AddC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',AddC.numContactID,'',1)">('',COALESCE(AddC.vcFirstName,''-''),'' '',COALESCE(AddC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN COALESCE(AddC.numPhone,'''') <> '''' THEN AddC.numPhone ELSE '''' END),(CASE WHEN COALESCE(AddC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',AddC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN COALESCE(AddC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',AddC.vcEmail,'''''','',AddC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><ul class="list-inline"><li><button class="btn btn-flat btn-task-finish" onclick="return ActionItemFinished('',Comm.numCommId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li><button class="btn btn-comm-followup btn-flat btn-warning" onclick="return ActionItemFollowup('',Comm.numCommId,'','',AddC.numcontactID,'');">Follow-up</button></li></ul></div></div>'') AS TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
(dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS dtDueDate,
(dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS DueDate,
''-'' As TotalProgress,
fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''|| Comp.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id, CAST(bitTask AS VARCHAR(15)) as bitTask,
 (dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as Startdate,                        
 (dtEndTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName || '' '' || AddC.vcLastName as Name, 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then AddC.numPhone || case when AddC.numPhoneExtension<>'''' then '' - '' || AddC.numPhoneExtension else '''' end  else '''' end as Phone, 
  Comp.vcCompanyName As vcCompanyName , COALESCE((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID=' || SUBSTR(Cast(COALESCE(v_numDomainID,0) AS VARCHAR(30)),1,30) || ' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select vcData from ListDetails where numListItemID=Div.numTerId LIMIT 1) AS numTerId,fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  CAST(comm.caseid AS VARCHAR(50)) as caseid,(select vcCaseNumber from cases where cases.numcaseid= comm.caseid LIMIT 1)as vcCasenumber,
  COALESCE(comm.casetimeId,0) casetimeId,COALESCE(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,COALESCE(bitFollowUpAnyTime,false) bitFollowUpAnyTime,  
  GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' ||
   CONCAT('COALESCE(FormatedDateFromDate(Div.dtLastFollowUp,',v_numDomainID,'),'''')') || ' AS dtLastFollowUp,
' || CONCAT('COALESCE(FormatedDateFromDate((TempLastOrder.bintCreatedDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval),',v_numDomainID,'),'''')') || ' AS vcLastSalesOrderDate,COALESCE(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN COALESCE(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(TempPerformance.monDealAmount::numeric(20,2),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  ' || coalesce(v_vcCustomColumnName,'') || '
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON true
 LEFT JOIN LATERAL (' || CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )  ') || ' AS TempPerformance ON true
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE ' || coalesce(v_ActivityRegularSearch,'') || ' AND
	Comm.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
   ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (dtStartTime + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || '''
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN AddC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
' || coalesce(v_CustomSearchCriteria,'') || '
 )   Q WHERE ' || coalesce(v_OtherRegularSearch,'') || '
  ' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',v_dynamicQuery;
   EXECUTE v_dynamicQuery;

-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''||CAST(ac.activityid AS VARCHAR)||'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
   IF(LENGTH(v_ActivityRegularSearch) = 0 OR RTRIM(LTRIM(v_ActivityRegularSearch)) = '1=1') then
      v_FormattedItems := '
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,EndTime,type';

v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(' || coalesce(v_FormattedItems,'') || ') 
SELECT  COUNT(*) OVER() RecordCount, * 
FROM (  
SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = true then true else false end AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="javascript:void(0)">Calendar</a><br/>'',COALESCE(Comp.vcCompanyName,''''),(CASE WHEN ACI.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ACI.numContactID,'',1)">('',COALESCE(ACI.vcFirstName,''-''),'' '',COALESCE(ACI.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN COALESCE(ACI.numPhone,'''') <> '''' THEN ACI.numPhone ELSE '''' END),(CASE WHEN COALESCE(ACI.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ACI.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN COALESCE(ACI.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ACI.vcEmail,'''''','',ACI.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return CalendarFinished('',ac.activityid,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') AS TaskTypeName,
12 AS TaskType,
CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END AS OrigDescription,
COALESCE(
replace(replace((SELECT 
CASE(COALESCE(bitpartycalendarTitle,true)) WHEN true THEN ''<b><font color=red>Title</font></b>'' || CASE(COALESCE(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END || ''<br>'' else ''''  END  
|| CASE(COALESCE(bitpartycalendarLocation,true)) WHEN true THEN ''<b><font color=green>Location</font></b>'' || CASE(COALESCE(ac.location,'''')) WHEN '''' THEN '''' else ac.location END || ''<br>'' else ''''  END  
|| CASE COALESCE(bitpartycalendarDescription,true) 
	WHEN true
	THEN CONCAT(''<b><font color="#3c8dbc">Description</font></b>'',
				(CASE 
					WHEN LENGTH(CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) > 100 AND NOT ((CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) ilike ''%<[A-Za-z0-9]%'' OR (CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) ilike ''%</[A-Za-z0-9]%'')
					THEN CONCAT(CAST((CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN COALESCE(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE COALESCE(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId=' || SUBSTR(Cast(COALESCE(v_numDomainID,0) AS VARCHAR(30)),1,30) || ' 
),''&lt;'', ''<''), ''&gt;'', ''>''),'''')  AS Description,
((startdatetimeutc + ( duration || '' seconds'')::interval) + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS dtDueDate,
((startdatetimeutc + ( duration || '' seconds'')::interval) + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID='' || CAST(Div.numDivisionID AS VARCHAR) || ''">'' || Comp.vcCompanyName || CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,(startdatetimeutc + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS Startdate
 ,((startdatetimeutc + ( COALESCE(duration,0) || '' seconds'')::interval) + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS EndTime,0 AS type
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
LEFT JOIN AdditionalContactsInformation ACI on ACI.numContactId = (SELECT ContactId FROM ActivityAttendees where ActivityID=ac.activityid LIMIT 1)           
 LEFT JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 LEFT JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 LEFT JOIN LATERAL (' || CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',v_numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') || ' ) AS TempLastOrder ON true                
 LEFT JOIN LATERAL (' || CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((COALESCE(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN COALESCE(V.monCost,0) * fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN COALESCE(OI.numCost,0) ELSE COALESCE(I.monAverageCost,0) END)) / COALESCE(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',v_numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND COALESCE(I.bitContainer,false) = false
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )  ') || ' AS TempPerformance ON true

 where  res.numUserCntId = ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) as VARCHAR(10)),1,10) || '             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10) || '  and 
((startdatetimeutc + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (startdatetimeutc + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''')
and status <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10) || '  
AND 1=(Case ' || SUBSTR(Cast(v_bitFilterRecord as VARCHAR(10)),1,10) || ' when true then Case when Comm.numAssign =' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) as VARCHAR(10)),1,10) || '  then 1 else 0 end 
		else  Case when (Comm.numAssign =' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) as VARCHAR(10)),1,10) || '  or Comm.numCreatedBy=' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) as VARCHAR(10)),1,10) || ' ) then 1 else 0 end end)   
		AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
      ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (Comm.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or Comm.numAssign= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ACI.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
)) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || '  ' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',v_dynamicQuery;
      EXECUTE v_dynamicQuery;
-------------------- 1 - CASE----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN true ELSE false END AS IsTaskClosed,
''<a onclick="openCaseDetails(''||CAST(C.numCaseId AS VARCHAR)||'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as timestamp) AS dtDueDate,
cast(C.intTargetResolveDate as timestamp) as DueDate,
''0'' As TotalProgress,
fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,cast(C.intTargetResolveDate as timestamp) AS Startdate,0 AS type

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND C.numStatus <> 136  AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
      ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or C.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND ((intTargetResolveDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (intTargetResolveDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (C.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or C.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN AddC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || ' ' || coalesce(v_IndivRecordPaging,'') || '
';
      RAISE NOTICE '%',CAST(v_dynamicQuery AS TEXT);
      EXECUTE v_dynamicQuery;

-------------------- 2 - Project Task ----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate,Startdate,type
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left">'',''<a onclick="openProjectTask(''||CAST(T.numProjectId AS VARCHAR)||'')" href="javascript:void(0)">Project Task</a><br/><span>'',COALESCE(OP.vcProjectID,''''),''</span></div><div id="divTaskControlsProject" class="pull-right">'',(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),0) 
	WHEN 4 THEN (CASE WHEN EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID) THEN CONCAT(''<ul class="list-inline">
	<li><button class="btn btn-xs btn-primary" onclick="return TaskClosed('',T.numTaskId,'');">Close</button></li>
    <li style="padding-left: 0px; padding-right: 0px">'',GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,1::SMALLINT),''</li>
    <li style="padding-left: 0px; padding-right: 0px"><img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" /></li>
    <li style="padding-left: 0px; padding-right: 0px"><input type="checkbox" onclick="addRemoveTimeProject('',T.numDomainID,'','',Div.numDivisionID,'','',GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,2::SMALLINT),'','',T.numTaskId,'','''''',
	((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=1) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval),'''''','''''',
	((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval),'''''','',T.numAssignTo,'','''''',T.vcTaskName,'''''','''''',(CASE WHEN GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,0::SMALLINT) = ''Invalid time sequence'' THEN ''00:00'' ELSE GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,0::SMALLINT) END),'''''','''''',COALESCE(ACustomer.vcEmail,''''),'''''','''''',fn_GetContactName(ACustomer.numContactId),'''''')"'',(CASE WHEN COALESCE(T.bitTimeAddedToContract,false)=true THEN '' checked=''''checked'''''' ELSE '''' END),'' class="chk_'',T.numTaskId,''" /></li>
</ul>'') ELSE ''<img src="../images/comflag.png" />'' END)
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,'',T.numTaskId,'',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
(OP.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS dtDueDate,
(OP.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as DueDate,
COALESCE((SELECT intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId LIMIT 1),0) As TotalProgress,
fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName || CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
,((COALESCE(T.numHours,0) * 60) + COALESCE(T.numMinutes,0)) numTaskEstimationInMinutes
,(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1),0) 
	WHEN 4 THEN ''0''
	WHEN 3 THEN GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,2::SMALLINT)
	WHEN 2 THEN GetTimeSpendOnTaskByProject(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,2::SMALLINT)
	WHEN 1 THEN ''0''
END) numTimeSpentInMinutes
,(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN ((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval)
	WHEN 2 THEN NULL
	WHEN 1 THEN ((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval)
END) dtLastStartDate, CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END AS Startdate,0 AS type';

v_dynamicQuery1 := ' FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	INNER JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId     
	LEFT JOIN LATERAL 
	(
		SELECT 
			CAST(MAX(Days) AS NUMERIC(18,2)) AS vcTime
		FROM
		(
			SELECT 
				coalesce(numAssignTo,0) AS numAssignTo,
				FLOOR(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0))/1440) AS Days,
				CAST((MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),1440)) AS decimal)/60 AS Hours,
				MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),60) AS Minutes
			FROM 
				StagePercentageDetailsTask 
			WHERE 
				numStageDetailsId = T.numStageDetailsId 
				AND bitSavedTask = true 
				AND 1 = (CASE WHEN T.numOppId > 0 THEN (CASE WHEN numOppId = T.numOppId THEN 1 ELSE 0 END) WHEN T.numProjectId > 0 THEN (CASE WHEN numProjectId = T.numProjectId THEN 1 ELSE 0 END) ELSE 0 END)
      		GROUP BY numAssignTo
		) AS T1
	) TEMPTime ON TRUE
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ACustomer ON OP.numCustPrjMgr = ACustomer.numContactId
WHERE
	T.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' 
	AND (NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) OR (COALESCE(T.bitTaskClosed,false) = false AND EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID)))
	AND ((CAST((OP.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS DATE) >= ''' || Cast(CAST(v_startDate AS DATE) as VARCHAR(30)) || ''' and CAST((OP.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS DATE) <= ''' || Cast(CAST(v_endDate AS DATE) as VARCHAR(30)) || ''') OR ((OP.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (OP.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''')) 
	AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
      ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END) 
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ACustomer.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || coalesce(v_IndivRecordPaging,'');

--PRINT CAST(@dynamicQuery AS NTEXT)
RAISE NOTICE '%',coalesce(v_dynamicQuery,'') || coalesce(v_dynamicQuery1,'');
      EXECUTE coalesce(v_dynamicQuery,'') || coalesce(v_dynamicQuery1,'');

------------------ 3 - Opportunity  Task ----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)||'')" href="javascript:void(0)">Opportunity Task</a><br/>'',COALESCE(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',COALESCE(ADC.vcFirstName,''-''),'' '',COALESCE(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN COALESCE(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN COALESCE(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN COALESCE(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + (S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END as DueDate,
COALESCE((SELECT intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId LIMIT 1),0) As TotalProgress,
fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating, CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END AS Startdate,0 AS type

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId
	LEFT JOIN LATERAL 
	(
		SELECT 
			CAST(MAX(Days) AS NUMERIC(18,2)) AS vcTime
		FROM
		(
			SELECT 
				coalesce(numAssignTo,0) AS numAssignTo,
				FLOOR(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0))/1440) AS Days,
				CAST((MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),1440)) AS decimal)/60 AS Hours,
				MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),60) AS Minutes
			FROM 
				StagePercentageDetailsTask 
			WHERE 
				numStageDetailsId = T.numStageDetailsId 
				AND bitSavedTask = true 
				AND 1 = (CASE WHEN T.numOppId > 0 THEN (CASE WHEN numOppId = T.numOppId THEN 1 ELSE 0 END) WHEN T.numProjectId > 0 THEN (CASE WHEN numProjectId = T.numProjectId THEN 1 ELSE 0 END) ELSE 0 END)
      		GROUP BY numAssignTo
		) AS T1
	) TEMPTime ON TRUE
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=true AND T.bitTaskClosed=false AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
      ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END) 
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || ' AND ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''')
' || coalesce(v_IndivRecordPaging,'') || ' 
 ';

 RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
      EXECUTE v_dynamicQuery;

------------------ 4 -  Sales Order Task ----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)||'')" href="javascript:void(0)">Sales Order Task</a><br/>'',COALESCE(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',COALESCE(ADC.vcFirstName,''-''),'' '',COALESCE(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN COALESCE(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN COALESCE(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN COALESCE(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval) 
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END as DueDate,
COALESCE((SELECT intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId LIMIT 1),0) As TotalProgress,
fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=true THEN (CAST(S.dtStartDate AS DATE) + ( S.intDueDays || '' days'')::interval)
		ELSE (CAST(S.dtStartDate AS DATE) + (COALESCE(TEMPTime.vcTime,0) || '' days'')::interval) END AS Startdate,0 AS type
 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId
	LEFT JOIN LATERAL 
	(
		SELECT 
			CAST(MAX(Days) AS NUMERIC(18,2)) AS vcTime
		FROM
		(
			SELECT 
				coalesce(numAssignTo,0) AS numAssignTo,
				FLOOR(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0))/1440) AS Days,
				CAST((MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),1440)) AS decimal)/60 AS Hours,
				MOD(SUM((coalesce(numHours,0)*60)+coalesce(numMinutes,0)),60) AS Minutes
			FROM 
				StagePercentageDetailsTask 
			WHERE 
				numStageDetailsId = T.numStageDetailsId 
				AND bitSavedTask = true 
				AND 1 = (CASE WHEN T.numOppId > 0 THEN (CASE WHEN numOppId = T.numOppId THEN 1 ELSE 0 END) WHEN T.numProjectId > 0 THEN (CASE WHEN numProjectId = T.numProjectId THEN 1 ELSE 0 END) ELSE 0 END)
      		GROUP BY numAssignTo
		) AS T1
	) TEMPTime ON TRUE
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=true AND T.bitTaskClosed=false AND 1 = (CASE ' || SUBSTR(CAST(COALESCE(v_tintActionItemsViewRights,0) AS VARCHAR(30)),1,30) ||
      ' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
ELSE 0 END) 
AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (T.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or T.numAssignTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || ' AND ((CAST(Q.DueDate AS DATE)+ ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''')
' || coalesce(v_IndivRecordPaging,'') || '
 ';

 RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
      EXECUTE v_dynamicQuery;
  
------------------ 5 -  Work Order Task ----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN true ELSE false END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
(WorkOrder.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS dtDueDate,
(WorkOrder.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as DueDate,
GetTotalProgress(WorkOrder.numDomainID::NUMERIC,WorkOrder.numWOID::NUMERIC,1::SMALLINT,1::SMALLINT,''''::VARCHAR,0::NUMERIC) As TotalProgress,
fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating, (WorkOrder.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS Startdate,0 AS type

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' || SUBSTR(CAST(COALESCE(v_numDomainDivisionID,0) AS VARCHAR(30)),1,30) || ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || '
	AND (((WorkOrder.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (WorkOrder.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') OR ((WorkOrder.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (WorkOrder.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || '''))
	AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (WorkOrder.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or WorkOrder.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN 1
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)

) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || ' ' || coalesce(v_IndivRecordPaging,'') || '';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
EXECUTE v_dynamicQuery;
------------------ 6 -  Work Center Task ----------------------------

v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate
,numRemainingQty
,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN true ELSE false END) AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a><br/><span>'',CONCAT(I.vcItemName,'' ('',W.numQtyItemsReq,'')''),''</span></div><div class="pull-right" id="divTaskControlsWorkOrder">'',(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC, ID DESC LIMIT 1),0) 
	WHEN 4 THEN ''<img src="../images/comflag.png" />''
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',1);">Resume</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,'',T.numTaskId,'',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
(W.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS dtDueDate,
(W.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((COALESCE((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 ,(((COALESCE(T.numHours,0) * 60) + COALESCE(T.numMinutes,0)) * COALESCE(W.numQtyItemsReq,0)) numTaskEstimationInMinutes
,(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1),0) 
	WHEN 4 THEN ''0''
	WHEN 3 THEN GetTimeSpendOnTask(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,2::SMALLINT)
	WHEN 2 THEN GetTimeSpendOnTask(T.numDomainID::NUMERIC,T.numTaskId::NUMERIC,2::SMALLINT)
	WHEN 1 THEN ''0''
END) numTimeSpentInMinutes
,(CASE COALESCE((SELECT tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC LIMIT 1),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN ((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval)
	WHEN 2 THEN NULL
	WHEN 1 THEN ((SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC LIMIT 1) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval)
END) dtLastStartDate
,(CASE
	WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4) THEN 0
	ELSE COALESCE(W.numQtyItemsReq,0) - COALESCE((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId),0)
END) AS numRemainingQty
, (W.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) AS Startdate,0 AS type
 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
	INNER JOIN Item I ON W.numItemCode=I.numItemCode
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' || SUBSTR(CAST(COALESCE(v_numDomainDivisionID,0) AS VARCHAR(30)),1,30) || ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(30)),1,30) || '
	AND (((W.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (W.dtmStartDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') OR ((W.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (W.dtmEndDate + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''')
	AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (W.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or W.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN 1
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
	) 
) Q WHERE ' || coalesce(v_OtherRegularSearch,'') || ' ' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
      EXECUTE v_dynamicQuery;

------------------ 7 -  Requisition Approval ----------------------------

      IF(v_bitREQPOApproval = true AND v_numUserCntID IN (SELECT CAST(Items AS NUMERIC(9,0)) FROM Split(v_vchREQPOApprovalEmp,',') WHERE Items <> '')) then

         v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN COALESCE(OP.intReqPOApproved,0)=0 THEN false ELSE true END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''||CAST(OP.numOppId AS VARCHAR)||'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''||CAST(OP.numOppId AS VARCHAR)||'')" href="javascript:void(0)">'' || OP.vcPOppName || ''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''||CAST(OP.numOppId AS VARCHAR)||'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''||CAST(OP.numOppId AS VARCHAR)||'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
fn_GetContactName(' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ') As numAssignedTo,
' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate  AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
WHERE
	OP.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN ' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || '>0 AND Op.monPAmount>' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || ' THEN 1  WHEN ' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || '=0 THEN 1  ELSE 0 END ) 
	AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (OP.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or OP.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
	) AS Q
WHERE ' || coalesce(v_OtherRegularSearch,'') || ' AND ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
         EXECUTE v_dynamicQuery;
      end if;
------------------ 8 -  PO Approval ----------------------------
      IF(v_bitREQPOApproval = true AND v_numUserCntID IN (SELECT CAST(Items AS NUMERIC(9,0)) FROM Split(v_vchREQPOApprovalEmp,',') WHERE Items <> '')) then

         v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN COALESCE(OP.intReqPOApproved,0)=0 THEN false ELSE true END AS IsTaskClosed,
''<a onclick="openSalesOrder(''||CAST(OP.numOppId AS VARCHAR)||'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''||CAST(OP.numOppId AS VARCHAR)||'')">''||OP.vcPOppName||''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''||CAST(OP.numOppId AS VARCHAR)||'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''||CAST(OP.numOppId AS VARCHAR)||'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
fn_GetContactName(' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ') As numAssignedTo,
' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''||Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
 LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
WHERE
	OP.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN ' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || '>0 AND Op.monPAmount>' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || ' THEN 1  WHEN ' || SUBSTR(CAST(v_decReqPOMinValue AS VARCHAR(100)),1,100) || '=0 THEN 1  ELSE 0 END )
	AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (OP.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or OP.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
	) AS Q
	WHERE  ' || coalesce(v_OtherRegularSearch,'') || ' AND  ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
         EXECUTE v_dynamicQuery;
      end if;

------------------ 9 -  Price Margin Approval ----------------------------

      v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
COALESCE(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
COALESCE(OP.bitReqPOApproved,false) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''||CAST(OP.numOppId AS VARCHAR)||'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''||CAST(OP.numOppId AS VARCHAR)||'')">''||OP.vcPOppName||''</a> : '' || ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)||''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''||CAST(OP.numOppId AS VARCHAR)||'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''||CAST(OP.numOppId AS VARCHAR)||'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
fn_GetContactName(' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ') As numAssignedTo,
' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(200)),1,200) || ' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(Div.numDivisionID AS VARCHAR)||''">''|| Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.bintCreatedDate AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
WHERE
	OP.numDomainID = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=true AND OpportunityItems.numOppId=OP.numOppId) >0
	AND ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(100)),1,100) || ' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || ' )
	AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
			WHEN 1 
			THEN (CASE WHEN (OP.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or OP.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
			WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			WHEN 3 THEN (CASE WHEN Div.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
			ELSE 1 
		END)
	) AS Q
	WHERE  ' || coalesce(v_OtherRegularSearch,'') || ' AND  ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
' || coalesce(v_IndivRecordPaging,'') || '

';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
      EXECUTE v_dynamicQuery;

------------------------10 - A/R Invoice Due------------------------------------
      IF(v_bitARInvoiceDue = true AND v_numUserCntID IN (SELECT CAST(Items AS NUMERIC(9,0)) FROM Split(v_vchARInvoiceDue,',') WHERE Items <> '')) then
         select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId = v_numDomainID;
         v_dynamicQuery :=  ' INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
COALESCE(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
false AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''||CAST(OM.numOppId AS VARCHAR)||'',''||CAST(OB.numOppBizDocsId AS VARCHAR)||'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''||CAST(OM.numOppId AS VARCHAR)||'',''||CAST(OB.numOppBizDocsId AS VARCHAR)||'')">''||CAST(OB.vcBizDocID AS VARCHAR)||''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''||CAST(OM.numOppId AS VARCHAR)||'')">''||CAST(OM.vcPOppName AS VARCHAR)||''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''||CAST(OM.numOppId AS VARCHAR)||'',''||CAST(OB.numOppBizDocsId AS VARCHAR)||'',''||CAST(OM.numDivisionId AS VARCHAR)||'')"   class="underLineHyperLink">'' || CAST(COALESCE(C.varCurrSymbol,'''') AS VARCHAR) || '''' || CAST(CAST(COALESCE(OB.monDealAmount  - COALESCE(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)||''</a>''  AS Description,
 CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN (OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval)
                          WHEN false THEN ob.dtFromDate
                        END AS dtDueDate,
 CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN FormatedDateFromDate((OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval),
                                                                   1)
                          WHEN false THEN FormatedDateFromDate(ob.dtFromDate,1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(DM.numDivisionID AS VARCHAR)||''">''|| Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN (OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval)
                          WHEN false THEN ob.dtFromDate
                        END) AS StartDate, 0 AS type
 FROM   OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
 LEFT JOIN AdditionalContactsInformation ADC ON OM.numContactId = ADC.numContactId 
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
			   LEFT JOIN LATERAL
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CAST(DM.dtDepositDate AS DATE) <= (''' || SUBSTR(Cast(v_endDate as VARCHAR(100)),1,100) || '''::TIMESTAMP + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' days'')::interval)
				) TablePayments ON true
        WHERE  OM.tintOppType = 1
               AND OM.tintOppStatus = 1
               AND om.numDomainId = ' || SUBSTR(Cast(COALESCE(v_numDomainID,0) as VARCHAR(10)),1,10)  || '
               AND OB.numBizDocId = ' || SUBSTR(CAST(v_AuthoritativeSalesBizDocId As VARCHAR(30)),1,30) || '
               AND OB.bitAuthoritativeBizDocs=1
               AND COALESCE(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
						WHEN 1 
						THEN (CASE WHEN (OM.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or OM.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
						WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE WHEN DM.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
						ELSE 1 
					END)
			   AND CAST(OB.dtCreatedDate AS DATE) <= (''' || SUBSTR(Cast(v_endDate as VARCHAR(100)),1,100) || '''::TIMESTAMP + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' days'')::interval)
               AND COALESCE(OB.monDealAmount
                            ,0) > 0  AND COALESCE(OB.monDealAmount * OM.fltExchangeRate
                        - COALESCE(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  ' || coalesce(v_OtherRegularSearch,'') || ' AND  ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
         EXECUTE v_dynamicQuery;
      end if;
------------------------11 - A/P Bill Due------------------------------------
      IF(v_bitAPBillsDue = true AND v_numUserCntID IN (SELECT CAST(Items AS NUMERIC(9,0)) FROM Split(v_vchAPBillsDue,',') WHERE Items <> '')) then
         select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId =  v_numDomainID;
         v_dynamicQuery := '  INSERT INTO tt_tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
COALESCE(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
false AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''||CAST(DM.numDivisionID AS VARCHAR)||'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''||CAST(OM.numOppId AS VARCHAR)||'',''||CAST(OB.numOppBizDocsId AS VARCHAR)||'')">''||CAST(OB.vcBizDocID AS VARCHAR)||''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''||CAST(OM.numOppId AS VARCHAR)||'')">''||CAST(OM.vcPOppName AS VARCHAR)||''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''||CAST(OM.numOppId AS VARCHAR)||'',''||CAST(OB.numOppBizDocsId AS VARCHAR)||'')"   class="underLineHyperLink">'' || CAST(COALESCE(C.varCurrSymbol,'''') AS VARCHAR) || '''' || CAST(CAST(COALESCE((OB.monDealAmount * OM.fltExchangeRate)  - (COALESCE(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)||''</a>''  AS Description,
 CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN (OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval)
                          WHEN false THEN ob.dtFromDate
                        END AS dtDueDate,
 CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN FormatedDateFromDate((OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval),
                                                                   1)
                          WHEN false THEN FormatedDateFromDate(ob.dtFromDate,1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''||CAST(DM.numDivisionID AS VARCHAR)||''">''|| Com.vcCompanyName|| CASE WHEN  COALESCE(listCompanyRating.VcData,'''')<>'''' THEN ''(''||listCompanyRating.VcData||'')'' ELSE '''' END ||''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN (OB.dtFromDate + ( CAST(COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0) AS INTEGER) || '' days'')::interval)
                          WHEN false THEN ob.dtFromDate
                        END) AS StartDate,0 AS type

   FROM   OpportunityMaster OM
               INNER JOIN DivisionMaster DM
                 ON OM.numDivisionId = DM.numDivisionID
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				 LEFT JOIN AdditionalContactsInformation ADC ON OM.numContactId = ADC.numContactId 
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN Currency C
                 ON OM.numCurrencyID = C.numCurrencyID
				 LEFT JOIN LATERAL
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= (''' || SUBSTR(Cast(v_endDate as VARCHAR(100)),1,100) || '''::TIMESTAMP + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' days'')::interval)
				) TablePayment ON true
        WHERE  OM.tintOppType = 2
               AND OM.tintOppStatus = 1
               AND om.numDomainId = 1
               AND OB.numBizDocId = ' || SUBSTR(CAST(v_AuthoritativePurchaseBizDocId AS VARCHAR(30)),1,30) || ' 
               AND OB.bitAuthoritativeBizDocs=1
               AND COALESCE(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			    AND 1 = (CASE ' || coalesce(v_tintFilterBy,0) || ' 
						WHEN 1 
						THEN (CASE WHEN (OM.numCreatedBy= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' or OM.numAssignedTo= ' || SUBSTR(Cast(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') THEN 1 ELSE 0 END) 
						WHEN 2 THEN (CASE WHEN ADC.numTeam in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE WHEN DM.numTerID in (select Id from SplitIDs(''' || coalesce(v_vcFilterValues,'') || ''','','')) THEN 1 ELSE 0 END)
						ELSE 1 
					END)
			   AND CAST(OB.dtCreatedDate AS DATE) <= (''' || SUBSTR(Cast(v_endDate as VARCHAR(100)),1,100) || '''::TIMESTAMP + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' days'')::interval)
               AND  COALESCE(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND COALESCE(OB.monDealAmount * OM.fltExchangeRate
                        - COALESCE(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  ' || coalesce(v_OtherRegularSearch,'') || ' AND  ((CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) >= ''' || SUBSTR(Cast(v_startDate as VARCHAR(30)),1,30) || ''' and (CAST(Q.DueDate AS DATE) + ( ' || CAST(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(10)) || ' || '' minutes'')::interval) <= ''' || SUBSTR(Cast(v_endDate as VARCHAR(30)),1,30) || ''') 
' || coalesce(v_IndivRecordPaging,'') || '
';

RAISE NOTICE '%',coalesce(v_dynamicQuery,'');
         EXECUTE v_dynamicQuery;
      end if;
   end if;

v_strDueDateUpdate1 := ' UPDATE tt_tempRecords 
SET DueDate = 
CASE 
WHEN CAST(Startdate AS DATE) = CAST((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) AS DATE) 
then CONCAT(''<b><font color="#FF0000" style="font-size:14px">Today''
	,CASE 
		WHEN COALESCE(bitFollowUpAnyTime,false)=true 
		THEN '' <i>Any time</i>'' 
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''||LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''||RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN COALESCE(bitFollowUpAnyTime,false)=true AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval '' 1 day'') AS DATE) 
THEN CONCAT(''<b><font color=#ED8F11 style="font-size:14px">Tommorow''
	,CASE 
		WHEN COALESCE(bitFollowUpAnyTime,false)=true 
		THEN '' <i>Any time</i>''  
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''||LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL 
		THEN  '' @ ''||RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval ''2 days'') AS DATE) then CONCAT(''<b><font color=#8FAADC style="font-size:14px">'',to_char(Startdate, '' day''),''<span style="color:#000;font-weight:500"> - '',CAST(CAST(date_part(''day'', Startdate) AS INTEGER) AS VARCHAR(10)),CASE
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 100 IN (11, 12, 13) THEN ''th''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 1 THEN ''st''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 2 THEN ''nd''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' ELSE '''' END,''</b>'')
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval ''3 days'') AS DATE) then CONCAT(''<b><font color=#CC99FF style="font-size:14px">'',to_char(Startdate, ''day''),''<span style="color:#000;font-weight:500"> - '',CAST(CAST(date_part(''day'', Startdate) AS INTEGER) AS VARCHAR(10)),CASE
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 100 IN (11, 12, 13) THEN ''th''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 1 THEN ''st''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 2 THEN ''nd''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' ELSE '''' END ,''</b>'')
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval ''4 days'') AS DATE) then CONCAT(''<b><font color=#AED495 style="font-size:14px">'',to_char(Startdate, ''day''),''<span style="color:#000;font-weight:500"> - '',CAST(CAST(date_part(''day'', Startdate) AS INTEGER) AS VARCHAR(10)),CASE
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 100 IN (11, 12, 13) THEN ''th''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 1 THEN ''st''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 2 THEN ''nd''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''';

   v_strDueDateUpdate2 := ',''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' ELSE '''' END ,''</b>'')
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval ''5 days'') AS DATE) then CONCAT(''<b><font color=#72DFDC style="font-size:14px">'',to_char(Startdate, ''day''),''<span style="color:#000;font-weight:500"> - '',CAST(CAST(date_part(''day'', Startdate) AS INTEGER) AS VARCHAR(10)),CASE
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 100 IN (11, 12, 13) THEN ''th''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 1 THEN ''st''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 2 THEN ''nd''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' ELSE '''' END,''</b>'')
WHEN CAST(Startdate AS DATE) = CAST(((now() at time zone ''utc'' + ( ' || Cast(COALESCE(-v_ClientTimeZoneOffset,0) AS VARCHAR(15)) || ' || '' minutes'')::interval) + interval ''6 days'') AS DATE) then CONCAT(''<b><font color=#FF9999 style="font-size:14px">'',to_char(Startdate, ''day''),''<span style="color:#000;font-weight:500"> - '',CAST(CAST(date_part(''day'', Startdate) AS INTEGER) AS VARCHAR(10)),CASE
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 100 IN (11, 12, 13) THEN ''th''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 1 THEN ''st''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 2 THEN ''nd''
	WHEN CAST(date_part(''day'', Startdate) AS INTEGER) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' ELSE '''' END ,''</b>'') 
ELSE CONCAT(''<span style="color:#909090;font-size:14px;font-weight:bold">'', 
to_char(Startdate, ''Mon'') || ''-'' || case when length(cast(extract(day from Startdate) as varchar)) < 2 then concat(''0'', cast(extract(day from Startdate) as varchar)) else cast(extract(day from Startdate) as varchar) end || ''-'' || cast(extract(year from Startdate) as varchar)
,CASE 
	WHEN COALESCE(bitFollowUpAnyTime,false)=true 
	THEN '' <i>Any time</i>''
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  '' @ ''||LEFT(RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''||RIGHT(''0''||LTRIM(RIGHT(CAST(Startdate AS VARCHAR),8)),7) 
	ELSE '''' 
END,CASE WHEN COALESCE(bitFollowUpAnyTime,false)=false AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''||RIGHT(''0''||LTRIM(RIGHT(CAST(EndTime AS VARCHAR),8)),7)||''</Span>'' 
ELSE '''' 
END,''</span>'') END  ';
   EXECUTE coalesce(v_strDueDateUpdate1,'') || coalesce(v_strDueDateUpdate2,'');
 
   IF LENGTH(v_columnName) > 0 then

      v_strSql3 := CONCAT(v_strSql3,' order by ',CASE WHEN v_columnName = 'Action-Item Participants' THEN CONCAT('"','Action-Item Participants','"') WHEN v_columnName = 'DueDate' THEN 'dtDueDate' ELSE v_columnName END,' ',v_columnSortOrder);
   ELSE
      v_strSql3 := CONCAT(v_strSql3,' ORDER BY dtDueDate ASC  ');
   end if;
   SELECT coalesce(SUM(DISTINCT RecordCount),0) INTO v_TotalRecordCount FROM tt_TEMPRECORDS;

   v_dynamicQuery := 'SELECT ' || SUBSTR(CAST(v_TotalRecordCount AS VARCHAR(400)),1,400) || ' AS TotalRecords,T.*
   FROM tt_tempRecords T WHERE 1=1 ' || coalesce(v_strSql3,'');
   
   RAISE NOTICE '%',v_dynamicQuery;
   OPEN SWV_RefCur FOR 
   EXECUTE v_dynamicQuery;
   
   DROP TABLE IF EXISTS tt_TEMPCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOLUMNS
   (
      vcFieldName VARCHAR(200),
      vcDbColumnName VARCHAR(200),
      vcOrigDbColumnName VARCHAR(200),
      bitAllowSorting BOOLEAN,
      numFieldId INTEGER,
      bitAllowEdit BOOLEAN,
      bitCustomField BOOLEAN,
      vcAssociatedControlType VARCHAR(50),
      vcListItemType  VARCHAR(200),
      numListID INTEGER,
      ListRelID INTEGER,
      vcLookBackTableName VARCHAR(200),
      bitAllowFiltering BOOLEAN,
      vcFieldDataType  VARCHAR(20),
      intColumnWidth INTEGER,
      bitClosedColumn BOOLEAN,
      bitFieldMessage BOOLEAN DEFAULT false,
      vcFieldMessage VARCHAR(500),
      bitIsRequired BOOLEAN DEFAULT false,
      bitIsNumeric BOOLEAN DEFAULT false,
      bitIsAlphaNumeric BOOLEAN DEFAULT false,
      bitIsEmail BOOLEAN DEFAULT false,
      bitIsLengthValidation  BOOLEAN DEFAULT false,
      intMaxLength INTEGER DEFAULT 0,
      intMinLength INTEGER DEFAULT 0
   );



   v_Nocolumns := 0;       
         
   select   COUNT(*) INTO v_Nocolumns FROM
   View_DynamicColumns WHERE
   numFormId = 43
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1; 
  
  
   if v_Nocolumns > 0 then

      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,CAST(vcFieldDataType AS VARCHAR(50)),vcOrigDbColumnName,bitAllowFiltering,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
      FROM View_DynamicColumns
      where numFormId = 43 and numUserCntID = v_numUserCntID and numDomainID = v_numDomainID AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      order by tintOrder asc;
   else 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 43 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
	
         v_IsMasterConfAvailable := true;
      end if;

	--If MasterConfiguration is available then load it otherwise load default columns
      IF v_IsMasterConfAvailable = true then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         43,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,NULL,1,0,intColumnWidth
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 43 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false;
         INSERT INTO tt_TEMPFORM
         SELECT(intRowNum+1) as tintOrder, vcDbColumnName,CAST(vcFieldDataType AS VARCHAR(50)),vcOrigDbColumnName,bitAllowFiltering, coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 43 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
         ORDER BY
         tintOrder ASC;
      ELSE
         INSERT INTO tt_TEMPFORM
         select tintOrder,vcDbColumnName,CAST(vcFieldDataType AS VARCHAR(50))
		,vcOrigDbColumnName,bitAllowFiltering,coalesce(vcCultureFieldName,vcFieldName),
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
		,cast(bitCustom as boolean),numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
         FROM View_DynamicDefaultColumns
         where numFormId = 43 and bitDefault = true AND coalesce(bitSettingField,false) = true AND numDomainID = v_numDomainID
         order by tintOrder asc;
      end if;
   end if;

   INSERT INTO tt_TEMPFORM
   SELECT
   tintOrder,vcDbColumnName,CAST(vcFieldDataType AS VARCHAR(50))
		,vcOrigDbColumnName,bitAllowFiltering,coalesce(vcCultureFieldName,vcFieldName),
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
		,cast(bitCustom as boolean),numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth
   FROM
   View_DynamicDefaultColumns
   WHERE
   numFormId = 43
   AND bitDefault = true
   AND coalesce(bitSettingField,false) = true
   AND numDomainID = v_numDomainID
   AND numFieldID NOT IN(SELECT numFieldId FROM tt_TEMPFORM)
   ORDER BY
   tintOrder asc;  
			
   UPDATE
   tt_TEMPFORM TC
   SET
   intColumnWidth =(CASE WHEN TC.vcOrigDbColumnName = 'DueDate' AND coalesce(DFCD.intColumnWidth,0) < 180 THEN 180 ELSE coalesce(DFCD.intColumnWidth,0) END)
   FROM
   DycFormConfigurationDetails DFCD
   WHERE
   TC.numFieldId = DFCD.numFieldID AND(DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 43
   AND DFCD.tintPageType = 1); 

   OPEN SWV_RefCur2 FOR
   SELECT * FROM tt_TEMPFORM order by tintOrder asc;

END; 
$$;


