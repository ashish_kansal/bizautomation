-- Stored procedure definition script USP_GetContractDdlList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContractDdlList(v_numDivisionId NUMERIC(9,0) DEFAULT 0,   
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                        
 v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   cast(numContractId as VARCHAR(255)),
cast(vcContractName as VARCHAR(255))
   from ContractManagement c
   where 
--@numUserCntID = numUserCntId and 
   c.numdomainId = v_numDomainID and numDivisionID = v_numDivisionId;
END; $$;












