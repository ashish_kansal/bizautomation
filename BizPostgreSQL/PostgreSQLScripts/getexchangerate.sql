-- Function definition script GetExchangeRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetExchangeRate(v_numDomainID NUMERIC,v_numCurrencyID NUMERIC)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseCurrencyID  NUMERIC;
   v_fltExchangeRate  DOUBLE PRECISION;
BEGIN
   select   numCurrencyID INTO v_numBaseCurrencyID from Domain Where numDomainId = v_numDomainID;

   if v_numBaseCurrencyID = v_numCurrencyID then 
      v_fltExchangeRate := 1;
   ELSEIF exists(select * from Currency Where numCurrencyID = v_numBaseCurrencyID and chrCurrency = 'USD')
   then
      select   fltExchangeRate INTO v_fltExchangeRate from Currency Where numCurrencyID = v_numCurrencyID;
   else
      select   C1.fltExchangeRate/(case when C2.fltExchangeRate = 0 then 1 else C2.fltExchangeRate end) INTO v_fltExchangeRate from Currency C1
      Join Currency C2
      on C1.numDomainId = C2.numDomainId where C1.numCurrencyID = v_numCurrencyID and C2.numCurrencyID = v_numBaseCurrencyID;
   end if;

   return coalesce(v_fltExchangeRate,1);
END; $$;

