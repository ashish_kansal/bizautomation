-- Stored procedure definition script USP_GetGatewatDTls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetGatewatDTls(v_numDomainID NUMERIC(9,0),
v_numSiteId  NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numSiteId,0) > 0 AND(SELECT coalesce(intPaymentGateWay,0) FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteId) > 0 then
	
      open SWV_RefCur for
      SELECT intPaymentGateWay FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteId;
   ELSE
      open SWV_RefCur for
      SELECT intPaymentGateWay FROM Domain WHERE numDomainId = v_numDomainID;
   end if;

   open SWV_RefCur2 for
   SELECT
   PG.intPaymentGateWay
		,vcGateWayName
		,vcFirstFldName
		,vcSecndFldName
		,vcThirdFldName
		,vcFirstFldValue
		,vcSecndFldValue
		,vcThirdFldValue
		,coalesce(bitTest,false) AS bitTest
		,coalesce(vcToolTip,'') AS vcToolTip
   FROM
   PaymentGateway PG
   LEFT JOIN
   PaymentGatewayDTLID PGDTLID
   ON
   PG.intPaymentGateWay = PGDTLID.intPaymentGateWay
   AND numDomainID = v_numDomainID
   AND PGDTLID.numSiteId = v_numSiteId;
   RETURN;
END; $$;


