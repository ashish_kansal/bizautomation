-- Function definition script GetProjectName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectName(v_numProId NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcProjectName  VARCHAR(100);
BEGIN
   v_vcProjectName := '';
   select   vcProjectName INTO v_vcProjectName from ProjectsMaster where numProId = v_numProId;
   RETURN v_vcProjectName;
END; $$;

