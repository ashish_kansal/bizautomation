-- Stored procedure definition script USP_GetMutltiExpensesDetailed for PostgreSQL
Create or replace FUNCTION USP_GetMutltiExpensesDetailed(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   drop table IF EXISTS tt_TEMPEXPENSEDTL CASCADE;
   create TEMPORARY TABLE tt_TEMPEXPENSEDTL
   (
      numDomainID INTEGER,
      vcDomainName VARCHAR(200),
      vcDomainCode VARCHAR(50),
      vcAccountName VARCHAR(200),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      Total NUMERIC(19,4)
   );

   INSERT INTO tt_TEMPEXPENSEDTL
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName

----------------------------------------
   union 
--------------------------
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName
   union
   select DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
   from VIEW_EXPENSEDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(select * from Domain DNV where numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.numDomainId,DN.vcDomainName,DN.vcDomainCode,vcAccountName;


open SWV_RefCur for select
   vcAccountName,
cast(coalesce(Sum(coalesce(Total,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as NUMERIC(19,4)) as Total
   from tt_TEMPEXPENSEDTL
   group by
   vcAccountName;

   RETURN;
END; $$;












