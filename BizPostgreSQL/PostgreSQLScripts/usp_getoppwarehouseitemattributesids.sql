-- Stored procedure definition script USP_GetOppWareHouseItemAttributesIds for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOppWareHouseItemAttributesIds(v_numRecID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numRecID,0) > 0 then
	
      open SWV_RefCur for
      SELECT fn_GetAttributesIds(v_numRecID);
   ELSE
		open SWV_RefCur for
      SELECT '';
   end if;
   RETURN;
END; $$;


