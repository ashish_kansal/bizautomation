DROP FUNCTION IF EXISTS USP_GetPOItemsForFulfillment;

CREATE OR REPLACE FUNCTION USP_GetPOItemsForFulfillment
(
	v_numDomainID NUMERIC(9,0),
	v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                                               
	v_SortChar CHAR(1) DEFAULT '0',                                                            
	v_CurrentPage INTEGER DEFAULT NULL,                                                              
	v_PageSize INTEGER DEFAULT NULL,                                                              
	INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                              
	v_columnName VARCHAR(50) DEFAULT NULL,                                                              
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_numRecordID NUMERIC(18,0) DEFAULT NULL,
	v_FilterBy SMALLINT DEFAULT NULL,
	v_Filter VARCHAR(300) DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numVendorInvoiceBizDocID NUMERIC(18,0) DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null,
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_firstRec INTEGER;                                                              
	v_lastRec INTEGER;
	v_Nocolumns SMALLINT DEFAULT 0;
	v_numFormId INTEGER DEFAULT 135;
	v_IsMasterConfAvailable BOOLEAN DEFAULT false;
	v_numUserGroup NUMERIC(18,0);
	v_strColumns TEXT DEFAULT '';
	v_FROM TEXT DEFAULT '';
	v_WHERE TEXT DEFAULT '';
	v_strSql  TEXT;

	v_SELECT  TEXT DEFAULT CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,COALESCE(Opp.bitStockTransfer,false) bitStockTransfer
										,OI.numoppitemtCode
										,CASE WHEN COALESCE(Opp.bitStockTransfer,false) = true THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END numWarehouseItmsID
										,WI.numWarehouseID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,COALESCE(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN coalesce(v_numVendorInvoiceBizDocID,0) > 0 THEN 'COALESCE(OBDI.numUnitHour,0)' ELSE 'COALESCE(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN coalesce(v_numVendorInvoiceBizDocID,0) > 0 THEN 'COALESCE(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'COALESCE(OI.numUnitHourReceived,0)' END),' numUnitHourReceived
										,COALESCE(Opp.fltExchangeRate,0) AS fltExchangeRate
										,COALESCE(Opp.numCurrencyID,0) AS numCurrencyID
										,COALESCE(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,COALESCE(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,COALESCE(I.numCOGsChartAcntId,0) AS itemCoGs
										,COALESCE(OI.bitDropShip,false) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,COALESCE(OI.numProjectID, 0) numProjectID
										,COALESCE(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,COALESCE(Opp.bitPPVariance,false) AS bitPPVariance
										,COALESCE(I.bitLotNo,false) as bitLotNo
										,COALESCE(I.bitSerialized,false) AS bitSerialized
										,cmp.vcCompanyName');

	v_tintOrder SMALLINT;
	v_vcFieldName VARCHAR(50);
	v_vcListItemType VARCHAR(3);
	v_vcListItemType1 VARCHAR(1);
	v_vcAssociatedControlType VARCHAR(30);
	v_numListID NUMERIC(9,0);
	v_vcDbColumnName VARCHAR(40);
	v_vcLookBackTableName VARCHAR(2000);
	v_bitCustom BOOLEAN;
	v_numFieldId NUMERIC;  
	v_bitAllowSorting BOOLEAN;   
	v_bitAllowEdit BOOLEAN;                 
	v_vcColumnName VARCHAR(500);
	v_ListRelID  NUMERIC(9,0); 
	v_i  INTEGER DEFAULT 1;
	v_iCount  INTEGER;
	v_Prefix  VARCHAR(5);
BEGIN
	DROP TABLE IF EXISTS tt_GetPOItemsForFulfillment CASCADE;

	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                             
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1); 

	IF LENGTH(coalesce(v_columnName,'')) = 0 then
		v_columnName := 'Opp.numOppID';
	end if;
		
	IF LENGTH(coalesce(v_columnSortOrder,'')) = 0 then
		v_columnSortOrder := 'DESC';
	end if;

	BEGIN
		CREATE TEMP SEQUENCE tt_TempForm_seq INCREMENT BY 1 START WITH 1;
		EXCEPTION WHEN OTHERS THEN
			NULL;
	END;

	DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPFORM
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1), 
		tintOrder SMALLINT,
		vcDbColumnName VARCHAR(50),
		vcOrigDbColumnName VARCHAR(50),
		vcFieldName VARCHAR(50),
		vcAssociatedControlType VARCHAR(50),
		vcListItemType VARCHAR(3),
		numListID NUMERIC(9,0),
		vcLookBackTableName VARCHAR(50),
		bitCustomField BOOLEAN,
		numFieldId NUMERIC,
		bitAllowSorting BOOLEAN,
		bitAllowEdit BOOLEAN,
		bitIsRequired BOOLEAN,
		bitIsEmail BOOLEAN,
		bitIsAlphaNumeric BOOLEAN,
		bitIsNumeric BOOLEAN,
		bitIsLengthValidation BOOLEAN,
		intMaxLength INTEGER,
		intMinLength INTEGER,
		bitFieldMessage BOOLEAN,
		vcFieldMessage VARCHAR(500),
		ListRelID NUMERIC(9,0),
		intColumnWidth INTEGER,
		bitAllowFiltering BOOLEAN,
		vcFieldDataType CHAR(1)
	);

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	SELECT 
		COALESCE(SUM(TotalRow),0) INTO v_Nocolumns 
	FROM
	(
		SELECT
			COUNT(*) AS TotalRow
		FROM
			View_DynamicColumns
		WHERE
			numFormId = v_numFormId
			AND numViewID = 0
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
		UNION
		SELECT
			COUNT(*) AS TotalRow
		FROM
			View_DynamicCustomColumns
		WHERE
			numFormId = v_numFormId
			AND numViewID = 0
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
	) TotalRows;

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF v_Nocolumns = 0 then
		select numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;

		IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = v_numFormId AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
			v_IsMasterConfAvailable := true;
		end if;
	end if;

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
   IF v_IsMasterConfAvailable = true then
		INSERT INTO DycFormConfigurationDetails
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT
			v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,0,1,false,intColumnWidth
		FROM
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
			View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
		UNION
		SELECT
			v_numFormId,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,0,1,1,intColumnWidth
		FROM
			View_DynamicCustomColumnsMasterConfig
		WHERE
			View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
			View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;

		INSERT INTO tt_TEMPFORM
		(
			tintOrder, vcDbColumnName, vcOrigDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, bitIsRequired, bitIsEmail, bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, ListRelID, intColumnWidth, bitAllowFiltering, vcFieldDataType
		)
		SELECT
			(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId = v_numFormId AND
			View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
			coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
		UNION
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
		FROM
			View_DynamicCustomColumnsMasterConfig
		WHERE
			View_DynamicCustomColumnsMasterConfig.numFormId = v_numFormId AND
			View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
			coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
		ORDER BY
			tintOrder ASC;
	ELSE
		IF v_Nocolumns = 0 then
			INSERT INTO DycFormConfigurationDetails
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT
				v_numFormId,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,0,1,false,intColumnWidth
			FROM
				View_DynamicDefaultColumns
			WHERE
				numFormId = v_numFormId
				AND bitDefault = true
				AND coalesce(bitSettingField,false) = true
				AND numDomainID = v_numDomainID
			ORDER BY
				tintOrder asc;
		end if;

		INSERT INTO tt_TEMPFORM
		(
			tintOrder,vcDbColumnName,vcOrigDbColumnName,vcFieldName,vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustomField,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
		)
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM
			View_DynamicColumns
		WHERE
			numFormId = v_numFormId
			AND numViewID = 0
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND coalesce(bitSettingField,false) = true
			AND coalesce(bitCustom,false) = false
		UNION
		SELECT
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
		FROM
			View_DynamicCustomColumns
		WHERE
			numFormId = v_numFormId
			AND numViewID = 0
			AND numUserCntID = v_numUserCntID
			AND numDomainID = v_numDomainID
			AND tintPageType = 1
			AND coalesce(bitCustom,false) = true
		ORDER BY
			tintOrder asc;
	end if;

	v_FROM := ' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on (CASE WHEN COALESCE(Opp.bitStockTransfer,false) = true THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END)=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID';

	v_WHERE := CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN coalesce(v_numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - COALESCE(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - COALESCE(OI.numUnitHourReceived,0) > 0' END),'
							AND I.charItemType IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=false or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',v_numDomainID);
	
	IF coalesce(v_numRecordID,0) > 0 then
		IF v_FilterBy = 1 then --Item
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND I.numItemCode = ',v_numRecordID);
		ELSEIF v_FilterBy = 2 then --Warehouse
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND WI.numWarehouseID = ',v_numRecordID);
		ELSEIF v_FilterBy = 3 then --Purchase Order
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND Opp.numOppID = ',v_numRecordID);
		ELSEIF v_FilterBy = 4 then --Vendor
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND DM.numDivisionID = ',v_numRecordID);
		ELSEIF v_FilterBy = 5 then --BizDoc
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',v_numRecordID,')');
		ELSEIF v_FilterBy = 6 then --SKU
			v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND I.numItemCode = ',v_numRecordID);
		end if;
	ELSEIF LENGTH(coalesce(v_Filter,'')) > 0 then
		IF v_FilterBy = 1 then --Item
			v_WHERE := coalesce(v_WHERE,'') || ' and OI.vcItemName like ''%' || coalesce(v_Filter,'') || '%''';
		ELSEIF v_FilterBy = 2 then --Warehouse
			v_WHERE := coalesce(v_WHERE,'') || ' and W.vcWareHouse like ''%' || coalesce(v_Filter,'') || '%''';
		ELSEIF v_FilterBy = 3 then --Purchase Order
			v_WHERE := coalesce(v_WHERE,'') || ' and Opp.vcPOppName like ''%' || coalesce(v_Filter,'') || '%''';
		ELSEIF v_FilterBy = 4 then --Vendor
			v_WHERE := coalesce(v_WHERE,'') || ' and cmp.vcCompanyName LIKE ''%' || coalesce(v_Filter,'') || '%''';
		ELSEIF v_FilterBy = 5 then --BizDoc
			v_WHERE := coalesce(v_WHERE,'') || ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' || coalesce(v_Filter,'') || '%'')';
		ELSEIF v_FilterBy = 6 then --SKU
			v_WHERE := coalesce(v_WHERE,'') || ' and  I.vcSKU LIKE ''%' || coalesce(v_Filter,'') || '%'' OR WI.vcWHSKU LIKE ''%' || coalesce(v_Filter,'') || '%'')';
		end if;
	end if;

	IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
		v_FROM := coalesce(v_FROM,'') || ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode';
		v_WHERE := coalesce(v_WHERE,'') || CONCAT(' AND OBD.numOppBizDocsId = ',v_numVendorInvoiceBizDocID);
	end if;

	v_tintOrder := 0;

	select COUNT(*) INTO v_iCount FROM tt_TEMPFORM;

	WHILE v_i <= v_iCount LOOP
		select 
			tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID 
		INTO 
			v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
		FROM
			tt_TEMPFORM 
		WHERE
			ID = v_i;

		IF v_bitCustom = false then
			IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
			   v_Prefix := 'ADC.';
			end if;
			IF v_vcLookBackTableName = 'DivisionMaster' then
			   v_Prefix := 'DM.';
			end if;
			IF v_vcLookBackTableName = 'OpportunityMaster' then
			   v_Prefix := 'Opp.';
			end if;
			IF v_vcLookBackTableName = 'OpportunityItems' then
			   v_Prefix := 'OI.';
			end if;
			IF v_vcLookBackTableName = 'Item' then
			   v_Prefix := 'I.';
			end if;
			IF v_vcLookBackTableName = 'WareHouseItems' then
			   v_Prefix := 'WI.';
			end if;
			IF v_vcLookBackTableName = 'Warehouses' then
			   v_Prefix := 'W.';
			end if;
			IF v_vcLookBackTableName = 'WarehouseLocation' then
			   v_Prefix := 'WL.';
			end if;
			IF v_vcLookBackTableName = 'CompanyInfo' then
			   v_Prefix := 'cmp.';
			end if;
			
			v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         
			IF v_vcAssociatedControlType = 'SelectBox' then
				IF v_vcDbColumnName = 'numPartner' then
				   v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) ' || ' "' || coalesce(v_vcColumnName,'') || '"';
				   v_strColumns := coalesce(v_strColumns,'') || ' ,(SELECT CONCAT(D.vcPartnerCode,''-'',C.vcCompanyName) FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner';
				ELSEIF v_vcDbColumnName = 'tintSource' then
					v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(fn_GetOpportunitySourceValue(COALESCE(Opp.tintSource,0),COALESCE(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' || ' "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcDbColumnName = 'numContactID' then
					v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcDbColumnName = 'vcLocation' then
					v_strColumns := coalesce(v_strColumns,'') || ',CONCAT(W.vcWarehouse,CASE WHEN LENGTH(COALESCE(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcListItemType = 'LI' then
					v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
					v_FROM := coalesce(v_FROM,'') || ' LEFT JOIN ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
				ELSEIF v_vcListItemType = 'U' then
					v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcListItemType = 'SGT' then
					v_strColumns := coalesce(v_strColumns,'') || ',(CASE CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS NUMERIC)
															WHEN 0 THEN ''Service Default''
															WHEN 1 THEN ''Adult Signature Required''
															WHEN 2 THEN ''Direct Signature''
															WHEN 3 THEN ''InDirect Signature''
															WHEN 4 THEN ''No Signature Required''
															ELSE ''''
															END) "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcListItemType = 'PSS' then
				   v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR COALESCE(numDomainID,0)=0) AND numShippingServiceID = ' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '),'''') "' || coalesce(v_vcColumnName,'') || '"';
				end if;
			ELSEIF v_vcAssociatedControlType = 'DateField' then
				IF v_vcDbColumnName = 'dtReleaseDate' then
					v_strColumns := coalesce(v_strColumns,'')
					|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE) = CAST(now() + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) then ''<b><font color=red>Today</font></b>''';

					v_strColumns := coalesce(v_strColumns,'')
					|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' AS DATE)= CAST(now() + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
					
					v_strColumns := coalesce(v_strColumns,'')
					|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') = CAST(now() + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
               
					v_strColumns := coalesce(v_strColumns,'')
					|| 'else FormatedDateFromDate('
					|| coalesce(v_Prefix,'')
					|| coalesce(v_vcDbColumnName,'')
					|| ','
					|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
					|| ') end  "'
					|| coalesce(v_vcColumnName,'') || '"';
				ELSE
					v_strColumns := coalesce(v_strColumns,'')
					|| ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
					v_strColumns := coalesce(v_strColumns,'')
					|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>Yesterday</font></b>''';
					v_strColumns := coalesce(v_strColumns,'')
					|| 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins  => -' || -COALESCE(v_ClientTimeZoneOffset,0) || ') AS DATE) = CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
					v_strColumns := coalesce(v_strColumns,'')
					|| 'else FormatedDateFromDate('
					|| coalesce(v_Prefix,'')
					|| coalesce(v_vcDbColumnName,'')
					|| ','
					|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
					|| ') end  "'
					|| coalesce(v_vcColumnName,'') || '"';
				end if;
			ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
				IF v_vcDbColumnName = 'numShipState' then
					v_strColumns := coalesce(v_strColumns,'') || ',fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' || ' "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcDbColumnName = 'numQtyToShipReceive' then				
					IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then					
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OBDI.numUnitHour,0) - COALESCE(OBDI.numVendorInvoiceUnitReceived,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
					ELSE
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OI.numUnitHour,0) - COALESCE(OI.numUnitHourReceived,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
					end if;
				ELSEIF v_vcDbColumnName = 'vcPathForTImage' then
					v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(II.vcPathForTImage,'''') AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
					v_FROM := coalesce(v_FROM,'') || ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = true';
				ELSEIF v_vcDbColumnName = 'SerialLotNo' then
					v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT string_agg(CONCAT(vcSerialNo,CASE WHEN COALESCE(I.bitLotNo,false)=true THEN CONCAT('' ('',whi.numQty,'')'') ELSE '''' end),'','' ORDER BY vcSerialNo) 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode),'''')  AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcDbColumnName = 'numUnitHour' then
					IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OBDI.numUnitHour,0) "' || coalesce(v_vcColumnName,'') || '"';
				   ELSE
					  v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',0) "' || coalesce(v_vcColumnName,'') || '"';
				   end if;
				ELSEIF v_vcDbColumnName = 'numUnitHourReceived' then
					IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OBDI.numVendorInvoiceUnitReceived,0) "' || coalesce(v_vcColumnName,'') || '"';
					ELSE
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',0) "' || coalesce(v_vcColumnName,'') || '"';
					end if;
				ELSEIF v_vcDbColumnName = 'vcSKU' then
				   v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OI.vcSKU,COALESCE(I.vcSKU,'''')) "' || coalesce(v_vcColumnName,'') || '"';
				ELSEIF v_vcDbColumnName = 'vcItemName' then
				   v_strColumns := coalesce(v_strColumns,'') || ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  "' || coalesce(v_vcColumnName,'') || '"';
				ELSE
				   v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
				end if;
			ELSEIF v_vcAssociatedControlType = 'Label' then
				IF v_vcDbColumnName = 'numRemainingQty' then
					IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OBDI.numUnitHour,0) - COALESCE(OBDI.numVendorInvoiceUnitReceived,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
					ELSE
						v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(OI.numUnitHour,0) - COALESCE(OI.numUnitHourReceived,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
					end if;
				ELSE
					v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
				end if;
			 end if;
		ELSE
			v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         
			IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Value  "'
				|| coalesce(v_vcColumnName,'') || '"';

				v_FROM := coalesce(v_FROM,'')
				|| ' left Join CFW_FLD_Values_Opp CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| ' on CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Id='
				|| SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
				|| 'and CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.RecId=Opp.numOppid ';
			ELSEIF v_vcAssociatedControlType = 'CheckBox' then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',case when COALESCE(CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Value,0)=0 then 0 when COALESCE(CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Value,0)=1 then 1 end "'
				|| coalesce(v_vcColumnName,'') || '"';

				v_FROM := coalesce(v_FROM,'')
				|| ' left Join CFW_FLD_Values_Opp CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| ' on CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Id='
				|| SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
				|| 'and CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.RecId=Opp.numOppid   ';
			ELSEIF v_vcAssociatedControlType = 'DateField' then
				v_strColumns := coalesce(v_strColumns,'')
				|| ',FormatedDateFromDate(CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Value,'
				|| SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
				|| ') "'
				|| coalesce(v_vcColumnName,'') || '"';
				v_FROM := coalesce(v_FROM,'')
				|| ' left Join CFW_FLD_Values_Opp CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| ' on CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Id='
				|| SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
				|| 'and CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.RecId=Opp.numOppid ';
			ELSEIF v_vcAssociatedControlType = 'SelectBox' then
				v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);

				v_strColumns := coalesce(v_strColumns,'')
				|| ',L'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.vcData'
				|| ' "'
				|| coalesce(v_vcColumnName,'') || '"';

				v_FROM := coalesce(v_FROM,'')
				|| ' left Join CFW_FLD_Values_Opp CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| ' on CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Id='
				|| SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
				|| 'and CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.RecId=Opp.numOppid ';

				v_FROM := coalesce(v_FROM,'')
				|| ' left Join ListDetails L'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| ' on L'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.numListItemID=CFW'
				|| SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
				|| '.Fld_Value';
			ELSE
				v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValueOpp(',v_numFieldId,',opp.numOppID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
			end if;
		end if;
      
		v_i := v_i::bigint+1;
	END LOOP;

	v_strSql := 'CREATE TEMPORARY TABLE tt_GetPOItemsForFulfillment AS ' || coalesce(v_SELECT,'') || coalesce(v_strColumns,'') || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ') SELECT * FROM POItems WHERE row > ' || COALESCE(v_firstRec,0) || ' and row <' || COALESCE(v_lastRec,0);

	RAISE NOTICE '%',CAST(v_strSql AS TEXT);

	EXECUTE v_strSql;

	open SWV_RefCur for EXECUTE 'SELECT * FROM tt_GetPOItemsForFulfillment ORDER BY row;';

	EXECUTE '(SELECT COUNT(*) ' || coalesce(v_FROM,'') || coalesce(v_WHERE,'') || ')' INTO v_TotRecs; 

	open SWV_RefCur2 for
	SELECT * FROM tt_TEMPFORM;

	RETURN;
END; $$;


