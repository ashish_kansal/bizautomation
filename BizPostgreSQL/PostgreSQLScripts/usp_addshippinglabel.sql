-- Stored procedure definition script USP_AddShippingLabel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddShippingLabel(v_ShippingReportItemId NUMERIC(18,0),
                v_vcShippingLabelImage VARCHAR(100),
                v_vcTrackingNumber     VARCHAR(50),
                v_numUserCntId         NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM   ShippingLabel WHERE  ShippingReportItemId = v_ShippingReportItemId) > 0 then
      
      UPDATE ShippingLabel
      SET    vcShippingLabelImage = v_vcShippingLabelImage,vcTrackingNumber = v_vcTrackingNumber
      WHERE  ShippingReportItemId = v_ShippingReportItemId;
   ELSE
      INSERT INTO ShippingLabel(ShippingReportItemId,
                    vcShippingLabelImage,
                    vcTrackingNumber,
                    dtCreateDate,
                    numCreatedBy)
        VALUES(v_ShippingReportItemId,
                    v_vcShippingLabelImage,
                    v_vcTrackingNumber,
                    TIMEZONE('UTC',now()),
                    v_numUserCntId);
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/



