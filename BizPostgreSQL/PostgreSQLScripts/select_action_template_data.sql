-- Stored procedure definition script Select_Action_Template_Data for PostgreSQL
CREATE OR REPLACE FUNCTION Select_Action_Template_Data(v_rowID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From tblActionItemData Where RowID = v_rowID;
END; $$;













