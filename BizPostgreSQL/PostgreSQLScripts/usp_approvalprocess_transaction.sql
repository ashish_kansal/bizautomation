-- Stored procedure definition script USP_ApprovalProcess_Transaction for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ApprovalProcess_Transaction(v_chrAction CHAR(10),
	v_numDomainID NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
    v_numModuleId NUMERIC(9,0),
    v_numRecordId NUMERIC(9,0),
    v_numApprovedBy NUMERIC(9,0),
	v_numApprovalComplete NUMERIC(9,0),
	INOUT v_vchoutput VARCHAR(100))
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRUserId  NUMERIC;
   v_num1stApproval  NUMERIC;
   v_num2ndApproval  NUMERIC;
   v_num3rdApproval  NUMERIC;
   v_num4thApproval  NUMERIC;
   v_num5thApproval  NUMERIC;
   v_DealStatus  NUMERIC(18,0);  
   v_tintOppStatus  SMALLINT;
   v_tintOppType  SMALLINT;
   v_tintShipped  SMALLINT; 
   v_numDivisionID  NUMERIC(18,0);  
   v_tintCommitAllocation  SMALLINT;
				      
   v_tintCRMType  NUMERIC(9,0);        
   v_AccountClosingDate  TIMESTAMP;
   
	
BEGIN
v_numDomainID := 0;
    v_numUserCntID  := 0;
    v_numModuleId  := 0;
    v_numRecordId  := 0;
    v_numApprovedBy  := 0;
	v_numApprovalComplete  := 0;
	
   IF(v_chrAction = 'UT') then--Time Expense Approval
	
      UPDATE
      timeandexpense
      SET
      numApprovalComplete = v_numApprovalComplete
      WHERE
      numCategoryHDRID = v_numRecordId;
      INSERT INTO Approval_transaction_log(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES(v_numDomainID,v_numModuleId,v_numRecordId,v_numApprovedBy,LOCALTIMESTAMP);
   end if;

   IF(v_chrAction = 'UTP') then--Promotion Approval
	
      UPDATE
      OpportunityMaster
      SET
      intPromotionApprovalStatus = v_numApprovalComplete
      WHERE
      numOppId = v_numRecordId;
      IF(v_numApprovalComplete = 0) then
         SELECT  intChangePromotionStatus INTO v_DealStatus FROM OpportunityMaster WHERE numOppId = v_numRecordId     LIMIT 1;
         UPDATE OpportunityMaster SET tintoppstatus = v_DealStatus WHERE numOppId = v_numRecordId;
         select   tintoppstatus, tintopptype, tintshipped, coalesce(tintCommitAllocation,1) INTO v_tintOppStatus,v_tintOppType,v_tintShipped,v_tintCommitAllocation from OpportunityMaster INNER JOIN Domain ON OpportunityMaster.numDomainId = Domain.numDomainId where numOppId = v_numRecordId;
         if v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
				
            PERFORM USP_UpdatingInventoryonCloseDeal(v_numRecordId,0,v_numUserCntID);
         end if;
         select   numDivisionId, bintAccountClosingDate INTO v_numDivisionID,v_AccountClosingDate from  OpportunityMaster where numOppId = v_numRecordId;
         select   tintCRMType INTO v_tintCRMType from DivisionMaster where numDivisionID = v_numDivisionID; 

				--When deal is won                                         
         if v_DealStatus = 1 then
				
            if v_AccountClosingDate is null then
               update OpportunityMaster set bintAccountClosingDate = TIMEZONE('UTC',now()) where numOppId = v_numRecordId;
            end if;         
				--When Deal is Lost
         ELSEIF v_DealStatus = 2
         then
				
            select   bintAccountClosingDate INTO v_AccountClosingDate from  OpportunityMaster where numOppId = v_numRecordId;
            if v_AccountClosingDate is null then
               update OpportunityMaster set bintAccountClosingDate = TIMEZONE('UTC',now()) where numOppId = v_numRecordId;
            end if;
         end if;                    

				/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
				-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
				
				--Promote Prospect to Account
         if v_tintCRMType = 0 AND v_DealStatus = 0 then --Lead & Open Opp
				
            update DivisionMaster set tintCRMType = 1,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
            bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
            where numDivisionID = v_numDivisionID;        
				-- Promote Lead to Account when Sales/Purchase Order is created against it
         ELSEIF v_tintCRMType = 0 AND v_DealStatus = 1
         then --Lead & Order
				
            update DivisionMaster set tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
            bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
            where numDivisionID = v_numDivisionID;
         ELSEIF v_tintCRMType = 1 AND v_DealStatus = 1
         then
				
            update DivisionMaster set tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
            where numDivisionID = v_numDivisionID;
         end if;
      end if;
      INSERT INTO Approval_transaction_log(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES(v_numDomainID,v_numModuleId,v_numRecordId,v_numApprovedBy,LOCALTIMESTAMP);
   end if;

   IF(v_chrAction = 'CF') then
	
      IF(v_numRecordId > 0) then
		
         SELECT  numUserCntID INTO v_numRUserId FROM timeandexpense WHERE numCategoryHDRID = v_numRecordId     LIMIT 1;
      ELSE
         v_numRUserId := v_numUserCntID;
      end if;
      select   numLevel1Authority, numLevel2Authority, numLevel3Authority, numLevel4Authority, numLevel5Authority INTO v_num1stApproval,v_num2ndApproval,v_num3rdApproval,v_num4thApproval,v_num5thApproval FROM
      ApprovalConfig WHERE
      numUserId = v_numRUserId AND numModule = 1    LIMIT 1;
      IF(coalesce(v_num1stApproval,0) = 0 and coalesce(v_num2ndApproval,0) = 0 and coalesce(v_num3rdApproval,0) = 0 and coalesce(v_num4thApproval,0) = 0 and coalesce(v_num5thApproval,0) = 0) then
		
         v_vchoutput := 'INVALID';
      ELSE
         v_vchoutput := 'VALID';
      end if;
   end if;

   IF(v_chrAction = 'CH') then
	
      IF(v_numRecordId > 0) then
		
         SELECT  numUserCntID INTO v_numRUserId FROM timeandexpense WHERE numCategoryHDRID = v_numRecordId     LIMIT 1;
      ELSE
         v_numRUserId := v_numUserCntID;
      end if;
      select   numLevel1Authority, numLevel2Authority, numLevel3Authority, numLevel4Authority, numLevel5Authority INTO v_num1stApproval,v_num2ndApproval,v_num3rdApproval,v_num4thApproval,v_num5thApproval FROM
      ApprovalConfig WHERE
      numUserId = v_numRUserId AND numModule = 1    LIMIT 1;
      IF(coalesce(v_num1stApproval,0) = 0 and v_numApprovalComplete = 1) then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num2ndApproval,0) = 0 and v_numApprovalComplete = 2)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num3rdApproval,0) = 0 and v_numApprovalComplete = 3)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num4thApproval,0) = 0 and v_numApprovalComplete = 4)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num5thApproval,0) = 0 and v_numApprovalComplete = 5)
      then
		
         v_vchoutput := 'INVALID';
      ELSE
         v_vchoutput := 'VALID';
      end if;
   end if;

   IF(v_chrAction = 'CHP') then--Check Promotion Approver Authority
	
      IF(v_numRecordId > 0) then
		
         SELECT  numCreatedBy INTO v_numRUserId FROM OpportunityMaster WHERE numOppId = v_numRecordId     LIMIT 1;
      ELSE
         v_numRUserId := v_numUserCntID;
      end if;
      select   numLevel1Authority, numLevel2Authority, numLevel3Authority, numLevel4Authority, numLevel5Authority INTO v_num1stApproval,v_num2ndApproval,v_num3rdApproval,v_num4thApproval,v_num5thApproval FROM
      ApprovalConfig WHERE
      numUserId = v_numRUserId AND numModule = 2    LIMIT 1;
      IF(coalesce(v_num1stApproval,0) = 0 and v_numApprovalComplete = 1) then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num2ndApproval,0) = 0 and v_numApprovalComplete = 2)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num3rdApproval,0) = 0 and v_numApprovalComplete = 3)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num4thApproval,0) = 0 and v_numApprovalComplete = 4)
      then
		
         v_vchoutput := 'INVALID';
      ELSEIF (coalesce(v_num5thApproval,0) = 0 and v_numApprovalComplete = 5)
      then
		
         v_vchoutput := 'INVALID';
      ELSE
         v_vchoutput := 'VALID';
      end if;
   end if;
   RETURN;
END; $$;


