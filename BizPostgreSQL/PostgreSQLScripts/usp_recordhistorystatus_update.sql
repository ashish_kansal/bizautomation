-- Stored procedure definition script USP_RecordHistoryStatus_Update for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecordHistoryStatus_Update(v_bitSuccess BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   RecordHistoryStatus
   SET
   dtLastExecuted = LOCALTIMESTAMP,intNoOfTimesTried =(CASE WHEN v_bitSuccess = true THEN 0 ELSE intNoOfTimesTried+1 END),bitSuccess = v_bitSuccess
   WHERE
   Id = 1;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/



