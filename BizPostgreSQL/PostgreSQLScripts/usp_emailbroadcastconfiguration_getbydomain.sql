-- Stored procedure definition script USP_EmailBroadcastConfiguration_GetByDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EmailBroadcastConfiguration_GetByDomain(v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM EmailBroadcastConfiguration WHERE numDomainID = v_numDomainID;
END; $$;  












