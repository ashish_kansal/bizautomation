-- Stored procedure definition script usp_GetListDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetListDetails(v_vcListName VARCHAR(100) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numListId  NUMERIC;
BEGIN
	select   numListID INTO v_numListId from listmaster where vcListName = v_vcListName;
	open SWV_RefCur for 
	Select 
		numListItemID AS "numListItemID", 
		vcData as "vcData" 
	from 
		Listdetails 
	where 
		numListID = v_numListId;
END; $$;












