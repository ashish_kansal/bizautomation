
CREATE OR REPLACE FUNCTION usp_DeleteContactGroupList(v_strcontact TEXT DEFAULT null,    
  v_numDomainID NUMERIC DEFAULT NULL,  
  v_numUserCntID NUMERIC DEFAULT NULL,  
  v_numGroupID NUMERIC DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	if  v_numGroupID <> 0 then
		if COALESCE(v_strcontact,'') <> '' then
			delete FROM ProfileEGroupDTL where numContactId not in (select X.numContactId FROM XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strcontact AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numContactId NUMERIC(18,0) PATH 'numContactId'
			) X) and numEmailGroupID =  v_numGroupID;
		end if;
	end if;

	RETURN;
END; $$;


