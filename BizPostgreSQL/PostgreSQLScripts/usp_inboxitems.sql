DROP FUNCTION IF EXISTS USP_InboxItems;

Create or replace FUNCTION USP_InboxItems(v_PageSize INTEGER ,                            
v_CurrentPage INTEGER,                            
v_srch VARCHAR(100) DEFAULT '',                                                              
v_ToEmail VARCHAR(100) DEFAULT NULL  ,        
v_columnName VARCHAR(50) DEFAULT NULL ,        
v_columnSortOrder VARCHAR(4) DEFAULT NULL  ,  
v_numDomainId NUMERIC(9,0) DEFAULT NULL,  
v_numUserCntId NUMERIC(9,0) DEFAULT NULL  ,    
v_numNodeId NUMERIC(9,0) DEFAULT NULL,  
--@chrSource as char(1),
v_ClientTimeZoneOffset INTEGER DEFAULT 0,
v_tintUserRightType INTEGER DEFAULT NULL,
v_EmailStatus NUMERIC(9,0) DEFAULT 0,
v_srchFrom VARCHAR(100) DEFAULT '', 
v_srchTo VARCHAR(100) DEFAULT '', 
v_srchSubject VARCHAR(100) DEFAULT '', 
v_srchHasWords VARCHAR(100) DEFAULT '', 
v_srchHasAttachment BOOLEAN DEFAULT false, 
v_srchIsAdvancedsrch BOOLEAN DEFAULT false,
v_srchInNode NUMERIC(9,0) DEFAULT 0,
v_FromDate DATE DEFAULT NULL,
v_ToDate DATE DEFAULT NULL,
v_dtSelectedDate DATE DEFAULT NULL,
v_bitExcludeEmailFromNonBizContact BOOLEAN DEFAULT false,
 v_vcRegularSearchCriteria TEXT DEFAULT '',
 v_vcCustomSearchCriteria TEXT DEFAULT '' ,
 v_intContactFilterType INTEGER DEFAULT 0,
 v_vcFollowupStatusIds VARCHAR(600) DEFAULT '',
 v_numRelationShipId NUMERIC(18,0) DEFAULT 0,
 v_numProfileId NUMERIC(18,0) DEFAULT -0,
 v_bitGroupBySubjectThread BOOLEAN DEFAULT false,
 INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
   AS $$
   DECLARE
   v_bitOpenActionItem  BOOLEAN DEFAULT true;
   v_bitOpencases  BOOLEAN DEFAULT true;
   v_bitOpenProject  BOOLEAN DEFAULT true;
   v_bitOpenSalesOpp  BOOLEAN DEFAULT true;
   v_bitBalancedue  BOOLEAN DEFAULT true;
   v_bitUnreadEmail  BOOLEAN DEFAULT true;
   v_bitCampaign  BOOLEAN DEFAULT true;

   v_tintOrder  SMALLINT;                                                      
   v_vcFieldName  VARCHAR(50);                                                      
   v_vcListItemType  VARCHAR(10);                                                 
   v_vcAssociatedControlType  VARCHAR(20);                                                      
   v_numListID  NUMERIC(9,0);                                                      
   v_vcDbColumnName  VARCHAR(20);                          
   v_WhereCondition  VARCHAR(2000);                           
   v_vcLookBackTableName  VARCHAR(2000);                    
   v_bitCustom  BOOLEAN;
   v_bitAllowEdit BOOLEAN;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;              
   v_bitAllowFiltering BOOLEAN;              
   v_vcColumnName  VARCHAR(500);  

   v_strSql  VARCHAR(5000);
   v_column  VARCHAR(50);                             
   v_join  VARCHAR(400); 
   v_Nocolumns  SMALLINT;               
   v_lastRec  INTEGER; 
   v_firstRec  INTEGER;                   
   v_vcSorting  VARCHAR(500) DEFAULT '';   
   v_ListRelID  NUMERIC(9,0); 

   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_fltTotalSize  DOUBLE PRECISION;
   v_lookbckTable  VARCHAR(50);
   v_strCondition  VARCHAR(2000);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP AS
      SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount  FROM VIEW_Email_Alert_Config WHERE numDomainid = v_numDomainId;

/************* START - GET ALERT CONFIGURATION **************/
   select   coalesce(bitOpenActionItem,true), coalesce(bitOpenCases,true), coalesce(bitOpenProject,true), coalesce(bitOpenSalesOpp,true), coalesce(bitBalancedue,true), coalesce(bitUnreadEmail,true), coalesce(bitCampaign,true) INTO v_bitOpenActionItem,v_bitOpencases,v_bitOpenProject,v_bitOpenSalesOpp,
   v_bitBalancedue,v_bitUnreadEmail,v_bitCampaign FROM
   AlertConfig WHERE
   AlertConfig.numDomainID = v_numDomainId AND
   AlertConfig.numContactId = v_numUserCntId;

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
   v_join := '';           
   v_strSql := '';
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
   v_tintOrder := 0;  
   v_WhereCondition := '';

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);   
   v_column := v_columnName;                
--DECLARE @join AS VARCHAR(400)                    
   v_join := '';             
   IF v_columnName ilike 'Cust%' then
    
      v_join := ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
      || REPLACE(v_columnName,'Cust','') || ' ';
      v_column := 'CFW.Fld_Value';
   ELSEIF v_columnName ilike 'DCust%'
   then
        
      v_join := ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
      || REPLACE(v_columnName,'DCust','');
      v_join := coalesce(v_join,'')
      || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  ';
      v_column := 'LstCF.vcData';
   ELSE
      v_lookbckTable := '';
      select   vcLookBackTableName INTO v_lookbckTable FROM    View_DynamicDefaultColumns WHERE   numFormId = 44
      AND vcDbColumnName = v_columnName  and numDomainID = v_numDomainId;
      IF v_lookbckTable <> '' then
                
         IF v_lookbckTable = 'EmailHistory' then
            v_column := 'ADC.' || coalesce(v_column,'');
         end if;
      end if;
   end if;          
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)  
   IF coalesce(v_bitGroupBySubjectThread,false) = true then

      v_vcSorting := coalesce(v_vcSorting,'') || ' ORDER BY EH.dtReceivedOn,EH.vcSubject ' || coalesce(v_columnSortOrder,'') || ' ';
   ELSE
      v_vcSorting := coalesce(v_vcSorting,'') || ' ORDER BY EH.dtReceivedOn  ' || coalesce(v_columnSortOrder,'') || ' ';
   end if;
   v_strSql := 'with FilterRows as (SELECT ';            
   v_strSql := coalesce(v_strSql,'') || 'ROW_NUMBER() OVER(ORDER BY EH.dtReceivedOn ' || coalesce(v_columnSortOrder,'') || ' ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,EH.vcSubject,COALESCE(EM.numContactId,0) as numContactId';
   v_strSql := coalesce(v_strSql,'') || ' FROM EmailHistory EH JOIN EmailMaster EM on EM.numEMailId=(EH.numEmailId) LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = EM.numContactId
	left JOIN DivisionMaster DMI ON ACI.numDivisionId = DMI.numDivisionID
	 left JOIN CompanyInfo CIM ON DMI.numCompanyID = CIM.numCompanyId ';
   v_strSql := coalesce(v_strSql,'') || ' 
WHERE EH.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || ' And numUserCntId = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(30)),1,30); 

   IF LENGTH(coalesce(v_srch,'')) = 0 AND v_dtSelectedDate IS NOT NULL AND LENGTH(v_dtSelectedDate:: TEXT) > 0 then

      v_strSql := CONCAT(coalesce(v_strSql,''),' AND CAST(EH.dtReceivedOn + make_interval(mins => ',SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30),' * -1) AS DATE) = ''',SUBSTR(CAST(v_dtSelectedDate AS VARCHAR(30)),1,30),'''',' ');
   end if;

   v_strSql := coalesce(v_strSql,'') || ' AND chrSource IN(''B'',''I'') ';

   IF coalesce(v_bitExcludeEmailFromNonBizContact,false) = true then

      v_strSql := coalesce(v_strSql,'') || ' AND COALESCE(EM.numContactId,0) > 0 ';
   end if;
   IF coalesce(v_intContactFilterType,0) = 1 then

      v_strSql := coalesce(v_strSql,'') || ' AND COALESCE(EM.numContactId,0) > 0 ';
   end if;
   IF coalesce(v_intContactFilterType,0) = 2 then

      v_strSql := coalesce(v_strSql,'') || ' AND COALESCE(EM.numContactId,0) = 0 ';
   end if;
   IF LENGTH(v_vcFollowupStatusIds) > 0 AND coalesce(v_vcFollowupStatusIds,'0') <> '0' then

      v_strSql := CONCAT(coalesce(v_strSql,''),' AND DMI.numFollowUpStatus IN(SELECT Items FROM Split(''',SUBSTR(CAST(v_vcFollowupStatusIds AS VARCHAR(500)),1,500),''','','') WHERE LENGTH(Items)>0 AND COALESCE(Items,0)<>''0'') ');
   end if;
   IF coalesce(v_numRelationShipId,0) > 0 then

      v_strSql := CONCAT(coalesce(v_strSql,''),' AND CIM.numCompanyType=''' , SUBSTR(CAST(v_numRelationShipId AS VARCHAR(500)),1,500) , ''' ');
   end if;
   IF coalesce(v_numProfileId,0) > 0 then

      v_strSql := CONCAT(coalesce(v_strSql,''),' AND CIM.vcProfile=''',SUBSTR(CAST(v_numProfileId AS VARCHAR(500)),1,500) , ''' ');
   end if;
--Simple Search for All Node
   if LENGTH(v_srch) > 0 then
	
      v_strSql := coalesce(v_strSql,'') || '
			AND (EH.vcSubject ILIKE ''%'' || ''' || coalesce(v_srch,'') || ''' || ''%'' or EH.vcBodyText ILIKE ''%'' || ''' || coalesce(v_srch,'') || ''' || ''%''
			or EH.vcFrom ILIKE ''%'' || ''' || coalesce(v_srch,'') || ''' || ''%'' or EH.vcTo ILIKE ''%'' || ''' || coalesce(v_srch,'') || ''' || ''%'')';
   end if;
   IF(coalesce(v_bitGroupBySubjectThread,false) = false) then
	 --Particular Node
      v_strSql := coalesce(v_strSql,'') || ' AND EH.numNodeId= ' || SUBSTR(CAST(v_numNodeId AS VARCHAR(30)),1,30) || ' ';
   end if;
   IF coalesce(v_bitGroupBySubjectThread,false) = true then

      v_strSql := coalesce(v_strSql,'') || ' AND EH.numNodeId IN(SELECT numNodeId FROM InboxTreeSort WHERE numUserCntID=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(500)),1,500) || ' AND numDomainID=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(500)),1,500) || ' AND vcNodeName IN(''Inbox'',''Sent Mail'')) AND
		
	 (SELECT COUNT(numEmailHstrID) FROM EmailHistory AS tempEmails WHERE (tempEmails.vcSubject=EH.vcSubject OR REPLACE(tempEmails.vcSubject,''Re: '','''')=REPLACE(EH.vcSubject,''Re: '','''')) 
	 AND tempEmails.numEmailHstrID<>EH.numEmailHstrID AND tempEmails.vcTo=EH.vcFrom AND numDomainID=''' || SUBSTR(CAST(v_numDomainId AS VARCHAR(500)),1,500) || ''' AND numUserCntId=''' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(500)),1,500) || ''')>0 ';
--Advanced Search for All Node or selected Node
   ELSEIF v_srchIsAdvancedsrch = true
   then

      v_strCondition := '';
      if LENGTH(v_srchSubject) > 0 then
         v_strCondition := 'EH.vcSubject ILIKE ''%'' || ''' || coalesce(v_srchSubject,'') || ''' || ''%''';
      end if;
      if LENGTH(v_srchFrom) > 0 then
	
         if LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' and ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' (EH.vcFrom ILIKE ''%' || REPLACE(v_srchFrom,',','%'' OR EH.vcFrom ILIKE ''%') || '%'')';
      end if;
      if LENGTH(v_srchTo) > 0 then
	
         if LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' and ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' (EH.vcTo ILIKE ''%' || REPLACE(v_srchTo,',','%'' OR EH.vcTo ILIKE ''%') || '%'')';
      end if;
      if LENGTH(v_srchHasWords) > 0 then
	
         if LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' and ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' EH.vcBodyText ILIKE ''%'' || ''' || coalesce(v_srchHasWords,'') || ''' || ''%''';
      end if;
      IF v_srchHasAttachment = true then

         if LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' and ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' EH.bitHasAttachments =  ''' || SUBSTR(CAST(v_srchHasAttachment AS VARCHAR(30)),1,30) || '''';
      end if;
      IF (LENGTH(v_FromDate:: TEXT) > 0 AND  swf_isdate(SUBSTR(CAST(v_FromDate AS VARCHAR(100)),1,100):: text) = 1) then

         IF LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' AND ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' EH.dtReceivedOn >= ''' || SUBSTR(CAST(v_FromDate AS VARCHAR(30)),1,30) || '''';
      end if;
      IF (LENGTH(v_ToDate:: TEXT) > 0 AND swf_isdate(SUBSTR(CAST(v_ToDate AS VARCHAR(100)),1,100):: text) = 1) then

         IF LENGTH(v_strCondition) > 0 then
            v_strCondition := coalesce(v_strCondition,'') || ' AND ';
         end if;
         v_strCondition := coalesce(v_strCondition,'') || ' EH.dtReceivedOn <= ''' || SUBSTR(CAST(v_ToDate AS VARCHAR(30)),1,30) || '''';
      end if;
      if LENGTH(v_strCondition) > 0 then
	
         v_strSql := coalesce(v_strSql,'') || ' and (' || coalesce(v_strCondition,'') || ')';
         if v_numNodeId > -1 then
            v_strSql := coalesce(v_strSql,'') || ' AND EH.numEmailHstrID IN(SELECT numEmailHstryId FROM EmailNodeRelationship WHERE numNodeId= ' || SUBSTR(CAST(v_numNodeId AS VARCHAR(30)),1,30) || ' AND EH.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || ' And numUserCntId = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(30)),1,30) || ') ';
         end if;
      ELSE --Particular Node
         v_strSql := coalesce(v_strSql,'') || ' AND EH.numEmailHstrID IN(SELECT numEmailHstryId FROM EmailNodeRelationship WHERE numNodeId= ' || SUBSTR(CAST(v_numNodeId AS VARCHAR(30)),1,30) || ' AND EH.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || ' And numUserCntId = ' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(30)),1,30) || ') ';
      end if;
   end if;


   IF v_vcRegularSearchCriteria <> '' then

      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;
 
   IF v_vcCustomSearchCriteria <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
   end if;

   v_strSql := coalesce(v_strSql,'') || ' LIMIT ' || SUBSTR(CAST(v_CurrentPage AS VARCHAR(30)),1,30) || ' * ' || SUBSTR(CAST(v_PageSize AS VARCHAR(30)),1,30) || ')';

   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = 44 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 44 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0) TotalRows;

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType CHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0)
   );


---Insert number of rows into temp table.
   IF  v_Nocolumns > 0 then
    
      INSERT INTO tt_TEMPFORM
      select tintRow+1 as tintOrder,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID
      FROM View_DynamicColumns
      where numFormId = 44 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0
      AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
      UNION
      select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,false AS bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
      from View_DynamicCustomColumns
      where numFormId = 44 and numUserCntID = v_numUserCntId and numDomainID = v_numDomainId AND tintPageType = 1 AND numRelCntType = 0
      AND coalesce(bitCustom,false) = true
      ORDER BY tintOrder ASC;
   ELSE
      INSERT INTO tt_TEMPFORM
      select tintOrder,vcDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,CASE WHEN bitCustom = 1 THEN true ELSE false END,numFieldId,bitAllowSorting,bitAllowFiltering,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID
      FROM View_DynamicDefaultColumns
      where numFormId = 44 and bitDefault = true AND coalesce(bitSettingField,false) = true AND numDomainID = v_numDomainId
      order by tintOrder asc;
   end if;

   v_strSql := coalesce(v_strSql,'') || ' Select TotalRowCount, COALESCE(EH.numNoofTimes,0)  AS NoOfTimeOpened, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LENGTH(Cast(EH.vcBodyText AS VARCHAR(1000)))>140 THEN SUBSTRING(EH.vcBodyText,0,140) || ''...'' ELSE
		 EH.vcBodyText END as vcBodyText ,COALESCE(EH.IsReplied,false) As IsReplied,FR.numContactId,EH.dtReceivedOn,CIM.vcCompanyName,EH.vcFrom,EH.vcTo,EH.vcSubject,
		 COALESCE(EH.numNoofTimes,0) AS numNoofTimes ';

   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowFiltering, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowFiltering,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'EmailHistory' then
            v_Prefix := 'EH.';
         end if;
         v_vcColumnName := coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'')
         || '~' || (CASE WHEN COALESCE(v_bitAllowSorting,false) = true THEN 1 ELSE 0 END) || '~'
         || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~'
         || (CASE WHEN COALESCE(v_bitAllowEdit,false) = true THEN 1 ELSE 0 END) || '~' || (CASE WHEN COALESCE(v_bitCustom,false) = true THEN 1 ELSE 0 END)
         || '~' || coalesce(v_vcAssociatedControlType,'')
         || '~' || (CASE WHEN COALESCE(v_bitAllowFiltering,false) = true THEN 1 ELSE 0 END);
                 
         IF v_vcAssociatedControlType = 'SelectBox' then
                    
            IF v_vcListItemType = 'L' then
                                
                            ---PRINT 'columnName' + @vcColumnName
               v_strSql := coalesce(v_strSql,'') || ',L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join ListDetails L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numListItemID=EH.numListItemID';
            end if;
         ELSEIF (v_vcAssociatedControlType = 'TextBox'
         OR v_vcAssociatedControlType = 'TextArea'
         OR v_vcAssociatedControlType = 'Image')  AND v_vcDbColumnName <> 'AlertPanel' AND v_vcDbColumnName <> 'RecentCorrespondance'
         then
                        
            IF v_numNodeId = 4 and v_vcDbColumnName = 'vcFrom' then
							
               v_vcColumnName := 'To' || '~' || coalesce(v_vcDbColumnName,'')
               || '~' || (CASE WHEN COALESCE(v_bitAllowSorting,false) = true THEN 1 ELSE 0 END) || '~'
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~'
               || (CASE WHEN COALESCE(v_bitAllowEdit,false) = true THEN 1 ELSE 0 END) || '~' || (CASE WHEN COALESCE(v_bitCustom,false) = true THEN 1 ELSE 0 END)
               || '~' || coalesce(v_vcAssociatedControlType,'')
               || '~' || (CASE WHEN COALESCE(v_bitAllowFiltering,false) = true THEN 1 ELSE 0 END);
               v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_Prefix,'')  || 'vcTo' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_Prefix,'')  || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcDbColumnName = 'AlertPanel'
         then
							
            v_strSql := CONCAT(coalesce(v_strSql,''),', GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,'
            , COALESCE(v_bitOpenActionItem,false)::VARCHAR , ','
            , COALESCE(v_bitOpencases,false)::VARCHAR , ','
            , COALESCE(v_bitOpenProject,false)::VARCHAR , ','
            , COALESCE(v_bitOpenSalesOpp,false)::VARCHAR , ','
            , COALESCE(v_bitBalancedue,false)::VARCHAR , ','
            , COALESCE(v_bitUnreadEmail,false)::VARCHAR , ','
            , COALESCE(v_bitCampaign,false)::VARCHAR , ',' ,
            'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  "', '' , coalesce(v_vcColumnName,'') , '' , '"');
         ELSEIF v_vcDbColumnName = 'RecentCorrespondance'
         then
							 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
            v_strSql := coalesce(v_strSql,'') || ',0 AS  "' || '' || coalesce(v_vcColumnName,'') || '' || '"';
         ELSEIF v_vcAssociatedControlType = 'DateField' AND v_vcDbColumnName <> 'dtReceivedOn'
         then
					
            v_strSql := coalesce(v_strSql,'') || ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE) = CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'DateField' AND v_vcDbColumnName = 'dtReceivedOn'
         then
					
            v_strSql := coalesce(v_strSql,'') || ',case when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ') AS DATE)= CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || '),' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then

         v_vcColumnName := coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '~0~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1~' || SUBSTR(CAST(v_bitCustom AS VARCHAR(1)),1,1) || '~' || coalesce(v_vcAssociatedControlType,'');
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master where    CFW_Fld_Master.Fld_id = v_numFieldId;
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=FR.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=FR.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=FR.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=FR.numContactId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         end if;
      end if;   

                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowFiltering, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowFiltering,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;

	 

   END LOOP;  
                     
   open SWV_RefCur for
   SELECT * FROM    tt_TEMPFORM;

-------Change Row Color-------
   v_vcCSOrigDbCOlumnName := '';
   v_vcCSLookBackTableName := '';

   DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
   Create TEMPORARY TABLE tt_TEMPCOLORSCHEME
   (
      vcOrigDbCOlumnName VARCHAR(50),
      vcLookBackTableName VARCHAR(50),
      vcFieldValue VARCHAR(50),
      vcFieldValue1 VARCHAR(50),
      vcColorScheme VARCHAR(50),
      vcAssociatedControlType VARCHAR(50)
   );

   insert into tt_TEMPCOLORSCHEME  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
   from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID AND DFCS.numFormID = DFFM.numFormID
   join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
   where DFCS.numDomainID = v_numDomainId and DFFM.numFormID = 10 AND DFCS.numFormID = 10 and coalesce(DFFM.bitAllowGridColor,false) = true;

   IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then

      select   vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType from tt_TEMPCOLORSCHEME     LIMIT 1;
   end if;   
---------------------------- 
   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      v_strSql := coalesce(v_strSql,'') || ',tCS.vcColorScheme AS "vcColorScheme"';
   end if;  

   v_strSql := coalesce(v_strSql,'') || ', ' || 'EH.bitIsRead As "IsRead~bitIsRead~0~0~0~0~Image"';

   select   coalesce(SUM(coalesce(SpaceOccupied,0)),0) INTO v_fltTotalSize FROM View_InboxCount where numDomainID = v_numDomainId and numUserCntID = v_numUserCntId;

   v_strSql := coalesce(v_strSql,'') || ', ' || SUBSTR(CAST(v_fltTotalSize AS VARCHAR(18)),1,18) || ' AS "TotalSize" From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID ';
--SET @strSql = @strSql + ', '+ COALESCE('EH.numNoofTimes',0) +' AS NoOfTimeOpened From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
   v_strSql := coalesce(v_strSql,'') || ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
	left JOIN DivisionMaster DMI ON ACI.numDivisionId = DMI.numDivisionID                                                                            
 left JOIN CompanyInfo CIM ON DMI.numCompanyID = CIM.numCompanyId
LEFT JOIN LATERAL
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		tt_TEMP
	WHERE
		tt_TEMP.numDivisionId = ACI.numDivisionId
) AS V1 ON TRUE
LEFT JOIN LATERAL
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		tt_TEMP
	WHERE
		tt_TEMP.numDivisionId = ACI.numDivisionId AND
		tt_TEMP.numContactId = FR.numContactId
) AS V2 ON TRUE
LEFT JOIN LATERAL
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT COALESCE(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC LIMIT 1) AND 
		COALESCE(bitSend,false) = false
) AS V3 ON TRUE ';
   v_strSql := coalesce(v_strSql,'') || coalesce(v_WhereCondition,'');

   IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then

      if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
         v_Prefix := 'ADC.';
      end if;
      if v_vcCSLookBackTableName = 'DivisionMaster' then
         v_Prefix := 'DM.';
      end if;
      if v_vcCSLookBackTableName = 'CompanyInfo' then
         v_Prefix := 'CI.';
      end if;
      if v_vcCSLookBackTableName = 'AddressDetails' then
         v_Prefix := 'AD.';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId ';
      IF v_vcCSAssociatedControlType = 'DateField' then
		v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) >= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue as int)) AS DATE)
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS DATE) <= CAST(timezone(''utc'', now())  + make_interval(days => cast(tCS.vcFieldValue1 as int)) AS DATE)';
      ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
      then
         v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || '::VARCHAR';
      end if;
   end if;

   v_strSql := coalesce(v_strSql,'') || ' Where RowNumber >'  || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10); 
   IF v_EmailStatus <> 0 then

      v_strSql := coalesce(v_strSql,'') || ' AND EH.numListItemId = ' || SUBSTR(CAST(v_EmailStatus AS VARCHAR(10)),1,10);
   end if;       
   v_strSql := coalesce(v_strSql,'') || ' order by RowNumber';

   RAISE NOTICE '%',v_strSql; 
   OPEN SWV_RefCur2 FOR EXECUTE v_strSql;   
          
 
   RETURN;
END; $$;
 