-- Stored procedure definition script USP_ManagePricingNamesTable for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePricingNamesTable(v_numDomainID NUMERIC,
    v_numCreatedBy NUMERIC,
    v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   DELETE  FROM PricingNamesTable WHERE numDomainID = v_numDomainID;
		
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
      INSERT  INTO PricingNamesTable(numDomainID,
            numCreatedBy,
            dtCreatedDate,
            tintPriceLevel,
            vcPriceLevelName)
      SELECT
      v_numDomainID,
			v_numCreatedBy,
			TIMEZONE('UTC',now()),
			X.tintPriceLevel,
			X.vcPriceLevelName
      FROM
	   XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				tintPriceLevel SMALLINT PATH 'tintPriceLevel',
				vcPriceLevelName VARCHAR(300) PATH 'vcPriceLevelName'
		) AS X;
   end if;
   RETURN;
END; $$;


