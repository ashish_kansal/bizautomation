DROP FUNCTION IF EXISTS USP_WorkSchedule_GetAll;

CREATE OR REPLACE FUNCTION USP_WorkSchedule_GetAll(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(UserMaster.numUserId as VARCHAR(255))
		,cast(UserMaster.numUserDetailId as VARCHAR(255))
		,cast(CONCAT(coalesce(vcFirstName,'-'),' ',coalesce(vcLastname,'-')) as VARCHAR(255)) AS vcUserName
		,cast(coalesce(WorkSchedule.ID,0) as VARCHAR(255)) AS numWorkScheduleID
		,cast(WorkSchedule.numWorkHours as VARCHAR(255))
		,cast(WorkSchedule.numWorkMinutes as VARCHAR(255))
		,cast(WorkSchedule.numProductiveHours as VARCHAR(255))
		,cast(WorkSchedule.numProductiveMinutes as VARCHAR(255))
		,cast(WorkSchedule.tmStartOfDay as VARCHAR(255))
		,cast(WorkSchedule.vcWorkDays as VARCHAR(255))
		,cast(OVERLAY((SELECT
      cast(CONCAT(', ',(CASE
      WHEN WorkScheduleDaysOff.dtDayOffFrom = WorkScheduleDaysOff.dtDayOffTo
      THEN FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,v_numDomainID)
      ELSE CONCAT(FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,v_numDomainID),' - ',
         FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffTo,v_numDomainID))
      END)) as VARCHAR(255))
      FROM
      WorkScheduleDaysOff
      WHERE
      numWorkScheduleID = coalesce(WorkSchedule.ID,0)
      ORDER BY
      dtDayOffFrom) placing '' from 1 for 1) as VARCHAR(255)) AS vcDaysOff
   FROM
   UserMaster
   INNER JOIN
   AdditionalContactsInformation
   ON
   UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
   LEFT JOIN
   WorkSchedule
   ON
   UserMaster.numUserDetailId = WorkSchedule.numUserCntID
   AND WorkSchedule.numDomainID = v_numDomainID
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND coalesce(UserMaster.numUserDetailId,0) > 0
   ORDER BY
   vcUserName;
END; $$;


