-- Stored procedure definition script USP_DeleteItemGroups for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteItemGroups(v_numItemGroupID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from ItemGroupsDTL where numItemGroupID = v_numItemGroupID;
   delete from ItemGroups where numItemGroupID = v_numItemGroupID;
   RETURN;
END; $$;


