-- Stored procedure definition script USP_GetOperationBudgetMasterForBudgetAnalyze for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetMasterForBudgetAnalyze(v_numBudgetId NUMERIC(9,0) DEFAULT 0,          
 v_numChartAcntId NUMERIC(9,0) DEFAULT 0,                  
 v_sintByte SMALLINT DEFAULT 0,                 
 v_sintForecast SMALLINT DEFAULT 0,             
 v_intFiscalYear INTEGER DEFAULT 0,            
 v_numUserCntID NUMERIC(9,0) DEFAULT 0,        
 v_numDomainId NUMERIC(9,0) DEFAULT 0,  
 v_intType INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_month  INTEGER;                          
   v_monAmount  DECIMAL(20,5);                          
   v_vcMonDescription  VARCHAR(200);       
   v_Date  TIMESTAMP;         
   v_lstrSQL  VARCHAR(8000);        
   v_dtStartDate  TIMESTAMP;        
   v_intCurrentYear  INTEGER;
BEGIN
   Drop table IF EXISTS tt_TEMP CASCADE;
   Create TEMPORARY TABLE tt_TEMP
   (
      numBudgetId NUMERIC(9,0),
      vcmonth VARCHAR(100),
      vcmonthName VARCHAR(10),
      monOriginalAmt DECIMAL(20,5),
      monAmount DECIMAL(20,5),
      monCashInflow DECIMAL(20,5),
      monSalesForecast DECIMAL(20,5),
      monTotalBudgetBalance DECIMAL(20,5),
      monProjectedBankBalance DECIMAL(20,5)
   );                    
   v_lstrSQL := '';            
   v_month := EXTRACT(month FROM TIMEZONE('UTC',now()));       
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);                    
   if v_sintByte = 0 then
	
      While v_month <= 24 LOOP
         v_dtStartDate := CAST('01/01/' || CAST(EXTRACT(year FROM TIMEZONE('UTC',now())) AS VARCHAR(4)) AS TIMESTAMP);
         v_dtStartDate := v_dtStartDate+CAST(v_month::bigint -1 || 'month' as interval);        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
         Insert into tt_TEMP(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance) Values(v_numBudgetId,CAST(CAST(EXTRACT(month FROM v_dtStartDate) AS VARCHAR(100)) || '~' || CAST(EXTRACT(year FROM v_dtStartDate) AS VARCHAR(4)) AS VARCHAR(100)),
				 CAST(CAST(TO_CHAR(v_dtStartDate, 'Month') AS VARCHAR(3)) AS VARCHAR(10)),CAST(fn_GetBudgetMonthDet(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5)),CAST(fn_GetBudgetMonthDet(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5))
				 ,GetCashInflowForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId),GetSalesForeCastForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast::INTEGER)
				 ,GetTotalBudgetBalanceForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(year FROM v_dtStartDate)::INTEGER,fn_GetBudgetMonthDet(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC,v_numDomainId,v_numChartAcntId)::DECIMAL(20,5),v_numDomainId,v_numUserCntID,v_sintForecast)
				 ,GetProjectedBankBalance(v_intType,v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast,v_numChartAcntId));
				 
         v_month := v_month::bigint+1;
      END LOOP;
   end if;                      
--For Market Budget    
   if v_sintByte = 1 then
	
      While v_month <= 24 LOOP
         v_dtStartDate := CAST('01/01/' || CAST(EXTRACT(year FROM v_Date) AS VARCHAR(4)) AS TIMESTAMP);
         v_dtStartDate := v_dtStartDate+CAST(v_month::bigint -1 || 'month' as interval);        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
         Insert into tt_TEMP(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance)
				 Values(v_numBudgetId,CAST(CAST(EXTRACT(month FROM v_dtStartDate) AS VARCHAR(100)) || '~' || CAST(EXTRACT(year FROM v_dtStartDate) AS VARCHAR(4)) AS VARCHAR(100)),
				 CAST(CAST(TO_CHAR(v_dtStartDate, 'Month') AS VARCHAR(3)) AS VARCHAR(10)),CAST(fn_GetMarketBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5)),CAST(fn_GetMarketBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5))
				 ,GetCashInflowForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId),GetSalesForeCastForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast::INTEGER)
				 , GetTotalBudgetBalanceForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(year FROM v_dtStartDate)::INTEGER,fn_GetMarketBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numChartAcntId)::DECIMAL(20,5),v_numDomainId,v_numUserCntID,v_sintForecast),
				 GetProjectedBankBalance(v_intType,v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast,v_numChartAcntId));
				 
         v_month := v_month::bigint+1;
      END LOOP;
   end if;                      
    
    
--For Procurement Budget    
   if v_sintByte = 2 then
	
      While v_month <= 24 LOOP
         v_dtStartDate := CAST('01/01/' || CAST(EXTRACT(year FROM v_Date) AS VARCHAR(4)) AS TIMESTAMP);
         v_dtStartDate := v_dtStartDate+CAST(v_month::bigint -1 || 'month' as interval);        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
         Insert into tt_TEMP(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance)
				 Values(v_numBudgetId,CAST(CAST(EXTRACT(month FROM v_dtStartDate) AS VARCHAR(100)) || '~' || CAST(EXTRACT(year FROM v_dtStartDate) AS VARCHAR(4)) AS VARCHAR(100)),
				 CAST(CAST(TO_CHAR(v_dtStartDate, 'Month') AS VARCHAR(3)) AS VARCHAR(10)),CAST(fn_GetProcurementBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5)),CAST(fn_GetProcurementBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::NUMERIC(9,0),v_numDomainId,v_numChartAcntId) AS DECIMAL(20,5))
				 ,GetCashInflowForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId),GetSalesForeCastForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast::INTEGER), GetTotalBudgetBalanceForMonths(EXTRACT(month FROM v_dtStartDate)::INTEGER,
				 EXTRACT(year FROM v_dtStartDate)::INTEGER,fn_GetProcurementBudgetMonthDetail(v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::SMALLINT,EXTRACT(year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numChartAcntId)::DECIMAL(20,5),v_numDomainId,v_numUserCntID,v_sintForecast),
				 GetProjectedBankBalance(v_intType,v_numBudgetId,EXTRACT(month FROM v_dtStartDate)::INTEGER,EXTRACT(Year FROM v_dtStartDate)::INTEGER,v_numDomainId,v_numUserCntID,v_sintForecast,v_numChartAcntId));
				 
         v_month := v_month::bigint+1;
      END LOOP;
   end if;                      
    
   open SWV_RefCur for Select * From tt_TEMP;                          

   RETURN;
END; $$;












