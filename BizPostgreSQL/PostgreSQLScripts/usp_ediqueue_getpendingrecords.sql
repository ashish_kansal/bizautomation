-- Stored procedure definition script USP_EDIQueue_GetPendingRecords for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EDIQueue_GetPendingRecords(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
		ID
		,EDIQueue.numDomainID
		,EDIQueue.numOppID
		,EDIType
   FROM
   EDIQueue
   INNER JOIN
   OpportunityMaster
   ON
   EDIQueue.numOppID = OpportunityMaster.numOppId
   WHERE
   coalesce(bitExecuted,false) = false
   AND 1 =(CASE
   WHEN EDIType = 940
   THEN(CASE WHEN coalesce(tintopptype,0) = 1 AND coalesce(tintoppstatus,0) = 1 THEN 1 ELSE 0 END)
   ELSE 1
   END) LIMIT 50;
END; $$;












