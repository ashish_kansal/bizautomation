-- Stored procedure definition script USP_GetPageNavigationRel for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPageNavigationRel(v_numModuleID NUMERIC(9,0)      ,    
v_numDomainID NUMERIC(9,0),
v_numGroupID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        coalesce(vcNavURL,'') AS vcNavURL,
        vcImageURL,
        0 AS numorder
   FROM    PageNavigationDTL PND
   JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID
   WHERE   1 = 1
   AND  coalesce(TNA.bitVisible,false) = true
              --OR ISNULL(numParentID, 0) = 0
            
   AND coalesce(PND.bitVisible,false) = true
   AND numModuleID = v_numModuleID
   AND numGroupID = v_numGroupID
   UNION
   SELECT  0,
        2 AS numModuleID,
        11 AS numParentID,
        L2.vcData AS vcPageNavName,
        '../prospects/frmProspectList.aspx?numProfile='
   || L2.numListItemID AS vcNavURL,
        null AS vcImageURL,
        0
   FROM    FieldRelationship AS FR
   INNER JOIN FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
   INNER JOIN Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
   INNER JOIN Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
   WHERE    FRD.numPrimaryListItemID = 46 
   AND  FR.numDomainID = v_numDomainID 
   UNION
   SELECT  0,
        2 AS numModuleID,
        12 AS numParentID,
        L2.vcData AS vcPageNavName,
        '../account/frmAccountList.aspx?numProfile='
   || L2.numListItemID AS vcNavURL,
        null AS vcImageURL,
        0
   FROM    FieldRelationship AS FR
   INNER JOIN FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
   INNER JOIN Listdetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
   INNER JOIN Listdetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
   WHERE    FRD.numPrimaryListItemID = 46 
   AND  FR.numDomainID = v_numDomainID 
   UNION    
--Select Contact
   SELECT  0,
        2 AS numModuleID,
        13 AS numParentID,
        vcData AS vcPageNavName,
        '../contact/frmContactList.aspx?ContactType='
   || numListItemID AS vcNavURL,
        null,
        sintOrder
   FROM    Listdetails
   WHERE   numListID = 8
   AND (constFlag = true
   OR numDomainid = v_numDomainID)
   UNION
   SELECT  0,
        2 AS numModuleID,
        13 AS numParentID,
        'Primary Contact' AS vcPageNavName,
        '../Contact/frmcontactList.aspx?ContactType=101',
        null,
        0
   ORDER BY 3;
/****** Object:  StoredProcedure [dbo].[USP_GetParentCategory]    Script Date: 09/25/2009 16:25:38 ******/
   RETURN;
END; $$;












