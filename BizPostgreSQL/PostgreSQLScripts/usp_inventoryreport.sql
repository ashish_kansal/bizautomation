-- Stored procedure definition script USP_INVENTORYREPORT for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_INVENTORYREPORT(v_numDomainID NUMERIC(9,0),
v_numUserCntID NUMERIC(9,0),
v_numDateRange NUMERIC(9,0),
v_numBasedOn NUMERIC(9,0),
v_numItemType NUMERIC(9,0),
v_numItemClass NUMERIC(9,0),
v_numItemClassId NUMERIC(9,0),
v_DateCondition VARCHAR(100),
v_numWareHouseId NUMERIC,
v_dtFromDate DATE,
v_dtToDate DATE,
v_CurrentPage INTEGER DEFAULT 0,                                                            
v_PageSize INTEGER DEFAULT NULL,
INOUT v_TotRecs NUMERIC(18,0)  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_FilterItemType  TEXT;
   v_FilterBasedOn  TEXT;
   v_FilterDateRange  TEXT;

   v_strSql  TEXT;
   v_strGroup  TEXT;
   v_ItemClassID  VARCHAR(100);
   v_strSort  TEXT;

   v_strItemSQL  TEXT;
   v_firstRec  INTEGER;                                                            
   v_lastRec  INTEGER;
BEGIN
   DELETE FROM InventroyReportDTL WHERE numUserID = v_numUserCntID;

   INSERT INTO InventroyReportDTL  SELECT v_numDomainID,v_numUserCntID,v_numDateRange,0;
   INSERT INTO InventroyReportDTL  SELECT v_numDomainID,v_numUserCntID,v_numBasedOn,0; 
   INSERT INTO InventroyReportDTL  SELECT v_numDomainID,v_numUserCntID,v_numItemType,0;
   INSERT INTO InventroyReportDTL  SELECT v_numDomainID,v_numUserCntID,v_numItemClass,v_numItemClassId;

   select   vcCondition INTO v_FilterItemType from InventoryReportMaster a inner join InventroyReportDTL b ON
   a.numReportID = b.numReportID and b.numUserID = v_numUserCntID and a.numTypeID = 1;

   select   vcCondition INTO v_FilterBasedOn from InventoryReportMaster a inner join InventroyReportDTL b ON
   a.numReportID = b.numReportID and b.numUserID = v_numUserCntID and a.numTypeID = 2;

   select   vcDesc INTO v_FilterDateRange from InventoryReportMaster a inner join InventroyReportDTL b ON
   a.numReportID = b.numReportID and b.numUserID = v_numUserCntID and a.numTypeID = 3;

   select(CASE numItemClassification WHEN 0 THEN '' ELSE ' AND a.numItemClassification=' || SUBSTR(CAST(numItemClassification AS VARCHAR(50)),1,50)  END) INTO v_ItemClassID FROM  InventroyReportDTL WHERE numUserID = v_numUserCntID; 

   RAISE NOTICE '%','@FilterItemType:' || coalesce(v_FilterItemType,'');
   RAISE NOTICE '%','@FilterBasedOn:' || coalesce(v_FilterBasedOn,'');
   RAISE NOTICE '%','@FilterDateRange:' || coalesce(v_FilterDateRange,'');
   RAISE NOTICE '%','@ItemClassID: ' || coalesce(v_ItemClassID,'');

   DROP TABLE IF EXISTS tt_ITEMWAREHOUSE CASCADE;
   CREATE TEMPORARY TABLE tt_ITEMWAREHOUSE
   (
      RowID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(2000),
--vcModelID VARCHAR(150),
      vcSKU VARCHAR(50),
      ItemClass VARCHAR(150),
      ItemDesc TEXT,
      numOnHand NUMERIC(18,2),
      AVGCOST DECIMAL(20,5),
      numAllocation NUMERIC(18,2),
      Amount DECIMAL(20,5),
      SalesUnit NUMERIC(18,2),
      ReturnQty NUMERIC(18,2),
      ReturnPer NUMERIC(18,2),
      COGS DECIMAL(20,5),
      PROFIT DECIMAL(20,5),
      SALESRETURN DECIMAL(20,5)
   );

   DROP TABLE IF EXISTS tt_TMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TMPITEMS
   (
      RowID NUMERIC(18,0),
      numItemCode NUMERIC(18,0)
   );


   v_strItemSQL := ' SELECT ROW_NUMBER() OVER (ORDER BY numItemCode) AS RowID ,numItemCode FROM Item a WHERE a.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' 
AND 1=1 ' || coalesce(v_FilterItemType,'') || ' ' || coalesce(v_ItemClassID,'');

   RAISE NOTICE '%',v_strItemSQL;
   EXECUTE 'INSERT INTO tt_TMPITEMS(RowID,numItemCode )
' || v_strItemSQL;

   v_strSql := 'SELECT DISTINCT RowID, A.numItemCode,A.vcItemName--,a.vcModelID
	, A.vcSKU, A.numItemClass,COALESCE(txtItemDesc,'''') AS ItemDesc,
(CASE WHEN CAST(now() AS DATE) = CAST( ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE) THEN SUM(COALESCE(c.numOnHand,0))
	 ELSE
			(SELECT WHIT.numOnHand 
					FROM WareHouseItems_Tracking WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' || SUBSTR(CAST(v_numWareHouseId as VARCHAR(10)),1,10) || '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  || SUBSTR(cast(v_numWareHouseId as VARCHAR(10)),1,10) || ' END) 
					AND WHIT.dtCreatedDate BETWEEN CAST(''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''' AS DATE) And CAST(''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE) 
					ORDER BY WHIT.numWareHouseItemTrackingID DESC LIMIT 1) 
END) numOnHand, 
(CASE WHEN CAST(now() AS DATE) = CAST( ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE) THEN COALESCE(A.monAverageCost,0)
	 ELSE
			(SELECT WHIT.monAverageCost 
					FROM WareHouseItems_Tracking WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' || SUBSTR(CAST(v_numWareHouseId as VARCHAR(10)),1,10) || '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  || SUBSTR(cast(v_numWareHouseId as VARCHAR(10)),1,10) || ' END) 
					AND WHIT.dtCreatedDate BETWEEN CAST(''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''' AS DATE) And CAST(''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE)
					ORDER BY WHIT.numWareHouseItemTrackingID DESC LIMIT 1) 
END) AVGCOST,
(CASE WHEN CAST(now() AS DATE) = CAST(''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''' AS DATE) THEN SUM(COALESCE(c.numAllocation,0))
	 ELSE
			(SELECT WHIT.numAllocation 
					FROM WareHouseItems_Tracking WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' || SUBSTR(CAST(v_numWareHouseId as VARCHAR(10)),1,10) || '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  || SUBSTR(cast(v_numWareHouseId as VARCHAR(10)),1,10) || ' END) 
					AND WHIT.dtCreatedDate BETWEEN CAST(''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''' AS DATE) And CAST(''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE) 
					ORDER BY WHIT.numWareHouseItemTrackingID DESC LIMIT 1) 
END) numAllocation,
SUM(COGS) as Amount,
sum(SalesUnit) as SalesUnit,
sum(ReturnQty) as ReturnQty,
COALESCE((sum(ReturnQty)/NULLIF(sum(SalesUnit),0)*100),0) as ReturnPer,
SUM(COGS) AS COGS,
SUM(PROFIT) AS PROFIT,
SUM(SALESRETURN) AS SALESRETURN
FROM Item A
JOIN tt_TMPITEMS ON tt_TMPITEMS.numItemCode = A.numItemCode 
JOIN WareHouseItems C ON C.numItemID = A.numItemCode AND numWareHouseID = (CASE ' || SUBSTR(CAST(v_numWareHouseId as VARCHAR(10)),1,10) || '  WHEN 0 THEN numWareHouseID ELSE '  || SUBSTR(cast(v_numWareHouseId as VARCHAR(10)),1,10) || ' END) ' || coalesce(v_FilterItemType,'')  ||  coalesce(v_ItemClassID,'') || '
LEFT JOIN 
	VIEW_INVENTORYOPPSALES B 
ON  
	b.numItemCode = A.numItemCode
	AND OppDate BETWEEN CAST(''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''' AS DATE) And CAST(''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE)
INNER JOIN 
	OpportunityBizDocItems
ON
	B.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	AND OpportunityBizDocItems.numWarehouseItmsID = C.numWareHouseItemID
WHERE 1=1 AND A.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10);  

   IF CAST(LOCALTIMESTAMP AS DATE) <> CAST(v_dtToDate AS DATE) then
	
      v_strSql := coalesce(v_strSql,'') || ' AND CONVERT(DATE, bintModifiedDate) >= CONVERT(DATE, + ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(50)),1,50) || ''') AND CONVERT(DATE, bintModifiedDate) <= CAST(''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(50)),1,50) || ''' AS DATE)';
   end if;

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1); 

   IF v_CurrentPage > 0 then

      v_strSql := coalesce(v_strSql,'') || ' AND RowID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' AND RowID < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' ';
   end if;

   v_strGroup := ' GROUP BY A.numItemCode,A.vcItemName--,a.vcModelID
,A.vcSKU,A.numItemClass,COALESCE(txtItemDesc,''''),A.monAverageCost, RowID ';

   SELECT COUNT(*) INTO v_TotRecs FROM tt_TMPITEMS;
   RAISE NOTICE '%',v_TotRecs;

--PRINT @DateCondition
--IF LEN(@DateCondition) > 0
--	SET @strsql = @strsql +' AND '+ @DateCondition + ' '+ @strGroup
--ELSE
--	SET @strsql = @strsql + ' ' + @strGroup + ';'

   v_strSql := coalesce(v_strSql,'') || ' ' || coalesce(v_strGroup,'') || '/** EDynId 6 1 **/  '; 
--SET @strsql = @strsql + ' ORDER BY ' + @FilterBasedOn + ' DESC '

   RAISE NOTICE '%',v_strSql;
   EXECUTE 'INSERT INTO tt_ITEMWAREHOUSE 
' || v_strSql;

--SELECT * FROM #ItemWareHouse AS IWH

--DECLARE @firstRec AS INTEGER                                                            
--DECLARE @lastRec AS INTEGER                                                            
--SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
--SET @lastRec = ( @CurrentPage * @PageSize + 1 ) 

   IF NOT EXISTS(SELECT * FROM tt_ITEMWAREHOUSE) then

      v_TotRecs := 0;
   end if;

   v_strSort := ' SELECT *, (coalesce(numOnHand,0)+coalesce(numAllocation,0)) AS TotalInventory,  ((coalesce(numOnHand,0)+coalesce(numAllocation,0))*AVGCOST) AS InventoryValue FROM tt_ITEMWAREHOUSE ORDER BY ' || coalesce(v_FilterBasedOn,'') || ' DESC ';

--WHERE 1=1 AND RowID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' AND RowID < '+ CONVERT(VARCHAR(10),@lastRec) + ' 


--CREATE TABLE #InventoryReport 
--(RowID NUMERIC(18,0),
--numItemCode NUMERIC(18),
--vcItemName VARCHAR(2000),
--vcModelID VARCHAR(150),
--ItemClass VARCHAR(150),
--ItemDesc VARCHAR(MAX),
--numOnHand NUMERIC(18,2),
--AVGCOST DECIMAL(20,5),
--TotalInventory NUMERIC(18,0),
--InventoryValue DECIMAL(20,5),
--numAllocation NUMERIC(18,2));

   RAISE NOTICE '%',v_strSort;
--INSERT INTO #InventoryReport 

   OPEN SWV_RefCur FOR EXECUTE v_strSort;

--SELECT * FROM #InventoryReport -- WHERE RowID > @firstRec AND RowID < @lastRec

--EXEC ('DROP TABLE #ItemWareHouse')
   DROP TABLE IF EXISTS tt_TMPITEMS CASCADE;
   DROP TABLE IF EXISTS tt_ITEMWAREHOUSE CASCADE;
   RETURN;
END; $$;


