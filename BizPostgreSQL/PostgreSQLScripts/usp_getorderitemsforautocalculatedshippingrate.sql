-- Stored procedure definition script USP_GetOrderItemsForAutoCalculatedShippingRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOrderItemsForAutoCalculatedShippingRate(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_dtShipByDate DATE, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numContainer  NUMERIC(18,0);
   v_numNoItemIntoContainer  DOUBLE PRECISION;
   v_fltItemWeight  DOUBLE PRECISION;
   v_fltItemHeight  DOUBLE PRECISION;
   v_fltItemWidth  DOUBLE PRECISION;
   v_fltItemLength  DOUBLE PRECISION;
   v_fltContainerWeight  DOUBLE PRECISION;
   v_fltContainerHeight  DOUBLE PRECISION;
   v_ltContainerWidth  DOUBLE PRECISION;
   v_fltContainerLength  DOUBLE PRECISION;
   v_numTotalQty  DOUBLE PRECISION;
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED


	-- GET FROM ADDRESS
	open SWV_RefCur for
   SELECT
   AD.vcStreet
		,AD.vcCity
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 91 AND vcStateName = fn_GetState(AD.numState)),'') AS vcStateFedex
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 88 AND vcStateName = fn_GetState(AD.numState)),'') AS vcStateUPS
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 90 AND vcStateName = fn_GetState(AD.numState)),'') AS vcStateUSPS
		,AD.vcPostalCode AS vcZipCode
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 91 AND vcCountryName = fn_GetListItemName(AD.numCountry)),'') AS vcCountryFedex
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 91 AND vcCountryName = fn_GetListItemName(AD.numCountry)),'') AS vcCountryUPS
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 91 AND vcCountryName = fn_GetListItemName(AD.numCountry)),'') AS vcCountryUSPS
   FROM
   Warehouses W
   LEFT JOIN
   AddressDetails AD
   ON
   W.numAddressID = AD.numAddressID
   WHERE
   W.numDomainID = v_numDomainID
   AND W.numWareHouseID = v_numWarehouseID;

	-- GET TO ADDRESS
   open SWV_RefCur2 for
   SELECT
   vcStreet
		,vcCity
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 91 AND vcStateName = fn_GetState(vcState)),'') AS vcStateFedex
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 88 AND vcStateName = fn_GetState(vcState)),'') AS vcStateUPS
		,coalesce((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany = 90 AND vcStateName = fn_GetState(vcState)),'') AS vcStateUSPS
		,vcZipCode
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 91 AND vcCountryName = fn_GetListItemName(vcCountry)),'') AS vcCountryFedex
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 88 AND vcCountryName = fn_GetListItemName(vcCountry)),'') AS vcCountryUPS
		,coalesce((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany = 90 AND vcCountryName = fn_GetListItemName(vcCountry)),'') AS vcCountryUSPS
   FROM
   fn_GetShippingReportAddress(2,v_numDomainID,v_numOppID);

	-- GET CONTAINERS/PACKAGES
   BEGIN
      CREATE TEMP SEQUENCE tt_TempContiner_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPCONTINER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCONTINER
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      fltContainerWeight DOUBLE PRECISION,
      fltContainerHeight DOUBLE PRECISION,
      fltContainerWidth DOUBLE PRECISION,
      fltContainerLength DOUBLE PRECISION,
      numQty DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numContainer NUMERIC(18,0),
      numNoItemIntoContainer DOUBLE PRECISION,
      fltItemWeight DOUBLE PRECISION,
      fltItemHeight DOUBLE PRECISION,
      fltItemWidth DOUBLE PRECISION,
      fltItemLength DOUBLE PRECISION,
      fltContainerWeight DOUBLE PRECISION,
      fltContainerHeight DOUBLE PRECISION,
      ltContainerWidth DOUBLE PRECISION,
      fltContainerLength DOUBLE PRECISION,
      numTotalQty DOUBLE PRECISION
   );

   INSERT INTO
   tt_TEMPITEMS(numContainer, numNoItemIntoContainer, fltItemWeight, fltItemHeight, fltItemWidth, fltItemLength, fltContainerWeight, fltContainerHeight, ltContainerWidth, fltContainerLength, numTotalQty)
   SELECT
   T1.numContainer
		,T1.numNoItemIntoContainer
		,T1.fltItemWeight
		,T1.fltItemHeight
		,T1.fltItemWidth
		,T1.fltItemLength
		,coalesce(IContainer.fltWeight,0) AS fltContainerWeight
		,coalesce(IContainer.fltHeight,0) AS fltContainerHeight
		,coalesce(IContainer.fltWidth,0) AS fltContainerWidth
		,coalesce(IContainer.fltLength,0) AS fltContainerLength
		,T1.numTotalQty
   FROM(SELECT
      I.numContainer
			,(CASE WHEN coalesce(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE SUM(OI.numUnitHour) END) AS numNoItemIntoContainer
			,coalesce(I.fltWeight,0) AS fltItemWeight
			,coalesce(I.fltHeight,0) AS fltItemHeight
			,coalesce(I.fltWidth,0) AS fltItemWidth
			,coalesce(I.fltLength,0) AS fltItemLength
			,SUM(OI.numUnitHour) AS numTotalQty
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      WareHouseItems WI
      ON
      OI.numWarehouseItmsID = WI.numWareHouseItemID
      INNER JOIN
      Item AS I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numOppId = v_numOppID
      AND WI.numWareHouseID = v_numWarehouseID
      AND coalesce(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) = v_dtShipByDate
      AND coalesce(I.bitContainer,false) = false
      GROUP BY
      I.numContainer,I.numNoItemIntoContainer,I.fltWeight,I.fltHeight,I.fltWidth,
      I.fltLength) AS T1
   LEFT JOIN
   Item IContainer
   ON
   T1.numContainer = IContainer.numItemCode;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPITEMS;

   WHILE v_i <= v_iCount LOOP
      select   numContainer, numNoItemIntoContainer, fltItemWeight, fltItemHeight, fltItemWidth, fltItemLength, fltContainerWeight, fltContainerHeight, ltContainerWidth, fltContainerLength, numTotalQty INTO v_numContainer,v_numNoItemIntoContainer,v_fltItemWeight,v_fltItemHeight,
      v_fltItemWidth,v_fltItemLength,v_fltContainerWeight,v_fltContainerHeight,
      v_ltContainerWidth,v_fltContainerLength,v_numTotalQty FROM
      tt_TEMPITEMS WHERE
      ID = v_i;
      IF v_numTotalQty <= v_numNoItemIntoContainer then
		
         INSERT INTO tt_TEMPCONTINER(fltContainerWeight
				,fltContainerHeight
				,fltContainerWidth
				,fltContainerLength
				,numQty)
			VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numTotalQty, 0)*coalesce(v_fltItemWeight, 0))
				,v_fltContainerHeight
				,v_ltContainerWidth
				,v_fltContainerLength
				,v_numTotalQty);
      ELSE
         WHILE v_numTotalQty > v_numNoItemIntoContainer LOOP
            INSERT INTO tt_TEMPCONTINER(fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty)
				VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numNoItemIntoContainer, 0)*coalesce(v_fltItemWeight, 0))
					,v_fltContainerHeight
					,v_ltContainerWidth
					,v_fltContainerLength
					,v_numNoItemIntoContainer);
				
            v_numTotalQty := v_numTotalQty -v_numNoItemIntoContainer;
         END LOOP;
         IF coalesce(v_numTotalQty,0) > 0 then
			
            INSERT INTO tt_TEMPCONTINER(fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty)
				VALUES(coalesce(v_fltContainerWeight, 0)+(coalesce(v_numTotalQty, 0)*coalesce(v_fltItemWeight, 0))
					,v_fltContainerHeight
					,v_ltContainerWidth
					,v_fltContainerLength
					,v_numTotalQty);
         end if;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur3 for
   SELECT * FROm tt_TEMPCONTINER;
   RETURN;
END; $$;


