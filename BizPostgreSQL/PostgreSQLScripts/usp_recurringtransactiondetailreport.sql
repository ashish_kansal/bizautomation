-- Stored procedure definition script USP_RecurringTransactionDetailReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RecurringTransactionDetailReport(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

				--BizDoc Recurrring
   open SWV_RefCur for SELECT   X.* FROM(SELECT distinct
      cast(RTR.numRecTranSeedId as VARCHAR(255)),
                     cast(RTR.tintRecType as VARCHAR(255)),
                     (CASE
      WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
      WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
      ELSE 'Unknown'
      END) AS TranType,
                     cast(RT.varRecurringTemplateName as VARCHAR(255)),
					 OM.vcpOppName AS RecurringName,
                     cast(OPR.numRecurringId as VARCHAR(255)),
                     CASE RT.chrIntervalType
      WHEN 'D' THEN 'Daily'
      WHEN 'M' THEN 'Monthly'
      WHEN 'Y' THEN 'Yearly'
      ELSE 'Unknown'
      END AS Frequency,
                     FormatedDateFromDate(RT.dtStartDate,OM.numDomainId) AS StartDate,
                     FormatedDateFromDate(RT.dtEndDate,OM.numDomainId) AS EndDate,
					(SELECT COUNT(*) FROM RecurringTransactionReport WHERE numRecTranSeedId = RTR.numRecTranSeedId) AS numNoTransactions,
                     CASE RT.chrIntervalType
      WHEN 'D' THEN DATE_PART('day',coalesce(RT.dtEndDate,TIMEZONE('UTC',now())):: timestamp -RT.dtStartDate:: timestamp)
      WHEN 'M' THEN(DATE_PART('year',AGE(coalesce(RT.dtEndDate,TIMEZONE('UTC',now())),RT.dtStartDate))*12+DATE_PART('month',AGE(coalesce(RT.dtEndDate,TIMEZONE('UTC',now())),RT.dtStartDate)))
      WHEN 'Y' THEN extract(year from coalesce(RT.dtEndDate,TIMEZONE('UTC',now()))) -extract(year from RT.dtStartDate)
      ELSE DATE_PART('day',coalesce(RT.dtEndDate,TIMEZONE('UTC',now())):: timestamp -RT.dtStartDate:: timestamp)
      END AS ExpectedChildRecords
      FROM   OpportunityRecurring AS OPR
      INNER JOIN OpportunityMaster AS OM
      ON OPR.numOppId = OM.numOppId
      INNER JOIN RecurringTemplate AS RT
      ON OPR.numRecurringId = RT.numRecurringId
      INNER JOIN RecurringTransactionReport AS RTR
      ON OPR.numOppId = RTR.numRecTranSeedId
      INNER JOIN OpportunityBizDocs AS OBD
      ON OM.numOppId = OBD.numoppid
      AND RTR.numRecTranBizDocID = OBD.numOppBizDocsId
      WHERE  OM.numDomainId = v_numDomainID AND RTR.tintRecType = 2
      UNION 
              --Sales Order Recurring 
      SELECT DISTINCT
      cast(RTR.numRecTranSeedId as VARCHAR(255)),
                     cast(RTR.tintRecType as VARCHAR(255)),
                     (CASE
      WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
      WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
      ELSE 'Unknown'
      END) AS TranType,
                     cast(RT.varRecurringTemplateName as VARCHAR(255)),
                     o2.vcpOppName AS RecurringName,
                     cast(OPR.numRecurringId as VARCHAR(255)),
                      CASE RT.chrIntervalType
      WHEN 'D' THEN 'Daily'
      WHEN 'M' THEN 'Monthly'
      WHEN 'Y' THEN 'Yearly'
      ELSE 'Unknown'
      END AS Frequency,
                     FormatedDateFromDate(RT.dtStartDate,o2.numDomainId) AS StartDate,
                     FormatedDateFromDate(RT.dtEndDate,o2.numDomainId) AS EndDate,
                     (SELECT COUNT(*) FROM RecurringTransactionReport WHERE numRecTranSeedId = RTR.numRecTranSeedId) AS numNoTransactions,
                     CASE RT.chrIntervalType
      WHEN 'D' THEN DATE_PART('day',coalesce(RT.dtEndDate,TIMEZONE('UTC',now())):: timestamp -RT.dtStartDate:: timestamp)
      WHEN 'M' THEN(DATE_PART('year',AGE(coalesce(RT.dtEndDate,TIMEZONE('UTC',now())),RT.dtStartDate))*12+DATE_PART('month',AGE(coalesce(RT.dtEndDate,TIMEZONE('UTC',now())),RT.dtStartDate)))
      WHEN 'Y' THEN extract(year from coalesce(RT.dtEndDate,TIMEZONE('UTC',now()))) -extract(year from RT.dtStartDate)
      ELSE DATE_PART('day',coalesce(RT.dtEndDate,TIMEZONE('UTC',now())):: timestamp -RT.dtStartDate:: timestamp)
      END AS ExpectedChildRecords
      FROM   RecurringTransactionReport RTR
      INNER JOIN OpportunityMaster AS o1
      ON RTR.numRecTranOppID = o1.numOppId
      INNER JOIN OpportunityMaster AS o2
      ON RTR.numRecTranSeedId = o2.numOppId
      LEFT OUTER JOIN OpportunityRecurring OPR
      ON OPR.numOppId = o2.numOppId
      INNER JOIN RecurringTemplate RT
      ON OPR.numRecurringId = RT.numRecurringId
      AND o1.numDomainId = v_numDomainID
      AND o2.numDomainId = v_numDomainID
      WHERE RTR.tintRecType = 1) AS X;
END; $$;












