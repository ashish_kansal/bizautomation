DROP FUNCTION IF EXISTS GetOrderAssemblyKitInclusionDetails;

CREATE OR REPLACE FUNCTION GetOrderAssemblyKitInclusionDetails
(
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_tintCommitAllocation SMALLINT,
	v_bitFromBizDoc BOOLEAN
)
RETURNS TEXT LANGUAGE plpgsql
AS $$
	DECLARE
	v_vcInclusionDetails  TEXT DEFAULT '';
	v_bitAssembly  BOOLEAN;
	v_bitKitParent  BOOLEAN;
	v_bitWorkOrder  BOOLEAN;
	v_i  INTEGER DEFAULT 1;
	v_iCount  INTEGER;
	v_numTempWOID NUMERIC(18,0);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPTABLEGetOrderAssembly_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLEGetOrderAssembly CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLEGetOrderAssembly
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppItemCode NUMERIC(18,0),
      bitDropship BOOLEAN,
      bitKitParent BOOLEAN,
      numOppChildItemID NUMERIC(18,0),
      numOppKitChildItemID NUMERIC(18,0),
      numTotalQty NUMERIC(18,2),
      numUOMID NUMERIC(18,0),
      tintLevel SMALLINT,
      vcInclusionDetail VARCHAR(1000)
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPTABLEGetOrderAssemblyFINAL_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLEGetOrderAssemblyFINAL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLEGetOrderAssemblyFINAL
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppItemCode NUMERIC(18,0),
      bitDropship BOOLEAN,
      bitKitParent BOOLEAN,
      numOppChildItemID NUMERIC(18,0),
      numOppKitChildItemID NUMERIC(18,0),
      numTotalQty NUMERIC(18,2),
      numUOMID NUMERIC(18,0),
      tintLevel SMALLINT,
      vcInclusionDetail VARCHAR(1000)
   );
   select   coalesce(bitAssembly,false), coalesce(bitKitParent,false), coalesce(bitWorkOrder,false) INTO v_bitAssembly,v_bitKitParent,v_bitWorkOrder FROM
   OpportunityItems
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode WHERE
   numOppId = v_numOppID AND numoppitemtCode = v_numOppItemID;

	IF v_bitKitParent = true then
		--FIRST LEVEL CHILD
		INSERT INTO tt_TEMPTABLEGetOrderAssembly
		(
			numOppItemCode
			,bitDropship
			,bitKitParent
			,numOppChildItemID
			,numOppKitChildItemID
			,numTotalQty
			,numUOMID
			,tintLevel
			,vcInclusionDetail
		)
		SELECT
			OKI.numOppItemID,
			OI.bitDropShip,
			coalesce(I.bitKitParent,false),
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			coalesce(OKI.numUOMId,0),
			1,
			(CASE
				WHEN OKI.numWareHouseItemId > 0 AND coalesce(OI.bitDropShip,false) = false AND coalesce(v_bitFromBizDoc,false) = false
				THEN
					CASE
						WHEN coalesce(I.bitAssembly,false) = true
						THEN
							CASE WHEN CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainId,v_numOppID,v_numOppItemID,OKI.numOppChildItemID,0,false,true,v_numQty*OKI.numQtyItemsReq_Orig) = true
							THEN
								CONCAT('<li>','<span style="color:red">',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),' ',coalesce(UOM.vcUnitName,'Units'),')','</span></li>')
            ELSE
               CONCAT('<li>',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
               ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
            END
         WHEN coalesce(I.bitKitParent,false) = true
         THEN
            CASE WHEN CheckIfAssemblyKitChildItemsIsOnBackorder(OM.numDomainId,v_numOppID,v_numOppItemID,OKI.numOppChildItemID,0,true,false,v_numQty*OKI.numQtyItemsReq_Orig) = true
            THEN
               CONCAT('<li><b><span style="color:red">',I.vcItemName,':</b> ','</span>')
            ELSE
               CONCAT('<li><b>',I.vcItemName,':</b> ')
            END
         ELSE
            CASE
            WHEN v_tintCommitAllocation = 2
            THEN(CASE
               WHEN coalesce(OKI.numQtyItemsReq,0) <= WI.numOnHand
               THEN CONCAT('<li>',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
               ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),
                  ')','</span></li>')
               END)
            ELSE(CASE
               WHEN coalesce(OKI.numQtyItemsReq,0) <= WI.numAllocation
               THEN CONCAT('<li>',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
               ELSE CONCAT('<li>','<span style="color:red">',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),
                  ')','</span></li>')
               END)
            END
         END
      ELSE(CASE
         WHEN coalesce(I.bitKitParent,false) = true
         THEN
            CONCAT('<li><b>',I.vcItemName,':</b> ')
         ELSE
            CONCAT('<li>',I.vcItemName,' (',CAST((v_numQty*OKI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
            ' ',coalesce(UOM.vcUnitName,'Units'),')</li>')
         END)
      END)
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      OpportunityKitItems OKI
      ON
      OI.numoppitemtCode = OKI.numOppItemID
      LEFT JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      LEFT JOIN
      UOM
      ON
      UOM.numUOMId =(CASE WHEN coalesce(OKI.numUOMId,0) > 0 THEN OKI.numUOMId ELSE I.numBaseUnit END)
      WHERE
      OM.numOppId = v_numOppID
      AND OI.numoppitemtCode = v_numOppItemID
      ORDER BY
      OKI.numOppChildItemID;

		--SECOND LEVEL CHILD
      INSERT INTO tt_TEMPTABLEGetOrderAssembly(numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail)
      SELECT
      OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			coalesce(OKCI.numUOMId,0),
			2,
			(CASE
      WHEN OKCI.numWareHouseItemId > 0 AND coalesce(c.bitDropship,false) = false AND coalesce(v_bitFromBizDoc,false) = false
      THEN
         CASE
         WHEN coalesce(I.bitAssembly,false) = true
         THEN
            CASE WHEN CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,v_numOppID,v_numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,false,true,c.numTotalQty*OKCI.numQtyItemsReq_Orig) = true
            THEN
               CONCAT('<span style="color:red">',I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
               ' ',coalesce(UOM.vcUnitName,'Units'),
               ')','</span>')
            ELSE
               CONCAT(I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
               ' ',coalesce(UOM.vcUnitName,'Units'),')')
            END
         WHEN coalesce(I.bitKitParent,false) = true
         THEN
            CASE WHEN CheckIfAssemblyKitChildItemsIsOnBackorder(I.numDomainID,v_numOppID,v_numOppItemID,c.numOppChildItemID,OKCI.numOppKitChildItemID,true,false,c.numTotalQty*OKCI.numQtyItemsReq_Orig) = true
            THEN
               CONCAT('<span style="color:red">',I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
               ' ',coalesce(UOM.vcUnitName,'Units'),
               ')','</span>')
            ELSE
               CONCAT(I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
               ' ',coalesce(UOM.vcUnitName,'Units'),')')
            END
         ELSE
            CASE
            WHEN v_tintCommitAllocation = 2
            THEN(CASE
               WHEN coalesce(numQtyItemsReq,0) <= WI.numOnHand
               THEN CONCAT(I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),')')
               ELSE CONCAT('<span style="color:red">',I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),
                  ')','</span>')
               END)
            ELSE(CASE
               WHEN coalesce(numQtyItemsReq,0) <= WI.numAllocation
               THEN CONCAT(I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),')')
               ELSE CONCAT('<span style="color:red">',I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
                  ' ',coalesce(UOM.vcUnitName,'Units'),
                  ')','</span>')
               END)
            END
         END
      ELSE CONCAT(I.vcItemName,' (',CAST((c.numTotalQty*OKCI.numQtyItemsReq_Orig) AS DOUBLE PRECISION),
         ' ',coalesce(UOM.vcUnitName,'Units'),')')
      END)
      FROM
      OpportunityKitChildItems OKCI
      LEFT JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode
      LEFT JOIN
      UOM
      ON
      UOM.numUOMId =(CASE WHEN coalesce(OKCI.numUOMId,0) > 0 THEN OKCI.numUOMId ELSE I.numBaseUnit END)
      INNER JOIN
      tt_TEMPTABLEGetOrderAssembly c
      ON
      OKCI.numOppId = v_numOppID
      AND OKCI.numOppItemID = c.numOppItemCode
      AND OKCI.numOppChildItemID = c.numOppChildItemID;
      INSERT INTO tt_TEMPTABLEGetOrderAssemblyFINAL(numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID,
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail)
      SELECT
      numOppItemCode,
			bitDropship,
			bitKitParent,
			numOppChildItemID,
			numOppKitChildItemID,
			numTotalQty,
			numUOMID,
			tintLevel,
			vcInclusionDetail
      FROM
      tt_TEMPTABLEGetOrderAssembly
      WHERE
      tintLevel = 1;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE tintLevel = 1;
      WHILE v_i <= v_iCount LOOP
         IF (coalesce((SELECT bitKitParent FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i),false) = false OR (coalesce((SELECT bitKitParent FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i),false) = true AND(SELECT COUNT(*) FROM tt_TEMPTABLEGetOrderAssembly WHERE tintLevel = 2 AND numOppChildItemID =(SELECT numOppChildItemID FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i)) > 0)) then
			
            v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,(SELECT vcInclusionDetail FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i));
         end if;
         IF(SELECT COUNT(*) FROM tt_TEMPTABLEGetOrderAssembly WHERE tintLevel = 2 AND numOppChildItemID =(SELECT numOppChildItemID FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i)) > 0 then
			
				--SET @vcInclusionDetails = CONCAT('<li>',@vcInclusionDetails,':')

            select   COALESCE(v_vcInclusionDetails,'') || vcInclusionDetail INTO v_vcInclusionDetails FROM tt_TEMPTABLEGetOrderAssembly WHERE tintLevel = 2 AND numOppChildItemID =(SELECT numOppChildItemID FROM tt_TEMPTABLEGetOrderAssemblyFINAL WHERE ID = v_i);
            v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,'</li>');
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      v_vcInclusionDetails := CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',v_vcInclusionDetails,
      '</ul>');
   ELSEIF v_bitAssembly = true AND v_bitWorkOrder = true
   then
      DROP TABLE IF EXISTS tt_TEMPGetOrderAssemblyKitInclusionDetails CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPGetOrderAssemblyKitInclusionDetails
      (
         ItemLevel INTEGER,
         numParentWOID NUMERIC(18,0),
         numWOID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numQty DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         numUOMID NUMERIC(18,0),
         bitWorkOrder BOOLEAN,
         tintBuildStatus SMALLINT
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPFINAL_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPFINAL CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINAL
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numParentWOID NUMERIC(18,0),
         numWOID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numQty DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         numUOMID NUMERIC(18,0),
         bitWorkOrder BOOLEAN,
         tintBuildStatus SMALLINT
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPWO_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPWO CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPWO
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numWOID NUMERIC(18,0)
      );
      INSERT
      INTO tt_TEMPGetOrderAssemblyKitInclusionDetails   with recursive CTEWorkOrder(ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,
      bitWorkOrder,tintBuildStatus) AS(SELECT
      CAST(1 AS INTEGER) AS ItemLevel,
				numParentWOID AS numParentWOID,
				numWOId AS numWOID,
				numItemCode AS numItemKitID,
				CAST(numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
				numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
      FROM
      WorkOrder
      WHERE
      numOppId = v_numOppID
      AND numOppItemID = v_numOppItemID
      UNION ALL
      SELECT
      c.ItemLevel+2 AS ItemLevel,
				c.numWOID AS numParentWOID,
				WorkOrder.numWOId AS numWOID,
				WorkOrder.numItemCode AS numItemKitID,
				CAST(WorkOrder.numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
				WorkOrder.numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
      FROM
      WorkOrder
      INNER JOIN
      CTEWorkOrder c
      ON
      WorkOrder.numParentWOID = c.numWOID) SELECT
      ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			0,
			bitWorkOrder,
			tintBuildStatus
      FROM
      CTEWorkOrder
      INNER JOIN
      Item
      ON
      CTEWorkOrder.numItemKitID = Item.numItemCode
      LEFT JOIN
      WareHouseItems
      ON
      CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
      INSERT INTO
      tt_TEMPGetOrderAssemblyKitInclusionDetails
      SELECT
      t1.ItemLevel::bigint+1,
			t1.numWOID,
			NULL,
			WorkOrderDetails.numChildItemID,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			WorkOrderDetails.numUOMId,
			false AS bitWorkOrder,
			(CASE
      WHEN t1.tintBuildStatus = 2
      THEN 2
      ELSE(CASE
         WHEN Item.charItemType = 'P'
         THEN
            CASE
            WHEN v_tintCommitAllocation = 2
            THEN(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
            ELSE(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END)
            END
         ELSE 1
         END)
      END)
      FROM
      WorkOrderDetails
      INNER JOIN
      tt_TEMPGetOrderAssemblyKitInclusionDetails t1
      ON
      WorkOrderDetails.numWOId = t1.numWOID
      INNER JOIN
      Item
      ON
      WorkOrderDetails.numChildItemID = Item.numItemCode
      AND 1 =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPGetOrderAssemblyKitInclusionDetails TInner WHERE TInner.bitWorkOrder = true AND TInner.numParentWOID = t1.numWOID AND TInner.numItemCode = Item.numItemCode) > 0 THEN 0 ELSE 1 END)
      LEFT JOIN
      WareHouseItems
      ON
      WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
      v_i  := 1;
      INSERT INTO tt_TEMPWO(numWOID) SELECT numWOID FROM tt_TEMPGetOrderAssemblyKitInclusionDetails WHERE bitWorkOrder = true ORDER BY ItemLevel DESC;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPWO;
      WHILE v_i <= v_iCount LOOP
         select   numWOID INTO v_numTempWOID FROM tt_TEMPWO WHERE ID = v_i;
         UPDATE
         tt_TEMPGetOrderAssemblyKitInclusionDetails T1
         SET
         tintBuildStatus =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPGetOrderAssemblyKitInclusionDetails TInner WHERE TInner.numParentWOID = T1.numWOID AND tintBuildStatus = 0) > 0 THEN 0 ELSE 1 END)
			
         WHERE
         numWOID = v_numTempWOID
         AND T1.tintBuildStatus <> 2;
         v_i := v_i::bigint+1;
      END LOOP;
      INSERT INTO tt_TEMPFINAL(numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMID
			,bitWorkOrder
			,tintBuildStatus)
      SELECT
      numParentWOID
			,numWOID
			,numItemCode
			,numQty
			,numWarehouseItemID
			,numUOMId
			,bitWorkOrder
			,tintBuildStatus
      FROM
      tt_TEMPGetOrderAssemblyKitInclusionDetails
      WHERE
      ItemLevel = 2;
      v_i := 1;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPFINAL;
      WHILE v_i <= v_iCount LOOP
         v_vcInclusionDetails := CONCAT(v_vcInclusionDetails,(CASE WHEN LENGTH(v_vcInclusionDetails) > 0 THEN ', ' ELSE '' END),
         (SELECT(CASE
         WHEN T1.tintBuildStatus = 1 OR T1.tintBuildStatus = 2
         THEN
            CONCAT(I.vcItemName,' (',CAST(T1.numQty AS DOUBLE PRECISION),' ',coalesce(UOM.vcUnitName,'Units'),
            ')')
         ELSE
            CONCAT('<span style="color:red">',I.vcItemName,' (',CAST(T1.numQty AS DOUBLE PRECISION),
            ' ',coalesce(UOM.vcUnitName,'Units'),')','</span>')
         END)
         FROM
         tt_TEMPFINAL T1
         INNER JOIN
         Item I
         ON
         T1.numItemCode = I.numItemCode
         LEFT JOIN
         WareHouseItems WI
         ON
         T1.numWarehouseItemID = WI.numWareHouseItemID
         LEFT JOIN
         UOM
         ON
         UOM.numUOMId =(CASE WHEN coalesce(T1.numUOMID,0) > 0 THEN T1.numUOMID ELSE I.numBaseUnit END)
         WHERE
         ID = v_i));
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
	

   RETURN REPLACE(v_vcInclusionDetails,'(, ','(');
END; $$;

