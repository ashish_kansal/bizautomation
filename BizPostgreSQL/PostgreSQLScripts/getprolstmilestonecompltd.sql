-- Function definition script GetProLstMileStoneCompltd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProLstMileStoneCompltd(v_numProid NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPercentage  VARCHAR(100);
BEGIN
   select  numstagepercentage INTO v_numPercentage from ProjectsStageDetails where numProId = v_numProid and numstagepercentage not  in(select numstagepercentage from ProjectsStageDetails
      where numProId = v_numProid  and bitStageCompleted = false)
   and numstagepercentage != 100 and bitStageCompleted = true   order by numstagepercentage desc  LIMIT 1;
   if v_numPercentage != '' then

      v_numPercentage := 'Milestone -' || coalesce(v_numPercentage,'') || ' %';
   end if;



   return coalesce(v_numPercentage,'-');
END; $$;

