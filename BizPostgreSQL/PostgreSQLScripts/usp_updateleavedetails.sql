-- Stored procedure definition script USP_UpdateLeaveDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateLeaveDetails(v_numCategoyHDRID NUMERIC(9,0) DEFAULT 0,
v_numType NUMERIC(9,0) DEFAULT 0,
v_bitApproved BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update timeandexpense set bitApproved = v_bitApproved,numtype = v_numType
   where numCategoryHDRID = v_numCategoyHDRID;
   RETURN;
END; $$;


