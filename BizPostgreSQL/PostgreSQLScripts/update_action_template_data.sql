-- Stored procedure definition script Update_Action_Template_Data for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Update_Action_Template_Data(v_templateName VARCHAR(100) ,    
v_dueDays INTEGER ,    
v_status INTEGER ,    
v_type VARCHAR(25) ,     
v_activity INTEGER ,    
v_comments VARCHAR(2000),  
v_rowID INTEGER ,    
v_bitSendEmailTemplate BOOLEAN,    
v_numEmailTemplate NUMERIC(9,0),    
v_tintHours SMALLINT DEFAULT 0,    
v_bitAlert BOOLEAN DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT NULL  ,
v_numUserCntID NUMERIC(9,0) DEFAULT NULL ,
v_numTaskType NUMERIC(9,0) DEFAULT NULL,
v_bitRemind BOOLEAN DEFAULT FALSE,
v_numRemindBeforeMinutes INTEGER DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UpDate tblActionItemData
   Set  TemplateName = v_templateName,DueDays = v_dueDays,Priority = v_status,
   Type = v_type,Activity = v_activity,Comments = v_comments,bitSendEmailTemplate = v_bitSendEmailTemplate,
   numEmailTemplate = v_numEmailTemplate,tintHours = v_tintHours,
   bitAlert = v_bitAlert,numDomainID = v_numDomainID,numModifiedBy = v_numUserCntID,
   dtModifiedBy = TIMEZONE('UTC',now()),
   numTaskType = v_numTaskType,bitRemind = CAST(v_bitRemind AS BOOLEAN),numRemindBeforeMinutes = v_numRemindBeforeMinutes
   Where RowID = v_rowID;
   RETURN;
END; $$;


