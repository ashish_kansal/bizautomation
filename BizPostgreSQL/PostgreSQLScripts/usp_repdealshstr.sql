-- Stored procedure definition script USP_RepDealsHstr for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RepDealsHstr(v_numDomainID NUMERIC,                    
  v_numFromDate TIMESTAMP,                    
  v_numToDate TIMESTAMP,                    
  v_numUserCntID NUMERIC DEFAULT 0,                         
  v_tintType SMALLINT DEFAULT NULL,             
  v_tintSortOrder INTEGER DEFAULT NULL,            
  v_tintRights INTEGER DEFAULT NULL,          
  v_DivisionID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   v_strSql := 'SELECT  Opp.numOppId,                     
  Opp.vcPOppName AS Name,                     
  GetOppStatus(Opp.numOppId) as STAGE,                     
  Opp.intPEstimatedCloseDate AS CloseDate,                     
  coalesce(Opp.numCreatedBy,0) ,                    
  ADC.vcFirstName || '' '' || ADC.vcLastName AS Contact,                     
  C.vcCompanyName AS Company,                    
  C.numCompanyId, case when Opp.tintOppType = 1 then ''Sales'' when Opp.tintOppType = 2 then ''Purchase'' end as Type,                
  Div.tintCRMType,                    
  ADC.numContactId,                    
  Div.numDivisionID,                    
  Div.numTerID,                    
  Opp.monPAmount,                    
  ADC1.vcFirstName || '' '' || ADC1.vcLastName  AS vcUserName                    
  FROM OpportunityMaster Opp                     
  INNER JOIN AdditionalContactsInformation ADC                     
  ON Opp.numContactId = ADC.numContactId                     
  INNER JOIN DivisionMaster Div                     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                     
  INNER JOIN CompanyInfo C                     
  ON Div.numCompanyID = C.numCompanyId   
  join    AdditionalContactsInformation  ADC1    
  on ADC1.numContactId = Opp.numRecOwner  
  WHERE Opp.tintOppStatus <> 0 and Opp.tintOppStatus <> 3 and Opp.bintCreatedDate Between ''' || SUBSTR(CAST(v_numFromDate AS VARCHAR(20)),1,20) || ''' And ''' || SUBSTR(CAST(v_numToDate AS VARCHAR(20)),1,20) || '''';                      
   if (v_tintSortOrder = 1 or  v_tintSortOrder = 2) then  
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintOppStatus <> 2 AND Opp.tintOppType = ' || SUBSTR(CAST(v_tintSortOrder AS VARCHAR(1)),1,1);
   end if;                    
   if v_tintSortOrder = 3 then  
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintOppStatus = 2 AND Opp.tintOppType = 1';
   end if;            
   if v_tintSortOrder = 4 then  
      v_strSql := coalesce(v_strSql,'') || ' and Opp.tintOppStatus = 2 AND Opp.tintOppType = 2';
   end if;           
   if v_tintSortOrder = 5 then  
      v_strSql := coalesce(v_strSql,'') || ' and Opp.numDivisionId =' || SUBSTR(CAST(v_DivisionID AS VARCHAR(15)),1,15);
   end if;           
   if v_tintRights = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' and Opp.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
   end if;                    
   if v_tintRights = 2 then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC1.numTeam in(select F.numTeam from ForReportsByTeam F                 
  where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ')';
   end if;            
   if v_tintRights = 3 then 
      v_strSql := coalesce(v_strSql,'') || ' and Div.numTerId in(select F.numTerritory from ForReportsByTerritory F                        
  where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(5)),1,5) || ')';
   end if;                 
   RAISE NOTICE '%',v_strSql;
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


