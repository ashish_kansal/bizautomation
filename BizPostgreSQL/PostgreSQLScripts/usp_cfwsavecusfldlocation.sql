-- Stored procedure definition script USP_cfwSaveCusfldLocation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_cfwSaveCusfldLocation(v_numDomainID NUMERIC(9,0) DEFAULT 0 ,   
v_strDetails TEXT DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_RecordCount  INTEGER; 
   v_vcTempData  TEXT;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDATA
   (
      ItemsData VARCHAR(50)
   );
   DROP TABLE IF EXISTS tt_TEMPDATALOCATION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDATALOCATION
   (
      fld_id BIGINT,
      vcitemsLocation VARCHAR(500)
   );
   ALTER TABLE tt_TEMPDATA add ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1);
   ALTER TABLE tt_TEMPDATALOCATION add ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1);
   INSERT INTO tt_TEMPDATA(ItemsData)
   SELECT Items FROM Split(v_strDetails,'_') WHERE Items <> '';

	
open SWV_RefCur for SELECT * FROM tt_TEMPDATA;
   SELECT COUNT(*) INTO v_RecordCount FROM tt_TEMPDATA;
   WHILE(v_i <= v_RecordCount) LOOP
      select   ItemsData INTO v_vcTempData FROM tt_TEMPDATA WHERE ID = v_i;
      UPDATE CFW_Fld_Master SET vcItemsLocation =(SELECT Items FROM Split(v_vcTempData,'#') WHERE Items <> ''  ORDER BY(SELECT NULL)) WHERE fld_id IN (SELECT Id FROM SplitIds(v_vcTempData,'#') ORDER BY(SELECT NULL));
      v_i := v_i::bigint+1;
   END LOOP;


   RETURN;
END; $$;












