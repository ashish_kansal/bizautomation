DROP FUNCTION IF EXISTS USP_ElasticSearch_GetContacts;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetContacts(v_numDomainID NUMERIC(18,0)
	,v_vcContactIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numContactId AS id
		,'contact' AS module
		,coalesce(tintCRMType,0) AS "tintOrgCRMType"
		,coalesce(numCompanyType,0) AS "numOrgCompanyType"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,coalesce(AdditionalContactsInformation.numRecOwner,0) AS "numRecOwner"
		,CONCAT('<b style="color:#0099cc">Contact:</b> ',(CASE WHEN LENGTH(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),
   ' ',(CASE WHEN LENGTH(vcLastname) > 0 THEN vcLastname ELSE '-' END),
   ', ',(CASE WHEN LENGTH(vcEmail) > 0 THEN vcEmail ELSE '-' END), 
   ', ',vcCompanyName) AS displaytext
		,CONCAT((CASE WHEN LENGTH(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),' ',
   (CASE WHEN LENGTH(vcLastname) > 0 THEN vcLastname ELSE '-' END)) AS text
		,CONCAT('/contact/frmContacts.aspx?CntId=',numContactId) AS url
		,coalesce(vcFirstName,'') AS "Search_vcFirstName"
		,coalesce(vcLastname,'') AS "Search_vcLastName"
		,CONCAT(coalesce(vcFirstName,''),' ',coalesce(vcLastname,'')) AS "Search_vcFullName"
		,coalesce(vcEmail,'') AS "Search_vcEmail"
		,coalesce(vcCompanyName,'') AS "Search_vcCompanyName"
		,coalesce(AdditionalContactsInformation.numCell,'') AS "Search_numCell"
		,coalesce(AdditionalContactsInformation.numHomePhone,'') AS "Search_numHomePhone"
		,coalesce(AdditionalContactsInformation.numPhone,'') AS "Search_numPhone"
   FROM
   AdditionalContactsInformation
   INNER JOIN
   DivisionMaster
   ON
   AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   AdditionalContactsInformation.numDomainID = v_numDomainID
   AND (NULLIF(v_vcContactIds,'') IS NULL OR AdditionalContactsInformation.numContactId IN(SELECT id FROM SplitIDs(coalesce(v_vcContactIds,'0'),',')));
END; $$;
-- USP_ExportDataBackUp 1,9












