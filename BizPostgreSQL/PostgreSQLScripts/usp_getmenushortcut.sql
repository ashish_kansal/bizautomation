-- Stored procedure definition script USP_GetMenuShortCut for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMenuShortCut(v_numGroupId NUMERIC,
    v_numDomainId NUMERIC,
    v_numcontactId NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*)
   FROM   ShortCutUsrCnf
   WHERE  numGroupID = v_numGroupId
   AND numDomainID = v_numDomainId
   AND numContactId = v_numcontactId
   AND coalesce(numTabId,0) > 0) > 0 then
        
      open SWV_RefCur for
      SELECT  Hdr.ID,
                    Hdr.vcLinkName AS Name,
                    Hdr.Link,
                    Hdr.numTabId,
                    Hdr.vcImage,
                    coalesce(Hdr.bitNew,false) AS bitNew,
                    coalesce(Hdr.bitNewPopup,false) AS bitNewPopup,
                    Hdr.NewLink,
                    coalesce(Hdr.bitPopup,false) AS bitPopup,
                    sconf.numOrder,
                    coalesce(sconf.bitFavourite,false) AS bitFavourite
      FROM    ShortCutUsrCnf sconf
      JOIN ShortCutBar Hdr ON Hdr.ID = sconf.numLinkid
      AND sconf.numTabId = Hdr.numTabId
      WHERE   sconf.numDomainID = v_numDomainId
      AND sconf.numGroupID = v_numGroupId
      AND sconf.numContactId = v_numcontactId
      AND coalesce(sconf.tintLinkType,0) = 0
      UNION
      SELECT  LD.numListItemID,
                    LD.vcData AS Name,
                    '../prospects/frmCompanyList.aspx?RelId='
      || CAST(LD.numListItemID AS VARCHAR(10)) AS link,
                    sconf.numTabId,
                    CAST(null as VARCHAR(255)) AS vcImage,
                    true AS bitNew,
                    true AS bitNewPopup,
                    '../include/frmAddOrganization.aspx?RelID='
      || CAST(LD.numListItemID AS VARCHAR(10)) || '&FormID=36' AS NewLink,
                    false AS bitPopup,
                    sconf.numOrder,
                    coalesce(sconf.bitFavourite,false) AS bitFavourite
      FROM    ShortCutUsrCnf sconf
      JOIN Listdetails LD ON LD.numListItemID = sconf.numLinkid
      WHERE   LD.numListID = 5
      AND LD.numListItemID <> 46
      AND (LD.numDomainid = v_numDomainId
      OR constflag = true)
      AND sconf.numDomainID = v_numDomainId
      AND sconf.numGroupID = v_numGroupId
      AND sconf.numContactId = v_numcontactId
      AND coalesce(sconf.tintLinkType,0) = 5
      UNION ALL
      SELECT  LD.numListItemID,
                    LD.vcData AS Name,
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
      || CAST(LD.numListItemID AS VARCHAR(10)) AS link,
                    sconf.numTabId,
                    CAST(null as VARCHAR(255)) AS vcImage,
                    false AS bitNew,
                    false AS bitNewPopup,
                    '' AS NewLink,
                    false AS bitPopup,
                    sconf.numOrder,
                    coalesce(sconf.bitFavourite,false) AS bitFavourite
      FROM    Listdetails LD
      JOIN ShortCutUsrCnf sconf ON LD.numListItemID = sconf.numLinkid
      WHERE   numListID = 27
      AND LD.numDomainid = v_numDomainId
      AND coalesce(constFlag,false) = false
      AND sconf.numContactId = v_numcontactId
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
      UNION ALL
      SELECT  LD.numListItemID,
                    LD.vcData AS Name,
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
      || CAST(LD.numListItemID AS VARCHAR(10)) AS link,
                    sconf.numTabId,
                    CAST(null as VARCHAR(255)) AS vcImage,
                    false AS bitNew,
                    false AS bitNewPopup,
                    '' AS NewLink,
                    false AS bitPopup,
                    sconf.numOrder,
                    coalesce(sconf.bitFavourite,false) AS bitFavourite
      FROM    Listdetails LD
      JOIN ShortCutUsrCnf sconf ON LD.numListItemID = sconf.numLinkid
      WHERE   numListID = 27
      AND LD.numDomainid = v_numDomainId
      AND coalesce(constFlag,false) = true
      ORDER BY 10;
   ELSE
      IF(SELECT COUNT(*)
      FROM   ShortCutGrpConf
      WHERE  numDomainId = v_numDomainId
      AND numGroupId = v_numGroupId
      AND coalesce(numTabId,0) > 0) > 0 then
                
         open SWV_RefCur for
         SELECT  Hdr.ID,
                            Hdr.vcLinkName AS Name,
                            Hdr.Link,
                            Hdr.numTabId,
                            Hdr.vcImage,
                            coalesce(Hdr.bitNew,false) AS bitNew,
                            coalesce(Hdr.bitNewPopup,false) AS bitNewPopup,
                            Hdr.NewLink,
                            coalesce(Hdr.bitPopup,false) AS bitPopup,
                            sconf.numOrder,
                            false AS bitFavourite
         FROM    ShortCutGrpConf sconf
         JOIN ShortCutBar Hdr ON Hdr.ID = sconf.numLinkId
         AND sconf.numTabId = Hdr.numTabId
         WHERE   sconf.numGroupId = v_numGroupId
         AND sconf.numDomainId = v_numDomainId
         AND Hdr.numContactId = 0
         AND coalesce(sconf.tintLinkType,0) = 0
         UNION
         SELECT  LD.numListItemID,
                            LD.vcData AS Name,
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            sconf.numTabId,
                            CAST(null as VARCHAR(255)) AS vcImage,
                            false AS bitNew,
                            false AS bitNewPopup,
                            '' AS NewLink,
                            false AS bitPopup,
                            sconf.numOrder,
                            false AS bitFavourite
         FROM    ShortCutGrpConf sconf
         JOIN Listdetails LD ON LD.numListItemID = sconf.numLinkId
         WHERE   LD.numListID = 5
         AND LD.numListItemID <> 46
         AND (LD.numDomainid = v_numDomainId
         OR constflag = true)
         AND sconf.numGroupId = v_numGroupId
         AND sconf.numDomainId = v_numDomainId
         AND coalesce(sconf.tintLinkType,0) = 5
         UNION ALL
         SELECT  LD.numListItemID,
                            LD.vcData AS Name,
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            sconf.numTabId,
                            CAST(null as VARCHAR(255)) AS vcImage,
                            false AS bitNew,
                            false AS bitNewPopup,
                            '' AS NewLink,
                            false AS bitPopup,
                            sconf.numOrder,
                            coalesce(sconf.bitFavourite,false) AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf sconf ON LD.numListItemID = sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,false) = false
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
         UNION ALL
         SELECT  LD.numListItemID,
                            LD.vcData AS Name,
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
         || CAST(LD.numListItemID AS VARCHAR(10)) AS link,
                            sconf.numTabId,
                            CAST(null as VARCHAR(255)) AS vcImage,
                            false AS bitNew,
                            false AS bitNewPopup,
                            '' AS NewLink,
                            false AS bitPopup,
                            sconf.numOrder,
                            coalesce(sconf.bitFavourite,false) AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf sconf ON LD.numListItemID = sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,false) = true
         ORDER BY 10;
      ELSE
         open SWV_RefCur for
         SELECT  ID,
                            vcLinkName AS Name,
                            Link,
                            numTabId,
                            vcImage,
                            coalesce(bitNew,false) AS bitNew,
                            coalesce(bitNewPopup,false) AS bitNewPopup,
                            NewLink,
                            coalesce(bitPopup,false) AS bitPopup,
                            false AS bitFavourite
         FROM    ShortCutBar
         WHERE   bitdefault = true
         AND numContactId = 0
         AND coalesce(numTabId,0) > 0
         UNION
         SELECT  numListItemID AS id,
                            vcData AS Name,
                            '../prospects/frmCompanyList.aspx?RelId='
         || CAST(numListItemID AS VARCHAR(10)) AS link,
                            7 AS numTabId,
                            CAST(null as INTEGER) AS vcImage,
                            false AS bitNew,
                            true AS bitNewPopup,
                            '../include/frmAddOrganization.aspx?RelID='
         || CAST(numListItemID AS VARCHAR(10))
         || '&FormID=36' AS NewLink,
                            false AS bitPopup,
                            false AS bitFavourite
         FROM    Listdetails
         WHERE   numListID = 5
         AND numListItemID <> 46
         AND (numDomainid = v_numDomainId
         OR constFlag = true);
         open SWV_RefCur2 for
         SELECT  numListItemID AS id,
                            vcData AS Name,
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
         || CAST(numListItemID AS VARCHAR(10)) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            false AS bitNew,
                            false AS bitNewPopup,
                            '' AS NewLink,
                            false AS bitPopup,
                            false AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,false) = false
         UNION ALL
         SELECT  numListItemID AS id,
                            vcData AS Name,
                            CAST('../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS VARCHAR(44)) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            false AS bitNew,
                            false AS bitNewPopup,
                            '' AS NewLink,
                            false AS bitPopup,
                            false AS bitFavourite
         FROM    Listdetails LD
         JOIN ShortCutUsrCnf Sconf ON LD.numListItemID = Sconf.numLinkid
         WHERE   numListID = 27
         AND LD.numDomainid = v_numDomainId
         AND coalesce(constFlag,false) = true;
      end if;
   end if;
   RETURN;
END; $$;    
 


