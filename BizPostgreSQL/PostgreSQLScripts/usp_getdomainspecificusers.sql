-- Stored procedure definition script usp_GetDomainSpecificUsers for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDomainSpecificUsers(v_numDomainId NUMERIC,
	v_numUserId NUMERIC  
-- 
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numUserId as VARCHAR(255)),cast(vcUserName as VARCHAR(255)),cast(vcUserDesc as VARCHAR(255)),cast(numDomainID as VARCHAR(255))
   FROM UserMaster WHERE numDomainID = v_numDomainId and numUserId <> v_numUserId;
END; $$;












