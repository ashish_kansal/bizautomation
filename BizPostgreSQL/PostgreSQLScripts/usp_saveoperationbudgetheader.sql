-- Stored procedure definition script USP_SaveOperationBudgetHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveOperationBudgetHeader(v_numBudgetId NUMERIC(9,0) DEFAULT 0,                          
v_vcBudgetName VARCHAR(100) DEFAULT '',                
v_numDepartmentId NUMERIC(9,0) DEFAULT 0,             
v_numDivisionId NUMERIC(9,0) DEFAULT 0,                          
v_numCostCenterId NUMERIC(9,0) DEFAULT 0,        
v_intFiscalYear NUMERIC(9,0) DEFAULT 0,                 
v_numDomainId NUMERIC(9,0) DEFAULT 0,                          
v_numUserCntId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_count  INTEGER;      
      
----Select @count=Count(*) From OperationBudgetMaster      
----Where numDivisionId=@numDivisionId And numDepartmentId=@numDepartmentId      
----And numCostCenterId=@numCostCenterId And numDomainId=@numDomainId --And numBudgetId<>@numBudgetId --And @intFiscalYear=intFiscalYear          
BEGIN
   if v_numBudgetId = 0 then

      Insert into OperationBudgetMaster(vcBudgetName,numDivisionId,numDepartmentId,numCostCenterId,intFiscalYear,numDomainId,numCreatedBy,dtCreationDate)
 Values(v_vcBudgetName,v_numDivisionId,v_numDepartmentId,v_numCostCenterId,v_intFiscalYear,v_numDomainId,v_numUserCntId,TIMEZONE('UTC',now()));
 
      v_numBudgetId := CURRVAL('OperationBudgetMaster_seq');
      open SWV_RefCur for
      Select v_numBudgetId;
   Else
      Update OperationBudgetMaster Set vcBudgetName = v_vcBudgetName,numDivisionId = v_numDivisionId,numDepartmentId = v_numDepartmentId,
      numCostCenterId = v_numCostCenterId,numModifiedBy = v_numUserCntId,
      dtModifiedDate = TIMEZONE('UTC',now()) Where numBudgetId = v_numBudgetId And  numDomainId = v_numDomainId;
      open SWV_RefCur for
      Select v_numBudgetId;
   end if;
   RETURN;
END; $$;


