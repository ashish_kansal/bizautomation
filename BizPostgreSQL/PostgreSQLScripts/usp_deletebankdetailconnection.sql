-- Stored procedure definition script USP_DeleteBankDetailConnection for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

/****** Added By : Joseph ******/
/******  Deletes the Active Connection Flag between Bank Account(mapped in Biz) and Financial Institution ******/

CREATE OR REPLACE FUNCTION USP_DeleteBankDetailConnection(v_numBankDetailID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT  numBankDetailID
   FROM    BankDetails
   WHERE   numBankDetailID = v_numBankDetailID) then
        
      UPDATE  BankDetails
      SET     bitIsActive = false,vcUserName = '',vcPassword = '',dtModifiedDate = LOCALTIMESTAMP
      WHERE   numBankDetailID = v_numBankDetailID;
      UPDATE  Chart_Of_Accounts
      SET     numBankDetailID = 0,IsConnected = false,IsBankAccount = true
      WHERE   numBankDetailID = v_numBankDetailID;
   end if;
   RETURN;
END; $$;


