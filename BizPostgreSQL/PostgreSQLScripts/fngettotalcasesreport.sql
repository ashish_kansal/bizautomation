-- Function definition script fnGetTotalCasesReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fnGetTotalCasesReport(v_numStatus SMALLINT,
	v_numAssignedTo BIGINT,
	v_intFromDate TIMESTAMP,
	v_intToDate TIMESTAMP)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalCases  INTEGER;
BEGIN
   if v_numStatus = 0 then
        
      select   COUNT(*) INTO v_TotalCases FROM Cases C WHERE C.numAssignedTo = v_numAssignedTo
      AND C.bintCreatedDate BETWEEN v_intFromDate AND v_intToDate;
   end if;
	
   if v_numStatus <> 0 then
	
      select   COUNT(*) INTO v_TotalCases FROM Cases C WHERE C.numAssignedTo = v_numAssignedTo AND C.numStatus = v_numStatus
      AND C.bintCreatedDate BETWEEN v_intFromDate AND v_intToDate;
   end if;
	
   RETURN v_TotalCases;
END; $$;

