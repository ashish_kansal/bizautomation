-- Stored procedure definition script USP_GetChartAcntDescription for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartAcntDescription(v_numChartAcntId NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select vcAccountName from Chart_Of_Accounts Where numAccountId = v_numChartAcntId And numDomainId = v_numDomainID;
END; $$;












