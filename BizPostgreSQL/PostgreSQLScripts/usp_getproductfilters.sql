-- Stored procedure definition script USP_GetProductFilters for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_GetProductFilters(v_numDomainId NUMERIC ,
    v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
    v_FilterQuery VARCHAR(2000) DEFAULT '',
    v_numCategoryID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sqlQuery  TEXT; 
   v_sqlwhere  TEXT; 
   v_sqlFilter  TEXT;
   v_dynamicFilterQuery  TEXT; 
   v_where  TEXT; 
   v_data  VARCHAR(100);
   v_row  INTEGER;        
   v_checkbox  VARCHAR(2000);
   v_checkboxGroupBy  VARCHAR(2000);
   v_dropdown  VARCHAR(2000);
   v_dropdownGroupBy  VARCHAR(2000);
   v_textbox  VARCHAR(2000);
   v_textboxGroupBy  VARCHAR(2000);
   v_filterNumItemIds  VARCHAR(2000);
   v_priceStart  VARCHAR(3000);
   v_priceInnerQuery  VARCHAR(3000);
   v_priceEnd  VARCHAR(3000);
   v_manufacturer  VARCHAR(2000);
   v_manufacturerGroupBy  VARCHAR(2000);
   v_attributes  VARCHAR(2000);
   v_filterByNumItemCode  VARCHAR(3000);
   v_count  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
      v_where := '';
      v_checkbox := '';
      IF v_numWareHouseID = 0 then
            
         select   coalesce(numDefaultWareHouseID,0) INTO v_numWareHouseID FROM    eCommerceDTL WHERE   numDomainID = v_numDomainId;
      end if;
      IF LENGTH(v_FilterQuery) > 0 then
				
         Drop table IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
            SELECT row_number() over(order by OutParam) as row, OutParam  FROM SplitString(v_FilterQuery,';');
         select   row, data INTO v_row,v_data from tt_TEMP     LIMIT 1;
         v_dynamicFilterQuery := '';
         v_count := 0;
         WHILE v_row > 0 LOOP
            IF SUBSTR(v_data,1,1) = 'f' then
	                        
               IF  POSITION('-2' IN v_data) >  0 then
	                                
                  v_dynamicFilterQuery  :=  ' SELECT   I.numItemCode 
                                                                    FROM 
                                                                         ( 
                                                                           ( SELECT              
                                                                                  numItemCode ,
                                                                                  CEILING
                                                                                  (
                                                                                     ISNULL
                                                                                     (
                                                                                       CASE WHEN I.[charItemType]=''P'' 
                                                                                            THEN 
                                                                                                 CASE WHEN I.bitSerialized = true 
                                                                                                      THEN 
                                                                                                          (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                                                                      ELSE 
                                                                                                          (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                                                           WHERE [numItemID]= I.numItemCode 
                                                                                                           and  [numWareHouseID]=' || SUBSTR(CAST(v_numWareHouseID AS VARCHAR(10)),1,10)
                  || ' ) 
                                                                                                  END
								                                                             ELSE 
								                                                                  (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                                                             END,0) 
								                                                    )  
                                                                             AS Price
                                                                             FROM 
                                                                                    dbo.ItemCategory IC 
                                                                             JOIN   Item I 
                                                                             ON     I.numItemCode = IC.numItemID
                                                                             WHERE
                                                                                       IC.numCategoryId = ' || SUBSTR(CAST(v_numCategoryID AS VARCHAR(10)),1,10) || '
                                                                                    AND I.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || '
                                                                          ))
                                                                    AS I 
                                                                    WHERE       I.Price 
                                                                    BETWEEN     SUBSTRING(''' || coalesce(v_data,'') || ''',CHARINDEX(''='',''' || coalesce(v_data,'') || ''')+1,(CHARINDEX(''~'',''' || coalesce(v_data,'') || ''')) - (CHARINDEX(''='',''' || coalesce(v_data,'') || ''')+1) )
                                                                    AND         SUBSTRING(''' || coalesce(v_data,'') || ''',CHARINDEX(''~'',''' || coalesce(v_data,'') || ''') + 1,(LEN(''' || coalesce(v_data,'') || '''))- (CHARINDEX(''~'',''' || coalesce(v_data,'') || ''')))
    
    ';
                  RAISE NOTICE '%',(v_dynamicFilterQuery);
               ELSEIF POSITION('-1' IN v_data) > 0
               then
                                    
                  v_dynamicFilterQuery  :=   'SELECT      I.numItemCode 
                               	                                   FROM 
                               	                                   dbo.Item    I 
                               	                                   INNER JOIN  dbo.ItemCategory  IC
                                                                   ON          I.numItemCode = IC.numItemID 
                                     
                                                                   WHERE       IC.numCategoryID =' || SUBSTR(CAST(v_numCategoryID AS VARCHAR(20)),1,20) || ' 
                                                                   AND         I.numDomainID    =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(20)),1,20) || '
                              	                                   AND         I.vcManufacturer = SUBSTRING(''' || coalesce(v_data,'') || ''',CHARINDEX(''='',''' || coalesce(v_data,'') || ''')+1,(LEN(''' || coalesce(v_data,'') || '''))-(CHARINDEX(''='',''' || coalesce(v_data,'') || ''')))';
               ELSE
                  v_dynamicFilterQuery := ' SELECT        DISTINCT     
                                                                               I.numItemCode 
                                                                 FROM          dbo.Item I 
                                                                 INNER JOIN    dbo.ItemCategory IC 
                                                                 ON            IC.numItemID = i.numItemCode
                                                                 INNER JOIN    dbo.ItemGroupsDTL IGD 
                                                                 ON            I.numItemGroup = IGD.numItemGroupID
                                                                 INNER JOIN    dbo.CFW_Fld_Master CFM 
                                                                 ON                IGD.numOppAccAttrID = CFM.Fld_id 
                                                                               AND CFM.numDomainID= ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || '
                                                                 LEFT JOIN     dbo.ListDetails LD 
                                                                 ON                LD.numListID = CFM.numlistid 
                                                                               AND CFM.numDomainID = LD.numDomainID
                                                                 INNER JOIN    dbo.WareHouseItems WHI 
                                                                 ON            WHI.numItemID = I.numItemCode 
                                                                 INNER JOIN    dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                                                 ON            CFVSI.RecId = WHI.numWareHouseItemID
                   
                                                                 WHERE 
                                                                     IC.numCategoryID = ' || SUBSTR(CAST(v_numCategoryID AS VARCHAR(10)),1,10) || ' 
                                                                 AND I.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' 
                                                                 AND LD.numListItemID = CFVSI.Fld_Value 
                                                                 AND CFM.Fld_id  =  CFVSI.Fld_ID
                                                                 AND CFM.Fld_id = CONVERT(NUMERIC(9,0),  SUBSTRING(''' || coalesce(v_data,'') || ''',CHARINDEX(''>'',''' || coalesce(v_data,'') || ''')+1,CHARINDEX(''='',''' || coalesce(v_data,'') || ''')-(CHARINDEX(''>'',''' || coalesce(v_data,'') || ''')+1) )) 
                                                                 AND LD.numListItemID = CONVERT( VARCHAR(1000), SUBSTRING(''' || coalesce(v_data,'') || ''',CHARINDEX(''='',''' || coalesce(v_data,'') || ''')+1,LEN(''' || coalesce(v_data,'') || ''')))  
                     ';
               end if;
            end if;
            DELETE from tt_TEMP where row = v_row;
            select   row, data INTO v_row,v_data from tt_TEMP     LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
                           
               v_row := 0;
               IF v_count = 0 then
                                   
                  v_where := coalesce(v_where,'') || coalesce(v_dynamicFilterQuery,'');
                  v_count := v_count+1;
               ELSE
                  v_where :=  coalesce(v_dynamicFilterQuery,'') || '  And I.numItemCode IN( ' || coalesce(v_where,'') || ' ) ';
                  v_count := v_count+1;
               end if;
            ELSE
               IF v_count = 0 then
                                   
                  v_where := coalesce(v_where,'') || coalesce(v_dynamicFilterQuery,'');
                  v_count := v_count+1;
               ELSE
                  v_where :=  coalesce(v_dynamicFilterQuery,'') || '  And I.numItemCode IN( ' || coalesce(v_where,'') || ' ) ';
                  v_count := v_count+1;
               end if;
            end if;
         END LOOP;
         Drop table IF EXISTS tt_TEMP CASCADE;
         v_sqlQuery  := 'SELECT numItemCode into #TempItemCode FROM (' || coalesce(v_where,'') || ') AS a';
      ELSE
         v_sqlQuery := '';
      end if;
      v_filterByNumItemCode := ' AND I.numItemCode IN ( SELECT * FROM #TempItemCode ) ';
      v_sqlQuery := coalesce(v_sqlQuery,'') ||  ' SELECT      DISTINCT  
                                                       CFM.Fld_id AS FilterID,
                                                       REPLACE(REPLACE(CFM.Fld_label,'';'','' '') ,''~'','' '')   AS FilterName,
                                                       LD.vcdata AS FilterValue,
                                                       LD.numListItemID AS FilterValueID,
                                                       ''f~'' +CFM.Fld_label + ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  LD.vcdata+''~''+ CAST(LD.numListItemID AS VARCHAR(10)) + '';'' AS FilterQuery ,
                                                       dbo.fn_GetItemCountBasedOnAttribute(CFM.Fld_id,LD.numListItemID,' || SUBSTR(CAST(v_numCategoryID AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')  ItemCount, LD.numListID
                                           FROM        dbo.Item I 
                                           INNER JOIN  dbo.ItemCategory IC 
                                           ON          IC.numItemID = I.numItemCode
                                           INNER JOIN  dbo.ItemGroupsDTL IGD 
                                           ON          I.numItemGroup = IGD.numItemGroupID
                                           INNER JOIN  dbo.CFW_Fld_Master CFM 
                                           ON              IGD.numOppAccAttrID = CFM.Fld_id 
                                                       AND CFM.numDomainID= :1
                                           LEFT JOIN   dbo.ListDetails LD 
                                           ON              LD.numListID = CFM.numlistid 
                                                       AND CFM.numDomainID = LD.numDomainID
                                           INNER JOIN  dbo.WareHouseItems WHI 
                                           ON          WHI.numItemID = I.numItemCode 
                                           INNER JOIN  dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                           ON          CFVSI.RecId = WHI.numWareHouseItemID
                   
                                           WHERE 
                                           
                                                          IC.numCategoryID = :2 
                                                       AND i.numDomainID = :1
                                                       AND CONVERT(VARCHAR(1000) ,LD.numListItemID)  = CFVSI.Fld_Value 
                                                       AND  CFM.Fld_id  =  CFVSI.Fld_ID 
                                           
                                           GROUP BY    LD.vcdata  ,
                                                       CFM.Fld_id ,
                                                       CFM.Fld_label,
                                                       LD.numListItemID,
													   LD.numListID
                                         ';
      v_manufacturer :=  'UNION
	       
		 SELECT       DISTINCT 
		             -1 AS FilterID,
		             ''Manufacturer'' ,
		              vcManufacturer  FilterValue,
		              0 AS FilterValueID, 
		              ''f~Manufacturer~-1^''+vcManufacturer+''~-1''+'';'' AS FilterQuery,
		              Count(*) ItemCount, -1 AS numListID 
		 FROM 
		              Item I INNER JOIN dbo.ItemCategory IC ON I.numItemCode = IC.numItemID 
		 where 
		              numDomainID=@numDomainId
		              AND IC.numCategoryID = @numCategoryID 
		              AND LEN(ISNULL(vcManufacturer,''''))>1 ';
      v_manufacturerGroupBy :=	'GROUP BY     I.vcManufacturer ';
      v_priceStart := '    UNION 
        
                    SELECT  
                      -2 AS FilterID,
                      ''Price'' AS FilterName,
                      CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))  AS FilterValueId,
                      CEILING(MAX(a.Price))  AS FilterVAlueID , 
                      ''f~Min~''+ CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))+''^Max~''+CAST(MAX(CEILING(a.Price)) AS VARCHAR(10))+'';'' AS FilterQuery ,
                      COUNT(price) AS ItemCount, -1 AS numListID 
                      FROM  (';
      v_priceInnerQuery := 'SELECT              
                                     CEILING(
                                               ISNULL
                                               (
                                                   CASE WHEN I.[charItemType]=''P'' 
                                                   THEN 
                                                       CASE WHEN I.bitSerialized = true 
                                                            THEN 
                                                                (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                            ELSE 
                                                                (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                 WHERE [numItemID]= I.numItemCode 
                                                                 and  [numWareHouseID]=' || SUBSTR(CAST(v_numWareHouseID AS VARCHAR(10)),1,10)
      || ' ) 
                                                         END
								                   ELSE 
								                        (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                   END,0) 
								                  
                                            )  
                              AS Price
                              FROM 
                                     dbo.ItemCategory IC JOIN Item I ON I.numItemCode = IC.numItemID
                              WHERE
                                         IC.numCategoryId = @numCategoryId
                                     AND I.numDomainId = @numDomainId 
                              ';
      v_priceEnd := ' )  AS a
                ';
      IF LENGTH(v_where) > 0 then
           
         v_sqlQuery := coalesce(v_sqlQuery,'') || coalesce(v_priceStart,'') || coalesce(v_priceInnerQuery,'') || coalesce(v_priceEnd,'') ||  coalesce(v_manufacturer,'') || coalesce(v_filterByNumItemCode,'') || coalesce(v_manufacturerGroupBy,'');
      ELSE
         v_sqlQuery := coalesce(v_sqlQuery,'') || coalesce(v_priceStart,'') || coalesce(v_priceInnerQuery,'') || coalesce(v_priceEnd,'') ||  coalesce(v_manufacturer,'') || coalesce(v_manufacturerGroupBy,'');
      end if;
   END;
   RAISE NOTICE '%',v_sqlQuery;
   OPEN SWV_RefCur FOR EXECUTE v_sqlQuery;
   RETURN;
END; $$;

--exec USP_GetProductFilters @numDomainId=1,@numCategoryID=1735,@FilterQuery='f>-2=335~700'
--exec USP_GetProductFilters @numDomainId=154,@numCategoryID=2297,@FilterQuery=NULL



