-- Stored procedure definition script USP_CreateExtranetAccountforContact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CreateExtranetAccountforContact(v_numDomainID NUMERIC(9,0),
	v_vcPassword VARCHAR(100),
	v_numContactID NUMERIC(9,0),
	v_tintAccessAllowed SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);
   v_numExtranetID  NUMERIC(9,0);
   v_numGroupID  NUMERIC;
   v_numCompanyID  NUMERIC(18,0);
BEGIN
   select   numDivisionId INTO v_numDivisionID from AdditionalContactsInformation where numContactId = v_numContactID;

   IF NOT EXISTS(SELECT * FROM ExtarnetAccounts where numDivisionID = v_numDivisionID) then
      select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      select   numGroupID INTO v_numGroupID FROM AuthenticationGroupMaster WHERE numDomainID = v_numDomainID AND tintGroupType = 2    LIMIT 1;
      IF v_numGroupID IS NULL then 
         v_numGroupID := 0;
      end if;
      INSERT INTO ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)
		  values(v_numCompanyID,v_numDivisionID,v_numGroupID,v_numDomainID);
   end if;

   select   numExtranetID INTO v_numExtranetID from ExtarnetAccounts where numDivisionID = v_numDivisionID;
   IF NOT EXISTS(SELECT * FROM ExtranetAccountsDtl WHERE numDomainID = v_numDomainID AND numContactID = v_numContactID::VARCHAR) then
	
      insert into ExtranetAccountsDtl(numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
		values(v_numExtranetID,v_numContactID,v_vcPassword,v_tintAccessAllowed,v_numDomainID);
   end if;
	--PRINT ISNULL(@numExtranetID,0)
   open SWV_RefCur for select cast(coalesce(v_numExtranetID,0) as NUMERIC(9,0));
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/













