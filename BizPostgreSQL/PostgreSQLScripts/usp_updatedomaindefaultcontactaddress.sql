-- Stored procedure definition script USP_UpdatedomainDefaultContactAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatedomainDefaultContactAddress(v_numDomainID NUMERIC(9,0) DEFAULT 0,                                      
v_bitAutoPopulateAddress BOOLEAN DEFAULT null,
v_tintPoulateAddressTo SMALLINT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Domain
   SET
   bitAutoPopulateAddress = v_bitAutoPopulateAddress,tintPoulateAddressTo = v_tintPoulateAddressTo
   WHERE
   numDomainId = v_numDomainID;
   RETURN;
END; $$;


