-- Stored procedure definition script USP_GetSurveyTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSurveyTemplate(v_numDomainID NUMERIC,
	v_numSurId NUMERIC,
	v_numPageType SMALLINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN



   open SWV_RefCur for
   SELECT numDomainID,
	numSurID,
	txtTemplate,
	numPageType
   FROM SurveyTemplate
   WHERE numDomainID = v_numDomainID
   AND numSurID = v_numSurId
   AND numPageType = v_numPageType;


   open SWV_RefCur2 for
   SELECT S.numCssID,S.styleFileName
   FROM   StyleSheetDetails SD
   INNER JOIN StyleSheets S
   ON SD.numCssID = S.numCssID
   WHERE  numSurTemplateId =(SELECT numSurTemplateId FROM SurveyTemplate
      WHERE numDomainID = v_numDomainID AND numSurID = v_numSurId AND numPageType = v_numPageType)
   AND tintType = 3;
         
   open SWV_RefCur3 for
   SELECT S.numCssID,S.styleFileName
   FROM   StyleSheetDetails SD
   INNER JOIN StyleSheets S
   ON SD.numCssID = S.numCssID
   WHERE  numSurTemplateId =(SELECT numSurTemplateId FROM SurveyTemplate
      WHERE numDomainID = v_numDomainID AND numSurID = v_numSurId AND numPageType = v_numPageType)
   AND tintType = 4;
   RETURN;
END; $$;
--Created By Anoop Jayaraj    
    
    


