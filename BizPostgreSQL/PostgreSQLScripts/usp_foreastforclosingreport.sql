-- Stored procedure definition script usp_ForeastForClosingReport for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ForeastForClosingReport(v_sintYear SMALLINT DEFAULT 0,
	v_tintQuarter SMALLINT DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT Distinct 
		Forecast.numForecastID as "numForecastID"
		,Forecast.numCreatedBy as "numCreatedBy"
	FROM 
		Forecast 
	INNER JOIN 
		AdditionalContactsInformation 
	ON 
		Forecast.numCreatedBy = AdditionalContactsInformation.numCreatedBy
	where 
		Forecast.sintYear = v_sintYear 
		and Forecast.tintQuarter = v_tintQuarter
		and AdditionalContactsInformation.numDomainID = v_numDomainID;
END; $$;


