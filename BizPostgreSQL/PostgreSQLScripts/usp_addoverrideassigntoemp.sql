-- Stored procedure definition script USP_AddOverrideAssigntoEmp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddOverrideAssigntoEmp(v_numDomainId NUMERIC DEFAULT 0,
 v_numAssignedContact NUMERIC DEFAULT 0,
 v_numContactId NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numAssignedContact > 0) then
	
      DELETE FROM OverrideAssignTo WHERE numAssignedContact = v_numAssignedContact AND numDomainId = v_numDomainId;
   ELSEIF (v_numContactId > 0)
   then
	
      IF EXISTS(SELECT numContactId FROM OverrideAssignTo WHERE numDomainId = v_numDomainId AND numContactId = v_numContactId) then
		
         v_numAssignedContact := 0;
      ELSE
         INSERT INTO	OverrideAssignTo(numContactId,
			numDomainId)VALUES(v_numContactId,
			v_numDomainId);
      end if;
   end if;
   PERFORM USP_OverrideAssigntoEmpList(v_numDomainId);
   RETURN;
END; $$;


