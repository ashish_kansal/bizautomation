-- Stored procedure definition script USP_GetPayrollHistory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPayrollHistory(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numPayrollHeaderID NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT PH.numPayrollHeaderID,numPayrolllReferenceNo,dtFromDate,dtToDate,dtPaydate,
  PD.numPayrollDetailID,UM.numUserId,coalesce(ADC.vcFirstName,'') || ' ' || coalesce(ADC.vcLastname,'-') AS vcUserName,
   ADC.numContactId AS numUserCntID,coalesce(UM.vcEmployeeId,'') AS vcEmployeeId,
   coalesce(PD.monTotalAmt,0) AS monTotalAmt,coalesce(numCheckStatus,0) AS numCheckStatus,
   CASE coalesce(numCheckStatus,0) WHEN 0 THEN 'Not Approved'
   WHEN 1 THEN 'Approved'
   WHEN 2 THEN 'Paid' END AS vcCheckStatus,
   coalesce(PD.monDeductions,0) AS monDeductions,ADC.numDivisionId,PD.numUserCntID,
   coalesce(CH.numCheckNo,0) AS numCheckNo
   FROM  PayrollHeader PH JOIN PayrollDetail PD ON PH.numPayrollHeaderID = PD.numPayrollHeaderID
   JOIN UserMaster UM ON UM.numUserDetailId = PD.numUserCntID
   JOIN AdditionalContactsInformation ADC ON ADC.numContactId = UM.numUserDetailId
   LEFT JOIN CheckHeader CH ON CH.numReferenceID = PD.numPayrollDetailID AND CH.tintReferenceType = 11
   WHERE UM.numDomainID = v_numDomainId;
END; $$;












