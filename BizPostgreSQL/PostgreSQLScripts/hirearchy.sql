-- Function definition script hirearchy for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION hirearchy(v_id INTEGER)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tree  VARCHAR(1000);
BEGIN
   v_tree := '';
---if (parent_cat_code <> 0 and  parent_cat_code is not  null )
   select   hirearchy(numAccountId::INTEGER)  || vcCatgyName INTO v_tree from Chart_Of_Accounts where numParntAcntTypeID = v_id;
   return v_tree;
END; $$;

