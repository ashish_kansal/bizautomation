-- Stored procedure definition script usp_GetAllTabbedData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAllTabbedData(           
--        
v_numGroupID NUMERIC(9,0) DEFAULT 0,
    v_numRelationShip NUMERIC(9,0) DEFAULT 0,
    v_numProfileID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numGroupID = 0 then
        
      open SWV_RefCur for
      SELECT  numGroupID,
                    numTabId,
                    '' AS numTabName,
                    bitallowed
      FROM    GroupTabDetails
      WHERE   coalesce(tintType,0) <> 1
      ORDER BY numTabId;
   ELSE
      IF v_tintMode = 0 then
                
         open SWV_RefCur for
         SELECT  T.numTabId,
                            CASE WHEN bitFixed = true
         THEN CASE WHEN EXISTS(SELECT numTabName
               FROM   TabDefault
               WHERE  numDomainID = v_numDomainID
               AND numTabId = T.numTabId
               AND tintTabType = T.tintTabType)
            THEN(SELECT 
                  numTabName
                  FROM      TabDefault
                  WHERE     numDomainID = v_numDomainID
                  AND numTabId = T.numTabId
                  AND tintTabType = T.tintTabType LIMIT 1)
            ELSE T.numTabName
            END
         ELSE T.numTabName
         END AS numTabname,
                            vcURL,
                            bitFixed,
                            G.tintType,
                            T.vcImage
         FROM    TabMaster T
         JOIN GroupTabDetails G ON G.numTabId = T.numTabId
         WHERE   (numDomainID = v_numDomainID
         OR bitFixed = true)
         AND numGroupID = v_numGroupID
         AND coalesce(numRelationShip,0) = v_numRelationShip
         AND coalesce(numProfileID,0) = v_numProfileID
         AND coalesce(G.tintType,0) <> 1
         ORDER BY numOrder;
      ELSE
         open SWV_RefCur for
         SELECT  T.numTabId,
                            CASE WHEN bitFixed = true
         THEN CASE WHEN EXISTS(SELECT numTabName
               FROM   TabDefault
               WHERE  numDomainID = v_numDomainID
               AND numTabId = T.numTabId
               AND tintTabType = T.tintTabType)
            THEN(SELECT 
                  numTabName
                  FROM      TabDefault
                  WHERE     numDomainID = v_numDomainID
                  AND numTabId = T.numTabId
                  AND tintTabType = T.tintTabType LIMIT 1)
            ELSE T.numTabName
            END
         ELSE T.numTabName
         END AS numTabname,
                            vcURL,
                            bitFixed,
                            G.tintType,
                            T.vcImage
         FROM    TabMaster T
         JOIN GroupTabDetails G ON G.numTabId = T.numTabId
         WHERE   (numDomainID = v_numDomainID
         OR bitFixed = true)
         AND numGroupID = v_numGroupID
         AND coalesce(numRelationShip,0) = v_numRelationShip
         AND coalesce(numProfileID,0) = v_numProfileID
         AND coalesce(G.tintType,0) <> 1
         UNION ALL
         SELECT -1 AS numTabID,
                           'Administration' AS numTabname,
                           '' AS vcURL,
                           true AS bitFixed,
                           0 AS tintType,
                           '' AS vcImage
         UNION ALL
         SELECT -3 AS numTabID,
                           'Advanced Search' AS numTabname,
                           '' AS vcURL,
                           true AS bitFixed,
                           0 AS tintType,
                           '' AS vcImage;
      end if;
   end if;
   RETURN;
END; $$;



