-- Stored procedure definition script Delete_This_Action_Template_UI_Data for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Delete_This_Action_Template_UI_Data(v_rowID INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Delete From tblActionItemData where RowID = v_rowID;
   RETURN;
END; $$;


