-- Stored procedure definition script USP_AdvancedSearchSurvey for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE  OR REPLACE FUNCTION USP_AdvancedSearchSurvey(v_WhereCondition VARCHAR(1000) DEFAULT '',                   
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                 
v_CurrentPage INTEGER DEFAULT NULL,                                                                              
v_PageSize INTEGER DEFAULT NULL,                                                                              
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                                                       
v_columnSortOrder VARCHAR(10) DEFAULT NULL,          
v_ColumnName VARCHAR(20) DEFAULT '',          
v_SortCharacter CHAR(1) DEFAULT NULL,                                  
v_SortColumnName VARCHAR(20) DEFAULT '',          
v_LookTable VARCHAR(10) DEFAULT '',
v_GetAll BOOLEAN DEFAULT NULL,
v_vcDisplayColumns TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  SMALLINT;                                        
   v_vcFormFieldName  VARCHAR(50);                                        
   v_vcListItemType  VARCHAR(3);                                   
   v_vcListItemType1  VARCHAR(3);                                       
   v_vcAssociatedControlType  VARCHAR(10);                                        
   v_numListID  NUMERIC(9,0);                                        
   v_vcDbColumnName  VARCHAR(30);                                         
   v_ColumnSearch  VARCHAR(10);   
   v_bitContactAddressJoinAdded  BOOLEAN;
   v_bitBillAddressJoinAdded  BOOLEAN;
   v_bitShipAddressJoinAdded  BOOLEAN;

                             
   v_strSql  VARCHAR(8000);                                                                  
   v_vcLocationID  VARCHAR(100) DEFAULT '0';
   v_firstRec  INTEGER;                                                                        
   v_lastRec  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   if (v_SortCharacter <> '0' and v_SortCharacter <> '') then 
      v_ColumnSearch := v_SortCharacter;
   end if;         
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTableAdvancedSearchSurvey_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLEAdvancedSearchSurvey CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLEAdvancedSearchSurvey 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numcontactid NUMERIC(18,0)
   );   
                                                                       
   v_strSql := 'Select ADC.numContactId                        
  FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   ';                              
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then
  
      select   vcListItemType INTO v_vcListItemType from View_DynamicDefaultColumns where vcDbColumnName = v_ColumnName and numFormId = 19 and numDomainID = v_numDomainID;
      if v_vcListItemType = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
      ELSEIF v_vcListItemType = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID =' || coalesce(v_ColumnName,'');
      ELSEIF v_vcListItemType = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
      end if;
   end if;                              
   if (v_SortColumnName <> '') then
  
      select   vcListItemType INTO v_vcListItemType1 from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 19  and numDomainID = v_numDomainID;
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID =' || coalesce(v_SortColumnName,'');
      ELSEIF v_vcListItemType1 = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      end if;
   end if;                              
   v_strSql := coalesce(v_strSql,'') || ' where DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||
   ' and ADC.numContactId in   
(select numRegisteredRespondentContactId from SurveyRespondentsMaster SRM   
join SurveyResponse  SR on SRM.numRespondantID = SR.numRespondantID  
where SRM.numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||  ' and bitRegisteredRespondant = true  ';  
   if v_WhereCondition <> '' then
      v_strSql := coalesce(v_strSql,'') || 'and(  ' || coalesce(v_WhereCondition,'') || ')';
   end if;  
   v_strSql := coalesce(v_strSql,'') || ')';  
                                       
                        
                                
                                                
                             
   if (v_SortColumnName <> '') then
  
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' order by S2.vcState ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' order by T2.vcTerName ' || coalesce(v_columnSortOrder,'');
      else  
         v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
   end if;                            
                                                               
   EXECUTE 'insert into tt_TEMPTABLEAdvancedSearchSurvey(numContactID)                   
' || v_strSql;    
   RAISE NOTICE '%',v_strSql;                                 
   v_strSql := '';                                  
                                  
   v_bitBillAddressJoinAdded := false;
   v_bitShipAddressJoinAdded := false;
   v_bitContactAddressJoinAdded := false;                            
   v_tintOrder := 0;                                  
   v_WhereCondition := '';                               
   v_strSql := 'select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType ';

   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN,
      tintOrder INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = 19;    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempIDs_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPIDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIDS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         vcFieldID VARCHAR(300)
      );
      INSERT INTO tt_TEMPIDS(vcFieldID)
      SELECT
      OutParam
      FROM
      SplitString(v_vcDisplayColumns,',');
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,(SELECT(ID::bigint -1) FROM tt_TEMPIDS T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
      FROM(SELECT
         numFieldID as numFormFieldID,
				false AS bitCustomField,
				CONCAT(numFieldID,'~0') AS vcFieldID
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 19
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,true AS bitCustomField
				,CONCAT(Fld_id,'~1') AS vcFieldID
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT vcFieldID FROM tt_TEMPIDS);
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
      FROM(SELECT
         A.numFormFieldID,
				false AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = 19
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = 19
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT
         A.numFormFieldID,
				true AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = 19
         AND C.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFieldID,
			CAST(0 AS BOOLEAN),
			1,
			0
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 19
      AND bitInResults = true
      AND bitDeleted = false
      AND numDomainID = v_numDomainID
      AND numFieldID = 3;
   end if;
                                
   select   T1.tintOrder::bigint+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   v_vcListItemType,v_numListID FROM
   View_DynamicDefaultColumns D
   INNER JOIN
   tt_TEMPSELECTEDCOLUMNS T1
   ON
   D.numFieldID = T1.numFieldID WHERE
   D.numFormId = 19   ORDER BY
   T1.tintOrder ASC LIMIT 1;           
                       
   WHILE v_tintOrder > 0 LOOP
      IF v_vcAssociatedControlType = 'SelectBox' then
		
         if v_vcListItemType = 'LI' then
			  
            IF v_numListID = 40 then--Country
				  
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               IF v_vcDbColumnName = 'numCountry' then
					
                  IF v_bitContactAddressJoinAdded = false then
					  
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
                     v_bitContactAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD.numCountry';
               ELSEIF v_vcDbColumnName = 'numBillCountry'
               then
					
                  IF v_bitBillAddressJoinAdded = false then
						
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                     v_bitBillAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry';
               ELSEIF v_vcDbColumnName = 'numShipCountry'
               then
					
                  IF v_bitShipAddressJoinAdded = false then
						
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                     v_bitShipAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry';
               end if;
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            end if;
         ELSEIF v_vcListItemType = 'S'
         then
			  
            v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            IF v_vcDbColumnName = 'numState' then
				
               IF v_bitContactAddressJoinAdded = false then
					   
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
                  v_bitContactAddressJoinAdded := true;
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD.numState';
            ELSEIF v_vcDbColumnName = 'numBillState'
            then
				
               IF v_bitBillAddressJoinAdded = false then
					
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                  v_bitBillAddressJoinAdded := true;
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD1.numState';
            ELSEIF v_vcDbColumnName = 'numShipState'
            then
				
               IF v_bitShipAddressJoinAdded = false then
					
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                  v_bitShipAddressJoinAdded := true;
               end if;
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD2.numState';
            end if;
         ELSEIF v_vcListItemType = 'T'
         then
			  
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
         end if;
      ELSEIF v_vcAssociatedControlType = 'TextBox'
      then
		
         IF v_vcDbColumnName = 'vcStreet' OR v_vcDbColumnName = 'vcCity' OR v_vcDbColumnName = 'vcPostalCode' then
			
            RAISE NOTICE 'hi';
            v_strSql := coalesce(v_strSql,'') || ',AD.' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            IF v_bitContactAddressJoinAdded = false then
			   
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=true and AD.numDomainID= ADC.numDomainID';
               v_bitContactAddressJoinAdded := true;
            end if;
            RAISE NOTICE '%',v_WhereCondition;
         ELSEIF v_vcDbColumnName = 'vcBillStreet' OR v_vcDbColumnName = 'vcBillCity' OR v_vcDbColumnName = 'vcBillPostCode'
         then
			
            v_strSql := coalesce(v_strSql,'') || ',AD1.' || REPLACE(REPLACE(v_vcDbColumnName,'Bill',''),'PostCode','PostalCode') || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            IF v_bitBillAddressJoinAdded = false then
			   
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
               v_bitBillAddressJoinAdded := true;
            end if;
         ELSEIF v_vcDbColumnName = 'vcShipStreet' OR v_vcDbColumnName = 'vcShipCity' OR v_vcDbColumnName = 'vcShipPostCode'
         then
			
            v_strSql := coalesce(v_strSql,'') || ',AD2.' || REPLACE(REPLACE(v_vcDbColumnName,'Ship',''),'PostCode','PostalCode') || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
            IF v_bitShipAddressJoinAdded = false then
			   
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
               v_bitShipAddressJoinAdded := true;
            end if;
         ELSE
            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
         end if;
      ELSE
         v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)' when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' "' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || '"';
      end if;
      select   T1.tintOrder::bigint+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID WHERE
      D.numFormId = 19
      AND T1.tintOrder > v_tintOrder -1   ORDER BY
      T1.tintOrder ASC LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_tintOrder := 0;
      end if;
   END LOOP;                                  
                                  
                                  
                                  
                                  
                             
                                                                        
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                          
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                         
   select count(*) INTO v_TotRecs from tt_TEMPTABLEAdvancedSearchSurvey;                                                       
                                              
   if v_GetAll = false then

      v_strSql := coalesce(v_strSql,'') || 'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   ' || coalesce(v_WhereCondition,'') || ' join tt_TEMPTABLEAdvancedSearchSurvey T on T.numContactId=ADC.numContactId                                                                   
  WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
   else
      v_strSql := coalesce(v_strSql,'') || 'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   ' || coalesce(v_WhereCondition,'') || ' join tt_TEMPTABLEAdvancedSearchSurvey T on T.numContactId=ADC.numContactId';
   end if;
   RAISE NOTICE '%',v_strSql;                                                          
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                                                                     
 
   RETURN;
END; $$;


