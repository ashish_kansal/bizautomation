-- FUNCTION: public.usp_getalertpaneldetail(integer, numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getalertpaneldetail(integer, numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getalertpaneldetail(
	v_numalerttype integer,
	v_numdivisionid numeric,
	v_numcontactid numeric,
	v_numdomainid numeric,
	INOUT swv_refcur refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_AuthoritativeSalesBizDocId  INTEGER;
BEGIN
   IF v_numAlertType = 1 then
      select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM AuthoritativeBizDocs WHERE  numDomainId =  v_numDomainID;
      select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM AuthoritativeBizDocs WHERE  numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT
			
      vcpOppName AS "Order",
			vcBizDocID AS "Authoritive BizDoc",
			TotalAmount AS "Total Amount",
			AmountPaid AS "Amount Paid",
			BalanceDue AS "Balance Due",
			FormatedDateFromDate(DueDate::VARCHAR(50),1::NUMERIC) AS "Due Date"
      FROM(SELECT DISTINCT
         OM.numOppId,
			OM.vcpOppName,
			OB.numOppBizDocsId,
			OB.vcBizDocID,
			coalesce(OB.monDealAmount,0) AS TotalAmount,--OM.[monPAmount]
			(OB.monAmountPaid) AS AmountPaid,
			coalesce(OB.monDealAmount -OB.monAmountPaid,0) AS BalanceDue,
			CASE coalesce(bitBillingTerms,false)
         WHEN true THEN OB.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(intBillingDays,0)),0) AS INTEGER) || 'day' as interval)
         WHEN false THEN OB.dtFromDate
         END AS DueDate
         FROM
         OpportunityMaster OM
         INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numoppid = OM.numOppId
         LEFT OUTER JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
         WHERE
         OM.tintopptype = 1
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OB.numBizDocId = coalesce(v_AuthoritativeSalesBizDocId,0)
         AND OB.bitAuthoritativeBizDocs = 1
         AND coalesce(OB.tintDeferred,0) <> 1
         AND DM.numDivisionID = v_numDivisionID
         AND coalesce(OB.monDealAmount,0) > 0  AND coalesce(OB.monDealAmount*OM.fltExchangeRate -OB.monAmountPaid*OM.fltExchangeRate,0) != 0
         UNION
         SELECT
         CAST(-1 AS NUMERIC(18,0)) AS numOppId,
				CAST('Journal' AS VARCHAR(100)) AS vcPOppName,
				CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
				CAST('' AS VARCHAR(100)) AS vcBizDocID,
				CAST(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END AS DECIMAL(20,5)) AS TotalAmount,
				CAST(0 AS DECIMAL(20,5)) AS AmountPaid,
				CAST(0 AS DECIMAL(20,5)) AS BalanceDue,
				GJH.datEntry_Date AS DueDate
         FROM
         General_Journal_Header GJH
         INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId														  AND GJH.numDomainId = GJD.numDomainId
         INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
         LEFT OUTER JOIN Currency C ON GJD.numCurrencyID = C.numCurrencyID
         WHERE
         GJH.numDomainId = v_numDomainID
         AND GJD.numCustomerId = v_numDivisionID
         AND COA.vcAccountCode ilike '01010105%'
         AND coalesce(numOppId,0) = 0 AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(GJH.numDepositId,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
         UNION
         SELECT
         CAST(-1 AS NUMERIC(18,0)) AS numOppId,
				CASE WHEN tintDepositePage = 2 THEN 'Unapplied Payment' ELSE 'Credit Memo' END AS vcPOppName,
				CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
				CAST('' AS VARCHAR(100)) AS vcBizDocID,
				CAST((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))*-1 AS DECIMAL(20,5)) AS TotalAmount,
				CAST(0 AS DECIMAL(20,5)) AS AmountPaid,
				CAST((coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0))*-1 AS DECIMAL(20,5)) AS BalanceDue,
				DM.dtDepositDate AS DueDate
         FROM
         DepositMaster DM
         WHERE
         DM.numDomainId = v_numDomainID
         AND tintDepositePage IN(2,3)
         AND DM.numDivisionID = v_numDivisionID
         AND(coalesce(monDepositAmount,0) -coalesce(monAppliedAmount,0)) > 0
         UNION
         SELECT
         CAST(Case When coalesce(BH.bitLandedCost,false) = true then  coalesce(BH.numOppId,0) else 0 end AS NUMERIC(18,0)) AS numOppId,
			CAST('Bill' AS VARCHAR(100)) AS vcPOppName,
			CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
			CASE WHEN LENGTH(BH.vcReference) = 0 THEN '' ELSE BH.vcReference END AS vcBizDocID,
			BH.monAmountDue as TotalAmount,
			coalesce(BH.monAmtPaid,0) as AmountPaid,
			coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) as BalanceDue,
			BH.dtDueDate AS DueDate
         FROM
         BillHeader BH
         WHERE
         BH.numDomainId = v_numDomainID
         AND BH.numDivisionId = v_numDivisionID
         AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0
         UNION
         SELECT
         CAST(0 AS NUMERIC(18,0)) AS numOppId,
			CAST('Check' AS VARCHAR(100)) AS vcPOppName,
			CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
			CAST('' AS VARCHAR(100)) AS vcBizDocID,
			coalesce(CD.monAmount,0) as TotalAmount,
			coalesce(CD.monAmount,0) as AmountPaid,
			CAST(0 AS DECIMAL(20,5)) as BalanceDue,
			CH.dtCheckDate AS DueDate
         FROM
         CheckHeader CH
         JOIN  CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
         INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
         WHERE
         CH.numDomainID = v_numDomainID
         AND CH.tintReferenceType = 1
         AND COA.vcAccountCode ilike '01020102%'
         AND CD.numCustomerId = v_numDivisionID
         UNION
         SELECT
         OM.numOppId,
			OM.vcpOppName,
			OB.numOppBizDocsId,
			OB.vcBizDocID,
			CAST(coalesce(OB.monDealAmount*OM.fltExchangeRate,0) AS DECIMAL(20,5)) AS TotalAmount,--OM.[monPAmount]
			CAST((OB.monAmountPaid*OM.fltExchangeRate) AS DECIMAL(20,5)) AS AmountPaid,
			CAST(coalesce(OB.monDealAmount*OM.fltExchangeRate -OB.monAmountPaid*OM.fltExchangeRate,0) AS DECIMAL(20,5))  AS BalanceDue,
			CASE coalesce(OM.bitBillingTerms,false)
         WHEN true THEN OB.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) AS INTEGER) || 'day' as interval)
         WHEN false THEN OB.dtFromDate
         END AS DueDate
         FROM
         OpportunityMaster OM
         INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
         LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numoppid = OM.numOppId
         LEFT OUTER JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
         WHERE
         OM.tintopptype = 2
         AND OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OB.numBizDocId = coalesce(v_AuthoritativePurchaseBizDocId,0)
         AND DM.numDivisionID = v_numDivisionID
         AND OB.bitAuthoritativeBizDocs = 1
         AND coalesce(OB.tintDeferred,0) <> 1
         AND  coalesce(OB.monDealAmount*OM.fltExchangeRate,0) > 0
         AND coalesce(OB.monDealAmount*OM.fltExchangeRate -OB.monAmountPaid*OM.fltExchangeRate,0) > 0
         UNION
         SELECT
         OM.numOppId,
			OM.vcpOppName,
			OB.numOppBizDocsId,
			OB.vcBizDocID,
			BDC.numComissionAmount,
			CAST(0 AS DECIMAL(20,5)) AS AmountPaid,
			BDC.numComissionAmount AS BalanceDue,
			CASE coalesce(OM.bitBillingTerms,false)
         WHEN true THEN OB.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) AS INTEGER) || 'day' as interval)
         WHEN false THEN OB.dtFromDate
         END AS DueDate
         FROM
         BizDocComission BDC
         JOIN OpportunityBizDocs OB ON BDC.numOppBizDocId = OB.numOppBizDocsId
         join OpportunityMaster OM on OM.numOppId = OB.numoppid
         JOIN Domain D on D.numDomainId = BDC.numDomainId
         LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId = coalesce(OBDD.numBizDocsPaymentDetId,0)
         WHERE
         OM.numDomainId = v_numDomainID
         AND BDC.numDomainId = v_numDomainID
         AND OB.bitAuthoritativeBizDocs = 1
         AND coalesce(OBDD.bitIntegratedToAcnt,false) = false
         AND D.numDivisionId = v_numDivisionID
         UNION
         SELECT
         GJH.numJOurnal_Id AS numOppId,
			CAST('Journal' AS VARCHAR(100)) AS vcPOppName,
			CAST(0 AS NUMERIC(18,0)) AS numOppBizDocsId,
			CAST('' AS VARCHAR(100)) AS vcBizDocID,
			CAST(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END AS DECIMAL(20,5)) AS TotalAmount,
			CAST(0 AS DECIMAL(20,5)) AS AmountPaid,
			CAST(1 AS DECIMAL(20,5)) AS BalanceDue,
			GJH.datEntry_Date AS DueDate
         FROM
         General_Journal_Header GJH
         INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId
         INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
         WHERE
         GJH.numDomainId = v_numDomainID
         AND GJD.numCustomerId = v_numDivisionID
         AND COA.vcAccountCode ilike '01020102%'
         AND coalesce(numOppId,0) = 0 AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numCheckHeaderID,0) = 0 AND coalesce(GJH.numReturnID,0) = 0
         AND GJH.numJOurnal_Id NOT IN(
			--Exclude Bill and Bill payment entries
			SELECT GH.numJOurnal_Id FROM General_Journal_Header GH INNER JOIN OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
            WHERE GH.numDomainId = v_numDivisionID and OBD.tintPaymentType IN(2,4,6) and GH.numBizDocsPaymentDetId > 0) AND coalesce(GJH.numBillID,0) = 0 AND coalesce(GJH.numBillPaymentID,0) = 0 AND coalesce(GJH.numCheckHeaderID,0) = 0) TEMP
      ORDER BY
      TEMP.DueDate DESC LIMIT 10;
	-- OPEN ORDERS
   ELSEIF v_numAlertType = 2
   then
	
      open SWV_RefCur for
      SELECT
			
      OM.vcpOppName AS "Order",
			coalesce((SELECT vcData FROM Listdetails WHERE numListItemID = coalesce(OM.numStatus,0)),'') AS "Order Status",
			coalesce(OM.intPEstimatedCloseDate,'1900-01-01 00:00:00.000') AS "Due Date",
			CAST(coalesce(numPercentageComplete,0) AS VARCHAR(3)) || ' %' As "Total Progress"
      FROM
      OpportunityMaster OM
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numDivisionId = v_numDivisionID
      AND (OM.tintopptype = 1 OR OM.tintopptype = 2)
      AND OM.tintoppstatus = 1
      AND coalesce(tintshipped,0) = 0
      ORDER BY
      OM.intPEstimatedCloseDate DESC LIMIT 20;
   ELSEIF v_numAlertType = 3
   then
	
      open SWV_RefCur for
      SELECT
			
      vcCaseNumber AS "Case #",
			textSubject AS Subject,
			textDesc AS Description,
			coalesce((SELECT vcData FROM Listdetails WHERE numListItemID = coalesce(Cases.numStatus,0)),'') AS Status
      FROM
      Cases
      WHERE
      Cases.numStatus <> 136
      AND numDomainID = v_numDomainID
      AND numDivisionID = v_numDivisionID
      ORDER BY
      Cases.bintCreatedDate DESC LIMIT 20;
   ELSEIF v_numAlertType = 4
   then
	
      open SWV_RefCur for
      SELECT
			
      vcProjectID AS Project,
			coalesce((SELECT vcData FROM Listdetails WHERE numListItemID = coalesce(ProjectsMaster.numProjectStatus,0)),'') AS Status,
			CAST(coalesce((SELECT  intTotalProgress FROM ProjectProgress WHERE numProId = numProId LIMIT 1),0) AS VARCHAR(3)) || ' %' As "Total Progress",
			coalesce(intDueDate,NULL) AS "Due Date",
			coalesce(txtComments,'') AS Comments
      FROM
      ProjectsMaster
      WHERE
      numdomainId = v_numDomainID
      AND numDivisionId = v_numDivisionID
      ORDER BY
      bintCreatedDate DESC LIMIT 20;
   ELSEIF v_numAlertType = 5
   then
	
      open SWV_RefCur for
      SELECT
			
      coalesce((SELECT vcData FROM Listdetails WHERE numListItemID = coalesce(bitTask,0)),'') AS Type,
			CONCAT(CAST(dtStartTime AS DATE),' ',TO_CHAR(dtStartTime,'hh:mm tt'),'-',TO_CHAR(dtEndTime,'hh:mm tt')) AS "Date, From, To",
			coalesce(textDetails,'') AS Comments
      FROM
      Communication
      WHERE
      bitClosedFlag = false
      AND numDomainID = v_numDomainID
      AND numDivisionID = v_numDivisionID
      AND numContactId = v_numContactID
      ORDER BY
      Communication.dtCreatedDate DESC LIMIT 20;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getalertpaneldetail(integer, numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
