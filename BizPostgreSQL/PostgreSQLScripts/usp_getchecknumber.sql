-- Stored procedure definition script USP_GetCheckNumber for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCheckNumber(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select coalesce(numCheckNo,0)+1 as numCheckNo,cast(numBankRoutingNo as VARCHAR(255)) as numBankRoutingNo,cast(numBankAccountNo as VARCHAR(255)) as numBankAccountNo from CheckDetails Where numDomainId = v_numDomainId And numCheckId =(Select cast(max(coalesce(numCheckId,0)) as TEXT) from  CheckDetails Where numDomainId = v_numDomainId);
END; $$;












