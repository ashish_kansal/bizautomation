DROP FUNCTION IF EXISTS usp_vendorcosttable_getbyitemvendor;

CREATE OR REPLACE FUNCTION usp_vendorcosttable_getbyitemvendor
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numvendortcode NUMERIC(18,0)
	,p_numvendorid NUMERIC(18,0)
	,p_numitemcode NUMERIC(18,0)
	,p_numcurrencyid NUMERIC(18,0)
	,p_clienttimezoneoffset INTEGER
	,INOUT p_swvrefcur REFCURSOR
)
LANGUAGE plpgsql
   AS $$
	DECLARE
		v_vcDefaultCostRange JSONB;
BEGIN
	SELECT vcDefaultCostRange INTO v_vcDefaultCostRange FROM Domain WHERE numDomainID=p_numdomainid;

	IF p_numvendortcode = -1 THEN
		SELECT numVendorTCode INTO p_numvendortcode FROM Vendor V WHERE V.numDomainID = p_numdomainid AND V.numVendorID = p_numvendorid AND V.numItemCode = p_numitemcode;
	END IF;

	IF EXISTS (SELECT 
					* 
				FROM 
					Vendor V
				INNER JOIN
					VendorCostTable VCT
				ON
					V.numVendorTcode = VCT.numVendorTcode
				WHERE
					V.numvendortcode = p_numvendortcode
					AND V.numDomainID = p_numdomainid
					AND V.numVendorID = p_numvendorid
					AND V.numItemCode = p_numitemcode
					AND VCT.numCurrencyID = p_numcurrencyid) THEN
		OPEN p_swvrefcur FOR
		SELECT 
			VCT.numvendorcosttableid,
			VCT.intRow,
			VCT.intFromQty,
			VCT.intToQty,
			VCT.monStaticCost,
			VCT.monDynamicCost,
			(CASE 
				WHEN VCT.numStaticCostModifiedBy IS NOT NULL AND VCT.dtStaticCostModifiedDate IS NOT NULL
				THEN CONCAT(CASE 
								WHEN CAST(VCT.dtStaticCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
								THEN '<b><font color=red>Today</font></b>' 
								WHEN CAST(VCT.dtStaticCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
								THEN '<b><font color=purple>YesterDay</font></b>' 
								WHEN CAST(VCT.dtStaticCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
								THEN '<b><font color=orange>Tommorow</font></b>' 
								ELSE FormatedDateFromDate(VCT.dtStaticCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval),p_numdomainid) 
							END, ' by ',fn_getcontactname(numStaticCostModifiedBy))
				ELSE '' 
			END) vcStaticCostModifiedBy,
			(CASE 
				WHEN VCT.numDynamicCostModifiedBy IS NOT NULL AND VCT.dtDynamicCostModifiedDate IS NOT NULL
				THEN CONCAT(CASE 
								WHEN CAST(VCT.dtDynamicCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP AS DATE) 
								THEN '<b><font color=red>Today</font></b>' 
								WHEN CAST(VCT.dtDynamicCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS DATE) 
								THEN '<b><font color=purple>YesterDay</font></b>' 
								WHEN CAST(VCT.dtDynamicCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval) AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) 
								THEN '<b><font color=orange>Tommorow</font></b>' 
								ELSE FormatedDateFromDate(VCT.dtDynamicCostModifiedDate+CAST(-p_clienttimezoneoffset || 'minute' as interval),p_numdomainid) 
							END, ' by ',(CASE WHEN VCT.bitIsDynamicCostByOrder = true THEN COALESCE((SELECT vcPoppName FROM OpportunityMaster WHERE numOppID=VCT.numDynamicCostModifiedBy),'-') ELSE fn_getcontactname(numDynamicCostModifiedBy) END))
				ELSE '' 
			END) vcDynamicCostModifiedBy
		FROM
			Vendor V
		INNER JOIN
			VendorCostTable VCT
		ON
			V.numVendorTcode = VCT.numVendorTcode
		WHERE
			V.numvendortcode = p_numvendortcode
			AND V.numDomainID = p_numdomainid
			AND V.numVendorID = p_numvendorid
			AND V.numItemCode = p_numitemcode
			AND VCT.numCurrencyID = p_numcurrencyid
		ORDER BY
			VCT.intRow;
	ELSEIF v_vcDefaultCostRange IS NOT NULL THEN
		OPEN p_swvrefcur FOR
		SELECT 
			0 AS numvendorcosttableid,
			"intRow" AS intRow,
			"intFromQty" AS intFromQty,
			"intToQty" AS intToQty,
			'' AS monStaticCost,
			'' AS monDynamicCost,
			'' AS vcStaticCostModifiedBy,
			'' AS  vcDynamicCostModifiedBy
		FROM 
			jsonb_to_recordset(v_vcDefaultCostRange) 
		AS X
		(
			"intRow" INT,
  			"intFromQty" INT,
			"intToQty" INT
		);
	END IF;
END; $$;