DROP FUNCTION IF EXISTS USP_MassSalesFulfillment_GetOrderItems;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetOrderItems(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_vcSelectedRecords TEXT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPackingMode  SMALLINT;
   v_tintInvoicingType  SMALLINT;
   v_numDefaultShippingBizDoc  NUMERIC(18,0);
BEGIN
   select   coalesce(numDefaultSalesShippingDoc,0) INTO v_numDefaultShippingBizDoc FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   select   tintPackingMode, tintInvoicingType INTO v_tintPackingMode,v_tintInvoicingType FROM
   MassSalesFulfillmentConfiguration WHERE
   MassSalesFulfillmentConfiguration.numDomainID = v_numDomainID;

   IF coalesce(v_vcSelectedRecords,'') <> '' then
      DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMS
      (
         numOppItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION
      );
      INSERT INTO tt_TEMPITEMS(numOppItemID,
			numUnitHour)
      SELECT
      CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS DOUBLE PRECISION)
      FROM
      SplitString(v_vcSelectedRecords,',');
      IF v_tintMode = 1 then -- For ship orders
		
         open SWV_RefCur for
         SELECT
			numoppitemtCode
			,coalesce(T1.numUnitHour,0) AS numUnitHour
			,monPrice
         FROM
         OpportunityItems
         INNER JOIN
         tt_TEMPITEMS T1
         ON
         OpportunityItems.numoppitemtCode = T1.numOppItemID
         WHERE
         numOppId = v_numOppID
         AND coalesce(bitMappingRequired,false) = false
         AND (coalesce(T1.numUnitHour,0) <= (CASE WHEN COALESCE((SELECT 
																	SUM(OBIInner.numUnitHour)
																FROM
																	OpportunityBizDocs OBInner
																INNER JOIN
																	OpportunityBizDocItems OBIInner
																ON
																	OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																WHERE
																	OBInner.numOppId = OpportunityItems.numOppID
																	AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > COALESCE((SELECT 
																																				SUM(OBIInner.numUnitHour)
																																			FROM
																																				OpportunityBizDocs OBInner
																																			INNER JOIN
																																				OpportunityBizDocItems OBIInner
																																			ON
																																				OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																			WHERE
																																				OBInner.numOppId = OpportunityItems.numOppID
																																				AND OBInner.numBizDocId=296
					 																															AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) THEN COALESCE((SELECT 
																																																									SUM(OBIInner.numUnitHour)
																																																								FROM
																																																									OpportunityBizDocs OBInner
																																																								INNER JOIN
																																																									OpportunityBizDocItems OBIInner
																																																								ON
																																																									OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																								WHERE
																																																									OBInner.numOppId = OpportunityItems.numOppID
																																																									AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
					 																																																				AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) - COALESCE((SELECT 
																																																																													SUM(OBIInner.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs OBInner
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems OBIInner
																																																																												ON
																																																																													OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																																												WHERE
																																																																													OBInner.numOppId = OpportunityItems.numOppID
																																																																													AND OBInner.numBizDocId=296
					 																																																																								AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) ELSE 0 END));
      ELSEIF v_tintMode = 2
      then -- For invoicing orders
		
         open SWV_RefCur for
         SELECT
         numoppitemtCode
				,coalesce(T1.numUnitHour,0) AS numUnitHour
				,monPrice
         FROM
         OpportunityItems
         INNER JOIN
         tt_TEMPITEMS T1
         ON
         OpportunityItems.numoppitemtCode = T1.numOppItemID
         WHERE
         numOppId = v_numOppID
         AND coalesce(bitMappingRequired,false) = false
         AND(CASE
         WHEN coalesce(v_tintInvoicingType,0) = 1
         THEN coalesce(OpportunityItems.numQtyShipped,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 287
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) > 0;
      ELSEIF v_tintMode = 3
      then -- For deffault shipping bizdoc
		
         IF coalesce(v_numDefaultShippingBizDoc,0) > 0 then
			
            open SWV_RefCur for
            SELECT
				numoppitemtCode
				,coalesce(T1.numUnitHour,0) AS numUnitHour
				,monPrice
            FROM
            OpportunityItems
            INNER JOIN
            tt_TEMPITEMS T1
            ON
            OpportunityItems.numoppitemtCode = T1.numOppItemID
            WHERE
            numOppId = v_numOppID
            AND coalesce(bitMappingRequired,false) = false
            AND (coalesce(T1.numUnitHour,0) <= (coalesce(OpportunityItems.numQtyPicked,0)-coalesce((SELECT
																										SUM(OpportunityBizDocItems.numUnitHour)
																									FROM
																										OpportunityBizDocs
																									INNER JOIN
																										OpportunityBizDocItems
																									ON
																										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																									WHERE
																										OpportunityBizDocs.numoppid = v_numOppID
																										AND OpportunityBizDocs.numBizDocId = v_numDefaultShippingBizDoc
																										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0)));
         ELSE
            RAISE NOTICE 'DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED';
         end if;
      end if;
   ELSE
      IF v_tintMode = 1 then -- For ship orders
		
         open SWV_RefCur for
         SELECT
			numoppitemtCode
			,(CASE WHEN COALESCE((SELECT 
										SUM(OBIInner.numUnitHour)
									FROM
										OpportunityBizDocs OBInner
									INNER JOIN
										OpportunityBizDocItems OBIInner
									ON
										OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
									WHERE
										OBInner.numOppId = OpportunityItems.numOppID
										AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
										AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > COALESCE((SELECT 
																													SUM(OBIInner.numUnitHour)
																												FROM
																													OpportunityBizDocs OBInner
																												INNER JOIN
																													OpportunityBizDocItems OBIInner
																												ON
																													OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																												WHERE
																													OBInner.numOppId = OpportunityItems.numOppID
																													AND OBInner.numBizDocId=296
					 																								AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) THEN COALESCE((SELECT 
																																																		SUM(OBIInner.numUnitHour)
																																																	FROM
																																																		OpportunityBizDocs OBInner
																																																	INNER JOIN
																																																		OpportunityBizDocItems OBIInner
																																																	ON
																																																		OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																	WHERE
																																																		OBInner.numOppId = OpportunityItems.numOppID
																																																		AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
					 																																													AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) - COALESCE((SELECT 
																																																																						SUM(OBIInner.numUnitHour)
																																																																					FROM
																																																																						OpportunityBizDocs OBInner
																																																																					INNER JOIN
																																																																						OpportunityBizDocItems OBIInner
																																																																					ON
																																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																																					WHERE
																																																																						OBInner.numOppId = OpportunityItems.numOppID
																																																																						AND OBInner.numBizDocId=296
					 																																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) ELSE 0 END) AS numUnitHour
			,monPrice
         FROM
			OpportunityItems
         WHERE
			numOppId = v_numOppID
			AND coalesce(bitMappingRequired,false) = false
			AND (CASE WHEN COALESCE((SELECT 
										SUM(OBIInner.numUnitHour)
									FROM
										OpportunityBizDocs OBInner
									INNER JOIN
										OpportunityBizDocItems OBIInner
									ON
										OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
									WHERE
										OBInner.numOppId = OpportunityItems.numOppID
										AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
										AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > COALESCE((SELECT 
																													SUM(OBIInner.numUnitHour)
																												FROM
																													OpportunityBizDocs OBInner
																												INNER JOIN
																													OpportunityBizDocItems OBIInner
																												ON
																													OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																												WHERE
																													OBInner.numOppId = OpportunityItems.numOppID
																													AND OBInner.numBizDocId=296
					 																								AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) THEN COALESCE((SELECT 
																																																		SUM(OBIInner.numUnitHour)
																																																	FROM
																																																		OpportunityBizDocs OBInner
																																																	INNER JOIN
																																																		OpportunityBizDocItems OBIInner
																																																	ON
																																																		OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																	WHERE
																																																		OBInner.numOppId = OpportunityItems.numOppID
																																																		AND OBInner.numBizDocId=COALESCE(v_numDefaultShippingBizDoc,0)
					 																																													AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) - COALESCE((SELECT 
																																																																						SUM(OBIInner.numUnitHour)
																																																																					FROM
																																																																						OpportunityBizDocs OBInner
																																																																					INNER JOIN
																																																																						OpportunityBizDocItems OBIInner
																																																																					ON
																																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																																					WHERE
																																																																						OBInner.numOppId = OpportunityItems.numOppID
																																																																						AND OBInner.numBizDocId=296
					 																																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) ELSE 0 END) > 0;
      ELSEIF v_tintMode = 2
      then -- For invoicing orders
		
         open SWV_RefCur for
         SELECT
         numoppitemtCode
				,(CASE
         WHEN coalesce(v_tintInvoicingType,0) = 1
         THEN coalesce(OpportunityItems.numQtyShipped,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 287
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) AS numUnitHour
				,monPrice
         FROM
         OpportunityItems
         WHERE
         numOppId = v_numOppID
         AND coalesce(bitMappingRequired,false) = false
         AND(CASE
         WHEN coalesce(v_tintInvoicingType,0) = 1
         THEN coalesce(OpportunityItems.numQtyShipped,0)
         ELSE coalesce(OpportunityItems.numUnitHour,0)
         END) -coalesce((SELECT
            SUM(OpportunityBizDocItems.numUnitHour)
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocs.numBizDocId = 287
            AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0) > 0;
      ELSEIF v_tintMode = 3
      then -- For deffault shipping bizdoc
		
         IF coalesce(v_numDefaultShippingBizDoc,0) > 0 then
			
            open SWV_RefCur for
            SELECT
				numoppitemtCode
				,(coalesce(OpportunityItems.numQtyPicked,0)-coalesce((SELECT
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numoppid = v_numOppID
																			AND OpportunityBizDocs.numBizDocId = v_numDefaultShippingBizDoc
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0)) AS numUnitHour
				,monPrice
            FROM
            OpportunityItems
            WHERE
            numOppId = v_numOppID
            AND coalesce(bitMappingRequired,false) = false
            AND (coalesce(OpportunityItems.numQtyPicked,0)-coalesce((SELECT
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numoppid = v_numOppID
																			AND OpportunityBizDocs.numBizDocId = v_numDefaultShippingBizDoc
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode),0)) > 0;
         ELSE
            RAISE NOTICE 'DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED';
         end if;
      end if;
   end if;
   RETURN;
END; $$;


