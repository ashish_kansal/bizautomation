-- Stored procedure definition script USP_GetCartParentCategory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCartParentCategory(v_numSiteID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bParentCatrgory  BOOLEAN;
BEGIN
   select   vcAttributeValue INTO v_bParentCatrgory FROM PageElementDetail WHERE numSiteID = v_numSiteID AND numAttributeID = 39;
   open SWV_RefCur for SElect cast(coalesce(v_bParentCatrgory,false) as BOOLEAN);
END; $$;
--exec USP_GetCashFlow @numDomainId=72,@dtFromDate='2009-01-01 00:00:00:000',@dtToDate='2009-10-16 00:00:00:000'












