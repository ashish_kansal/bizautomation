-- Stored procedure definition script USP_ManageShipFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShipFields(v_numDomainID NUMERIC(9,0),
v_numLIstItemID NUMERIC(9,0),
v_str TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   delete from ShippingFieldValues where numDomainID = v_numDomainID and numListItemID = v_numLIstItemID;
                         
   insert into
   ShippingFieldValues(intShipFieldID,vcShipFieldValue,numDomainID,numListItemID)
   select X.intShipFieldID,X.vcShipFieldValue,v_numDomainID,v_numLIstItemID 
   from
    XMLTABLE
	(
		'NewDataSet/ShipDTL'
		PASSING 
			CAST(v_str AS XML)
		COLUMNS
			id FOR ORDINALITY,
			intShipFieldID INTEGER PATH 'intShipFieldID',
			vcShipFieldValue VARCHAR(200) PATH 'vcShipFieldValue'
	) X;    
 
   RETURN;
END; $$;


