-- Function definition script GetWorkOrderPlannedStartDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWorkOrderPlannedStartDate(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0))
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtCreatedDate  TIMESTAMP;
   v_dtMaxItemExpectedDate  TIMESTAMP;
   v_dtPlannedStartDate  TIMESTAMP;
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   select   bintCreatedDate, coalesce(dtmStartDate,bintCreatedDate), coalesce(WareHouseItems.numWareHouseID,0) INTO v_dtCreatedDate,v_dtPlannedStartDate,v_numWarehouseID FROM
   WorkOrder
   LEFT JOIN
   WareHouseItems
   ON
   WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID;

   SELECT
   MAX(dtItemExpectedDate) INTO v_dtMaxItemExpectedDate FROM(SELECT(CASE
      WHEN EXISTS(SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainId = v_numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = v_numWOID)
      THEN GetWorkOrderPlannedStartDate(v_numDomainID,(SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainId = v_numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = v_numWOID))
      ELSE v_dtCreatedDate+CAST(coalesce(GetVendorPreferredMethodLeadTime(Item.numVendorID,v_numWarehouseID),
         0) || 'day' as interval)
      END) AS dtItemExpectedDate
      FROM
      WorkOrderDetails
      INNER JOIN
      Item
      ON
      WorkOrderDetails.numChildItemID = Item.numItemCode
      INNER JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      INNER JOIN
      WareHouseItems
      ON
      WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
      WHERE
      numWOId = v_numWOID
      AND (EXISTS(SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainId = v_numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = v_numWOID) OR WorkOrderDetails.numQtyItemsReq > coalesce(WareHouseItems.numAllocation,0))) TEMP;

   IF v_dtMaxItemExpectedDate IS NOT NULL AND v_dtMaxItemExpectedDate > v_dtPlannedStartDate then
	
      v_dtPlannedStartDate := v_dtMaxItemExpectedDate;
   end if;

   RETURN v_dtPlannedStartDate;
END; $$;

