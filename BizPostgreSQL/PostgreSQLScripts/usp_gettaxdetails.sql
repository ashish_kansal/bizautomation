-- Stored procedure definition script usp_GetTaxDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetTaxDetails(v_numDomainId NUMERIC(9,0),
v_numTaxItemID NUMERIC(9,0) DEFAULT 0,
v_numCountry NUMERIC(9,0) DEFAULT 0,  
v_numState NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numTaxID as VARCHAR(255)),
		fn_GetListName(numCountryID,0::BOOLEAN) as country,
		CASE WHEN numStateID = 0 THEN 'All' ELSE fn_GetState(numStateID) END AS state,
		cast(decTaxPercentage as VARCHAR(255)),
		CASE
   WHEN TaxDetails.numTaxItemID = 0 OR TaxDetails.numTaxItemID IS NULL THEN 'Sales Tax(Default)'
   WHEN TaxDetails.numTaxItemID = 1 THEN CONCAT('CRV',CASE WHEN coalesce(vcTaxUniqueName,'') = '' THEN '' ELSE ' (' || vcTaxUniqueName || ')' END)
   ELSE coalesce(vcTaxName,'')
   END AS vcTaxName
		,coalesce(vcCity,'') as vcCity
		,coalesce(vcZipPostal,'') as vcZipPostal,
		CASE WHEN tintTaxType = 2 THEN CONCAT(TO_CHAR(decTaxPercentage,'FM9,999,999,999,990.0000'),' (Flat Amount)') ELSE CONCAT(TO_CHAR(decTaxPercentage,'FM9,999,999,999,990.0000'),' %') END AS vcTax,
		tintTaxType
   FROM
   TaxDetails
   LEFT JOIN
   TaxItems
   ON
   TaxItems.numTaxItemID = TaxDetails.numTaxItemID
   WHERE
   TaxDetails.numDomainId = v_numDomainId
   AND (TaxDetails.numTaxItemID = v_numTaxItemID OR v_numTaxItemID = 0)
   AND (TaxDetails.numCountryID = v_numCountry OR v_numCountry = 0)
   AND (TaxDetails.numStateID = v_numState OR v_numState = 0);
END; $$;












