-- Stored procedure definition script USP_ManageShippingGlobalValues for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingGlobalValues(v_numDomainID NUMERIC(9,0)
	,v_minShippingCost DOUBLE PRECISION
	,v_bitEnableStaticShipping BOOLEAN
	,v_bitEnableShippingExceptions BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM Domain
   WHERE numDomainId = v_numDomainID) then
        
      UPDATE Domain
      SET	 minShippingCost = v_minShippingCost,bitEnableStaticShippingRule = v_bitEnableStaticShipping,
      bitEnableShippingExceptions = v_bitEnableShippingExceptions
      WHERE numDomainId = v_numDomainID;
   ELSE
      INSERT INTO Domain(numDomainId
				,minShippingCost
				,bitEnableStaticShippingRule
				,bitEnableShippingExceptions)
			VALUES(v_numDomainID
				,v_minShippingCost
				,v_bitEnableStaticShipping
				,v_bitEnableShippingExceptions);
   end if;
   RETURN;
END; $$;


