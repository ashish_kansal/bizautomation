-- Stored procedure definition script USP_ReportListMaster_Top10SalesOpportunityByRevenue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10SalesOpportunityByRevenue(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_numRecordCount NUMERIC(18,0)
	,v_vcTimeLine VARCHAR(50)
	,v_vcDealAmount TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TodayDate  DATE;
   v_CreatedDate  DATE;
BEGIN
   v_TodayDate := TIMEZONE('UTC',now())+CAST(v_ClientTimeZoneOffset || 'minute' as interval);

   IF v_vcTimeLine = 'Last12Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-12 month';
   end if;
   IF v_vcTimeLine = 'Last6Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-6 month';
   end if;
   IF v_vcTimeLine = 'Last3Months' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-3 month';
   end if;
   IF v_vcTimeLine = 'Last30Days' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-30 day';
   end if;
   IF v_vcTimeLine = 'Last7Days' then
	
      v_CreatedDate := v_TodayDate+INTERVAL '-7 day';
   end if;
	

   open SWV_RefCur for SELECT 
   OM.vcpOppName
		,cast(CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) as VARCHAR(255)) AS URL
		,cast(coalesce(monTotAmount,0) as VARCHAR(255)) AS monTotAmount
		,cast(coalesce(OM.numPercentageComplete,0) as NUMERIC(9,0)) AS numPercentageComplete
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   LEFT JOIN
   Listdetails LD
   ON
		--LD.numListID=50 
   LD.numListID =(v_numRecordCount)
   AND (coalesce(LD.constFlag,false) = true OR LD.numDomainid = v_numDomainID)
   AND LD.numListItemID = OM.numPercentageComplete
   WHERE
   OM.numDomainId = v_numDomainID
   AND coalesce(OM.tintopptype,0) = 1
   AND coalesce(OM.tintoppstatus,0) = 0
		--AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
   AND OM.bintCreatedDate >= v_CreatedDate
   AND (1 =(CASE WHEN POSITION('1-5K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 1000:: DECIMAL(20,5) AND 5000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('5-10K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 5000:: DECIMAL(20,5) AND 10000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('10-20K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 10000:: DECIMAL(20,5) AND 20000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('20-50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 20000:: DECIMAL(20,5) AND 50000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
   OR 1 =(CASE WHEN POSITION('>50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
   ORDER BY
   coalesce(monTotAmount,0) DESC LIMIT 10;
END; $$;












