-- Stored procedure definition script USP_ShippingReport_GetForTrackingDetailUpdate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ShippingReport_GetForTrackingDetailUpdate(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ShippingReport.numShippingReportID
		,ShippingReport.numShippingCompany
		,ShippingBox.vcTrackingNumber
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 91 AND intShipFieldID = 9),'') AS FedexServerURL
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 91 AND intShipFieldID = 6),'') AS FedexAccountNumber
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 91 AND intShipFieldID = 7),'') AS FedexMeterNumber
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 91 AND intShipFieldID = 11),'') AS FedexDeveloperKey
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 91 AND intShipFieldID = 12),'') AS FedexPassword
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 88 AND intShipFieldID = 14),'') AS UPSServerURL
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 88 AND intShipFieldID = 1),'') AS UPSAccessKey
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 88 AND intShipFieldID = 2),'') AS UPSUserID
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 88 AND intShipFieldID = 3),'') AS UPSPassword
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 90 AND intShipFieldID = 17),'') AS USPSServerURL
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 90 AND intShipFieldID = 15),'') AS USPSUserID
		,coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = ShippingReport.numDomainID AND numListItemID = 90 AND intShipFieldID = 16),'') AS USPSPassword
   FROM
   ShippingReport
   INNER JOIN
   OpportunityMaster
   ON
   ShippingReport.numOppID = OpportunityMaster.numOppId
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   INNER JOIN
   ShippingBox
   ON
   ShippingReport.numShippingReportID = ShippingBox.numShippingReportID
   AND coalesce(ShippingBox.bitIsMasterTrackingNo,false) = true
   WHERE
   ShippingReport.numDomainID NOT IN(1,163,166,184,186)
   AND coalesce(ShippingBox.vcTrackingNumber,'') <> ''
   AND coalesce(ShippingReport.numShippingCompany,0) IN(88,90,91)
   AND coalesce(ShippingReport.bitDelivered,false) = false;
   RETURN;
END; $$;













