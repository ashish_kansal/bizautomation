-- Stored procedure definition script USP_GetDateFieldWorkFlows for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDateFieldWorkFlows(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM WorkFlowMaster WFM
   WHERE coalesce(WFM.vcDateField,'') <> '' AND WFM.bitActive = true;
END; $$;















