CREATE OR REPLACE FUNCTION USP_GetPromotionDTLForCartItems(v_numDomainID NUMERIC(18,0),
	v_itemIdSearch VARCHAR(100), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   OPEN SWV_RefCur FOR SELECT * FROM PromotionOfferOrderBased WHERE cSaleType = 'I' AND numProItemId IN (SELECT Id FROM SplitIds(COALESCE(v_itemIdSearch,''),','));
   RETURN;
END; $$;



