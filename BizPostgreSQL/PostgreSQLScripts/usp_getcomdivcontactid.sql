DROP FUNCTION IF EXISTS USP_GetComDivContactID;

CREATE OR REPLACE FUNCTION USP_GetComDivContactID(INOUT v_numCompanyID NUMERIC(9,0) DEFAULT 0 ,  
INOUT v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,  
INOUT v_numContactID NUMERIC(9,0) DEFAULT 0 ,  
INOUT v_vcCompanyName VARCHAR(50) DEFAULT '' ,
v_Email VARCHAR(100) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   A.numContactId, D.numDivisionID, C.numCompanyId, C.vcCompanyName INTO v_numContactID,v_numDivisionID,v_numCompanyID,v_vcCompanyName from AdditionalContactsInformation A
   join DivisionMaster D
   on D.numDivisionID = A.numDivisionId
   join CompanyInfo C
   on C.numCompanyId = D.numCompanyID where vcEmail = v_Email  and vcEmail <> '' and NULLIF(vcEmail,'') is not NULL
   AND A.numDomainID = v_numDomainID    LIMIT 1;

   v_numContactID := COALESCE(v_numContactID,0);
   v_numDivisionID := COALESCE(v_numDivisionID,0);
   v_numCompanyID := COALESCE(v_numCompanyID,0);
   v_vcCompanyName := COALESCE(v_vcCompanyName,'');

   RETURN;
END; $$;


