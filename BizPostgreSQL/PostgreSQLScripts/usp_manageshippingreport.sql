-- Stored procedure definition script USP_ManageShippingReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingReport(v_numDomainId NUMERIC(9,0),
    INOUT v_numShippingReportId NUMERIC(9,0) DEFAULT 0 ,
    v_numOppBizDocId NUMERIC(9,0) DEFAULT NULL,
    v_numShippingCompany NUMERIC(9,0) DEFAULT NULL,
    v_Value1 VARCHAR(100) DEFAULT NULL,
    v_Value2 VARCHAR(100) DEFAULT NULL,
    v_Value3 VARCHAR(100) DEFAULT NULL,
    v_Value4 VARCHAR(100) DEFAULT NULL,
    v_vcFromState VARCHAR(50) DEFAULT '',
    v_vcFromZip VARCHAR(50) DEFAULT '',
    v_vcFromCountry VARCHAR(50) DEFAULT '',
    v_vcToState VARCHAR(50) DEFAULT '',
    v_vcToZip VARCHAR(50) DEFAULT '',
    v_vcToCountry VARCHAR(50) DEFAULT '',
    v_numUserCntId NUMERIC(9,0) DEFAULT NULL,
    v_strItems TEXT DEFAULT NULL,
    v_tintMode SMALLINT DEFAULT 0,
	v_tintPayorType SMALLINT DEFAULT 0,
	v_vcPayorAccountNo VARCHAR(20) DEFAULT '',
	v_numPayorCountry NUMERIC(9,0) DEFAULT NULL,
	v_vcPayorZip VARCHAR(50) DEFAULT '',
	v_vcFromCity VARCHAR(50) DEFAULT '',
    v_vcFromAddressLine1 VARCHAR(50) DEFAULT '',
    v_vcFromAddressLine2 VARCHAR(50) DEFAULT '',
    v_vcToCity VARCHAR(50) DEFAULT '',
    v_vcToAddressLine1 VARCHAR(50) DEFAULT '',
    v_vcToAddressLine2 VARCHAR(50) DEFAULT '',
    v_vcFromName			VARCHAR(1000) DEFAULT '',
    v_vcFromCompany		VARCHAR(1000) DEFAULT '',
    v_vcFromPhone		VARCHAR(100) DEFAULT '',
    v_bitFromResidential	BOOLEAN DEFAULT false,
    v_vcToName			VARCHAR(1000) DEFAULT '',
    v_vcToCompany		VARCHAR(1000) DEFAULT '',
    v_vcToPhone			VARCHAR(100) DEFAULT '',
    v_bitToResidential	BOOLEAN DEFAULT false,
    v_IsCOD					BOOLEAN DEFAULT false,
	v_IsDryIce				BOOLEAN DEFAULT false,
	v_IsHoldSaturday			BOOLEAN DEFAULT false,
	v_IsHomeDelivery			BOOLEAN DEFAULT false,
	v_IsInsideDelevery		BOOLEAN DEFAULT false,
	v_IsInsidePickup			BOOLEAN DEFAULT false,
	v_IsReturnShipment		BOOLEAN DEFAULT false,
	v_IsSaturdayDelivery		BOOLEAN DEFAULT false,
	v_IsSaturdayPickup		BOOLEAN DEFAULT false,
	v_numCODAmount			NUMERIC(18,0) DEFAULT 0,
	v_vcCODType				VARCHAR(50) DEFAULT 'Any',
	v_numTotalInsuredValue	NUMERIC(18,2) DEFAULT 0,
	v_IsAdditionalHandling	BOOLEAN DEFAULT NULL,
	v_IsLargePackage			BOOLEAN DEFAULT NULL,
	v_vcDeliveryConfirmation	VARCHAR(1000) DEFAULT NULL,
	v_vcDescription			TEXT DEFAULT NULL,
	v_numOppId				NUMERIC(18,0) DEFAULT NULL,
	v_numTotalCustomsValue	NUMERIC(18,2) DEFAULT NULL,
	v_tintSignatureType NUMERIC(18,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBoxID  NUMERIC;
   v_hDocItem  INTEGER;                                
                
   v_bitUseShippersAccountNo  BOOLEAN;
   v_numTempDivisionID  NUMERIC(18,0);
   v_vcPayerAccountNo  TEXT;
   v_vcPayerStreet  TEXT;
   v_vcPayerCity  TEXT;
   v_numPayerState  NUMERIC(18,0);
BEGIN
   IF v_numShippingReportId = 0 then
      select   IsCOD, IsHomeDelivery, IsInsideDelevery, IsInsidePickup, IsSaturdayDelivery, IsSaturdayPickup, IsAdditionalHandling, IsLargePackage, vcCODType, vcDeliveryConfirmation, vcDescription, CAST(coalesce(vcSignatureType,'0') AS SMALLINT) INTO v_IsCOD,v_IsHomeDelivery,v_IsInsideDelevery,v_IsInsidePickup,v_IsSaturdayDelivery,
      v_IsSaturdayPickup,v_IsAdditionalHandling,v_IsLargePackage,
      v_vcCODType,v_vcDeliveryConfirmation,v_vcDescription,v_tintSignatureType FROM
      OpportunityMaster
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      DivisionMasterShippingConfiguration
      ON
      DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID WHERE
      OpportunityMaster.numDomainId = v_numDomainId
      AND DivisionMaster.numDomainID = v_numDomainId
      AND DivisionMasterShippingConfiguration.numDomainID = v_numDomainId
      AND OpportunityMaster.numOppId = v_numOppId;
      INSERT  INTO ShippingReport(numOppBizDocId,
               numShippingCompany,
               vcValue1,
               vcValue2,
               vcValue3,
               vcValue4,
               vcFromState,
               vcFromZip,
               vcFromCountry,
               vcToState,
               vcToZip,
               vcToCountry,
               dtCreateDate,
               numCreatedBy,
               numDomainID,
				tintPayorType,
				vcPayorAccountNo,
				numPayorCountry,
				vcPayorZip,
				vcFromCity ,
				vcFromAddressLine1 ,
				vcFromAddressLine2 ,
				vcToCity ,
				vcToAddressLine1 ,
				vcToAddressLine2,
				vcFromName,
				vcFromCompany,
				vcFromPhone,
				bitFromResidential,
				vcToName,
				vcToCompany,
				vcToPhone,
				bitToResidential,
				IsCOD,
				IsDryIce,
				IsHoldSaturday,
				IsHomeDelivery,
				IsInsideDelevery,
				IsInsidePickup,
				IsReturnShipment,
				IsSaturdayDelivery,
				IsSaturdayPickup,
				numCODAmount,
				vcCODType,
				numTotalInsuredValue,
				IsAdditionalHandling,
				IsLargePackage,
				vcDeliveryConfirmation,
				vcDescription,
				numOppID,numTotalCustomsValue,tintSignatureType)
            VALUES(v_numOppBizDocId,
               v_numShippingCompany,
               v_Value1,
               v_Value2,
               v_Value3,
               v_Value4,
               v_vcFromState,
               v_vcFromZip,
               v_vcFromCountry,
               v_vcToState,
               v_vcToZip,
               v_vcToCountry,
               TIMEZONE('UTC',now()),
               v_numUserCntId,
               v_numDomainId,
				v_tintPayorType,
				v_vcPayorAccountNo,
				v_numPayorCountry,
				v_vcPayorZip,
				v_vcFromCity ,
				v_vcFromAddressLine1 ,
				v_vcFromAddressLine2 ,
				v_vcToCity ,
				v_vcToAddressLine1 ,
				v_vcToAddressLine2,
				v_vcFromName,
				v_vcFromCompany,
				v_vcFromPhone,
				v_bitFromResidential,
				v_vcToName,
				v_vcToCompany,
				v_vcToPhone,
				v_bitToResidential,
				v_IsCOD,
				v_IsDryIce,
				v_IsHoldSaturday,
				v_IsHomeDelivery,
				v_IsInsideDelevery,
				v_IsInsidePickup,
				v_IsReturnShipment,
				v_IsSaturdayDelivery,
				v_IsSaturdayPickup,
				v_numCODAmount,
				v_vcCODType,
				v_numTotalInsuredValue,
				v_IsAdditionalHandling,
				v_IsLargePackage,
				v_vcDeliveryConfirmation,
				v_vcDescription,
				v_numOppId,v_numTotalCustomsValue,v_tintSignatureType) RETURNING numShippingReportId INTO v_numShippingReportId;
			
      INSERT INTO ShippingBox(vcBoxName,
				numShippingReportID,
				dtCreateDate,
				numCreatedBy) VALUES( 
				/* vcBoxName - varchar(20) */ 'Box1',
				/* numShippingReportId - numeric(18, 0) */ v_numShippingReportId,
				/* dtCreateDate - datetime */ TIMEZONE('UTC',now()),
				/* numCreatedBy - numeric(18, 0) */ v_numUserCntId) RETURNING numBoxID INTO v_numBoxID;
			
--				END
                
                
      IF (v_strItems IS NOT NULL) then
     
				 --numShippingReportId  NUMERIC(18,0),
         INSERT  INTO ShippingReportItems(numShippingReportId,
							numItemCode,
							tintServiceType,
							dtDeliveryDate,
							monShippingRate,
							fltTotalWeight,
							intNoOfBox,
							fltHeight,
							fltWidth,
							fltLength,
							dtCreateDate,
							numCreatedBy,
							numBoxID,
							numOppBizDocItemID)
         SELECT  v_numShippingReportId,--X.[numShippingReportId],
								X.numItemCode,
								X.tintServiceType,
								X.dtDeliveryDate,
								X.monShippingRate,
								X.fltTotalWeight,
								X.intNoOfBox,
								X.fltHeight,
								X.fltWidth,
								X.fltLength,
								TIMEZONE('UTC',now()),--X.[dtCreateDate],
								v_numUserCntId,
								v_numBoxID,
								CASE v_tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE(SELECT  numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode LIMIT 1) END AS numOppBizDocItemID
         FROM
		  XMLTABLE
			(
				'NewDataSet/Items'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numItemCode NUMERIC(18,0) PATH 'numItemCode'
					,tintServiceType SMALLINT PATH 'tintServiceType'
					,dtDeliveryDate TIMESTAMP PATH 'dtDeliveryDate'
					,monShippingRate DECIMAL(20,5) PATH 'monShippingRate'
					,fltTotalWeight DOUBLE PRECISION PATH 'fltTotalWeight'
					,intNoOfBox INTEGER PATH 'intNoOfBox'
					,fltHeight DOUBLE PRECISION PATH 'fltHeight'
					,fltWidth DOUBLE PRECISION PATH 'fltWidth'
					,fltLength DOUBLE PRECISION PATH 'fltLength'
					,numOppBizDocItemID NUMERIC PATH 'numOppBizDocItemID'
			)  X;
     
      end if;
      IF coalesce(v_numOppId,0) > 0 then
         select   coalesce(bitUseShippersAccountNo,false), DivisionMaster.numDivisionID INTO v_bitUseShippersAccountNo,v_numTempDivisionID FROM
         OpportunityMaster
         INNER JOIN
         DivisionMaster
         ON
         OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE
         OpportunityMaster.numDomainId = v_numDomainId
         AND OpportunityMaster.numOppId = v_numOppId;
         IF coalesce(v_bitUseShippersAccountNo,false) = true AND coalesce(v_tintPayorType,0) <> 2 AND EXISTS(SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID = v_numTempDivisionID AND numShipViaID = v_numShippingCompany) then
				
            select   coalesce(vcAccountNumber,''), coalesce(vcStreet,''), coalesce(vcCity,''), numCountry, numState, vcZipCode INTO v_vcPayerAccountNo,v_vcPayerStreet,v_vcPayerCity,v_numPayorCountry,v_numPayerState,
            v_vcPayorZip FROM
            DivisionMasterShippingAccount WHERE
            numDivisionID = v_numTempDivisionID
            AND numShipViaID = v_numShippingCompany;
            UPDATE
            ShippingReport
            SET
            tintPayorType = 2,vcPayorAccountNo = v_vcPayerAccountNo,vcPayerStreet = v_vcPayerStreet,
            vcPayerCity = v_vcPayerCity,numPayorCountry = v_numPayorCountry,
            numPayerState = v_numPayerState,vcPayorZip = v_vcPayorZip
            WHERE
            numShippingReportID = v_numShippingReportId;
         end if;
      end if;
   ELSEIF v_numShippingReportId > 0
   then
        
      UPDATE
      ShippingReport
      SET
      numShippingCompany = v_numShippingCompany,vcValue1 = v_Value1,vcValue2 = v_Value2,
      vcValue3 = v_Value3,vcValue4 = v_Value4,vcFromState = v_vcFromState,
      vcFromZip = v_vcFromZip,vcFromCountry = v_vcFromCountry,vcToState = v_vcToState,
      vcToZip = v_vcToZip,vcToCountry = v_vcToCountry,tintPayorType = v_tintPayorType,
      vcPayorAccountNo = v_vcPayorAccountNo,numPayorCountry = v_numPayorCountry,
      vcPayorZip = v_vcPayorZip,vcFromCity = v_vcFromCity,
      vcFromAddressLine1 = v_vcFromAddressLine1,vcFromAddressLine2 = v_vcFromAddressLine2,
      vcToCity = v_vcToCity,vcToAddressLine1 = v_vcToAddressLine1,
      vcToAddressLine2 = v_vcToAddressLine2,vcFromName = v_vcFromName,vcFromCompany = v_vcFromCompany,
      vcFromPhone = v_vcFromPhone,bitFromResidential = v_bitFromResidential,
      vcToName = v_vcToName,vcToCompany = v_vcToCompany,
      vcToPhone = v_vcToPhone,bitToResidential = v_bitToResidential,
      IsCOD = v_IsCOD,IsDryIce = v_IsDryIce,IsHoldSaturday  = v_IsHoldSaturday,
      IsHomeDelivery = v_IsHomeDelivery,IsInsideDelevery = v_IsInsideDelevery,
      IsInsidePickup = v_IsInsidePickup,IsReturnShipment = v_IsReturnShipment,
      IsSaturdayDelivery = v_IsSaturdayDelivery,IsSaturdayPickup = v_IsSaturdayPickup,
      numCODAmount = v_numCODAmount,vcCODType = v_vcCODType,
      numTotalInsuredValue = v_numTotalInsuredValue,IsAdditionalHandling = v_IsAdditionalHandling,
      IsLargePackage = v_IsLargePackage,vcDeliveryConfirmation = v_vcDeliveryConfirmation,
      vcDescription = v_vcDescription,numOppID = v_numOppId,
      numTotalCustomsValue = v_numTotalCustomsValue,tintSignatureType = v_tintSignatureType
      WHERE
      numShippingReportID = v_numShippingReportId;
   end if;
   RETURN;
END; $$;


