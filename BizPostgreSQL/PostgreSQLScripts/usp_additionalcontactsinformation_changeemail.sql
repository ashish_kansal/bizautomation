-- Stored procedure definition script USP_AdditionalContactsInformation_ChangeEmail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_AdditionalContactsInformation_ChangeEmail(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numContactID NUMERIC(18,0)
	,v_vcEmail VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   AdditionalContactsInformation
   SET
   vcEmail = v_vcEmail,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now())
   WHERE
   numDomainID = v_numDomainID
   AND numContactId = v_numContactID;
   RETURN;
END; $$;


