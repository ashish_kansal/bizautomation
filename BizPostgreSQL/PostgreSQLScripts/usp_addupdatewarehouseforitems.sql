DROP FUNCTION IF EXISTS USP_AddUpdateWareHouseForItems;

CREATE OR REPLACE FUNCTION USP_AddUpdateWareHouseForItems
(
	v_numItemCode NUMERIC(9,0) DEFAULT 0,  
	v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
	INOUT v_numWareHouseItemID NUMERIC(9,0) DEFAULT 0 ,
	v_vcLocation VARCHAR(250) DEFAULT '',
	v_monWListPrice DECIMAL(20,5) DEFAULT 0,
	v_numOnHand DOUBLE PRECISION DEFAULT 0,
	v_numReorder DOUBLE PRECISION DEFAULT 0,
	v_vcWHSKU VARCHAR(100) DEFAULT '',
	v_vcBarCode VARCHAR(50) DEFAULT '',
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_strFieldList TEXT DEFAULT '',
	v_vcSerialNo VARCHAR(100) DEFAULT '',
	v_vcComments VARCHAR(1000) DEFAULT '',
	v_numQty NUMERIC(18,0) DEFAULT 0,
	v_byteMode SMALLINT DEFAULT 0,
	v_numWareHouseItmsDTLID NUMERIC(18,0) DEFAULT 0,
	v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	v_dtAdjustmentDate TIMESTAMP DEFAULT NULL,
	v_numWLocationID NUMERIC(9,0) DEFAULT 0
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);
   v_numGlobalWarehouseItemID  NUMERIC(18,0);
   v_bitLotNo  BOOLEAN DEFAULT 0;	
   v_bitSerialized  BOOLEAN DEFAULT 0;
   v_bitMatrix  BOOLEAN DEFAULT 0;
   v_numItemGroup  NUMERIC(18,0) DEFAULT 0;
   v_vcSKU  VARCHAR(200) DEFAULT '';
   v_vcUPC  VARCHAR(200) DEFAULT '';
   v_monListPrice  DECIMAL(20,5);
   v_bitRemoveGlobalWarehouse  BOOLEAN;

   v_vcDescription  VARCHAR(100);
   v_OldQty  DOUBLE PRECISION DEFAULT 0;

   v_bitOppOrderExists  BOOLEAN DEFAULT 0;
   v_hDoc  INTEGER;
   v_rows  INTEGER;
BEGIN
   v_numDomain := v_numDomainID;

   select   coalesce(bitRemoveGlobalLocation,false) INTO v_bitRemoveGlobalWarehouse FROM Domain WHERE numDomainId = v_numDomainID;

   select   coalesce(Item.bitLotNo,false), coalesce(Item.bitSerialized,false), coalesce(bitMatrix,false), coalesce(v_numItemGroup,0), coalesce(vcSKU,''), coalesce(numBarCodeId,''), coalesce(monListPrice,0) INTO v_bitLotNo,v_bitSerialized,v_bitMatrix,v_numItemGroup,v_vcSKU,v_vcUPC,
   v_monListPrice FROM
   Item WHERE
   numItemCode = v_numItemCode
   AND numDomainID = v_numDomainID;

   IF v_byteMode = 0 or v_byteMode = 1 or v_byteMode = 2 or v_byteMode = 5 then
	
		--Insert/Update WareHouseItems
      IF v_byteMode = 0 or v_byteMode = 1 then
		
         IF v_numWareHouseItemID > 0 then
			
            IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numWareHouseItemID = v_numWareHouseItemID) then
				
               RAISE EXCEPTION 'WAREHOUSE_DETAIL_NOT_FOUND';
            end if;
            UPDATE
            WareHouseItems
            SET
            numWareHouseID = v_numWareHouseID,numReorder = v_numReorder,monWListPrice = v_monWListPrice,
            vcLocation = v_vcLocation,numWLocationID = v_numWLocationID,
            vcWHSKU = v_vcWHSKU,vcBarCode = v_vcBarCode,dtModified = LOCALTIMESTAMP
            WHERE
            numItemID = v_numItemCode
            AND numDomainID = v_numDomainID
            AND numWareHouseItemID = v_numWareHouseItemID;
            IF NOT EXISTS(SELECT numItemCode FROM OpportunityItems WHERE numItemCode = v_numItemCode) AND NOT EXISTS(SELECT numChildItemID FROM OpportunityKitItems WHERE numChildItemID = v_numItemCode) AND NOT EXISTS(SELECT numItemID FROM OpportunityKitChildItems WHERE numItemID = v_numItemCode) then
				
               IF ((SELECT coalesce(numOnHand,0) FROM WareHouseItems where numItemID = v_numItemCode AND numDomainID = v_numDomainID and numWareHouseItemID = v_numWareHouseItemID) = 0) then
					
                  UPDATE
                  WareHouseItems
                  SET
                  numOnHand = v_numOnHand
                  WHERE
                  numItemID = v_numItemCode
                  AND numDomainID = v_numDomainID
                  AND numWareHouseItemID = v_numWareHouseItemID;
               end if;
            end if;
            v_vcDescription := 'UPDATE WareHouse';
         ELSE
            IF NOT EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode) then
				
               RAISE EXCEPTION 'INVALID_ITEM_CODE';
            end if;
            INSERT INTO WareHouseItems(numItemID,
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID)
				VALUES(v_numItemCode,
					v_numWareHouseID,
					(Case When v_bitLotNo = true or v_bitSerialized = true then 0 else v_numOnHand end),
					v_numReorder,
					v_monWListPrice,
					v_vcLocation,
					v_vcWHSKU,
					v_vcBarCode,
					v_numDomainID,
					LOCALTIMESTAMP ,
					v_numWLocationID) RETURNING numWareHouseItemID INTO v_numWareHouseItemID;
				
            IF(SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID = v_numWareHouseID AND numItemID = v_numItemCode AND numWLocationID = -1) = 0 AND v_bitRemoveGlobalWarehouse = false then
				
               INSERT INTO WareHouseItems(numItemID,
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified)
					VALUES(v_numItemCode,
						v_numWareHouseID,
						-1,
						0,
						0,
						v_monWListPrice,
						'',
						v_vcWHSKU,
						v_vcBarCode,
						v_numDomainID,
						LOCALTIMESTAMP) RETURNING numWareHouseItemID INTO v_numGlobalWarehouseItemID;
				
            end if;
            v_vcDescription := 'INSERT WareHouse';
         end if;
      end if;

		--Insert/Update WareHouseItmsDTL

      IF v_byteMode = 0 or v_byteMode = 2 or v_byteMode = 5 then
		
         IF v_bitLotNo = true or v_bitSerialized = true then
			
            IF v_bitSerialized = true then
               v_numQty := 1;
            end if;
            IF v_byteMode = 0 then
				
               select   numWareHouseItmsDTLID, numQty INTO v_numWareHouseItmsDTLID,v_OldQty FROM
               WareHouseItmsDTL WHERE
               numWareHouseItemID = v_numWareHouseItemID
               AND vcSerialNo = v_vcSerialNo and numQty > 0    LIMIT 1;
            ELSEIF v_numWareHouseItmsDTLID > 0
            then
				
               select   numQty INTO v_OldQty FROM
               WareHouseItmsDTL WHERE
               numWareHouseItmsDTLID = v_numWareHouseItmsDTLID    LIMIT 1;
            end if;
            IF v_numWareHouseItmsDTLID > 0 then
				
               UPDATE
               WareHouseItmsDTL
               SET
               vcComments = v_vcComments,numQty = v_numQty,vcSerialNo = v_vcSerialNo
               WHERE
               numWareHouseItmsDTLID = v_numWareHouseItmsDTLID
               AND numWareHouseItemID = v_numWareHouseItemID;
               v_vcDescription := CASE WHEN v_byteMode = 5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END || coalesce(v_vcSerialNo,'');
            ELSE
               INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)
				   VALUES(v_numWareHouseItemID,v_vcSerialNo,v_vcComments,v_numQty,0) RETURNING numWareHouseItmsDTLID INTO v_numWareHouseItmsDTLID;
				   
               v_vcDescription := CASE WHEN v_byteMode = 5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END || coalesce(v_vcSerialNo,'');
            end if;
            UPDATE
            WareHouseItems
            SET
            numOnHand = numOnHand+(v_numQty -v_OldQty),dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID
            AND numDomainID = v_numDomainID
            AND(numOnHand+(v_numQty -v_OldQty)) >= 0;
         end if;
      end if;
      IF(SELECT COUNT(OI.numOppId) FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode) > 0 then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitItems OI WHERE OI.numChildItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      ELSEIF(SELECT COUNT(OI.numOppId) FROM OpportunityKitChildItems OI WHERE OI.numItemID = v_numItemCode) > 0
      then
		
         v_bitOppOrderExists := true;
      end if;
		 --  numeric(9, 0)
		 --  numeric(9, 0)
		 --  tinyint
		 --  varchar(100)
      IF v_bitMatrix = true AND coalesce(v_bitOppOrderExists,false) <> true then -- ITEM LEVEL ATTRIBUTES
		
         IF coalesce(v_numWareHouseItemID,0) > 0 then
			
            DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = v_numWareHouseItemID;
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized)
            SELECT
            Fld_ID,
					coalesce(CAST(Fld_Value AS VARCHAR(30)),'') AS Fld_Value,
					v_numWareHouseItemID,
					false
            FROM
            ItemAttributes
            WHERE
            numDomainID = v_numDomainID
            AND numItemCode = v_numItemCode;
         end if;
         IF coalesce(v_numGlobalWarehouseItemID,0) > 0 then
			
            DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = v_numGlobalWarehouseItemID;
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized)
            SELECT
            Fld_ID,
					coalesce(CAST(Fld_Value AS VARCHAR(30)),'') AS Fld_Value,
					v_numGlobalWarehouseItemID,
					false
            FROM
            ItemAttributes
            WHERE
            numDomainID = v_numDomainID
            AND numItemCode = v_numItemCode;
         end if;
      ELSEIF OCTET_LENGTH(v_strFieldList) > 2 AND coalesce(v_bitOppOrderExists,false) <> true
      then
		
         select 
			count(*) INTO v_rows
		FROM 
		XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strFieldList AS XML)
				COLUMNS
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(100) PATH 'Fld_Value'
			) AS X;

         if v_rows > 0 then
			
            BEGIN
               CREATE TEMP SEQUENCE tt_tempTable_seq;
               EXCEPTION WHEN OTHERS THEN
                  NULL;
            END;
            drop table IF EXISTS tt_TEMPTABLE CASCADE;
            Create TEMPORARY TABLE tt_TEMPTABLE 
            (
               ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
               Fld_ID NUMERIC(9,0),
               Fld_Value VARCHAR(100)
            );
            insert into tt_TEMPTABLE(Fld_ID,Fld_Value)
            SELECT Fld_ID,Fld_Value FROM XMLTABLE
			(
				'NewDataSet/CusFlds'
				PASSING 
					CAST(v_strFieldList AS XML)
				COLUMNS
					Fld_ID NUMERIC(9,0) PATH 'Fld_ID',
					Fld_Value VARCHAR(100) PATH 'Fld_Value'
			) AS X;
            DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID AND C.RecId = v_numWareHouseItemID;
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)
            select Fld_ID,coalesce(Fld_Value,'') as Fld_Value,v_numWareHouseItemID,false from tt_TEMPTABLE;
            drop table IF EXISTS tt_TEMPTABLE CASCADE;
         end if;
 
      end if;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
      v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,v_dtRecordDate := v_dtAdjustmentDate,
      v_numDomainID := v_numDomain,SWV_RefCur := null);
      IF coalesce(v_numGlobalWarehouseItemID,0) > 0 then
		
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numGlobalWarehouseItemID,v_numReferenceID := v_numItemCode,
         v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
         v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,
         v_dtRecordDate := v_dtAdjustmentDate,v_numDomainID := v_numDomainID,SWV_RefCur := null);
      end if;

		-- KEEP IT LAST IN SECTION
      IF coalesce(v_numItemGroup,0) > 0 OR coalesce(v_bitMatrix,false) = true then -- IF IT's MATRIX ITEM THEN 
		
         UPDATE WareHouseItems SET monWListPrice =(CASE WHEN v_monListPrice > 0 THEN v_monListPrice ELSE monWListPrice END),vcWHSKU = v_vcSKU,vcBarCode = v_vcUPC WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode;
      end if;
   ELSEIF v_byteMode = 3
   then
	
      IF EXISTS(SELECT * FROM OpportunityMaster OM INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId WHERE OM.numDomainId = v_numDomainID AND numWarehouseItmsID = v_numWareHouseItemID) then
		
         RAISE EXCEPTION 'OpportunityItems_Depend';
         RETURN;
      end if;
      IF EXISTS(SELECT * FROM OpportunityItemsReceievedLocation WHERE numDomainId = v_numDomainID AND numWarehouseItemID = v_numWareHouseItemID) then
		
         RAISE EXCEPTION 'OpportunityItems_Depend';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId = v_numWareHouseItemID) > 0 then
 		
         RAISE EXCEPTION 'OpportunityKitItems_Depend';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numWareHouseItemId = v_numWareHouseItemID) > 0 then
 		
         RAISE EXCEPTION 'OpportunityKitChildItems_Depend';
         RETURN;
      end if;
      IF(SELECT COUNT(*) FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWareHouseItemId = v_numWareHouseItemID) > 0 then
 		
         RAISE EXCEPTION 'WorkOrder_Depend';
         RETURN;
      end if;
      IF v_bitLotNo = true OR v_bitSerialized = true then
		
         DELETE from CFW_Fld_Values_Serialized_Items where RecId in(select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID = v_numWareHouseItemID)
         and bitSerialized =(Case when v_bitLotNo = true or v_bitSerialized = true then true else false end);
      ELSE
         DELETE from CFW_Fld_Values_Serialized_Items where RecId = v_numWareHouseItemID
         and bitSerialized =(Case when v_bitLotNo = true or v_bitSerialized = true then true else false end);
      end if;
      DELETE from WareHouseItmsDTL where numWareHouseItemID = v_numWareHouseItemID;
      DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID = v_numWareHouseItemID and numDomainID = v_numDomainID;
      DELETE from WareHouseItems where numWareHouseItemID = v_numWareHouseItemID and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 4
   then
	
      IF v_bitLotNo = true or v_bitSerialized = true then
		
         DELETE from CFW_Fld_Values_Serialized_Items where RecId = v_numWareHouseItmsDTLID
         and bitSerialized =(Case when v_bitLotNo = true or v_bitSerialized = true then true else false end);
      end if;
      update WareHouseItems WHI SET numOnHand = WHI.numOnHand -WHID.numQty,dtModified = LOCALTIMESTAMP
      from WareHouseItmsDTL WHID
      where WHI.numWareHouseItemID = WHID.numWareHouseItemID AND(WHID.numWareHouseItmsDTLID = v_numWareHouseItmsDTLID
      AND WHI.numDomainID = v_numDomainID
      AND(WHI.numOnHand -WHID.numQty) >= 0);
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
      select   WHI.numWareHouseItemID, numItemID INTO v_numWareHouseItemID,v_numItemCode from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID = WHID.numWareHouseItemID where WHID.numWareHouseItmsDTLID = v_numWareHouseItmsDTLID
      AND WHI.numDomainID = v_numDomainID;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := 'DELETE Lot/Serial#',
      v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,
      v_dtRecordDate := NULL,v_numDomainID := v_numDomain,SWV_RefCur := null);
      DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
   end if;
END; $$;


