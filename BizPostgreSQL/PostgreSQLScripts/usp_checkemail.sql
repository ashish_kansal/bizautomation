-- Stored procedure definition script USP_checkEmail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_checkEmail(v_vcEmail VARCHAR(50) DEFAULT '',
 v_numDomainId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  vcEmail from AdditionalContactsInformation   where LOWER(vcEmail) = LOWER(v_vcEmail)
   and numDomainID = v_numDomainId LIMIT 1;
END; $$;












