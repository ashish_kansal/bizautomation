-- Stored procedure definition script USP_GetWarehouseForSF for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetWarehouseForSF(v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select numWareHouseID,vcWareHouse from Warehouses W
   where numDomainID = v_numDomainID;



   open SWV_RefCur2 for
   select WS.numWareHouseID,vcWareHouse from Warehouses W
   Join WareHouseForSalesFulfillment WS
   on WS.numWareHouseID = W.numWareHouseID
   where WS.numDomainID = v_numDomainID;
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseID]    Script Date: 07/26/2008 16:18:50 ******/
   RETURN;
END; $$;


