-- Stored procedure definition script usp_GetLayoutInfoDdl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetLayoutInfoDdl(v_numUserCntId NUMERIC(9,0) DEFAULT 0,                              
v_intcoulmn NUMERIC(9,0) DEFAULT NULL,                              
v_numDomainID NUMERIC(9,0) DEFAULT 0 ,                    
--@Ctype as char ,              
v_PageId NUMERIC(9,0) DEFAULT 4,              
v_numRelation NUMERIC(9,0) DEFAULT 0,
v_numFormID NUMERIC(9,0) DEFAULT 0,
v_tintPageType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(v_intcoulmn <> 0) then--Fields added to layout

      IF(v_tintPageType = 3) then

         IF((SELECT COUNT(*) FROM View_DynamicColumns WHERE      numUserCntID = v_numUserCntId and  numDomainID = v_numDomainID
         and numFormId = v_numFormID and numRelCntType = v_numRelation
         AND tintPageType = v_tintPageType AND
         1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
         WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
         ELSE 0 END)) = 0) then

            INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
            SELECT numFormID,numFieldID,intColumnNum,intRowNum,v_numDomainID,numGroupID,v_numRelation,v_tintPageType,bitCustom,v_numUserCntId,numFormFieldGroupId,true FROM
            BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID
            AND numFormID = v_numFormID AND tintPageType = v_tintPageType
            AND numRelCntType = v_numRelation
            AND numGroupID =(SELECT  numGroupID FROM  UserMaster WHERE  numDomainID =  v_numDomainID AND numUserDetailId = v_numUserCntId LIMIT 1);
         end if;
      end if;
      open SWV_RefCur for
      SELECT coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldID,
CAST(bitCustom AS CHAR(1)) as bitCustomField,tintRow,bitRequired,coalesce(bitDefaultByAdmin,false) AS bitDefaultByAdmin
      FROM View_DynamicColumns
      WHERE numUserCntID = v_numUserCntId and  numDomainID = v_numDomainID  and tintColumn = v_intcoulmn
      and numFormId = v_numFormID and coalesce(bitCustom,false) = false and numRelCntType = v_numRelation
      AND tintPageType = v_tintPageType AND
      1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
      WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
      ELSE 0 END)  order by tintRow;
      if v_PageId = 1 or v_PageId = 4  OR v_PageId = 12 OR v_PageId = 13 OR v_PageId = 14 then
 
         open SWV_RefCur2 for
         select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,coalesce(bitDefaultByAdmin,false) AS bitDefaultByAdmin
         from View_DynamicCustomColumns_RelationShip
         where grp_id = v_PageId and numDomainID = v_numDomainID and CAST(subgrp AS INT) = 0
         and numUserCntID = v_numUserCntId and tintColumn = v_intcoulmn
         AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelation AND tintPageType = v_tintPageType;
      end if;
      if v_PageId = 2 or  v_PageId = 3 or v_PageId = 5 or v_PageId = 6 or v_PageId = 7 or v_PageId = 8    or v_PageId = 11 then

         open SWV_RefCur2 for
         select vcFieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,coalesce(bitDefaultByAdmin,false) AS bitDefaultByAdmin
         from View_DynamicCustomColumns
         where Grp_id = v_PageId and numDomainID = v_numDomainID and CAST(subgrp AS INT) = 0
         and numUserCntID = v_numUserCntId and  tintColumn = v_intcoulmn
         AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelation AND tintPageType = v_tintPageType;
      end if;
   end if;                        
                        
                          
   if v_intcoulmn = 0 then --Available Fields

      open SWV_RefCur for
      SELECT coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldID,false as bitCustomField,bitRequired, false AS bitDefaultByAdmin
      FROM View_DynamicDefaultColumns
      WHERE numFormId = v_numFormID AND numDomainID = v_numDomainID  AND
      1 =(CASE WHEN v_tintPageType = 2 THEN CASE WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
      WHEN v_tintPageType = 3 THEN CASE WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
      ELSE 0 END)
      AND numFieldID NOT IN(SELECT numFieldID FROM View_DynamicColumns
         WHERE numUserCntID = v_numUserCntId and  numDomainID = v_numDomainID
         and numFormId = v_numFormID and coalesce(bitCustom,false) = false and numRelCntType = v_numRelation
         AND tintPageType = v_tintPageType AND 1 =(CASE WHEN v_tintPageType = 2 THEN Case WHEN coalesce(bitAddField,false) = true THEN 1 ELSE 0 end
         WHEN v_tintPageType = 3 THEN Case WHEN coalesce(bitDetailField,false) = true THEN 1 ELSE 0 END
         ELSE 0 END)) ORDER BY coalesce(vcCultureFieldName,vcFieldName);
      if v_PageId = 4 or v_PageId = 1  OR v_PageId = 12 OR v_PageId = 13  OR v_PageId = 14 then

         open SWV_RefCur2 for
         select fld_label as vcfieldName ,fld_id as numFieldId,true as bitCustomField,false AS bitRequired, false AS bitDefaultByAdmin
         from CFW_Fld_Master CFM
         left join CFw_Grp_Master on CAST(subgrp AS INT) = CFM.Grp_id
         where CFM.Grp_id = v_PageId and CFM.numDomainID = v_numDomainID and CAST(subgrp AS INT) = 0
         AND fld_id not IN(SELECT numFieldId FROM View_DynamicCustomColumns_RelationShip where
            numUserCntID = v_numUserCntId and  numDomainID = v_numDomainID
            AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelation AND tintPageType = v_tintPageType)
         order by CAST(subgrp AS INT);
      end if;
      if v_PageId = 2 or  v_PageId = 3 or v_PageId = 5 or v_PageId = 6 or v_PageId = 7 or v_PageId = 8   or v_PageId = 11 then

         open SWV_RefCur2 for
         select fld_label as vcfieldName ,fld_id as numFieldId,true as bitCustomField,false AS bitRequired,false AS bitDefaultByAdmin
         from CFW_Fld_Master CFM left join CFw_Grp_Master on CAST(subgrp AS INT) = CFM.Grp_id
         where CFM.Grp_id = v_PageId and CFM.numDomainID = v_numDomainID and CAST(subgrp AS INT) = 0
         AND fld_id not IN(SELECT numFieldId FROM View_DynamicCustomColumns where
            numUserCntID = v_numUserCntId and  numDomainID = v_numDomainID
            AND numFormId = v_numFormID and coalesce(bitCustom,false) = true and numRelCntType = v_numRelation AND tintPageType = v_tintPageType)
         order by CAST(subgrp AS INT);
      end if;
   end if;
   RETURN;
END; $$;


